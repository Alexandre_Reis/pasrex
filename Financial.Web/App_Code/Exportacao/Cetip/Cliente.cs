using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using FileHelpers;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.WebConfigConfiguration;
using Financial.Investidor.Enums;
using Financial.Interfaces.Export.RendaFixa;

namespace Financial.Export.Cetip
{
    /// <summary>
    /// Summary description for Cliente
    /// </summary>
    public class Cliente
    {
        public void ExportaArquivo(DateTime dataExportacao, out string conteudoArquivo, out string nomeArquivo)
        {
            ClienteCollection clientes = new ClienteCollection();
            ClienteCetipViewModel clienteCetipViewModel = clientes.ExportaClienteCetip(dataExportacao);

            nomeArquivo = String.Format("cliente_cetip_{0}.txt", dataExportacao.ToString("yyyyMMdd"));

            FileHelperEngine engine = new FileHelperEngine(typeof(HeaderClienteCetip));

            List<HeaderClienteCetip> headers = new List<HeaderClienteCetip>();
            headers.Add(clienteCetipViewModel.header);

            engine = new FileHelperEngine(typeof(HeaderClienteCetip));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            conteudoArquivo = engine.WriteString(headers);

            foreach (RegistroClienteCetip registroClienteCetip in clienteCetipViewModel.registros)
            {
                engine = new FileHelperEngine(typeof(DadosIdentificacaoClienteCetip));
                engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                List<DadosIdentificacaoClienteCetip> dadosIdentificacaoClienteCetip = new List<DadosIdentificacaoClienteCetip>();
                dadosIdentificacaoClienteCetip.Add(registroClienteCetip.dadosIdentificacao);
                conteudoArquivo += engine.WriteString(dadosIdentificacaoClienteCetip);

                engine = new FileHelperEngine(typeof(DadosBasicosClienteCetip));
                engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                List<DadosBasicosClienteCetip> dadosBasicosClienteCetip = new List<DadosBasicosClienteCetip>();
                dadosBasicosClienteCetip.Add(registroClienteCetip.dadosBasicos);
                conteudoArquivo += engine.WriteString(dadosBasicosClienteCetip);

                engine = new FileHelperEngine(typeof(EnderecoClienteCetip));
                engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                List<EnderecoClienteCetip> enderecoClienteCetip = new List<EnderecoClienteCetip>();
                enderecoClienteCetip.Add(registroClienteCetip.endereco);
                conteudoArquivo += engine.WriteString(enderecoClienteCetip);
                
                if (registroClienteCetip.detalhesPF != null)
                {
                    engine = new FileHelperEngine(typeof(DetalhesPessoaFisicaClienteCetip));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    List<DetalhesPessoaFisicaClienteCetip> detalhesPessoaFisicaClienteCetip = new List<DetalhesPessoaFisicaClienteCetip>();
                    detalhesPessoaFisicaClienteCetip.Add(registroClienteCetip.detalhesPF);
                    conteudoArquivo += engine.WriteString(detalhesPessoaFisicaClienteCetip);
                }
                else
                {
                    engine = new FileHelperEngine(typeof(DetalhesPessoaJuridicaClienteCetip));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    List<DetalhesPessoaJuridicaClienteCetip> detalhesPessoaJuridicaClienteCetip = new List<DetalhesPessoaJuridicaClienteCetip>();
                    detalhesPessoaJuridicaClienteCetip.Add(registroClienteCetip.detalhesPJ);
                    conteudoArquivo += engine.WriteString(detalhesPessoaJuridicaClienteCetip);
                }
            }


            //List<FooterClienteCetip> footers = new List<FooterClienteCetip>();
            //footers.Add(clienteCetipViewModel.footer);
            //engine = new FileHelperEngine(typeof(FooterClienteCetip));
            //engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            //conteudoArquivo += engine.WriteString(footers);
        }
    }
}
