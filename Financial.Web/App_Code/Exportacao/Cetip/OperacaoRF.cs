﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using FileHelpers;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.WebConfigConfiguration;
using Financial.Investidor.Enums;
using Financial.Interfaces.Export.RendaFixa;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;

namespace Financial.Export.Cetip
{
    /// <summary>
    /// Summary description for Cliente
    /// </summary>
    public class OperacaoRF
    {
        public void ExportaArquivo(DateTime dataExportacao, out MemoryStream ms, out string nomeArquivoZip)
        {

            ms = new MemoryStream();
            nomeArquivoZip = String.Format("operacoes_cetip_{0}.zip", dataExportacao.ToString("yyyyMMdd"));

            List<string> nomeArquivosExportados = new List<string>();

            string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
            if (!path.Trim().EndsWith("\\"))
            {
                path += "\\";
            }
            path = path.Replace("\\", "\\\\");

            string login = HttpContext.Current.User.Identity.Name;
            path += login + @"\";

            // Se Diretorio Existe Apaga o Diretorio
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            // Cria o Diretorio
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            OperacaoRendaFixaCollection operacoes = new OperacaoRendaFixaCollection();

            Dictionary<int, string> tiposOperacaoExportados = new Dictionary<int, string>();
            
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CCB, "CCB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CDB, "CDB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.Debenture, "DEB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CRI, "CRI");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LF, "LF");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CRA, "CRA");

            /*tiposOperacaoExportados.Add((int)ClasseRendaFixa.CCB, "CCB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CCI, "CCI");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CDA, "CDA");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CDB, "CDB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CDCA, "CDCA");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CPR, "CPR");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CRA, "CRA");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.CRI, "CRI");            
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.Debenture, "DEB");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.DPGE, "DPGE");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LC, "LC");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LCA, "LCA");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LCI, "LCI");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LF, "LF");
            tiposOperacaoExportados.Add((int)ClasseRendaFixa.LH, "LH");*/

            foreach (KeyValuePair<int, string> tipoOperacaoExportado in tiposOperacaoExportados)
            {
                OperacaoCetipViewModel operacaoCetipViewModel = operacoes.ExportaOperacaoCetip(dataExportacao, tipoOperacaoExportado.Key);

                if (operacaoCetipViewModel.registros.Count > 0)
                {
                    string nomeArquivoExportado = String.Format("{0}_cetip_{1}.txt", tipoOperacaoExportado.Value, dataExportacao.ToString("yyyyMMdd"));

                    FileHelperEngine engine = new FileHelperEngine(typeof(HeaderOperacaoCetip));

                    List<HeaderOperacaoCetip> headers = new List<HeaderOperacaoCetip>();
                    headers.Add(operacaoCetipViewModel.header);

                    engine = new FileHelperEngine(typeof(HeaderOperacaoCetip));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    string output = engine.WriteString(headers);

                    engine = new FileHelperEngine(typeof(RegistroOperacaoCetip));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    output += engine.WriteString(operacaoCetipViewModel.registros);

                    nomeArquivosExportados.Add(nomeArquivoExportado);

                    File.WriteAllText(path + nomeArquivoExportado, output);
                }
            
            }

            //Zipar os diretórios
            Archive arquivoZipado = new Archive();
            try
            {
                // For dos Arquivos
                for (int i = 0; i < nomeArquivosExportados.Count; i++)
                {
                    arquivoZipado.Add(path + nomeArquivosExportados[i]);
                    arquivoZipado[i].Name = nomeArquivosExportados[i];
                }

                arquivoZipado.Zip(ms);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
            }

        }
    }
}
