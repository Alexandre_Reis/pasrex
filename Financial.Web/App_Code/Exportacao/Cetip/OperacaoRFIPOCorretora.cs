using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using FileHelpers;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.WebConfigConfiguration;
using Financial.Investidor.Enums;
using Financial.Interfaces.Export.RendaFixa;
using Financial.RendaFixa;

namespace Financial.Export.Cetip
{
    /// <summary>
    /// Summary description for OperacaoIPOClientes
    /// </summary>
    public class OperacaoIPOCorretora
    {
        public void ExportaArquivo(int idOperacao, out string conteudoArquivo, out string nomeArquivo)
        {
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            operacaoRendaFixa.LoadByPrimaryKey(idOperacao);

            OperacaoIPOCorretoraCetipViewModel operacaoIPOCorretora = operacaoRendaFixa.ExportaIPOCorretoraCetip();

            nomeArquivo = String.Format("operacao_ipo_cetip_corretora_{0}.txt", idOperacao);

            FileHelperEngine engine = new FileHelperEngine(typeof(HeaderOperacaoIPOCorretoraCetip));

            //Apesar de ser apenas um header, o filehelperengine soh trabalha com listas
            List<HeaderOperacaoIPOCorretoraCetip> headers = new List<HeaderOperacaoIPOCorretoraCetip>();
            headers.Add(operacaoIPOCorretora.header);

            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            conteudoArquivo = engine.WriteString(headers);

            engine = new FileHelperEngine(typeof(RegistroOperacaoIPOCorretoraCetip));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            conteudoArquivo += engine.WriteString(operacaoIPOCorretora.registros);
         
        }
    }
}
