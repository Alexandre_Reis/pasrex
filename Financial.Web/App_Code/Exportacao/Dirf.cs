﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.CRM;
using Financial.Fundo.Enums;

namespace Financial.Export
{

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public class Dirf
    {
        const string CODIGO_RECEITA_FUNDO_RENDAFIXA = "6800";
        const string CODIGO_RECEITA_FUNDO_ACOES = "6813";

        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda
        {
            [StringValue("00")]
            _2Casas = 1,

            [StringValue("0000")]
            _4Casas = 2,

            [StringValue("00000")]
            _5Casas = 3,

            [StringValue("000000")]
            _6Casas = 4,

            [StringValue("00000000")]
            _8Casas = 5,

            [StringValue("0000000000")]
            _10Casas = 6,

            [StringValue("00000000000000")]
            _14Casas = 7,
        }

        /// <summary>
        /// Valores de Detalhe
        /// </summary>
        #region Valores do Detalhe
        class ValoresDetail
        {

            /*----------------------------*/
            // 0 = Info Fundo ou Clube
            // 4 = Info Fundo ou Clube IDREC|6800 IRRF - APLICAÇÕES FINANCEIRAS EM FUNDOS DE INVESTIMENTO DE RENDA FIXA ou 6813 IRRF - FUNDOS DE INVESTIMENTO - AÇÕES 
            /*----------------------------*/
            // 1 = primeira linha
            // 2 = segunda linha = rendimento
            // 3 = terceira linha = IR
            /*----------------------------*/
            public int tipo;

            /* Linha 0 */
            public string descricaoFundo = "";
            public string cnpj = "";
            public string nomeFundo = "";

            public string codigoReceita = "";


            /* Primeira Linha */
            public string descricao = "";
            public string cnpjCotista;
            public string nomeCotista = "";
            public string dataLaudoMolestiaGrave = "";

            /* Lista de Rendimento por Mes 
             * NULL se não tiver valor
             * [0] = Janeiro
             * [1] = Fev
             * [2] = Mar
             * [11] = Dez
             * [12] = NULL Fixo
             */
            /* Segunda Linha */
            public List<Int64?> listaRendimentoPorMes = new List<Int64?>(new Int64?[] {
                                                                 null, null, null, null,
                                                                 null, null, null, null,
                                                                 null, null, null, null,
                                                                 null 
                                                            });
            /* Lista de ValorIR por Mes
             * [0] = Janeiro
             * [1] = Fev
             * [2] = Mar
             * [11] = Dez
             * [12] = NULL Fixo
             */
            /* Terceira Linha */
            public List<Int64?> listaValorIRPorMes = new List<Int64?>(new Int64?[] {
                                                                 null, null, null, null,
                                                                 null, null, null, null,
                                                                 null, null, null, null,
                                                                 null 
                                                            });
        }

        private List<ValoresDetail> valoresDetail = new List<ValoresDetail>();

        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo txt. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo BacenJud</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo BanceJud</param>        
        /// <returns></returns>
        /// <exception cref="ProcessaMensalClubesException">
        /// throws ProcessaMensalClubesException se occoreu algum erro no processamento
        /// </exception>
        public void ExportaDirf(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
        {
            ms = new MemoryStream();
            nomeArquivo = "";

            #region Busca os Clientes
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery, pessoaQuery.Cpfcnpj);
            clienteQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);
            //
            clienteQuery.Where(clienteQuery.TipoControle.In((byte)TipoControleCliente.Completo, (byte)TipoControleCliente.Cotista) &&
                               clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.Clube) &&
                               clienteQuery.IdCliente.In(idClientes)
                              );
            //
            clienteQuery.OrderBy(pessoaQuery.Cpfcnpj.Ascending);
            //
            clienteCollection.Load(clienteQuery);
            #endregion

            if (clienteCollection.HasData)
            {
                StringBuilder nomeArquivoAux = new StringBuilder();

                nomeArquivoAux.Append("DIRF_" + this.FormataValor(Convert.ToDecimal(dataPosicao.Year), AlinhamentoEsquerda._4Casas, false) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Month), AlinhamentoEsquerda._2Casas, false) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Day), AlinhamentoEsquerda._2Casas, false));
                nomeArquivoAux.Append(".txt");

                // out
                nomeArquivo = nomeArquivoAux.ToString();

                //
                DateTime dataAnoInicio = new DateTime(dataPosicao.Year, 01, 01);
                DateTime dataAnoFim = new DateTime(dataPosicao.Year, 12, 31);
                //
                for (int i = 0; i < clienteCollection.Count; i++)
                {
                    int idCliente = clienteCollection[i].IdCliente.Value;

                    PessoaQuery pessoaCotistaQuery = new PessoaQuery("P");
                    OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                    CotistaQuery cotistaQuery = new CotistaQuery("C");
                    OperacaoCotistaCollection oper = new OperacaoCotistaCollection();

                    operacaoCotistaQuery.Select(pessoaQuery.Cpfcnpj);
                    operacaoCotistaQuery.InnerJoin(cotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
                    operacaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
                    operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira == idCliente &&
                                             operacaoCotistaQuery.RendimentoResgate > 0 &&
                                             operacaoCotistaQuery.ValorIR > 0 &&
                                             operacaoCotistaQuery.DataConversao.Between(dataAnoInicio, dataAnoFim),
                                             pessoaQuery.Cpfcnpj.IsNotNull());
                    operacaoCotistaQuery.GroupBy(pessoaQuery.Cpfcnpj);
                    operacaoCotistaQuery.OrderBy(pessoaQuery.Cpfcnpj.Ascending);

                    oper.Load(operacaoCotistaQuery);

                    if (oper.Count > 0)
                    {
                        #region Adiciona Fundo ou Clube
                        ValoresDetail valorDetailFundo = new ValoresDetail();
                        ValoresDetail valorDetailAux = new ValoresDetail();
                        //
                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCliente);
                        //
                        Carteira carteira = new Carteira();
                        carteira.LoadByPrimaryKey(idCliente);

                        valorDetailFundo.tipo = 0;
                        valorDetailFundo.descricaoFundo = "FCI";
                        valorDetailFundo.cnpj = cliente.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                        //
                        if (cliente.UpToPessoaByIdPessoa.IsPessoaFisica())
                        {
                            valorDetailFundo.cnpj = valorDetailFundo.cnpj.PadLeft(11, '0');
                        }
                        else
                        {
                            valorDetailFundo.cnpj = valorDetailFundo.cnpj.PadLeft(14, '0');
                        }
                        //
                        valorDetailFundo.nomeFundo = cliente.Nome.Trim();
                        //                    
                        this.valoresDetail.Add(valorDetailFundo);
                        //
                        valorDetailAux.tipo = 4;
                        valorDetailAux.codigoReceita =
                            (carteira.TipoTributacao == (byte)TipoTributacaoFundo.Acoes) ?
                                CODIGO_RECEITA_FUNDO_ACOES :
                                CODIGO_RECEITA_FUNDO_RENDAFIXA;
                        //
                        this.valoresDetail.Add(valorDetailAux);
                        //
                        #endregion
                    }

                    for (int j = 0; j < oper.Count; j++) //loop para pessoa fisica
                    {
                        #region Para cada Cotista
                        //int idCotista = oper[j].IdCotista.Value;
                        string cnpjCotista = Convert.ToString(oper[j].GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj));

                        PessoaCollection p = new PessoaCollection();
                        p.Query.Select(p.Query.IdPessoa)
                               .Where(p.Query.Cpfcnpj == cnpjCotista);

                        p.Query.Load();
                        //
                        int idCotista = p[0].IdPessoa.Value;
                        //
                        ValoresDetail valorDetail = new ValoresDetail();
                        //
                        Cotista c = new Cotista();
                        c.LoadByPrimaryKey(idCotista);
                        //
                        
                        if (!c.UpToPessoaByIdPessoa.IsPessoaFisica())
                            continue;

                        valorDetail.tipo = 1;
                        valorDetail.descricao = c.UpToPessoaByIdPessoa.IsPessoaFisica() ? "BPFFCI" : "BPJFCI";
                        valorDetail.cnpjCotista = c.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                        if (c.UpToPessoaByIdPessoa.IsPessoaFisica())
                        {
                            valorDetail.cnpjCotista = valorDetail.cnpjCotista.PadLeft(11, '0');
                        }
                        else
                        {
                            valorDetail.cnpjCotista = valorDetail.cnpjCotista.PadLeft(14, '0');
                        }

                        valorDetail.nomeCotista = c.Nome.Trim();

                        // Adiciona Primeira Linha
                        this.valoresDetail.Add(valorDetail);
                        //

                        ValoresDetail valorDetailRendimento = new ValoresDetail();
                        valorDetailRendimento.tipo = 2;
                        //
                        ValoresDetail valorDetailValorIR = new ValoresDetail();
                        valorDetailValorIR.tipo = 3;

                        //
                        for (int mes = 1; mes <= 12; mes++)
                        {
                            #region Para cada Mes
                            //
                            DateTime dataMesInicio = new DateTime(dataPosicao.Year, mes, 01);
                            DateTime dataMesFim = Calendario.RetornaUltimoDiaCorridoMes(new DateTime(dataPosicao.Year, mes, 1), 0);
                            //
                            PessoaQuery pessoaAuxQuery = new PessoaQuery("P1");
                            OperacaoCotistaQuery operAuxQuery = new OperacaoCotistaQuery("Oper");

                            OperacaoCotista operAux = new OperacaoCotista();
                            //
                            operAuxQuery.Select(operAuxQuery.RendimentoResgate.Sum(), operAuxQuery.ValorIR.Sum());
                            operAuxQuery.InnerJoin(pessoaAuxQuery).On(pessoaAuxQuery.IdPessoa == operAuxQuery.IdCotista);
                            //
                            operAuxQuery.Where(operAuxQuery.IdCarteira == idCliente &&
                                             pessoaAuxQuery.Cpfcnpj == cnpjCotista &&
                                             operAuxQuery.RendimentoResgate > 0 &&
                                             operAuxQuery.ValorIR > 0 &&
                                             operAuxQuery.DataConversao.Between(dataMesInicio, dataMesFim));

                            operAux.Load(operAuxQuery);
                            //
                            if (operAux.es.HasData)
                            {

                                // Preenche Linha RTRT
                                if (operAux.RendimentoResgate.HasValue)
                                {
                                    valorDetailRendimento.listaRendimentoPorMes[mes - 1] = Convert.ToInt64(operAux.RendimentoResgate.Value * 100);
                                }

                                // Preenche Linha RTIRF
                                if (operAux.ValorIR.HasValue)
                                {
                                    valorDetailValorIR.listaValorIRPorMes[mes - 1] = Convert.ToInt64(operAux.ValorIR.Value * 100);
                                }
                            }
                            #endregion
                        }

                        // Adiciona Segunda Linha
                        this.valoresDetail.Add(valorDetailRendimento);

                        // Adiciona Terceira Linha
                        this.valoresDetail.Add(valorDetailValorIR);
                        #endregion
                    }

                    for (int j = 0; j < oper.Count; j++) //loop para pessoa juridica
                    {
                        #region Para cada Cotista
                        //int idCotista = oper[j].IdCotista.Value;
                        string cnpjCotista = Convert.ToString(oper[j].GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj));

                        PessoaCollection p = new PessoaCollection();
                        p.Query.Select(p.Query.IdPessoa)
                               .Where(p.Query.Cpfcnpj == cnpjCotista);

                        p.Query.Load();
                        //
                        int idCotista = p[0].IdPessoa.Value;
                        //
                        ValoresDetail valorDetail = new ValoresDetail();
                        //
                        Cotista c = new Cotista();
                        c.LoadByPrimaryKey(idCotista);

                        if (!c.UpToPessoaByIdPessoa.IsPessoaJuridica())
                            continue;
                        //
                        valorDetail.tipo = 1;
                        valorDetail.descricao = c.UpToPessoaByIdPessoa.IsPessoaFisica() ? "BPFFCI" : "BPJFCI";
                        valorDetail.cnpjCotista = c.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                        if (c.UpToPessoaByIdPessoa.IsPessoaFisica())
                        {
                            valorDetail.cnpjCotista = valorDetail.cnpjCotista.PadLeft(11, '0');
                        }
                        else
                        {
                            valorDetail.cnpjCotista = valorDetail.cnpjCotista.PadLeft(14, '0');
                        }

                        valorDetail.nomeCotista = c.Nome.Trim();

                        // Adiciona Primeira Linha
                        this.valoresDetail.Add(valorDetail);
                        //

                        ValoresDetail valorDetailRendimento = new ValoresDetail();
                        valorDetailRendimento.tipo = 2;
                        //
                        ValoresDetail valorDetailValorIR = new ValoresDetail();
                        valorDetailValorIR.tipo = 3;

                        //
                        for (int mes = 1; mes <= 12; mes++)
                        {
                            #region Para cada Mes
                            //
                            DateTime dataMesInicio = new DateTime(dataPosicao.Year, mes, 01);
                            DateTime dataMesFim = Calendario.RetornaUltimoDiaCorridoMes(new DateTime(dataPosicao.Year, mes, 1), 0);
                            //
                            PessoaQuery pessoaAuxQuery = new PessoaQuery("P1");
                            OperacaoCotistaQuery operAuxQuery = new OperacaoCotistaQuery("Oper");
                            CotistaQuery cotistaAuxQuery = new CotistaQuery("CAQ");
                            OperacaoCotista operAux = new OperacaoCotista();
                            //
                            operAuxQuery.Select(operAuxQuery.RendimentoResgate.Sum(), operAuxQuery.ValorIR.Sum());
                            operAuxQuery.InnerJoin(cotistaAuxQuery).On(operAuxQuery.IdCotista == cotistaAuxQuery.IdCotista);
                            operAuxQuery.InnerJoin(pessoaAuxQuery).On(pessoaAuxQuery.IdPessoa == cotistaAuxQuery.IdPessoa);
                            //
                            operAuxQuery.Where(operAuxQuery.IdCarteira == idCliente &&
                                             pessoaAuxQuery.Cpfcnpj == cnpjCotista &&
                                             operAuxQuery.RendimentoResgate > 0 &&
                                             operAuxQuery.ValorIR > 0 &&
                                             operAuxQuery.DataConversao.Between(dataMesInicio, dataMesFim));

                            operAux.Load(operAuxQuery);
                            //
                            if (operAux.es.HasData)
                            {

                                // Preenche Linha RTRT
                                if (operAux.RendimentoResgate.HasValue)
                                {
                                    valorDetailRendimento.listaRendimentoPorMes[mes - 1] = Convert.ToInt64(operAux.RendimentoResgate.Value * 100);
                                }

                                // Preenche Linha RTIRF
                                if (operAux.ValorIR.HasValue)
                                {
                                    valorDetailValorIR.listaValorIRPorMes[mes - 1] = Convert.ToInt64(operAux.ValorIR.Value * 100);
                                }
                            }
                            #endregion
                        }

                        // Adiciona Segunda Linha
                        this.valoresDetail.Add(valorDetailRendimento);

                        // Adiciona Terceira Linha
                        this.valoresDetail.Add(valorDetailValorIR);
                        #endregion
                    }
                }

                // 
                #region EscreveArquivo
                try
                {
                    //Abrir o arquivo
                    StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);

                    #region Header
                    this.EscreveHeader(arquivo, dataAnoFim);
                    #endregion

                    #region Detalhes
                    for (int i = 0; i < this.valoresDetail.Count; i++)
                    {
                        this.EscreveDetalhe(arquivo, i);
                    }
                    #endregion

                    #region Rodape
                    this.EscreveRodape(arquivo);
                    #endregion

                    arquivo.Flush();

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);

                    //if (this.valoresDetail.Count == 0) {
                    //    throw new ProcessaDirfException("Não existem Dados para Geração do arquivo DIRF.");
                    //}
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo Dirf com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;

                    throw new ProcessaDirfException(mensagem);
                }
                #endregion
            }

            // 0 registros
            else
            {
                throw new ProcessaDirfException("Não existem Dados para Geração do arquivo DIRF.");
            }
        }

        /// <summary>
        /// Escreve header do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        private void EscreveHeader(StreamWriter arquivo, DateTime dataAnoFim)
        {
            int ano = dataAnoFim.Year + 1;
            int anoAnterior = dataAnoFim.Year;

            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            agenteMercado.LoadByPrimaryKey(idAgente);


            string cpfResponsavel1 = "";
            if (!String.IsNullOrEmpty(agenteMercado.CPFResponsavel))
            {
                cpfResponsavel1 = Utilitario.RemoveCaracteresEspeciais(agenteMercado.CPFResponsavel).PadLeft(11, '0');
            }

            string cpfResponsavel2 = "";
            if (!String.IsNullOrEmpty(agenteMercado.CPFResponsavel2))
            {
                cpfResponsavel2 = Utilitario.RemoveCaracteresEspeciais(agenteMercado.CPFResponsavel2).PadLeft(11, '0');
            }

            string nomeResponsavel = "";
            if (!String.IsNullOrEmpty(agenteMercado.NomeResponsavel))
            {
                nomeResponsavel = Utilitario.RemoveCaracteresEspeciais(agenteMercado.NomeResponsavel);
            }

            string ddd = "";
            if (!String.IsNullOrEmpty(agenteMercado.Ddd))
            {
                ddd = agenteMercado.Ddd.PadRight(2, ' ');
            }

            string telefone = "";
            if (!String.IsNullOrEmpty(agenteMercado.Telefone))
            {
                telefone = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Telefone);
            }

            string ramal = "";
            if (!String.IsNullOrEmpty(agenteMercado.Ramal))
            {
                ramal = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Ramal);
            }

            string fax = "";
            if (!String.IsNullOrEmpty(agenteMercado.Fax))
            {
                fax = agenteMercado.Fax.Trim();
            }

            string email = "";
            if (!String.IsNullOrEmpty(agenteMercado.Email))
            {
                email = agenteMercado.Email;
            }

            string cnpj = "";
            if (!String.IsNullOrEmpty(agenteMercado.Cnpj))
            {
                cnpj = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj).PadRight(14, '0');
            }

            string nomeEmpresarial = "";
            if (!String.IsNullOrEmpty(agenteMercado.Nome))
            {
                nomeEmpresarial = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Nome);
            }

            string identificacaoEstrutura = "";
            if (ano == 2013)
            {
                identificacaoEstrutura = "1A4MA1R";
            }
            else if (ano == 2014)
            {
                identificacaoEstrutura = "F8UCL6S";
            }
            else if (ano == 2015)
            {
                identificacaoEstrutura = "M1LB5V2";
            }
            else if (ano == 2016)
            {
                identificacaoEstrutura = "L35QJS2";
            }

            string linha1 = "Dirf|" + ano.ToString() + "|" + anoAnterior.ToString() + "|N||" + identificacaoEstrutura + "|";
            string linha2 = "RESPO|" + cpfResponsavel1 + "|" + nomeResponsavel + "|" + ddd + "|" + telefone + "|" + ramal + "|" + fax + "|" + email + "|";
            string linha3 = "DECPJ|" + cnpj + "|" + nomeEmpresarial + "|0|" + cpfResponsavel2 + "|N|N|S|N|N|N|N|N||";

            arquivo.Write(linha1);
            arquivo.WriteLine();
            //
            arquivo.Write(linha2);
            arquivo.WriteLine();
            //
            arquivo.Write(linha3);
            arquivo.WriteLine();
        }

        /// <summary>
        /// Escreve Rodape do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        private void EscreveRodape(StreamWriter arquivo)
        {
            const string linha = "FIMDirf|";
            //
            arquivo.WriteLine();
            arquivo.Write(linha);
        }

        /// <summary>
        /// Escreve no Arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">Numero da Linha de detalhe a escrever começando de 0</param>
        private void EscreveDetalhe(StreamWriter arquivo, int linha)
        {

            if (this.valoresDetail[linha].tipo == 0)
            {
                #region Fundo
                arquivo.Write(this.valoresDetail[linha].descricaoFundo);
                arquivo.Write("|");
                //
                arquivo.Write(this.valoresDetail[linha].cnpj);
                arquivo.Write("|");
                //
                string fundo = this.FormataCaracter(this.valoresDetail[linha].nomeFundo.Trim());
                //                
                arquivo.Write(fundo);
                arquivo.Write("|");
                //
                #endregion
            }

            if (this.valoresDetail[linha].tipo == 4)
            {
                #region Constante Fundo
                arquivo.Write("IDREC");
                arquivo.Write("|");
                arquivo.Write(this.valoresDetail[linha].codigoReceita);
                arquivo.Write("|");
                #endregion
            }

            if (this.valoresDetail[linha].tipo == 1)
            {
                #region Cotista
                arquivo.Write(this.valoresDetail[linha].descricao);
                arquivo.Write("|");
                //
                arquivo.Write(this.valoresDetail[linha].cnpjCotista);
                arquivo.Write("|");
                //
                string cotista = this.FormataCaracter(this.valoresDetail[linha].nomeCotista.Trim());
                //                
                arquivo.Write(cotista);
                arquivo.Write("|");

                if (this.valoresDetail[linha].descricao != "BPJFCI")
                {
                    arquivo.Write(this.valoresDetail[linha].dataLaudoMolestiaGrave);
                    arquivo.Write("|");
                }

                #endregion
            }

            if (this.valoresDetail[linha].tipo == 2)
            { // Rendimento
                #region Rendimento
                arquivo.Write("RTRT");
                arquivo.Write("|");
                //
                // Percorre a lista de 13 Meses
                for (int i = 0; i < this.valoresDetail[linha].listaRendimentoPorMes.Count; i++)
                {
                    if (this.valoresDetail[linha].listaRendimentoPorMes[i].HasValue)
                    {
                        arquivo.Write(this.valoresDetail[linha].listaRendimentoPorMes[i].Value);
                    }
                    arquivo.Write("|");
                }
                #endregion
            }

            if (this.valoresDetail[linha].tipo == 3)
            { // ValorIR
                #region ValorIR
                arquivo.Write("RTIRF");
                arquivo.Write("|");
                //
                // Percorre a lista de 13 Meses
                for (int i = 0; i < this.valoresDetail[linha].listaValorIRPorMes.Count; i++)
                {
                    if (this.valoresDetail[linha].listaValorIRPorMes[i].HasValue)
                    {
                        arquivo.Write(this.valoresDetail[linha].listaValorIRPorMes[i].Value);
                    }
                    arquivo.Write("|");
                }
                #endregion
            }

            // Utima Linha não imprime
            if (linha != this.valoresDetail.Count - 1)
            {
                arquivo.WriteLine();
            }
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se numero é Positivo Coloca um Sinal de + na frente
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda)
        {
            return this.FormataValor(valor, numeroCasasAlinhamentoEsquerda, true);
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se colocaPositivo=true e Se numero é Positivo Coloca um Sinal de + na frente
        /// Sinal Negativo Sempre Aparece
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <param name="colocaPositivo"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda, bool colocaPositivo)
        {
            if (colocaPositivo)
            {
                return valor.ToString(
                    valor >= 0
                    ? "+" + StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda)
                    : StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
            else
            {
                return valor.ToString(StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
        }

        /// <summary>
        /// Substitue Caracteres Inválidos
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        private string FormataCaracter(string texto)
        {
            string retorno = texto;

            List<char> caracteres = new List<char>(new Char[] { 
                '!', '@', '#', '$', '%', '&', '*', '(', ')', '-', '_',
                '+', '=', '{', '[', '^', '~', '}', ']', '|', '<', 
                ',' , '>', '.' , ';', ':', '?', '*', '/', '\\', '\''
            });

            for (int i = 0; i < caracteres.Count; i++)
            {
                if (retorno.Contains(caracteres[i].ToString()))
                {
                    retorno = retorno.Replace(caracteres[i].ToString(), "");
                }
            }

            // Subtitui Ç
            if (retorno.Contains("Ç"))
            {
                retorno = retorno.Replace("Ç", "C");
            }

            // Subtitui ç
            if (retorno.Contains("ç"))
            {
                retorno = retorno.Replace("ç", "c");
            }

            
            // Subtitui á
            if (retorno.Contains("á"))
            {
                retorno = retorno.Replace("á", "a");
            }

            // Subtitui Á
            if (retorno.Contains("Á"))
            {
                retorno = retorno.Replace("Á", "A");
            }

            // Subtitui é
            if (retorno.Contains("é"))
            {
                retorno = retorno.Replace("é", "e");
            }

            // Subtitui É
            if (retorno.Contains("É"))
            {
                retorno = retorno.Replace("É", "E");
            }
            
            // Subtitui í
            if (retorno.Contains("í"))
            {
                retorno = retorno.Replace("í", "i");
            }

            // Subtitui Í
            if (retorno.Contains("Í"))
            {
                retorno = retorno.Replace("Í", "I");
            }
            
            // Subtitui ó
            if (retorno.Contains("ó"))
            {
                retorno = retorno.Replace("ó", "o");
            }

            // Subtitui Ó
            if (retorno.Contains("Ó"))
            {
                retorno = retorno.Replace("Ó", "O");
            }

            // Subtitui ú
            if (retorno.Contains("ú"))
            {
                retorno = retorno.Replace("ú", "u");
            }

            // Subtitui Ú
            if (retorno.Contains("Ú"))
            {
                retorno = retorno.Replace("Ú", "U");
            }
            
            // Subtitui ã
            if (retorno.Contains("ã"))
            {
                retorno = retorno.Replace("ã", "a");
            }

            // Subtitui Â
            if (retorno.Contains("Â"))
            {
                retorno = retorno.Replace("Â", "A");
            }

            // Subtitui õ
            if (retorno.Contains("õ"))
            {
                retorno = retorno.Replace("õ", "o");
            }

            // Subtitui Õ
            if (retorno.Contains("Õ"))
            {
                retorno = retorno.Replace("Õ", "O");
            }

            

            return retorno;
        }
    }
}