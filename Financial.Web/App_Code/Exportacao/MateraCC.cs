﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using FileHelpers;
using Financial.Investidor.Enums;

namespace Financial.Export
{
    /// <summary>
    /// Summary description for MateraCC
    /// </summary>
    public class MateraCC
    {
        [FixedLengthRecord()]
        public class LiquidacaoMatera
        {

            [FieldFixedLength(3)]
            public int numeroLinha;

            [FieldFixedLength(5)]
            public string tipoLiquidacao;

            [FieldFixedLength(15)]
            public string nomeSistema;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataLiquidacao;

            [FieldFixedLength(12)]
            public int numeroEmpresa;

            [FieldFixedLength(12)]
            public int numeroContraparte;

            [FieldFixedLength(30)]
            public string nomeResponsavel;

            [FieldFixedLength(4)]
            public int numeroAgencia;

            [FieldFixedLength(12)]
            public int numeroConta;

            [FieldFixedLength(15)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal valor;

            [FieldFixedLength(4)]
            public int codigoHistorico;

            [FieldFixedLength(1)]
            public string indIntegraSDConta;

            [FieldFixedLength(1)]
            public string indSumarizaLcto;

            [FieldFixedLength(1)]
            public string indAprovaLiquidacao;

            [FieldFixedLength(30)]
            public string complemento;

            [FieldFixedLength(19)]
            public string horaAgendamento; //por enquanto está como string pois é NULL - o default de conversão de datetime nula resulta numa data default interna do c#

            [FieldFixedLength(1)]
            public string indIntegraSDCompe;

            [FieldFixedLength(656)]
            public string filler;

            internal class TwoDecimalConverter : ConverterBase
            {
                public override object StringToField(string from)
                {
                    decimal res = Convert.ToDecimal(from);
                    return res / 100;
                }

                public override string FieldToString(object from)
                {
                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }

            }
        }

        public void ExportaLiquidacaoMatera(DateTime dataVencimento)
        {
            const string PATH_ARQUIVO_OUTPUT = "c:\\FileOut.txt";
            const string TIPO_LIQUIDACAO = "C";
            const string NOME_SISTEMA = "SDTES";
            const string NOME_RESPONSAVEL = "SDBANCO";
            const int NUMERO_EMPRESA = 1;
            const int HISTORICO_DEBITO = 92;
            const int HISTORICO_CREDITO = 93;
            const string INDINTEGRASDCONTA = "S";
            const string INDSUMARIZALCTO = "N";
            const string INDAPROVALIQUIDACAO = "S";
            const string INDINTEGRASDCOMPE = "N";

            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.Equal(dataVencimento),
                                  clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
            liquidacaoQuery.OrderBy(liquidacaoQuery.IdCliente.Ascending);

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Load(liquidacaoQuery);

            if (liquidacaoCollection.Count == 0)
            {
                return;
            }

            LiquidacaoMatera[] liquidacaoMateraArray = new LiquidacaoMatera[liquidacaoCollection.Count];

            int countLinhas = 0;
            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                LiquidacaoMatera liquidacaoMatera = new LiquidacaoMatera();
                liquidacaoMatera.numeroLinha = ++countLinhas;
                liquidacaoMatera.tipoLiquidacao = TIPO_LIQUIDACAO;
                liquidacaoMatera.nomeSistema = NOME_SISTEMA;

                liquidacaoMatera.dataLiquidacao = liquidacao.DataVencimento.Value;
                liquidacaoMatera.numeroEmpresa = NUMERO_EMPRESA;
                liquidacaoMatera.numeroContraparte = liquidacao.IdCliente.Value;
                liquidacaoMatera.nomeResponsavel = NOME_RESPONSAVEL;

                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                contaCorrente.LoadByPrimaryKey(liquidacao.IdConta.Value);

                Agencia agencia = new Agencia();
                agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);

                liquidacaoMatera.numeroAgencia = Convert.ToInt32(agencia.Codigo);
                liquidacaoMatera.numeroConta = Convert.ToInt32(contaCorrente.Numero);

                liquidacaoMatera.valor = Math.Abs(liquidacao.Valor.Value);
                liquidacaoMatera.codigoHistorico = liquidacao.Valor < 0 ? HISTORICO_DEBITO : HISTORICO_CREDITO;
                liquidacaoMatera.indIntegraSDConta = INDINTEGRASDCONTA;
                liquidacaoMatera.indSumarizaLcto = INDSUMARIZALCTO;
                liquidacaoMatera.indAprovaLiquidacao = INDAPROVALIQUIDACAO;
                liquidacaoMatera.indIntegraSDCompe = INDINTEGRASDCOMPE;

                liquidacaoMateraArray[countLinhas - 1] = liquidacaoMatera;

            }

            FileHelperEngine engine = new FileHelperEngine(typeof(LiquidacaoMatera));
            engine.WriteFile(PATH_ARQUIVO_OUTPUT, liquidacaoMateraArray);

            throw new Exception("Arquivo exportado com sucesso.");
        }
    }
}
