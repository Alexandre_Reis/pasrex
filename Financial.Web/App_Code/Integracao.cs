﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Financial.Investidor;
using DevExpress.Web;
using EntitySpaces.Interfaces;
using Financial.Investidor.Controller;
using Financial.Investidor.Enums;
using System.Web.Script.Services;
using Financial.Util;
using System.Threading;
using Financial.Interfaces;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Common;
using Financial.WebConfigConfiguration;
using System.Text;


/// <summary>
/// Summary description for Integracao
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Integracao : System.Web.Services.WebService
{
    private enum TipoImportacao
    {
        BDIN = 1,
        BDPregao = 2,
        Indic = 3,
        RefVol = 4,
        TarPar = 5,
        TarPreg = 6,
        TaxaSwap = 7,
        AndimaMercado = 8,
        Andima238 = 9,
        Andima550 = 10,
        AndimaDebenture = 11,
        PAPD = 12,
        CDIX = 13,
        CMDF = 14,
        RComiesp = 15,
        CONL = 16,
        CONR = 17,
        CSGD = 18,
        DBTC = 19,
        DBTL = 20,
        NEGS = 21,
        POSR = 22,
        PROD = 23
    }

    public Integracao()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetTipoIntegracao()
    {
        string tipoIntegracao = Context.User.Identity.Name + ":TipoIntegracao";
        return Context.Cache[tipoIntegracao] as string;
    }

    [WebMethod]
    public string GetProgresso()
    {
        string progresso = Context.User.Identity.Name + ":ProgressoIntegracao";
        return Context.Cache[progresso] as string;
    }

    [WebMethod]
    public void ProcessaIntegracao(string dataReferencia, string listaId)
    {
        DateTime data = Convert.ToDateTime(dataReferencia);

        char[] caracter = new char[] { '|' };
        StringTokenizer token = new StringTokenizer(listaId, caracter);

        string tipoIntegracao = Context.User.Identity.Name + ":TipoIntegracao";
        string progresso = Context.User.Identity.Name + ":ProgressoIntegracao";

        int totalProcessado = 0;
        int totalProcessar = 100;
        int tempoPorIntegracao = (int)totalProcessar / token.Count();

        Context.Cache[progresso] = 0;

        // Arquivos são procurados no diretorio Downloads
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + "Downloads/";
        foreach (string item in token)
        {
            int id = Convert.ToInt32(item);
            
            switch (id)
            {
                case (int)TipoImportacao.BDIN:
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    //
                    cotacaoBolsa.CarregaCotacaoBdin(data, path);
                    cotacaoIndice.CarregaCotacaoIndiceBdin(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.BDIN.ToString();
                    break;
                case (int)TipoImportacao.BDPregao:
                    AtivoBMF ativoBMF = new AtivoBMF();
                    cotacaoIndice = new CotacaoIndice();
                    //
                    ativoBMF.CarregaAtivoBdPregao(data, path);                    
                    cotacaoIndice.CarregaCotacaoIndiceBdPregao(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.BDPregao.ToString();
                    break;
                case (int)TipoImportacao.Indic:
                    cotacaoIndice = new CotacaoIndice();
                    //
                    cotacaoIndice.CarregaCotacaoIndiceIndic(data, path);
                    Context.Cache[tipoIntegracao] = TipoImportacao.Indic.ToString();
                    break;
                case (int)TipoImportacao.RefVol:
                    TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();
                    tabelaVolatilidadeEstrategia.CarregaTabelaVolatilidadeEstrategiaRefVol(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.RefVol.ToString();
                    break;
                case (int)TipoImportacao.TarPar:
                    TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
                    cotacaoIndice = new CotacaoIndice();
                    //
                    tabelaCalculoBMF.CarregaTabelaCalculoBMFTarPar(data, path);                    
                    cotacaoIndice.CarregaCotacaoIndiceTarPar(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.TarPar.ToString();
                    break;
                case (int)TipoImportacao.TarPreg:
                    TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();
                    //
                    tabelaCustosBMF.CarregaTarPreg(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.TarPreg.ToString();
                    break;
                case (int)TipoImportacao.TaxaSwap:
                    TabelaTaxaSwap tabelaTaxaSwap = new TabelaTaxaSwap();
                    //
                    tabelaTaxaSwap.CarregaTaxaSwap(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.TaxaSwap.ToString();
                    break;
                case (int)TipoImportacao.AndimaMercado:
                    CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
                    
                    cotacaoMercadoAndima.CarregaAndima(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.AndimaMercado.ToString();
                    break;
                case (int)TipoImportacao.Andima238:
                    CotacaoResolucao238 cotacaoResolucao238 = new CotacaoResolucao238();
                    //
                    cotacaoResolucao238.CarregaRes238(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.Andima238.ToString();
                    break;
                case (int)TipoImportacao.Andima550:
                    CotacaoResolucao550 cotacaoResolucao550 = new CotacaoResolucao550();
                    cotacaoResolucao550.CarregaRes550(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.Andima550.ToString();
                    break;
                case (int)TipoImportacao.AndimaDebenture:
                    CotacaoMercadoDebenture cotacaoMercadoDebenture = new CotacaoMercadoDebenture();
                    cotacaoMercadoDebenture.CarregaDebenture(data, path);
                    //
                    Context.Cache[tipoIntegracao] = TipoImportacao.AndimaDebenture.ToString();
                    break;                
            }

            totalProcessado += tempoPorIntegracao;
            Context.Cache[progresso] = Convert.ToString(totalProcessado);            
        }

        totalProcessado = 100;
        Context.Cache[progresso] = Convert.ToString(totalProcessado);
    }

}

