﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using Financial.Security;
using Financial.Util;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using Financial.Security.Enums;
using Financial.WebConfigConfiguration;

/// <summary>
/// Summary description for FinancialSiteMapProvider
/// </summary>
public class FinancialSiteMapProvider : XmlSiteMapProvider {
    protected ConfiguracaoCollection configColl;

    #region Properties
    private bool testaAcesso;
    public bool TestaAcesso {
        set { testaAcesso = value; }
    }

    /* Define os componentes de cada grupo de menu */
    private List<int> menuAcoesOpcoes = new List<int>(new int[] { 1820, 2100, 20000 });
    private List<int> menuTermoAcoes = new List<int>(new int[] { 2300 });
    private List<int> menuBTCAcoes = new List<int>(new int[] { 2400, 1840, 20140, 20150 });
    private List<int> menuBMF = new List<int>(new int[] { 1830, 3000, 21000 });
    private List<int> menuRendaFixa = new List<int>(new int[] { 3500, 22000 });
    private List<int> menuFundos = new List<int>(new int[] { 4800, 15185, 15190, 15210, 15220, 16200, 17500 });
    private List<int> menuSwap = new List<int>(new int[] { 4000, 23000 });
    private List<int> menuGerencial = new List<int>(new int[] { 21500 });
    private List<int> menuRebates = new List<int>(new int[] { 9000, 18500 });
    private List<int> menuCalculoDespesas = new List<int>(new int[] { 7300, 7400, 7500, 7600, 15160, 15170, 15180, 16120 });
    private List<int> menuEnquadramento = new List<int>(new int[] { 11000, 24000 });
    private List<int> menuCotista = new List<int>(new int[] { 10000, 18000, 40160 });
    private List<int> menuContabil = new List<int>(new int[] { 13000 });
    //

    /// <summary>
    /// idMenus do Conjunto Ações e Opções
    /// </summary>
    public List<int> MenuAcoesOpcoes {
        get { return menuAcoesOpcoes; }
    }

    /// <summary>
    /// idMenus do Conjunto Termo de Ações
    /// </summary>
    public List<int> MenuTermoAcoes {
        get { return menuTermoAcoes; }
    }

    /// <summary>
    /// idMenus do Conjunto BTC
    /// </summary>
    public List<int> MenuBTCAcoes {
        get { return menuBTCAcoes; }
    }

    /// <summary>
    /// idMenus do Conjunto BMF
    /// </summary>
    public List<int> MenuBMF {
        get { return menuBMF; }
    }

    /// <summary>
    /// idMenus do Conjunto RendaFixa
    /// </summary>
    public List<int> MenuRendaFixa {
        get { return menuRendaFixa; }
    }

    /// <summary>
    /// idMenus do Conjunto Fundos
    /// </summary>
    public List<int> MenuFundos {
        get { return menuFundos; }
    }

    /// <summary>
    /// idMenus do Conjunto Swap
    /// </summary>
    public List<int> MenuSwap {
        get { return menuSwap; }
    }

    /// <summary>
    /// idMenus do Conjunto Gerencial
    /// </summary>
    public List<int> MenuGerencial {
        get { return menuGerencial; }
    }

    /// <summary>
    /// idMenus do Conjunto Rebates
    /// </summary>
    public List<int> MenuRebates {
        get { return menuRebates; }
    }

    /// <summary>
    /// idMenus do Conjunto Despesas
    /// </summary>
    public List<int> MenuCalculoDespesas {
        get { return menuCalculoDespesas; }
    }

    /// <summary>
    /// idMenus do Conjunto Enquadramento
    /// </summary>
    public List<int> MenuEnquadramento {
        get { return menuEnquadramento; }
    }

    /// <summary>
    /// idMenus do Conjunto Cotista
    /// </summary>
    public List<int> MenuCotista {
        get { return menuCotista; }
    }

    /// <summary>
    /// idMenus do Conjunto Contábil
    /// </summary>
    public List<int> MenuContabil {
        get { return menuContabil; }
    }
    #endregion

    /// <summary>
    /// Baseado na propriedade clienteEspecifico do node do sitemap determina se é um menu especifico de algum cliente
    /// </summary>
    /// <param name="node">node["clienteEspecifico"] = se estiver prenchido conterá o nome do Cliente</param>
    /// <returns>true se é um menu especifico, false caso contrário</returns>
    public bool isMenuEspecificoPorCliente(SiteMapNode node) {
        string especifico = node["clienteEspecifico"] ?? "";

        return especifico != ""; // se a propriedade clienteEspecifico estiver preenchida então é um menu especifico
    } 

    /// <summary>
    /// Construtor
    /// </summary>
    public FinancialSiteMapProvider() {
        this.testaAcesso = true;
    }

    /// <summary>
    /// Determina Visibilidade do nó no Menu
    /// </summary>
    /// <param name="context"></param>
    /// <param name="node"></param>
    /// <returns></returns>
    private bool IsVisible(HttpContext context, SiteMapNode node) {
        configColl = new ConfiguracaoCollection();
        configColl.LoadAll();
        //
        string login = context.User.Identity.Name;
       
        // Se login = master habilita tudo
        try {
            if (Convert.ToBoolean(context.Session["IsUserMaster"]) == true) {
                return true;
            }
        }
        catch (Exception){
            //se der erro é pq a chamada é pelo portal, nesse caso continua
        }

        // Tratamento para Menu PortoPar
        if (this.isMenuEspecificoPorCliente(node)) { // Tratamento para menus específicos
            string especifico = node["clienteEspecifico"];

            string cliente = "";
            if (!string.IsNullOrEmpty(WebConfig.AppSettings.Cliente))
                cliente = WebConfig.AppSettings.Cliente.Substring(0, 1).ToUpper() + WebConfig.AppSettings.Cliente.Substring(1).ToLower();
            //

            if (String.Compare(especifico, cliente, true) == 0) {
                return true;
            }
            else {
                return false;
            }
        }

        if (this.testaAcesso) {
            Usuario usuario = new Usuario();
            int idGrupo = usuario.BuscaUsuario(login) ? usuario.IdGrupo.Value : 0;

            string menuIDString = node["menuID"];
            int menuID;

            if (int.TryParse(menuIDString, out menuID)) 
            {
                GrupoUsuario grupoUsuario = new GrupoUsuario();
                grupoUsuario.LoadByPrimaryKey(idGrupo);
                if (grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Administrador && menuID >= 40000 && menuID <= 40180) 
                {
                    return true;
                }
                else 
                {
                    if (grupoUsuario.TipoPerfil.Value != (byte)TipoPerfilGrupo.Administrador && menuID == 14580) //Opção de Reset de Status IsProcessando
                    {
                        return false;
                    }

                    PermissaoMenu permissaoMenuGrupoZero = new PermissaoMenu();
                    if (permissaoMenuGrupoZero.LoadByPrimaryKey(0, menuID)) {
                        if (permissaoMenuGrupoZero.PermissaoLeitura == "N") {
                            return false;
                        }
                    }
                    else 
                    {
                        permissaoMenuGrupoZero = new PermissaoMenu();
                        permissaoMenuGrupoZero.IdMenu = menuID;
                        permissaoMenuGrupoZero.IdGrupo = 0;
                        permissaoMenuGrupoZero.PermissaoAlteracao = "S";
                        permissaoMenuGrupoZero.PermissaoExclusao = "S";
                        permissaoMenuGrupoZero.PermissaoInclusao = "S";
                        permissaoMenuGrupoZero.PermissaoLeitura = "S";
                        permissaoMenuGrupoZero.Save();
                    }

                    PermissaoMenu permissaoMenu = new PermissaoMenu();
                    if (permissaoMenu.LoadByPrimaryKey(idGrupo, menuID))
                    {
                        return permissaoMenu.PermissaoLeitura == "S";
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        else 
        {
            return true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="menuId"></param>
    /// <returns></returns>
    private bool TestaAcessoCliente(int menuId) {
        if (!AcessoAcoesOpcoes(menuId)) {
            return false;
        }
        else if (!AcessoTermoAcoes(menuId)) {
            return false;
        }
        else if (!AcessoBTCAcoes(menuId)) {
            return false;
        }
        else if (!AcessoBMF(menuId)) {
            return false;
        }
        else if (!AcessoRendaFixa(menuId)) {
            return false;
        }
        else if (!AcessoFundos(menuId)) {
            return false;
        }
        else if (!AcessoSwap(menuId)) {
            return false;
        }
        else if (!AcessoGerencial(menuId)) {
            return false;
        }
        else if (!AcessoCalculoDespesas(menuId)) {
            return false;
        }
        else if (!AcessoRebates(menuId)) {
            return false;
        }
        else if (!AcessoEnquadramento(menuId)) {
            return false;
        }
        else if (!AcessoCotista(menuId)) {
            return false;
        }
        else if (!AcessoContabil(menuId)) {
            return false;
        }
        else if (!AcessoInformes(menuId)) {
            return false;
        }

        return true;
    }

    protected bool AcessoAcoesOpcoes(int idMenu) {
        bool acessa = true;
        
        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.AcoesOpcoes);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuAcoesOpcoes.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoTermoAcoes(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.TermoAcoes);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuTermoAcoes.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoBTCAcoes(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BTC_Aluguel);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuBTCAcoes.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoBMF(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BMF);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuBMF.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoRendaFixa(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.RendaFixa);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuRendaFixa.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoFundos(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Fundos);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuFundos.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoSwap(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Swap);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuSwap.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoGerencial(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Gerencial);

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuGerencial.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoRebates(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Rebates);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuRebates.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoCalculoDespesas(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.CalculoDespesas);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuCalculoDespesas.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }    
    protected bool AcessoEnquadramento(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Enquadramento);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuEnquadramento.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoCotista(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Cotista);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuCotista.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoContabil(int idMenu) {
        bool acessa = true;

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Contabil);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (this.menuContabil.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoInformes(int idMenu) {
        bool acessa = true;

        Configuracao configClubes = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeClubes);
        Configuracao configFundos = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeFundos);
        Configuracao configApuracaoIR = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.ApuracaoIR);

        bool acessaInformeClubes = configClubes.ValorTexto.Trim().ToUpper() == "S";
        bool acessaInformeFundos = configFundos.ValorTexto.Trim().ToUpper() == "S";
        bool acessaInformeApuracaoIR = configApuracaoIR.ValorTexto.Trim().ToUpper() == "S";

        if (idMenu == 25100 && !acessaInformeClubes) {
            acessa = false;
        }
        else if (idMenu == 25220 && !acessaInformeClubes && !acessaInformeFundos) {
            acessa = false;
        }
        else if (idMenu == 25230 && !acessaInformeApuracaoIR) {
            acessa = false;
        }
        else if (idMenu == 25240 && !acessaInformeFundos) {
            acessa = false;
        }
        else if (idMenu == 25000 && !acessaInformeClubes && !acessaInformeFundos && !acessaInformeApuracaoIR) {
            acessa = false;
        }
        else if (idMenu == 25200 && !acessaInformeClubes && !acessaInformeFundos && !acessaInformeApuracaoIR) {
            acessa = false;
        }

        return acessa;
    }
       
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="node"></param>
    /// <returns></returns>
    public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node) {
        return IsVisible(context, node);
    }
}