﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.InvestidorCotista;
using Financial.CRM.Enums;
using System.Globalization;
using Financial.Util;
using System.Text;
using Financial.Investidor;
using System.Collections.Generic;
using log4net;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using System.Threading;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SaldoAplicacaoCotista
    /// </summary>
    public class ReportSaldoAplicacaoCotista : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportSaldoAplicacaoCotista));

        #region Enum
        public enum TipoRelatorio {
            Analitico = 1,
            Consolidado = 2
        }

        public enum TipoGrupamento {
            PorNome = 1,
            PorCodigo = 2
        }

        public enum OpcaoValorAplicado
        {
            ValorAtualizado = 1,
            ValorOriginal = 2
        }

        public enum TipoLingua
        {
            [StringValue("pt-BR")]
            Portugues = 1,

            [StringValue("en-US")]
            Ingles = 2,
        }
        #endregion

        private TipoRelatorio tipoRelatorio;
        private TipoGrupamento tipoGrupamento;
        private OpcaoValorAplicado opcaoValorAplicado;
        //
        private bool exibeRentabilidade = false;
        //
        private int idCarteira;
        private int? idCotista;

        private string login;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        public int? IdCotista
        {
            get { return idCotista; }
            set { idCotista = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private TipoLingua tipoLingua;

        public TipoLingua Lingua
        {
            get { return tipoLingua; }
        }

        private int numeroLinhasDataTable;

        // Define de consulta será histórica ou não
        private enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoCotista;
        //

        /// <summary>
        /// Retorna Visibilidade do Relatório
        /// </summary>
        public bool TemDados {
            get { return !this.xrSubreport3.Visible; }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRPageInfo xrPageInfo2;
        private XRPageInfo xrPageInfo3;
        private ReportHeaderBand ReportHeader;
        private XRPanel xrPanel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell17;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCellValorCotaAplicacao;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCellDataAplicacao;
        private XRTableCell xrTableCell34;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell80;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableCell xrTableCell83;
        private XRTable xrTable8;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCellCotistaGroupFooter;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell72;
        private XRTableCell xrTableCell73;
        private XRTable xrTable9;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell74;
        private XRTableCell xrTableCell75;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow11;
        private XRTableRow xrTableRow15;
        private XRTable xrTable11;
        private XRTable xrTable15;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell76;
        private XRTableCell xrTableCell77;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell78;
        private XRTableCell xrTableCell79;
        private GroupHeaderBand GroupHeader2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell25;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private CalculatedField calculatedFieldParticipacao;
        private DevExpress.XtraReports.Parameters.Parameter ParameterTotalValorBruto;
        private XRTable xrTable12;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>
        /// <param name="exibeRentabilidade">Coloca Rentabilidade Mensal/Ano ou Não</param>
        /// <param name="login"></param>
        public ReportSaldoAplicacaoCotista(int idCarteira, int? idCotista, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, bool exibeRentabilidade, string login,
                                                TipoLingua lingua) 
        {

            this.SaldoAplicacaoCotista(idCarteira, idCotista, data, tipoRelatorio,
                                   tipoGrupamento, opcaoValorAplicado, exibeRentabilidade, login, lingua);

        }

        /// <summary>
        /// Assume que rentabilidade Mes/Anual não aparece.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>        
        /// <param name="login"></param>
        public ReportSaldoAplicacaoCotista(int idCarteira, int? idCotista, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, string login,
                                                TipoLingua lingua)
        {

            this.SaldoAplicacaoCotista(idCarteira, idCotista, data, tipoRelatorio,
                                    tipoGrupamento, opcaoValorAplicado, false, login, lingua);
        }

        /// <summary>
        /// Construtor private
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>
        /// <param name="exibeRentabilidade"></param>
        /// <param name="login"></param>
        private void SaldoAplicacaoCotista(int idCarteira, int? idCotista, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, bool exibeRentabilidade, string login,
                                                TipoLingua lingua)
        {            
            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.data = data;
            this.login = login;
            this.tipoRelatorio = tipoRelatorio;
            this.tipoGrupamento = tipoGrupamento;
            this.opcaoValorAplicado = opcaoValorAplicado;
            this.tipoLingua = lingua;
            //
            this.exibeRentabilidade = exibeRentabilidade;
            //
            this.idCotistaGrupo = new List<int>();
            //

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, this.IdCarteira);

            if (data >= cliente.DataDia.Value) {
                this.tipoPesquisa = TipoPesquisa.PosicaoCotista;
            }
            else {
                this.tipoPesquisa = TipoPesquisa.PosicaoCotistaHistorico;
            }

            this.InitializeComponent();
                        
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2396);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();                      
        }


        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                
                this.xrSubreport3.Visible = true;
                //
                this.Detail.Visible = false;
                this.GroupHeader1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.GroupFooter1.Visible = false;
                this.ReportFooter.Visible = false;
                this.xrTable6.Visible = false;
                this.xrTable7.Visible = false;
                
                // Caso onde Report não tem Dados
                this.xrTable5.Visible = false;

                // Caso onde Report não tem Dados
                this.xrTable12.Visible = false;
            }
        }

        private void PersonalInitialize() {
            string linguaSelecionada = StringEnum.GetStringValue((TipoLingua)this.tipoLingua);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);
  
            if (this.tipoLingua == TipoLingua.Ingles) {
                //Linhas de somatorio de PF e PJ
                xrTableRow12.Visible = false;
                xrTableRow13.Visible = false;
            }

            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.ReportSaldoAplicacaoCotista._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportSaldoAplicacaoCotista._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportSaldoAplicacaoCotista._DataPosicao;
            this.xrTableCell7.Text = Resources.ReportSaldoAplicacaoCotista._Carteira;
            this.xrTableCell6.Text = Resources.ReportSaldoAplicacaoCotista._ValorCotaAtual;
            this.xrTableCell82.Text = Resources.ReportSaldoAplicacaoCotista._ValorCotaMedia;
            this.xrTableCellDataAplicacao.Text = Resources.ReportSaldoAplicacaoCotista._DataAplicacao;
            this.xrTableCellValorCotaAplicacao.Text = Resources.ReportSaldoAplicacaoCotista._ValorCotaAplicacao;
            this.xrTableCell26.Text = Resources.ReportSaldoAplicacaoCotista._ValorAplicado;
            this.xrTableCell27.Text = Resources.ReportSaldoAplicacaoCotista._QtdCotas;
            this.xrTableCell34.Text = Resources.ReportSaldoAplicacaoCotista._SaldoBruto;
            this.xrTableCell28.Text = Resources.ReportSaldoAplicacaoCotista._ValorIR;
            this.xrTableCell29.Text = Resources.ReportSaldoAplicacaoCotista._ValorIOF;
            this.xrTableCell30.Text = Resources.ReportSaldoAplicacaoCotista._SaldoLiquido;
            this.xrTableCell31.Text = Resources.ReportSaldoAplicacaoCotista._ValorPerformance;
            this.xrTableCell32.Text = Resources.ReportSaldoAplicacaoCotista._Participacao;
            this.xrTableCell70.Text = Resources.ReportSaldoAplicacaoCotista._QtdePendenteLiquidacao;
            this.xrTableCell71.Text = Resources.ReportSaldoAplicacaoCotista._ValorPendenteLiquidacao;                                      
            #endregion                     
        }

        private DataTable FillDados() {                                 
            esUtility u = new esUtility();

            esParameters esParams = new esParameters();
            esParams.Add("DataHistorico", this.data);  

            #region SQL
            StringBuilder sqlText = new StringBuilder();            
            sqlText.AppendLine(" SELECT A.Nome AS NomeCarteira, ");
            sqlText.AppendLine("        A.IdCarteira AS IdCarteira, ");
            sqlText.AppendLine("        C.IdCotista AS IdCotista, ");
            sqlText.AppendLine("        C.Nome AS NomeCotista, ");
            sqlText.AppendLine("        C.CodigoInterface, ");
            sqlText.AppendLine("        P.CotaDia, ");
            sqlText.AppendLine("        P.DataAplicacao, ");
            sqlText.AppendLine("        P.CotaAplicacao, ");

            if (this.opcaoValorAplicado == OpcaoValorAplicado.ValorAtualizado) {
                sqlText.AppendLine("        P.Quantidade * P.CotaAplicacao AS ValorAplicacao, ");
            }
            else {
                sqlText.AppendLine("        P.ValorAplicacao, ");
            }

            sqlText.AppendLine("        P.Quantidade, ");
            sqlText.AppendLine("        P.ValorBruto, ");
            sqlText.AppendLine("        P.ValorIR, ");
            sqlText.AppendLine("        P.ValorIOF, ");
            sqlText.AppendLine("        P.ValorPerformance, ");
            sqlText.AppendLine("        P.ValorLiquido, ");
            sqlText.AppendLine("        ((P.CotaDia - CotaAplicacao) * P.Quantidade) As 'Rendimento', ");
            sqlText.AppendLine("        ((P.CotaDia/CotaAplicacao) - 1) As 'PercentualRetorno', ");
            sqlText.AppendLine("        P.QtdePendenteLiquidacao, ");
            sqlText.AppendLine("        P.ValorPendenteLiquidacao ");

            if (this.tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                sqlText.AppendLine(" FROM [PosicaoCotista] P, ");
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                sqlText.AppendLine(" FROM [PosicaoCotistaHistorico] P, ");
            }

            sqlText.AppendLine("      [Cotista] C, ");
            sqlText.AppendLine("      [Carteira] A, ");
            sqlText.AppendLine("      [PermissaoCotista] PC, ");
            sqlText.AppendLine("      [Usuario] U ");

            sqlText.AppendLine(" WHERE P.IdCotista = C.IdCotista ");
            sqlText.AppendLine("       AND P.IdCarteira = A.IdCarteira ");            
            sqlText.AppendLine("       AND P.Quantidade != 0 ");
            sqlText.AppendLine("       AND P.IdCarteira = " + this.idCarteira);

            if (this.idCotista.HasValue) {
                sqlText.AppendLine("       AND P.IdCotista = " + this.idCotista.Value);
            }

            if (this.tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                sqlText.AppendLine("    AND P.DataHistorico = @DataHistorico ");
            }

            sqlText.AppendLine("       AND PC.idCotista = C.idCotista ");
            sqlText.AppendLine("       AND PC.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND U.login = '" + this.login + "'");

            //if (log.IsInfoEnabled) {
                log.Info(sqlText.ToString());
            //}
            #endregion                       

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);
            
            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Cotista/Xml/SaldoAplicacaoCotista.xml");
            
            return dt;
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportSaldoAplicacaoCotista.resx"; 
        */
        private void InitializeComponent() {
            string resourceFileName = "ReportSaldoAplicacaoCotista.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellDataAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValorCotaAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellCotistaGroupFooter = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.calculatedFieldParticipacao = new DevExpress.XtraReports.UI.CalculatedField();
            this.ParameterTotalValorBruto = new DevExpress.XtraReports.Parameters.Parameter();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 53F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell80,
            this.xrTableCell81});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DataAplicacao", "{0:d}")});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.077212020033388978;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CotaAplicacao", "{0:n8}")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "$ValorCotaAplicacao";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.099814888329287046;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorAplicacao", "{0:n2}")});
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "$ValorAplicado";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.10422647913859284;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:n8}")});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "$QtdCotas";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.15783619799010826;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto", "{0:n2}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "$SaldoBruto";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.10848971334000852;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR", "{0:n2}")});
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Text = "$ValorIR";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.080133555926544239;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF", "{0:n2}")});
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "$ValorIOF";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.090150250417362271;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido", "{0:n2}")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "$SaldoLiquido";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.11404043134888321;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorPerformance", "{0:n2}")});
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "$ValorPfee";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.089819166471003145;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldParticipacao")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.078277297004821486;
            this.xrTableCell36.WordWrap = false;
            this.xrTableCell36.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QtdePendenteLiquidacao", "{0:n8}")});
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell80.Text = "$QtdePendenteLiquidacao";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell80.Weight = 0.11944706587933232;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorPendenteLiquidacao", "{0:n2}")});
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell81.Text = "$QtdePendenteLiquidacao";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell81.Weight = 0.092572967509649329;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrTable10,
            this.xrPageInfo2,
            this.xrPanel1,
            this.xrTable7});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 433F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(16F, 318F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(64F, 64F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1842F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell25.Weight = 0.12649294245385451;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.87350705754614555;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2392F, 265F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(84F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15,
            this.xrTable11,
            this.xrPageInfo3,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 101F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(2286F, 260F);
            // 
            // xrTable15
            // 
            this.xrTable15.Dpi = 254F;
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(3F, 200F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(613F, 42F);
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82,
            this.xrTableCell83});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.CanGrow = false;
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell82.Text = "#ValorCotaMedia";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell82.Weight = 0.44861337683523655;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.CanGrow = false;
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell83.Weight = 0.5513866231647635;
            // 
            // xrTable11
            // 
            this.xrTable11.Dpi = 254F;
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(3F, 148F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(613F, 42F);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell56});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#ValorCotaAtual";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.44861337683523655;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.CanGrow = false;
            this.xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CotaDia", "{0:n8}")});
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell56.Weight = 0.5513866231647635;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(275F, 3F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(249F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 103F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1593F, 42F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#Carteira";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.17263025737602009;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.NomeCarteira")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.82736974262397989;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(593F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.463743676222597;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.53625632377740307;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(275F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 380F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellDataAplicacao,
            this.xrTableCellValorCotaAplicacao,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCellDataAplicacao
            // 
            this.xrTableCellDataAplicacao.Dpi = 254F;
            this.xrTableCellDataAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataAplicacao.Name = "xrTableCellDataAplicacao";
            this.xrTableCellDataAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataAplicacao.Text = "#DataAplicaçao ";
            this.xrTableCellDataAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellDataAplicacao.Weight = 0.080968280467445738;
            // 
            // xrTableCellValorCotaAplicacao
            // 
            this.xrTableCellValorCotaAplicacao.Dpi = 254F;
            this.xrTableCellValorCotaAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellValorCotaAplicacao.Name = "xrTableCellValorCotaAplicacao";
            this.xrTableCellValorCotaAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellValorCotaAplicacao.Text = "#ValorCotaAplicacao";
            this.xrTableCellValorCotaAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellValorCotaAplicacao.Weight = 0.091059937569435631;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#ValorAplicado";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.10128344994219965;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "#QtdCotas";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.15337940431368854;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "#SaldoBruto";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.10542634118352545;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#ValorIR";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.0778707657120405;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Text = "#ValorIOF";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.08760473005158334;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Text = "#SaldoLiquido";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.1108203176364309;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Text = "#ValorPfee";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.087282855554016284;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Text = "#Particip.";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.076066989728519829;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell70.Text = "#QtdePendenteLiquidacao.";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell70.Weight = 0.11713509144712413;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.Text = "#ValorPendenteLiquidacao.";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell71.Weight = 0.088898163606010022;
            // 
            // xrTable12
            // 
            this.xrTable12.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable12.Dpi = 254F;
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(100F, 212F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable12.SizeF = new System.Drawing.SizeF(2396F, 42F);
            this.xrTable12.StylePriority.UseBackColor = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeBeforePrint);
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell38,
            this.xrTableCell66,
            this.xrTableCell67});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "#RentabilidadeMensal";
            this.xrTableCell33.Weight = 0.12604340567612687;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.Text = "RentMensal";
            this.xrTableCell38.Weight = 0.10601001669449083;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.Text = "#RentabilidadeAno";
            this.xrTableCell66.Weight = 0.11477462437395659;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.Text = "RentAno";
            this.xrTableCell67.Weight = 0.65317195325542576;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 79F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(101F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(386F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCotista", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 53F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.BackColor = System.Drawing.Color.LightGray;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "$Cotista";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 1;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloGroupHeaderBeforePrint);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable8});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 95F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupFooter1.AfterPrint += new System.EventHandler(this.GroupFooteAfterPrint);
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 61F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2396F, 30F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 1;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable8.BorderWidth = 1;
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellCotistaGroupFooter,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell72,
            this.xrTableCell73});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCellCotistaGroupFooter
            // 
            this.xrTableCellCotistaGroupFooter.CanShrink = true;
            this.xrTableCellCotistaGroupFooter.Dpi = 254F;
            this.xrTableCellCotistaGroupFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellCotistaGroupFooter.Name = "xrTableCellCotistaGroupFooter";
            this.xrTableCellCotistaGroupFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellCotistaGroupFooter.Text = "$Cotista";
            this.xrTableCellCotistaGroupFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellCotistaGroupFooter.Weight = 0.15936027682781317;
            this.xrTableCellCotistaGroupFooter.WordWrap = false;
            this.xrTableCellCotistaGroupFooter.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloGroupFooterBeforePrint);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorAplicacao")});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell39.Summary = xrSummary1;
            this.xrTableCell39.Text = "xrTableCell39";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.11453246749232408;
            this.xrTableCell39.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorAplicacaoSummaryCalculated);
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary2.FormatString = "{0:n8}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell40.Summary = xrSummary2;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell40.Weight = 0.1537055424571625;
            this.xrTableCell40.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.QuantidadeSummaryCalculated);
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto")});
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell41.Summary = xrSummary3;
            this.xrTableCell41.Text = "xrTableCell41";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.10565041610498036;
            this.xrTableCell41.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorBrutoSummaryCalculated);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell42.Summary = xrSummary4;
            this.xrTableCell42.Text = "xrTableCell42";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.0780363882104056;
            this.xrTableCell42.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorIRSummaryCalculated);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell43.Summary = xrSummary5;
            this.xrTableCell43.Text = "xrTableCell43";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.087790931099199082;
            this.xrTableCell43.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorIOFSummaryCalculated);
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell44.Summary = xrSummary6;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.11105594644950491;
            this.xrTableCell44.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorLiquidoSummaryCalculated);
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorPerformance")});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell45.Summary = xrSummary7;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.087468492957228736;
            this.xrTableCell45.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorPerformanceSummaryCalculated);
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldParticipacao")});
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell46.Summary = xrSummary8;
            this.xrTableCell46.Text = "xrTableCell46";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.076228719072317125;
            this.xrTableCell46.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            this.xrTableCell46.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ParticipacaoSummaryCalculated);
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QtdePendenteLiquidacao")});
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary9.IgnoreNullValues = true;
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell72.Summary = xrSummary9;
            this.xrTableCell72.Text = "xrTableCell72";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell72.Weight = 0.1163210697464267;
            this.xrTableCell72.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.QtdePendenteLiquidacaoSummaryCalculated);
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorPendenteLiquidacao")});
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell73.Summary = xrSummary10;
            this.xrTableCell73.Text = "xrTableCell73";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell73.Weight = 0.090150250417362271;
            this.xrTableCell73.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorPendenteLiquidacaoSummaryCalculated);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12,
            this.xrTable9});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 254F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.BackColor = System.Drawing.Color.LightGray;
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable9.BorderWidth = 1;
            this.xrTable9.Dpi = 254F;
            this.xrTable9.KeepTogether = true;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow12,
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(2396F, 159F);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableSummaryFinalBeforePrint);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell78,
            this.xrTableCell79});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.33333333333333331;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.Text = "#TotalPessoaFisica";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.15868405823645104;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.Text = "TotalValorAplicadoPessoaFisica";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.11404647426755188;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.Text = "TotalQuantidadePessoaFisica";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.15493409114226966;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.Text = "TotalSaldoBrutoPessoaFisica";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.10332136499628276;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.Text = "TotalValorIRPessoaFisica";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.077705201093145992;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.Text = "TotalValorIOFPessoaFisica";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell62.Weight = 0.087418470050686176;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.Text = "TotalSaldoLiquidoPessoaFisica";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell63.Weight = 0.11058476717965705;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.Text = "TotalValorPerformancePessoaFisica";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell64.Weight = 0.087097284493180063;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell65.Text = "TotalParticipacaoPessoaFisica";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell65.Weight = 0.075905262340972085;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell78.Text = "TotalQtdePendenteLiquidacaoPessoaFisica";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell78.Weight = 0.11794910299446107;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.Text = "TotalValorPendenteLiquidacaoPessoaFisica";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell79.Weight = 0.08764607679465776;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell76,
            this.xrTableCell77});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.33333333333333331;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "#TotalPessoaJuridica";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.15868405823645104;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "TotalValorAplicadoPessoaJuridica";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.1140464443284282;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "TotalQuantidadePessoaJuridica";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.15493412108139332;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "TotalsaldoBrutoPessoaJuridica";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.10332136499628275;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "TotalValorIRPessoaJuridica";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.077705201093145992;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "TotalValorIOFPessoaJuridica";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.087418470050686176;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "TotalSaldoLiquidoPessoaJuridica";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.11058470730140979;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "TotalValorPerformancePessoaJuridica";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.08709734437142734;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "TotalParticipacaoPessoaJuridica";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.075905262340972085;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell76.Text = "TotalQtdePendenteLiquidacaoPessoaJuridica";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell76.Weight = 0.11794910299446107;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell77.Text = "TotalValorPendenteLiquidacaoPessoaJuridica";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell77.Weight = 0.08764607679465776;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow9.Weight = 0.33333333333333331;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Text = "#TotalCotista";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 0.15868405823645104;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.Text = "TotalValorAplicadoCotista";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.11404647426755188;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "TotalQuantidadeCotista";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.15493409114226966;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.Text = "TotalSaldoBrutoCotista";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.10332136499628275;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "TotalValorIRCotista";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.077705201093145992;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "TotalValorIOFCotista";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.087418470050686176;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "TotalSaldoLiquidoCotista";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.11058476717965705;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Text = "TotalValorPerformanceCotista";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.087097284493180063;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.Text = "TotalParticipacaoCotista";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell54.Weight = 0.075905262340972085;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.Text = "TotalQtdePendenteLiquidacao";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell74.Weight = 0.11794910299446107;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.Text = "TotalValorPendenteLiquidacao";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell75.Weight = 0.08764607679465776;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.Expanded = false;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DataAplicacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.Height = 0;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // calculatedFieldParticipacao
            // 
            this.calculatedFieldParticipacao.DataMember = "esUtility";
            this.calculatedFieldParticipacao.Expression = "[ValorBruto]  / [Parameters.ParameterTotalValorBruto]";
            this.calculatedFieldParticipacao.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldParticipacao.Name = "calculatedFieldParticipacao";
            // 
            // ParameterTotalValorBruto
            // 
            this.ParameterTotalValorBruto.Name = "ParameterTotalValorBruto";
            this.ParameterTotalValorBruto.Type = typeof(decimal);
            this.ParameterTotalValorBruto.Value = 0;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportSaldoAplicacaoCotista
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.GroupHeader2,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldParticipacao});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(25, 20, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterTotalValorBruto});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RelatorioBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportSaldoAplicacaoCotista.ResourceManager;

        }

        // Colunas da Tabela
        private string idCotistaColumn = "IdCotista";
        private string nomeCotistaColumn = "NomeCotista";        
        //
        #region Variaveis Internas do Relatorio
        //
        protected class ValoresTotais {
            public decimal totalSaldoBruto = 0;
            public decimal totalQuantidadeCotas = 0;
            public decimal totalValorIR = 0;
            public decimal totalValorIOF = 0;
            public decimal totalValorPerformance = 0;
            public decimal totalSaldoLiquido = 0;
            public decimal totalValorAplicacao = 0;
            public decimal totalParticipacao = 0;
            public decimal totalQtdePendenteLiquidacao = 0;
            public decimal totalValorPendenteLiquidacao = 0;
        }
        //
        private ValoresTotais valoresPessoaFisica = new ValoresTotais();
        private ValoresTotais valoresPessoaJuridica = new ValoresTotais();
        private ValoresTotais valoresTotalCotista = new ValoresTotais();                
        //
        private int totalCotistas = 0;
        private int totalPessoasFisicas = 0;
        private int totalPessoasJuridicas = 0;
        //

        // Controla a Quebra de Pagina do Grupo IdCotista - controla quais grupo de Cotista já passaram
        // para fazer a soma por Pessoa fisica e Juridica
        private List<int> idCotistaGrupo = new List<int>();

        #endregion

        #region Funções Internas do Relatorio
        //
        #region Summary Final
        private void ValorAplicacaoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalValorAplicacao += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalValorAplicacao += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalValorAplicacao += Convert.ToDecimal(e.Value);
            }
        }
        
        private void QuantidadeSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalQuantidadeCotas += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalQuantidadeCotas += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalQuantidadeCotas += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorBrutoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalSaldoBruto += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalSaldoBruto += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalSaldoBruto += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorIRSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalValorIR += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalValorIR += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalValorIR += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorIOFSummaryCalculated(object sender, TextFormatEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalValorIOF += Convert.ToDecimal(e.Value);
                }
                else
                {
                    this.valoresPessoaJuridica.totalValorIOF += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalValorIOF += Convert.ToDecimal(e.Value);
            }
        }

        private void QtdePendenteLiquidacaoSummaryCalculated(object sender, TextFormatEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalQtdePendenteLiquidacao += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalQtdePendenteLiquidacao += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalQtdePendenteLiquidacao += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorPendenteLiquidacaoSummaryCalculated(object sender, TextFormatEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalValorPendenteLiquidacao += Convert.ToDecimal(e.Value);
                }
                else
                {
                    this.valoresPessoaJuridica.totalValorPendenteLiquidacao += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalValorPendenteLiquidacao += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorLiquidoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalSaldoLiquido += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalSaldoLiquido += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalSaldoLiquido += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorPerformanceSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                #region Somatoria Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalValorPerformance += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalValorPerformance += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalValorPerformance += Convert.ToDecimal(e.Value);
            }
        }

        private void ParticipacaoSummaryCalculated(object sender, TextFormatEventArgs e) {            
            if (this.numeroLinhasDataTable != 0) {
                #region Calcula Participação Por Pessoa Fisica e Juridica
                int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);

                if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                {
                    this.valoresPessoaFisica.totalParticipacao += Convert.ToDecimal(e.Value);
                }
                else {
                    this.valoresPessoaJuridica.totalParticipacao += Convert.ToDecimal(e.Value);
                }
                #endregion
                //                        
                this.valoresTotalCotista.totalParticipacao += Convert.ToDecimal(e.Value);
            }                                         
        }

        private void GroupFooteAfterPrint(object sender, EventArgs e) {
            if (this.numeroLinhasDataTable != 0) {

                int controleIdCotistaGrupo = (int)this.GetCurrentColumnValue(idCotistaColumn);

                if (!this.idCotistaGrupo.Contains(controleIdCotistaGrupo)) {
                    // Adiciona o IdCotista do Grupo Atual
                    this.idCotistaGrupo.Add(controleIdCotistaGrupo);
                    //
                    this.totalCotistas++;
                    //                    
                    Cotista cotista = new Cotista();
                    cotista.LoadByPrimaryKey(controleIdCotistaGrupo);

                    if (cotista.UpToPessoaByIdPessoa.IsPessoaFisica())
                    {
                        this.totalPessoasFisicas++;
                    }
                    else {
                        this.totalPessoasJuridicas++;
                    }
                }
            }
        }
        #endregion    

        private void TableSummaryFinalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            // Prenche Linha 0 - Total Pessoa Fisica
            /* Total Cotista
               ValorAplicacao
               Quantidade
               SaldoBruto
               ValorIR
               ValorIOF
               ValorLiquido
               ValorPerformance
               Participacao
               Rendimento
               Retorno 
             */
            string pessoasFisicas = Resources.ReportSaldoAplicacaoCotista._PessoasFisicas;
            string pessoasJuridicas = Resources.ReportSaldoAplicacaoCotista._PessoasJuridicas;
            string total = Resources.ReportSaldoAplicacaoCotista._Total;

            #region Linha 0
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];            
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = pessoasFisicas + ": " + this.totalPessoasFisicas;
            ((XRTableCell)summaryFinalRow0.Cells[1]).Text = this.valoresPessoaFisica.totalValorAplicacao.ToString("n2");            
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = this.valoresPessoaFisica.totalQuantidadeCotas.ToString("n8");
            ((XRTableCell)summaryFinalRow0.Cells[3]).Text = this.valoresPessoaFisica.totalSaldoBruto.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[4]).Text = this.valoresPessoaFisica.totalValorIR.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[5]).Text = this.valoresPessoaFisica.totalValorIOF.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[6]).Text = this.valoresPessoaFisica.totalSaldoLiquido.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[7]).Text = this.valoresPessoaFisica.totalValorPerformance.ToString("n2");

            // Os valores foram somados em porcentagem, assim é necessario multiplicar por 100
            this.valoresPessoaFisica.totalParticipacao = this.valoresPessoaFisica.totalParticipacao * 100;
            ((XRTableCell)summaryFinalRow0.Cells[8]).Text = this.valoresPessoaFisica.totalParticipacao.ToString("n2") + "%";

            ((XRTableCell)summaryFinalRow0.Cells[9]).Text = this.valoresPessoaFisica.totalQtdePendenteLiquidacao.ToString("n8");
            ((XRTableCell)summaryFinalRow0.Cells[10]).Text = this.valoresPessoaFisica.totalValorPendenteLiquidacao.ToString("n2");
            #endregion

            // Prenche Linha 1 - Total Pessoa Juridica
            #region Linha 1
            XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];            
            ((XRTableCell)summaryFinalRow1.Cells[0]).Text = pessoasJuridicas + ": " + this.totalPessoasJuridicas;
            ((XRTableCell)summaryFinalRow1.Cells[1]).Text = this.valoresPessoaJuridica.totalValorAplicacao.ToString("n2");
            ((XRTableCell)summaryFinalRow1.Cells[2]).Text = this.valoresPessoaJuridica.totalQuantidadeCotas.ToString("n8");
            ((XRTableCell)summaryFinalRow1.Cells[3]).Text = this.valoresPessoaJuridica.totalSaldoBruto.ToString("n2");
            ((XRTableCell)summaryFinalRow1.Cells[4]).Text = this.valoresPessoaJuridica.totalValorIR.ToString("n2");
            ((XRTableCell)summaryFinalRow1.Cells[5]).Text = this.valoresPessoaJuridica.totalValorIOF.ToString("n2");
            ((XRTableCell)summaryFinalRow1.Cells[6]).Text = this.valoresPessoaJuridica.totalSaldoLiquido.ToString("n2");
            ((XRTableCell)summaryFinalRow1.Cells[7]).Text = this.valoresPessoaJuridica.totalValorPerformance.ToString("n2");
           
            // Os valores foram somados em porcentagem, assim é necessario multiplicar por 100
            this.valoresPessoaJuridica.totalParticipacao = this.valoresPessoaJuridica.totalParticipacao * 100;
            ((XRTableCell)summaryFinalRow1.Cells[8]).Text = this.valoresPessoaJuridica.totalParticipacao.ToString("n2") + "%";

            ((XRTableCell)summaryFinalRow1.Cells[9]).Text = this.valoresPessoaJuridica.totalQtdePendenteLiquidacao.ToString("n8");
            ((XRTableCell)summaryFinalRow1.Cells[10]).Text = this.valoresPessoaJuridica.totalValorPendenteLiquidacao.ToString("n2");
            #endregion

            // Prenche Linha 2 - Total Cotista
            #region Linha 2
            XRTableRow summaryFinalRow2 = summaryFinal.Rows[2];
            ((XRTableCell)summaryFinalRow2.Cells[0]).Text = total + ": " + this.totalCotistas;
            ((XRTableCell)summaryFinalRow2.Cells[1]).Text = this.valoresTotalCotista.totalValorAplicacao.ToString("n2");
            ((XRTableCell)summaryFinalRow2.Cells[2]).Text = this.valoresTotalCotista.totalQuantidadeCotas.ToString("n8");
            ((XRTableCell)summaryFinalRow2.Cells[3]).Text = this.valoresTotalCotista.totalSaldoBruto.ToString("n2");
            ((XRTableCell)summaryFinalRow2.Cells[4]).Text = this.valoresTotalCotista.totalValorIR.ToString("n2");
            ((XRTableCell)summaryFinalRow2.Cells[5]).Text = this.valoresTotalCotista.totalValorIOF.ToString("n2");
            ((XRTableCell)summaryFinalRow2.Cells[6]).Text = this.valoresTotalCotista.totalSaldoLiquido.ToString("n2");
            ((XRTableCell)summaryFinalRow2.Cells[7]).Text = this.valoresTotalCotista.totalValorPerformance.ToString("n2");
            // 100%
            this.valoresTotalCotista.totalParticipacao = 100M;
            ((XRTableCell)summaryFinalRow2.Cells[8]).Text = this.valoresTotalCotista.totalParticipacao.ToString("n2") + "%";

            ((XRTableCell)summaryFinalRow2.Cells[9]).Text = this.valoresTotalCotista.totalQtdePendenteLiquidacao.ToString("n8");
            ((XRTableCell)summaryFinalRow2.Cells[10]).Text = this.valoresTotalCotista.totalValorPendenteLiquidacao.ToString("n2");
            #endregion
        }        

        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell textNomeCarteira = sender as XRTableCell;
            //
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCarteira);

            string nomeCarteira = this.idCarteira + " - " + carteira.Nome;

            ClienteInterface clienteInterface = new ClienteInterface();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(clienteInterface.Query.CodigoYMF);
            clienteInterface.LoadByPrimaryKey(campos, this.idCarteira);

            string codigoInterface = "";
            if (!String.IsNullOrEmpty(clienteInterface.CodigoYMF))
            {
                codigoInterface = Convert.ToString(clienteInterface.CodigoYMF);
            }

            if (codigoInterface != "")
            {
                nomeCarteira = nomeCarteira + " (" + codigoInterface + ") ";
            }
            //
            textNomeCarteira.Text = nomeCarteira;
        }

        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.data.ToString("d");
        }

        private void TituloGroupHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) 
        {
            if (this.numeroLinhasDataTable != 0) {
                XRTableCell tituloGroupHeaderXRTableCell = sender as XRTableCell;
                //
                string titulo = "";
                int idCotista = (int)GetCurrentColumnValue(CotistaMetadata.ColumnNames.IdCotista);
                
                string codigoInterface = "";
                if (!String.IsNullOrEmpty(Convert.ToString(GetCurrentColumnValue(CotistaMetadata.ColumnNames.CodigoInterface))))
                {
                    codigoInterface = Convert.ToString(GetCurrentColumnValue(CotistaMetadata.ColumnNames.CodigoInterface));
                }

                string nomeCotista = (string)GetCurrentColumnValue(this.nomeCotistaColumn);
                titulo = idCotista + " - " + nomeCotista.Trim();

                if (codigoInterface != "")
                {
                    titulo = titulo + " (" + codigoInterface + ") ";
                }


                // Prejuizo Cotista
                int idCarteira = Convert.ToInt32(GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira));

                decimal prejuizoAcumulado = 0.00M;
                if (this.tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                    #region Prejuizo Cotista
                    PrejuizoCotista p = new PrejuizoCotista();
                    p.Query.Select(p.Query.ValorPrejuizo.Sum())
                           .Where(p.Query.IdCotista == idCotista &&
                                  p.Query.IdCarteira == idCarteira);

                    if (p.Query.Load()) {
                        if (p.ValorPrejuizo.HasValue) {
                            prejuizoAcumulado = p.ValorPrejuizo.Value;
                        }
                    }
                    #endregion
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                    #region Prejuizo Cotista Historico
                    PrejuizoCotistaHistorico p = new PrejuizoCotistaHistorico();
                    p.Query.Select(p.Query.ValorPrejuizo.Sum())
                           .Where(p.Query.IdCotista == idCotista &&
                                  p.Query.IdCarteira == idCarteira &&
                                  p.Query.DataHistorico == this.data);

                    if (p.Query.Load()) {
                        if (p.ValorPrejuizo.HasValue) {
                            prejuizoAcumulado = p.ValorPrejuizo.Value;
                        }
                    }
                    #endregion
                }

                titulo += " - " + Resources.ReportSaldoAplicacaoCotista._Prejuizo_Acumulado + ": " + prejuizoAcumulado.ToString("n2");
                
                tituloGroupHeaderXRTableCell.Text = titulo;

                #region Verifica se carteira utiliza cota media no rendimento
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                if (carteira.TipoCusto.Equals((byte)TipoCustoFundo.MedioAplicado))
                {
                    #region Seleciona cota média
                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    this.xrTableCell83.Text = (posicaoCotista.RetornaCotaMedia(idCarteira, idCotista).ToString("n8"));
                    #endregion
                }
                else
                {
                    this.xrTable15.Visible = false;
                }
                #endregion

            }
        }

        private void TituloGroupFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Desaparece com Cotista do Grupo Footer se for Relatorio Analitico
            if (this.numeroLinhasDataTable != 0) {
                if (this.tipoRelatorio == TipoRelatorio.Consolidado) {
                    XRTableCell tituloGroupFooterXRTableCell = sender as XRTableCell;
                    //
                    string titulo = "";
                    int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
                    string nomeCotista = (string)GetCurrentColumnValue(this.nomeCotistaColumn);
                    titulo = idCotista + " - " + nomeCotista;
                    tituloGroupFooterXRTableCell.Text = titulo;
                }
                else {
                    this.xrTableCellCotistaGroupFooter.Text = "";
                }
            }
        }
        
        private void RelatorioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Define se Detail será exibido
            #region Define se Detail será exibido
            if (this.tipoRelatorio == TipoRelatorio.Analitico) {
                this.GroupHeader1.Visible = true;
                this.Detail.Visible = true;                
            }
            else if (this.tipoRelatorio == TipoRelatorio.Consolidado) {
                this.GroupHeader1.Visible = false;
                this.Detail.Visible = false;
            }
            #endregion
            
            #region Define o Tipo de Agrupamento do Relatorio
            if (this.tipoGrupamento == TipoGrupamento.PorCodigo) {
                // Define o Agrupamento do Relatorio
                this.GroupHeader1.GroupFields[0].FieldName = this.idCotistaColumn;
                //this.GroupHeader1.GroupFields[0].SortOrder = XRColumnSortOrder.Ascending;
            }
            else if (this.tipoGrupamento == TipoGrupamento.PorNome) {
                // Define o Agrupamento do Relatorio
                this.GroupHeader1.GroupFields[0].FieldName = this.nomeCotistaColumn;
                //this.GroupHeader1.GroupFields[0].SortOrder = XRColumnSortOrder.Ascending;
            }
            #endregion

            // Desaparece com DataAplicacao e ValorCotaAplicacao
            if (this.tipoRelatorio == TipoRelatorio.Consolidado) {
                this.xrTableCellDataAplicacao.Text = "";
                this.xrTableCellValorCotaAplicacao.Text = "";
            }
            
            #region Preenche Parametros para Poder Fazer SUM de % de Participação
            
            // Calcula o TotalValorLiquido para ser usado no Calculo da Participação
            decimal totalValorBruto = 0.0M;
            //
            if (this.tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                totalValorBruto = posicaoCotista.RetornaSumValorBruto(this.idCarteira);
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                totalValorBruto = posicaoCotistaHistorico.RetornaSumValorBruto(this.idCarteira, this.data);
            }
            // Preenche o TotalValorMercado para ser Usado no Calculo do Swap
            this.Parameters["ParameterTotalValorBruto"].Value = totalValorBruto;
            //
            #endregion
        }

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);

        }

        private void RentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;

            #region Zera os campos da Table
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            ((XRTableCell)xrTableRow0.Cells[0]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[1]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[2]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[3]).Text = "";
            #endregion
            //
            xrTable.Visible = this.exibeRentabilidade;
            if (this.numeroLinhasDataTable == 0) {
                // Se Relatório não tem dados não exibe a Tabela
                xrTable.Visible = false;
            }
            
            //
            // Se for para Exibir
            if (this.exibeRentabilidade) {                
                /*
                 * Rentabilidades
                 * 1-(Mês) - 2-(Ano)
                 * 
                */
                #region Calcula Rentabilidade Cota
                CalculoMedida calculoMedida = new CalculoMedida();

                // Carrega a Carteira
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(this.idCarteira);
                //
                calculoMedida.SetIdCarteira(this.idCarteira);
                //
                calculoMedida.SetDataInicio(carteira.DataInicioCota.Value);
                
                #region rentabilidadeMes
                decimal? rentabilidadeMes = null;
                try {
                    rentabilidadeMes = calculoMedida.CalculaRetornoMes(this.data);
                    rentabilidadeMes = rentabilidadeMes / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }                
                #endregion

                #region rentabilidadeAno
                decimal? rentabilidadeAno = null;
                try {
                    rentabilidadeAno = calculoMedida.CalculaRetornoAno(this.data);
                    rentabilidadeAno = rentabilidadeAno / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                #endregion

                #endregion

                /* Exibe Rentabilidade */
                #region Exibe Rentabilidade
                ((XRTableCell)xrTableRow0.Cells[0]).Text = Resources.ReportSaldoAplicacaoCotista._RentabilidadeMensal + ": ";
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[1], rentabilidadeMes);
                //
                ((XRTableCell)xrTableRow0.Cells[2]).Text = Resources.ReportSaldoAplicacaoCotista._RentabilidadeAnual + ": ";
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[3], rentabilidadeAno);
                #endregion
            }
        }

        #endregion

        #endregion
    }
}
