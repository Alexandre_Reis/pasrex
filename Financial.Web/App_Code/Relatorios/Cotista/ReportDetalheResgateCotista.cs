﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using log4net;
using Financial.Security;
using Financial.InvestidorCotista;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportDetalheResgateCotista
    /// </summary>
    public class ReportDetalheResgateCotista : XtraReport {

        private static readonly ILog log = LogManager.GetLogger(typeof(ReportDetalheResgateCotista));

        private int? idCarteira;

        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int? idCotista;

        public int? IdCotista {
            get { return idCotista; }
            set { idCotista = value; }
        }

        //
        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }
       
        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCellDataReferencia;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell25;
        private XRPageInfo xrPageInfo1;
        private XRSubreport xrSubreport17;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport2;
        private SubReportLogotipo subReportLogotipo1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private Financial.InvestidorCotista.OperacaoCotistaCollection operacaoCotistaCollection1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private CalculatedField calculatedFieldCotista;
        private CalculatedField calculatedFieldCarteira;
        private XRSubreport xrSubreport3;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private SubReportDetalheResgateCotista subReportDetalheResgateCotista1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell39;        

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public ReportDetalheResgateCotista(int? idCarteira, int? idCotista, DateTime dataInicio, DateTime dataFim) {
            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2490);
        }

        /// <summary>
        /// Se Relatório não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (!this.HasData) {
                this.xrSubreport17.Visible = true;
            }
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData {
            get {
                return this.numeroLinhasDataTable != 0;
            }
        }

        /// <summary>
        /// Retorna o Cabeçalho dos Dados do Pai
        /// </summary>
        /// <returns></returns>
        [Obsolete("Campo Desnecessário")]
        public XRTable GetRegistroPai {
            get {
                return this.xrTable4;
            }
        }

        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;
        }

        private DataView FillDados() {
            #region SQL
            PermissaoClienteQuery p = new PermissaoClienteQuery("P");
            UsuarioQuery u = new UsuarioQuery("U");
            //
            CotistaQuery cotista = new CotistaQuery("C");
            CarteiraQuery carteira = new CarteiraQuery("A");
            //
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            operacaoCotistaQuery.Select(operacaoCotistaQuery.IdOperacao,
                                        operacaoCotistaQuery.IdCarteira,
                                        operacaoCotistaQuery.IdCotista,
                                        operacaoCotistaQuery.IdPosicaoResgatada,
                                        operacaoCotistaQuery.DataConversao,
                                        operacaoCotistaQuery.DataOperacao,
                                        operacaoCotistaQuery.ValorLiquido,
                                        operacaoCotistaQuery.ValorBruto,
                                        operacaoCotistaQuery.ValorIOF,
                                        operacaoCotistaQuery.ValorIR,
                                        operacaoCotistaQuery.TipoOperacao,
                                        operacaoCotistaQuery.TipoResgate,
                                        operacaoCotistaQuery.Quantidade,
                                        carteira.IdCarteira,
                                        carteira.Nome.Trim(),
                                         (
                                           cotista.IdCotista.Cast(esCastType.String) + " - " + cotista.Nome.Trim() 
                                         )
                                         .As("Cotista"),
                                         (
                                            carteira.IdCarteira.Cast(esCastType.String) + " - " + carteira.Nome.Trim()
                                         )  
                                         .As("Carteira"));
            //
            operacaoCotistaQuery.InnerJoin(cotista).On(operacaoCotistaQuery.IdCotista == cotista.IdCotista);
            operacaoCotistaQuery.InnerJoin(carteira).On(operacaoCotistaQuery.IdCarteira == carteira.IdCarteira);            
            //
            operacaoCotistaQuery.InnerJoin(p).On(p.IdCliente == operacaoCotistaQuery.IdCarteira);
            operacaoCotistaQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
            operacaoCotistaQuery.Where(u.Login == HttpContext.Current.User.Identity.Name);
            //
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.NotEqual(operacaoCotistaQuery.IdCotista));

            // Adiciona Filtro de Carteira
            if (this.idCarteira.HasValue) {
                operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira == this.idCarteira);
            }

            // Adiciona Filtro de Cotista
            if (this.idCotista.HasValue) {
                operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista == this.idCotista);
            }
            #endregion
            //
            this.operacaoCotistaCollection1.QueryReset();
            this.operacaoCotistaCollection1.Load(operacaoCotistaQuery);
            
            #region Elimina Registros Quando Quantidade e ValorLiquido = 0
            OperacaoCotistaCollection operClone = (OperacaoCotistaCollection)Utilitario.Clone(this.operacaoCotistaCollection1);
            
            // Elimina Registros Quando Quantidade e ValorLiquido = 0
            for (int i = 0; i < operClone.Count; i++) {
                OperacaoCotista oper = operClone[i];
                if (oper.Quantidade == 0.00M && oper.ValorLiquido == 0.00M) {
                    //Remove da Collection Original
                    OperacaoCotista o = this.operacaoCotistaCollection1.FindByPrimaryKey(oper.IdOperacao.Value);
                    this.operacaoCotistaCollection1.DetachEntity(o);
                }               
            }
            #endregion

            //string filtro = OperacaoCotistaMetadata.ColumnNames.Quantidade + " <> 0 AND " + OperacaoCotistaMetadata.ColumnNames.ValorLiquido + " <> 0";
            //this.operacaoCotistaCollection1.Filter = filtro;

            #region Elimina os Registros que nao tem Filho
            // Elimina os Registros que nao tem Filho
            OperacaoCotistaCollection operCloneAux = (OperacaoCotistaCollection)Utilitario.Clone(this.operacaoCotistaCollection1);
            for (int i = 0; i < operCloneAux.Count; i++) {
                OperacaoCotista oper = operCloneAux[i];
                //                
                if (!this.ExisteFilho(oper.IdCarteira, oper.IdCotista, oper.IdOperacao.Value, oper.DataConversao.Value)) {
                    //Remove da Collection Original
                    OperacaoCotista o = this.operacaoCotistaCollection1.FindByPrimaryKey(oper.IdOperacao.Value);
                    this.operacaoCotistaCollection1.DetachEntity(o);
                }
            }
            #endregion
            //
            return this.operacaoCotistaCollection1.LowLevelBind();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/DetalheRegateCotista/ReportDetalheResgateCotista.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportDetalheResgateCotista.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDetalheResgateCotista1 = new Financial.Relatorio.SubReportDetalheResgateCotista();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrSubreport17 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataReferencia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.operacaoCotistaCollection1 = new Financial.InvestidorCotista.OperacaoCotistaCollection();
            this.calculatedFieldCotista = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldCarteira = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDetalheResgateCotista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrTable4});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 238.3333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdCotista", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("DataConversao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 125F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportDetalheResgateCotista1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(656F, 106F);
            this.xrSubreport3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubReportBeforePrint);
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow5});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2475F, 112F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell26,
            this.xrTableCell35,
            this.xrTableCell33,
            this.xrTableCell30});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.33333333333333331;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Carteira:";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.072727272727272724;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldCarteira")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.47429283065025257;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Tipo:";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.074284643308080822;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UsePadding = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "TipoOperacao";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell35.Weight = 0.16354373816287879;
            this.xrTableCell35.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoOperacaoBeforePrint);
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Modalidade:";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.08255060369318179;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "TipoResgate";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.13260091145833336;
            this.xrTableCell30.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ModalidadeBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell29,
            this.xrTableCell37,
            this.xrTableCell34,
            this.xrTableCell39});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.33333333333333331;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Cotista:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.072727272727272724;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldCotista")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.47429287997159092;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrSubreport2,
            this.xrSubreport17,
            this.xrPageInfo1,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 291.7916F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1693F, 64F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.15002953337271116;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Detalhamento de Resgates de Cotistas";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.84997046662728881;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(768F, 142F);
            // 
            // xrSubreport17
            // 
            this.xrSubreport17.Dpi = 254F;
            this.xrSubreport17.LocationFloat = new DevExpress.Utils.PointFloat(52.37499F, 252.6666F);
            this.xrSubreport17.Name = "xrSubreport17";
            this.xrSubreport17.ReportSource = this.reportSemDados1;
            this.xrSubreport17.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport17.Visible = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2519F, 198F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(63F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrTable2,
            this.xrTable8,
            this.xrPageInfo3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 174F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1444.292F, 106.3125F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:d}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(190.8333F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(820.2084F, 41F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCellDataReferencia,
            this.xrTableCell13,
            this.xrTableCell3});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.48809523809523808;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.CanGrow = false;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "Período:";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.13242662848962061;
            // 
            // xrTableCellDataReferencia
            // 
            this.xrTableCellDataReferencia.CanGrow = false;
            this.xrTableCellDataReferencia.Dpi = 254F;
            this.xrTableCellDataReferencia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataReferencia.Name = "xrTableCellDataReferencia";
            this.xrTableCellDataReferencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataReferencia.StylePriority.UseFont = false;
            this.xrTableCellDataReferencia.StylePriority.UseTextAlignment = false;
            this.xrTableCellDataReferencia.Text = "DataInicio";
            this.xrTableCellDataReferencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellDataReferencia.Weight = 0.10254113778269282;
            this.xrTableCellDataReferencia.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "à";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.020773710825653185;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Data Fim";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.33137976415002013;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataFimBeforePrint);
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(185F, 42.00002F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.CanGrow = false;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Data Emissão:";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.96943241776324729;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(338.8333F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 102F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(645F, 100F);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // operacaoCotistaCollection1
            // 
            this.operacaoCotistaCollection1.AllowDelete = true;
            this.operacaoCotistaCollection1.AllowEdit = true;
            this.operacaoCotistaCollection1.AllowNew = true;
            this.operacaoCotistaCollection1.EnableHierarchicalBinding = true;
            this.operacaoCotistaCollection1.Filter = "";
            this.operacaoCotistaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.operacaoCotistaCollection1.Sort = "";
            // 
            // calculatedFieldCotista
            // 
            this.calculatedFieldCotista.DataSource = this.operacaoCotistaCollection1;
            this.calculatedFieldCotista.Expression = "[Cotista]";
            this.calculatedFieldCotista.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldCotista.Name = "calculatedFieldCotista";
            // 
            // calculatedFieldCarteira
            // 
            this.calculatedFieldCarteira.DataSource = this.operacaoCotistaCollection1;
            this.calculatedFieldCarteira.Expression = "[Carteira]";
            this.calculatedFieldCarteira.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldCarteira.Name = "calculatedFieldCarteira";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 151F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "Operação:";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.074284643308080808;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "Conversão:";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.082550603693181818;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataOperacao", "{0:dd/MM/yyyy}")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "[DataOperacao]";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 0.16354368884154039;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataConversao", "{0:dd/MM/yyyy}")});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "xrTableCell39";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.13260091145833333;
            // 
            // ReportDetalheResgateCotista
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldCotista,
            this.calculatedFieldCarteira});
            this.DataSource = this.operacaoCotistaCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 151);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportDetalheResgateCotista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportDetalheResgateCotista.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataInicioTableCell = sender as XRTableCell;
            dataInicioTableCell.Text = this.dataInicio.ToString("d");
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataFimTableCell = sender as XRTableCell;
            dataFimTableCell.Text = this.dataFim.ToString("d");
        }

        private void SubReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                int idOperacao = Convert.ToInt32(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.IdOperacao));
                DateTime dataConversao = Convert.ToDateTime(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.DataConversao));
                //
                // Pega o idCotista e idCarteira corrente da linha em execução
                int? idCotistaCorrente = Convert.ToInt32(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.IdCotista));
                int? idCarteiraCorrente = Convert.ToInt32(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.IdCarteira));

                // Passa Parametros para o SubReport
                this.subReportDetalheResgateCotista1.PersonalInitialize(idCarteiraCorrente, idCotistaCorrente, idOperacao, dataConversao);
                //
                //this.xrTable4.Visible = this.subReportDetalheResgateCotista1.HasData;
                //this.xrTable4.Visible = false;
            }
        }

        [Obsolete("Campo selecionado Através do SQL")]
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell carteiraTableCell = sender as XRTableCell;
            //carteiraTableCell.Text = "";

            //if (this.numeroLinhasDataTable != 0) {
            //    string nomeCarteira = Convert.ToString(this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.Nome));
            //    //
            //    carteiraTableCell.Text = nomeCarteira;
            //}
        }
        
        private void TipoOperacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoTableCell = sender as XRTableCell;
            tipoTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                int tipo = Convert.ToInt32(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.TipoOperacao));
                //
                tipoTableCell.Text = StringEnum.GetStringValue((TipoOperacaoCotista)tipo);
            }
        }

        private void ModalidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell modalidadeTableCell = sender as XRTableCell;
            modalidadeTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                int? tipoResgate = null;

                if (!Convert.IsDBNull(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.TipoResgate))) {
                    tipoResgate = Convert.ToInt32(this.GetCurrentColumnValue(OperacaoCotistaMetadata.ColumnNames.TipoResgate));
                }

                if (tipoResgate.HasValue) {
                    modalidadeTableCell.Text = StringEnum.GetStringValue((TipoResgateCotista)tipoResgate);
                }
            }
        }

        /// <summary>
        /// Determina se Existe Registro Filho
        /// </summary>
        /// <returns>True se Existe Registro Filho, False Caso Contrário</returns>
        private bool ExisteFilho(int? idCarteiraCorrente, int? idCotistaCorrente, int idOperacao, DateTime dataConversao) {

            #region SQL
            PosicaoCotistaHistoricoQuery p = new PosicaoCotistaHistoricoQuery("P");
            OperacaoCotistaQuery operacaoQ = new OperacaoCotistaQuery("O");
            PrejuizoCotistaUsadoQuery prejuizoUsadoQ = new PrejuizoCotistaUsadoQuery("R");
            DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
            //            
            detalheResgateCotistaQuery.Select(p.CotaAplicacao, p.DataConversao,
                                        detalheResgateCotistaQuery.Quantidade,
                                        detalheResgateCotistaQuery.ValorLiquido,
                                        detalheResgateCotistaQuery.ValorBruto,
                                        detalheResgateCotistaQuery.PrejuizoUsado,
                                        detalheResgateCotistaQuery.RendimentoResgate,
                                        prejuizoUsadoQ.IdCarteiraCompensada);
            //
            detalheResgateCotistaQuery.InnerJoin(operacaoQ).On(detalheResgateCotistaQuery.IdOperacao == operacaoQ.IdOperacao);
            detalheResgateCotistaQuery.InnerJoin(p).On(detalheResgateCotistaQuery.IdPosicaoResgatada == p.IdPosicao & p.DataHistorico == operacaoQ.DataConversao);
            detalheResgateCotistaQuery.LeftJoin(prejuizoUsadoQ).On(operacaoQ.IdCarteira == prejuizoUsadoQ.IdCarteira & operacaoQ.IdCotista == prejuizoUsadoQ.IdCotista & operacaoQ.DataConversao == prejuizoUsadoQ.DataCompensacao);
            //
            detalheResgateCotistaQuery.Where(detalheResgateCotistaQuery.IdOperacao == idOperacao);
            detalheResgateCotistaQuery.Where(p.DataHistorico == dataConversao);

            // Adiciona Filtro de Carteira
            if (this.idCarteira.HasValue) {
                detalheResgateCotistaQuery.Where(detalheResgateCotistaQuery.IdCarteira == idCarteiraCorrente);
            }

            // Adiciona Filtro de Cotista
            if (this.idCotista.HasValue) {
                detalheResgateCotistaQuery.Where(detalheResgateCotistaQuery.IdCotista == idCotistaCorrente);
            }
            #endregion
            //
            DetalheResgateCotistaCollection d = new DetalheResgateCotistaCollection();
            d.QueryReset();
            d.Load(detalheResgateCotistaQuery);
            
            //return d.LowLevelBind().Count != 0;
            return d.Count != 0;
        }

        #endregion

    }
}
