﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Web;
using System.Globalization;
using Financial.Util;
using System.Resources;
using Financial.Util.Enums;
using System.Text;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Web.Enums;
using System.Threading;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportNotaResgate
    /// </summary>
    public class ReportNotaResgate : XtraReport {
        private int idOperacao;

        public int IdOperacao {
            get { return idOperacao; }
            set { idOperacao = value; }
        }

        // Língua do Relatório
        private TipoLingua tipoLingua;

        public TipoLingua Lingua {
            get { return tipoLingua; }
        }

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private ReportHeaderBand ReportHeader;
        private XRPanel xrPanel3;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell3;
        private XRPanel xrPanel10;
        private XRPanel xrPanel7;
        private XRLabel xrLabel25;
        private XRPanel xrPanel6;
        private XRPanel xrPanel5;
        private XRLabel xrLabel17;
        private XRLabel xrLabel15;
        private XRLabel xrLabel14;
        private XRPanel xrPanel4;
        private XRPanel xrPanel2;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell4;
        private XRLabel xrLabel2;
        private XRLabel xrLabel4;
        private XRRichText xrRichText1;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private XRLabel xrLabel12;
        private XRLabel xrLabel13;
        private XRLabel xrLabel18;
        private XRLabel xrLabel19;
        private XRLabel xrLabel20;
        private XRLabel xrLabel1;
        private XRLabel xrLabel16;
        private XRLabel xrLabel11;
        private XRLabel xrLabel23;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRLabel xrLabel10;
        private XRLabel xrLabel3;
        private XRLabel xrLabel21;
        private XRLabel xrLabel22;
        private XRLabel xrLabel24;
        private XRLabel xrLabel27;
        private XRLabel xrLabel28;
        private XRLabel xrLabel29;
        private XRLabel xrLabel30;
        private XRLabel xrLabel5;
        private XRPanel xrPanel8;
        private XRLabel xrLabel32;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell8;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRLabel xrLabelIdCarteira;
        private XRLabel xrLabel31;
        private XRPanel xrPanel9;
        private XRLabel xrLabel26;
        private XRLabel xrLabel33;
        private XRLabel xrLabel34;
        private XRSubreport xrSubreport2;
        private XRLabel xrLabel35;
        private XRLabel xrLabel36;
        private XRLabel xrLabel39;
        private XRLabel xrLabel38;
        private XRLabel xrLabel37;
        private XRLabel xrLabel41;
        private XRLabel xrLabel40;
        private XRLabel xrLabel42;
        private XRLabel xrLabel44;
        private XRLabel xrLabel43;
        private XRLabel xrLabel45;
        private XRLabel xrLabel47;
        private XRLabel xrLabel46;
        private XRLabel xrLabel49;
        private XRLabel xrLabel48;
        private SubReportLogotipo subReportLogotipo1;
        private DevExpress.XtraReports.Parameters.Parameter parameter01_Distribuidora;
        private DevExpress.XtraReports.Parameters.Parameter parameter02_EnderecoCompleto;
        private DevExpress.XtraReports.Parameters.Parameter parameter03_Cep;
        private DevExpress.XtraReports.Parameters.Parameter parameter04_Cidade;
        private DevExpress.XtraReports.Parameters.Parameter parameter05_Telefone;
        private DevExpress.XtraReports.Parameters.Parameter parameter06_Fax;
        private DevExpress.XtraReports.Parameters.Parameter parameter07_Internet;
        private DevExpress.XtraReports.Parameters.Parameter parameter08_Email;
        private DevExpress.XtraReports.Parameters.Parameter parameter09_CNPJ;
        private DevExpress.XtraReports.Parameters.Parameter parameter10_TelefoneOuvidoria;
        private DevExpress.XtraReports.Parameters.Parameter parameter11_EmailOuvidoria;

        /* Guarda o idCarteira para poder construir a imagem do Logotipo*/
        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportNotaResgate(int idOperacao, TipoLingua tipoLingua) {
            this.idCarteira = 0;
            
            this.idOperacao = idOperacao;
            this.tipoLingua = tipoLingua;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                
                //this.ReportHeader.Visible = false;
                //this.subreport1.Visible = true;
            }
        }

        private void PersonalInitialize() {

            // Força o Carregamento do Resource de acordo com a Lingua
            string linguaSelecionada = StringEnum.GetStringValue((TipoLingua)this.tipoLingua);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);

            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt!= null ? dt.Rows.Count : 0;

            #region Pega Campos do resource
            this.xrLabel14.Text = Resources.ReportNotaResgate._NumeroCota;
            this.xrTableCell2.Text = Resources.ReportNotaResgate._ViaCliente;
            this.xrLabel15.Text = Resources.ReportNotaResgate._ValorBruto;
            this.xrLabel3.Text = Resources.ReportNotaResgate._ValorIR;
            this.xrLabel22.Text = Resources.ReportNotaResgate._ValorIOF;
            this.xrLabel27.Text = Resources.ReportNotaResgate._ValorPfee;
            this.xrLabel29.Text = Resources.ReportNotaResgate._ValorLiquido;
            this.xrLabel8.Text = Resources.ReportNotaResgate._ValorLiquidoExtenso;
            this.xrTableCell3.Text = Resources.ReportNotaResgate._NotaResgate;
            this.xrLabel7.Text = Resources.ReportNotaResgate._NumeroOperacao;
            this.xrLabel17.Text = Resources.ReportNotaResgate._ValorCota;
            //this.xrLabel26.Text = Resources.ReportNotaResgate._Cliente;
            this.xrLabel25.Text = Resources.ReportNotaResgate._TipoDoc;
            this.xrTableCell6.Text = Resources.ReportNotaResgate._Cep;
            this.xrLabel5.Text = Resources.ReportNotaResgate._AssinaturasAutorizadas;
            this.xrLabel23.Text = Resources.ReportNotaResgate._Data;
            //
            #endregion

            this.PreencheCabecalho();
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();
            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" SELECT O.IdOperacao, \n");
            sqlText.AppendLine("        O.DataOperacao, \n");
            sqlText.AppendLine("        O.ValorBruto, \n");
            sqlText.AppendLine("        O.ValorLiquido, \n");
            sqlText.AppendLine("        O.ValorIR, \n");
            sqlText.AppendLine("        O.ValorIOF, \n");
            sqlText.AppendLine("        O.ValorPerformance, \n");
            sqlText.AppendLine("        O.Quantidade, \n");
            sqlText.AppendLine("        O.CotaOperacao, \n");
            sqlText.AppendLine("        O.DadosBancarios, \n");
            sqlText.AppendLine("        C.IdCarteira as IdCarteira, \n");
            sqlText.AppendLine("        C.Nome as NomeCarteira, \n");
            sqlText.AppendLine("        A.Nome as NomeCotista, \n");
            sqlText.AppendLine("        A.IdCotista as IdCotista, \n");
            sqlText.AppendLine("        F.Descricao, \n");
            sqlText.AppendLine("        E.Endereco, \n");
            sqlText.AppendLine("        E.Numero, \n");
            sqlText.AppendLine("        E.Complemento, \n");
            sqlText.AppendLine("        E.Bairro, \n");
            sqlText.AppendLine("        E.Cidade, \n");
            sqlText.AppendLine("        E.CEP, \n");
            sqlText.AppendLine("        P.Tipo, P.CpfCnpj, \n");
            sqlText.AppendLine("        (E.Endereco + ', ' + E.Numero + ' ' + E.Complemento + ' - ' + E.Cidade + ' - ' + E.UF) AS EnderecoCompleto \n");
            sqlText.AppendLine(" FROM   OperacaoCotista O, \n");
            sqlText.AppendLine("        Carteira C, \n");
            sqlText.AppendLine("        Cotista A, \n");
            sqlText.AppendLine("        FormaLiquidacao F, \n");

            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                sqlText.AppendLine(" Pessoa P ");
                sqlText.AppendLine(" LEFT JOIN PessoaEndereco E ON P.IdPessoa = E.IdPessoa AND E.RecebeCorrespondencia = 'S' ");
            }
            else
            {
                sqlText.AppendLine("    Pessoa P, \n");
                sqlText.AppendLine("    PessoaEndereco E \n");
            }

            sqlText.AppendLine(" WHERE  O.IdCarteira = C.IdCarteira \n");
            sqlText.AppendLine("        AND O.IdCotista = A.IdCotista \n");
            sqlText.AppendLine("        AND A.IdPessoa = P.IdPessoa \n");
            sqlText.AppendLine("        AND O.IdFormaLiquidacao = F.IdFormaLiquidacao \n");

            if (esConfigSettings.DefaultConnection.Provider != "EntitySpaces.SqlClientProvider")
            {
                sqlText.AppendLine("        AND P.IdPessoa = E.IdPessoa (+) \n");
                sqlText.AppendLine("        AND E.RecebeCorrespondencia = 'S' \n");
            }

            sqlText.AppendLine("        AND O.IdOperacao = " + this.idOperacao);
            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString());

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Cotista/Xml/ReportNotaResgate.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportNotaResgate.resx"; 
         */
        private void InitializeComponent() {
            string resourceFileName = "ReportNotaResgate.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportNotaResgate.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPanel9 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter11_EmailOuvidoria = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter10_TelefoneOuvidoria = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter09_CNPJ = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter08_Email = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter07_Internet = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter06_Fax = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter05_Telefone = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter04_Cidade = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter03_Cep = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter02_EnderecoCompleto = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter01_Distribuidora = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrPanel10 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel8 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel7 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelIdCarteira = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrPanel9,
            this.xrPanel10,
            this.xrPanel8,
            this.xrPanel3,
            this.xrPanel2,
            this.xrPanel4,
            this.xrPanel5,
            this.xrPanel6,
            this.xrPanel7,
            this.xrPanel1,
            this.xrTable3,
            this.xrLabelIdCarteira,
            this.xrTable1,
            this.xrTable2});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 1716.854F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 95.21269F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(736.6874F, 95.00002F);
            // 
            // xrPanel9
            // 
            this.xrPanel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel9.BorderWidth = 1;
            this.xrPanel9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel49,
            this.xrLabel48,
            this.xrLabel47,
            this.xrLabel46,
            this.xrLabel45,
            this.xrLabel44,
            this.xrLabel43,
            this.xrLabel41,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel34,
            this.xrLabel33,
            this.xrLabel26,
            this.xrLabel35,
            this.xrLabel42});
            this.xrPanel9.Dpi = 254F;
            this.xrPanel9.LocationFloat = new DevExpress.Utils.PointFloat(84.99998F, 10.23254F);
            this.xrPanel9.Name = "xrPanel9";
            this.xrPanel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel9.SizeF = new System.Drawing.SizeF(1948F, 292.77F);
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderWidth = 0;
            this.xrLabel49.CanGrow = false;
            this.xrLabel49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter11_EmailOuvidoria, "Text", "")});
            this.xrLabel49.Dpi = 254F;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(1477F, 232.7655F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(457F, 43.00003F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UsePadding = false;
            this.xrLabel49.Text = "#EmailOuvidoria";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter11_EmailOuvidoria
            // 
            this.parameter11_EmailOuvidoria.Name = "parameter11_EmailOuvidoria";
            // 
            // xrLabel48
            // 
            this.xrLabel48.BorderWidth = 0;
            this.xrLabel48.Dpi = 254F;
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(1252F, 232.7679F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(225F, 43F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.Text = "#EmailOuvidoria";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.BorderWidth = 0;
            this.xrLabel47.CanGrow = false;
            this.xrLabel47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter10_TelefoneOuvidoria, "Text", "")});
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(963F, 232.7679F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(288F, 43.00002F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UsePadding = false;
            this.xrLabel47.Text = "#OuvidoriaTel";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter10_TelefoneOuvidoria
            // 
            this.parameter10_TelefoneOuvidoria.Name = "parameter10_TelefoneOuvidoria";
            // 
            // xrLabel46
            // 
            this.xrLabel46.BorderWidth = 0;
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(765.0001F, 232.7655F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(198F, 43F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UsePadding = false;
            this.xrLabel46.Text = "#OuvidoriaTel";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.BorderWidth = 0;
            this.xrLabel45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter09_CNPJ, "Text", "")});
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(863.0001F, 188.77F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(419.3957F, 43F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UsePadding = false;
            this.xrLabel45.Text = "#CNPJ";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter09_CNPJ
            // 
            this.parameter09_CNPJ.Name = "parameter09_CNPJ";
            // 
            // xrLabel44
            // 
            this.xrLabel44.BorderWidth = 0;
            this.xrLabel44.CanGrow = false;
            this.xrLabel44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter08_Email, "Text", "")});
            this.xrLabel44.Dpi = 254F;
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(1477F, 143.7677F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(462.0005F, 43F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UsePadding = false;
            this.xrLabel44.Text = "#email";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter08_Email
            // 
            this.parameter08_Email.Name = "parameter08_Email";
            // 
            // xrLabel43
            // 
            this.xrLabel43.BorderWidth = 0;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(1373F, 143.7675F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(104F, 43F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.Text = "#Email";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.BorderWidth = 0;
            this.xrLabel42.CanGrow = false;
            this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter07_Internet, "Text", "")});
            this.xrLabel42.Dpi = 254F;
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(892.0001F, 143.7675F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(480.9999F, 43F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UsePadding = false;
            this.xrLabel42.Text = "#http";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter07_Internet
            // 
            this.parameter07_Internet.Name = "parameter07_Internet";
            // 
            // xrLabel41
            // 
            this.xrLabel41.BorderWidth = 0;
            this.xrLabel41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter06_Fax, "Text", "")});
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(1477F, 100.7677F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(462.0005F, 42.99998F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UsePadding = false;
            this.xrLabel41.Text = "#Fax";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter06_Fax
            // 
            this.parameter06_Fax.Name = "parameter06_Fax";
            // 
            // xrLabel40
            // 
            this.xrLabel40.BorderWidth = 0;
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(1404F, 100.7675F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(73F, 42.99998F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.Text = "#Fax";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.BorderWidth = 0;
            this.xrLabel39.CanGrow = false;
            this.xrLabel39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter05_Telefone, "Text", "")});
            this.xrLabel39.Dpi = 254F;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(834.0001F, 100.7675F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(398.2289F, 43F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UsePadding = false;
            this.xrLabel39.Text = "#Telefone";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter05_Telefone
            // 
            this.parameter05_Telefone.Name = "parameter05_Telefone";
            // 
            // xrLabel38
            // 
            this.xrLabel38.BorderWidth = 0;
            this.xrLabel38.CanGrow = false;
            this.xrLabel38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter04_Cidade, "Text", "")});
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(1584.521F, 57.76751F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(354.4795F, 43F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UsePadding = false;
            this.xrLabel38.Text = "#CidadeEstado";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter04_Cidade
            // 
            this.parameter04_Cidade.Name = "parameter04_Cidade";
            // 
            // xrLabel37
            // 
            this.xrLabel37.BorderWidth = 0;
            this.xrLabel37.CanGrow = false;
            this.xrLabel37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter03_Cep, "Text", "")});
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(1439F, 56.77003F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(145.5209F, 43F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UsePadding = false;
            this.xrLabel37.Text = "xrLabel37";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter03_Cep
            // 
            this.parameter03_Cep.Name = "parameter03_Cep";
            // 
            // xrLabel36
            // 
            this.xrLabel36.BorderWidth = 0;
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(765.0001F, 188.77F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(98F, 43F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UsePadding = false;
            this.xrLabel36.Text = "#CNPJ";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.BorderWidth = 0;
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(765.0001F, 143.77F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(126F, 43F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UsePadding = false;
            this.xrLabel35.Text = "#Internet";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.BorderWidth = 0;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(765F, 100.7675F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(69F, 42.99998F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UsePadding = false;
            this.xrLabel34.Text = "#Tel";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.BorderWidth = 0;
            this.xrLabel33.CanGrow = false;
            this.xrLabel33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter02_EnderecoCompleto, "Text", "")});
            this.xrLabel33.Dpi = 254F;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(765F, 57.76751F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(674F, 43F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UsePadding = false;
            this.xrLabel33.Text = "#EnderecoCompleto";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter02_EnderecoCompleto
            // 
            this.parameter02_EnderecoCompleto.Name = "parameter02_EnderecoCompleto";
            // 
            // xrLabel26
            // 
            this.xrLabel26.BorderWidth = 0;
            this.xrLabel26.CanGrow = false;
            this.xrLabel26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter01_Distribuidora, "Text", "")});
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(765.0001F, 13.76999F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(1174F, 43.00002F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UsePadding = false;
            this.xrLabel26.Text = "#Distribuidora";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter01_Distribuidora
            // 
            this.parameter01_Distribuidora.Name = "parameter01_Distribuidora";
            // 
            // xrPanel10
            // 
            this.xrPanel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel10.BorderWidth = 1;
            this.xrPanel10.CanGrow = false;
            this.xrPanel10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5});
            this.xrPanel10.Dpi = 254F;
            this.xrPanel10.LocationFloat = new DevExpress.Utils.PointFloat(868.0001F, 1526.854F);
            this.xrPanel10.Name = "xrPanel10";
            this.xrPanel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel10.SizeF = new System.Drawing.SizeF(1164F, 106F);
            this.xrPanel10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AssinaturaAutorizadaPanelBeforePrint);
            // 
            // xrLabel5
            // 
            this.xrLabel5.BorderWidth = 0;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(153F, 19F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(466F, 64F);
            this.xrLabel5.Text = "#AssinaturasAutorizadas";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPanel8
            // 
            this.xrPanel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel8.BorderWidth = 1;
            this.xrPanel8.CanGrow = false;
            this.xrPanel8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel32});
            this.xrPanel8.Dpi = 254F;
            this.xrPanel8.LocationFloat = new DevExpress.Utils.PointFloat(85.00004F, 1103.854F);
            this.xrPanel8.Name = "xrPanel8";
            this.xrPanel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel8.SizeF = new System.Drawing.SizeF(1947F, 145F);
            this.xrPanel8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DadosBancariosBeforePrint);
            // 
            // xrLabel32
            // 
            this.xrLabel32.BorderWidth = 0;
            this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DadosBancarios")});
            this.xrLabel32.Dpi = 254F;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel32.Multiline = true;
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(1884F, 101F);
            this.xrLabel32.Text = "xrLabel4";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel3
            // 
            this.xrPanel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel3.BorderWidth = 1;
            this.xrPanel3.CanGrow = false;
            this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText1});
            this.xrPanel3.Dpi = 254F;
            this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(85.00004F, 1267.854F);
            this.xrPanel3.Name = "xrPanel3";
            this.xrPanel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel3.SizeF = new System.Drawing.SizeF(1947F, 245F);
            this.xrPanel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PanelImportanteBeforePrint);
            // 
            // xrRichText1
            // 
            this.xrRichText1.BorderWidth = 0;
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 19F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1884F, 212F);
            this.xrRichText1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel2
            // 
            this.xrPanel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel2.BorderWidth = 1;
            this.xrPanel2.CanGrow = false;
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(1185F, 338.8539F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel2.SizeF = new System.Drawing.SizeF(848F, 101F);
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderWidth = 0;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 13F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(424F, 64F);
            this.xrLabel7.Text = "#NumeroOperacao";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NumeroOperacaoBeforePrint);
            // 
            // xrPanel4
            // 
            this.xrPanel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel4.BorderWidth = 1;
            this.xrPanel4.CanGrow = false;
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel8});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(85F, 452.854F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel4.SizeF = new System.Drawing.SizeF(1079F, 402F);
            // 
            // xrLabel9
            // 
            this.xrLabel9.BorderWidth = 0;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(90F, 106F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(974F, 172F);
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorLiquidoBeforePrint);
            // 
            // xrLabel8
            // 
            this.xrLabel8.BorderWidth = 0;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(45F, 24F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(550F, 43F);
            this.xrLabel8.Text = "#ValorLiquidoExtenso";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel5
            // 
            this.xrPanel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel5.BorderWidth = 1;
            this.xrPanel5.CanGrow = false;
            this.xrPanel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel20,
            this.xrLabel14,
            this.xrLabel30,
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel24,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel3,
            this.xrLabel19,
            this.xrLabel15});
            this.xrPanel5.Dpi = 254F;
            this.xrPanel5.LocationFloat = new DevExpress.Utils.PointFloat(1185F, 452.854F);
            this.xrPanel5.Name = "xrPanel5";
            this.xrPanel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel5.SizeF = new System.Drawing.SizeF(847F, 402F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.BorderWidth = 0;
            this.xrLabel16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CotaOperacao", "{0:n8}")});
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(339F, 355F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(465F, 42F);
            this.xrLabel16.Text = "xrLabel16";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BorderWidth = 0;
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(42F, 355F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel17.Text = "#ValorCota";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BorderWidth = 0;
            this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:n8}")});
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(339F, 296F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(466F, 42F);
            this.xrLabel20.Text = "xrLabel20";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderWidth = 0;
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(42F, 296F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel14.Text = "#NumeroCotas";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.BorderWidth = 0;
            this.xrLabel30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido", "{0:n2}")});
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(339F, 238F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(465F, 42F);
            this.xrLabel30.Text = "xrLabel16";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderWidth = 0;
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(42F, 238F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel29.Text = "#ValorLiquido";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BorderWidth = 0;
            this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorPerformance", "{0:n2}")});
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(339F, 177F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(465F, 42F);
            this.xrLabel28.Text = "xrLabel16";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.BorderWidth = 0;
            this.xrLabel27.Dpi = 254F;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(42F, 177F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel27.Text = "#ValorPfee";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BorderWidth = 0;
            this.xrLabel24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF", "{0:n2}")});
            this.xrLabel24.Dpi = 254F;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(339F, 122F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(465F, 42F);
            this.xrLabel24.Text = "xrLabel16";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.BorderWidth = 0;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(42F, 122F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel22.Text = "#ValorIOF";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.BorderWidth = 0;
            this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR", "{0:n2}")});
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(339F, 64F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(465F, 42F);
            this.xrLabel21.Text = "xrLabel16";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(42F, 64F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 42F);
            this.xrLabel3.Text = "#ValorIR";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderWidth = 0;
            this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto", "{0:n2}")});
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(339F, 8F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(466F, 42F);
            this.xrLabel19.Text = "xrLabel19";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderWidth = 0;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(42F, 5F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(254F, 43F);
            this.xrLabel15.Text = "#ValorBruto";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel6
            // 
            this.xrPanel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel6.BorderWidth = 1;
            this.xrPanel6.CanGrow = false;
            this.xrPanel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23,
            this.xrLabel4});
            this.xrPanel6.Dpi = 254F;
            this.xrPanel6.LocationFloat = new DevExpress.Utils.PointFloat(85F, 1526.854F);
            this.xrPanel6.Name = "xrPanel6";
            this.xrPanel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel6.SizeF = new System.Drawing.SizeF(770F, 106F);
            this.xrPanel6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPanelBeforePrint);
            // 
            // xrLabel23
            // 
            this.xrLabel23.BorderWidth = 0;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(45F, 34F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(106F, 64F);
            this.xrLabel23.Text = "#Data";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BorderWidth = 0;
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DataOperacao", "{0:d}")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(159F, 34F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(381F, 43F);
            this.xrLabel4.Text = "xrLabel4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel7
            // 
            this.xrPanel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel7.BorderWidth = 1;
            this.xrPanel7.CanGrow = false;
            this.xrPanel7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrLabel25,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable4,
            this.xrLabel10});
            this.xrPanel7.Dpi = 254F;
            this.xrPanel7.LocationFloat = new DevExpress.Utils.PointFloat(85.00004F, 872.8539F);
            this.xrPanel7.Name = "xrPanel7";
            this.xrPanel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel7.SizeF = new System.Drawing.SizeF(1948F, 212.0001F);
            // 
            // xrTable5
            // 
            this.xrTable5.BorderWidth = 0;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(1524F, 87F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(415F, 42F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell8});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "#CPFCNPJ";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 0.21445783132530122;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.LabelCPFBeforePrint);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "CPFCNPJ";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.78554216867469884;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CPFCNPFBeforePrint);
            // 
            // xrLabel25
            // 
            this.xrLabel25.BorderWidth = 0;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(1185F, 21F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(297F, 64F);
            this.xrLabel25.Text = "#TipoDoc";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BorderWidth = 0;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.EnderecoCompleto")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(45F, 90F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(1312F, 42F);
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BorderWidth = 0;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Descricao")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(1524F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(402F, 64F);
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BorderWidth = 0;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(45F, 151F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(593F, 42F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#Cep";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.21416526138279932;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CEP")});
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.78583473861720066;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BorderWidth = 0;
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.NomeCotista")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(44.99998F, 19F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(1122F, 63.99994F);
            this.xrLabel10.Text = "xrLabel10";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotistaBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.BorderWidth = 1;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel31,
            this.xrLabel6});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(85F, 338.8539F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1080F, 101F);
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderWidth = 0;
            this.xrLabel31.CanGrow = false;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(42F, 55F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(1010F, 40F);
            this.xrLabel31.Text = "CNPJ";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel31.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CNPJ1PrintOnPage);
            // 
            // xrLabel6
            // 
            this.xrLabel6.BorderWidth = 0;
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.NomeCarteira")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(44.99998F, 10F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(1010F, 40F);
            this.xrLabel6.Text = "xrLabel6";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel6.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CNPJPrintOnPage);
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1778F, 1648.854F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(254F, 43F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "#ViaCliente";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 1;
            // 
            // xrLabelIdCarteira
            // 
            this.xrLabelIdCarteira.BorderWidth = 0;
            this.xrLabelIdCarteira.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.IdCarteira")});
            this.xrLabelIdCarteira.Dpi = 254F;
            this.xrLabelIdCarteira.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabelIdCarteira.LocationFloat = new DevExpress.Utils.PointFloat(85F, 1646.375F);
            this.xrLabelIdCarteira.Name = "xrLabelIdCarteira";
            this.xrLabelIdCarteira.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelIdCarteira.SizeF = new System.Drawing.SizeF(297F, 45.47919F);
            this.xrLabelIdCarteira.Text = "idCarteiraVisiblefalse";
            this.xrLabelIdCarteira.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabelIdCarteira.Visible = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(85F, 303.0025F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(313.5861F, 35F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "#NotaResgate";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell3.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(1758F, 303.8539F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(275F, 35F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell4});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "Nº";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 0.25454545454545452;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.IdOperacao")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.74545454545454548;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BorderWidth = 0;
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CEP")});
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(148F, 161F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(275F, 64F);
            this.xrLabel11.Text = "xrLabel11";
            // 
            // xrLabel12
            // 
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CotaOperacao")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(550F, 212F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel12.Text = "xrLabel12";
            // 
            // xrLabel13
            // 
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(529F, 127F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel13.Text = "xrLabel13";
            // 
            // xrLabel18
            // 
            this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto")});
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(593F, 42F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel18.Text = "xrLabel18";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 51F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 51F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportNotaResgate
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(25, 20, 51, 51);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter01_Distribuidora,
            this.parameter02_EnderecoCompleto,
            this.parameter03_Cep,
            this.parameter04_Cidade,
            this.parameter05_Telefone,
            this.parameter06_Fax,
            this.parameter07_Internet,
            this.parameter08_Email,
            this.parameter09_CNPJ,
            this.parameter10_TelefoneOuvidoria,
            this.parameter11_EmailOuvidoria});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        // Colunas da Tabela
        private string valorLiquidoColumn = "ValorLiquido";

        //     
        #region Funções Internas do Relatorio
        //
        private void ValorLiquidoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel valorLiquidoExtensoXRLabel = sender as XRLabel;

            decimal? valorLiquidoExtenso = (decimal?)this.GetCurrentColumnValue(this.valorLiquidoColumn);

            // Transforma para Valor Extenso
            if (valorLiquidoExtenso.HasValue) {                
                valorLiquidoExtensoXRLabel.Text = this.tipoLingua == TipoLingua.Ingles
                    ? Utilitario.ValorExtenso(valorLiquidoExtenso.Value, PatternLingua.Ingles)
                    : Utilitario.ValorExtenso(valorLiquidoExtenso.Value, PatternLingua.Portugues);
            }
        }

        private void NumeroOperacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel numeroOperacaoXRLabel = sender as XRLabel;
            numeroOperacaoXRLabel.Text += " " + this.idOperacao;
        }
        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportNotaResgate.ResourceManager;

        }

        private void CPFCNPFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCPFCNPJ = sender as XRTableCell;
            xrTableCellCPFCNPJ.Text = "";
            //
            byte tipo = (byte)this.GetCurrentColumnValue(PessoaMetadata.ColumnNames.Tipo);
            string cpfCnpj = "";

            if (!Convert.IsDBNull(this.GetCurrentColumnValue("CpfCnpj"))) {
                cpfCnpj = Convert.ToString(this.GetCurrentColumnValue("CpfCnpj"));
            }

            if (!String.IsNullOrEmpty(cpfCnpj)) {
                xrTableCellCPFCNPJ.Text = tipo == (byte)TipoPessoa.Fisica
                                            ? Utilitario.MascaraCPF(cpfCnpj)
                                            : Utilitario.MascaraCNPJ(cpfCnpj);
            }
        }
              
        private void LabelCPFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCPFCNPJ = sender as XRTableCell;
            xrTableCellCPFCNPJ.Text = "";

            byte tipo = (byte)this.GetCurrentColumnValue(PessoaMetadata.ColumnNames.Tipo);

            xrTableCellCPFCNPJ.Text = tipo == (byte)TipoPessoa.Fisica
                                            ? Resources.ReportNotaResgate._CPF
                                            : Resources.ReportNotaResgate._CNPJ;
        }

        private void PanelImportanteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.tipoLingua == TipoLingua.Ingles) {
                XRPanel importanteXRPanel = sender as XRPanel;
                importanteXRPanel.Visible = false;
                //importanteXRPanel.Location = new System.Drawing.Point(0, 0);
            }
        }

         private void DadosBancariosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.tipoLingua == TipoLingua.Ingles) {
                XRPanel dadosBancariosXRPanel = sender as XRPanel;
                dadosBancariosXRPanel.Visible = false;
            }
        }

        private void DataPanelBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.tipoLingua == TipoLingua.Ingles) {
                XRPanel dataXRPanel = sender as XRPanel;
                dataXRPanel.Location = new System.Drawing.Point(85, 810);

            }
        }

        private void AssinaturaAutorizadaPanelBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.tipoLingua == TipoLingua.Ingles) {
                XRPanel assinaturaAutorizadaXRPanel = sender as XRPanel;
                assinaturaAutorizadaXRPanel.Location = new System.Drawing.Point(868, 810);
            }
        }

        private void CNPJPrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRLabel CNPJ = sender as XRLabel;

            if (!String.IsNullOrEmpty(CNPJ.Text)) {

                if (!String.IsNullOrEmpty(xrLabelIdCarteira.Text)) {

                    // Carrega o Carteira
                    Pessoa pessoa = new Pessoa();
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(Convert.ToInt32(xrLabelIdCarteira.Text));

                    pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

                    // id + nome
                    string carteira = xrLabelIdCarteira.Text + " - " + CNPJ.Text.Trim();

                    if (carteira.Length < 60) {
                        CNPJ.Text = carteira;
                    }
                    else {

                        CNPJ.Text = carteira.Substring(0, 60);
                    }
                }
            }
        }

        private void CNPJ1PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRLabel CNPJ1 = sender as XRLabel;
            CNPJ1.Text = "";

            if (!String.IsNullOrEmpty(xrLabelIdCarteira.Text)) {

                // Carrega o Carteira
                Pessoa pessoa = new Pessoa();
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(Convert.ToInt32(xrLabelIdCarteira.Text));

                pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);
                //

                if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                    CNPJ1.Text = Resources.ReportNotaResgate._CNPJ + pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
                }
            }
        }

        private void CotistaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel xrLabelCotista = sender as XRLabel;
            xrLabelCotista.Text = "";

            int idCotista = Convert.ToInt32(this.GetCurrentColumnValue(CotistaMetadata.ColumnNames.IdCotista));
            string nomeCotista = Convert.ToString(this.GetCurrentColumnValue("NomeCotista"));

            xrLabelCotista.Text = idCotista.ToString() + " - " + nomeCotista;
        }

        /// <summary>
        /// Preenche Informações do Cabeçalho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreencheCabecalho() {

            DataTable d = (DataTable)this.DataSource;

            int? idCarteira = null;
            if (d != null) {
                idCarteira = Convert.ToInt32( d.Rows[0]["IdCarteira"] );

                this.idCarteira = idCarteira.Value; // armazena Internamente
            }
            
            int? idAgente = ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao;
            if (!idAgente.HasValue) {

                if (idCarteira.HasValue) {

                    // Carrega o Carteira
                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(Convert.ToInt32(idCarteira.Value));

                    idAgente = carteira.IdAgenteAdministrador.Value;
                }
            }
           
            #region Labels
            this.xrLabel34.Text = Resources.ReportNotaResgate._Telefone;
            this.xrLabel40.Text = Resources.ReportNotaResgate._Fax;
            this.xrLabel35.Text = Resources.ReportNotaResgate._Internet;
            this.xrLabel43.Text = Resources.ReportNotaResgate._Email;
            this.xrLabel36.Text = Resources.ReportNotaResgate._CNPJ;
            this.xrLabel46.Text = Resources.ReportNotaResgate._OuvidoriaTelefone;
            this.xrLabel48.Text = Resources.ReportNotaResgate._EmailOuvidoria;
            #endregion
            //
            //this.parameter01_Distribuidora.Value = "Distribuidora";
            //this.parameter02_EnderecoCompleto.Value = "R. Funchal";
            //this.parameter03_Cep.Value = "04551-060";
            //this.parameter04_Cidade.Value = "São Paulo - SP";
            //this.parameter05_Telefone.Value = "(11)3198-5151";
            //this.parameter06_Fax.Value = "(11)3198-5151";
            //this.parameter07_Internet.Value = "www.intrader.com.br";
            //this.parameter08_Email.Value = "intrader@intrader@intrader.com.br";
            //this.parameter09_CNPJ.Value = "15.489.568/0001-95";
            //this.parameter10_TelefoneOuvidoria.Value = "800-8788888";
            //this.parameter11_EmailOuvidoria.Value = "ouvidoria@intrader.com.br";

            AgenteMercado a = new AgenteMercado();
            if (idAgente.HasValue) {
                a.LoadByPrimaryKey(idAgente.Value);

                #region Parameters
                string ddd = "";
                if (!String.IsNullOrEmpty(a.str.Ddd)) {
                    ddd = "(" + a.str.Ddd + ") ";
                }

                #region CidadeEstado
                string cidadeEstado = "";
                if (!String.IsNullOrEmpty(a.str.Cidade)) {
                    cidadeEstado += a.str.Cidade;
                }
                if (!String.IsNullOrEmpty(a.str.Uf)) {
                    cidadeEstado += " - " + a.str.Uf;
                }
                #endregion

                #region Endereco
                string endereco = "";
                if (!String.IsNullOrEmpty(a.str.Endereco)) {
                    endereco += a.str.Endereco;
                }
                if (!String.IsNullOrEmpty(a.str.Bairro)) {
                    if (!String.IsNullOrEmpty(a.str.Endereco)) {
                        endereco += ", ";
                    }
                    endereco += a.str.Bairro;
                }
                #endregion

                this.parameter01_Distribuidora.Value = a.Nome.Trim();
                this.parameter02_EnderecoCompleto.Value = endereco;
                this.parameter03_Cep.Value = a.str.Cep;
                this.parameter04_Cidade.Value = cidadeEstado;
                this.parameter05_Telefone.Value = !String.IsNullOrEmpty(a.str.Telefone) ? ddd + a.str.Telefone : "";
                this.parameter06_Fax.Value = !String.IsNullOrEmpty(a.str.Fax) ? ddd + a.str.Fax : "";
                this.parameter07_Internet.Value = a.str.Website;
                this.parameter08_Email.Value = a.str.Email;
                this.parameter09_CNPJ.Value = a.str.Cnpj == String.Empty ? "" : Utilitario.MascaraCNPJ(a.str.Cnpj);
                this.parameter10_TelefoneOuvidoria.Value = !String.IsNullOrEmpty(a.str.TelOuvidoria) ? ddd + a.str.TelOuvidoria : "";
                this.parameter11_EmailOuvidoria.Value = a.str.EmailOuvidoria;
                #endregion
            }
        }
    }
}