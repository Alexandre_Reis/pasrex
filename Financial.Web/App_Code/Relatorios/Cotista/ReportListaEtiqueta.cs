﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using EntitySpaces.Core;
using System.Text;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportListaEtiqueta
    /// </summary>
    public class ReportListaEtiqueta : DevExpress.XtraReports.UI.XtraReport {
        //
        private int? idCarteira;

        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int numeroLinhasDataTable;

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private ReportSemDados reportSemDados1;
        private XRPanel xrPanel1;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRLabel xrLabel12;
        private XRLabel xrLabel13;
        private XRLabel xrLabel14;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private XRLabel xrLabel10;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ReportListaEtiquetaInternal() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }
        #endregion

        #region Construtores Publics
        public ReportListaEtiqueta() {
            this.ReportListaEtiquetaInternal();
        }

        public ReportListaEtiqueta(int idCarteira) {
            this.idCarteira = idCarteira;
            this.ReportListaEtiquetaInternal();
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                this.PageHeader.Visible = true;
                this.xrSubreport1.Visible = true;
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;            
            this.numeroLinhasDataTable = dt.Rows.Count;            
        }
        
        private DataTable FillDados() {
            esUtility u = new esUtility();
            //
            #region SQL
            StringBuilder sqlText = new StringBuilder();                       
            sqlText.Append("SELECT C.IdCotista, \n");
            sqlText.Append("       C.Nome, \n");
            sqlText.Append("       E.Endereco, \n");
            sqlText.Append("       E.Numero, \n");
            sqlText.Append("       E.Complemento, \n");
            sqlText.Append("       E.Bairro, \n");
            sqlText.Append("       E.Cidade, \n");
            sqlText.Append("       E.CEP, \n");
            sqlText.Append("       E.UF \n");
            sqlText.Append("FROM   Cotista C, \n");
            sqlText.Append("       PessoaEndereco E \n");
            sqlText.Append("WHERE  C.IdPessoa = E.IdPessoa \n");
            sqlText.Append("       AND E.RecebeCorrespondencia = 'S' \n");

            // Filtra os Cotistas que possuem Posição em Determinada Carteira
            if (this.IdCarteira.HasValue) {
                sqlText.Append("   AND C.IdCotista In ( SELECT Distinct IdCotista \n");
                sqlText.Append("                        FROM   PosicaoCotista \n");
                sqlText.Append("                        WHERE  IdCarteira = " + this.idCarteira.Value + "\n");
                sqlText.Append("                        AND    Quantidade <> 0 )");
            }
                      
            #endregion
            //
            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString());
            //
            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Cotista/Xml/ReportListaEtiqueta.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportListaEtiqueta.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportListaEtiqueta.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 338F;
            this.Detail.MultiColumn.ColumnSpacing = 51F;
            this.Detail.MultiColumn.ColumnWidth = 1016F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel14,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1016F, 338F);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Cidade")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(64F, 169F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel12.Text = "xrLabel12";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.UF")});
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(64F, 212F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel13.Text = "xrLabel13";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(64F, 254F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel14.Text = "Cep";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CepBeforePrint);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(64F, 42F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel8.Text = "Nome";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeBeforePrint);
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(64F, 85F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel9.Text = "Endereco";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.EnderecoBeforePrint);
            // 
            // xrLabel10
            // 
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Bairro")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(64F, 127F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(900F, 39F);
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdPessoa")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Endereco")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(42F, 85F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Numero")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(339F, 85F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Complemento")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(614F, 85F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Bairro")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(42F, 169F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cidade")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(42F, 254F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Uf")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel7.Text = "xrLabel7";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 87F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 15F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(492F, 64F);
            this.xrSubreport1.Visible = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 211F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportListaEtiqueta
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(41, 0, 211, 0);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
               
        #region Funções Personalizadas
        private void NomeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel nomeXRLabel = sender as XRLabel;
            nomeXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string nome = (string)this.GetCurrentColumnValue(CotistaMetadata.ColumnNames.Nome);
                nome = nome.ToUpper();
                nomeXRLabel.Text = nome;
            }
        }

        private void EnderecoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel enderecoXRLabel = sender as XRLabel;
            enderecoXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string enderecoCompleto = "";
                string endereco = (string)this.GetCurrentColumnValue(PessoaEnderecoMetadata.ColumnNames.Endereco);
                string numero = (string)this.GetCurrentColumnValue(PessoaEnderecoMetadata.ColumnNames.Numero);
                
                string complemento = null;
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(PessoaEnderecoMetadata.ColumnNames.Complemento))) {
                    complemento = (string)this.GetCurrentColumnValue(PessoaEnderecoMetadata.ColumnNames.Complemento);
                }
                enderecoCompleto = endereco + ", " + numero;
                if (!String.IsNullOrEmpty(complemento)) {
                    enderecoCompleto = enderecoCompleto + " - " + complemento; 
                }
                enderecoXRLabel.Text = enderecoCompleto.Trim();
            }
        }

        private void CepBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel cepXRLabel = sender as XRLabel;
            cepXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cep = (string)this.GetCurrentColumnValue(PessoaEnderecoMetadata.ColumnNames.Cep);
                cepXRLabel.Text = Utilitario.MascaraCEP(cep);
            }
        }
        #endregion       

        private void BairroBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            /*XRLabel bairroXRLabel = sender as XRLabel;
            bairroXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string bairro = (string)this.GetCurrentColumnValue("Bairro");
                bairroXRLabel.Text = bairro.Trim();
            }*/
        }

        private void CidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            /*XRLabel cidadeXRLabel = sender as XRLabel;
            cidadeXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cidade = (string)this.GetCurrentColumnValue("Cidade");
                cidadeXRLabel.Text = cidade.Trim().ToLower();
            }*/
        }

        private void UfBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            /*XRLabel UFXRLabel = sender as XRLabel;
            UFXRLabel.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string uf = (string)this.GetCurrentColumnValue("UF");
                UFXRLabel.Text = uf.Trim();
            }*/
        }
    }
}