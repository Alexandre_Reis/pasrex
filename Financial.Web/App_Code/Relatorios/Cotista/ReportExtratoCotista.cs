﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Util;
using System.Text;
using Financial.Investidor;
using Financial.Common;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportExtratoCotista
    /// </summary>
    public class ReportExtratoCotista : XtraReport
    {
        private int idCarteira;

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idCotista;

        public int IdCotista
        {
            get { return idCotista; }
            set { idCotista = value; }
        }

        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private bool incluiComposicaoCarteira;

        public bool IncluiComposicaoCarteira
        {
            get { return incluiComposicaoCarteira; }
            set { incluiComposicaoCarteira = value; }
        }

        private int? idAgenteAdministrador;

        public int? IdAgenteAdministrador
        {
            get { return idAgenteAdministrador; }
            set { idAgenteAdministrador = value; }
        }
       
        //private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private DetailReportBand DetailReport;
        private DetailBand Detail1;
        private XRSubreport xrSubreport4;
        private XRPageInfo xrPageInfo1;        
        private XRSubreport xrSubreport3;
        private PageHeaderBand PageHeader;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell11;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell12;
        private XRSubreport xrSubreport5;
        private XRSubreport xrSubreport6;
        private XRTable xrTable3;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell13;
        private XRSubreport xrSubreport2;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell16;
        private XRTableCell xrlblDataEmissao;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTC_DataEmissao;
        private XRTableCell xrTableCell14;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport1;
        private XRRichText xrRichText2;
        private ReportSemDados reportSemDados1;
        private SubReportQuadroRetorno subReportQuadroRetorno1;
        private SubReportLogotipo subReportLogotipo1;
        private SubReportMovimentacaoCotista subReportMovimentacaoCotista1;
        private SubReportComposicaoCarteira subReportComposicaoCarteira1;
        private SubReportRodape subReportRodape1;
        private XRLabel xrLabel8;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Não Inclui SubReport Composição Carteira
        public ReportExtratoCotista(int idCarteira, int idCotista, DateTime dataInicio, DateTime dataFim) {
            this.ReportExtratoCotistaInit(idCarteira, idCotista, dataInicio, dataFim, false);
        }

        public ReportExtratoCotista(int idCarteira, int idCotista, DateTime dataInicio, DateTime dataFim, bool incluiComposicaoCarteira) {
            this.ReportExtratoCotistaInit(idCarteira, idCotista, dataInicio, dataFim, incluiComposicaoCarteira);
        }

        // Chamada entrada ReportExtratoCotista
        private void ReportExtratoCotistaInit(int idCarteira, int idCotista, DateTime dataInicio, DateTime dataFim, bool incluiComposicaoCarteira) {

            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            this.IncluiComposicaoCarteira = incluiComposicaoCarteira;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (!this.HasData)
            {
                //this.xrSubreport3.Visible = true;
            }
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// Se algum subreport possue dados então o report possue dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return true;
                //this.subReportExtratoCotistaBolsa1.HasData ||
                //this.subReportExtratoCotistaBMF1.HasData ||
                //this.subReportExtratoCotistaBolsaDepositosRetiradas1.HasData ||
                //this.subReportExtratoCotistaBolsaOpcoesNaoExercidas1.HasData ||
                //this.subReportExtratoCotistaBolsaTransferencias1.HasData ||
                //this.subReportExtratoCotistaBolsaAberturaEmprestimo1.HasData ||
                //this.subReportExtratoCotistaBolsaLiquidacaoEmprestimo1.HasData || 
                //this.subReportExtratoCotistaBolsaLiquidacaoTermo1.HasData || 
                //this.subReportExtratoCotistaBolsaExercicioOpcoes1.HasData;
            }
        }

        private void PersonalInitialize()
        {
            //this.PaperKind = System.Drawing.Printing.PaperKind.A4;

            // Passa os parametros para os SubReports
            #region Parametros SubReports
            Carteira carteira = new Carteira();
            carteira.Query.Select(carteira.Query.DataInicioCota);
            carteira.Query.Select(carteira.Query.IdAgenteAdministrador);
            carteira.Query.Where(carteira.Query.IdCarteira.Equal(this.IdCarteira));
            carteira.Query.Load();

            DateTime dataInicioCarteira = new DateTime();
            //            
            this.subReportMovimentacaoCotista1.PersonalInitialize(this.IdCarteira, this.IdCotista, carteira.DataInicioCota.Value, this.dataInicio, this.dataFim);

            this.subReportQuadroRetorno1.PersonalInitialize(this.IdCarteira, carteira.DataInicioCota.Value, this.dataInicio, this.dataFim);
            
            this.xrSubreport1.Visible = incluiComposicaoCarteira;
            if (this.xrSubreport1.Visible)
            {
                this.subReportComposicaoCarteira1.PersonalInitialize(this.IdCarteira, this.IdCotista, this.dataFim);
            }

            this.IdAgenteAdministrador = carteira.IdAgenteAdministrador;

            #endregion
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportExtratoCotista.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportExtratoCotista.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportComposicaoCarteira1 = new Financial.Relatorio.SubReportComposicaoCarteira();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMovimentacaoCotista1 = new Financial.Relatorio.SubReportMovimentacaoCotista();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrSubreport5 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportQuadroRetorno1 = new Financial.Relatorio.SubReportQuadroRetorno();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblDataEmissao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTC_DataEmissao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport6 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportComposicaoCarteira1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentacaoCotista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportQuadroRetorno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrSubreport4});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 64F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1863F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(85F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportRodape1;
            this.xrSubreport4.SizeF = new System.Drawing.SizeF(360F, 64F);
            this.xrSubreport4.Visible = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupFooter1,
            this.Detail1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DetailReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrSubreport2});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 227.1459F;
            this.Detail1.KeepTogether = true;
            this.Detail1.MultiColumn.ColumnCount = 2;
            this.Detail1.Name = "Detail1";
            this.Detail1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 159.5833F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportComposicaoCarteira1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(534F, 64F);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportMovimentacaoCotista1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(534F, 64F);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrRichText2,
            this.xrSubreport5});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 178.8331F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PrintAtBottom = true;
            // 
            // xrRichText2
            // 
            this.xrRichText2.CanShrink = true;
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 126.1666F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1850F, 30F);
            this.xrRichText2.StylePriority.UsePadding = false;
            this.xrRichText2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ObservacaoBeforePrint);
            // 
            // xrSubreport5
            // 
            this.xrSubreport5.Dpi = 254F;
            this.xrSubreport5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrSubreport5.Name = "xrSubreport5";
            this.xrSubreport5.ReportSource = this.subReportQuadroRetorno1;
            this.xrSubreport5.SizeF = new System.Drawing.SizeF(534F, 64F);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 561.4791F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrSubreport3.Visible = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrSubreport6,
            this.xrTable3,
            this.xrTable2,
            this.xrSubreport3});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 611.4791F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 185F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1850F, 45F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell22,
            this.xrTableCell17,
            this.xrTableCell10,
            this.xrlblDataEmissao,
            this.xrTC_DataEmissao});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.17578125;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "Administrador:";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell16.Weight = 0.15155313446735974;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "#Administrador";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.28904725901991718;
            this.xrTableCell22.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AdministradorBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "CNPJ:";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.1168052833735464;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "CPFCNPJAdministrador";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.41182920739485274;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CPFCNPJ_AdministradorBeforePrint);
            // 
            // xrlblDataEmissao
            // 
            this.xrlblDataEmissao.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblDataEmissao.Dpi = 254F;
            this.xrlblDataEmissao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrlblDataEmissao.Name = "xrlblDataEmissao";
            this.xrlblDataEmissao.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrlblDataEmissao.StylePriority.UseBorders = false;
            this.xrlblDataEmissao.StylePriority.UseFont = false;
            this.xrlblDataEmissao.StylePriority.UsePadding = false;
            this.xrlblDataEmissao.StylePriority.UseTextAlignment = false;
            this.xrlblDataEmissao.Text = "Data Emissão:";
            this.xrlblDataEmissao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrlblDataEmissao.Weight = 0.13617052114972941;
            // 
            // xrTC_DataEmissao
            // 
            this.xrTC_DataEmissao.Dpi = 254F;
            this.xrTC_DataEmissao.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTC_DataEmissao.Name = "xrTC_DataEmissao";
            this.xrTC_DataEmissao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTC_DataEmissao.StylePriority.UseFont = false;
            this.xrTC_DataEmissao.StylePriority.UseTextAlignment = false;
            this.xrTC_DataEmissao.Text = "Data Emissão";
            this.xrTC_DataEmissao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTC_DataEmissao.Weight = 0.23905405405405406;
            this.xrTC_DataEmissao.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataEmissaoBeforePrint);
            // 
            // xrSubreport6
            // 
            this.xrSubreport6.Dpi = 254F;
            this.xrSubreport6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrSubreport6.Name = "xrSubreport6";
            this.xrSubreport6.ReportSource = this.subReportLogotipo1;
            this.xrSubreport6.SizeF = new System.Drawing.SizeF(767F, 148F);
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(889F, 100F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1060F, 64F);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "EXTRATO CONSOLIDADO";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 279.7292F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1850F, 256F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell14,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBackColor = false;
            this.xrTableRow3.Weight = 0.25;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Carteira";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 0.75405405405405412;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "CNPJ:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.0702702702702703;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "#CNPJCarteira";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.14729729729729729;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CNPJCarteiraBeforePrint);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Weight = 0.02837837837837838;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell11});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.25;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Cotista";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.66216216216216217;
            this.xrTableCell9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotistaBeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 35, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "CPFCNPJ";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.33783783783783783;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CPFCNPJBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.25;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Endereco";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 1;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.EnderecoBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.25;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "BairroCidadeCep";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.74702702702702706;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BairroCidadeCepBeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Período: ";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.08;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "DataPeriodo";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.17297297297297298;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PeriodoBeforePrint);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 96.16661F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(238.5416F, 30F);
            this.xrLabel8.Text = "Administração";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel8.Visible = false;
            // 
            // ReportExtratoCotista
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.DetailReport,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(101, 101, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportComposicaoCarteira1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentacaoCotista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportQuadroRetorno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.ReportExtratoCotista.ResourceManager;
        }

        #region Funções Internas do Relatorio

        #region Cabeçalho Carteira/Cotista
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellCarteira = sender as XRTableCell;
            xrTableCellCarteira.Text = "";
            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            if (carteira.LoadByPrimaryKey(campos, this.idCarteira))
            {
                xrTableCellCarteira.Text = carteira.Nome.Trim();
            }
        }

        private void CNPJCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellCNPJCarteira = sender as XRTableCell;
            xrTableCellCNPJCarteira.Text = "";

            Pessoa pessoa = new Pessoa();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCarteira);

            if (pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value))
            {
                xrTableCellCNPJCarteira.Text = pessoa.str.Cpfcnpj.Trim();
            }
        }

        private void CotistaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellCotista = sender as XRTableCell;
            xrTableCellCotista.Text = "";
            // Carrega o Cotista
            Cotista cotista = new Cotista();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cotista.Query.Nome);
            if (cotista.LoadByPrimaryKey(campos, this.idCotista))
            {
                xrTableCellCotista.Text = cotista.Nome.Trim();
            }
        }

        private void EnderecoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellEndereco = sender as XRTableCell;
            xrTableCellEndereco.Text = "";
            // Carrega o Endereco

            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            CotistaQuery c = new CotistaQuery("C");

            p.Select(p.Endereco, p.Numero, p.Complemento);
            p.InnerJoin(c).On(p.IdPessoa == c.IdPessoa);
            p.Where(p.RecebeCorrespondencia == "S" & c.IdCotista == this.idCotista);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            if (pessoaEnderecoCollection.HasData)
            {
                string endereco = pessoaEnderecoCollection[0].str.Endereco.Trim();
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim()))
                {
                    endereco += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + " " + pessoaEnderecoCollection[0].str.Complemento.Trim();
                }
                //
                xrTableCellEndereco.Text = endereco.Trim();
            }
            else
            {
                //xrTableCellEndereco.Text = "Endereço Não Cadastrado";
            }
        }

        private void PeriodoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell periodoTableCell = sender as XRTableCell;
            periodoTableCell.Text = this.dataInicio.ToString("d") + " à " + this.dataFim.ToString("d");
        }

        private void CPFCNPJBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell CPFCNPJTableCell = sender as XRTableCell;
            CPFCNPJTableCell.Text = "";

            // Carrega o Cotista
            Pessoa pessoa = new Pessoa();
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(this.idCotista);

            pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);
            //
            string cpfcnpj = pessoa.IsPessoaFisica() ? "CPF: " : "CNPJ: ";
            // Código do espaço deve ser ALT+255
            cpfcnpj += !String.IsNullOrEmpty(pessoa.str.Cpfcnpj) ? pessoa.str.Cpfcnpj : "                   "; // Overload já com a mascara Correta de CPF ou CNPJ

            CPFCNPJTableCell.Text = cpfcnpj;
        }

        private void BairroCidadeCepBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellEndereco = sender as XRTableCell;
            xrTableCellEndereco.Text = "";

            // Carrega o Endereco
            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            CotistaQuery c = new CotistaQuery("C");

            p.Select(p.Bairro, p.Cidade, p.Cep);
            p.InnerJoin(c).On(p.IdPessoa == c.IdPessoa);
            p.Where(p.RecebeCorrespondencia == "S" & c.IdCotista == this.idCotista);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            if (pessoaEnderecoCollection.HasData)
            {
                string bairro = "";

                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim()))
                {
                    bairro = pessoaEnderecoCollection[0].str.Bairro.Trim() + "        ";
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim()))
                {
                    bairro += pessoaEnderecoCollection[0].str.Cidade.Trim() + "        ";
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim()))
                {
                    bairro += Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
                }
                //
                xrTableCellEndereco.Text = bairro;
            }
        }

        #endregion

        #region Cabeçalho Administrador
        private void AdministradorBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellAdministrador = sender as XRTableCell;
            xrTableCellAdministrador.Text = "";

            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            campos.Add(carteira.Query.IdAgenteAdministrador);
            if (carteira.LoadByPrimaryKey(campos, this.idCarteira))
            {
                xrTableCellAdministrador.Text = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Nome.Trim();
            }
        }

        private void CPFCNPJ_AdministradorBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell CPFCNPJTableCellAdministrador = sender as XRTableCell;
            CPFCNPJTableCellAdministrador.Text = "";

            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            campos.Add(carteira.Query.IdAgenteAdministrador);

            if (carteira.LoadByPrimaryKey(campos, this.idCarteira))
            {
                string cnpj = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Cnpj.Trim();

                // Código do espaço deve ser ALT+255
                string cnpjAux = !String.IsNullOrEmpty(cnpj) ? Utilitario.MascaraCNPJ(cnpj) : "                   ";

                CPFCNPJTableCellAdministrador.Text = cnpjAux;
            }
        }

        private void EnderecoAdministradorBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellEnderecoAdministrador = sender as XRTableCell;
            xrTableCellEnderecoAdministrador.Text = "";

            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            campos.Add(carteira.Query.IdAgenteAdministrador);

            if (carteira.LoadByPrimaryKey(campos, this.idCarteira))
            {
                string endereco = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Endereco.Trim();
                xrTableCellEnderecoAdministrador.Text = endereco;
            }
        }

        private void BairroCidadeCep_AdministradorBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableCellEnderecoAdministrador = sender as XRTableCell;
            xrTableCellEnderecoAdministrador.Text = "";

            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            campos.Add(carteira.Query.IdAgenteAdministrador);

            if (carteira.LoadByPrimaryKey(campos, this.idCarteira))
            {
                string end = "";

                string bairro = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Bairro.Trim();
                string cidade = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Cidade.Trim();
                string cep = carteira.UpToAgenteMercadoByIdAgenteAdministrador.str.Cep.Trim();

                if (!String.IsNullOrEmpty(bairro))
                {
                    end = bairro + "        ";
                }
                if (!String.IsNullOrEmpty(cidade))
                {
                    end += cidade + "        ";
                }
                if (!String.IsNullOrEmpty(cep))
                {
                    end += Utilitario.MascaraCEP(cep);
                }
                //
                xrTableCellEnderecoAdministrador.Text = end;
            }
        }
        #endregion

        private void ObservacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText observacao = sender as XRRichText;

            string texto = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCotista.ObservacaoCotista;
            if (!String.IsNullOrEmpty(texto)) {
                observacao.Html = texto;
                observacao.Borders = DevExpress.XtraPrinting.BorderSide.All;
                observacao.Visible = true;
                xrLabel8.Visible = true;
            }
            else {
                observacao.Visible = false;
                xrLabel8.Visible = false;
            }                       
        }
        
        private void DataEmissaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataEmissaoTableCell = sender as XRTableCell;
            dataEmissaoTableCell.Text = DateTime.Now.ToString("d");
        }

        #endregion
    }
}