﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Util;
using System.Collections.Generic;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportComposicaoCarteira
    /// </summary>
    public class SubReportComposicaoCarteira : XtraReport {
        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idCotista;

        public int IdCotista {
            get { return idCotista; }
            set { idCotista = value; }
        }

        private DateTime data;

        public DateTime Data{
            get { return data; }
            set { data = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se Relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow1;
        private XRTable xrTable1;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow2;
        private XRTable xrTable2;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell10;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;                
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell18;
        private PageHeaderBand PageHeader;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.Parameters.Parameter ParameterTotalValorMercado;
        private CalculatedField calculatedField_PL;
        private GroupFooterBand GroupFooter1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportComposicaoCarteira() {
            this.InitializeComponent();
        }

        /// <summary>
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="idOperacao"></param>
        /// <param name="dataConversao"></param>
        public void PersonalInitialize(int idCarteira, int idCotista, DateTime data) {
            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.data = data;

            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
            //
            ReportBase relatorioBase = new ReportBase(this);

            this.SetRelatorioSemDados();
        }
      
        /// <summary>
        /// Se Relatório não tem Dados Deixa Invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            //this.xrTable3.Visible = this.numeroLinhasDataTable != 0;
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();

            DateTime dataPrimeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(this.data, 0);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            //                       
            esParameters esParams = new esParameters();
            esParams.Add("IdCarteira", this.idCarteira);
            esParams.Add("DataHistorico", this.data);
            esParams.Add("DataPrimeiroDiaMes", dataPrimeiroDiaMes);
            //
            #region Ações
            sqlText.AppendLine(" SELECT '1' as Identificador, 'Bov.Ações' as TipoAtivo, ");
            sqlText.AppendLine("	a.cdativobolsa as CodigoAtivo, ");
            sqlText.AppendLine("	a.Descricao as Descricao, ");
            sqlText.AppendLine("	null AS DataVencimento, ");
            sqlText.AppendLine("	SUM(Quantidade) as Quantidade, ");
            sqlText.AppendLine("	SUM(valorMercado) as SaldoBruto ");
            sqlText.AppendLine(" FROM Posicaobolsahistorico p, Ativobolsa a, Cliente c ");
            sqlText.AppendLine(" WHERE p.cdativobolsa = a.cdativobolsa ");
            sqlText.AppendLine(" and p.idcliente = c.idcliente ");
            sqlText.AppendLine(" and quantidade != 0 ");
            sqlText.AppendLine(" and p.TipoMercado = '" + TipoMercadoBolsa.MercadoVista + "' ");
            sqlText.AppendLine(" and p.idcliente = @IdCarteira ");
            sqlText.AppendLine(" and p.datahistorico = @DataHistorico ");
            sqlText.AppendLine(" GROUP BY a.cdativobolsa, ");
            sqlText.AppendLine("	      a.Descricao");
            #endregion

            #region Opções
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '2' as Identificador, 'Bov.Opções' as TipoAtivo, ");
            sqlText.AppendLine(" 	a.cdativobolsa as CodigoAtivo, ");
            sqlText.AppendLine(" 	'Opção de ' + a.CdAtivoBolsaObjeto as Descricao, ");
            sqlText.AppendLine(" 	a.DataVencimento AS DataVencimento,  ");
            sqlText.AppendLine(" 	SUM(Quantidade) as Quantidade,  ");
            sqlText.AppendLine(" 	SUM(valorMercado) as SaldoBruto ");
            sqlText.AppendLine(" FROM Posicaobolsahistorico p, Ativobolsa a, Cliente c ");
            sqlText.AppendLine(" WHERE p.cdativobolsa = a.cdativobolsa ");
            sqlText.AppendLine(" and p.idcliente = c.idcliente ");
            sqlText.AppendLine(" and quantidade != 0 ");
            sqlText.AppendLine(" and p.tipomercado IN ('" + TipoMercadoBolsa.OpcaoCompra + "', '" + TipoMercadoBolsa.OpcaoVenda + "') ");
            sqlText.AppendLine(" and p.idcliente = @IdCarteira ");
            sqlText.AppendLine(" and p.datahistorico = @DataHistorico ");
            sqlText.AppendLine(" GROUP BY a.cdativobolsa, ");
            sqlText.AppendLine("          a.CdAtivoBolsaObjeto, ");
            sqlText.AppendLine("          'Opção de ' + a.CdAtivoBolsaObjeto, ");
            sqlText.AppendLine("          a.DataVencimento ");
            #endregion

            #region Imobilário
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '3' as Identificador, 'Bov.FII' as TipoAtivo, ");
            sqlText.AppendLine(" 	a.cdativobolsa as CodigoAtivo, ");
            sqlText.AppendLine(" 	a.Descricao as Descricao, ");
            sqlText.AppendLine(" 	null AS DataVencimento, ");
            sqlText.AppendLine(" 	SUM(Quantidade) as Quantidade, ");
            sqlText.AppendLine(" 	SUM(valorMercado) as SaldoBruto ");
            sqlText.AppendLine(" FROM Posicaobolsahistorico p, Ativobolsa a, Cliente c ");
            sqlText.AppendLine(" WHERE p.cdativobolsa = a.cdativobolsa ");
            sqlText.AppendLine(" and p.idcliente = c.idcliente ");
            sqlText.AppendLine(" and quantidade != 0");
            sqlText.AppendLine(" and p.TipoMercado = '" + TipoMercadoBolsa.Imobiliario + "' ");
            sqlText.AppendLine(" and p.idcliente = @IdCarteira");
            sqlText.AppendLine(" and p.datahistorico = @DataHistorico");
            sqlText.AppendLine(" GROUP BY a.cdativobolsa, ");
            sqlText.AppendLine(" 	      a.Descricao");
            #endregion

            #region Fundos
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '4' as Identificador, 'Cotas de Fundos' as TipoAtivo, ");
            sqlText.AppendLine(" a.CodigoAnbid as CodigoAtivo, ");
            sqlText.AppendLine(" 	a.apelido as Descricao, ");
            sqlText.AppendLine(" 	null AS DataVencimento, ");
            sqlText.AppendLine(" 	SUM(Quantidade) as Quantidade,  ");
            sqlText.AppendLine(" 	SUM(ValorBruto) as SaldoBruto ");
            sqlText.AppendLine(" FROM Posicaofundohistorico p, carteira a, cliente c ");
            sqlText.AppendLine(" WHERE p.idcarteira = a.idcarteira ");
            sqlText.AppendLine(" and p.idcliente = c.idcliente ");
            sqlText.AppendLine(" and quantidade != 0 ");
            sqlText.AppendLine(" and p.idcliente = @IdCarteira ");
            sqlText.AppendLine(" and p.datahistorico = @DataHistorico ");
            sqlText.AppendLine(" GROUP BY a.CodigoAnbid, ");
            sqlText.AppendLine(" 		  a.apelido ");
            #endregion
            
            #region RendaFixa
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '5' as Identificador, 'Renda Fixa' AS TipoAtivo, ");
            sqlText.AppendLine(" 	    e.descricao AS CodigoAtivo, ");
            sqlText.AppendLine(" 	    m.nome AS Descricao, ");
            sqlText.AppendLine(" 	    p.datavencimento AS DataVencimento, ");
            sqlText.AppendLine(" 	    SUM(Quantidade) AS Quantidade, ");
            sqlText.AppendLine(" 	    SUM(valorMercado) AS SaldoBruto ");
            sqlText.AppendLine(" FROM PosicaoRendaFixaHistorico P, ");
            sqlText.AppendLine(" 	  PapelRendafixa E, ");
            sqlText.AppendLine(" 	  Cliente C, ");
            sqlText.AppendLine(" 	  TituloRendafixa T, ");
            sqlText.AppendLine(" 	  Emissor M ");
            sqlText.AppendLine(" WHERE p.idtitulo = t.idtitulo ");
            sqlText.AppendLine(" and t.idpapel = e.idpapel ");
            sqlText.AppendLine(" and t.idemissor = m.idemissor ");
            sqlText.AppendLine(" and p.idcliente = c.idcliente ");
            sqlText.AppendLine(" and quantidade != 0 ");
            sqlText.AppendLine(" and p.idcliente = @IdCarteira ");
            sqlText.AppendLine(" and p.datahistorico = @DataHistorico ");
            sqlText.AppendLine(" GROUP by e.descricao, ");
            sqlText.AppendLine(" 	      m.nome, ");
            sqlText.AppendLine(" 	      p.datavencimento ");
            #endregion
            
            #region Taxas de Administração
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '6' as Identificador, 'Taxas' as TipoAtivo, ");
            sqlText.AppendLine(" 	'' as CodigoAtivo, ");
            sqlText.AppendLine(" 	'Taxa de Administração' as Descricao, ");
            sqlText.AppendLine(" 	DataVencimento AS DataVencimento, ");
            sqlText.AppendLine(" 	null as Quantidade, ");
            sqlText.AppendLine(" 	Valor as SaldoBruto	");
            sqlText.AppendLine(" FROM Liquidacaohistorico l ");
            sqlText.AppendLine(" WHERE l.datavencimento > @DataHistorico");
            sqlText.AppendLine(" and l.datalancamento <= @DataHistorico");
            sqlText.AppendLine(" and l.valor != 0");
            sqlText.AppendLine(" and l.origem = '" + OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao + "'");
            sqlText.AppendLine(" and l.idcliente = @IdCarteira");
            sqlText.AppendLine(" and l.datahistorico = @DataHistorico");
            #endregion

            #region Taxas de Gestão
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '7' as Identificador, 'Taxas' as TipoAtivo, ");
            sqlText.AppendLine("	    '' as CodigoAtivo, ");
            sqlText.AppendLine("	    'Taxa de Gestão' as Descricao, ");
            sqlText.AppendLine("	    DataVencimento AS DataVencimento, ");
            sqlText.AppendLine("	    null as Quantidade, ");
            sqlText.AppendLine("	    Valor as SaldoBruto	");
            sqlText.AppendLine(" FROM Liquidacaohistorico l ");
            sqlText.AppendLine(" WHERE l.datavencimento > @DataHistorico ");
            sqlText.AppendLine(" and l.datalancamento <= @DataHistorico ");
            sqlText.AppendLine(" and l.valor != 0");
            sqlText.AppendLine(" and l.origem = '" + OrigemLancamentoLiquidacao.Provisao.TaxaGestao + "'");
            sqlText.AppendLine(" and l.idcliente = @IdCarteira");
            sqlText.AppendLine(" and l.datahistorico = @DataHistorico");
            #endregion

            #region Taxa de Performance
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '8' as Identificador, 'Taxas' as TipoAtivo, ");
            sqlText.AppendLine(" 	'' as CodigoAtivo, ");
            sqlText.AppendLine(" 	'Taxa de Performance' as Descricao, ");
            sqlText.AppendLine(" 	DataVencimento AS DataVencimento, ");
            sqlText.AppendLine(" 	null as Quantidade, ");
            sqlText.AppendLine(" 	Valor as SaldoBruto	");
            sqlText.AppendLine(" FROM Liquidacaohistorico l ");
            sqlText.AppendLine(" WHERE l.datavencimento > @DataHistorico ");
            sqlText.AppendLine(" and l.datalancamento <= @DataHistorico ");
            sqlText.AppendLine(" and l.valor != 0");
            sqlText.AppendLine(" and l.origem = '" + OrigemLancamentoLiquidacao.Provisao.TaxaPerformance + "'");
            sqlText.AppendLine(" and l.idcliente = @IdCarteira");
            sqlText.AppendLine(" and l.datahistorico = @DataHistorico");
            #endregion

            #region Caixa
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '9' as Identificador, 'Saldo c/c' as TipoAtivo,");
            sqlText.AppendLine(" 	    '' as CodigoAtivo,");
            sqlText.AppendLine(" 	    'Total do saldo em c/c' as Descricao,");
            sqlText.AppendLine(" 	    null AS DataVencimento,");
            sqlText.AppendLine(" 	    null as Quantidade, ");
            sqlText.AppendLine(" 	    SaldoFechamento as SaldoBruto	");
            sqlText.AppendLine(" FROM Saldocaixa s, Cliente c");
            sqlText.AppendLine(" WHERE s.idcliente = c.idcliente");
            sqlText.AppendLine(" and s.idcliente = @IdCarteira");
            sqlText.AppendLine(" and s.data = @DataHistorico");
            #endregion
                        
            #region Corretagem Bovespa Paga no periodo
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '10' as Identificador, 'Vl Desp.' as TipoAtivo, ");
            sqlText.AppendLine("	    '' as CodigoAtivo, ");
            sqlText.AppendLine("	    'Corretagem Bovespa no Mês' as Descricao, ");
            sqlText.AppendLine("	    null AS DataVencimento, ");
            sqlText.AppendLine("	    null as Quantidade, ");
            sqlText.AppendLine("	    SUM(Corretagem * -1) as SaldoBruto	");
            sqlText.AppendLine(" FROM OperacaoBolsa o ");
            sqlText.AppendLine(" WHERE o.data >= @DataPrimeiroDiaMes ");
            sqlText.AppendLine(" and o.data <= @DataHistorico ");
            sqlText.AppendLine(" and o.idcliente = @IdCarteira");
            //sqlText.AppendLine(" GROUP BY TipoAtivo, CodigoAtivo, Descricao, DataVencimento, Quantidade ");
            #endregion

            #region Corretagem BMF Paga no periodo
            sqlText.AppendLine("");
            sqlText.AppendLine(" Union All ");
            sqlText.AppendLine(" SELECT '11' as Identificador, 'Vl Desp.' as TipoAtivo, ");
            sqlText.AppendLine("	    '' as CodigoAtivo, ");
            sqlText.AppendLine("	    'Corretagem BMF no Mês' as Descricao, ");
            sqlText.AppendLine("	    null AS DataVencimento, ");
            sqlText.AppendLine("	    null as Quantidade, ");
            sqlText.AppendLine("	    SUM(Corretagem * -1) as SaldoBruto	");
            sqlText.AppendLine(" FROM OperacaoBMF o ");
            sqlText.AppendLine(" WHERE o.data >= @DataPrimeiroDiaMes ");
            sqlText.AppendLine(" and o.data <= @DataHistorico ");
            sqlText.AppendLine(" and o.idcliente = @IdCarteira");            
            #endregion

            #endregion
            //
            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            #region Calcula Total SaldoBruto
            decimal totalSaldoBruto = 0;
            foreach (DataRow dr in dt.Rows) 
            {
                decimal? saldoBruto = Utilitario.CastColumnValueToDecimal(dr["SaldoBruto"]);
                if (saldoBruto != null)
                {
                    totalSaldoBruto += saldoBruto.Value;
                }                
            }
            #endregion

            // Preenche o TotalValorMercado para ser Usado no Calculo de %PL
            this.Parameters["ParameterTotalValorMercado"].Value = totalSaldoBruto;

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            string resourceFileName = "SubReportComposicaoCarteira.resx";
            System.Resources.ResourceManager resources = global::Resources.SubReportComposicaoCarteira.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.ParameterTotalValorMercado = new DevExpress.XtraReports.Parameters.Parameter();
            this.calculatedField_PL = new DevExpress.XtraReports.UI.CalculatedField();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 49F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TipoAtivo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1850F, 48F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell8,
            this.xrTableCell12,
            this.xrTableCell7,
            this.xrTableCell18,
            this.xrTableCell10});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.TipoAtivo")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.10947365946654405;
            this.xrTableCell3.WordWrap = false;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.CanGrow = false;
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CodigoAtivo")});
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.12892215247988531;
            this.xrTableCell11.WordWrap = false;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Descricao")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.26053367074285427;
            this.xrTableCell8.WordWrap = false;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DataVencimento", "{0:d}")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 0.10748419849292648;
            this.xrTableCell12.WordWrap = false;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:n2}")});
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.13702623574052056;
            this.xrTableCell7.WordWrap = false;
            this.xrTableCell7.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatNoZero);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.CanGrow = false;
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.SaldoBruto", "{0:n2}")});
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.14274697845589213;
            this.xrTableCell18.WordWrap = false;
            this.xrTableCell18.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.CanGrow = false;
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedField_PL")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.11381310462137713;
            this.xrTableCell10.WordWrap = false;
            this.xrTableCell10.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1850F, 48F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell9,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell6,
            this.xrTableCell16});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Tipo";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell1.Weight = 0.10947368421052632;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Código";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell2.Weight = 0.12892211948790897;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Descrição";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.26053371198282471;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Data Vencimento";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell14.Weight = 0.10748419849292649;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Quantidade";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.13702616975656787;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Valor Mercado";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell6.Weight = 0.14274697845589213;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "% PL";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.11381313761335349;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TipoAtivo")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell4.Weight = 0.98804780876494025;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell27.Text = "-";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell27.Weight = 0.011952191235059761;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell4});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1757F, 55F);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell20.Weight = 1;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 56F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1750F, 15F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 84.66666F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(100F, 25F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1850F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "Composição da carteira do Clube";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 52.91667F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // ParameterTotalValorMercado
            // 
            this.ParameterTotalValorMercado.Name = "ParameterTotalValorMercado";
            this.ParameterTotalValorMercado.Type = typeof(decimal);
            this.ParameterTotalValorMercado.Value = 0;
            // 
            // calculatedField_PL
            // 
            this.calculatedField_PL.DataMember = "esUtility";
            this.calculatedField_PL.Expression = "[SaldoBruto] /[Parameters.ParameterTotalValorMercado]";
            this.calculatedField_PL.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedField_PL.Name = "calculatedField_PL";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 2.645833F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // SubReportComposicaoCarteira
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedField_PL});
            this.DataSourceSchema = resources.GetString("$this.DataSourceSchema");
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterTotalValorMercado});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportComposicaoCarteira.ResourceManager;

        }

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// Se valor da celula for zero não coloca nada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatNoZero(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal? valor = null;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
        }
        #endregion
    }
}
