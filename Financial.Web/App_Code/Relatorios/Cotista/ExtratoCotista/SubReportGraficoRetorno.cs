﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportGraficoRetorno
    /// </summary>
    public class SubReportGraficoRetorno : XtraReport {

        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        // Carteira que será gerado o grafico
        private int idCarteira;
        
        // Lista de Indices que será gerado o grafico
        private List<int> listaIdIndice;
        
        #region Classe Interna que Contem o IdCarteira E os Dados de Rentabilidade da Carteira
        protected class DadosCarteira {
            //
            public int idCarteira;
            public string nomeFundo;
            
            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeFundo = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosCarteira dadosCarteira;
        #endregion

        #region Classe Interna que Contem o IdIndice E os Dados de Rentabilidade do Indice
        protected class DadosIndice {
            public int idIndice;
            public string nomeIndice;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeIndice = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosIndice[] dadosIndice;
        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRLine xrLine1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportGraficoRetorno() {
            this.InitializeComponent();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">Carteira a ser colocado no gráfico</param>
        /// <param name="dataInicio">Data Inicio do Gráfico De Rentabilidade</param>
        /// <param name="dataFim">Data Fim do Gráfico De Rentabilidade</param>
        public SubReportGraficoRetorno(int idCarteira, DateTime dataInicio, DateTime dataFim) {
            this.idCarteira = idCarteira;
            //
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            // Inicializa a classe De Dados de Carteira - Apenas 1 Carteira
            this.dadosCarteira = new DadosCarteira(); ;
            //
            this.dadosCarteira.idCarteira = idCarteira;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.IdCarteira);
            campos.Add(carteira.Query.Apelido);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            //
            this.dadosCarteira.nomeFundo = carteira.Apelido.Trim();
            //
            this.listaIdIndice = new List<int>();
            ListaBenchmarkCollection listaBenchmarkCollection = carteira.ListaBenchmarkCollectionByIdCarteira;
            foreach (ListaBenchmark l in listaBenchmarkCollection) {
                this.listaIdIndice.Add(l.IdIndice.Value);
            }

            // Inicializa a classe De Dados de Indice
            this.dadosIndice = new DadosIndice[listaIdIndice.Count];
            for (int i = 0; i < listaIdIndice.Count; i++) {
                this.dadosIndice[i] = new DadosIndice();
                //
                this.dadosIndice[i].idIndice = listaIdIndice[i];

                Indice indice = new Indice();
                List<esQueryItem> camposAux = new List<esQueryItem>();
                camposAux.Add(indice.Query.Descricao);
                indice.LoadByPrimaryKey(camposAux, (short)listaIdIndice[i]);
                //
                this.dadosIndice[i].nomeIndice = indice.Descricao.Trim();
            }

            this.InitializeComponent();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">Carteira a ser colocado no gráfico</param>
        /// <param name="dataInicio">Data Inicio do Gráfico De Rentabilidade</param>
        /// <param name="dataFim">Data Fim do Gráfico De Rentabilidade</param>
        public void PersonalInitialize(int idCarteira, DateTime dataInicio, DateTime dataFim) {
            this.idCarteira = idCarteira;
            //
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            // Inicializa a classe De Dados de Carteira - Apenas 1 Carteira
            this.dadosCarteira = new DadosCarteira();;
            //
            this.dadosCarteira.idCarteira = idCarteira;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.IdCarteira);
            campos.Add(carteira.Query.Apelido);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            //
            this.dadosCarteira.nomeFundo = carteira.Apelido.Trim();
            //
            this.listaIdIndice = new List<int>();
            ListaBenchmarkCollection listaBenchmarkCollection = carteira.ListaBenchmarkCollectionByIdCarteira;            
            foreach (ListaBenchmark l in listaBenchmarkCollection) {
                this.listaIdIndice.Add(l.IdIndice.Value);
            }

            // Inicializa a classe De Dados de Indice
            this.dadosIndice = new DadosIndice[listaIdIndice.Count];
            for (int i = 0; i < listaIdIndice.Count; i++) {
                this.dadosIndice[i] = new DadosIndice();
                //
                this.dadosIndice[i].idIndice = listaIdIndice[i];

                Indice indice = new Indice();
                List<esQueryItem> camposAux = new List<esQueryItem>();
                camposAux.Add(indice.Query.Descricao);
                indice.LoadByPrimaryKey(camposAux, (short)listaIdIndice[i]);
                //
                this.dadosIndice[i].nomeIndice = indice.Descricao.Trim();
            }

            //this.InitializeComponent();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();            
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize() {
            //
            this.CarregaDadosRentabilidade();
            //
            this.FillDadosGrafico();
        }
        
        /// <summary>
        /// Carrega num Dicionario uma lista de Datas com as Respectivas Rentabilidades Do Fundo e Indices
        /// </summary>
        private void CarregaDadosRentabilidade() {

            #region Limpa os Dicionarios
            // Limpa o Dicionario da Carteira            
            this.dadosCarteira.dadosRentabilidadeFundo.Clear();
            
            // Limpa o Dicionario de Indice
            for (int j = 0; j < this.dadosIndice.Length; j++) {
                this.dadosIndice[j].dadosRentabilidadeIndice.Clear();
            }
            #endregion

            //DateTime dataAux = this.dataInicio;
            //DateTime dataFinal = this.dataFim;
            ////
            //CalculoMedida c = new CalculoMedida(dadosCarteira.idCarteira);
            ////

            //#region Carrega Dados da Carteira
            //Carteira carteira = new Carteira();
            ////
            //List<esQueryItem> campos = new List<esQueryItem>();
            //campos.Add(carteira.Query.TipoCota);
            //campos.Add(carteira.Query.IdCarteira);
            //campos.Add(carteira.Query.DataInicioCota);
            ////            
            //carteira.QueryReset();
            //carteira.LoadByPrimaryKey(campos, this.idCarteira);
            ////
            //DateTime dataInicioCliente = carteira.DataInicioCota.Value;
            //#endregion
            ////
            //c.SetDataInicio(dataInicioCliente);

            //#region Dados Dos Fundos
            //// Carrega o Dicionário
            //while (dataAux <= dataFinal) {

            //    #region Fundos

            //    try {
            //        decimal rentabilidade = c.CalculaRetornoDia(dataAux);

            //        rentabilidade = rentabilidade * 100;
            //        this.dadosCarteira.dadosRentabilidadeFundo.Add(dataAux, rentabilidade);
            //    }
            //    catch (Exception e) {
            //        // Não adiciona o Ponto se der Exceção
            //    }
            //    #endregion

            //    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
            //}
            //#endregion

            //for (int j = 0; j < this.dadosIndice.Length; j++) {
            //    DateTime dataAuxIndice = this.dataInicio;
            //    DateTime dataFinalIndice = this.dataFim;

            //    CalculoMedida cIndice = new CalculoMedida((short)dadosIndice[j].idIndice);
            //    //
            //    #region Dados Dos Índices
            //    // Carrega o Dicionário
            //    while (dataAuxIndice <= dataFinalIndice) {

            //        #region Fundos

            //        try {
            //            decimal rentabilidade = cIndice.CalculaRetornoDiaIndice(dataAuxIndice);

            //            rentabilidade = rentabilidade * 100;
            //            this.dadosIndice[j].dadosRentabilidadeIndice.Add(dataAuxIndice, rentabilidade);
            //        }
            //        catch (Exception e) {
            //            // Não adiciona o Ponto se der Exceção
            //        }
            //        #endregion

            //        dataAuxIndice = Calendario.AdicionaDiaUtil(dataAuxIndice, 1);
            //    }
            //    #endregion
            //}

            DateTime dataInicio = this.dataInicio;
            DateTime dataFinal = this.dataFim;

            #region Dados Dos Fundos
            // Carrega o Dicionario
            decimal cotaPrimeiroDia = 1;
            int i = 0;
            while (dataInicio.CompareTo(dataFinal) <= 0) {

                #region Fundos
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCota(this.dadosCarteira.idCarteira, dataInicio);

                // Não Adiciona Rentabilidade quando não houver cota
                if (historicoCota.es.HasData && historicoCota.CotaFechamento.HasValue) {
                    decimal cotaFechamento = historicoCota.CotaFechamento.Value;
                    if (i == 0) {
                        cotaPrimeiroDia = cotaFechamento;
                    }

                    if (cotaPrimeiroDia != 0.0M) {
                        decimal rendimento = (cotaFechamento / cotaPrimeiroDia) - 1;
                        rendimento = rendimento * 100;

                        this.dadosCarteira.dadosRentabilidadeFundo.Add(dataInicio, rendimento);
                        i++;
                    }
                }
                #endregion

                dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1);
            }
            #endregion

            for (int j = 0; j < this.dadosIndice.Length; j++) {
                DateTime dataAux = this.dataInicio;
                DateTime dataFinalIndice = this.dataFim;

                #region Dados Dos Indices
                // Carrega o Dicionario
                decimal fatorMultiplicacao = 1;

                Indice indice = new Indice();
                short idIndice = (short)this.dadosIndice[j].idIndice;
                indice.LoadByPrimaryKey(idIndice);
                byte tipoIndice = indice.Tipo.Value;
                byte tipoDivulgacao = indice.TipoDivulgacao.Value;

                while (dataAux.CompareTo(dataFinalIndice) <= 0) {

                    #region Indices
                    try 
                    {
                        CalculoMedida calculoMedida = new CalculoMedida(this.dadosIndice[j].idIndice);
                        
                        decimal fator = 1;

                        if (tipoIndice == (int)TipoIndice.Percentual)
                        {
                            fator = calculoMedida.CalculaRetornoIndice(dataAux, dataAux);
                        }
                        else
                        {
                            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            fator = calculoMedida.CalculaRetornoIndice(dataAnterior, dataAux);
                        }

                        fator = fator * fatorMultiplicacao;

                        decimal rendimentoIndice = fator - 1;
                        this.dadosIndice[j].dadosRentabilidadeIndice.Add(dataAux, rendimentoIndice * 100);
                        //FatorMultiplicacao
                        fatorMultiplicacao = fator;
                    }
                    catch (CotacaoIndiceNaoCadastradoException e) {
                        //Não adiciona se não houver CotacaoIndice
                    }
                    #endregion
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
                }
                #endregion
            }
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico() {

            #region Adiciona Série de Fundos
            //
            Series series1 = new Series(this.dadosCarteira.nomeFundo, ViewType.Line);
            //
            foreach (KeyValuePair<DateTime, decimal> pair in this.dadosCarteira.dadosRentabilidadeFundo) {
                series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series1.ArgumentScaleType = ScaleType.DateTime;
            series1.Label.Visible = false;
            //
            // Só Adiciona Serie se houver Dados
            if (this.dadosCarteira.dadosRentabilidadeFundo.Count > 0) {
                this.xrChart1.Series.Add(series1);
            }
            #endregion

            #region Adiciona N Série de Indices
            for (int i = 0; i < this.dadosIndice.Length; i++) {
                Series series2 = new Series(this.dadosIndice[i].nomeIndice, ViewType.Line);
                //
                foreach (KeyValuePair<DateTime, decimal> pair in this.dadosIndice[i].dadosRentabilidadeIndice) {
                    series2.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
                }
                series2.ArgumentScaleType = ScaleType.DateTime;
                series2.Label.Visible = false;
                //
                // Só Adiciona Serie se houver Dados
                if (this.dadosIndice[i].dadosRentabilidadeIndice.Count > 0) {
                    this.xrChart1.Series.Add(series2);
                }
            }
            #endregion

            this.xrChart1.Series[0].ShowInLegend = false;
            this.xrChart1.Series[1].ShowInLegend = false;

            //((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Day;

            // Determina a escala baseado no Numero de Pontos

            // Calcula o Numero de Dias uteis no Periodo
            //int numeroDiasUteis = Calendario.NumeroDias(this.dataInicio, this.dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            ////
            //if (numeroDiasUteis <= 240) {
            //    ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Day;
            //}
            //if (numeroDiasUteis > 240 && numeroDiasUteis <= 1000) {
            //    ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Month;
            //}
            //else {
            //    ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Quarter;
            //}
        }
       
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ExtratoCotista/SubReportGraficoRetorno.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportGraficoRetorno.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel3 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 8F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrLabel1,
            this.xrChart1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 1101F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.SteelBlue;
            this.xrLine1.LineWidth = 7;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(614F, 42F);
            this.xrLine1.StylePriority.UseBackColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(50F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(423F, 64F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Histórico de Rentabilidade";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Terracotta Pie";
            this.xrChart1.BorderColor = System.Drawing.Color.Black;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram1.AxisX.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Angle = 90;
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Title.Text = "Data";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisY.Label.EndText = "%";
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "Retorno Acumulado (%)";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYZooming = true;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(10, 10);
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(21F, 148F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Civic";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "CDIAAux";
            lineSeriesView1.LineMarkerOptions.Visible = false;
            series1.View = lineSeriesView1;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel2.Visible = false;
            series2.Label = pointSeriesLabel2;
            series2.Name = "FIC DE FIM GPAR";
            lineSeriesView2.LineMarkerOptions.Visible = false;
            series2.View = lineSeriesView2;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = pointSeriesLabel3;
            lineSeriesView3.ColorEach = true;
            this.xrChart1.SeriesTemplate.View = lineSeriesView3;
            this.xrChart1.SizeF = new System.Drawing.SizeF(1334F, 931F);
            this.xrChart1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            chartTitle1.Font = new System.Drawing.Font("Tahoma", 16F);
            chartTitle1.Indent = 2;
            chartTitle1.Text = "Retorno Acumulado";
            chartTitle1.TextColor = System.Drawing.Color.Black;
            this.xrChart1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            this.xrChart1.CustomDrawSeries += new DevExpress.XtraCharts.CustomDrawSeriesEventHandler(this.xrChart1_CustomDrawSeries);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportGraficoRetorno
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 1246, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportGraficoRetorno.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e) {
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
        }
    }
}