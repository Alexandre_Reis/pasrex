﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Investidor.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Fundo.Exceptions;
using Financial.InvestidorCotista;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common.Exceptions;
using System.Text;
using log4net;
using Financial.Common;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportQuadroRetorno
    /// </summary>
    public class SubReportQuadroRetorno : XtraReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SubReportQuadroRetorno));

        private int idCarteira;

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private DateTime dataInicioCota;

        public DateTime DataInicioCota
        {
            get { return dataInicioCota; }
            set { dataInicioCota = value; }
        }

        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private DateTime dataSaldoAnterior;

        public DateTime DataSaldoAnterior
        {
            get { return dataSaldoAnterior; }
            set { dataSaldoAnterior = value; }
        }


        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRControlStyle xrControlStyle1;
        private XRControlStyle xrControlStyle2;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private GroupHeaderBand GroupHeader1;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell22;
        private XRTable xrTable2;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private PageFooterBand PageFooter;
        private XRTable xrTable3;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell37;
        private XRControlStyle xrControlStyle3;
        private XRTable xrTable5;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportQuadroRetorno()
        {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataInicioCota, DateTime dataInicio, DateTime dataFim)
        {
            this.idCarteira = idCarteira;
            this.dataInicioCota = dataInicioCota;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;

            this.dataSaldoAnterior = Calendario.SubtraiDiaUtil(this.dataInicio, 1);

            if (this.dataSaldoAnterior < this.dataInicioCota)
            {
                this.dataSaldoAnterior = this.dataInicioCota;
            }

            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
            //
            ReportBase relatorioBase = new ReportBase(this);

            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            //if (this.numeroLinhasDataTable == 0) {
            //    // Desaparece com as todas as bandas menos o subreport                                
            //    this.subreport3.Visible = true;
            //    //
            //    this.xrTable7.Visible = false;
            //    this.GroupHeader1.Visible = false;
            //    this.GroupHeader2.Visible = false;                
            //    this.ReportFooter.Visible = false;

            //}
        }

        private DataTable FillDados()
        {
            ListaBenchmarkQuery listaBenchmarkQuery = new ListaBenchmarkQuery("L");
            IndiceQuery indiceQuery = new IndiceQuery("I");

            listaBenchmarkQuery.Select(listaBenchmarkQuery.IdIndice, indiceQuery.Descricao);
            listaBenchmarkQuery.InnerJoin(indiceQuery).On(listaBenchmarkQuery.IdIndice == indiceQuery.IdIndice);
            listaBenchmarkQuery.Where(listaBenchmarkQuery.IdCarteira == this.idCarteira);

            ListaBenchmarkCollection l = new ListaBenchmarkCollection();
            l.Load(listaBenchmarkQuery);

            return l.Query.LoadDataTable();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ExtratoCotista/SubReportQuadroRetorno.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportQuadroRetorno.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrControlStyle3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Detail.HeightF = 46F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.EvenStyleName = "xrControlStyle1";
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1850F, 46F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell32,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell33,
            this.xrTableCell25,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 1;
            this.xrTableRow3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeIndiceBeforePrint);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Índice";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.26648648648648648;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorderColor = false;
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "RentabilidadeMes";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.08;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "RentabildadeAno";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.099459459459459457;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorderColor = false;
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Rentabilidade6Meses";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell24.Weight = 0.0918918918918919;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorderColor = false;
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Rentabilidade12Meses";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.091351351351351348;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell25.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorderColor = false;
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Rentabilidade24Meses";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 0.091351351351351348;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell34.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorderColor = false;
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "Rentabilidade2007";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 0.0918918918918919;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell35.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorderColor = false;
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Rentabilidade2008";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.091351351351351348;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "RentabilidadePeriodo";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.096216216216216219;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Transparent;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.BorderWidth = 0;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 90F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow4});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1850F, 138F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseBorderWidth = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow1.BorderWidth = 1;
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.StylePriority.UseBackColor = false;
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UseBorderWidth = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.33333333333333331;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.DarkGray;
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "RENTABILIDADES (em %)";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.BackColor = System.Drawing.Color.LightGray;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell8,
            this.xrTableCell11,
            this.xrTableCell3,
            this.xrTableCell26,
            this.xrTableCell29,
            this.xrTableCell9});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.StylePriority.UseBackColor = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.33333333333333331;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell18.BorderWidth = 1;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseBorderWidth = false;
            this.xrTableCell18.Weight = 0.26648648648648648;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell12.BorderWidth = 1;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBorderColor = false;
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseBorderWidth = false;
            this.xrTableCell12.StylePriority.UseForeColor = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Mês";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.08;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell13.BorderWidth = 1;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseBorderColor = false;
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseBorderWidth = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseForeColor = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Ano";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.10270270270270271;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell8.BorderWidth = 1;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorderColor = false;
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseBorderWidth = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "6 Meses";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.091351351351351348;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell11.BorderWidth = 1;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorderColor = false;
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseBorderWidth = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "12 Meses";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.0918918918918919;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell3.BorderWidth = 1;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorderColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseBorderWidth = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "24 Meses";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.091351351351351348;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell26.BorderWidth = 1;
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseBorderWidth = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Ano1";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.091351351351351348;
            this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Ano1BeforePrint);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell29.BorderWidth = 1;
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorderColor = false;
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseBorderWidth = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseForeColor = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "Ano2";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.0918918918918919;
            this.xrTableCell29.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Ano2BeforePrint);
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell9.BorderWidth = 1;
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseBorderWidth = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseForeColor = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Acum.";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.092972972972972967;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell2,
            this.xrTableCell50,
            this.xrTableCell10,
            this.xrTableCell17,
            this.xrTableCell4,
            this.xrTableCell27,
            this.xrTableCell30,
            this.xrTableCell15});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.33333333333333331;
            this.xrTableRow4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeCarteiraBeforePrint);
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell22.BorderWidth = 1;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseBorderWidth = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Carteira";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.26648648648648648;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.BorderWidth = 1;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorderColor = false;
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseBorderWidth = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "RentabilidadeMes";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.08;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell50.BorderWidth = 1;
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.StylePriority.UseBorderColor = false;
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseBorderWidth = false;
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "RentabilidadeAno";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell50.Weight = 0.10270270270270271;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell10.BorderWidth = 1;
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorderColor = false;
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseBorderWidth = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "Rentabilidade6Meses";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.091351351351351348;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell17.BorderWidth = 1;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorderColor = false;
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseBorderWidth = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Rentabilidade12Meses";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.0918918918918919;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell4.BorderWidth = 1;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorderColor = false;
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseBorderWidth = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Rentabilidade24Meses";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.091351351351351348;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell27.BorderWidth = 1;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorderColor = false;
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.StylePriority.UseBorderWidth = false;
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Rentabilidade2007";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.091351351351351348;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell30.BorderWidth = 1;
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorderColor = false;
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseBorderWidth = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Rentabilidade2008";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.0918918918918919;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell15.BorderWidth = 1;
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseBorderWidth = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "RentabilidadePeriodo";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.092972972972972967;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.Transparent;
            this.xrControlStyle2.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle2.BorderWidth = 1;
            this.xrControlStyle2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle2.Name = "xrControlStyle2";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrTable1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 230F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 5F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1482F, 80F);
            this.xrTable5.StylePriority.UseBackColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CabecalhoCotaPatrimonioBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.23255813953488372;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.CanGrow = false;
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = "CotaDataInicio";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 0.17139001349527666;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.CanGrow = false;
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell46.Weight = 0.29959514170040485;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "CotaDataFim";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 0.17543859649122809;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell48.Weight = 0.35357624831309042;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.23255813953488369;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "PLDataInicio";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.17139001349527666;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.29959514170040485;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "PLDataFim";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell52.Weight = 0.17543859649122809;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 0.35357624831309042;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 21F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1850F, 15F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 1;
            // 
            // xrControlStyle3
            // 
            this.xrControlStyle3.Name = "xrControlStyle3";
            this.xrControlStyle3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportQuadroRetorno
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.GroupHeader1,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2,
            this.xrControlStyle3});
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportQuadroRetorno.ResourceManager;
        }

        #region Funções Internas do Relatorio

        private void CabecalhoCotaPatrimonioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable cabecalho = sender as XRTable;

            HistoricoCota hDataInicio = new HistoricoCota();
            HistoricoCota hDataFim = new HistoricoCota();
            //
            decimal? cotaInicio = null, PLInicio = null;
            if (hDataInicio.LoadByPrimaryKey(this.dataSaldoAnterior, this.idCarteira))
            {
                cotaInicio = hDataInicio.CotaFechamento.Value;
                PLInicio = hDataInicio.PLFechamento.Value;
            }

            decimal? cotaFim = null, PLFim = null;
            if (hDataFim.LoadByPrimaryKey(this.dataFim, this.idCarteira))
            {
                cotaFim = hDataFim.CotaFechamento.Value;
                PLFim = hDataFim.PLFechamento.Value;
            }

            /* Cabeçalho*/
            #region Cabecalho
            XRTableRow cabecalho1 = cabecalho.Rows[0]; // 1 Linha
            //
            ((XRTableCell)cabecalho1.Cells[0]).Text = "Cota em " + this.dataSaldoAnterior.ToString("d");
            ((XRTableCell)cabecalho1.Cells[1]).Text = cotaInicio.HasValue ? cotaInicio.Value.ToString("n8") : "";
            ((XRTableCell)cabecalho1.Cells[2]).Text = "Cota em " + this.dataFim.ToString("d");
            ((XRTableCell)cabecalho1.Cells[3]).Text = cotaFim.HasValue ? cotaFim.Value.ToString("n8") : "";
            //
            //
            XRTableRow cabecalho2 = cabecalho.Rows[1]; // 2 Linha
            //
            ((XRTableCell)cabecalho2.Cells[0]).Text = "PL em " + this.dataSaldoAnterior.ToString("d");
            ((XRTableCell)cabecalho2.Cells[1]).Text = PLInicio.HasValue ? PLInicio.Value.ToString("n2") : "";
            ((XRTableCell)cabecalho2.Cells[2]).Text = "PL em " + this.dataFim.ToString("d");
            ((XRTableCell)cabecalho2.Cells[3]).Text = PLFim.HasValue ? PLFim.Value.ToString("n2") : "";
            #endregion
        }

        private void RentabilidadeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableRow xrTableRowRentabilidade = sender as XRTableRow;

            #region Zera os campos da Table
            ((XRTableCell)xrTableRowRentabilidade.Cells[0]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[1]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[2]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[3]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[4]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[5]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[6]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[7]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[8]).Text = "";
            #endregion

            #region Carrega Dados da Carteira
            Carteira carteira = new Carteira();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.IdCarteira);
            //            
            carteira.QueryReset();
            carteira.LoadByPrimaryKey(campos, this.idCarteira);
            #endregion

            #region Calcula datas dos periodos
            DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(this.dataFim, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(this.dataFim, -1);
            DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 6);
            DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 12);
            DateTime? data24Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 24);

            if (dataMes < this.dataInicioCota)
            {
                dataMes = null;
            }
            if (dataAno < this.dataInicioCota)
            {
                dataAno = null;
            }

            if (data6Meses < this.dataInicioCota)
            {
                data6Meses = null;
            }

            if (data12Meses < this.dataInicioCota)
            {
                data12Meses = null;
            }

            if (data24Meses < this.dataInicioCota)
            {
                data24Meses = null;
            }
            #endregion

            /*
             * Rentabilidade Carteira
             * 0-(Dia) / 1-(Mês) / 2-(Ano) / 3-(6 Meses) / 4-(12 Meses) / 5-(24 Meses)/ 6-(36 Meses)/ 
             * 7-(2007) / 8-(2008) / 9-(Periodo)
             *
            */
            List<decimal?> rentabilidadeCarteira = new List<decimal?>();
            //
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetDataInicio(this.dataInicioCota);
            //                                     

            #region Calcula Rentabilidade Carteira

            calculoMedida.SetIdCarteira(carteira.IdCarteira.Value);

            #region rentabilidadeMes
            decimal? rentabilidadeMesNomimal = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeMesNomimal = calculoMedida.CalculaRetornoMes(this.dataFim);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeAnoNomimal = null;
            if (dataAno != null)
            {
                try
                {
                    rentabilidadeAnoNomimal = calculoMedida.CalculaRetornoAno(this.dataFim);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidade6MesesNomimal = null;
            if (data6Meses != null)
            {
                try
                {
                    rentabilidade6MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataFim, 6);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidade12MesesNomimal = null;
            if (data12Meses != null)
            {
                try
                {
                    rentabilidade12MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataFim, 12);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade24Meses
            decimal? rentabilidade24MesesNomimal = null;
            if (data24Meses != null)
            {
                try
                {
                    rentabilidade24MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataFim, 24);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadePeriodo
            decimal? rentabilidadePeriodo = null;
            try
            {
                rentabilidadePeriodo = calculoMedida.CalculaRetorno(this.dataInicioCota, this.dataFim);
            }
            catch (HistoricoCotaNaoCadastradoException) { }
            #endregion

            #region rentabilidadeAno1
            decimal? rentCarteiraAno1 = null;
            int ano1 = this.dataFim.Year - 2;
            try
            {
                rentCarteiraAno1 = calculoMedida.CalculaRetornoAnoFechado(new DateTime(ano1, 12, 31));
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            #endregion

            #region rentabilidadeAno2
            decimal? rentCarteiraAno2 = null;
            int ano2 = this.dataFim.Year - 1;
            try
            {
                rentCarteiraAno2 = calculoMedida.CalculaRetornoAnoFechado(new DateTime(ano2, 12, 31));
            }
            catch (HistoricoCotaNaoCadastradoException) { }
            #endregion

            rentabilidadeCarteira.Add(rentabilidadeMesNomimal);
            rentabilidadeCarteira.Add(rentabilidadeAnoNomimal);
            rentabilidadeCarteira.Add(rentabilidade6MesesNomimal);
            rentabilidadeCarteira.Add(rentabilidade12MesesNomimal);
            rentabilidadeCarteira.Add(rentabilidade24MesesNomimal);
            rentabilidadeCarteira.Add(rentCarteiraAno1);
            rentabilidadeCarteira.Add(rentCarteiraAno2);
            rentabilidadeCarteira.Add(rentabilidadePeriodo);

            /* Exibe Retorno Carteira */
            #region Exibe Retorno Carteira

            //
            ((XRTableCell)xrTableRowRentabilidade.Cells[0]).Text = carteira.str.Nome;
            //
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[1], rentabilidadeCarteira[0], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[2], rentabilidadeCarteira[1], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[3], rentabilidadeCarteira[2], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[4], rentabilidadeCarteira[3], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[5], rentabilidadeCarteira[4], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[6], rentabilidadeCarteira[5], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[7], rentabilidadeCarteira[6], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[8], rentabilidadeCarteira[7], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            #endregion

            #endregion
        }

        private void RentabilidadeIndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableRow xrTableRowRentabilidade = sender as XRTableRow;

            short idIndice = Convert.ToInt16(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.IdIndice));
            string descricaoIndice = Convert.ToString(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.Descricao));

            #region Zera os campos da Table
            ((XRTableCell)xrTableRowRentabilidade.Cells[0]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[1]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[2]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[3]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[4]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[5]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[6]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[7]).Text = "";
            ((XRTableCell)xrTableRowRentabilidade.Cells[8]).Text = "";
            #endregion

            #region Calcula datas dos periodos
            DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(this.dataFim, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(this.dataFim, -1);
            DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 6);
            DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 12);
            DateTime? data24Meses = Calendario.RetornaDiaMesAnterior(this.dataFim, 24);

            int ano1 = this.dataFim.Year - 2;
            DateTime? dataAno1 = new DateTime(ano1, 12, 31);

            int ano2 = this.dataFim.Year - 1;
            DateTime? dataAno2 = new DateTime(ano2, 12, 31);

            if (dataMes < this.dataInicioCota)
            {
                dataMes = null;
            }
            if (dataAno < this.dataInicioCota)
            {
                dataAno = null;
            }

            if (data6Meses < this.dataInicioCota)
            {
                data6Meses = null;
            }

            if (data12Meses < this.dataInicioCota)
            {
                data12Meses = null;
            }

            if (data24Meses < this.dataInicioCota)
            {
                data24Meses = null;
            }

            if (dataAno1 < this.dataInicioCota)
            {
                dataAno1 = null;
            }

            if (dataAno2 < this.dataInicioCota)
            {
                dataAno2 = null;
            }
            #endregion

            /*
             * Rentabilidade Indice
             * 0-(Dia) / 1-(Mês) / 2-(Ano) / 3-(6 Meses) / 4-(12 Meses) / 5-(24 Meses)/ 6-(36 Meses)/ 
             * 7-(2007) / 8-(2008) / 9-(Periodo)
             *
            */
            List<decimal?> rentabilidadeIndice = new List<decimal?>();
            //
            CalculoMedida calculoMedida = new CalculoMedida(this.idCarteira, (int)idIndice);
            //                                     

            #region Calcula Rentabilidade Indice

            #region rentabilidadeMes
            decimal? rentabilidadeMes = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeMes = calculoMedida.CalculaRetornoMesIndice(this.dataFim);
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeAno = null;
            if (dataAno != null)
            {
                try
                {
                    rentabilidadeAno = calculoMedida.CalculaRetornoAnoIndice(this.dataFim);
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidade6Meses = null;
            if (data6Meses != null)
            {
                try
                {
                    rentabilidade6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataFim, 6);
                    //rentabilidade6Meses = rentabilidade6Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidade12Meses = null;
            if (data12Meses != null)
            {
                try
                {
                    rentabilidade12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataFim, 12);

                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade24Meses
            decimal? rentabilidade24Meses = null;
            if (data24Meses != null)
            {
                try
                {
                    rentabilidade24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataFim, 24);
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            DateTime dataInicioCota = carteira.DataInicioCota.Value;

            #region rentabilidadeAno1
            decimal? rentabilidadeIndiceAno1 = null;
            if (dataAno1 != null)
            {
                try
                {
                    rentabilidadeIndiceAno1 = calculoMedida.CalculaRetornoAnoFechadoIndice(dataAno1.Value);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno2
            decimal? rentabilidadeIndiceAno2 = null;
            if (dataAno2 != null)
            {
                try
                {
                    rentabilidadeIndiceAno2 = calculoMedida.CalculaRetornoAnoFechadoIndice(dataAno2.Value);
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadePeriodo
            decimal? rentabilidadePeriodo = null;
            try
            {
                rentabilidadePeriodo = calculoMedida.CalculaRetornoIndice(dataInicioCota, this.dataFim);
            }
            catch (CotacaoIndiceNaoCadastradoException) { }
            #endregion

            rentabilidadeIndice.Add(rentabilidadeMes);
            rentabilidadeIndice.Add(rentabilidadeAno);
            rentabilidadeIndice.Add(rentabilidade6Meses);
            rentabilidadeIndice.Add(rentabilidade12Meses);
            rentabilidadeIndice.Add(rentabilidade24Meses);
            rentabilidadeIndice.Add(rentabilidadeIndiceAno1);
            rentabilidadeIndice.Add(rentabilidadeIndiceAno2);
            rentabilidadeIndice.Add(rentabilidadePeriodo);

            /* Exibe Retorno Carteira */
            #region Exibe Retorno Carteira
            //
            ((XRTableCell)xrTableRowRentabilidade.Cells[0]).Text = descricaoIndice;

            //            
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[1], rentabilidadeIndice[0], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[2], rentabilidadeIndice[1], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[3], rentabilidadeIndice[2], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[4], rentabilidadeIndice[3], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[5], rentabilidadeIndice[4], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[6], rentabilidadeIndice[5], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[7], rentabilidadeIndice[6], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRowRentabilidade.Cells[8], rentabilidadeIndice[7], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //
            #endregion

            #endregion
        }

        #endregion

        private void Ano1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell ano1TableCell = sender as XRTableCell;
            ano1TableCell.Text = "";

            int ano = this.dataFim.Year - 2;
            ano1TableCell.Text = ano.ToString();
        }

        private void Ano2BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell ano2TableCell = sender as XRTableCell;
            ano2TableCell.Text = "";

            int ano = this.dataFim.Year - 1;
            ano2TableCell.Text = ano.ToString();
        }
    }
}