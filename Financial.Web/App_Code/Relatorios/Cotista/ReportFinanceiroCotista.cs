﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.InvestidorCotista;
using Financial.CRM.Enums;
using System.Globalization;
using Financial.Util;
using System.Text;
using Financial.Investidor;
using System.Collections.Generic;
using log4net;
using Financial.Fundo;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for FinanceiroCotista
    /// </summary>
    public class ReportFinanceiroCotista : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportFinanceiroCotista));

        public enum TipoOrdenacao {
            PorCodigo = 0,
            PorNome = 1
        }

        private TipoOrdenacao tipoOrdenacao;
        //
        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private decimal valor;

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }

        private int numeroLinhasDataTable;

        // Define de consulta será histórica ou não
        private enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoCotista;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRPageInfo xrPageInfo1;
        private XRPanel xrPanel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCellValorCotaAplicacao;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCellDataAplicacao;
        private XRTableCell xrTableCell34;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTable xrTable9;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow11;
        private XRTable xrTable11;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private CalculatedField calculatedFieldParticipacao;
        private XRPageInfo xrPageInfo3;
        private PosicaoCotistaCollection posicaoCotistaCollection1;
        private PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection1;
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport3;
        private SubReportLogotipo subReportLogotipo1;
        private SubReportRodape subReportRodape1;
        private CalculatedField calculatedFieldValorDistribuir;
        private XRSubreport xrSubreport2;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="valor">Valor A distribuir</param>
        /// <param name="tipoOrdenacao"></param>
        public ReportFinanceiroCotista(int idCarteira, DateTime data, decimal valor, TipoOrdenacao tipoOrdenacao) {
            this.idCarteira = idCarteira;
            this.data = data;
            this.valor = valor;
            this.tipoOrdenacao = tipoOrdenacao;
            //
            this.InitializeComponent();
            this.PersonalInitialize();
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1850);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport
                this.xrSubreport2.Visible = true;
                //
                this.xrTable9.Visible = false;
                this.xrTable7.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;         
        }

        private DataView FillDados() {
            // Define se faz Consulta PosicaoCustodia ou PosicaoCustodiaHistorico
            /* Obs: idCarteira e data são obrigatorios */
            
            // Verifica A dataDia Do Cliente            
            Cliente cliente = new Cliente();
            //
            this.tipoPesquisa = cliente.IsClienteNaData(this.IdCarteira, this.data)
                                ? TipoPesquisa.PosicaoCotista
                                : TipoPesquisa.PosicaoCotistaHistorico;

            #region Busca ValorPL
            HistoricoCota h = new HistoricoCota();
            h.BuscaValorPatrimonioDia(this.idCarteira, this.data);
            decimal valorPL = 1.0M;
            if(h.es.HasData) {
                valorPL = h.PLFechamento.Value;
            }
            #endregion

            // Formato Decimal com "." Sem esse formato, quando o 
            // Browser está em portugues o numero decimal fica com ","
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";

            if (tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                #region PosicaoCotista
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
                CotistaQuery cotistaQuery = new CotistaQuery("C");
               
                posicaoCotistaQuery.Select(posicaoCotistaQuery.IdCotista,
                                           cotistaQuery.Nome,
                                           posicaoCotistaQuery.Quantidade.Sum(),
                                           posicaoCotistaQuery.ValorBruto.Sum().As("ValorBruto"),
                                           ( (posicaoCotistaQuery.ValorBruto * this.valor.ToString(provider)) / valorPL.ToString(provider) ).Sum().As("ValorDistribuir") );
                //
                posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
                //
                posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira == this.idCarteira,
                                          posicaoCotistaQuery.Quantidade.NotEqual(0));
                //
                posicaoCotistaQuery.GroupBy(posicaoCotistaQuery.IdCotista, cotistaQuery.Nome);

                this.posicaoCotistaCollection1.QueryReset();
                
                //try {
                    this.posicaoCotistaCollection1.Load(posicaoCotistaQuery);
                //}
                //catch (Exception) {
                //    Console.WriteLine(this.posicaoCotistaCollection1.Query.es.LastQuery);                    
                //}                               
                #endregion
            }
            else if (tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                #region PosicaoCotistaHistorico
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                CotistaQuery cotistaQuery = new CotistaQuery("C");
                                                                             
                posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.IdCotista,
                                           cotistaQuery.Nome,
                                           posicaoCotistaHistoricoQuery.Quantidade.Sum(),
                                           posicaoCotistaHistoricoQuery.ValorBruto.Sum().As("ValorBruto"),
                                           ( ( posicaoCotistaHistoricoQuery.ValorBruto * this.valor.ToString(provider) ) / valorPL.ToString(provider) ).Sum().As("ValorDistribuir"));
                //
                posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                //
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira == this.idCarteira &
                                                   posicaoCotistaHistoricoQuery.DataHistorico == this.data,
                                                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0));
                //
                posicaoCotistaHistoricoQuery.GroupBy(posicaoCotistaHistoricoQuery.IdCotista, cotistaQuery.Nome);

                this.posicaoCotistaHistoricoCollection1.QueryReset();

                //try {
                    this.posicaoCotistaHistoricoCollection1.Load(posicaoCotistaHistoricoQuery);
                //}
                //catch (Exception) {
                //    Console.WriteLine(this.posicaoCotistaHistoricoCollection1.Query.es.LastQuery);
                //}                                
                #endregion
            }

            return tipoPesquisa == TipoPesquisa.PosicaoCotista
                                   ? this.posicaoCotistaCollection1.LowLevelBind()
                                   : this.posicaoCotistaHistoricoCollection1.LowLevelBind();
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportFinanceiroCotista.resx"; 
        */
        private void InitializeComponent() {
            string resourceFileName = "ReportFinanceiroCotista.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellDataAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValorCotaAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.calculatedFieldParticipacao = new DevExpress.XtraReports.UI.CalculatedField();
            this.posicaoCotistaCollection1 = new Financial.InvestidorCotista.PosicaoCotistaCollection();
            this.posicaoCotistaHistoricoCollection1 = new Financial.InvestidorCotista.PosicaoCotistaHistoricoCollection();
            this.calculatedFieldValorDistribuir = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 53F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1700F, 53F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell20,
            this.xrTableCell21});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdCotista")});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.15235294117647058;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "NomeCotista";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.31176470588235294;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCotistaBeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade", "{0:n8}")});
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.19941176470588234;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto", "{0:n2}")});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.17411764705882352;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorDistribuir", "{0:n2}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "ValorDistribuir";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.16235294117647059;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrSubreport2,
            this.xrSubreport3,
            this.xrTable10,
            this.xrPanel1,
            this.xrTable7});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 390F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(1842F, 19F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(108F, 42F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 331F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.reportSemDados1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport2.Visible = false;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportLogotipo1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(720F, 64F);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(855F, 19F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(950F, 64F);
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "Informe Financeiro de Cotistas";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrTable11,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 101F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1804F, 212F);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(275F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(251.6458F, 42F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable11
            // 
            this.xrTable11.Dpi = 254F;
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(3F, 148F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(613F, 42F);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell56});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "ValorDistribuir: ";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.44861337683523655;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.CanGrow = false;
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.Text = "Valor";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell56.Weight = 0.5513866231647635;
            this.xrTableCell56.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorDistribuirBeforePrint);
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 103F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1593F, 42F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Carteira: ";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.17263025737602009;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Carteira";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.82736974262397989;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(593F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Data Posição: ";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.463743676222597;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.StylePriority.UseFont = false;
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.53625632377740307;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(275F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Data Emissão: ";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 331F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1700F, 53F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellDataAplicacao,
            this.xrTableCellValorCotaAplicacao,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell34});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCellDataAplicacao
            // 
            this.xrTableCellDataAplicacao.Dpi = 254F;
            this.xrTableCellDataAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataAplicacao.Name = "xrTableCellDataAplicacao";
            this.xrTableCellDataAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataAplicacao.StylePriority.UseFont = false;
            this.xrTableCellDataAplicacao.StylePriority.UseTextAlignment = false;
            this.xrTableCellDataAplicacao.Text = "Código";
            this.xrTableCellDataAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellDataAplicacao.Weight = 0.15235294117647058;
            // 
            // xrTableCellValorCotaAplicacao
            // 
            this.xrTableCellValorCotaAplicacao.Dpi = 254F;
            this.xrTableCellValorCotaAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellValorCotaAplicacao.Name = "xrTableCellValorCotaAplicacao";
            this.xrTableCellValorCotaAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellValorCotaAplicacao.StylePriority.UseFont = false;
            this.xrTableCellValorCotaAplicacao.StylePriority.UseTextAlignment = false;
            this.xrTableCellValorCotaAplicacao.Text = "Nome";
            this.xrTableCellValorCotaAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellValorCotaAplicacao.Weight = 0.31176470588235294;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Quantidade Cotas";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.19941176470588234;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.Text = "Saldo Financeiro";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.17294117647058824;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "Valor Distribuir";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.1635294117647059;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1863F, 21F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(84F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrPageInfo1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 71.00003F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRodape1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(381F, 64F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 82F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.BackColor = System.Drawing.Color.LightGray;
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable9.Dpi = 254F;
            this.xrTable9.KeepTogether = true;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable9.SizeF = new System.Drawing.SizeF(1700F, 53F);
            this.xrTable9.StylePriority.UseBackColor = false;
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 1;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.15235294117647058;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.31176470588235294;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.19941176470588234;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto")});
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.StylePriority.UseFont = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell60.Summary = xrSummary1;
            this.xrTableCell60.Text = "xrTableCell60";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.17411764705882352;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorDistribuir")});
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.StylePriority.UseFont = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell61.Summary = xrSummary2;
            this.xrTableCell61.Text = "xrTableCell61";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.16235294117647059;
            // 
            // calculatedFieldParticipacao
            // 
            this.calculatedFieldParticipacao.DataMember = "esUtility";
            this.calculatedFieldParticipacao.Expression = "[ValorLiquido]  / [Parameters.ParameterTotalValorLiquido]";
            this.calculatedFieldParticipacao.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldParticipacao.Name = "calculatedFieldParticipacao";
            // 
            // posicaoCotistaCollection1
            // 
            this.posicaoCotistaCollection1.AllowDelete = true;
            this.posicaoCotistaCollection1.AllowEdit = true;
            this.posicaoCotistaCollection1.AllowNew = true;
            this.posicaoCotistaCollection1.EnableHierarchicalBinding = true;
            this.posicaoCotistaCollection1.Filter = "";
            this.posicaoCotistaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoCotistaCollection1.Sort = "";
            // 
            // posicaoCotistaHistoricoCollection1
            // 
            this.posicaoCotistaHistoricoCollection1.AllowDelete = true;
            this.posicaoCotistaHistoricoCollection1.AllowEdit = true;
            this.posicaoCotistaHistoricoCollection1.AllowNew = true;
            this.posicaoCotistaHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoCotistaHistoricoCollection1.Filter = "";
            this.posicaoCotistaHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoCotistaHistoricoCollection1.Sort = "";
            // 
            // calculatedFieldValorDistribuir
            // 
            this.calculatedFieldValorDistribuir.DataSource = this.posicaoCotistaCollection1;
            this.calculatedFieldValorDistribuir.Expression = "[ValorDistribuir]";
            this.calculatedFieldValorDistribuir.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldValorDistribuir.Name = "calculatedFieldValorDistribuir";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportFinanceiroCotista
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldValorDistribuir});
            this.DataSource = this.posicaoCotistaCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RelatorioBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportFinanceiroCotista.ResourceManager;

        }

        #region Funções Internas do Relatorio
        //        
        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeCarteira = sender as XRTableCell;
            //
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCarteira);
            //
            nomeCarteira.Text = this.idCarteira + " - " + carteira.Nome;                       
        }

        private void ValorDistribuirBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorDistribuirXRTableCell = sender as XRTableCell;
            valorDistribuirXRTableCell.Text = this.valor.ToString("n2");
        }

        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.data.ToString("d");
        }
        
        private void RelatorioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            #region Define a Ordenação do Relatorio
            if (this.tipoOrdenacao == TipoOrdenacao.PorCodigo) {                               
                this.Detail.SortFields.Add(new GroupField("IdCotista", XRColumnSortOrder.Ascending));
            }
            else if (this.tipoOrdenacao == TipoOrdenacao.PorNome) {
                this.Detail.SortFields.Add(new GroupField("Nome", XRColumnSortOrder.Ascending));
            }
            #endregion                       
        }

        private void NomeCotistaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeCotistaXRTableCell = sender as XRTableCell;

            int idCotista = (int)GetCurrentColumnValue(PosicaoCotistaMetadata.ColumnNames.IdCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            string codigoInterface = "";
            if (!String.IsNullOrEmpty(Convert.ToString(cotista.CodigoInterface)))
            {
                codigoInterface = Convert.ToString(cotista.CodigoInterface);
            }

            string nomeCotista = (string)this.GetCurrentColumnValue(CotistaMetadata.ColumnNames.Nome);
            string titulo = nomeCotista.Trim();

            if (codigoInterface != "")
            {
                titulo = titulo + " (" + codigoInterface + ") ";
            }

            nomeCotistaXRTableCell.Text = titulo;
        }

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);

        }
        #endregion
        
        #endregion
    }
}
