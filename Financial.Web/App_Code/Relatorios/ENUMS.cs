﻿using System;
using Financial.Util;

namespace Financial.Relatorio.Enums
{

    public enum MenuId
    {
        Relatorios = 15000
    }

    public enum TipoReportExtratoCliente
    {
        [StringValue("Retorno Carteira")]
        Tabela_RetornoCarteira = 1,

        Grafico_RetornoAcumulado = 2,

        Grafico_EvolucaoPL = 3,

        Grafico_Retorno12meses = 4,

        [StringValue("Risco Retorno")]
        Tabela_RiscoRetorno = 5,

        Grafico_Volatilidade = 6, //Simples, mas não fazer agora!

        Grafico_RiscoRetorno = 7, //NÃO FAZER POR ORA

        Grafico_AlocacaoGestor = 8,

        Grafico_AlocacaoDireta = 9, //Simples, mas não fazer agora!

        [StringValue("Posição Ativos")]
        Tabela_PosicaoAtivos = 10,

        Grafico_Liquidez = 11,

        Grafico_AlocacaoEstrategia = 12,

        [StringValue("Conta Corrente")]
        Tabela_ContaCorrente = 13, 

        [StringValue("Movimento Período")]
        Tabela_MovimentoPeriodo = 14,

        [StringValue("Retorno Ativos")]
        Tabela_RetornoAtivos = 15,

        [StringValue("Retorno Estratégias")]
        Tabela_RetornoEstrategias = 16,

        [StringValue("Resultado Estratégias")]
        Tabela_ResultadoEstrategias = 17,

        Grafico_DistribuicaoRetorno = 18, //NÃO FAZER POR ORA

        [StringValue("Posição Aplicações")]
        Tabela_PosicaoAplicacoes = 19,

        [StringValue("Retorno Carteira Resumido")]
        Tabela_RetornoCarteiraResumido = 20,

        Grafico_AlocacaoAtivo = 21,

        [StringValue("Retorno Estratégias Benchmark")]
        Tabela_RetornoEstrategiasBenchmark = 22,

        [StringValue("Retorno Estratégias Líquido")]
        Tabela_RetornoEstrategiasLiquido = 23,

        [StringValue("Resultado Estratégias Líquido")]
        Tabela_ResultadoEstrategiasLiquido = 24,

        [StringValue("Retorno Estratégias Benchmark Líquido")]
        Tabela_RetornoEstrategiasBenchmarkLiquido = 25,

        [StringValue("Posição Ativo Explodida")]
        Tabela_PosicaoAtivosExplodida = 26,

        [StringValue("Retorno Ativos Explodidos")]
        Tabela_RetornoAtivosExplodidos = 27,

        [StringValue("Frequëncia Retornos")]
        Grafico_FrequenciaRetornos = 30,

        [StringValue("Retorno Comparativo")]
        Grafico_RetornoComparativo = 31,

        [StringValue("Risco Retorno 2")]
        Tabela_RiscoRetorno2 = 32,

        [StringValue("Grafico Liquidez 2")]
        Grafico_Liquidez2 = 33,

        [StringValue("Movimentação")]
        Tabela_Movimentacao = 34,

        [StringValue("Retorno Índices")]
        Tabela_RetornoIndices = 35,

        [StringValue("Gráfico Retorno Acumulado2")]
        Grafico_RetornoAcumulado2 = 36,

        [StringValue("Gráfico Alocação Estratégia Explodida")]
        Grafico_AlocacaoEstrategiaExplodida = 37,

        [StringValue("Gráfico Retorno Acumulado3")]
        Grafico_RetornoAcumulado3 = 38,

        [StringValue("Alocação Ativo 2")]
        Grafico_AlocacaoAtivo2 = 39,

        [StringValue("Posição Ativo 2")]
        Tabela_PosicaoAtivos2 = 40,

        [StringValue("Detalhamento Fundo")]
        Detalhamento_Fundo = 41,

        [StringValue("Espaço em Branco")]
        Espaço_Branco = 999,

        [StringValue("Espaço em Branco")]
        Espaço_Branco1 = 9991,

        [StringValue("Espaço em Branco")]
        Espaço_Branco2 = 9992,

        [StringValue("Espaço em Branco")]
        Espaço_Branco3 = 9993,

        [StringValue("Espaço em Branco")]
        Espaço_Branco4 = 9994,

        [StringValue("Observação")]
        Observacao = 99,

        Logotipo_Rodape = 100,

        /*********************************************************
        /* Usados para controlar combinação de pares de relatórios 
         * - Esses números não aparecem na tabelaExtrato. São gerados dois registros
         * 
         */
        [StringValue("Gráfico Retorno Acumulado/Evolução PL")]
        Grafico_RetornoAcumulado_EvolucaoPL = 200,       // Combinação 2 e 3

        [StringValue("Gráfico Retorno 12 Meses/Liquidez")]
        Grafico_Retorno12Meses_Liquidez = 201,           // Combinação 4 e 11

        [StringValue("Gráfico Alocação Gestor/Estratégia")]
        Grafico_AlocacaoGestor_AlocacaoEstrategia = 202,  // Combinação 8 e 12

        [StringValue("Gráfico Alocação Gestor/Ativo")]
        Grafico_AlocacaoGestor_AlocacaoAtivo = 203,  // Combinação 8 e 21

        [StringValue("Gráfico Alocação Estratégia/Ativo")]
        Grafico_AlocacaoEstrategia_AlocacaoAtivo = 204,  // Combinação 12 e 21


        /**********************************************************************
        /* Graficos Isolados */ 
        /* Esses números são o que aparecem na tabelaExtrato
           Caso aparece os numeros 8, 12 e 21 eles serão usados para relatórios Duplos
         */
        [StringValue("Gráfico Alocação Estratégia")]
        /* Devido ao id=12 já estar sendo usado de maneira combinada é necessário outro id para essa opção */
        Somente_Grafico_AlocacaoEstrategia = 400,

        [StringValue("Gráfico Alocação Ativo")]
        /* Devido ao id=21 já estar sendo usado de maneira combinada é necessário outro id para essa opção */
        Somente_Grafico_AlocacaoAtivo = 401,

        [StringValue("Gráfico Genérico")]
        /* o item 402 deve ser mantido no enum */
        Somente_Grafico_Generico = 402        
        
    }


    public enum TipoResultadoEstrategia {
        Normal = 1,
        Liquido = 2
    }

    public enum TipoRetornoEstrategia {
        Normal = 1,
        Liquido = 2
    }

    public enum TipoRetornoEstrategiaBenchMark {
        Normal = 1,
        Liquido = 2
    }
}