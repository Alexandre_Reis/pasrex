﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using Financial.Security;
using Financial.Investidor.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportResultadoConsolidado
    /// </summary>
    public class ReportResultadoConsolidado : XtraReport {

        private DateTime dataReferencia;
        private int? idCarteira;
        // "S" ou "N"
        private string enquadrado;
        private int? tipo;
        private bool mostraDefasado;

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private SubReportLogotipo subReportLogotipo1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell10;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTableCell xrTableCell16;
        private GroupFooterBand GroupFooter1;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell1;
        private XRTableRow xrTableRow1;
        private XRTable xrTable1;
        private XRTableCell xrTableCellDataInicio;
        private XRTableCell xrTableCell4;
        private XRTableRow xrTableRow2;
        private XRTable xrTable2;
        private XRPageInfo xrPageInfo3;
        private XRPanel xrPanel1;
        private XRTableCell xrTableCell55;
        private XRTableRow xrTableRow10;
        private XRTable xrTable10;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell38;
        private XRTableRow xrTableRow7;
        private XRTable xrTable7;
        private ReportSemDados reportSemDados1;
        private PageHeaderBand PageHeader;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ResultadoConsolidado() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2450);

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        private void ResultadoConsolidado(DateTime dataReferencia) {
            this.dataReferencia = dataReferencia;
            //
            this.ResultadoConsolidado();
        }
        #endregion
                          
        #region Construtores Publics
        public ReportResultadoConsolidado(DateTime dataReferencia) {
            this.ResultadoConsolidado(dataReferencia);
        }

        public ReportResultadoConsolidado(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;
            this.ResultadoConsolidado(dataReferencia);
        }

        public ReportResultadoConsolidado(DateTime dataReferencia, string enquadrado) {
            this.enquadrado = enquadrado;
            this.ResultadoConsolidado(dataReferencia);
        }

        public ReportResultadoConsolidado(int idCarteira, DateTime dataReferencia, string enquadrado) 
        {
            this.tipo = null;
            this.idCarteira = idCarteira;
            this.enquadrado = enquadrado;
            this.ResultadoConsolidado(dataReferencia);
        }

        public ReportResultadoConsolidado(int? idCarteira, DateTime dataReferencia, string enquadrado) 
        {
            this.tipo = null;
            this.idCarteira = idCarteira;
            this.enquadrado = enquadrado;
            this.ResultadoConsolidado(dataReferencia);
        }

        public ReportResultadoConsolidado(int? idCarteira, DateTime dataReferencia, string enquadrado, int? tipo, bool mostraDefasado)
        {
            this.tipo = tipo;
            this.idCarteira = idCarteira;
            this.enquadrado = enquadrado;
            this.mostraDefasado = mostraDefasado;
            this.ResultadoConsolidado(dataReferencia);
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.GroupHeader1.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.ReportResultadoConsolidado._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportResultadoConsolidado._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportResultadoConsolidado._DataPosicao;
            this.xrTableCell38.Text = Resources.ReportResultadoConsolidado._TipoRegra;
            this.xrTableCell9.Text = Resources.ReportResultadoConsolidado._DescricaoRegra;
            this.xrTableCell25.Text = Resources.ReportResultadoConsolidado._Status;
            this.xrTableCell28.Text = Resources.ReportResultadoConsolidado._Tipo;
            this.xrTableCell26.Text = Resources.ReportResultadoConsolidado._Minimo;
            this.xrTableCell5.Text = Resources.ReportResultadoConsolidado._Maximo;
            this.xrTableCell3.Text = Resources.ReportResultadoConsolidado.__Exposicao;
            this.xrTableCell35.Text = Resources.ReportResultadoConsolidado._ValorBase;
            this.xrTableCell7.Text = Resources.ReportResultadoConsolidado._ValorExposicao;
            #endregion
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();
            //
            esParameters esParams = new esParameters();
            esParams.Add("DataReferencia", this.dataReferencia);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" Select E.IdRegra, R.ValorCriterio, R.ValorBase, R.Resultado, R.Enquadrado, ");
            sqlText.AppendLine("        E.Descricao, E.TipoEnquadramento, E.TipoRegra, E.Minimo, E.Maximo, ");
            sqlText.AppendLine("        C.IdCarteira, C.Apelido ");
            sqlText.AppendLine(" From   EnquadraResultado R, ");
            sqlText.AppendLine("        EnquadraRegra E, ");
            sqlText.AppendLine("        Carteira C, ");
            sqlText.AppendLine("       [PermissaoCliente] P, ");
            sqlText.AppendLine("       [Usuario] U ");
            sqlText.AppendLine(" Where  R.IdRegra = E.IdRegra ");
            sqlText.AppendLine("        AND R.IdCarteira = C.IdCarteira ");
            sqlText.AppendLine("        AND R.Data = @DataReferencia ");
            sqlText.AppendLine("       AND P.idCliente = C.idCarteira ");
            sqlText.AppendLine("       AND P.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND U.login = '" + HttpContext.Current.User.Identity.Name + "'");
                                    
            if (!String.IsNullOrEmpty(this.enquadrado)) {
                sqlText.AppendLine("    AND R.Enquadrado = '" + this.enquadrado + "' ");
            }

            if (this.tipo.HasValue)
            {
                sqlText.AppendLine("    AND E.TipoEnquadramento = " + this.tipo.Value);
            }

            if (this.idCarteira.HasValue) {
                sqlText.AppendLine("    AND R.IdCarteira = " + this.idCarteira);
            }
            sqlText.AppendLine(" ORDER BY E.TipoRegra, E.Descricao ");
            #endregion
            //
            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            if (this.mostraDefasado) {
                ClienteQuery clienteQuery = new ClienteQuery("C");
                EnquadraRegraQuery enquadraRegraQuery = new EnquadraRegraQuery("R");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");
                PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

                clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido, clienteQuery.DataDia);
                clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(enquadraRegraQuery).On(enquadraRegraQuery.IdCarteira == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
                clienteQuery.Where(usuarioQuery.Login == HttpContext.Current.User.Identity.Name &                                   
                                   clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo) &
                                   (clienteQuery.DataDia.LessThan(this.dataReferencia) |
                                    clienteQuery.DataDia.LessThan(this.dataReferencia) & clienteQuery.Status.In((byte)StatusCliente.Aberto, (byte)StatusCliente.FechadoComErro)));
                clienteQuery.es.Distinct = true;

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                for (int i = 0; i < clienteCollection.Count; i++) {
                    DataRow dr = dt.NewRow();
                    //                            
                    dr["IdRegra"] = 0;
                    dr["ValorCriterio"] = 0;
                    dr["ValorBase"] = 0;
                    dr["Resultado"] = 0;                    

                    //dr["TipoEnquadramento"] = DBNull.Value;
                    //dr["TipoRegra"] = DBNull.Value;
                    dr["Minimo"] = DBNull.Value;
                    dr["Maximo"] = DBNull.Value;
                    dr["TipoEnquadramento"] = 1;
                    dr["TipoRegra"] = 100;

                    dr["Enquadrado"] = "N";
                    dr["Descricao"] = "CARTEIRA DEFASADA EM " + clienteCollection[i].DataDia.Value.ToShortDateString();
                    dr["IdCarteira"] = clienteCollection[i].IdCliente.Value;
                    dr["Apelido"] = clienteCollection[i].Apelido.Trim();
                    //
                    dt.Rows.Add(dr);
                }
            }

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Enquadra/Xml/ReportResultadoConsolidado.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Enquadra/ReportResultadoConsolidado.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportResultadoConsolidado.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 46F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DataOperacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdOperacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell10,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell36,
            this.xrTableCell16});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "TipoRegra";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.1489795918367347;
            this.xrTableCell39.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoRegraBeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Descricao")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.21591836734693878;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "Status";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.095510204081632646;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.StatusBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "TipoEnquadramento";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.068979591836734688;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoEnquadramentoBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Minimo", "{0:n2}")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.068979591836734688;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MinimoBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Maximo", "{0:n2}")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.077959183673469393;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MaximoBeforePrint);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Resultado", "{0:n2}")});
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.077551020408163265;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ResultadoBeforePrint);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "ValorBase";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.12938775510204081;
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorBaseBeforePrint);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "ValorExposicao";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.11673469387755102;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorExposicaoBeforePrint);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.KeepTogether = true;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Apelido", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2450F, 48F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "Carteira";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 21F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataReferencia";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.66614173228346452;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrTable1,
            this.xrTable2,
            this.xrPageInfo3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 106F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(365F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(1058F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1524F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#ValorExposicao";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell7.Weight = 0.11673469387755102;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "#ValorBase";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.12938775510204081;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#%Exposicao";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.077551020408163265;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#Maximo";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.077959183673469393;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#Minimo";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.068979591836734688;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#Tipo";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell28.Weight = 0.068979591836734688;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Status";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.095510204081632646;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#DescricaoRegra";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.21591836734693878;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "#TipoRegra";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.1489795918367347;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell35,
            this.xrTableCell7});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 215F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2450F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrTable10,
            this.xrPanel1,
            this.xrSubreport3});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 264.4792F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2484F, 169F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(64F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(400.42F, 100F);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(48.81251F, 191.5833F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.Visible = false;
            // 
            // ReportResultadoConsolidado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        
        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportResultadoConsolidado.ResourceManager;            
        }
        
        #region Variaveis Internas do Relatorio
        #endregion

        #region Funções Internas do Relatorio
        //
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaXRTableCell = sender as XRTableCell;
            dataReferenciaXRTableCell.Text = this.dataReferencia.ToString("d");
        }
       
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell carteiraXRTableCell = sender as XRTableCell;
            carteiraXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {               
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                string apelido = (string)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.Apelido);
                string nomeCarteira = idCarteira.ToString() + " - " + apelido;
                //
                carteiraXRTableCell.Text = nomeCarteira;
            }
        }

        private void TipoRegraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoRegraXRTableCell = sender as XRTableCell;
            tipoRegraXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string tipoRegraString = "";
                int? tipoRegra = null;
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(EnquadraRegraMetadata.ColumnNames.TipoRegra))) {
                    tipoRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraRegraMetadata.ColumnNames.TipoRegra));
                }
                
                tipoRegraString = tipoRegra.HasValue 
                    ? TraducaoEnumsEnquadra.EnumTipoRegraEnquadra.TraduzEnum(tipoRegra.Value)
                    : "-";                
                //
                tipoRegraXRTableCell.Text = tipoRegraString;  
            }
        }

        private void StatusBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell statusXRTableCell = sender as XRTableCell;
            statusXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string status = Convert.ToString(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.Enquadrado));

                string statusString = status == "S"
                    ? Resources.ReportResultadoConsolidado._Enquadrado
                    : Resources.ReportResultadoConsolidado._Desenquadrado;
                //
                if (status == "N")
                {
                    statusXRTableCell.ForeColor = Color.Red;                    
                }
                else
                {
                    statusXRTableCell.ForeColor = Color.Black;                    
                }
                statusXRTableCell.Text = statusString;
            }
        }

        private void TipoEnquadramentoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoEnquadramentoXRTableCell = sender as XRTableCell;
            tipoEnquadramentoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                int tipoEnquadramento = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraRegraMetadata.ColumnNames.TipoEnquadramento));
                //
                string tipoEnquadramentoString = TraducaoEnumsEnquadra.EnumTipoEnquadramentoEnquadra.TraduzEnum(tipoEnquadramento);

                tipoEnquadramentoXRTableCell.Text = tipoEnquadramentoString;
            }
        }

        private void MinimoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell minimoXRTableCell = sender as XRTableCell;

            if (this.numeroLinhasDataTable != 0)
            {
                if (!String.IsNullOrEmpty(minimoXRTableCell.Text))
                {
                    decimal minimo = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraRegraMetadata.ColumnNames.Minimo));                    

                    int idRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.IdRegra));
                    EnquadraRegra enquadraRegra = new EnquadraRegra();
                    enquadraRegra.LoadByPrimaryKey(idRegra);

                    if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista)
                    {
                        ReportBase.ConfiguraSinalNegativo(minimoXRTableCell, minimo, false, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                    }
                    else if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioMensal || enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioAnual)
                    {
                        if (enquadraRegra.Minimo.HasValue)
                        {
                            decimal desvio = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorCriterio));
                            decimal media = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorBase));
                            
                            if (desvio != 9999999999 && media != 9999999999)
                            {
                                minimo = media - (desvio * minimo);
                            }

                            minimo = minimo / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                            ReportBase.ConfiguraSinalNegativo(minimoXRTableCell, minimo, false, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                        }                        
                    }
                    else
                    {
                        minimo = minimo / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                        ReportBase.ConfiguraSinalNegativo(minimoXRTableCell, minimo, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                    }
                }

            }
        }

        private void MaximoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell maximoXRTableCell = sender as XRTableCell;

            if (this.numeroLinhasDataTable != 0) 
            {                
                if (!String.IsNullOrEmpty(maximoXRTableCell.Text))
                {
                    decimal maximo = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraRegraMetadata.ColumnNames.Maximo));
                    
                    int idRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.IdRegra));
                    EnquadraRegra enquadraRegra = new EnquadraRegra();
                    enquadraRegra.LoadByPrimaryKey(idRegra);

                    if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista)
                    {
                        ReportBase.ConfiguraSinalNegativo(maximoXRTableCell, maximo, false, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                    }
                    else if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioMensal || enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioAnual)
                    {
                        if (enquadraRegra.Maximo.HasValue)
                        {
                            decimal desvio = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorCriterio));
                            decimal media = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorBase));
                            
                            if (desvio != 9999999999 && media != 9999999999)
                            {
                                maximo = media + (desvio * maximo);
                            }

                            maximo = maximo / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                            ReportBase.ConfiguraSinalNegativo(maximoXRTableCell, maximo, false, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                        }                        
                    }
                    else
                    {
                        maximo = maximo / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                        ReportBase.ConfiguraSinalNegativo(maximoXRTableCell, maximo, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                    }                
                }
                   
            }
            
        }

        private void ValorBaseBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorBaseXRTableCell = sender as XRTableCell;
            valorBaseXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal? valorBase = null;
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorBase))) {
                    valorBase = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorBase));
                }

                int idRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.IdRegra));
                EnquadraRegra enquadraRegra = new EnquadraRegra();
                enquadraRegra.LoadByPrimaryKey(idRegra);

                if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista ||
                    enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.PosicaoDescoberto ||
                    enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteOscilacao ||
                    enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.MediaMovel)
                {
                    valorBaseXRTableCell.Text = "-";
                }
                else
                {
                    ReportBase.ConfiguraSinalNegativo(valorBaseXRTableCell, valorBase, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                }
            }
        }

        private void ValorExposicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorCriterioXRTableCell = sender as XRTableCell;
            valorCriterioXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal valorCriterio = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.ValorCriterio));
                //

                int idRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.IdRegra));
                EnquadraRegra enquadraRegra = new EnquadraRegra();
                enquadraRegra.LoadByPrimaryKey(idRegra);

                if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista)
                {
                    ReportBase.ConfiguraSinalNegativo(valorCriterioXRTableCell, valorCriterio, false, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                }
                else if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteOscilacao)
                {
                    valorCriterio = valorCriterio / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                    ReportBase.ConfiguraSinalNegativo(valorCriterioXRTableCell, valorCriterio, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                }
                else if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.MediaMovel)
                {
                    valorCriterioXRTableCell.Text = "-";
                }
                else
                {
                    ReportBase.ConfiguraSinalNegativo(valorCriterioXRTableCell, valorCriterio, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                }
            }
        }

        private void ResultadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell resultadoXRTableCell = sender as XRTableCell;
            resultadoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0)
            {
                decimal resultado = 0;
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.Resultado)))
                {
                    resultado = Convert.ToDecimal(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.Resultado));
                    resultado = resultado / 100; //Porque na funcao de ConfiguraSinalNegativo já vai multiplicar por 100
                }


                int idRegra = Convert.ToInt32(this.GetCurrentColumnValue(EnquadraResultadoMetadata.ColumnNames.IdRegra));
                EnquadraRegra enquadraRegra = new EnquadraRegra();
                enquadraRegra.LoadByPrimaryKey(idRegra);

                if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteOscilacao ||
                         enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista)
                {
                    resultadoXRTableCell.Text = "-";
                }
                else if (enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioMensal || enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteDesvioAnual)
                {
                    resultado = resultado * 100;
                    ReportBase.ConfiguraSinalNegativo(resultadoXRTableCell, resultado, false, ReportBase.NumeroCasasDecimais.QuatroCasasDecimais);
                }
                else
                {
                    ReportBase.ConfiguraSinalNegativo(resultadoXRTableCell, resultado, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                }
            }
        }
        #endregion

    }
}