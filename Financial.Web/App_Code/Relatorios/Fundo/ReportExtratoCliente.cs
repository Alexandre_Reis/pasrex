﻿using System;
using System.Drawing;
using DevExpress.XtraReports.UI;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio.Enums;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Fundo.Exceptions;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportExtratoCliente
    /// </summary>
    public class ReportExtratoCliente : XtraReport
    {
        private Cliente cliente;
        private Carteira carteira;
        private DateTime dataInicio;
        private CalculoMedida calculoMedida;
        private bool carteiraSimulada;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; } 
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int idCarteira;

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idIndice;

        public int IdIndice
        {
            get { return idIndice; }
            set { idIndice = value; }
        }

        /* Indica se Vai Mostar o Caixa no Relatorio */
        private bool mostraCaixa;

        private bool _explodeFundos;

        #region Disponibiliza Subreports para acesso Externo
        public SubReportPosicaoAtivo GetSubReportPosicaoAtivo
        {
            get { return this.subReportPosicaoAtivo1; }
        }

        public SubReportRiscoRetorno GetSubReportRiscoRetorno
        {
            get { return this.subReportRiscoRetorno1; }
        }

        public SubReportRetornoCarteira GetSubReportRetornoCarteira
        {
            get { return this.subReportRetornoCarteira1; }
        }

        public SubReportRetornoAtivos GetSubReportRetornoAtivos
        {
            get { return this.subReportRetornoAtivos1; }
        }
        #endregion

        /* Dicionario que Guarda os SubRelatorios e as coordenadas X,Y dos subrelatorios de 
         * um determinado Cliente - Chave é o IdSubRelatorio de acordo com o Enum TipoReportExtratoCliente */
        private Dictionary<int, Point> subRelatorios = new Dictionary<int, Point>();

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private PageHeaderBand PageHeader;

        // 20 Subreports
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private XRSubreport xrSubreport4;
        private XRSubreport xrSubreport5;
        private XRSubreport xrSubreport6;
        private XRSubreport xrSubreport7;
        private XRSubreport xrSubreport8;
        private XRSubreport xrSubreport9;
        private XRSubreport xrSubreport10;
        private XRSubreport xrSubreport11;
        private XRSubreport xrSubreport12;
        private XRSubreport xrSubreport13;
        private XRSubreport xrSubreport14;
        private XRSubreport xrSubreport15;
        private XRSubreport xrSubreport16;
        private XRSubreport xrSubreport17;
        private XRSubreport xrSubreport18;
        private XRSubreport xrSubreport19;
        private XRSubreport xrSubreport22;
        private XRSubreport xrSubreport24;
        private XRSubreport xrSubreport25;
        private XRSubreport xrSubreport26;
        private XRSubreport xrSubreport27;
        private XRSubreport xrSubreport28;
        private XRSubreport xrSubreport29;
        private XRSubreport xrSubreport30;
        private XRSubreport xrSubreport31;
        private XRSubreport xrSubreport32;
        private XRSubreport xrSubreport33;
        private XRSubreport xrSubreport34;
        private XRSubreport xrSubreport35;
        private XRSubreport xrSubreport36;

        private XRSubreport xrSubreport39;
        private XRSubreport xrSubreport40;
        private XRSubreport xrSubreport41;

        //
        private DetailReportBand DetailReport;
        private DetailBand Detail1;

        private SubReportGraficoEvolucaoPL subReportGraficoEvolucaoPL1;
        private SubReportGraficoRetornoAcumulado subReportGraficoRetornoAcumulado1;
        private SubReportPosicaoAtivoExplodido subReportPosicaoAtivoExplodido1;
        private SubReportGraficoRetornoAcumulado2 subReportGraficoRetornoAcumulado21;
        private SubReportGraficoRetornoAcumulado3 subReportGraficoRetornoAcumulado31;
        private SubReportPosicaoAtivo2 subReportPosicaoAtivo2;
        private SubReportDetalhamentoFundo subReportDetalhamentoFundo1;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo2;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCellDataReferencia;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell25;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRSubreport xrSubreport21;
        private XRSubreport xrSubreport20;
        private XRPageInfo xrPageInfo1;
        private PageFooterBand PageFooter;
        private SubReportRodape subReportRodape1;
        private SubReportLogotipo1 subReportLogotipo11;
        private SubReportGraficoAlocacaoEstrategiaExplodida subReportGraficoAlocacaoEstrategiaExplodida1;
        private SubReportGraficoAlocacaoGestor subReportGraficoAlocacaoGestor1;
        private SubReportGraficoLiquidez subReportGraficoLiquidez1;
        private SubReportGraficoLiquidez2 subReportGraficoLiquidez21;
        private SubReportGraficoAlocacaoEstrategia subReportGraficoAlocacaoEstrategia1;
        private SubReportGraficoRetorno12Meses subReportGraficoRetorno12Meses1;
        private SubReportRiscoRetorno subReportRiscoRetorno1;
        private SubReportRetornoCarteira subReportRetornoCarteira1;
        private SubReportMovimentoPeriodo subReportMovimentoPeriodo1;
        private SubReportMovimentacao subReportMovimentacao1;
        private SubReportPosicaoAtivo subReportPosicaoAtivo1;
        private SubReportRetornoAtivos subReportRetornoAtivos1;
        private SubReportRetornoEstrategia subReportRetornoEstrategia1;
        private SubReportRetornoEstrategia subReportRetornoEstrategiaLiquido;
        private SubReportResultadoEstrategia subReportResultadoEstrategia1;
        private SubReportResultadoEstrategia subReportResultadoEstrategiaLiquido;
        private SubReportPosicaoAplicacao subReportPosicaoAplicacao1;
        private SubReportFrequenciaRetorno subReportFrequenciaRetorno1;
        private SubReportGraficoRetornoComparativo subReportGraficoRetornoComparativo1;
        private SubReportRetornoIndices subReportRetornoIndices1;
        private SubReportRiscoRetorno2 subReportRiscoRetorno21;
        private XRControlStyle Header1;
        private XRTable xrTable5;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRControlStyle HeaderField;
        private XRSubreport xrSubreport100;
        private SubReportLogotipoRodape subReportLogotipoRodape1;
        private ReportFooterBand ReportFooter;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell9;
        private XRSubreport xrSubreport23;
        private SubReportObservacao subReportObservacao1;
        private ReportSemDados reportSemDados1;
        private XRSubreport xrSubreport400;
        private XRSubreport xrSubreport401;

        private SubReportGraficoAlocacaoEstrategia subReportGraficoAlocacaoEstrategia2;

        private SubReportRetornoCarteiraResumido subReportRetornoCarteiraResumido1;

        private SubReportGraficoAlocacaoAtivo subReportGraficoAlocacaoAtivo1;
        private SubReportGraficoAlocacaoAtivo subReportGraficoAlocacaoAtivo2;
        private SubReportContaCorrente subReportContaCorrente;
        private SubReportGraficoAlocacaoAtivo2 subReportGraficoAlocacaoAtivo21;

        private SubReportRetornoEstrategiaBenchmark subReportRetornoEstrategiaBenchmark1;
        private SubReportRetornoEstrategiaBenchmark subReportRetornoEstrategiaBenchmarkLiquido;
        private XRTable xrTable4;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRSubreport xrSubreport37;
        private XRSubreport xrSubreport38;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportExtratoCliente(int idCarteira, int idIndice, DateTime dataInicio, DateTime dataFim, bool mostraCaixa, bool explodeFundos)
        {
            this.InitReportExtratoCliente(idCarteira, idIndice, dataInicio, dataFim, mostraCaixa, explodeFundos);
        }

        public void InitReportExtratoCliente(int idCarteira, int idIndice, DateTime dataInicio, DateTime dataFim, bool mostraCaixa, bool explodeFundos)
        {
            this.idCarteira = idCarteira;
            this.idIndice = idIndice;
            this.mostraCaixa = mostraCaixa;
            _explodeFundos = explodeFundos;

            cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.TipoControle);
            cliente.LoadByPrimaryKey(campos, this.idCarteira);

            carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DataInicioCota);
            campos.Add(carteira.Query.Nome);
            carteira.LoadByPrimaryKey(campos, this.idCarteira);

            this.calculoMedida = new CalculoMedida(idCarteira, idIndice, dataInicio, dataFim, explodeFundos);
            
            TabelaExtratoClienteCollection t = new TabelaExtratoClienteCollection();

            this.carteiraSimulada = false;
            if (cliente.TipoControle.Value == (byte)TipoControleCliente.CarteiraSimulada)
            {
                this.carteiraSimulada = true;
                this.subRelatorios = t.RetornaPosicaoSubReportsSimulacao();
            }
            else
            {
                this.subRelatorios = t.RetornaPosicaoSubReports(idCarteira);
            }

            //Datas foram ajustadas em calculo medida - passar a utilizar estas ao inves dos parametros
            this.dataInicio = calculoMedida.DataInicioJanela.Value;
            this.dataFim = calculoMedida.DataFimJanela.Value;

            // Baseado no parametro de Configuração exibe o Patrimonio
            //string mostraPatrimonio = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio;
            //this.xrTable4.Visible = true;

            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1930);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (!this.HasData)
            {
                this.xrSubreport21.Visible = true;
            }
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// Se algum subreport do conjunto de subreports de cliente possue dados então o report possue dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return
                    /* relatorio 1*/
                this.subReportRetornoCarteira1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteira) ||
                    /* relatorio 2*/
                this.subReportGraficoRetornoAcumulado1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado) ||
                    /* relatorio 3*/
                this.subReportGraficoEvolucaoPL1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_EvolucaoPL) ||
                    /* relatorio 4*/
                this.subReportGraficoRetorno12Meses1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Retorno12meses) ||
                    /* relatorio 5*/
                this.subReportRiscoRetorno1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno) ||
                    /* relatorio 6*/
                this.subReportFrequenciaRetorno1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_FrequenciaRetornos) ||
                    /* relatório 7*/
                this.subReportGraficoRetornoComparativo1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoComparativo) ||
                    /* relatorio 8*/
                this.subReportGraficoAlocacaoGestor1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoGestor) ||
                    /* relatorio 10*/
                this.subReportPosicaoAtivo1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos) ||
                    /* relatorio 11*/
                this.subReportGraficoLiquidez1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez) ||
                    /* relatorio 27*/
                this.subReportGraficoLiquidez21.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez2) ||
                    /* relatorio 12*/
                this.subReportGraficoAlocacaoEstrategia1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategia) ||
                    /* relatorio 14*/
                this.subReportMovimentoPeriodo1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_MovimentoPeriodo) ||
                    /* relatorio 28*/
                this.subReportMovimentacao1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_Movimentacao) ||
                    /* relatorio 15*/
                this.subReportRetornoAtivos1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivos) ||
                    /* relatorio 16*/
                this.subReportRetornoAtivos1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos) ||
                    /* relatorio 27*/
                this.subReportRetornoEstrategia1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategias) ||
                    /* relatorio 40*/
                this.subReportRetornoEstrategiaLiquido.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido) ||
                    /* relatorio 17*/
                this.subReportResultadoEstrategia1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategias) ||
                /* relatorio 39*/
                this.subReportResultadoEstrategiaLiquido.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido) ||
                /* relatorio 19*/
                this.subReportPosicaoAplicacao1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAplicacoes) ||
                    /* relatorio 20*/
                this.subReportRiscoRetorno21.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno2) ||
                    /* relatorio 24*/
                this.subReportRetornoCarteiraResumido1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteiraResumido) ||
                    /* relatorio 30*/
                this.subReportRetornoIndices1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoIndices) ||
                    /* relatorio 31*/
                this.subReportGraficoRetornoAcumulado21.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado2) ||
                    /* relatorio 32*/
                this.subReportGraficoAlocacaoEstrategiaExplodida1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida) ||
                    /* relatorio 33*/
                this.subReportGraficoRetornoAcumulado31.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado3) ||
                    /* relatorio 34*/
                this.subReportGraficoAlocacaoAtivo21.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo2) ||
                    /* relatorio 35*/
                this.subReportContaCorrente.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ContaCorrente) ||
                    /* relatorio 36*/
                this.subReportPosicaoAtivo2.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos2) ||                    
                    /* relatorio 37*/
                this.subReportDetalhamentoFundo1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Detalhamento_Fundo) ||

                    /* relatorio 23* - Não Considera SubObservação para efeitos de Aparecer Relatório Sem Dados */
                    // || this.subReportObservacao1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Observacao);

                    /* relatorio 400 */
                this.subReportGraficoAlocacaoEstrategia2.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia) ||

                /* relatorio 25*/
                this.subReportGraficoAlocacaoAtivo1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo) ||

                /* relatorio 26*/
                this.subReportRetornoEstrategiaBenchmark1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark) ||

                /* relatorio 41*/
                this.subReportRetornoEstrategiaBenchmarkLiquido.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido) ||

                /* relatorio 38*/ //TODO
                this.subReportPosicaoAtivoExplodido1.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida) ||

                /* relatorio 401*/
                this.subReportGraficoAlocacaoAtivo2.HasData && this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo);
            }
        }

        /// <summary>
        /// True se o SubRelatorio é Visivel, False caso Contrario. 
        /// Analisa a TabelaExtratoCliente para ver se SubRelatorio está presente
        /// Não analisa possibilidade do SubRelatorio não ter dados Para exibir
        /// </summary>
        /// <param name="tipoSubrelatorio"></param>
        /// <returns></returns>
        private bool subReportIsVisible(TipoReportExtratoCliente tipoSubrelatorio)
        {
            return subRelatorios.ContainsKey((int)tipoSubrelatorio);
        }

        /// <summary>
        /// Relatorios: Tabela_RetornoCarteira = 1, 
        ///             Grafico_RetornoAcumulado = 2
        ///             Grafico_EvolucaoPL = 3
        ///             Grafico_Retorno12Meses = 4 
        ///             Tabela_RiscoRetorno = 5
        ///             Grafico_AlocacaoGestor = 8
        ///             Tabela_PosicaoAtivos = 10        
        ///             Grafico_Liquidez = 11
        ///             Grafico_AlocacaoEstrategia = 12
        ///             Tabela_MovimentoPeriodo = 14
        ///             Tabela_RetornoAtivos = 15
        ///             Tabela_RetornoEstrategias = 16
        ///             Tabela_ResultadoEstrategias = 17
        ///             Tabela_PosicaoAplicacoes = 19
        /// </summary>
        private void PersonalInitialize()
        {
            /* Se Relatorio Tipo = 1 (SubReportRetornoCarteira) ou Tipo = 5 (SubReportRiscoRetorno) ou
             * Tipo = 4 (SubReportRetorno12Meses) ou Tipo = 20 (SubReportRetornoCarteiraResumido) é Vísivel
               Calcula os Retornos Rentabilidade 1 única vez
             */
            #region Calcula Retornos Rentabilidade

            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteira) ||
                this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteiraResumido) ||
                this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno) ||
                this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Retorno12meses))
            {

                //inicializa property ListaRetornoMensal no calculoMedida para ser usado pelos subreports
                //A janela de data a ser utilizada começa na data inicio do cliente (disponivel em calculomedida)
                //e vai até a data fim (passada para este relatório pelo controle de datas)
                if (this.carteiraSimulada)
                {
                    this.calculoMedida.InitListaRetornoMensal(this.dataInicio, this.dataFim);
                }
                else
                {
                    this.calculoMedida.InitListaRetornoMensal(carteira.DataInicioCota.Value, this.dataFim);
                }
            }
            #endregion

            // Carrega a Posicao na Tela de cada SubRelatorio
            foreach (KeyValuePair<int, Point> s in subRelatorios)
            {

                switch (s.Key)
                {
                    #region Case de SubRelatorios
                    #region Tabela_RetornoCarteira
                    case (int)TipoReportExtratoCliente.Tabela_RetornoCarteira:
                        this.xrSubreport1.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_RetornoAcumulado Combinado com Evolução PL
                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado:
                        this.xrSubreport2.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_EvolucaoPL Combinado com Retorno Acumulado
                    case (int)TipoReportExtratoCliente.Grafico_EvolucaoPL:
                        this.xrSubreport3.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_Retorno12meses Combinado com Grafico Liquidez
                    case (int)TipoReportExtratoCliente.Grafico_Retorno12meses:
                        this.xrSubreport4.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RiscoRetorno
                    case (int)TipoReportExtratoCliente.Tabela_RiscoRetorno:
                        this.xrSubreport5.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_Volatilidade
                    case (int)TipoReportExtratoCliente.Grafico_Volatilidade:
                        //this.xrSubreport1.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_RiscoRetorno
                    case (int)TipoReportExtratoCliente.Grafico_RiscoRetorno:
                        //this.xrSubreport1.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_FrequenciaRetornos
                    case (int)TipoReportExtratoCliente.Grafico_FrequenciaRetornos:
                        this.xrSubreport6.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoGestor Combinado com Alocação Estrategia
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor:
                        this.xrSubreport8.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_Liquidez Combinado com Grafico Retorno 12 Meses
                    case (int)TipoReportExtratoCliente.Grafico_Liquidez:
                        this.xrSubreport11.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_Liquidez2
                    case (int)TipoReportExtratoCliente.Grafico_Liquidez2:
                        this.xrSubreport28.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoDireta
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoDireta:
                        //this.xrSubreport1.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_PosicaoAtivos
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos:
                        this.xrSubreport10.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoEstrategia Combinado com Alocação Gestor
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia:
                        this.xrSubreport12.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoEstrategia Isolado - Somente esse
                    case (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia:
                        this.xrSubreport400.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_MovimentoPeriodo
                    case (int)TipoReportExtratoCliente.Tabela_MovimentoPeriodo:
                        this.xrSubreport14.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_Movimentação
                    case (int)TipoReportExtratoCliente.Tabela_Movimentacao:
                        this.xrSubreport29.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoIndices
                    case (int)TipoReportExtratoCliente.Tabela_RetornoIndices:
                        this.xrSubreport30.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_RetornoAcumulado2
                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado2:
                        this.xrSubreport31.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoEstrategiaExplodida
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida:
                        this.xrSubreport32.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_RetornoAcumulado3
                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado3:
                        this.xrSubreport33.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoAtivo2
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo2:
                        this.xrSubreport34.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_ContaCorrente
                    case (int)TipoReportExtratoCliente.Tabela_ContaCorrente:
                        this.xrSubreport35.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_PosiçãoAtivo2
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos2:
                        this.xrSubreport36.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_PosicaoAtivosExplodida
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida:
                        this.xrSubreport38.Location = s.Value;
                        break;
                    #endregion
                    #region Detalhamento Fundo
                    case (int)TipoReportExtratoCliente.Detalhamento_Fundo:
                        this.xrSubreport37.Location = s.Value;
                        break;
                        #endregion
                    #region Tabela_RetornoAtivos
                    case (int)TipoReportExtratoCliente.Tabela_RetornoAtivos:
                        this.xrSubreport15.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoAtivosExplodidos
                    case (int)TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos:
                        this.xrSubreport15.Location = s.Value;
                        break;
                        #endregion
                    #region Tabela_RetornoEstrategias
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategias:
                        this.xrSubreport16.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoEstrategiasLiquida
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido:
                        this.xrSubreport40.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_ResultadoEstrategias
                    case (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategias:
                        this.xrSubreport17.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_ResultadoEstrategiasLiquida
                    case (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido:
                        this.xrSubreport39.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_DistribuicaoRetorno
                    case (int)TipoReportExtratoCliente.Grafico_DistribuicaoRetorno:
                        //this.xrSubreport1.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_PosicaoAplicacoes
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAplicacoes:
                        this.xrSubreport22.Location = s.Value;
                        break;
                    #endregion
                    #region Observacao
                    case (int)TipoReportExtratoCliente.Observacao:
                        //this.xrSubreport23.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoCarteiraResumido
                    case (int)TipoReportExtratoCliente.Tabela_RetornoCarteiraResumido:
                        this.xrSubreport24.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoEstrategiasBenchmark
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark:
                        this.xrSubreport26.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RetornoEstrategiasBenchmarkLiquido
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido:
                        this.xrSubreport41.Location = s.Value;
                        break;
                    #endregion
                    #region Tabela_RiscoRetorno2
                    case (int)TipoReportExtratoCliente.Tabela_RiscoRetorno2:
                        this.xrSubreport27.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoAtivo Combinado com Alocação Gestor
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo:
                        this.xrSubreport25.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_AlocacaoAtivo Isolado - Somente esse
                    case (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo:
                        this.xrSubreport401.Location = s.Value;
                        break;
                    #endregion
                    #region Grafico_RetornoComparativo isolado
                    case (int)TipoReportExtratoCliente.Grafico_RetornoComparativo:
                        this.xrSubreport7.Location = s.Value;
                        break;
                    #endregion
                    //
                    #region Logotipo_Rodape
                    case (int)TipoReportExtratoCliente.Logotipo_Rodape:
                        //this.xrSubreport100.Location = s.Value;
                        break;
                    #endregion
                    #endregion
                }
            }

            // Caso tenhamos especificado no web.config o uso de uma imagem de largura full, iniciar
            // a area do logo totalmente à esquerda
            if (subReportLogotipo11.HasLogoFullWidth)
            {
                this.xrSubreport20.Location = new System.Drawing.Point(0, 0);
            }

            // Passa os parametros para os SubReports
            #region Parametros SubReports

            /* -------SubReportRetornoCarteira */
            #region SubReportRetornoCarteira
            // Só executa Calculos Subreport se o idSubreport Estiver na TabelaExtratoCliente
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteira))
            {
                //UTILIZA O MÉTODO InitListaRetornoMensal QUE PREVIAMENTE DÁ LOAD NA ESTRUTURA DE RETORNOS MENSAIS
                bool janelaMovelSubReportRetornoCarteira = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.JanelaMovelSubReportRetornoCarteira;
                if (janelaMovelSubReportRetornoCarteira)
                {
                    this.subReportRetornoCarteira1.PersonalInitialize(this.calculoMedida, this.DataInicio, this.DataFim);
                }
                else
                {
                    this.subReportRetornoCarteira1.PersonalInitialize(this.calculoMedida);
                }
            }
            //
            this.xrSubreport1.Visible = this.subReportRetornoCarteira1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteira);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoRisco */
            #region SubReportRetornoRisco
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno))
            {
                //UTILIZA O MÉTODO InitListaRetornoMensal QUE PREVIAMENTE DÁ LOAD NA ESTRUTURA DE RETORNOS MENSAIS
                this.subReportRiscoRetorno1.PersonalInitialize(this.calculoMedida);
            }
            //
            this.xrSubreport5.Visible = this.subReportRiscoRetorno1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportFrequenciaRetorno */
            #region SubReportFrequenciaRetornos
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_FrequenciaRetornos))
            {
                this.subReportFrequenciaRetorno1.PersonalInitialize(this.calculoMedida, this.DataFim, this.DataInicio);
            }
            this.xrSubreport6.Visible = this.subReportFrequenciaRetorno1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_FrequenciaRetornos);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportGraficoRetornoComparativo */
            #region SubReportGraficoRetornoComparativo
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoComparativo))
            {
                this.subReportGraficoRetornoComparativo1.PersonalInitialize(this.calculoMedida, this.DataInicio, this.DataFim);
            }
            this.xrSubreport7.Visible = this.subReportGraficoRetornoComparativo1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoComparativo);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportTabelaRetornoIndices */
            #region subReportTabelaRetornoIndices
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoIndices))
            {
                this.subReportRetornoIndices1.PersonalInitialize(this.calculoMedida, this.DataFim);
            }
            this.xrSubreport30.Visible = this.subReportRetornoIndices1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoIndices);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportGraficoRetornoAcumulado2 */
            #region subReportGraficoRetornoAcumulado2
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado2))
            {
                this.subReportGraficoRetornoAcumulado21.PersonalInitialize(this.calculoMedida, this.dataInicio, this.DataFim);
            }
            this.xrSubreport31.Visible = this.subReportGraficoRetornoAcumulado21.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado2);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportGraficoRetornoAcumulado3 */
            #region subReportGraficoRetornoAcumulado3
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado3))
            {
                this.subReportGraficoRetornoAcumulado31.PersonalInitialize(this.calculoMedida, this.dataInicio, this.DataFim);
            }
            this.xrSubreport33.Visible = this.subReportGraficoRetornoAcumulado31.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado3);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportGraficoAlocacaoAtivo2 */
            #region subReportGraficoAlocacaoAtivo2
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo2))
            {
                this.subReportGraficoAlocacaoAtivo21.PersonalInitialize(this.calculoMedida, this.DataFim);
            }
            this.xrSubreport34.Visible = this.subReportGraficoAlocacaoAtivo21.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo2);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportContacorrente */
            #region subReportContaCorrente
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ContaCorrente))
            {
                this.subReportContaCorrente.PersonalInitialize(this.calculoMedida, this.dataInicio, this.DataFim);
            }
            this.xrSubreport35.Visible = this.subReportContaCorrente.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ContaCorrente);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------subReportPosicaoAtivos2 */
            #region subReportPosicaoAtivos2
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos2))
            {
                this.subReportPosicaoAtivo2.PersonalInitialize(this.calculoMedida, this.dataInicio, this.DataFim);
            }
            this.xrSubreport36.Visible = this.subReportPosicaoAtivo2.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos2);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------subReportDetalhamentoFundo */
            #region subReportDetalhamentoFundo
            if (this.subReportIsVisible(TipoReportExtratoCliente.Detalhamento_Fundo)) {
                this.subReportDetalhamentoFundo1.PersonalInitialize(this.calculoMedida.carteira.IdCarteira.Value, this.DataFim);
            }
            this.xrSubreport37.Visible = this.subReportDetalhamentoFundo1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Detalhamento_Fundo);
            #endregion
            /* -------------------------------------------------------------------*/
            /* -------SubReportGraficoRetornoAcumulado2 */
            #region SubReportGraficoAlocacaoEstrategiaExplodida1
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida))
            {
                this.subReportGraficoAlocacaoEstrategiaExplodida1.PersonalInitialize(this.calculoMedida, this.DataFim);
            }
            this.xrSubreport32.Visible = this.subReportGraficoAlocacaoEstrategiaExplodida1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida);
            #endregion
            /* -------------------------------------------------------------------*/
            
            /* -------SubReportGraficoRetorno12Meses */
            #region SubReportGraficoRetorno12Meses
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Retorno12meses))
            {
                //UTILIZA O MÉTODO InitListaRetornoMensal QUE PREVIAMENTE DÁ LOAD NA ESTRUTURA DE RETORNOS MENSAIS
                this.subReportGraficoRetorno12Meses1.PersonalInitialize(this.calculoMedida);
            }
            //
            this.xrSubreport4.Visible = this.subReportGraficoRetorno12Meses1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Retorno12meses);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportAlocacaoGestor */
            #region SubReportAlocacaoGestor
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoGestor))
            {
                this.subReportGraficoAlocacaoGestor1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport8.Visible = this.subReportGraficoAlocacaoGestor1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoGestor);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportAlocacaoEstrategia */
            #region SubReportAlocacaoEstrategia
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategia))
            {
                this.subReportGraficoAlocacaoEstrategia1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport12.Visible = this.subReportGraficoAlocacaoEstrategia1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoEstrategia);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportAlocacaoEstrategia Somente - para Gráfico aparecendo Isoladamente  */
            #region SubReportAlocacaoEstrategia Somente
            if (this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia))
            {
                this.subReportGraficoAlocacaoEstrategia2.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport400.Visible = this.subReportGraficoAlocacaoEstrategia2.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportLiquidez */
            #region SubReportLiquidez
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez))
            {
                this.subReportGraficoLiquidez1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport11.Visible = this.subReportGraficoLiquidez1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportLiquidez2 */
            #region SubReportLiquidez2
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez2))
            {
                this.subReportGraficoLiquidez21.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport28.Visible = this.subReportGraficoLiquidez21.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_Liquidez2);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportGraficoRetornoAcumulado */
            #region SubReportGraficoRetornoAcumulado
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado))
            {
                if (this.carteiraSimulada)
                {
                    this.subReportGraficoRetornoAcumulado1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
                }
                else
                {
                    this.subReportGraficoRetornoAcumulado1.PersonalInitialize(this.calculoMedida, carteira.DataInicioCota.Value, this.dataFim);
                }
            }
            //
            this.xrSubreport2.Visible = this.subReportGraficoRetornoAcumulado1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_RetornoAcumulado);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportGraficoEvolucaoPL */
            #region SubReportGraficoEvolucaoPL
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_EvolucaoPL))
            {
                this.subReportGraficoEvolucaoPL1.PersonalInitialize(this.calculoMedida, cliente.DataImplantacao.Value, this.dataFim);
            }
            //
            this.xrSubreport3.Visible = this.subReportGraficoEvolucaoPL1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Grafico_EvolucaoPL);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportPosicaoAtivos */
            #region SubReportPosicaoAtivos
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos))
            {
                this.subReportPosicaoAtivo1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim, this.mostraCaixa, _explodeFundos);
            }
            //
            this.xrSubreport10.Visible = this.subReportPosicaoAtivo1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivos);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportMovimentoPeriodo */
            #region SubReportMovimentoPeriodo
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_MovimentoPeriodo))
            {
                this.subReportMovimentoPeriodo1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
            }
            //
            this.xrSubreport14.Visible = this.subReportMovimentoPeriodo1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_MovimentoPeriodo);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportMovimentação */
            #region SubReportMovimentação
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_Movimentacao))
            {
                this.subReportMovimentacao1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
            }
            //
            this.xrSubreport29.Visible = this.subReportMovimentacao1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Tabela_Movimentacao);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoAtivos */
            #region SubReportRetornoAtivos
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivos))
            {
                if (!this.carteiraSimulada)
                {
                    this.calculoMedida.SetDataInicio(cliente.DataImplantacao.Value);
                }

                this.subReportRetornoAtivos1.PersonalInitialize(this.calculoMedida, this.dataFim, _explodeFundos);
            }
            //
            this.xrSubreport15.Visible = this.subReportRetornoAtivos1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivos);
            bool retornoAtivos = this.xrSubreport15.Visible;
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoAtivos */
            #region SubReportRetornoAtivos
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos))
            {
                if (!this.carteiraSimulada)
                {
                    this.calculoMedida.SetDataInicio(cliente.DataImplantacao.Value);
                }

                this.subReportRetornoAtivos1.PersonalInitialize(this.calculoMedida, this.dataFim, true);
            }
            //
            this.xrSubreport15.Visible = ( this.subReportRetornoAtivos1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos)) || retornoAtivos;
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportResultadoEstrategias */
            #region SubReportResultadoEstrategias
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategias))
            {
                this.calculoMedida.SetDataInicio(cliente.DataImplantacao.Value);
                this.subReportResultadoEstrategia1.PersonalInitialize(this.calculoMedida, this.dataFim, TipoResultadoEstrategia.Normal);
            }
            //
            this.xrSubreport17.Visible = this.subReportResultadoEstrategia1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategias);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportResultadoEstrategiasLiquida */
            #region SubReportResultadoEstrategiasLiquida
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido)) {
                this.calculoMedida.SetDataInicio(cliente.DataImplantacao.Value);
                this.subReportResultadoEstrategiaLiquido.PersonalInitialize(this.calculoMedida, this.dataFim, TipoResultadoEstrategia.Liquido);
            }
            //
            this.xrSubreport39.Visible = this.subReportResultadoEstrategiaLiquido.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportPosicaoAplicacao */
            #region SubReportPosicaoAplicacao
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAplicacoes))
            {
                this.calculoMedida.SetDataInicio(cliente.DataImplantacao.Value);
                this.subReportPosicaoAplicacao1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport22.Visible = this.subReportPosicaoAplicacao1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAplicacoes);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoEstrategias */
            #region SubReportRetornoEstrategias
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategias))
            {
                this.subReportRetornoEstrategia1.PersonalInitialize(this.calculoMedida, this.dataFim, TipoRetornoEstrategia.Normal);
            }
            //
            this.xrSubreport16.Visible = this.subReportRetornoEstrategia1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategias);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoEstrategiasLiquida */
            #region SubReportRetornoEstrategiasLiquida
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido)) {
                this.subReportRetornoEstrategiaLiquido.PersonalInitialize(this.calculoMedida, this.dataFim, TipoRetornoEstrategia.Liquido);
            }
            //
            this.xrSubreport40.Visible = this.subReportRetornoEstrategiaLiquido.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportObservacao */
            #region SubReportObservacao
            this.xrSubreport23.Visible = this.subReportIsVisible(TipoReportExtratoCliente.Observacao);

            // Corrige Posição do Rodapé se não tiver SubRelatorio de Observação
            if (!this.xrSubreport23.Visible)
            {
                this.xrSubreport19.LocationF = new PointF(0, 0);
                this.xrPageInfo1.LocationF = new PointF(1770, 55);
                this.PageFooter.HeightF = 145;
            }
            #endregion
            /* -------------------------------------------------------------------*/


            /* New */
            /* -------SubReportRetornoCarteiraResumido */
            #region SubReportRetornoCarteiraResumido

            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteiraResumido))
            {
                this.subReportRetornoCarteiraResumido1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }

            this.xrSubreport24.Visible = this.subReportRetornoCarteiraResumido1.HasData &&
                                        this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoCarteiraResumido);

            #endregion
            /* -------------------------------------------------------------------*/


            /* -------SubReportAlocacaoAtivo */
            #region SubReportAlocacaoAtivo
            if (this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo))
            {
                this.subReportGraficoAlocacaoAtivo1.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            this.xrSubreport25.Visible = this.subReportGraficoAlocacaoAtivo1.HasData &&
                                         this.subReportIsVisible(TipoReportExtratoCliente.Grafico_AlocacaoAtivo);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportAlocacaoAtivo Somente - para Gráfico aparecendo Isoladamente  */
            #region SubReportAlocacaoAtivo Somente
            if (this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo))
            {
                this.subReportGraficoAlocacaoAtivo2.PersonalInitialize(this.calculoMedida, this.dataFim);
            }
            //
            //this.xrSubreport401.Visible = this.subReportGraficoAlocacaoAtivo2.HasData &&
            //                              this.subReportIsVisible(TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoEstrategiasBenchmark */
            #region SubReportRetornoEstrategiasBenchmark
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark))
            {
                this.subReportRetornoEstrategiaBenchmark1.PersonalInitialize(this.calculoMedida, this.dataFim, TipoRetornoEstrategiaBenchMark.Normal);
            }
            //
            this.xrSubreport26.Visible = this.subReportRetornoEstrategiaBenchmark1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRetornoEstrategiasBenchmarkLiquido */
            #region SubReportRetornoEstrategiasBenchmarkLiquido
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido)) {
                this.subReportRetornoEstrategiaBenchmarkLiquido.PersonalInitialize(this.calculoMedida, this.dataFim, TipoRetornoEstrategiaBenchMark.Liquido);
            }
            //
            this.xrSubreport41.Visible = this.subReportRetornoEstrategiaBenchmarkLiquido.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportRiscoRetorno2*/
            #region SubReportRiscoRetorno2
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno2))
            {
                this.subReportRiscoRetorno21.PersonalInitialize(this.calculoMedida, dataInicio, DataFim, (short)idIndice);
            }
            //
            this.xrSubreport27.Visible = this.subReportRiscoRetorno21.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_RiscoRetorno2);
            #endregion
            /* -------------------------------------------------------------------*/

            /* -------SubReportPosicaoAtivoExplodido*/
            #region SubReportPosicaoAtivoExplodido
            if (this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida))
            {
                this.subReportPosicaoAtivoExplodido1.PersonalInitialize(this.calculoMedida, dataInicio, DataFim);
            }
            //
            this.xrSubreport38.Visible = this.subReportPosicaoAtivoExplodido1.HasData &&
                                            this.subReportIsVisible(TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida);
            #endregion
            /* -------------------------------------------------------------------*/

            #endregion

            #region Visibildade do SubRelatorio Logotipo_Rodape
            // Se relatorio sem Dados aparece Logotipo não aparece. Se relatorio sem Dados não aparece LogotipoRodape aparece. 
            this.xrSubreport100.Visible = this.subReportIsVisible(TipoReportExtratoCliente.Logotipo_Rodape) &&
                                          this.xrSubreport21.Visible == false;
            #endregion

            // Se LogotipoRodape não aparece ReportFooter deve desaperecer para não ocupar espaço.
            /* Trata caso de Duplicação de Cabeçalho aparecer numa pagina sem mais nenhum dado */
            if (!this.xrSubreport100.Visible)
            {
                this.ReportFooter.Visible = false;
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportExtratoCliente.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportExtratoCliente.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataReferencia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport21 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport20 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo11 = new Financial.Relatorio.SubReportLogotipo1();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoCarteira1 = new Financial.Relatorio.SubReportRetornoCarteira();
            this.subReportGraficoEvolucaoPL1 = new Financial.Relatorio.SubReportGraficoEvolucaoPL();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetornoAcumulado1 = new Financial.Relatorio.SubReportGraficoRetornoAcumulado();
            this.subReportPosicaoAtivoExplodido1 = new Financial.Relatorio.SubReportPosicaoAtivoExplodido();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport37 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDetalhamentoFundo1 = new Financial.Relatorio.SubReportDetalhamentoFundo();
            this.xrSubreport26 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoEstrategiaBenchmark1 = new Financial.Relatorio.SubReportRetornoEstrategiaBenchmark();
            this.xrSubreport41 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoEstrategiaBenchmarkLiquido = new Financial.Relatorio.SubReportRetornoEstrategiaBenchmark();
            this.xrSubreport27 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRiscoRetorno21 = new Financial.Relatorio.SubReportRiscoRetorno2();
            this.xrSubreport28 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoLiquidez21 = new Financial.Relatorio.SubReportGraficoLiquidez2();
            this.xrSubreport29 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMovimentacao1 = new Financial.Relatorio.SubReportMovimentacao();
            this.xrSubreport30 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoIndices1 = new Financial.Relatorio.SubReportRetornoIndices();
            this.xrSubreport31 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetornoAcumulado21 = new Financial.Relatorio.SubReportGraficoRetornoAcumulado2();
            this.xrSubreport32 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoEstrategiaExplodida1 = new Financial.Relatorio.SubReportGraficoAlocacaoEstrategiaExplodida();
            this.xrSubreport33 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetornoAcumulado31 = new Financial.Relatorio.SubReportGraficoRetornoAcumulado3();
            this.xrSubreport34 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoAtivo21 = new Financial.Relatorio.SubReportGraficoAlocacaoAtivo2();
            this.xrSubreport35 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportContaCorrente = new Financial.Relatorio.SubReportContaCorrente();
            this.xrSubreport36 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportPosicaoAtivo2 = new Financial.Relatorio.SubReportPosicaoAtivo2();
            this.xrSubreport401 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoAtivo2 = new Financial.Relatorio.SubReportGraficoAlocacaoAtivo();
            this.xrSubreport25 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoAtivo1 = new Financial.Relatorio.SubReportGraficoAlocacaoAtivo();
            this.xrSubreport24 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoCarteiraResumido1 = new Financial.Relatorio.SubReportRetornoCarteiraResumido();
            this.xrSubreport400 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoEstrategia2 = new Financial.Relatorio.SubReportGraficoAlocacaoEstrategia();
            this.xrSubreport22 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportPosicaoAplicacao1 = new Financial.Relatorio.SubReportPosicaoAplicacao();
            this.xrSubreport18 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport17 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportResultadoEstrategia1 = new Financial.Relatorio.SubReportResultadoEstrategia();
            this.xrSubreport39 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportResultadoEstrategiaLiquido = new Financial.Relatorio.SubReportResultadoEstrategia();
            this.xrSubreport16 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoEstrategia1 = new Financial.Relatorio.SubReportRetornoEstrategia();
            this.xrSubreport40 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoEstrategiaLiquido = new Financial.Relatorio.SubReportRetornoEstrategia();
            this.xrSubreport15 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoAtivos1 = new Financial.Relatorio.SubReportRetornoAtivos();
            this.xrSubreport14 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMovimentoPeriodo1 = new Financial.Relatorio.SubReportMovimentoPeriodo();
            this.xrSubreport13 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport12 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoEstrategia1 = new Financial.Relatorio.SubReportGraficoAlocacaoEstrategia();
            this.xrSubreport11 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoLiquidez1 = new Financial.Relatorio.SubReportGraficoLiquidez();
            this.xrSubreport10 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportPosicaoAtivo1 = new Financial.Relatorio.SubReportPosicaoAtivo();
            this.xrSubreport9 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport8 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoAlocacaoGestor1 = new Financial.Relatorio.SubReportGraficoAlocacaoGestor();
            this.xrSubreport7 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetornoComparativo1 = new Financial.Relatorio.SubReportGraficoRetornoComparativo();
            this.xrSubreport6 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportFrequenciaRetorno1 = new Financial.Relatorio.SubReportFrequenciaRetorno();
            this.xrSubreport5 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRiscoRetorno1 = new Financial.Relatorio.SubReportRiscoRetorno();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetorno12Meses1 = new Financial.Relatorio.SubReportGraficoRetorno12Meses();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrSubreport100 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipoRodape1 = new Financial.Relatorio.SubReportLogotipoRodape();
            this.xrSubreport23 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportObservacao1 = new Financial.Relatorio.SubReportObservacao();
            this.xrSubreport19 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.Header1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.HeaderField = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrSubreport38 = new DevExpress.XtraReports.UI.XRSubreport();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteira1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoEvolucaoPL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivoExplodido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDetalhamentoFundo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaBenchmark1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaBenchmarkLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRiscoRetorno21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoLiquidez21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentacao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoIndices1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategiaExplodida1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportContaCorrente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteiraResumido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategia2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAplicacao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportResultadoEstrategia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportResultadoEstrategiaLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoAtivos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentoPeriodo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoLiquidez1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoGestor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoComparativo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportFrequenciaRetorno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRiscoRetorno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetorno12Meses1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipoRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportObservacao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrTable3,
            this.xrSubreport21,
            this.xrSubreport20});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeader.HeightF = 285.75F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable8,
            this.xrPageInfo2,
            this.xrTable5});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 70F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1900F, 145F);
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(1470.821F, 91.00002F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable4.SizeF = new System.Drawing.SizeF(415.9498F, 40.00005F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable4.Visible = false;
            this.xrTable4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PatrimonioVisibleBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.32283464566929132;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.CanGrow = false;
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell10.StyleName = "HeaderField";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseForeColor = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "Patrimônio:";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.20431573491144356;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.CanGrow = false;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell11.StyleName = "HeaderField";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "Valor";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.36454308318378376;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PatrimonioBeforePrint);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 5F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(135F, 34F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrPageInfo3.Visible = false;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(1470.821F, 47.99998F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(415.9498F, 40.00005F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCellDataReferencia});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.32283464566929132;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.CanGrow = false;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell22.StyleName = "HeaderField";
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Período:";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.15727560841009844;
            // 
            // xrTableCellDataReferencia
            // 
            this.xrTableCellDataReferencia.CanGrow = false;
            this.xrTableCellDataReferencia.Dpi = 254F;
            this.xrTableCellDataReferencia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataReferencia.Name = "xrTableCellDataReferencia";
            this.xrTableCellDataReferencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataReferencia.StyleName = "HeaderField";
            this.xrTableCellDataReferencia.StylePriority.UseFont = false;
            this.xrTableCellDataReferencia.StylePriority.UseForeColor = false;
            this.xrTableCellDataReferencia.StylePriority.UseTextAlignment = false;
            this.xrTableCellDataReferencia.Text = "Data";
            this.xrTableCellDataReferencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellDataReferencia.Weight = 0.4115832096851289;
            this.xrTableCellDataReferencia.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBeforePrint);
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(1470.82F, 12F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(115F, 35F);
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.CanGrow = false;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell25.StyleName = "HeaderField";
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseForeColor = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Emissão:";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.81146941034279108;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(1596.404F, 12F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(290.3667F, 34.00002F);
            this.xrPageInfo2.StyleName = "HeaderField";
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(5F, 5F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow7});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1140.021F, 129F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StyleName = "HeaderField";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Cliente:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.17655740519404739;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StyleName = "HeaderField";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Carteira";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 1.0479550829957089;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell3.StyleName = "HeaderField";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "BenchMark:";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.17655740519404739;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StyleName = "HeaderField";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Indice";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 1.0479550829957089;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IndiceBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell9});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StyleName = "HeaderField";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Data Início:";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 0.17655740519404739;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StyleName = "HeaderField";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "DataInicioCota";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 1.0479550829957089;
            this.xrTableCell9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioCotaBeforePrint);
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 225F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1900F, 35F);
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.StylePriority.UseBorderColor = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StyleName = "Header1";
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseForeColor = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "EXTRATO CONSOLIDADO DE INVESTIMENTOS";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 1;
            // 
            // xrSubreport21
            // 
            this.xrSubreport21.Dpi = 254F;
            this.xrSubreport21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 268F);
            this.xrSubreport21.Name = "xrSubreport21";
            this.xrSubreport21.ReportSource = this.reportSemDados1;
            this.xrSubreport21.SizeF = new System.Drawing.SizeF(64F, 15F);
            this.xrSubreport21.Visible = false;
            // 
            // xrSubreport20
            // 
            this.xrSubreport20.Dpi = 254F;
            this.xrSubreport20.LocationFloat = new DevExpress.Utils.PointFloat(1180F, 0F);
            this.xrSubreport20.Name = "xrSubreport20";
            this.xrSubreport20.ReportSource = this.subReportLogotipo11;
            this.xrSubreport20.SizeF = new System.Drawing.SizeF(720F, 61F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1790F, 167F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(146F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRetornoCarteira1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport1.Visible = false;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(650F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportGraficoRetornoAcumulado1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport2.Visible = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DetailReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport38,
            this.xrSubreport37,
            this.xrSubreport26,
            this.xrSubreport41,
            this.xrSubreport27,
            this.xrSubreport28,
            this.xrSubreport29,
            this.xrSubreport30,
            this.xrSubreport31,
            this.xrSubreport32,
            this.xrSubreport33,
            this.xrSubreport34,
            this.xrSubreport35,
            this.xrSubreport36,
            this.xrSubreport401,
            this.xrSubreport25,
            this.xrSubreport24,
            this.xrSubreport400,
            this.xrSubreport22,
            this.xrSubreport18,
            this.xrSubreport17,
            this.xrSubreport39,
            this.xrSubreport16,
            this.xrSubreport40,
            this.xrSubreport15,
            this.xrSubreport14,
            this.xrSubreport13,
            this.xrSubreport12,
            this.xrSubreport11,
            this.xrSubreport10,
            this.xrSubreport9,
            this.xrSubreport8,
            this.xrSubreport7,
            this.xrSubreport6,
            this.xrSubreport5,
            this.xrSubreport4,
            this.xrSubreport3,
            this.xrSubreport2,
            this.xrSubreport1});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 1427.521F;
            this.Detail1.KeepTogether = true;
            this.Detail1.Name = "Detail1";
            this.Detail1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport37
            // 
            this.xrSubreport37.Dpi = 254F;
            this.xrSubreport37.LocationFloat = new DevExpress.Utils.PointFloat(650F, 1320F);
            this.xrSubreport37.Name = "xrSubreport37";
            this.xrSubreport37.ReportSource = this.subReportDetalhamentoFundo1;
            this.xrSubreport37.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport37.Visible = false;
            // 
            // xrSubreport26
            // 
            this.xrSubreport26.Dpi = 254F;
            this.xrSubreport26.LocationFloat = new DevExpress.Utils.PointFloat(650F, 1210F);
            this.xrSubreport26.Name = "xrSubreport26";
            this.xrSubreport26.ReportSource = this.subReportRetornoEstrategiaBenchmark1;
            this.xrSubreport26.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport26.Visible = false;
            // 
            // xrSubreport41
            // 
            this.xrSubreport41.Dpi = 254F;
            this.xrSubreport41.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 770F);
            this.xrSubreport41.Name = "xrSubreport41";
            this.xrSubreport41.ReportSource = this.subReportRetornoEstrategiaBenchmarkLiquido;
            this.xrSubreport41.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport41.Visible = false;
            // 
            // xrSubreport27
            // 
            this.xrSubreport27.Dpi = 254F;
            this.xrSubreport27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 880F);
            this.xrSubreport27.Name = "xrSubreport27";
            this.xrSubreport27.ReportSource = this.subReportRiscoRetorno21;
            this.xrSubreport27.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport27.Visible = false;
            // 
            // xrSubreport28
            // 
            this.xrSubreport28.Dpi = 254F;
            this.xrSubreport28.LocationFloat = new DevExpress.Utils.PointFloat(650F, 880F);
            this.xrSubreport28.Name = "xrSubreport28";
            this.xrSubreport28.ReportSource = this.subReportGraficoLiquidez21;
            this.xrSubreport28.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport28.Visible = false;
            // 
            // xrSubreport29
            // 
            this.xrSubreport29.Dpi = 254F;
            this.xrSubreport29.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1210F);
            this.xrSubreport29.Name = "xrSubreport29";
            this.xrSubreport29.ReportSource = this.subReportMovimentacao1;
            this.xrSubreport29.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport29.Visible = false;
            // 
            // xrSubreport30
            // 
            this.xrSubreport30.Dpi = 254F;
            this.xrSubreport30.LocationFloat = new DevExpress.Utils.PointFloat(0F, 990F);
            this.xrSubreport30.Name = "xrSubreport30";
            this.xrSubreport30.ReportSource = this.subReportRetornoIndices1;
            this.xrSubreport30.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport30.Visible = false;
            // 
            // xrSubreport31
            // 
            this.xrSubreport31.Dpi = 254F;
            this.xrSubreport31.LocationFloat = new DevExpress.Utils.PointFloat(650F, 990F);
            this.xrSubreport31.Name = "xrSubreport31";
            this.xrSubreport31.ReportSource = this.subReportGraficoRetornoAcumulado21;
            this.xrSubreport31.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport31.Visible = false;
            // 
            // xrSubreport32
            // 
            this.xrSubreport32.Dpi = 254F;
            this.xrSubreport32.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 990F);
            this.xrSubreport32.Name = "xrSubreport32";
            this.xrSubreport32.ReportSource = this.subReportGraficoAlocacaoEstrategiaExplodida1;
            this.xrSubreport32.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport32.Visible = false;
            // 
            // xrSubreport33
            // 
            this.xrSubreport33.Dpi = 254F;
            this.xrSubreport33.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 880F);
            this.xrSubreport33.Name = "xrSubreport33";
            this.xrSubreport33.ReportSource = this.subReportGraficoRetornoAcumulado31;
            this.xrSubreport33.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport33.Visible = false;
            // 
            // xrSubreport34
            // 
            this.xrSubreport34.Dpi = 254F;
            this.xrSubreport34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1100F);
            this.xrSubreport34.Name = "xrSubreport34";
            this.xrSubreport34.ReportSource = this.subReportGraficoAlocacaoAtivo21;
            this.xrSubreport34.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport34.Visible = false;
            // 
            // xrSubreport35
            // 
            this.xrSubreport35.Dpi = 254F;
            this.xrSubreport35.LocationFloat = new DevExpress.Utils.PointFloat(650F, 1100F);
            this.xrSubreport35.Name = "xrSubreport35";
            this.xrSubreport35.ReportSource = this.subReportContaCorrente;
            this.xrSubreport35.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport35.Visible = false;
            // 
            // xrSubreport36
            // 
            this.xrSubreport36.Dpi = 254F;
            this.xrSubreport36.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 1100F);
            this.xrSubreport36.Name = "xrSubreport36";
            this.xrSubreport36.ReportSource = this.subReportPosicaoAtivo2;
            this.xrSubreport36.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport36.Visible = false;
            // 
            // xrSubreport401
            // 
            this.xrSubreport401.Dpi = 254F;
            this.xrSubreport401.LocationFloat = new DevExpress.Utils.PointFloat(650F, 770F);
            this.xrSubreport401.Name = "xrSubreport401";
            this.xrSubreport401.ReportSource = this.subReportGraficoAlocacaoAtivo2;
            this.xrSubreport401.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport401.Visible = false;
            // 
            // xrSubreport25
            // 
            this.xrSubreport25.Dpi = 254F;
            this.xrSubreport25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 770F);
            this.xrSubreport25.Name = "xrSubreport25";
            this.xrSubreport25.ReportSource = this.subReportGraficoAlocacaoAtivo1;
            this.xrSubreport25.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport25.Visible = false;
            // 
            // xrSubreport24
            // 
            this.xrSubreport24.Dpi = 254F;
            this.xrSubreport24.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 660.0001F);
            this.xrSubreport24.Name = "xrSubreport24";
            this.xrSubreport24.ReportSource = this.subReportRetornoCarteiraResumido1;
            this.xrSubreport24.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport24.Visible = false;
            // 
            // xrSubreport400
            // 
            this.xrSubreport400.Dpi = 254F;
            this.xrSubreport400.LocationFloat = new DevExpress.Utils.PointFloat(650F, 660.0001F);
            this.xrSubreport400.Name = "xrSubreport400";
            this.xrSubreport400.ReportSource = this.subReportGraficoAlocacaoEstrategia2;
            this.xrSubreport400.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport400.Visible = false;
            // 
            // xrSubreport22
            // 
            this.xrSubreport22.Dpi = 254F;
            this.xrSubreport22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 660F);
            this.xrSubreport22.Name = "xrSubreport22";
            this.xrSubreport22.ReportSource = this.subReportPosicaoAplicacao1;
            this.xrSubreport22.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport22.Visible = false;
            // 
            // xrSubreport18
            // 
            this.xrSubreport18.Dpi = 254F;
            this.xrSubreport18.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 550F);
            this.xrSubreport18.Name = "xrSubreport18";
            this.xrSubreport18.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport18.Visible = false;
            // 
            // xrSubreport17
            // 
            this.xrSubreport17.Dpi = 254F;
            this.xrSubreport17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1320F);
            this.xrSubreport17.Name = "xrSubreport17";
            this.xrSubreport17.ReportSource = this.subReportResultadoEstrategia1;
            this.xrSubreport17.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport17.Visible = false;
            // 
            // xrSubreport39
            // 
            this.xrSubreport39.Dpi = 254F;
            this.xrSubreport39.LocationFloat = new DevExpress.Utils.PointFloat(650F, 550F);
            this.xrSubreport39.Name = "xrSubreport39";
            this.xrSubreport39.ReportSource = this.subReportResultadoEstrategiaLiquido;
            this.xrSubreport39.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport39.Visible = false;
            // 
            // xrSubreport16
            // 
            this.xrSubreport16.Dpi = 254F;
            this.xrSubreport16.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 1210F);
            this.xrSubreport16.Name = "xrSubreport16";
            this.xrSubreport16.ReportSource = this.subReportRetornoEstrategia1;
            this.xrSubreport16.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport16.Visible = false;
            // 
            // xrSubreport40
            // 
            this.xrSubreport40.Dpi = 254F;
            this.xrSubreport40.LocationFloat = new DevExpress.Utils.PointFloat(0F, 550F);
            this.xrSubreport40.Name = "xrSubreport40";
            this.xrSubreport40.ReportSource = this.subReportRetornoEstrategiaLiquido;
            this.xrSubreport40.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport40.Visible = false;
            // 
            // xrSubreport15
            // 
            this.xrSubreport15.Dpi = 254F;
            this.xrSubreport15.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 440F);
            this.xrSubreport15.Name = "xrSubreport15";
            this.xrSubreport15.ReportSource = this.subReportRetornoAtivos1;
            this.xrSubreport15.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport15.Visible = false;
            // 
            // xrSubreport14
            // 
            this.xrSubreport14.Dpi = 254F;
            this.xrSubreport14.LocationFloat = new DevExpress.Utils.PointFloat(650F, 440F);
            this.xrSubreport14.Name = "xrSubreport14";
            this.xrSubreport14.ReportSource = this.subReportMovimentoPeriodo1;
            this.xrSubreport14.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport14.Visible = false;
            // 
            // xrSubreport13
            // 
            this.xrSubreport13.Dpi = 254F;
            this.xrSubreport13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 440F);
            this.xrSubreport13.Name = "xrSubreport13";
            this.xrSubreport13.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport13.Visible = false;
            // 
            // xrSubreport12
            // 
            this.xrSubreport12.Dpi = 254F;
            this.xrSubreport12.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 330F);
            this.xrSubreport12.Name = "xrSubreport12";
            this.xrSubreport12.ReportSource = this.subReportGraficoAlocacaoEstrategia1;
            this.xrSubreport12.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport12.Visible = false;
            // 
            // xrSubreport11
            // 
            this.xrSubreport11.Dpi = 254F;
            this.xrSubreport11.LocationFloat = new DevExpress.Utils.PointFloat(650F, 330F);
            this.xrSubreport11.Name = "xrSubreport11";
            this.xrSubreport11.ReportSource = this.subReportGraficoLiquidez1;
            this.xrSubreport11.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport11.Visible = false;
            // 
            // xrSubreport10
            // 
            this.xrSubreport10.Dpi = 254F;
            this.xrSubreport10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 330F);
            this.xrSubreport10.Name = "xrSubreport10";
            this.xrSubreport10.ReportSource = this.subReportPosicaoAtivo1;
            this.xrSubreport10.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport10.Visible = false;
            // 
            // xrSubreport9
            // 
            this.xrSubreport9.Dpi = 254F;
            this.xrSubreport9.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 220F);
            this.xrSubreport9.Name = "xrSubreport9";
            this.xrSubreport9.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport9.Visible = false;
            // 
            // xrSubreport8
            // 
            this.xrSubreport8.Dpi = 254F;
            this.xrSubreport8.LocationFloat = new DevExpress.Utils.PointFloat(650F, 220F);
            this.xrSubreport8.Name = "xrSubreport8";
            this.xrSubreport8.ReportSource = this.subReportGraficoAlocacaoGestor1;
            this.xrSubreport8.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport8.Visible = false;
            // 
            // xrSubreport7
            // 
            this.xrSubreport7.Dpi = 254F;
            this.xrSubreport7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 220F);
            this.xrSubreport7.Name = "xrSubreport7";
            this.xrSubreport7.ReportSource = this.subReportGraficoRetornoComparativo1;
            this.xrSubreport7.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport7.Visible = false;
            // 
            // xrSubreport6
            // 
            this.xrSubreport6.Dpi = 254F;
            this.xrSubreport6.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 110F);
            this.xrSubreport6.Name = "xrSubreport6";
            this.xrSubreport6.ReportSource = this.subReportFrequenciaRetorno1;
            this.xrSubreport6.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport6.Visible = false;
            // 
            // xrSubreport5
            // 
            this.xrSubreport5.Dpi = 254F;
            this.xrSubreport5.LocationFloat = new DevExpress.Utils.PointFloat(650F, 110F);
            this.xrSubreport5.Name = "xrSubreport5";
            this.xrSubreport5.ReportSource = this.subReportRiscoRetorno1;
            this.xrSubreport5.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport5.Visible = false;
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 110F);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportGraficoRetorno12Meses1;
            this.xrSubreport4.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport4.Visible = false;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 0F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportGraficoEvolucaoPL1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport3.Visible = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport100});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            // 
            // xrSubreport100
            // 
            this.xrSubreport100.Dpi = 254F;
            this.xrSubreport100.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 0F);
            this.xrSubreport100.Name = "xrSubreport100";
            this.xrSubreport100.ReportSource = this.subReportLogotipoRodape1;
            this.xrSubreport100.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport100.Visible = false;
            // 
            // xrSubreport23
            // 
            this.xrSubreport23.Dpi = 254F;
            this.xrSubreport23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport23.Name = "xrSubreport23";
            this.xrSubreport23.ReportSource = this.subReportObservacao1;
            this.xrSubreport23.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport23.Visible = false;
            // 
            // xrSubreport19
            // 
            this.xrSubreport19.Dpi = 254F;
            this.xrSubreport19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrSubreport19.Name = "xrSubreport19";
            this.xrSubreport19.ReportSource = this.subReportRodape1;
            this.xrSubreport19.SizeF = new System.Drawing.SizeF(625F, 65.00007F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport19,
            this.xrPageInfo1,
            this.xrSubreport23});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 219.5833F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Header1
            // 
            this.Header1.Name = "Header1";
            this.Header1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // HeaderField
            // 
            this.HeaderField.Name = "HeaderField";
            this.HeaderField.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 75F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrSubreport38
            // 
            this.xrSubreport38.Dpi = 254F;
            this.xrSubreport38.LocationFloat = new DevExpress.Utils.PointFloat(1300F, 1320F);
            this.xrSubreport38.Name = "xrSubreport38";
            this.xrSubreport38.ReportSource = this.subReportPosicaoAtivoExplodido1;
            this.xrSubreport38.SizeF = new System.Drawing.SizeF(625F, 100F);
            this.xrSubreport38.Visible = false;
            // 
            // ReportExtratoCliente
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.DetailReport,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 100, 75);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header1,
            this.HeaderField});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteira1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivoExplodido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoEvolucaoPL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDetalhamentoFundo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaBenchmark1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaBenchmarkLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRiscoRetorno21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoLiquidez21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentacao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoIndices1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategiaExplodida1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportContaCorrente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoAtivo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteiraResumido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategia2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAplicacao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportResultadoEstrategia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportResultadoEstrategiaLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoEstrategiaLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoAtivos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMovimentoPeriodo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoEstrategia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoLiquidez1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPosicaoAtivo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoAlocacaoGestor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoComparativo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportFrequenciaRetorno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRiscoRetorno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetorno12Meses1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipoRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportObservacao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.ReportExtratoCliente.ResourceManager;
        }

        #region Funções Internas do Relatorio
        /// <summary>
        /// Preenche a Data do Cabeçalho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataXRTableCell = sender as XRTableCell;
            dataXRTableCell.Text = this.dataInicio.ToString("d") + " a " + this.dataFim.ToString("d");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell carteiraXRTableCell = sender as XRTableCell;
            carteiraXRTableCell.Text = carteira.Nome;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell indiceXRTableCell = sender as XRTableCell;
            indiceXRTableCell.Text = this.calculoMedida.indice.Descricao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataInicioCotaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataInicioCotaXRTableCell = sender as XRTableCell;

            if (this.carteiraSimulada)
            {
                dataInicioCotaXRTableCell.Visible = false;
                this.xrTableCell6.Visible = false;
            }
            else
            {
                dataInicioCotaXRTableCell.Text = carteira.DataInicioCota.Value.ToString("d");
            }
        }
        #endregion

        // Mostra o Patrimonio
        private void PatrimonioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell historicoCotaXRTableCell = sender as XRTableCell;
            historicoCotaXRTableCell.Text = "";

            HistoricoCota historicoCota = new HistoricoCota();

            try {	        
		      historicoCota.BuscaValorPatrimonioDia(this.idCarteira, this.dataFim);
              //
              //
              if (historicoCota.PLFechamento.HasValue) {
                  historicoCotaXRTableCell.Text = historicoCota.PLFechamento.Value.ToString("N2");
              }
              else {
                  historicoCotaXRTableCell.Text = " - ";
              }
	        }
	        catch (HistoricoCotaNaoCadastradoException r) {
                historicoCotaXRTableCell.Text = " - ";
	        }                       
        }
        
        /// <summary>
        /// Controla Visibilidade do patrimonio
        /// Baseado no parametro de Configuração exibe o Patrimonio 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PatrimonioVisibleBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;
            
            string mostraPatrimonio = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio;
            table.Visible = mostraPatrimonio == "S" ? true : false;
        }
    }
}