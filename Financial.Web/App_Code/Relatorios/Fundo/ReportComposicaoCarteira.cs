﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using Financial.Util;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Fundo.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportComposicaoCarteira
    /// </summary>
    public class ReportComposicaoCarteira : XtraReport {

        #region Enum
        public enum TipoRelatorio {
            Abertura = 0,
            Fechamento = 1
        }

        public enum TipoLingua {
            [StringValue("pt-BR")]
            Portugues = 1,

            [StringValue("en-US")]
            Ingles = 2,
        }

        #endregion

        private TipoRelatorio tipoRelatorio;

        public TipoRelatorio TipoRelatorioComposicaoCarteira {
            get { return tipoRelatorio; }
        }

        private TipoLingua tipoLingua;

        public TipoLingua Lingua {
            get { return tipoLingua; }
        }

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idMoeda;

        public int IdMoeda {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        private bool separaCustodia;

        public bool SeparaCustodia
        {
            get { return separaCustodia; }
            set { separaCustodia = value; }
        }


        //private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport3;
        private XRSubreport xrSubreport4;
        private XRSubreport xrSubreport5;
        private XRSubreport xrSubreport6;
        private XRSubreport xrSubreport7;
        private XRSubreport xrSubreport8;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private SubReportAcao subReportAcao1;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCellDataReferencia;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell25;
        private SubReportOpcaoBolsa subReportOpcaoBolsa1;
        private XRSubreport xrSubreport9;
        private XRSubreport xrSubreport10;
        private XRSubreport xrSubreport11;
        private XRSubreport xrSubreport12;
        private XRSubreport xrSubreport14;
        private XRSubreport xrSubreport15;
        private XRSubreport xrSubreport22;
        private SubReportEmprestimoAcao subReportEmprestimoAcao1;
        private SubReportFuturoBMF subReportFuturoBMF1;
        //private SubReportOpcaoDisponivelBMF subReportOpcaoDisponivelBMF1;
        private SubReportCarteiraResumo subReportCarteiraResumo1;
        private SubReportCarteiraResumoProventos subReportCarteiraResumoProventos1;
        private SubReportSaldosCC subReportSaldosCC1;
        //private SubReportCotaInvestimento subReportCotaInvestimento1;
        private SubReportRendaFixa subReportRendaFixa1;
        private SubReportOpcaoFuturoBMF subReportOpcaoFuturoBMF1;
        private XRSubreport xrSubreport16;
        private SubReportLiquidacao subReportLiquidacao1;
        //private SubReportSwap subReportSwap1;
        private SubReportRendaFixaCompromisso subReportRendaFixaCompromisso1;
        private SubReportTermoBolsa subReportTermoBolsa1;
        private XRPageInfo xrPageInfo1;
        private DetailReportBand DetailReport;
        private DetailBand Detail1;
        private XRSubreport xrSubreport17;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport13;
        private XRSubreport xrSubreport2;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport18;
        private SubReportDisponivelBMF subReportDisponivelBMF1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRSubreport xrSubreport19;
        private SubReportLiquidacaoPendente subReportLiquidacaoPendente1;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRSubreport xrSubreport20;
        private SubReportStock subReportStock1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private SubReportSwap subReportSwap1;
        private SubReportOpcaoDisponivelBMF subReportOpcaoDisponivelBMF1;
        private SubReportCotaInvestimento subReportCotaInvestimento1;
        private XRSubreport xrSubreport21;
        private SubReportRendaFixaTermo subReportRendaFixaTermo1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportComposicaoCarteira(int idCarteira, DateTime dataReferencia, TipoRelatorio tipoRelatorio, TipoLingua lingua, int? idMoeda, bool separaCustodia) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            this.tipoRelatorio = tipoRelatorio;
            this.tipoLingua = lingua;
            this.separaCustodia = separaCustodia;
            //

            if (idMoeda.HasValue) { // Moeda passada como Parametro
                this.idMoeda = idMoeda.Value;
            }
            else { //Carrega a moeda do cliente associado                            
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdMoeda);
                cliente.LoadByPrimaryKey(campos, this.idCarteira);
                //
                this.idMoeda = cliente.IdMoeda.Value;
            }

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2744);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (!this.HasData) {
                xrTableCell3.Text = "";
                this.xrSubreport17.Visible = true;
            }
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// Se algum subreport possue dados então o report possue dados
        /// </summary>
        /// <returns></returns>
        public bool HasData {
            get {
                return
                    this.subReportAcao1.HasData ||
                    this.subReportStock1.HasData ||
                    this.subReportOpcaoBolsa1.HasData ||
                    this.subReportTermoBolsa1.HasData ||
                    this.subReportEmprestimoAcao1.HasData ||
                    this.subReportFuturoBMF1.HasData ||
                    this.subReportDisponivelBMF1.HasData ||
                    this.subReportOpcaoDisponivelBMF1.HasData ||
                    this.subReportOpcaoFuturoBMF1.HasData ||
                    this.subReportCotaInvestimento1.HasData ||
                    this.subReportRendaFixa1.HasData ||
                    this.subReportRendaFixaTermo1.HasData ||
                    this.subReportRendaFixaCompromisso1.HasData ||
                    this.subReportSwap1.HasData ||
                    this.subReportLiquidacao1.HasData ||
                    this.subReportLiquidacaoPendente1.HasData ||
                    this.subReportSaldosCC1.HasData ||
                    this.subReportCarteiraResumo1.HasData ||
                    this.subReportCarteiraResumoProventos1.HasData;
            }
        }

        private void PersonalInitialize() {
            //string selectedLanguage = "en-us";

            // Força o Carregamento do Resource de acordo com a Lingua
            string linguaSelecionada = StringEnum.GetStringValue((TipoLingua)this.tipoLingua);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);

            // Passa os parametros para os SubReports
            #region Parametros SubReports

            /* SubReportAcao */
            this.subReportAcao1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport3.Visible = this.subReportAcao1.HasData;

            /* SubReportStock */
            this.subReportStock1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport20.Visible = this.subReportStock1.HasData;

            /* SubReportOpcaoBolsa */
            this.subReportOpcaoBolsa1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport4.Visible = this.subReportOpcaoBolsa1.HasData;

            /* SubReportTermoBolsa */
            this.subReportTermoBolsa1.PersonalInitialize(this.idCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport5.Visible = this.subReportTermoBolsa1.HasData;

            /* SubReportEmprestimoAcao */
            this.subReportEmprestimoAcao1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport6.Visible = this.subReportEmprestimoAcao1.HasData;

            /* SubReportFuturoBMF */
            this.subReportFuturoBMF1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport7.Visible = this.subReportFuturoBMF1.HasData;

            /* SubReportDisponivelBMF */
            this.subReportDisponivelBMF1.PersonalInitialize(this.IdCarteira, this.dataReferencia);
            this.xrSubreport18.Visible = this.subReportDisponivelBMF1.HasData;

            /* SubReportOpcaoDisponivelBMF */
            this.subReportOpcaoDisponivelBMF1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport8.Visible = this.subReportOpcaoDisponivelBMF1.HasData;

            /* SubReportOpcaoFuturoBMF */
            this.subReportOpcaoFuturoBMF1.PersonalInitialize(this.idCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport9.Visible = this.subReportOpcaoFuturoBMF1.HasData;

            /* SubReportCotaInvestimento */
            this.subReportCotaInvestimento1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport10.Visible = this.subReportCotaInvestimento1.HasData;

            /* SubReportRendaFixa  */
            this.subReportRendaFixa1.PersonalInitialize(this.idCarteira, this.dataReferencia, this.idMoeda, this.separaCustodia);
            this.xrSubreport11.Visible = this.subReportRendaFixa1.HasData;

            /* SubReportRendaFixaTermo  */
            this.subReportRendaFixaTermo1.PersonalInitialize(this.idCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport21.Visible = this.subReportRendaFixaTermo1.HasData;

            /* SubReportRendaFixaCompromisso  */
            this.subReportRendaFixaCompromisso1.PersonalInitialize(this.idCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport12.Visible = this.subReportRendaFixaCompromisso1.HasData;

            /* SubReportSwap */
            this.subReportSwap1.PersonalInitialize(this.IdCarteira, this.dataReferencia);
            this.xrSubreport13.Visible = this.subReportSwap1.HasData;

            /* SubReportLiquidacao */
            this.subReportLiquidacao1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport16.Visible = this.subReportLiquidacao1.HasData;

            /* SubReportLiquidacaoPendente */
            this.subReportLiquidacaoPendente1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.idMoeda);
            this.xrSubreport19.Visible = this.subReportLiquidacaoPendente1.HasData;

            /* SubReportSaldosCC */
            this.subReportSaldosCC1.PersonalInitialize(this.IdCarteira, this.dataReferencia, this.tipoRelatorio, this.idMoeda);
            this.xrSubreport14.Visible = this.subReportSaldosCC1.HasData;

            // SubReportCarteiraResumo
            this.subReportCarteiraResumo1.PersonalInitialize(this.IdCarteira, this.dataReferencia);
            //this.subReportCarteiraResumo1.Visible = this.subReportCarteiraResumo1.HasData;

            // SubReportCarteiraResumo
            this.subReportCarteiraResumoProventos1.PersonalInitialize(this.IdCarteira, this.dataReferencia);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCarteira);
            if (carteira.Rendimento.Equals((int)Rendimento.CotaPatrimonial))
            {
                this.subReportCarteiraResumoProventos1.Visible = false;
            }
            //this.subReportCarteiraResumoProventos1.Visible = this.subReportCarteiraResumoProventos1.HasData;

            #endregion

            /* Usado para testes - Verifica um relatorio por vez
             */
            /*
            this.subReportAcao1.Visible = false;
            this.subReportOpcaoBolsa1.Visible = false;
            this.subReportTermoBolsa1.Visible = false;
            this.subReportEmprestimoAcao1.Visible = false;
            this.subReportFuturoBMF1.Visible = false;
            this.subReportDisponivelBMF1.Visible = true;
            this.subReportOpcaoDisponivelBMF1.Visible = false;
            this.subReportOpcaoFuturoBMF1.Visible = false;
            this.subReportCotaInvestimento1.Visible = false;
            this.subReportRendaFixa1.Visible = false;
            this.subReportRendaFixaCompromisso1.Visible = false;
            this.subReportSwap1.Visible = false;
            this.subReportLiquidacao1.Visible = false;
            this.subReportSaldosCC1.Visible = false;
            this.subReportCarteiraResumo1.Visible = false;
            */

            #region Pega Campos do resource
            //
            string tipoRel = this.tipoRelatorio == TipoRelatorio.Abertura
                            ? Resources.ReportComposicaoCarteira._Abertura
                            : Resources.ReportComposicaoCarteira._Fechamento;
            //
            this.xrTableCell5.Text = Resources.ReportComposicaoCarteira._TituloRelatorio + " (" + tipoRel + ")";
            this.xrTableCell22.Text = Resources.ReportComposicaoCarteira._DataPosicao;
            this.xrTableCell25.Text = Resources.ReportComposicaoCarteira._DataEmissao;
            this.xrTableCell3.Text = Resources.ReportComposicaoCarteira._Carteira;
            //this.xrTableCell7.Text = Resources.ReportComposicaoCarteira._TipoRelatorio;
            this.xrTableCell15.Text = Resources.ReportComposicaoCarteira._Moeda;
            this.xrTableCell12.Text = Resources.ReportComposicaoCarteira._Cambio;
            #endregion

            #region Desaparece com cambio se moeda for diferente de Real
            // Cambio é false
            this.xrTableCell12.Visible = false;
            this.xrTableCell18.Visible = false;
            if (this.idMoeda != (int)ListaMoedaFixo.Real) {
                this.xrTableCell12.Visible = true;
                this.xrTableCell18.Visible = true;
            }
            #endregion

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportComposicaoCarteira.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportComposicaoCarteira.resx";
            this.xrSubreport13 = new DevExpress.XtraReports.UI.XRSubreport();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport8 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport7 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportFuturoBMF1 = new Financial.Relatorio.SubReportFuturoBMF();
            this.xrSubreport6 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportEmprestimoAcao1 = new Financial.Relatorio.SubReportEmprestimoAcao();
            this.xrSubreport5 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportTermoBolsa1 = new Financial.Relatorio.SubReportTermoBolsa();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportOpcaoBolsa1 = new Financial.Relatorio.SubReportOpcaoBolsa();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportAcao1 = new Financial.Relatorio.SubReportAcao();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrSubreport17 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataReferencia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubreport16 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLiquidacao1 = new Financial.Relatorio.SubReportLiquidacao();
            this.xrSubreport15 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport22 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportCarteiraResumo1 = new Financial.Relatorio.SubReportCarteiraResumo();
            this.subReportCarteiraResumoProventos1 = new Financial.Relatorio.SubReportCarteiraResumoProventos();
            this.xrSubreport14 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportSaldosCC1 = new Financial.Relatorio.SubReportSaldosCC();
            this.xrSubreport12 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRendaFixaCompromisso1 = new Financial.Relatorio.SubReportRendaFixaCompromisso();
            this.xrSubreport11 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRendaFixa1 = new Financial.Relatorio.SubReportRendaFixa();
            this.xrSubreport10 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport9 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportOpcaoFuturoBMF1 = new Financial.Relatorio.SubReportOpcaoFuturoBMF();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport20 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportStock1 = new Financial.Relatorio.SubReportStock();
            this.xrSubreport19 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLiquidacaoPendente1 = new Financial.Relatorio.SubReportLiquidacaoPendente();
            this.xrSubreport18 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDisponivelBMF1 = new Financial.Relatorio.SubReportDisponivelBMF();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.subReportSwap1 = new Financial.Relatorio.SubReportSwap();
            this.subReportOpcaoDisponivelBMF1 = new Financial.Relatorio.SubReportOpcaoDisponivelBMF();
            this.subReportCotaInvestimento1 = new Financial.Relatorio.SubReportCotaInvestimento();
            this.xrSubreport21 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRendaFixaTermo1 = new Financial.Relatorio.SubReportRendaFixaTermo();
            ((System.ComponentModel.ISupportInitialize)(this.subReportFuturoBMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportEmprestimoAcao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportTermoBolsa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoBolsa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportAcao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLiquidacao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCarteiraResumo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCarteiraResumoProventos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportSaldosCC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixaCompromisso1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoFuturoBMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportStock1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLiquidacaoPendente1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDisponivelBMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportSwap1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoDisponivelBMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCotaInvestimento1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixaTermo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrSubreport13
            // 
            this.xrSubreport13.Dpi = 254F;
            this.xrSubreport13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1300F);
            this.xrSubreport13.Name = "xrSubreport13";
            this.xrSubreport13.ReportSource = this.subReportSwap1;
            this.xrSubreport13.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceSwap);
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrSubreport8
            // 
            this.xrSubreport8.Dpi = 254F;
            this.xrSubreport8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 700F);
            this.xrSubreport8.Name = "xrSubreport8";
            this.xrSubreport8.ReportSource = this.subReportOpcaoDisponivelBMF1;
            this.xrSubreport8.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceDisponivelBMF);
            // 
            // xrSubreport7
            // 
            this.xrSubreport7.Dpi = 254F;
            this.xrSubreport7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 500F);
            this.xrSubreport7.Name = "xrSubreport7";
            this.xrSubreport7.ReportSource = this.subReportFuturoBMF1;
            this.xrSubreport7.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceFuturoBMF);
            // 
            // xrSubreport6
            // 
            this.xrSubreport6.Dpi = 254F;
            this.xrSubreport6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 400F);
            this.xrSubreport6.Name = "xrSubreport6";
            this.xrSubreport6.ReportSource = this.subReportEmprestimoAcao1;
            this.xrSubreport6.SizeF = new System.Drawing.SizeF(645F, 99.99997F);
            this.xrSubreport6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceEmprestimoAcao);
            // 
            // xrSubreport5
            // 
            this.xrSubreport5.Dpi = 254F;
            this.xrSubreport5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 300F);
            this.xrSubreport5.Name = "xrSubreport5";
            this.xrSubreport5.ReportSource = this.subReportTermoBolsa1;
            this.xrSubreport5.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceTermoBolsa);
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 200F);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportOpcaoBolsa1;
            this.xrSubreport4.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceOpcaoBolsa);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportAcao1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(645F, 100F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrSubreport2,
            this.xrSubreport17,
            this.xrPageInfo1,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 175F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(25F, 10F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1661.533F, 40F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.57014385610578666;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "#TituloRelatorio + (Tipo)";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.5423356058077734;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(1768F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1001F, 95.00002F);
            // 
            // xrSubreport17
            // 
            this.xrSubreport17.Dpi = 254F;
            this.xrSubreport17.LocationFloat = new DevExpress.Utils.PointFloat(100F, 170F);
            this.xrSubreport17.Name = "xrSubreport17";
            this.xrSubreport17.ReportSource = this.reportSemDados1;
            this.xrSubreport17.SizeF = new System.Drawing.SizeF(30F, 5.000015F);
            this.xrSubreport17.Visible = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2706F, 108F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(63F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrTable2,
            this.xrTable8,
            this.xrPageInfo3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 50.00002F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1661.533F, 115F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:d}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(226F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 43F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1497F, 70F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCellDataReferencia,
            this.xrTableCell15,
            this.xrTableCell9,
            this.xrTableCell12,
            this.xrTableCell18});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.27559055118110232;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.CanGrow = false;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Text = "#DataPosicao";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.15103793843951324;
            // 
            // xrTableCellDataReferencia
            // 
            this.xrTableCellDataReferencia.CanGrow = false;
            this.xrTableCellDataReferencia.Dpi = 254F;
            this.xrTableCellDataReferencia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataReferencia.Name = "xrTableCellDataReferencia";
            this.xrTableCellDataReferencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataReferencia.Text = "DataReferencia";
            this.xrTableCellDataReferencia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataReferencia.Weight = 0.18819241919773688;
            this.xrTableCellDataReferencia.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "#Moeda";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.12740417604476434;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Moeda";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.16948834438143762;
            this.xrTableCell9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MoedaBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "#Cambio";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.16891250886406553;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Cambio";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.19496461307248234;
            this.xrTableCell18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CambioBeforePrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.27559055118110237;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#Carteira";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.15103793843951324;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "Carteira";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.8489620615604867;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(226F, 42F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.CanGrow = false;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#DataEmissao";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell25.Weight = 1.0660377358490567;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(374F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport16
            // 
            this.xrSubreport16.Dpi = 254F;
            this.xrSubreport16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1400F);
            this.xrSubreport16.Name = "xrSubreport16";
            this.xrSubreport16.ReportSource = this.subReportLiquidacao1;
            this.xrSubreport16.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceLiquidacao);
            // 
            // xrSubreport22
            // 
            this.xrSubreport22.Dpi = 254F;
            this.xrSubreport22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1800F);
            this.xrSubreport22.Name = "xrSubreport22";
            this.xrSubreport22.ReportSource = this.subReportCarteiraResumoProventos1;
            this.xrSubreport22.SizeF = new System.Drawing.SizeF(645F, 50F);
            // 
            // xrSubreport15
            // 
            this.xrSubreport15.Dpi = 254F;
            this.xrSubreport15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1700F);
            this.xrSubreport15.Name = "xrSubreport15";
            this.xrSubreport15.ReportSource = this.subReportCarteiraResumo1;
            this.xrSubreport15.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceCarteiraResumo);
            // 
            // xrSubreport14
            // 
            this.xrSubreport14.Dpi = 254F;
            this.xrSubreport14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1600F);
            this.xrSubreport14.Name = "xrSubreport14";
            this.xrSubreport14.ReportSource = this.subReportSaldosCC1;
            this.xrSubreport14.SizeF = new System.Drawing.SizeF(645F, 99.99988F);
            this.xrSubreport14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceSaldosCC);
            // 
            // xrSubreport12
            // 
            this.xrSubreport12.Dpi = 254F;
            this.xrSubreport12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1200F);
            this.xrSubreport12.Name = "xrSubreport12";
            this.xrSubreport12.ReportSource = this.subReportRendaFixaCompromisso1;
            this.xrSubreport12.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceRendaFixaCompromisso);
            // 
            // xrSubreport11
            // 
            this.xrSubreport11.Dpi = 254F;
            this.xrSubreport11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 999.9999F);
            this.xrSubreport11.Name = "xrSubreport11";
            this.xrSubreport11.ReportSource = this.subReportRendaFixa1;
            this.xrSubreport11.SizeF = new System.Drawing.SizeF(645F, 100.0001F);
            this.xrSubreport11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceRendaFixa);
            // 
            // xrSubreport10
            // 
            this.xrSubreport10.Dpi = 254F;
            this.xrSubreport10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 900F);
            this.xrSubreport10.Name = "xrSubreport10";
            this.xrSubreport10.ReportSource = this.subReportCotaInvestimento1;
            this.xrSubreport10.SizeF = new System.Drawing.SizeF(645F, 99.99994F);
            this.xrSubreport10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceCotaInvestimento);
            // 
            // xrSubreport9
            // 
            this.xrSubreport9.Dpi = 254F;
            this.xrSubreport9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 800F);
            this.xrSubreport9.Name = "xrSubreport9";
            this.xrSubreport9.ReportSource = this.subReportOpcaoFuturoBMF1;
            this.xrSubreport9.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceOpcaoFuturoBMF);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 102F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(645F, 100F);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DetailReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport21,
            this.xrSubreport20,
            this.xrSubreport19,
            this.xrSubreport18,
            this.xrSubreport6,
            this.xrSubreport4,
            this.xrSubreport5,
            this.xrSubreport3,
            this.xrSubreport7,
            this.xrSubreport15,
            this.xrSubreport22,
            this.xrSubreport8,
            this.xrSubreport9,
            this.xrSubreport10,
            this.xrSubreport11,
            this.xrSubreport12,
            this.xrSubreport13,
            this.xrSubreport16,
            this.xrSubreport14});
            this.Detail1.Dpi = 254F;
            this.Detail1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail1.HeightF = 1800F;
            this.Detail1.Name = "Detail1";
            this.Detail1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail1.StylePriority.UseFont = false;
            this.Detail1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport20
            // 
            this.xrSubreport20.Dpi = 254F;
            this.xrSubreport20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrSubreport20.Name = "xrSubreport20";
            this.xrSubreport20.ReportSource = this.subReportStock1;
            this.xrSubreport20.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceStocks);
            // 
            // xrSubreport19
            // 
            this.xrSubreport19.Dpi = 254F;
            this.xrSubreport19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1500F);
            this.xrSubreport19.Name = "xrSubreport19";
            this.xrSubreport19.ReportSource = this.subReportLiquidacaoPendente1;
            this.xrSubreport19.SizeF = new System.Drawing.SizeF(645F, 100.0001F);
            this.xrSubreport19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceLiquidacaoPendente);
            // 
            // xrSubreport18
            // 
            this.xrSubreport18.Dpi = 254F;
            this.xrSubreport18.LocationFloat = new DevExpress.Utils.PointFloat(0F, 600F);
            this.xrSubreport18.Name = "xrSubreport18";
            this.xrSubreport18.ReportSource = this.subReportDisponivelBMF1;
            this.xrSubreport18.SizeF = new System.Drawing.SizeF(645F, 100F);
            this.xrSubreport18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceDisponivelBMF);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrSubreport21
            // 
            this.xrSubreport21.Dpi = 254F;
            this.xrSubreport21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1100F);
            this.xrSubreport21.Name = "xrSubreport21";
            this.xrSubreport21.ReportSource = this.subReportRendaFixaTermo1;
            this.xrSubreport21.SizeF = new System.Drawing.SizeF(645F, 100.0001F);
            this.xrSubreport21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SpaceRendaFixaTermo);
            // 
            // ReportComposicaoCarteira
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.DetailReport,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportFuturoBMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportEmprestimoAcao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportTermoBolsa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoBolsa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportAcao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLiquidacao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCarteiraResumo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCarteiraResumoProventos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportSaldosCC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixaCompromisso1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoFuturoBMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportStock1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLiquidacaoPendente1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDisponivelBMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportSwap1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportOpcaoDisponivelBMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCotaInvestimento1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRendaFixaTermo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportComposicaoCarteira.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaTableCell = sender as XRTableCell;
            dataReferenciaTableCell.Text = this.dataReferencia.ToString("d");
        }

        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCarteira = sender as XRTableCell;
            xrTableCellCarteira.Text = "";
            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            if (carteira.LoadByPrimaryKey(campos, this.idCarteira)) {
                xrTableCellCarteira.Text = this.idCarteira + " - " + carteira.Nome;
            }
        }
        
        private void MoedaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell moedaTableCell = sender as XRTableCell;
            //
            //MoedaQuery moedaQuery = new MoedaQuery("M");
            //ClienteQuery clienteQuery = new ClienteQuery("C");
            ////
            //moedaQuery.Select(moedaQuery.Nome);
            //moedaQuery.InnerJoin(clienteQuery).On(moedaQuery.IdMoeda == clienteQuery.IdMoeda);
            //moedaQuery.Where(clienteQuery.IdCliente == this.idCarteira);

            //Moeda m = new Moeda();
            //m.Load(moedaQuery);
            //
            Moeda m = new Moeda();
            m.Query.Where(m.Query.IdMoeda == this.IdMoeda);
            m.Query.Load();
            //
            moedaTableCell.Text = m.str.Nome.Trim();
        }

        private void CambioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell cambioTableCell = sender as XRTableCell;
                      
            #region Fator de Conversão
            decimal ptax = 0;
            decimal fatorConversao = 0.0M;

            if (this.idMoeda == (int)ListaMoedaFixo.Dolar) {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, this.dataReferencia);
            }

            if (this.idMoeda != (int)ListaMoedaFixo.Real) {
                fatorConversao = 1;

                if (idMoeda == (int)ListaMoedaFixo.Dolar) {
                    fatorConversao = 1 / ptax;
                }
                else {
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe == this.idMoeda,
                                               conversaoMoeda.Query.IdMoedaPara == (int)ListaMoedaFixo.Real);
                    
                    if (conversaoMoeda.Query.Load()) {
                        int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                    }
                }
            }
            #endregion

            cambioTableCell.Text = fatorConversao.ToString("n4");
        }
        #endregion

        #region xrSubreport.Visible = false DevXpress 15.2.7 Ocupa Espaço

        private void SpaceStocks(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 100;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceOpcaoBolsa(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 200;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceTermoBolsa(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 300;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceEmprestimoAcao(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 400;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceFuturoBMF(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 500;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceDisponivelBMF(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 600;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceOpcaoDisponivelBMF(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 700;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceOpcaoFuturoBMF(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 800;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceCotaInvestimento(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 900;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceRendaFixa(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1000;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceRendaFixaTermo(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1100;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceRendaFixaCompromisso(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1200;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceSwap(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1300;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaCompromisso1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceLiquidacao(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1400;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaCompromisso1.HasData) {
                y -= 100;
            }
            if (!this.subReportSwap1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceLiquidacaoPendente(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1500;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaCompromisso1.HasData) {
                y -= 100;
            }
            if (!this.subReportSwap1.HasData) {
                y -= 100;
            }
            if (!this.subReportLiquidacao1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceSaldosCC(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1600;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaCompromisso1.HasData) {
                y -= 100;
            }
            if (!this.subReportSwap1.HasData) {
                y -= 100;
            }
            if (!this.subReportLiquidacao1.HasData) {
                y -= 100;
            }
            if (!this.subReportLiquidacaoPendente1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        }
        private void SpaceCarteiraResumo(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRSubreport a = sender as XRSubreport;
            float y = 1700;

            if (!this.subReportAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportStock1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportTermoBolsa1.HasData) {
                y -= 100;
            }
            if (!this.subReportEmprestimoAcao1.HasData) {
                y -= 100;
            }
            if (!this.subReportFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoDisponivelBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportOpcaoFuturoBMF1.HasData) {
                y -= 100;
            }
            if (!this.subReportCotaInvestimento1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixa1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaTermo1.HasData) {
                y -= 100;
            }
            if (!this.subReportRendaFixaCompromisso1.HasData) {
                y -= 100;
            }
            if (!this.subReportSwap1.HasData) {
                y -= 100;
            }
            if (!this.subReportLiquidacao1.HasData) {
                y -= 100;
            }
            if (!this.subReportLiquidacaoPendente1.HasData) {
                y -= 100;
            }
            if (!this.subReportSaldosCC1.HasData) {
                y -= 100;
            }
            a.LocationF = new DevExpress.Utils.PointFloat(0F, y);
        } 
        #endregion
    }
}