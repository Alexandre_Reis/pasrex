﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportDemonstrativoReceita
    /// </summary>
    public class ReportDemonstrativoReceita : XtraReport {

        private DateTime data;

        private string login;

        private bool taxaPerformancePositivo;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell19;
        private SubReportRodape subReportRodape1;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell7;
        private GroupFooterBand GroupFooter1;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell5;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo1;
        private XRControlStyle xrControlStyle1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;       

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        //

        public ReportDemonstrativoReceita(DateTime data, bool taxaPerformancePositivo, string login) {
            this.data = data;
            this.login = login;
            this.taxaPerformancePositivo = taxaPerformancePositivo;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1860);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.xrTable5.Visible = false;
                this.xrTable3.Visible = false;
                this.GroupHeader1.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.ReportDemonstrativoReceita._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportDemonstrativoReceita._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportDemonstrativoReceita._DataPosicao;
            this.xrTableCell9.Text = Resources.ReportDemonstrativoReceita._Carteira;
            this.xrTableCell28.Text = Resources.ReportDemonstrativoReceita._TaxaAdministracao;
            this.xrTableCell3.Text = Resources.ReportDemonstrativoReceita._TaxaGestao;
            this.xrTableCell26.Text = Resources.ReportDemonstrativoReceita._TaxaPerformance;
            #endregion
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();
            string tipoControle = (int)TipoControleCliente.CarteiraRentabil + ", " + (int)TipoControleCliente.Completo;

            esParameters esParams = new esParameters();
            esParams.Add("Data", this.data);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine("Select C.IdCarteira, ");
            sqlText.AppendLine("       C.Nome, ");
            sqlText.AppendLine("       T.Descricao, ");
            sqlText.AppendLine("       E.DataImplantacao, ");
            sqlText.AppendLine("       E.DataDia ");
            sqlText.AppendLine("FROM   Carteira C, ");
            sqlText.AppendLine("       Cliente E, ");
            sqlText.AppendLine("       TipoCliente T, ");
            sqlText.AppendLine("       [PermissaoCliente] P, ");
            sqlText.AppendLine("       [Usuario] U ");
            sqlText.AppendLine("WHERE  C.IdCarteira = E.IdCliente ");
            sqlText.AppendLine("       AND E.StatusAtivo = " + (int)StatusAtivoCliente.Ativo);
            sqlText.AppendLine("       AND E.IdTipo = T.IdTipo ");
            sqlText.AppendLine("       AND E.TipoControle In (" + tipoControle + ") ");
            sqlText.AppendLine("       AND E.idCliente = C.IdCarteira ");
            sqlText.AppendLine("       AND P.idCliente = E.idCliente ");
            sqlText.AppendLine("       AND P.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND E.DataImplantacao <= @Data ");
            sqlText.AppendLine("       AND E.DataDia >= @Data ");
            sqlText.AppendLine("       AND U.login = '" + this.login + "'");

            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            //string filter = "E.DataImplantacao >= '" + this.data + "' And E.dataDia <= '" + this.data + "'";         
            //dt.DefaultView.RowFilter = filter;

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/ReportDemonstrativoReceita.xml");

            //return dt.DefaultView;

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportDemonstrativoReceita.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportDemonstrativoReceita.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 45F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.OddStyleName = "xrControlStyle1";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1859F, 40F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell15,
            this.xrTableCell7,
            this.xrTableCell19});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "Carteira";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.51533082302313071;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "TaxaAdministracao";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.13663259817105972;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TaxaAdministracaoBeforePrint);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "TaxaGestao";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.16083916083916083;
            this.xrTableCell7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TaxaGestaoBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "TaxaPerformance";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.18719741796664874;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TaxaPerformanceBeforePrint);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(804F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1122F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 200F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1859F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell28,
            this.xrTableCell3,
            this.xrTableCell26});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Carteira";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.51533082302313071;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#TaxaAdministracao";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.13663259817105972;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#TaxaGestao";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.16083916083916083;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#TaxaPerformance";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.18719741796664874;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 87F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(619F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrPanel1,
            this.xrTable7,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 252F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(55.87498F, 197F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(619F, 64F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1820F, 90F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(135F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 90F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 85F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(349F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(215F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(130F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(466F, 43F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.45493562231759654;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.54506437768240346;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 40F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderBeforePrint);
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1859F, 40F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Descricao")});
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseBackColor = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 56F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1859F, 40F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalPorGrupoBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell11,
            this.xrTableCell8,
            this.xrTableCell12});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#NumeroCarteiras";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 0.51264120494889731;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "TaxaAdministracaoPorGrupo";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.13932221624529317;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "TaxaGestaoPorGrupo";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.16083916083916083;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "TaxaPerformancePorGrupo";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.18719741796664874;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.ReportFooter.HeightF = 42F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.LightGray;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1859F, 40F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell17});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#TotalCarteiras";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.51533082302313071;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "TotalTaxaAdministracao";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.13663259817105972;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "TotalTaxaGestao";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.16083916083916083;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "TotalTaxaPerformance";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.18719741796664874;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportDemonstrativoReceita
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportDemonstrativoReceita.ResourceManager;
        }

        // Controla a Quebra de Pagina do Grupo Descrição
        private string descricaoGrupo = "";

        // Colunas da Tabela        
        private string IdCarteiraColumn = "IdCarteira";
        private string NomeCarteiraColumn = "Nome";

        private int totalCarteirasPorGrupo = 0;
        private int totalCarteiras = 0;
        //
        private decimal totalTaxaAdministracaoPorGrupo = 0;
        private decimal totalTaxaGestaoPorGrupo = 0;
        private decimal totalTaxaPerformancePorGrupo = 0;
        //        
        private List<decimal> totalTaxaAdministracao = new List<decimal>();
        private List<decimal> totalTaxaGestao = new List<decimal>();
        private List<decimal> totalTaxaPerformance = new List<decimal>();

        //     
        #region Funções Internas do Relatorio
        //
        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.data.ToString("d");
        }

        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeCarteiraXRTableCell = sender as XRTableCell;
            nomeCarteiraXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                string nomeCarteira = (string)this.GetCurrentColumnValue(this.NomeCarteiraColumn);
                if (nomeCarteira.Length > 45) {
                    nomeCarteira = nomeCarteira.Substring(0, 44);
                }

                string carteira = idCarteira + "-" + nomeCarteira;
                nomeCarteiraXRTableCell.Text = carteira;

                // Totaliza Total Carteira
                this.totalCarteirasPorGrupo++;
                this.totalCarteiras++;
            }
        }

        private void TaxaAdministracaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell taxaAdministracaoXRTableCell = sender as XRTableCell;
            taxaAdministracaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                CalculoAdministracaoHistorico calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
                decimal valor = calculoAdministracaoHistorico.RetornaValorAdministracao(idCarteira,
                                                    this.data, (int)TipoCadastroAdministracao.TaxaAdministracao);
                taxaAdministracaoXRTableCell.Text = valor.ToString("n2");

                // Totaliza Taxa Administracao
                this.totalTaxaAdministracaoPorGrupo += valor;
                this.totalTaxaAdministracao.Add(valor);
            }
        }

        private void TaxaGestaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell taxaGestaoXRTableCell = sender as XRTableCell;
            taxaGestaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                CalculoAdministracaoHistorico calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
                decimal valor = calculoAdministracaoHistorico.RetornaValorAdministracao(idCarteira,
                                                    this.data, (int)TipoCadastroAdministracao.TaxaGestao);
                taxaGestaoXRTableCell.Text = valor.ToString("n2");

                // Totaliza Taxa Gestão
                this.totalTaxaGestaoPorGrupo += valor;
                this.totalTaxaGestao.Add(valor);
            }
        }

        private void TaxaPerformanceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell taxaPerformanceXRTableCell = sender as XRTableCell;
            taxaPerformanceXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();

                decimal valor = calculoPerformanceHistorico.RetornaValorPerformance(idCarteira, this.data, this.taxaPerformancePositivo);                
                taxaPerformanceXRTableCell.Text = valor.ToString("n2");

                // Totaliza Taxa Performance
                this.totalTaxaPerformancePorGrupo += valor;
                this.totalTaxaPerformance.Add(valor);
            }
        }

        private void GroupHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Só Zerar quando não for quebra de Pagina
            const string descricaoColumn = "Descricao";
            //
            string controleDescricaoGrupo = (string)this.GetCurrentColumnValue(descricaoColumn);

            if (controleDescricaoGrupo != this.descricaoGrupo) {
                // Salva a Descrição do Grupo Atual
                this.descricaoGrupo = controleDescricaoGrupo;

                #region Zera Totais Por Grupo
                // Zera o total de Carteiras no Grupo
                this.totalCarteirasPorGrupo = 0;

                // Zera TotalTaxaAdministracao Por Grupo
                this.totalTaxaAdministracaoPorGrupo = 0;
                //
                // Zera TotalTaxaGestao Por Grupo
                this.totalTaxaGestaoPorGrupo = 0;
                //
                // Zera TotalTaxaPerformance Por Grupo
                this.totalTaxaPerformancePorGrupo = 0;

                #endregion
            }
        }

        #region Calcula e Exibe Totais
        private void TableTotalPorGrupoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            /* TotalCarteirasPorGrupo
               TotalTaxaAdministracaoPorGrupo
               TotalTaxaGestaoPorGrupo
               TotalTaxaPerformancePorGrupo                             
             */
            string carteirasGrupo = Resources.ReportDemonstrativoReceita._TotalCarteiras;

            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = carteirasGrupo + " " + this.totalCarteirasPorGrupo.ToString();
            ((XRTableCell)summaryFinalRow0.Cells[1]).Text = this.totalTaxaAdministracaoPorGrupo.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = this.totalTaxaGestaoPorGrupo.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[3]).Text = this.totalTaxaPerformancePorGrupo.ToString("n2");
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            /* TotalCarteiras
               TotalTaxaAdministracao
               TotalTaxaGestao
               TotalTaxaPerformance
             */

            // CalculaTotais - Percorre Lista com todos os valores Parcias

            #region CalculaTotais
            decimal totalTaxaAdministracaoAux = 0;
            decimal totalTaxaGestaoAux = 0;
            decimal totalTaxaPerformanceAux = 0;
            for (int i = 0; i < this.totalTaxaAdministracao.Count; i++) {
                totalTaxaAdministracaoAux += this.totalTaxaAdministracao[i];
            }
            for (int i = 0; i < this.totalTaxaGestao.Count; i++) {
                totalTaxaGestaoAux += this.totalTaxaGestao[i];
            }
            for (int i = 0; i < this.totalTaxaPerformance.Count; i++) {
                totalTaxaPerformanceAux += this.totalTaxaPerformance[i];
            }
            #endregion

            string totalCarteiras = Resources.ReportDemonstrativoReceita._TotalCarteiras;
            //
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = totalCarteiras + this.totalCarteiras.ToString();
            ((XRTableCell)summaryFinalRow0.Cells[1]).Text = totalTaxaAdministracaoAux.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = totalTaxaGestaoAux.ToString("n2");
            ((XRTableCell)summaryFinalRow0.Cells[3]).Text = totalTaxaPerformanceAux.ToString("n2");
        }
        #endregion

        #endregion
    }
}