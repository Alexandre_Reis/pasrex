﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Investidor.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Fundo.Exceptions;
using Financial.InvestidorCotista;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common.Exceptions;
using System.Text;
using log4net;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportQuadroRetorno
    /// </summary>
    public class ReportQuadroRetorno : XtraReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportQuadroRetorno));

        private DateTime dataReferencia;

        private string login;

        public DateTime DataReferencia
        {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private DateTime dataInicioCliente;

        // Armazena a carteira de cada linha do relatorio
        private Carteira carteira = new Carteira();
        //private int tipoClientePorGrupo = 0;
        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell34;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell69;
        private GroupHeaderBand GroupHeader2;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell72;
        private XRTableCell xrTableCell73;
        private XRTableRow xrTableRow16;
        private XRTableCell xrTableCell94;
        private XRTableCell xrTableCell95;
        private XRTableCell xrTableCell96;
        private XRTableCell xrTableCell98;
        private XRTableCell xrTableCell99;
        private XRTableCell xrTableCell100;
        private XRTableCell xrTableCell101;
        private XRTableCell xrTableCell102;
        private XRTableCell xrTableCell103;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell74;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell22;
        private XRControlStyle xrControlStyle1;
        private XRPageInfo xrPageInfo1;
        private XRControlStyle xrControlStyle2;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell35;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private ReportSemDados reportSemDados1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportQuadroRetorno(DateTime dataReferencia, string login)
        {
            this.dataReferencia = dataReferencia;
            this.login = login;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2450);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (this.numeroLinhasDataTable == 0)
            {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.GroupHeader1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.ReportFooter.Visible = false;

            }
        }

        private void PersonalInitialize()
        {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do Resource
            this.xrTableCell55.Text = Resources.ReportQuadroRetorno._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportQuadroRetorno._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportQuadroRetorno._DataPosicao;
            this.xrTableCell25.Text = Resources.ReportQuadroRetorno._Carteira;
            this.xrTableCell26.Text = Resources.ReportQuadroRetorno._BenchMark;
            this.xrTableCell5.Text = Resources.ReportQuadroRetorno._ValorCota;
            this.xrTableCell3.Text = Resources.ReportQuadroRetorno._ValorPL;
            this.xrTableCell27.Text = Resources.ReportQuadroRetorno._Dia;
            this.xrTableCell34.Text = Resources.ReportQuadroRetorno._Mes;
            this.xrTableCell2.Text = Resources.ReportQuadroRetorno._Ano;
            this.xrTableCell9.Text = Resources.ReportQuadroRetorno._6Meses;
            this.xrTableCell15.Text = Resources.ReportQuadroRetorno._12Meses;
            this.xrTableCell28.Text = Resources.ReportQuadroRetorno._24Meses;
            this.xrTableCell79.Text = Resources.ReportQuadroRetorno._Volatilidade;
            #endregion
        }

        private DataTable FillDados()
        {
            esUtility u = new esUtility();
            string tipoControle = (int)TipoControleCliente.CarteiraRentabil + ", " + (int)TipoControleCliente.Completo;

            esParameters esParams = new esParameters();
            esParams.Add("Data", this.dataReferencia);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine("SELECT C.IdCarteira, \n");
            sqlText.AppendLine("       C.Apelido as NomeCarteira, ");
            sqlText.AppendLine("       I.Descricao as DescricaoIndice, ");
            sqlText.AppendLine("       T.Descricao as TipoCliente, ");
            sqlText.AppendLine("       H.CotaAbertura, ");
            sqlText.AppendLine("       H.CotaFechamento, ");
            sqlText.AppendLine("       H.PLAbertura, ");
            sqlText.AppendLine("       H.PLFechamento, ");
            sqlText.AppendLine("       H.QuantidadeFechamento, ");
            sqlText.AppendLine("       I.Tipo as TipoIndice ");
            sqlText.AppendLine(" FROM  [HistoricoCota] H, ");
            sqlText.AppendLine("       [Carteira] C, ");
            sqlText.AppendLine("       [Indice] I, ");
            sqlText.AppendLine("       [Cliente] E, ");
            sqlText.AppendLine("       [TipoCliente] T, ");
            sqlText.AppendLine("       [PermissaoCliente] P, ");
            sqlText.AppendLine("       [Usuario] U ");
            sqlText.AppendLine(" WHERE H.IdCarteira = C.IdCarteira ");
            sqlText.AppendLine("       AND C.IdIndiceBenchmark = I.IdIndice ");
            sqlText.AppendLine("       AND C.IdCarteira = E.IdCliente ");
            sqlText.AppendLine("       AND E.IdTipo = T.IdTipo ");
            sqlText.AppendLine("       AND E.StatusAtivo = " + (int)StatusAtivoCliente.Ativo);
            sqlText.AppendLine("       AND E.TipoControle In (" + tipoControle + ") ");
            sqlText.AppendLine("       AND H.Data = @Data");
            sqlText.AppendLine("       AND E.idCliente = C.IdCarteira ");
            sqlText.AppendLine("       AND P.idCliente = E.idCliente ");
            sqlText.AppendLine("       AND P.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND E.DataImplantacao <= @Data");
            sqlText.AppendLine("       AND E.DataDia >= @Data");
            sqlText.AppendLine("       AND U.login = '" + this.login + "'");

            sqlText.AppendLine(" UNION ALL ");

            tipoControle = (int)TipoControleCliente.ApenasCotacao + ", " + (int)TipoControleCliente.Cotista;
            sqlText = new StringBuilder();
            sqlText.AppendLine("SELECT C.IdCarteira, \n");
            sqlText.AppendLine("       C.Apelido as NomeCarteira, ");
            sqlText.AppendLine("       I.Descricao as DescricaoIndice, ");
            sqlText.AppendLine("       T.Descricao as TipoCliente, ");
            sqlText.AppendLine("       H.CotaAbertura, ");
            sqlText.AppendLine("       H.CotaFechamento, ");
            sqlText.AppendLine("       H.PLAbertura, ");
            sqlText.AppendLine("       H.PLFechamento, ");
            sqlText.AppendLine("       H.QuantidadeFechamento, ");
            sqlText.AppendLine("       I.Tipo as TipoIndice ");
            sqlText.AppendLine(" FROM  [HistoricoCota] H, ");
            sqlText.AppendLine("       [Carteira] C, ");
            sqlText.AppendLine("       [Indice] I, ");
            sqlText.AppendLine("       [Cliente] E, ");
            sqlText.AppendLine("       [TipoCliente] T ");
            sqlText.AppendLine(" WHERE H.IdCarteira = C.IdCarteira ");
            sqlText.AppendLine("       AND C.IdIndiceBenchmark = I.IdIndice ");
            sqlText.AppendLine("       AND C.IdCarteira = E.IdCliente ");
            sqlText.AppendLine("       AND E.IdTipo = T.IdTipo ");
            sqlText.AppendLine("       AND E.StatusAtivo = " + (int)StatusAtivoCliente.Ativo);
            sqlText.AppendLine("       AND E.TipoControle In (" + tipoControle + ") ");
            sqlText.AppendLine("       AND H.Data = @Data");
            sqlText.AppendLine("       AND E.idCliente = C.IdCarteira ");            
            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            log.Info(sqlText.ToString());
            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/ReportQuadroRetorno.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportQuadroRetorno.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportQuadroRetorno.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Detail.HeightF = 93F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.OddStyleName = "xrControlStyle1";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow16});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2450F, 92F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailRentabilidadeBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell11,
            this.xrTableCell17,
            this.xrTableCell16,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.5;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.CanGrow = false;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "Carteira";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.17510204081632652;
            this.xrTableCell21.WordWrap = false;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DescricaoIndice")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.12979591836734694;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Cota";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.11183673469387755;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotaBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Patrimonio";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.077959183673469393;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PatrimonioBeforePrint);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "#RentabilidadeDia";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.095102040816326533;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "RentabilidadeMes";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.068979591836734688;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "RentabilidadeAno";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.068979591836734688;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "Rentabilidade6Meses";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.069387755102040816;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell72.Text = "#Rentabilidade12Meses";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell72.Weight = 0.068979591836734688;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.Text = "Rentabilidade24Meses";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell73.Weight = 0.068979591836734688;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.Text = "Volatilidade";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell74.Weight = 0.06489795918367347;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell75});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow16.Weight = 0.5;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell94.Weight = 0.17510204081632652;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell95.Weight = 0.12979591836734694;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.CanGrow = false;
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell96.Text = "#RetornosDiferenciais";
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell96.Weight = 0.18816326530612246;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell98.Text = "#RentabilidadeDia";
            this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell98.Weight = 0.097551020408163269;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell99.Text = "RentabilidadeMes";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell99.Weight = 0.068979591836734688;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell100.Text = "RentabilidadeAno";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell100.Weight = 0.068979591836734688;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell101.Text = "Rentabilidade6Meses";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell101.Weight = 0.069387755102040816;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell102.Text = "Rentabilidade12Meses";
            this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell102.Weight = 0.068979591836734688;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell103.Text = "Rentabilidade24Meses";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell103.Weight = 0.068979591836734688;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell75.Weight = 0.06408163265306123;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 106F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(355F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(106F, 40F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(135F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(529F, 43F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.40075614366729678;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.59924385633270316;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1651F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell35.Weight = 0.14112658994548757;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.85887341005451245;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(101F, 212F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell2,
            this.xrTableCell9,
            this.xrTableCell15,
            this.xrTableCell28,
            this.xrTableCell79});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Carteira";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.17469387755102042;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#BenchMark";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.12979591836734694;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#ValorCota";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.11224489795918367;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#ValorPL";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.077959183673469393;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "#Dia";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.095102040816326533;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "#Mes";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.068979591836734688;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "#Ano";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.068979591836734688;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#6Meses";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.069387755102040816;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "#12Meses";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.068979591836734688;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#24Meses";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.068979591836734688;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.Text = "#Volatilidade";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell79.Weight = 0.06489795918367347;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageFooter.HeightF = 66F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(381F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrPanel1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeader.HeightF = 259F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(72F, 230F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(5F, 5F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2490F, 87F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(64F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TipoCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.LightGray;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.TipoCliente")});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 1;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 204F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 20F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow9,
            this.xrTableRow11});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2450F, 183F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableIndiceBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.BackColor = System.Drawing.Color.LightGray;
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell60,
            this.xrTableCell53,
            this.xrTableCell66,
            this.xrTableCell7});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.25136612021857924;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell23.Weight = 0.3130612244897959;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "#Dia";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell24.Weight = 0.086530612244897956;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Text = "#Mes";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell29.Weight = 0.095102040816326533;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Text = "#Ano";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell30.Weight = 0.095102040816326533;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.Text = "#6Meses";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell60.Weight = 0.0946938775510204;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Text = "#12Meses";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell53.Weight = 0.1036734693877551;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.Text = "#24Meses";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell66.Weight = 0.1036734693877551;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.10816326530612246;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell18,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell61,
            this.xrTableCell54,
            this.xrTableCell67,
            this.xrTableCell8});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.25136612021857924;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "#RetornoCDI";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.3130612244897959;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.086530612244897956;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.095102040816326533;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.095102040816326533;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.0946938775510204;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell54.Weight = 0.1036734693877551;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell67.Weight = 0.1036734693877551;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.10816326530612246;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.BackColor = System.Drawing.Color.LightGray;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell62,
            this.xrTableCell56,
            this.xrTableCell68,
            this.xrTableCell20});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.24590163934426229;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Text = "#RetornoIbovMedio";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell33.Weight = 0.3130612244897959;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.086530612244897956;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.095102040816326533;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell40.Weight = 0.095102040816326533;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell62.Weight = 0.0946938775510204;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.1036734693877551;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell68.Weight = 0.1036734693877551;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 0.10816326530612246;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell63,
            this.xrTableCell57,
            this.xrTableCell69,
            this.xrTableCell22});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.25136612021857924;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "#RetornoDolarPtax800Venda";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.3130612244897959;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.086530612244897956;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.095102040816326533;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.095102040816326533;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell63.Weight = 0.0946938775510204;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.1036734693877551;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell69.Weight = 0.1036734693877551;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 0.10816326530612246;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderIdCarteiraBeforePrint);
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.Transparent;
            this.xrControlStyle2.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle2.BorderWidth = 1;
            this.xrControlStyle2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle2.Name = "xrControlStyle2";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportQuadroRetorno
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.GroupHeader1,
            this.ReportFooter,
            this.GroupHeader2,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 90, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.ReportQuadroRetorno.ResourceManager;

        }

        // Colunas da Tabela        
        private string IdCarteiraColumn = "IdCarteira";
        private string NomeCarteiraColumn = "NomeCarteira";
        private string PLAberturaColumn = "PLAbertura";
        private string PLFechamentoColumn = "PLFechamento";
        private string CotaAberturaColumn = "CotaAbertura";
        private string CotaFechamentoColumn = "CotaFechamento";
        private string TipoIndiceColumn = "TipoIndice";
        //  

        //
        #region Funções Internas do Relatorio
        //
        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.dataReferencia.ToString("d");
        }

        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                XRTableCell nomeCarteiraXRTableCell = sender as XRTableCell;
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                string nomeCarteira = (string)this.GetCurrentColumnValue(this.NomeCarteiraColumn);
                string carteira = idCarteira + " - " + nomeCarteira;

                if (carteira.Length >= 30)
                {
                    carteira = carteira.Substring(0, 29);
                }
                nomeCarteiraXRTableCell.Text = carteira;
            }
        }

        // Armazena o Tipo da Carteira de cada linha do relatorio
        private void GroupHeaderIdCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                //                

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(this.carteira.Query.TipoCota);
                campos.Add(this.carteira.Query.IdCarteira);
                campos.Add(this.carteira.Query.DataInicioCota);
                this.carteira.QueryReset();
                this.carteira.LoadByPrimaryKey(campos, idCarteira);
                //
                this.dataInicioCliente = this.carteira.DataInicioCota.Value;
            }
        }

        private void PatrimonioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                XRTableCell patrimonioXRTableCell = sender as XRTableCell;
                //                                
                decimal patrimonio = this.carteira.IsTipoCotaAbertura()
                                        ? (decimal)this.GetCurrentColumnValue(this.PLAberturaColumn)
                                        : (decimal)this.GetCurrentColumnValue(this.PLFechamentoColumn);

                //
                patrimonioXRTableCell.Text = patrimonio.ToString("n2");
            }
        }

        private void CotaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.numeroLinhasDataTable != 0)
            {
                XRTableCell cotaXRTableCell = sender as XRTableCell;
                decimal cota = this.carteira.IsTipoCotaAbertura()
                                            ? (decimal)this.GetCurrentColumnValue(this.CotaAberturaColumn)
                                            : (decimal)this.GetCurrentColumnValue(this.CotaFechamentoColumn);

                cotaXRTableCell.Text = cota.ToString("n8");
            }
        }

        private void TableDetailRentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable summaryFinal = sender as XRTable;
            #region Zera os campos da Table
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[4]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[5]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[6]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[7]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[8]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[9]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[10]).Text = "";

            XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];
            ((XRTableCell)summaryFinalRow1.Cells[4]).Text = "";
            ((XRTableCell)summaryFinalRow1.Cells[5]).Text = "";
            ((XRTableCell)summaryFinalRow1.Cells[6]).Text = "";
            ((XRTableCell)summaryFinalRow1.Cells[7]).Text = "";
            ((XRTableCell)summaryFinalRow1.Cells[8]).Text = "";
            ((XRTableCell)summaryFinalRow1.Cells[9]).Text = "";
            #endregion

            /*
             * Retorno Nominal
             * 4-(Dia) - 5-(Mês) - 6-(Ano) - 7-(6 Meses) - 8-(12 Meses) - 9-(24 Meses)
             *
             * Retorno Diferencial
             * 3-(Dia) - 4-(Mês) - 5-(Ano) - 6-(6 Meses) - 7-(12 Meses) - 8-(24 Meses)
             * 
            */
            List<decimal?> rentabilidadeNominal = new List<decimal?>();
            List<decimal?> rentabilidadeDiferencial = new List<decimal?>();
            //
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
            calculoMedida.SetDataInicio(this.dataInicioCliente);
            //                                                                       
            #region Calcula Rentabilidade Nominal
            calculoMedida.SetIdCarteira(this.carteira.IdCarteira.Value);
            if (this.numeroLinhasDataTable != 0)
            {

                #region rentabilidadeDia
                decimal? rentabilidadeDiaNomimal = null;
                try
                {
                    rentabilidadeDiaNomimal = calculoMedida.CalculaRetornoDia(this.dataReferencia);
                    rentabilidadeDiaNomimal = rentabilidadeDiaNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) {}
                #endregion

                #region rentabilidadeMes
                decimal? rentabilidadeMesNomimal = null;
                try
                {
                    rentabilidadeMesNomimal = calculoMedida.CalculaRetornoMes(this.dataReferencia);
                    rentabilidadeMesNomimal = rentabilidadeMesNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) {}
                #endregion

                #region rentabilidadeAno
                decimal? rentabilidadeAnoNomimal = null;
                try
                {
                    rentabilidadeAnoNomimal = calculoMedida.CalculaRetornoAno(this.dataReferencia);
                    rentabilidadeAnoNomimal = rentabilidadeAnoNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) {}
                #endregion

                #region rentabilidade6Meses
                decimal? rentabilidade6MesesNomimal = null;
                try
                {
                    rentabilidade6MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataReferencia, 6);
                    rentabilidade6MesesNomimal = rentabilidade6MesesNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) { }
                #endregion

                #region rentabilidade12Meses
                decimal? rentabilidade12MesesNomimal = null;
                try
                {
                    rentabilidade12MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataReferencia, 12);
                    rentabilidade12MesesNomimal = rentabilidade12MesesNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) { }
                #endregion

                #region rentabilidade24Meses
                decimal? rentabilidade24MesesNomimal = null;
                try
                {
                    rentabilidade24MesesNomimal = calculoMedida.CalculaRetornoPeriodoMes(this.dataReferencia, 24);
                    rentabilidade24MesesNomimal = rentabilidade24MesesNomimal / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
                //catch (Exception) {}
                #endregion

                rentabilidadeNominal.Add(rentabilidadeDiaNomimal);
                rentabilidadeNominal.Add(rentabilidadeMesNomimal);
                rentabilidadeNominal.Add(rentabilidadeAnoNomimal);
                rentabilidadeNominal.Add(rentabilidade6MesesNomimal);
                rentabilidadeNominal.Add(rentabilidade12MesesNomimal);
                rentabilidadeNominal.Add(rentabilidade24MesesNomimal);
            }
            #endregion

            /* Exibe Retorno Nominal */
            #region Exibe Retorno Nominal
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[4], rentabilidadeNominal[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[5], rentabilidadeNominal[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[6], rentabilidadeNominal[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[7], rentabilidadeNominal[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[8], rentabilidadeNominal[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[9], rentabilidadeNominal[5]);
            #endregion

            #region Calcula Rentabilidade Diferencial
            if (this.numeroLinhasDataTable != 0)
            {
                // Pega o Indice
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.IdIndiceBenchmark);
                carteira.LoadByPrimaryKey(this.carteira.IdCarteira.Value);

                int idIndiceBenchmark = carteira.IdIndiceBenchmark.Value;
                calculoMedida.SetIdIndice(idIndiceBenchmark);

                int tipoIndice = Convert.ToInt16(this.GetCurrentColumnValue(this.TipoIndiceColumn));

                #region rentabilidadeDia
                decimal? rentabilidadeDia = null;
                try
                {
                    rentabilidadeDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                    rentabilidadeDia = rentabilidadeDia / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidadeMes
                decimal? rentabilidadeMes = null;
                try
                {
                    rentabilidadeMes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                    rentabilidadeMes = rentabilidadeMes / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidadeAno
                decimal? rentabilidadeAno = null;
                try
                {
                    rentabilidadeAno = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                    rentabilidadeAno = rentabilidadeAno / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidade6Meses
                decimal? rentabilidade6Meses = null;
                try
                {
                    rentabilidade6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                    rentabilidade6Meses = rentabilidade6Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidade12Meses
                decimal? rentabilidade12Meses = null;
                try
                {
                    rentabilidade12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                    rentabilidade12Meses = rentabilidade12Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidade24Meses
                decimal? rentabilidade24Meses = null;
                try
                {
                    rentabilidade24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 24);
                    rentabilidade24Meses = rentabilidade24Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                decimal? rentabilidadeDiaDiferencial;
                decimal? rentabilidadeMesDiferencial;
                decimal? rentabilidadeAnoDiferencial;
                decimal? rentabilidade6MesesDiferencial;
                decimal? rentabilidade12MesesDiferencial;
                decimal? rentabilidade24MesesDiferencial;

                if (tipoIndice == (short)TipoIndice.Decimal)
                {
                    #region TipoIndice = Decimal
                    rentabilidadeDiaDiferencial = rentabilidadeNominal[0].HasValue && rentabilidadeDia.HasValue && rentabilidadeDia != 0
                                                ? rentabilidadeNominal[0] - rentabilidadeDia
                                                : null;

                    rentabilidadeMesDiferencial = rentabilidadeNominal[1].HasValue && rentabilidadeMes.HasValue && rentabilidadeMes != 0
                                                ? rentabilidadeNominal[1] - rentabilidadeMes
                                                : null;


                    rentabilidadeAnoDiferencial = rentabilidadeNominal[2].HasValue && rentabilidadeAno.HasValue && rentabilidadeAno != 0
                                                ? rentabilidadeNominal[2] - rentabilidadeAno
                                                : null;

                    rentabilidade6MesesDiferencial = rentabilidadeNominal[3].HasValue && rentabilidade6Meses.HasValue && rentabilidade6Meses != 0
                                                ? rentabilidadeNominal[3] - rentabilidade6Meses
                                                : null;

                    rentabilidade12MesesDiferencial = rentabilidadeNominal[4].HasValue && rentabilidade12Meses.HasValue && rentabilidade12Meses != 0
                                                ? rentabilidadeNominal[4] - rentabilidade12Meses
                                                : null;

                    rentabilidade24MesesDiferencial = rentabilidadeNominal[5].HasValue && rentabilidade24Meses.HasValue && rentabilidade24Meses != 0
                                                ? rentabilidadeNominal[5] - rentabilidade24Meses
                                                : null;
                    #endregion
                }
                else
                {
                    #region TipoIndice = Percentual
                    rentabilidadeDiaDiferencial = rentabilidadeNominal[0].HasValue && rentabilidadeDia.HasValue && rentabilidadeDia != 0
                            ? rentabilidadeNominal[0] / rentabilidadeDia
                            : null;

                    rentabilidadeMesDiferencial = rentabilidadeNominal[1].HasValue && rentabilidadeMes.HasValue && rentabilidadeMes != 0
                                                ? rentabilidadeNominal[1] / rentabilidadeMes
                                                : null;


                    rentabilidadeAnoDiferencial = rentabilidadeNominal[2].HasValue && rentabilidadeAno.HasValue && rentabilidadeAno != 0
                                                ? rentabilidadeNominal[2] / rentabilidadeAno
                                                : null;

                    rentabilidade6MesesDiferencial = rentabilidadeNominal[3].HasValue && rentabilidade6Meses.HasValue && rentabilidade6Meses != 0
                                                ? rentabilidadeNominal[3] / rentabilidade6Meses
                                                : null;

                    rentabilidade12MesesDiferencial = rentabilidadeNominal[4].HasValue && rentabilidade12Meses.HasValue && rentabilidade12Meses != 0
                                                ? rentabilidadeNominal[4] / rentabilidade12Meses
                                                : null;

                    rentabilidade24MesesDiferencial = rentabilidadeNominal[5].HasValue && rentabilidade24Meses.HasValue && rentabilidade24Meses != 0
                                                ? rentabilidadeNominal[5] / rentabilidade24Meses
                                                : null;
                    #endregion
                }

                rentabilidadeDiferencial.Add(rentabilidadeDiaDiferencial);
                rentabilidadeDiferencial.Add(rentabilidadeMesDiferencial);
                rentabilidadeDiferencial.Add(rentabilidadeAnoDiferencial);
                rentabilidadeDiferencial.Add(rentabilidade6MesesDiferencial);
                rentabilidadeDiferencial.Add(rentabilidade12MesesDiferencial);
                rentabilidadeDiferencial.Add(rentabilidade24MesesDiferencial);
            }
            #endregion

            /* Exibe Retorno Diferencial */
            #region Exibe Retorno Diferencial
            string retornoDiferencial = Resources.ReportQuadroRetorno._RetornosDiferenciais;
            ((XRTableCell)summaryFinalRow1.Cells[2]).Text = retornoDiferencial;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[3], rentabilidadeDiferencial[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[4], rentabilidadeDiferencial[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[5], rentabilidadeDiferencial[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[6], rentabilidadeDiferencial[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[7], rentabilidadeDiferencial[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[8], rentabilidadeDiferencial[5]);
            #endregion

            #region Calcula Volatilidade
            if (this.numeroLinhasDataTable != 0)
            {
                calculoMedida.SetIdCarteira(this.carteira.IdCarteira.Value);
                DateTime dataInicio = Calendario.RetornaDiaMesAnterior(this.dataReferencia, 12);

                #region volatilidade
                decimal? volatilidade = null;
                try
                {
                    volatilidade = calculoMedida.CalculaVolatilidade(dataInicio, this.dataReferencia);
                    volatilidade = volatilidade / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }

                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[10], volatilidade);

                #endregion
            }

            #endregion
        }

        private void TableIndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable summaryFinal = sender as XRTable;
            /*
             * Retorno CDI
             * Dia - Mês - Ano - 6 Meses - 12 Meses - 24 Meses
             *
             * Retorno Ibovespa
             * Dia - Mês - Ano - 6 Meses - 12 Meses - 24 Meses
             * 
             * Retorno Dolar (Ptax 800 Venda)
             * Dia - Mês - Ano - 6 Meses - 12 Meses - 24 Meses
            */
            #region Calcula Rentabilidade CDI/Ibov Medio/Dolar
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetIdIndice(ListaIndiceFixo.CDI);
            //
            #region CDI
            List<decimal?> cdi = new List<decimal?>();
            //
            decimal? cdiDia = null;
            try
            {
                cdiDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                cdiDia = cdiDia / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? cdiMes = null;
            try
            {
                cdiMes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                cdiMes = cdiMes / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? cdiAno = null;
            try
            {
                cdiAno = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                cdiAno = cdiAno / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? cdi6Meses = null;
            try
            {
                cdi6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                cdi6Meses = cdi6Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? cdi12Meses = null;
            try
            {
                cdi12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                cdi12Meses = cdi12Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? cdi24Meses = null;
            try
            {
                cdi24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 24);
                cdi24Meses = cdi24Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            cdi.Add(cdiDia);
            cdi.Add(cdiMes);
            cdi.Add(cdiAno);
            cdi.Add(cdi6Meses);
            cdi.Add(cdi12Meses);
            cdi.Add(cdi24Meses);
            #endregion

            calculoMedida.SetIdIndice(ListaIndiceFixo.IBOVESPA_FECHA);
            #region Ibovespa
            List<decimal?> ibovespa = new List<decimal?>();
            //
            decimal? ibovespaDia = null;
            try
            {
                ibovespaDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                ibovespaDia = ibovespaDia / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaMes = null;
            try
            {
                ibovespaMes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                ibovespaMes = ibovespaMes / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaAno = null;
            try
            {
                ibovespaAno = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                ibovespaAno = ibovespaAno / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespa6Meses = null;
            try
            {
                ibovespa6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                ibovespa6Meses = ibovespa6Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespa12Meses = null;
            try
            {
                ibovespa12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                ibovespa12Meses = ibovespa12Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespa24Meses = null;
            try
            {
                ibovespa24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 24);
                ibovespa24Meses = ibovespa24Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            ibovespa.Add(ibovespaDia);
            ibovespa.Add(ibovespaMes);
            ibovespa.Add(ibovespaAno);
            ibovespa.Add(ibovespa6Meses);
            ibovespa.Add(ibovespa12Meses);
            ibovespa.Add(ibovespa24Meses);
            #endregion

            calculoMedida.SetIdIndice(ListaIndiceFixo.PTAX_800VENDA);
            #region Dolar
            List<decimal?> ptax800Venda = new List<decimal?>();
            //
            decimal? ptax800Dia = null;
            try
            {
                ptax800Dia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                ptax800Dia = ptax800Dia / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Mes = null;
            try
            {
                ptax800Mes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                ptax800Mes = ptax800Mes / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano = null;
            try
            {
                ptax800Ano = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                ptax800Ano = ptax800Ano / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax8006Meses = null;
            try
            {
                ptax8006Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                ptax8006Meses = ptax8006Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax80012Meses = null;
            try
            {
                ptax80012Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                ptax80012Meses = ptax80012Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax80024Meses = null;
            try
            {
                ptax80024Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 24);
                ptax80024Meses = ptax80024Meses / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            ptax800Venda.Add(ptax800Dia);
            ptax800Venda.Add(ptax800Mes);
            ptax800Venda.Add(ptax800Ano);
            ptax800Venda.Add(ptax8006Meses);
            ptax800Venda.Add(ptax80012Meses);
            ptax800Venda.Add(ptax80024Meses);
            #endregion

            #endregion
            //

            /* Cabeçalho*/
            #region Cabecalho
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[1]).Text = Resources.ReportQuadroRetorno._Dia;
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = Resources.ReportQuadroRetorno._Mes;
            ((XRTableCell)summaryFinalRow0.Cells[3]).Text = Resources.ReportQuadroRetorno._Ano;
            ((XRTableCell)summaryFinalRow0.Cells[4]).Text = Resources.ReportQuadroRetorno._6Meses;
            ((XRTableCell)summaryFinalRow0.Cells[5]).Text = Resources.ReportQuadroRetorno._12Meses;
            ((XRTableCell)summaryFinalRow0.Cells[6]).Text = Resources.ReportQuadroRetorno._24Meses;
            #endregion

            /* CDI */
            #region CDI
            XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];
            ((XRTableCell)summaryFinalRow1.Cells[0]).Text = Resources.ReportQuadroRetorno._CDI;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[1], cdi[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[2], cdi[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[3], cdi[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[4], cdi[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[5], cdi[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[6], cdi[5]);
            #endregion

            /* Ibovespa */
            #region Ibov
            XRTableRow summaryFinalRow2 = summaryFinal.Rows[2];
            ((XRTableCell)summaryFinalRow2.Cells[0]).Text = Resources.ReportQuadroRetorno._Ibovespa;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[1], ibovespa[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[2], ibovespa[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[3], ibovespa[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[4], ibovespa[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[5], ibovespa[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[6], ibovespa[5]);
            #endregion

            /* Dolar */
            #region Dolar
            XRTableRow summaryFinalRow3 = summaryFinal.Rows[3];
            ((XRTableCell)summaryFinalRow3.Cells[0]).Text = Resources.ReportQuadroRetorno._Dolar;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[1], ptax800Venda[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[2], ptax800Venda[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[3], ptax800Venda[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[4], ptax800Venda[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[5], ptax800Venda[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[6], ptax800Venda[5]);
            #endregion
        }

        #endregion
    }
}