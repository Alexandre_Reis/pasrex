using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Financial.Fundo;
using Financial.Relatorio;
using EntitySpaces.Interfaces;
using System.Collections.Generic;

namespace Financial.Relatorio
{
    /// <summary>
    /// Summary description for ReportLamina
    /// </summary>
    public class ReportLamina : DevExpress.XtraReports.UI.XtraReport
    {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private SubReportPerfilFundo subReportPerfilFundo1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport1;
        private SubReportCaracteristicas subReportCaracteristicas1;
        private DetailReportBand DetailReport;
        private DetailBand Detail1;
        private XRSubreport xrSubreport3;
        private XRSubreport xrSubreport4;
        private XRSubreport xrSubreport5;
        private XRSubreport xrSubreport6;

        private SubReportGraficoEvolucaoPL subReportGraficoEvolucaoPL1;
        private SubReportGraficoRetornoAcumulado subReportGraficoRetornoAcumulado1;
        private SubReportHistoricoFundo subReportHistoricoFundo1;
        private SubReportInfosOperacionais subReportInfosOperacionais1;
        private SubReportRetornoCarteiraResumido subReportRetornoCarteiraResumido1;

        private Carteira carteira;
        private CalculoMedida calculoMedida;
        private DateTime dataInicio, dataFim;
        private XRSubreport xrSubreport20;
        private SubReportLogotipo1 subReportLogotipo11;
        private XRPageInfo xrPageInfo2;
        private XRLine xrLine1;
        private XRTable xrTable5;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport7;

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public ReportLamina(int idCarteira)
        {
            InitializeComponent();

            this.idCarteira = idCarteira;

            this.carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            this.calculoMedida = new CalculoMedida(idCarteira, carteira.IdIndiceBenchmark.Value);
            this.carteira.InitFundoSIAnbid();

            this.dataInicio = this.calculoMedida.DataInicio.Value;
            this.dataFim = this.calculoMedida.DataAtual.Value;

            this.calculoMedida.InitListaRetornoMensal(this.dataInicio, this.dataFim);

            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

        }

        private void PersonalInitialize()
        {
            this.subReportCaracteristicas1.PersonalInitialize(this.carteira);
            this.subReportPerfilFundo1.PersonalInitialize(this.carteira);
            this.subReportGraficoEvolucaoPL1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
            this.subReportGraficoRetornoAcumulado1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
            this.subReportHistoricoFundo1.PersonalInitialize(this.calculoMedida, this.dataInicio, this.dataFim);
            this.subReportInfosOperacionais1.PersonalInitialize(this.carteira);

            DateTime dataInicio24Meses = this.dataFim.AddMonths(-24);
            DateTime dataInicioSubReportRetornoCarteira = dataInicio24Meses > this.dataInicio ? dataInicio24Meses : dataInicio;

            this.subReportRetornoCarteiraResumido1.PersonalInitialize(this.calculoMedida, this.dataFim);

            //Ajustar posicionamento a direita dos SubReports PerilFundo, GraficoRetornoAcumulado e InfosOperacionais
            this.xrSubreport2.Location = new Point(975, this.xrSubreport2.Location.Y);
            this.xrSubreport4.Location = new Point(975, this.xrSubreport4.Location.Y);
            this.xrSubreport6.Location = new Point(975, this.xrSubreport6.Location.Y);
        }

        private void ReportLaminaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.xrTableCell7.Text = this.calculoMedida.carteira.IdCarteira.Value + ": " + this.calculoMedida.carteira.Nome;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            string resourceFileName = "ReportLamina.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportCaracteristicas1 = new Financial.Relatorio.SubReportCaracteristicas();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportPerfilFundo1 = new Financial.Relatorio.SubReportPerfilFundo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport20 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo11 = new Financial.Relatorio.SubReportLogotipo1();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoEvolucaoPL1 = new Financial.Relatorio.SubReportGraficoEvolucaoPL();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoRetornoAcumulado1 = new Financial.Relatorio.SubReportGraficoRetornoAcumulado();
            this.xrSubreport5 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportHistoricoFundo1 = new SubReportHistoricoFundo();
            this.xrSubreport6 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportInfosOperacionais1 = new SubReportInfosOperacionais();
            this.xrSubreport7 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRetornoCarteiraResumido1 = new Financial.Relatorio.SubReportRetornoCarteiraResumido();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.subReportCaracteristicas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPerfilFundo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoEvolucaoPL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportHistoricoFundo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportInfosOperacionais1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteiraResumido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportCaracteristicas1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(685F, 127F);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(741F, 21F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportPerfilFundo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(685F, 127F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport20,
            this.xrPageInfo2,
            this.xrLine1,
            this.xrTable5});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 140F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport20
            // 
            this.xrSubreport20.Dpi = 254F;
            this.xrSubreport20.LocationFloat = new DevExpress.Utils.PointFloat(1180F, 0F);
            this.xrSubreport20.Name = "xrSubreport20";
            this.xrSubreport20.ReportSource = this.subReportLogotipo11;
            this.xrSubreport20.SizeF = new System.Drawing.SizeF(720F, 61F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:d}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(1630F, 75F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(277F, 34F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1905F, 5F);
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(18F, 75F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable5.SizeF = new System.Drawing.SizeF(931F, 43F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Cliente:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 1;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DetailReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrSubreport2,
            this.xrSubreport3,
            this.xrSubreport4,
            this.xrSubreport5,
            this.xrSubreport6,
            this.xrSubreport7});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 796F;
            this.Detail1.KeepTogether = true;
            this.Detail1.Name = "Detail1";
            this.Detail1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail1.StylePriority.UseBackColor = false;
            this.Detail1.StylePriority.UseForeColor = false;
            this.Detail1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 381F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportGraficoEvolucaoPL1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(685F, 148F);
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.LocationFloat = new DevExpress.Utils.PointFloat(741F, 381F);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportGraficoRetornoAcumulado1;
            this.xrSubreport4.SizeF = new System.Drawing.SizeF(685F, 148F);
            // 
            // xrSubreport5
            // 
            this.xrSubreport5.Dpi = 254F;
            this.xrSubreport5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 572F);
            this.xrSubreport5.Name = "xrSubreport5";
            this.xrSubreport5.ReportSource = this.subReportHistoricoFundo1;
            this.xrSubreport5.SizeF = new System.Drawing.SizeF(685F, 169F);
            // 
            // xrSubreport6
            // 
            this.xrSubreport6.Dpi = 254F;
            this.xrSubreport6.LocationFloat = new DevExpress.Utils.PointFloat(741F, 572F);
            this.xrSubreport6.Name = "xrSubreport6";
            this.xrSubreport6.ReportSource = this.subReportInfosOperacionais1;
            this.xrSubreport6.SizeF = new System.Drawing.SizeF(685F, 169F);
            // 
            // xrSubreport7
            // 
            this.xrSubreport7.Dpi = 254F;
            this.xrSubreport7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169F);
            this.xrSubreport7.Name = "xrSubreport7";
            this.xrSubreport7.ReportSource = this.subReportRetornoCarteiraResumido1;
            this.xrSubreport7.SizeF = new System.Drawing.SizeF(1418F, 169F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 75F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportLamina
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.DetailReport,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 100, 75);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportLaminaBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.subReportCaracteristicas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportPerfilFundo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoEvolucaoPL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoRetornoAcumulado1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportHistoricoFundo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportInfosOperacionais1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRetornoCarteiraResumido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
    }
}