﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Security;
using DevExpress.Utils;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportMovimentacaoFundo
    /// </summary>
    public class ReportMovimentacaoFundo : XtraReport {

        #region Data Operação
        private DateTime? dataInicioOperacao;

        public DateTime? DataInicioOperacao {
            get { return dataInicioOperacao; }
            set { dataInicioOperacao = value; }
        }

        private DateTime? dataFimOperacao;

        public DateTime? DataFimOperacao {
            get { return dataFimOperacao; }
            set { dataFimOperacao = value; }
        } 
        #endregion

        #region Data Conversão
        private DateTime? dataInicioConversao;

        public DateTime? DataInicioConversao {
            get { return dataInicioConversao; }
            set { dataInicioConversao = value; }
        }

        private DateTime? dataFimConversao;

        public DateTime? DataFimConversao {
            get { return dataFimConversao; }
            set { dataFimConversao = value; }
        } 
        #endregion

        #region Data Liquidação
        private DateTime? dataInicioLiquidacao;

        public DateTime? DataInicioLiquidacao {
            get { return dataInicioLiquidacao; }
            set { dataInicioLiquidacao = value; }
        }

        private DateTime? dataFimLiquidacao;

        public DateTime? DataFimLiquidacao {
            get { return dataFimLiquidacao; }
            set { dataFimLiquidacao = value; }
        }
        
        #endregion

        #region TipoOperacao
        private byte? tipoOperacao;

        public byte? TipoOperacao {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        } 
        #endregion

        #region Cliente
        private int? idCliente;

        public int? IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        
        #endregion

        #region Carteira
        private int? idCarteira;

        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        } 
        #endregion

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCellDataFim;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCell37;
        private OperacaoFundoCollection operacaoFundoCollection1;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRTable xrTable8;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell45;
        private XRTable xrTable9;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell72;
        private XRPanel xrPanel1;
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport2;
        private XRTableCell xrTableCell74;
        private XRSubreport xrSubreport3;
        private SubReportLogotipo subReportLogotipo1;
        private ReportSemDados reportSemDados1;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTable xrTable11;
        private XRTableRow xrTableRow15;
        private XRTableCell xrTableCell94;
        private XRTableCell xrTableCell95;
        private XRTableCell xrTableCell96;
        private XRTableCell xrTableCell97;
        private XRTableCell xrTableCell98;
        private XRTableCell xrTableCell99;
        private XRTableCell xrTableCell100;
        private XRTableCell xrTableCell101;
        private XRTableCell xrTableCell102;
        private XRTableCell xrTableCell103;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;        

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        
        private void MovimentacaoFundo() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2490);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }
        
        public ReportMovimentacaoFundo(int? idCliente, int? idCarteira, 
                                         DateTime? dataInicioOperacao, DateTime? dataFimOperacao,
                                         DateTime? dataInicioConversao, DateTime? dataFimConversao,
                                         DateTime? dataInicioLiquidacao, DateTime? dataFimLiquidacao, 
                                         byte? tipoOperacao) {
            this.idCliente = idCliente;
            this.idCarteira = idCarteira;
            //
            this.dataInicioOperacao = dataInicioOperacao;
            this.dataFimOperacao = dataFimOperacao;
            //
            this.dataInicioConversao = dataInicioConversao;
            this.dataFimConversao = dataFimConversao;
            //
            this.dataInicioLiquidacao = dataInicioLiquidacao;
            this.dataFimLiquidacao = dataFimLiquidacao;
            //
            this.tipoOperacao = tipoOperacao;
            //
            this.MovimentacaoFundo();
        }
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport1.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable3.Visible = false;
                this.xrTable11.Visible = false;
                this.xrTable5.Visible = false;

                // Controla Localização do Subreport Sem Dados de acordo com o cabeçalho
                // Se não tem Cliente nem Carteira
                if (!this.IdCliente.HasValue && !this.IdCarteira.HasValue) {
                    this.xrSubreport1.LocationF = new PointF(this.xrSubreport1.LocationF.X, 191F);                                     
                }
                // Se tem apenas o Cliente ou Apenas a Carteira
                else if (this.IdCarteira.HasValue && !this.IdCliente.HasValue ||
                         this.IdCliente.HasValue && !this.IdCarteira.HasValue) {
                    this.xrSubreport1.LocationF = new PointF(this.xrSubreport1.LocationF.X, 236F);
                }
                // Se tem Cliente e Carteira
                else if (this.IdCliente.HasValue && this.IdCarteira.HasValue) {
                    this.xrSubreport1.LocationF = new PointF(this.xrSubreport1.LocationF.X, 307F);
                }
            }
        }

        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;

            // Controla se Informação da carteira será visivel
            this.xrTable8.Visible = this.IdCarteira.HasValue;

            // Controla se Informação do cliente será visivel
            this.xrTable9.Visible = this.IdCliente.HasValue;

            // Se tem Cliente e não tem Carteira Tabela de Cliente muda de localização
            if (this.IdCliente.HasValue && !this.IdCarteira.HasValue) {
                this.xrTable9.LocationF = new PointF(this.xrTable9.LocationF.X, 85F); // Localização relativo ao painel 
            }

            // Controla Localização do Começo do Relatorio de acordo com o cabeçalho
            // Se não tem Cliente nem Carteira
            if (!this.IdCliente.HasValue && !this.IdCarteira.HasValue) {                
                this.xrPanel1.HeightF = 84F;
                this.xrTable7.LocationF = new PointF(100F, 191F);
                //
                //this.GroupHeader1.LocationFloat = new PointFloat(this.GroupHeader1.LocationFloat.X, this.GroupHeader1.LocationFloat.Y - 30F);
                //this.PageHeader.HeightF = this.PageHeader.HeightF - 50F;
            }
            // Se tem apenas o Cliente ou Apenas a Carteira
            else if (this.IdCarteira.HasValue && !this.IdCliente.HasValue ||
                     this.IdCliente.HasValue && !this.IdCarteira.HasValue) {
                
                this.xrPanel1.HeightF = 126F;
                this.xrTable7.LocationF = new PointF(this.xrTable7.LocationF.X, 236F);

                //this.GroupHeader1.LocationFloat = new PointFloat(this.GroupHeader1.LocationF.X, 236F + 48F);
            }
            // Se tem Cliente e Carteira
            else if (this.IdCliente.HasValue && this.IdCarteira.HasValue) {
                this.xrTable7.LocationF = new PointF(this.xrTable7.LocationF.X, 307F);
                //this.GroupHeader1.LocationFloat = new PointFloat(this.GroupHeader1.LocationF.X, 236F + 48F);
            }

            #region Pega Campos do resource
            
            this.xrTableCell55.Text = Resources.ReportMovimentacaoFundo._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportMovimentacaoFundo._DataEmissao;
            this.xrTableCell6.Text = Resources.ReportMovimentacaoFundo._a;
            //

            if (this.IsDataConversao()) {
                this.xrTableCell4.Text = Resources.ReportMovimentacaoCotista._DataConversao;
            }
            else if (this.IsDataLiquidacao()) {
                this.xrTableCell4.Text = Resources.ReportMovimentacaoCotista._DataLiquidacao;
            }
            else if (this.IsDataOperacao()) {
                this.xrTableCell4.Text = Resources.ReportMovimentacaoCotista._DataOperacao;
            }

            this.xrTableCell38.Text = Resources.ReportMovimentacaoFundo._Cliente;
            this.xrTableCell42.Text = Resources.ReportMovimentacaoFundo._Header_Carteira;
            this.xrTableCell62.Text = Resources.ReportMovimentacaoFundo._Header_Cliente;
            this.xrTableCell9.Text = Resources.ReportMovimentacaoFundo._DataConversao;
            this.xrTableCell25.Text = Resources.ReportMovimentacaoFundo._DataLiquidacao;
            this.xrTableCell28.Text = Resources.ReportMovimentacaoFundo._TipoOperacao;
            this.xrTableCell26.Text = Resources.ReportMovimentacaoFundo._Quantidade;
            this.xrTableCell5.Text = Resources.ReportMovimentacaoFundo._Cota;
            this.xrTableCell3.Text = Resources.ReportMovimentacaoFundo._ValorBruto;
            this.xrTableCell35.Text = Resources.ReportMovimentacaoFundo._ValorIR;
            this.xrTableCell7.Text = Resources.ReportMovimentacaoFundo._ValorIOF;
            this.xrTableCell8.Text = Resources.ReportMovimentacaoFundo._ValorPfee;
            this.xrTableCell11.Text = Resources.ReportMovimentacaoFundo._ValorLiquido;        
            #endregion
        }

        private DataView FillDados() {
            #region SQL

            PermissaoClienteQuery p = new PermissaoClienteQuery("p");
            UsuarioQuery u = new UsuarioQuery("u");
            //
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("o");
            operacaoFundoQuery.Select(operacaoFundoQuery.IdCarteira,
                                      operacaoFundoQuery.IdCliente,
                                      operacaoFundoQuery.IdOperacao,
                                      operacaoFundoQuery.DataOperacao,
                                      operacaoFundoQuery.DataConversao,
                                      operacaoFundoQuery.DataLiquidacao,
                                      operacaoFundoQuery.TipoOperacao,
                                      operacaoFundoQuery.Quantidade,
                                      operacaoFundoQuery.CotaOperacao,
                                      operacaoFundoQuery.ValorBruto,
                                      operacaoFundoQuery.ValorIR,
                                      operacaoFundoQuery.ValorIOF,
                                      operacaoFundoQuery.ValorPerformance,
                                      operacaoFundoQuery.ValorLiquido);
            //
            operacaoFundoQuery.InnerJoin(p).On(p.IdCliente == operacaoFundoQuery.IdCliente);
            operacaoFundoQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
            operacaoFundoQuery.Where(u.Login == HttpContext.Current.User.Identity.Name);
                       
            #region Data Operação
            if (this.dataInicioOperacao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao >= this.dataInicioOperacao.Value);
            }
            if (this.dataFimOperacao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao <= this.dataFimOperacao.Value);
            } 
            #endregion

            #region Data Conversão
            if (this.dataInicioConversao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataConversao >= this.dataInicioConversao.Value);
            }
            if (this.dataFimConversao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataConversao <= this.dataFimConversao.Value);
            }
            #endregion

            #region Data Liquidação
            if (this.dataInicioLiquidacao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataLiquidacao >= this.dataInicioLiquidacao.Value);
            }
            if (this.dataFimLiquidacao.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataLiquidacao <= this.dataFimLiquidacao.Value);
            } 
            #endregion

            #region Tipo Operação
            if (this.tipoOperacao.HasValue) 
            {
                if (this.tipoOperacao.Value == 200)
                {
                    operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                                (byte)TipoOperacaoFundo.ResgateCotas,
                                                                                (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                                (byte)TipoOperacaoFundo.ResgateTotal));
            }
                else if (this.tipoOperacao.Value == 201)
                {
                    operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                                (byte)TipoOperacaoFundo.ResgateCotas,
                                                                                (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                                (byte)TipoOperacaoFundo.ResgateTotal,
                                                                                (byte)TipoOperacaoFundo.ComeCotas));
                }
                else
                {
                    operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao == this.tipoOperacao.Value);
                }
            } 
            #endregion

            #region Carteira
            if (this.idCarteira.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.IdCarteira == this.idCarteira);
            } 
            #endregion

            #region Cliente
            if (this.idCliente.HasValue) {
                operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente == this.idCliente);
            }            
            #endregion
            //

            #endregion

            this.operacaoFundoCollection1.QueryReset();
            this.operacaoFundoCollection1.Load(operacaoFundoQuery);

            return this.operacaoFundoCollection1.LowLevelBind();            
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportMovimentacaoFundo.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportMovimentacaoFundo.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.operacaoFundoCollection1 = new Financial.Fundo.OperacaoFundoCollection();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataFim = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 48F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DataOperacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdOperacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2480F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell10,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell36,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "Cliente";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.17096774193548386;
            this.xrTableCell39.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataConversao", "{0:d}")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.068145161290322581;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataLiquidacao", "{0:d}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "xrTableCell21";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.059677419354838709;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "TipoOperacao";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.11290322580645161;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoOperacaoBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade", "{0:n8}")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.1028225806451613;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CotaOperacao", "{0:n8}")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "PontaAtivo";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.0939516129032258;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto", "{0:n2}")});
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.085080645161290325;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorBrutoBeforePrint);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR", "{0:n2}")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.068548387096774188;
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorIRBeforePrint);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF", "{0:n2}")});
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.076612903225806453;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorIOFBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorPerformance", "{0:n2}")});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.07701612903225806;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorPFeeBeforePrint);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorLiquido", "{0:n2}")});
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.0842741935483871;
            this.xrTableCell18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorLiquidoBeforePrint);
            // 
            // operacaoFundoCollection1
            // 
            this.operacaoFundoCollection1.AllowDelete = true;
            this.operacaoFundoCollection1.AllowEdit = true;
            this.operacaoFundoCollection1.AllowNew = true;
            this.operacaoFundoCollection1.EnableHierarchicalBinding = true;
            this.operacaoFundoCollection1.Filter = "";
            this.operacaoFundoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.operacaoFundoCollection1.Sort = "";
            // 
            // xrTable9
            // 
            this.xrTable9.Dpi = 254F;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 130F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable9.SizeF = new System.Drawing.SizeF(2328.375F, 42F);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable9.Visible = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell72});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.CanGrow = false;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.Text = "#Cliente";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell62.Weight = 0.1758957654723127;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.CanGrow = false;
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell72.Text = "Cliente";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell72.Weight = 1.7201750814332248;
            this.xrTableCell72.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteCabecalhoBeforePrint);
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable8.SizeF = new System.Drawing.SizeF(2328.375F, 42F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable8.Visible = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell45});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.CanGrow = false;
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Text = "#Carteira";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell42.Weight = 0.17599351175993511;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.CanGrow = false;
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Text = "Carteira";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell45.Weight = 1.7123884833738847;
            this.xrTableCell45.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraCabecalhoBeforePrint);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(365F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(217F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio,
            this.xrTableCell6,
            this.xrTableCellDataFim});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#Periodo";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataInicio";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.26614173228346455;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioBeforePrint);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#a";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.099212598425196849;
            // 
            // xrTableCellDataFim
            // 
            this.xrTableCellDataFim.CanGrow = false;
            this.xrTableCellDataFim.Dpi = 254F;
            this.xrTableCellDataFim.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataFim.Name = "xrTableCellDataFim";
            this.xrTableCellDataFim.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataFim.Text = "DataFim";
            this.xrTableCellDataFim.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataFim.Weight = 0.30078740157480316;
            this.xrTableCellDataFim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataFimBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrSubreport1,
            this.xrTable7,
            this.xrPanel1,
            this.xrPageInfo1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
            this.PageHeader.HeightF = 360F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 307F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(42F, 42F);
            this.xrSubreport1.Visible = false;
            // 
            // xrTable7
            // 
            this.xrTable7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(101F, 307F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2480F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell35,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell11});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "#Cliente";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.17096774193548386;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#DataConversao";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.068145161290322581;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#DataLiquidacao";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.059677419354838709;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#TipoOperacao";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell28.Weight = 0.11290322580645161;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#Quantidade";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.1028225806451613;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#Cota";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.0939516129032258;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#ValorBruto";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.085080645161290325;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "#ValorIR";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.068548387096774188;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#ValorIOF";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell7.Weight = 0.076612903225806453;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "#ValorPfee";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell8.Weight = 0.07701612903225806;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "#ValorLiquido";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell11.Weight = 0.0842741935483871;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrTable2,
            this.xrTable9,
            this.xrTable8,
            this.xrTable1,
            this.xrPageInfo3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 106F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(2342.417F, 185F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2453F, 101F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(910F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1672F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell74.Weight = 0.13935406698564593;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.8606459330143541;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 77F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(238F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2480F, 48F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "Carteira";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 61F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.Transparent;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2480F, 45F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell12,
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell40.Weight = 0.17096774193548386;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.068145161290322581;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.059677419354838709;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell24.Weight = 0.11330645161290323;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary1.FormatString = "{0:n8}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell27.Summary = xrSummary1;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.1028225806451613;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.0935483870967742;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto")});
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell30.Summary = xrSummary2;
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.085080645161290325;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell31.Summary = xrSummary3;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.068548387096774188;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell12.Summary = xrSummary4;
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.076612903225806453;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorPerformance")});
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell32.Summary = xrSummary5;
            this.xrTableCell32.Text = "xrTableCell32";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.076612903225806453;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorLiquido")});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell33.Summary = xrSummary6;
            this.xrTableCell33.Text = "xrTableCell33";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.0846774193548387;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11,
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.ReportFooter.HeightF = 212F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable11
            // 
            this.xrTable11.BackColor = System.Drawing.Color.LightGray;
            this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable11.Dpi = 254F;
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable11.SizeF = new System.Drawing.SizeF(2480F, 45F);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell94.Text = "Total Geral:";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell94.Weight = 0.25;
            this.xrTableCell94.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TotalBeforePrint);
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell95.Weight = 0.068145161290322581;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell96.Weight = 0.068145161290322581;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade")});
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary7.FormatString = "{0:n8}";
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell97.Summary = xrSummary7;
            this.xrTableCell97.Text = "xrTableCell97";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell97.Weight = 0.12822580645161291;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell98.Weight = 0.0939516129032258;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto")});
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary8.FormatString = "{0:n2}";
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell99.Summary = xrSummary8;
            this.xrTableCell99.Text = "xrTableCell99";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell99.Weight = 0.085080645161290325;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR")});
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary9.FormatString = "{0:n2}";
            xrSummary9.IgnoreNullValues = true;
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell100.Summary = xrSummary9;
            this.xrTableCell100.Text = "xrTableCell100";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell100.Weight = 0.068548387096774188;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF")});
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary10.FormatString = "{0:n2}";
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell101.Summary = xrSummary10;
            this.xrTableCell101.Text = "xrTableCell101";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell101.Weight = 0.076612903225806453;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorPerformance")});
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary11.FormatString = "{0:n2}";
            xrSummary11.IgnoreNullValues = true;
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell102.Summary = xrSummary11;
            this.xrTableCell102.Text = "xrTableCell102";
            this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell102.Weight = 0.07701612903225806;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorLiquido")});
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary12.FormatString = "{0:n2}";
            xrSummary12.IgnoreNullValues = true;
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell103.Summary = xrSummary12;
            this.xrTableCell103.Text = "xrTableCell103";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell103.Weight = 0.0842741935483871;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 64F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow9});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2480F, 137F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.Visible = false;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell37,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell50,
            this.xrTableCell20,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.33576642335766421;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "Total Pessoa Fisica: TABLE DEPRECATED";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.25;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.068145161290322581;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 0.068145161290322581;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Text = "QtdPessoaFisica";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.12822580645161291;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.0939516129032258;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "ValorBrutoPessoaFisica";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.085080645161290325;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "ValorIRPessoaFisica";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.068548387096774188;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "ValorIOFPessoaFisica";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.076612903225806453;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "ValorPerformancePessoaFisica";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.07701612903225806;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "ValorLiquidoPessoaFisica";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.0842741935483871;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell46,
            this.xrTableCell49,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.33576642335766421;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "Total Pessoa Juridica:";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell34.Weight = 0.25;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell46.Weight = 0.068145161290322581;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell49.Weight = 0.068145161290322581;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Text = "QtdPessoaJuridica";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.12822580645161291;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell54.Weight = 0.0939516129032258;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.Text = "ValorBrutoPessoaJuridica";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.085080645161290325;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.Text = "ValorIRPessoaJuridica";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.068548387096774188;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.Text = "ValorIOFPessoaJuridica";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.076612903225806453;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.Text = "ValorPerformancePessoaJuridica";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.07701612903225806;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.Text = "ValorLiquidoPessoaJuridica";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.0842741935483871;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.32846715328467152;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.Text = "Total Geral:";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 0.25;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell63.Weight = 0.068145161290322581;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell64.Weight = 0.068145161290322581;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell65.Text = "QtdTotal";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell65.Weight = 0.12822580645161291;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell66.Weight = 0.0939516129032258;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell67.Text = "ValorBrutoTotal";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell67.Weight = 0.085080645161290325;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell68.Text = "ValorIRTotal";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell68.Weight = 0.068548387096774188;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell69.Text = "ValorIOFPessoalJuridica";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell69.Weight = 0.076612903225806453;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell70.Text = "ValorPerformanceTotal";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell70.Weight = 0.07701612903225806;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.Text = "ValorLiquidoTotal";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell71.Weight = 0.0842741935483871;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportMovimentacaoFundo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.operacaoFundoCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportMovimentacaoFundo.ResourceManager;
        }
        
        //#region Variaveis Internas do Relatorio
        //protected class ValoresTotais {
        //    public decimal totalQuantidade = 0;
        //    public decimal totalValorBruto = 0;
        //    public decimal totalValorIR = 0;
        //    public decimal totalValorIOF = 0;
        //    public decimal totalValorPerformance = 0;
        //    public decimal totalValorLiquido = 0;
        //}

        ////
        //private ValoresTotais valoresPessoaFisica = new ValoresTotais();
        //private ValoresTotais valoresPessoaJuridica = new ValoresTotais();
        //private ValoresTotais valoresTotal = new ValoresTotais();
        //#endregion

        #region Funções Internas do Relatorio

        private bool IsDataOperacao() {
            return this.DataInicioOperacao.HasValue && this.DataFimOperacao.HasValue;
        }

        private bool IsDataConversao() {
            return this.DataInicioConversao.HasValue && this.DataFimConversao.HasValue;
        }

        private bool IsDataLiquidacao() {
            return this.DataInicioLiquidacao.HasValue && this.DataFimLiquidacao.HasValue;
        }
                
        //
        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {            
            XRTableCell dataInicioXRTableCell = sender as XRTableCell;

            if (this.IsDataConversao()) {
                dataInicioXRTableCell.Text = this.dataInicioConversao.Value.ToString("d");
            }
            else if (this.IsDataLiquidacao()) {
                dataInicioXRTableCell.Text = this.dataInicioLiquidacao.Value.ToString("d");
            }
            else if (this.IsDataOperacao()) {
                dataInicioXRTableCell.Text = this.dataInicioOperacao.Value.ToString("d");
            }
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataFimXRTableCell = sender as XRTableCell;

            if (this.IsDataConversao()) {
                dataFimXRTableCell.Text = this.dataFimConversao.Value.ToString("d");
            }
            else if (this.IsDataLiquidacao()) {
                dataFimXRTableCell.Text = this.dataFimLiquidacao.Value.ToString("d");
            }
            else if (this.IsDataOperacao()) {
                dataFimXRTableCell.Text = this.dataFimOperacao.Value.ToString("d");
            }          
        }
       
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell carteiraXRTableCell = sender as XRTableCell;
            carteiraXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {               
                int idCarteira = (int)this.GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCarteira);
                //
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Nome);
                //                
                carteira.LoadByPrimaryKey(campos, idCarteira);
                string nome = carteira.Nome;
                string nomeCarteira = idCarteira.ToString() + " - " + nome;
                //
                carteiraXRTableCell.Text = nomeCarteira;
            }
        }

        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell clienteXRTableCell = sender as XRTableCell;
            clienteXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCliente = (int)this.GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
                //
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                //                
                cliente.LoadByPrimaryKey(campos, idCliente);
                string apelido = cliente.Apelido;
                string nomeCliente = idCliente.ToString() + " - " + apelido;
                //
                //if (nomeCliente.Length > 30) {
                //    nomeCliente = nomeCliente.Substring(0, 29);
                //}
                clienteXRTableCell.Text = nomeCliente;
            }
        }

        private void TipoOperacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoOperacaoXRTableCell = sender as XRTableCell;

            tipoOperacaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                Int16 tipoOperacao = Convert.ToInt16(this.GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.TipoOperacao));
                int codigo = Convert.ToInt32(tipoOperacao);
                string tipoOperacaoString = TraducaoEnumsFundo.EnumTipoOperacaoFundo.TraduzEnum(codigo);

                tipoOperacaoXRTableCell.Text = tipoOperacaoString;
            }
        }

        private void ValorBrutoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal valorBruto = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.ValorBruto);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalValorBruto += valorBruto;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalValorBruto += valorBruto;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalValorBruto += valorBruto;
            //}
        }

        private void ValorIRBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal valorIR = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.ValorIR);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalValorIR += valorIR;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalValorIR += valorIR;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalValorIR += valorIR;
            //}
        }

        private void ValorIOFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal valorIOF = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.ValorIOF);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalValorIOF += valorIOF;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalValorIOF += valorIOF;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalValorIOF += valorIOF;
            //}

        }

        private void ValorPFeeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal valorPerformance = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.ValorPerformance);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalValorPerformance += valorPerformance;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalValorPerformance += valorPerformance;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalValorPerformance += valorPerformance;
            //}
        }

        private void ValorLiquidoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal valorLiquido = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.ValorLiquido);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalValorLiquido += valorLiquido;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalValorLiquido += valorLiquido;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalValorLiquido += valorLiquido;
            //}
        }

        private void QuantidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {

            //    decimal quantidade = (decimal)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.Quantidade);

            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCliente = (int)GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
            //    Pessoa pessoa = new Pessoa();
            //    //                
            //    List<esQueryItem> campos = new List<esQueryItem>();
            //    campos.Add(pessoa.Query.Tipo);
            //    pessoa.LoadByPrimaryKey(campos, idCliente);

            //    if (pessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalQuantidade += quantidade;
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalQuantidade += quantidade;
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotal.totalQuantidade += quantidade;
            //}
        }

        private void TableBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTable summaryFinal = sender as XRTable;
            
            //#region Limpa os Dados da Tabela
            //// 
            //for (int i = 0; i < summaryFinal.Rows.Count; i++) {
            //    int colunas = ((XRTableRow) summaryFinal.Rows[i]).Cells.Count;
            //    for (int j = 0; j < colunas; j++) {
            //        ((XRTableCell)summaryFinal.Rows[i].Cells[j]).Text = "";
            //    }                
            //}
            //#endregion

            //if (this.numeroLinhasDataTable != 0) {
            //    // Prenche Linha 0 - Total Pessoa Fisica
            //    /* 0-Pessoa Fisica Total:
            //       3-Quantidade
            //       5-ValorBruto
            //       6-ValorIR               
            //       7-ValorIOF
            //       8-ValorPerformance
            //       9-ValorLiquido
            //     */
            //    string pessoaFisica = Resources.ReportMovimentacaoFundo._PessoaFisica;
            //    string pessoaJuridica = Resources.ReportMovimentacaoFundo._PessoaJuridica;
            //    string total = Resources.ReportMovimentacaoFundo._Total;

            //    #region Linha 0
            //    XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            //    ((XRTableCell)summaryFinalRow0.Cells[0]).Text = pessoaFisica;
            //    ((XRTableCell)summaryFinalRow0.Cells[3]).Text = this.valoresPessoaFisica.totalQuantidade.ToString("n8");
            //    ((XRTableCell)summaryFinalRow0.Cells[5]).Text = this.valoresPessoaFisica.totalValorBruto.ToString("n2");
            //    ((XRTableCell)summaryFinalRow0.Cells[6]).Text = this.valoresPessoaFisica.totalValorIR.ToString("n2");
            //    ((XRTableCell)summaryFinalRow0.Cells[7]).Text = this.valoresPessoaFisica.totalValorIOF.ToString("n2");
            //    ((XRTableCell)summaryFinalRow0.Cells[8]).Text = this.valoresPessoaFisica.totalValorPerformance.ToString("n2");
            //    ((XRTableCell)summaryFinalRow0.Cells[9]).Text = this.valoresPessoaFisica.totalValorLiquido.ToString("n2");
            //    #endregion

            //    // Prenche Linha 1 - Total Pessoa Juridica
            //    #region Linha 1
            //    XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];
            //    ((XRTableCell)summaryFinalRow1.Cells[0]).Text = pessoaJuridica;
            //    ((XRTableCell)summaryFinalRow1.Cells[3]).Text = this.valoresPessoaJuridica.totalQuantidade.ToString("n8");
            //    ((XRTableCell)summaryFinalRow1.Cells[5]).Text = this.valoresPessoaJuridica.totalValorBruto.ToString("n2");
            //    ((XRTableCell)summaryFinalRow1.Cells[6]).Text = this.valoresPessoaJuridica.totalValorIR.ToString("n2");
            //    ((XRTableCell)summaryFinalRow1.Cells[7]).Text = this.valoresPessoaJuridica.totalValorIOF.ToString("n2");
            //    ((XRTableCell)summaryFinalRow1.Cells[8]).Text = this.valoresPessoaJuridica.totalValorPerformance.ToString("n2");
            //    ((XRTableCell)summaryFinalRow1.Cells[9]).Text = this.valoresPessoaJuridica.totalValorLiquido.ToString("n2");
            //    #endregion

            //    // Prenche Linha 2 - Total Geral
            //    #region Linha 2
            //    // Somatoria Pessoa Fisica, Pessoa Juridica
            //    //this.valoresTotal.totalQuantidade = this.valoresPessoaFisica.totalQuantidade + this.valoresPessoaJuridica.totalQuantidade;
            //    //this.valoresTotal.totalValorBruto = this.valoresPessoaFisica.totalValorBruto + this.valoresPessoaJuridica.totalValorBruto;
            //    //this.valoresTotal.totalValorIR = this.valoresPessoaFisica.totalValorIR + this.valoresPessoaJuridica.totalValorIR;
            //    //this.valoresTotal.totalValorIOF = this.valoresPessoaFisica.totalValorIOF + this.valoresPessoaJuridica.totalValorIOF;
            //    //this.valoresTotal.totalValorPerformance = this.valoresPessoaFisica.totalValorPerformance + this.valoresPessoaJuridica.totalValorPerformance;
            //    //this.valoresTotal.totalValorLiquido = this.valoresPessoaFisica.totalValorLiquido + this.valoresPessoaJuridica.totalValorLiquido;

            //    XRTableRow summaryFinalRow2 = summaryFinal.Rows[2];
            //    ((XRTableCell)summaryFinalRow2.Cells[0]).Text = total;
            //    ((XRTableCell)summaryFinalRow2.Cells[3]).Text = this.valoresTotal.totalQuantidade.ToString("n8");
            //    ((XRTableCell)summaryFinalRow2.Cells[5]).Text = this.valoresTotal.totalValorBruto.ToString("n2");
            //    ((XRTableCell)summaryFinalRow2.Cells[6]).Text = this.valoresTotal.totalValorIR.ToString("n2");
            //    ((XRTableCell)summaryFinalRow2.Cells[7]).Text = this.valoresTotal.totalValorIOF.ToString("n2");
            //    ((XRTableCell)summaryFinalRow2.Cells[8]).Text = this.valoresTotal.totalValorPerformance.ToString("n2");
            //    ((XRTableCell)summaryFinalRow2.Cells[9]).Text = this.valoresTotal.totalValorLiquido.ToString("n2");
            //    #endregion                
            //}
        }
       
        private void CarteiraCabecalhoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell carteiraXRTableCell = sender as XRTableCell;
            if (this.IdCarteira.HasValue) {
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Nome);
                cliente.LoadByPrimaryKey(campos, this.idCarteira.Value);

                string nome = !String.IsNullOrEmpty(cliente.Nome) ? cliente.Nome : "";
                carteiraXRTableCell.Text = nome;
            }
        }

        private void ClienteCabecalhoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell clienteXRTableCell = sender as XRTableCell;
            if (this.IdCliente.HasValue) {
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Nome);
                cliente.LoadByPrimaryKey(campos, this.idCliente.Value);

                string nome = !String.IsNullOrEmpty(cliente.Nome) ? cliente.Nome : "";
                clienteXRTableCell.Text = nome;
            }
        }

        private void TotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell totalXRTableCell = sender as XRTableCell;
            totalXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                totalXRTableCell.Text = Resources.ReportMapaOperacaoBolsa._TotalGeral;
            }

        }
        #endregion        
    }
}