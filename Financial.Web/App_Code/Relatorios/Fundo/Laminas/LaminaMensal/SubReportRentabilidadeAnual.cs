﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common.Exceptions;
using Financial.Fundo.Exceptions;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportRentabilidadeAnual
    /// </summary>
    public class SubReportRentabilidadeAnual : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        //
        private Carteira carteira;

        //private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        //public bool HasData {
        //    get { return this.numeroLinhasDataTable != 0; }
        //}

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell14;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell12;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRLabel xrLabel1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportRentabilidadeAnual() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //
            this.dataReferencia = dataReferencia;
            //
            // Consulta do SubRelatorio
            //DataTable dt = this.FillDados();
            //this.DataSource = dt;
            //this.numeroLinhasDataTable = dt.Rows.Count;

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }
        #endregion

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            //if (this.numeroLinhasDataTable == 0) {
                //this.xrTable10.Visible = false;
                //this.xrTable7.Visible = false;
                //this.xrTable4.Visible = false;
                //this.xrTable3.Visible = false;
                //this.xrTable5.Visible = false;
                //this.xrTable1.Visible = false;
            //}
        }

        private DataTable FillDados() {

            return new DataTable();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportRentabilidadeAnual.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportRentabilidadeAnual.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Height = 0;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PrintOnEmptyDataSource = false;
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable2});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.Height = 558;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(21, 3);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.Size = new System.Drawing.Size(444, 64);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Rentabilidade Anual do FIC de FIM GPAR em comparação com indicadores";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Transparent;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.xrTable2.Location = new System.Drawing.Point(21, 79);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable2.Size = new System.Drawing.Size(1000, 450);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDadosBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell14,
            this.xrTableCell13,
            this.xrTableCell34,
            this.xrTableCell45});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Size = new System.Drawing.Size(149, 45);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell14.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = "GPAR";
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell13.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Size = new System.Drawing.Size(251, 45);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.Text = "CDI";
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell34.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Size = new System.Drawing.Size(203, 45);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "Dólar";
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell45.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Size = new System.Drawing.Size(196, 45);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.Text = "Ibovespa";
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell20,
            this.xrTableCell36,
            this.xrTableCell47});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow3.StylePriority.UseBorders = false;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell4.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "2001";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell25,
            this.xrTableCell37,
            this.xrTableCell48});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow4.StylePriority.UseBorders = false;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell7.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "2002";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell26,
            this.xrTableCell38,
            this.xrTableCell49});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow5.StylePriority.UseBorders = false;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "2003";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell27,
            this.xrTableCell39,
            this.xrTableCell50});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow6.StylePriority.UseBorders = false;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell16.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "2004";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell6,
            this.xrTableCell28,
            this.xrTableCell40,
            this.xrTableCell51});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow7.StylePriority.UseBorders = false;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell3.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "2005";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Size = new System.Drawing.Size(202, 45);
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Location = new System.Drawing.Point(803, 0);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Size = new System.Drawing.Size(197, 45);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell12,
            this.xrTableCell29,
            this.xrTableCell41,
            this.xrTableCell52});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow8.StylePriority.UseBorders = false;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell9.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "2006";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell18,
            this.xrTableCell30,
            this.xrTableCell42,
            this.xrTableCell53});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow9.StylePriority.UseBorders = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell15.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "2007";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell31,
            this.xrTableCell43,
            this.xrTableCell54});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow11.StylePriority.UseBorders = false;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell21.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Size = new System.Drawing.Size(149, 45);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "2008*";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.AliceBlue;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Location = new System.Drawing.Point(149, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Size = new System.Drawing.Size(201, 45);
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Size = new System.Drawing.Size(251, 45);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Location = new System.Drawing.Point(601, 0);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Size = new System.Drawing.Size(203, 45);
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.Size = new System.Drawing.Size(196, 45);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell32});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(1000, 45);
            this.xrTableRow12.StylePriority.UseBorders = false;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Size = new System.Drawing.Size(899, 45);
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Mensagem";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Location = new System.Drawing.Point(899, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Size = new System.Drawing.Size(101, 45);
            // 
            // SubReportRentabilidadeAnual
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportRentabilidadeAnual.ResourceManager;
        }
        
        #region Funções Internas do Relatorio
        private void TableDadosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            /*
             *0       GPAR - CDI - Dolar - Ibovespa
             *1 2001
             *2 2002
             *3 2003
             *4 2004
             *5 2005
             *6 2006
             *7 2007
             *8 2008
            */

            // Hoje - 1 dia considerando Feriado e Final de Semana.
            DateTime dataOntem = Calendario.SubtraiDiaUtil(DateTime.Today, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            #region Calcula Rentabilidade do Fic Gpar

            List<decimal?> rentabilidadeGpar = new List<decimal?>();
            //
            CalculoMedida calculoMedidaGpar = new CalculoMedida(this.idCarteira);
            //
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();                        
            campos.Add(this.carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //                       
            calculoMedidaGpar.SetDataInicio(carteira.DataInicioCota.Value);
            //                                                                                                              
            #region rentabilidadeAno2001
            decimal? rentabilidadeAno2001 = null;
            try {
                //rentabilidadeAno2001 = calculoMedidaGpar.CalculaRetorno(new DateTime(2001, 1, 1), new DateTime(2001, 12, 31));
                rentabilidadeAno2001 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2001, 12, 31));
                rentabilidadeAno2001 = rentabilidadeAno2001 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2002 = null;
            try {
                //rentabilidadeAno2002 = calculoMedidaGpar.CalculaRetorno(new DateTime(2002, 1, 1), new DateTime(2002, 12, 31));
                rentabilidadeAno2002 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2002, 12, 31));
                rentabilidadeAno2002 = rentabilidadeAno2002 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2003 = null;
            try {
                //rentabilidadeAno2003 = calculoMedidaGpar.CalculaRetorno(new DateTime(2003, 1, 1), new DateTime(2003, 12, 31));
                rentabilidadeAno2003 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2003, 12, 31));
                rentabilidadeAno2003 = rentabilidadeAno2003 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2004 = null;
            try {                
                //rentabilidadeAno2004 = calculoMedidaGpar.CalculaRetorno(new DateTime(2004, 1, 1), new DateTime(2004, 12, 31));
                rentabilidadeAno2004 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2004, 12, 31));
                rentabilidadeAno2004 = rentabilidadeAno2004 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2005 = null;
            try {
                //rentabilidadeAno2005 = calculoMedidaGpar.CalculaRetorno(new DateTime(2005, 1, 1), new DateTime(2005, 12, 31));
                rentabilidadeAno2005 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2005, 12, 31));
                rentabilidadeAno2005 = rentabilidadeAno2005 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2006 = null;
            try {
                //rentabilidadeAno2006 = calculoMedidaGpar.CalculaRetorno(new DateTime(2006, 1, 1), new DateTime(2006, 12, 31));
                rentabilidadeAno2006 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2006, 12, 31));
                rentabilidadeAno2006 = rentabilidadeAno2006 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            decimal? rentabilidadeAno2007 = null;
            try {
                //rentabilidadeAno2007 = calculoMedidaGpar.CalculaRetorno(new DateTime(2007, 1, 1), new DateTime(2007, 12, 31));
                rentabilidadeAno2007 = calculoMedidaGpar.CalculaRetornoAnoFechado(new DateTime(2007, 12, 31));
                rentabilidadeAno2007 = rentabilidadeAno2007 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }

            // TODO em 2009 esse calculo muda
            decimal? rentabilidadeAno2008 = null;
            try {
                rentabilidadeAno2008 = calculoMedidaGpar.CalculaRetorno(new DateTime(2008, 12, 31), dataOntem);
                rentabilidadeAno2008 = rentabilidadeAno2008 / 100;
            }
            catch (HistoricoCotaNaoCadastradoException e1) { }

            #endregion

            rentabilidadeGpar.Add(rentabilidadeAno2001);
            rentabilidadeGpar.Add(rentabilidadeAno2002);
            rentabilidadeGpar.Add(rentabilidadeAno2003);
            rentabilidadeGpar.Add(rentabilidadeAno2004);
            rentabilidadeGpar.Add(rentabilidadeAno2005);
            rentabilidadeGpar.Add(rentabilidadeAno2006);
            rentabilidadeGpar.Add(rentabilidadeAno2007);
            rentabilidadeGpar.Add(rentabilidadeAno2008);
            #endregion

            #region Calcula Rentabilidade CDI/Dolar/Ibov Fechamento
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetIdIndice(ListaIndiceFixo.CDI);
            //
            #region CDI
            List<decimal?> cdi = new List<decimal?>();
            //
            decimal? cdiAno2001 = null;
            try {
                //cdiAno2001 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2001, 1, 1));
                //cdiAno2001 = calculoMedida.CalculaRetornoIndice(new DateTime(2000, 12, 31), new DateTime(2001, 12, 31));
                cdiAno2001 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2001, 12, 31));
                cdiAno2001 = cdiAno2001 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2002 = null;
            try {
                //cdiAno2002 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2002, 1, 1));
                //cdiAno2002 = calculoMedida.CalculaRetornoIndice(new DateTime(2001, 12, 31), new DateTime(2002, 12, 30));
                cdiAno2002 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2002, 12, 31));
                cdiAno2002 = cdiAno2002 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2003 = null;
            try {
                //cdiAno2003 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2003, 1, 1));
                //cdiAno2003 = calculoMedida.CalculaRetornoIndice(new DateTime(2003, 1, 1), new DateTime(2003, 12, 31));
                cdiAno2003 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2003, 12, 31));
                cdiAno2003 = cdiAno2003 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2004 = null;
            try {
                //cdiAno2004 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2004, 1, 1));
                //cdiAno2004 = calculoMedida.CalculaRetornoIndice(new DateTime(2004, 1, 1), new DateTime(2004, 12, 31));
                cdiAno2004 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2004, 12, 31));
                cdiAno2004 = cdiAno2004 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2005 = null;
            try {
                //cdiAno2005 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2005, 1, 1));
                //cdiAno2005 = calculoMedida.CalculaRetornoIndice(new DateTime(2005, 1, 1), new DateTime(2005, 12, 31));
                cdiAno2005 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2005, 12, 31));
                cdiAno2005 = cdiAno2005 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2006 = null;
            try {
                //cdiAno2006 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2006, 1, 1));
                //cdiAno2006 = calculoMedida.CalculaRetornoIndice(new DateTime(2006, 1, 1), new DateTime(2006, 12, 31));
                cdiAno2006 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2006, 12, 31));
                cdiAno2006 = cdiAno2006 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            //
            decimal? cdiAno2007 = null;
            try {
                //cdiAno2007 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2007, 1, 1));
                //cdiAno2007 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 1, 1), new DateTime(2007, 12, 31));
                cdiAno2007 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2007, 12, 31));
                cdiAno2007 = cdiAno2007 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            // TODO: Ano Corrente - Ao passar para 2009 esse calculo muda
            decimal? cdiAno2008 = null;            
            try {
                cdiAno2008 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 12, 31), dataOntem);
                cdiAno2008 = cdiAno2008 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            cdi.Add(cdiAno2001);
            cdi.Add(cdiAno2002);
            cdi.Add(cdiAno2003);
            cdi.Add(cdiAno2004);
            cdi.Add(cdiAno2005);
            cdi.Add(cdiAno2006);
            cdi.Add(cdiAno2007);
            cdi.Add(cdiAno2008);
            #endregion

            calculoMedida.SetIdIndice(ListaIndiceFixo.PTAX_800VENDA);
            #region Dolar
            List<decimal?> ptax800Venda = new List<decimal?>();
            //
            decimal? ptax800Ano2001 = null;
            try {
                //ptax800Ano2001 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2001,1,1));
                //ptax800Ano2001 = calculoMedida.CalculaRetornoIndice(new DateTime(2001, 1, 1), new DateTime(2001, 12, 31));
                ptax800Ano2001 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2001, 12, 31));
                ptax800Ano2001 = ptax800Ano2001 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException e1) { }

            decimal? ptax800Ano2002 = null;
            try {
                //ptax800Ano2002 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2002, 1, 1));
                //ptax800Ano2002 = calculoMedida.CalculaRetornoIndice(new DateTime(2002, 1, 1), new DateTime(2002, 12, 31));
                ptax800Ano2002 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2002, 12, 31));
                ptax800Ano2002 = ptax800Ano2002 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano2003 = null;
            try {
                //ptax800Ano2003 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2003, 1, 1));
                //ptax800Ano2003 = calculoMedida.CalculaRetornoIndice(new DateTime(2003, 1, 1), new DateTime(2003, 12, 31));
                ptax800Ano2003 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2003, 12, 31));
                ptax800Ano2003 = ptax800Ano2003 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano2004 = null;
            try {
                //ptax800Ano2004 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2004, 1, 1));
                //ptax800Ano2004 = calculoMedida.CalculaRetornoIndice(new DateTime(2004, 1, 1), new DateTime(2004, 12, 31));
                ptax800Ano2004 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2004, 12, 31));
                ptax800Ano2004 = ptax800Ano2004 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano2005 = null;
            try {
                //ptax800Ano2005 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2005, 1, 1));
                //ptax800Ano2005 = calculoMedida.CalculaRetornoIndice(new DateTime(2005, 1, 1), new DateTime(2005, 12, 31));
                ptax800Ano2005 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2005, 12, 31));
                ptax800Ano2005 = ptax800Ano2005 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano2006 = null;
            try {
                //ptax800Ano2006 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2006, 1, 1));
                //ptax800Ano2006 = calculoMedida.CalculaRetornoIndice(new DateTime(2006, 1, 1), new DateTime(2006, 12, 31));
                ptax800Ano2006 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2006, 12, 31));
                ptax800Ano2006 = ptax800Ano2006 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ptax800Ano2007 = null;
            try {
                //ptax800Ano2007 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2007, 1, 1));
                //ptax800Ano2007 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 1, 1), new DateTime(2007, 12, 31));
                ptax800Ano2007 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2007, 12, 31));
                ptax800Ano2007 = ptax800Ano2007 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            // TODO: Ano Corrente - Ao passar para 2009 esse calculo muda
            decimal? ptax800Ano2008 = null;
            try {
                ptax800Ano2008 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 12, 31), dataOntem);
                ptax800Ano2008 = ptax800Ano2008 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            ptax800Venda.Add(ptax800Ano2001);
            ptax800Venda.Add(ptax800Ano2002);
            ptax800Venda.Add(ptax800Ano2003);
            ptax800Venda.Add(ptax800Ano2004);
            ptax800Venda.Add(ptax800Ano2005);
            ptax800Venda.Add(ptax800Ano2006);
            ptax800Venda.Add(ptax800Ano2007);
            ptax800Venda.Add(ptax800Ano2008);
            #endregion

            calculoMedida.SetIdIndice(ListaIndiceFixo.IBOVESPA_FECHA);
            #region IbovFechamento
            List<decimal?> ibovespaFechamento = new List<decimal?>();
            //
            decimal? ibovespaFechamentoAno2001 = null;
            try {
                //ibovespaFechamentoAno2001 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2001, 1, 1));
                //ibovespaFechamentoAno2001 = calculoMedida.CalculaRetornoIndice(new DateTime(2001, 1, 1), new DateTime(2001, 12, 31));
                ibovespaFechamentoAno2001 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2001, 12, 31));
                ibovespaFechamentoAno2001 = ibovespaFechamentoAno2001 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2002 = null;
            try {
                //ibovespaFechamentoAno2002 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2002, 1, 1));
                //ibovespaFechamentoAno2002 = calculoMedida.CalculaRetornoIndice(new DateTime(2002, 1, 1), new DateTime(2002, 12, 31));
                ibovespaFechamentoAno2002 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2002, 12, 31));
                ibovespaFechamentoAno2002 = ibovespaFechamentoAno2002 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2003 = null;
            try {
                //ibovespaFechamentoAno2003 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2003, 1, 1));
                //ibovespaFechamentoAno2003 = calculoMedida.CalculaRetornoIndice(new DateTime(2003, 1, 1), new DateTime(2003, 12, 31));
                ibovespaFechamentoAno2003 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2003, 12, 31));
                ibovespaFechamentoAno2003 = ibovespaFechamentoAno2003 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2004 = null;
            try {
                //ibovespaFechamentoAno2004 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2004, 1, 1));
                //ibovespaFechamentoAno2004 = calculoMedida.CalculaRetornoIndice(new DateTime(2004, 1, 1), new DateTime(2004, 12, 31));
                ibovespaFechamentoAno2004 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2004, 12, 31));
                ibovespaFechamentoAno2004 = ibovespaFechamentoAno2004 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2005 = null;
            try {
                //ibovespaFechamentoAno2005 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2005, 1, 1));
                //ibovespaFechamentoAno2005 = calculoMedida.CalculaRetornoIndice(new DateTime(2005, 1, 1), new DateTime(2005, 12, 31));
                ibovespaFechamentoAno2005 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2005, 12, 31));
                ibovespaFechamentoAno2005 = ibovespaFechamentoAno2005 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2006 = null;
            try {
                //ibovespaFechamentoAno2006 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2006, 1, 1));
                //ibovespaFechamentoAno2006 = calculoMedida.CalculaRetornoIndice(new DateTime(2006, 1, 1), new DateTime(2006, 12, 31));
                ibovespaFechamentoAno2006 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2006, 12, 31));
                ibovespaFechamentoAno2006 = ibovespaFechamentoAno2006 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            decimal? ibovespaFechamentoAno2007 = null;
            try {
                //ibovespaFechamentoAno2007 = calculoMedida.CalculaRetornoAnoIndice(new DateTime(2007, 1, 1));
                //ibovespaFechamentoAno2007 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 1, 1), new DateTime(2007, 12, 31));
                ibovespaFechamentoAno2007 = calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(2007, 12, 31));
                ibovespaFechamentoAno2007 = ibovespaFechamentoAno2007 / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            // TODO: Ano Corrente - Ao passar para 2009 esse calculo muda
            decimal? ibovespaFechamentoAno2008 = null;
            try {
                ibovespaFechamentoAno2008 = calculoMedida.CalculaRetornoIndice(new DateTime(2007, 12, 31), dataOntem);
                ibovespaFechamentoAno2008 = ibovespaFechamentoAno2008 / 100;            
            }
            catch (CotacaoIndiceNaoCadastradoException) { }

            ibovespaFechamento.Add(ibovespaFechamentoAno2001);
            ibovespaFechamento.Add(ibovespaFechamentoAno2002);
            ibovespaFechamento.Add(ibovespaFechamentoAno2003);
            ibovespaFechamento.Add(ibovespaFechamentoAno2004);
            ibovespaFechamento.Add(ibovespaFechamentoAno2005);
            ibovespaFechamento.Add(ibovespaFechamentoAno2006);
            ibovespaFechamento.Add(ibovespaFechamentoAno2007);
            ibovespaFechamento.Add(ibovespaFechamentoAno2008);
            #endregion

            #endregion
            // Linhas
            XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];
            XRTableRow summaryFinalRow2 = summaryFinal.Rows[2];
            XRTableRow summaryFinalRow3 = summaryFinal.Rows[3];
            XRTableRow summaryFinalRow4 = summaryFinal.Rows[4];
            XRTableRow summaryFinalRow5 = summaryFinal.Rows[5];
            XRTableRow summaryFinalRow6 = summaryFinal.Rows[6];
            XRTableRow summaryFinalRow7 = summaryFinal.Rows[7];
            XRTableRow summaryFinalRow8 = summaryFinal.Rows[8];
            XRTableRow summaryFinalRow9 = summaryFinal.Rows[9];

            /* GPar - Coluna 1 */
            #region Gpar
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[1], rentabilidadeGpar[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[1], rentabilidadeGpar[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[1], rentabilidadeGpar[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[1], rentabilidadeGpar[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[1], rentabilidadeGpar[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[1], rentabilidadeGpar[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[1], rentabilidadeGpar[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[1], rentabilidadeGpar[7]);
            #endregion

            /* CDI - Coluna 2 */ 
            #region CDI
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[2], cdi[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[2], cdi[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[2], cdi[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[2], cdi[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[2], cdi[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[2], cdi[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[2], cdi[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[2], cdi[7]);
            #endregion

            /* Dolar - Coluna 3 */
            #region Dolar
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[3], ptax800Venda[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[3], ptax800Venda[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[3], ptax800Venda[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[3], ptax800Venda[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[3], ptax800Venda[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[3], ptax800Venda[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[3], ptax800Venda[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[3], ptax800Venda[7]);
            #endregion

            /* IbovFechamento - Coluna 4 */
            #region Ibov
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[4], ibovespaFechamento[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[4], ibovespaFechamento[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[4], ibovespaFechamento[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[4], ibovespaFechamento[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[4], ibovespaFechamento[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[4], ibovespaFechamento[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[4], ibovespaFechamento[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[4], ibovespaFechamento[7]);
            #endregion

            ((XRTableCell)summaryFinalRow9.Cells[0]).Text = "* Retorno Acumulado no Ano até " + dataOntem.ToString("d");

        }
        #endregion
    }
}
