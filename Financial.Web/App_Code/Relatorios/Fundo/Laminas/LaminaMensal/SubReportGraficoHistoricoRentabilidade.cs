﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportGraficoHistoricoRentabilidade
    /// </summary>
    public class SubReportGraficoHistoricoRentabilidade : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        /// <summary>
        /// Dicionario onde a chave é a Data Do Fundo GPAR e o Valor é a Rentabilidade na Data
        /// </summary>
        private Dictionary<DateTime, decimal> dadosRentabilidadeGpar = new Dictionary<DateTime, decimal>();

        /// <summary>
        /// Dicionario onde a chave é a Data Do Fundo CDI e o Valor é a Rentabilidade na Data
        /// </summary>
        private Dictionary<DateTime, decimal> dadosRentabilidadeCDI = new Dictionary<DateTime, decimal>();

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRLine xrLine1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportGraficoHistoricoRentabilidade() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            //
            ReportBase relatorioBase = new ReportBase(this);
            this.CarregaDadosRentabilidade();
            this.FillDadosGrafico();
        }
        #endregion

        /// <summary>
        /// Carrega num Dicionario uma lista de Datas com as Respectivas Rentabilidades Do GPAR e do CDI
        /// </summary>
        private void CarregaDadosRentabilidade() {
            this.dadosRentabilidadeCDI.Clear();
            this.dadosRentabilidadeGpar.Clear();

            //this.dadosRentabilidadeCDI.Add(new DateTime(2008, 1, 1), 100);
            //this.dadosRentabilidadeCDI.Add(new DateTime(2008, 2, 1), 111);
            //this.dadosRentabilidadeCDI.Add(new DateTime(2008, 3, 1), 112);

            //this.dadosRentabilidadeGpar.Add(new DateTime(2008, 1, 1), 10);
            //this.dadosRentabilidadeGpar.Add(new DateTime(2007, 1, 1), 20);
            //this.dadosRentabilidadeGpar.Add(new DateTime(2006, 1, 1), 30);

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.IdCarteira);
            campos.Add(carteira.Query.DataInicioCota);
            //
            carteira.LoadByPrimaryKey(campos, this.idCarteira);

            //DateTime dataInicioAux = carteira.DataInicioCota.Value;
            DateTime dataInicioAux = this.dataReferencia;
            DateTime dataInicio = Calendario.AdicionaDiaUtil(dataInicioAux, 0);
            DateTime dataFinal = Calendario.SubtraiDiaUtil(DateTime.Today,5);

            #region Dados Do GPAR/CDI
            // Carrega o Dicionario
            decimal fatorMultiplicacao = 1;
            decimal cotaPrimeiroDia = 1;
            int i = 0;
            while (dataInicio.CompareTo(dataFinal) <= 0) {

                #region Gpar1
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCota(this.idCarteira, dataInicio);

                // Não adiciona Rentabilidade quando não houver cota
                if (historicoCota.es.HasData && historicoCota.CotaFechamento.HasValue) {
                    decimal cotaFechamento = historicoCota.CotaFechamento.Value;
                    if (i == 0) {
                        cotaPrimeiroDia = cotaFechamento;
                    }                    
                    //decimal rendimento = cotaFechamento - 1;
                    decimal rendimento = (cotaFechamento / cotaPrimeiroDia) - 1;
                    rendimento = rendimento * 100;

                    this.dadosRentabilidadeGpar.Add(dataInicio, rendimento);
                }
                i++;
                #endregion

                #region CDI
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                try {
                    cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.CDI, dataInicio);
                    decimal fator = CalculoFinanceiro.ConverteBaseFator(cotacaoIndice.Valor.Value, BaseCalculo.Base252);
                    fator = fator * fatorMultiplicacao;

                    decimal rendimentoCDI = fator - 1;
                    this.dadosRentabilidadeCDI.Add(dataInicio, rendimentoCDI * 100);
                     //FatorMultiplicacao
                    fatorMultiplicacao = fator;
                }
                catch (CotacaoIndiceNaoCadastradoException e) {
                     //Não adiciona se não houver CotacaoIndice
                }
                #endregion
                dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1);
            }
            #endregion                        
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico() {
            #region DataTableExample
            
            // Codigo para Funcionar com DataTable
            //DataTable tableDados = new DataTable();
            //// Add two columns to the table
            //tableDados.Columns.Add("Rentabilidades", typeof(String));
            //tableDados.Columns.Add("Datas", typeof(DateTime));
            //tableDados.Columns.Add("Valores", typeof(Decimal));

            //tableDados.Rows.Add(new object[] { "CDI", new DateTime(2008, 1, 1), 20.0M });
            //tableDados.Rows.Add(new object[] { "CDI", new DateTime(2008, 2, 1), 30.0M });
            //tableDados.Rows.Add(new object[] { "CDI", new DateTime(2008, 3, 1), 25.0M });
            //tableDados.Rows.Add(new object[] { "CDI", new DateTime(2008, 4, 1), 40.0M });

            //tableDados.Rows.Add(new object[] { "GPAR", new DateTime(2008, 1, 1), 120.0M });
            //tableDados.Rows.Add(new object[] { "GPAR", new DateTime(2008, 2, 1), 130.0M });
            //tableDados.Rows.Add(new object[] { "GPAR", new DateTime(2008, 3, 1), 125.0M });
            //tableDados.Rows.Add(new object[] { "GPAR", new DateTime(2008, 4, 1), 140.0M });

            //foreach (KeyValuePair<DateTime, decimal> pair in this.dadosRentabilidadeCDI) {
            //    tableDados.Rows.Add(new object[] { "CDI", pair.Key, pair.Value });
            //}

            ////Generate a data table and bind the chart to it.
            //this.xrChart1.DataSource = tableDados;

            //// Specify data members to bind the chart's series template.
            //this.xrChart1.SeriesDataMember = "Rentabilidades";
            //this.xrChart1.SeriesTemplate.ArgumentDataMember = "Datas";
            //this.xrChart1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Valores" });

            //LineSeriesView l = new LineSeriesView();
            ////l.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Year;
            //this.xrChart1.SeriesTemplate.View = l;            

            #endregion           
                       
            #region Adiciona a Série de GPAR
            Series series1 = new Series("GPAR", ViewType.Line);
            //
            foreach (KeyValuePair<DateTime, decimal> pair in this.dadosRentabilidadeGpar) {
                series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series1.ArgumentScaleType = ScaleType.DateTime;
            //
            this.xrChart1.Series.Add(series1);
            #endregion

            #region Adiciona a Série de CDI
            Series series2 = new Series("CDI", ViewType.Line);
            //
            //SeriesPoint seriesPoint1 = new SeriesPoint(new System.DateTime(2008, 1, 1, 0, 0, 0, 0), new object[] { ((object)(100)) });
            //SeriesPoint seriesPoint2 = new SeriesPoint(new System.DateTime(2008, 2, 1, 0, 0, 0, 0), new object[] { ((object)(110)) });
            //SeriesPoint seriesPoint3 = new SeriesPoint(new System.DateTime(2008, 3, 1, 0, 0, 0, 0), new object[] { ((object)(120)) });
            //SeriesPoint seriesPoint4 = new SeriesPoint(new System.DateTime(2008, 3, 1, 0, 0, 0, 0), new object[] { ((object)(130)) });
            //series1.Points.AddRange(new SeriesPoint[] { seriesPoint1, seriesPoint2, seriesPoint3, seriesPoint4 });

            //series1.Points.Add(new SeriesPoint(new System.DateTime(2008, 1, 1), new object[] { 10.0M }));
            //series1.Points.Add(new SeriesPoint(new System.DateTime(2008, 2, 1), new object[] { 11.0M }));
            //series1.Points.Add(new SeriesPoint(new System.DateTime(2008, 3, 1), new object[] { 12.0M }));
            //series1.Points.Add(new SeriesPoint(new System.DateTime(2008, 4, 1), new object[] { 12.0M }));
            //
            foreach (KeyValuePair<DateTime, decimal> pair in this.dadosRentabilidadeCDI) {
                series2.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series2.ArgumentScaleType = ScaleType.DateTime;

            //
            this.xrChart1.Series.Add(series2);
            #endregion

            this.xrChart1.Series[2].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.Series[3].LabelsVisibility = DevExpress.Utils.DefaultBoolean.False; // CDI
            ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Day;
            //((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.GridAlignment = DateTimeGridAlignment.Day;

        }
       
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/Laminas/LaminaMensal/SubReportGraficoHistoricoRentabilidade.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportGraficoHistoricoRentabilidade.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Height = 0;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PrintOnEmptyDataSource = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrLabel1,
            this.xrChart1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.Height = 1273;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.SteelBlue;
            this.xrLine1.LineWidth = 7;
            this.xrLine1.Location = new System.Drawing.Point(0, 64);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(614, 42);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(53, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.Size = new System.Drawing.Size(423, 64);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Histórico de Rentabilidade";
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Terracotta Pie";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.Visible = false;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(1, 1);
            this.xrChart1.Legend.MarkerVisible = false;
            this.xrChart1.Location = new System.Drawing.Point(0, 127);
            this.xrChart1.Name = "xrChart1";
            series1.Name = "CDIAAux";
            lineSeriesView1.LineMarkerOptions.Visible = false;
            series1.View = lineSeriesView1;
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series2.Name = "FIC DE FIM GPAR";
            lineSeriesView2.LineMarkerOptions.Visible = false;
            series2.View = lineSeriesView2;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.Visible = false;
            series2.Label = pointSeriesLabel2;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.xrChart1.SeriesTemplate.View = lineSeriesView3;
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            this.xrChart1.Size = new System.Drawing.Size(1905, 783);
            chartTitle1.Indent = 2;
            chartTitle1.Text = "Retorno Acumulado FIC de FIM GPAR vs CDI";
            chartTitle1.TextColor = System.Drawing.Color.Black;
            this.xrChart1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            this.xrChart1.CustomDrawSeries += new DevExpress.XtraCharts.CustomDrawSeriesEventHandler(this.xrChart1_CustomDrawSeries);
            // 
            // SubReportGraficoHistoricoRentabilidade
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportGraficoHistoricoRentabilidade.ResourceManager;
        }

        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e) {
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            //((LineDrawOptions)e.SeriesDrawOptions).LineStyle.DashStyle = DashStyle.Solid;
        }
        
        #region Funções Internas do Relatorio
        
        #endregion
    }
}
