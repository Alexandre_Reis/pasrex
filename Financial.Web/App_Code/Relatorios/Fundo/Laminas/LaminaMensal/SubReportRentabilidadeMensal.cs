﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common.Exceptions;
using Financial.Fundo.Exceptions;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportRentabilidadeMensal
    /// </summary>
    public class SubReportRentabilidadeMensal : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        //
        private Carteira carteira;

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRTable xrTable2;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell12;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableRow xrTableRow15;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell14;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell31;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell26;
        private XRLabel xrLabel3;
        private XRLabel xrLabel1;
        private XRTableRow xrTableRow16;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableRow xrTableRow17;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCell72;
        private XRTableCell xrTableCell73;
        private XRTableCell xrTableCell74;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell76;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell78;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell80;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableCell xrTableCell83;
        private XRTableCell xrTableCell84;
        private XRTableCell xrTableCell85;
        private XRTableCell xrTableCell86;
        private XRTableCell xrTableCell87;
        private XRTableCell xrTableCell88;
        private XRTableCell xrTableCell89;
        private XRTableCell xrTableCell90;
        private XRTableCell xrTableCell91;
        private XRTableCell xrTableCell92;
        private XRTableCell xrTableCell93;
        private XRTableCell xrTableCell94;
        private XRTableCell xrTableCell95;
        private XRTableCell xrTableCell96;
        private XRTableCell xrTableCell97;
        private XRTableCell xrTableCell98;
        private XRTableCell xrTableCell99;
        private XRTableCell xrTableCell100;
        private XRTableCell xrTableCell101;
        private XRTableCell xrTableCell102;
        private XRTableCell xrTableCell103;
        private XRTableCell xrTableCell104;
        private XRTableCell xrTableCell105;
        private XRTableCell xrTableCell106;
        private XRTableCell xrTableCell107;
        private XRTableCell xrTableCell108;
        private XRTableCell xrTableCell109;
        private XRTableCell xrTableCell110;
        private XRTableCell xrTableCell111;
        private XRTableCell xrTableCell112;
        private XRTableCell xrTableCell113;
        private XRTableCell xrTableCell114;
        private XRTableCell xrTableCell115;
        private XRTableCell xrTableCell116;
        private XRTableCell xrTableCell117;
        private XRTableCell xrTableCell118;
        private XRTableCell xrTableCell119;
        private XRTableCell xrTableCell120;
        private XRTableCell xrTableCell121;
        private XRTableCell xrTableCell122;
        private XRTableCell xrTableCell123;
        private XRTableCell xrTableCell124;
        private XRTableCell xrTableCell125;
        private XRTableCell xrTableCell126;
        private XRTableCell xrTableCell127;
        private XRTableCell xrTableCell128;
        private XRTableCell xrTableCell129;
        private XRTableCell xrTableCell130;
        private XRTableCell xrTableCell131;
        private XRTableCell xrTableCell132;
        private XRTableCell xrTableCell133;
        private XRTableCell xrTableCell134;
        private XRTableCell xrTableCell135;
        private XRTableCell xrTableCell136;
        private XRTableCell xrTableCell137;
        private XRTableCell xrTableCell155;
        private XRTableCell xrTableCell138;
        private XRTableCell xrTableCell156;
        private XRTableCell xrTableCell139;
        private XRTableCell xrTableCell157;
        private XRTableCell xrTableCell140;
        private XRTableCell xrTableCell158;
        private XRTableCell xrTableCell141;
        private XRTableCell xrTableCell159;
        private XRTableCell xrTableCell142;
        private XRTableCell xrTableCell160;
        private XRTableCell xrTableCell143;
        private XRTableCell xrTableCell161;
        private XRTableCell xrTableCell144;
        private XRTableCell xrTableCell162;
        private XRTableCell xrTableCell145;
        private XRTableCell xrTableCell163;
        private XRTableCell xrTableCell146;
        private XRTableCell xrTableCell164;
        private XRTableCell xrTableCell147;
        private XRTableCell xrTableCell165;
        private XRTableCell xrTableCell148;
        private XRTableCell xrTableCell166;
        private XRTableCell xrTableCell149;
        private XRTableCell xrTableCell167;
        private XRTableCell xrTableCell150;
        private XRTableCell xrTableCell168;
        private XRTableCell xrTableCell151;
        private XRTableCell xrTableCell169;
        private XRTableCell xrTableCell152;
        private XRTableCell xrTableCell170;
        private XRTableCell xrTableCell153;
        private XRTableCell xrTableCell171;
        private XRTableCell xrTableCell154;
        private XRTableCell xrTableCell172;
        private XRTableCell xrTableCell173;
        private XRTableCell xrTableCell174;
        private XRTableCell xrTableCell175;
        private XRTableCell xrTableCell176;
        private XRTableCell xrTableCell177;
        private XRTableCell xrTableCell178;
        private XRTableCell xrTableCell179;
        private XRTableCell xrTableCell180;
        private XRTableCell xrTableCell181;
        private XRTableCell xrTableCell182;
        private XRTableCell xrTableCell183;
        private XRTableCell xrTableCell184;
        private XRTableCell xrTableCell185;
        private XRTableCell xrTableCell186;
        private XRTableCell xrTableCell187;
        private XRTableCell xrTableCell188;
        private XRTableCell xrTableCell189;
        private XRTableCell xrTableCell206;
        private XRTableCell xrTableCell223;
        private XRTableCell xrTableCell190;
        private XRTableCell xrTableCell207;
        private XRTableCell xrTableCell224;
        private XRTableCell xrTableCell191;
        private XRTableCell xrTableCell208;
        private XRTableCell xrTableCell225;
        private XRTableCell xrTableCell192;
        private XRTableCell xrTableCell209;
        private XRTableCell xrTableCell226;
        private XRTableCell xrTableCell193;
        private XRTableCell xrTableCell210;
        private XRTableCell xrTableCell227;
        private XRTableCell xrTableCell194;
        private XRTableCell xrTableCell211;
        private XRTableCell xrTableCell228;
        private XRTableCell xrTableCell195;
        private XRTableCell xrTableCell212;
        private XRTableCell xrTableCell229;
        private XRTableCell xrTableCell196;
        private XRTableCell xrTableCell213;
        private XRTableCell xrTableCell230;
        private XRTableCell xrTableCell197;
        private XRTableCell xrTableCell214;
        private XRTableCell xrTableCell231;
        private XRTableCell xrTableCell198;
        private XRTableCell xrTableCell215;
        private XRTableCell xrTableCell232;
        private XRTableCell xrTableCell199;
        private XRTableCell xrTableCell216;
        private XRTableCell xrTableCell233;
        private XRTableCell xrTableCell200;
        private XRTableCell xrTableCell217;
        private XRTableCell xrTableCell234;
        private XRTableCell xrTableCell201;
        private XRTableCell xrTableCell218;
        private XRTableCell xrTableCell235;
        private XRTableCell xrTableCell202;
        private XRTableCell xrTableCell219;
        private XRTableCell xrTableCell236;
        private XRTableCell xrTableCell203;
        private XRTableCell xrTableCell220;
        private XRTableCell xrTableCell237;
        private XRTableCell xrTableCell204;
        private XRTableCell xrTableCell221;
        private XRTableCell xrTableCell238;
        private XRTableCell xrTableCell205;
        private XRTableCell xrTableCell222;
        private XRTableCell xrTableCell239;
        private XRTableCell xrTableCell240;
        private XRTableCell xrTableCell241;
        private XRTableCell xrTableCell242;
        private XRTableCell xrTableCell243;
        private XRTableCell xrTableCell244;
        private XRTableCell xrTableCell245;
        private XRTableCell xrTableCell246;
        private XRTableCell xrTableCell247;
        private XRTableCell xrTableCell248;
        private XRTableCell xrTableCell249;
        private XRTableCell xrTableCell250;
        private XRTableCell xrTableCell251;
        private XRTableCell xrTableCell252;
        private XRTableCell xrTableCell253;
        private XRTableCell xrTableCell254;
        private XRTableCell xrTableCell255;
        private XRTableCell xrTableCell256;
        private XRTableRow xrTableRow18;
        private XRTableCell xrTableCell257;
        private XRTableCell xrTableCell258;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportRentabilidadeMensal() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //
            this.dataReferencia = dataReferencia;
            //
            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }
        #endregion

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                //this.xrTable10.Visible = false;
                //this.xrTable7.Visible = false;
                //this.xrTable4.Visible = false;
                //this.xrTable3.Visible = false;
                //this.xrTable5.Visible = false;
                //this.xrTable1.Visible = false;
            }
        }

        private DataTable FillDados() {

            return new DataTable();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportRentabilidadeMensal.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportRentabilidadeMensal.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Height = 13;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PrintOnEmptyDataSource = false;
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel1,
            this.xrTable2});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.Height = 609;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.xrLabel3.Location = new System.Drawing.Point(10, 566);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.Size = new System.Drawing.Size(847, 26);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Rentabilidade líquida de taxa de administração e bruta de impostos";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(10, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.Size = new System.Drawing.Size(804, 35);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Rentabilidade histórica mensal do FIC de FIM GPAR em comparação com CDI";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Transparent;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.xrTable2.Location = new System.Drawing.Point(0, 58);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow13,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable2.Size = new System.Drawing.Size(1947, 487);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDadosBeforePrint);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell53,
            this.xrTableCell36,
            this.xrTableCell121,
            this.xrTableCell70,
            this.xrTableCell155,
            this.xrTableCell87,
            this.xrTableCell172,
            this.xrTableCell104,
            this.xrTableCell138,
            this.xrTableCell189,
            this.xrTableCell206,
            this.xrTableCell223,
            this.xrTableCell240});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow1.StylePriority.UseBorders = false;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.Text = "Jan";
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "Fev";
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell121.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell121.StylePriority.UseFont = false;
            this.xrTableCell121.Text = "Mar";
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell70.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.Text = "Abr";
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell155.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell155.StylePriority.UseFont = false;
            this.xrTableCell155.Text = "Mai";
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell87.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.Text = "Jun";
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell172.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell172.StylePriority.UseFont = false;
            this.xrTableCell172.Text = "Jul";
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell104.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.Text = "Ago";
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Dpi = 254F;
            this.xrTableCell138.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell138.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell138.StylePriority.UseFont = false;
            this.xrTableCell138.Text = "Set";
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell189.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell189.StylePriority.UseFont = false;
            this.xrTableCell189.Text = "Out";
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell206.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell206.StylePriority.UseFont = false;
            this.xrTableCell206.Text = "Nov";
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Dpi = 254F;
            this.xrTableCell223.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell223.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell223.StylePriority.UseFont = false;
            this.xrTableCell223.Text = "Dez";
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Dpi = 254F;
            this.xrTableCell240.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell240.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell240.Text = "Ano";
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell54,
            this.xrTableCell37,
            this.xrTableCell122,
            this.xrTableCell71,
            this.xrTableCell156,
            this.xrTableCell88,
            this.xrTableCell173,
            this.xrTableCell105,
            this.xrTableCell139,
            this.xrTableCell190,
            this.xrTableCell207,
            this.xrTableCell224,
            this.xrTableCell241});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow3.StylePriority.UseBorders = false;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell4.Location = new System.Drawing.Point(0, 1);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "2001";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell5.Location = new System.Drawing.Point(141, 1);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "GPAR%";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Location = new System.Drawing.Point(300, 2);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell54.StylePriority.UseBackColor = false;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Location = new System.Drawing.Point(428, 2);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell37.StylePriority.UseBackColor = false;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell122.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell122.StylePriority.UseBackColor = false;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell71.StylePriority.UseBackColor = false;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell156.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell156.StylePriority.UseBackColor = false;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell88.Dpi = 254F;
            this.xrTableCell88.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell88.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell88.StylePriority.UseBackColor = false;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell173.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell173.StylePriority.UseBackColor = false;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell105.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell105.StylePriority.UseBackColor = false;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell139.Dpi = 254F;
            this.xrTableCell139.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell139.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell139.StylePriority.UseBackColor = false;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell190.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell190.StylePriority.UseBackColor = false;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell207.Dpi = 254F;
            this.xrTableCell207.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell207.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell207.StylePriority.UseBackColor = false;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell224.Dpi = 254F;
            this.xrTableCell224.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell224.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell224.StylePriority.UseBackColor = false;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell241.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell241.Dpi = 254F;
            this.xrTableCell241.Location = new System.Drawing.Point(1817, 2);
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell241.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell241.StylePriority.UseBackColor = false;
            this.xrTableCell241.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell31,
            this.xrTableCell55,
            this.xrTableCell38,
            this.xrTableCell123,
            this.xrTableCell72,
            this.xrTableCell157,
            this.xrTableCell89,
            this.xrTableCell174,
            this.xrTableCell106,
            this.xrTableCell140,
            this.xrTableCell191,
            this.xrTableCell208,
            this.xrTableCell225,
            this.xrTableCell242});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(1947, 27);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Location = new System.Drawing.Point(0, 1);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Size = new System.Drawing.Size(141, 27);
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "CDI%";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell123.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell72.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell157.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell89.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell174.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell106.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Dpi = 254F;
            this.xrTableCell140.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell140.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell191.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Dpi = 254F;
            this.xrTableCell208.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell208.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Dpi = 254F;
            this.xrTableCell225.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell225.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Dpi = 254F;
            this.xrTableCell242.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell242.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell56,
            this.xrTableCell39,
            this.xrTableCell124,
            this.xrTableCell73,
            this.xrTableCell158,
            this.xrTableCell90,
            this.xrTableCell175,
            this.xrTableCell107,
            this.xrTableCell141,
            this.xrTableCell192,
            this.xrTableCell209,
            this.xrTableCell226,
            this.xrTableCell243});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow4.StylePriority.UseBorders = false;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "2002";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "GPAR%";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell56.StylePriority.UseBackColor = false;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell39.StylePriority.UseBackColor = false;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell124.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell124.StylePriority.UseBackColor = false;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell73.StylePriority.UseBackColor = false;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell158.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell158.StylePriority.UseBackColor = false;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell90.Dpi = 254F;
            this.xrTableCell90.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell90.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell90.StylePriority.UseBackColor = false;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell175.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell175.StylePriority.UseBackColor = false;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell107.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell107.StylePriority.UseBackColor = false;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell141.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell141.StylePriority.UseBackColor = false;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell192.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell192.StylePriority.UseBackColor = false;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell209.Dpi = 254F;
            this.xrTableCell209.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell209.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell209.StylePriority.UseBackColor = false;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell226.Dpi = 254F;
            this.xrTableCell226.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell226.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell226.StylePriority.UseBackColor = false;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell243.Dpi = 254F;
            this.xrTableCell243.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell243.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell243.StylePriority.UseBackColor = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell57,
            this.xrTableCell40,
            this.xrTableCell125,
            this.xrTableCell74,
            this.xrTableCell159,
            this.xrTableCell91,
            this.xrTableCell176,
            this.xrTableCell108,
            this.xrTableCell142,
            this.xrTableCell193,
            this.xrTableCell210,
            this.xrTableCell227,
            this.xrTableCell244});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow5.StylePriority.UseBorders = false;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "CDI%";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Location = new System.Drawing.Point(556, 2);
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell125.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Location = new System.Drawing.Point(677, 2);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell159.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Location = new System.Drawing.Point(931, 2);
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell91.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell176.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Dpi = 254F;
            this.xrTableCell108.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell108.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell142.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell193.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Dpi = 254F;
            this.xrTableCell210.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell210.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Dpi = 254F;
            this.xrTableCell227.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell227.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Dpi = 254F;
            this.xrTableCell244.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell244.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell58,
            this.xrTableCell41,
            this.xrTableCell126,
            this.xrTableCell75,
            this.xrTableCell160,
            this.xrTableCell92,
            this.xrTableCell177,
            this.xrTableCell109,
            this.xrTableCell143,
            this.xrTableCell194,
            this.xrTableCell211,
            this.xrTableCell228,
            this.xrTableCell245});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow6.StylePriority.UseBorders = false;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "2003";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "GPAR%";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell58.StylePriority.UseBackColor = false;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell41.StylePriority.UseBackColor = false;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell126.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell126.StylePriority.UseBackColor = false;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell75.StylePriority.UseBackColor = false;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell160.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell160.StylePriority.UseBackColor = false;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell92.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell92.StylePriority.UseBackColor = false;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell177.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell177.StylePriority.UseBackColor = false;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell109.Dpi = 254F;
            this.xrTableCell109.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell109.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell109.StylePriority.UseBackColor = false;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell143.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell143.StylePriority.UseBackColor = false;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell194.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell194.StylePriority.UseBackColor = false;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell211.Dpi = 254F;
            this.xrTableCell211.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell211.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell211.StylePriority.UseBackColor = false;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell228.Dpi = 254F;
            this.xrTableCell228.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell228.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell228.StylePriority.UseBackColor = false;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell245.Dpi = 254F;
            this.xrTableCell245.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell245.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell245.StylePriority.UseBackColor = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell6,
            this.xrTableCell59,
            this.xrTableCell42,
            this.xrTableCell127,
            this.xrTableCell76,
            this.xrTableCell161,
            this.xrTableCell93,
            this.xrTableCell178,
            this.xrTableCell110,
            this.xrTableCell144,
            this.xrTableCell195,
            this.xrTableCell212,
            this.xrTableCell229,
            this.xrTableCell246});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow7.StylePriority.UseBorders = false;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "CDI%";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell127.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell76.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell161.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell93.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell178.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Dpi = 254F;
            this.xrTableCell110.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell110.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell144.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Dpi = 254F;
            this.xrTableCell195.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell195.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Dpi = 254F;
            this.xrTableCell212.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell212.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Dpi = 254F;
            this.xrTableCell229.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell229.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Dpi = 254F;
            this.xrTableCell246.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell246.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell12,
            this.xrTableCell60,
            this.xrTableCell43,
            this.xrTableCell128,
            this.xrTableCell77,
            this.xrTableCell162,
            this.xrTableCell94,
            this.xrTableCell179,
            this.xrTableCell111,
            this.xrTableCell145,
            this.xrTableCell196,
            this.xrTableCell213,
            this.xrTableCell230,
            this.xrTableCell247});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow8.StylePriority.UseBorders = false;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Location = new System.Drawing.Point(0, 1);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "2004";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Location = new System.Drawing.Point(141, 1);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "GPAR%";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Location = new System.Drawing.Point(300, 2);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell60.StylePriority.UseBackColor = false;
            this.xrTableCell60.StylePriority.UseBorders = false;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Location = new System.Drawing.Point(428, 2);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell43.StylePriority.UseBackColor = false;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell128.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell128.StylePriority.UseBackColor = false;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell77.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell77.StylePriority.UseBackColor = false;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell162.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell162.StylePriority.UseBackColor = false;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell94.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell94.StylePriority.UseBackColor = false;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell179.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell179.StylePriority.UseBackColor = false;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell111.Dpi = 254F;
            this.xrTableCell111.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell111.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell111.StylePriority.UseBackColor = false;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell145.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell145.StylePriority.UseBackColor = false;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell196.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell196.StylePriority.UseBackColor = false;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell213.Dpi = 254F;
            this.xrTableCell213.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell213.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell213.StylePriority.UseBackColor = false;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell230.Dpi = 254F;
            this.xrTableCell230.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell230.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell230.StylePriority.UseBackColor = false;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell247.Dpi = 254F;
            this.xrTableCell247.Location = new System.Drawing.Point(1817, 2);
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell247.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell247.StylePriority.UseBackColor = false;
            this.xrTableCell247.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell18,
            this.xrTableCell61,
            this.xrTableCell44,
            this.xrTableCell129,
            this.xrTableCell78,
            this.xrTableCell163,
            this.xrTableCell95,
            this.xrTableCell180,
            this.xrTableCell112,
            this.xrTableCell146,
            this.xrTableCell197,
            this.xrTableCell214,
            this.xrTableCell231,
            this.xrTableCell248});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow9.StylePriority.UseBorders = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "CDI%";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Location = new System.Drawing.Point(556, 1);
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell129.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Location = new System.Drawing.Point(677, 1);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell78.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell163.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Location = new System.Drawing.Point(931, 1);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell95.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell180.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Dpi = 254F;
            this.xrTableCell112.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell112.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell146.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell197.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Dpi = 254F;
            this.xrTableCell214.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell214.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Dpi = 254F;
            this.xrTableCell231.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell231.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Dpi = 254F;
            this.xrTableCell248.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell248.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell62,
            this.xrTableCell45,
            this.xrTableCell130,
            this.xrTableCell79,
            this.xrTableCell164,
            this.xrTableCell96,
            this.xrTableCell181,
            this.xrTableCell113,
            this.xrTableCell147,
            this.xrTableCell198,
            this.xrTableCell215,
            this.xrTableCell232,
            this.xrTableCell249});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow10.StylePriority.UseBorders = false;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "2005";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "GPAR%";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell62.StylePriority.UseBackColor = false;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell45.StylePriority.UseBackColor = false;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Location = new System.Drawing.Point(556, 1);
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell130.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell130.StylePriority.UseBackColor = false;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Location = new System.Drawing.Point(677, 1);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell79.StylePriority.UseBackColor = false;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell164.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell164.StylePriority.UseBackColor = false;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Location = new System.Drawing.Point(931, 1);
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell96.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell96.StylePriority.UseBackColor = false;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell181.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell181.StylePriority.UseBackColor = false;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell113.Dpi = 254F;
            this.xrTableCell113.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell113.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell113.StylePriority.UseBackColor = false;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell147.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell147.StylePriority.UseBackColor = false;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell198.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell198.StylePriority.UseBackColor = false;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell215.Dpi = 254F;
            this.xrTableCell215.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell215.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell215.StylePriority.UseBackColor = false;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell232.Dpi = 254F;
            this.xrTableCell232.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell232.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell232.StylePriority.UseBackColor = false;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell249.Dpi = 254F;
            this.xrTableCell249.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell249.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell249.StylePriority.UseBackColor = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell63,
            this.xrTableCell46,
            this.xrTableCell131,
            this.xrTableCell80,
            this.xrTableCell165,
            this.xrTableCell97,
            this.xrTableCell182,
            this.xrTableCell114,
            this.xrTableCell148,
            this.xrTableCell199,
            this.xrTableCell216,
            this.xrTableCell233,
            this.xrTableCell250});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow11.StylePriority.UseBorders = false;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "CDI%";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell131.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell80.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell165.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell97.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell182.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Dpi = 254F;
            this.xrTableCell114.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell114.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell148.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell199.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Dpi = 254F;
            this.xrTableCell216.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell216.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Dpi = 254F;
            this.xrTableCell233.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell233.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Dpi = 254F;
            this.xrTableCell250.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell250.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell64,
            this.xrTableCell47,
            this.xrTableCell132,
            this.xrTableCell81,
            this.xrTableCell166,
            this.xrTableCell98,
            this.xrTableCell183,
            this.xrTableCell115,
            this.xrTableCell149,
            this.xrTableCell200,
            this.xrTableCell217,
            this.xrTableCell234,
            this.xrTableCell251});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow12.StylePriority.UseBorders = false;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Location = new System.Drawing.Point(0, 2);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "2006";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "GPAR%";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell64.StylePriority.UseBackColor = false;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell47.StylePriority.UseBackColor = false;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell132.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell132.StylePriority.UseBackColor = false;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell81.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell81.StylePriority.UseBackColor = false;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell166.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell166.StylePriority.UseBackColor = false;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell98.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell98.StylePriority.UseBackColor = false;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell183.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell183.StylePriority.UseBackColor = false;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell115.Dpi = 254F;
            this.xrTableCell115.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell115.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell115.StylePriority.UseBackColor = false;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell149.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell149.StylePriority.UseBackColor = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell200.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell200.StylePriority.UseBackColor = false;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell217.Dpi = 254F;
            this.xrTableCell217.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell217.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell217.StylePriority.UseBackColor = false;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell234.Dpi = 254F;
            this.xrTableCell234.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell234.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell234.StylePriority.UseBackColor = false;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell251.Dpi = 254F;
            this.xrTableCell251.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell251.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell251.StylePriority.UseBackColor = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell65,
            this.xrTableCell48,
            this.xrTableCell133,
            this.xrTableCell82,
            this.xrTableCell167,
            this.xrTableCell99,
            this.xrTableCell184,
            this.xrTableCell116,
            this.xrTableCell150,
            this.xrTableCell201,
            this.xrTableCell218,
            this.xrTableCell235,
            this.xrTableCell252});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow14.StylePriority.UseBorders = false;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Size = new System.Drawing.Size(141, 27);
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "CDI%";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell65.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Location = new System.Drawing.Point(556, 1);
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell133.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Location = new System.Drawing.Point(677, 1);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell82.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell167.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Location = new System.Drawing.Point(931, 1);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell99.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell184.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Dpi = 254F;
            this.xrTableCell116.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell116.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell150.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell201.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Dpi = 254F;
            this.xrTableCell218.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell218.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Dpi = 254F;
            this.xrTableCell235.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell235.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Dpi = 254F;
            this.xrTableCell252.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell252.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell66,
            this.xrTableCell49,
            this.xrTableCell134,
            this.xrTableCell83,
            this.xrTableCell168,
            this.xrTableCell100,
            this.xrTableCell185,
            this.xrTableCell117,
            this.xrTableCell151,
            this.xrTableCell202,
            this.xrTableCell219,
            this.xrTableCell236,
            this.xrTableCell253});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(1947, 27);
            this.xrTableRow15.StylePriority.UseBorders = false;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Location = new System.Drawing.Point(0, 1);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "2007";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.WordWrap = false;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Location = new System.Drawing.Point(141, 1);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "GPAR%";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell66.StylePriority.UseBackColor = false;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell49.StylePriority.UseBackColor = false;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell134.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell134.StylePriority.UseBackColor = false;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell83.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell83.StylePriority.UseBackColor = false;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell168.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell168.StylePriority.UseBackColor = false;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell100.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell100.StylePriority.UseBackColor = false;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell185.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell185.StylePriority.UseBackColor = false;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell117.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell117.StylePriority.UseBackColor = false;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell151.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell151.StylePriority.UseBackColor = false;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell202.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell202.StylePriority.UseBackColor = false;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell219.Dpi = 254F;
            this.xrTableCell219.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell219.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell219.StylePriority.UseBackColor = false;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell236.Dpi = 254F;
            this.xrTableCell236.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell236.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell236.StylePriority.UseBackColor = false;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell253.Dpi = 254F;
            this.xrTableCell253.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell253.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell253.StylePriority.UseBackColor = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell67,
            this.xrTableCell50,
            this.xrTableCell135,
            this.xrTableCell84,
            this.xrTableCell169,
            this.xrTableCell101,
            this.xrTableCell186,
            this.xrTableCell118,
            this.xrTableCell152,
            this.xrTableCell203,
            this.xrTableCell220,
            this.xrTableCell237,
            this.xrTableCell254});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(1947, 27);
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Location = new System.Drawing.Point(0, 1);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Size = new System.Drawing.Size(141, 27);
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Location = new System.Drawing.Point(141, 1);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "CDI%";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Location = new System.Drawing.Point(300, 1);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell67.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Location = new System.Drawing.Point(428, 1);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Location = new System.Drawing.Point(556, 1);
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell135.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Location = new System.Drawing.Point(677, 1);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell84.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Location = new System.Drawing.Point(804, 1);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell169.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Location = new System.Drawing.Point(931, 1);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell101.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Location = new System.Drawing.Point(1037, 1);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell186.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Location = new System.Drawing.Point(1155, 1);
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell118.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Location = new System.Drawing.Point(1283, 1);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell152.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Location = new System.Drawing.Point(1433, 1);
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell203.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Dpi = 254F;
            this.xrTableCell220.Location = new System.Drawing.Point(1561, 1);
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell220.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Dpi = 254F;
            this.xrTableCell237.Location = new System.Drawing.Point(1689, 1);
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell237.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Dpi = 254F;
            this.xrTableCell254.Location = new System.Drawing.Point(1817, 1);
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell254.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell68,
            this.xrTableCell51,
            this.xrTableCell136,
            this.xrTableCell85,
            this.xrTableCell170,
            this.xrTableCell102,
            this.xrTableCell187,
            this.xrTableCell119,
            this.xrTableCell153,
            this.xrTableCell204,
            this.xrTableCell221,
            this.xrTableCell238,
            this.xrTableCell255});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Size = new System.Drawing.Size(1947, 27);
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Size = new System.Drawing.Size(141, 27);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "2008";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "GPAR%";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell68.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell68.StylePriority.UseBackColor = false;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell51.StylePriority.UseBackColor = false;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell136.Size = new System.Drawing.Size(121, 27);
            this.xrTableCell136.StylePriority.UseBackColor = false;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell85.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell85.StylePriority.UseBackColor = false;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell170.Size = new System.Drawing.Size(127, 27);
            this.xrTableCell170.StylePriority.UseBackColor = false;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell102.Size = new System.Drawing.Size(106, 27);
            this.xrTableCell102.StylePriority.UseBackColor = false;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell187.Size = new System.Drawing.Size(118, 27);
            this.xrTableCell187.StylePriority.UseBackColor = false;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell119.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell119.StylePriority.UseBackColor = false;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell153.Size = new System.Drawing.Size(150, 27);
            this.xrTableCell153.StylePriority.UseBackColor = false;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell204.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell204.StylePriority.UseBackColor = false;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell221.Dpi = 254F;
            this.xrTableCell221.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell221.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell221.StylePriority.UseBackColor = false;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell238.Dpi = 254F;
            this.xrTableCell238.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell238.Size = new System.Drawing.Size(128, 27);
            this.xrTableCell238.StylePriority.UseBackColor = false;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.xrTableCell255.Dpi = 254F;
            this.xrTableCell255.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell255.Size = new System.Drawing.Size(130, 27);
            this.xrTableCell255.StylePriority.UseBackColor = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell69,
            this.xrTableCell52,
            this.xrTableCell137,
            this.xrTableCell86,
            this.xrTableCell171,
            this.xrTableCell103,
            this.xrTableCell188,
            this.xrTableCell120,
            this.xrTableCell154,
            this.xrTableCell205,
            this.xrTableCell222,
            this.xrTableCell239,
            this.xrTableCell256});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(1947, 27);
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Size = new System.Drawing.Size(141, 27);
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Location = new System.Drawing.Point(141, 0);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Size = new System.Drawing.Size(159, 27);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "CDI%";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell69.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Location = new System.Drawing.Point(428, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Location = new System.Drawing.Point(556, 0);
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell137.Size = new System.Drawing.Size(121, 27);
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Location = new System.Drawing.Point(677, 0);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell86.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Location = new System.Drawing.Point(804, 0);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell171.Size = new System.Drawing.Size(127, 27);
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Location = new System.Drawing.Point(931, 0);
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell103.Size = new System.Drawing.Size(106, 27);
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Location = new System.Drawing.Point(1037, 0);
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell188.Size = new System.Drawing.Size(118, 27);
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Location = new System.Drawing.Point(1155, 0);
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell120.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Location = new System.Drawing.Point(1283, 0);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell154.Size = new System.Drawing.Size(150, 27);
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Location = new System.Drawing.Point(1433, 0);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell205.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Dpi = 254F;
            this.xrTableCell222.Location = new System.Drawing.Point(1561, 0);
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell222.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Dpi = 254F;
            this.xrTableCell239.Location = new System.Drawing.Point(1689, 0);
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell239.Size = new System.Drawing.Size(128, 27);
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Dpi = 254F;
            this.xrTableCell256.Location = new System.Drawing.Point(1817, 0);
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell256.Size = new System.Drawing.Size(130, 27);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell14.Size = new System.Drawing.Size(470, 61);
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell257,
            this.xrTableCell258});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(1947, 28);
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Dpi = 254F;
            this.xrTableCell257.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell257.Size = new System.Drawing.Size(1615, 28);
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "Retorno";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Dpi = 254F;
            this.xrTableCell258.Location = new System.Drawing.Point(1615, 0);
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell258.Size = new System.Drawing.Size(332, 28);
            // 
            // SubReportRentabilidadeMensal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportRentabilidadeMensal.ResourceManager;
        }
        
        #region Funções Internas do Relatorio
        private void TableDadosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            /*              2     3     4     5     6     7     8     9     10    11   12     13    14
             *0            jan - fev - mar - abr - mai - jun - jul - ago - set - out - nov - dez - ano
             *1 2001  gpar
             *2       cdi
             *3 2002  gpar
             *4       cdi
             *5 2003  gpar
             *6       cid
             *7 2004  gpar
             *8       cdi
             *9 2005  gpar
             *10      cdi
             *11 2006 gpar
             *12      cdi
             *13 2007 gpar  
             *14      cdi
             *15      gpar
             *16      cdi
             *17      retorno 
            */   

            // Hoje - 1 dia considerando Feriado e Final de Semana.
            DateTime dataOntem = Calendario.SubtraiDiaUtil(DateTime.Today, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                       
            RentabilidadeFundo rentGpar = new RentabilidadeFundo(this.idCarteira);
            //
            RentabilidadeIndice rentCDI = new RentabilidadeIndice(ListaIndiceFixo.CDI);
            //                        
            // Linhas
            XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];
            XRTableRow summaryFinalRow2 = summaryFinal.Rows[2];
            XRTableRow summaryFinalRow3 = summaryFinal.Rows[3];
            XRTableRow summaryFinalRow4 = summaryFinal.Rows[4];
            XRTableRow summaryFinalRow5 = summaryFinal.Rows[5];
            XRTableRow summaryFinalRow6 = summaryFinal.Rows[6];
            XRTableRow summaryFinalRow7 = summaryFinal.Rows[7];
            XRTableRow summaryFinalRow8 = summaryFinal.Rows[8];
            XRTableRow summaryFinalRow9 = summaryFinal.Rows[9];
            XRTableRow summaryFinalRow10 = summaryFinal.Rows[10];
            XRTableRow summaryFinalRow11 = summaryFinal.Rows[11];
            XRTableRow summaryFinalRow12 = summaryFinal.Rows[12];
            XRTableRow summaryFinalRow13 = summaryFinal.Rows[13];
            XRTableRow summaryFinalRow14 = summaryFinal.Rows[14];
            XRTableRow summaryFinalRow15 = summaryFinal.Rows[15];
            XRTableRow summaryFinalRow16 = summaryFinal.Rows[16];
            XRTableRow summaryFinalRow17 = summaryFinal.Rows[17];

            /* GPar */
            #region Gpar

            #region 2001
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[2], rentGpar.ListaRentabilidade2001[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[3], rentGpar.ListaRentabilidade2001[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[4], rentGpar.ListaRentabilidade2001[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[5], rentGpar.ListaRentabilidade2001[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[6], rentGpar.ListaRentabilidade2001[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[7], rentGpar.ListaRentabilidade2001[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[8], rentGpar.ListaRentabilidade2001[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[9], rentGpar.ListaRentabilidade2001[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[10], rentGpar.ListaRentabilidade2001[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[11], rentGpar.ListaRentabilidade2001[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[12], rentGpar.ListaRentabilidade2001[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[13], rentGpar.ListaRentabilidade2001[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow1.Cells[14], rentGpar.ListaRentabilidadesAnuais[0]);
            #endregion

            #region 2002
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[2], rentGpar.ListaRentabilidade2002[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[3], rentGpar.ListaRentabilidade2002[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[4], rentGpar.ListaRentabilidade2002[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[5], rentGpar.ListaRentabilidade2002[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[6], rentGpar.ListaRentabilidade2002[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[7], rentGpar.ListaRentabilidade2002[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[8], rentGpar.ListaRentabilidade2002[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[9], rentGpar.ListaRentabilidade2002[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[10], rentGpar.ListaRentabilidade2002[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[11], rentGpar.ListaRentabilidade2002[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[12], rentGpar.ListaRentabilidade2002[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[13], rentGpar.ListaRentabilidade2002[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow3.Cells[14], rentGpar.ListaRentabilidadesAnuais[1]);
            #endregion

            #region 2003
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[2], rentGpar.ListaRentabilidade2003[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[3], rentGpar.ListaRentabilidade2003[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[4], rentGpar.ListaRentabilidade2003[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[5], rentGpar.ListaRentabilidade2003[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[6], rentGpar.ListaRentabilidade2003[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[7], rentGpar.ListaRentabilidade2003[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[8], rentGpar.ListaRentabilidade2003[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[9], rentGpar.ListaRentabilidade2003[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[10], rentGpar.ListaRentabilidade2003[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[11], rentGpar.ListaRentabilidade2003[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[12], rentGpar.ListaRentabilidade2003[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[13], rentGpar.ListaRentabilidade2003[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow5.Cells[14], rentGpar.ListaRentabilidadesAnuais[2]);
            #endregion

            #region 2004
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[2], rentGpar.ListaRentabilidade2004[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[3], rentGpar.ListaRentabilidade2004[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[4], rentGpar.ListaRentabilidade2004[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[5], rentGpar.ListaRentabilidade2004[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[6], rentGpar.ListaRentabilidade2004[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[7], rentGpar.ListaRentabilidade2004[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[8], rentGpar.ListaRentabilidade2004[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[9], rentGpar.ListaRentabilidade2004[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[10], rentGpar.ListaRentabilidade2004[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[11], rentGpar.ListaRentabilidade2004[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[12], rentGpar.ListaRentabilidade2004[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[13], rentGpar.ListaRentabilidade2004[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow7.Cells[14], rentGpar.ListaRentabilidadesAnuais[3]);
            #endregion

            #region 2005
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[2], rentGpar.ListaRentabilidade2005[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[3], rentGpar.ListaRentabilidade2005[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[4], rentGpar.ListaRentabilidade2005[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[5], rentGpar.ListaRentabilidade2005[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[6], rentGpar.ListaRentabilidade2005[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[7], rentGpar.ListaRentabilidade2005[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[8], rentGpar.ListaRentabilidade2005[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[9], rentGpar.ListaRentabilidade2005[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[10], rentGpar.ListaRentabilidade2005[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[11], rentGpar.ListaRentabilidade2005[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[12], rentGpar.ListaRentabilidade2005[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[13], rentGpar.ListaRentabilidade2005[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow9.Cells[14], rentGpar.ListaRentabilidadesAnuais[4]);
            #endregion

            #region 2006
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[2], rentGpar.ListaRentabilidade2006[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[3], rentGpar.ListaRentabilidade2006[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[4], rentGpar.ListaRentabilidade2006[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[5], rentGpar.ListaRentabilidade2006[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[6], rentGpar.ListaRentabilidade2006[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[7], rentGpar.ListaRentabilidade2006[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[8], rentGpar.ListaRentabilidade2006[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[9], rentGpar.ListaRentabilidade2006[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[10], rentGpar.ListaRentabilidade2006[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[11], rentGpar.ListaRentabilidade2006[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[12], rentGpar.ListaRentabilidade2006[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[13], rentGpar.ListaRentabilidade2006[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow11.Cells[14], rentGpar.ListaRentabilidadesAnuais[5]);
            #endregion

            #region 2007
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[2], rentGpar.ListaRentabilidade2007[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[3], rentGpar.ListaRentabilidade2007[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[4], rentGpar.ListaRentabilidade2007[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[5], rentGpar.ListaRentabilidade2007[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[6], rentGpar.ListaRentabilidade2007[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[7], rentGpar.ListaRentabilidade2007[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[8], rentGpar.ListaRentabilidade2007[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[9], rentGpar.ListaRentabilidade2007[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[10], rentGpar.ListaRentabilidade2007[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[11], rentGpar.ListaRentabilidade2007[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[12], rentGpar.ListaRentabilidade2007[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[13], rentGpar.ListaRentabilidade2007[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow13.Cells[14], rentGpar.ListaRentabilidadesAnuais[6]);
            #endregion

            #region 2008
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[2], rentGpar.ListaRentabilidade2008[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[3], rentGpar.ListaRentabilidade2008[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[4], rentGpar.ListaRentabilidade2008[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[5], rentGpar.ListaRentabilidade2008[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[6], rentGpar.ListaRentabilidade2008[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[7], rentGpar.ListaRentabilidade2008[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[8], rentGpar.ListaRentabilidade2008[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[9], rentGpar.ListaRentabilidade2008[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[10], rentGpar.ListaRentabilidade2008[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[11], rentGpar.ListaRentabilidade2008[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[12], rentGpar.ListaRentabilidade2008[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[13], rentGpar.ListaRentabilidade2008[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow15.Cells[14], rentGpar.ListaRentabilidadesAnuais[7]);
            #endregion
            #endregion

            /* CDI */
            #region CDI
            #region 2001
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[2], rentCDI.ListaRentabilidade2001[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[3], rentCDI.ListaRentabilidade2001[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[4], rentCDI.ListaRentabilidade2001[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[5], rentCDI.ListaRentabilidade2001[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[6], rentCDI.ListaRentabilidade2001[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[7], rentCDI.ListaRentabilidade2001[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[8], rentCDI.ListaRentabilidade2001[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[9], rentCDI.ListaRentabilidade2001[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[10], rentCDI.ListaRentabilidade2001[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[11], rentCDI.ListaRentabilidade2001[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[12], rentCDI.ListaRentabilidade2001[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[13], rentCDI.ListaRentabilidade2001[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[14], rentCDI.ListaRentabilidadesAnuais[0]);
            #endregion

            #region 2002
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[2], rentCDI.ListaRentabilidade2002[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[3], rentCDI.ListaRentabilidade2002[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[4], rentCDI.ListaRentabilidade2002[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[5], rentCDI.ListaRentabilidade2002[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[6], rentCDI.ListaRentabilidade2002[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[7], rentCDI.ListaRentabilidade2002[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[8], rentCDI.ListaRentabilidade2002[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[9], rentCDI.ListaRentabilidade2002[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[10], rentCDI.ListaRentabilidade2002[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[11], rentCDI.ListaRentabilidade2002[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[12], rentCDI.ListaRentabilidade2002[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[13], rentCDI.ListaRentabilidade2002[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow4.Cells[14], rentCDI.ListaRentabilidadesAnuais[1]);
            #endregion

            #region 2003
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[2], rentCDI.ListaRentabilidade2003[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[3], rentCDI.ListaRentabilidade2003[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[4], rentCDI.ListaRentabilidade2003[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[5], rentCDI.ListaRentabilidade2003[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[6], rentCDI.ListaRentabilidade2003[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[7], rentCDI.ListaRentabilidade2003[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[8], rentCDI.ListaRentabilidade2003[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[9], rentCDI.ListaRentabilidade2003[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[10], rentCDI.ListaRentabilidade2003[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[11], rentCDI.ListaRentabilidade2003[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[12], rentCDI.ListaRentabilidade2003[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[13], rentCDI.ListaRentabilidade2003[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow6.Cells[14], rentCDI.ListaRentabilidadesAnuais[2]);
            #endregion

            #region 2004
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[2], rentCDI.ListaRentabilidade2004[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[3], rentCDI.ListaRentabilidade2004[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[4], rentCDI.ListaRentabilidade2004[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[5], rentCDI.ListaRentabilidade2004[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[6], rentCDI.ListaRentabilidade2004[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[7], rentCDI.ListaRentabilidade2004[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[8], rentCDI.ListaRentabilidade2004[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[9], rentCDI.ListaRentabilidade2004[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[10], rentCDI.ListaRentabilidade2004[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[11], rentCDI.ListaRentabilidade2004[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[12], rentCDI.ListaRentabilidade2004[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[13], rentCDI.ListaRentabilidade2004[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow8.Cells[14], rentCDI.ListaRentabilidadesAnuais[3]);
            #endregion

            #region 2005
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[2], rentCDI.ListaRentabilidade2005[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[3], rentCDI.ListaRentabilidade2005[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[4], rentCDI.ListaRentabilidade2005[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[5], rentCDI.ListaRentabilidade2005[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[6], rentCDI.ListaRentabilidade2005[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[7], rentCDI.ListaRentabilidade2005[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[8], rentCDI.ListaRentabilidade2005[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[9], rentCDI.ListaRentabilidade2005[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[10], rentCDI.ListaRentabilidade2005[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[11], rentCDI.ListaRentabilidade2005[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[12], rentCDI.ListaRentabilidade2005[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[13], rentCDI.ListaRentabilidade2005[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow10.Cells[14], rentCDI.ListaRentabilidadesAnuais[4]);
            #endregion

            #region 2006
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[2], rentCDI.ListaRentabilidade2006[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[3], rentCDI.ListaRentabilidade2006[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[4], rentCDI.ListaRentabilidade2006[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[5], rentCDI.ListaRentabilidade2006[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[6], rentCDI.ListaRentabilidade2006[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[7], rentCDI.ListaRentabilidade2006[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[8], rentCDI.ListaRentabilidade2006[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[9], rentCDI.ListaRentabilidade2006[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[10], rentCDI.ListaRentabilidade2006[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[11], rentCDI.ListaRentabilidade2006[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[12], rentCDI.ListaRentabilidade2006[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[13], rentCDI.ListaRentabilidade2006[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow12.Cells[14], rentCDI.ListaRentabilidadesAnuais[5]);
            #endregion

            #region 2007
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[2], rentCDI.ListaRentabilidade2007[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[3], rentCDI.ListaRentabilidade2007[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[4], rentCDI.ListaRentabilidade2007[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[5], rentCDI.ListaRentabilidade2007[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[6], rentCDI.ListaRentabilidade2007[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[7], rentCDI.ListaRentabilidade2007[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[8], rentCDI.ListaRentabilidade2007[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[9], rentCDI.ListaRentabilidade2007[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[10], rentCDI.ListaRentabilidade2007[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[11], rentCDI.ListaRentabilidade2007[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[12], rentCDI.ListaRentabilidade2007[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[13], rentCDI.ListaRentabilidade2007[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow14.Cells[14], rentCDI.ListaRentabilidadesAnuais[6]);
            #endregion

            #region 2008
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[2], rentCDI.ListaRentabilidade2008[0]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[3], rentCDI.ListaRentabilidade2008[1]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[4], rentCDI.ListaRentabilidade2008[2]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[5], rentCDI.ListaRentabilidade2008[3]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[6], rentCDI.ListaRentabilidade2008[4]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[7], rentCDI.ListaRentabilidade2008[5]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[8], rentCDI.ListaRentabilidade2008[6]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[9], rentCDI.ListaRentabilidade2008[7]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[10], rentCDI.ListaRentabilidade2008[8]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[11], rentCDI.ListaRentabilidade2008[9]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[12], rentCDI.ListaRentabilidade2008[10]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[13], rentCDI.ListaRentabilidade2008[11]);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow16.Cells[14], rentCDI.ListaRentabilidadesAnuais[7]);
            #endregion

            #endregion

            ((XRTableCell)summaryFinalRow17.Cells[0]).Text = "* Retorno Acumulado no Ano até " + dataOntem.ToString("d");

        }
        #endregion
    }
}
