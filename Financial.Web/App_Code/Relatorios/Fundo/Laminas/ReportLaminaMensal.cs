﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Util;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportLaminaMensal
    /// </summary>
    public class ReportLaminaMensal : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        //private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRSubreport xrSubreport2;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport3;
        private XRSubreport xrSubreport4;
        private SubReportDadosHistoricos subReportDadosHistoricos1;
        private SubReportRentabilidadeAnual subReportRentabilidadeAnual1;
        private SubReportRentabilidadeMensal subReportRentabilidadeMensal1;
        private SubReportGraficoHistoricoRentabilidade subReportGraficoHistoricoRentabilidade1;
        private XRLabel xrLabel1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportLaminaMensal(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        private void PersonalInitialize() {
            // Passa os parametros para os SubReports

            #region Parametros SubReports

            /* SubReportdadosHistoricos */
            this.subReportDadosHistoricos1.PersonalInitialize(this.IdCarteira, this.dataReferencia);

            /* SubReportRentabilidadeAnual */
            this.subReportRentabilidadeAnual1.PersonalInitialize(this.IdCarteira, this.dataReferencia);

            /* SubReportRentabilidadeMensal */
            this.subReportRentabilidadeMensal1.PersonalInitialize(this.IdCarteira, this.dataReferencia);

            /* SubReportGraficoHistoricoRentabilidade */
            this.subReportGraficoHistoricoRentabilidade1.PersonalInitialize(this.IdCarteira, this.dataReferencia);

            #endregion

            #region Pega Campos do resource
            //
            //this.xrTableCell5.Text = Resources.ReportLaminaDia._TituloRelatorio;
            #endregion
        }
       
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportLaminaMensal.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportLaminaMensal.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportLaminaMensal.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRentabilidadeAnual1 = new Financial.Relatorio.SubReportRentabilidadeAnual();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportDadosHistoricos1 = new Financial.Relatorio.SubReportDadosHistoricos();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRentabilidadeMensal1 = new Financial.Relatorio.SubReportRentabilidadeMensal();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportGraficoHistoricoRentabilidade1 = new Financial.Relatorio.SubReportGraficoHistoricoRentabilidade();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRentabilidadeAnual1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDadosHistoricos1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRentabilidadeMensal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoHistoricoRentabilidade1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Height = 24;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Location = new System.Drawing.Point(5, 79);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(635, 42);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(635, 42);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Size = new System.Drawing.Size(212, 42);
            this.xrTableCell1.Text = "#Carteira";
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Location = new System.Drawing.Point(212, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Size = new System.Drawing.Size(423, 42);
            this.xrTableCell2.Text = "DataInicio";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.Location = new System.Drawing.Point(939, 169);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRentabilidadeAnual1;
            this.xrSubreport2.Size = new System.Drawing.Size(677, 106);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrSubreport2,
            this.xrSubreport4,
            this.xrSubreport3,
            this.xrSubreport1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Height = 614;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.Location = new System.Drawing.Point(868, 466);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportDadosHistoricos1;
            this.xrSubreport4.Size = new System.Drawing.Size(762, 106);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.Location = new System.Drawing.Point(0, 318);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportRentabilidadeMensal1;
            this.xrSubreport3.Size = new System.Drawing.Size(847, 106);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.Location = new System.Drawing.Point(0, 169);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportGraficoHistoricoRentabilidade1;
            this.xrSubreport1.Size = new System.Drawing.Size(698, 106);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel1.Location = new System.Drawing.Point(1482, 42);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.Size = new System.Drawing.Size(402, 45);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "DataAtual";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataAtualBeforePrint);
            // 
            // ReportLaminaMensal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(99, 99, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.1";
            this.Watermark.Font = new System.Drawing.Font("Verdana", 54F, System.Drawing.FontStyle.Bold);
            this.Watermark.ForeColor = System.Drawing.Color.White;
            this.Watermark.Image = ((System.Drawing.Image)(resources.GetObject("ReportLaminaMensal.Watermark.Image")));
            this.Watermark.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Watermark.ImageTransparency = 2;
            this.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Zoom;
            this.Watermark.PageRange = "1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRentabilidadeAnual1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportDadosHistoricos1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRentabilidadeMensal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportGraficoHistoricoRentabilidade1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportLaminaMensal.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void DataAtualBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell dataAtualXRTableCell = sender as XRTableCell;           
            ////
            //DateTime data = DateTime.Today;
            //int ano = data.Year;
            //int mes = data.Month;
            ////
            //string mesString = Utilitario.RetornaMesString(mes);

            ////dataAtualXRTableCell.Text = mesString + " de " + ano;
            //dataAtualXRTableCell.Text = DateTime.Today.ToShortDateString();
        }
        #endregion
    }
}