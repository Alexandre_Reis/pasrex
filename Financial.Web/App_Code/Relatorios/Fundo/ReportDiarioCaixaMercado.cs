﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportDiarioCaixaMercado
    /// </summary>
    public class ReportDiarioCaixaMercado : XtraReport {

        private DateTime dataReferencia;
        
        private string login;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell37;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell12;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCellDataInicio;
        private XRPageInfo xrPageInfo2;
        private ReportSemDados reportSemDados1;
        private XRControlStyle xrControlStyle1;
        private XRPageInfo xrPageInfo1;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell23;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportDiarioCaixaMercado(DateTime dataReferencia, string login) {
            this.dataReferencia = dataReferencia;
            this.login = login;
            
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            
            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2460);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                
                this.xrSubreport2.Visible = true;
                //
                this.xrTable5.Visible = false;
                this.xrTable7.Visible = false;                
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.ReportDiarioCaixaMercado._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportDiarioCaixaMercado._DataEmissao;
            this.xrTableCell22.Text = Resources.ReportDiarioCaixaMercado._DataPosicao;
            this.xrTableCell38.Text = Resources.ReportDiarioCaixaMercado._Carteira;
            this.xrTableCell9.Text = Resources.ReportDiarioCaixaMercado._SaldoInicial;
            this.xrTableCell25.Text = Resources.ReportDiarioCaixaMercado._Bovespa;
            this.xrTableCell28.Text = Resources.ReportDiarioCaixaMercado._Proventos;
            this.xrTableCell26.Text = Resources.ReportDiarioCaixaMercado._BMF;
            this.xrTableCell5.Text = Resources.ReportDiarioCaixaMercado._RF_Vcto;
            this.xrTableCell3.Text = Resources.ReportDiarioCaixaMercado._RF_Compra;
            this.xrTableCell35.Text = Resources.ReportDiarioCaixaMercado._RF_Venda;
            this.xrTableCell7.Text = Resources.ReportDiarioCaixaMercado._Despesas;
            this.xrTableCell8.Text = Resources.ReportDiarioCaixaMercado._Ingressos;
            this.xrTableCell11.Text = Resources.ReportDiarioCaixaMercado._Retiradas;
            this.xrTableCell42.Text = Resources.ReportDiarioCaixaMercado._SaldoFinal;            
            #endregion
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();
            string tipoControle = (int)TipoControleCliente.CarteiraRentabil + ", " + (int)TipoControleCliente.Completo;

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine("Select C.IdCarteira, C.Apelido, ");
            sqlText.AppendLine("       C.Nome, ");
            sqlText.AppendLine("       T.Descricao ");
            sqlText.AppendLine("FROM   Carteira C, ");
            sqlText.AppendLine("       Cliente E, ");
            sqlText.AppendLine("       TipoCliente T, ");
            sqlText.AppendLine("       [PermissaoCliente] P, ");
            sqlText.AppendLine("       [Usuario] U ");
            sqlText.AppendLine("WHERE  C.IdCarteira = E.IdCliente ");
            sqlText.AppendLine("       AND E.StatusAtivo = " + (int)StatusAtivoCliente.Ativo);
            sqlText.AppendLine("       AND E.IdTipo = T.IdTipo ");
            sqlText.AppendLine("       AND E.TipoControle In (" + tipoControle + ") ");
            sqlText.AppendLine("       AND E.idCliente = C.IdCarteira ");
            sqlText.AppendLine("       AND P.idCliente = E.idCliente ");
            sqlText.AppendLine("       AND P.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND U.login = '" + this.login + "'");
            sqlText.AppendLine("       ORDER BY E.IdTipo, C.Apelido ");
            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString());

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/ReportDiarioCaixaMercado.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportDiarioCaixaMercado.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportDiarioCaixaMercado.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 48F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.OddStyleName = "xrControlStyle1";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2485F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell10,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell36,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell45});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.CanGrow = false;
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "Carteira";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.18993963782696177;
            this.xrTableCell39.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "SaldoInicial";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.076458752515090544;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoInicialBeforePrint);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "Bovespa";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.068008048289738429;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BovespaBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "Proventos";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.068410462776659964;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ProventosBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "BMF";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.068008048289738429;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BMFBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "RFVcto";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.076458752515090544;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RFVctoBeforePrint);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "RFCompra";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.076861167002012079;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RFCompraBeforePrint);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "RFVenda";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.076861167002012079;
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RFVendaBeforePrint);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "Despesas";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.08490945674044266;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DespesasBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "Ingressos";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.068008048289738429;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IngressosBeforePrint);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "Retiradas";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.076861167002012079;
            this.xrTableCell18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RetiradaBeforePrint);
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Text = "SaldoFinal";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.069215291750503019;
            this.xrTableCell45.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoFinalBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo3,
            this.xrTable1,
            this.xrTable3});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 98F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1354F, 125F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(347F, 11F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(130F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(215F, 10F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(130F, 42F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 56F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(487F, 42F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCellDataInicio});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.CanGrow = false;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Text = "#DataPosicao";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 0.43531827515400412;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.56468172484599588;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrTable10,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 289F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2476F, 87F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(106F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 241F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2485F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell35,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell11,
            this.xrTableCell42});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "#Carteira";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.18993963782696177;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#SaldoInicial";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.076458752515090544;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Bovespa";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell25.Weight = 0.068008048289738429;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#Proventos";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.068410462776659964;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#BMF";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.068008048289738429;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#RF_Vcto";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.076458752515090544;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#RF_Compra";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.076861167002012079;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "#RF_Venda";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.076861167002012079;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#Despesas";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell7.Weight = 0.08490945674044266;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "#Ingressos";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell8.Weight = 0.068008048289738429;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "#Retiradas";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell11.Weight = 0.076861167002012079;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Text = "#SaldoFinal";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell42.Weight = 0.069215291750503019;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1693F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell23.Weight = 0.13762551683402244;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.86237448316597753;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 85F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.ReportFooter.HeightF = 46F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2485F, 46F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell37,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell50,
            this.xrTableCell20,
            this.xrTableCell51,
            this.xrTableCell2,
            this.xrTableCell12,
            this.xrTableCell52});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "Total";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.18993963782696177;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Text = "TotalSaldoInicial";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.076458752515090544;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.Text = "TotalBovespa";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.068008048289738429;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Text = "TotalProvento";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.068410462776659964;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.Text = "TotalBMF";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.068008048289738429;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "TotalRFVcto";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.076458752515090544;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "TotalRFCompra";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.076861167002012079;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "TotalRFVenda";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.076861167002012079;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "TotalDespesas";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.076458752515090544;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "TotalIngressos";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 0.076458752515090544;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "TotalRetiradas";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.076861167002012079;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "TotallSaldoFinal";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.069215291750503019;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(466F, 40F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell4.Weight = 0.0042918454935622317;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell6.Text = "DataReferencia";
            this.xrTableCell6.Weight = 0.99570815450643779;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.LightGray;
            this.xrControlStyle1.BorderColor = System.Drawing.Color.Black;
            this.xrControlStyle1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.Color.Black;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(41.79166F, 219.0625F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport2.Visible = false;
            this.xrSubreport2.ReportSource = this.reportSemDados1;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(767F, 64F);
            this.xrSubreport3.ReportSource = this.subReportRodapeLandScape1;
            // 
            // ReportDiarioCaixaMercado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportDiarioCaixaMercado.ResourceManager;
        }

        #region Variaveis Internas do Relatorio
        // Armazena os valores de cada linha do relatorio para poder calcular o saldoFinal
        protected class ValoresLinha {
            public decimal valorSaldoInicial = 0;
            public decimal valorBovespa = 0;
            public decimal valorProvento = 0;
            public decimal valorBMF = 0;
            public decimal valorRFVcto = 0;
            public decimal valorRFCompra = 0;
            public decimal valorRFVenda = 0;
            public decimal valorDespesas = 0;
            public decimal valorIngresso = 0;
            public decimal valorRetirada = 0;
            //
            public decimal saldoFinal {
                get {
                    return this.valorSaldoInicial + this.valorBovespa + this.valorProvento + 
                           this.valorBMF + this.valorRFVcto + this.valorRFCompra + this.valorRFVenda +
                           this.valorDespesas + this.valorIngresso + this.valorRetirada;
                }
            }
        }

        //
        private ValoresLinha valoresLinha = new ValoresLinha();

        protected class ValoresTotais {
            public decimal totalSaldoInicial = 0;
            public decimal totalBovespa = 0;
            public decimal totalProvento = 0;
            public decimal totalBMF = 0;
            public decimal totalRFVcto = 0;
            public decimal totalRFCompra = 0;
            public decimal totalRFVenda = 0;
            public decimal totalDespesas = 0;
            public decimal totalIngresso = 0;
            public decimal totalRetirada = 0;
            public decimal totalSaldoFinal = 0;
        }

        //
        private ValoresTotais valoresTotais = new ValoresTotais();
        #endregion

        #region Funções Internas do Relatorio
        //
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaXRTableCell = sender as XRTableCell;
            dataReferenciaXRTableCell.Text = this.dataReferencia.ToString("d");
        }
       
        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell carteiraXRTableCell = sender as XRTableCell;
            carteiraXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {               
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                string apelido = (string)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.Apelido);
                //
                string nomeCarteira = idCarteira.ToString() + "-" + apelido;
                //
                if (nomeCarteira.Length > 30) {
                    nomeCarteira = nomeCarteira.Substring(0, 29);
                }
                carteiraXRTableCell.Text = nomeCarteira;
            }
        }

        private void SaldoInicialBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoInicialXRTableCell = sender as XRTableCell;
            saldoInicialXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                //                
                saldoCaixa.BuscaSaldoCaixa(idCarteira, this.dataReferencia);
                // Salva SaldoCaixa
                this.valoresLinha.valorSaldoInicial = saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0;                
                ReportBase.ConfiguraSinalNegativo(saldoInicialXRTableCell, this.valoresLinha.valorSaldoInicial);             
   
                // Computa o Sum da coluna
                this.valoresTotais.totalSaldoInicial += this.valoresLinha.valorSaldoInicial;
            }
        }

        private void BovespaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell bovespaXRTableCell = sender as XRTableCell;
            bovespaXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                #region calcula Provento
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origemProvento = new List<int>();
                origemProvento.Add(OrigemLancamentoLiquidacao.Bolsa.JurosCapital);
                origemProvento.Add(OrigemLancamentoLiquidacao.Bolsa.Dividendo);
                origemProvento.Add(OrigemLancamentoLiquidacao.Bolsa.Rendimento);

                decimal valorProvento = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origemProvento, SinalValorLiquidacao.Entrada_Retirada);
                #endregion

                #region calcula Bovespa
                liquidacao = new Liquidacao();
                //                
                List<int> origem = OrigemLancamentoLiquidacao.Bolsa.Values();
                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                
                // Subtrai o valor Bovespa
                valor = valor - valorProvento;

                // Salva o valor Bovespa da Linha
                this.valoresLinha.valorBovespa = valor;
                #endregion

                ReportBase.ConfiguraSinalNegativo(bovespaXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalBovespa += this.valoresLinha.valorBovespa;
            }
        }

        private void ProventosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell proventosXRTableCell = sender as XRTableCell;
            proventosXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = new List<int>();
                origem.Add(OrigemLancamentoLiquidacao.Bolsa.JurosCapital);
                origem.Add(OrigemLancamentoLiquidacao.Bolsa.Dividendo);
                origem.Add(OrigemLancamentoLiquidacao.Bolsa.Rendimento);
                origem.Add(OrigemLancamentoLiquidacao.Bolsa.OutrosProventos);

                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                // Salva o valor Provento da Linha
                this.valoresLinha.valorProvento = valor;
                //
                ReportBase.ConfiguraSinalNegativo(proventosXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalProvento += this.valoresLinha.valorProvento;
            }
        }

        private void BMFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell bmfXRTableCell = sender as XRTableCell;
            bmfXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = OrigemLancamentoLiquidacao.BMF.Values();
                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                //

                // Salva o valor BMF da Linha
                this.valoresLinha.valorBMF = valor;

                ReportBase.ConfiguraSinalNegativo(bmfXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalBMF += this.valoresLinha.valorBMF;
            }
        }

        private void RFVctoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell RFVctoXRTableCell = sender as XRTableCell;
            RFVctoXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = new List<int>();
                origem.Add(OrigemLancamentoLiquidacao.RendaFixa.Vencimento);
                origem.Add(OrigemLancamentoLiquidacao.Swap.LiquidacaoAntecipacao);
                origem.Add(OrigemLancamentoLiquidacao.Swap.LiquidacaoVencimento);
                origem.Add(OrigemLancamentoLiquidacao.RendaFixa.Juros);
                origem.Add(OrigemLancamentoLiquidacao.RendaFixa.Amortizacao);
                
                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                //
                // Salva o valor RF da linha
                this.valoresLinha.valorRFVcto = valor;
                //
                ReportBase.ConfiguraSinalNegativo(RFVctoXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalRFVcto += this.valoresLinha.valorRFVcto;
            }
        }

        private void RFCompraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell RFCompraXRTableCell = sender as XRTableCell;
            RFCompraXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = new List<int>();
                origem.Add(OrigemLancamentoLiquidacao.RendaFixa.CompraFinal);

                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                //
                // Salva o valor RF da Linha
                this.valoresLinha.valorRFCompra = valor;

                ReportBase.ConfiguraSinalNegativo(RFCompraXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalRFCompra += this.valoresLinha.valorRFCompra;
            }
        }

        private void RFVendaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell RFVendaXRTableCell = sender as XRTableCell;
            RFVendaXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = new List<int>();
                origem.Add(OrigemLancamentoLiquidacao.RendaFixa.VendaFinal);

                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                //

                // Salva o valor RF da Linha
                this.valoresLinha.valorRFVenda = valor;

                ReportBase.ConfiguraSinalNegativo(RFVendaXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalRFVenda += this.valoresLinha.valorRFVenda;
            }
        }

        private void DespesasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell despesasXRTableCell = sender as XRTableCell;
            despesasXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                
                List<int> origem = OrigemLancamentoLiquidacao.Provisao.Values();
                List<int> origemAux = OrigemLancamentoLiquidacao.IR.Values();
                /*for (int i = 0; i < origemAux.Count; i++) {
                    origem.Add(origemAux[i]);                    
                }*/
                origem.AddRange(origemAux);
                origem.Add(OrigemLancamentoLiquidacao.Swap.DespesasTaxas);

                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada_Retirada);
                //
                // Salva o valor Despesa da Linha
                this.valoresLinha.valorDespesas = valor;
                    
                ReportBase.ConfiguraSinalNegativo(despesasXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalDespesas += this.valoresLinha.valorDespesas;
            }
        }

        private void IngressosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell ingressosXRTableCell = sender as XRTableCell;
            ingressosXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                                
                List<int> origem = OrigemLancamentoLiquidacao.Fundo.Values();
                origem.Add(OrigemLancamentoLiquidacao.Outros);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.Resgate);

                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Entrada);
                
                // Salva o valor Ingresso da Linha
                this.valoresLinha.valorIngresso = valor;
                //
                ReportBase.ConfiguraSinalNegativo(ingressosXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalIngresso += this.valoresLinha.valorIngresso;
            }
        }

        private void RetiradaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell retiradasXRTableCell = sender as XRTableCell;
            retiradasXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                //
                Liquidacao liquidacao = new Liquidacao();
                //                                
                List<int> origem = OrigemLancamentoLiquidacao.Fundo.Values();
                origem.Add(OrigemLancamentoLiquidacao.Outros);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.Aplicacao);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.ComeCotas);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.IOFResgate);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.IRResgate);
                origem.Add(OrigemLancamentoLiquidacao.Cotista.PfeeResgate);
                
                decimal valor = liquidacao.RetornaValor(idCarteira, this.dataReferencia, origem, SinalValorLiquidacao.Retirada);
                //
                // Salva o valor Retirada da Linha
                this.valoresLinha.valorRetirada = valor;

                ReportBase.ConfiguraSinalNegativo(retiradasXRTableCell, valor);

                // Computa o Sum da coluna
                this.valoresTotais.totalRetirada += this.valoresLinha.valorRetirada;
            }
        }

        private void SaldoFinalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoFinalXRTableCell = sender as XRTableCell;
            saldoFinalXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                decimal saldoFinal = this.valoresLinha.saldoFinal;
                ReportBase.ConfiguraSinalNegativo(saldoFinalXRTableCell, saldoFinal);

                // Computa o Sum da coluna
                this.valoresTotais.totalSaldoFinal += saldoFinal;
            }
        }

        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell clienteXRTableCell = sender as XRTableCell;
            clienteXRTableCell.Text = "";
            if (this.numeroLinhasDataTable != 0) {
                int idCliente = (int)this.GetCurrentColumnValue(OperacaoFundoMetadata.ColumnNames.IdCliente);
                //
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Nome);
                //                
                cliente.LoadByPrimaryKey(campos, idCliente);
                string nome = cliente.Nome;
                string nomeCliente = idCliente.ToString() + " - " + nome;
                //
                clienteXRTableCell.Text = nomeCliente;
            }
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            
            #region Limpa os Dados da Tabela
            // 
            for (int i = 0; i < summaryFinal.Rows.Count; i++) {
                int colunas = ((XRTableRow) summaryFinal.Rows[i]).Cells.Count;
                for (int j = 0; j < colunas; j++) {
                    ((XRTableCell)summaryFinal.Rows[i].Cells[j]).Text = "";
                }                
            }
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha
                /* 0-Totais:
                 * 1-TotalSaldoInicial
                 * 2-TotalBovespa
                 * 3-TotalProventos
                 * 4-TotalBMF
                 * 5-TotalRFVcto
                 * 6-TotalRFCompra               
                 * 7-TotalRFVenda
                 * 8-TotalDespesas
                 * 9-TotalIngressos
                 * 10-TotalRetiradas
                 * 11-SaldoFinal
                 */                
                string total = Resources.ReportDiarioCaixaMercado._Total;

                #region Linha 0
                XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = total;                
                //
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[1], this.valoresTotais.totalSaldoInicial);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[2], this.valoresTotais.totalBovespa);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[3], this.valoresTotais.totalProvento);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[4], this.valoresTotais.totalBMF);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[5], this.valoresTotais.totalRFVcto);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[6], this.valoresTotais.totalRFCompra);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[7], this.valoresTotais.totalRFVenda);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[8], this.valoresTotais.totalDespesas);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[9], this.valoresTotais.totalIngresso);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[10], this.valoresTotais.totalRetirada);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[11], this.valoresTotais.totalSaldoFinal);
                //                                              
                #endregion
            }
        }

        #endregion
    }
}