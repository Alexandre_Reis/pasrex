﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Investidor;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportAlocacaoDistribuicaoFundos
    /// </summary>
    public class ReportAlocacaoDistribuicaoFundos : XtraReport {

        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        /// <summary>
        /// Dicionario onde a chave é o nome do Fundo X e o Valor é a Alocação Média do Fundo X
        /// </summary>
        private Dictionary<String, Decimal> dadosAlocacaoMedia = new Dictionary<String, Decimal>();

        /// <summary>
        /// Dicionario onde a chave é o nome do Fundo X e o Valor é a Contribuição Absoluta do Fundo X
        /// </summary>
        private Dictionary<string, decimal> dadosContribuicaoAbsoluta = new Dictionary<string, decimal>();

        /// <summary>
        /// Lista com os fundos de um Determinado Cliente
        /// </summary>
        private List<int> listaFundosCliente = new List<int>();

        /// <summary>
        /// Lista com os Nomes dos Fundos de um Determinado Cliente
        /// </summary>
        private List<string> listaNomeFundosCliente = new List<string>();

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRLabel xrLabel1;
        private XRChart xrChart1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell3;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell4;
        private XRChart xrChart2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportAlocacaoDistribuicaoFundos(int idcliente, DateTime dataInicio, DateTime dataFim) {
            this.idCliente = idcliente;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            #region Carrega a Lista de Fundos do Cliente
            PosicaoFundoHistoricoQuery p = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery c = new CarteiraQuery("C");
            p.es.Distinct = true;

            p.Select(p.IdCarteira, c.Nome);
            p.InnerJoin(c).On(p.IdCarteira == c.IdCarteira);
            p.Where(p.IdCliente == this.idCliente);
            p.OrderBy(c.Nome.Ascending);

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Load(p);
            //
            foreach (PosicaoFundoHistorico pos in posicaoFundoHistoricoCollection) {
                this.listaFundosCliente.Add(pos.IdCarteira.Value);
                this.listaNomeFundosCliente.Add( (string)pos.GetColumn(CarteiraMetadata.ColumnNames.Nome) );
            }
            #endregion
            //                
            this.InitializeComponent();
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Carrega num Dicionario uma lista de Fundos com suas Respectivas alocações Médias
        /// </summary>
        private void CarregaDadosAlocacaoMedia() {
            this.dadosAlocacaoMedia.Clear();

            //this.dadosAlocacaoMedia.Add("FUNDO A", 82.3M);
            //this.dadosAlocacaoMedia.Add("FUNDO B", 84.5M);
            //this.dadosAlocacaoMedia.Add("FUNDO C", 94.5M);
            //this.dadosAlocacaoMedia.Add("FUNDO D", 94.5M);
            //this.dadosAlocacaoMedia.Add("FUNDO E", 96.5M);
            //this.dadosAlocacaoMedia.Add("FUNDO F", 98.5M);
            //this.dadosAlocacaoMedia.Add("FUNDO G", 100M);

            // Carrega o Dicionario
            CalculoMedida c = new CalculoMedida();
            for (int i = 0; i < this.listaFundosCliente.Count; i++) {
                c.SetIdCarteira(listaFundosCliente[i]);
                decimal alocacaoMedia = c.CalculaAlocacaoMedia(this.idCliente, this.dataInicio, this.dataFim);
                alocacaoMedia = Math.Round(alocacaoMedia, 2);
                //
                if (alocacaoMedia != 0.0M) {
                    this.dadosAlocacaoMedia.Add(this.listaNomeFundosCliente[i], (alocacaoMedia / 100));
                }
            }                       
        }
        
        /// <summary>
        /// Carrega uma lista de Fundos com suas Respectivas Contribuicões Absolutas
        /// </summary>
        private void CarregaDadosContribuicaoAbsoluta() {
            this.dadosContribuicaoAbsoluta.Clear();

            // Carrega o Dicionario
            CalculoMedida c = new CalculoMedida();
            for (int i = 0; i < this.listaFundosCliente.Count; i++) {            
                c.SetIdCarteira(listaFundosCliente[i]);
                decimal contribuicaoAbsoluta = c.CalculaContribuicao(this.idCliente, this.dataInicio, this.dataFim);
                // Adiciona no Dicionario

                decimal valor = Math.Abs(contribuicaoAbsoluta);
                valor = Math.Round(valor, 2);

                if (valor >= 0.01M) {
                    this.dadosContribuicaoAbsoluta.Add(this.listaNomeFundosCliente[i], contribuicaoAbsoluta/100);
                }
            }
        }

        public void PersonalInitialize() {
            ReportBase relatorioBase = new ReportBase(this);
            this.CarregaDadosAlocacaoMedia();
            this.CarregaDadosContribuicaoAbsoluta();
            this.FillDadosGrafico();
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados dos Graficos de Alocação Media e Contribuição Absoluta
        /// </summary>
        private void FillDadosGrafico() {            
            #region AlocacaoMedia
            DataTable tableDadosAlocacaoMedia = new DataTable();

            // Add three columns to the table.
            tableDadosAlocacaoMedia.Columns.Add("NomeFundo", typeof(String));
            tableDadosAlocacaoMedia.Columns.Add("AlocacaoMedia", typeof(String));
            tableDadosAlocacaoMedia.Columns.Add("Valores", typeof(Decimal));

            foreach (KeyValuePair<string, decimal> pair in this.dadosAlocacaoMedia) {
                tableDadosAlocacaoMedia.Rows.Add(new object[] { pair.Key, pair.Key, pair.Value });
            }

            // Define o Tamanho do Chart
            //int totalFundos = tableDadosAlocacaoMedia.Rows.Count;
            //int tamanho = (totalFundos * 500) + 50;
            //if (tamanho > 1950) {
            //    // Define Tamanho Maximo
            //    //tamanho = 1950;
            //}

            //this.xrChart1.Size = new System.Drawing.Size(tamanho, this.xrChart1.Height);

            //Generate a data table and bind the chart to it.
            this.xrChart1.DataSource = tableDadosAlocacaoMedia;

            this.xrChart1.SeriesDataMember = "NomeFundo";
            this.xrChart1.SeriesTemplate.ArgumentDataMember = "AlocacaoMedia";
            this.xrChart1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Valores" });
            this.xrChart1.SeriesTemplate.Label.Visible = false;
            //this.xrChart1.SeriesTemplate.View.Color = Color.Blue;

            #endregion

            #region ContribuicaoAbsoluta
            DataTable tableDadosContribuicaoAbsoluta = new DataTable();

            // Add three columns to the table.
            tableDadosContribuicaoAbsoluta.Columns.Add("NomeFundo", typeof(String));
            tableDadosContribuicaoAbsoluta.Columns.Add("ContribuicaoAbsoluta", typeof(String));
            tableDadosContribuicaoAbsoluta.Columns.Add("Valores", typeof(Decimal));

            // Add data rows to the table.
            foreach (KeyValuePair<string, decimal> pair in this.dadosContribuicaoAbsoluta) {
                tableDadosContribuicaoAbsoluta.Rows.Add(new object[] { pair.Key, pair.Key, pair.Value });
            }

            //Generate a data table and bind the chart to it.
            this.xrChart2.DataSource = tableDadosContribuicaoAbsoluta;

            this.xrChart2.SeriesDataMember = "NomeFundo";
            this.xrChart2.SeriesTemplate.ArgumentDataMember = "ContribuicaoAbsoluta";
            this.xrChart2.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Valores" });
            this.xrChart2.SeriesTemplate.Label.Visible = false;  
            //
            #endregion          
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportAlocacaoDistribuicaoFundos.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportAlocacaoDistribuicaoFundos.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions2 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel4 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.ChartTitle chartTitle2 = new DevExpress.XtraCharts.ChartTitle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrChart2 = new DevExpress.XtraReports.UI.XRChart();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart2,
            this.xrTable1,
            this.xrChart1,
            this.xrLabel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 1953F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrChart2
            // 
            this.xrChart2.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrChart2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram1.AxisX.Label.Angle = 40;
            xyDiagram1.AxisX.Label.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "Alocação Média";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.PaneDistance = 25;
            this.xrChart2.Diagram = xyDiagram1;
            this.xrChart2.Dpi = 254F;
            this.xrChart2.LocationFloat = new DevExpress.Utils.PointFloat(60F, 1035F);
            this.xrChart2.Name = "xrChart2";
            this.xrChart2.PaletteBaseColorNumber = 2;
            this.xrChart2.PaletteName = "Module";
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            sideBySideBarSeriesLabel1.Shadow.Color = System.Drawing.Color.Transparent;
            sideBySideBarSeriesLabel1.Visible = false;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.LegendPointOptions = pointOptions1;
            series1.LegendText = "Alocação Média";
            series1.Name = "Alocação Média";
            series1.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series1.SeriesPointsSortingKey = DevExpress.XtraCharts.SeriesPointKey.Value_1;
            series1.ShowInLegend = false;
            series1.SynchronizePointOptions = false;
            this.xrChart2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            sideBySideBarSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart2.SeriesTemplate.Label = sideBySideBarSeriesLabel2;
            this.xrChart2.SideBySideEqualBarWidth = true;
            this.xrChart2.SizeF = new System.Drawing.SizeF(2476F, 826F);
            chartTitle1.Alignment = System.Drawing.StringAlignment.Near;
            chartTitle1.Font = new System.Drawing.Font("Tahoma", 14F);
            chartTitle1.Text = "Contribuição Absoluta";
            this.xrChart2.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(65F, 90F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1609F, 80F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.5;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Cliente:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.078931013051584842;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Cliente";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.92106898694841521;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell4});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.5;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Data:";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.078931013051584842;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Periodo";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.92106898694841521;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PeriodoBeforePrint);
            // 
            // xrChart1
            // 
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram2.AxisX.Label.Angle = 40;
            xyDiagram2.AxisX.Label.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram2.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            xyDiagram2.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram2.AxisY.Title.Text = "Alocação Média";
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.PaneDistance = 25;
            this.xrChart1.Diagram = xyDiagram2;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(64F, 190F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteBaseColorNumber = 2;
            this.xrChart1.PaletteName = "Module";
            sideBySideBarSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            sideBySideBarSeriesLabel3.Shadow.Color = System.Drawing.Color.Transparent;
            sideBySideBarSeriesLabel3.Visible = false;
            series2.Label = sideBySideBarSeriesLabel3;
            series2.LegendPointOptions = pointOptions2;
            series2.LegendText = "Alocação Média";
            series2.Name = "Alocação Média";
            series2.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            series2.SeriesPointsSortingKey = DevExpress.XtraCharts.SeriesPointKey.Value_1;
            series2.ShowInLegend = false;
            series2.SynchronizePointOptions = false;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            sideBySideBarSeriesLabel4.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel4;
            this.xrChart1.SideBySideEqualBarWidth = true;
            this.xrChart1.SizeF = new System.Drawing.SizeF(2476F, 826F);
            chartTitle2.Alignment = System.Drawing.StringAlignment.Near;
            chartTitle2.Font = new System.Drawing.Font("Tahoma", 14F);
            chartTitle2.Text = "Alocação Média";
            this.xrChart1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle2});
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(66F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(950F, 64F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Alocação x Distribuição em Fundos";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportAlocacaoDistribuicaoFundos
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportAlocacaoDistribuicaoFundos.ResourceManager;
        }
        
        #region Funções Internas do Relatorio
        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell clienteXRTableCell = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            if (cliente.LoadByPrimaryKey(campos, this.idCliente)) {
                clienteXRTableCell.Text = this.idCliente + " - " + cliente.Nome;
            }            
        }

        private void PeriodoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataXRTableCell = sender as XRTableCell;
            dataXRTableCell.Text = this.dataInicio.ToString("d") + " à " + this.dataFim.ToString("d");
        }        
        #endregion
    }
}