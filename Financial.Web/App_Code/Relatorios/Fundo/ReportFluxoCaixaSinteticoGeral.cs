﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using System.Text;
using log4net;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportFluxoCaixaSinteticoGeral
    /// </summary>
    public class ReportFluxoCaixaSinteticoGeral : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportFluxoCaixaSinteticoGeral));

        private int? idCliente;

        public int? IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
       
        private int? idTipo;
        private DateTime dataReferencia;

        private Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa;

        /* Determina se as Datas de ListData são datas após a data Atual da Carteira */
        private List<bool> dataAposDataReferencia = new List<bool>(7);
        
        // Lista de Dias Úteis a Partir da data de Referência
        private List<DateTime> ListData = new List<DateTime>(7);

        // Data Atual da Carteira do Cliente
        private DateTime dataDiaCliente = new DateTime();

        private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell34;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRControlStyle xrControlStyle1;
        private ClienteCollection clienteCollection1;
        private XRTableCell xrTableCell23;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell30;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private ReportSemDados reportSemDados1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTable xrTable5;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell36;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell7;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell66;
        private XRControlStyle xrControlStyle2;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportFluxoCaixaSinteticoGeral(int? idCliente, DateTime dataReferencia, int? idTipo, Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            this.idCliente = idCliente;
            this.dataReferencia = dataReferencia;
            this.idTipo = idTipo;
            this.booleanosFluxoCaixa = booleanosFluxoCaixa;
            
            // Lista de Dias Uteis
            this.ListData.Add(this.dataReferencia);
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 1));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 2));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 3));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 4));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 5));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 6));
            //
            
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2450);
        }
       
        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable5.Visible = false;
                this.xrTable7.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do Resource
            this.xrTableCell55.Text = Resources.ReportFluxoCaixaSinteticoGeral._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportFluxoCaixaSinteticoGeral._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportFluxoCaixaSinteticoGeral._DataReferencia;
            #endregion
        }

        private DataTable FillDados() 
        {
            #region SQL
            ClienteQuery c = new ClienteQuery("C");
            
            c.Select(c.IdCliente);
            //
            c.Where(c.DataImplantacao <= this.dataReferencia,
                    c.DataDia >= this.dataReferencia,
                    c.StatusAtivo == (byte)StatusAtivoCliente.Ativo,
                    c.TipoControle.In((byte)TipoControleCliente.Carteira,
                                      (byte)TipoControleCliente.CarteiraRentabil,
                                      (byte)TipoControleCliente.Completo));

            if (this.idCliente.HasValue) 
            {
                c.Where(c.IdCliente == this.idCliente);
            }

            if (this.idTipo.HasValue)
            {
                c.Where(c.IdTipo == this.idTipo.Value);
            }
            #endregion

            this.clienteCollection1.Load(c);
            //
            return this.clienteCollection1.Query.LoadDataTable();            
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportFluxoCaixaSinteticoGeral.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportFluxoCaixaSinteticoGeral.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.clienteCollection1 = new ClienteCollection();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Detail.HeightF = 184F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.EvenStyleName = "xrControlStyle2";
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow3,
            this.xrTableRow9,
            this.xrTableRow11});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2450F, 184F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.CanGrow = false;
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "Carteira";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell36.Weight = 0.99999999999999989;
            this.xrTableCell36.WordWrap = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell37,
            this.xrTableCell6,
            this.xrTableCell51,
            this.xrTableCell15,
            this.xrTableCell56,
            this.xrTableCell47,
            this.xrTableCell60});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "#SaldoInicial";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell24.Weight = 0.28422619878029337;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "Valor";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.0865306371572066;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Valor1";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.088571403659119863;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "Valor2";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.0865305873325893;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Valor3";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.087346988600127545;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "Valor4";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.086530562420280607;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Valor5";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.086122498804209177;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "Valor6";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.19414112324617344;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell7,
            this.xrTableCell65,
            this.xrTableCell52,
            this.xrTableCell18,
            this.xrTableCell57,
            this.xrTableCell48,
            this.xrTableCell61});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.StylePriority.UsePadding = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "#LiquidacaoData";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell63.Weight = 0.28422619878029337;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Valor";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.0865306371572066;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "Valor1";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell65.Weight = 0.088571403659119863;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "Valor2";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.0865305873325893;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Valor3";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.087346988600127545;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "Valor4";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.086530562420280607;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.Text = "Valor5";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.086122498804209177;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Valor6";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.19414112324617344;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64,
            this.xrTableCell8,
            this.xrTableCell66,
            this.xrTableCell53,
            this.xrTableCell20,
            this.xrTableCell58,
            this.xrTableCell49,
            this.xrTableCell62});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.StylePriority.UsePadding = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "#SaldoFinal";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell64.Weight = 0.28422619878029337;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Valor";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.0865306371572066;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "Valor1";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell66.Weight = 0.088571403659119863;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "Valor2";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.0865305873325893;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Valor3";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.087346988600127545;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Valor4";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.086530562420280607;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "Valor5";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.086122498804209177;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "Valor6";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell62.Weight = 0.19414112324617344;
            // 
            // clienteCollection1
            // 
            this.clienteCollection1.AllowDelete = true;
            this.clienteCollection1.AllowEdit = true;
            this.clienteCollection1.AllowNew = true;
            this.clienteCollection1.EnableHierarchicalBinding = true;
            this.clienteCollection1.Filter = "";
            this.clienteCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.clienteCollection1.Sort = "";
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 87.00002F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1139.521F, 101.625F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(362F, 5.999957F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 5.999957F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 48F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(529F, 43F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataReferencia";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.40075614366729678;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataReferencia";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.59924385633270316;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.999957F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1651F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell30.Weight = 0.14112658994548757;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.85887341005451245;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(98.35418F, 201.3958F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableHeaderBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell2,
            this.xrTableCell23});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.5;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Descricao";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.28489795918367344;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "Data";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.086530612244897956;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "Data1";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.088571428571428565;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "Data2";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.086530612244897956;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "Data3";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.087346938775510211;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "Data4";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.086530612244897956;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "Data5";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.086122448979591842;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "#Acima";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell23.Weight = 0.19346938775510203;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageFooter.HeightF = 71.00003F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(365F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrPanel1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeader.HeightF = 249.4166F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(61.35418F, 203.3959F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2455F, 93F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(96F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Transparent;
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.LightGray;
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // ReportFluxoCaixaSinteticoGeral
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.clienteCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 90, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = true;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportFluxoCaixaSinteticoGeral.ResourceManager;

        }

        #region Variaveis Internas do Relatorio
        protected class ValoresSaldo {
            public decimal valorSaldo0 = 0;
            public decimal valorSaldo1 = 0;
            public decimal valorSaldo2 = 0;
            public decimal valorSaldo3 = 0;
            public decimal valorSaldo4 = 0;
            public decimal valorSaldo5 = 0;
            public decimal valorSaldo6 = 0;
            public decimal totalSaldoFinal = 0;
        }
        //
        private ValoresSaldo valoresSaldoInicial = new ValoresSaldo();
        private ValoresSaldo valoresSaldoFinal = new ValoresSaldo();

        #endregion

        //
        #region Funções Internas do Relatorio
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaXRTableCell = sender as XRTableCell;
            dataReferenciaXRTableCell.Text = this.dataReferencia.ToString("d");
        }
              
        private void TableHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable header = sender as XRTable;
            
            // Limpa Dados da Table
            ReportBase.LimpaDadosTable(header);

            #region Linha 0
            XRTableRow header0 = header.Rows[0];
            ((XRTableCell)header0.Cells[0]).Text = Resources.ReportFluxoCaixaSinteticoGeral._Descricao;
            ((XRTableCell)header0.Cells[1]).Text = this.ListData[0].ToString("d");
            ((XRTableCell)header0.Cells[2]).Text = this.ListData[1].ToString("d");
            ((XRTableCell)header0.Cells[3]).Text = this.ListData[2].ToString("d");
            ((XRTableCell)header0.Cells[4]).Text = this.ListData[3].ToString("d");
            ((XRTableCell)header0.Cells[5]).Text = this.ListData[4].ToString("d");
            ((XRTableCell)header0.Cells[6]).Text = this.ListData[5].ToString("d");
            ((XRTableCell)header0.Cells[7]).Text = Resources.ReportFluxoCaixaSinteticoGeral._Acima;
            #endregion
        }

        private void TableDetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable detail = sender as XRTable;

            // Limpa Dados da Table
            ReportBase.LimpaDadosTable(detail);

            if (this.numeroLinhasDataTable != 0)
            {
                int idCliente = (int)this.GetCurrentColumnValue(LiquidacaoMetadata.ColumnNames.IdCliente);
                Cliente cliente = new Cliente();
                cliente.Query.Select(cliente.Query.IdCliente, cliente.Query.DataDia);
                cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
                cliente.Query.Load();
                //

                #region Linha 0

                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Nome);
                //                
                carteira.LoadByPrimaryKey(campos, idCliente);
                string nome = carteira.Nome;
                string nomeCarteira = idCliente.ToString() + " - " + nome;
                //
                XRTableRow detail0 = detail.Rows[0];
                ((XRTableCell)detail0.Cells[0]).Text = nomeCarteira;
                #endregion

                #region Linha 1
                //
                this.dataAposDataReferencia.Clear();
                
                // Determina Se as Datas de ListData são datas após a data de referência passada.
                for (int i = 0; i < this.ListData.Count; i++) {
                    this.dataAposDataReferencia.Add(this.ListData[i] > this.dataReferencia);
                }

                List<decimal> saldoInicial = new List<decimal>();

                #region Calcula Saldo

                #region Saldo0
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[0]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo1
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[1]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo2
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[2]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo3
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[3]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo4
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[4]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo5
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[5]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo6
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, this.ListData[6]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion
                #endregion

                // Se a data de Previsão for depois da Data Atual da Carteira Acrescenta 
                // as Liquidações naquela data.
                for (int i = 0; i < this.dataAposDataReferencia.Count; i++) {
                    if (this.dataAposDataReferencia[i]) {

                        // Busca o Saldo Final da Data Atual do Cliente
                        SaldoCaixa saldoCaixaAux = new SaldoCaixa();
                        saldoCaixaAux.BuscaSaldoCaixa(idCliente, this.dataReferencia);
                        //
                        decimal saldoFinal = saldoCaixaAux.SaldoFechamento.HasValue ? saldoCaixaAux.SaldoFechamento.Value : 0;

                        // Primeiro dia Após dataCliente
                        if (i == 1) {
                            saldoInicial[i] += saldoFinal;
                        }

                        // Do Segundo dia Após DataCliente até o 6 dia após - Saldo Anterior + liquidações no dia anterior
                        #region Calcula Liquidacao
                        decimal valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, this.ListData[i - 1], this.booleanosFluxoCaixa);
                        //
                        saldoInicial[i] = saldoInicial[i - 1] + valor;
                        #endregion
                    }
                }

                #region Armazena Saldos Iniciais
                this.valoresSaldoInicial.valorSaldo0 = saldoInicial[0];
                this.valoresSaldoInicial.valorSaldo1 = saldoInicial[1];
                this.valoresSaldoInicial.valorSaldo2 = saldoInicial[2];
                this.valoresSaldoInicial.valorSaldo3 = saldoInicial[3];
                this.valoresSaldoInicial.valorSaldo4 = saldoInicial[4];
                this.valoresSaldoInicial.valorSaldo5 = saldoInicial[5];
                this.valoresSaldoInicial.valorSaldo6 = saldoInicial[6];
                #endregion

                #region  Prenche Saldos Iniciais
                XRTableRow detail1 = detail.Rows[1];
                //
                ((XRTableCell)detail1.Cells[0]).Text = Resources.ReportFluxoCaixaSinteticoGeral._SaldoInicial;
                //
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[1], saldoInicial[0], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[2], saldoInicial[1], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[3], saldoInicial[2], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[4], saldoInicial[3], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[5], saldoInicial[4], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[6], saldoInicial[5], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail1.Cells[7], saldoInicial[6], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                #endregion

                #endregion

                #region Linha 2

                List<decimal> valorLiquidacao = new List<decimal>();

                #region Calcula Liquidacao
                for (int i = 0; i < 7; i++) {
                    //
                    Liquidacao l = new Liquidacao();

                    Carteira.BooleanosFluxoCaixa booleanosFluxoCaixaParam = new Carteira.BooleanosFluxoCaixa();
                    booleanosFluxoCaixaParam.PosicaoBolsa = booleanosFluxoCaixa.PosicaoBolsa;
                    booleanosFluxoCaixaParam.PosicaoFundos = booleanosFluxoCaixa.PosicaoFundos;
                    booleanosFluxoCaixaParam.RendaFixa = booleanosFluxoCaixa.RendaFixa;
                    booleanosFluxoCaixaParam.ResgatesNaoConvertidos = booleanosFluxoCaixa.ResgatesNaoConvertidos;

                    if (this.ListData[i] < this.dataReferencia)
                    {
                        this.booleanosFluxoCaixa.PosicaoBolsa = false;
                        this.booleanosFluxoCaixa.PosicaoFundos = false;
                        this.booleanosFluxoCaixa.RendaFixa = false;
                    }

                    decimal valorLiquidar = 0;
                    if (i == 6)
                    { // Ultimo
                        valorLiquidar = new Carteira().RetornaFluxoLiquidarAposData(cliente, this.dataReferencia, this.ListData[i], this.booleanosFluxoCaixa);
                    }
                    else {
                        valorLiquidar = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, this.ListData[i], this.booleanosFluxoCaixa);
                    }                    

                    valorLiquidacao.Add(valorLiquidar);                
                }
                #endregion

                #region Prenche Liquidação
                XRTableRow detail2 = detail.Rows[2];
                //
                ((XRTableCell)detail2.Cells[0]).Text = Resources.ReportFluxoCaixaSinteticoGeral._LiquidacaoData;                
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[1], valorLiquidacao[0], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[2], valorLiquidacao[1], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[3], valorLiquidacao[2], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[4], valorLiquidacao[3], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[5], valorLiquidacao[4], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[6], valorLiquidacao[5], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail2.Cells[7], valorLiquidacao[6], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                #endregion
                #endregion

                #region Linha 3

                #region Calcula SaldoFinal
                this.valoresSaldoFinal.valorSaldo0 = this.valoresSaldoInicial.valorSaldo0 + valorLiquidacao[0];
                this.valoresSaldoFinal.valorSaldo1 = this.valoresSaldoInicial.valorSaldo1 + valorLiquidacao[1];
                this.valoresSaldoFinal.valorSaldo2 = this.valoresSaldoInicial.valorSaldo2 + valorLiquidacao[2];
                this.valoresSaldoFinal.valorSaldo3 = this.valoresSaldoInicial.valorSaldo3 + valorLiquidacao[3];
                this.valoresSaldoFinal.valorSaldo4 = this.valoresSaldoInicial.valorSaldo4 + valorLiquidacao[4];
                this.valoresSaldoFinal.valorSaldo5 = this.valoresSaldoInicial.valorSaldo5 + valorLiquidacao[5];
                this.valoresSaldoFinal.valorSaldo6 = this.valoresSaldoInicial.valorSaldo6 + valorLiquidacao[6];
                #endregion

                #region Prenche Saldo Final
                XRTableRow detail3 = detail.Rows[3];
                //
                ((XRTableCell)detail3.Cells[0]).Text = Resources.ReportFluxoCaixaSinteticoGeral._SaldoFinal;
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[1], this.valoresSaldoFinal.valorSaldo0);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[2], this.valoresSaldoFinal.valorSaldo1);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[3], this.valoresSaldoFinal.valorSaldo2);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[4], this.valoresSaldoFinal.valorSaldo3);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[5], this.valoresSaldoFinal.valorSaldo4);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[6], this.valoresSaldoFinal.valorSaldo5);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail3.Cells[7], this.valoresSaldoFinal.valorSaldo6);
                //
                #endregion

                #endregion
            }
        }
        #endregion                                    
    }
}