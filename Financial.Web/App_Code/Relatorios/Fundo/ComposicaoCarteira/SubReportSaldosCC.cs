﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using log4net;
using Financial.Fundo.Exceptions;
using Financial.Common.Enums;
using Financial.Common;
 
namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportSaldosCC
    /// </summary>
    public class SubReportSaldosCC : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(SubReportSaldosCC));

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idMoeda;

        public int IdMoeda {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        private int numeroLinhasDataTable;

        private ReportComposicaoCarteira.TipoRelatorio tipoRelatorio;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell38;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell59;
        private PageFooterBand PageFooter;
        private GroupHeaderBand GroupHeader1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell60;
		private XRTableCell xrTableCell61;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportSaldosCC() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia, ReportComposicaoCarteira.TipoRelatorio tipoRelatorio, int idMoeda) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            this.tipoRelatorio = tipoRelatorio;
            //
            //Carrega a moeda do cliente associado                        
            this.idMoeda = idMoeda;
            //
            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportSaldosCC._TituloRelatorio;
            this.xrTableCell9.Text = Resources.SubReportSaldosCC._Descricao;
            this.xrTableCell25.Text = Resources.SubReportSaldosCC._Valor;
            this.xrTableCell4.Text = Resources.SubReportSaldosCC.__C_C;
            this.xrTableCell6.Text = Resources.SubReportSaldosCC.__PL;
            this.xrTableCell60.Text = Resources.SubReportSaldosCC._Moeda;
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }


        /// <summary>
        /// Usado para Chamada Pelo Composicao Carteira Sem Resumo
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="tipoRelatorio"></param>
        public void PersonalInitialize(int idCarteira, DateTime dataReferencia, ReportComposicaoCarteiraSemResumo.TipoRelatorio tipoRelatorio, int idMoeda) {
            if (tipoRelatorio == ReportComposicaoCarteiraSemResumo.TipoRelatorio.Abertura) {
                this.PersonalInitialize(idCarteira, dataReferencia, ReportComposicaoCarteira.TipoRelatorio.Abertura, idMoeda);
            }
            else if (tipoRelatorio == ReportComposicaoCarteiraSemResumo.TipoRelatorio.Fechamento) {
                this.PersonalInitialize(idCarteira, dataReferencia, ReportComposicaoCarteira.TipoRelatorio.Fechamento, idMoeda);
            }
        }
        #endregion

        #region Usado para Testar Individualmente cada Report
        public SubReportSaldosCC(int idCarteira, DateTime dataReferencia, ReportComposicaoCarteira.TipoRelatorio tipoRelatorio) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            this.tipoRelatorio = tipoRelatorio;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportSaldosCC._TituloRelatorio;
            this.xrTableCell9.Text = Resources.SubReportSaldosCC._Descricao;
            this.xrTableCell25.Text = Resources.SubReportSaldosCC._Valor;
            this.xrTableCell4.Text = Resources.SubReportSaldosCC.__C_C;
            this.xrTableCell6.Text = Resources.SubReportSaldosCC.__PL;
            this.xrTableCell60.Text = Resources.SubReportSaldosCC._Moeda;
            #endregion

            //this.SetRelatorioSemDados();
        }

        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                //this.xrTable10.Visible = false;
                //this.xrTable7.Visible = false;
                //this.xrTable5.Visible = false;
                //this.xrTable1.Visible = false;
            }
        }

        private DataTable FillDados() {
            esUtility u = new esUtility();
            //
            esParameters esParams = new esParameters();
            esParams.Add("DataReferencia", this.dataReferencia);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" SELECT C.IdConta, ");
            sqlText.AppendLine("       C.Numero, ");
            sqlText.AppendLine("       C.IdTipoConta, ");
            sqlText.AppendLine("       C.ContaDefault, ");
            sqlText.AppendLine("       S.SaldoAbertura, ");
            sqlText.AppendLine("       S.SaldoFechamento, ");
            sqlText.AppendLine("       S.IdCliente, ");
            sqlText.AppendLine("       M.Nome ");
            sqlText.AppendLine(" FROM  [ContaCorrente] C, ");
            sqlText.AppendLine("       [SaldoCaixa] S, ");
            sqlText.AppendLine("       [Moeda] M ");
            sqlText.AppendLine(" WHERE S.IdConta = C.IdConta ");
            sqlText.AppendLine("       AND S.Data = @DataReferencia");
            sqlText.AppendLine("       AND S.IdCliente = " + this.IdCarteira);
            sqlText.AppendLine("       AND M.IdMoeda = C.IdMoeda");
            #endregion

            //log.Info(sqlText);

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            #region Trata multi-moeda
            foreach (DataRow dr in dt.Rows) {
                int idConta = Convert.ToInt32(dr[0]);
                decimal saldoAbertura = Convert.ToDecimal(dr[4]);

                decimal saldoFechamento = 0;
                if (dr[5] != null && !string.IsNullOrEmpty(dr[5].ToString())) {
                    saldoFechamento = Convert.ToDecimal(dr[5]);
                }

                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.IdMoeda);
                contaCorrente.LoadByPrimaryKey(campos, idConta);
                int idMoedaConta = contaCorrente.IdMoeda.Value;

                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaConta, this.dataReferencia);
                
                decimal saldoAberturaConvertido = Math.Round(saldoAbertura * fatorConversao, 2);
                decimal saldoFechamentoConvertido = Math.Round(saldoFechamento * fatorConversao, 2);

                dr[4] = saldoAberturaConvertido;
                dr[5] = saldoFechamentoConvertido;
                
            }
            #endregion

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/SubReportSaldosCC.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportSaldosCC.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportSaldosCC.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 46.00002F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("ContaDefault", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Numero", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell61,
            this.xrTableCell21,
            this.xrTableCell12,
            this.xrTableCell38});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "Descricao";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.5717741935483871D;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DescricaoBeforePrint);
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.Text = "Moeda";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 0.5717741935483871D;
            this.xrTableCell61.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MoedaBeforePrint);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "Valor";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.28185483870967742D;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "C/C";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.078629032258064516D;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ContaCorrenteBeforePrint);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "PL";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.067741935483870974D;
            this.xrTableCell38.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PLBeforePrint);
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 72.99998F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2744F, 48.00002F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell60,
            this.xrTableCell25,
            this.xrTableCell4,
            this.xrTableCell6});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Descricao";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.5717741935483871D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.Text = "#Moeda";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell60.Weight = 0.5717741935483871D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Valor";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell25.Weight = 0.28185483870967742D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#%C/C";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell4.Weight = 0.078629032258064516D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#%PL";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell6.Weight = 0.067741935483870974D;
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 14.99997F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2744F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1D;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 54.00002F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(26.00003F, 7.999996F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell43,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "#Total";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.5717741935483871D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Text = "Valor";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.28185483870967742D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "C/CTotal";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.078629032258064516D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "PLTotal";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.067741935483870974D;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell59.Weight = 1D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrTable10});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 121F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportSaldosCC
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportFooter,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportPrintOptions.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportSaldosCC.ResourceManager;
        }

        #region Variaveis Internas do Relatorio
        // Armazena os totais do relatorio
        protected class ValoresTotais {
            public decimal valor = 0;
            public decimal CC = 0;
            public decimal pl = 0;
        }
        private ValoresTotais valoresTotais = new ValoresTotais();

        // Armazena o total Valor para ser usado no calculo do campo C/C
        /* totalValor = saldoAbertura ou saldoFechamento de acordo com o tipo de relatório */
        private decimal totalValor = 0;
        #endregion

        #region Funções Internas do Relatorio
        //
        private void DescricaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell descricaoXRTableCell = sender as XRTableCell;
            descricaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string contaDefault = (string)this.GetCurrentColumnValue(ContaCorrenteMetadata.ColumnNames.ContaDefault);
                string numeroConta = (string)this.GetCurrentColumnValue(ContaCorrenteMetadata.ColumnNames.Numero);
                //
                contaDefault = contaDefault.ToUpper();
                string descricao = Resources.SubReportSaldosCC._MsgContaNaoDefault + numeroConta.Trim();
                //
                descricaoXRTableCell.Text = descricao;
            }
        }

        //
        private void MoedaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell descricaoXRTableCell = sender as XRTableCell;
            descricaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0)
            {
                string descricaoMoeda = (string)this.GetCurrentColumnValue(MoedaMetadata.ColumnNames.Nome);
                descricaoXRTableCell.Text = descricaoMoeda;
            }
        }

        private void ValorBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            valorXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldo = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                        ? (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoAbertura)
                        : (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoFechamento);
                //
                // Realiza a Somatoria do campo
                this.valoresTotais.valor += saldo;
                //
                ReportBase.ConfiguraSinalNegativo(valorXRTableCell, saldo);
            }
        }

        private void ContaCorrenteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            valorXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldo = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                        ? (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoAbertura)
                        : (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoFechamento);
                //
                decimal porcentagemSaldo = 0;
                if (this.totalValor != 0) {
                    porcentagemSaldo = saldo / this.totalValor;
                }

                // Realiza a Somatoria do campo
                this.valoresTotais.CC += porcentagemSaldo;
                //
                ReportBase.ConfiguraSinalNegativo(valorXRTableCell, porcentagemSaldo, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            }
        }

        private void PLBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell PLXRTableCell = sender as XRTableCell;
            PLXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {

                HistoricoCota historicoCota = new HistoricoCota();

                bool excecaoCota = false;
                try {
                    historicoCota.BuscaValorPatrimonioDia(this.idCarteira, this.dataReferencia);
                }
                catch (HistoricoCotaNaoCadastradoException e1) {
                    excecaoCota = true;
                }

                decimal valorPLPorcentagem = 0;

                if (!excecaoCota) {

                    decimal valorPL = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                                ? historicoCota.PLAbertura.Value
                                : historicoCota.PLFechamento.Value;

                    decimal valor = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                            ? (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoAbertura)
                            : (decimal)this.GetCurrentColumnValue(SaldoCaixaMetadata.ColumnNames.SaldoFechamento);


                    if (valorPL != 0) {
                        valorPLPorcentagem = valor / valorPL;
                    }
                }

                // Realiza a Somatória do campo
                this.valoresTotais.pl += valorPLPorcentagem;
                //
                ReportBase.ConfiguraSinalNegativo(PLXRTableCell, valorPLPorcentagem, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            }
        }

        /*
         *  Executado no start do relatorio
         */
        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Calcula o totalValor de acordo com o tipo de relatorio

            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            saldoCaixaCollection.Query.Select(saldoCaixaCollection.Query.IdConta,
                                              saldoCaixaCollection.Query.SaldoAbertura.Sum(),
                                              saldoCaixaCollection.Query.SaldoFechamento.Sum());
            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(this.idCarteira),
                                             saldoCaixaCollection.Query.Data.Equal(this.dataReferencia));
            saldoCaixaCollection.Query.GroupBy(saldoCaixaCollection.Query.IdConta);
            saldoCaixaCollection.Query.Load();

            decimal totalSaldo = 0;
            foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
            {
                int idConta = saldoCaixa.IdConta.Value;
                
                decimal saldo = 0;
                if (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura) {
                    saldo = saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0;
                }
                else if (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Fechamento) {
                    saldo = saldoCaixa.SaldoFechamento.HasValue ? saldoCaixa.SaldoFechamento.Value : 0;
                }

                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.IdMoeda);
                contaCorrente.LoadByPrimaryKey(campos, idConta);
                int idMoedaConta = contaCorrente.IdMoeda.Value;

                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaConta, this.dataReferencia);

                saldo = Math.Round(saldo * fatorConversao, 2);

                totalSaldo += saldo;
            }

            this.totalValor = totalSaldo;
        }

        #region Calcula e Exibe Totais
        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            /* TotalValor
             * TotalCC
             * TotalPL
             */
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[1]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = "";
            ((XRTableCell)summaryFinalRow0.Cells[3]).Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string total = Resources.SubReportSaldosCC._Total;

                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = total;
                //
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[1],
                        this.valoresTotais.valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                //
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[2],
                        this.valoresTotais.CC, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                //
                ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[3],
                        this.valoresTotais.pl, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            }
        }
        #endregion

        #endregion
    }
}