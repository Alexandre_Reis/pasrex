﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using log4net;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
 
namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportLiquidacao
    /// </summary>
    public class SubReportLiquidacao : XtraReport {
        //
        private static readonly ILog log = LogManager.GetLogger(typeof(SubReportLiquidacao));

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idMoeda;

        public int IdMoeda
        {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        private Carteira carteira;

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private enum TipoPesquisa {
            Liquidacao = 0,
            LiquidacaoAbertura = 1,
            LiquidacaoHistorico = 2
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.Liquidacao;

        /// <summary>
        /// Armazena o Tipo do Relatorio
        /// </summary>
        private ReportComposicaoCarteira.TipoRelatorio tipoRelatorio {
            get {
                // Se não tem relatorio Pai então lança Exceção
                if (this.MasterReport == null) {
                    throw new Exception("Relatorio não tem Pai");
                }
                else {

                    if (this.MasterReport is ReportComposicaoCarteiraMasa) {
                        return ReportComposicaoCarteira.TipoRelatorio.Fechamento;
                    }


                    //
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.TipoControle);
                    cliente.LoadByPrimaryKey(campos, this.idCarteira);

                    if (cliente.TipoControle == (byte)TipoControleCliente.IRRendaVariavel ||
                        cliente.TipoControle == (byte)TipoControleCliente.Carteira)
                    {
                        // Sem Resumo
                        if (((ReportComposicaoCarteiraSemResumo)this.MasterReport).TipoRelatorioComposicaoCarteiraSemResumo == ReportComposicaoCarteiraSemResumo.TipoRelatorio.Abertura) {
                            return ReportComposicaoCarteira.TipoRelatorio.Abertura;
                        }
                        else {
                            return ReportComposicaoCarteira.TipoRelatorio.Fechamento;
                        }
                    }
                    else {
                        return ((ReportComposicaoCarteira)this.MasterReport).TipoRelatorioComposicaoCarteira;
                    }
                }
            }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private Financial.ContaCorrente.LiquidacaoCollection liquidacaoCollection1;
        private Financial.ContaCorrente.LiquidacaoAberturaCollection liquidacaoAberturaCollection1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell22;
        private LiquidacaoHistoricoCollection liquidacaoHistoricoCollection1;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.Parameters.Parameter ValorPL;
        private DevExpress.XtraReports.Parameters.Parameter TotalLiquidar;
        private CalculatedField Participacao;
        private CalculatedField ParticipacaoPL;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportLiquidacao() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia, int idMoeda) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //
            this.dataReferencia = dataReferencia;

            this.idMoeda = idMoeda;
            //
            // Consulta do SubRelatorio
            DataView dt = this.FillDados(idMoeda);
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportLiquidacao._TituloRelatorio;
            this.xrTableCell9.Text = Resources.SubReportLiquidacao._Descricao;
            this.xrTableCell25.Text = Resources.SubReportLiquidacao._DataLancamento;
            this.xrTableCell28.Text = Resources.SubReportLiquidacao._DataVencimento;
            this.xrTableCell49.Text = Resources.SubReportLiquidacao._Valor;
            this.xrTableCell53.Text = Resources.SubReportLiquidacao._ValoresLiquidar;
            this.xrTableCell54.Text = Resources.SubReportLiquidacao.__PL;
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }

        #endregion

        #region Usado para Testar Individualmente cada Report
        public SubReportLiquidacao(int idCarteira, DateTime dataReferencia) {
            this.dataReferencia = dataReferencia;
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        [Obsolete("Não usado")]
        private void PersonalInitialize() {
            DataView dt = this.FillDados(1);
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportLiquidacao._TituloRelatorio;
            this.xrTableCell9.Text = Resources.SubReportLiquidacao._Descricao;
            this.xrTableCell25.Text = Resources.SubReportLiquidacao._DataLancamento;
            this.xrTableCell28.Text = Resources.SubReportLiquidacao._DataVencimento;
            this.xrTableCell49.Text = Resources.SubReportLiquidacao._Valor;
            this.xrTableCell53.Text = Resources.SubReportLiquidacao._ValoresLiquidar;
            this.xrTableCell54.Text = Resources.SubReportLiquidacao.__PL;
            #endregion
        }
        #endregion

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>        
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                this.xrTable10.Visible = false;
                this.xrTable7.Visible = false;
                this.xrTable2.Visible = false;
                this.xrTable1.Visible = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idMoeda"></param>
        /// <returns></returns>
        private DataView FillDados(int idMoeda) {
            // Define se faz Consulta Liquidacao ou LiquidacaoBolsaHistorico
            // Verifica A dataDia Do Cliente
            Cliente cliente = new Cliente();
            //this.tipoPesquisa = cliente.IsClienteNaData(this.IdCarteira, this.dataReferencia)
            //                    ? TipoPesquisa.Liquidacao
            //                    : TipoPesquisa.LiquidacaoHistorico;

            this.tipoPesquisa = cliente.IsClienteNaData(this.IdCarteira, this.dataReferencia)
                    ? (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Fechamento)
                                            ? TipoPesquisa.Liquidacao
                                            : TipoPesquisa.LiquidacaoAbertura
                    : TipoPesquisa.LiquidacaoHistorico;


            decimal totalLiquidar = 0;
            decimal totalLiquidarConvertido = 0;
            #region SQL
            if (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura) {
                #region SQL p/ Cota Abertura
                if (this.tipoPesquisa == TipoPesquisa.Liquidacao) {
                    #region Consulta em Liquidacao
                    this.liquidacaoCollection1.QueryReset();
                    //           
                    this.liquidacaoCollection1.Query
                         .Select(this.liquidacaoCollection1.Query.Descricao,
                                 this.liquidacaoCollection1.Query.DataLancamento,
                                 this.liquidacaoCollection1.Query.DataVencimento,
                                 this.liquidacaoCollection1.Query.Valor.Sum())
                         .Where(this.liquidacaoCollection1.Query.IdCliente == this.idCarteira,
                                this.liquidacaoCollection1.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                this.liquidacaoCollection1.Query.DataLancamento.LessThan(this.dataReferencia),
                                this.liquidacaoCollection1.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                this.liquidacaoCollection1.Query.Valor != 0)
                         .GroupBy(this.liquidacaoCollection1.Query.Descricao,
                                  this.liquidacaoCollection1.Query.DataLancamento,
                                  this.liquidacaoCollection1.Query.DataVencimento);

                    this.liquidacaoCollection1.Query.Load();
                    //log.Info(this.liquidacaoCollection1.Query.es.LastQuery);
                    //
                    /* Para casos onde a somatoria de valor resulta em 0 há a possibilidadade de aparecer liquidações com zero
                       Retira os registros com valor zero da collection         
                    */
                    this.liquidacaoCollection1.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";
                    //
                    LiquidacaoCollection liquidacaoCollection2 = new LiquidacaoCollection();
                    liquidacaoCollection2.Query
                         .Select(liquidacaoCollection2.Query.Descricao,
                                 liquidacaoCollection2.Query.DataLancamento,
                                 liquidacaoCollection2.Query.DataVencimento,
                                 liquidacaoCollection2.Query.Valor.Sum())
                         .Where(liquidacaoCollection2.Query.IdCliente == this.idCarteira,
                                liquidacaoCollection2.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                liquidacaoCollection2.Query.DataLancamento == this.dataReferencia,
                                liquidacaoCollection2.Query.Fonte == FonteLancamentoLiquidacao.Interno,
                                liquidacaoCollection2.Query.Situacao == (byte)SituacaoLancamentoLiquidacao.Normal,
                                liquidacaoCollection2.Query.Valor != 0)
                         .GroupBy(liquidacaoCollection2.Query.Descricao,
                                  liquidacaoCollection2.Query.DataLancamento,
                                  liquidacaoCollection2.Query.DataVencimento);

                    liquidacaoCollection2.Query.Load();
                    //
                    //log.Info(liquidacaoCollection2.Query.es.LastQuery);
                    liquidacaoCollection2.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";

                    // Merge das Collections
                    this.liquidacaoCollection1.Combine(liquidacaoCollection2);
                    //
                    // Somatoria do campo Valor para ser usado no calculo do campo Valores a Liquidar                    
                    for (int i = 0; i < liquidacaoCollection1.Count; i++) {
                        totalLiquidar += liquidacaoCollection1[i].Valor.Value;
                    }
                    #endregion
                }
                else if (this.tipoPesquisa == TipoPesquisa.LiquidacaoHistorico) {
                    #region Consulta em LiquidacaoHistorico
                    this.liquidacaoHistoricoCollection1.QueryReset();
                    //           
                    this.liquidacaoHistoricoCollection1.Query
                         .Select(this.liquidacaoHistoricoCollection1.Query.Descricao,
                                 this.liquidacaoHistoricoCollection1.Query.DataLancamento,
                                 this.liquidacaoHistoricoCollection1.Query.DataVencimento,
                                 this.liquidacaoHistoricoCollection1.Query.Valor.Sum())
                         .Where(this.liquidacaoHistoricoCollection1.Query.IdCliente == this.idCarteira,
                                this.liquidacaoHistoricoCollection1.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                this.liquidacaoHistoricoCollection1.Query.DataLancamento.LessThan(this.dataReferencia),
                                this.liquidacaoHistoricoCollection1.Query.DataHistorico == this.dataReferencia,
                                this.liquidacaoHistoricoCollection1.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                this.liquidacaoHistoricoCollection1.Query.Valor != 0)
                         .GroupBy(this.liquidacaoHistoricoCollection1.Query.Descricao,
                                  this.liquidacaoHistoricoCollection1.Query.DataLancamento,
                                  this.liquidacaoHistoricoCollection1.Query.DataVencimento);

                    this.liquidacaoHistoricoCollection1.Query.Load();
                    //log.Info(this.liquidacaoHistoricoCollection1.Query.es.LastQuery);
                    //
                    /* Para casos onde a somatoria de valor resulta em 0 há a possibilidadade de aparecer liquidações com zero
                       Retira os registros com valor zero da collection         
                    */
                    this.liquidacaoHistoricoCollection1.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";
                    //
                    LiquidacaoHistoricoCollection liquidacaoHistoricoCollection2 = new LiquidacaoHistoricoCollection();
                    liquidacaoHistoricoCollection2.Query
                         .Select(liquidacaoHistoricoCollection2.Query.Descricao,
                                 liquidacaoHistoricoCollection2.Query.DataLancamento,
                                 liquidacaoHistoricoCollection2.Query.DataVencimento,
                                 liquidacaoHistoricoCollection2.Query.Valor.Sum())
                         .Where(liquidacaoHistoricoCollection2.Query.IdCliente == this.idCarteira,
                                liquidacaoHistoricoCollection2.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                liquidacaoHistoricoCollection2.Query.DataLancamento == this.dataReferencia,
                                liquidacaoHistoricoCollection2.Query.DataHistorico == this.dataReferencia,
                                liquidacaoHistoricoCollection2.Query.Fonte == FonteLancamentoLiquidacao.Interno,
                                liquidacaoHistoricoCollection2.Query.Situacao == (byte)SituacaoLancamentoLiquidacao.Normal,
                                liquidacaoHistoricoCollection2.Query.Valor != 0)
                         .GroupBy(liquidacaoHistoricoCollection2.Query.Descricao,
                                  liquidacaoHistoricoCollection2.Query.DataLancamento,
                                  liquidacaoHistoricoCollection2.Query.DataVencimento);

                    liquidacaoHistoricoCollection2.Query.Load();
                    //
                    //log.Info(liquidacaoHistoricoCollection2.Query.es.LastQuery);
                    liquidacaoHistoricoCollection2.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";

                    // Merge das Collections
                    this.liquidacaoHistoricoCollection1.Combine(liquidacaoHistoricoCollection2);
                    //
                    // Somatoria do campo Valor para ser usado no calculo do campo Valores a Liquidar
                    for (int i = 0; i < liquidacaoHistoricoCollection1.Count; i++) {
                        totalLiquidar += liquidacaoHistoricoCollection1[i].Valor.Value;
                    }
                    #endregion
                }
                else if (this.tipoPesquisa == TipoPesquisa.LiquidacaoAbertura) {
                    #region Consulta em LiquidacaoAbertura
                    this.liquidacaoAberturaCollection1.QueryReset();
                    //           
                    this.liquidacaoAberturaCollection1.Query
                         .Select(this.liquidacaoAberturaCollection1.Query.Descricao,
                                 this.liquidacaoAberturaCollection1.Query.DataLancamento,
                                 this.liquidacaoAberturaCollection1.Query.DataVencimento,
                                 this.liquidacaoAberturaCollection1.Query.Valor.Sum())
                         .Where(this.liquidacaoAberturaCollection1.Query.IdCliente == this.idCarteira,
                                this.liquidacaoAberturaCollection1.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                this.liquidacaoAberturaCollection1.Query.DataLancamento.LessThan(this.dataReferencia),
                                this.liquidacaoAberturaCollection1.Query.DataHistorico == this.dataReferencia,
                                this.liquidacaoAberturaCollection1.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                this.liquidacaoAberturaCollection1.Query.Valor != 0)
                         .GroupBy(this.liquidacaoAberturaCollection1.Query.Descricao,
                                  this.liquidacaoAberturaCollection1.Query.DataLancamento,
                                  this.liquidacaoAberturaCollection1.Query.DataVencimento);

                    this.liquidacaoAberturaCollection1.Query.Load();
                    //log.Info(this.liquidacaoHistoricoCollection1.Query.es.LastQuery);
                    //
                    /* Para casos onde a somatoria de valor resulta em 0 há a possibilidadade de aparecer liquidações com zero
                       Retira os registros com valor zero da collection         
                    */
                    this.liquidacaoAberturaCollection1.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";
                    //
                    LiquidacaoAberturaCollection liquidacaoAberturaCollection2 = new LiquidacaoAberturaCollection();
                    liquidacaoAberturaCollection2.Query
                         .Select(liquidacaoAberturaCollection2.Query.Descricao,
                                 liquidacaoAberturaCollection2.Query.DataLancamento,
                                 liquidacaoAberturaCollection2.Query.DataVencimento,
                                 liquidacaoAberturaCollection2.Query.Valor.Sum())
                         .Where(liquidacaoAberturaCollection2.Query.IdCliente == this.idCarteira,
                                liquidacaoAberturaCollection2.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                liquidacaoAberturaCollection2.Query.DataLancamento == this.dataReferencia,
                                liquidacaoAberturaCollection2.Query.DataHistorico == this.dataReferencia,
                                liquidacaoAberturaCollection2.Query.Fonte == FonteLancamentoLiquidacao.Interno,
                                liquidacaoAberturaCollection2.Query.Situacao == (byte)SituacaoLancamentoLiquidacao.Normal,
                                liquidacaoAberturaCollection2.Query.Valor != 0)
                         .GroupBy(liquidacaoAberturaCollection2.Query.Descricao,
                                  liquidacaoAberturaCollection2.Query.DataLancamento,
                                  liquidacaoAberturaCollection2.Query.DataVencimento);

                    liquidacaoAberturaCollection2.Query.Load();
                    //
                    //log.Info(liquidacaoHistoricoCollection2.Query.es.LastQuery);
                    liquidacaoAberturaCollection2.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";

                    // Merge das Collections
                    this.liquidacaoAberturaCollection1.Combine(liquidacaoAberturaCollection2);
                    //
                    // Somatoria do campo Valor para ser usado no calculo do campo Valores a Liquidar
                    for (int i = 0; i < liquidacaoAberturaCollection1.Count; i++) {
                        totalLiquidar += liquidacaoAberturaCollection1[i].Valor.Value;
                    }
                    #endregion
                }
                #endregion

                totalLiquidarConvertido = totalLiquidar;
            }
            else if (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Fechamento) {
                #region SQL p/ Cota Fechamento
                #region Consulta em LiquidacaoHistorico
                this.liquidacaoHistoricoCollection1.QueryReset();
                //           
                this.liquidacaoHistoricoCollection1.Query
                     .Select(this.liquidacaoHistoricoCollection1.Query.Descricao,
                             this.liquidacaoHistoricoCollection1.Query.IdConta,
                             this.liquidacaoHistoricoCollection1.Query.DataLancamento,
                             this.liquidacaoHistoricoCollection1.Query.DataVencimento,
                             this.liquidacaoHistoricoCollection1.Query.Valor.Sum())
                     .Where(this.liquidacaoHistoricoCollection1.Query.IdCliente == this.idCarteira &&
                            this.liquidacaoHistoricoCollection1.Query.DataLancamento.LessThanOrEqual(this.dataReferencia) &
                            (
                                (this.liquidacaoHistoricoCollection1.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Normal, (byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    this.liquidacaoHistoricoCollection1.Query.DataVencimento.GreaterThan(this.dataReferencia)) |
                                (this.liquidacaoHistoricoCollection1.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    this.liquidacaoHistoricoCollection1.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia) &
                                    this.liquidacaoHistoricoCollection1.Query.Origem.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter))
                            )&
                            this.liquidacaoHistoricoCollection1.Query.DataHistorico == this.dataReferencia &
                            this.liquidacaoHistoricoCollection1.Query.Valor != 0)

                     .GroupBy(this.liquidacaoHistoricoCollection1.Query.Descricao,
                              this.liquidacaoHistoricoCollection1.Query.IdConta,
                              this.liquidacaoHistoricoCollection1.Query.DataLancamento,
                              this.liquidacaoHistoricoCollection1.Query.DataVencimento);

                this.liquidacaoHistoricoCollection1.Query.Load();
                //
                //log.Info(this.liquidacaoHistoricoCollection1.Query.es.LastQuery);
                this.liquidacaoHistoricoCollection1.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";
                #endregion

                #region  Trata multi-moeda
                foreach (LiquidacaoHistorico liquidacao in this.liquidacaoHistoricoCollection1) 
                {
                    int idConta = liquidacao.IdConta.Value;
                    decimal valor = liquidacao.Valor.Value;

                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(contaCorrente.Query.IdMoeda);
                    contaCorrente.LoadByPrimaryKey(campos, idConta);
                    int idMoedaConta = contaCorrente.IdMoeda.Value;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaConta, this.dataReferencia);

                    valor = Math.Round(valor * fatorConversao, 2);
                    liquidacao.Valor = valor;                        

                    totalLiquidarConvertido += liquidacao.Valor.Value;
                }
                #endregion                    
                #endregion
            }
            #endregion

            HistoricoCota historicoCota = new HistoricoCota();

            bool excecaoCota = false;
            try {
                historicoCota.BuscaValorPatrimonioDia(this.idCarteira, this.dataReferencia);
            }
            catch (HistoricoCotaNaoCadastradoException e) {
                excecaoCota = true;
            }

            decimal valorPL = 0;

            if (!excecaoCota) {
                if (this.carteira.IsTipoCotaAbertura() &&
                    this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura) {
                    valorPL = historicoCota.PLAbertura.Value;
                }
                else {
                    valorPL = historicoCota.PLFechamento.Value;
                }
            }

            this.Parameters["ValorPL"].Value = valorPL;
            //
            this.Parameters["TotalLiquidar"].Value = totalLiquidarConvertido;
            //            
            //return (this.tipoPesquisa == TipoPesquisa.Liquidacao)
            //        ? liquidacaoCollection1.LowLevelBind()
            //        : liquidacaoHistoricoCollection1.LowLevelBind();

            if (this.tipoPesquisa == TipoPesquisa.Liquidacao || this.tipoPesquisa == TipoPesquisa.LiquidacaoHistorico)
            {
                return liquidacaoHistoricoCollection1.LowLevelBind();
            }
            else { // Abertura
                return liquidacaoAberturaCollection1.LowLevelBind();
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/ComposicaoCarteira/Fundo/SubReportLiquidacao.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportLiquidacao.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.liquidacaoCollection1 = new Financial.ContaCorrente.LiquidacaoCollection();
            this.liquidacaoAberturaCollection1 = new Financial.ContaCorrente.LiquidacaoAberturaCollection();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.liquidacaoHistoricoCollection1 = new Financial.ContaCorrente.LiquidacaoHistoricoCollection();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ValorPL = new DevExpress.XtraReports.Parameters.Parameter();
            this.TotalLiquidar = new DevExpress.XtraReports.Parameters.Parameter();
            this.Participacao = new DevExpress.XtraReports.UI.CalculatedField();
            this.ParticipacaoPL = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 48F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DataLancamento", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("DataVencimento", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell50,
            this.xrTableCell56,
            this.xrTableCell57});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Descricao")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.35201612903225804D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataLancamento", "{0:d}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.10241935483870968D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataVencimento", "{0:d}")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.10040322580645161D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Valor")});
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "xrTableCell50";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.29233870967741937D;
            this.xrTableCell50.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.CanGrow = false;
            this.xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Participacao")});
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.Text = "xrTableCell56";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.0939516129032258D;
            this.xrTableCell56.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.CanGrow = false;
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ParticipacaoPL")});
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.Text = "xrTableCell57";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.058870967741935482D;
            this.xrTableCell57.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // liquidacaoCollection1
            // 
            this.liquidacaoCollection1.AllowDelete = true;
            this.liquidacaoCollection1.AllowEdit = true;
            this.liquidacaoCollection1.AllowNew = true;
            this.liquidacaoCollection1.EnableHierarchicalBinding = true;
            this.liquidacaoCollection1.Filter = "";
            this.liquidacaoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.liquidacaoCollection1.Sort = "";
            // 
            // liquidacaoAberturaCollection1
            // 
            this.liquidacaoAberturaCollection1.AllowDelete = true;
            this.liquidacaoAberturaCollection1.AllowEdit = true;
            this.liquidacaoAberturaCollection1.AllowNew = true;
            this.liquidacaoAberturaCollection1.EnableHierarchicalBinding = true;
            this.liquidacaoAberturaCollection1.Filter = "";
            this.liquidacaoAberturaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.liquidacaoAberturaCollection1.Sort = "";
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 14.99997F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2744F, 50F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 74.99995F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2744F, 48.00002F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell49,
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Descricao";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.35201612903225804D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#DataLancamento";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.10241935483870968D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#DataVencimento";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell28.Weight = 0.10040322580645161D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.Text = "#Valor";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell49.Weight = 0.29233870967741937D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Text = "#ValoresLiquidar";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell53.Weight = 0.0939516129032258D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.Text = "#%PL";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell54.Weight = 0.058870967741935482D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 15F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1D;
            // 
            // liquidacaoHistoricoCollection1
            // 
            this.liquidacaoHistoricoCollection1.AllowDelete = true;
            this.liquidacaoHistoricoCollection1.AllowEdit = true;
            this.liquidacaoHistoricoCollection1.AllowNew = true;
            this.liquidacaoHistoricoCollection1.EnableHierarchicalBinding = true;
            this.liquidacaoHistoricoCollection1.Filter = "";
            this.liquidacaoHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.liquidacaoHistoricoCollection1.Sort = "";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.xrTable7});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 123F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 52.97628F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "#Total";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.35201612903225804D;
            this.xrTableCell1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TotalBeforePrint);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.10241935483870968D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.10040322580645161D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Valor")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell4.Summary = xrSummary1;
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.29233870967741937D;
            this.xrTableCell4.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.CanGrow = false;
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Participacao")});
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell5.Summary = xrSummary2;
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 0.0939516129032258D;
            this.xrTableCell5.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ParticipacaoPL")});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell6.Summary = xrSummary3;
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.058870967741935482D;
            this.xrTableCell6.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // ValorPL
            // 
            this.ValorPL.Description = "ValorPL";
            this.ValorPL.Name = "ValorPL";
            this.ValorPL.Type = typeof(decimal);
            this.ValorPL.ValueInfo = "0";
            // 
            // TotalLiquidar
            // 
            this.TotalLiquidar.Description = "TotalLiquidar";
            this.TotalLiquidar.Name = "TotalLiquidar";
            this.TotalLiquidar.Type = typeof(decimal);
            this.TotalLiquidar.ValueInfo = "0";
            // 
            // Participacao
            // 
            this.Participacao.DisplayName = "Participacao";
            this.Participacao.Expression = "[Valor] / [Parameters.TotalLiquidar]";
            this.Participacao.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.Participacao.Name = "Participacao";
            // 
            // ParticipacaoPL
            // 
            this.ParticipacaoPL.DisplayName = "ParticipacaoPL";
            this.ParticipacaoPL.Expression = "[Valor] /  [Parameters.ValorPL]";
            this.ParticipacaoPL.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.ParticipacaoPL.Name = "ParticipacaoPL";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportLiquidacao
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.Participacao,
            this.ParticipacaoPL});
            this.DataSource = this.liquidacaoCollection1;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ValorPL,
            this.TotalLiquidar});
            this.ReportPrintOptions.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportLiquidacao.ResourceManager;
        }

        #region Eventos CustomFormat
        protected void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        protected void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            //decimal valor = (decimal)this.GetCurrentColumnValue(LiquidacaoMetadata.ColumnNames.Valor);
            //decimal valorPL = Convert.ToDecimal(this.Parameters["ValorPL"].Value);
            //decimal participacao = Math.Round(valor / valorPL, 2);

            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
        }
        #endregion

        private void TotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell totalXRTableCell = sender as XRTableCell;
            totalXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                totalXRTableCell.Text = Resources.SubReportLiquidacao._Total;
            }
        }
    }
}