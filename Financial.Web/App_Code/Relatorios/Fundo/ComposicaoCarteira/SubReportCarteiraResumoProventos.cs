﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.Swap;
using Financial.Swap.Enums;
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.Common.Exceptions;
using Financial.Fundo.Exceptions;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.Util;
 
namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportCarteiraResumo
    /// </summary>
    public class SubReportCarteiraResumoProventos : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        //
        private Carteira carteira;

        private DateTime dataInicioCliente;

        private int numeroLinhasDataTable;

        /// <summary>
        /// Armazena o Tipo do Relatorio - Abertura ou Fechamento
        /// </summary>
        private ReportComposicaoCarteira.TipoRelatorio tipoRelatorio {
            get {
                // Se não tem relatorio Pai então lança Exceção
                if (this.MasterReport == null) {
                    throw new Exception("Relatorio não tem Pai");
                }
                else {
                    if (this.MasterReport is ReportComposicaoCarteiraMasa) {
                        return ReportComposicaoCarteira.TipoRelatorio.Fechamento;
                    }

                    else {
                        return ((ReportComposicaoCarteira)this.MasterReport).TipoRelatorioComposicaoCarteira;
                    }
                }
            }
        }

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        //private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell24;
        //private XRTable xrTable1;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell47;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCell72;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell79;
        private XRTable xrTable2;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell48;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell25;
        private ListaBenchmarkCollection listaBenchmarkCollection1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell8;
        private GroupHeaderBand GroupHeader1;
        //private XRTable xrTable4;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell17;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportCarteiraResumoProventos() {
            this.InitializeComponent();
        }

        /// <summary>
        /// Define Visibilidade das Tables
        /// </summary>
        private void DefineVisibilidade() {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            cliente.LoadByPrimaryKey(campos, this.idCarteira);
            
            // Tudo Vísivel
            //this.xrTable4.Visible = false;
            //this.xrTable10.Visible = false;
            //this.xrTable1.Visible = false;
            this.xrTable2.Visible = true;
            this.xrTable6.Visible = false;
            this.xrTable3.Visible = false;
            //
            //switch (cliente.TipoControle) {
            //  case (byte)TipoControleCliente.Carteira:
            //      this.xrTable4.Visible = true;
            //      this.xrTable10.Visible = false;
            //      this.xrTable1.Visible = false;
            //      this.xrTable2.Visible = false;
            //      this.xrTable6.Visible = false;
            //      this.xrTable3.Visible = false;
            //      break;	 	  
            //}            
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            this.carteira.LoadByPrimaryKey(this.idCarteira);
            //
            this.dataReferencia = dataReferencia;

            // Carrega a data inicio do Cliente
            this.dataInicioCliente = carteira.DataInicioCota.Value;

            //
            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportCarteiraResumoProventos._TituloRelatorio;
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.DefineVisibilidade();
        }

        #endregion
        
        #region Usado para Testar Individualmente cada Report
        public SubReportCarteiraResumoProventos(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            this.carteira.LoadByPrimaryKey(this.idCarteira);
            //
            this.dataReferencia = dataReferencia;
            //
            // Carrega a data inicio do Cliente
            this.dataInicioCliente = carteira.DataInicioCota.Value;
            
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportCarteiraResumoProventos._TituloRelatorio;
            #endregion

            this.SetRelatorioSemDados();
        }

        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport
                //this.subreport1.Visible = true;
                //this.PageHeader.Visible = false;
                //this.Detail.Visible = false;
            }
        }

        private DataTable FillDados() {
            #region SQL
            this.listaBenchmarkCollection1.QueryReset();
            this.listaBenchmarkCollection1.Query
                 .Select(this.listaBenchmarkCollection1.Query.IdIndice)
                 .Where(this.listaBenchmarkCollection1.Query.IdCarteira == this.IdCarteira);
            #endregion
            //
            return this.listaBenchmarkCollection1.Query.LoadDataTable();            
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportCarteiraResumo.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportCarteiraResumoProventos.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            //this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            //this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.listaBenchmarkCollection1 = new Financial.Fundo.ListaBenchmarkCollection();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            //this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 45F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TipoSerie", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBMF", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Serie", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(0F, 0F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailRentabilidadeBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell24,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell13,
            this.xrTableCell1,
            this.xrTableCell17});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "DescricaoIndice";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.25380705697743405;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "RentabilidadeDia";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.12276849913665297;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "RentabilidadeMes";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.12303354992521148;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "RentabilidadeAno";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.12309731972623028;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Rentabilidade6Meses";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.12507120076043427;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Rentabilidade12Meses";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 0.12292294883317094;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "RentabilidadeInicio";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.12929942464086591;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Transparent;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 255F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow5,
            this.xrTableRow9});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2045F, 1F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableRentabilidadeBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.25405405405405407;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Rentabilidade";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 1;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell37,
            this.xrTableCell40,
            this.xrTableCell48,
            this.xrTableCell3,
            this.xrTableCell26});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.24864864864864866;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell34.Weight = 0.25380714808722693;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "#Dia";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.12276840723642103;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Text = "#Mes";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell37.Weight = 0.12303354689909737;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Text = "#Ano";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell40.Weight = 0.12309732780937741;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "#6Meses";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell48.Weight = 0.12507119640979092;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "#12Meses";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.12292294428649157;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "#Ínicio";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.12929942927159493;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell6,
            this.xrTableCell27});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.24864864864864866;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.25380714808722693;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.12276840723642103;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.12303354689909737;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.12309732780937741;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.12507119640979092;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.12292294428649157;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.12929942927159493;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell25,
            this.xrTableCell16,
            this.xrTableCell28});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.24864864864864866;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.25380714808722693;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.12276840723642103;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.12303354689909737;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.12309732780937741;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell25.Weight = 0.12507119640979092;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.12292294428649157;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.12929942927159493;
            // 
            // xrTable1
            // 
            //this.xrTable1.Dpi = 254F;
            //this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 145F);
            //this.xrTable1.Name = "xrTable1";
            //this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            //this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            //this.xrTableRow2,
            //this.xrTableRow3});
            //this.xrTable1.SizeF = new System.Drawing.SizeF(0F, 0F);
            //this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PatrimonioCotaBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell36,
            this.xrTableCell39,
            this.xrTableCell47});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.33576642335766421;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell31.BorderWidth = 1;
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell31.Weight = 0.028;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Text = "#QtdCotas";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.14133333333333334;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Text = "ValorQtdCotas";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.212;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell36.Weight = 0.14066666666666666;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "#CotaBruta";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.20133333333333334;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell47.BorderWidth = 1;
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Text = "ValorCotaBruta";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.27666666666666667;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell75,
            this.xrTableCell77,
            this.xrTableCell79});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.32846715328467152;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.BorderWidth = 1;
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell70.Weight = 0.028;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell71.Weight = 0.14133333333333334;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell72.Weight = 0.212;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell75.Weight = 0.14066666666666666;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell77.Text = "#CotaLiquida";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell77.Weight = 0.20133333333333334;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.BorderWidth = 1;
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.Text = "ValorCotaLiquida";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell79.Weight = 0.27666666666666667;
            // 
            // xrTable10
            // 
            //this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            //this.xrTable10.Dpi = 254F;
            //this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 83.00002F);
            //this.xrTable10.Name = "xrTable10";
            //this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            //this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            //this.xrTableRow10});
            //this.xrTable10.SizeF = new System.Drawing.SizeF(0F, 0F);
            //this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(575.0001F, 14.99997F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 1;
            // 
            // listaBenchmarkCollection1
            // 
            this.listaBenchmarkCollection1.AllowDelete = true;
            this.listaBenchmarkCollection1.AllowEdit = true;
            this.listaBenchmarkCollection1.AllowNew = true;
            this.listaBenchmarkCollection1.EnableHierarchicalBinding = true;
            this.listaBenchmarkCollection1.Filter = "";
            this.listaBenchmarkCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.listaBenchmarkCollection1.Sort = "";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 447F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            //this.xrTable4.BackColor = System.Drawing.Color.WhiteSmoke;
            //this.xrTable4.Dpi = 254F;
            //this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 14.99997F);
            //this.xrTable4.Name = "xrTable4";
            //this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            //this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            //this.xrTableRow12});
            //this.xrTable4.SizeF = new System.Drawing.SizeF(0F, 0F);
            //this.xrTable4.StylePriority.UseBackColor = false;
            //this.xrTable4.StylePriority.UseBorders = false;
            //this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //this.xrTable4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PLBeforePrint);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell2});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.33576642335766421;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "#PLAbertura/PLFechamento";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.41261831430606943;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "ValorPL";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 0.20341608871530054;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 0.10575037388151715;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportCarteiraResumo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.listaBenchmarkCollection1;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 150, 150);
            this.PageHeight = 100;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportCarteiraResumo.ResourceManager;
        }

        #region Variaveis Internas do Relatorio
        // Armazena os valores do patrimonio
        protected class ValoresPatrimonio {
            public decimal? cota;
            public decimal? cotaBruta;
            public decimal? pl;            
            public decimal? quantidadeFechamento;
        }
        private ValoresPatrimonio valoresPatrimonio = new ValoresPatrimonio();
        #endregion
       
        // Contém o Valor das Rentalidades da Carteira 
        /* rentabilidadeCotaDia
         * rentabilidadeCotaMes
         * rentabilidadeCotaAno
         * rentabilidadeCota6Meses
         * rentabilidadeCota12Meses
         * rentabilidadeCotaInicio
         */
        // Variável Global ao relatorio Setado no método TableRentabilidadeBeforePrint
        private List<decimal?> rentabilidadeCotaTable = new List<decimal?>(6); 
        //

        #region Funções Internas do Relatorio
        //
        private void PatrimonioCotaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;

            // Limpa os dados da Table
            ReportBase.LimpaDadosTable(xrTable);
            //

            HistoricoCota historicoCota = new HistoricoCota();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(historicoCota.Query.IdCarteira);
            campos.Add(historicoCota.Query.CotaAbertura);
            campos.Add(historicoCota.Query.CotaFechamento);
            campos.Add(historicoCota.Query.CotaBruta);
            campos.Add(historicoCota.Query.QuantidadeFechamento);
            //

            if (historicoCota.LoadByPrimaryKey(campos, this.dataReferencia, this.idCarteira))
            {
                int tipoCota = historicoCota.UpToCarteiraByIdCarteira.TipoCota.Value;
                //
                this.valoresPatrimonio.cotaBruta = historicoCota.CotaBruta.Value;
                if (tipoCota == (int)TipoCotaFundo.Abertura) 
                {
                    if (this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Fechamento)
                    {
                        this.valoresPatrimonio.cota = historicoCota.CotaFechamento.Value;
                        this.valoresPatrimonio.quantidadeFechamento = historicoCota.QuantidadeFechamento.Value;
                    }
                    else
                    {
                        this.valoresPatrimonio.cota = historicoCota.CotaAbertura.Value;
                    
                        DateTime dataAnterior = Util.Calendario.SubtraiDiaUtil(this.dataReferencia, 1);

                        HistoricoCota historicoCotaQtde = new HistoricoCota();
                        campos.Clear();
                        campos.Add(historicoCotaQtde.Query.QuantidadeFechamento);
                        if (historicoCotaQtde.LoadByPrimaryKey(campos, dataAnterior, this.idCarteira))
                        {
                            this.valoresPatrimonio.quantidadeFechamento = historicoCotaQtde.QuantidadeFechamento;
                        }
                    }
                }
                else 
                {
                    this.valoresPatrimonio.cota = historicoCota.CotaFechamento.Value;
                    this.valoresPatrimonio.quantidadeFechamento = historicoCota.QuantidadeFechamento.Value;
                }
              
                #region Exibe os Valores
                /* Cabeçalho - Titulo */
                //#region Cabecalho
                //XRTableRow xrTableRow0 = xrTable.Rows[0];
                //((XRTableCell)xrTableRow0.Cells[0]).Text = Resources.SubReportCarteiraResumo._PatrimonioCota;
                //#endregion

                #region Linha 0 - Quantidade de Cotas
                XRTableRow xrTableRow1 = xrTable.Rows[0];
                ((XRTableCell)xrTableRow1.Cells[1]).Text = Resources.SubReportCarteiraResumo._QtdCotas;
                ((XRTableCell)xrTableRow1.Cells[2]).Text =
                        this.valoresPatrimonio.quantidadeFechamento.HasValue
                        ? this.valoresPatrimonio.quantidadeFechamento.Value.ToString("n8") : "-";
                                               
                //
                ((XRTableCell)xrTableRow1.Cells[4]).Text = Resources.SubReportCarteiraResumo._CotaBruta;
                ((XRTableCell)xrTableRow1.Cells[5]).Text = 
                            this.valoresPatrimonio.cotaBruta.HasValue
                            ? this.valoresPatrimonio.cotaBruta.Value.ToString("n8") : "-";
                #endregion

                #region Linha1
                XRTableRow xrTableRow2 = xrTable.Rows[1];
                //
                ((XRTableCell)xrTableRow2.Cells[4]).Text = Resources.SubReportCarteiraResumo._CotaLiquida;
                ((XRTableCell)xrTableRow2.Cells[5]).Text =
                            this.valoresPatrimonio.cota.HasValue
                            ? this.valoresPatrimonio.cota.Value.ToString("n8") : "-";
                #endregion

                #endregion
            }
        }

        private void PLBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;

            // Limpa os dados da Table
            ReportBase.LimpaDadosTable(xrTable);
            //
            
            HistoricoCota historicoCota = new HistoricoCota();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(historicoCota.Query.IdCarteira);
            campos.Add(historicoCota.Query.PLAbertura);
            campos.Add(historicoCota.Query.PLFechamento);
            //           

            if (WebConfigConfiguration.WebConfig.AppSettings.Cliente == "PORTOPAR" ||
                WebConfigConfiguration.WebConfig.AppSettings.UsaPatrimonioLiquidoComposicaoCarteira)
            {
                this.valoresPatrimonio.pl = historicoCota.RetornaPatrimonioSemTributos(this.idCarteira, this.dataReferencia);
            }
            else
            {
                if (historicoCota.LoadByPrimaryKey(campos, this.dataReferencia, this.idCarteira))
                {
                    int tipoCota = historicoCota.UpToCarteiraByIdCarteira.TipoCota.Value;   
                    //
                    if (tipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        this.valoresPatrimonio.pl = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Fechamento
                                                    ? historicoCota.PLFechamento.Value
                                                    : historicoCota.PLAbertura.Value;
                    }
                    else
                    {
                        this.valoresPatrimonio.pl = historicoCota.PLFechamento.Value;
                    }


                }
            }

            #region Linha 0 - PLAbertura ou PLFechamento
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            ((XRTableCell)xrTableRow0.Cells[0]).Text = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                ? Resources.SubReportCarteiraResumo._PLAbertura : Resources.SubReportCarteiraResumo._PLFechamento;
            ((XRTableCell)xrTableRow0.Cells[1]).Text = this.valoresPatrimonio.pl.Value.ToString("n2");
            #endregion
        }

        private void TableRentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;
            //
            #region Limpa Valores da Tabela
            // Limpa somente as linhas 2,3
            for (int i = 2; i < xrTable.Rows.Count; i++) {
                int colunas = ((XRTableRow)xrTable.Rows[i]).Cells.Count;
                for (int j = 0; j < colunas; j++) {
                    ((XRTableCell)xrTable.Rows[i].Cells[j]).Text = "";
                }
            }
            #endregion

            /* Exibe Cabeçalho */
            #region Exibe Cabeçalho
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            XRTableRow xrTableRow1 = xrTable.Rows[1];
            //
            ((XRTableCell)xrTableRow0.Cells[0]).Text = Resources.SubReportCarteiraResumoProventos._Rentabilidade;
            ((XRTableCell)xrTableRow1.Cells[1]).Text = Resources.SubReportCarteiraResumoProventos._Dia;
            ((XRTableCell)xrTableRow1.Cells[2]).Text = Resources.SubReportCarteiraResumoProventos._Mes;
            ((XRTableCell)xrTableRow1.Cells[3]).Text = Resources.SubReportCarteiraResumoProventos._Ano;
            ((XRTableCell)xrTableRow1.Cells[4]).Text = Resources.SubReportCarteiraResumoProventos._6Meses;
            ((XRTableCell)xrTableRow1.Cells[5]).Text = Resources.SubReportCarteiraResumoProventos._12Meses;
            ((XRTableCell)xrTableRow1.Cells[6]).Text = Resources.SubReportCarteiraResumoProventos._Inicial;
            //((XRTableCell)xrTableRow1.Cells[5]).Text = Resources.SubReportCarteiraResumo._24Meses;
            //((XRTableCell)xrTableRow1.Cells[6]).Text = Resources.SubReportCarteiraResumo._36Meses;
            #endregion

            /*
             * Rentabilidade Cota
             * 1-(Dia) - 2-(Mês) - 3-(Ano) - 4-(6 Meses) - 5-(12 Meses) - 6-(Inicio)
             *
             * Rentabilidade Diferencial
             * 1-(Dia) - 2-(Mês) - 3-(Ano) - 4-(6 Meses) - 5-(12 Meses) - 6-(Inicio)
             * 
            */

            #region Calcula datas dos periodos
            DateTime? dataAnterior = Calendario.SubtraiDiaUtil(dataReferencia, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(dataReferencia, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(dataReferencia, -1);
            DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 6);
            DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 12);

            if (dataAnterior < this.dataInicioCliente) {
                dataAnterior = null;
            }
            if (dataMes < this.dataInicioCliente) {
                dataMes = null;
            }
            if (dataAno < this.dataInicioCliente) {
                dataAno = null;
            }

            if (data6Meses < this.dataInicioCliente) {
                data6Meses = null;
            }

            if (data12Meses < this.dataInicioCliente) {
                data12Meses = null;
            }
            #endregion

            List<decimal?> rentabilidadeCota = new List<decimal?>();
            List<decimal?> rentabilidadeIndice = new List<decimal?>();
            List<decimal?> rentabilidadeDiferencial = new List<decimal?>();
            //
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetDataInicio(this.dataInicioCliente);

            calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
            //                                                                       
            #region Calcula Rentabilidade Cota
            calculoMedida.SetIdCarteira(this.idCarteira);

            #region rentabilidadeDia
            decimal? rentabilidadeCotaDia = null;
            if (dataAnterior != null)
            {
                try
                {
                    rentabilidadeCotaDia = calculoMedida.CalculaRetornoDiaProventos(this.dataReferencia);
                    rentabilidadeCotaDia = rentabilidadeCotaDia / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeMes
            decimal? rentabilidadeCotaMes = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeCotaMes = calculoMedida.CalculaRetornoMesProventos(this.dataReferencia);
                    rentabilidadeCotaMes = rentabilidadeCotaMes / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeCotaAno = null;
            if (dataAno != null)
            {                
                try
                {
                    rentabilidadeCotaAno = calculoMedida.CalculaRetornoAnoProventos(this.dataReferencia);
                    rentabilidadeCotaAno = rentabilidadeCotaAno / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeCota6Meses = null;
            if (data6Meses != null) {
                try {
                    rentabilidadeCota6Meses = calculoMedida.CalculaRetornoPeriodoMesProventos(this.dataReferencia, 6);
                    rentabilidadeCota6Meses = rentabilidadeCota6Meses / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeCota12Meses = null;
            if (data12Meses != null)
            {                
                try
                {
                    rentabilidadeCota12Meses = calculoMedida.CalculaRetornoPeriodoMesProventos(this.dataReferencia, 12);
                    rentabilidadeCota12Meses = rentabilidadeCota12Meses / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeInicio
            decimal? rentabilidadeCotaInicio = null;
            try
            {
                rentabilidadeCotaInicio = calculoMedida.CalculaRetornoProventos(this.dataInicioCliente, this.dataReferencia);
                rentabilidadeCotaInicio = rentabilidadeCotaInicio / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }            
            #endregion

            rentabilidadeCota.Add(rentabilidadeCotaDia);
            rentabilidadeCota.Add(rentabilidadeCotaMes);
            rentabilidadeCota.Add(rentabilidadeCotaAno);
            rentabilidadeCota.Add(rentabilidadeCota6Meses);
            rentabilidadeCota.Add(rentabilidadeCota12Meses);
            rentabilidadeCota.Add(rentabilidadeCotaInicio);

            // Copia o valor na variável Global rentabilidadeCotaTable
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[0]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[1]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[2]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[3]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[4]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[5]);

            #endregion
          
            /* Exibe Rentabilidade Cota */            
            #region Exibe Rentabilidade Cota
            XRTableRow xrTableRow2 = xrTable.Rows[2];
            //
            ((XRTableCell)xrTableRow2.Cells[0]).Text = Resources.SubReportCarteiraResumo._Carteira;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[1], rentabilidadeCota[0], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[2], rentabilidadeCota[1], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[3], rentabilidadeCota[2], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[4], rentabilidadeCota[3], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[5], rentabilidadeCota[4], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[6], rentabilidadeCota[5], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            #endregion
                                
            #region Calcula Rentabilidade Indice
            // Pega o Indice
            int idIndice = Convert.ToInt16(this.carteira.IdIndiceBenchmark);
            calculoMedida.SetIdIndice(idIndice);

            #region rentabilidadeDia
            decimal? rentabilidadeIndiceDia = null;
            if (dataAnterior != null)
            {
                try
                {
                    rentabilidadeIndiceDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                    rentabilidadeIndiceDia = rentabilidadeIndiceDia / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeMes
            decimal? rentabilidadeIndiceMes = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeIndiceMes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                    rentabilidadeIndiceMes = rentabilidadeIndiceMes / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeIndiceAno = null;
            if (dataAno != null)
            {
                try
                {
                    rentabilidadeIndiceAno = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                    rentabilidadeIndiceAno = rentabilidadeIndiceAno / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeIndice6Meses = null;
            if (data6Meses != null) {
                try {
                    rentabilidadeIndice6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                    rentabilidadeIndice6Meses = rentabilidadeIndice6Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeIndice12Meses = null;
            if (data12Meses != null)
            {
                try
                {
                    rentabilidadeIndice12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                    rentabilidadeIndice12Meses = rentabilidadeIndice12Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeInicio
            decimal? rentabilidadeIndiceInicio = null;
            try
            {
                rentabilidadeIndiceInicio = calculoMedida.CalculaRetornoIndice(this.dataInicioCliente, this.dataReferencia);
                rentabilidadeIndiceInicio = rentabilidadeIndiceInicio / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }            
            #endregion
                        
            rentabilidadeIndice.Add(rentabilidadeIndiceDia);
            rentabilidadeIndice.Add(rentabilidadeIndiceMes);
            rentabilidadeIndice.Add(rentabilidadeIndiceAno);
            rentabilidadeIndice.Add(rentabilidadeIndice6Meses);
            rentabilidadeIndice.Add(rentabilidadeIndice12Meses);
            rentabilidadeIndice.Add(rentabilidadeIndiceInicio);
            #endregion
                        
            #region Calcula Rentabilidade Diferencial
            //
            decimal? rentabilidadeDiaDiferencial;
            decimal? rentabilidadeMesDiferencial;
            decimal? rentabilidadeAnoDiferencial;
            decimal? rentabilidade6MesesDiferencial;
            decimal? rentabilidade12MesesDiferencial;
            decimal? rentabilidadeInicioDiferencial;
            
            int? tipoIndice = null; 
            if (this.carteira.UpToIndiceByIdIndiceBenchmark.es.HasData) {
                tipoIndice = this.carteira.UpToIndiceByIdIndiceBenchmark.Tipo.Value;
            }
            if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Decimal) {
                #region TipoIndice = Decimal
                rentabilidadeDiaDiferencial = rentabilidadeCota[0].HasValue && rentabilidadeIndice[0].HasValue
                                            ? rentabilidadeCota[0] - rentabilidadeIndice[0]
                                            : null;

                rentabilidadeMesDiferencial = rentabilidadeCota[1].HasValue && rentabilidadeIndice[1].HasValue
                                            ? rentabilidadeCota[1] - rentabilidadeIndice[1]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCota[2].HasValue && rentabilidadeIndice[2].HasValue
                                            ? rentabilidadeCota[2] - rentabilidadeIndice[2]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCota[3].HasValue && rentabilidadeIndice[3].HasValue
                                            ? rentabilidadeCota[3] - rentabilidadeIndice[3]
                                            : null;


                rentabilidade12MesesDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue
                                            ? rentabilidadeCota[4] - rentabilidadeIndice[4]
                                            : null;

                //rentabilidade24MesesDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue
                //                            ? rentabilidadeCota[4] - rentabilidadeIndice[4]
                //                            : null;

                //rentabilidade36MesesDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue
                //                            ? rentabilidadeCota[5] - rentabilidadeIndice[5]
                //                            : null;

                rentabilidadeInicioDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue
                                            ? rentabilidadeCota[5] - rentabilidadeIndice[5]
                                            : null;
                #endregion
            }
            else {
                #region TipoIndice = Percentual
                rentabilidadeDiaDiferencial = rentabilidadeCota[0].HasValue && rentabilidadeIndice[0].HasValue && rentabilidadeIndice[0]!= 0
                                            ? rentabilidadeCota[0] / rentabilidadeIndice[0]
                                            : null;

                rentabilidadeMesDiferencial = rentabilidadeCota[1].HasValue && rentabilidadeIndice[1].HasValue && rentabilidadeIndice[1]!= 0
                                            ? rentabilidadeCota[1] / rentabilidadeIndice[1]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCota[2].HasValue && rentabilidadeIndice[2].HasValue && rentabilidadeIndice[2]!= 0
                                            ? rentabilidadeCota[2] / rentabilidadeIndice[2]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCota[3].HasValue && rentabilidadeIndice[3].HasValue && rentabilidadeIndice[3] != 0
                            ? rentabilidadeCota[3] / rentabilidadeIndice[3]
                            : null;

                rentabilidade12MesesDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue && rentabilidadeIndice[4]!= 0
                                            ? rentabilidadeCota[4] / rentabilidadeIndice[4]
                                            : null;

                //rentabilidade24MesesDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue && rentabilidadeIndice[4]!= 0
                //                            ? rentabilidadeCota[4] / rentabilidadeIndice[4]
                //                            : null;

                //rentabilidade36MesesDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue && rentabilidadeIndice[5]!= 0
                //                            ? rentabilidadeCota[5] / rentabilidadeIndice[5]
                //                            : null;

                rentabilidadeInicioDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue && rentabilidadeIndice[5] != 0
                                            ? rentabilidadeCota[5] / rentabilidadeIndice[5]
                                            : null;
                #endregion
            }

            rentabilidadeDiferencial.Add(rentabilidadeDiaDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeMesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeAnoDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade6MesesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade12MesesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeInicioDiferencial);
            #endregion

            /* Exibe Rentabilidade Diferencial */
            #region Exibe Rentabilidade Diferencial
            XRTableRow xrTableRow3 = xrTable.Rows[3];

            string descricaoIndice = "";
            if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Decimal)
            {
                descricaoIndice = "(-) ";
            }
            else if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Percentual)
            {
                descricaoIndice = "(%) ";
            }

            if (this.carteira.UpToIndiceByIdIndiceBenchmark.es.HasData) 
            {
                descricaoIndice = descricaoIndice + this.carteira.UpToIndiceByIdIndiceBenchmark.Descricao;
            }


            // Se alguma rentabilidade Cota for nula rentabilidadeDiferencial também vai ser nula
            /* ----------------------------------------------------*/
            for (int i = 0; i < rentabilidadeDiferencial.Count; i++) {
                if (!rentabilidadeCota[i].HasValue) {
                    rentabilidadeDiferencial[i] = null;
                }
            }
            /* ----------------------------------------------------*/
            ((XRTableCell)xrTableRow3.Cells[0]).Text = descricaoIndice;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[1], rentabilidadeDiferencial[0], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[2], rentabilidadeDiferencial[1], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[3], rentabilidadeDiferencial[2], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[4], rentabilidadeDiferencial[3], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[5], rentabilidadeDiferencial[4], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow3.Cells[6], rentabilidadeDiferencial[5], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            #endregion
        }

        private void TableDetailRentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;
            #region Zera os campos da Table
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            ((XRTableCell)xrTableRow0.Cells[0]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[1]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[2]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[3]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[4]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[5]).Text = "";
            ((XRTableCell)xrTableRow0.Cells[6]).Text = "";
            //((XRTableCell)xrTableRow0.Cells[7]).Text = "";
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                /*
                 * Rentabilidades
                 * 1-(Dia) - 2-(Mês) - 3-(Ano) - 4-(6 Meses) - 5-(12 Meses) - 6-(Inicio)
                 * 
                */
                List<decimal?> rentabilidadeIndice = new List<decimal?>();
                //
                CalculoMedida calculoMedida = new CalculoMedida();
                calculoMedida.SetDataInicio(this.dataInicioCliente);

                calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
                //                        
                int idIndice = Convert.ToInt16(this.GetCurrentColumnValue(ListaBenchmarkMetadata.ColumnNames.IdIndice));
                calculoMedida.SetIdIndice(idIndice);
                
                #region Calcula Rentabilidade

                #region rentabilidadeDia
                    decimal? rentabilidadeDia = null;
                    try {
                        rentabilidadeDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
                        rentabilidadeDia = rentabilidadeDia / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                    #endregion

                #region rentabilidadeMes
                decimal? rentabilidadeMes = null;
                try {
                    rentabilidadeMes = calculoMedida.CalculaRetornoMesIndice(this.dataReferencia);
                    rentabilidadeMes = rentabilidadeMes / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidadeAno
                decimal? rentabilidadeAno = null;
                try {
                    rentabilidadeAno = calculoMedida.CalculaRetornoAnoIndice(this.dataReferencia);
                    rentabilidadeAno = rentabilidadeAno / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidade6Meses
                decimal? rentabilidade6Meses = null;
                try {
                    rentabilidade6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 6);
                    rentabilidade6Meses = rentabilidade6Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                #region rentabilidade12Meses
                decimal? rentabilidade12Meses = null;
                try {
                    rentabilidade12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 12);
                    rentabilidade12Meses = rentabilidade12Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion
                
                //#region rentabilidade24Meses
                //decimal? rentabilidade24Meses = null;
                //try {
                //    rentabilidade24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 24);
                //    rentabilidade24Meses = rentabilidade24Meses / 100;
                //}
                //catch (CotacaoIndiceNaoCadastradoException ex) {
                //    //Console.Write(ex.Message);                
                //}
                //#endregion

                //#region rentabilidade36Meses
                //decimal? rentabilidade36Meses = null;
                //try {
                //    rentabilidade36Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.dataReferencia, 36);
                //    rentabilidade36Meses = rentabilidade36Meses / 100;
                //}
                //catch (CotacaoIndiceNaoCadastradoException) { }
                //#endregion

                #region rentabilidadeInicio
                decimal? rentabilidadeInicio = null;
                try
                {
                    DateTime dataInicioCota = this.carteira.DataInicioCota.Value;
                    rentabilidadeInicio = calculoMedida.CalculaRetornoIndice(dataInicioCota, this.dataReferencia);
                    rentabilidadeInicio = rentabilidadeInicio / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
                #endregion

                rentabilidadeIndice.Add(rentabilidadeDia);
                rentabilidadeIndice.Add(rentabilidadeMes);
                rentabilidadeIndice.Add(rentabilidadeAno);
                rentabilidadeIndice.Add(rentabilidade6Meses);
                rentabilidadeIndice.Add(rentabilidade12Meses);
                rentabilidadeIndice.Add(rentabilidadeInicio);
               
                #endregion                        

                /* Exibe Rentabilidade */
                #region Exibe Rentabilidade

                Indice indice = new Indice();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(indice.Query.Descricao);
                indice.LoadByPrimaryKey(campos, (short)idIndice);

                // Se alguma rentabilidade Cota for nula rentabilidadeIndice também vai ser nula
                /* ----------------------------------------------------*/
                for (int i = 0; i < this.rentabilidadeCotaTable.Count; i++) {
                    if (!rentabilidadeCotaTable[i].HasValue) {
                        rentabilidadeIndice[i] = null;
                    }
                }
                /* ----------------------------------------------------*/

                ((XRTableCell)xrTableRow0.Cells[0]).Text = !String.IsNullOrEmpty(indice.Descricao) ? indice.Descricao : "";                
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[1], rentabilidadeIndice[0], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[2], rentabilidadeIndice[1], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[3], rentabilidadeIndice[2], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[4], rentabilidadeIndice[3], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[5], rentabilidadeIndice[4], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow0.Cells[6], rentabilidadeIndice[5], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
                #endregion
            }
            else {
                // Desaparecer com o detail para não ocupar espaço                
                //this.xrTable6.Visible = false;                
            }
        }
        
        #endregion                                   
    }
}