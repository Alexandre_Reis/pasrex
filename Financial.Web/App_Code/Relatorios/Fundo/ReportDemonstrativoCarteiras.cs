﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Investidor.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Fundo.Exceptions;
using Financial.InvestidorCotista;
using Financial.Investidor;
using System.Collections.Generic;
using System.Text;
using Financial.Util;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportDemonstrativoCarteiras
    /// </summary>
    public class ReportDemonstrativoCarteiras : XtraReport {
        private DateTime dataReferencia;

        private string login;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        // Armazena a carteira de cada linha do relatorio
        private Carteira carteira = new Carteira();
        private int tipoClientePorGrupo = 0;
        //private int idCategoriaPorGrupo = 0;
        private int numeroLinhasDataTable;

        private DateTime dataInicioCliente;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell34;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell7;
        private GroupHeaderBand GroupHeader2;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell22;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell30;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTableCell xrTableCell10;
        private XRSubreport xrSubreport3;
        private XRSubreport xrSubreport2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportDemonstrativoCarteiras(DateTime dataReferencia, string login) {
            this.dataReferencia = dataReferencia;
            this.login = login;
            //
            this.InitializeComponent();
            this.PersonalInitialize();
           
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            //this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2450);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.GroupHeader1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.GroupFooter1.Visible = false;
                this.ReportFooter.Visible = false;

            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();                            
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do Resource
            this.xrTableCell55.Text = Resources.ReportDemonstrativoCarteiras._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportDemonstrativoCarteiras._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportDemonstrativoCarteiras._DataPosicao;
            this.xrTableCell25.Text = Resources.ReportDemonstrativoCarteiras._Carteira;
            this.xrTableCell28.Text = Resources.ReportDemonstrativoCarteiras._Caixa;
            this.xrTableCell26.Text = Resources.ReportDemonstrativoCarteiras._ValorPL;
            this.xrTableCell5.Text = Resources.ReportDemonstrativoCarteiras._ValorCota;
            this.xrTableCell3.Text = Resources.ReportDemonstrativoCarteiras._QtdCotas;
            this.xrTableCell27.Text = Resources.ReportDemonstrativoCarteiras._Cotistas;
            this.xrTableCell34.Text = Resources.ReportDemonstrativoCarteiras._Dia;
            this.xrTableCell2.Text = Resources.ReportDemonstrativoCarteiras._Mes;
            this.xrTableCell9.Text = Resources.ReportDemonstrativoCarteiras._Ano;            
            #endregion
        }
        
        private DataTable FillDados() {
            esUtility u = new esUtility();
            string tipoControle = (int)TipoControleCliente.CarteiraRentabil + ", " + (int)TipoControleCliente.Completo;

            esParameters esParams = new esParameters();
            esParams.Add("DataReferencia", this.dataReferencia);

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine("SELECT C.IdCarteira, ");
            sqlText.AppendLine("       C.Apelido As ApelidoCarteira, ");            
            sqlText.AppendLine("       C.Nome AS Nome, ");
            sqlText.AppendLine("       T.Descricao as Descricao1, ");            
            sqlText.AppendLine("       H.CotaAbertura, ");
            sqlText.AppendLine("       H.CotaFechamento, ");
            sqlText.AppendLine("       H.PLAbertura, ");
            sqlText.AppendLine("       H.PLFechamento, ");
            sqlText.AppendLine("       H.QuantidadeFechamento, ");
            sqlText.AppendLine("       CatFundo.Descricao as Descricao, ");
            sqlText.AppendLine("       CatFundo.IdCategoria as IdCategoria, ");
            sqlText.AppendLine("       T.IdTipo AS TipoCliente ");
            sqlText.AppendLine(" FROM  [HistoricoCota] H, ");
            sqlText.AppendLine("       [Carteira] C, ");
            sqlText.AppendLine("       [CategoriaFundo] CatFundo, ");
            sqlText.AppendLine("       [Cliente] E, ");
            sqlText.AppendLine("       [TipoCliente] T, ");
            sqlText.AppendLine("       [PermissaoCliente] P, ");
            sqlText.AppendLine("       [Usuario] U ");
            sqlText.AppendLine(" WHERE H.IdCarteira = C.IdCarteira ");
            sqlText.AppendLine("       AND C.IdCarteira = E.IdCliente ");
            sqlText.AppendLine("       AND E.IdTipo = T.IdTipo ");
            sqlText.AppendLine("       AND C.IdCategoria = CatFundo.IdCategoria ");
            sqlText.AppendLine("       AND E.TipoControle In (" + tipoControle + ") ");
            sqlText.AppendLine("       AND E.StatusAtivo = " + (int)StatusAtivoCliente.Ativo);
            sqlText.AppendLine("       AND H.Data = @DataReferencia ");
            sqlText.AppendLine("       AND E.idCliente = C.IdCarteira ");
            sqlText.AppendLine("       AND P.idCliente = E.idCliente ");
            sqlText.AppendLine("       AND P.IdUsuario = U.IdUsuario ");
            sqlText.AppendLine("       AND U.login = '" + this.login + "'");
            sqlText.AppendLine("       ORDER BY E.IdTipo, C.Apelido ");
            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/ReportDemonstrativoCarteiras.xml");

            return dt;
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportDemonstrativoCarteiras.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportDemonstrativoCarteiras.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 50F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("ApelidoCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(101F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell11,
            this.xrTableCell17,
            this.xrTableCell16});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.CanGrow = false;
            this.xrTableCell21.CanShrink = true;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "Carteira";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.29591836734693877;
            this.xrTableCell21.WordWrap = false;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "Caixa";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.11877551020408163;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CaixaBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "Patrimonio";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.1036734693877551;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PatrimonioBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Cota";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.12081632653061225;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotaBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuantidadeFechamento", "{0:n8}")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.12122448979591836;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "Cotistas";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.051836734693877548;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotistasBeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "RentabilidadeDia";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.060408163265306125;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeDiaBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "RentabilidadeMes";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.060408163265306125;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeMesBeforePrint);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "RentabilidadeAno";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.066938775510204079;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadeAnoBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 85F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(368F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(214F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(153F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(466F, 43F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.45493562231759654;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.54506437768240346;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable7,
            this.xrTable10,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeader.HeightF = 238.8333F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(40.14585F, 183.8333F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(683F, 64F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2434F, 87F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(114F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 192F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell2,
            this.xrTableCell9});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Carteira";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.29591836734693877;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#Caixa";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.11877551020408163;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#ValorPL";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.1036734693877551;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#ValorCota";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.12081632653061225;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#QtdCotas";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.12122448979591836;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "#Cotistas";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.051836734693877548;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "#Dia";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.060408163265306125;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "#Mes";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.060408163265306125;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Ano";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.066938775510204079;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(826F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1714F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell10.Weight = 0.16044340723453909;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.83955659276546091;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 98F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(680F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCategoria", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeader1BeforePrint);
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.LightGray;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Descricao")});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 64F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalPorGrupoBeforePrint);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell20,
            this.xrTableCell8,
            this.xrTableCell22,
            this.xrTableCell36,
            this.xrTableCell35,
            this.xrTableCell37});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "TotalCarteirasPorGrupo";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.29591836734693877;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "TotalCaixaPorGrupo";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.11877551020408163;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "TotalPatrimonioPorGrupo";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.1036734693877551;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 0.12122448979591836;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell36.Weight = 0.097142857142857142;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "TotalCotistas";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.075510204081632656;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell37.Weight = 0.18775510204081633;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("ApelidoCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderIdCarteiraBeforePrint);
            this.GroupHeader2.AfterPrint += new System.EventHandler(this.GroupHeaderIdCarteiraAfterPrint);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 56F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell29,
            this.xrTableCell30});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "TotalCarteiras";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.29591836734693877;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "TotalCaixa";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.11877551020408163;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Text = "TotalPatrimonio";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.1036734693877551;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell30.Weight = 0.48163265306122449;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportDemonstrativoCarteiras
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader2,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 90, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportDemonstrativoCarteiras.ResourceManager;

        }

        // Controla a Quebra de Pagina do Grupo Descrição
        private string descricaoGrupo = "";

        // Colunas da Tabela        
        private string IdCarteiraColumn = "IdCarteira";
        //private string NomeCarteiraColumn = "Nome";
        private string ApelidoCarteiraColumn = "ApelidoCarteira";
        private string PLAberturaColumn = "PLAbertura";
        private string PLFechamentoColumn = "PLFechamento";
        private string CotaAberturaColumn = "CotaAbertura";
        private string CotaFechamentoColumn = "CotaFechamento";
        private string TipoClienteColumn = "TipoCliente";
        //private string IdCategoriaColumn = "idCategoria";
        //  

        private int totalCarteirasPorGrupo = 0;
        private int totalCarteiras = 0;
        //
        private decimal totalCaixaPorGrupo = 0;
        private decimal totalPatrimonioPorGrupo = 0;
        //        
        private List<decimal> totalCaixa = new List<decimal>();        
        private List<decimal> totalPatrimonio = new List<decimal>();
        private List<int> totalCotistas = new List<int>();

        private enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoCotista;
        
        //
        #region Funções Internas do Relatorio
        //
        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.dataReferencia.ToString("d");
        }

        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                XRTableCell nomeCarteiraXRTableCell = sender as XRTableCell;
                //
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                string nomeCarteira = (string)this.GetCurrentColumnValue(this.ApelidoCarteiraColumn);
                string carteira = idCarteira + " - " + nomeCarteira;
                //
                //if (carteira.Length >= 30) {
                //    carteira = carteira.Substring(0, 29);
                //}
                nomeCarteiraXRTableCell.Text = carteira;    
            }
        }

        // Armazena o Tipo da Carteira de cada linha do relatorio
        private void GroupHeaderIdCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);
                //                
                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                campos.Add(carteira.Query.DataInicioCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                //
                int tipoCotaCarteira = carteira.TipoCota.Value;
                //
                this.dataInicioCliente = carteira.DataInicioCota.Value;
                //
                this.carteira = new Carteira();
                this.carteira.IdCarteira = idCarteira;
                this.carteira.TipoCota = Convert.ToByte(tipoCotaCarteira);
            }
        }
        
        // Soma Total Carteiras Por Grupo
        private void GroupHeaderIdCarteiraAfterPrint(object sender, EventArgs e) {
            this.totalCarteirasPorGrupo++;
            this.totalCarteiras++;
        }

        // Armazena o TipoCliente de cada Grupo do Report
        private void GroupHeader1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {

            // Só Zerar quando não for quebra de Pagina
            const string descricaoColumn = "Descricao";
            //
            string controleDescricaoGrupo = (string)this.GetCurrentColumnValue(descricaoColumn);

            if (controleDescricaoGrupo != this.descricaoGrupo) {
                // Salva a Descrição do Grupo Atual
                this.descricaoGrupo = controleDescricaoGrupo;
                
                #region Zera Totais Por Grupo
                // Zera o total de Carteiras no Grupo
                this.totalCarteirasPorGrupo = 0;

                // Zera o total Caixa no Grupo
                this.totalCaixaPorGrupo = 0;
                //
                // Zera o total Patrimonio no Grupo
                this.totalPatrimonioPorGrupo = 0;
                //
                #endregion
            }

            if (this.numeroLinhasDataTable != 0) {
                this.tipoClientePorGrupo = (int)this.GetCurrentColumnValue(this.TipoClienteColumn);
                //this.idCategoriaPorGrupo = (int)this.GetCurrentColumnValue(this.IdCategoriaColumn);                
            }
        }

        private void CaixaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                XRTableCell caixaXRTableCell = sender as XRTableCell;
                //
                /*int idCarteira = (int)this.GetCurrentColumnValue(this.IdCarteiraColumn);               
                Carteira carteira = new Carteira();
                //
                int tipoCotaCarteira = carteira.BuscaTipoCota(idCarteira);
                carteira.TipoCota = tipoCotaCarteira; */
                //
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.carteira.IdCarteira.Value, this.dataReferencia);
                //                
                decimal caixa = this.carteira.IsTipoCotaAbertura()
                    ? saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0
                    : saldoCaixa.SaldoFechamento.HasValue ? saldoCaixa.SaldoFechamento.Value : 0;

                ReportBase.ConfiguraSinalNegativo(caixaXRTableCell, caixa, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                // Totaliza o Caixa
                this.totalCaixaPorGrupo += caixa;
                this.totalCaixa.Add(caixa);
            }
        }

        private void PatrimonioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                XRTableCell patrimonioXRTableCell = sender as XRTableCell;
                
                decimal patrimonio = this.carteira.IsTipoCotaAbertura()
                                        ? (decimal)this.GetCurrentColumnValue(this.PLAberturaColumn)
                                        : (decimal)this.GetCurrentColumnValue(this.PLFechamentoColumn);

                //
                patrimonioXRTableCell.Text = patrimonio.ToString("n2");
                // Totaliza o Patrimonio
                this.totalPatrimonioPorGrupo += patrimonio;
                this.totalPatrimonio.Add(patrimonio);
            }
        }

        private void CotaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                
                XRTableCell cotaXRTableCell = sender as XRTableCell;
                
                decimal cota = this.carteira.IsTipoCotaAbertura()
                                            ? (decimal)this.GetCurrentColumnValue(this.CotaAberturaColumn)
                                            : (decimal)this.GetCurrentColumnValue(this.CotaFechamentoColumn);
                cotaXRTableCell.Text = cota.ToString("n8");
                    
            }
        }

        private void CotistasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell cotistasXRTableCell = sender as XRTableCell;            
            //
            Cliente cliente = new Cliente();
            cliente.IdTipo = this.tipoClientePorGrupo;
            if (!cliente.IsTipoClienteFundo() && !cliente.IsTipoClienteClube()) {
                cotistasXRTableCell.Text = "";
            }
            else {
                // Define se faz Consulta PosicaoCotista ou PosicaoCotistaHistorico
                // Verifica A dataDia Do Cliente
                cliente = new Cliente();
                this.tipoPesquisa = cliente.IsClienteNaData(this.carteira.IdCarteira.Value, this.dataReferencia)
                                        ? TipoPesquisa.PosicaoCotista
                                        : TipoPesquisa.PosicaoCotistaHistorico;

                int cotistasPorGrupo = 0;
                if (this.tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    cotistasPorGrupo = posicaoCotista.RetornaCountNumeroCotistas(this.carteira.IdCarteira.Value);
                
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                    cotistasPorGrupo = posicaoCotistaHistorico.RetornaCountNumeroCotistas(this.carteira.IdCarteira.Value, this.dataReferencia);
                }
                                              
                //
                cotistasXRTableCell.Text = cotistasPorGrupo.ToString();
                this.totalCotistas.Add(cotistasPorGrupo);
            }                      
        }

        private void RentabilidadeDiaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {                
                XRTableCell rentabilidadeDiaXRTableCell = sender as XRTableCell;
                //            
                CalculoMedida calculoMedida = new CalculoMedida(this.carteira.IdCarteira.Value);
                calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
                calculoMedida.SetDataInicio(this.dataInicioCliente);
                               
                decimal rentabilidadeDia = 0;
                try {
                    rentabilidadeDia = calculoMedida.CalculaRetornoDia(this.dataReferencia);
                    rentabilidadeDia = rentabilidadeDia / 100;
                    //
                    ReportBase.ConfiguraSinalNegativo(rentabilidadeDiaXRTableCell, rentabilidadeDia,
                                                        ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    rentabilidadeDiaXRTableCell.Text = " - ";
                }
                catch (Exception) {
                    rentabilidadeDiaXRTableCell.Text = " - ";
                }
            }
        }

        private void RentabilidadeMesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {                
                XRTableCell rentabilidadeMesXRTableCell = sender as XRTableCell;
                //
                CalculoMedida calculoMedida = new CalculoMedida(this.carteira.IdCarteira.Value);
                calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
                calculoMedida.SetDataInicio(this.dataInicioCliente);
                decimal rentabilidadeMes = 0;
                try {
                    rentabilidadeMes = calculoMedida.CalculaRetornoMes(this.dataReferencia);
                    rentabilidadeMes = rentabilidadeMes / 100;
                    //
                    ReportBase.ConfiguraSinalNegativo(rentabilidadeMesXRTableCell, rentabilidadeMes,
                                        ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    rentabilidadeMesXRTableCell.Text = " - ";
                }
                catch (Exception) {
                    rentabilidadeMesXRTableCell.Text = " - ";
                }                 
            }
        }

        private void RentabilidadeAnoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {                
                XRTableCell rentabilidadeAnoXRTableCell = sender as XRTableCell;
                //
                CalculoMedida calculoMedida = new CalculoMedida(this.carteira.IdCarteira.Value);
                calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
                calculoMedida.SetDataInicio(this.dataInicioCliente);
                decimal rentabilidadeAno = 0;
                try {
                    rentabilidadeAno = calculoMedida.CalculaRetornoAno(this.dataReferencia);
                    rentabilidadeAno = rentabilidadeAno / 100;
                    //
                    ReportBase.ConfiguraSinalNegativo(rentabilidadeAnoXRTableCell, rentabilidadeAno,
                        ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    rentabilidadeAnoXRTableCell.Text = " - ";
                }
                catch (Exception) {
                    rentabilidadeAnoXRTableCell.Text = " - ";
                }
            }
        }
       
        #region Calcula e Exibe Totais
        private void TableTotalPorGrupoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            /* TotalCarteirasPorGrupo
               TotalCaixaPorGrupo
               TotalPatrimonioPorGrupo
               TotalCotistas              
             */
            string carteirasGrupo = Resources.ReportDemonstrativoCarteiras._NumeroCarteirasGrupo;

            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = carteirasGrupo + " " +this.totalCarteirasPorGrupo.ToString();
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[1], this.totalCaixaPorGrupo, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = this.totalPatrimonioPorGrupo.ToString("n2");

            // Total Cotistas
            #region Total Cotistas
            Cliente cliente = new Cliente();
            cliente.IdTipo = this.tipoClientePorGrupo;
            if (!cliente.IsTipoClienteFundo() && !cliente.IsTipoClienteClube()) {
                ((XRTableCell)summaryFinalRow0.Cells[5]).Text = "";
            }
            else {
                // Calcula Total Cotistas
                #region Calcula Total Cotistas
                decimal totalCotistaAux = 0;
                for (int i = 0; i < this.totalCotistas.Count; i++) {
                    totalCotistaAux += this.totalCotistas[i];
                }
                #endregion
                ((XRTableCell)summaryFinalRow0.Cells[5]).Text = totalCotistaAux.ToString();
            }
            #endregion
        }
        
        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            /* TotalCarteiras
               TotalCaixa
               TotalPatrimonio
             */
            
            // CalculaTotais
            #region CalculaTotais
            decimal totalCaixaAux = 0;
            decimal totalPatrimonioAux = 0;
            for (int i = 0; i < this.totalCaixa.Count; i++) {
                totalCaixaAux += this.totalCaixa[i];
            }
            for (int i = 0; i < this.totalPatrimonio.Count; i++) {
                totalPatrimonioAux += this.totalPatrimonio[i];
            }
            #endregion

            string totalCarteiras = Resources.ReportDemonstrativoCarteiras._TotalCarteiras;
            //
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = totalCarteiras + this.totalCarteiras.ToString();
            ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow0.Cells[1], totalCaixaAux, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ((XRTableCell)summaryFinalRow0.Cells[2]).Text = totalPatrimonioAux.ToString("n2");           
        }
        #endregion

        #endregion
    }
}