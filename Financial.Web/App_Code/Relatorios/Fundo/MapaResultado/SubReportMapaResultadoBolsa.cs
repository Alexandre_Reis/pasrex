﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Bolsa.Enums;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using System.Text;
using Financial.Util;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportMapaResultadoBolsa
    /// </summary>
    public class SubReportMapaResultadoBolsa : XtraReport {
        // Parametros para o relatorio
        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private OperacaoBolsaCollection operacaoBolsaCollectionCopia = new OperacaoBolsaCollection();

        //                    
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell13;
        private XRTableRow xrTableRow3;
        private XRTable xrTable3;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow1;
        private XRTable xrTable1;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow2;
        private XRTable xrTable2;        
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell26;
        private OperacaoBolsaCollection operacaoBolsaCollection1;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell6;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell28;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportMapaResultadoBolsa() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCliente, DateTime dataInicio, DateTime dataFim) {
            this.idCliente = idCliente;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;

            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
            
            #region Pega Campos do resource
            this.xrTableCell13.Text = Resources.SubReportMapaResultadoBolsa._Ativo;
            this.xrTableCell2.Text = Resources.SubReportMapaResultadoBolsa._QtdeCompras;
            this.xrTableCell1.Text = Resources.SubReportMapaResultadoBolsa._ValorCompras;
            this.xrTableCell14.Text = Resources.SubReportMapaResultadoBolsa._QtdeVendas;
            this.xrTableCell15.Text = Resources.SubReportMapaResultadoBolsa._ValorVendas;
            this.xrTableCell16.Text = Resources.SubReportMapaResultadoBolsa._ResultNormal;
            this.xrTableCell17.Text = Resources.SubReportMapaResultadoBolsa._ResultDayTrade;
            this.xrTableCell18.Text = Resources.SubReportMapaResultadoBolsa._Despesa;
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);

            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                this.xrTable3.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable6.Visible = false;
            }
        }

        /// <summary>
        /// Retorna a Table De Total do subRelatorio
        /// - Usada no Relatorio PAI
        /// </summary>
        /// <returns></returns>
        public XRTable getTableTotalBolsa() {
            return this.xrTable6;
        }

        private DataTable FillDados() {
            #region SQL
            this.operacaoBolsaCollection1.QueryReset();
            //

            /* OBS: No Start do relatorio é feito uma query copia igual a essa.
               Se essa query mudar a copia também deve mudar
             */
            this.operacaoBolsaCollection1.Query.es.Distinct = true;
            //
            this.operacaoBolsaCollection1.Query
                 .Select(this.operacaoBolsaCollection1.Query.CdAtivoBolsa,
                         this.operacaoBolsaCollection1.Query.TipoMercado)
                 .Where(this.operacaoBolsaCollection1.Query.IdCliente == this.idCliente &
                        this.operacaoBolsaCollection1.Query.Data.Between(this.dataInicio, this.dataFim) &
                          (
                            (this.operacaoBolsaCollection1.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade) &
                             this.operacaoBolsaCollection1.Query.Origem.In(OrigemOperacaoBolsa.Primaria))
                            |
                            (this.operacaoBolsaCollection1.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade) &
                             this.operacaoBolsaCollection1.Query.Origem.In(OrigemOperacaoBolsa.ExercicioOpcaoCompra, OrigemOperacaoBolsa.ExercicioOpcaoVenda))
                           )                              
                        )
                .OrderBy(this.operacaoBolsaCollection1.Query.TipoMercado.Descending, this.operacaoBolsaCollection1.Query.CdAtivoBolsa.Ascending);

            #endregion
            //
            return this.operacaoBolsaCollection1.Query.LoadDataTable();                 
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/MapaResultado/SubReportMapaResultadoBolsa.resx";
         */
        private void InitializeComponent() {
            string resourceFileName = "SubReportMapaResultadoBolsa.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.operacaoBolsaCollection1 = new Financial.Bolsa.OperacaoBolsaCollection();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 48F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1855F, 48F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell8,
            this.xrTableCell5,
            this.xrTableCell12,
            this.xrTableCell7,
            this.xrTableCell10,
            this.xrTableCell6});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CdAtivoBolsa")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.1045822102425876;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "QuantidadeCompras";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.1169811320754717;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeComprasBeforePrint);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "ValorCompras";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.1353099730458221;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorComprasBeforePrint);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "QuantidadeVendas";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 0.11428571428571428;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeVendasBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "ValorVendas";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.13692722371967656;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorVendasBeforePrint);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "ResultadoNormal";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.13692722371967656;
            this.xrTableCell7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ResultadoNormalBeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "ResutladoDaytrade";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.12560646900269543;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ResultadoDaytradeBeforePrint);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "Despesas";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.1293800539083558;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DespesasBeforePrint);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "#Despesa";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell18.Weight = 0.12991913746630729;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "#ResultDayTrade";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell17.Weight = 0.12560646900269543;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "#ResultNormal";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.13692722371967656;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "#ValorVendas";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.13692722371967656;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "#QtdeVendas";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell14.Weight = 0.11428571428571428;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell13.Multiline = true;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "#Ativo";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell13.Weight = 0.10512129380053908;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell2,
            this.xrTableCell1,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "#QtdeCompras";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.11428571428571428;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#ValorCompras";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell1.Weight = 0.13692722371967656;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.Transparent;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1855F, 40F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 45F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1855F, 40F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "TipoMercado";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell26.Weight = 1;
            this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoMercadoBeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.TipoMercado")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell4.Weight = 0.98804780876494025;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell27.Text = "-";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell27.Weight = 0.011952191235059761;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell4});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1757F, 55F);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell20.Weight = 1;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 56F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1750F, 15F);
            // 
            // operacaoBolsaCollection1
            // 
            this.operacaoBolsaCollection1.AllowDelete = true;
            this.operacaoBolsaCollection1.AllowEdit = true;
            this.operacaoBolsaCollection1.AllowNew = true;
            this.operacaoBolsaCollection1.EnableHierarchicalBinding = true;
            this.operacaoBolsaCollection1.Filter = "";
            this.operacaoBolsaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.operacaoBolsaCollection1.Sort = "";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 77F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 13F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1855F, 48F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell19,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell28});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "TotalAcoesOpcoesTermo";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.20862533692722371;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.01293800539083558;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "ValorCompras";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.1353099730458221;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.11428571428571428;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "ValorVendas";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.13692722371967656;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "ResultadoNormal";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.13692722371967656;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "ResutladoDaytrade";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell25.Weight = 0.12560646900269543;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "Despesas";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.1293800539083558;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TipoMercado", DevExpress.XtraReports.UI.XRColumnSortOrder.Descending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 42F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupTipoMercadoBeforePrint);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 64F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1855F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalGrupoBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.Weight = 0.1045822102425876;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.1169811320754717;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Text = "ValorCompras";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.1353099730458221;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.11428571428571428;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Text = "ValorVendas";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.13692722371967656;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "ResultadoNormal";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.13692722371967656;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "ResutladoDaytrade";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.12560646900269543;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "Despesas";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.1293800539083558;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportMapaResultadoBolsa
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.ReportFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.operacaoBolsaCollection1;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.Version = "10.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportMapaResultadoBolsa.ResourceManager;

        }
        
        /// <summary>
        /// Retorna os Valores Totais do SubReport de Bolsa
        /// </summary>
        public ValoresTotaisBolsa ValoresTotaisSubreportBolsa {
            get { return valoresTotalBolsa; }
        }

        #region Variaveis Internas do Relatorio
        //SUM - Total
        public class ValoresTotaisBolsa {
            public decimal valorCompraSum = 0.00M;
            public decimal valorVendaSum = 0.00M;
            public decimal resultadoNormalSum = 0.00M;
            public decimal resultadoDaytradeSum = 0.00M;
            public decimal taxasSum = 0.00M;
        }
        //
        private ValoresTotaisBolsa valoresTotalBolsa = new ValoresTotaisBolsa();

        //SUM - Totais Valor Por Mercado
        protected class ValoresTotaisGrupo {
            public decimal valorCompraPorGrupo = 0;
            public decimal valorVendaPorGrupo = 0;
            public decimal resultadoNormalPorGrupo = 0;
            public decimal resultadoDaytradePorGrupo = 0;
            public decimal taxasPorGrupo = 0;
        }
        //
        private ValoresTotaisGrupo valoresTotalPorGrupo = new ValoresTotaisGrupo();
        #endregion

        private void TipoMercadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoMercadoTableCell = sender as XRTableCell;
            tipoMercadoTableCell.Text = "";
            
            if (this.numeroLinhasDataTable != 0) {
                //string tipoMercado = Resources.SubReportMapaResultadoBolsa._MercadoBolsa;
                string tipoMercado = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.TipoMercado);
                string tipoMercadoTraduzido = TipoMercadoBolsa.str.RetornaTexto(tipoMercado);
                tipoMercadoTableCell.Text = tipoMercadoTraduzido;
            }
        }

        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Executado no start do relatorio
            this.operacaoBolsaCollectionCopia.QueryReset();
            //
            this.operacaoBolsaCollectionCopia.Query
                .Select(this.operacaoBolsaCollectionCopia.Query.IdOperacao,
                        this.operacaoBolsaCollectionCopia.Query.CdAtivoBolsa,
                        this.operacaoBolsaCollectionCopia.Query.TipoMercado,
                        this.operacaoBolsaCollectionCopia.Query.TipoOperacao,
                        this.operacaoBolsaCollectionCopia.Query.Origem,
                        this.operacaoBolsaCollectionCopia.Query.Data,
                        this.operacaoBolsaCollectionCopia.Query.ResultadoTermo,
                        this.operacaoBolsaCollectionCopia.Query.IdCliente,
                        this.operacaoBolsaCollectionCopia.Query.Quantidade,
                        this.operacaoBolsaCollectionCopia.Query.Valor,
                        this.operacaoBolsaCollectionCopia.Query.PULiquido,
                        this.operacaoBolsaCollectionCopia.Query.ValorLiquido.As("ResultadoDaytrade"),
                        (this.operacaoBolsaCollectionCopia.Query.ResultadoRealizado + this.operacaoBolsaCollectionCopia.Query.ResultadoExercicio + this.operacaoBolsaCollectionCopia.Query.ResultadoTermo).As("Resultado"),
                        (this.operacaoBolsaCollectionCopia.Query.Corretagem + this.operacaoBolsaCollectionCopia.Query.Emolumento + this.operacaoBolsaCollectionCopia.Query.LiquidacaoCBLC + this.operacaoBolsaCollectionCopia.Query.RegistroBolsa + this.operacaoBolsaCollectionCopia.Query.RegistroCBLC).As("Taxa")
                 )
                 .Where(this.operacaoBolsaCollectionCopia.Query.IdCliente == this.idCliente &
                        this.operacaoBolsaCollectionCopia.Query.Data.Between(this.dataInicio, this.dataFim) &
                        (
                            (this.operacaoBolsaCollection1.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade) &
                             this.operacaoBolsaCollection1.Query.Origem.In(OrigemOperacaoBolsa.Primaria))
                            |
                            (this.operacaoBolsaCollection1.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade) &
                             this.operacaoBolsaCollection1.Query.Origem.In(OrigemOperacaoBolsa.ExercicioOpcaoCompra, OrigemOperacaoBolsa.ExercicioOpcaoVenda))
                           )    
                        )
                 .OrderBy(this.operacaoBolsaCollectionCopia.Query.TipoMercado.Descending, this.operacaoBolsaCollectionCopia.Query.CdAtivoBolsa.Ascending);

            this.operacaoBolsaCollectionCopia.Query.Load();

            //Faz um tratamento de ajuste no resultadoDayTrade para os casos de ExercicioCompra e ExercicioVenda
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollectionCopia)
            {
                if ((operacaoBolsa.Origem.Value == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                     operacaoBolsa.Origem.Value == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                    &&
                    (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade ||
                     operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade))
                {
                    decimal quantidade = operacaoBolsa.Quantidade.Value;
                    decimal puLiquido = operacaoBolsa.PULiquido.Value;
                    string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                    decimal fator = 1;
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, operacaoBolsa.Data.Value))
                    {
                        fator = fatorCotacaoBolsa.Fator.Value;
                    }

                    decimal novoValorLiquido = Utilitario.Truncate(quantidade * puLiquido / fator, 2);
                    operacaoBolsa.SetColumn("ResultadoDaytrade", novoValorLiquido);
                }
            }            
        }

        private void QuantidadeComprasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell quantidadeComprasTableCell = sender as XRTableCell;
            quantidadeComprasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal quantidadeCompras = 0.00M;
                //
                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND (TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.CompraDaytrade + "'" +")" );
                               
                //this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'" + " AND TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'";
                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                //
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    quantidadeCompras += operacaoBolsaCollectionCopia[i].Quantidade.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                quantidadeComprasTableCell.Text = quantidadeCompras.ToString("n2");
            }
        }

        private void ValorComprasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorComprasTableCell = sender as XRTableCell;
            valorComprasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal valorCompras = 0.00M;

                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND (TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.CompraDaytrade + "'" + ")");

                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    valorCompras += operacaoBolsaCollectionCopia[i].Valor.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                valorCompras = valorCompras * -1;
                
                // Soma Por Grupo
                this.valoresTotalPorGrupo.valorCompraPorGrupo += valorCompras;
                
                // Soma Total
                this.valoresTotalBolsa.valorCompraSum += valorCompras;
                //
                ReportBase.ConfiguraSinalNegativo(valorComprasTableCell, valorCompras);
            }
        }

        private void QuantidadeVendasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell quantidadeVendasTableCell = sender as XRTableCell;
            quantidadeVendasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal quantidadeVendas = 0.00M;

                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND (TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.VendaDaytrade + "'" + ")");

                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    quantidadeVendas += operacaoBolsaCollectionCopia[i].Quantidade.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                ReportBase.ConfiguraSinalNegativo(quantidadeVendasTableCell, quantidadeVendas);
            }
        }

        private void ValorVendasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorVendasTableCell = sender as XRTableCell;
            valorVendasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal valorVendas = 0.00M;
                //
                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND (TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.VendaDaytrade + "'" + ")");

                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    valorVendas += operacaoBolsaCollectionCopia[i].Valor.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                // Soma Por Grupo
                this.valoresTotalPorGrupo.valorVendaPorGrupo += valorVendas;

                // Soma Total
                this.valoresTotalBolsa.valorVendaSum += valorVendas;

                ReportBase.ConfiguraSinalNegativo(valorVendasTableCell, valorVendas);
            }
        }

        private void ResultadoNormalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell resultadoNormalTableCell = sender as XRTableCell;
            resultadoNormalTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal resultadoNormal = 0.00M;

                this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'" + " AND (TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.Venda + "') ";
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    resultadoNormal += (decimal)operacaoBolsaCollectionCopia[i].GetColumn("Resultado");
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                // Soma Por Grupo
                this.valoresTotalPorGrupo.resultadoNormalPorGrupo += resultadoNormal;

                // Soma Total
                this.valoresTotalBolsa.resultadoNormalSum += resultadoNormal;

                ReportBase.ConfiguraSinalNegativo(resultadoNormalTableCell, resultadoNormal);
            }
        }

        private void ResultadoDaytradeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell resultadoDaytradeTableCell = sender as XRTableCell;
            resultadoDaytradeTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal resultadoDaytrade = 0.00M;

                this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'" + " AND (TipoOperacao = '" + TipoOperacaoBolsa.CompraDaytrade + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.VendaDaytrade + "') ";
                //
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    string tipoOperacao = (string)operacaoBolsaCollectionCopia[i].TipoOperacao;
                    decimal valor = tipoOperacao == TipoOperacaoBolsa.CompraDaytrade 
                                    ? (decimal)operacaoBolsaCollectionCopia[i].GetColumn("ResultadoDayTrade") * -1
                                    : (decimal)operacaoBolsaCollectionCopia[i].GetColumn("ResultadoDayTrade");
                                               
                    resultadoDaytrade += valor;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                // Soma Por Grupo
                this.valoresTotalPorGrupo.resultadoDaytradePorGrupo += resultadoDaytrade;

                // Soma Total
                this.valoresTotalBolsa.resultadoDaytradeSum += resultadoDaytrade;

                ReportBase.ConfiguraSinalNegativo(resultadoDaytradeTableCell, resultadoDaytrade);
            }
        }

        private void DespesasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell taxasTableCell = sender as XRTableCell;
            taxasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal taxas = 0.00M;

                this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'";
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    taxas += (decimal)operacaoBolsaCollectionCopia[i].GetColumn("Taxa");
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                taxas = taxas * -1;
                
                // Soma Por Grupo
                this.valoresTotalPorGrupo.taxasPorGrupo += taxas;

                // Soma Total
                this.valoresTotalBolsa.taxasSum += taxas;

                ReportBase.ConfiguraSinalNegativo(taxasTableCell, taxas);
            }
        }

        private void GroupTipoMercadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Zera os totais por Grupo
            this.valoresTotalPorGrupo.valorCompraPorGrupo = 0;
            this.valoresTotalPorGrupo.valorVendaPorGrupo = 0;
            this.valoresTotalPorGrupo.resultadoDaytradePorGrupo = 0;
            this.valoresTotalPorGrupo.resultadoNormalPorGrupo = 0;
            this.valoresTotalPorGrupo.taxasPorGrupo = 0;
        }

        private void TableTotalGrupoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            
            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(summaryFinal);
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
                //
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[2], this.valoresTotalPorGrupo.valorCompraPorGrupo);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[4], this.valoresTotalPorGrupo.valorVendaPorGrupo);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[5], this.valoresTotalPorGrupo.resultadoNormalPorGrupo);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[6], this.valoresTotalPorGrupo.resultadoDaytradePorGrupo);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[7], this.valoresTotalPorGrupo.taxasPorGrupo);
            }
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
           XRTable summaryFinal = sender as XRTable;
            
            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(summaryFinal);
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
                //
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = Resources.SubReportMapaResultadoBolsa._TotalAcoesOpcoesTermo + ": ";
                //
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[2], this.valoresTotalBolsa.valorCompraSum);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[4], this.valoresTotalBolsa.valorVendaSum);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[5], this.valoresTotalBolsa.resultadoNormalSum);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[6], this.valoresTotalBolsa.resultadoDaytradeSum);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[7], this.valoresTotalBolsa.taxasSum);
            }
        }
    }
}
