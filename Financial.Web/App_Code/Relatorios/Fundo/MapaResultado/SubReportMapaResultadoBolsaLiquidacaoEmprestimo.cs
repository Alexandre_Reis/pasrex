﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Bolsa.Enums;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using System.Text;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportMapaResultadoBolsaLiquidacaoEmprestimo
    /// </summary>
    public class SubReportMapaResultadoBolsaLiquidacaoEmprestimo : XtraReport {
        // Parametros para o relatorio
        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private OperacaoBolsaCollection operacaoBolsaCollectionCopia = new OperacaoBolsaCollection();

        //                    
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell13;
        private XRTableRow xrTableRow3;
        private XRTable xrTable3;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow1;
        private XRTable xrTable1;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow2;
        private XRTable xrTable2;        
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell26;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell6;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell28;
        private OperacaoBolsaCollection operacaoBolsaCollection1;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportMapaResultadoBolsaLiquidacaoEmprestimo() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCliente, DateTime dataInicio, DateTime dataFim) {
            this.idCliente = idCliente;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;

            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
            
            #region Pega Campos do resource
            this.xrTableCell13.Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._Ativo;
            this.xrTableCell2.Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._QtdeCompras;
            this.xrTableCell14.Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._QtdeVendas;
            this.xrTableCell16.Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._ResultNormal;
            this.xrTableCell18.Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._Despesa;
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);

            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                this.xrTable3.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable6.Visible = false;
            }
        }

        /// <summary>
        /// Retorna a Table De Total do subRelatorio
        /// - Usada no Relatorio PAI
        /// </summary>
        /// <returns></returns>
        public XRTable getTableTotalTransferencias() {
            return this.xrTable6;
        }

        private DataTable FillDados() {            
            #region SQL
            this.operacaoBolsaCollection1.QueryReset();

            this.operacaoBolsaCollection1.Query.es.Distinct = true;

            /* OBS: No Start do relatorio é feito uma query copia igual a essa.
               Se essa query mudar a copia também deve mudar
            */
            this.operacaoBolsaCollection1.Query
                 .Select(this.operacaoBolsaCollection1.Query.CdAtivoBolsa)
                 .Where(this.operacaoBolsaCollection1.Query.IdCliente == this.idCliente,                        
                        this.operacaoBolsaCollection1.Query.Origem.In(OrigemOperacaoBolsa.LiquidacaoEmprestimo),
                        this.operacaoBolsaCollection1.Query.Data.Between(this.dataInicio, this.dataFim));
          
            #endregion

            return this.operacaoBolsaCollection1.Query.LoadDataTable();
        } 

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/MapaResultado/SubReportMapaResultadoBolsaTransferencias.resx";
         */
        private void InitializeComponent() {
            string resourceFileName = "Relatorios/Fundo/MapaResultado/SubReportMapaResultadoBolsaLiquidacaoEmprestimo.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.operacaoBolsaCollection1 = new Financial.Bolsa.OperacaoBolsaCollection();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.Height = 48;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PrintOnEmptyDataSource = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Location = new System.Drawing.Point(100, 0);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.Size = new System.Drawing.Size(1855, 48);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell8,
            this.xrTableCell5,
            this.xrTableCell12,
            this.xrTableCell7,
            this.xrTableCell10,
            this.xrTableCell6});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.Size = new System.Drawing.Size(1855, 48);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CdAtivoBolsa", "")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Size = new System.Drawing.Size(194, 48);
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Location = new System.Drawing.Point(194, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Size = new System.Drawing.Size(217, 48);
            this.xrTableCell11.Text = "QuantidadeCompras";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeComprasBeforePrint);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Location = new System.Drawing.Point(411, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Size = new System.Drawing.Size(251, 48);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Location = new System.Drawing.Point(662, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Size = new System.Drawing.Size(212, 48);
            this.xrTableCell5.Text = "QuantidadeVendas";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeVendasBeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Location = new System.Drawing.Point(874, 0);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Size = new System.Drawing.Size(254, 48);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Location = new System.Drawing.Point(1128, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Size = new System.Drawing.Size(254, 48);
            this.xrTableCell7.Text = "ResultadoNormal";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ResultadoNormalBeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Location = new System.Drawing.Point(1382, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Size = new System.Drawing.Size(233, 48);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Location = new System.Drawing.Point(1615, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Size = new System.Drawing.Size(240, 48);
            this.xrTableCell6.Text = "Despesas";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DespesasBeforePrint);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell18.Location = new System.Drawing.Point(1614, 0);
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Size = new System.Drawing.Size(241, 40);
            this.xrTableCell18.Text = "#Despesa";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell17.Location = new System.Drawing.Point(1381, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Size = new System.Drawing.Size(233, 40);
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell16.Location = new System.Drawing.Point(1127, 0);
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Size = new System.Drawing.Size(254, 40);
            this.xrTableCell16.Text = "#ResultNormal";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell15.Location = new System.Drawing.Point(873, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Size = new System.Drawing.Size(254, 40);
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell14.Location = new System.Drawing.Point(661, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Size = new System.Drawing.Size(212, 40);
            this.xrTableCell14.Text = "#QtdeVendas";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell13.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell13.Multiline = true;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Size = new System.Drawing.Size(195, 40);
            this.xrTableCell13.Text = "#Ativo";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell2,
            this.xrTableCell1,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.Size = new System.Drawing.Size(1855, 40);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Location = new System.Drawing.Point(195, 0);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Size = new System.Drawing.Size(212, 40);
            this.xrTableCell2.Text = "#QtdeCompras";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableCell1.Location = new System.Drawing.Point(407, 0);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Size = new System.Drawing.Size(254, 40);
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.Transparent;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Location = new System.Drawing.Point(100, 10);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.Size = new System.Drawing.Size(1855, 40);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable4.Location = new System.Drawing.Point(100, 55);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.Size = new System.Drawing.Size(1855, 40);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.Size = new System.Drawing.Size(1855, 40);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Size = new System.Drawing.Size(1855, 40);
            this.xrTableCell26.Text = "TipoMercado";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoMercadoBeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.TipoMercado", "")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Location = new System.Drawing.Point(21, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell4.Size = new System.Drawing.Size(1736, 55);
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell27.Size = new System.Drawing.Size(21, 55);
            this.xrTableCell27.Text = "-";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell4});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(1757, 55);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Location = new System.Drawing.Point(0, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(1757, 55);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell20.Size = new System.Drawing.Size(1750, 15);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(1750, 15);
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Location = new System.Drawing.Point(0, 56);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.Size = new System.Drawing.Size(1750, 15);
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Location = new System.Drawing.Point(100, 5);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.Size = new System.Drawing.Size(1855, 48);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell19,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell28});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.Size = new System.Drawing.Size(1855, 48);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Size = new System.Drawing.Size(493, 48);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "TotalLiquidacaoEmprestimo";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Location = new System.Drawing.Point(493, 0);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Size = new System.Drawing.Size(64, 48);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Location = new System.Drawing.Point(557, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Size = new System.Drawing.Size(105, 48);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Location = new System.Drawing.Point(662, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Size = new System.Drawing.Size(212, 48);
            this.xrTableCell22.Text = "QuantidadeVendas";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Location = new System.Drawing.Point(874, 0);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Size = new System.Drawing.Size(254, 48);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Location = new System.Drawing.Point(1128, 0);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Size = new System.Drawing.Size(254, 48);
            this.xrTableCell24.Text = "ResultadoNormal";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Location = new System.Drawing.Point(1382, 0);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Size = new System.Drawing.Size(233, 48);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Location = new System.Drawing.Point(1615, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Size = new System.Drawing.Size(240, 48);
            this.xrTableCell28.Text = "Despesas";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // operacaoBolsaCollection1
            // 
            this.operacaoBolsaCollection1.AllowDelete = true;
            this.operacaoBolsaCollection1.AllowEdit = true;
            this.operacaoBolsaCollection1.AllowNew = true;
            this.operacaoBolsaCollection1.EnableHierarchicalBinding = true;
            this.operacaoBolsaCollection1.Filter = "";
            this.operacaoBolsaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.operacaoBolsaCollection1.Sort = "";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.Height = 98;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.Height = 61;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // SubReportMapaResultadoBolsaLiquidacaoEmprestimo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DataSource = this.operacaoBolsaCollection1;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.Version = "8.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo.ResourceManager;
        }

        /// <summary>
        /// Retorna os Valores Totais do SubReport de LiquidacaoEmprestimo
        /// </summary>
        public ValoresTotaisLiquidacaoEmprestimo ValoresTotaisSubreportLiquidacaoEmprestimo {
            get { return valoresTotaisLiquidacaoEmprestimo; }
        }

        //SUM - Total
        #region Variaveis Internas do Relatorio
        public class ValoresTotaisLiquidacaoEmprestimo {
            public decimal resultadoNormalSum = 0.00M;
            public decimal taxasSum = 0.00M;
        }
        //
        private ValoresTotaisLiquidacaoEmprestimo valoresTotaisLiquidacaoEmprestimo = new ValoresTotaisLiquidacaoEmprestimo();
        #endregion

        private void TipoMercadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoMercadoTableCell = sender as XRTableCell;
            tipoMercadoTableCell.Text = "";
            
            if (this.numeroLinhasDataTable != 0) {
                string tipoMercado = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._MercadoBolsa;
                tipoMercadoTableCell.Text = tipoMercado;
            }
        }

        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Executado no start do relatorio
            this.operacaoBolsaCollectionCopia.QueryReset();
            //
            this.operacaoBolsaCollectionCopia.Query
                .Select(this.operacaoBolsaCollectionCopia.Query.IdOperacao,
                        this.operacaoBolsaCollectionCopia.Query.CdAtivoBolsa,
                        this.operacaoBolsaCollectionCopia.Query.TipoMercado,
                        this.operacaoBolsaCollectionCopia.Query.TipoOperacao,
                        this.operacaoBolsaCollectionCopia.Query.Data,
                        this.operacaoBolsaCollectionCopia.Query.ResultadoTermo,
                        this.operacaoBolsaCollectionCopia.Query.IdCliente,
                        this.operacaoBolsaCollectionCopia.Query.Quantidade,
                        this.operacaoBolsaCollectionCopia.Query.Valor,
                        this.operacaoBolsaCollectionCopia.Query.ValorLiquido.As("ResultadoDaytrade"),                        
                        (this.operacaoBolsaCollectionCopia.Query.ResultadoRealizado + this.operacaoBolsaCollectionCopia.Query.ResultadoExercicio + this.operacaoBolsaCollectionCopia.Query.ResultadoTermo).As("Resultado"),
                        (this.operacaoBolsaCollectionCopia.Query.Corretagem + this.operacaoBolsaCollectionCopia.Query.Emolumento + this.operacaoBolsaCollectionCopia.Query.LiquidacaoCBLC + this.operacaoBolsaCollectionCopia.Query.RegistroBolsa + this.operacaoBolsaCollectionCopia.Query.RegistroCBLC).As("Taxa")
                 )
                 .Where(this.operacaoBolsaCollectionCopia.Query.IdCliente == this.idCliente,                        
                        this.operacaoBolsaCollectionCopia.Query.Origem.In(OrigemOperacaoBolsa.LiquidacaoEmprestimo),
                        this.operacaoBolsaCollectionCopia.Query.Data.Between(this.dataInicio, this.dataFim));

            this.operacaoBolsaCollectionCopia.Query.Load();
        }

        private void QuantidadeComprasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell quantidadeComprasTableCell = sender as XRTableCell;
            quantidadeComprasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal quantidadeCompras = 0.00M;
                //
                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
                                               
                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                //
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    quantidadeCompras += operacaoBolsaCollectionCopia[i].Quantidade.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                quantidadeComprasTableCell.Text = quantidadeCompras.ToString("n2");
            }
        }

        private void QuantidadeVendasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell quantidadeVendasTableCell = sender as XRTableCell;
            quantidadeVendasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal quantidadeVendas = 0.00M;

                StringBuilder filtro = new StringBuilder();
                filtro.Append("CdAtivoBolsa = '" + cdAtivoBolsa + "'");
                filtro.Append(" AND TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");

                this.operacaoBolsaCollectionCopia.Filter = filtro.ToString();
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    quantidadeVendas += operacaoBolsaCollectionCopia[i].Quantidade.Value;
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                ReportBase.ConfiguraSinalNegativo(quantidadeVendasTableCell, quantidadeVendas);
            }
        }
      
        private void ResultadoNormalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell resultadoNormalTableCell = sender as XRTableCell;
            resultadoNormalTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal resultadoNormal = 0.00M;

                this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'" + " AND (TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'" + " OR TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "') ";
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    resultadoNormal += (decimal)operacaoBolsaCollectionCopia[i].GetColumn("Resultado");
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                // Soma Total
                this.valoresTotaisLiquidacaoEmprestimo.resultadoNormalSum += resultadoNormal;

                ReportBase.ConfiguraSinalNegativo(resultadoNormalTableCell, resultadoNormal);
            }
        }

        private void DespesasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell taxasTableCell = sender as XRTableCell;
            taxasTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                decimal taxas = 0.00M;

                this.operacaoBolsaCollectionCopia.Filter = "CdAtivoBolsa = '" + cdAtivoBolsa + "'";
                for (int i = 0; i < operacaoBolsaCollectionCopia.Count; i++) {
                    taxas += (decimal)operacaoBolsaCollectionCopia[i].GetColumn("Taxa");
                }
                // Zera o Filtro
                this.operacaoBolsaCollectionCopia.Filter = "";
                //
                taxas = taxas * -1;

                // Soma Total
                this.valoresTotaisLiquidacaoEmprestimo.taxasSum += taxas;

                ReportBase.ConfiguraSinalNegativo(taxasTableCell, taxas);
            }
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
           XRTable summaryFinal = sender as XRTable;
            
            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(summaryFinal);
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
                //
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = Resources.SubReportMapaResultadoBolsaLiquidacaoEmprestimo._TotalLiquidacaoEmprestimo + ": ";
                //
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[5], this.valoresTotaisLiquidacaoEmprestimo.resultadoNormalSum);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow0.Cells[7], this.valoresTotaisLiquidacaoEmprestimo.taxasSum);
            }
        }         
    }
}
