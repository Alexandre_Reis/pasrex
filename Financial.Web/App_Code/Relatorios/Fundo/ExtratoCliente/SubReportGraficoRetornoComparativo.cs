﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;
using Financial.Investidor;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportGraficoRetorno12Meses
    /// </summary>
    public class SubReportGraficoRetornoComparativo : XtraReport
    {
        private ReportBase relatorioBase;

        private CalculoMedida calculoMedida;

        private List<Indice> listaBenchmarks = new List<Indice>();
        private Dictionary<string, decimal> dadosRetornoCarteira = new Dictionary<string, decimal>();
        private Dictionary<string, decimal> dadosRetornoBenchmark = new Dictionary<string, decimal>();

        private DateTime dataInicio;
        private DateTime dataFim;

        private DataTable dataTable = new DataTable();

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return this.dadosRetornoCarteira != null && this.dadosRetornoBenchmark != null ? this.dadosRetornoBenchmark.Count != 0 : false;
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private PageFooterBand PageFooter;
        private XRControlStyle Header2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private GroupFooterBand GroupFooter1;
        private XRLabel xrLabel1;
        private XRChart xrChart1;
        private XRControlStyle EvenStyle;
        private XRControlStyle OddStyle;
        private XRSubreport xrSubreport1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportGraficoRetornoComparativo()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">idCarteira a ser colocado no gráfico</param>
        /// Grafico será gerado 12 Meses para trás
        /// </param>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataInicio, DateTime dataFim)
        {
            this.calculoMedida = calculoMedida;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            
            // Configura o Relatorio
            relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            this.PageFooter.Visible = false;
        }

        
        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {

            DataTable dt = calculoMedida.RetornaTabelaRetornoIndices(dataFim);
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            this.listaBenchmarks.Clear();
            this.dadosRetornoCarteira.Clear();
            this.dadosRetornoBenchmark.Clear();

            this.listaBenchmarks = calculoMedida.RetornaListaBenchMarks();
            this.dadosRetornoCarteira = calculoMedida.RetornaTabelaRetornoCarteira(this.dataInicio, this.dataFim, listaBenchmarks);
            this.dadosRetornoBenchmark = calculoMedida.RetornaTabelaRetornoBenchmark(this.dataInicio, this.dataFim, listaBenchmarks);

            if (this.HasData)
            {
                this.FillDadosGrafico();
            }
            else
            {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico()
        {

            Series series1 = new Series("Carteira", ViewType.Bar);
            //
            foreach (KeyValuePair<string, decimal> pair in this.dadosRetornoCarteira)
            {
                series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series1.ArgumentScaleType = ScaleType.Qualitative;
            series1.Label.Visible = false;
            series1.PointOptions.PointView = PointView.Values;

            // Só Adiciona Serie se houver Dados
            if (this.dadosRetornoCarteira.Count > 0)
            {
                this.xrChart1.Series.Add(series1);
            }
            
            Series series2 = new Series("Benchmark", ViewType.Bar);

            foreach (KeyValuePair<string, decimal> pair in this.dadosRetornoBenchmark)
            {
                series2.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series2.ArgumentScaleType = ScaleType.Qualitative;
            series2.Label.Visible = false;
            series2.PointOptions.PointView = PointView.Values;

            // Só Adiciona Serie se houver Dados
            if (this.dadosRetornoBenchmark.Count > 0)
            {
                this.xrChart1.Series.Add(series2);
            }

            // Access the view-type-specific options of the series.
            //((SideBySideBarSeriesView)this.xrChart1.Series[1].View).ColorEach = true;
            //((SideBySideBarSeriesView)this.xrChart1.Series[1].View).Color = Color.DimGray

            //((SideBySideBarSeriesLabel)this.xrChart1.Series[1].Label).Border = false;

            this.xrChart1.Series[0].ShowInLegend = false;
            //((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Month;
            //((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.ScaleMode = ScaleMode.Manual;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportGraficoRetorno12Meses.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportGraficoRetornoComparativo.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrChart1,
            this.xrLabel1});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 578F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Chameleon";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;            
            xyDiagram1.AxisX.DateTimeScaleOptions.GridAlignment = DateTimeGridAlignment.Month;
            xyDiagram1.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Month;
            xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "MMM/yy";
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Tahoma", 7F);
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Tickmarks.Length = 1;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.EndText = "%";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.BackColor = System.Drawing.Color.White;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 15);
            this.xrChart1.Legend.Visible = false;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 47.99998F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Private";
            this.xrChart1.PaletteRepository.Add("Palette 1", new DevExpress.XtraCharts.Palette("Palette 1", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.Silver, System.Drawing.Color.Silver)}));
            this.xrChart1.PaletteRepository.Add("Private", new DevExpress.XtraCharts.Palette("Private", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))))}));
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "RetornoCarteira";
            sideBySideBarSeriesLabel2.Border.Color = System.Drawing.Color.Black;
            sideBySideBarSeriesLabel2.LineColor = System.Drawing.Color.Black;
            sideBySideBarSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            sideBySideBarSeriesLabel2.TextColor = System.Drawing.Color.Black;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.Name = "RetornoBenchmark";
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            sideBySideBarSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            sideBySideBarSeriesView1.ColorEach = true;
            this.xrChart1.SeriesTemplate.View = sideBySideBarSeriesView1;
            this.xrChart1.SideBySideBarDistanceFixed = 0;
            this.xrChart1.SideBySideEqualBarWidth = true;
            this.xrChart1.SizeF = new System.Drawing.SizeF(930F, 530F);
            this.xrChart1.StylePriority.UseBackColor = false;
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(930F, 35F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "RETORNO COMPARATIVO - NO PERÍODO";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // EvenStyle
            // 
            this.EvenStyle.Name = "EvenStyle";
            this.EvenStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // OddStyle
            // 
            this.OddStyle.BackColor = System.Drawing.Color.White;
            this.OddStyle.Name = "OddStyle";
            this.OddStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrSubreport1.Visible = false;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(212.5F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // SubReportGraficoRetornoComparativo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.GroupFooter1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 1757, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2,
            this.EvenStyle,
            this.OddStyle});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportGraficoRetorno12Meses.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e)
        {
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Kind = MarkerKind.Square;
        }


        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais em Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e)
        {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal? valor = null;
            try
            {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
                valor = valor / 100;
            }
            catch (Exception e1)
            {
                // Não faz nada
            }

            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            //
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
        }
    }
}