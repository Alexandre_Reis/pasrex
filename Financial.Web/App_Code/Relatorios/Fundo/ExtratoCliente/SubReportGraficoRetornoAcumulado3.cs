﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportGraficoRetornoAcumulado3
    /// </summary>
    public class SubReportGraficoRetornoAcumulado3 : XtraReport
    {

        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        // Carteira que será gerado o grafico
        private CalculoMedida calculoMedida;

        #region Classe Interna que Contem o IdCarteira E os Dados de Rentabilidade da Carteira
        protected class DadosCarteira
        {
            //
            public int idCarteira;
            public string nomeFundo;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeFundo = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosCarteira[] dadosCarteira;
        #endregion

        #region Classe Interna que Contem o IdIndice E os Dados de Rentabilidade do Indice
        protected class DadosIndice
        {
            public int idIndice;
            public string nomeIndice;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeIndice = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosIndice[] dadosIndice;
        #endregion

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {

                if (this.dadosCarteira == null && dadosIndice == null)
                {
                    return false;
                }
                else
                {

                    bool retorno = false;

                    for (int i = 0; i < this.dadosCarteira.Length; i++)
                    {
                        if (this.dadosCarteira[i].dadosRentabilidadeFundo.Count != 0)
                        {
                            retorno = true;
                            break;
                        }
                    }
                    if (!retorno)
                    {
                        for (int i = 0; i < this.dadosIndice.Length; i++)
                        {
                            if (this.dadosIndice[i].dadosRentabilidadeIndice.Count != 0)
                            {
                                retorno = true;
                                break;
                            }
                        }
                    }
                    return retorno;

                }
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRSubreport xrSubreport1;
        private PageFooterBand PageFooter;
        
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private GroupHeaderBand GroupHeader1;
        private XRControlStyle Header2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private ReportSemDados reportSemDados1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportGraficoRetornoAcumulado3()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">idCarteira a ser colocado no gráfico</param>
        /// <param name="idIndice">indice a ser colocado no gráfico</param>
        /// <param name="dataInicio">Data Inicio do Gráfico De Rentabilidade</param>
        /// <param name="dataFim">Data Fim do Gráfico De Rentabilidade</param>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataInicio, DateTime dataFim)
        {
            this.calculoMedida = calculoMedida;
            //
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            // Inicializa a classe De Dados de Carteira
            this.dadosCarteira = new DadosCarteira[1];

            int idCarteira = this.calculoMedida.carteira.IdCarteira.Value;

            this.dadosCarteira[0] = new DadosCarteira();
            this.dadosCarteira[0].idCarteira = idCarteira;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Apelido);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            //
            this.dadosCarteira[0].nomeFundo = carteira.Apelido.Trim();
            //

            int idIndice = this.calculoMedida.indice.IdIndice.Value;

            // Inicializa a classe De Dados de Indice
            this.dadosIndice = new DadosIndice[1];
            this.dadosIndice[0] = new DadosIndice();
            this.dadosIndice[0].idIndice = idIndice;

            Indice indice = new Indice();
            campos = new List<esQueryItem>();
            campos.Add(indice.Query.Descricao);
            indice.LoadByPrimaryKey(campos, (short)idIndice);
            //
            this.dadosIndice[0].nomeIndice = indice.Descricao.Trim();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            this.GroupHeader1.Visible = false;
            this.xrTable2.Visible = false;
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {
            //
            this.CarregaDadosRentabilidade();
            //
            if (this.HasData)
            {
                this.FillDadosGrafico();
            }
            else
            {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Carrega num Dicionario uma lista de Datas com as Respectivas Rentabilidades Dos Fundos e Indices
        /// </summary>
        private void CarregaDadosRentabilidade()
        {

            #region Limpa os Dicionarios
            // Limpa o Dicionario de Carteira
            for (int i = 0; i < this.dadosCarteira.Length; i++)
            {
                this.dadosCarteira[i].dadosRentabilidadeFundo.Clear();
            }

            // Limpa o Dicionario de Indice
            for (int i = 0; i < this.dadosIndice.Length; i++)
            {
                this.dadosIndice[i].dadosRentabilidadeIndice.Clear();
            }
            #endregion



            CalculoMedida.DicsEstatisticaRetornoAcumulado dicsRetornosAcumulados = this.calculoMedida.RetornaDicsRetornosAcumuladosFimMes(this.DataInicio, this.dataFim);
            // Só tem 1 Carteira
            for (int j = 0; j < this.dadosCarteira.Length; j++)
            {
                this.dadosCarteira[j].dadosRentabilidadeFundo = dicsRetornosAcumulados.retornoAcumulado;
            }

            #region Dados do Indice
            for (int j = 0; j < this.dadosIndice.Length; j++)
            {
                this.dadosIndice[j].dadosRentabilidadeIndice = dicsRetornosAcumulados.retornoBenchmarkAcumulado;
            }
            #endregion
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico()
        {

            #region Adiciona N Série de Fundos
            //
            for (int i = 0; i < this.dadosCarteira.Length; i++)
            {
                //Series series1 = new Series(this.dadosCarteira[i].nomeFundo, ViewType.Line);
                Series series1 = new Series("Carteira", ViewType.Line);
                //
                foreach (KeyValuePair<DateTime, decimal> pair in this.dadosCarteira[i].dadosRentabilidadeFundo)
                {
                    series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
                }
                series1.ArgumentScaleType = ScaleType.DateTime;
                series1.Label.Visible = false;
                series1.View.Color = Color.FromArgb(8, 33, 40);
                //
                // Só Adiciona Serie se houver Dados
                if (this.dadosCarteira[i].dadosRentabilidadeFundo.Count > 0)
                {
                    this.xrChart1.Series.Add(series1);
                }
            }
            #endregion

            #region Adiciona N Série de Indices
            for (int i = 0; i < this.dadosIndice.Length; i++)
            {
                Series series2 = new Series(this.dadosIndice[i].nomeIndice, ViewType.Line);
                //
                foreach (KeyValuePair<DateTime, decimal> pair in this.dadosIndice[i].dadosRentabilidadeIndice)
                {
                    series2.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
                }
                series2.ArgumentScaleType = ScaleType.DateTime;
                series2.Label.Visible = false;
                series2.View.Color = Color.FromArgb(200, 193, 166);
                //
                // Só Adiciona Serie se houver Dados
                if (this.dadosIndice[i].dadosRentabilidadeIndice.Count > 0)
                {
                    this.xrChart1.Series.Add(series2);
                }
            }
            #endregion

            this.xrChart1.Series[0].ShowInLegend = false;
            this.xrChart1.Series[1].ShowInLegend = false;

            ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Day;
            ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeOptions.FormatString = (this.dataFim - this.DataInicio).TotalDays > 60 ? "MMM/yy" : "dd/MMM";
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportGraficoRetornoAcumulado3.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportGraficoRetornoAcumulado3.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel3 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrSubreport1.Visible = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(930F, 35F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "RETORNO ACUMULADO - CONSIDERANDO FINAL DE CADA MÊS";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Terracotta Pie";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrChart1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "MMM/yy";
            xyDiagram1.AxisX.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Angle = 270;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Times New Roman", 7F);
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Title.Text = "";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisY.Label.EndText = "%";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Times New Roman", 7F);
            xyDiagram1.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.BackColor = System.Drawing.Color.White;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYZooming = true;
            xyDiagram1.Margins.Top = 2;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrChart1.Legend.Margins.Bottom = 1;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 1);
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteRepository.Add("Private", new DevExpress.XtraCharts.Palette("Private", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(193)))), ((int)(((byte)(166))))), System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(193)))), ((int)(((byte)(166)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(52)))), ((int)(((byte)(58))))), System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(52)))), ((int)(((byte)(58))))))}));
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "CDIAAux";
            lineSeriesView1.LineMarkerOptions.Visible = false;
            series1.View = lineSeriesView1;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel2.Visible = false;
            series2.Label = pointSeriesLabel2;
            series2.Name = "FIC DE FIM GPAR";
            lineSeriesView2.LineMarkerOptions.Visible = false;
            series2.View = lineSeriesView2;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = pointSeriesLabel3;
            lineSeriesView3.ColorEach = true;
            this.xrChart1.SeriesTemplate.View = lineSeriesView3;
            this.xrChart1.SizeF = new System.Drawing.SizeF(930F, 530F);
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            this.xrChart1.CustomDrawSeries += new DevExpress.XtraCharts.CustomDrawSeriesEventHandler(this.xrChart1_CustomDrawSeries);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart1,
            this.xrLabel1,
            this.xrSubreport1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 587F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportGraficoRetornoAcumulado3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 1757, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportGraficoRetornoAcumulado3.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e)
        {
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Kind = MarkerKind.Square;
        }
    }
}