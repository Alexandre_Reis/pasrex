﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;
using Financial.Investidor;
using Financial.WebConfigConfiguration;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportGraficoRetorno12Meses
    /// </summary>
    public class SubReportFrequenciaRetorno : XtraReport
    {
        private CalculoMedida calculoMedida;

        private DateTime dataReferencia;

        private int numeroClasses;

        private DateTime? dataInicio;

        private List<CalculoMedida.FrequenciaRetorno> listaFrequenciaRetorno = new List<CalculoMedida.FrequenciaRetorno>();

        private Dictionary<string, decimal> dadosFrequenciaRetorno = new Dictionary<string, decimal>();

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return this.listaFrequenciaRetorno != null ? this.listaFrequenciaRetorno.Count != 0 : false;
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private PageFooterBand PageFooter;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private GroupHeaderBand GroupHeader1;
        private XRControlStyle Header2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRSubreport xrSubreport1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportFrequenciaRetorno()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">idCarteira a ser colocado no gráfico</param>
        /// Grafico será gerado 12 Meses para trás
        /// </param>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataReferencia, DateTime? dataInicio)
        {
            this.dataReferencia = dataReferencia;

            this.numeroClasses = 8;

            this.dataInicio = dataInicio;

            this.calculoMedida = calculoMedida;
            
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            this.GroupHeader1.Visible = false;
            this.PageFooter.Visible = false;
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {
            this.listaFrequenciaRetorno.Clear();
            this.listaFrequenciaRetorno = this.calculoMedida.RetornaListaFrequenciaRetorno(this.dataReferencia, 21, this.numeroClasses, this.dataInicio.Value);

            this.dadosFrequenciaRetorno.Clear();
            int i = 0;
            foreach (CalculoMedida.FrequenciaRetorno frequenciaRetorno in listaFrequenciaRetorno)
            {
                i++;
                dadosFrequenciaRetorno.Add(( i.ToString() + " - " + Decimal.Round(frequenciaRetorno.FrequenciaInicio * 100, 0)).ToString() + " - " + Decimal.Round(frequenciaRetorno.FrequenciaFim * 100, 0).ToString(), frequenciaRetorno.PercentualFrequencia);
            }
            
            if (this.HasData)
            {
                this.FillDadosGrafico();
            }
            else
            {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico()
        {

            Series series1 = new Series("Rentabilidade", ViewType.Bar);
            //
            foreach (KeyValuePair<string, decimal> pair in this.dadosFrequenciaRetorno)
            {
                series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
            }
            series1.ArgumentScaleType = ScaleType.Qualitative;
            series1.Label.Visible = true;
            series1.Label.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            series1.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;

            string fonteWebConfig = "Times New Roman"; // padrao se vier errado do webconfig
            try
            {
                fonteWebConfig = WebConfig.AppSettings.TipoFonteRelatorio;
            }
            catch (Exception)
            {
                fonteWebConfig = "Times New Roman";
            }

            series1.Label.Font = new Font(fonteWebConfig, 7.5F);

            series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
            series1.PointOptions.ValueNumericOptions.Precision = 2;

            // Só Adiciona Serie se houver Dados
            if (this.dadosFrequenciaRetorno.Count > 0)
            {
                this.xrChart1.Series.Add(series1);
            }

            this.xrChart1.Series[0].ShowInLegend = false;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportGraficoRetorno12Meses.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportFrequenciaRetorno.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView2 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(930F, 35F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "FREQUENCIA DE RETORNOS - %CDI (Janelas Móveis)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Chameleon";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram1.AxisX.Label.Angle = 315;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Tickmarks.Length = 1;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.NumericOptions.Format = NumericFormat.Percent;
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Times New Roman", 6F);
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.BackColor = System.Drawing.Color.White;
            xyDiagram1.LabelsResolveOverlappingMinIndent = 0;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 15);
            this.xrChart1.Legend.Visible = false;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 54.70831F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Private";
            this.xrChart1.PaletteRepository.Add("Private", new DevExpress.XtraCharts.Palette("Private", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(40)))), ((int)(((byte)(62))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(40)))), ((int)(((byte)(62))))))}));
            sideBySideBarSeriesLabel1.BackColor = System.Drawing.Color.Transparent;
            sideBySideBarSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 5F);
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            sideBySideBarSeriesLabel1.ShowForZeroValues = true;
            sideBySideBarSeriesLabel1.TextColor = System.Drawing.Color.Black;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "RentabilidadeAux";
            pointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            series1.PointOptions = pointOptions1;
            series1.TopNOptions.Enabled = true;
            series1.TopNOptions.Mode = DevExpress.XtraCharts.TopNMode.ThresholdPercent;
            series1.TopNOptions.ThresholdPercent = 40;
            sideBySideBarSeriesView1.BarWidth = 0.5;
            sideBySideBarSeriesView1.ColorEach = true;
            series1.View = sideBySideBarSeriesView1;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            sideBySideBarSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel2;
            sideBySideBarSeriesView2.ColorEach = true;
            this.xrChart1.SeriesTemplate.View = sideBySideBarSeriesView2;
            this.xrChart1.SideBySideBarDistanceFixed = 0;
            this.xrChart1.SideBySideEqualBarWidth = true;
            this.xrChart1.SizeF = new System.Drawing.SizeF(930F, 530F);
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            this.xrChart1.CustomDrawSeries += new DevExpress.XtraCharts.CustomDrawSeriesEventHandler(this.xrChart1_CustomDrawSeries);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrChart1,
            this.xrLabel1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 584.7083F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // Header2
            // 
            this.Header2.BackColor = System.Drawing.Color.DimGray;
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrSubreport1.Visible = false;
            // 
            // SubReportFrequenciaRetorno
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(101, 1759, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportGraficoRetorno12Meses.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e)
        {
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Kind = MarkerKind.Square;
        }
    }
}