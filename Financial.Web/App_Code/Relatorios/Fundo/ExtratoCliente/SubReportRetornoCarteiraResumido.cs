﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportRetornoCarteiraResumido
    /// </summary>
    public class SubReportRetornoCarteiraResumido : XtraReport
    {

        private CalculoMedida calculoMedida;

        private ReportBase relatorioBase;

        private int numeroLinhasDataTable;

        private DateTime dataFim;

        /// <summary>
        /// Retorna true se o Report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                //return this.numeroLinhasDataTable != 0;
                return true; // Sempre tem dados
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRLabel xrLabel1;
        private PageFooterBand PageFooter;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell72;
        private XRTableCell xrTableCell73;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private GroupFooterBand GroupFooter1;
        private XRControlStyle Header2;
        private XRControlStyle Header3;
        private XRControlStyle EvenRow;
        private XRControlStyle OddRow;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell64;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTable xrTable1;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell120;
        private XRTableCell xrTableCell121;
        private XRTableCell xrTableCell122;
        private XRTableCell xrTableCell123;
        private XRTableCell xrTableCell124;
        private XRTableCell xrTableCell125;
        private XRTableCell xrTableCell126;
        private XRTableCell xrTableCell127;
        private XRTableCell xrTableCell128;
        private XRTableCell xrTableCell129;
        private XRTableCell xrTableCell130;
        private XRTableCell xrTableCell131;
        private XRTableCell xrTableCell132;
        private XRTableCell xrTableCell133;
        private XRTableCell xrTableCell134;
        private XRTableCell xrTableCell135;
        private XRTableCell xrTableCell136;
        private ListaBenchmarkCollection listaBenchmarkCollection1;
        private XRControlStyle xrControlStyle1;
        private XRControlStyle xrControlStyle2;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportRetornoCarteiraResumido()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataFim)
        {
            this.calculoMedida = calculoMedida;
            this.dataFim = dataFim;
            //
            // Configura o Relatorio
            relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
        }

        private DataTable FillDados() {
            #region SQL
            this.listaBenchmarkCollection1.QueryReset();
            this.listaBenchmarkCollection1.Query
                 .Select(this.listaBenchmarkCollection1.Query.IdIndice)
                 .Where(this.listaBenchmarkCollection1.Query.IdCarteira == this.calculoMedida.carteira.IdCarteira &&
                        this.listaBenchmarkCollection1.Query.IdIndice.NotIn(this.calculoMedida.carteira.IdIndiceBenchmark.Value)
                 );
            #endregion
            //
            return this.listaBenchmarkCollection1.Query.LoadDataTable();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            // Se não tem dados
            if (this.numeroLinhasDataTable == 0)
            {
                //this.GroupHeader1.Visible = false;
                //this.GroupFooter1.Visible = false;
                //this.PageFooter.Visible = false;

                // Some com Detail
                this.Detail.Visible = false;
                //this.xrTable6.Visible = false;
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportRetornoCarteiraResumido.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportRetornoCarteiraResumido.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Header3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenRow = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddRow = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.listaBenchmarkCollection1 = new Financial.Fundo.ListaBenchmarkCollection();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 35.45415F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable1.BorderColor = System.Drawing.Color.White;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1894.439F, 35F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailRentabilidadeBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.5;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.EvenStyleName = "EvenRow";
            this.xrTableCell120.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell120.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.OddStyleName = "OddRow";
            this.xrTableCell120.StylePriority.UseFont = false;
            this.xrTableCell120.StylePriority.UseForeColor = false;
            this.xrTableCell120.Text = "Indice";
            this.xrTableCell120.Weight = 0.069422349131141189;
            this.xrTableCell120.WordWrap = false;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.EvenStyleName = "EvenRow";
            this.xrTableCell121.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell121.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.OddStyleName = "OddRow";
            this.xrTableCell121.StylePriority.UseFont = false;
            this.xrTableCell121.StylePriority.UseForeColor = false;
            this.xrTableCell121.Text = "xrTableCell6";
            this.xrTableCell121.Weight = 0.065615890333912283;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.EvenStyleName = "EvenRow";
            this.xrTableCell122.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell122.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.OddStyleName = "OddRow";
            this.xrTableCell122.StylePriority.UseFont = false;
            this.xrTableCell122.StylePriority.UseForeColor = false;
            this.xrTableCell122.Text = "xrTableCell7";
            this.xrTableCell122.Weight = 0.065615888280651236;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.EvenStyleName = "EvenRow";
            this.xrTableCell123.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell123.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.OddStyleName = "OddRow";
            this.xrTableCell123.StylePriority.UseFont = false;
            this.xrTableCell123.StylePriority.UseForeColor = false;
            this.xrTableCell123.Text = "xrTableCell8";
            this.xrTableCell123.Weight = 0.065615888280651222;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.EvenStyleName = "EvenRow";
            this.xrTableCell124.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell124.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.OddStyleName = "OddRow";
            this.xrTableCell124.StylePriority.UseFont = false;
            this.xrTableCell124.StylePriority.UseForeColor = false;
            this.xrTableCell124.Text = "xrTableCell9";
            this.xrTableCell124.Weight = 0.065615890555526024;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.EvenStyleName = "EvenRow";
            this.xrTableCell125.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell125.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.OddStyleName = "OddRow";
            this.xrTableCell125.StylePriority.UseFont = false;
            this.xrTableCell125.StylePriority.UseForeColor = false;
            this.xrTableCell125.Text = "xrTableCell15";
            this.xrTableCell125.Weight = 0.065615889378741771;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.EvenStyleName = "EvenRow";
            this.xrTableCell126.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.OddStyleName = "OddRow";
            this.xrTableCell126.StylePriority.UseFont = false;
            this.xrTableCell126.StylePriority.UseForeColor = false;
            this.xrTableCell126.Text = "xrTableCell18";
            this.xrTableCell126.Weight = 0.065615888791104179;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.EvenStyleName = "EvenRow";
            this.xrTableCell127.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell127.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.OddStyleName = "OddRow";
            this.xrTableCell127.StylePriority.UseFont = false;
            this.xrTableCell127.StylePriority.UseForeColor = false;
            this.xrTableCell127.Text = "xrTableCell20";
            this.xrTableCell127.Weight = 0.0656158872093005;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.EvenStyleName = "EvenRow";
            this.xrTableCell128.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell128.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.OddStyleName = "OddRow";
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.StylePriority.UseForeColor = false;
            this.xrTableCell128.Text = "xrTableCell23";
            this.xrTableCell128.Weight = 0.065615887513842674;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.EvenStyleName = "EvenRow";
            this.xrTableCell129.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell129.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.OddStyleName = "OddRow";
            this.xrTableCell129.StylePriority.UseFont = false;
            this.xrTableCell129.StylePriority.UseForeColor = false;
            this.xrTableCell129.Text = "xrTableCell24";
            this.xrTableCell129.Weight = 0.065615887502775277;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.EvenStyleName = "EvenRow";
            this.xrTableCell130.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell130.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.OddStyleName = "OddRow";
            this.xrTableCell130.StylePriority.UseFont = false;
            this.xrTableCell130.StylePriority.UseForeColor = false;
            this.xrTableCell130.Text = "xrTableCell25";
            this.xrTableCell130.Weight = 0.065615889148864376;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.EvenStyleName = "EvenRow";
            this.xrTableCell131.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell131.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.OddStyleName = "OddRow";
            this.xrTableCell131.StylePriority.UseFont = false;
            this.xrTableCell131.StylePriority.UseForeColor = false;
            this.xrTableCell131.Text = "xrTableCell26";
            this.xrTableCell131.Weight = 0.065615890735995414;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.EvenStyleName = "EvenRow";
            this.xrTableCell132.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell132.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.OddStyleName = "OddRow";
            this.xrTableCell132.StylePriority.UseFont = false;
            this.xrTableCell132.StylePriority.UseForeColor = false;
            this.xrTableCell132.Text = "xrTableCell27";
            this.xrTableCell132.Weight = 0.065615888319431048;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.EvenStyleName = "EvenRow";
            this.xrTableCell133.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell133.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.OddStyleName = "OddRow";
            this.xrTableCell133.StylePriority.UseFont = false;
            this.xrTableCell133.StylePriority.UseForeColor = false;
            this.xrTableCell133.Text = "xrTableCell49";
            this.xrTableCell133.Weight = 0.065615889115812329;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.EvenStyleName = "EvenRow";
            this.xrTableCell134.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell134.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.OddStyleName = "OddRow";
            this.xrTableCell134.StylePriority.UseFont = false;
            this.xrTableCell134.StylePriority.UseForeColor = false;
            this.xrTableCell134.Text = "xrTableCell50";
            this.xrTableCell134.Weight = 0.065615889115812343;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.EvenStyleName = "EvenRow";
            this.xrTableCell135.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell135.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.OddStyleName = "OddRow";
            this.xrTableCell135.StylePriority.UseFont = false;
            this.xrTableCell135.StylePriority.UseForeColor = false;
            this.xrTableCell135.Text = "xrTableCell51";
            this.xrTableCell135.Weight = 0.065615889002043556;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.EvenStyleName = "EvenRow";
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell136.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.OddStyleName = "OddRow";
            this.xrTableCell136.StylePriority.UseFont = false;
            this.xrTableCell136.StylePriority.UseForeColor = false;
            this.xrTableCell136.Text = "xrTableCell52";
            this.xrTableCell136.Weight = 0.066207004871727287;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11.00003F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1894.439F, 39F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "RENTABILIDADES DA CARTEIRA (%) ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 21.00008F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.xrLabel1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.GroupHeader1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 190F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.StylePriority.UseBackColor = false;
            this.GroupHeader1.StylePriority.UseFont = false;
            this.GroupHeader1.StylePriority.UseForeColor = false;
            // 
            // xrTable6
            // 
            this.xrTable6.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable6.BorderColor = System.Drawing.Color.White;
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow1});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1894.439F, 140F);
            this.xrTable6.StylePriority.UseBackColor = false;
            this.xrTable6.StylePriority.UseBorderColor = false;
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableRentabilidadeCabecalhoBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell1,
            this.xrTableCell4,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell17,
            this.xrTableCell16,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell59,
            this.xrTableCell68,
            this.xrTableCell65,
            this.xrTableCell62});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.5;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell21.CanGrow = false;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StyleName = "Header3";
            this.xrTableCell21.StylePriority.UseBackColor = false;
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseForeColor = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell21.Weight = 0.069422349131141189;
            this.xrTableCell21.WordWrap = false;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StyleName = "Header3";
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseForeColor = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Mês\r\n";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.065615890333912283;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StyleName = "Header3";
            this.xrTableCell4.StylePriority.UseBackColor = false;
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseForeColor = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Ano";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.065615888280651236;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StyleName = "Header3";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "6 Meses";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.065615888280651222;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StyleName = "Header3";
            this.xrTableCell13.StylePriority.UseBackColor = false;
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseForeColor = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "12 Meses";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.065615890555526024;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StyleName = "Header3";
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseForeColor = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Mes12";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.065615889378741771;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StyleName = "Header3";
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Mes11";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.065615888791104179;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StyleName = "Header3";
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Mes10";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.0656158872093005;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StyleName = "Header3";
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "Mes9";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.065615887513842674;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StyleName = "Header3";
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseForeColor = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Mes8";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.065615887502775277;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StyleName = "Header3";
            this.xrTableCell16.StylePriority.UseBackColor = false;
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseForeColor = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "Mes7";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.065615889148864376;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell72.StyleName = "Header3";
            this.xrTableCell72.StylePriority.UseBackColor = false;
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UseForeColor = false;
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.Text = "Mes6";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell72.Weight = 0.065615890735995414;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.StyleName = "Header3";
            this.xrTableCell73.StylePriority.UseBackColor = false;
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UseForeColor = false;
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.Text = "Mes5";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell73.Weight = 0.065615888319431048;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StyleName = "Header3";
            this.xrTableCell59.StylePriority.UseBackColor = false;
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseForeColor = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "Mes4";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell59.Weight = 0.065615889115812329;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StyleName = "Header3";
            this.xrTableCell68.StylePriority.UseBackColor = false;
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UseForeColor = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "Mes3";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.065615889115812343;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StyleName = "Header3";
            this.xrTableCell65.StylePriority.UseBackColor = false;
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "Mes2";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.065615889002043556;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 6.7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StyleName = "Header3";
            this.xrTableCell62.StylePriority.UseBackColor = false;
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "Mes1";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.066207004871727287;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell60,
            this.xrTableCell69,
            this.xrTableCell66,
            this.xrTableCell63});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StyleName = "Header3";
            this.xrTableRow2.Weight = 0.5;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StyleName = "OddRow";
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "Carteira";
            this.xrTableCell22.Weight = 0.069422349131141189;
            this.xrTableCell22.WordWrap = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StyleName = "OddRow";
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.Weight = 0.065615890333912283;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StyleName = "OddRow";
            this.xrTableCell29.StylePriority.UseBackColor = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.Weight = 0.065615888280651236;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StyleName = "OddRow";
            this.xrTableCell30.StylePriority.UseBackColor = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.Weight = 0.065615888280651222;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StyleName = "OddRow";
            this.xrTableCell31.StylePriority.UseBackColor = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.Weight = 0.065615890555526024;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StyleName = "OddRow";
            this.xrTableCell33.StylePriority.UseBackColor = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "xrTableCell33";
            this.xrTableCell33.Weight = 0.065615889378741771;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StyleName = "OddRow";
            this.xrTableCell34.StylePriority.UseBackColor = false;
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "xrTableCell34";
            this.xrTableCell34.Weight = 0.065615888791104179;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StyleName = "OddRow";
            this.xrTableCell35.StylePriority.UseBackColor = false;
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "xrTableCell35";
            this.xrTableCell35.Weight = 0.0656158872093005;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StyleName = "OddRow";
            this.xrTableCell36.StylePriority.UseBackColor = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.Weight = 0.065615887513842674;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StyleName = "OddRow";
            this.xrTableCell37.StylePriority.UseBackColor = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.Weight = 0.065615887502775277;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StyleName = "OddRow";
            this.xrTableCell38.StylePriority.UseBackColor = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.Text = "xrTableCell38";
            this.xrTableCell38.Weight = 0.065615889148864376;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StyleName = "OddRow";
            this.xrTableCell39.StylePriority.UseBackColor = false;
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.Text = "xrTableCell39";
            this.xrTableCell39.Weight = 0.065615890735995414;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StyleName = "OddRow";
            this.xrTableCell40.StylePriority.UseBackColor = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.Weight = 0.065615888319431048;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StyleName = "OddRow";
            this.xrTableCell60.StylePriority.UseBackColor = false;
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.Text = "xrTableCell60";
            this.xrTableCell60.Weight = 0.065615889115812329;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StyleName = "OddRow";
            this.xrTableCell69.StylePriority.UseBackColor = false;
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.Text = "xrTableCell69";
            this.xrTableCell69.Weight = 0.065615889115812343;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StyleName = "OddRow";
            this.xrTableCell66.StylePriority.UseBackColor = false;
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.Text = "xrTableCell66";
            this.xrTableCell66.Weight = 0.065615889002043556;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StyleName = "OddRow";
            this.xrTableCell63.StylePriority.UseBackColor = false;
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.Text = "xrTableCell63";
            this.xrTableCell63.Weight = 0.066207004871727287;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell61,
            this.xrTableCell70,
            this.xrTableCell67,
            this.xrTableCell64});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.5;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell41.Multiline = true;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StyleName = "EvenRow";
            this.xrTableCell41.StylePriority.UseBackColor = false;
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "%Indice\r\n";
            this.xrTableCell41.Weight = 0.069422349131141189;
            this.xrTableCell41.WordWrap = false;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StyleName = "EvenRow";
            this.xrTableCell42.StylePriority.UseBackColor = false;
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Text = "xrTableCell42";
            this.xrTableCell42.Weight = 0.065615890333912283;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StyleName = "EvenRow";
            this.xrTableCell43.StylePriority.UseBackColor = false;
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.Text = "xrTableCell43";
            this.xrTableCell43.Weight = 0.065615888280651236;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StyleName = "EvenRow";
            this.xrTableCell44.StylePriority.UseBackColor = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.Weight = 0.065615888280651222;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StyleName = "EvenRow";
            this.xrTableCell45.StylePriority.UseBackColor = false;
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.Weight = 0.065615890555526024;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StyleName = "EvenRow";
            this.xrTableCell47.StylePriority.UseBackColor = false;
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.Text = "xrTableCell47";
            this.xrTableCell47.Weight = 0.065615889378741771;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StyleName = "EvenRow";
            this.xrTableCell48.StylePriority.UseBackColor = false;
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.Text = "xrTableCell48";
            this.xrTableCell48.Weight = 0.065615888791104179;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StyleName = "EvenRow";
            this.xrTableCell53.StylePriority.UseBackColor = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.Text = "xrTableCell53";
            this.xrTableCell53.Weight = 0.0656158872093005;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StyleName = "EvenRow";
            this.xrTableCell54.StylePriority.UseBackColor = false;
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.Text = "xrTableCell54";
            this.xrTableCell54.Weight = 0.065615887513842674;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StyleName = "EvenRow";
            this.xrTableCell55.StylePriority.UseBackColor = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "xrTableCell55";
            this.xrTableCell55.Weight = 0.065615887502775277;
            this.xrTableCell55.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StyleName = "EvenRow";
            this.xrTableCell56.StylePriority.UseBackColor = false;
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.Text = "xrTableCell56";
            this.xrTableCell56.Weight = 0.065615889148864376;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StyleName = "EvenRow";
            this.xrTableCell57.StylePriority.UseBackColor = false;
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.Text = "xrTableCell57";
            this.xrTableCell57.Weight = 0.065615890735995414;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StyleName = "EvenRow";
            this.xrTableCell58.StylePriority.UseBackColor = false;
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.Text = "xrTableCell58";
            this.xrTableCell58.Weight = 0.065615888319431048;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StyleName = "EvenRow";
            this.xrTableCell61.StylePriority.UseBackColor = false;
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.Text = "xrTableCell61";
            this.xrTableCell61.Weight = 0.065615889115812329;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StyleName = "EvenRow";
            this.xrTableCell70.StylePriority.UseBackColor = false;
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.Text = "xrTableCell70";
            this.xrTableCell70.Weight = 0.065615889115812343;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StyleName = "EvenRow";
            this.xrTableCell67.StylePriority.UseBackColor = false;
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.Text = "xrTableCell67";
            this.xrTableCell67.Weight = 0.065615889002043556;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StyleName = "EvenRow";
            this.xrTableCell64.StylePriority.UseBackColor = false;
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.Text = "xrTableCell64";
            this.xrTableCell64.Weight = 0.066207004871727287;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell15,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.5;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StyleName = "OddRow";
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Indice";
            this.xrTableCell5.Weight = 0.069422349131141189;
            this.xrTableCell5.WordWrap = false;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StyleName = "OddRow";
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 0.065615890333912283;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StyleName = "OddRow";
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 0.065615888280651236;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StyleName = "OddRow";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 0.065615888280651222;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StyleName = "OddRow";
            this.xrTableCell9.StylePriority.UseBackColor = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 0.065615890555526024;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StyleName = "OddRow";
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.Text = "xrTableCell15";
            this.xrTableCell15.Weight = 0.065615889378741771;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StyleName = "OddRow";
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 0.065615888791104179;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StyleName = "OddRow";
            this.xrTableCell20.StylePriority.UseBackColor = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Text = "xrTableCell20";
            this.xrTableCell20.Weight = 0.0656158872093005;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StyleName = "OddRow";
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "xrTableCell23";
            this.xrTableCell23.Weight = 0.065615887513842674;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StyleName = "OddRow";
            this.xrTableCell24.StylePriority.UseBackColor = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "xrTableCell24";
            this.xrTableCell24.Weight = 0.065615887502775277;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StyleName = "OddRow";
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.Text = "xrTableCell25";
            this.xrTableCell25.Weight = 0.065615889148864376;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StyleName = "OddRow";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.Weight = 0.065615890735995414;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StyleName = "OddRow";
            this.xrTableCell27.StylePriority.UseBackColor = false;
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.Weight = 0.065615888319431048;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StyleName = "OddRow";
            this.xrTableCell49.StylePriority.UseBackColor = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.Weight = 0.065615889115812329;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StyleName = "OddRow";
            this.xrTableCell50.StylePriority.UseBackColor = false;
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.Text = "xrTableCell50";
            this.xrTableCell50.Weight = 0.065615889115812343;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StyleName = "OddRow";
            this.xrTableCell51.StylePriority.UseBackColor = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.Text = "xrTableCell51";
            this.xrTableCell51.Weight = 0.065615889002043556;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(229)))), ((int)(((byte)(199)))));
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 6.7F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StyleName = "OddRow";
            this.xrTableCell52.StylePriority.UseBackColor = false;
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.Weight = 0.066207004871727287;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 0F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // Header3
            // 
            this.Header3.Name = "Header3";
            this.Header3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // EvenRow
            // 
            this.EvenRow.Name = "EvenRow";
            this.EvenRow.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // OddRow
            // 
            this.OddRow.Name = "OddRow";
            this.OddRow.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // listaBenchmarkCollection1
            // 
            this.listaBenchmarkCollection1.AllowDelete = true;
            this.listaBenchmarkCollection1.AllowEdit = true;
            this.listaBenchmarkCollection1.AllowNew = true;
            this.listaBenchmarkCollection1.EnableHierarchicalBinding = true;
            this.listaBenchmarkCollection1.Filter = "";
            this.listaBenchmarkCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.listaBenchmarkCollection1.Sort = "";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // SubReportRetornoCarteiraResumido
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupFooter1,
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.listaBenchmarkCollection1;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(99, 752, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2,
            this.Header3,
            this.EvenRow,
            this.OddRow,
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportRetornoCarteiraResumido.ResourceManager;
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e)
        {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal? valor = null;
            try
            {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1)
            {
                // Não faz nada
            }

            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativoDesprezaWebConfig(valorXRTableCell, valor, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
        }

        // Contém o Valor das Rentalidades da Carteira 
        /* rentabilidadeCotaMes
         * rentabilidadeCotaAno
         * rentabilidadeCota6Meses
         * rentabilidadeCota12Meses
         */
        // Variável Global ao relatorio Setado no método TableRentabilidadeCabecalhoBeforePrint
        private List<decimal?> rentabilidadeCotaTable = new List<decimal?>(4);

        /// <summary>
        /// Metodo para imprimir rentabilidade da carteira, indice e Indice Diferencial
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TableRentabilidadeCabecalhoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;
            //
            //    #region Limpa Valores da Tabela
            //    // Limpa somente as linhas 2,3
            //    for (int i = 2; i < xrTable.Rows.Count; i++) {
            //        int colunas = ((XRTableRow)xrTable.Rows[i]).Cells.Count;
            //        for (int j = 0; j < colunas; j++) {
            //            ((XRTableCell)xrTable.Rows[i].Cells[j]).Text = "";
            //        }
            //    }
            //    #endregion

            XRTableRow xrTableRow0 = xrTable.Rows[0];
            XRTableRow xrTableRow1 = xrTable.Rows[1];
            XRTableRow xrTableRow2 = xrTable.Rows[2];
            XRTableRow xrTableRow3 = xrTable.Rows[3];

            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;

            #region Exibe Cabeçalho - Meses
            //
            List<DateTime> datas = new List<DateTime>(12); // 12 meses

            DateTime d = this.dataFim;
            for (int i = 0; i <= 11; i++) {
                d = d.AddMonths(-1);
                datas.Add(d);
            }

            for (int i = 0; i <= 11; i++) {
                string mes = datas[i].ToString("MMM").Substring(0, 1).ToUpper() + datas[i].ToString("MMM").Substring(1);
                ((XRTableCell)xrTableRow0.Cells[i + 5]).Text = mes + "/" + datas[i].ToString("yy");
            }
            #endregion

            List<decimal?> rentabilidadeCarteira = new List<decimal?>();
            List<decimal?> rentabilidadeDiferencial = new List<decimal?>();
            List<decimal?> rentabilidadeIndice = new List<decimal?>();            

            #region Linha 1 - Carteira - Mes/Ano/6Meses/12meses/24Meses

            #region Calcula datas dos periodos
            DateTime dataReferencia = this.dataFim;

            DateTime? dataAnterior = Calendario.SubtraiDiaUtil(dataReferencia, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(dataReferencia, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(dataReferencia, -1);
            DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 6);
            DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 12);
            //DateTime? data24Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 24);

            DateTime dataInicioCliente = this.calculoMedida.carteira.DataInicioCota.Value;

            if (dataAnterior < dataInicioCliente) {
                dataAnterior = null;
            }
            if (dataMes < dataInicioCliente) {
                dataMes = null;
            }
            if (dataAno < dataInicioCliente) {
                dataAno = null;
            }

            if (data6Meses < dataInicioCliente) {
                data6Meses = null;
            }
            if (data12Meses < dataInicioCliente) {
                data12Meses = null;
            }
            //if (data24Meses < dataInicioCliente) {
            //    data24Meses = null;
            //}
            #endregion

            //
            //CalculoMedida calculoMedida = new CalculoMedida();
            //calculoMedida.SetDataInicio(this.dataInicioCliente);
            //this.calculoMedida.SetDataInicio(dataInicioCliente);
            //                                                                       
            #region Calcula Rentabilidade Carteira

            //#region rentabilidadeDia
            //decimal? rentabilidadeCotaDia = null;
            //if (dataAnterior != null) {
            //    try {
            //        rentabilidadeCotaDia = this.calculoMedida.CalculaRetornoDia(dataReferencia);
            //        rentabilidadeCotaDia = rentabilidadeCotaDia / 100;
            //    }
            //    catch (HistoricoCotaNaoCadastradoException) { }
            //}
            //#endregion

            #region rentabilidadeMes
            decimal? rentabilidadeCotaMes = null;
            if (dataMes != null) {
                try {
                    rentabilidadeCotaMes = calculoMedida.CalculaRetornoMes(dataReferencia);
                    rentabilidadeCotaMes = rentabilidadeCotaMes;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeCotaAno = null;
            if (dataAno != null) {
                try {
                    rentabilidadeCotaAno = calculoMedida.CalculaRetornoAno(dataReferencia);
                    rentabilidadeCotaAno = rentabilidadeCotaAno;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeCota6Meses = null;
            if (data6Meses != null) {
                try {
                    rentabilidadeCota6Meses = calculoMedida.CalculaRetornoPeriodoMes(dataReferencia, 6);
                    rentabilidadeCota6Meses = rentabilidadeCota6Meses;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeCota12Meses = null;
            if (data12Meses != null) {
                try {
                    rentabilidadeCota12Meses = calculoMedida.CalculaRetornoPeriodoMes(dataReferencia, 12);
                    rentabilidadeCota12Meses = rentabilidadeCota12Meses;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            //#region rentabilidade24Meses
            //decimal? rentabilidadeCota24Meses = null;
            //if (data24Meses != null) {
            //    try {
            //        rentabilidadeCota24Meses = calculoMedida.CalculaRetornoPeriodoMes(dataReferencia, 24);
            //        rentabilidadeCota24Meses = rentabilidadeCota24Meses / 100;
            //    }
            //    catch (HistoricoCotaNaoCadastradoException) { }
            //}
            //#endregion

            //#region rentabilidadeInicio
            //decimal? rentabilidadeCotaInicio = null;
            //try {
            //    rentabilidadeCotaInicio = calculoMedida.CalculaRetorno(this.dataInicioCliente, this.dataReferencia);
            //    rentabilidadeCotaInicio = rentabilidadeCotaInicio / 100;
            //}
            //catch (HistoricoCotaNaoCadastradoException) { }
            //#endregion

            rentabilidadeCarteira.Add(rentabilidadeCotaMes);
            rentabilidadeCarteira.Add(rentabilidadeCotaAno);
            rentabilidadeCarteira.Add(rentabilidadeCota6Meses);
            rentabilidadeCarteira.Add(rentabilidadeCota12Meses);
            //rentabilidadeCarteira.Add(rentabilidadeCota24Meses);

            // Copia o valor na variável Global rentabilidadeCotaTable
            this.rentabilidadeCotaTable.Add(rentabilidadeCarteira[0]); // Mes
            this.rentabilidadeCotaTable.Add(rentabilidadeCarteira[1]); // Ano
            this.rentabilidadeCotaTable.Add(rentabilidadeCarteira[2]); // 6 Meses
            this.rentabilidadeCotaTable.Add(rentabilidadeCarteira[3]); // 12 Meses
            #endregion

            /* Exibe Rentabilidade Carteira */
            #region Exibe Rentabilidade Carteira

            ((XRTableCell)xrTableRow1.Cells[0]).Text = "Carteira";

            for (int i = 0; i <= 3; i++) {
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow1.Cells[i + 1], rentabilidadeCarteira[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            }
            #endregion

            #endregion

            #region Linha1/Linha2/Linha3 - 12 Meses Rentabilidade Carteira/Índice/Dif Índice/

            List<decimal?> rentabilidadeCarteiraMeses = new List<decimal?>(12); // Armazena a rentabilidade dos 12 meses
            List<decimal?> rentabilidadeIndiceDifMeses = new List<decimal?>(12);
            List<decimal?> rentabilidadeIndiceMeses = new List<decimal?>(12);
            //
            for (int i = 0; i < datas.Count; i++) { // Para Cada Data Mes procura o retorno no Vetor de retorno Mensal
                CalculoMedida.EstatisticaRetornoMensal cMensal = this.calculoMedida.ListaRetornoMensal.Find(
                            delegate(CalculoMedida.EstatisticaRetornoMensal item) {
                                return item.Data.Year == datas[i].Year &&
                                       item.Data.Month == datas[i].Month;
                            });

                if (cMensal != null) {
                    rentabilidadeCarteiraMeses.Add(cMensal.Retorno.Value);

                    #region Diferencial
                    if (cMensal.RetornoDiferencial.HasValue) {
                        rentabilidadeIndiceDifMeses.Add(cMensal.RetornoDiferencial.Value);
                    }
                    else {
                        rentabilidadeIndiceDifMeses.Add(null);
                    }
                    #endregion

                    #region Indice
                    if (cMensal.RetornoBenchmark.HasValue) {
                        rentabilidadeIndiceMeses.Add(cMensal.RetornoBenchmark.Value);
                    }
                    else {
                        rentabilidadeIndiceMeses.Add(null);
                    }
                    #endregion
                }
                else {
                    rentabilidadeCarteiraMeses.Add(null);
                    rentabilidadeIndiceDifMeses.Add(null);
                    rentabilidadeIndiceMeses.Add(null);
                }
            }

            for (int i = 0; i <= 11; i++) {
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow1.Cells[i + 5], rentabilidadeCarteiraMeses[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow2.Cells[i + 5], rentabilidadeIndiceDifMeses[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow3.Cells[i + 5], rentabilidadeIndiceMeses[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            }    
            #endregion

            #region Linha 2/Linha 3 - mes/ano/6meses/12meses/24meses - indice e %indice

            #region Calcula Rentabilidade Indice
            // Pega o Indice
            int idIndice = (Int16)this.calculoMedida.carteira.IdIndiceBenchmark;

            //#region rentabilidadeDia
            //decimal? rentabilidadeIndiceDia = null;
            //if (dataAnterior != null) {
            //    try {
            //        rentabilidadeIndiceDia = calculoMedida.CalculaRetornoDiaIndice(this.dataReferencia);
            //        rentabilidadeIndiceDia = rentabilidadeIndiceDia / 100;
            //    }
            //    catch (CotacaoIndiceNaoCadastradoException) { }
            //}
            //#endregion

            #region rentabilidadeMes
            decimal? rentabilidadeIndiceMes = null;
            if (dataMes != null) {
                try {
                    rentabilidadeIndiceMes = calculoMedida.CalculaRetornoMesIndice(dataReferencia);
                    rentabilidadeIndiceMes = rentabilidadeIndiceMes;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeIndiceAno = null;
            if (dataAno != null) {
                try {
                    rentabilidadeIndiceAno = calculoMedida.CalculaRetornoAnoIndice(dataReferencia);
                    rentabilidadeIndiceAno = rentabilidadeIndiceAno;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeIndice6Meses = null;
            if (data6Meses != null) {
                try {
                    rentabilidadeIndice6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(dataReferencia, 6);
                    rentabilidadeIndice6Meses = rentabilidadeIndice6Meses;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeIndice12Meses = null;
            if (data12Meses != null) {
                try {
                    rentabilidadeIndice12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(dataReferencia, 12);
                    rentabilidadeIndice12Meses = rentabilidadeIndice12Meses;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            //#region rentabilidade24Meses
            //decimal? rentabilidadeIndice24Meses = null;
            //if (data24Meses != null) {
            //    try {
            //        rentabilidadeIndice24Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(dataReferencia, 24);
            //        rentabilidadeIndice24Meses = rentabilidadeIndice24Meses / 100;
            //    }
            //    catch (CotacaoIndiceNaoCadastradoException) { }
            //}
            //#endregion

            //#region rentabilidadeInicio
            //decimal? rentabilidadeIndiceInicio = null;
            //try {
            //    rentabilidadeIndiceInicio = calculoMedida.CalculaRetornoIndice(this.dataInicioCliente, this.dataReferencia);
            //    rentabilidadeIndiceInicio = rentabilidadeIndiceInicio / 100;
            //}
            //catch (CotacaoIndiceNaoCadastradoException) { }
            //#endregion

            rentabilidadeIndice.Add(rentabilidadeIndiceMes);
            rentabilidadeIndice.Add(rentabilidadeIndiceAno);
            rentabilidadeIndice.Add(rentabilidadeIndice6Meses);
            rentabilidadeIndice.Add(rentabilidadeIndice12Meses);
            //rentabilidadeIndice.Add(rentabilidadeIndice24Meses);
            #endregion

            #region Calcula Rentabilidade Diferencial
            //
            //decimal? rentabilidadeDiaDiferencial;
            decimal? rentabilidadeMesDiferencial;
            decimal? rentabilidadeAnoDiferencial;
            decimal? rentabilidade6MesesDiferencial;
            decimal? rentabilidade12MesesDiferencial;
            //decimal? rentabilidade24MesesDiferencial;

            int? tipoIndice = null;
            if (this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.es.HasData) {
                tipoIndice = this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.Tipo.Value;
            }
            if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Decimal) {
                #region TipoIndice = Decimal
                //rentabilidadeDiaDiferencial = rentabilidadeCarteira[0].HasValue && rentabilidadeIndice[0].HasValue
                //                            ? rentabilidadeCarteira[0] - rentabilidadeIndice[0]
                //                            : null;

                rentabilidadeMesDiferencial = rentabilidadeCarteira[0].HasValue && rentabilidadeIndice[0].HasValue
                                            ? rentabilidadeCarteira[0] - rentabilidadeIndice[0]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCarteira[1].HasValue && rentabilidadeIndice[1].HasValue
                                            ? rentabilidadeCarteira[1] - rentabilidadeIndice[1]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCarteira[2].HasValue && rentabilidadeIndice[2].HasValue
                                            ? rentabilidadeCarteira[2] - rentabilidadeIndice[2]
                                            : null;


                rentabilidade12MesesDiferencial = rentabilidadeCarteira[3].HasValue && rentabilidadeIndice[3].HasValue
                                            ? rentabilidadeCarteira[3] - rentabilidadeIndice[3]
                                            : null;

                //rentabilidade24MesesDiferencial = rentabilidadeCarteira[4].HasValue && rentabilidadeIndice[4].HasValue
                //                            ? rentabilidadeCarteira[4] - rentabilidadeIndice[4]
                //                            : null;

                //rentabilidade36MesesDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue
                //                            ? rentabilidadeCota[5] - rentabilidadeIndice[5]
                //                            : null;

                //rentabilidadeInicioDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue
                //                            ? rentabilidadeCota[5] - rentabilidadeIndice[5]
                //                            : null;
                #endregion
            }
            else {
                #region TipoIndice = Percentual
                //rentabilidadeDiaDiferencial = rentabilidadeCota[0].HasValue && rentabilidadeIndice[0].HasValue && rentabilidadeIndice[0] != 0
                //                            ? rentabilidadeCota[0] / rentabilidadeIndice[0]
                //                            : null;

                rentabilidadeMesDiferencial = rentabilidadeCarteira[0].HasValue && rentabilidadeIndice[0].HasValue && rentabilidadeIndice[0] != 0
                                            ? rentabilidadeCarteira[0] / rentabilidadeIndice[0]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCarteira[1].HasValue && rentabilidadeIndice[1].HasValue && rentabilidadeIndice[1] != 0
                                            ? rentabilidadeCarteira[1] / rentabilidadeIndice[1]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCarteira[2].HasValue && rentabilidadeCarteira[2].HasValue && rentabilidadeIndice[2] != 0
                            ? rentabilidadeCarteira[2] / rentabilidadeIndice[2]
                            : null;

                rentabilidade12MesesDiferencial = rentabilidadeCarteira[3].HasValue && rentabilidadeIndice[3].HasValue && rentabilidadeIndice[3] != 0
                                            ? rentabilidadeCarteira[3] / rentabilidadeIndice[3]
                                            : null;

                //rentabilidade24MesesDiferencial = rentabilidadeCarteira[4].HasValue && rentabilidadeIndice[4].HasValue && rentabilidadeIndice[4]!= 0
                //                            ? rentabilidadeCarteira[4] / rentabilidadeIndice[4]
                //                            : null;

                //rentabilidade36MesesDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue && rentabilidadeIndice[5]!= 0
                //                            ? rentabilidadeCota[5] / rentabilidadeIndice[5]
                //                            : null;

                //rentabilidadeInicioDiferencial = rentabilidadeCota[5].HasValue && rentabilidadeIndice[5].HasValue && rentabilidadeIndice[5] != 0
                //                            ? rentabilidadeCota[5] / rentabilidadeIndice[5]
                //                            : null;
                #endregion
            }

            //rentabilidadeDiferencial.Add(rentabilidadeDiaDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeMesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeAnoDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade6MesesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade12MesesDiferencial);
            //rentabilidadeDiferencial.Add(rentabilidade24MesesDiferencial);
            #endregion

            /* Exibe Rentabilidade Diferencial */
            #region Exibe Rentabilidade %Indice e Indice

            // Se alguma rentabilidade Carteira for nula rentabilidadeDiferencial também vai ser nula
            /* ----------------------------------------------------*/
            for (int i = 0; i < rentabilidadeDiferencial.Count; i++) {
                if (!rentabilidadeCarteira[i].HasValue) {
                    rentabilidadeDiferencial[i] = null;
                }
            }

            string textoIndice = "Indx";
            string textoIndiceDescricao = this.calculoMedida.indice.Descricao;
            string prefixoIndice = (this.calculoMedida.indice.Tipo == (byte)TipoIndice.Decimal ? "(-) " : "% ");
            string textoIndiceDif = prefixoIndice + textoIndice;
            string textoIndiceDifDescricao = prefixoIndice + textoIndiceDescricao;

            /* ----------------------------------------------------*/
            ((XRTableCell)xrTableRow2.Cells[0]).Text = textoIndiceDifDescricao;
            ((XRTableCell)xrTableRow3.Cells[0]).Text = this.calculoMedida.indice.Descricao;

            for (int i = 0; i <= 3; i++) {
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow2.Cells[i + 1], rentabilidadeDiferencial[i]*100, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
                ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow3.Cells[i + 1], rentabilidadeIndice[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            }    
            #endregion 

            #endregion
        }

        /// <summary>
        /// Metodo para imprimir rentabilidade do Indice do Detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TableDetailRentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable xrTable = sender as XRTable;
            
            #region Zera os campos da Table
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            
            for (int i = 0; i < 17; i++) {
			 ((XRTableCell)xrTableRow0.Cells[i]).Text = "";
			}            
            #endregion

            #region Indice
            if (this.numeroLinhasDataTable != 0) {

                List<decimal?> rentabilidadeIndice = new List<decimal?>();
                //
                CalculoMedida calculoMedida = new CalculoMedida();
                calculoMedida.SetDataInicio(this.calculoMedida.carteira.DataInicioCota.Value);
                //                        
                int idIndice = Convert.ToInt16(this.GetCurrentColumnValue(ListaBenchmarkMetadata.ColumnNames.IdIndice)); // Indice da lista de BenchMark
                calculoMedida.SetIdIndice(idIndice);

                #region Calcula datas dos periodos
                DateTime dataReferencia = this.dataFim;
                //                
                DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(dataReferencia, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(dataReferencia, -1);
                DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 6);
                DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(dataReferencia, 12);                
                //
                DateTime dataInicioCliente = this.calculoMedida.carteira.DataInicioCota.Value;

                if (dataMes < dataInicioCliente) {
                    dataMes = null;
                }
                if (dataAno < dataInicioCliente) {
                    dataAno = null;
                }
                if (data6Meses < dataInicioCliente) {
                    data6Meses = null;
                }
                if (data12Meses < dataInicioCliente) {
                    data12Meses = null;
                }
                #endregion

                #region Calcula Rentabilidade Indice

                #region rentabilidadeMes
                decimal? rentabilidadeIndiceMes = null;
                if (dataMes != null) {
                    try {
                        rentabilidadeIndiceMes = calculoMedida.CalculaRetornoMesIndice(dataReferencia);
                        rentabilidadeIndiceMes = rentabilidadeIndiceMes;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                }
                #endregion

                #region rentabilidadeAno
                decimal? rentabilidadeIndiceAno = null;
                if (dataAno != null) {
                    try {
                        rentabilidadeIndiceAno = calculoMedida.CalculaRetornoAnoIndice(dataReferencia);
                        rentabilidadeIndiceAno = rentabilidadeIndiceAno;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                }
                #endregion

                #region rentabilidade6Meses
                decimal? rentabilidadeIndice6Meses = null;
                if (data6Meses != null) {
                    try {
                        rentabilidadeIndice6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(dataReferencia, 6);
                        rentabilidadeIndice6Meses = rentabilidadeIndice6Meses;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                }
                #endregion

                #region rentabilidade12Meses
                decimal? rentabilidadeIndice12Meses = null;
                if (data12Meses != null) {
                    try {
                        rentabilidadeIndice12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(dataReferencia, 12);
                        rentabilidadeIndice12Meses = rentabilidadeIndice12Meses;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                }
                #endregion

                rentabilidadeIndice.Add(rentabilidadeIndiceMes);
                rentabilidadeIndice.Add(rentabilidadeIndiceAno);
                rentabilidadeIndice.Add(rentabilidadeIndice6Meses);
                rentabilidadeIndice.Add(rentabilidadeIndice12Meses);
                #endregion

                #region Exibe Rentabilidade Indice

                Indice indice = new Indice();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(indice.Query.Descricao);
                indice.LoadByPrimaryKey(campos, (short)idIndice);

                // Se alguma rentabilidade Cota for nula rentabilidadeIndice também vai ser nula
                /* ----------------------------------------------------*/
                for (int i = 0; i < this.rentabilidadeCotaTable.Count; i++) {
                    if (!rentabilidadeCotaTable[i].HasValue) {
                        rentabilidadeIndice[i] = null;
                    }
                }
                /* ----------------------------------------------------*/

                Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
                Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;

                for (int i = 0; i <= 3; i++) {
                    ((XRTableCell)xrTableRow0.Cells[0]).Text = !String.IsNullOrEmpty(indice.Descricao) ? indice.Descricao : "";
                    //
                    ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow0.Cells[i + 1], rentabilidadeIndice[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
                }
                #endregion 

                #region Indice por Data

                List<DateTime> datas = new List<DateTime>(12); // 12 meses

                DateTime d = this.dataFim;
                for (int i = 0; i <= 11; i++) {
                    d = d.AddMonths(-1);
                    datas.Add(d);
                }

                CalculoMedida cm = new CalculoMedida();
                cm.SetDataAtual(dataReferencia);
                cm.SetIdIndice(Convert.ToInt16(this.GetCurrentColumnValue(ListaBenchmarkMetadata.ColumnNames.IdIndice)));
                cm.SetDataInicio(this.calculoMedida.carteira.DataInicioCota.Value);
                //
                bool apenasBenchmark = true;
                List<CalculoMedida.EstatisticaRetornoMensal> retornoIndice = cm.RetornaListaRetornosMensais(this.calculoMedida.carteira.DataInicioCota.Value, this.dataFim, apenasBenchmark);
                //               
                List<decimal?> rentabilidadeIndiceMeses = new List<decimal?>(12);
                //
                for (int i = 0; i < datas.Count; i++) { // Para Cada Data Mes Procura o Retorno no Vetor de retorno Mensal
                    CalculoMedida.EstatisticaRetornoMensal cMensal = retornoIndice.Find(
                                delegate(CalculoMedida.EstatisticaRetornoMensal item) {
                                    return item.Data.Year == datas[i].Year &&
                                           item.Data.Month == datas[i].Month;
                                });

                    if (cMensal != null) {
                        #region Indice
                        if (cMensal.RetornoBenchmark.HasValue) {
                            rentabilidadeIndiceMeses.Add(cMensal.RetornoBenchmark.Value);
                        }
                        else {
                            rentabilidadeIndiceMeses.Add(null);
                        }
                        #endregion
                    }
                    else {
                        rentabilidadeIndiceMeses.Add(null);
                    }
                }

                for (int i = 0; i <= 11; i++) {
                    ReportBase.ConfiguraSinalNegativoDesprezaWebConfig((XRTableCell)xrTableRow0.Cells[i + 5], rentabilidadeIndiceMeses[i], true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
                }
                
                #endregion
            }
            #endregion
        }
    }
}