﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;
using Financial.WebConfigConfiguration;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportGraficoAlocacaoAtivo2
    /// </summary>
    public class SubReportGraficoAlocacaoAtivo2 : XtraReport
    {

        private CalculoMedida calculoMedida;
        
        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private Dictionary<string, decimal> dadosAlocacao = new Dictionary<string, decimal>();

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return this.dadosAlocacao != null ? this.dadosAlocacao.Count != 0 : false;
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRSubreport xrSubreport1;
        private PageFooterBand PageFooter;
        
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private GroupHeaderBand GroupHeader1;
        private XRControlStyle Header2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportGraficoAlocacaoAtivo2()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataFim">Data Fim do Gráfico De Alocação</param>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataFim)
        {
            this.calculoMedida = calculoMedida;
            this.dataFim = dataFim;
            //
            // Inicializa a classe De Dados de Carteira

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            this.GroupHeader1.Visible = false;
            this.xrTable2.Visible = false;
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {
            this.dadosAlocacao.Clear();
            this.dadosAlocacao = this.calculoMedida.RetornaDicAlocacaoAtivoAbreviado2(this.dataFim);

            if (this.HasData)
            {
                this.FillDadosGrafico();
            }
            else
            {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Alocação
        /// </summary>
        private void FillDadosGrafico()
        {

            Series series1 = new Series("Pie Series 1", ViewType.Pie3D);
            if (ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao
                == "P")
            {
                series1 = new Series("Pie Series 1", ViewType.Pie3D);
            }
            if (ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao
                == "D")
            {
                series1 = new Series("Pie Series 1", ViewType.Doughnut3D);
            }

            #region Filter data
            int topNPieChart = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.FiltraTopNPieChart;
            if (topNPieChart > 0)
            {
                
                List<KeyValuePair<string, decimal>> dadosAlocacaoList = new List<KeyValuePair<string, decimal>>(this.dadosAlocacao);
                this.dadosAlocacao = new Dictionary<string, decimal>();

                dadosAlocacaoList.Sort(
                    delegate(KeyValuePair<string, decimal> firstPair,
                    KeyValuePair<string, decimal> nextPair)
                    {
                        return -1 * firstPair.Value.CompareTo(nextPair.Value); //sort descending
                    }
                );

                decimal somaOutros = 0;
                int countPairs = 0;
                foreach (KeyValuePair<string, decimal> pair in dadosAlocacaoList)
                {
                    countPairs++;
                    if (countPairs > topNPieChart)
                    {
                        somaOutros += pair.Value;
                    }
                    else
                    {
                        this.dadosAlocacao.Add(pair.Key, pair.Value);
                    }
                }

                if (somaOutros > 0)
                {
                    this.dadosAlocacao.Add("Outros", somaOutros);
                }

            }
            #endregion

            //
            foreach (KeyValuePair<string, decimal> pair in this.dadosAlocacao)
            {
                string descricao = pair.Key.Trim();
                //if (descricao.Length >= 21) {
                //    descricao = descricao.Substring(0, 20);
                //}
                series1.Points.Add(new SeriesPoint(descricao, pair.Value));
            }

            // Adjust the value numeric options of the series.
            series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
            series1.PointOptions.ValueNumericOptions.Precision = 2;
            //
            series1.PointOptions.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;

            ((PiePointOptions)series1.PointOptions).PercentOptions.ValueAsPercent = true;
            ((PiePointOptions)series1.PointOptions).PercentOptions.PercentageAccuracy = 4;
            //
            
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            ((Pie3DSeriesLabel)series1.Label).Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //((Pie3DSeriesLabel)series1.Label).EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True;
            ((Pie3DSeriesLabel)series1.Label).FillStyle.FillMode = FillMode.Solid;
            //
            ((Pie3DSeriesLabel)series1.Label).MaxWidth = 115;
            ((Pie3DSeriesLabel)series1.Label).MaxLineCount = 3;

            //((Pie3DSeriesLabel)series1.LegendPointOptions).

            string fonteWebConfig = "Times New Roman"; // padrao se vier errado do webconfig
            try {
                fonteWebConfig = WebConfig.AppSettings.TipoFonteRelatorio;
            }
            catch (Exception) {
                fonteWebConfig = "Times New Roman";
            }
            
            //((Pie3DSeriesLabel)series1.Label).Font = new System.Drawing.Font("Tahoma", 7F);
            ((Pie3DSeriesLabel)series1.Label).Font = new Font(fonteWebConfig, 7.5F);
            //    

            ((Pie3DSeriesLabel)series1.Label).ColumnIndent = 0;
            ((Pie3DSeriesLabel)series1.Label).Position = PieSeriesLabelPosition.TwoColumns;
            //((Pie3DSeriesLabel)series1.Label).LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            ((Pie3DSeriesLabel)series1.Label).LineLength = 3;

            ((Pie3DSeriesLabel)series1.Label).ResolveOverlappingMinIndent = 8;

            ((Pie3DSeriesLabel)series1.Label).ResolveOverlappingMode = ResolveOverlappingMode.Default;

            ((SimpleDiagram3D)this.xrChart1.Diagram).ZoomPercent = 100;
            ((SimpleDiagram3D)this.xrChart1.Diagram).RotationType = RotationType.UseAngles;
            ((SimpleDiagram3D)this.xrChart1.Diagram).RotationAngleX = -40;

            // Só Adiciona Serie se houver Dados
            if (this.dadosAlocacao.Count > 0)
            {
                this.xrChart1.Series.Add(series1);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportGraficoAlocacaoAtivo2.resx";
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel1 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel2 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrSubreport1.Visible = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(930F, 35F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "CARTEIRA - ATIVO";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Light";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrChart1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            simpleDiagram3D1.LabelsResolveOverlappingMinIndent = 2;
            simpleDiagram3D1.RotationMatrixSerializable = "0.999999717627107;0.000750860376374229;3.08933774511076E-05;0;-0.0004194352054844" +
                "62;0.591772329797632;-0.806105038912419;0;-0.000623554178864508;0.80610479833243" +
                "7;0.591772477634444;0;0;0;0;1";
            simpleDiagram3D1.ZoomPercent = 110;
            this.xrChart1.Diagram = simpleDiagram3D1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Right;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.EquallySpacedItems = false;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrChart1.Legend.HorizontalIndent = 0;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 15);
            this.xrChart1.Legend.Padding.Bottom = 0;
            this.xrChart1.Legend.Padding.Left = 0;
            this.xrChart1.Legend.Padding.Right = 0;
            this.xrChart1.Legend.Padding.Top = 0;
            this.xrChart1.Legend.TextOffset = 0;
            this.xrChart1.Legend.VerticalIndent = 0;
            this.xrChart1.Legend.Visible = false;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Private";
            this.xrChart1.PaletteRepository.Add("Private", new DevExpress.XtraCharts.Palette("Private", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(216)))), ((int)(((byte)(195))))), System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(216)))), ((int)(((byte)(195)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(39)))), ((int)(((byte)(70)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(151)))), ((int)(((byte)(130))))), System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(151)))), ((int)(((byte)(130)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96))))), System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(133)))), ((int)(((byte)(96)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(103)))), ((int)(((byte)(51))))), System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(103)))), ((int)(((byte)(51)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(208))))), System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(208))))))}));
            pie3DSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            pie3DSeriesLabel1.ColumnIndent = 10;
            pie3DSeriesLabel1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Empty;
            pie3DSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 6F);
            pie3DSeriesLabel1.LineLength = 15;
            pie3DSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pie3DSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            series1.Label = pie3DSeriesLabel1;
            series1.Name = "SeriesAux";
            series1.ShowInLegend = false;
            series1.View = pie3DSeriesView1;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pie3DSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = pie3DSeriesLabel2;
            this.xrChart1.SeriesTemplate.View = pie3DSeriesView2;
            this.xrChart1.SizeF = new System.Drawing.SizeF(930F, 530F);
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            this.xrChart1.CustomDrawSeries += new DevExpress.XtraCharts.CustomDrawSeriesEventHandler(this.xrChart1_CustomDrawSeries);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 16F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart1,
            this.xrLabel1,
            this.xrSubreport1});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 580F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportGraficoAlocacaoAtivo2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 1757, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportGraficoAlocacaoAtivo2.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e)
        {
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            //((PointDrawOptions)e.SeriesDrawOptions).Marker.Kind = MarkerKind.Square;
        }
    }
}