﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportRiscoRetorno
    /// </summary>
    public class SubReportRiscoRetorno : XtraReport
    {

        private CalculoMedida calculoMedida;
        private CalculoMedida.InfoRiscoRetorno infoRiscoRetorno;
        private ReportBase relatorioBase;

        /// <summary>
        /// Retorna true se o Report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {
                return true;
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRLabel xrLabel1;
        private PageFooterBand PageFooter;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell19;
        private XRTable xrTable1;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell25;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableRow xrTableRow15;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableRow xrTableRow16;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableRow xrTableRow18;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell9;
        private XRControlStyle Header2;
        private XRControlStyle Header3;
        private XRControlStyle OddRow;
        private XRControlStyle EvenRow;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell10;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell23;
        private XRTableRow xrTableRow17;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableRow xrTableRow19;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell53;
        private XRTableRow xrTableRow20;
        private XRTableCell xrTableCell61;
        private XRTableRow xrTableRow21;
        private XRTableCell xrTableCell65;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportRiscoRetorno()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="calculoMedida"></param>
        public void PersonalInitialize(CalculoMedida calculoMedida)
        {
            this.calculoMedida = calculoMedida;
            //
            // Configura o Relatorio
            relatorioBase = new ReportBase(this);

            // Faz os Calculos Necessarios e deixa Pronto
            this.PersonalInitialize();

            //
            // Tratamento para Report sem dados
            //this.SetRelatorioSemDados();

            if (calculoMedida.CarteiraSimulada)
            {
                this.xrTableCell22.Visible = false;
            }
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            //// Se não tem dados
            //if (this.numeroLinhasDataTable == 0) {
            //    this.GroupHeader1.Visible = false;
            //    this.GroupFooter1.Visible = false;
            //    this.PageFooter.Visible = false;
            //}
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportRiscoRetorno.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportRiscoRetorno.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Header3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddRow = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenRow = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Expanded = false;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1900F, 38F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "ANÁLISE DE RISCO / RETORNO DA CARTEIRA  *";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 31.58334F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 538F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(671.9792F, 30.875F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "* Rentabilidades líquidas de taxas";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable7,
            this.xrLabel1,
            this.xrTable2});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 576.4167F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(955F, 49F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow18,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow20,
            this.xrTableRow21});
            this.xrTable1.SizeF = new System.Drawing.SizeF(945F, 480F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RentabilidadesCarteiraBeforePrint);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StyleName = "Header3";
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseForeColor = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Rentabilidade (%)";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 1;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell51,
            this.xrTableCell68,
            this.xrTableCell52});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell27.StyleName = "EvenRow";
            this.xrTableCell27.StylePriority.UseBackColor = false;
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseForeColor = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Período";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.18518518205998516;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell51.StyleName = "EvenRow";
            this.xrTableCell51.StylePriority.UseBackColor = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseForeColor = false;
            this.xrTableCell51.Text = "Carteira";
            this.xrTableCell51.Weight = 0.16931215472790262;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell68.CanShrink = true;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell68.Multiline = true;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell68.StyleName = "EvenRow";
            this.xrTableCell68.StylePriority.UseBackColor = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UseForeColor = false;
            this.xrTableCell68.Text = "#Indice";
            this.xrTableCell68.Weight = 0.31746031016818421;
            this.xrTableCell68.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IndiceBeforePrint);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell52.CanShrink = true;
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell52.Multiline = true;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell52.StyleName = "EvenRow";
            this.xrTableCell52.StylePriority.UseBackColor = false;
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseForeColor = false;
            this.xrTableCell52.Text = "#IndDif";
            this.xrTableCell52.Weight = 0.32804235304392815;
            this.xrTableCell52.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IndiceDifBeforePrint);
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell55});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.99999999999999978;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.BackColor = System.Drawing.Color.White;
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell29.StyleName = "OddRow";
            this.xrTableCell29.StylePriority.UseBackColor = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseForeColor = false;
            this.xrTableCell29.Text = "03 Meses";
            this.xrTableCell29.Weight = 0.18518518205998516;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.BackColor = System.Drawing.Color.White;
            this.xrTableCell30.CanGrow = false;
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell30.StyleName = "OddRow";
            this.xrTableCell30.StylePriority.UseBackColor = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseForeColor = false;
            this.xrTableCell30.Text = "Carteira3Meses";
            this.xrTableCell30.Weight = 0.16931217556256931;
            this.xrTableCell30.WordWrap = false;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.BackColor = System.Drawing.Color.White;
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell31.StyleName = "OddRow";
            this.xrTableCell31.StylePriority.UseBackColor = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseForeColor = false;
            this.xrTableCell31.Text = "Benchmark3Meses";
            this.xrTableCell31.Weight = 0.31746031589771756;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.BackColor = System.Drawing.Color.White;
            this.xrTableCell55.CanGrow = false;
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell55.StyleName = "OddRow";
            this.xrTableCell55.StylePriority.UseBackColor = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseForeColor = false;
            this.xrTableCell55.Text = "Diferencial3Meses";
            this.xrTableCell55.Weight = 0.32804232647972814;
            this.xrTableCell55.WordWrap = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell56});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell32.StyleName = "EvenRow";
            this.xrTableCell32.StylePriority.UseBackColor = false;
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseForeColor = false;
            this.xrTableCell32.Text = "06 Meses";
            this.xrTableCell32.Weight = 0.18518518205998516;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell33.CanGrow = false;
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell33.StyleName = "EvenRow";
            this.xrTableCell33.StylePriority.UseBackColor = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseForeColor = false;
            this.xrTableCell33.Text = "Carteira6Meses";
            this.xrTableCell33.Weight = 0.16931217556256931;
            this.xrTableCell33.WordWrap = false;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell34.StyleName = "EvenRow";
            this.xrTableCell34.StylePriority.UseBackColor = false;
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseForeColor = false;
            this.xrTableCell34.Text = "Benchmark6Meses";
            this.xrTableCell34.Weight = 0.31746031589771756;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell56.CanGrow = false;
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell56.StyleName = "EvenRow";
            this.xrTableCell56.StylePriority.UseBackColor = false;
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.StylePriority.UseForeColor = false;
            this.xrTableCell56.Text = "Diferencial6Meses";
            this.xrTableCell56.Weight = 0.32804232647972814;
            this.xrTableCell56.WordWrap = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell57});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.0000000000000002;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.BackColor = System.Drawing.Color.White;
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell35.StyleName = "OddRow";
            this.xrTableCell35.StylePriority.UseBackColor = false;
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseForeColor = false;
            this.xrTableCell35.Text = "12 Meses";
            this.xrTableCell35.Weight = 0.18518518205998516;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.BackColor = System.Drawing.Color.White;
            this.xrTableCell36.CanGrow = false;
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell36.StyleName = "OddRow";
            this.xrTableCell36.StylePriority.UseBackColor = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseForeColor = false;
            this.xrTableCell36.Text = "Carteira12Meses";
            this.xrTableCell36.Weight = 0.16931217556256931;
            this.xrTableCell36.WordWrap = false;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.Color.White;
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell37.StyleName = "OddRow";
            this.xrTableCell37.StylePriority.UseBackColor = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseForeColor = false;
            this.xrTableCell37.Text = "Benchmark12Meses";
            this.xrTableCell37.Weight = 0.31746031589771756;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.BackColor = System.Drawing.Color.White;
            this.xrTableCell57.CanGrow = false;
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell57.StyleName = "OddRow";
            this.xrTableCell57.StylePriority.UseBackColor = false;
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseForeColor = false;
            this.xrTableCell57.Text = "Diferencial12Meses";
            this.xrTableCell57.Weight = 0.32804232647972814;
            this.xrTableCell57.WordWrap = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell58});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.0000000000000004;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell38.StyleName = "EvenRow";
            this.xrTableCell38.StylePriority.UseBackColor = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseForeColor = false;
            this.xrTableCell38.Text = "24 Meses";
            this.xrTableCell38.Weight = 0.18518518205998516;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell39.CanGrow = false;
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell39.StyleName = "EvenRow";
            this.xrTableCell39.StylePriority.UseBackColor = false;
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseForeColor = false;
            this.xrTableCell39.Text = "Carteira24Meses";
            this.xrTableCell39.Weight = 0.16931217556256931;
            this.xrTableCell39.WordWrap = false;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell40.StyleName = "EvenRow";
            this.xrTableCell40.StylePriority.UseBackColor = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseForeColor = false;
            this.xrTableCell40.Text = "Benchmark24Meses";
            this.xrTableCell40.Weight = 0.31746031589771756;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell58.CanGrow = false;
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell58.StyleName = "EvenRow";
            this.xrTableCell58.StylePriority.UseBackColor = false;
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseForeColor = false;
            this.xrTableCell58.Text = "Diferencial24Meses";
            this.xrTableCell58.Weight = 0.32804232647972814;
            this.xrTableCell58.WordWrap = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell59});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.99999999999999956;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.BackColor = System.Drawing.Color.White;
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell41.StyleName = "OddRow";
            this.xrTableCell41.StylePriority.UseBackColor = false;
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UseForeColor = false;
            this.xrTableCell41.Text = "36 Meses";
            this.xrTableCell41.Weight = 0.18518518205998516;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.BackColor = System.Drawing.Color.White;
            this.xrTableCell42.CanGrow = false;
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell42.StyleName = "OddRow";
            this.xrTableCell42.StylePriority.UseBackColor = false;
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UseForeColor = false;
            this.xrTableCell42.Text = "Carteira36Meses";
            this.xrTableCell42.Weight = 0.16931217556256931;
            this.xrTableCell42.WordWrap = false;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.BackColor = System.Drawing.Color.White;
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell43.StyleName = "OddRow";
            this.xrTableCell43.StylePriority.UseBackColor = false;
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseForeColor = false;
            this.xrTableCell43.Text = "Benchmark36Meses";
            this.xrTableCell43.Weight = 0.31746031589771756;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.BackColor = System.Drawing.Color.White;
            this.xrTableCell59.CanGrow = false;
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell59.StyleName = "OddRow";
            this.xrTableCell59.StylePriority.UseBackColor = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseForeColor = false;
            this.xrTableCell59.Text = "Diferencial36Meses";
            this.xrTableCell59.Weight = 0.32804232647972814;
            this.xrTableCell59.WordWrap = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell60});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 1.0000000000000007;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell44.StyleName = "EvenRow";
            this.xrTableCell44.StylePriority.UseBackColor = false;
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseForeColor = false;
            this.xrTableCell44.Text = "Desde o Ínicio";
            this.xrTableCell44.Weight = 0.18518518205998516;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell45.CanGrow = false;
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell45.StyleName = "EvenRow";
            this.xrTableCell45.StylePriority.UseBackColor = false;
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseForeColor = false;
            this.xrTableCell45.Text = "CarteiraRentabilidade";
            this.xrTableCell45.Weight = 0.16931217556256931;
            this.xrTableCell45.WordWrap = false;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell46.StyleName = "EvenRow";
            this.xrTableCell46.StylePriority.UseBackColor = false;
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseForeColor = false;
            this.xrTableCell46.Text = "BenchmarkRentabilidade";
            this.xrTableCell46.Weight = 0.31746031589771756;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell60.CanGrow = false;
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell60.StyleName = "EvenRow";
            this.xrTableCell60.StylePriority.UseBackColor = false;
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UseForeColor = false;
            this.xrTableCell60.Text = "DiferencialInicio";
            this.xrTableCell60.Weight = 0.32804232647972814;
            this.xrTableCell60.WordWrap = false;
            // 
            // xrTable7
            // 
            this.xrTable7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 49F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow8,
            this.xrTableRow19,
            this.xrTableRow4,
            this.xrTableRow9,
            this.xrTableRow17});
            this.xrTable7.SizeF = new System.Drawing.SizeF(945F, 480F);
            this.xrTable7.StylePriority.UseBackColor = false;
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell28,
            this.xrTableCell26});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.83905008181924068;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(34, 5, 0, 0, 254F);
            this.xrTableCell9.StyleName = "Header3";
            this.xrTableCell9.StylePriority.UseBackColor = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseForeColor = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Benchmark";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.447384365932753;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StyleName = "Header3";
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseForeColor = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "Qtd.";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 0.24729476019798602;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StyleName = "Header3";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "%";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.30532087386926104;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.83905008181924079;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(35, 5, 0, 0, 254F);
            this.xrTableCell1.StyleName = "EvenRow";
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseForeColor = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Meses Acima do BenchMark";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.447384365932753;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StyleName = "EvenRow";
            this.xrTableCell2.StylePriority.UseBackColor = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseForeColor = false;
            this.xrTableCell2.Text = "#MesesAcimaBenchMark";
            this.xrTableCell2.Weight = 0.24729476019798602;
            this.xrTableCell2.WordWrap = false;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MesesAcimaBenchMark);
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StyleName = "EvenRow";
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.Text = "#PorcentagemAcima";
            this.xrTableCell3.Weight = 0.30532087386926104;
            this.xrTableCell3.WordWrap = false;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PorcentagemAcimaBenchMark);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 0.83905008181924057;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BackColor = System.Drawing.Color.White;
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(35, 5, 0, 0, 254F);
            this.xrTableCell4.StyleName = "OddRow";
            this.xrTableCell4.StylePriority.UseBackColor = false;
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseForeColor = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Meses Abaixo do BenchMark";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.447384365932753;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BackColor = System.Drawing.Color.White;
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell5.CanGrow = false;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StyleName = "OddRow";
            this.xrTableCell5.StylePriority.UseBackColor = false;
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseForeColor = false;
            this.xrTableCell5.Text = "#MesesAbaixoBenchMark";
            this.xrTableCell5.Weight = 0.24729476019798602;
            this.xrTableCell5.WordWrap = false;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MesesAbaixoBenchMark);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.White;
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StyleName = "OddRow";
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseForeColor = false;
            this.xrTableCell6.Text = "#PorcentagemAbaixo";
            this.xrTableCell6.Weight = 0.30532087386926104;
            this.xrTableCell6.WordWrap = false;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PorcentagemAbaixoBenchMark);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.839050081819241;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(34, 5, 0, 0, 254F);
            this.xrTableCell11.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
            this.xrTableCell11.StyleName = "Header3";
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "Rentabilidade";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.447384365932753;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StyleName = "Header3";
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseForeColor = false;
            this.xrTableCell12.Text = "Rentab.";
            this.xrTableCell12.Weight = 0.24729476019798602;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StyleName = "Header3";
            this.xrTableCell13.StylePriority.UseBackColor = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseForeColor = false;
            this.xrTableCell13.Text = "Mês";
            this.xrTableCell13.Weight = 0.30532087386926104;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.83905008181924079;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(35, 5, 0, 0, 254F);
            this.xrTableCell14.StyleName = "EvenRow";
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Maior Rentabilidade da Carteira";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.447384365932753;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell15.CanGrow = false;
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StyleName = "EvenRow";
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseForeColor = false;
            this.xrTableCell15.Text = "#MaiorRentabilidade";
            this.xrTableCell15.Weight = 0.24729476019798602;
            this.xrTableCell15.WordWrap = false;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MaiorRentabilidadeCarteira);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell16.CanGrow = false;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StyleName = "EvenRow";
            this.xrTableCell16.StylePriority.UseBackColor = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseForeColor = false;
            this.xrTableCell16.Text = "#MesMaiorRentabilidade";
            this.xrTableCell16.Weight = 0.30532087386926104;
            this.xrTableCell16.WordWrap = false;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MesMaiorRentabilidadeCarteira);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 0.83905008076435916;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.White;
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(35, 5, 0, 0, 254F);
            this.xrTableCell17.StyleName = "OddRow";
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseForeColor = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Menor Rentabilidade da Carteira";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 0.447384365932753;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.White;
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell18.CanGrow = false;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StyleName = "OddRow";
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.Text = "#MenorRentabilidade";
            this.xrTableCell18.Weight = 0.24729476019798602;
            this.xrTableCell18.WordWrap = false;
            this.xrTableCell18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MenorRentabilidadeCarteira);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.White;
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell19.CanGrow = false;
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StyleName = "OddRow";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.Text = "#MesMenorRentabilidade";
            this.xrTableCell19.Weight = 0.30532087386926104;
            this.xrTableCell19.WordWrap = false;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MesMenorRentabilidadeCarteira);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 0F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // Header3
            // 
            this.Header3.Name = "Header3";
            this.Header3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // OddRow
            // 
            this.OddRow.Name = "OddRow";
            this.OddRow.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // EvenRow
            // 
            this.EvenRow.Name = "EvenRow";
            this.EvenRow.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell10});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.83905008076435916;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StyleName = "EvenRow";
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseForeColor = false;
            this.xrTableCell7.Text = "03 Meses";
            this.xrTableCell7.Weight = 0.2236921829663765;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StyleName = "EvenRow";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.Text = "06 Meses";
            this.xrTableCell8.Weight = 0.24729476019798602;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StyleName = "EvenRow";
            this.xrTableCell10.StylePriority.UseBackColor = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseForeColor = false;
            this.xrTableCell10.Text = "12 Meses";
            this.xrTableCell10.Weight = 0.30532087386926104;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell23});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.839050080764359;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.BackColor = System.Drawing.Color.White;
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StyleName = "OddRow";
            this.xrTableCell20.StylePriority.UseBackColor = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseForeColor = false;
            this.xrTableCell20.Text = "Vol3Meses";
            this.xrTableCell20.Weight = 0.2236921829663765;
            this.xrTableCell20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Volatilidade3MesesBeforePrint);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.BackColor = System.Drawing.Color.White;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StyleName = "OddRow";
            this.xrTableCell21.StylePriority.UseBackColor = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseForeColor = false;
            this.xrTableCell21.Text = "Vol6Meses";
            this.xrTableCell21.Weight = 0.24729476019798602;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Volatilidade6MesesBeforePrint);
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.White;
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StyleName = "OddRow";
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseForeColor = false;
            this.xrTableCell23.Text = "Vol12Meses";
            this.xrTableCell23.Weight = 0.30532087386926104;
            this.xrTableCell23.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Volatilidade12MesesBeforePrint);
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell24,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.839050080764359;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StyleName = "EvenRow";
            this.xrTableCell24.StylePriority.UseBackColor = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseForeColor = false;
            this.xrTableCell24.Text = "Sharpe3Meses";
            this.xrTableCell24.Weight = 0.2236921829663765;
            this.xrTableCell24.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Sharpe3MesesBeforePrint);
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StyleName = "EvenRow";
            this.xrTableCell47.StylePriority.UseBackColor = false;
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseForeColor = false;
            this.xrTableCell47.Text = "Sharpe6Meses";
            this.xrTableCell47.Weight = 0.24729476019798602;
            this.xrTableCell47.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Sharpe6MesesBeforePrint);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StyleName = "EvenRow";
            this.xrTableCell48.StylePriority.UseBackColor = false;
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseForeColor = false;
            this.xrTableCell48.Text = "Sharpe12Meses";
            this.xrTableCell48.Weight = 0.30532087386926104;
            this.xrTableCell48.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Sharpe12MesesBeforePrint);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StyleName = "EvenRow";
            this.xrTableCell49.StylePriority.UseBackColor = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseForeColor = false;
            this.xrTableCell49.Weight = 0.2236921829663765;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.BackColor = System.Drawing.Color.White;
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StyleName = "OddRow";
            this.xrTableCell50.StylePriority.UseBackColor = false;
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UseForeColor = false;
            this.xrTableCell50.Text = "Volatilidade";
            this.xrTableCell50.Weight = 0.2236921829663765;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StyleName = "EvenRow";
            this.xrTableCell53.StylePriority.UseBackColor = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseForeColor = false;
            this.xrTableCell53.Text = "Sharpe";
            this.xrTableCell53.Weight = 0.2236921829663765;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.839050080764359;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(189)))));
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StyleName = "Header3";
            this.xrTableCell54.StylePriority.UseBackColor = false;
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseForeColor = false;
            this.xrTableCell54.Text = "Volatilidade(%) / Sharpe";
            this.xrTableCell54.Weight = 1;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StyleName = "OddRow";
            this.xrTableRow20.Weight = 1.0000000000000007;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.BackColor = System.Drawing.Color.White;
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StyleName = "OddRow";
            this.xrTableCell61.StylePriority.UseBackColor = false;
            this.xrTableCell61.Weight = 1.0000000000000002;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1.0000000000000007;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(245)))), ((int)(((byte)(215)))));
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StyleName = "EvenRow";
            this.xrTableCell65.StylePriority.UseBackColor = false;
            this.xrTableCell65.Weight = 1.0000000000000002;
            // 
            // SubReportRiscoRetorno
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(99, 752, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2,
            this.Header3,
            this.OddRow,
            this.EvenRow});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportRiscoRetorno.ResourceManager;
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e)
        {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal? valor = null;
            try
            {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1)
            {
                // Não faz nada
            }


            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
        }

        /// <summary>
        /// modifies: this.infoRiscoRetorno
        /// </summary>
        public void PersonalInitialize()
        {
            this.infoRiscoRetorno = this.calculoMedida.RetornaInfoRiscoRetorno();
        }

        #region Funções Internas para Preenchimento da Tabela 1
        private void MesesAcimaBenchMark(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMesesAcima = sender as XRTableCell;
            xrTableMesesAcima.Text = this.infoRiscoRetorno.MesesAcimaBenchMark.ToString("n0");
        }

        private void MesesAbaixoBenchMark(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMesesAbaixo = sender as XRTableCell;
            xrTableMesesAbaixo.Text = this.infoRiscoRetorno.MesesAbaixoBenchMark.ToString("n0");
        }

        private void PorcentagemAcimaBenchMark(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTablePorcentagemAcima = sender as XRTableCell;
            xrTablePorcentagemAcima.Text = this.infoRiscoRetorno.PercentualAcimaBenchMark.ToString("P2");
        }

        private void PorcentagemAbaixoBenchMark(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTablePorcentagemAbaixo = sender as XRTableCell;
            xrTablePorcentagemAbaixo.Text = this.infoRiscoRetorno.PercentualAbaixoBenchMark.ToString("P2");
        }

        private void MaiorRentabilidadeCarteira(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMaiorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(xrTableMaiorRentabilidadeCarteira, this.infoRiscoRetorno.MaiorRentabilidadeCarteira / 100M, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
        }

        private void MenorRentabilidadeCarteira(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.MenorRentabilidadeCarteira / 100M, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
        }

        private void MesMaiorRentabilidadeCarteira(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMesMaiorRentabilidadeCarteira = sender as XRTableCell;
            xrTableMesMaiorRentabilidadeCarteira.Text = this.infoRiscoRetorno.MesMaiorRentabilidade;
        }

        private void MesMenorRentabilidadeCarteira(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell xrTableMesMenorRentabilidadeCarteira = sender as XRTableCell;
            xrTableMesMenorRentabilidadeCarteira.Text = this.infoRiscoRetorno.MesMenorRentabilidade;
        }

        /* Volatilidades/ Sharpes */
        #region Volatidades
        private void Volatilidade3MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTable3MesesVolatilidade = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;

            if (this.infoRiscoRetorno.Vol3Meses.HasValue) {
                ReportBase.ConfiguraSinalNegativo(xrTable3MesesVolatilidade, this.infoRiscoRetorno.Vol3Meses.Value / 100M, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
            else {
                ReportBase.ConfiguraSinalNegativo(xrTable3MesesVolatilidade, this.infoRiscoRetorno.Vol3Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
        }

        private void Volatilidade6MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;

            if (this.infoRiscoRetorno.Vol6Meses.HasValue) {
                ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Vol6Meses.Value / 100M, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
            else {
                ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Vol6Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
        }

        private void Volatilidade12MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;

            if (this.infoRiscoRetorno.Vol12Meses.HasValue) {
                ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Vol12Meses.Value / 100M, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
            else {
                ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Vol12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem, positivoColor, negativoColor);
            }
        }
        #endregion

        #region Sharpes
        private void Sharpe3MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Sharpe3Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
        }

        private void Sharpe6MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Sharpe6Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
        }

        private void Sharpe12MesesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableMenorRentabilidadeCarteira = sender as XRTableCell;
            // Pode ser Vermelho
            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            ReportBase.ConfiguraSinalNegativo(xrTableMenorRentabilidadeCarteira, this.infoRiscoRetorno.Sharpe12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
        }
        #endregion

        #endregion

        #region Funções Internas para Preenchimento da Tabela 2
        private void RentabilidadesCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // Limpa os dados da tabela
            //ReportBase.LimpaDadosTable((XRTable)sender);

            Color positivoColor = this.relatorioBase.getControlStyle(this.relatorioBase.POSITIVO_NAME).ForeColor;
            Color negativoColor = this.relatorioBase.getControlStyle(this.relatorioBase.NEGATIVO_NAME).ForeColor;
            XRTable rentabilidades = sender as XRTable;

            #region Linha 1 - coluna 3 - Titulo
            Indice indice = new Indice();
            indice.LoadByPrimaryKey((short)this.calculoMedida.indice.IdIndice.Value);
            string textoIndice = indice.Tipo == (byte)TipoIndice.Decimal ? "(-) Indx" : "% Indx";
            //
            XRTableRow rentabilidadeRow1 = rentabilidades.Rows[1];
            ((XRTableCell)rentabilidadeRow1.Cells[3]).Text = textoIndice;
            #endregion

            #region Linha 2 - coluna 1, 2, 3 - Rentabilidade 3 Meses
            XRTableRow rentabilidadeRow2 = rentabilidades.Rows[2];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow2.Cells[1], this.infoRiscoRetorno.rentabCarteira3Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow2.Cells[2], this.infoRiscoRetorno.rentabBenchMark3Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow2.Cells[3], this.infoRiscoRetorno.rentabDiferencial3Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion

            #region Linha 3 - coluna 1, 2, 3 - Rentabilidade 6 Meses
            XRTableRow rentabilidadeRow3 = rentabilidades.Rows[3];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow3.Cells[1], this.infoRiscoRetorno.rentabCarteira6Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow3.Cells[2], this.infoRiscoRetorno.rentabBenchMark6Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow3.Cells[3], this.infoRiscoRetorno.rentabDiferencial6Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion

            #region Linha 4 - coluna 1, 2, 3 - Rentabilidade 12 Meses
            XRTableRow rentabilidadeRow4 = rentabilidades.Rows[4];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow4.Cells[1], this.infoRiscoRetorno.rentabCarteira12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow4.Cells[2], this.infoRiscoRetorno.rentabBenchMark12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow4.Cells[3], this.infoRiscoRetorno.rentabDiferencial12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion

            #region Linha 5 - coluna 1, 2, 3 - Rentabilidade 24 Meses
            XRTableRow rentabilidadeRow5 = rentabilidades.Rows[5];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow5.Cells[1], this.infoRiscoRetorno.rentabCarteira24Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow5.Cells[2], this.infoRiscoRetorno.rentabBenchMark24Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow5.Cells[3], this.infoRiscoRetorno.rentabDiferencial24Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion

            #region Linha 6 - coluna 1, 2, 3 - Rentabilidade 36 Meses
            XRTableRow rentabilidadeRow6 = rentabilidades.Rows[6];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow6.Cells[1], this.infoRiscoRetorno.rentabCarteira36Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow6.Cells[2], this.infoRiscoRetorno.rentabBenchMark36Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow6.Cells[3], this.infoRiscoRetorno.rentabDiferencial36Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion

            #region Linha 7 - coluna 1, 2, 3 - Rentabilidade Desde o Ínicio
            XRTableRow rentabilidadeRow7 = rentabilidades.Rows[7];
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow7.Cells[1], this.infoRiscoRetorno.rentabCarteiraDesdeInicio, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow7.Cells[2], this.infoRiscoRetorno.rentabBenchMarkDesdeInicio, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)rentabilidadeRow7.Cells[3], this.infoRiscoRetorno.rentabDiferencialDesdeInicio, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais, positivoColor, negativoColor);
            #endregion
        }
        #endregion

        private void IndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableIndice = sender as XRTableCell;
            //
            string textoIndiceDescricao = this.calculoMedida.indice.Descricao.Trim();
            //
            xrTableIndice.Text = textoIndiceDescricao;
        }

        private void IndiceDifBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableIndice = sender as XRTableCell;
            //
            string prefixoIndice = (this.calculoMedida.indice.Tipo == (byte)TipoIndice.Decimal ? "(-) " : "% ");
            string textoIndiceDifDescricao = prefixoIndice + this.calculoMedida.indice.Descricao.Trim();
            //
            xrTableIndice.Text = textoIndiceDifDescricao;
        }
    }
}