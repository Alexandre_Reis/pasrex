﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportGraficoRetornoAcumulado2
    /// </summary>
    public class SubReportGraficoRetornoAcumulado2 : XtraReport
    {

        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int idCarteira;

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int numeroLinhasDataTable;

        private DateTime dataInicioCliente;

        private ListaBenchmarkCollection listaBenchmarkCollection1;

        // Carteira que será gerado o grafico
        private CalculoMedida calculoMedida;

        #region Classe Interna que Contem o IdCarteira E os Dados de Rentabilidade da Carteira
        protected class DadosCarteira
        {
            //
            public int idCarteira;
            public string nomeFundo;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeFundo = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosCarteira[] dadosCarteira;
        #endregion

        #region Classe Interna que Contem o IdIndice E os Dados de Rentabilidade do Indice
        protected class DadosIndice
        {
            public int idIndice;
            public string nomeIndice;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeIndice = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosIndice[] dadosIndice;
        #endregion

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        public bool HasData
        {
            get
            {

                if (this.dadosCarteira == null && dadosIndice == null)
                {
                    return false;
                }
                else
                {

                    bool retorno = false;

                    for (int i = 0; i < this.dadosCarteira.Length; i++)
                    {
                        if (this.dadosCarteira[i].dadosRentabilidadeFundo.Count != 0)
                        {
                            retorno = true;
                            break;
                        }
                    }
                    if (!retorno)
                    {
                        for (int i = 0; i < this.dadosIndice.Length; i++)
                        {
                            if (this.dadosIndice[i].dadosRentabilidadeIndice.Count != 0)
                            {
                                retorno = true;
                                break;
                            }
                        }
                    }
                    return retorno;

                }
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private PageFooterBand PageFooter;
        
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private GroupHeaderBand GroupHeader1;
        private XRControlStyle Header2;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell26;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell28;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportGraficoRetornoAcumulado2()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira">idCarteira a ser colocado no gráfico</param>
        /// <param name="idIndice">indice a ser colocado no gráfico</param>
        /// <param name="dataInicio">Data Inicio do Gráfico De Rentabilidade</param>
        /// <param name="dataFim">Data Fim do Gráfico De Rentabilidade</param>
        public void PersonalInitialize(CalculoMedida calculoMedida, DateTime dataInicio, DateTime dataFim)
        {
            this.calculoMedida = calculoMedida;
            //
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            // Inicializa a classe De Dados de Carteira
            this.dadosCarteira = new DadosCarteira[1];

            int idCarteira = this.calculoMedida.carteira.IdCarteira.Value;

            this.idCarteira = idCarteira;

            this.dataInicioCliente = this.calculoMedida.carteira.DataInicioCota.Value;

            this.dadosCarteira[0] = new DadosCarteira();
            this.dadosCarteira[0].idCarteira = idCarteira;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Apelido);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            //
            this.dadosCarteira[0].nomeFundo = carteira.Apelido.Trim();
            //

            int idIndice = this.calculoMedida.indice.IdIndice.Value;

            // Inicializa a classe De Dados de Indice
            this.dadosIndice = new DadosIndice[1];
            this.dadosIndice[0] = new DadosIndice();
            this.dadosIndice[0].idIndice = idIndice;

            Indice indice = new Indice();
            campos = new List<esQueryItem>();
            campos.Add(indice.Query.Descricao);
            indice.LoadByPrimaryKey(campos, (short)idIndice);
            //
            this.dadosIndice[0].nomeIndice = indice.Descricao.Trim();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();
        }

        /// <summary>
        /// Se Relatorio não tem Dados Desaparece com Tudo
        /// </summary>
        private void SetRelatorioSemDados()
        {
            this.GroupHeader1.Visible = false;
            this.xrTable2.Visible = false;
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>
        public void PersonalInitialize()
        {
            //
            this.CarregaDadosRentabilidade();
            //
            if (this.HasData)
            {
                this.FillDadosGrafico();

                //
                // Consulta do SubRelatorio
                DataTable dt = this.FillDados();
                this.DataSource = dt;
                this.numeroLinhasDataTable = dt.Rows.Count;
            }
            else
            {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Carrega num Dicionario uma lista de Datas com as Respectivas Rentabilidades Dos Fundos e Indices
        /// </summary>
        private void CarregaDadosRentabilidade()
        {

            #region Limpa os Dicionarios
            // Limpa o Dicionario de Carteira
            for (int i = 0; i < this.dadosCarteira.Length; i++)
            {
                this.dadosCarteira[i].dadosRentabilidadeFundo.Clear();
            }

            // Limpa o Dicionario de Indice
            for (int i = 0; i < this.dadosIndice.Length; i++)
            {
                this.dadosIndice[i].dadosRentabilidadeIndice.Clear();
            }
            #endregion



            CalculoMedida.DicsEstatisticaRetornoAcumulado dicsRetornosAcumulados = this.calculoMedida.RetornaDicsRetornosAcumulados(this.DataInicio, this.dataFim);
            // Só tem 1 Carteira
            for (int j = 0; j < this.dadosCarteira.Length; j++)
            {
                this.dadosCarteira[j].dadosRentabilidadeFundo = dicsRetornosAcumulados.retornoAcumulado;
            }

            #region Dados do Indice
            for (int j = 0; j < this.dadosIndice.Length; j++)
            {
                this.dadosIndice[j].dadosRentabilidadeIndice = dicsRetornosAcumulados.retornoBenchmarkAcumulado;
            }
            #endregion
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico de Rentabilidade
        /// </summary>
        private void FillDadosGrafico()
        {

            #region Adiciona N Série de Fundos
            //
            for (int i = 0; i < this.dadosCarteira.Length; i++)
            {
                //Series series1 = new Series(this.dadosCarteira[i].nomeFundo, ViewType.Line);
                Series series1 = new Series("Carteira", ViewType.Line);
                //
                foreach (KeyValuePair<DateTime, decimal> pair in this.dadosCarteira[i].dadosRentabilidadeFundo)
                {
                    series1.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
                }
                series1.ArgumentScaleType = ScaleType.DateTime;
                series1.Label.Visible = false;
                series1.View.Color = Color.FromArgb(8, 33, 40);
                ((LineSeriesView)series1.View).LineMarkerOptions.Size = 1;
                //
                // Só Adiciona Serie se houver Dados
                if (this.dadosCarteira[i].dadosRentabilidadeFundo.Count > 0)
                {
                    this.xrChart1.Series.Add(series1);
                }
            }
            #endregion

            #region Adiciona N Série de Indices
            for (int i = 0; i < this.dadosIndice.Length; i++)
            {
                Series series2 = new Series(this.dadosIndice[i].nomeIndice, ViewType.Line);
                //
                foreach (KeyValuePair<DateTime, decimal> pair in this.dadosIndice[i].dadosRentabilidadeIndice)
                {
                    series2.Points.Add(new SeriesPoint(pair.Key, new object[] { pair.Value }));
                }
                series2.ArgumentScaleType = ScaleType.DateTime;
                series2.Label.Visible = false;
                series2.View.Color = Color.FromArgb(200, 193, 166);
                ((LineSeriesView)series2.View).LineMarkerOptions.Size = 1;
                //
                // Só Adiciona Serie se houver Dados
                if (this.dadosIndice[i].dadosRentabilidadeIndice.Count > 0)
                {
                    this.xrChart1.Series.Add(series2);
                }
            }
            #endregion

            this.xrChart1.Series[0].ShowInLegend = false;
            this.xrChart1.Series[1].ShowInLegend = false;

            ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Day;
            ((XYDiagram)this.xrChart1.Diagram).AxisX.DateTimeOptions.FormatString = (this.dataFim - this.DataInicio).TotalDays > 60 ? "MMM/yy" : "dd/MMM";
        }

        private DataTable FillDados()
        {
            #region SQL
            this.listaBenchmarkCollection1.QueryReset();
            this.listaBenchmarkCollection1.Query
                 .Select(this.listaBenchmarkCollection1.Query.IdIndice)
                 .Where(this.listaBenchmarkCollection1.Query.IdCarteira == this.IdCarteira);
            #endregion
            //
            return this.listaBenchmarkCollection1.Query.LoadDataTable();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/SubReportGraficoRetornoAcumulado2.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportGraficoRetornoAcumulado2.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel3 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            this.listaBenchmarkCollection1 = new Financial.Fundo.ListaBenchmarkCollection();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.Header2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // listaBenchmarkCollection1
            // 
            this.listaBenchmarkCollection1.AllowDelete = true;
            this.listaBenchmarkCollection1.AllowEdit = true;
            this.listaBenchmarkCollection1.AllowNew = true;
            this.listaBenchmarkCollection1.EnableHierarchicalBinding = true;
            this.listaBenchmarkCollection1.Filter = "";
            this.listaBenchmarkCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.listaBenchmarkCollection1.Sort = "";
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrSubreport1.Visible = false;
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 21.125F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Terracotta Pie";
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrChart1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrChart1.DataSource = this.listaBenchmarkCollection1;
            xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "MMM/yy";
            xyDiagram1.AxisX.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Angle = 270;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Times New Roman", 7F);
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Title.Text = "";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisY.Label.EndText = "%";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Times New Roman", 7F);
            xyDiagram1.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.BackColor = System.Drawing.Color.White;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYZooming = true;
            xyDiagram1.Margins.Top = 2;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrChart1.Legend.Margins.Bottom = 1;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 1);
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 186F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteRepository.Add("Private", new DevExpress.XtraCharts.Palette("Private", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(193)))), ((int)(((byte)(166))))), System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(193)))), ((int)(((byte)(166)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(52)))), ((int)(((byte)(58))))), System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(52)))), ((int)(((byte)(58))))))}));
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "CDIAAux";
            lineSeriesView1.LineMarkerOptions.Size = 1;
            lineSeriesView1.LineMarkerOptions.Visible = false;
            series1.View = lineSeriesView1;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel2.Visible = false;
            series2.Label = pointSeriesLabel2;
            series2.Name = "FIC DE FIM GPAR";
            lineSeriesView2.LineMarkerOptions.Size = 1;
            lineSeriesView2.LineMarkerOptions.Visible = false;
            series2.View = lineSeriesView2;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = pointSeriesLabel3;
            lineSeriesView3.ColorEach = true;
            this.xrChart1.SeriesTemplate.View = lineSeriesView3;
            this.xrChart1.SizeF = new System.Drawing.SizeF(930F, 530F);
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(92.06253F, 715.9999F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(500F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell22.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLabel1,
            this.xrSubreport1,
            this.xrChart1,
            this.xrTable2});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 731.0416F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Transparent;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 47.99998F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow5,
            this.xrTableRow9});
            this.xrTable1.SizeF = new System.Drawing.SizeF(930F, 138F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableRentabilidadeBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell37,
            this.xrTableCell40,
            this.xrTableCell48,
            this.xrTableCell3,
            this.xrTableCell26});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.24864864864864866;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseBackColor = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell34.Weight = 0.161290320760278;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseBackColor = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.Text = "Mês";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell37.Weight = 0.16774194345108456;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseBackColor = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UsePadding = false;
            this.xrTableCell40.Text = "Ano";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell40.Weight = 0.16774193584554176;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell48.StylePriority.UseBackColor = false;
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UsePadding = false;
            this.xrTableCell48.Text = "6 Meses";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell48.Weight = 0.16774194061867026;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "12 Meses";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.16774192910777017;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Ínicio";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.16774193021665543;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell6,
            this.xrTableCell27});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.24864864864864866;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.161290320760278;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.16774194345108456;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.16774193584554176;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.16774194061867026;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.16774192910777017;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseBackColor = false;
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.16774193021665543;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell1,
            this.xrTableCell23,
            this.xrTableCell25,
            this.xrTableCell16,
            this.xrTableCell28});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.24864864864864866;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.161290320760278;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseBackColor = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 0.16774194345108456;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.16774193584554176;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell25.Weight = 0.16774194061867026;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseBackColor = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.16774192910777017;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Arial", 7F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 10, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.16774193021665543;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(211)))), ((int)(((byte)(159)))));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(50)))), ((int)(((byte)(82)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 13F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(930F, 35F);
            this.xrLabel1.StyleName = "Header2";
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "RETORNO ACUMULADO";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Header2
            // 
            this.Header2.Name = "Header2";
            this.Header2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportGraficoRetornoAcumulado2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.listaBenchmarkCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 1757, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Header2});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportGraficoRetornoAcumulado2.ResourceManager;
        }

        /// <summary>
        /// Define o marcador de cada ponto como invisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrChart1_CustomDrawSeries(object sender, CustomDrawSeriesEventArgs e)
        {
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Size = 1;
            ((PointDrawOptions)e.SeriesDrawOptions).Marker.Kind = MarkerKind.Square;
        }

        private List<decimal?> rentabilidadeCotaTable = new List<decimal?>(6); 

        private void TableRentabilidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable xrTable = sender as XRTable;
            //
            #region Limpa Valores da Tabela
            // Limpa somente as linhas 2,3
            for (int i = 2; i < xrTable.Rows.Count; i++)
            {
                int colunas = ((XRTableRow)xrTable.Rows[i]).Cells.Count;
                for (int j = 0; j < colunas; j++)
                {
                    ((XRTableCell)xrTable.Rows[i].Cells[j]).Text = "";
                }
            }
            #endregion

            /* Exibe Cabeçalho */
            #region Exibe Cabeçalho
            XRTableRow xrTableRow0 = xrTable.Rows[0];
            //
            ((XRTableCell)xrTableRow0.Cells[1]).Text = Resources.SubReportCarteiraResumo._Mes;
            ((XRTableCell)xrTableRow0.Cells[2]).Text = Resources.SubReportCarteiraResumo._Ano;
            ((XRTableCell)xrTableRow0.Cells[3]).Text = Resources.SubReportCarteiraResumo._6Meses;
            ((XRTableCell)xrTableRow0.Cells[4]).Text = Resources.SubReportCarteiraResumo._12Meses;
            ((XRTableCell)xrTableRow0.Cells[5]).Text = Resources.SubReportCarteiraResumo._Inicial;
            #endregion


            #region Calcula datas dos periodos
            DateTime? dataAnterior = Calendario.SubtraiDiaUtil(DataFim, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(DataFim, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(DataFim, -1);
            DateTime? data6Meses = Calendario.RetornaDiaMesAnterior(DataFim, 6);
            DateTime? data12Meses = Calendario.RetornaDiaMesAnterior(DataFim, 12);

            if (dataAnterior < this.dataInicioCliente)
            {
                dataAnterior = null;
            }
            if (dataMes < this.dataInicioCliente)
            {
                dataMes = null;
            }
            if (dataAno < this.dataInicioCliente)
            {
                dataAno = null;
            }

            if (data6Meses < this.dataInicioCliente)
            {
                data6Meses = null;
            }

            if (data12Meses < this.dataInicioCliente)
            {
                data12Meses = null;
            }
            #endregion

            List<decimal?> rentabilidadeCota = new List<decimal?>();
            List<decimal?> rentabilidadeIndice = new List<decimal?>();
            List<decimal?> rentabilidadeDiferencial = new List<decimal?>();
            //
            CalculoMedida calculoMedida = new CalculoMedida();
            calculoMedida.SetDataInicio(this.dataInicioCliente);

            calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");
            //                                                                       
            #region Calcula Rentabilidade Cota
            calculoMedida.SetIdCarteira(this.idCarteira);

            #region rentabilidadeMes
            decimal? rentabilidadeCotaMes = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeCotaMes = calculoMedida.CalculaRetornoMes(this.DataFim);
                    rentabilidadeCotaMes = rentabilidadeCotaMes / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeCotaAno = null;
            if (dataAno != null)
            {
                try
                {
                    rentabilidadeCotaAno = calculoMedida.CalculaRetornoAno(this.DataFim);
                    rentabilidadeCotaAno = rentabilidadeCotaAno / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeCota6Meses = null;
            if (data6Meses != null)
            {
                try
                {
                    rentabilidadeCota6Meses = calculoMedida.CalculaRetornoPeriodoMes(this.DataFim, 6);
                    rentabilidadeCota6Meses = rentabilidadeCota6Meses / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeCota12Meses = null;
            if (data12Meses != null)
            {
                try
                {
                    rentabilidadeCota12Meses = calculoMedida.CalculaRetornoPeriodoMes(this.DataFim, 12);
                    rentabilidadeCota12Meses = rentabilidadeCota12Meses / 100;
                }
                catch (HistoricoCotaNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeInicio
            decimal? rentabilidadeCotaInicio = null;
            try
            {
                rentabilidadeCotaInicio = calculoMedida.CalculaRetorno(this.dataInicioCliente, this.DataFim);
                rentabilidadeCotaInicio = rentabilidadeCotaInicio / 100;
            }
            catch (HistoricoCotaNaoCadastradoException) { }
            #endregion

            rentabilidadeCota.Add(rentabilidadeCotaMes);
            rentabilidadeCota.Add(rentabilidadeCotaAno);
            rentabilidadeCota.Add(rentabilidadeCota6Meses);
            rentabilidadeCota.Add(rentabilidadeCota12Meses);
            rentabilidadeCota.Add(rentabilidadeCotaInicio);

            // Copia o valor na variável Global rentabilidadeCotaTable
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[0]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[1]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[2]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[3]);
            this.rentabilidadeCotaTable.Add(rentabilidadeCota[4]);

            #endregion

            /* Exibe Rentabilidade Cota */
            #region Exibe Rentabilidade Cota
            XRTableRow xrTableRow1 = xrTable.Rows[1];
            //
            ((XRTableCell)xrTableRow1.Cells[0]).Text = Resources.SubReportCarteiraResumo._Carteira;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow1.Cells[1], rentabilidadeCota[0], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow1.Cells[2], rentabilidadeCota[1], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow1.Cells[3], rentabilidadeCota[2], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow1.Cells[4], rentabilidadeCota[3], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow1.Cells[5], rentabilidadeCota[4], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            #endregion

            #region Calcula Rentabilidade Indice
            // Pega o Indice
            int idIndice = Convert.ToInt16(this.calculoMedida.carteira.IdIndiceBenchmark);
            calculoMedida.SetIdIndice(idIndice);

            #region rentabilidadeMes
            decimal? rentabilidadeIndiceMes = null;
            if (dataMes != null)
            {
                try
                {
                    rentabilidadeIndiceMes = calculoMedida.CalculaRetornoMesIndice(this.DataFim);
                    rentabilidadeIndiceMes = rentabilidadeIndiceMes / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeAno
            decimal? rentabilidadeIndiceAno = null;
            if (dataAno != null)
            {
                try
                {
                    rentabilidadeIndiceAno = calculoMedida.CalculaRetornoAnoIndice(this.DataFim);
                    rentabilidadeIndiceAno = rentabilidadeIndiceAno / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade6Meses
            decimal? rentabilidadeIndice6Meses = null;
            if (data6Meses != null)
            {
                try
                {
                    rentabilidadeIndice6Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.DataFim, 6);
                    rentabilidadeIndice6Meses = rentabilidadeIndice6Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidade12Meses
            decimal? rentabilidadeIndice12Meses = null;
            if (data12Meses != null)
            {
                try
                {
                    rentabilidadeIndice12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(this.DataFim, 12);
                    rentabilidadeIndice12Meses = rentabilidadeIndice12Meses / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }
            }
            #endregion

            #region rentabilidadeInicio
            decimal? rentabilidadeIndiceInicio = null;
            try
            {
                rentabilidadeIndiceInicio = calculoMedida.CalculaRetornoIndice(this.dataInicioCliente, this.DataFim);
                rentabilidadeIndiceInicio = rentabilidadeIndiceInicio / 100;
            }
            catch (CotacaoIndiceNaoCadastradoException) { }
            #endregion

            rentabilidadeIndice.Add(rentabilidadeIndiceMes);
            rentabilidadeIndice.Add(rentabilidadeIndiceAno);
            rentabilidadeIndice.Add(rentabilidadeIndice6Meses);
            rentabilidadeIndice.Add(rentabilidadeIndice12Meses);
            rentabilidadeIndice.Add(rentabilidadeIndiceInicio);
            #endregion

            #region Calcula Rentabilidade Diferencial
            //
            decimal? rentabilidadeMesDiferencial;
            decimal? rentabilidadeAnoDiferencial;
            decimal? rentabilidade6MesesDiferencial;
            decimal? rentabilidade12MesesDiferencial;
            decimal? rentabilidadeInicioDiferencial;

            int? tipoIndice = null;
            if (this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.es.HasData)
            {
                tipoIndice = this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.Tipo.Value;
            }
            if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Decimal)
            {
                #region TipoIndice = Decimal
                rentabilidadeMesDiferencial = rentabilidadeCota[0].HasValue && rentabilidadeIndice[0].HasValue
                                            ? rentabilidadeCota[0] - rentabilidadeIndice[0]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCota[1].HasValue && rentabilidadeIndice[1].HasValue
                                            ? rentabilidadeCota[1] - rentabilidadeIndice[1]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCota[2].HasValue && rentabilidadeIndice[2].HasValue
                                            ? rentabilidadeCota[2] - rentabilidadeIndice[2]
                                            : null;


                rentabilidade12MesesDiferencial = rentabilidadeCota[3].HasValue && rentabilidadeIndice[3].HasValue
                                            ? rentabilidadeCota[3] - rentabilidadeIndice[3]
                                            : null;

                rentabilidadeInicioDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue
                                            ? rentabilidadeCota[4] - rentabilidadeIndice[4]
                                            : null;
                #endregion
            }
            else
            {
                #region TipoIndice = Percentual
                rentabilidadeMesDiferencial = rentabilidadeCota[0].HasValue && rentabilidadeIndice[0].HasValue && rentabilidadeIndice[0] != 0
                                            ? rentabilidadeCota[0] / rentabilidadeIndice[0]
                                            : null;


                rentabilidadeAnoDiferencial = rentabilidadeCota[1].HasValue && rentabilidadeIndice[1].HasValue && rentabilidadeIndice[1] != 0
                                            ? rentabilidadeCota[1] / rentabilidadeIndice[1]
                                            : null;

                rentabilidade6MesesDiferencial = rentabilidadeCota[2].HasValue && rentabilidadeIndice[2].HasValue && rentabilidadeIndice[2] != 0
                            ? rentabilidadeCota[2] / rentabilidadeIndice[2]
                            : null;

                rentabilidade12MesesDiferencial = rentabilidadeCota[3].HasValue && rentabilidadeIndice[3].HasValue && rentabilidadeIndice[3] != 0
                                            ? rentabilidadeCota[3] / rentabilidadeIndice[3]
                                            : null;

                rentabilidadeInicioDiferencial = rentabilidadeCota[4].HasValue && rentabilidadeIndice[4].HasValue && rentabilidadeIndice[4] != 0
                                            ? rentabilidadeCota[4] / rentabilidadeIndice[4]
                                            : null;
                #endregion
            }

            rentabilidadeDiferencial.Add(rentabilidadeMesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeAnoDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade6MesesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidade12MesesDiferencial);
            rentabilidadeDiferencial.Add(rentabilidadeInicioDiferencial);
            #endregion

            /* Exibe Rentabilidade Diferencial */
            #region Exibe Rentabilidade Diferencial
            XRTableRow xrTableRow2 = xrTable.Rows[2];

            string descricaoIndice = "";
            if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Decimal)
            {
                descricaoIndice = "(-) ";
            }
            else if (tipoIndice.HasValue && tipoIndice == (short)TipoIndice.Percentual)
            {
                descricaoIndice = "(%) ";
            }

            if (this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.es.HasData)
            {
                descricaoIndice = descricaoIndice + this.calculoMedida.carteira.UpToIndiceByIdIndiceBenchmark.Descricao;
            }


            // Se alguma rentabilidade Cota for nula rentabilidadeDiferencial também vai ser nula
            /* ----------------------------------------------------*/
            for (int i = 0; i < rentabilidadeDiferencial.Count; i++)
            {
                if (!rentabilidadeCota[i].HasValue)
                {
                    rentabilidadeDiferencial[i] = null;
                }
            }
            /* ----------------------------------------------------*/
            ((XRTableCell)xrTableRow2.Cells[0]).Text = descricaoIndice;
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[1], rentabilidadeDiferencial[0], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[2], rentabilidadeDiferencial[1], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[3], rentabilidadeDiferencial[2], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[4], rentabilidadeDiferencial[3], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo((XRTableCell)xrTableRow2.Cells[5], rentabilidadeDiferencial[4], true, ReportBase.NumeroCasasDecimais.QuatroCasasDecimaisPorcentagem);
            #endregion
        }
    }
}