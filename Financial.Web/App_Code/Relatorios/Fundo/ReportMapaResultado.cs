﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Investidor;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportMapaResultado
    /// </summary>
    public class ReportMapaResultado : XtraReport {
        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        //private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private DetailReportBand DetailReport;
        private DetailBand Detail1;
        private XRSubreport xrSubreport1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport4;
        private SubReportRodape subReportRodape1;
        private SubReportMapaResultadoBolsa subReportMapaResultadoBolsa1;
        private SubReportMapaResultadoBMF subReportMapaResultadoBMF1;
        private XRTableCell xrTableCell55;
        private XRTableRow xrTableRow10;
        private XRTable xrTable10;
        private XRPageInfo xrPageInfo1;
        private XRTableCell xrTableCell3;
        private XRTableRow xrTableRow3;
        private XRTable xrTable3;
        private XRTableCell xrTableCellDataFim;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCellDataInicio;
        private XRTableCell xrTableCell4;
        private XRTableRow xrTableRow2;
        private XRTable xrTable2;
        private XRPageInfo xrPageInfo3;
        private XRPageInfo xrPageInfo2;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow4;
        private XRTable xrTable4;
        private XRPanel xrPanel1;
        private ReportSemDados reportSemDados1;
        private XRSubreport xrSubreport3;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport5;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport6;
        private SubReportMapaResultadoBolsaDepositosRetiradas subReportMapaResultadoBolsaDepositosRetiradas1;
        private XRSubreport xrSubreport7;
        private SubReportMapaResultadoBolsaOpcoesNaoExercidas subReportMapaResultadoBolsaOpcoesNaoExercidas1;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell28;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRSubreport xrSubreport8;
        private XRSubreport xrSubreport9;
        private XRSubreport xrSubreport10;
        private SubReportMapaResultadoBolsaLiquidacaoEmprestimo subReportMapaResultadoBolsaLiquidacaoEmprestimo1;
        private SubReportMapaResultadoBolsaAberturaEmprestimo subReportMapaResultadoBolsaAberturaEmprestimo1;
        private SubReportMapaResultadoBolsaTransferencias subReportMapaResultadoBolsaTransferencias1;
        private XRSubreport xrSubreport11;
        private XRSubreport xrSubreport12;
        private SubReportMapaResultadoBolsaLiquidacaoTermo subReportMapaResultadoBolsaLiquidacaoTermo1;
        private SubReportMapaResultadoBolsaExercicioOpcoes subReportMapaResultadoBolsaExercicioOpcoes1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportMapaResultado(int idCarteira, DateTime dataInicio, DateTime dataFim) {
            this.idCliente = idCarteira;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            this.InitializeComponent();
            this.PersonalInitialize();
           
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
           
            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1855);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (!this.HasData) {
                this.xrSubreport3.Visible = true;
            }
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// Se algum subreport possue dados então o report possue dados
        /// </summary>
        /// <returns></returns>
        public bool HasData {
            get {
                return
                    this.subReportMapaResultadoBolsa1.HasData ||
                    this.subReportMapaResultadoBMF1.HasData ||
                    this.subReportMapaResultadoBolsaDepositosRetiradas1.HasData ||
                    this.subReportMapaResultadoBolsaOpcoesNaoExercidas1.HasData ||
                    this.subReportMapaResultadoBolsaTransferencias1.HasData ||
                    this.subReportMapaResultadoBolsaAberturaEmprestimo1.HasData ||
                    this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1.HasData || 
                    this.subReportMapaResultadoBolsaLiquidacaoTermo1.HasData || 
                    this.subReportMapaResultadoBolsaExercicioOpcoes1.HasData;
            }
        }

        private void PersonalInitialize() {
            // Passa os parametros para os SubReports
            #region Parametros SubReports
            //            
            this.subReportMapaResultadoBolsa1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport2.Visible = this.subReportMapaResultadoBolsa1.HasData;
            //
            this.subReportMapaResultadoBMF1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport1.Visible = this.subReportMapaResultadoBMF1.HasData;
            //
            this.subReportMapaResultadoBolsaDepositosRetiradas1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport6.Visible = this.subReportMapaResultadoBolsaDepositosRetiradas1.HasData;
            //
            this.subReportMapaResultadoBolsaOpcoesNaoExercidas1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport7.Visible = this.subReportMapaResultadoBolsaOpcoesNaoExercidas1.HasData;
            //
            this.subReportMapaResultadoBolsaTransferencias1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport8.Visible = this.subReportMapaResultadoBolsaTransferencias1.HasData;
            //
            this.subReportMapaResultadoBolsaAberturaEmprestimo1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport9.Visible = this.subReportMapaResultadoBolsaAberturaEmprestimo1.HasData;
            //
            this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport10.Visible = this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1.HasData;
            //
            this.subReportMapaResultadoBolsaLiquidacaoTermo1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport12.Visible = this.subReportMapaResultadoBolsaLiquidacaoTermo1.HasData;
            //
            this.subReportMapaResultadoBolsaExercicioOpcoes1.PersonalInitialize(this.IdCliente, this.dataInicio, this.dataFim);
            this.xrSubreport11.Visible = this.subReportMapaResultadoBolsaExercicioOpcoes1.HasData;
            //
            #endregion

            //// Verifica se é para sumir com o header do SubRelatorio de BMF
            //XRTable header = this.subReportMapaResultadoBMF1.getHeader();
            //header.Visible = false;
            //// Se subRelatorioBMF tem dados e SubRelatorioBolsa nao tem dados então header de BMF é visivel
            //if (!subReportMapaResultadoBolsa1.HasData && subReportMapaResultadoBMF1.HasData) {
            //    header.Visible = true;
            //}

            #region Pega Campos do resource
            //
            this.xrTableCell55.Text = Resources.ReportMapaResultado._TituloRelatorio;
            this.xrTableCell4.Text = Resources.ReportMapaResultado._Periodo;
            this.xrTableCell3.Text = Resources.ReportMapaResultado._DataEmissao;
            this.xrTableCell6.Text = Resources.ReportMapaResultado._a;
            this.xrTableCell5.Text = Resources.ReportMapaResultado._Carteira;
            #endregion
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportMapaResultado.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportMapaResultado.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport12 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaLiquidacaoTermo1 = new Financial.Relatorio.SubReportMapaResultadoBolsaLiquidacaoTermo();
            this.xrSubreport11 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaExercicioOpcoes1 = new Financial.Relatorio.SubReportMapaResultadoBolsaExercicioOpcoes();
            this.xrSubreport10 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1 = new Financial.Relatorio.SubReportMapaResultadoBolsaLiquidacaoEmprestimo();
            this.xrSubreport9 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaAberturaEmprestimo1 = new Financial.Relatorio.SubReportMapaResultadoBolsaAberturaEmprestimo();
            this.xrSubreport8 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaTransferencias1 = new Financial.Relatorio.SubReportMapaResultadoBolsaTransferencias();
            this.xrSubreport7 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaOpcoesNaoExercidas1 = new Financial.Relatorio.SubReportMapaResultadoBolsaOpcoesNaoExercidas();
            this.xrSubreport6 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsaDepositosRetiradas1 = new Financial.Relatorio.SubReportMapaResultadoBolsaDepositosRetiradas();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBolsa1 = new Financial.Relatorio.SubReportMapaResultadoBolsa();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportMapaResultadoBMF1 = new Financial.Relatorio.SubReportMapaResultadoBMF();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCellDataFim = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrSubreport5 = new DevExpress.XtraReports.UI.XRSubreport();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaLiquidacaoTermo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaExercicioOpcoes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaAberturaEmprestimo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaTransferencias1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaOpcoesNaoExercidas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaDepositosRetiradas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport4});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 64F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Dpi = 254F;
            this.xrSubreport4.LocationFloat = new DevExpress.Utils.PointFloat(85F, 0F);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subReportRodape1;
            this.xrSubreport4.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.DetailReport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport12,
            this.xrSubreport11,
            this.xrSubreport10,
            this.xrSubreport9,
            this.xrSubreport8,
            this.xrSubreport7,
            this.xrSubreport6,
            this.xrSubreport2,
            this.xrSubreport1});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 627F;
            this.Detail1.KeepTogether = true;
            this.Detail1.Name = "Detail1";
            this.Detail1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport12
            // 
            this.xrSubreport12.Dpi = 254F;
            this.xrSubreport12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 556F);
            this.xrSubreport12.Name = "xrSubreport12";
            this.xrSubreport12.ReportSource = this.subReportMapaResultadoBolsaLiquidacaoTermo1;
            this.xrSubreport12.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport11
            // 
            this.xrSubreport11.Dpi = 254F;
            this.xrSubreport11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 487F);
            this.xrSubreport11.Name = "xrSubreport11";
            this.xrSubreport11.ReportSource = this.subReportMapaResultadoBolsaExercicioOpcoes1;
            this.xrSubreport11.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport10
            // 
            this.xrSubreport10.Dpi = 254F;
            this.xrSubreport10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 418F);
            this.xrSubreport10.Name = "xrSubreport10";
            this.xrSubreport10.ReportSource = this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1;
            this.xrSubreport10.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport9
            // 
            this.xrSubreport9.Dpi = 254F;
            this.xrSubreport9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 347F);
            this.xrSubreport9.Name = "xrSubreport9";
            this.xrSubreport9.ReportSource = this.subReportMapaResultadoBolsaAberturaEmprestimo1;
            this.xrSubreport9.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport8
            // 
            this.xrSubreport8.Dpi = 254F;
            this.xrSubreport8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 278F);
            this.xrSubreport8.Name = "xrSubreport8";
            this.xrSubreport8.ReportSource = this.subReportMapaResultadoBolsaTransferencias1;
            this.xrSubreport8.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport7
            // 
            this.xrSubreport7.Dpi = 254F;
            this.xrSubreport7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 209F);
            this.xrSubreport7.Name = "xrSubreport7";
            this.xrSubreport7.ReportSource = this.subReportMapaResultadoBolsaOpcoesNaoExercidas1;
            this.xrSubreport7.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport6
            // 
            this.xrSubreport6.Dpi = 254F;
            this.xrSubreport6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 138F);
            this.xrSubreport6.Name = "xrSubreport6";
            this.xrSubreport6.ReportSource = this.subReportMapaResultadoBolsaDepositosRetiradas1;
            this.xrSubreport6.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportMapaResultadoBolsa1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportMapaResultadoBMF1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 122F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable6
            // 
            this.xrTable6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1855F, 96F);
            this.xrTable6.StylePriority.UseBackColor = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.Visible = false;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TotalGeralBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.5;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Weight = 0.2215633423180593;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "TotalValorCompras";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.1353099730458221;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Weight = 0.11374663072776281;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "TotalValorVendas";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.13692722371967656;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "TotResultadoNormal";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.13692722371967656;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "TotResultadoDaytrade";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.12614555256064691;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "TotalDespesas";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.1293800539083558;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell28});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.5;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "TotaGeral";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.2215633423180593;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "TotalValorCompras";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.1353099730458221;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.11428571428571428;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "TotalValorVendas";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.13692722371967656;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "TotResultadoNormal";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.13692722371967656;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "TotResultadoDaytrade";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell25.Weight = 0.12560646900269543;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "TotalDespesas";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.1293800539083558;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Transparent;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1058F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1863F, 148F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(85F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#DataEmissao";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell3.Weight = 1;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCellDataFim
            // 
            this.xrTableCellDataFim.CanGrow = false;
            this.xrTableCellDataFim.Dpi = 254F;
            this.xrTableCellDataFim.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataFim.Name = "xrTableCellDataFim";
            this.xrTableCellDataFim.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataFim.Text = "DataFim";
            this.xrTableCellDataFim.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataFim.Weight = 0.36220472440944884;
            this.xrTableCellDataFim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataFimBeforePrint);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.CanGrow = false;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#a";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.067716535433070865;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataInicio";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.24251968503937008;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioBeforePrint);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#Periodo";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.32755905511811023;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio,
            this.xrTableCell6,
            this.xrTableCellDataFim});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "Carteira";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.83686274509803915;
            this.xrTableCell7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CarteiraBeforePrint);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.CanGrow = false;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#Carteira";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 0.16313725490196077;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell7});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 87F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1275F, 42F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrTable2,
            this.xrPageInfo3,
            this.xrPageInfo2,
            this.xrTable4});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 101F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1466F, 132F);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 245F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(290F, 50F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport5
            // 
            this.xrSubreport5.Dpi = 254F;
            this.xrSubreport5.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport5.Name = "xrSubreport5";
            this.xrSubreport5.ReportSource = this.subReportLogotipo1;
            this.xrSubreport5.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport5,
            this.xrSubreport3,
            this.xrPanel1,
            this.xrPageInfo1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 323F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportMapaResultado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.DetailReport,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaLiquidacaoTermo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaExercicioOpcoes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaAberturaEmprestimo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaTransferencias1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaOpcoesNaoExercidas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsaDepositosRetiradas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBolsa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportMapaResultadoBMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportMapaResultado.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataInicioXRTableCell = sender as XRTableCell;
            dataInicioXRTableCell.Text = this.dataInicio.ToString("d");
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataFimXRTableCell = sender as XRTableCell;
            dataFimXRTableCell.Text = this.dataFim.ToString("d");
        }

        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCarteira = sender as XRTableCell;
            xrTableCellCarteira.Text = "";
           
            // Carrega o Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            if (cliente.LoadByPrimaryKey(campos, this.idCliente)) {
                xrTableCellCarteira.Text = this.idCliente + " - " + cliente.Nome;
            }
        }

        /// <summary>
        /// Realiza a somatoria de (Ações/Opções/Termo) + BMF + Deposito/Retirada + Opções não Exercidas
        /// se existirem tais parcelas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TotalGeralBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            /* Colunas 
             * 0 - TotalGeral
             * 
             * 1 - TotalValorCompras - coluna2 de Bolsa(Se existir) + coluna2 de BMF(Se existir)
             * 
             * 3 - TotalValorVendas - coluna4 de Bolsa(Se existir) + coluna4 de BMF(Se existir)
             * 
             * 4 - TotalResultadoNormal - coluna5 de Bolsa(Se existir) + 
             *                            coluna5 de BMF(Se existir) +
             *                            coluna5 de DepositoRetirada(Se existir) +
             *                            coluna5 de OpcoesNaoExercidas(Se existir)   
             *                            coluna5 de Transferencias(Se existir) +
             *                            coluna5 de AberturaEmprestimo(Se existir) +
             *                            coluna5 de LiquidacaoEmprestimo(Se existir) +
             *                            coluna5 de LiquidacaoTermo(Se existir) +
             *                            coluna5 de ExercicioOpcoes(Se existir) +
             *
             * 5 - TotalResultadoDaytrade - coluna6 de Bolsa(Se existir) +  
             *                              coluna6 de BMF(Se existir) +
             * 
             * 6 - TotalTaxas - coluna7 de Bolsa(Se existir) +  
             *                  coluna7 de BMF(Se existir) +
             *                  coluna7 de DepositoRetirada(Se existir) +
             *                  coluna7 de OpcoesNaoExercidas(Se existir)   
             *                  coluna7 de Transferencias(Se existir) +
             *                  coluna7 de AberturaEmprestimo(Se existir) +
             *                  coluna7 de LiquidacaoEmprestimo(Se existir)
             *                  coluna7 de LiquidacaoTermo(Se existir)
             *                  coluna7 de ExercicioOpcoes(Se existir)
             */
            XRTable summaryFinal = sender as XRTable;
            
            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(summaryFinal);
            #endregion

            summaryFinal.Visible = this.HasData;

            // Se o Relatório tem Dados
            if (this.HasData) {
                XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
                XRTableRow summaryFinalRow1 = summaryFinal.Rows[1];

                //XRTable tableBolsa = this.subReportMapaResultadoBolsa1.HasData
                //                        ? this.subReportMapaResultadoBolsa1.getTableTotalBolsa()
                //                        : null;
                ////                
                //XRTable tableBMF = this.subReportMapaResultadoBMF1.HasData
                //                        ? this.subReportMapaResultadoBMF1.getTableTotalBMF()
                //                        : null;

                // Preenche o Cabecalho
                ((XRTableCell)summaryFinalRow0.Cells[1]).Text = Resources.ReportMapaResultado._TotalValorCompra;
                ((XRTableCell)summaryFinalRow0.Cells[3]).Text = Resources.ReportMapaResultado._TotalValorVenda;
                ((XRTableCell)summaryFinalRow0.Cells[4]).Text = Resources.ReportMapaResultado._TotalResultadoNormal;
                ((XRTableCell)summaryFinalRow0.Cells[5]).Text = Resources.ReportMapaResultado._TotalResultadoDaytrade;
                ((XRTableCell)summaryFinalRow0.Cells[6]).Text = Resources.ReportMapaResultado._TotalDespesas;
                //
                // Preenche os Resultados
                decimal valorComprasTotal = this.subReportMapaResultadoBolsa1.ValoresTotaisSubreportBolsa.valorCompraSum +
                                            this.subReportMapaResultadoBMF1.ValoresTotaisSubreportBMF.valorCompraSum;
                //
                decimal valorVendasTotal = this.subReportMapaResultadoBolsa1.ValoresTotaisSubreportBolsa.valorVendaSum +
                                           this.subReportMapaResultadoBMF1.ValoresTotaisSubreportBMF.valorVendaSum;
                //
                decimal resultadoNormalTotal = this.subReportMapaResultadoBolsa1.ValoresTotaisSubreportBolsa.resultadoNormalSum +
                                               this.subReportMapaResultadoBMF1.ValoresTotaisSubreportBMF.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaDepositosRetiradas1.ValoresTotaisSubreportDepositoRetirada.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaOpcoesNaoExercidas1.ValoresTotaisSubreportOpcoesNaoExercidas.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaTransferencias1.ValoresTotaisSubreportTransferencias.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaAberturaEmprestimo1.ValoresTotaisSubreportAberturaEmprestimo.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1.ValoresTotaisSubreportLiquidacaoEmprestimo.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaLiquidacaoTermo1.ValoresTotaisSubreportLiquidacaoTermo.resultadoNormalSum +
                                               this.subReportMapaResultadoBolsaExercicioOpcoes1.ValoresTotaisSubreportExercicioOpcoes.resultadoNormalSum;
                //
                decimal resultadoDaytradeTotal = this.subReportMapaResultadoBolsa1.ValoresTotaisSubreportBolsa.resultadoDaytradeSum +
                                               this.subReportMapaResultadoBMF1.ValoresTotaisSubreportBMF.resultadoDaytradeSum;
                //
                decimal taxasTotal = this.subReportMapaResultadoBolsa1.ValoresTotaisSubreportBolsa.taxasSum +
                                     this.subReportMapaResultadoBMF1.ValoresTotaisSubreportBMF.taxasSum +
                                     this.subReportMapaResultadoBolsaDepositosRetiradas1.ValoresTotaisSubreportDepositoRetirada.taxasSum +
                                     this.subReportMapaResultadoBolsaOpcoesNaoExercidas1.ValoresTotaisSubreportOpcoesNaoExercidas.taxasSum +
                                     this.subReportMapaResultadoBolsaTransferencias1.ValoresTotaisSubreportTransferencias.taxasSum +
                                     this.subReportMapaResultadoBolsaAberturaEmprestimo1.ValoresTotaisSubreportAberturaEmprestimo.taxasSum +
                                     this.subReportMapaResultadoBolsaLiquidacaoEmprestimo1.ValoresTotaisSubreportLiquidacaoEmprestimo.taxasSum +
                                     this.subReportMapaResultadoBolsaLiquidacaoTermo1.ValoresTotaisSubreportLiquidacaoTermo.taxasSum +
                                     this.subReportMapaResultadoBolsaExercicioOpcoes1.ValoresTotaisSubreportExercicioOpcoes.taxasSum;
                //                                               
                ((XRTableCell)summaryFinalRow1.Cells[0]).Text = Resources.ReportMapaResultado._TotalGeral + ": ";
                //
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow1.Cells[1], valorComprasTotal);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow1.Cells[3], valorVendasTotal);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow1.Cells[4], resultadoNormalTotal);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow1.Cells[5], resultadoDaytradeTotal);
                ReportBase.ConfiguraSinalNegativo(summaryFinalRow1.Cells[6], taxasTotal);
            }
        }
        #endregion
    }
}