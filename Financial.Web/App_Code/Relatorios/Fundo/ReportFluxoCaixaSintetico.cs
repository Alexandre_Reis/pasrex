﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using System.Text;
using log4net;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Investidor;
using Financial.Fundo;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportFluxoCaixaSintetico
    /// </summary>
    public class ReportFluxoCaixaSintetico : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportFluxoCaixaSintetico));

        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime dataReferencia;
        private Cliente cliente;

        private Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa;

        /* Determina se as Datas de ListData são datas após a data Atual da Carteira */
        private List<bool> dataAposDataReferencia = new List<bool>(7);
        
        // Lista de Dias Úteis a Partir da data de Referência
        private List<DateTime> ListData = new List<DateTime>(7);

        private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell34;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRControlStyle xrControlStyle1;
        private LiquidacaoCollection liquidacaoCollection1;
        private ReportFooterBand ReportFooter;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell16;
        private XRTable xrTable3;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRPageInfo xrPageInfo1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell30;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private ReportSemDados reportSemDados1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportFluxoCaixaSintetico(int idCliente, DateTime dataReferencia, Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            this.idCliente = idCliente;
            this.dataReferencia = dataReferencia;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            this.cliente = cliente;
                        
            this.booleanosFluxoCaixa = booleanosFluxoCaixa;

            // Lista de Dias Uteis
            this.ListData.Add(this.dataReferencia);
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 1));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 2));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 3));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 4));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 5));
            this.ListData.Add(Calendario.AdicionaDiaUtil(this.dataReferencia, 6));
            //                                    
            
            // Determina Se as Datas de ListData são datas após a dataReferencia
            for (int i = 0; i < this.ListData.Count; i++) {
                this.dataAposDataReferencia.Add(this.ListData[i] > this.dataReferencia);
			}

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2450);
        }
       
        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.ReportFooter.Visible = false;
                this.xrTable7.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do Resource
            this.xrTableCell55.Text = Resources.ReportFluxoCaixaSintetico._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportFluxoCaixaSintetico._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportFluxoCaixaSintetico._DataReferencia;
            this.xrTableCell28.Text = Resources.ReportFluxoCaixaSintetico._Cliente;
            #endregion
        }

        private DataTable FillDados() 
        {
            #region SQL
            this.liquidacaoCollection1.QueryReset();
            this.liquidacaoCollection1.Query.es.Distinct = true;

            this.liquidacaoCollection1.Query
                 .Select(this.liquidacaoCollection1.Query.Descricao)
                 .Where(this.liquidacaoCollection1.Query.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                        this.liquidacaoCollection1.Query.IdCliente == this.idCliente,
                        this.liquidacaoCollection1.Query.Situacao.Equal((Int16)SituacaoLancamentoLiquidacao.Normal)
                  );
            #endregion

            liquidacaoCollection1.Query.Load();
            //
            Liquidacao liquidacaoAdd = new Liquidacao();
            liquidacaoAdd.Descricao = "Vencimento em títulos";
            liquidacaoCollection1.AttachEntity(liquidacaoAdd);

            liquidacaoAdd = new Liquidacao();
            liquidacaoAdd.Descricao = "Liquidez em ações";
            liquidacaoCollection1.AttachEntity(liquidacaoAdd);

            liquidacaoAdd = new Liquidacao();
            liquidacaoAdd.Descricao = "Liquidez em opções de ações";
            liquidacaoCollection1.AttachEntity(liquidacaoAdd);

            liquidacaoAdd = new Liquidacao();
            liquidacaoAdd.Descricao = "Liquidez em fundos";
            liquidacaoCollection1.AttachEntity(liquidacaoAdd);

            liquidacaoAdd = new Liquidacao();
            liquidacaoAdd.Descricao = "Resgates não cotizados";
            liquidacaoCollection1.AttachEntity(liquidacaoAdd);

            return this.liquidacaoCollection1.GetTable();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportFluxoCaixaSintetico.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportFluxoCaixaSintetico.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.liquidacaoCollection1 = new Financial.ContaCorrente.LiquidacaoCollection();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Detail.HeightF = 46F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell11,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            this.xrTableRow8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailBeforePrint);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.CanGrow = false;
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Descricao")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.28489795918367344;
            this.xrTableCell21.WordWrap = false;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "Valor";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.086530612244897956;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Valor1";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.088571428571428565;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Valor2";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.086530612244897956;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "Valor3";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.086122448979591842;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "Valor4";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.086530612244897956;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "Valor5";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.087346938775510211;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "Valor6";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.19346938775510203;
            // 
            // liquidacaoCollection1
            // 
            this.liquidacaoCollection1.AllowDelete = true;
            this.liquidacaoCollection1.AllowEdit = true;
            this.liquidacaoCollection1.AllowNew = true;
            this.liquidacaoCollection1.EnableHierarchicalBinding = true;
            this.liquidacaoCollection1.Filter = "";
            this.liquidacaoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.liquidacaoCollection1.Sort = "";
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrPageInfo1,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1121F, 181F);
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1037F, 42F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.CanGrow = false;
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#Cliente";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell28.Weight = 0.20443587270973965;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.CanGrow = false;
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Text = "Cliente";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell29.Weight = 0.79556412729026038;
            this.xrTableCell29.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(362F, 61F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 61F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 114F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(529F, 43F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataReferencia";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.40075614366729678;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataReferencia";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.59924385633270316;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataReferenciaBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 61F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1651F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell30.Weight = 0.14112658994548757;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.85887341005451245;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(101F, 294F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow3});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2450F, 92F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableHeaderBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell2,
            this.xrTableCell23});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.5;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Descricao";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.28489795918367344;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "Data";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.086530612244897956;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "Data1";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.088571428571428565;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "Data2";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.086530612244897956;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "Data3";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.087346938775510211;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "Data4";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.086530612244897956;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.Text = "Data5";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.086122448979591842;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "#Acima";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell23.Weight = 0.19346938775510203;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.BackColor = System.Drawing.Color.LightGray;
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrTableCell15,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell24});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableRow3.Weight = 0.5;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#SaldoInicial";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell6.Weight = 0.28489795918367344;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "ValorSaldo";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell7.Weight = 0.086530612244897956;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "ValorSaldo1";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell8.Weight = 0.088571428571428565;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "ValorSaldo2";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell10.Weight = 0.086530612244897956;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "ValorSaldo3";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.087346938775510211;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "ValorSaldo4";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell18.Weight = 0.086530612244897956;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "ValorSaldo5";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell20.Weight = 0.086122448979591842;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "ValorSaldo6";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell24.Weight = 0.19346938775510203;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageFooter.HeightF = 71F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(365F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrPanel1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeader.HeightF = 387F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(64F, 296F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2455F, 93F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(96F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Transparent;
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle1.BorderWidth = 1;
            this.xrControlStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrControlStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 46F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2450F, 46F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableSaldoFinalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.BackColor = System.Drawing.Color.LightGray;
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "#SaldoFinal";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.28489795918367344;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "ValorSaldo";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell39.Weight = 0.086530612244897956;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Text = "ValorSaldo1";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell40.Weight = 0.088571428571428565;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "ValorSaldo2";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell41.Weight = 0.086530612244897956;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Text = "ValorSaldo3";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell42.Weight = 0.087346938775510211;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Text = "ValorSaldo4";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell43.Weight = 0.086530612244897956;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.Text = "ValorSaldo5";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell44.Weight = 0.086122448979591842;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Text = "ValorSaldo6";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell45.Weight = 0.19346938775510203;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportFluxoCaixaSintetico
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.liquidacaoCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 90, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = true;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportFluxoCaixaSintetico.ResourceManager;

        }

        #region Variaveis Internas do Relatorio
        protected class ValoresSaldo {
            public decimal valorSaldo0 = 0;
            public decimal valorSaldo1 = 0;
            public decimal valorSaldo2 = 0;
            public decimal valorSaldo3 = 0;
            public decimal valorSaldo4 = 0;
            public decimal valorSaldo5 = 0;
            public decimal valorSaldo6 = 0;
            public decimal totalSaldoFinal = 0;
        }
        //
        private ValoresSaldo valoresSaldoInicial = new ValoresSaldo();
        private ValoresSaldo valoresSaldoFinal = new ValoresSaldo();

        protected class ValoresTotalLiquidacao {
            public decimal totalLiquidacao0 = 0;
            public decimal totalLiquidacao1 = 0;
            public decimal totalLiquidacao2 = 0;
            public decimal totalLiquidacao3 = 0;
            public decimal totalLiquidacao4 = 0;
            public decimal totalLiquidacao5 = 0;
            public decimal totalLiquidacao6 = 0;
            public decimal liquidacaoTotal = 0;
        }
        //
        private ValoresTotalLiquidacao valoresTotalLiquidacao = new ValoresTotalLiquidacao();
        #endregion

        //
        #region Funções Internas do Relatorio        
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaXRTableCell = sender as XRTableCell;
            dataReferenciaXRTableCell.Text = this.dataReferencia.ToString("d");
        }
        
        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeClienteXRTableCell = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            string nomeCliente = "";
            if(cliente.LoadByPrimaryKey(campos, this.idCliente)) {
                nomeCliente = this.idCliente + " - " + cliente.str.Nome;
            }
            nomeClienteXRTableCell.Text = nomeCliente;                                
        }
      
        private void TableHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable header = sender as XRTable;
            
            // Limpa Dados da Table
            ReportBase.LimpaDadosTable(header);

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha 0
                /* 0-Descricao
                   1-Data
                   2-Data1
                   3-Data2               
                   4-Data3
                   5-Data4
                   6-Data5
                   7-Acima
                   8-Total 
                 */                                
                #region Linha 0
                XRTableRow header0 = header.Rows[0];
                ((XRTableCell)header0.Cells[0]).Text = Resources.ReportFluxoCaixaSintetico._Descricao;
                ((XRTableCell)header0.Cells[1]).Text = this.ListData[0].ToString("d");
                ((XRTableCell)header0.Cells[2]).Text = this.ListData[1].ToString("d");
                ((XRTableCell)header0.Cells[3]).Text = this.ListData[2].ToString("d");
                ((XRTableCell)header0.Cells[4]).Text = this.ListData[3].ToString("d");
                ((XRTableCell)header0.Cells[5]).Text = this.ListData[4].ToString("d");
                ((XRTableCell)header0.Cells[6]).Text = this.ListData[5].ToString("d");
                ((XRTableCell)header0.Cells[7]).Text = Resources.ReportFluxoCaixaSintetico._Acima;                
                #endregion

                List<decimal> saldoInicial = new List<decimal>();

                #region Calcula Saldo
                #region Saldo0
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[0]);
                //
                saldoInicial.Add( saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0 );                
                #endregion
                
                #region Saldo1
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[1]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo2
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[2]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo3
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[3]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo4
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[4]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo5
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[5]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion

                #region Saldo6
                saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(this.idCliente, ListData[6]);
                //
                saldoInicial.Add(saldoCaixa.SaldoAbertura.HasValue ? saldoCaixa.SaldoAbertura.Value : 0);
                #endregion
                #endregion

                // Se a data de Previsão for depois da dataReferencia acrescenta as Liquidações naquela data.
                for (int i = 0; i < this.dataAposDataReferencia.Count; i++) {
                    if (this.dataAposDataReferencia[i]) {

                        // Busca o Saldo Final da Data Referencia
                        SaldoCaixa saldoCaixaAux = new SaldoCaixa();
                        saldoCaixaAux.BuscaSaldoCaixa(this.idCliente, this.dataReferencia);
                        //
                        decimal saldoFinal = saldoCaixaAux.SaldoFechamento.HasValue ? saldoCaixaAux.SaldoFechamento.Value : 0;

                        // Primeiro dia Após dataReferencia
                        if (i == 1) {
                            saldoInicial[i] += saldoFinal;
                        }
                        
                        // Do Segundo dia Após DataCliente até o 6 dia após - Saldo Anterior + liquidações no dia anterior
                        #region Calcula Liquidacao
                        decimal valor = new Carteira().RetornaFluxoLiquidarData(this.cliente, this.dataReferencia, ListData[i-1], this.booleanosFluxoCaixa);
                        //
                        saldoInicial[i] = saldoInicial[i - 1] + valor;
                        #endregion                            
                    }
			    }

                #region Armazena Saldos Iniciais
                this.valoresSaldoInicial.valorSaldo0 = saldoInicial[0];
                this.valoresSaldoInicial.valorSaldo1 = saldoInicial[1];
                this.valoresSaldoInicial.valorSaldo2 = saldoInicial[2];
                this.valoresSaldoInicial.valorSaldo3 = saldoInicial[3];
                this.valoresSaldoInicial.valorSaldo4 = saldoInicial[4];
                this.valoresSaldoInicial.valorSaldo5 = saldoInicial[5];
                this.valoresSaldoInicial.valorSaldo6 = saldoInicial[6];
                #endregion

                // Prenche Linha 1
                #region Linha 1
                XRTableRow header1 = header.Rows[1];
                ((XRTableCell)header1.Cells[0]).Text = Resources.ReportFluxoCaixaSintetico._SaldoInicial;

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[1],
                        saldoInicial[0], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[2],
                        saldoInicial[1], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[3],
                        saldoInicial[2], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[4],
                        saldoInicial[3], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[5],
                        saldoInicial[4], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[6],
                        saldoInicial[5], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)header1.Cells[7],
                        saldoInicial[6], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                #endregion
            }
        }

        private void DetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableRow detail = sender as XRTableRow;

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha 0
                /* 1-Valor
                   2-Valor1
                   3-Valor2               
                   4-Valor3
                   5-Valor4
                   6-Valor5
                   7-Valor6
                   8-Total 
                 */
                
                List<decimal> valorLiquidacao = new List<decimal>();
                string descricao = (string)this.GetCurrentColumnValue(LiquidacaoMetadata.ColumnNames.Descricao);
                decimal valor = 0;
                
                #region Calcula Liquidacao

                #region Liquidacao0
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[0], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao1
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[1], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao2
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[2], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao3
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[3], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao4
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[4], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao5
                valor = new Carteira().RetornaFluxoLiquidarData(cliente, this.dataReferencia, ListData[5], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Liquidacao6
                valor = new Carteira().RetornaFluxoLiquidarAposData(cliente, this.dataReferencia, ListData[6], descricao, this.booleanosFluxoCaixa);
                //
                valorLiquidacao.Add(valor);
                #endregion

                #region Total
                decimal total = 0;
                foreach (decimal valorAux in valorLiquidacao) {
                    total += valorAux;
                }
                #endregion

                #endregion

                #region Armazena Totais de Liquidacao
                this.valoresTotalLiquidacao.totalLiquidacao0 += valorLiquidacao[0];
                this.valoresTotalLiquidacao.totalLiquidacao1 += valorLiquidacao[1];
                this.valoresTotalLiquidacao.totalLiquidacao2 += valorLiquidacao[2];
                this.valoresTotalLiquidacao.totalLiquidacao3 += valorLiquidacao[3];
                this.valoresTotalLiquidacao.totalLiquidacao4 += valorLiquidacao[4];
                this.valoresTotalLiquidacao.totalLiquidacao5 += valorLiquidacao[5];
                this.valoresTotalLiquidacao.totalLiquidacao6 += valorLiquidacao[6];
                this.valoresTotalLiquidacao.liquidacaoTotal += total;
                #endregion

                // Prenche Linha 0
                #region Linha 0
                
                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[1],
                        valorLiquidacao[0], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[2],
                        valorLiquidacao[1], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[3],
                        valorLiquidacao[2], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[4],
                        valorLiquidacao[3], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[5],
                        valorLiquidacao[4], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[6],
                        valorLiquidacao[5], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo((XRTableCell)detail.Cells[7],
                        valorLiquidacao[6], ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                
                #endregion
            }
        }

        private void TableSaldoFinalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable saldoFinal = sender as XRTable;

            // Limpa Dados da Table
            ReportBase.LimpaDadosTable(saldoFinal);

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha 0
                /* 0-#SaldoFinal
                   1-Saldo0
                   2-Saldo1
                   3-Saldo2               
                   4-Saldo3
                   5-Saldo4
                   6-Saldo5
                   7-Saldo6
                   8-Total 
                 */

                #region Calcula SaldoFinal
                this.valoresSaldoFinal.valorSaldo0 = this.valoresSaldoInicial.valorSaldo0 + this.valoresTotalLiquidacao.totalLiquidacao0;
                this.valoresSaldoFinal.valorSaldo1 = this.valoresSaldoInicial.valorSaldo1 + this.valoresTotalLiquidacao.totalLiquidacao1;
                this.valoresSaldoFinal.valorSaldo2 = this.valoresSaldoInicial.valorSaldo2 + this.valoresTotalLiquidacao.totalLiquidacao2;
                this.valoresSaldoFinal.valorSaldo3 = this.valoresSaldoInicial.valorSaldo3 + this.valoresTotalLiquidacao.totalLiquidacao3;
                this.valoresSaldoFinal.valorSaldo4 = this.valoresSaldoInicial.valorSaldo4 + this.valoresTotalLiquidacao.totalLiquidacao4;
                this.valoresSaldoFinal.valorSaldo5 = this.valoresSaldoInicial.valorSaldo5 + this.valoresTotalLiquidacao.totalLiquidacao5;
                this.valoresSaldoFinal.valorSaldo6 = this.valoresSaldoInicial.valorSaldo6 + this.valoresTotalLiquidacao.totalLiquidacao6;
                this.valoresSaldoFinal.totalSaldoFinal = this.valoresSaldoFinal.valorSaldo6 + this.valoresTotalLiquidacao.liquidacaoTotal;
                #endregion

                // Prenche Linha 0
                #region Linha 0
                XRTableRow saldo0 = saldoFinal.Rows[0];
                //
                ((XRTableCell)saldo0.Cells[0]).Text = Resources.ReportFluxoCaixaSintetico._SaldoFinal;
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[1], this.valoresSaldoFinal.valorSaldo0);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[2], this.valoresSaldoFinal.valorSaldo1);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[3], this.valoresSaldoFinal.valorSaldo2);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[4], this.valoresSaldoFinal.valorSaldo3);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[5], this.valoresSaldoFinal.valorSaldo4);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[6], this.valoresSaldoFinal.valorSaldo5);
                ReportBase.ConfiguraSinalNegativo((XRTableCell)saldo0.Cells[7], this.valoresSaldoFinal.valorSaldo6);                
                //
                #endregion
            }
        }

        #endregion                            
    }
}