﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using EntitySpaces.Core;
using System.Text;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.Swap;
using Financial.InvestidorCotista.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportPerfilMensalExportacaoV3
    /// </summary>
    public class ReportPerfilMensalExportacaoV3 : DevExpress.XtraReports.UI.XtraReport {
        #region Nodes

        public class NR_CLIENT {
            public NR_CLIENT() { }
            public int NR_PF_PRIV_BANK;
            public int NR_PF_VARJ;
            public int NR_PJ_N_FINANC_PRIV_BANK;
            public int NR_PJ_N_FINANC_VARJ;
            public int NR_BNC_COMERC;
            public int NR_PJ_CORR_DIST;
            public int NR_PJ_OUTR_FINANC;
            public int NR_INV_N_RES;
            public int NR_ENT_AB_PREV_COMPL;
            public int NR_ENT_FC_PREV_COMPL;
            public int NR_REG_PREV_SERV_PUB;
            public int NR_SOC_SEG_RESEG;
            public int NR_SOC_CAPTLZ_ARRENDM_MERC;
            public int NR_FDOS_CLUB_INV;
            public int NR_COTST_DISTR_FDO;
            public int NR_OUTROS_N_RELAC;
        }

        public class DISTR_PATRIM {
            public DISTR_PATRIM() { }

            public decimal PR_PF_PRIV_BANK;
            public decimal PR_PF_VARJ;
            public decimal PR_PJ_N_FINANC_PRIV_BANK;
            public decimal PR_PJ_N_FINANC_VARJ;
            public decimal PR_BNC_COMERC;
            public decimal PR_PJ_CORR_DIST;
            public decimal PR_PJ_OUTR_FINANC;
            public decimal PR_INV_N_RES;
            public decimal PR_ENT_AB_PREV_COMPL;
            public decimal PR_ENT_FC_PREV_COMPL;
            public decimal PR_REG_PREV_SERV_PUB;
            public decimal PR_SOC_SEG_RESEG;
            public decimal PR_SOC_CAPTLZ_ARRENDM_MERC;
            public decimal PR_FDOS_CLUB_INV;
            public decimal PR_COTST_DISTR_FDO;
            public decimal PR_OUTROS_N_RELAC;
        }
        #endregion
                        
        //
        private int? idCliente;

        public int? IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private Cliente cliente;

        private TabelaPerfilMensalCVM info = new TabelaPerfilMensalCVM();

        private int numeroLinhasDataTable;

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private PageHeaderBand PageHeader;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.Parameters.Parameter parameter1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell6;
        private XRLabel xrLabel8;
        private XRLabel xrLabel10;
        private XRTable xrTable4;
        private XRTableRow xrTableRow35;
        private XRTableCell xrTableCell114;
        private XRTableCell xrTableCell115;
        private XRTableRow xrTableRow61;
        private XRTableCell xrTableCell132;
        private XRTableCell xrTableCell133;
        private XRTableCell xrTableCell134;
        private XRTableCell xrTableCell120;
        private XRTableCell xrTableCell135;
        private XRTableRow xrTableRow62;
        private XRTableCell xrTableCell136;
        private XRTableCell xrTableCell137;
        private XRTableCell xrTableCell138;
        private XRTableCell xrTableCell121;
        private XRTableCell xrTableCell139;
        private XRTableRow xrTableRow63;
        private XRTableCell xrTableCell140;
        private XRTableCell xrTableCell141;
        private XRTableCell xrTableCell142;
        private XRTableCell xrTableCell122;
        private XRTableCell xrTableCell143;
        private XRTableRow xrTableRow64;
        private XRTableCell xrTableCell144;
        private XRTableCell xrTableCell145;
        private XRTableCell xrTableCell146;
        private XRTableCell xrTableCell123;
        private XRTableCell xrTableCell147;
        private XRTableRow xrTableRow65;
        private XRTableCell xrTableCell148;
        private XRTableCell xrTableCell149;
        private XRTableCell xrTableCell150;
        private XRTableCell xrTableCell124;
        private XRTableCell xrTableCell151;
        private XRTableRow xrTableRow52;
        private XRTableCell xrTableCell116;
        private XRTableCell xrTableCell117;
        private XRTableCell xrTableCell118;
        private XRTableCell xrTableCell125;
        private XRTableCell xrTableCell119;
        private XRTableRow xrTableRow66;
        private XRTableCell xrTableCell152;
        private XRTableCell xrTableCell153;
        private XRTableCell xrTableCell154;
        private XRTableCell xrTableCell126;
        private XRTableCell xrTableCell155;
        private XRTableRow xrTableRow67;
        private XRTableCell xrTableCell156;
        private XRTableCell xrTableCell127;
        private XRTableCell xrTableCell129;
        private XRTableCell xrTableCell157;
        private XRTableRow xrTableRow69;
        private XRTableCell xrTableCell158;
        private XRTableCell xrTableCell128;
        private XRTableCell xrTableCell164;
        private XRTableCell xrTableCell159;
        private XRTableRow xrTableRow70;
        private XRTableCell xrTableCell160;
        private XRTableCell xrTableCell165;
        private XRTableCell xrTableCell166;
        private XRTableCell xrTableCell161;
        private XRTableRow xrTableRow55;
        private XRTableCell xrTableCell167;
        private XRTableCell xrTableCell168;
        private XRTableRow xrTableRow71;
        private XRTableCell xrTableCell162;
        private XRTableCell xrTableCell173;
        private XRTableCell xrTableCell174;
        private XRTableCell xrTableCell163;
        private XRTableRow xrTableRow57;
        private XRTableCell xrTableCell171;
        private XRTableCell xrTableCell175;
        private XRTableCell xrTableCell176;
        private XRTableCell xrTableCell172;
        private XRTableRow xrTableRow56;
        private XRTableCell xrTableCell169;
        private XRTableCell xrTableCell177;
        private XRTableCell xrTableCell178;
        private XRTableCell xrTableCell170;
        private XRTableRow xrTableRow58;
        private XRTableCell xrTableCell179;
        private XRTableCell xrTableCell180;
        private XRTableRow xrTableRow59;
        private XRTableCell xrTableCell183;
        private XRTableCell xrTableCell184;
        private XRTableRow xrTableRow60;
        private XRTableCell xrTableCell187;
        private XRTableCell xrTableCell188;
        private XRTableCell xrTableCell190;
        private XRTableRow xrTableRow72;
        private XRTableCell xrTableCell191;
        private XRTableCell xrTableCell192;
        private XRTable xrTable1;
        private XRTableRow xrTableRow36;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell68;
        private XRTableRow xrTableRow37;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell70;
        private XRTableRow xrTableRow38;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCell72;
        private XRTableRow xrTableRow39;
        private XRTableCell xrTableCell73;
        private XRTableCell xrTableCell74;
        private XRTableRow xrTableRow40;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell76;
        private XRTableRow xrTableRow41;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell78;
        private XRTableRow xrTableRow42;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell80;
        private XRTableRow xrTableRow43;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableRow xrTableRow44;
        private XRTableCell xrTableCell83;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell99;
        private XRTableCell xrTableCell84;
        private XRTableRow xrTableRow45;
        private XRTableCell xrTableCell85;
        private XRTableCell xrTableCell104;
        private XRTableCell xrTableCell105;
        private XRTableRow xrTableRow46;
        private XRTableCell xrTableCell87;
        private XRTableCell xrTableCell106;
        private XRTableCell xrTableCell107;
        private XRTableRow xrTableRow47;
        private XRTableCell xrTableCell89;
        private XRTableCell xrTableCell108;
        private XRTableCell xrTableCell109;
        private XRTableRow xrTableRow48;
        private XRTableCell xrTableCell91;
        private XRTableCell xrTableCell110;
        private XRTableCell xrTableCell111;
        private XRTableRow xrTableRow49;
        private XRTableCell xrTableCell93;
        private XRTableCell xrTableCell112;
        private XRTableCell xrTableCell113;
        private XRTableRow xrTableRow50;
        private XRTableCell xrTableCell95;
        private XRTableCell xrTableCell96;
        private XRTableRow xrTableRow51;
        private XRTableCell xrTableCell97;
        private XRTableCell xrTableCell98;
        private XRTableRow xrTableRow53;
        private XRTableCell xrTableCell100;
        private XRTableCell xrTableCell101;
        private XRTableRow xrTableRow54;
        private XRTableCell xrTableCell102;
        private XRTableCell xrTableCell103;
        private XRPageBreak xrPageBreak1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell7;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableRow xrTableRow15;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableRow xrTableRow16;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow17;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell32;
        private XRTableRow xrTableRow18;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableRow xrTableRow19;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableRow xrTableRow20;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell11;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell25;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell28;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell39;
        private XRTableRow xrTableRow21;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableRow xrTableRow23;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableRow xrTableRow24;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableRow xrTableRow22;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableRow xrTableRow25;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableRow xrTableRow26;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableRow xrTableRow27;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableRow xrTableRow28;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell55;
        private XRTableRow xrTableRow29;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableRow xrTableRow30;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableRow xrTableRow31;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableRow xrTableRow32;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableRow xrTableRow33;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableRow xrTableRow34;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private XRTableRow xrTableRow68;
        private XRTableCell xrTableCell130;
        private XRTableCell xrTableCell131;
        private XRPageBreak xrPageBreak2;
        private XRLabel xrLabel9;
        private XRRichText xrRichText2;
        private XRRichText xrRichText1;
        private XRTableCell xrTableCell181;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ReportPerfilMensalExportacaoV3Internal() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            //this.SetRelatorioSemDados();            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }
        #endregion

        #region Construtores Publics
        public ReportPerfilMensalExportacaoV3() {
            this.ReportPerfilMensalExportacaoV3Internal();
        }

        public ReportPerfilMensalExportacaoV3(int idCliente, DateTime data) {
            this.data = data;
            this.idCliente = idCliente;
            this.ReportPerfilMensalExportacaoV3Internal();
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                //this.PageHeader.Visible = true;
                //this.xrSubreport1.Visible = true;
            }
        }

        private void PersonalInitialize() {
            // Carrega a Cliente
            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            //
            this.cliente.LoadByPrimaryKey(campos, this.IdCliente.Value);

            //         
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCliente.Value);
            //
            this.parameter1.Value = "";
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                this.parameter1.Value = pessoa.str.Cpfcnpj;
            }

            #region InformacoesComplementaresFundo
            TabelaPerfilMensalCVMCollection infoCollection = new TabelaPerfilMensalCVMCollection();

            // Busca todas as informações menores que a data de referencia ordenado decrescente pela data
            infoCollection.Query
                 .Where(infoCollection.Query.Data.LessThanOrEqual(this.data),
                        infoCollection.Query.IdCarteira == this.idCliente)
                 .OrderBy(infoCollection.Query.Data.Descending);

            infoCollection.Query.Load();

            // Pega a Primeira se existir Perfil
            bool existeInfo = infoCollection.HasData;
            if (existeInfo) {
                this.info = infoCollection[0];
            }
            #endregion
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportListaEtiqueta.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportPerfilMensalExportacaoV3.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportPerfilMensalExportacaoV3.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageBreak2 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter1 = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrPageBreak2,
            this.xrLabel9,
            this.xrRichText2,
            this.xrRichText1,
            this.xrPageBreak1,
            this.xrTable1,
            this.xrTable4});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 6510.58F;
            this.Detail.MultiColumn.ColumnSpacing = 51F;
            this.Detail.MultiColumn.ColumnWidth = 1016F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 216.3337F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow7,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow21,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow22,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow68});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1913.154F, 1957.575F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Dados1_BeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.StylePriority.UseFont = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow5.Weight = 0.57446808510638292;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "1) Número de cotistas do Fundo no último dia útil do mês de referência, por tipo " +
                "de cotista:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.95242090217117414;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.574468085106383;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "pessoa natural private banking;";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.62072049119496775;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.33170041097620639;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.574468085106383;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.Text = "pessoa natural varejo;";
            this.xrTableCell15.Weight = 0.62072049119496775;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.Weight = 0.33170041097620639;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.574468085106383;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.Text = "pessoa jurídica não financeira private banking;";
            this.xrTableCell13.Weight = 0.62072049119496775;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.Weight = 0.33170041097620639;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.574468085106383;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "pessoa jurídica não financeira varejo;";
            this.xrTableCell17.Weight = 0.62072049119496775;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Weight = 0.33170041097620639;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.574468085106383;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "banco comercial;";
            this.xrTableCell19.Weight = 0.62072049119496775;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.Weight = 0.33170041097620639;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.574468085106383;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "corretora ou distribuidora;";
            this.xrTableCell21.Weight = 0.62072049119496775;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.Weight = 0.33170041097620639;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.574468085106383;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "outras pessoas jurídicas financeiras;";
            this.xrTableCell23.Weight = 0.62072049119496775;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.Weight = 0.33170041097620639;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.574468085106383;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "investidores não residentes;";
            this.xrTableCell26.Weight = 0.62072049119496775;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.Weight = 0.33170041097620639;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell32});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.574468085106383;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "entidade aberta de previdência complementar;";
            this.xrTableCell29.Weight = 0.62072049119496775;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.Weight = 0.33170041097620639;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.574468085106383;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "entidade fechada de previdência complementar;";
            this.xrTableCell33.Weight = 0.62072049119496775;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.Weight = 0.33170041097620639;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.574468085106383;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "regime próprio de previdência dos servidores públicos;";
            this.xrTableCell35.Weight = 0.62072049119496775;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UsePadding = false;
            this.xrTableCell36.Weight = 0.33170041097620639;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.574468085106383;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "sociedade seguradora ou resseguradora;";
            this.xrTableCell37.Weight = 0.62072049119496775;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.Weight = 0.33170041097620639;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.574468085106383;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "sociedade de capitalização e de arrendamento mercantil;";
            this.xrTableCell1.Weight = 0.62072049119496775;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.33170041097620639;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.574468085106383;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "fundos e clubes de investimento;";
            this.xrTableCell3.Weight = 0.62072049119496775;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.33170041097620639;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell11});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.574468085106383;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "clientes de distribuidores do fundo (distribuição por conta e ordem);";
            this.xrTableCell5.Weight = 0.62072049119496775;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.33170041097620639;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell25});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.574468085106383;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "outros tipos de cotistas não relacionados.";
            this.xrTableCell12.Weight = 0.62072049119496775;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.33170041097620639;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.574468085106383;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "2) Distribuição percentual do patrimônio do Fundo no último dia útil do mês de re" +
                "ferência, por tipo de cliente cotista:";
            this.xrTableCell28.Weight = 0.95242090217117414;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell39});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.574468085106383;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "pessoa natural private banking;";
            this.xrTableCell31.Weight = 0.51863990270688276;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.43378099946429138;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.574468085106383;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "pessoa natural varejo;";
            this.xrTableCell40.Weight = 0.51863990270688276;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.43378099946429138;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.574468085106383;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "pessoa jurídica não financeira private banking;";
            this.xrTableCell44.Weight = 0.51863990270688276;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.43378099946429138;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.574468085106383;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "pessoa jurídica não financeira varejo;";
            this.xrTableCell46.Weight = 0.51863990270688276;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 0.43378099946429138;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.574468085106383;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "banco comercial;";
            this.xrTableCell42.Weight = 0.51863990270688276;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.43378099946429138;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.574468085106383;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Text = "corretora ou distribuidora;";
            this.xrTableCell48.Weight = 0.51863990270688276;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.43378099946429138;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.574468085106383;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "outras pessoas jurídicas financeiras;";
            this.xrTableCell50.Weight = 0.51863990270688276;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.43378099946429138;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell53});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.574468085106383;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "investidores não residentes;";
            this.xrTableCell52.Weight = 0.51863990270688276;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.43378099946429138;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.574468085106383;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "entidade aberta de previdência complementar;";
            this.xrTableCell54.Weight = 0.51863990270688276;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.43378099946429138;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell57});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.574468085106383;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Text = "entidade fechada de previdência complementar;";
            this.xrTableCell56.Weight = 0.51863990270688276;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 0.43378099946429138;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.574468085106383;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Text = "regime próprio de previdência dos servidores públicos;";
            this.xrTableCell58.Weight = 0.51863990270688276;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.43378099946429138;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell61});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.574468085106383;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "sociedade seguradora ou resseguradora;";
            this.xrTableCell60.Weight = 0.51863990270688276;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.43378099946429138;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell63});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.574468085106383;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "sociedade de capitalização e de arrendamento mercantil;";
            this.xrTableCell62.Weight = 0.51863990270688276;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.43378099946429138;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64,
            this.xrTableCell65});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.574468085106383;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "fundos e clubes de investimento;";
            this.xrTableCell64.Weight = 0.51863990270688276;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.43378099946429138;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.574468085106383;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "clientes de distribuidores do fundo (distribuição por conta e ordem);";
            this.xrTableCell66.Weight = 0.51863990270688276;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.43378099946429138;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell131});
            this.xrTableRow68.Dpi = 254F;
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 0.574468085106383;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "outros tipos de cotistas não relacionados.";
            this.xrTableCell130.Weight = 0.51863990270688276;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 0.43378099946429138;
            // 
            // xrPageBreak2
            // 
            this.xrPageBreak2.Dpi = 254F;
            this.xrPageBreak2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2227.249F);
            this.xrPageBreak2.Name = "xrPageBreak2";
            // 
            // xrLabel9
            // 
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter1, "Text", "")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(737.5001F, 62.50006F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(363.7325F, 61.742F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter1
            // 
            this.parameter1.Name = "parameter1";
            // 
            // xrRichText2
            // 
            this.xrRichText2.CanShrink = true;
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(25.00002F, 0F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1909F, 60.69601F);
            this.xrRichText2.StylePriority.UseFont = false;
            this.xrRichText2.StylePriority.UsePadding = false;
            this.xrRichText2.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco1_PrintOnPage);
            // 
            // xrRichText1
            // 
            this.xrRichText1.CanShrink = true;
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 125.0001F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1890.866F, 71.17589F);
            this.xrRichText1.StylePriority.UsePadding = false;
            this.xrRichText1.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco2_PrintOnPage);
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.Dpi = 254F;
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4306.708F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 2269.5F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow53,
            this.xrTableRow54});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1913.154F, 2000F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Dados2_BeforePrint);
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell68});
            this.xrTableRow36.Dpi = 254F;
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.574468085106383;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = resources.GetString("xrTableCell30.Text");
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.51863990270688276;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UsePadding = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.43378099946429138;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow37.Dpi = 254F;
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.574468085106383;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.Text = resources.GetString("xrTableCell69.Text");
            this.xrTableCell69.Weight = 0.51863990270688276;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell70.StylePriority.UsePadding = false;
            this.xrTableCell70.Weight = 0.43378099946429138;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.41361700374522109;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.Text = "5) Qual é o VAR (Valor de risco) de um dia como percentual do PL calculado para 2" +
                "1 dias úteis e 95% de confiança?";
            this.xrTableCell71.Weight = 0.51863990270688276;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell72.StylePriority.UsePadding = false;
            this.xrTableCell72.Weight = 0.43378099946429138;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74});
            this.xrTableRow39.Dpi = 254F;
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.41361700374522103;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.Text = "6) Qual classe de modelos foi utilizada para o cálculo do VAR reportado na questã" +
                "o anterior?";
            this.xrTableCell73.Weight = 0.51863990270688276;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell74.StylePriority.UsePadding = false;
            this.xrTableCell74.Weight = 0.43378099946429138;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell76});
            this.xrTableRow40.Dpi = 254F;
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.51702131165849408;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.Text = "7) No último dia útil do mês de referência, qual o prazo médio da carteira de tít" +
                "ulos do fundo? (em meses (30 dias) e calculado de acordo com a metodologia regul" +
                "amentada pela RFB)";
            this.xrTableCell75.Weight = 0.51863990270688276;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell76.StylePriority.UsePadding = false;
            this.xrTableCell76.Weight = 0.43378099946429138;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow41.Dpi = 254F;
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.51702131165849408;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.Text = "8) Caso tenha sido realizada, no mês de referência, alguma assembleia geral de co" +
                "tistas do fundo, relatar resumidamente as principais deliberações aprovadas.";
            this.xrTableCell77.Weight = 0.51863990270688276;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell78.StylePriority.UsePadding = false;
            this.xrTableCell78.Weight = 0.43378099946429138;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow42.Dpi = 254F;
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.51702131165849408;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseFont = false;
            this.xrTableCell79.Text = "9) Total de recursos (em US$) enviados para o exterior para aquisição de ativos -" +
                " Valor total dos contratos de compra de US$ liquidados no mês.";
            this.xrTableCell79.Weight = 0.51863990270688276;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell80.StylePriority.UsePadding = false;
            this.xrTableCell80.Weight = 0.43378099946429138;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow43.Dpi = 254F;
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.517021311658494;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseFont = false;
            this.xrTableCell81.Text = "10) Total de recursos (em US$) ingressados no Brasil referente à venda de ativos " +
                "- Total de contratos de venda de US$ liquidados no mês.";
            this.xrTableCell81.Weight = 0.51863990270688276;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell82.StylePriority.UsePadding = false;
            this.xrTableCell82.Weight = 0.43378099946429138;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell8,
            this.xrTableCell99,
            this.xrTableCell84});
            this.xrTableRow44.Dpi = 254F;
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.22404254880864549;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.Text = "11) Considerando os cenários de estresse definidos pela ";
            this.xrTableCell83.Weight = 0.51863990270688276;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Fator primitivo de risco";
            this.xrTableCell8.Weight = 0.21689049973214569;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Text = "Cenário utilizado";
            this.xrTableCell99.Weight = 0.10844524986607285;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell84.StylePriority.UsePadding = false;
            this.xrTableCell84.Weight = 0.10844524986607285;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell104,
            this.xrTableCell105});
            this.xrTableRow45.Dpi = 254F;
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 0.22404252251158341;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.StylePriority.UseFont = false;
            this.xrTableCell85.Text = "BM&FBOVESPA para os fatores primitivos de risco (FPR) que ";
            this.xrTableCell85.Weight = 0.51863990270688276;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 0.21689049973214569;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.21689049973214569;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell106,
            this.xrTableCell107});
            this.xrTableRow46.Dpi = 254F;
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.22404248306599062;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.Text = "gerem o pior resultado para o fundo, qual a variação diária ";
            this.xrTableCell87.Weight = 0.51863990270688276;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.21689049973214569;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.21689049973214569;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.22404248306599084;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.StylePriority.UseFont = false;
            this.xrTableCell89.Text = "percentual esperada para o valor da cota. Especificar quais foram ";
            this.xrTableCell89.Weight = 0.51863990270688276;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Dpi = 254F;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.21689049973214569;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Dpi = 254F;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.21689049973214569;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTableCell110,
            this.xrTableCell111});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.22404248306599062;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Text = "os cenários adotados da BM&FBOVESPA.";
            this.xrTableCell91.Weight = 0.51863990270688276;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Dpi = 254F;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.21689049973214569;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Dpi = 254F;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 0.21689049973214569;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell112,
            this.xrTableCell113});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 0.4136170300422829;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.51863990270688276;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Dpi = 254F;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 0.21689049973214569;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Dpi = 254F;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.21689049973214569;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.xrTableCell96});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.51702133795555572;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Text = "12) Qual a variação diária percentual esperada para o valor da cota do fundo no p" +
                "ior cenário de estresse definido pelo seu administrador.";
            this.xrTableCell95.Weight = 0.51863990270688276;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.43378099946429138;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98});
            this.xrTableRow51.Dpi = 254F;
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.51702120647024641;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseFont = false;
            this.xrTableCell97.Text = "13) Qual a variação diária percentual esperada para o patrimônio do fundo caso oc" +
                "orra uma variação negativa de 1% na taxa anual de juros (pré). Considerar o últi" +
                "mo dia útil do mês de referência.";
            this.xrTableCell97.Weight = 0.51863990270688276;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.43378099946429138;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.51702120647024685;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "14) Qual a variação diária percentual esperada para o patrimônio do fundo caso oc" +
                "orra uma variação negativa de 1% na taxa de cambio (US$/Real). Considerar o últi" +
                "mo dia útil do mês de referência.";
            this.xrTableCell100.Weight = 0.51863990270688276;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.43378099946429138;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell103});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.51702120647024552;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "15) Qual a variação diária percentual esperada para o patrimônio do fundo caso oc" +
                "orra uma variação negativa de 1% no preço das ações (IBOVESPA). Considerar o últ" +
                "imo dia útil do mês de referência.";
            this.xrTableCell102.Weight = 0.51863990270688276;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.43378099946429138;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(20.84556F, 4330.58F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65,
            this.xrTableRow52,
            this.xrTableRow66,
            this.xrTableRow67,
            this.xrTableRow69,
            this.xrTableRow70,
            this.xrTableRow55,
            this.xrTableRow71,
            this.xrTableRow57,
            this.xrTableRow56,
            this.xrTableRow58,
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow72});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1917.308F, 2150F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Dados3_BeforePrint);
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell114,
            this.xrTableCell181,
            this.xrTableCell115});
            this.xrTableRow35.Dpi = 254F;
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.73386792751780394;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Dpi = 254F;
            this.xrTableCell114.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseFont = false;
            this.xrTableCell114.StylePriority.UsePadding = false;
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.Text = resources.GetString("xrTableCell114.Text");
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell114.Weight = 0.51863990270688276;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.18761060635129812;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Dpi = 254F;
            this.xrTableCell115.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseFont = false;
            this.xrTableCell115.StylePriority.UsePadding = false;
            this.xrTableCell115.StylePriority.UseTextAlignment = false;
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell115.Weight = 0.24617039311299324;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell120,
            this.xrTableCell135});
            this.xrTableRow61.Dpi = 254F;
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 0.19080566042211478;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.StylePriority.UseFont = false;
            this.xrTableCell132.Text = "17) Qual o valor nocional  total de todos os contratos derivativos ";
            this.xrTableCell132.Weight = 0.51863990270688276;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "Fator de Risco";
            this.xrTableCell133.Weight = 0.15354582306701176;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "Long";
            this.xrTableCell134.Weight = 0.085894963265603391;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Text = "Short";
            this.xrTableCell120.Weight = 0.085894963265603391;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell135.StylePriority.UsePadding = false;
            this.xrTableCell135.Text = "Colateral";
            this.xrTableCell135.Weight = 0.10844524986607285;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell121,
            this.xrTableCell139});
            this.xrTableRow62.Dpi = 254F;
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 0.19080565980701669;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.StylePriority.UseFont = false;
            this.xrTableCell136.Text = "negociados em balcão mantidos pelo fundo, em % do patrimônio ";
            this.xrTableCell136.Weight = 0.51863990270688276;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "IBOVESPA";
            this.xrTableCell137.Weight = 0.15354582306701176;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Dpi = 254F;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.085894963265603391;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.085894963265603391;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Dpi = 254F;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell139.StylePriority.UsePadding = false;
            this.xrTableCell139.Weight = 0.10844524986607285;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell122,
            this.xrTableCell143});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 0.19080566515316808;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell140.Dpi = 254F;
            this.xrTableCell140.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.StylePriority.UseFont = false;
            this.xrTableCell140.Text = "líquido, conforme tabela (informar valor numérico inteiro,";
            this.xrTableCell140.Weight = 0.51863990270688276;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "Juros - Pré";
            this.xrTableCell141.Weight = 0.15354582306701176;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.085894963265603391;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.085894963265603391;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell143.StylePriority.UsePadding = false;
            this.xrTableCell143.Weight = 0.10844524986607285;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell123,
            this.xrTableCell147});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 0.19080566515316827;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseFont = false;
            this.xrTableCell144.Text = "contemplando a soma dos nocionais em módulo).";
            this.xrTableCell144.Weight = 0.51863990270688276;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "Cupom Cambial";
            this.xrTableCell145.Weight = 0.15354582306701176;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 0.085894963265603391;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.085894963265603391;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell147.StylePriority.UsePadding = false;
            this.xrTableCell147.Weight = 0.10844524986607285;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell124,
            this.xrTableCell151});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 0.1908056651531681;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Weight = 0.51863990270688276;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Multiline = true;
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Text = "Dólar\r\n";
            this.xrTableCell149.Weight = 0.15354582306701176;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.085894963265603391;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Weight = 0.085894963265603391;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 0.10844524986607285;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell125,
            this.xrTableCell119});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.19080567220652578;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell116.Dpi = 254F;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.51863990270688276;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "Outros (especificar)";
            this.xrTableCell117.Weight = 0.15354582306701176;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.085894963265603391;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.085894963265603391;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 0.10844524986607285;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell126,
            this.xrTableCell155});
            this.xrTableRow66.Dpi = 254F;
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 0.25057685594118861;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.51863990270688276;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Text = "Totais";
            this.xrTableCell153.Weight = 0.15354582306701176;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 0.085894963265603391;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 0.085894963265603391;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 0.10844524986607285;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156,
            this.xrTableCell127,
            this.xrTableCell129,
            this.xrTableCell157});
            this.xrTableRow67.Dpi = 254F;
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 0.35375558519739025;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "18) Para operações cursadas em mercado de balcão, sem garantia de contraparte cen" +
                "tral, identifique os 3 maiores comitentes que atuaram como";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell156.Weight = 0.51863990270688276;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 0.1535458838369716;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 0.13772520401688021;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.14250991161043958;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell158,
            this.xrTableCell128,
            this.xrTableCell164,
            this.xrTableCell159});
            this.xrTableRow69.Dpi = 254F;
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.StylePriority.UseTextAlignment = false;
            this.xrTableRow69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableRow69.Weight = 0.35375555882972742;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Text = "contraparte do fundo, informando o seu CPF/CNPJ, se é parte relacionada ao admini" +
                "strador ou gestor do fundo e o valor total das operações";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell158.Weight = 0.51863990270688276;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseTextAlignment = false;
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell128.Weight = 0.1535458838369716;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell164.Weight = 0.13772520401688021;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseTextAlignment = false;
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 0.14250991161043958;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell161});
            this.xrTableRow70.Dpi = 254F;
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 0.35375555882972776;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.StylePriority.UseTextAlignment = false;
            this.xrTableCell160.Text = "realizadas no mês por contraparte. O termo parte relacionada é aquele do artigo 1" +
                "02, § 1º, incisos II e III, da Instrução CVM nº 555.";
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell160.Weight = 0.51863990270688276;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Weight = 0.1535458838369716;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Weight = 0.13772520401688021;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.14250991161043958;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.442194461584682;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "19) Total dos ativos (em % do PL) em estoque de emissão de partes relacionadas. O" +
                " termo parte relacionada é aquele do artigo 102, § 1º, incisos II e III, da Inst" +
                "rução CVM nº 555.";
            this.xrTableCell167.Weight = 0.51863990270688276;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.43378099946429138;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell163});
            this.xrTableRow71.Dpi = 254F;
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 0.44219445472296826;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.Text = "20) Relacionar os 3 maiores emissores de títulos de crédito privado que o fundo é" +
                " credor, informando o CNPJ/CPF do emissor, se é parte relacionada ao administrad" +
                "or ou gestor do fundo, e o valor total";
            this.xrTableCell162.Weight = 0.51863990270688276;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Weight = 0.21689049973214569;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.10844524986607285;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 0.10844524986607285;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell172});
            this.xrTableRow57.Dpi = 254F;
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 0.44219445472296826;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.StylePriority.UseFont = false;
            this.xrTableCell171.Text = "aplicado pelo fundo, em % do seu  patrimônio líquido. Considerar como de um mesmo" +
                " emissor os ativos emitidos por partes relacionadas de um mesmo grupo econômico " +
                "(informar CNPJ/CPF do emissor";
            this.xrTableCell171.Weight = 0.51863990270688276;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.21689049973214569;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.10844524986607285;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 0.10844524986607285;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell170});
            this.xrTableRow56.Dpi = 254F;
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 0.44219445472296837;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.Text = "mais representativo). O termo parte relacionada é aquele do artigo 102, § 1º, inc" +
                "isos. II e III, da Instrução CVM nº 555.";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell169.Weight = 0.51863990270688276;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.21689049973214569;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Weight = 0.10844524986607285;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.10844524986607285;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell180});
            this.xrTableRow58.Dpi = 254F;
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 0.25978923978038804;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Text = "21) Total dos ativos de crédito privado (em % do PL) em estoque.";
            this.xrTableCell179.Weight = 0.51863990270688276;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Weight = 0.43378099946429138;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184});
            this.xrTableRow59.Dpi = 254F;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 0.65813275454566822;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseFont = false;
            this.xrTableCell183.Text = resources.GetString("xrTableCell183.Text");
            this.xrTableCell183.Weight = 0.51863990270688276;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseTextAlignment = false;
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell184.Weight = 0.43378099946429138;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell190});
            this.xrTableRow60.Dpi = 254F;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 0.51957847529209777;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Text = "23) No caso de a resposta à pergunta anterior ser afirmativa, informar a data e o" +
                " valor da cota do fundo quando da última cobrança de performance efetuada.";
            this.xrTableCell187.Weight = 0.51863990270688276;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Weight = 0.21689049973214569;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Weight = 0.21689049973214569;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192});
            this.xrTableRow72.Dpi = 254F;
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 0.60617515319304027;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Font = new System.Drawing.Font("Times New Roman", 9.5F);
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseFont = false;
            this.xrTableCell191.Text = resources.GetString("xrTableCell191.Text");
            this.xrTableCell191.Weight = 0.51863990270688276;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.43378099946429138;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdPessoa")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Endereco")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(42F, 85F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Numero")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(339F, 85F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Complemento")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(614F, 85F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Bairro")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(42F, 169F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cidade")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(42F, 254F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Uf")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel7.Text = "xrLabel7";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel8,
            this.xrTable2});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 206.742F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(53.33871F, 145F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(1168.728F, 61.742F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "INSTRUÇÃO CVM Nº 555, DE 17 DE DEZEMBRO DE 2014                                  " +
                "                               ";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(1168.728F, 55F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "COMISSÃO DE VALORES MOBILIÁRIOS";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.BorderWidth = 0;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(131.8053F, 55.00013F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1784.06F, 89.99997F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseBorderWidth = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.28316356628780426;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Sede: Rua Sete de Setembro, 111/2-5 e 23-34 Andares  - Centro - Rio de Janeiro - " +
                "RJ - CEP: 20050-901 - Brasil - Tel.: (21) 3554-8686 - http://www.cvm.gov.br.";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 1;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Expanded = false;
            this.PageFooter.HeightF = 150.282F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseFont = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 2.781204F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // ReportPerfilMensalExportacaoV3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.ReportHeader,
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.PageFooter,
            this.ReportFooter});
            this.DetailPrintCount = 1;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter1});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region Funções Personalizadas

        private void Bloco1_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", this.cliente.Apelido);
        }

        private void Bloco2_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            string mes = Utilitario.RetornaMesString(data.Month);
            string dataString = mes + " de " + this.data.Year.ToString();
            valor.Rtf = valor.Rtf.Replace("[#1]", dataString);
        }

        private void Dados1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            #region Linhas
            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            XRTableRow linha4 = table.Rows[4];
            XRTableRow linha5 = table.Rows[5];
            XRTableRow linha6 = table.Rows[6];
            XRTableRow linha7 = table.Rows[7];
            XRTableRow linha8 = table.Rows[8];
            XRTableRow linha9 = table.Rows[9];
            XRTableRow linha10 = table.Rows[10];
            XRTableRow linha11 = table.Rows[11];
            XRTableRow linha12 = table.Rows[12];
            XRTableRow linha13 = table.Rows[13];
            XRTableRow linha14 = table.Rows[14];
            XRTableRow linha15 = table.Rows[15];
            XRTableRow linha16 = table.Rows[16];
            XRTableRow linha17 = table.Rows[17];
            XRTableRow linha18 = table.Rows[18];
            XRTableRow linha19 = table.Rows[19];
            XRTableRow linha20 = table.Rows[20];
            XRTableRow linha21 = table.Rows[21];
            XRTableRow linha22 = table.Rows[22];
            XRTableRow linha23 = table.Rows[23];
            XRTableRow linha24 = table.Rows[24];
            XRTableRow linha25 = table.Rows[25];
            XRTableRow linha26 = table.Rows[26];
            XRTableRow linha27 = table.Rows[27];
            XRTableRow linha28 = table.Rows[28];
            XRTableRow linha29 = table.Rows[29];
            XRTableRow linha30 = table.Rows[30];
            XRTableRow linha31 = table.Rows[31];
            XRTableRow linha32 = table.Rows[32];
            XRTableRow linha33 = table.Rows[33];
            #endregion
            //

            #region Trata tabela de tipos de cotistas
            //CotistaQuery cotistaQuery = new CotistaQuery("C");
            //PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            //cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, cotistaQuery.TipoCotistaCVM.Count().As("ContTipo"));
            //cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            //cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira == this.idCliente,
            //                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
            //                   posicaoCotistaHistoricoQuery.DataHistorico == this.data,
            //                   cotistaQuery.TipoCotistaCVM.IsNotNull());
            //cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
            //CotistaCollection cotistaCollection = new CotistaCollection();
            //cotistaCollection.Load(cotistaQuery);

            CotistaQuery cotistaQuery = new CotistaQuery("C");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            //
            cotistaQuery.es.Distinct = true;

            cotistaQuery.Select(cotistaQuery.IdCotista,
                                cotistaQuery.TipoCotistaCVM.As("TipoCotistaCVM"));
            cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(this.idCliente),
                               posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                               posicaoCotistaHistoricoQuery.DataHistorico.Equal(this.data),
                               cotistaQuery.TipoCotistaCVM.IsNotNull());


            // Query Principal
            CotistaQuery cotistaQueryPrincipal = new CotistaQuery("C");
            //
            cotistaQueryPrincipal.Select("<C.TipoCotistaCVM>", "<COUNT(C.TipoCotistaCVM) AS ContTipo>");
            //
            cotistaQueryPrincipal.From(cotistaQuery).As("C");
            cotistaQueryPrincipal.GroupBy("TipoCotistaCVM");
            //    
            CotistaCollection cotistaCollection = new CotistaCollection();
            cotistaCollection.Load(cotistaQueryPrincipal);
            #endregion

            #region NR_CLIENT
            ReportPerfilMensalExportacaoV3.NR_CLIENT nr = new ReportPerfilMensalExportacaoV3.NR_CLIENT();
            nr.NR_PF_PRIV_BANK = 0;
            nr.NR_PF_VARJ = 0;
            nr.NR_PJ_N_FINANC_PRIV_BANK = 0;
            nr.NR_PJ_N_FINANC_VARJ = 0;
            nr.NR_BNC_COMERC = 0;
            nr.NR_PJ_CORR_DIST = 0;
            nr.NR_PJ_OUTR_FINANC = 0;
            nr.NR_INV_N_RES = 0;
            nr.NR_ENT_AB_PREV_COMPL = 0;
            nr.NR_ENT_FC_PREV_COMPL = 0;
            nr.NR_REG_PREV_SERV_PUB = 0;
            nr.NR_SOC_SEG_RESEG = 0;
            nr.NR_SOC_CAPTLZ_ARRENDM_MERC = 0;
            nr.NR_FDOS_CLUB_INV = 0;
            nr.NR_COTST_DISTR_FDO = 0;
            nr.NR_OUTROS_N_RELAC = 0;
            //

            for (int i = 0; i < cotistaCollection.Count; i++) {
                byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                int cont = Convert.ToInt32(cotistaCollection[i].GetColumn("ContTipo"));

                if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                    nr.NR_PF_PRIV_BANK = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                    nr.NR_PF_VARJ = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                    nr.NR_PJ_N_FINANC_PRIV_BANK = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                    nr.NR_PJ_N_FINANC_VARJ = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                    nr.NR_BNC_COMERC = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                    nr.NR_PJ_CORR_DIST = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                    nr.NR_PJ_OUTR_FINANC = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                    nr.NR_INV_N_RES = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                    nr.NR_ENT_AB_PREV_COMPL = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                    nr.NR_ENT_FC_PREV_COMPL = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                    nr.NR_REG_PREV_SERV_PUB = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                    nr.NR_SOC_SEG_RESEG = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                    nr.NR_SOC_CAPTLZ_ARRENDM_MERC = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                    nr.NR_FDOS_CLUB_INV = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                    nr.NR_COTST_DISTR_FDO = cont;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                    nr.NR_OUTROS_N_RELAC = cont;
                }
            }
            #endregion

            //           
            ((XRTableCell)linha1.Cells[1]).Text = nr.NR_PF_PRIV_BANK.ToString();
            ((XRTableCell)linha2.Cells[1]).Text = nr.NR_PF_VARJ.ToString(); ;
            ((XRTableCell)linha3.Cells[1]).Text = nr.NR_PJ_N_FINANC_PRIV_BANK.ToString(); ;
            ((XRTableCell)linha4.Cells[1]).Text = nr.NR_PJ_N_FINANC_VARJ.ToString(); ;
            ((XRTableCell)linha5.Cells[1]).Text = nr.NR_BNC_COMERC.ToString(); ;
            ((XRTableCell)linha6.Cells[1]).Text = nr.NR_PJ_CORR_DIST.ToString(); ;
            ((XRTableCell)linha7.Cells[1]).Text = nr.NR_PJ_OUTR_FINANC.ToString(); ;
            ((XRTableCell)linha8.Cells[1]).Text = nr.NR_INV_N_RES.ToString(); ;
            ((XRTableCell)linha9.Cells[1]).Text = nr.NR_ENT_AB_PREV_COMPL.ToString(); ;
            ((XRTableCell)linha10.Cells[1]).Text = nr.NR_ENT_FC_PREV_COMPL.ToString(); ;
            ((XRTableCell)linha11.Cells[1]).Text = nr.NR_REG_PREV_SERV_PUB.ToString(); ;
            ((XRTableCell)linha12.Cells[1]).Text = nr.NR_SOC_SEG_RESEG.ToString(); ;
            ((XRTableCell)linha13.Cells[1]).Text = nr.NR_SOC_CAPTLZ_ARRENDM_MERC.ToString(); ;
            ((XRTableCell)linha14.Cells[1]).Text = nr.NR_FDOS_CLUB_INV.ToString(); ;
            ((XRTableCell)linha15.Cells[1]).Text = nr.NR_COTST_DISTR_FDO.ToString(); ;
            ((XRTableCell)linha16.Cells[1]).Text = nr.NR_OUTROS_N_RELAC.ToString(); ;
            //

            #region Trata tabela de tipos de cotistas por patrimônio detido
            cotistaQuery = new CotistaQuery("C");
            posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, posicaoCotistaHistoricoQuery.Quantidade.Sum().As("SaldoTipo"));
            cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira == this.idCliente,
                               posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                               posicaoCotistaHistoricoQuery.DataHistorico == this.data,
                               cotistaQuery.TipoCotistaCVM.IsNotNull());
            cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
            cotistaCollection = new CotistaCollection();
            cotistaCollection.Load(cotistaQuery);
            #endregion

            #region DISTR_PATRIM
            HistoricoCota historicoCota = new HistoricoCota();
            //historicoCota.BuscaValorPatrimonioDia(this.idCliente.Value, this.Data);
            historicoCota.BuscaQuantidadeCotas(this.idCliente.Value, this.Data);
           
            //decimal valorPL = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
            decimal valorPL = historicoCota.QuantidadeFechamento.HasValue ? historicoCota.QuantidadeFechamento.Value : 0;

            if (valorPL == 0) {
                //throw new Exception("PL zerado ou inexistente para o fundo " + idCliente.ToString());
                throw new Exception("Quantidade Fechamento Zerada  para o fundo " + idCliente.ToString());
            }

            ReportPerfilMensalExportacaoV3.DISTR_PATRIM dp = new ReportPerfilMensalExportacaoV3.DISTR_PATRIM();
            //
            dp.PR_PF_PRIV_BANK = 0;
            dp.PR_PF_VARJ = 0;
            dp.PR_PJ_N_FINANC_PRIV_BANK = 0;
            dp.PR_PJ_N_FINANC_VARJ = 0;
            dp.PR_BNC_COMERC = 0;
            dp.PR_PJ_CORR_DIST = 0;
            dp.PR_PJ_OUTR_FINANC = 0;
            dp.PR_INV_N_RES = 0;
            dp.PR_ENT_AB_PREV_COMPL = 0;
            dp.PR_ENT_FC_PREV_COMPL = 0;
            dp.PR_REG_PREV_SERV_PUB = 0;
            dp.PR_SOC_SEG_RESEG = 0;
            dp.PR_SOC_CAPTLZ_ARRENDM_MERC = 0;
            dp.PR_FDOS_CLUB_INV = 0;
            dp.PR_COTST_DISTR_FDO = 0;
            dp.PR_OUTROS_N_RELAC = 0;

            for (int i = 0; i < cotistaCollection.Count; i++) {
                byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                decimal percentSaldo = Math.Round(Convert.ToDecimal(cotistaCollection[i].GetColumn("SaldoTipo")) / valorPL * 100M, 1);

                if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                    dp.PR_PF_PRIV_BANK = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                    dp.PR_PF_VARJ = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                    dp.PR_PJ_N_FINANC_PRIV_BANK = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                    dp.PR_PJ_N_FINANC_VARJ = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                    dp.PR_BNC_COMERC = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                    dp.PR_PJ_CORR_DIST = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                    dp.PR_PJ_OUTR_FINANC = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                    dp.PR_INV_N_RES = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                    dp.PR_ENT_AB_PREV_COMPL = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                    dp.PR_ENT_FC_PREV_COMPL = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                    dp.PR_REG_PREV_SERV_PUB = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                    dp.PR_SOC_SEG_RESEG = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                    dp.PR_SOC_CAPTLZ_ARRENDM_MERC = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                    dp.PR_FDOS_CLUB_INV = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                    dp.PR_COTST_DISTR_FDO = percentSaldo;
                }
                else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                    dp.PR_OUTROS_N_RELAC = percentSaldo;
                }
            }
            //
            #endregion

            ((XRTableCell)linha18.Cells[1]).Text = dp.PR_PF_PRIV_BANK.ToString();
            ((XRTableCell)linha19.Cells[1]).Text = dp.PR_PF_VARJ.ToString();
            ((XRTableCell)linha20.Cells[1]).Text = dp.PR_PJ_N_FINANC_PRIV_BANK.ToString();
            ((XRTableCell)linha21.Cells[1]).Text = dp.PR_PJ_N_FINANC_VARJ.ToString();
            ((XRTableCell)linha22.Cells[1]).Text = dp.PR_BNC_COMERC.ToString();
            ((XRTableCell)linha23.Cells[1]).Text = dp.PR_PJ_CORR_DIST.ToString();
            ((XRTableCell)linha24.Cells[1]).Text = dp.PR_PJ_OUTR_FINANC.ToString();
            ((XRTableCell)linha25.Cells[1]).Text = dp.PR_INV_N_RES.ToString();
            ((XRTableCell)linha26.Cells[1]).Text = dp.PR_ENT_AB_PREV_COMPL.ToString();
            ((XRTableCell)linha27.Cells[1]).Text = dp.PR_ENT_FC_PREV_COMPL.ToString();
            ((XRTableCell)linha28.Cells[1]).Text = dp.PR_REG_PREV_SERV_PUB.ToString();
            ((XRTableCell)linha29.Cells[1]).Text = dp.PR_SOC_SEG_RESEG.ToString();
            ((XRTableCell)linha30.Cells[1]).Text = dp.PR_SOC_CAPTLZ_ARRENDM_MERC.ToString();
            ((XRTableCell)linha31.Cells[1]).Text = dp.PR_FDOS_CLUB_INV.ToString();
            ((XRTableCell)linha32.Cells[1]).Text = dp.PR_COTST_DISTR_FDO.ToString();
            ((XRTableCell)linha33.Cells[1]).Text = dp.PR_OUTROS_N_RELAC.ToString();
        }

        private void Dados2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            #region Linhas
            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            XRTableRow linha4 = table.Rows[4];
            XRTableRow linha5 = table.Rows[5];
            XRTableRow linha6 = table.Rows[6];
            XRTableRow linha7 = table.Rows[7];
            XRTableRow linha8 = table.Rows[8];
            XRTableRow linha9 = table.Rows[9];
            XRTableRow linha10 = table.Rows[10];
            XRTableRow linha11 = table.Rows[11];
            XRTableRow linha12 = table.Rows[12];
            XRTableRow linha13 = table.Rows[13];
            XRTableRow linha14 = table.Rows[14];
            XRTableRow linha15 = table.Rows[15];
            XRTableRow linha16 = table.Rows[16];
            XRTableRow linha17 = table.Rows[17];
            #endregion
            //
            ((XRTableCell)linha0.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao3) ? info.Secao3.Trim() : "";
            ((XRTableCell)linha1.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao4) ? info.Secao4.Trim() : "";

            ((XRTableCell)linha2.Cells[1]).Text = "";
            if (info.Secao5.HasValue) {
                ((XRTableCell)linha2.Cells[1]).Text = info.Secao5.Value.ToString("N4");
            }

            string modelo = "";
            if (info.Secao6.HasValue) {
                if (info.Secao6.Value == 1) {
                    modelo = "Paramétrico";
                }
                else if (info.Secao6.Value == 2) {
                    modelo = "Não Paramétrico";
                }
                else if (info.Secao6.Value == 3) {
                    modelo = "Simulação Monte Carlo";
                }

            }
            ((XRTableCell)linha3.Cells[1]).Text = modelo;
            //

            string prazoMedio = "";
            if (info.Secao7.HasValue && info.Secao7.Value != 0) {
                prazoMedio = Math.Abs(info.Secao7.Value).ToString("N4");
            }
            else {
                decimal prazoMedio1 = new Carteira().RetornaPrazoMedio(this.idCliente.Value, this.data) / 30M; //em meses (30 dias)
                decimal prazoAux = Math.Abs(Math.Round(prazoMedio1, 4));
                prazoMedio = prazoAux.ToString("N4");
            }
            ((XRTableCell)linha4.Cells[1]).Text = prazoMedio;
            //

            ((XRTableCell)linha5.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao8) ? info.Secao8.Trim() : "";
            ((XRTableCell)linha6.Cells[1]).Text = "0.00"; //POR ORA NãO SERA CONTROLADO FIEX OU ALGO PARECIDO
            ((XRTableCell)linha7.Cells[1]).Text = "0.00"; //POR ORA NAO SERA CONTROLADO FIEX OU ALGO PARECIDO
            //

            ((XRTableCell)linha8.Cells[3]).Text = "";
            if (info.Secao11PercentCota.HasValue) {
                ((XRTableCell)linha8.Cells[3]).Text = info.Secao11PercentCota.Value.ToString("N2");
            }

            // linha 9 - 13
            ((XRTableCell)linha9.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao11Fator1) ? info.Secao11Fator1.Trim() : "";
            ((XRTableCell)linha9.Cells[2]).Text = !String.IsNullOrEmpty(info.Secao11Cenario1) ? info.Secao11Cenario1.Trim() : "";

            ((XRTableCell)linha10.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao11Fator2) ? info.Secao11Fator2.Trim() : "";
            ((XRTableCell)linha10.Cells[2]).Text = !String.IsNullOrEmpty(info.Secao11Cenario2) ? info.Secao11Cenario2.Trim() : "";
            //
            ((XRTableCell)linha11.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao11Fator3) ? info.Secao11Fator3.Trim() : "";
            ((XRTableCell)linha11.Cells[2]).Text = !String.IsNullOrEmpty(info.Secao11Cenario3) ? info.Secao11Cenario3.Trim() : "";
            //
            ((XRTableCell)linha12.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao11Fator4) ? info.Secao11Fator4.Trim() : "";
            ((XRTableCell)linha12.Cells[2]).Text = !String.IsNullOrEmpty(info.Secao11Cenario4) ? info.Secao11Cenario4.Trim() : "";
            //
            ((XRTableCell)linha13.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao11Fator5) ? info.Secao11Fator5.Trim() : "";
            ((XRTableCell)linha13.Cells[2]).Text = !String.IsNullOrEmpty(info.Secao11Cenario5) ? info.Secao11Cenario5.Trim() : "";
            //
            //
            ((XRTableCell)linha14.Cells[1]).Text = "";
            ((XRTableCell)linha15.Cells[1]).Text = "";
            ((XRTableCell)linha16.Cells[1]).Text = "";
            ((XRTableCell)linha17.Cells[1]).Text = "";

            if (info.Secao12.HasValue && info.Secao12.Value != 0) {
                ((XRTableCell)linha14.Cells[1]).Text = info.Secao12.Value.ToString("N2");
            }
            if (info.Secao13.HasValue && info.Secao13.Value != 0) {
                ((XRTableCell)linha15.Cells[1]).Text = info.Secao13.Value.ToString("N2");
            }
            if (info.Secao14.HasValue && info.Secao14.Value != 0) {
                ((XRTableCell)linha16.Cells[1]).Text = info.Secao14.Value.ToString("N2");
            }
            if (info.Secao15.HasValue && info.Secao15.Value != 0) {
                ((XRTableCell)linha17.Cells[1]).Text = info.Secao15.Value.ToString("N2");
            }
        }

        private void Dados3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            #region Linhas
            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            XRTableRow linha4 = table.Rows[4];
            XRTableRow linha5 = table.Rows[5];
            XRTableRow linha6 = table.Rows[6];
            XRTableRow linha7 = table.Rows[7];
            XRTableRow linha8 = table.Rows[8];
            XRTableRow linha9 = table.Rows[9];
            XRTableRow linha10 = table.Rows[10];
            XRTableRow linha11 = table.Rows[11];
            XRTableRow linha12 = table.Rows[12];
            XRTableRow linha13 = table.Rows[13];
            XRTableRow linha14 = table.Rows[14];
            XRTableRow linha15 = table.Rows[15];
            XRTableRow linha16 = table.Rows[16];
            XRTableRow linha17 = table.Rows[17];
            XRTableRow linha18 = table.Rows[18];
            #endregion
            //

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(this.idCliente.Value, this.data);

            decimal valorPL = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            if (valorPL == 0) {
                throw new Exception("PL zerado ou inexistente para o fundo " + idCliente.ToString());
            }

            ((XRTableCell)linha0.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao16Fator) ? info.Secao16Fator.Trim() : "";

            ((XRTableCell)linha0.Cells[2]).Text = "";
            if (info.Secao16Variacao.HasValue) {
                ((XRTableCell)linha0.Cells[2]).Text = info.Secao16Variacao.Value.ToString("N2");
            }

            //POR ORA SEM CONTROLE DE FUNDOS COM POSIÇÂO EM FUTUROS DE BALCÃO
            /*--------------------------------------------------------------------------------------------- */
            ((XRTableCell)linha2.Cells[2]).Text = "";
            ((XRTableCell)linha2.Cells[3]).Text = "";
            //
            ((XRTableCell)linha3.Cells[2]).Text = "";
            ((XRTableCell)linha3.Cells[3]).Text = "";
            //
            ((XRTableCell)linha4.Cells[2]).Text = "";
            ((XRTableCell)linha4.Cells[3]).Text = "";
            //
            ((XRTableCell)linha5.Cells[2]).Text = "";
            ((XRTableCell)linha5.Cells[3]).Text = "";
            //
            ((XRTableCell)linha6.Cells[2]).Text = "";
            ((XRTableCell)linha6.Cells[3]).Text = "";
            //
            ((XRTableCell)linha7.Cells[1]).Text = "";
            ((XRTableCell)linha7.Cells[2]).Text = "";
            ((XRTableCell)linha7.Cells[3]).Text = "";
            /*--------------------------------------------------------------------------------------------- */

            //
            #region Linha 8
            ((XRTableCell)linha8.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao18CnpjComitente1) ?
                                                                 "Comitente (" + info.Secao18CnpjComitente1.Trim() + ")" : "";

            string relacionada1 = "";
            if (!String.IsNullOrEmpty(info.Secao18ParteRelacionada1)) {
                if (info.Secao18ParteRelacionada1.Trim().ToUpper() == "S") {
                    relacionada1 = "Parte Relacionada (SIM)";
                }
                else if (info.Secao18ParteRelacionada1.Trim().ToUpper() == "N") {
                    relacionada1 = "Parte Relacionada (NÃO)";
                }
            }
            ((XRTableCell)linha8.Cells[2]).Text = relacionada1;

            if (info.Secao18ValorTotal1.HasValue) {
                ((XRTableCell)linha8.Cells[3]).Text = info.Secao18ValorTotal1.Value.ToString("N1");
            }

            #endregion

            //
            #region linha 9
            ((XRTableCell)linha9.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao18CnpjComitente2) ?
                                                             "Comitente (" + info.Secao18CnpjComitente2.Trim() + ")" : "";

            string relacionada2 = "";
            if (!String.IsNullOrEmpty(info.Secao18ParteRelacionada2)) {
                if (info.Secao18ParteRelacionada2.Trim().ToUpper() == "S") {
                    relacionada2 = "Parte Relacionada (SIM)";
                }
                else if (info.Secao18ParteRelacionada2.Trim().ToUpper() == "N") {
                    relacionada2 = "Parte Relacionada (NÃO)";
                }
            }
            ((XRTableCell)linha9.Cells[2]).Text = relacionada2;

            if (info.Secao18ValorTotal2.HasValue) {
                ((XRTableCell)linha9.Cells[3]).Text = info.Secao18ValorTotal2.Value.ToString("N1");
            }
            #endregion
            //
            #region linha 10
            ((XRTableCell)linha10.Cells[1]).Text = !String.IsNullOrEmpty(info.Secao18CnpjComitente3) ?
                                                             "Comitente (" + info.Secao18CnpjComitente3.Trim() + ")" : "";

            string relacionada3 = "";
            if (!String.IsNullOrEmpty(info.Secao18ParteRelacionada3)) {
                if (info.Secao18ParteRelacionada3.Trim().ToUpper() == "S") {
                    relacionada3 = "Parte Relacionada (SIM)";
                }
                else if (info.Secao18ParteRelacionada3.Trim().ToUpper() == "N") {
                    relacionada3 = "Parte Relacionada (NÃO)";
                }
            }
            ((XRTableCell)linha10.Cells[2]).Text = relacionada3;

            if (info.Secao18ValorTotal3.HasValue) {
                ((XRTableCell)linha10.Cells[3]).Text = info.Secao18ValorTotal3.Value.ToString("N1");
            }
            #endregion
            //            
            #region Linhas 11/12/13/14/15

            string tipoPessoa1 = "", tipoPessoa2 = "", tipoPessoa3 = "";
            string cpfcnpj1 = "", cpfcnpj2 = "", cpfcnpj3 = "";
            string indicaParte1 = "", indicaParte2 = "", indicaParte3 = "";
            decimal percentEmissor1 = 0, percentEmissor2 = 0, percentEmissor3 = 0;
            decimal valorPartesRelacionadas = 0;

            #region Calcula lista com até 3 emissores e total em partes relacionadas
            Carteira carteiraInvestidor = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteiraInvestidor.Query.IdAgenteAdministrador);
            campos.Add(carteiraInvestidor.Query.IdAgenteGestor);
            carteiraInvestidor.LoadByPrimaryKey(campos, this.idCliente.Value);

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                         posicaoFundoHistoricoCollection.Query.ValorBruto.Sum());
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(this.idCliente.Value),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(this.data),
                                                        posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
            posicaoFundoHistoricoCollection.Query.Load();

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection) {
                int idCarteira = posicaoFundoHistorico.IdCarteira.Value;
                decimal valor = posicaoFundoHistorico.ValorBruto.Value;

                Carteira carteiraAplicada = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteiraAplicada.Query.IdAgenteAdministrador);
                campos.Add(carteiraAplicada.Query.IdAgenteGestor);
                carteiraAplicada.LoadByPrimaryKey(campos, idCarteira);

                if (carteiraInvestidor.IdAgenteAdministrador.Value == carteiraAplicada.IdAgenteAdministrador.Value ||
                    carteiraInvestidor.IdAgenteGestor.Value == carteiraAplicada.IdAgenteGestor.Value) {
                    valorPartesRelacionadas += valor;
                }
            }

            decimal valorEmissor1 = 0, valorEmissor2 = 0, valorEmissor3 = 0;
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo,
                                                             posicaoRendaFixaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(this.idCliente.Value),
                                                        posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(this.data),
                                                        posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoRendaFixaHistoricoCollection.Query.GroupBy(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
            posicaoRendaFixaHistoricoCollection.Query.Load();

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection) {
                int idTitulo = posicaoRendaFixaHistorico.IdTitulo.Value;
                decimal valor = posicaoRendaFixaHistorico.ValorMercado.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdPapel);
                campos.Add(tituloRendaFixa.Query.IdEmissor);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                campos = new List<esQueryItem>();
                campos.Add(papelRendaFixa.Query.TipoPapel);
                papelRendaFixa.LoadByPrimaryKey(campos, tituloRendaFixa.IdPapel.Value);

                Emissor emissor = new Emissor();
                campos = new List<esQueryItem>();
                campos.Add(emissor.Query.IdAgente);
                campos.Add(emissor.Query.TipoEmissor);
                campos.Add(emissor.Query.Cnpj);
                emissor.LoadByPrimaryKey(campos, tituloRendaFixa.IdEmissor.Value);

                string parteRelacionada = "N";
                if (emissor.IdAgente.HasValue && (carteiraInvestidor.IdAgenteAdministrador.Value == emissor.IdAgente.Value ||
                    carteiraInvestidor.IdAgenteGestor.Value == emissor.IdAgente.Value)) {
                    parteRelacionada = "S";
                    valorPartesRelacionadas += valor;
                }

                if (papelRendaFixa.TipoPapel == (byte)TipoPapelTitulo.Privado) {
                    if (valor >= valorEmissor1) {
                        valorEmissor3 = valorEmissor2;
                        valorEmissor2 = valorEmissor1;
                        valorEmissor1 = valor;

                        tipoPessoa3 = tipoPessoa2;
                        tipoPessoa2 = tipoPessoa1;
                        tipoPessoa1 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                        cpfcnpj3 = cpfcnpj2;
                        cpfcnpj2 = cpfcnpj1;
                        cpfcnpj1 = emissor.Cnpj;

                        indicaParte3 = indicaParte2;
                        indicaParte2 = indicaParte1;
                        indicaParte1 = parteRelacionada;

                        percentEmissor3 = percentEmissor2;
                        percentEmissor2 = percentEmissor1;
                        percentEmissor1 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                    }
                    else if (valor >= valorEmissor2) {
                        valorEmissor3 = valorEmissor2;
                        valorEmissor2 = valor;

                        tipoPessoa3 = tipoPessoa2;
                        tipoPessoa2 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                        cpfcnpj3 = cpfcnpj2;
                        cpfcnpj2 = emissor.Cnpj;

                        indicaParte3 = indicaParte2;
                        indicaParte2 = parteRelacionada;

                        percentEmissor3 = percentEmissor2;
                        percentEmissor2 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                    }
                    else if (valor >= valorEmissor3) {
                        valorEmissor3 = valor;

                        tipoPessoa3 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                        cpfcnpj3 = emissor.Cnpj;

                        indicaParte3 = parteRelacionada;

                        percentEmissor3 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                    }
                }


            }
            #endregion
            decimal percentPartesRelacionadas = valorPartesRelacionadas / valorPL * 100M;
            if (tipoPessoa1 != "") {

                ((XRTableCell)linha12.Cells[1]).Text = tipoPessoa1 + "(" + cpfcnpj1 + ")";
                ((XRTableCell)linha12.Cells[2]).Text = "Parte Relacionada (" + indicaParte1 + ")";

                decimal valAux1 = Math.Round(percentEmissor1, 1);
                ((XRTableCell)linha12.Cells[3]).Text = valAux1.ToString("N1");

            }
            if (tipoPessoa2 != "") {

                ((XRTableCell)linha13.Cells[1]).Text = tipoPessoa2 + "(" + cpfcnpj2 + ")";
                ((XRTableCell)linha13.Cells[2]).Text = "Parte Relacionada (" + indicaParte2 + ")";

                decimal valAux2 = Math.Round(percentEmissor2, 1);
                ((XRTableCell)linha13.Cells[3]).Text = valAux2.ToString("N1");
            }
            if (tipoPessoa3 != "") {

                ((XRTableCell)linha14.Cells[1]).Text = tipoPessoa3 + "(" + cpfcnpj3 + ")";
                ((XRTableCell)linha14.Cells[2]).Text = "Parte Relacionada (" + indicaParte3 + ")";

                decimal valAux3 = Math.Round(percentEmissor3, 1);
                ((XRTableCell)linha14.Cells[3]).Text = valAux3.ToString("N1");
            }

            #region Calcula total de títulos privados
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(this.idCliente),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(this.data),
                                                 papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado));

            PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoPrivado = new PosicaoRendaFixaHistorico();
            posicaoRendaFixaHistoricoPrivado.Load(posicaoRendaFixaHistoricoQuery);

            decimal valorRendaFixaPrivado = posicaoRendaFixaHistoricoPrivado.ValorMercado.HasValue ? posicaoRendaFixaHistoricoPrivado.ValorMercado.Value : 0;
            #endregion
            decimal percentRFPrivado = valorRendaFixaPrivado / valorPL * 100M;

            decimal valAux = Math.Round(percentPartesRelacionadas, 1);
            ((XRTableCell)linha11.Cells[1]).Text = valAux.ToString("N1");

            decimal valAux5 = Math.Round(percentRFPrivado, 1);
            ((XRTableCell)linha15.Cells[1]).Text = valAux5.ToString("N1");


            #endregion

            string vedadaCobrancaPerformance = "";
            if (!String.IsNullOrEmpty(info.Secao18VedadaCobrancaTaxa)) {
                if (info.Secao18VedadaCobrancaTaxa.Trim().ToUpper() == "S") {
                    vedadaCobrancaPerformance = "S";
                }
                else if (info.Secao18VedadaCobrancaTaxa.Trim().ToUpper() == "N") {
                    vedadaCobrancaPerformance = "N";
                }
            }
            ((XRTableCell)linha16.Cells[1]).Text = vedadaCobrancaPerformance;

            if (!String.IsNullOrEmpty(info.Secao18VedadaCobrancaTaxa)) {
                if (info.Secao18VedadaCobrancaTaxa.Trim().ToUpper() == "S") {
                    if (info.Secao18DataUltimaCobranca.HasValue) {
                        ((XRTableCell)linha17.Cells[1]).Text = info.Secao18DataUltimaCobranca.Value.ToString("dd/MM/yyyy");
                    }
                    if (info.Secao18ValorUltimaCota.HasValue) {
                        ((XRTableCell)linha17.Cells[2]).Text = info.Secao18ValorUltimaCota.Value.ToString("N5");
                    }
                }
            }

            DateTime dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(this.data);

            Liquidacao l = new Liquidacao();
            l.Query.Select(l.Query.Valor.Sum())
                 .Where(l.Query.IdCliente == this.idCliente.Value,
                        l.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao),
                        l.Query.DataLancamento.Between(dataInicio, this.data));

            l.Query.Load();

            decimal proventos = 0.0M;
            if (l.es.HasData && l.Valor.HasValue) {
                proventos = Math.Abs(l.Valor.Value);
            }

            ((XRTableCell)linha18.Cells[1]).Text = proventos.ToString("N2");
        }

        #endregion
    }    
}