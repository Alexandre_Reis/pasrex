﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using EntitySpaces.Core;
using System.Text;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.Swap;
using System.Drawing.Printing;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportLaminaExportacao
    /// </summary>
    public class ReportLaminaExportacao : DevExpress.XtraReports.UI.XtraReport {
        //
        private int? idCliente;

        public int? IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private Cliente cliente;

        private Lamina lamina = new Lamina();

        private int numeroLinhasDataTable;

        int ano;

        public int Ano {
            get { return ano; }
            set { ano = value; }
        }

        // Armazena os últimos 5 anos em relação ao ano passado em ordem crescente
        private List<int> ultimos_5_Anos;

        /* Valores da Table1 */
        private List<CalculoMedida.EstatisticaRetornoMensal> listaRetornosMensais;

        /* Valores da Table2 */
        private List<CalculoMedida.EstatisticaRetornoAnual> listaRetornosAnuais;


        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private PageHeaderBand PageHeader;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.Parameters.Parameter parameter1;
        private XRRichText xrRichText1;
        private XRLabel xrLabel9;
        private XRRichText xrRichText4;
        private XRRichText xrRichText3;
        private XRRichText xrRichText5;
        private XRRichText xrRichText6;
        private XRRichText xrRichText7;
        private XRRichText xrRichText8;
        private XRRichText xrRichText9;
        private XRTable xrTable6;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell28;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTable xrTable1;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell5;
        private XRRichText xrRichText10;
        private XRRichText xrRichText12;
        private XRRichText xrRichText11;
        private XRTable xrTable3;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private XRTableRow xrTableRow12;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableRow xrTableRow15;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableRow xrTableRow16;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableRow xrTableRow17;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell32;
        private XRTableRow xrTableRow18;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableRow xrTableRow19;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableRow xrTableRow20;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRRichText xrRichText13;
        private XRRichText xrRichText15;
        private XRTable xrTable4;
        private XRTableRow xrTableRow22;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTable xrTable5;
        private XRTableRow xrTableRow21;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableRow xrTableRow23;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell50;
        private XRRichText xrRichText14;
        private XRRichText xrRichText16;
        private XRRichText xrRichText17;
        private XRRichText xrRichText19;
        private XRRichText xrRichText21;
        private XRRichText xrRichText22;
        private XRRichText xrRichText23;
        private XRRichText xrRichText24;
        private XRTable xrTable7;
        private XRTableRow xrTableRow24;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell55;
        private XRTableRow xrTableRow25;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell60;
        private XRTableRow xrTableRow26;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell65;
        private XRTableRow xrTableRow27;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell57;
        private XRRichText xrRichText25;
        private XRRichText xrRichText26;
        private XRRichText xrRichText27;
        private XRRichText xrRichText28;
        private XRRichText xrRichText29;
        private XRRichText xrRichText30;
        private XRPictureBox xrPictureBox1;
        private XRPictureBox xrPictureBox2;
        private XRPictureBox xrPictureBox3;
        private XRPictureBox xrPictureBox4;
        private XRPictureBox xrPictureBox5;
        private XRRichText xrRichText31;
        private XRPanel xrPanel1;
        private XRPanel xrPanel2;
        private XRPanel xrPanel3;
        private XRRichText xrRichText34;
        private XRRichText xrRichText35;
        private XRPanel xrPanel4;
        private XRRichText xrRichText18;
        private XRTable xrTable8;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableRow xrTableRow28;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell67;
        private XRTableRow xrTableRow29;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell71;
        private XRTableCell xrTableCell72;
        private XRTableRow xrTableRow30;
        private XRTableCell xrTableCell73;
        private XRTableCell xrTableCell74;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell76;
        private XRTableRow xrTableRow31;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell78;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell80;
        private XRTableRow xrTableRow32;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableCell xrTableCell83;
        private XRTableCell xrTableCell84;
        private XRTable xrTable10;
        private XRTableRow xrTableRow47;
        private XRTableCell xrTableCell141;
        private XRTableCell xrTableCell142;
        private XRTableRow xrTableRow48;
        private XRTableCell xrTableCell143;
        private XRTableCell xrTableCell144;
        private XRTableRow xrTableRow49;
        private XRTableCell xrTableCell145;
        private XRTableCell xrTableCell146;
        private XRTableRow xrTableRow50;
        private XRTableCell xrTableCell147;
        private XRTableCell xrTableCell148;
        private XRTableRow xrTableRow51;
        private XRTableCell xrTableCell149;
        private XRTableCell xrTableCell150;
        private XRPageBreak xrPageBreak1;
        private XRPageBreak xrPageBreak2;
        private XRPageBreak xrPageBreak3;
        private XRPageBreak xrPageBreak4;
        private XRTable xrTable11;
        private XRTableRow xrTableRow52;
        private XRTableCell xrTableCell151;
        private XRTableCell xrTableCell152;
        private XRTableCell xrTableCell153;
        private XRTableCell xrTableCell154;
        private XRTableRow xrTableRow53;
        private XRTableCell xrTableCell155;
        private XRTableCell xrTableCell156;
        private XRTableCell xrTableCell157;
        private XRTableCell xrTableCell158;
        private XRTableRow xrTableRow54;
        private XRTableCell xrTableCell159;
        private XRTableCell xrTableCell160;
        private XRTableCell xrTableCell161;
        private XRTableCell xrTableCell162;
        private XRTableRow xrTableRow55;
        private XRTableCell xrTableCell163;
        private XRTableCell xrTableCell164;
        private XRTableCell xrTableCell165;
        private XRTableCell xrTableCell166;
        private XRTableRow xrTableRow56;
        private XRTableCell xrTableCell167;
        private XRTableCell xrTableCell168;
        private XRTableCell xrTableCell169;
        private XRTableCell xrTableCell170;
        private XRTableRow xrTableRow57;
        private XRTableCell xrTableCell171;
        private XRTableCell xrTableCell172;
        private XRTableCell xrTableCell173;
        private XRTableCell xrTableCell174;
        private XRTableRow xrTableRow58;
        private XRTableCell xrTableCell175;
        private XRTableCell xrTableCell176;
        private XRTableCell xrTableCell177;
        private XRTableCell xrTableCell178;
        private XRTableRow xrTableRow59;
        private XRTableCell xrTableCell179;
        private XRTableCell xrTableCell180;
        private XRTableCell xrTableCell181;
        private XRTableCell xrTableCell182;
        private XRTableRow xrTableRow60;
        private XRTableCell xrTableCell183;
        private XRTableCell xrTableCell184;
        private XRTableCell xrTableCell185;
        private XRTableCell xrTableCell186;
        private XRTableRow xrTableRow61;
        private XRTableCell xrTableCell187;
        private XRTableCell xrTableCell188;
        private XRTableCell xrTableCell189;
        private XRTableCell xrTableCell190;
        private XRTableRow xrTableRow62;
        private XRTableCell xrTableCell191;
        private XRTableCell xrTableCell192;
        private XRTableCell xrTableCell193;
        private XRTableCell xrTableCell194;
        private XRTableRow xrTableRow63;
        private XRTableCell xrTableCell195;
        private XRTableCell xrTableCell196;
        private XRTableCell xrTableCell197;
        private XRTableCell xrTableCell198;
        private XRTableRow xrTableRow64;
        private XRTableCell xrTableCell199;
        private XRTableCell xrTableCell200;
        private XRTableCell xrTableCell201;
        private XRTableCell xrTableCell202;
        private XRTableRow xrTableRow65;
        private XRTableCell xrTableCell203;
        private XRTableCell xrTableCell204;
        private XRTableCell xrTableCell205;
        private XRTableCell xrTableCell206;
        private XRRichText xrRichText2;
        private XRRichText xrRichText20;
        private XRRichText xrRichText32;
        private XRRichText xrRichText33;
        private XRPageInfo xrPageInfo3;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ReportLaminaExportacaoInternal() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            //this.SetRelatorioSemDados();            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }
        #endregion

        #region Construtores Publics
        public ReportLaminaExportacao() {
            this.ReportLaminaExportacaoInternal();
        }

        public ReportLaminaExportacao(int idCliente, DateTime data) {
            this.data = data;
            this.idCliente = idCliente;
            this.ReportLaminaExportacaoInternal();
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                //this.PageHeader.Visible = true;
                //this.xrSubreport1.Visible = true;
            }
        }

        private void PersonalInitialize() {
            // Carrega a Cliente
            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            //
            this.cliente.LoadByPrimaryKey(campos, this.IdCliente.Value);

            //         
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCliente.Value);
            //
            this.parameter1.Value = "";
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                this.parameter1.Value = pessoa.str.Cpfcnpj;
            }

            // Carrega Lamina
            DateTime dataUltimoDiaMes = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            #region Lamina
            LaminaCollection laminaCollection = new LaminaCollection();

            // Busca todas as laminas menores que a data de referencia ordenado decrescente pela data
            laminaCollection.Query
                 .Where(laminaCollection.Query.InicioVigencia.LessThanOrEqual(dataUltimoDiaMes),
                        laminaCollection.Query.IdCarteira == this.idCliente)
                 .OrderBy(laminaCollection.Query.InicioVigencia.Descending);

            laminaCollection.Query.Load();

            // Pega a Primeira se existir Lamina
            bool existeLamina = laminaCollection.HasData;
            if (existeLamina) {
                this.lamina = laminaCollection[0];
            }
            #endregion

            #region Rentabilidade Anual/Mensal
            this.ano = this.data.Year;

            this.ultimos_5_Anos = new List<int>(new int[5] { 
                this.ano - 4, this.ano - 3,
                this.ano - 2, this.ano - 1,
                this.ano
            });

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            //int mesInicio = 1;
            //DateTime dataInicio = new DateTime(this.ano, mesInicio, 01);
            DateTime dataInicioAux = data.AddMonths(-11);
            DateTime dataInicio = new DateTime(dataInicioAux.Year, dataInicioAux.Month, 01);

            int mesFinal = data.Month;
            DateTime dataFim = new DateTime(this.ano, mesFinal, 01);
            //
            dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataFim, 0);
            //            
            DateTime dataDia = this.cliente.DataDia.Value;

            //Ajusta datas pela data de inicio da cota e dataDia do cliente
            if (dataFim > dataDia) {
                dataFim = dataDia;
            }

            if (dataInicio < carteira.DataInicioCota.Value) {
                dataInicio = carteira.DataInicioCota.Value;
            }

            CalculoMedida calculoMedida = new CalculoMedida(this.idCliente.Value);
            calculoMedida.SetIdIndice(carteira.IdIndiceBenchmark.Value);
            calculoMedida.SetBenchmarkPercentual(true);
            calculoMedida.SetBuscaCotaMaisProxima(true);
            this.listaRetornosMensais = calculoMedida.RetornaListaRetornosMensais(dataInicio, dataFim);

            #region Busca retornos mensais e anuais
            DateTime dataAnteriorAno = new DateTime(dataInicio.Year - 4, 01, 01); //Data 5 anos anteriores

            if (dataAnteriorAno < carteira.DataInicioCota.Value) {
                dataAnteriorAno = carteira.DataInicioCota.Value;
            }
            #endregion

            this.listaRetornosAnuais = calculoMedida.RetornaListaRetornosAnuais(dataAnteriorAno, dataFim);
            #endregion

            //this.Detail.HeightF = 15546.831F;

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportListaEtiqueta.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportLaminaExportacao.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportLaminaExportacao.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrRichText33 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPageBreak4 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak3 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak2 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText18 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText17 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText19 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText16 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText34 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText35 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText24 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText25 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText31 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText30 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText29 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText28 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText27 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText26 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText14 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox5 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText15 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText13 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText11 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText12 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText10 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText9 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText8 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText7 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText6 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText5 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText3 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText4 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter1 = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText21 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText22 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText23 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText20 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrRichText32 = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText33)).BeginInit();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText33,
            this.xrRichText2,
            this.xrPageBreak4,
            this.xrPageBreak3,
            this.xrPageBreak2,
            this.xrPageBreak1,
            this.xrTable10,
            this.xrPanel4,
            this.xrPanel3,
            this.xrPanel2,
            this.xrRichText31,
            this.xrRichText30,
            this.xrRichText29,
            this.xrRichText28,
            this.xrRichText27,
            this.xrRichText26,
            this.xrRichText14,
            this.xrTable5,
            this.xrTable4,
            this.xrRichText15,
            this.xrRichText13,
            this.xrTable3,
            this.xrRichText11,
            this.xrRichText12,
            this.xrRichText10,
            this.xrTable1,
            this.xrTable6,
            this.xrRichText9,
            this.xrRichText8,
            this.xrRichText7,
            this.xrRichText6,
            this.xrRichText5,
            this.xrRichText3,
            this.xrRichText4,
            this.xrLabel9,
            this.xrRichText1,
            this.xrPanel1,
            this.xrRichText20});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 9303.415F;
            this.Detail.KeepTogether = true;
            this.Detail.MultiColumn.ColumnSpacing = 51F;
            this.Detail.MultiColumn.ColumnWidth = 1016F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailBeforePrint);
            // 
            // xrRichText33
            // 
            this.xrRichText33.CanShrink = true;
            this.xrRichText33.Dpi = 254F;
            this.xrRichText33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12.87162F);
            this.xrRichText33.Name = "xrRichText33";
            this.xrRichText33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText33.SerializableRtfString = resources.GetString("xrRichText33.SerializableRtfString");
            this.xrRichText33.SizeF = new System.Drawing.SizeF(1953.381F, 60.696F);
            this.xrRichText33.StylePriority.UsePadding = false;
            // 
            // xrRichText2
            // 
            this.xrRichText2.CanShrink = true;
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 93.28596F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1957F, 60.696F);
            this.xrRichText2.StylePriority.UsePadding = false;
            this.xrRichText2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco1_PrintOnPage);
            // 
            // xrPageBreak4
            // 
            this.xrPageBreak4.Dpi = 254F;
            this.xrPageBreak4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3596.26F);
            this.xrPageBreak4.Name = "xrPageBreak4";
            // 
            // xrPageBreak3
            // 
            this.xrPageBreak3.Dpi = 254F;
            this.xrPageBreak3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3596.26F);
            this.xrPageBreak3.Name = "xrPageBreak3";
            // 
            // xrPageBreak2
            // 
            this.xrPageBreak2.Dpi = 254F;
            this.xrPageBreak2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1798.236F);
            this.xrPageBreak2.Name = "xrPageBreak2";
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.Dpi = 254F;
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8202.843F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(73.7151F, 2813.609F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1747.79F, 269.9999F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UsePadding = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item5_BeforePrint);
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow47.StylePriority.UseFont = false;
            this.xrTableRow47.StylePriority.UseTextAlignment = false;
            this.xrTableRow47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow47.Weight = 0.57446808510638292;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.StylePriority.UseFont = false;
            this.xrTableCell141.StylePriority.UsePadding = false;
            this.xrTableCell141.StylePriority.UseTextAlignment = false;
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell141.Weight = 0.51863990270688276;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.StylePriority.UseFont = false;
            this.xrTableCell142.StylePriority.UsePadding = false;
            this.xrTableCell142.Weight = 0.43378099946429138;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.574468085106383;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.StylePriority.UseFont = false;
            this.xrTableCell143.StylePriority.UsePadding = false;
            this.xrTableCell143.StylePriority.UseTextAlignment = false;
            this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell143.Weight = 0.51863990270688276;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseFont = false;
            this.xrTableCell144.StylePriority.UsePadding = false;
            this.xrTableCell144.StylePriority.UseTextAlignment = false;
            this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell144.Weight = 0.43378099946429138;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell146});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 0.574468085106383;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseFont = false;
            this.xrTableCell145.Weight = 0.51863990270688276;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell146.StylePriority.UsePadding = false;
            this.xrTableCell146.Weight = 0.43378099946429138;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell148});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.574468085106383;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseFont = false;
            this.xrTableCell147.Weight = 0.51863990270688276;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell148.StylePriority.UsePadding = false;
            this.xrTableCell148.Weight = 0.43378099946429138;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150});
            this.xrTableRow51.Dpi = 254F;
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.574468085106383;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseFont = false;
            this.xrTableCell149.Weight = 0.51863990270688276;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell150.StylePriority.UsePadding = false;
            this.xrTableCell150.Weight = 0.43378099946429138;
            // 
            // xrPanel4
            // 
            this.xrPanel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11,
            this.xrTable8,
            this.xrRichText18,
            this.xrRichText17,
            this.xrRichText19,
            this.xrRichText16});
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(24.73371F, 3727.507F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel4.SizeF = new System.Drawing.SizeF(1881.545F, 1591.309F);
            this.xrPanel4.Visible = false;
            this.xrPanel4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7VisibilidadeFundoTodosBeforePrint);
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.Dpi = 254F;
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(27.73512F, 886.0887F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow57,
            this.xrTableRow58,
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65});
            this.xrTable11.SizeF = new System.Drawing.SizeF(1800F, 683.6667F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7RentabilidadeMensal_BeforePrint);
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.06043855536994075;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseFont = false;
            this.xrTableCell151.StylePriority.UseTextAlignment = false;
            this.xrTableCell151.Text = "Mês";
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell151.Weight = 0.1136271214109604;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseFont = false;
            this.xrTableCell152.StylePriority.UseTextAlignment = false;
            this.xrTableCell152.Text = "Rentabilidade(líquida de despesas, mas não de impostos)";
            this.xrTableCell152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell152.Weight = 0.2639190491485181;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseFont = false;
            this.xrTableCell153.StylePriority.UseTextAlignment = false;
            this.xrTableCell153.Text = "Variação Percentual do ";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell153.Weight = 0.2673931297952879;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseFont = false;
            this.xrTableCell154.StylePriority.UseTextAlignment = false;
            this.xrTableCell154.Text = "Desempenho do Fundo como % do ";
            this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell154.Weight = 0.3252421249066404;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow53.Weight = 0.0302192773583879;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell155.Text = RetornaNomeMesSomado(this.data, -11);
            this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell155.Weight = 0.1136271214109604;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell156.Weight = 0.2639190491485181;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell157.StylePriority.UseFont = false;
            this.xrTableCell157.StylePriority.UseTextAlignment = false;
            this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell157.Weight = 0.2673931297952879;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell158.Weight = 0.3252421249066404;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow54.Weight = 0.030219276987151211;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell159.StylePriority.UseBackColor = false;
            this.xrTableCell159.Text = RetornaNomeMesSomado(this.data, -10);
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 0.1136271214109604;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell160.StylePriority.UseBackColor = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.StylePriority.UseTextAlignment = false;
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell160.Weight = 0.2639190491485181;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell161.StylePriority.UseFont = false;
            this.xrTableCell161.StylePriority.UseTextAlignment = false;
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell161.Weight = 0.2673931297952879;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.StylePriority.UseTextAlignment = false;
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell162.Weight = 0.3252421249066404;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow55.Weight = 0.030219277098753272;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell163.Text = RetornaNomeMesSomado(this.data, -9);
            this.xrTableCell163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell163.Weight = 0.1136271214109604;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell164.StylePriority.UseFont = false;
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell164.Weight = 0.2639190491485181;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell165.StylePriority.UseFont = false;
            this.xrTableCell165.StylePriority.UseTextAlignment = false;
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell165.Weight = 0.2673931297952879;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseFont = false;
            this.xrTableCell166.StylePriority.UseTextAlignment = false;
            this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell166.Weight = 0.3252421249066404;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170});
            this.xrTableRow56.Dpi = 254F;
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow56.Weight = 0.030219277098753269;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell167.StylePriority.UseBackColor = false;
            this.xrTableCell167.Text = RetornaNomeMesSomado(this.data, -8);
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell167.Weight = 0.1136271214109604;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell168.StylePriority.UseBackColor = false;
            this.xrTableCell168.StylePriority.UseFont = false;
            this.xrTableCell168.StylePriority.UseTextAlignment = false;
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell168.Weight = 0.2639190491485181;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell169.Weight = 0.2673931297952879;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseFont = false;
            this.xrTableCell170.StylePriority.UseTextAlignment = false;
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell170.Weight = 0.3252421249066404;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174});
            this.xrTableRow57.Dpi = 254F;
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow57.Weight = 0.030219278359501184;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell171.StylePriority.UsePadding = false;
            this.xrTableCell171.StylePriority.UseTextAlignment = false;
            this.xrTableCell171.Text = RetornaNomeMesSomado(this.data, -7);
            this.xrTableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell171.Weight = 0.1136271214109604;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell172.StylePriority.UseFont = false;
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell172.Weight = 0.2639190491485181;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell173.StylePriority.UseFont = false;
            this.xrTableCell173.StylePriority.UseTextAlignment = false;
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell173.Weight = 0.2673931297952879;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseFont = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell174.Weight = 0.3252421249066404;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178});
            this.xrTableRow58.Dpi = 254F;
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow58.Weight = 0.030219278377746135;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell175.StylePriority.UseBackColor = false;
            this.xrTableCell175.StylePriority.UsePadding = false;
            this.xrTableCell175.Text = RetornaNomeMesSomado(this.data, -6);
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell175.Weight = 0.1136271214109604;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell176.StylePriority.UseBackColor = false;
            this.xrTableCell176.StylePriority.UseFont = false;
            this.xrTableCell176.StylePriority.UseTextAlignment = false;
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell176.Weight = 0.2639190491485181;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell177.StylePriority.UseFont = false;
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell177.Weight = 0.2673931297952879;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseFont = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell178.Weight = 0.3252421249066404;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182});
            this.xrTableRow59.Dpi = 254F;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow59.Weight = 0.030219278377746128;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell179.Text = RetornaNomeMesSomado(this.data, -5);
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell179.Weight = 0.1136271214109604;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell180.StylePriority.UseFont = false;
            this.xrTableCell180.StylePriority.UseTextAlignment = false;
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell180.Weight = 0.2639190491485181;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell181.StylePriority.UseFont = false;
            this.xrTableCell181.StylePriority.UseTextAlignment = false;
            this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell181.Weight = 0.2673931297952879;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseTextAlignment = false;
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell182.Weight = 0.3252421249066404;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186});
            this.xrTableRow60.Dpi = 254F;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow60.Weight = 0.030219278359501174;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell183.StylePriority.UseBackColor = false;
            this.xrTableCell183.StylePriority.UsePadding = false;
            this.xrTableCell183.Text = RetornaNomeMesSomado(this.data, -4);
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell183.Weight = 0.1136271214109604;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell184.StylePriority.UseBackColor = false;
            this.xrTableCell184.StylePriority.UseFont = false;
            this.xrTableCell184.StylePriority.UseTextAlignment = false;
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell184.Weight = 0.2639190491485181;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell185.StylePriority.UseFont = false;
            this.xrTableCell185.StylePriority.UseTextAlignment = false;
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell185.Weight = 0.2673931297952879;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell186.Weight = 0.3252421249066404;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190});
            this.xrTableRow61.Dpi = 254F;
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow61.Weight = 0.030219278564460349;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell187.StylePriority.UsePadding = false;
            this.xrTableCell187.Text = RetornaNomeMesSomado(this.data, -3);
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell187.Weight = 0.1136271214109604;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseTextAlignment = false;
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell188.Weight = 0.2639190491485181;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell189.StylePriority.UseFont = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell189.Weight = 0.2673931297952879;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseFont = false;
            this.xrTableCell190.StylePriority.UseTextAlignment = false;
            this.xrTableCell190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell190.Weight = 0.3252421249066404;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194});
            this.xrTableRow62.Dpi = 254F;
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow62.Weight = 0.030219278564460363;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell191.StylePriority.UseBackColor = false;
            this.xrTableCell191.Text = RetornaNomeMesSomado(this.data, -2);
            this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell191.Weight = 0.1136271214109604;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell192.StylePriority.UseBackColor = false;
            this.xrTableCell192.StylePriority.UseFont = false;
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell192.Weight = 0.26391908230178063;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell193.Weight = 0.26739327898496912;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseFont = false;
            this.xrTableCell194.StylePriority.UseTextAlignment = false;
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell194.Weight = 0.32524194256369665;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow63.Weight = 0.030219276987151204;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell195.Dpi = 254F;
            this.xrTableCell195.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell195.Text = RetornaNomeMesSomado(this.data, -1);
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell195.Weight = 0.1136271214109604;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell196.StylePriority.UseFont = false;
            this.xrTableCell196.StylePriority.UseTextAlignment = false;
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell196.Weight = 0.26391908230178063;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell197.StylePriority.UseFont = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell197.Weight = 0.26739327898496912;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseFont = false;
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell198.Weight = 0.32524194256369665;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow64.Weight = 0.030219278564460356;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell199.StylePriority.UseBackColor = false;
            this.xrTableCell199.Text = RetornaNomeMesSomado(this.data, 0);
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell199.Weight = 0.1136271214109604;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell200.BorderColor = System.Drawing.Color.Black;
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell200.StylePriority.UseBackColor = false;
            this.xrTableCell200.StylePriority.UseBorderColor = false;
            this.xrTableCell200.StylePriority.UseFont = false;
            this.xrTableCell200.StylePriority.UseTextAlignment = false;
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell200.Weight = 0.2639190491485181;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell201.Weight = 0.2673931297952879;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.StylePriority.UseFont = false;
            this.xrTableCell202.StylePriority.UseTextAlignment = false;
            this.xrTableCell202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell202.Weight = 0.3252421249066404;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow65.Weight = 0.032663499977084259;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell203.Text = "12 meses";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell203.Weight = 0.1136271214109604;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell204.StylePriority.UseFont = false;
            this.xrTableCell204.StylePriority.UseTextAlignment = false;
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell204.Weight = 0.2639190491485181;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseTextAlignment = false;
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell205.Weight = 0.2673931297952879;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseFont = false;
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell206.Weight = 0.3252421249066404;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(36.4614F, 471.1344F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32});
            this.xrTable8.SizeF = new System.Drawing.SizeF(1800F, 305.3279F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7RentabilidadeAnual_BeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell58,
            this.xrTableCell59});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.068125776975298438;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell11.CanGrow = false;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Ano";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.1136271214109604;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "Rentabilidade (líquida de despesas, mas não de impostos)";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.2639190491485181;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Variação Percentual do ";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 0.2673931297952879;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "Desempenho do Fundo como % do ";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell59.Weight = 0.3252421249066404;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell67});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 0.030015169161471053;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.Text = "Ano0";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell62.Weight = 0.1136271214109604;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.2639190491485181;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.2673931297952879;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.3252421249066404;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow29.Weight = 0.030015169691102815;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell68.StylePriority.UseBackColor = false;
            this.xrTableCell68.Text = "Ano1";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.1136271214109604;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell69.StylePriority.UseBackColor = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.2639190491485181;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.2673931297952879;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell72.Weight = 0.3252421249066404;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow30.Weight = 0.030015169542689787;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.Text = "Ano2";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell73.Weight = 0.1136271214109604;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell74.Weight = 0.2639190491485181;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell75.Weight = 0.2673931297952879;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.3252421249066404;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow31.Weight = 0.030015170905770591;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell77.StylePriority.UseBackColor = false;
            this.xrTableCell77.Text = "Ano3";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell77.Weight = 0.1136271214109604;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell78.StylePriority.UseBackColor = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell78.Weight = 0.2639190491485181;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.StylePriority.UseFont = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell79.Weight = 0.2673931297952879;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell80.Weight = 0.3252421249066404;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow32.Weight = 0.030015170367003484;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell81.StylePriority.UsePadding = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "Ano4";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell81.Weight = 0.1136271214109604;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell82.Weight = 0.2639190491485181;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.2673931297952879;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseFont = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.3252421249066404;
            // 
            // xrRichText18
            // 
            this.xrRichText18.CanShrink = true;
            this.xrRichText18.Dpi = 254F;
            this.xrRichText18.LocationFloat = new DevExpress.Utils.PointFloat(29.04585F, 301.3065F);
            this.xrRichText18.Name = "xrRichText18";
            this.xrRichText18.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 10, 0, 0, 254F);
            this.xrRichText18.SerializableRtfString = resources.GetString("xrRichText18.SerializableRtfString");
            this.xrRichText18.SizeF = new System.Drawing.SizeF(1840F, 143.3521F);
            this.xrRichText18.StylePriority.UsePadding = false;
            this.xrRichText18.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7Inicio_PrintOnPage);
            this.xrRichText18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7InicioBeforePrint);
            // 
            // xrRichText17
            // 
            this.xrRichText17.CanShrink = true;
            this.xrRichText17.Dpi = 254F;
            this.xrRichText17.LocationFloat = new DevExpress.Utils.PointFloat(24.99999F, 107.0033F);
            this.xrRichText17.Name = "xrRichText17";
            this.xrRichText17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText17.SerializableRtfString = resources.GetString("xrRichText17.SerializableRtfString");
            this.xrRichText17.SizeF = new System.Drawing.SizeF(1827.881F, 152.7061F);
            this.xrRichText17.StylePriority.UsePadding = false;
            this.xrRichText17.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7CRentabilidade_PrintOnPage);
            // 
            // xrRichText19
            // 
            this.xrRichText19.CanShrink = true;
            this.xrRichText19.Dpi = 254F;
            this.xrRichText19.LocationFloat = new DevExpress.Utils.PointFloat(18.40389F, 799.2108F);
            this.xrRichText19.Name = "xrRichText19";
            this.xrRichText19.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText19.SerializableRtfString = resources.GetString("xrRichText19.SerializableRtfString");
            this.xrRichText19.SizeF = new System.Drawing.SizeF(1834.002F, 62.31836F);
            this.xrRichText19.StylePriority.UsePadding = false;
            this.xrRichText19.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7BRentabilidade_PrintOnPage);
            // 
            // xrRichText16
            // 
            this.xrRichText16.CanShrink = true;
            this.xrRichText16.Dpi = 254F;
            this.xrRichText16.LocationFloat = new DevExpress.Utils.PointFloat(20.85958F, 4.812582E-05F);
            this.xrRichText16.Name = "xrRichText16";
            this.xrRichText16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText16.SerializableRtfString = resources.GetString("xrRichText16.SerializableRtfString");
            this.xrRichText16.SizeF = new System.Drawing.SizeF(1840F, 79.18311F);
            this.xrRichText16.StylePriority.UsePadding = false;
            this.xrRichText16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7ABeforePrint);
            // 
            // xrPanel3
            // 
            this.xrPanel3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPanel3.CanGrow = false;
            this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText34,
            this.xrRichText35});
            this.xrPanel3.Dpi = 254F;
            this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(25.30571F, 5346.854F);
            this.xrPanel3.Name = "xrPanel3";
            this.xrPanel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel3.SizeF = new System.Drawing.SizeF(1909F, 654.5288F);
            this.xrPanel3.Visible = false;
            this.xrPanel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7VisibilidadeFundoEstruturadoPrint);
            // 
            // xrRichText34
            // 
            this.xrRichText34.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrRichText34.CanShrink = true;
            this.xrRichText34.Dpi = 254F;
            this.xrRichText34.KeepTogether = true;
            this.xrRichText34.LocationFloat = new DevExpress.Utils.PointFloat(8.389296F, 25.00017F);
            this.xrRichText34.Name = "xrRichText34";
            this.xrRichText34.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 10, 0, 0, 254F);
            this.xrRichText34.SerializableRtfString = resources.GetString("xrRichText34.SerializableRtfString");
            this.xrRichText34.SizeF = new System.Drawing.SizeF(1869.532F, 278.4282F);
            this.xrRichText34.StylePriority.UsePadding = false;
            this.xrRichText34.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7ACenarios_PrintOnPage);
            // 
            // xrRichText35
            // 
            this.xrRichText35.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrRichText35.CanShrink = true;
            this.xrRichText35.Dpi = 254F;
            this.xrRichText35.KeepTogether = true;
            this.xrRichText35.LocationFloat = new DevExpress.Utils.PointFloat(2.356725F, 323.7559F);
            this.xrRichText35.Name = "xrRichText35";
            this.xrRichText35.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 10, 0, 0, 254F);
            this.xrRichText35.SerializableRtfString = resources.GetString("xrRichText35.SerializableRtfString");
            this.xrRichText35.SizeF = new System.Drawing.SizeF(1875.565F, 305.7729F);
            this.xrRichText35.StylePriority.UsePadding = false;
            this.xrRichText35.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7BDesempenhoFundo_PrintOnPage);
            // 
            // xrPanel2
            // 
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText24,
            this.xrTable7,
            this.xrRichText25});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(24.73371F, 6809.847F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel2.SizeF = new System.Drawing.SizeF(1888.689F, 1368.948F);
            this.xrPanel2.Visible = false;
            this.xrPanel2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item9BeforePrint);
            // 
            // xrRichText24
            // 
            this.xrRichText24.CanShrink = true;
            this.xrRichText24.Dpi = 254F;
            this.xrRichText24.LocationFloat = new DevExpress.Utils.PointFloat(29.58569F, 24.99977F);
            this.xrRichText24.Name = "xrRichText24";
            this.xrRichText24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText24.SerializableRtfString = resources.GetString("xrRichText24.SerializableRtfString");
            this.xrRichText24.SizeF = new System.Drawing.SizeF(1840F, 500.1167F);
            this.xrRichText24.StylePriority.UsePadding = false;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(34.56939F, 562.9314F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow26,
            this.xrTableRow25,
            this.xrTableRow27});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1840F, 317F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableItem9_BeforePrint);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell55,
            this.xrTableCell52});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.574468085106383;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "Simulação das Despesas ";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.74718792127526323;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UsePadding = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "[+3 anos]";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.115370509935462;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "[+5 anos]";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell52.Weight = 0.13744156878927483;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell65,
            this.xrTableCell53});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.574468085106383;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell61.StylePriority.UsePadding = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Saldo bruto acumulado (hipotético - rentabilidade bruta anual de 10%) ";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 0.74718792127526323;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell65.StylePriority.UsePadding = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell65.Weight = 0.115370509935462;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 0.13744156878927483;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell60,
            this.xrTableCell54});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.58510638297872342;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "Despesas previstas (se a TAXA TOTAL DE DESPESAS se mantiver constante)";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell56.Weight = 0.74718792127526323;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell60.StylePriority.UsePadding = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.115370509935462;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell54.StylePriority.UsePadding = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 0.13744156878927483;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell70,
            this.xrTableCell57});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1.6382978723404258;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell66.StylePriority.UsePadding = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "Retorno bruto hipotético após dedução das despesas e do valor do investimento ori" +
                "ginal (antes da incidência de impostos, de taxas de ingresso e/ou saída, ou de t" +
                "axa de performance)";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell66.Weight = 0.74718792127526323;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell70.StylePriority.UsePadding = false;
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell70.Weight = 0.115370509935462;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.13744156878927483;
            // 
            // xrRichText25
            // 
            this.xrRichText25.CanShrink = true;
            this.xrRichText25.Dpi = 254F;
            this.xrRichText25.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 923.0386F);
            this.xrRichText25.Name = "xrRichText25";
            this.xrRichText25.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText25.SerializableRtfString = resources.GetString("xrRichText25.SerializableRtfString");
            this.xrRichText25.SizeF = new System.Drawing.SizeF(1840.505F, 410.1582F);
            this.xrRichText25.StylePriority.UsePadding = false;
            // 
            // xrRichText31
            // 
            this.xrRichText31.CanShrink = true;
            this.xrRichText31.Dpi = 254F;
            this.xrRichText31.KeepTogether = true;
            this.xrRichText31.LocationFloat = new DevExpress.Utils.PointFloat(48.16235F, 8243.679F);
            this.xrRichText31.Name = "xrRichText31";
            this.xrRichText31.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText31.SerializableRtfString = resources.GetString("xrRichText31.SerializableRtfString");
            this.xrRichText31.SizeF = new System.Drawing.SizeF(1899.583F, 80F);
            this.xrRichText31.StylePriority.UsePadding = false;
            // 
            // xrRichText30
            // 
            this.xrRichText30.CanShrink = true;
            this.xrRichText30.Dpi = 254F;
            this.xrRichText30.KeepTogether = true;
            this.xrRichText30.LocationFloat = new DevExpress.Utils.PointFloat(42.28997F, 8921F);
            this.xrRichText30.Name = "xrRichText30";
            this.xrRichText30.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText30.SerializableRtfString = resources.GetString("xrRichText30.SerializableRtfString");
            this.xrRichText30.SizeF = new System.Drawing.SizeF(1914.372F, 346.6592F);
            this.xrRichText30.StylePriority.UsePadding = false;
            // 
            // xrRichText29
            // 
            this.xrRichText29.CanShrink = true;
            this.xrRichText29.Dpi = 254F;
            this.xrRichText29.KeepTogether = true;
            this.xrRichText29.LocationFloat = new DevExpress.Utils.PointFloat(79.40076F, 8761.732F);
            this.xrRichText29.Name = "xrRichText29";
            this.xrRichText29.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText29.SerializableRtfString = resources.GetString("xrRichText29.SerializableRtfString");
            this.xrRichText29.SizeF = new System.Drawing.SizeF(1873.775F, 113.8242F);
            this.xrRichText29.StylePriority.UsePadding = false;
            this.xrRichText29.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco10Reclamacoes_PrintOnPage);
            // 
            // xrRichText28
            // 
            this.xrRichText28.CanShrink = true;
            this.xrRichText28.Dpi = 254F;
            this.xrRichText28.KeepTogether = true;
            this.xrRichText28.LocationFloat = new DevExpress.Utils.PointFloat(79.40076F, 8659.813F);
            this.xrRichText28.Name = "xrRichText28";
            this.xrRichText28.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText28.SerializableRtfString = resources.GetString("xrRichText28.SerializableRtfString");
            this.xrRichText28.SizeF = new System.Drawing.SizeF(1873.98F, 80F);
            this.xrRichText28.StylePriority.UsePadding = false;
            this.xrRichText28.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco10UrlAtendimento_PrintOnPage);
            // 
            // xrRichText27
            // 
            this.xrRichText27.CanShrink = true;
            this.xrRichText27.Dpi = 254F;
            this.xrRichText27.KeepTogether = true;
            this.xrRichText27.LocationFloat = new DevExpress.Utils.PointFloat(79.40076F, 8562.018F);
            this.xrRichText27.Name = "xrRichText27";
            this.xrRichText27.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText27.SerializableRtfString = resources.GetString("xrRichText27.SerializableRtfString");
            this.xrRichText27.SizeF = new System.Drawing.SizeF(1869.677F, 80F);
            this.xrRichText27.StylePriority.UsePadding = false;
            this.xrRichText27.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco10Telefone_PrintOnPage);
            // 
            // xrRichText26
            // 
            this.xrRichText26.CanShrink = true;
            this.xrRichText26.Dpi = 254F;
            this.xrRichText26.KeepTogether = true;
            this.xrRichText26.LocationFloat = new DevExpress.Utils.PointFloat(42.28997F, 8464.92F);
            this.xrRichText26.Name = "xrRichText26";
            this.xrRichText26.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText26.SerializableRtfString = resources.GetString("xrRichText26.SerializableRtfString");
            this.xrRichText26.SizeF = new System.Drawing.SizeF(1910.15F, 80F);
            this.xrRichText26.StylePriority.UsePadding = false;
            // 
            // xrRichText14
            // 
            this.xrRichText14.CanShrink = true;
            this.xrRichText14.Dpi = 254F;
            this.xrRichText14.KeepTogether = true;
            this.xrRichText14.LocationFloat = new DevExpress.Utils.PointFloat(48.45013F, 3626.917F);
            this.xrRichText14.Name = "xrRichText14";
            this.xrRichText14.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText14.SerializableRtfString = resources.GetString("xrRichText14.SerializableRtfString");
            this.xrRichText14.SizeF = new System.Drawing.SizeF(1840F, 74.61133F);
            this.xrRichText14.StylePriority.UsePadding = false;
            this.xrRichText14.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco7Titulo_PrintOnPage);
            this.xrRichText14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco7Titulo_BeforePrint);
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(327.6505F, 3356.588F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21,
            this.xrTableRow23});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1321.384F, 178.0762F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item6_BeforePrint);
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell45,
            this.xrTableCell47,
            this.xrTableCell40,
            this.xrTableCell49});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.574468085106383;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "1";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell39.Weight = 0.20633099916581063;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = "2";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell45.Weight = 0.21829647319381421;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "3";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell47.Weight = 0.19493774503167949;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UsePadding = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "4";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 0.19021739130434784;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "5";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 0.19021739130434784;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell46,
            this.xrTableCell48,
            this.xrTableCell44,
            this.xrTableCell50});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 2.1276547647739741;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.CanGrow = false;
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.20633099916581063;
            this.xrTableCell43.WordWrap = false;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(61.72863F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(159.4827F, 123.042F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox1.Visible = false;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.CanGrow = false;
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox2});
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.21829647319381421;
            this.xrTableCell46.WordWrap = false;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox2.Dpi = 254F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(70.44971F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(150.4969F, 109.5627F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox2.Visible = false;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.CanGrow = false;
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox3});
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.19493774503167949;
            this.xrTableCell48.WordWrap = false;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox3.Dpi = 254F;
            this.xrPictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox3.Image")));
            this.xrPictureBox3.LocationFloat = new DevExpress.Utils.PointFloat(59.62627F, 0F);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.SizeF = new System.Drawing.SizeF(150.4969F, 114.0562F);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox3.Visible = false;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.CanGrow = false;
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox4});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.19021739130434784;
            this.xrTableCell44.WordWrap = false;
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox4.Dpi = 254F;
            this.xrPictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox4.Image")));
            this.xrPictureBox4.LocationFloat = new DevExpress.Utils.PointFloat(51.95739F, 0.0008226791F);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.SizeF = new System.Drawing.SizeF(150.4968F, 123.0415F);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox4.Visible = false;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.CanGrow = false;
            this.xrTableCell50.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox5});
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 0.19021739130434784;
            this.xrTableCell50.WordWrap = false;
            // 
            // xrPictureBox5
            // 
            this.xrPictureBox5.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox5.Dpi = 254F;
            this.xrPictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox5.Image")));
            this.xrPictureBox5.LocationFloat = new DevExpress.Utils.PointFloat(48.89579F, 0.001096905F);
            this.xrPictureBox5.Name = "xrPictureBox5";
            this.xrPictureBox5.SizeF = new System.Drawing.SizeF(150.4968F, 114.0557F);
            this.xrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            this.xrPictureBox5.Visible = false;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(52.06438F, 3281.228F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1709.364F, 54F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.574468085106383;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(150, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UsePadding = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = "Menor Risco";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.46077210799507462;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 40, 0, 0, 254F);
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UsePadding = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "Maior Risco";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.46823007334833566;
            // 
            // xrRichText15
            // 
            this.xrRichText15.CanShrink = true;
            this.xrRichText15.Dpi = 254F;
            this.xrRichText15.LocationFloat = new DevExpress.Utils.PointFloat(30.73054F, 3138.786F);
            this.xrRichText15.Name = "xrRichText15";
            this.xrRichText15.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText15.SerializableRtfString = resources.GetString("xrRichText15.SerializableRtfString");
            this.xrRichText15.SizeF = new System.Drawing.SizeF(1798.895F, 112.4713F);
            this.xrRichText15.StylePriority.UsePadding = false;
            this.xrRichText15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco6Risco_PrintOnPage);
            // 
            // xrRichText13
            // 
            this.xrRichText13.CanShrink = true;
            this.xrRichText13.Dpi = 254F;
            this.xrRichText13.LocationFloat = new DevExpress.Utils.PointFloat(33.5874F, 2681.815F);
            this.xrRichText13.Name = "xrRichText13";
            this.xrRichText13.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText13.SerializableRtfString = resources.GetString("xrRichText13.SerializableRtfString");
            this.xrRichText13.SizeF = new System.Drawing.SizeF(1798.895F, 112.4713F);
            this.xrRichText13.StylePriority.UsePadding = false;
            this.xrRichText13.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco5ComposicaoCarteira_PrintOnPage);
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(39.37314F, 1958.158F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1747.79F, 701.9999F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item4_BeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.StylePriority.UseFont = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow5.Weight = 0.57446808510638292;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Investimento inicial mínimo";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.51863990270688276;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.Weight = 0.43378099946429138;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.574468085106383;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Investimento adicional mínimo";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.51863990270688276;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.43378099946429138;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.574468085106383;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.Text = "Resgate mínimo";
            this.xrTableCell15.Weight = 0.51863990270688276;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.Weight = 0.43378099946429138;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.574468085106383;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.Text = "Horário para aplicação e resgate";
            this.xrTableCell13.Weight = 0.51863990270688276;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.Weight = 0.43378099946429138;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.574468085106383;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "Valor mínimo para permanência";
            this.xrTableCell17.Weight = 0.51863990270688276;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Weight = 0.43378099946429138;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.574468085106383;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "Prazo de carência";
            this.xrTableCell19.Weight = 0.51863990270688276;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.Weight = 0.43378099946429138;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.574468085106383;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "Conversão das cotas";
            this.xrTableCell21.Weight = 0.51863990270688276;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.Weight = 0.43378099946429138;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.574468085106383;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "Pagamento dos resgates";
            this.xrTableCell23.Weight = 0.51863990270688276;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.Weight = 0.43378099946429138;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.574468085106383;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Taxa de administração";
            this.xrTableCell26.Weight = 0.51863990270688276;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.Weight = 0.43378099946429138;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell32});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.574468085106383;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "Taxa de entrada";
            this.xrTableCell29.Weight = 0.51863990270688276;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.Weight = 0.43378099946429138;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.574468085106383;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "Taxa de saída";
            this.xrTableCell33.Weight = 0.51863990270688276;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.Weight = 0.43378099946429138;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.574468085106383;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "Taxa de performance";
            this.xrTableCell35.Weight = 0.51863990270688276;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UsePadding = false;
            this.xrTableCell36.Weight = 0.43378099946429138;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.574468085106383;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "Taxa total de despesas";
            this.xrTableCell37.Weight = 0.51863990270688276;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.Weight = 0.43378099946429138;
            // 
            // xrRichText11
            // 
            this.xrRichText11.CanShrink = true;
            this.xrRichText11.Dpi = 254F;
            this.xrRichText11.LocationFloat = new DevExpress.Utils.PointFloat(43.86446F, 1830.684F);
            this.xrRichText11.Name = "xrRichText11";
            this.xrRichText11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText11.SerializableRtfString = resources.GetString("xrRichText11.SerializableRtfString");
            this.xrRichText11.SizeF = new System.Drawing.SizeF(1798.895F, 80F);
            this.xrRichText11.StylePriority.UseBorders = false;
            this.xrRichText11.StylePriority.UsePadding = false;
            // 
            // xrRichText12
            // 
            this.xrRichText12.CanShrink = true;
            this.xrRichText12.Dpi = 254F;
            this.xrRichText12.LocationFloat = new DevExpress.Utils.PointFloat(42.25F, 1691.45F);
            this.xrRichText12.Name = "xrRichText12";
            this.xrRichText12.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText12.SerializableRtfString = resources.GetString("xrRichText12.SerializableRtfString");
            this.xrRichText12.SizeF = new System.Drawing.SizeF(1906.828F, 66.25F);
            this.xrRichText12.StylePriority.UsePadding = false;
            this.xrRichText12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco3D_BeforePrint);
            // 
            // xrRichText10
            // 
            this.xrRichText10.CanShrink = true;
            this.xrRichText10.Dpi = 254F;
            this.xrRichText10.LocationFloat = new DevExpress.Utils.PointFloat(42.25F, 1594.905F);
            this.xrRichText10.Name = "xrRichText10";
            this.xrRichText10.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText10.SerializableRtfString = resources.GetString("xrRichText10.SerializableRtfString");
            this.xrRichText10.SizeF = new System.Drawing.SizeF(1910.19F, 66.24988F);
            this.xrRichText10.StylePriority.UsePadding = false;
            this.xrRichText10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco3C_PrintOnPage);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(42.25F, 1402.375F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1840F, 171.5237F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.StylePriority.UseFont = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.425531914893617;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.CanShrink = true;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = resources.GetString("xrTableCell5.Text");
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 1;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(97.41805F, 1134.692F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow1,
            this.xrTableRow2});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1589.307F, 230F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item3B_BeforePrint);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell28});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow13.StylePriority.UseFont = false;
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.54428500742206842;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Aplicar em ativos no exterior até o limite de";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.58119668579198014;
            this.xrTableCell25.WordWrap = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.Weight = 0.41880331420801986;
            this.xrTableCell28.WordWrap = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.51954477981197433;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Aplicar em crédito privado até o limite de";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.58119668579198014;
            this.xrTableCell30.WordWrap = false;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 5, 0, 0, 254F);
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 0.41880331420801986;
            this.xrTableCell31.WordWrap = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.53191489361702138;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Utiliza derivativos apenas para proteção da carteira?";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.58119668579198014;
            this.xrTableCell1.WordWrap = false;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Weight = 0.41880331420801986;
            this.xrTableCell2.WordWrap = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.53191489361702127;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Alavancar-se até o limite de (i)";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.58119668579198014;
            this.xrTableCell3.WordWrap = false;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(21, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.Weight = 0.41880331420801986;
            this.xrTableCell4.WordWrap = false;
            // 
            // xrRichText9
            // 
            this.xrRichText9.CanShrink = true;
            this.xrRichText9.Dpi = 254F;
            this.xrRichText9.LocationFloat = new DevExpress.Utils.PointFloat(42.29F, 1049.807F);
            this.xrRichText9.Name = "xrRichText9";
            this.xrRichText9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText9.SerializableRtfString = resources.GetString("xrRichText9.SerializableRtfString");
            this.xrRichText9.SizeF = new System.Drawing.SizeF(1913F, 55F);
            this.xrRichText9.StylePriority.UsePadding = false;
            // 
            // xrRichText8
            //             
            this.xrRichText8.CanShrink = true;
            this.xrRichText8.Dpi = 254F;
            this.xrRichText8.LocationFloat = new DevExpress.Utils.PointFloat(42F, 890F);
            this.xrRichText8.Name = "xrRichText8";
            this.xrRichText8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText8.SerializableRtfString = resources.GetString("xrRichText8.SerializableRtfString");
            this.xrRichText8.SizeF = new System.Drawing.SizeF(1913.167F, 54.99988F);
            this.xrRichText8.StylePriority.UsePadding = false;
            // 
            // xrRichText7
            //             
            this.xrRichText7.CanShrink = true;
            this.xrRichText7.Dpi = 254F;
            this.xrRichText7.LocationFloat = new DevExpress.Utils.PointFloat(42.29F, 967.0199F);
            this.xrRichText7.Name = "xrRichText7";
            this.xrRichText7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText7.SerializableRtfString = resources.GetString("xrRichText7.SerializableRtfString");
            this.xrRichText7.SizeF = new System.Drawing.SizeF(1913F, 66.25F);
            this.xrRichText7.StylePriority.UsePadding = false;
            this.xrRichText7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco3PoliticaInvestimentos_PrintOnPage);
            // 
            // xrRichText6
            //             
            this.xrRichText6.CanShrink = true;
            this.xrRichText6.Dpi = 254F;
            this.xrRichText6.LocationFloat = new DevExpress.Utils.PointFloat(42.28999F, 788.6874F);
            this.xrRichText6.Name = "xrRichText6";
            this.xrRichText6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText6.SerializableRtfString = resources.GetString("xrRichText6.SerializableRtfString");
            this.xrRichText6.SizeF = new System.Drawing.SizeF(1914.71F, 73.93237F);
            this.xrRichText6.StylePriority.UsePadding = false;
            this.xrRichText6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco2ObjetivoFundo_PrintOnPage);
            // 
            // xrRichText5
            // 
            this.xrRichText5.CanShrink = true;
            this.xrRichText5.Dpi = 254F;
            this.xrRichText5.LocationFloat = new DevExpress.Utils.PointFloat(42.29307F, 670F);
            this.xrRichText5.Name = "xrRichText5";
            this.xrRichText5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText5.SerializableRtfString = resources.GetString("xrRichText5.SerializableRtfString");
            this.xrRichText5.SizeF = new System.Drawing.SizeF(1906.795F, 78.48511F);
            this.xrRichText5.StylePriority.UsePadding = false;
            this.xrRichText5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco1PublicoAlvo_PrintOnPage);
            // 
            // xrRichText3
            // 
            this.xrRichText3.CanShrink = true;
            this.xrRichText3.Dpi = 254F;
            this.xrRichText3.LocationFloat = new DevExpress.Utils.PointFloat(153.6902F, 600F);
            this.xrRichText3.Name = "xrRichText3";
            this.xrRichText3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText3.SerializableRtfString = resources.GetString("xrRichText3.SerializableRtfString");
            this.xrRichText3.SizeF = new System.Drawing.SizeF(1584.977F, 45.33374F);
            this.xrRichText3.StylePriority.UsePadding = false;
            // 
            // xrRichText4
            // 
            this.xrRichText4.CanShrink = true;
            this.xrRichText4.Dpi = 254F;
            this.xrRichText4.LocationFloat = new DevExpress.Utils.PointFloat(46.86787F, 338.9093F);
            this.xrRichText4.Name = "xrRichText4";
            this.xrRichText4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText4.SerializableRtfString = resources.GetString("xrRichText4.SerializableRtfString");
            this.xrRichText4.SizeF = new System.Drawing.SizeF(1890.866F, 252.5031F);
            this.xrRichText4.StylePriority.UsePadding = false;
            this.xrRichText4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco3_PrintOnPage);
            // 
            // xrLabel9
            // 
            this.xrLabel9.CanShrink = true;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter1, "Text", "")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(4.928293F, 160F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(1948.453F, 61.74199F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabel9.WordWrap = false;
            // 
            // parameter1
            // 
            this.parameter1.Name = "parameter1";
            // 
            // xrRichText1
            // 
            this.xrRichText1.CanShrink = true;
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(46.86784F, 247.6377F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1890.866F, 71.17589F);
            this.xrRichText1.StylePriority.UsePadding = false;
            this.xrRichText1.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco2_PrintOnPage);
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText21,
            this.xrRichText22,
            this.xrRichText23});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(28.86924F, 6031.886F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1909F, 760.2725F);
            this.xrPanel1.Visible = false;
            this.xrPanel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item8BeforePrint);
            // 
            // xrRichText21
            // 
            this.xrRichText21.CanShrink = true;
            this.xrRichText21.Dpi = 254F;
            this.xrRichText21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0009689331F);
            this.xrRichText21.Name = "xrRichText21";
            this.xrRichText21.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText21.SerializableRtfString = resources.GetString("xrRichText21.SerializableRtfString");
            this.xrRichText21.SizeF = new System.Drawing.SizeF(1840F, 212.0283F);
            this.xrRichText21.StylePriority.UsePadding = false;
            // 
            // xrRichText22
            // 
            this.xrRichText22.CanShrink = true;
            this.xrRichText22.Dpi = 254F;
            this.xrRichText22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 236.7349F);
            this.xrRichText22.Name = "xrRichText22";
            this.xrRichText22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText22.SerializableRtfString = resources.GetString("xrRichText22.SerializableRtfString");
            this.xrRichText22.SizeF = new System.Drawing.SizeF(1840F, 278.4282F);
            this.xrRichText22.StylePriority.UsePadding = false;
            this.xrRichText22.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco8A_PrintOnPage);
            // 
            // xrRichText23
            // 
            this.xrRichText23.CanShrink = true;
            this.xrRichText23.Dpi = 254F;
            this.xrRichText23.LocationFloat = new DevExpress.Utils.PointFloat(8.726097F, 530.1309F);
            this.xrRichText23.Name = "xrRichText23";
            this.xrRichText23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText23.SerializableRtfString = resources.GetString("xrRichText23.SerializableRtfString");
            this.xrRichText23.SizeF = new System.Drawing.SizeF(1840F, 185.5693F);
            this.xrRichText23.StylePriority.UsePadding = false;
            this.xrRichText23.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco8B_PrintOnPage);
            // 
            // xrRichText20
            // 
            this.xrRichText20.CanShrink = true;
            this.xrRichText20.Dpi = 254F;
            this.xrRichText20.KeepTogether = true;
            this.xrRichText20.LocationFloat = new DevExpress.Utils.PointFloat(96.33783F, 8338.65F);
            this.xrRichText20.Name = "xrRichText20";
            this.xrRichText20.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText20.SerializableRtfString = resources.GetString("xrRichText20.SerializableRtfString");
            this.xrRichText20.SizeF = new System.Drawing.SizeF(1817.085F, 90F);
            this.xrRichText20.StylePriority.UsePadding = false;
            this.xrRichText20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco10PoliticaDistribuicao_PrintOnPage);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(1850F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(87.86926F, 40.00014F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdPessoa")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Endereco")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(42F, 85F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Numero")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(339F, 85F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Complemento")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(614F, 85F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Bairro")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(42F, 169F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cidade")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(42F, 254F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Uf")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel7.Text = "xrLabel7";
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 2.243655F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Visible = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 22F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText32,
            this.xrPageInfo3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 91.2271F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrRichText32
            // 
            this.xrRichText32.CanGrow = false;
            this.xrRichText32.Dpi = 254F;
            this.xrRichText32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrRichText32.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 1.227315F);
            this.xrRichText32.Name = "xrRichText32";
            this.xrRichText32.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText32.SerializableRtfString = resources.GetString("xrRichText32.SerializableRtfString");
            this.xrRichText32.SizeF = new System.Drawing.SizeF(1804.626F, 64.99991F);
            this.xrRichText32.StylePriority.UseFont = false;
            this.xrRichText32.StylePriority.UsePadding = false;
            this.xrRichText32.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Rodape_PrintOnPage);
            // 
            // ReportLaminaExportacao
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.PageFooter});
            this.DetailPrintCount = 1;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(101, 78, 150, 22);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter1});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region Funções Personalizadas

        // Ordena um dicionario por valor
        private Dictionary<string, decimal> SortDictionaryByValue(Dictionary<string, decimal> myDictionary) {
            List<KeyValuePair<string, decimal>> tempList = new List<KeyValuePair<string, decimal>>(myDictionary);

            tempList.Sort(delegate(KeyValuePair<string, decimal> firstPair, KeyValuePair<string, decimal> secondPair) {
                return firstPair.Value.CompareTo(secondPair.Value);
            }
                         );

            Dictionary<string, decimal> mySortedDictionary = new Dictionary<string, decimal>();
            foreach (KeyValuePair<string, decimal> pair in tempList) {
                mySortedDictionary.Add(pair.Key, pair.Value);
            }

            return mySortedDictionary;
        }
         
        private void Bloco1_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", this.cliente.Apelido.Trim());
        }

        private void Bloco2_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            string mes = Utilitario.RetornaMesString(data.Month);
            valor.Rtf = valor.Rtf.Replace("[#1]", mes);
            valor.Rtf = valor.Rtf.Replace("[#2]", data.Year.ToString());
        }

        private void Bloco3_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", cliente.Nome);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);
            //
            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
            //
            AgenteMercado agenteGestor = new AgenteMercado();
            agenteGestor.LoadByPrimaryKey(carteira.IdAgenteGestor.Value);

            valor.Rtf = valor.Rtf.Replace("[#2]", !String.IsNullOrEmpty(agenteAdministrador.Nome) ? agenteAdministrador.Nome.Trim() : "-");
            valor.Rtf = valor.Rtf.Replace("[#3]", !String.IsNullOrEmpty(agenteGestor.Nome) ? agenteGestor.Nome.Trim() : "-");
            valor.Rtf = valor.Rtf.Replace("[#4]", !String.IsNullOrEmpty(lamina.EnderecoEletronico) ? lamina.EnderecoEletronico.Trim() : "-");
        }

        private void Bloco3C_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            bool fundoInvestimento = !String.IsNullOrEmpty(lamina.FundosInvestimento);
            bool fundoInvestimentoCotas = !String.IsNullOrEmpty(lamina.FundosInvestimentoCotas);

            // tenta carregar fundoInvestimento se nao carrega fundoInvestimentoCotas
            string texto = "";
            if (fundoInvestimento) {
                texto = lamina.FundosInvestimento;
            }
            else if (fundoInvestimentoCotas) {
                texto = lamina.FundosInvestimentoCotas;
            }
            valor.Rtf = valor.Rtf.Replace("[#1]", texto);
        }

        private void Bloco3D_PrintOnPage(object sender, PrintOnPageEventArgs e) {            
        }

        private void Bloco3D_BeforePrint(object sender, PrintEventArgs e) {
            XRRichText panel = sender as XRRichText;
            panel.Visible = this.lamina.EstrategiaPerdas.Trim().ToUpper() == "S";

            XRRichText valor = sender as XRRichText;
            bool regulamentoPerdasPatrimoniais = !String.IsNullOrEmpty(lamina.RegulamentoPerdasPatrimoniais);
            bool regulamentoPatrimonioNegativo = !String.IsNullOrEmpty(lamina.RegulamentoPatrimonioNegativo);

            // Tenta Carregar regulamentoPerdasPatrimoniais se nao carrega regulamentoPatrimonioNegativo
            string texto = "";
            if (regulamentoPerdasPatrimoniais) {
                texto = lamina.RegulamentoPerdasPatrimoniais;
            }
            if (regulamentoPatrimonioNegativo) {
                if (!String.IsNullOrEmpty(texto)) {
                    if (!texto.EndsWith(".")) {
                        texto += ".";
                    }
                    texto += " ";
                }
                texto += lamina.RegulamentoPatrimonioNegativo;
            }
            valor.Rtf = valor.Rtf.Replace("[#1]", texto);
        }

        private void Bloco1PublicoAlvo_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;                       

            string publicoAlvo = !String.IsNullOrEmpty(lamina.DescricaoPublicoAlvo) ? lamina.DescricaoPublicoAlvo.Trim() : "";
            if(publicoAlvo.EndsWith(".")) {
                publicoAlvo = publicoAlvo.Remove(publicoAlvo.Length-1);
            }

            valor.Rtf = valor.Rtf.Replace("[#1]", publicoAlvo);
        }

        private void Bloco2ObjetivoFundo_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;

            string objetivo = !String.IsNullOrEmpty(lamina.ObjetivoFundo) ? lamina.ObjetivoFundo.Trim() : "";
            if (objetivo.EndsWith(".")) {
                objetivo = objetivo.Remove(objetivo.Length - 1);
            }
            valor.Rtf = valor.Rtf.Replace("[#1]", objetivo);
        }

        private void Bloco3PoliticaInvestimentos_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;

            string politica = !String.IsNullOrEmpty(lamina.DescricaoPoliticaInvestimento) ? lamina.DescricaoPoliticaInvestimento.Trim() : "";
            if (politica.EndsWith(".")) {
                politica = politica.Remove(politica.Length - 1);
            }

            valor.Rtf = valor.Rtf.Replace("[#1]", politica);
        }
        
        private void Bloco10PoliticaDistribuicao_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.PoliticaDistribuicao) ? lamina.PoliticaDistribuicao.Trim() : "");
        }

        private void Bloco8A_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            string anoAnterior = Convert.ToString(this.data.Year - 1);
            string ano = this.data.Year.ToString(); ;

            valor.Rtf = valor.Rtf.Replace("[#1]", anoAnterior);
            valor.Rtf = valor.Rtf.Replace("[#2]", ano);

            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(this.data.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilAno(new DateTime(this.data.Year, 1, 1));
            //
            HistoricoCota historicoCota1 = new HistoricoCota();
            historicoCota1.LoadByPrimaryKey(dataInicio, this.idCliente.Value);
            decimal cotaFechamentoInicio = historicoCota1.CotaFechamento.HasValue ? historicoCota1.CotaFechamento.Value : 0;
            //
            HistoricoCota historicoCota2 = new HistoricoCota();
            historicoCota2.LoadByPrimaryKey(dataFim, this.idCliente.Value);
            decimal cotaFechamentoFim = historicoCota2.CotaFechamento.HasValue ? historicoCota2.CotaFechamento.Value : 0;

            decimal valorBruto = 0;
            if (historicoCota1.CotaFechamento.HasValue) {
                valorBruto = 1000 * (cotaFechamentoFim / cotaFechamentoInicio);
            }
            else {
                valorBruto = 1000 * 1;
            }

            decimal valorIR = 0;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Acoes) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.15M;
            }
            else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Isento) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0M;
            }
            else {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.20M;
            }

            valorBruto -= valorIR;

            valor.Rtf = valor.Rtf.Replace("[#3]", valorBruto.ToString("N2"));
            valor.Rtf = valor.Rtf.Replace("[#4]", valorIR.ToString("N2"));

            //
            valor.Rtf = valor.Rtf.Replace("[#5]", "");
            // Não implementado
            //// Se tem Taxa de Entrada/Saida
            //if ( !String.IsNullOrEmpty(lamina.TaxaEntrada) && !String.IsNullOrEmpty(lamina.TaxaSaida) ) {
            //    // TODO ajuste sobre performance individual
            //    string texto = "A taxa de ingresso teria custado R$ " + lamina.TaxaEntrada + ", a taxa de saída teria custado R$ " +lamina.TaxaSaida+ ", e o ajuste sobre performance individual teria custado R$ .";                
            //    valor.Rtf = valor.Rtf.Replace("[#5]", texto);                
            //}
            //else {
            //    valor.Rtf = valor.Rtf.Replace("[#5]", "");
            //}                                    
        }

        private void Bloco8B_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;

            decimal despesas = this.CalculaDespesas();

            valor.Rtf = valor.Rtf.Replace("[#1]", despesas.ToString("N2"));
        }

        /// <summary>
        /// Para os itens 8B e 9
        /// </summary>
        /// <returns></returns>
        private decimal CalculaDespesas() {
            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(this.data.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilAno(new DateTime(this.data.Year, 1, 1));

            #region
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == this.idCliente,
                                  liquidacaoQuery.DataVencimento.Between(dataInicio, dataFim),
                                  liquidacaoQuery.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaFiscalizacaoCVM,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros
                                                            ));
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;
            return despesas;
        }

        #region 11. Atendimento ao Cotista
        private void Bloco10Telefone_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.Telefone) ? lamina.Telefone.Trim() : "");
        }

        private void Bloco10UrlAtendimento_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.UrlAtendimento) ? lamina.UrlAtendimento.Trim() : "");
        }

        private void Bloco10Reclamacoes_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.Reclamacoes) ? lamina.Reclamacoes.Trim() : "");
        }
        #endregion

        private void Item3B_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            //
            ((XRTableCell)linha0.Cells[1]).Text = this.lamina.LimiteAplicacaoExterior.HasValue ? this.lamina.LimiteAplicacaoExterior.Value.ToString("N2") + "% do Patrimônio Líquido " : "Não";
            ((XRTableCell)linha1.Cells[1]).Text = this.lamina.LimiteCreditoPrivado.HasValue ? this.lamina.LimiteCreditoPrivado.Value.ToString("N2") + "% do Patrimônio Líquido " : "Não";
            ((XRTableCell)linha2.Cells[1]).Text = !String.IsNullOrEmpty(this.lamina.DerivativosProtecaoCarteira) ? this.lamina.DerivativosProtecaoCarteira.ToUpper() == "S" ? "Sim" : "Não"
                                                                                                                 : "";
            ((XRTableCell)linha3.Cells[1]).Text = this.lamina.LimiteAlavancagem.HasValue ? this.lamina.LimiteAlavancagem.Value.ToString("N2") + "% do Patrimônio Líquido " : "Sem Alavancagem";
        }

        private void Item4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            XRTableRow linha4 = table.Rows[4];
            XRTableRow linha5 = table.Rows[5];
            XRTableRow linha6 = table.Rows[6];
            XRTableRow linha7 = table.Rows[7];
            XRTableRow linha8 = table.Rows[8];
            XRTableRow linha9 = table.Rows[9];
            XRTableRow linha10 = table.Rows[10];
            XRTableRow linha11 = table.Rows[11];
            XRTableRow linha12 = table.Rows[12];
            //
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);
            //
            //
            ((XRTableCell)linha0.Cells[1]).Text = "R$ " + carteira.ValorMinimoInicial.Value.ToString("N2");
            ((XRTableCell)linha1.Cells[1]).Text = "R$ " + carteira.ValorMinimoAplicacao.Value.ToString("N2");
            ((XRTableCell)linha2.Cells[1]).Text = "R$ " + carteira.ValorMinimoResgate.Value.ToString("N2");
            //            
            string horario = "";
            if (carteira.HorarioFim.HasValue && carteira.HorarioFimResgate.HasValue) {
                horario = carteira.HorarioFim.Value.ToString("hh:mm:ss") + " às " + carteira.HorarioFimResgate.Value.ToString("hh:mm:ss");
            }
            ((XRTableCell)linha3.Cells[1]).Text = horario;
            ((XRTableCell)linha4.Cells[1]).Text = "R$ " + carteira.ValorMinimoSaldo.Value.ToString("N2");

            string carencia = "";
            if (!String.IsNullOrEmpty(this.lamina.PrazoCarencia)) {
                carencia = "Os recursos investidos no fundo não podem ser resgatados antes de " + this.lamina.PrazoCarencia.Trim() + " dias contados da data da aplicação.";
            }

            ((XRTableCell)linha5.Cells[1]).Text = !String.IsNullOrEmpty(carencia) ? carencia : "Não há";
            //
            int tipoCota = (int)carteira.TipoCota.Value;
            int diasConversaoAplic = carteira.DiasCotizacaoAplicacao.Value;
            int diasConversaoResgate = carteira.DiasCotizacaoResgate.Value;
            string contagem = carteira.ContagemDiasConversaoResgate == (int)ContagemDiasLiquidacaoResgate.DiasUteis ? "úteis" : "corridos";
            string cota = tipoCota == (int)TipoCotaFundo.Abertura ? "na abertura" : "no fechamento";

            string conversaoCotas = "Na aplicação, o número de cotas compradas será calculado de acordo com o valor das cotas " + cota + " do " + diasConversaoAplic.ToString() + " dia contado da data da aplicação.\n ";
            conversaoCotas += "No resgate, o número de cotas canceladas será calculado de acordo com o valor das cotas " + cota + " do " + diasConversaoResgate.ToString() + " dia contado da data do pedido de resgate.";
            //
            ((XRTableCell)linha6.Cells[1]).Text = conversaoCotas;
            //                        
            int diasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
            string resgate = "O prazo para o efetivo pagamento dos resgates é de " + diasLiquidacaoResgate.ToString() + "  dias " + contagem + " contados da data do pedido de resgate.";
            ((XRTableCell)linha7.Cells[1]).Text = resgate;
            //

            CadastroTaxaAdministracaoQuery cadastroTaxaAdm = new CadastroTaxaAdministracaoQuery("D");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia, tabelaTaxaAdministracaoQuery.Taxa,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.InnerJoin(cadastroTaxaAdm).On(tabelaTaxaAdministracaoQuery.IdCadastro == cadastroTaxaAdm.IdCadastro);
            tabelaTaxaAdministracaoQuery.Where((tabelaTaxaAdministracaoQuery.IdCarteira == this.idCliente),
                                               (cadastroTaxaAdm.Descricao.ToLower().Equal("TAXA ADMINISTRAÇÃO")));

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {
                decimal? taxa = tab.Taxa;
                ((XRTableCell)linha8.Cells[1]).Text = taxa.HasValue ? taxa.Value.ToString("N2") + "% do patrimônio líquido ao ano" : "";
            }

            string entrada = "";
            if (!String.IsNullOrEmpty(this.lamina.TaxaEntrada)) {
                entrada = "Para entrar no fundo, o investidor paga uma taxa de " + this.lamina.TaxaEntrada.Trim() + "% da aplicação inicial, que é deduzida diretamente do valor a ser aplicado.";
            }
            ((XRTableCell)linha9.Cells[1]).Text = !String.IsNullOrEmpty(entrada) ? entrada : "Não há";
            //

            string saida = "";
            if (!String.IsNullOrEmpty(this.lamina.TaxaSaida)) {
                saida = "Para resgatar suas cotas do fundo [, antes de decorridos " + diasConversaoAplic.ToString() + " dias da data de aplicação], o investidor paga uma taxa de " + this.lamina.TaxaSaida.Trim() + "% do valor do resgate, que é deduzida diretamente do valor a ser recebido.";
            }
            ((XRTableCell)linha10.Cells[1]).Text = !String.IsNullOrEmpty(saida) ? saida : "Não há";

            IndiceQuery indiceQuery = new IndiceQuery("I");
            TabelaTaxaPerformanceQuery tabelaTaxas = new TabelaTaxaPerformanceQuery("T");
            carteiraQuery = new CarteiraQuery("C");
            tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                  tabelaTaxas.DataReferencia,
                                  tabelaTaxas.TaxaPerformance,
                                  tabelaTaxas.PercentualIndice,
                                  carteiraQuery.IdCarteira,
                                  carteiraQuery.Apelido,
                                  indiceQuery.Descricao);
            tabelaTaxas.InnerJoin(carteiraQuery).On(tabelaTaxas.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxas.InnerJoin(indiceQuery).On(tabelaTaxas.IdIndice == indiceQuery.IdIndice);
            tabelaTaxas.Where(tabelaTaxas.IdCarteira == this.idCliente);

            TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
            tabelaTaxaPerformanceCollection.Load(tabelaTaxas);

            if (tabelaTaxaPerformanceCollection.HasData) {
                decimal? taxaPerformance = tabelaTaxaPerformanceCollection[0].TaxaPerformance;
                decimal? percentualIndicePerformance = tabelaTaxaPerformanceCollection[0].PercentualIndice;
                string   descricaoIndicie = tabelaTaxaPerformanceCollection[0].GetColumn(IndiceMetadata.ColumnNames.Descricao).ToString();
                
                if (taxaPerformance.HasValue) 
                {
                    ((XRTableCell)linha11.Cells[1]).Text = taxaPerformance.Value.ToString("N2")+"%"+ " do que exceder a " + percentualIndicePerformance.Value.ToString("N2") + "% do " + descricaoIndicie;
                }
                else
                {
                     ((XRTableCell)linha11.Cells[1]).Text = "Não há";
                }

            }

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //           
            #region Liquidacao
            DateTime data2 = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);
            DateTime dataAux = dataFim.AddMonths(-12);
            DateTime data1 = Calendario.RetornaUltimoDiaUtilMes(dataAux, 0);
            //
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == this.idCliente,
                                  liquidacaoQuery.DataVencimento.Between(data1, data2),
                                  liquidacaoQuery.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                  liquidacaoQuery.Valor < 0,
                                  clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo));

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;

            #region PL
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query
                 .Select(historicoCota.Query.PLFechamento.Sum())
                 .Where(historicoCota.Query.IdCarteira == this.idCliente,
                        historicoCota.Query.Data.Between(data1, data2));

            historicoCota.Query.Load();
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
            #endregion
            //

            // Média das despesas por patrimonio do fundo. 
            decimal? media = null;
            if (plFechamento > 0) {
                media = despesas / plFechamento;
            }
            //
            string texto = "";
            if (media.HasValue) {
                texto = "As despesas pagas pelo fundo representaram " + media.Value.ToString("N2") + "% do seu patrimônio líquido diário médio no período que vai de " + data1.ToString("d") + " a " + data2.ToString("d") + ". A taxa de despesas pode variar de período para período e reduz a rentabilidade do fundo. O quadro com a descrição das despesas do fundo pode ser encontrado em " + lamina.EnderecoEletronico + ".";
            }

            if (!String.IsNullOrEmpty(texto)) {
                ((XRTableCell)linha12.Cells[1]).Text = maisDeUmAno ? texto : "Não Há";
            }
        }

        private void Item5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            XRTableRow linha0 = table.Rows[0];
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            XRTableRow linha4 = table.Rows[4];
            //
            DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(this.data);

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataAux, this.idCliente.Value);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            decimal porcentagem1 = 0.0M;
            decimal porcentagem2 = 0.0M;
            decimal porcentagem3 = 0.0M;
            decimal porcentagem4 = 0.0M;
            decimal porcentagem5 = 0.0M;
            decimal porcentagem6 = 0.0M;
            decimal porcentagem7 = 0.0M;
            decimal porcentagem8 = 0.0M;
            decimal porcentagem9 = 0.0M;
            decimal porcentagem10 = 0.0M;
            decimal porcentagem11 = 0.0M;

            #region 1
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            decimal valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem1 = valormercado / plFechamento;
            }

            #endregion

            #region 2
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.IdCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Publico)
                                        );

            PosicaoRendaFixaHistoricoCollection oper = new PosicaoRendaFixaHistoricoCollection();

            oper.Load(posicaoRendaFixaQuery);

            decimal valor2 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor2 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem2 = valor2 / plFechamento;
            }

            #endregion

            #region 3
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.IdCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Privado)
                                        );

            oper = new PosicaoRendaFixaHistoricoCollection();
            oper.Load(posicaoRendaFixaQuery);

            decimal valor3 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor3 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem3 = valor3 / plFechamento;
            }
            #endregion

            #region 4
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real);

            PosicaoBolsaHistorico p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor4 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor4 = p.ValorMercado.Value;
            }
            if (plFechamento != 0) {
                porcentagem4 = valor4 / plFechamento;
            }
            #endregion

            #region 5
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.Classe.In((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                        (int)ClasseRendaFixa.LF,
                                                                        (int)ClasseRendaFixa.DPGE,
                                                                        (int)ClasseRendaFixa.CCB,
                                                                        (int)ClasseRendaFixa.LCA,
                                                                        (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            #region SaldoCaixa
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            //
            SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("S");
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            MoedaQuery moedaQuery = new MoedaQuery("M");

            saldoCaixaQuery.Select(saldoCaixaQuery.SaldoFechamento.Sum());
            saldoCaixaQuery.InnerJoin(contaCorrenteQuery).On(saldoCaixaQuery.IdConta == contaCorrenteQuery.IdConta);
            saldoCaixaQuery.InnerJoin(moedaQuery).On(contaCorrenteQuery.IdMoeda == moedaQuery.IdMoeda);

            saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente == this.idCliente,
                                  saldoCaixaQuery.Data == dataAux);

            saldoCaixaCollection.Load(saldoCaixaQuery);
            //

            decimal porcentagemSaldoCaixa = 0;
            if (saldoCaixaCollection.HasData) {
                if (saldoCaixaCollection[0].SaldoFechamento.HasValue) {
                    if (plFechamento != 0) {
                        porcentagemSaldoCaixa = saldoCaixaCollection[0].SaldoFechamento.Value / plFechamento;
                    }
                }
            }
            #endregion

            #region ValoresLiquidar
            LiquidacaoHistoricoCollection l = new LiquidacaoHistoricoCollection();

            l.Query.Select(l.Query.Descricao,
                           l.Query.IdConta,
                           l.Query.DataLancamento,
                           l.Query.DataVencimento,
                           l.Query.Valor.Sum())
                     .Where(l.Query.IdCliente == this.idCliente &&
                            l.Query.DataLancamento.LessThanOrEqual(dataAux) &
                            (
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Normal, (byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThan(dataAux)) |
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThanOrEqual(dataAux) &
                                    l.Query.Origem.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter))
                            ) &
                            l.Query.DataHistorico == dataAux &
                            l.Query.Valor != 0)

                     .GroupBy(l.Query.Descricao,
                              l.Query.IdConta,
                              l.Query.DataLancamento,
                              l.Query.DataVencimento);

            l.Query.Load();
            //
            l.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";
            decimal totalLiquidar = 0;
            decimal porcentagemLiquidacao = 0;
            //
            foreach (LiquidacaoHistorico liquidacao in l) {
                totalLiquidar += liquidacao.Valor.Value;
            }

            if (plFechamento != 0) {
                porcentagemLiquidacao = totalLiquidar / plFechamento;
            }

            #endregion

            if (plFechamento != 0) {
                porcentagem5 = valormercado / plFechamento;
                porcentagem5 += porcentagemSaldoCaixa;
                porcentagem5 += porcentagemLiquidacao;
            }
            #endregion

            #region 6

            //clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            //
            PosicaoFundoHistoricoCollection pf = new PosicaoFundoHistoricoCollection();
            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == this.idCliente.Value,
                                    posicaoFundoQuery.DataHistorico == dataAux);

            pf.Load(posicaoFundoQuery);

            decimal valor6 = 0.0M;
            if (pf.HasData) {
                if (pf[0].ValorBruto.HasValue) {
                    valor6 = pf[0].ValorBruto.Value;
                }
            }
            if (plFechamento != 0) {
                porcentagem6 = valor6 / plFechamento;
            }
            #endregion

            #region 7
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor7 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor7 = p.ValorMercado.Value;
            }

            if (plFechamento != 0) {
                porcentagem7 = valor7 / plFechamento;
            }
            #endregion

            #region 8
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            clienteQuery = new ClienteQuery("L");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //            
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real,
                                        papelRendaFixaQuery.Classe.NotIn((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                           (int)ClasseRendaFixa.LF,
                                                                           (int)ClasseRendaFixa.DPGE,
                                                                           (int)ClasseRendaFixa.CCB,
                                                                           (int)ClasseRendaFixa.LCA,
                                                                           (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem8 = valormercado / plFechamento;
            }
            #endregion

            #region 9
            #region Bolsa
            PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente == this.idCliente,
                                     posicaoBolsa.Query.DataHistorico == dataAux,
                                     posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsa.Query.Load();

            decimal valor = 0.0M; ;
            if (posicaoBolsa.ValorMercado.HasValue) {
                valor = posicaoBolsa.ValorMercado.Value;
            }
            #endregion

            #region BMF
            PosicaoBMFHistorico posicaoBMF = new PosicaoBMFHistorico();
            posicaoBMF.Query.Select(posicaoBMF.Query.ValorMercado.Sum());
            posicaoBMF.Query.Where(posicaoBMF.Query.IdCliente == this.idCliente,
                                   posicaoBMF.Query.DataHistorico == dataAux);
            posicaoBMF.Query.Load();

            if (posicaoBMF.ValorMercado.HasValue) {
                valor += posicaoBMF.ValorMercado.Value;
            }
            #endregion

            #region Termo
            PosicaoTermoBolsaHistorico posicaoTermoBolsa = new PosicaoTermoBolsaHistorico();

            posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.ValorMercado.Sum());
            posicaoTermoBolsa.Query.Where(posicaoTermoBolsa.Query.IdCliente == this.idCliente,
                                          posicaoTermoBolsa.Query.DataHistorico == dataAux);
            posicaoTermoBolsa.Query.Load();

            if (posicaoTermoBolsa.ValorMercado.HasValue) {
                valor += posicaoTermoBolsa.ValorMercado.Value;
            }
            #endregion

            #region Swap
            PosicaoSwapHistorico posicaoSwap = new PosicaoSwapHistorico();
            posicaoSwap.Query.Select(posicaoSwap.Query.Saldo.Sum());
            posicaoSwap.Query.Where(posicaoSwap.Query.IdCliente == this.idCliente,
                                    posicaoSwap.Query.DataHistorico == dataAux);
            posicaoSwap.Query.Load();

            if (posicaoSwap.Saldo.HasValue) {
                valor += posicaoSwap.Saldo.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem9 = valor / plFechamento;
            }
            #endregion

            #region 10

            #region Bolsa
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor10 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor10 = p.ValorMercado.Value;
            }
            #endregion

            #region RF
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);

            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            PosicaoRendaFixaHistorico prf = new PosicaoRendaFixaHistorico();
            prf.Load(posicaoRendaFixaQuery);

            if (prf.ValorMercado.HasValue) {
                valor10 += prf.ValorMercado.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem10 = valor10 / plFechamento;
            }

            #endregion

            #region 11
            porcentagem11 = 0;
            #endregion

            // Dicionario com as descrições e porcentagens de patrimônio
            Dictionary<string, decimal> porcentagens = new Dictionary<string, decimal>();
            porcentagens.Add("Títulos Públicos federais", porcentagem1);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos públicos federais", porcentagem2);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos privados", porcentagem3);
            porcentagens.Add("Acões", porcentagem4);
            porcentagens.Add("Depósito a prazo e outros títulos de instituições financeiras", porcentagem5);
            porcentagens.Add("Cotas de Fundos de Investimento 555", porcentagem6);
            porcentagens.Add("Outras Cotas de Fundos de Investimento", porcentagem7);
            porcentagens.Add("Títulos de crédito privado", porcentagem8);
            porcentagens.Add("Derivativos", porcentagem9);
            porcentagens.Add("Investimento no exterior", porcentagem10);
            porcentagens.Add("Outras Aplicações", porcentagem11);

            // pegar as posições [10-9-8-7-6] do dicionario ordenado
            Dictionary<string, decimal> porcentagensOrdenadas = this.SortDictionaryByValue(porcentagens);

            int j = -1;
            List<string> descricao = new List<string>();
            List<decimal> valores = new List<decimal>();
            foreach (KeyValuePair<string, decimal> pair in porcentagensOrdenadas) {
                j++;
                if (j <= 5) {
                    continue;
                }
                else {
                    descricao.Add(pair.Key);
                    valores.Add(pair.Value);
                }
            }
            //
            decimal v4 = valores[4] * 100;
            decimal v3 = valores[3] * 100;
            decimal v2 = valores[2] * 100;
            decimal v1 = valores[1] * 100;
            decimal v0 = valores[0] * 100;
            //
            ((XRTableCell)linha0.Cells[0]).Text = descricao[4];
            ((XRTableCell)linha0.Cells[1]).Text = v4.ToString("N2") + "% do patrimônio Líquido";

            ((XRTableCell)linha1.Cells[0]).Text = descricao[3];
            ((XRTableCell)linha1.Cells[1]).Text = v3.ToString("N2") + "% do patrimônio Líquido";

            ((XRTableCell)linha2.Cells[0]).Text = descricao[2];
            ((XRTableCell)linha2.Cells[1]).Text = v2.ToString("N2") + "% do patrimônio Líquido";

            ((XRTableCell)linha3.Cells[0]).Text = descricao[1];
            ((XRTableCell)linha3.Cells[1]).Text = v1.ToString("N2") + "% do patrimônio Líquido"; ;

            ((XRTableCell)linha4.Cells[0]).Text = descricao[0];
            ((XRTableCell)linha4.Cells[1]).Text = v0.ToString("N2") + "% do patrimônio Líquido"; ;
        }

        private void Item6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (lamina.Risco >= 1 && lamina.Risco <= 5) {
                xrPictureBox1.Visible = lamina.Risco == 1;
                xrPictureBox2.Visible = lamina.Risco == 2;
                xrPictureBox3.Visible = lamina.Risco == 3;
                xrPictureBox4.Visible = lamina.Risco == 4;
                xrPictureBox5.Visible = lamina.Risco == 5;
            }
        }

        private void Item8BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRPanel panel = sender as XRPanel;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //
            panel.Visible = maisDeUmAno;

            if (!panel.Visible) {
                this.Detail.HeightF = this.Detail.HeightF - 760.27F;
            }
        }

        private void Item9BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRPanel panel = sender as XRPanel;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //
            panel.Visible = maisDeUmAno;

            if (!panel.Visible) {
                this.Detail.HeightF = this.Detail.HeightF - 1368.95F;
            }
        }

        private void TableItem9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;

            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            //
            ((XRTableCell)linha1.Cells[1]).Text = "R$ 1331,00";
            ((XRTableCell)linha1.Cells[2]).Text = "R$ 1610,51";
            //

            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(this.data.Year, 1, 1));
            //
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataInicio, this.idCliente.Value);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            /* regra de tres
             *   1000 - PL
             *   x  - Despesas
             *      X = (Despesas X 1000) / PL
            */
            decimal despesas = this.CalculaDespesas();

            decimal x = 0M;
            if (plFechamento != 0) {
                x = (despesas * 1000) / plFechamento;
            }

            decimal tresAnos = x * 3;
            decimal cincoAnos = x * 5;

            ((XRTableCell)linha2.Cells[1]).Text = "R$ " + tresAnos.ToString("N2");
            ((XRTableCell)linha2.Cells[2]).Text = "R$ " + cincoAnos.ToString("N2");
            //
            decimal retornoBruto1 = 331M - tresAnos;
            decimal retornoBruto2 = 610.51M - cincoAnos;

            ((XRTableCell)linha3.Cells[1]).Text = "R$ " + retornoBruto1.ToString("N2");
            ((XRTableCell)linha3.Cells[2]).Text = "R$ " + retornoBruto2.ToString("N2");
        }

        private void Bloco6Risco_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);
            //
            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
            //
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(agenteAdministrador.Nome) ? agenteAdministrador.Nome.Trim() : "-");
        }

        private void Bloco5ComposicaoCarteira_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;

            HistoricoCota h1 = new HistoricoCota();
            DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(this.data);
            //
            decimal pl = 0.00M;
            try {
                h1.BuscaValorPatrimonioDia(this.idCliente.Value, dataAux);
                pl = h1.PLFechamento.Value;
            }
            catch (HistoricoCotaNaoCadastradoException e1) {
                pl = 0.00M;
            }

            valor.Rtf = valor.Rtf.Replace("[#1]", pl.ToString("N2"));
        }

        #region 7

        /// <summary>
        /// Exibido somente se FundoEstruturado= N e fundo com mais de 1 ano
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item7VisibilidadeFundoTodosBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRPanel panel = sender as XRPanel;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //
            //panel.Visible = true;
            panel.Visible = maisDeUmAno && this.lamina.FundoEstruturado.ToUpper() == "N";

            if (!panel.Visible) {
                this.Detail.HeightF = this.Detail.HeightF - 1591.31F;
            }
        }

        /// <summary>
        /// Exibido somente se FundoEstruturado e fundo com mais de 1 ano
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item7VisibilidadeFundoEstruturadoPrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRPanel panel = sender as XRPanel;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //

            //panel.Visible = true;
            panel.Visible = maisDeUmAno && this.lamina.FundoEstruturado.ToUpper() == "S";

            if (!panel.Visible) {
                this.Detail.HeightF = this.Detail.HeightF - 654.53F;
            }
        }
        
        private void Bloco7Titulo_BeforePrint(object sender, PrintEventArgs e) {
            XRRichText panel = sender as XRRichText;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDeUmAno = (resultado.Days >= 365);
            //
            //panel.Visible = true;
            panel.Visible = (maisDeUmAno && this.lamina.FundoEstruturado.ToUpper() == "S") ||
                            (maisDeUmAno && this.lamina.FundoEstruturado.ToUpper() == "N");
        }

        private void Bloco7Titulo_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            //
            string titulo = this.lamina.FundoEstruturado.ToUpper() == "S" ? "SIMULAÇÃO DE DESEMPENHO" : "HISTÓRICO DE RENTABILIDADE";

            valor.Rtf = valor.Rtf.Replace("[#1]", titulo);
        }

        [Obsolete("Não mais usado")]
        private void Item7ABeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRRichText xrRichText = sender as XRRichText;
            //xrRichText.Visible = this.lamina.FundoEstruturado.ToUpper() == "N";
        }

        [Obsolete("Não mais usado")]
        private void Bloco7BRentabilidade_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            //XRRichText valor = sender as XRRichText;
            // Nada a fazer          
        }

        /// <summary>
        /// Exibido se Fundo tiver menos de 5 anos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item7InicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText xrRichText = sender as XRRichText;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            DateTime dataInicio = carteira.DataInicioCota.Value;
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            // Subtrair maior de menor
            TimeSpan resultado = dataFim.Subtract(dataInicio);
            bool maisDe5Anos = (resultado.Days >= 365 * 5);
            //
            //xrRichText.Visible = true;
            xrRichText.Visible = !maisDe5Anos;
        }

        private void Bloco7Inicio_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            //
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            valor.Rtf = valor.Rtf.Replace("[#1]", carteira.DataInicioCota.Value.ToString("d"));
        }

        private void Bloco7ACenarios_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.CenariosApuracaoRentabilidade) ? lamina.CenariosApuracaoRentabilidade.Trim() : "");
        }

        private void Bloco7BDesempenhoFundo_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(lamina.DesempenhoFundo) ? lamina.DesempenhoFundo.Trim() : "");
        }

        private void Bloco7CRentabilidade_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            decimal p1 = 1;
            decimal p2 = 1;
            decimal p3 = 1;
            decimal p4 = 1;
            decimal p5 = 1;
            //
            decimal benchmark1 = 1;
            decimal benchmark2 = 1;
            decimal benchmark3 = 1;
            decimal benchmark4 = 1;
            decimal benchmark5 = 1;
            //
            #region ano0
            if (ano0 != null && ano0.Retorno.HasValue) {
                p1 = (ano0.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano1
            if (ano1 != null && ano1.Retorno.HasValue) {
                p2 = (ano1.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano2
            if (ano2 != null && ano2.Retorno.HasValue) {
                p3 = (ano2.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano3
            if (ano3 != null && ano3.Retorno.HasValue) {
                p4 = (ano3.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano4
            if (ano4 != null && ano4.Retorno.HasValue) {
                p5 = (ano4.Retorno.Value / 100M) + 1M;
            }
            #endregion

            decimal? fatorAcumulado = ((p1 * p2 * p3 * p4 * p5) - 1M) * 100M;

            #region benchmark0
            if (ano0 != null && ano0.RetornoBenchmark.HasValue) {
                benchmark1 = (ano0.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark1
            if (ano1 != null && ano1.RetornoBenchmark.HasValue) {
                benchmark2 = (ano1.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark2
            if (ano2 != null && ano2.RetornoBenchmark.HasValue) {
                benchmark3 = (ano2.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark3
            if (ano3 != null && ano3.RetornoBenchmark.HasValue) {
                benchmark4 = (ano3.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark4
            if (ano4 != null && ano4.RetornoBenchmark.HasValue) {
                benchmark5 = (ano4.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            decimal? indiceAcumulado = ((benchmark1 * benchmark2 * benchmark3 * benchmark4 * benchmark5) - 1M) * 100M;

            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", fatorAcumulado.HasValue ? fatorAcumulado.Value.ToString("N2") : "-");
            valor.Rtf = valor.Rtf.Replace("[#2]", descricaoIndice);
            valor.Rtf = valor.Rtf.Replace("[#3]", indiceAcumulado.HasValue ? indiceAcumulado.Value.ToString("N2") : "-");

            // TODO
            bool mesesNegativos = false;
            if (mesesNegativos) {
                valor.Rtf = valor.Rtf.Replace("[#4]", "O fundo obteve rentabilidade negativa em [x] desses anos.]");
            }
        }

        #region Rentabilidade Mensal/Anual

        #region Preenche os valores das tables
        private void Item7RentabilidadeMensal_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[2]).Text += descricaoIndice;
            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[3]).Text += descricaoIndice;
            //
            //
            CalculoMedida.EstatisticaRetornoMensal _1 = this.listaRetornosMensais.Count >= 1 ? this.listaRetornosMensais[0] : null;
            CalculoMedida.EstatisticaRetornoMensal _2 = this.listaRetornosMensais.Count >= 2 ? this.listaRetornosMensais[1] : null;
            CalculoMedida.EstatisticaRetornoMensal _3 = this.listaRetornosMensais.Count >= 3 ? this.listaRetornosMensais[2] : null;
            CalculoMedida.EstatisticaRetornoMensal _4 = this.listaRetornosMensais.Count >= 4 ? this.listaRetornosMensais[3] : null;
            CalculoMedida.EstatisticaRetornoMensal _5 = this.listaRetornosMensais.Count >= 5 ? this.listaRetornosMensais[4] : null;
            CalculoMedida.EstatisticaRetornoMensal _6 = this.listaRetornosMensais.Count >= 6 ? this.listaRetornosMensais[5] : null;
            CalculoMedida.EstatisticaRetornoMensal _7 = this.listaRetornosMensais.Count >= 7 ? this.listaRetornosMensais[6] : null;
            CalculoMedida.EstatisticaRetornoMensal _8 = this.listaRetornosMensais.Count >= 8 ? this.listaRetornosMensais[7] : null;
            CalculoMedida.EstatisticaRetornoMensal _9 = this.listaRetornosMensais.Count >= 9 ? this.listaRetornosMensais[8] : null;
            CalculoMedida.EstatisticaRetornoMensal _10 = this.listaRetornosMensais.Count >= 10 ? this.listaRetornosMensais[9] : null;
            CalculoMedida.EstatisticaRetornoMensal _11 = this.listaRetornosMensais.Count >= 11 ? this.listaRetornosMensais[10] : null;
            CalculoMedida.EstatisticaRetornoMensal _12 = this.listaRetornosMensais.Count >= 12 ? this.listaRetornosMensais[11] : null;
            //
            decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);

            #region Preenche com -
            for (int k = 1; k <= 12; k++) {
                for (int p = 1; p <= 3; p++) {
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                }
            }
            #endregion

            #region mes1
            if (_1 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[1]), _1.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), _1.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[3]), _1.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes2
            if (_2 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[1]), _2.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), _2.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[3]), _2.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes3
            if (_3 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[1]), _3.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[2]), _3.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[3]), _3.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes4
            if (_4 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[1]), _4.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[2]), _4.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[3]), _4.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes5
            if (_5 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), _5.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[2]), _5.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[3]), _5.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes6
            if (_6 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[1]), _6.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[2]), _6.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[3]), _6.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes7
            if (_7 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[1]), _7.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[2]), _7.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[3]), _7.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes8
            if (_8 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[1]), _8.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[2]), _8.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[3]), _8.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes9
            if (_9 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[1]), _9.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[2]), _9.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[3]), _9.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes10
            if (_10 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[1]), _10.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[2]), _10.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[3]), _10.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes11
            if (_11 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[1]), _11.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[2]), _11.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[3]), _11.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region mes12
            if (_12 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[1]), _12.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[2]), _12.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[3]), _12.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion

            decimal? rentabilidade12MesesDiferencial = null;
            if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue) {
                if (rentabilidade12MesesIndice != 0) {
                    rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
                }
            }

            /* Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial */
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

            #endregion
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12Meses(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.Retorno.HasValue) {
                    rentabilidadesM.Add(c.Retorno.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnual = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnual = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnual *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnual - 1) * 100;
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses do Indice baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12MesesIndice(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais do Benchamark não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.RetornoBenchmark.HasValue) {
                    rentabilidadesM.Add(c.RetornoBenchmark.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnualIndice = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnualIndice = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnualIndice *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnualIndice - 1) * 100;
        }

        /// <summary>
        /// Tabela rentabilidade Anual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item7RentabilidadeAnual_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[2]).Text += descricaoIndice;
            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[3]).Text += descricaoIndice;
            //
            // Informações Gerais
            ((XRTableCell)((XRTableRow)table.Rows[1]).Cells[0]).Text = this.ultimos_5_Anos[4].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[2]).Cells[0]).Text = this.ultimos_5_Anos[3].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[3]).Cells[0]).Text = this.ultimos_5_Anos[2].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[4]).Cells[0]).Text = this.ultimos_5_Anos[1].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[5]).Cells[0]).Text = this.ultimos_5_Anos[0].ToString();
            //

            #region Preenche com -
            for (int k = 1; k <= 5; k++) {
                for (int p = 1; p <= 3; p++) {
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                }
            }
            #endregion

            //
            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            #region Ano0
            if (ano0 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[1]), ano0.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), ano0.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[3]), ano0.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano1
            if (ano1 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[1]), ano1.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), ano1.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[3]), ano1.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano2
            if (ano2 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[1]), ano2.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[2]), ano2.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[3]), ano2.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano3
            if (ano3 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[1]), ano3.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[2]), ano3.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[3]), ano3.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano4
            if (ano4 != null) {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), ano4.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[2]), ano4.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[3]), ano4.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #endregion
        }

        #endregion
        #endregion

        #endregion

        private void Rodape_PrintOnPage(object sender, PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;

            //#region Dados Cliente

            //PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");

            //p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro, p.Cidade, p.Cep, p.Uf, p.IdPessoa);
            //p.Where(p.RecebeCorrespondencia == "S" & p.IdPessoa == this.idCliente.Value);

            //PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            //pessoaEnderecoCollection.Load(p);

            //string enderecoCompletoCliente = "";
            ////
            //if (pessoaEnderecoCollection.HasData) {
            //    #region EnderecoCompleto
            //    string enderecoCliente = pessoaEnderecoCollection[0].str.Endereco.Trim();

            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim())) {
            //        enderecoCliente += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + "  - " + pessoaEnderecoCollection[0].str.Complemento.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim())) {
            //        enderecoCliente += " " + pessoaEnderecoCollection[0].str.Bairro.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim())) {
            //        enderecoCliente += " " + Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim())) {
            //        enderecoCliente += " " + pessoaEnderecoCollection[0].str.Cidade.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
            //        enderecoCliente += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
            //    }

            //    Pessoa pes = new Pessoa();
            //    pes.LoadByPrimaryKey(pessoaEnderecoCollection[0].IdPessoa.Value);

            //    if (!String.IsNullOrEmpty(pes.DDD_Fone)) {
            //        enderecoCliente += " - " + pes.DDD_Fone;
            //    }

            //    enderecoCompletoCliente = enderecoCliente;
            //    #endregion
            //}
            //#endregion

            Carteira b = new Carteira();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(b.Query.IdCarteira);
            campos.Add(b.Query.Nome);
            b.LoadByPrimaryKey(campos, this.idCliente.Value);
            //

            #region CNPJ
            string cpfcnpj = "";
            //
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.idCliente.Value);

            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }
            #endregion

            string texto = b.Nome.Trim();
            if (!String.IsNullOrEmpty(cpfcnpj)) {
                texto += " - " + cpfcnpj.Trim();
            }

            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(texto) ? texto.Trim() : "-");
        }

        #endregion

        private void DetailBeforePrint(object sender, PrintEventArgs e) {
            DetailBand detail = sender as DetailBand;
            //detail.HeightF = 7000F;
        }

        private string RetornaNomeMesSomado(DateTime data, int mesesSoma)
        {
            if (mesesSoma > 12 || mesesSoma < -12)
                return "";

            int mesAux = data.Month + mesesSoma;

            if (mesAux > 12)
            {
                mesAux = mesAux - 12;
            }
            else
            {
                if (mesAux < 1)
                {
                    mesAux = mesAux + 12;
                }
            }

            return Utilitario.RetornaMesString(mesAux) ;
        }
    }
}