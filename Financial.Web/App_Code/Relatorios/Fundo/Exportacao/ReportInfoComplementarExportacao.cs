﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using EntitySpaces.Core;
using System.Text;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.Swap;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportInfoComplementarExportacao
    /// </summary>
    public class ReportInfoComplementarExportacao : DevExpress.XtraReports.UI.XtraReport {
        //
        private int? idCliente;

        public int? IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private Cliente cliente;

        private InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();

        private int numeroLinhasDataTable;

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private PageHeaderBand PageHeader;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private ReportFooterBand ReportFooter;
        private XRRichText xrRichText2;
        private DevExpress.XtraReports.Parameters.Parameter parameter1;
        private XRRichText xrRichText1;
        private XRLabel xrLabel9;
        private XRRichText xrRichText4;
        private XRRichText xrRichText3;
        private XRRichText xrRichText5;
        private XRRichText xrRichText6;
        private XRRichText xrRichText7;
        private XRRichText xrRichText8;
        private XRTable xrTable2;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell6;
        private XRRichText xrRichText10;
        private XRRichText xrRichText12;
        private XRRichText xrRichText11;
        private XRRichText xrRichText13;
        private XRRichText xrRichText15;
        private XRRichText xrRichText26;
        private XRRichText xrRichText32;
        private XRRichText xrRichText31;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ReportInfoComplementarExportacaoInternal() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            //this.SetRelatorioSemDados();            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }
        #endregion

        #region Construtores Publics
        public ReportInfoComplementarExportacao() {
            this.ReportInfoComplementarExportacaoInternal();
        }

        public ReportInfoComplementarExportacao(int idCliente, DateTime data) {
            this.data = data;
            this.idCliente = idCliente;
            this.ReportInfoComplementarExportacaoInternal();
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                //this.PageHeader.Visible = true;
                //this.xrSubreport1.Visible = true;
            }
        }

        private void PersonalInitialize() {
            // Carrega a Cliente
            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            //
            this.cliente.LoadByPrimaryKey(campos, this.IdCliente.Value);

            //         
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCliente.Value);
            //
            this.parameter1.Value = "";
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                this.parameter1.Value = pessoa.str.Cpfcnpj;
            }

            #region InformacoesComplementaresFundo
            InformacoesComplementaresFundoCollection infoCollection = new InformacoesComplementaresFundoCollection();

            // Busca todas as informações menores que a data de referencia ordenado decrescente pela data
            infoCollection.Query
                 .Where(infoCollection.Query.DataInicioVigencia.LessThanOrEqual(this.data),
                        infoCollection.Query.IdCarteira == this.idCliente)
                 .OrderBy(infoCollection.Query.DataInicioVigencia.Descending);

            infoCollection.Query.Load();

            // Pega a Primeira se existir Info Complementar
            bool existeInfo = infoCollection.HasData;
            if (existeInfo) {
                this.info = infoCollection[0];
            }
            #endregion
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportListaEtiqueta.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportInfoComplementarExportacao.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportInfoComplementarExportacao.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrRichText32 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText31 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText26 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText15 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText13 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText11 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText12 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText10 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText8 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText7 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText6 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText5 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText3 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText4 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter1 = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.MultiColumn.ColumnSpacing = 51F;
            this.Detail.MultiColumn.ColumnWidth = 1016F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdPessoa")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Endereco")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(42F, 85F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Numero")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(339F, 85F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Complemento")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(614F, 85F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Bairro")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(42F, 169F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cidade")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(42F, 254F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Uf")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel7.Text = "xrLabel7";
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 2.243655F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Visible = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 211F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 5F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 150.282F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(80.86314F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1741.677F, 135.25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.Visible = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.425531914893617;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Sede: Rua Sete de Setembro, 111/2-5º e 23-34º Andares – Centro – Rio de Janeiro -" +
                " RJ – CEP: 20050-901 – Brasil – Tel.: (21) 3554-8686 – http://www.cvm.gov.br.";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 1;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText32,
            this.xrRichText31,
            this.xrRichText26,
            this.xrRichText15,
            this.xrRichText13,
            this.xrRichText11,
            this.xrRichText12,
            this.xrRichText10,
            this.xrRichText8,
            this.xrRichText7,
            this.xrRichText6,
            this.xrRichText5,
            this.xrRichText3,
            this.xrRichText4,
            this.xrLabel9,
            this.xrRichText1,
            this.xrRichText2});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.ReportHeader.HeightF = 3360.944F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseFont = false;
            // 
            // xrRichText32
            // 
            this.xrRichText32.CanShrink = true;
            this.xrRichText32.CanGrow = true;
            this.xrRichText32.Dpi = 254F;
            this.xrRichText32.LocationFloat = new DevExpress.Utils.PointFloat(2.235491F, 2898.6F);
            this.xrRichText32.Name = "xrRichText32";
            this.xrRichText32.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText32.SerializableRtfString = resources.GetString("xrRichText32.SerializableRtfString");
            this.xrRichText32.SizeF = new System.Drawing.SizeF(1916.751F, 190F);
            this.xrRichText32.StylePriority.UsePadding = false;
            this.xrRichText32.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item11_PrintOnPage);
            // 
            // xrRichText31
            // 
            this.xrRichText31.CanShrink = true;
            this.xrRichText31.CanGrow = true;
            this.xrRichText31.Dpi = 254F;
            this.xrRichText31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2687.075F);
            this.xrRichText31.Name = "xrRichText31";
            this.xrRichText31.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText31.SerializableRtfString = resources.GetString("xrRichText31.SerializableRtfString");
            this.xrRichText31.SizeF = new System.Drawing.SizeF(1924.044F, 190F);
            this.xrRichText31.StylePriority.UsePadding = false;
            this.xrRichText31.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item10_PrintOnPage);
            // 
            // xrRichText26
            // 
            this.xrRichText26.CanShrink = true;
            this.xrRichText12.CanGrow = true;
            this.xrRichText26.Dpi = 254F;
            this.xrRichText26.LocationFloat = new DevExpress.Utils.PointFloat(2.235452F, 3123.163F);
            this.xrRichText26.Name = "xrRichText26";
            this.xrRichText26.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText26.SerializableRtfString = resources.GetString("xrRichText26.SerializableRtfString");
            this.xrRichText26.SizeF = new System.Drawing.SizeF(1916.751F, 189.9998F);
            this.xrRichText26.StylePriority.UsePadding = false;          
            this.xrRichText26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item12_PrintOnPage);
            // 
            // xrRichText15
            // 
            this.xrRichText15.CanShrink = true;
            this.xrRichText15.CanGrow = true;
            this.xrRichText15.Dpi = 254F;
            this.xrRichText15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2451.036F);
            this.xrRichText15.Name = "xrRichText15";
            this.xrRichText15.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText15.SerializableRtfString = resources.GetString("xrRichText15.SerializableRtfString");
            this.xrRichText15.SizeF = new System.Drawing.SizeF(1901.737F, 190F);
            this.xrRichText15.StylePriority.UsePadding = false;
            this.xrRichText15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item9_PrintOnPage);
            // 
            // xrRichText13
            // 
            this.xrRichText13.CanShrink = true;
            this.xrRichText13.CanGrow = true;
            this.xrRichText13.Dpi = 254F;
            this.xrRichText13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2220.503F);
            this.xrRichText13.Name = "xrRichText13";
            this.xrRichText13.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText13.SerializableRtfString = resources.GetString("xrRichText13.SerializableRtfString");
            this.xrRichText13.SizeF = new System.Drawing.SizeF(1906.795F, 190F);
            this.xrRichText13.StylePriority.UsePadding = false;          
            this.xrRichText13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item8_PrintOnPage);
            // 
            // xrRichText11
            // 
            this.xrRichText11.CanShrink = true;
            this.xrRichText11.CanGrow = true;
            this.xrRichText11.Dpi = 254F;
            this.xrRichText11.LocationFloat = new DevExpress.Utils.PointFloat(2.235491F, 1997.66F);
            this.xrRichText11.Name = "xrRichText11";
            this.xrRichText11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText11.SerializableRtfString = resources.GetString("xrRichText11.SerializableRtfString");
            this.xrRichText11.SizeF = new System.Drawing.SizeF(1906.795F, 190F);
            this.xrRichText11.StylePriority.UseBorders = false;
            this.xrRichText11.StylePriority.UsePadding = false;
            this.xrRichText11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item7_PrintOnPage);
            // 
            // xrRichText12
            // 
            this.xrRichText12.CanShrink = true;
            this.xrRichText12.CanGrow = true;
            this.xrRichText12.Dpi = 254F;
            this.xrRichText12.LocationFloat = new DevExpress.Utils.PointFloat(12.19143F, 1769.496F);
            this.xrRichText12.Name = "xrRichText12";
            this.xrRichText12.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText12.SerializableRtfString = resources.GetString("xrRichText12.SerializableRtfString");
            this.xrRichText12.SizeF = new System.Drawing.SizeF(1911.853F, 190F);
            this.xrRichText12.StylePriority.UsePadding = false;
            this.xrRichText12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item6_PrintOnPage);
            // 
            // xrRichText10
            // 
            this.xrRichText10.CanShrink = true;
            this.xrRichText10.CanGrow = true;
            this.xrRichText10.Dpi = 254F;
            this.xrRichText10.LocationFloat = new DevExpress.Utils.PointFloat(18.56022F, 1553.822F);
            this.xrRichText10.Name = "xrRichText10";
            this.xrRichText10.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText10.SerializableRtfString = resources.GetString("xrRichText10.SerializableRtfString");
            this.xrRichText10.SizeF = new System.Drawing.SizeF(1916.751F, 190F);
            this.xrRichText10.StylePriority.UsePadding = false;
            this.xrRichText10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item5_PrintOnPage);
            // 
            // xrRichText8
            // 
            this.xrRichText8.CanShrink = true;
            this.xrRichText8.CanGrow = true;
            this.xrRichText8.Dpi = 254F;
            this.xrRichText8.LocationFloat = new DevExpress.Utils.PointFloat(18.56022F, 1110F);
            this.xrRichText8.Name = "xrRichText8";
            this.xrRichText8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText8.SerializableRtfString = resources.GetString("xrRichText8.SerializableRtfString");
            this.xrRichText8.SizeF = new System.Drawing.SizeF(1911.693F, 190F);
            this.xrRichText8.StylePriority.UsePadding = false;
            this.xrRichText8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item3_PrintOnPage);
            // 
            // xrRichText7
            // 
            this.xrRichText7.CanShrink = true;
            this.xrRichText7.CanGrow = true;
            this.xrRichText7.Dpi = 254F;
            this.xrRichText7.LocationFloat = new DevExpress.Utils.PointFloat(17.24934F, 1332.984F);
            this.xrRichText7.Name = "xrRichText7";
            this.xrRichText7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText7.SerializableRtfString = resources.GetString("xrRichText7.SerializableRtfString");
            this.xrRichText7.SizeF = new System.Drawing.SizeF(1906.795F, 190F);
            this.xrRichText7.StylePriority.UsePadding = false;
            this.xrRichText7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item4_PrintOnPage);
            // 
            // xrRichText6
            // 
            this.xrRichText6.CanShrink = true;
            this.xrRichText6.CanGrow = true;
            this.xrRichText6.Dpi = 254F;
            this.xrRichText6.LocationFloat = new DevExpress.Utils.PointFloat(17.24936F, 885F);
            this.xrRichText6.Name = "xrRichText6";
            this.xrRichText6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText6.SerializableRtfString = resources.GetString("xrRichText6.SerializableRtfString");
            this.xrRichText6.SizeF = new System.Drawing.SizeF(1911.693F, 190.1207F);
            this.xrRichText6.StylePriority.UsePadding = false;
            this.xrRichText6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item2_PrintOnPage);
            // 
            // xrRichText5
            // 
            this.xrRichText5.CanShrink = true;
            this.xrRichText5.CanGrow = true;
            this.xrRichText5.Dpi = 254F;
            this.xrRichText5.LocationFloat = new DevExpress.Utils.PointFloat(17.24936F, 733.3959F);
            this.xrRichText5.Name = "xrRichText5";
            this.xrRichText5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText5.SerializableRtfString = resources.GetString("xrRichText5.SerializableRtfString");
            this.xrRichText5.SizeF = new System.Drawing.SizeF(1906.795F, 112.4713F);
            this.xrRichText5.StylePriority.UsePadding = false;
            this.xrRichText5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item1_BeforePrint);
            // 
            // xrRichText3
            // 
            this.xrRichText3.CanShrink = true;
            this.xrRichText3.Dpi = 254F;
            this.xrRichText3.LocationFloat = new DevExpress.Utils.PointFloat(157.9618F, 612.2017F);
            this.xrRichText3.Name = "xrRichText3";
            this.xrRichText3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText3.SerializableRtfString = resources.GetString("xrRichText3.SerializableRtfString");
            this.xrRichText3.SizeF = new System.Drawing.SizeF(1584.977F, 68.16766F);
            this.xrRichText3.StylePriority.UsePadding = false;
            // 
            // xrRichText4
            // 
            this.xrRichText4.CanShrink = true;
            this.xrRichText4.Dpi = 254F;
            this.xrRichText4.LocationFloat = new DevExpress.Utils.PointFloat(54.40147F, 269.9935F);
            this.xrRichText4.Name = "xrRichText4";
            this.xrRichText4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText4.SerializableRtfString = resources.GetString("xrRichText4.SerializableRtfString");
            this.xrRichText4.SizeF = new System.Drawing.SizeF(1890.866F, 252.5031F);
            this.xrRichText4.StylePriority.UsePadding = false;
            this.xrRichText4.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco3_PrintOnPage);
            // 
            // xrLabel9
            // 
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter1, "Text", "")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(766.5837F, 60.69596F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(363.7325F, 61.742F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // parameter1
            // 
            this.parameter1.Name = "parameter1";
            // 
            // xrRichText1
            // 
            this.xrRichText1.CanShrink = true;
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(54.40147F, 122.438F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1890.866F, 71.17589F);
            this.xrRichText1.StylePriority.UsePadding = false;
            this.xrRichText1.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco2_PrintOnPage);
            // 
            // xrRichText2
            // 
            this.xrRichText2.CanShrink = true;
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(54.40147F, 0F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1890.865F, 60.69599F);
            this.xrRichText2.StylePriority.UsePadding = false;
            this.xrRichText2.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco1_PrintOnPage);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 2.781204F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // ReportInfoComplementarExportacao
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.ReportHeader,
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.PageFooter,
            this.ReportFooter});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 211, 5);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter1});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region Funções Personalizadas
        
        private void Bloco1_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;            
            valor.Rtf = valor.Rtf.Replace("[#1]", this.cliente.Apelido);
        }

        private void Bloco2_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            string mes = Utilitario.RetornaMesString(data.Month);
            string dataString = this.data.Day.ToString().PadLeft(2, '0') + " de " + mes + " de " + this.data.Year.ToString();
            valor.Rtf = valor.Rtf.Replace("[#1]", dataString);            
        }

        private void Bloco3_PrintOnPage(object sender, PrintOnPageEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", cliente.Nome);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);
            //
            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
            //
            AgenteMercado agenteGestor = new AgenteMercado();
            agenteGestor.LoadByPrimaryKey(carteira.IdAgenteGestor.Value);

            valor.Rtf = valor.Rtf.Replace("[#2]", !String.IsNullOrEmpty(agenteAdministrador.Nome) ? agenteAdministrador.Nome.Trim() : "-");
            valor.Rtf = valor.Rtf.Replace("[#3]", !String.IsNullOrEmpty(agenteGestor.Nome) ? agenteGestor.Nome.Trim() : "-");
        }

        #region Itens 1 a 12
        private void Item1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.Periodicidade) ? info.Periodicidade.Trim() : "");
        }

        private void Item2_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "");
        }

        private void Item3_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.LocalFormaSolicitacaoCotista) ? info.LocalFormaSolicitacaoCotista.Trim() : "");
        }

        private void Item4_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.FatoresRisco) ? info.FatoresRisco.Trim() : "");
        }

        private void Item5_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.PoliticaExercicioVoto) ? info.PoliticaExercicioVoto.Trim() : "");
        }

        private void Item6_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.TributacaoAplicavel) ? info.TributacaoAplicavel.Trim() : "");
        }

        private void Item7_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.PoliticaAdministracaoRisco) ? info.PoliticaAdministracaoRisco.Trim() : "");
        }

        private void Item8_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.AgenciaClassificacaoRisco) ? info.AgenciaClassificacaoRisco.Trim() : "");
        }

        private void Item9_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.RecursosServicosGestor) ? info.RecursosServicosGestor.Trim() : "");
        }

        private void Item10_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.PrestadoresServicos) ? info.PrestadoresServicos.Trim() : "");
        }

        private void Item11_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.PoliticaDistribuicaoCotas) ? info.PoliticaDistribuicaoCotas.Trim() : "");
        }

        private void Item12_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(info.Observacoes) ? info.Observacoes.Trim() : "");
        }
        #endregion

        #endregion
    }
}