﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Collections.Generic;

using DevExpress.XtraReports.UI;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;

using log4net;

using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Util;

using Financial.Fundo.Exceptions;
using Financial.Security;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SaldoAplicacaoFundo
    /// </summary>
    public class ReportSaldoAplicacaoFundoView2 : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportSaldoAplicacaoFundoView2));

        #region Enum
        public enum TipoRelatorio {
            Analitico = 1,
            Consolidado = 2
        }

        public enum TipoGrupamento {
            PorNome = 1,
            PorCodigo = 2
        }

        public enum OpcaoValorAplicado {
            ValorAtualizado = 1,
            ValorOriginal = 2
        }

        public enum TipoLingua {
            [StringValue("pt-BR")]
            Portugues = 1,

            [StringValue("en-US")]
            Ingles = 2,
        }
        #endregion

        private TipoRelatorio tipoRelatorio;
        private TipoGrupamento tipoGrupamento;
        private OpcaoValorAplicado opcaoValorAplicado;
        //
        private int? idCarteira;
        private int idCliente;

        private bool exibeRentabilidade = false;

        private string login;

        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private TipoLingua tipoLingua;

        public TipoLingua Lingua {
            get { return tipoLingua; }
        }

        private int numeroLinhasDataTable;

        // Define de consulta será histórica ou não
        private enum TipoPesquisa {
            PosicaoFundo = 0,
            PosicaoFundoHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoFundo;

        /// <summary>
        /// Retorna Visibilidade do Relatório
        /// </summary>
        public bool TemDados {
            get { return !this.xrSubreport3.Visible; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRPageInfo xrPageInfo2;
        private XRPageInfo xrPageInfo3;
        private XRPanel xrPanel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell17;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCellValorCotaAplicacao;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCellDataAplicacao;
        private XRTableCell xrTableCell34;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTable xrTable8;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCellCotistaGroupFooter;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTable xrTable9;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private GroupHeaderBand GroupHeader2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell73;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private PosicaoFundoCollection posicaoFundoCollection1;
        private PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection1;
        private CalculatedField calculatedFieldPercentualRetorno;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>
        /// <param name="exibeRentabilidade">Coloca Rentabilidade Mensal/Ano ou Não</param>
        /// <param name="login"></param>
        public ReportSaldoAplicacaoFundoView2(int? idCarteira, int idCliente, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, bool exibeRentabilidade, string login,
                                                TipoLingua lingua) {

            this.SaldoAplicacaoFundoView(idCarteira, idCliente, data, tipoRelatorio,
                                   tipoGrupamento, opcaoValorAplicado, exibeRentabilidade, login, lingua);

        }

        /// <summary>
        /// Assume que rentabilidade Mes/Anual não aparece.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>        
        /// <param name="login"></param>
        public ReportSaldoAplicacaoFundoView2(int? idCarteira, int idCliente, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, string login,
                                                TipoLingua lingua) {

            this.SaldoAplicacaoFundoView(idCarteira, idCliente, data, tipoRelatorio,
                                    tipoGrupamento, opcaoValorAplicado, false, login, lingua);
        }

        /// <summary>
        /// Construtor private
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoRelatorio"></param>
        /// <param name="tipoGrupamento"></param>
        /// <param name="opcaoValorAplicado"></param>
        /// <param name="exibeRentabilidade"></param>
        /// <param name="login"></param>
        private void SaldoAplicacaoFundoView(int? idCarteira, int idCliente, DateTime data,
                                                TipoRelatorio tipoRelatorio, TipoGrupamento tipoGrupamento,
                                                OpcaoValorAplicado opcaoValorAplicado, bool exibeRentabilidade, string login,
                                                TipoLingua lingua) {

            this.idCarteira = idCarteira;
            this.idCliente = idCliente;
            this.data = data;
            this.login = login;
            this.tipoRelatorio = tipoRelatorio;
            this.tipoGrupamento = tipoGrupamento;
            this.opcaoValorAplicado = opcaoValorAplicado;
            this.tipoLingua = lingua;
            //
            this.exibeRentabilidade = exibeRentabilidade;
            //
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);

            if (data >= cliente.DataDia.Value) {
                this.tipoPesquisa = TipoPesquisa.PosicaoFundo;
            }
            else {
                this.tipoPesquisa = TipoPesquisa.PosicaoFundoHistorico;
            }

            this.InitializeComponent();
            this.PersonalInitialize();
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2396);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                
                this.xrSubreport3.Visible = true;
                //
                this.Detail.Visible = false;
                this.GroupHeader1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.GroupFooter1.Visible = false;
                this.ReportFooter.Visible = false;
                this.xrTable6.Visible = false;
                this.xrTable7.Visible = false;

                // Caso onde Report não tem Dados
                this.xrTable5.Visible = false;
            }
        }

        private void PersonalInitialize() {
            string linguaSelecionada = StringEnum.GetStringValue((TipoLingua)this.tipoLingua);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);

            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.ReportSaldoAplicacaoFundoView2._TituloRelatorio;
            this.xrTableCell1.Text = Resources.ReportSaldoAplicacaoFundoView2._DataEmissao;
            this.xrTableCell4.Text = Resources.ReportSaldoAplicacaoFundoView2._DataPosicao;
            this.xrTableCell7.Text = Resources.ReportSaldoAplicacaoFundoView2._Cliente;
            this.xrTableCellDataAplicacao.Text = Resources.ReportSaldoAplicacaoFundoView2._DataAplicacao;
            this.xrTableCellValorCotaAplicacao.Text = Resources.ReportSaldoAplicacaoFundoView2._ValorCotaAplicacao;
            this.xrTableCell26.Text = Resources.ReportSaldoAplicacaoFundoView2._ValorAplicado;
            this.xrTableCell27.Text = Resources.ReportSaldoAplicacaoFundoView2._QtdCotas;
            this.xrTableCell34.Text = Resources.ReportSaldoAplicacaoFundoView2._SaldoBruto;
            this.xrTableCell28.Text = Resources.ReportSaldoAplicacaoFundoView2._ValorIR;
            this.xrTableCell29.Text = Resources.ReportSaldoAplicacaoFundoView2._ValorIOF;
            this.xrTableCell30.Text = Resources.ReportSaldoAplicacaoFundoView2._SaldoLiquido;
            this.xrTableCell31.Text = Resources.ReportSaldoAplicacaoFundoView2._Rendimento;
            this.xrTableCell32.Text = this.tipoRelatorio == TipoRelatorio.Analitico ? Resources.ReportSaldoAplicacaoFundoView2._Retorno : "";
            #endregion
        }

        private DataView FillDados() {          
            DataView dt = new DataView();
            #region SQL

            if (this.tipoPesquisa == TipoPesquisa.PosicaoFundo) {
                #region Consulta PosicaoFundo
                PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Pe");
                CarteiraQuery carteiraQuery = new CarteiraQuery("CR");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");
                //                              
                posicaoFundoQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome.As("NomeCarteira"),
                                           clienteQuery.IdCliente, clienteQuery.Nome.As("NomeCliente"),
                                           posicaoFundoQuery.CotaDia, posicaoFundoQuery.DataConversao,
                                           posicaoFundoQuery.CotaAplicacao, posicaoFundoQuery.Quantidade,
                                           posicaoFundoQuery.ValorBruto, posicaoFundoQuery.ValorIR,
                                           posicaoFundoQuery.ValorIOF, posicaoFundoQuery.ValorPerformance,
                                           posicaoFundoQuery.ValorLiquido,
                                           ((posicaoFundoQuery.CotaDia - posicaoFundoQuery.CotaAplicacao) * posicaoFundoQuery.Quantidade).As("ValorRendimento"),
                                           ((posicaoFundoQuery.CotaDia / posicaoFundoQuery.CotaAplicacao) - 1).As("PercentualRetorno")
                                        );

                if (this.opcaoValorAplicado == OpcaoValorAplicado.ValorAtualizado) {
                    posicaoFundoQuery.Select((posicaoFundoQuery.Quantidade * posicaoFundoQuery.CotaAplicacao).As("ValorAplicacao"));
                }
                else {
                    posicaoFundoQuery.Select(posicaoFundoQuery.ValorAplicacao);
                }
                //                
                posicaoFundoQuery.InnerJoin(clienteQuery).On(posicaoFundoQuery.IdCliente == clienteQuery.IdCliente);
                posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
                //
                posicaoFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
                posicaoFundoQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);

                posicaoFundoQuery.Where(posicaoFundoQuery.Quantidade != 0 &&
                                      posicaoFundoQuery.IdCliente == this.idCliente &&
                                      usuarioQuery.Login == this.login);

                if (this.idCarteira.HasValue) {
                    posicaoFundoQuery.Where(posicaoFundoQuery.IdCarteira == this.idCarteira.Value);
                }

                //
                this.posicaoFundoCollection1.Load(posicaoFundoQuery);

                if (log.IsInfoEnabled) {
                    log.Info(this.posicaoFundoCollection1.Query.es.LastQuery);
                }

                dt = this.posicaoFundoCollection1.LowLevelBind();

                #endregion
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoFundoHistorico) {
                #region Consulta PosicaoFundoHistorico
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Pe");
                CarteiraQuery carteiraQuery = new CarteiraQuery("CR");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");
                //                              
                posicaoFundoHistoricoQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome.As("NomeCarteira"),
                                           clienteQuery.IdCliente, clienteQuery.Nome.As("NomeCliente"),
                                           posicaoFundoHistoricoQuery.CotaDia, posicaoFundoHistoricoQuery.DataConversao,
                                           posicaoFundoHistoricoQuery.CotaAplicacao, posicaoFundoHistoricoQuery.Quantidade,
                                           posicaoFundoHistoricoQuery.ValorBruto, posicaoFundoHistoricoQuery.ValorIR,
                                           posicaoFundoHistoricoQuery.ValorIOF, posicaoFundoHistoricoQuery.ValorPerformance,
                                           posicaoFundoHistoricoQuery.ValorLiquido,
                                           ((posicaoFundoHistoricoQuery.CotaDia - posicaoFundoHistoricoQuery.CotaAplicacao) * posicaoFundoHistoricoQuery.Quantidade).As("Rendimento"),
                                           ((posicaoFundoHistoricoQuery.CotaDia / posicaoFundoHistoricoQuery.CotaAplicacao) - 1).As("PercentualRetorno")
                                        );

                if (this.opcaoValorAplicado == OpcaoValorAplicado.ValorAtualizado) {
                    posicaoFundoHistoricoQuery.Select((posicaoFundoHistoricoQuery.Quantidade * posicaoFundoHistoricoQuery.CotaAplicacao).As("ValorAplicacao"));
                }
                else {
                    posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorAplicacao);
                }
                //                
                posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoFundoHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);
                //
                posicaoFundoHistoricoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
                posicaoFundoHistoricoQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);

                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.Quantidade != 0 &&
                                      posicaoFundoHistoricoQuery.IdCliente == this.idCliente &&
                                      posicaoFundoHistoricoQuery.DataHistorico == this.data &&
                                      usuarioQuery.Login == this.login);

                if (this.idCarteira.HasValue) {
                    posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCarteira == this.idCarteira.Value);
                }
                //
                this.posicaoFundoHistoricoCollection1.Load(posicaoFundoHistoricoQuery);

                if (log.IsInfoEnabled) {
                    log.Info(this.posicaoFundoHistoricoCollection1.Query.es.LastQuery);
                }

                dt = this.posicaoFundoHistoricoCollection1.LowLevelBind();

                #endregion
            }
            #endregion
           
            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportSaldoAplicacaoFundoView2.resx"; 
        */
        private void InitializeComponent() {
            string resourceFileName = "ReportSaldoAplicacaoFundoView2.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellDataAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValorCotaAplicacao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellCotistaGroupFooter = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.posicaoFundoCollection1 = new Financial.Fundo.PosicaoFundoCollection();
            this.posicaoFundoHistoricoCollection1 = new Financial.Fundo.PosicaoFundoHistoricoCollection();
            this.calculatedFieldPercentualRetorno = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 53F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell33,
            this.xrTableCell38});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataConversao", "{0:d}")});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.081803005008347252;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CotaAplicacao", "{0:n8}")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "$ValorCotaAplicacao";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.16110183639398998;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorAplicacao", "{0:n2}")});
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "$ValorAplicado";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.088063439065108509;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade", "{0:n8}")});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "$QtdCotas";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.11227045075125208;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto", "{0:n2}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.Text = "$SaldoBruto";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.10434056761268781;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR", "{0:n2}")});
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.Text = "$ValorIR";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.080133555926544239;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF", "{0:n2}")});
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.Text = "$ValorIOF";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.07178631051752922;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorLiquido", "{0:n2}")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.Text = "$SaldoLiquido";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.10475792988313856;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorRendimento")});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.11727879799666111;
            this.xrTableCell33.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldPercentualRetorno")});
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.078464106844741241;
            this.xrTableCell38.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrTable10,
            this.xrPageInfo2,
            this.xrPanel1,
            this.xrTable7});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 384F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(16F, 318F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(64F, 64F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1842F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell25.Weight = 0.12649294245385451;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.87350705754614555;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2392F, 265F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(84F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 101F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(2286F, 211F);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(275F, 3F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(249F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 103F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1593F, 42F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#Cliente";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.17263025737602009;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "$Cliente";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.82736974262397989;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeClienteBeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(593F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#DataPosicao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.463743676222597;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataPosicao";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.53625632377740307;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(275F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#DataEmissao";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 331F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellDataAplicacao,
            this.xrTableCellValorCotaAplicacao,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell34,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCellDataAplicacao
            // 
            this.xrTableCellDataAplicacao.Dpi = 254F;
            this.xrTableCellDataAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataAplicacao.Name = "xrTableCellDataAplicacao";
            this.xrTableCellDataAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataAplicacao.Text = "#DataAplicaçao ";
            this.xrTableCellDataAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellDataAplicacao.Weight = 0.080968280467445738;
            // 
            // xrTableCellValorCotaAplicacao
            // 
            this.xrTableCellValorCotaAplicacao.Dpi = 254F;
            this.xrTableCellValorCotaAplicacao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellValorCotaAplicacao.Name = "xrTableCellValorCotaAplicacao";
            this.xrTableCellValorCotaAplicacao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellValorCotaAplicacao.Text = "#ValorCotaAplicacao";
            this.xrTableCellValorCotaAplicacao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellValorCotaAplicacao.Weight = 0.15984974958263773;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#ValorAplicado";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.088480801335559259;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.Text = "#QtdCotas";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.10601001669449083;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.Text = "#SaldoBruto";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.10601001669449083;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#ValorIR";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.088480801335559259;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.Text = "#ValorIOF";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.070534223706176957;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Text = "#SaldoLiquido";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.097245409015025042;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Text = "#Rendimento";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.12395659432387313;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Text = "#Retorno";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.078464106844741241;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 79F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(101F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(386F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 53F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.BackColor = System.Drawing.Color.LightGray;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "$Carteira";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 1;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloGroupHeaderBeforePrint);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable8});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 95F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 61F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2396F, 30F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 1;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable8.BorderWidth = 1;
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellCotistaGroupFooter,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell66,
            this.xrTableCell67});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCellCotistaGroupFooter
            // 
            this.xrTableCellCotistaGroupFooter.CanShrink = true;
            this.xrTableCellCotistaGroupFooter.Dpi = 254F;
            this.xrTableCellCotistaGroupFooter.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellCotistaGroupFooter.Name = "xrTableCellCotistaGroupFooter";
            this.xrTableCellCotistaGroupFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellCotistaGroupFooter.Text = "$Carteira";
            this.xrTableCellCotistaGroupFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellCotistaGroupFooter.Weight = 0.24290484140233723;
            this.xrTableCellCotistaGroupFooter.WordWrap = false;
            this.xrTableCellCotistaGroupFooter.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloGroupFooterBeforePrint);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorAplicacao")});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell39.Summary = xrSummary1;
            this.xrTableCell39.Text = "xrTableCell39";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.088063439065108509;
            this.xrTableCell39.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorAplicacaoSummaryCalculated);
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade")});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary2.FormatString = "{0:n8}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell40.Summary = xrSummary2;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell40.Weight = 0.11227045075125208;
            this.xrTableCell40.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.QuantidadeSummaryCalculated);
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorBruto")});
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell41.Summary = xrSummary3;
            this.xrTableCell41.Text = "xrTableCell41";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.10434056761268781;
            this.xrTableCell41.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorBrutoSummaryCalculated);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR")});
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell42.Summary = xrSummary4;
            this.xrTableCell42.Text = "xrTableCell42";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.080133555926544239;
            this.xrTableCell42.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorIRSummaryCalculated);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF")});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell43.Summary = xrSummary5;
            this.xrTableCell43.Text = "xrTableCell43";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.07178631051752922;
            this.xrTableCell43.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorIOFSummaryCalculated);
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorLiquido")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell44.Summary = xrSummary6;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.10475792988313856;
            this.xrTableCell44.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.ValorLiquidoSummaryCalculated);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorRendimento")});
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell66.Summary = xrSummary7;
            this.xrTableCell66.Text = "xrTableCell66";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell66.Weight = 0.11727879799666111;
            this.xrTableCell66.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            this.xrTableCell66.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.RendimentoSummaryCalculated);
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell67.Weight = 0.078464106844741241;
            this.xrTableCell67.SummaryCalculated += new DevExpress.XtraReports.UI.TextFormatEventHandler(this.PercentualRetornoSummaryCalculated);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 64F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.BackColor = System.Drawing.Color.LightGray;
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable9.BorderWidth = 1;
            this.xrTable9.Dpi = 254F;
            this.xrTable9.KeepTogether = true;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(2396F, 53F);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell70,
            this.xrTableCell73});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow9.Weight = 0.33333333333333331;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.Text = "#TotalCotista";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 0.24290484140233723;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.Text = "TotalValorAplicadoCotista";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.088063439065108509;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "TotalQuantidadeCotista";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.11227045075125208;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.Text = "TotalSaldoBrutoCotista";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.10434056761268781;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "TotalValorIRCotista";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.080133555926544239;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "TotalValorIOFCotista";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.07178631051752922;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "TotalSaldoLiquidoCotista";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.10475792988313856;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorRendimento")});
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell70.Summary = xrSummary8;
            this.xrTableCell70.Text = "xrTableCell70";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell70.Weight = 0.11727879799666111;
            this.xrTableCell70.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell73.Weight = 0.078464106844741241;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DataConversao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // posicaoFundoCollection1
            // 
            this.posicaoFundoCollection1.AllowDelete = true;
            this.posicaoFundoCollection1.AllowEdit = true;
            this.posicaoFundoCollection1.AllowNew = true;
            this.posicaoFundoCollection1.EnableHierarchicalBinding = true;
            this.posicaoFundoCollection1.Filter = "";
            this.posicaoFundoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoFundoCollection1.Sort = "";
            // 
            // posicaoFundoHistoricoCollection1
            // 
            this.posicaoFundoHistoricoCollection1.AllowDelete = true;
            this.posicaoFundoHistoricoCollection1.AllowEdit = true;
            this.posicaoFundoHistoricoCollection1.AllowNew = true;
            this.posicaoFundoHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoFundoHistoricoCollection1.Filter = "";
            this.posicaoFundoHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoFundoHistoricoCollection1.Sort = "";
            // 
            // calculatedFieldPercentualRetorno
            // 
            this.calculatedFieldPercentualRetorno.DataSource = this.posicaoFundoCollection1;
            this.calculatedFieldPercentualRetorno.DisplayName = "calculatedFieldPercentualRetorno";
            this.calculatedFieldPercentualRetorno.Expression = "[PercentualRetorno]";
            this.calculatedFieldPercentualRetorno.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldPercentualRetorno.Name = "calculatedFieldPercentualRetorno";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportSaldoAplicacaoFundoView2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.GroupHeader2,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldPercentualRetorno});
            this.DataSource = this.posicaoFundoCollection1;
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(25, 20, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RelatorioBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportSaldoAplicacaoFundoView2.ResourceManager;

        }

        #region Variaveis Internas do Relatorio
        //
        protected class ValoresTotais {
            public decimal totalSaldoBruto = 0;
            public decimal totalQuantidadeCotas = 0;
            public decimal totalValorIR = 0;
            public decimal totalValorIOF = 0;
            public decimal totalValorPerformance = 0;
            public decimal totalSaldoLiquido = 0;
            public decimal totalValorAplicacao = 0;
            public decimal totalRendimento = 0;
            public decimal totalPercentualRetorno = 0;
        }
        //
        private ValoresTotais valoresTotalCliente = new ValoresTotais();

        #endregion

        #region Funções Internas do Relatorio
        //
        #region Cabecalho
        //
        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoXRTableCell = sender as XRTableCell;
            dataPosicaoXRTableCell.Text = this.data.ToString("d");
        }

        private void NomeClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeCliente = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            //
            nomeCliente.Text = this.idCliente + " - " + cliente.Nome;
        }

        #endregion
       
        #region Summary Final de Cada Coluna
        private void ValorAplicacaoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalValorAplicacao += Convert.ToDecimal(e.Value);
            }
        }

        private void QuantidadeSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalQuantidadeCotas += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorBrutoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalSaldoBruto += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorIRSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalValorIR += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorIOFSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalValorIOF += Convert.ToDecimal(e.Value);
            }
        }

        private void ValorLiquidoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalSaldoLiquido += Convert.ToDecimal(e.Value);
            }
        }

        private void RendimentoSummaryCalculated(object sender, TextFormatEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                this.valoresTotalCliente.totalRendimento += Convert.ToDecimal(e.Value);
            }
        }

        private void PercentualRetornoSummaryCalculated(object sender, TextFormatEventArgs e) {
            //if (this.numeroLinhasDataTable != 0) {
            //    #region Somatoria Por Pessoa Fisica e Juridica
            //    int idCotista = (int)GetCurrentColumnValue(this.idCotistaColumn);
            //    Cotista cotista = new Cotista();
            //    cotista.LoadByPrimaryKey(idCotista);

            //    if (cotista.UpToPessoa.IsPessoaFisica()) {
            //        this.valoresPessoaFisica.totalPercentualRetorno += Convert.ToDecimal(e.Value);
            //    }
            //    else {
            //        this.valoresPessoaJuridica.totalPercentualRetorno += Convert.ToDecimal(e.Value);
            //    }
            //    #endregion
            //    //                        
            //    this.valoresTotalCotista.totalPercentualRetorno += Convert.ToDecimal(e.Value);
            //}
        }        
        #endregion

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable tableTotalFinal = sender as XRTable;

            // Prenche Linha 0
            /* Total
               ValorAplicacao
               Quantidade
               SaldoBruto
               ValorIR
               ValorIOF
               ValorLiquido
               Rendimento
               Retorno 
             */

            // Prenche Linha 0
            #region Linha 2
            XRTableRow tableTotalRow0 = tableTotalFinal.Rows[0];
            ((XRTableCell)tableTotalRow0.Cells[0]).Text = Resources.ReportSaldoAplicacaoFundoView2._Total;
            ((XRTableCell)tableTotalRow0.Cells[1]).Text = this.valoresTotalCliente.totalValorAplicacao.ToString("n2");
            ((XRTableCell)tableTotalRow0.Cells[2]).Text = this.valoresTotalCliente.totalQuantidadeCotas.ToString("n8");
            ((XRTableCell)tableTotalRow0.Cells[3]).Text = this.valoresTotalCliente.totalSaldoBruto.ToString("n2");
            ((XRTableCell)tableTotalRow0.Cells[4]).Text = this.valoresTotalCliente.totalValorIR.ToString("n2");
            ((XRTableCell)tableTotalRow0.Cells[5]).Text = this.valoresTotalCliente.totalValorIOF.ToString("n2");
            ((XRTableCell)tableTotalRow0.Cells[6]).Text = this.valoresTotalCliente.totalSaldoLiquido.ToString("n2");

            //this.valoresTotalCliente.totalRendimento = this.valoresPessoaFisica.totalRendimento + this.valoresPessoaJuridica.totalRendimento;
            //ReportBase.ConfiguraSinalNegativo((XRTableCell)summaryFinalRow2.Cells[7], this.valoresTotalCliente.totalRendimento);
            //((XRTableCell)summaryFinalRow2.Cells[7]).Text = this.valoresTotalCotista.totalRendimento.ToString("n2");
            #endregion
        }
           
        private void TituloGroupHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                XRTableCell tituloGroupHeaderXRTableCell = sender as XRTableCell;
                //
                string titulo = "";
                int idCarteira = (int)GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                string nomeCarteira = (string)GetCurrentColumnValue("NomeCarteira");
                int idCliente = (int)GetCurrentColumnValue(ClienteMetadata.ColumnNames.IdCliente);

                titulo = idCarteira + " - " + nomeCarteira.Trim();

                // Cota Dia
                decimal cotaDia = Convert.ToDecimal(GetCurrentColumnValue(PosicaoFundoMetadata.ColumnNames.CotaDia));
                titulo += " - " + Resources.ReportSaldoAplicacaoFundoView2._CotaDia + ": " + cotaDia.ToString("n8");

                // Prejuizo Fundo
                decimal prejuizoAcumulado = 0.00M;
                if (this.tipoPesquisa == TipoPesquisa.PosicaoFundo) {
                    #region Prejuizo Fundo
                    PrejuizoFundo p = new PrejuizoFundo();
                    p.Query.Select(p.Query.ValorPrejuizo.Sum())
                           .Where(p.Query.IdCliente == idCliente &&
                                  p.Query.IdCarteira == idCarteira);

                    if (p.Query.Load()) {
                        if (p.ValorPrejuizo.HasValue) {
                            prejuizoAcumulado = p.ValorPrejuizo.Value;
                        }
                    }
                    #endregion
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoFundoHistorico) {
                    #region Prejuizo Fundo Historico
                    PrejuizoFundoHistorico p = new PrejuizoFundoHistorico();
                    p.Query.Select(p.Query.ValorPrejuizo.Sum())
                           .Where(p.Query.IdCliente == idCliente &&
                                  p.Query.IdCarteira == idCarteira &&
                                  p.Query.DataHistorico == this.data);

                    if (p.Query.Load()) {
                        if (p.ValorPrejuizo.HasValue) {
                            prejuizoAcumulado = p.ValorPrejuizo.Value;
                        }
                    }
                    #endregion
                }

                if (prejuizoAcumulado != 0) {
                    titulo += " - (" + Resources.ReportSaldoAplicacaoFundoView2._Prejuizo_Acumulado + ": " + prejuizoAcumulado.ToString("n2") + ")";
                }

                tituloGroupHeaderXRTableCell.Text = titulo;
            }
        }

        private void TituloGroupFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Desaparece com a Carteira do Grupo Footer se for Relatorio Analitico
            if (this.numeroLinhasDataTable != 0) {
                if (this.tipoRelatorio == TipoRelatorio.Consolidado) {
                    XRTableCell tituloGroupFooterXRTableCell = sender as XRTableCell;
                    //
                    int idCarteira = (int)GetCurrentColumnValue(CarteiraMetadata.ColumnNames.IdCarteira);
                    string nomeCarteira = (string)GetCurrentColumnValue("NomeCarteira");
                    //                    
                    tituloGroupFooterXRTableCell.Text = idCarteira + " - " + nomeCarteira;
                }
                else {
                    this.xrTableCellCotistaGroupFooter.Text = "";
                }
            }
        }

        /// <summary>
        /// Define as Configurações de Acordo com o Tipo do Relatorio
        /// </summary>
        private void ConfiguraTipoRelatorio() {
            #region Define se Detail será exibido
            if (this.tipoRelatorio == TipoRelatorio.Analitico) {
                this.GroupHeader1.Visible = true;
                this.Detail.Visible = true;
            }
            else if (this.tipoRelatorio == TipoRelatorio.Consolidado) {
                this.GroupHeader1.Visible = false;
                this.Detail.Visible = false;

                // Desaparece com DataAplicacao e ValorCotaAplicacao                
                this.xrTableCellDataAplicacao.Text = "";
                this.xrTableCellValorCotaAplicacao.Text = "";
            }
            #endregion
        }

        /// <summary>
        /// Define as Configurações do Agrupamento de Carteira por idCarteira ou NomeCarteira
        /// </summary>
        private void ConfiguraTipoAgrupamento() {
            #region Define o Tipo de Agrupamento do Relatorio
            if (this.tipoGrupamento == TipoGrupamento.PorCodigo) {
                // Define o Agrupamento do Relatorio                                
                this.GroupHeader1.GroupFields[0].FieldName = CarteiraMetadata.ColumnNames.IdCarteira;                
            }
            else if (this.tipoGrupamento == TipoGrupamento.PorNome) {
                // Define o Agrupamento do Relatorio                
                this.GroupHeader1.GroupFields[0].FieldName = "NomeCarteira";
            }
            #endregion
        }

        private void RelatorioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            this.ConfiguraTipoRelatorio();
            this.ConfiguraTipoAgrupamento();
        }

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);

        }
        #endregion

        #endregion
    }
}