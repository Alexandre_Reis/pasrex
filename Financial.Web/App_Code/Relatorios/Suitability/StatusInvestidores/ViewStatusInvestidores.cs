/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/11/2015 14:25:12
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Relatorios
{

	[Serializable]
	abstract public class esViewStatusInvestidoresCollection : esEntityCollection
	{
		public esViewStatusInvestidoresCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ViewStatusInvestidoresCollection";
		}

		#region Query Logic
		protected void InitQuery(esViewStatusInvestidoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esViewStatusInvestidoresQuery);
		}
		#endregion
		
		virtual public ViewStatusInvestidores DetachEntity(ViewStatusInvestidores entity)
		{
			return base.DetachEntity(entity) as ViewStatusInvestidores;
		}
		
		virtual public ViewStatusInvestidores AttachEntity(ViewStatusInvestidores entity)
		{
			return base.AttachEntity(entity) as ViewStatusInvestidores;
		}
		
		virtual public void Combine(ViewStatusInvestidoresCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ViewStatusInvestidores this[int index]
		{
			get
			{
				return base[index] as ViewStatusInvestidores;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ViewStatusInvestidores);
		}
	}



	[Serializable]
	abstract public class esViewStatusInvestidores : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esViewStatusInvestidoresQuery GetDynamicQuery()
		{
			return null;
		}

		public esViewStatusInvestidores()
		{

		}

		public esViewStatusInvestidores(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "Cotista": this.str.Cotista = (string)value; break;							
						case "DocumentoCotista": this.str.DocumentoCotista = (string)value; break;							
						case "Assessor": this.str.Assessor = (string)value; break;							
						case "PerfilRiscoInvestidor": this.str.PerfilRiscoInvestidor = (string)value; break;							
						case "TipoPessoa": this.str.TipoPessoa = (string)value; break;							
						case "DataUltimaMovimentacao": this.str.DataUltimaMovimentacao = (string)value; break;							
						case "FundoQueInveste": this.str.FundoQueInveste = (string)value; break;							
						case "PerfilRiscoFundo": this.str.PerfilRiscoFundo = (string)value; break;							
						case "PerfilRiscoPortifolioCliente": this.str.PerfilRiscoPortifolioCliente = (string)value; break;							
						case "CotistaDesenquadrado": this.str.CotistaDesenquadrado = (string)value; break;							
						case "AssinouIncompatibilidadeDePerfilRisco": this.str.AssinouIncompatibilidadeDePerfilRisco = (string)value; break;							
						case "AssinouTermoAdesao": this.str.AssinouTermoAdesao = (string)value; break;							
						case "DataUltimaAlteracao": this.str.DataUltimaAlteracao = (string)value; break;							
						case "DataProximaRenovacao": this.str.DataProximaRenovacao = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "DataUltimaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaMovimentacao = (System.DateTime?)value;
							break;
						
						case "DataUltimaAlteracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaAlteracao = (System.DateTime?)value;
							break;
						
						case "DataProximaRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataProximaRenovacao = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(ViewStatusInvestidoresMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(ViewStatusInvestidoresMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.Cotista
		/// </summary>
		virtual public System.String Cotista
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.Cotista);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.Cotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.DocumentoCotista
		/// </summary>
		virtual public System.String DocumentoCotista
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.DocumentoCotista);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.DocumentoCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.Assessor
		/// </summary>
		virtual public System.String Assessor
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.Assessor);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.Assessor, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.PerfilRiscoInvestidor
		/// </summary>
		virtual public System.String PerfilRiscoInvestidor
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoInvestidor);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.TipoPessoa
		/// </summary>
		virtual public System.String TipoPessoa
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.TipoPessoa);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.TipoPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.DataUltimaMovimentacao
		/// </summary>
		virtual public System.DateTime? DataUltimaMovimentacao
		{
			get
			{
				return base.GetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaMovimentacao);
			}
			
			set
			{
				base.SetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaMovimentacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.FundoQueInveste
		/// </summary>
		virtual public System.String FundoQueInveste
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.FundoQueInveste);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.FundoQueInveste, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.PerfilRiscoFundo
		/// </summary>
		virtual public System.String PerfilRiscoFundo
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoFundo);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.PerfilRiscoPortifolioCliente
		/// </summary>
		virtual public System.String PerfilRiscoPortifolioCliente
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoPortifolioCliente);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoPortifolioCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.CotistaDesenquadrado
		/// </summary>
		virtual public System.String CotistaDesenquadrado
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.CotistaDesenquadrado);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.CotistaDesenquadrado, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.AssinouIncompatibilidadeDePerfilRisco
		/// </summary>
		virtual public System.String AssinouIncompatibilidadeDePerfilRisco
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.AssinouIncompatibilidadeDePerfilRisco);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.AssinouIncompatibilidadeDePerfilRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.AssinouTermoAdesao
		/// </summary>
		virtual public System.String AssinouTermoAdesao
		{
			get
			{
				return base.GetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.AssinouTermoAdesao);
			}
			
			set
			{
				base.SetSystemString(ViewStatusInvestidoresMetadata.ColumnNames.AssinouTermoAdesao, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.DataUltimaAlteracao
		/// </summary>
		virtual public System.DateTime? DataUltimaAlteracao
		{
			get
			{
				return base.GetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaAlteracao);
			}
			
			set
			{
				base.SetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaAlteracao, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.DataProximaRenovacao
		/// </summary>
		virtual public System.DateTime? DataProximaRenovacao
		{
			get
			{
				return base.GetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataProximaRenovacao);
			}
			
			set
			{
				base.SetSystemDateTime(ViewStatusInvestidoresMetadata.ColumnNames.DataProximaRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ViewStatusInvestidoresMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ViewStatusInvestidoresMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ViewStatusInvestidores.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(ViewStatusInvestidoresMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(ViewStatusInvestidoresMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esViewStatusInvestidores entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String Cotista
			{
				get
				{
					System.String data = entity.Cotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cotista = null;
					else entity.Cotista = Convert.ToString(value);
				}
			}
				
			public System.String DocumentoCotista
			{
				get
				{
					System.String data = entity.DocumentoCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DocumentoCotista = null;
					else entity.DocumentoCotista = Convert.ToString(value);
				}
			}
				
			public System.String Assessor
			{
				get
				{
					System.String data = entity.Assessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Assessor = null;
					else entity.Assessor = Convert.ToString(value);
				}
			}
				
			public System.String PerfilRiscoInvestidor
			{
				get
				{
					System.String data = entity.PerfilRiscoInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRiscoInvestidor = null;
					else entity.PerfilRiscoInvestidor = Convert.ToString(value);
				}
			}
				
			public System.String TipoPessoa
			{
				get
				{
					System.String data = entity.TipoPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPessoa = null;
					else entity.TipoPessoa = Convert.ToString(value);
				}
			}
				
			public System.String DataUltimaMovimentacao
			{
				get
				{
					System.DateTime? data = entity.DataUltimaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaMovimentacao = null;
					else entity.DataUltimaMovimentacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FundoQueInveste
			{
				get
				{
					System.String data = entity.FundoQueInveste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FundoQueInveste = null;
					else entity.FundoQueInveste = Convert.ToString(value);
				}
			}
				
			public System.String PerfilRiscoFundo
			{
				get
				{
					System.String data = entity.PerfilRiscoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRiscoFundo = null;
					else entity.PerfilRiscoFundo = Convert.ToString(value);
				}
			}
				
			public System.String PerfilRiscoPortifolioCliente
			{
				get
				{
					System.String data = entity.PerfilRiscoPortifolioCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRiscoPortifolioCliente = null;
					else entity.PerfilRiscoPortifolioCliente = Convert.ToString(value);
				}
			}
				
			public System.String CotistaDesenquadrado
			{
				get
				{
					System.String data = entity.CotistaDesenquadrado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotistaDesenquadrado = null;
					else entity.CotistaDesenquadrado = Convert.ToString(value);
				}
			}
				
			public System.String AssinouIncompatibilidadeDePerfilRisco
			{
				get
				{
					System.String data = entity.AssinouIncompatibilidadeDePerfilRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AssinouIncompatibilidadeDePerfilRisco = null;
					else entity.AssinouIncompatibilidadeDePerfilRisco = Convert.ToString(value);
				}
			}
				
			public System.String AssinouTermoAdesao
			{
				get
				{
					System.String data = entity.AssinouTermoAdesao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AssinouTermoAdesao = null;
					else entity.AssinouTermoAdesao = Convert.ToString(value);
				}
			}
				
			public System.String DataUltimaAlteracao
			{
				get
				{
					System.DateTime? data = entity.DataUltimaAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaAlteracao = null;
					else entity.DataUltimaAlteracao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataProximaRenovacao
			{
				get
				{
					System.DateTime? data = entity.DataProximaRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataProximaRenovacao = null;
					else entity.DataProximaRenovacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esViewStatusInvestidores entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esViewStatusInvestidoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esViewStatusInvestidores can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}



	[Serializable]
	abstract public class esViewStatusInvestidoresQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ViewStatusInvestidoresMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Cotista
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.Cotista, esSystemType.String);
			}
		} 
		
		public esQueryItem DocumentoCotista
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.DocumentoCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem Assessor
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.Assessor, esSystemType.String);
			}
		} 
		
		public esQueryItem PerfilRiscoInvestidor
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoInvestidor, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoPessoa
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.TipoPessoa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataUltimaMovimentacao
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaMovimentacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FundoQueInveste
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.FundoQueInveste, esSystemType.String);
			}
		} 
		
		public esQueryItem PerfilRiscoFundo
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem PerfilRiscoPortifolioCliente
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoPortifolioCliente, esSystemType.String);
			}
		} 
		
		public esQueryItem CotistaDesenquadrado
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.CotistaDesenquadrado, esSystemType.String);
			}
		} 
		
		public esQueryItem AssinouIncompatibilidadeDePerfilRisco
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.AssinouIncompatibilidadeDePerfilRisco, esSystemType.String);
			}
		} 
		
		public esQueryItem AssinouTermoAdesao
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.AssinouTermoAdesao, esSystemType.String);
			}
		} 
		
		public esQueryItem DataUltimaAlteracao
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaAlteracao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataProximaRenovacao
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.DataProximaRenovacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, ViewStatusInvestidoresMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ViewStatusInvestidoresCollection")]
	public partial class ViewStatusInvestidoresCollection : esViewStatusInvestidoresCollection, IEnumerable<ViewStatusInvestidores>
	{
		public ViewStatusInvestidoresCollection()
		{

		}
		
		public static implicit operator List<ViewStatusInvestidores>(ViewStatusInvestidoresCollection coll)
		{
			List<ViewStatusInvestidores> list = new List<ViewStatusInvestidores>();
			
			foreach (ViewStatusInvestidores emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ViewStatusInvestidoresMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ViewStatusInvestidoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ViewStatusInvestidores(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ViewStatusInvestidores();
		}
		
		
		override public bool LoadAll()
		{
			return base.LoadAll(esSqlAccessType.DynamicSQL);
		}	
		
		#endregion


		[BrowsableAttribute( false )]
		public ViewStatusInvestidoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ViewStatusInvestidoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ViewStatusInvestidoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ViewStatusInvestidores AddNew()
		{
			ViewStatusInvestidores entity = base.AddNewEntity() as ViewStatusInvestidores;
			
			return entity;
		}


		#region IEnumerable<ViewStatusInvestidores> Members

		IEnumerator<ViewStatusInvestidores> IEnumerable<ViewStatusInvestidores>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ViewStatusInvestidores;
			}
		}

		#endregion
		
		private ViewStatusInvestidoresQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ViewStatusInvestidores' view
	/// </summary>

	[Serializable]
	public partial class ViewStatusInvestidores : esViewStatusInvestidores
	{
		public ViewStatusInvestidores()
		{

		}
	
		public ViewStatusInvestidores(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ViewStatusInvestidoresMetadata.Meta();
			}
		}
		
		
		
		override protected esViewStatusInvestidoresQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ViewStatusInvestidoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ViewStatusInvestidoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ViewStatusInvestidoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ViewStatusInvestidoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ViewStatusInvestidoresQuery query;
	}



	[Serializable]
	public partial class ViewStatusInvestidoresQuery : esViewStatusInvestidoresQuery
	{
		public ViewStatusInvestidoresQuery()
		{

		}		
		
		public ViewStatusInvestidoresQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ViewStatusInvestidoresMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ViewStatusInvestidoresMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.IdCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.Cotista, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.Cotista;
			c.CharacterMaxLength = 63;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.DocumentoCotista, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.DocumentoCotista;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.Assessor, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.Assessor;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoInvestidor, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.PerfilRiscoInvestidor;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.TipoPessoa, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.TipoPessoa;
			c.CharacterMaxLength = 8;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaMovimentacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.DataUltimaMovimentacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.FundoQueInveste, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.FundoQueInveste;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoFundo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.PerfilRiscoFundo;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.PerfilRiscoPortifolioCliente, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.PerfilRiscoPortifolioCliente;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.CotistaDesenquadrado, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.CotistaDesenquadrado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.AssinouIncompatibilidadeDePerfilRisco, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.AssinouIncompatibilidadeDePerfilRisco;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.AssinouTermoAdesao, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.AssinouTermoAdesao;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.DataUltimaAlteracao, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.DataUltimaAlteracao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.DataProximaRenovacao, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.DataProximaRenovacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.IdCarteira, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewStatusInvestidoresMetadata.ColumnNames.Tipo, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ViewStatusInvestidoresMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ViewStatusInvestidoresMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Cotista = "Cotista";
			 public const string DocumentoCotista = "DocumentoCotista";
			 public const string Assessor = "Assessor";
			 public const string PerfilRiscoInvestidor = "PerfilRiscoInvestidor";
			 public const string TipoPessoa = "TipoPessoa";
			 public const string DataUltimaMovimentacao = "DataUltimaMovimentacao";
			 public const string FundoQueInveste = "FundoQueInveste";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string PerfilRiscoPortifolioCliente = "PerfilRiscoPortifolioCliente";
			 public const string CotistaDesenquadrado = "CotistaDesenquadrado";
			 public const string AssinouIncompatibilidadeDePerfilRisco = "AssinouIncompatibilidadeDePerfilRisco";
			 public const string AssinouTermoAdesao = "AssinouTermoAdesao";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string DataProximaRenovacao = "DataProximaRenovacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Cotista = "Cotista";
			 public const string DocumentoCotista = "DocumentoCotista";
			 public const string Assessor = "Assessor";
			 public const string PerfilRiscoInvestidor = "PerfilRiscoInvestidor";
			 public const string TipoPessoa = "TipoPessoa";
			 public const string DataUltimaMovimentacao = "DataUltimaMovimentacao";
			 public const string FundoQueInveste = "FundoQueInveste";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string PerfilRiscoPortifolioCliente = "PerfilRiscoPortifolioCliente";
			 public const string CotistaDesenquadrado = "CotistaDesenquadrado";
			 public const string AssinouIncompatibilidadeDePerfilRisco = "AssinouIncompatibilidadeDePerfilRisco";
			 public const string AssinouTermoAdesao = "AssinouTermoAdesao";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string DataProximaRenovacao = "DataProximaRenovacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ViewStatusInvestidoresMetadata))
			{
				if(ViewStatusInvestidoresMetadata.mapDelegates == null)
				{
					ViewStatusInvestidoresMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ViewStatusInvestidoresMetadata.meta == null)
				{
					ViewStatusInvestidoresMetadata.meta = new ViewStatusInvestidoresMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Cotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DocumentoCotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Assessor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PerfilRiscoInvestidor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoPessoa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataUltimaMovimentacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FundoQueInveste", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PerfilRiscoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PerfilRiscoPortifolioCliente", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CotistaDesenquadrado", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AssinouIncompatibilidadeDePerfilRisco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AssinouTermoAdesao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataUltimaAlteracao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataProximaRenovacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "ViewStatusInvestidores";
				meta.Destination = "ViewStatusInvestidores";
				
				meta.spInsert = "proc_ViewStatusInvestidoresInsert";				
				meta.spUpdate = "proc_ViewStatusInvestidoresUpdate";		
				meta.spDelete = "proc_ViewStatusInvestidoresDelete";
				meta.spLoadAll = "proc_ViewStatusInvestidoresLoadAll";
				meta.spLoadByPrimaryKey = "proc_ViewStatusInvestidoresLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ViewStatusInvestidoresMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
