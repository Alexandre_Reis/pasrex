﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using log4net;
using Financial.Captacao;
using Financial.Common;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Investidor;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary Description for ReportRebateCliente
    /// </summary>
    public class ReportRebateCliente : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportRebateCliente));
        //
        private int ano; // ano do relatorio
        private int mes; // mes do relatorio

        private List<int> mesExecucao; // Lista com os meses de execução do relatorio em ordem crescente
        private List<int> anoExecucao; // Lista com os anos de execução do relatorio em ordem crescente
        private List<DateTime> listaPrimeiroDiaUtilMes; // Contém o Primeiro Dia Útil do Mês Referente aos Meses e Anos de Execução em ordem Decrescente
        private List<DateTime> listaUltimoDiaUtilMes; // Contém o Último Dia Útil do Mês Referente aos Meses e Anos de Execução em ordem Decrescente
        //
        private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell49;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private SubReportRodape subReportRodape1;
        private CalculoRebateCarteiraCollection calculoRebateCarteiraCollection1;
        private CalculatedField calculatedFieldRebate1;
        private CalculatedField calculatedFieldRebate2;
        private CalculatedField calculatedFieldRebate3;
        private CalculatedField calculatedFieldRebate4;
        private CalculatedField calculatedFieldNomeCarteira;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell3;
        private CalculatedField calculatedFieldCotista;
        private XRTable xrTable9;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell9;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
       
        public ReportRebateCliente(int mes, int ano) {
            this.mes = mes;
            this.ano = ano;
            //
            #region Define Mes e Ano de execução do Relatório
            this.mesExecucao = new List<int>();
            this.anoExecucao = new List<int>();

            int mesAux = this.mes - 3; // tres meses atras
            // março para tras
            if (mesAux <= 0) {
                #region Janeiro/Fevereiro/Março
                switch (this.mes) {
                    //Janeiro
                    case 1:
                        this.mesExecucao.Add(10); //Outubro
                        this.mesExecucao.Add(11);
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Fevereiro
                    case 2:
                        this.mesExecucao.Add(11); //Novembro
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Março
                    case 3:
                        this.mesExecucao.Add(12); //Dezembro
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.mesExecucao.Add(3);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                }
                #endregion
            }
            else {
                // Adiciona 4 meses do menor para o maior
                for (int i = 3; i >= 0; i--) {
                    this.anoExecucao.Add(this.ano);
                    this.mesExecucao.Add(this.mes - i);
                }
            }
            #endregion

            #region Define os Primeiros Dias Utéis do Mês em Ordem Decrescente
            this.listaPrimeiroDiaUtilMes = new List<DateTime>();

            for (int i = this.mesExecucao.Count - 1; i >= 0; i--) {
                DateTime data = new DateTime(this.anoExecucao[i], this.mesExecucao[i], 1);
                DateTime dataAux = Calendario.RetornaPrimeiroDiaUtilMes(data);
                //
                this.listaPrimeiroDiaUtilMes.Add(dataAux);
            }
            #endregion

            #region Define os Últimos Dias Utéis do Mês em Ordem Decrescente
            this.listaUltimoDiaUtilMes = new List<DateTime>();

            for (int i = this.mesExecucao.Count -1; i >= 0; i--) {
			    DateTime data = new DateTime(this.anoExecucao[i], this.mesExecucao[i], 1);
                DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(data);
                //
                this.listaUltimoDiaUtilMes.Add(dataAux);
			}
            #endregion

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1840);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                //
                this.GroupHeader1.Visible = false;                
                this.GroupFooter1.Visible = false;                
                this.ReportFooter.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;
        }

        private DataView FillDados() {

            #region Consultas Horizontalmente
            // Une as Consultas Horizontalmente
            CalculoRebateCotistaCollection calculoRebateCotistaCollectionOriginal = new CalculoRebateCotistaCollection();

            // Adiciona Colunas Extras
            calculoRebateCotistaCollectionOriginal.CreateColumnsForBinding();
            calculoRebateCotistaCollectionOriginal.AddColumn("Rebate1", typeof(System.Decimal));
            calculoRebateCotistaCollectionOriginal.AddColumn("Rebate2", typeof(System.Decimal));
            calculoRebateCotistaCollectionOriginal.AddColumn("Rebate3", typeof(System.Decimal));
            calculoRebateCotistaCollectionOriginal.AddColumn("Rebate4", typeof(System.Decimal));
            //
            calculoRebateCotistaCollectionOriginal.AddColumn("NomeCotista", typeof(System.String));
            calculoRebateCotistaCollectionOriginal.AddColumn("NomeCarteira", typeof(System.String)); 
            #endregion
            //
            // Realiza 4 Consultas e Une o DataTable na Horizontal
            for (int i = 0; i < 4; i++) {
                int num = i + 1;
                
                #region Consulta por Data
                
                CalculoRebateCotistaQuery calculoRebateCotistaQuery = new CalculoRebateCotistaQuery("CR");
                AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");
                CarteiraQuery carteiraQuery = new CarteiraQuery("C");
                PessoaQuery pessoaQuery = new PessoaQuery("P");
                CotistaQuery cotistaQuery = new CotistaQuery("CO");
                //
                calculoRebateCotistaQuery.Select(calculoRebateCotistaQuery.IdCarteira,
                                                 calculoRebateCotistaQuery.IdCotista,
                                                 pessoaQuery.Nome.Trim().As("NomeCotista"),
                                                 carteiraQuery.Nome.Trim().As("NomeCarteira"),
                                                 (
                                                   calculoRebateCotistaQuery.RebateAdministracaoDia
                                                 ).Sum().As("Rebate" + num)
                                                 );
                //
                calculoRebateCotistaQuery.InnerJoin(carteiraQuery)
                                         .On(calculoRebateCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);

                calculoRebateCotistaQuery.InnerJoin(agenteMercadoQuery)
                                         .On(carteiraQuery.IdAgenteGestor == agenteMercadoQuery.IdAgente);

                calculoRebateCotistaQuery.InnerJoin(cotistaQuery)
                         .On(calculoRebateCotistaQuery.IdCotista == cotistaQuery.IdCotista);

                calculoRebateCotistaQuery.InnerJoin(pessoaQuery)
                         .On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);

                //
                calculoRebateCotistaQuery.Where(calculoRebateCotistaQuery.DataCalculo.Between(this.listaPrimeiroDiaUtilMes[i], this.listaUltimoDiaUtilMes[i]) &
                                                agenteMercadoQuery.FuncaoGestor == "S");
                //
                calculoRebateCotistaQuery.GroupBy( calculoRebateCotistaQuery.IdCarteira,
                                                   calculoRebateCotistaQuery.IdCotista,
                                                   pessoaQuery.Nome,
                                                   carteiraQuery.Nome);
                //
                //calculoRebateCotistaQuery.OrderBy(agenteMercadoQuery.Nome.Ascending,
                //                                  calculoRebateCotistaQuery.IdCarteira.Ascending);
                //
                CalculoRebateCotistaCollection c = new CalculoRebateCotistaCollection();

                c.Load(calculoRebateCotistaQuery);
                //

                // Transforma a Collection Horizontalmente
                for (int j = 0; j < c.Count; j++) 
                {
                    CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
                    carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                    carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(c[j].IdCarteira.Value));
                    carteiraMaeCollection.Query.Load();

                    if (carteiraMaeCollection.Count > 0)
                    {
                        continue; //Carteira mãe não deve ser computada
                    }

                    CalculoRebateCotista cAux = this.FindByValues(calculoRebateCotistaCollectionOriginal, c[j].IdCarteira.Value, c[j].IdCotista.Value);
                    // Se não achou Adiciona na Collection
                    if (cAux == null) {
                        calculoRebateCotistaCollectionOriginal.AttachEntity(c[j]);

                    }
                    // Update
                    else {
                        int index = this.IndexOf(calculoRebateCotistaCollectionOriginal, c[j].IdCarteira.Value, c[j].IdCotista.Value);
                        if (index != -1) {
                            calculoRebateCotistaCollectionOriginal[index].SetColumn("Rebate" + num, c[j].GetColumn("Rebate" + num));
                        }
                    }
                }
                #endregion
            }

            return calculoRebateCotistaCollectionOriginal.LowLevelBind();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/ReportRebateCliente.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportRebateCliente.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.calculoRebateCarteiraCollection1 = new Financial.Captacao.CalculoRebateCarteiraCollection();
            this.calculatedFieldRebate1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRebate2 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRebate3 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRebate4 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldNomeCarteira = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.calculatedFieldCotista = new DevExpress.XtraReports.UI.CalculatedField();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 47.64583F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("calculatedFieldNomeCarteira", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell36});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldNomeCarteira")});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.44782608695652176;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate1", "{0:n2}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.13804347826086957;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate2", "{0:n2}")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.13804347826086957;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate3", "{0:n2}")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.13804347826086957;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateBeforePrint);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate4", "{0:n2}")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.13804347826086957;
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 106F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 1.525879E-05F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 42.00003F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Período:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCellDataInicio.StylePriority.UseFont = false;
            this.xrTableCellDataInicio.StylePriority.UsePadding = false;
            this.xrTableCellDataInicio.Text = "Data";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.66614173228346452;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Data Emissão:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable7,
            this.xrTable10,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 251.125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(50.77084F, 200F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1860F, 16F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(92F, 43F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 200F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1840F, 48F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell12,
            this.xrTableCell35});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.44782608695652176;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "Mes1";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.13804347826086957;
            this.xrTableCell28.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes1BeforePrint);
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mes2";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.13804347826086957;
            this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes2BeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Mes3";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell12.Weight = 0.13804347826086957;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes3BeforePrint);
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Mes4";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.13804347826086957;
            this.xrTableCell35.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes4BeforePrint);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(952F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "Rebate por Gestor (Analítico)";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 74F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(254F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("calculatedFieldCotista", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(1840F, 48F);
            this.xrTable8.StylePriority.UseBackColor = false;
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldCotista")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 66.16666F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable9.Dpi = 254F;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Total Cliente:";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.44782608695652176;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate1")});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell6.Summary = xrSummary1;
            this.xrTableCell6.Text = "xrTableCell23";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.13804347826086957;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate2")});
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell7.Summary = xrSummary2;
            this.xrTableCell7.Text = "xrTableCell24";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.13804347826086957;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate3")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell8.Summary = xrSummary3;
            this.xrTableCell8.Text = "xrTableCell27";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.13804347826086957;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate4")});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell9.Summary = xrSummary4;
            this.xrTableCell9.Text = "xrTableCell37";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell9.Weight = 0.13804347826086957;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 60F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 15F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable5.StylePriority.UseBackColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell49});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "Total Geral:";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.44782608695652176;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate1")});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell43.Summary = xrSummary5;
            this.xrTableCell43.Text = "xrTableCell43";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.13804347826086957;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate2")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell44.Summary = xrSummary6;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.13804347826086957;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate3")});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell45.Summary = xrSummary7;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.13804347826086957;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebate4")});
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:n2}";
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell49.Summary = xrSummary8;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.13804347826086957;
            // 
            // calculoRebateCarteiraCollection1
            // 
            this.calculoRebateCarteiraCollection1.AllowDelete = true;
            this.calculoRebateCarteiraCollection1.AllowEdit = true;
            this.calculoRebateCarteiraCollection1.AllowNew = true;
            this.calculoRebateCarteiraCollection1.EnableHierarchicalBinding = true;
            this.calculoRebateCarteiraCollection1.Filter = "";
            this.calculoRebateCarteiraCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.calculoRebateCarteiraCollection1.Sort = "";
            // 
            // calculatedFieldRebate1
            // 
            this.calculatedFieldRebate1.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldRebate1.Expression = "[Rebate1]";
            this.calculatedFieldRebate1.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebate1.Name = "calculatedFieldRebate1";
            // 
            // calculatedFieldRebate2
            // 
            this.calculatedFieldRebate2.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldRebate2.Expression = "[Rebate2]";
            this.calculatedFieldRebate2.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebate2.Name = "calculatedFieldRebate2";
            // 
            // calculatedFieldRebate3
            // 
            this.calculatedFieldRebate3.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldRebate3.Expression = "[Rebate3]";
            this.calculatedFieldRebate3.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebate3.Name = "calculatedFieldRebate3";
            // 
            // calculatedFieldRebate4
            // 
            this.calculatedFieldRebate4.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldRebate4.Expression = "[Rebate4]";
            this.calculatedFieldRebate4.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebate4.Name = "calculatedFieldRebate4";
            // 
            // calculatedFieldNomeCarteira
            // 
            this.calculatedFieldNomeCarteira.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldNomeCarteira.Expression = "[NomeCarteira]";
            this.calculatedFieldNomeCarteira.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldNomeCarteira.Name = "calculatedFieldNomeCarteira";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // calculatedFieldCotista
            // 
            this.calculatedFieldCotista.DataSource = this.calculoRebateCarteiraCollection1;
            this.calculatedFieldCotista.Expression = "[NomeCotista]";
            this.calculatedFieldCotista.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldCotista.Name = "calculatedFieldCotista";
            // 
            // ReportRebateCliente
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldRebate1,
            this.calculatedFieldRebate2,
            this.calculatedFieldRebate3,
            this.calculatedFieldRebate4,
            this.calculatedFieldNomeCarteira,
            this.calculatedFieldCotista});
            this.DataSource = this.calculoRebateCarteiraCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportRebateCliente.ResourceManager;
        }

        #region Funções Internas do Relatorio
        //
        /// <summary>
        /// Data do Cabeçalho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellData = sender as XRTableCell;
            //
            // Primeiro Mes
            string mes = Utilitario.RetornaMesString(this.mesExecucao[0]);
            
            // Último Mes            
            string ultimoMes = Utilitario.RetornaMesString(this.mesExecucao[this.mesExecucao.Count - 1]);

            xrTableCellData.Text = mes + "/" + this.anoExecucao[0] + " à " + ultimoMes + "/" + this.anoExecucao[this.anoExecucao.Count-1];
        }
        
        /// <summary>
        /// Preenche o Mes de Execução do Relatório
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>             
        private void Mes1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[3]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[3];
        }

        /// <summary>
        /// Preenche o Mes de Execução do Relatório
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mes2BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[2]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[2];
        }

        /// <summary>
        /// Preenche o Mes de Execução do Relatório 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mes3BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[1]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[1];
        }

        /// <summary>
        /// Preenche o Mes de Execução do Relatório 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mes4BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[0]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[0];
        }

        /// <summary>
        /// Procura Elemento na Collection por idCarteira e idCotista
        /// </summary>
        /// <param name="c">CalculoRebateCotistaCollection</param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns>Primeiro Objeto CalculoRebateCotista que Satisfaz os Critérios de Busca ou Null se não Achar</returns>
        private CalculoRebateCotista FindByValues(CalculoRebateCotistaCollection c, int idCarteira, int idCotista) {
            // Loop da Collection
            for (int i = 0; i < c.Count; i++) {
                CalculoRebateCotista calculoRebateCotista = c[i];
                if (calculoRebateCotista.IdCarteira.Value == idCarteira &&
                    calculoRebateCotista.IdCotista.Value == idCotista) {
                    return calculoRebateCotista;
                }
            }

            return null;
        }

        /// <summary>
        /// Procura Elemento na Collection por idCarteira e idCotista
        /// </summary>
        /// <param name="c">CalculoRebateCotistaCollection</param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns>Indice do Primeiro Objeto CalculoRebateCotista que Satisfaz os Critérios 
        ///          de Busca ou -1 se não Achar</returns>
        private int IndexOf(CalculoRebateCotistaCollection c, int idCarteira, int idCotista) {
            // Loop da Collection
            for (int i = 0; i < c.Count; i++) {
                CalculoRebateCotista calculoRebateCotista = c[i];
                if (calculoRebateCotista.IdCarteira.Value == idCarteira &&
                    calculoRebateCotista.IdCotista.Value == idCotista) {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Caso Rebate seja vazio Substitue por Traço
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RebateBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellRebate = sender as XRTableCell;

            if (String.IsNullOrEmpty(xrTableCellRebate.Text)) {
                xrTableCellRebate.Text = "  -  ";
            }
        }

        #endregion
    }
}