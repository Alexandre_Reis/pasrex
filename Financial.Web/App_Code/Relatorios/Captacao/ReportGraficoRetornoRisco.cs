﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Common.Exceptions;
using Financial.Util.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportGraficoRetornoRisco
    /// </summary>
    public class ReportGraficoRetornoRisco : XtraReport {

        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private byte tipoRetorno;

        public byte TipoRetorno
        {
            get { return tipoRetorno; }
            set { tipoRetorno = value; }
        }

        // Lista de Carteiras que será gerado o Grafico
        private List<int> listaIdCarteira;

        #region Classe Interna que Contem o IdCarteira E os Dados de Volatilidade da Carteira
        protected class DadosCarteira {
            //
            public int idCarteira;
            public string nomeFundo;
            //
            public decimal? retorno; // Retorno Especificado num intervalo de tempo
            public decimal? volatilidade; // Volatilidade Especificada num intervalo de tempo
        }
        //
        private DadosCarteira[] dadosCarteira;
        #endregion

        /// <summary>
        /// Retorna true se o report tem dados
        /// </summary>
        /// <returns></returns>
        private bool HasData {
            get {
                bool retorno = false;

                for (int i = 0; i < this.dadosCarteira.Length; i++) {
                    if (this.dadosCarteira[i].retorno.HasValue && this.dadosCarteira[i].volatilidade.HasValue) {
                        retorno = true;
                        break;
                    }
                }
                return retorno;
            }
        }

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private PageHeaderBand PageHeader;
        private XRChart xrChart1;
        private XRLabel xrLabel1;
        private XRLine xrLine1;
        private XRSubreport xrSubreport1;
        private ReportSemDados reportSemDados1;
        private PageFooterBand PageFooter;
        private XRSubreport xrSubreport2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRPageInfo xrPageInfo1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaIdCarteira">Lista de IdCarteiras a ser colocado no gráfico</param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public ReportGraficoRetornoRisco(List<int> listaIdCarteira, DateTime dataInicio, DateTime dataFim, byte tipoRetorno) {
            this.listaIdCarteira = listaIdCarteira;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            this.tipoRetorno = tipoRetorno;
            //
            // Inicializa a classe De Dados de Carteira
            this.dadosCarteira = new DadosCarteira[listaIdCarteira.Count];
            for (int i = 0; i < listaIdCarteira.Count; i++) {
                this.dadosCarteira[i] = new DadosCarteira();
                //
                this.dadosCarteira[i].idCarteira = listaIdCarteira[i];

                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Apelido);
                carteira.LoadByPrimaryKey(campos, listaIdCarteira[i]);
                //
                this.dadosCarteira[i].nomeFundo = carteira.Apelido.Trim();
            }

            this.InitializeComponent();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.PersonalInitialize();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2410);
        }

        /// <summary>
        /// Se Relatorio não tem Dados Mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            // Desaparece com o Grafico
            this.xrChart1.Visible = false;
            // Aparece com o SubRelatorio sem Dados
            this.xrSubreport1.Visible = true;
        }

        /// <summary>
        /// Inicializações Personalizadas
        /// </summary>        
        public void PersonalInitialize() {
            //
            this.CarregaRiscoVolatilidade();
            //
            if (this.HasData) {
                this.FillDadosGrafico();
                // Define as Cores dos Pontos das Series
                ReportBase.PersonalizaCoresPointSeries(this.xrChart1);
            }
            else {
                this.SetRelatorioSemDados();
            }
        }

        /// <summary>
        /// Carrega Retorno e Volatidade Dos Fundos
        /// </summary>
        private void CarregaRiscoVolatilidade() {
            for (int j = 0; j < this.dadosCarteira.Length; j++) {
                CalculoMedida calculoRetorno = new CalculoMedida(dadosCarteira[j].idCarteira);
                calculoRetorno.SetAjustaCota(true);
                //
                CalculoMedida calculoVolatilidade = new CalculoMedida(dadosCarteira[j].idCarteira);
                calculoVolatilidade.SetAjustaCota(true);
                //
                #region Retorno

                try 
                {
                    decimal retorno = 0;
                    if (this.TipoRetorno == 0) //Retorno Efetivo
                    {
                        retorno = calculoRetorno.CalculaRetorno(this.dataInicio, this.dataFim);
                    }
                    else //Retorno Anualizado
                    {
                        retorno = calculoRetorno.CalculaRetornoAnualizado(this.dataInicio, this.dataFim);
                    }
                    this.dadosCarteira[j].retorno = retorno;
                }
                catch (Exception e) {
                    continue;
                }
                #endregion

                #region Volatilidade

                try {
                    decimal volatilidade = calculoVolatilidade.CalculaVolatilidade(this.dataInicio, this.dataFim);
                    this.dadosCarteira[j].volatilidade = volatilidade;
                }
                catch (Exception e) {
                    continue;
                }
                #endregion
            }
        }

        /// <summary>
        /// Realiza o DataBinding dos Dados do Grafico
        /// </summary>
        private void FillDadosGrafico() {

            //#region Adiciona N Série de Fundos
            ////
            //for (int i = 0; i < this.dadosCarteira.Length; i++) {
            //    Series series1 = new Series(this.dadosCarteira[i].nomeFundo, ViewType.Point);
            //    //

            //    // Só Adiciona Serie se houver Dados
            //    if (this.dadosCarteira[i].volatilidade.HasValue && this.dadosCarteira[i].retorno.HasValue) {
            //        series1.Points.Add(new SeriesPoint( new object[] { this.dadosCarteira[i].volatilidade.Value },
            //                                            new object[] { this.dadosCarteira[i].retorno.Value }));

            //        //series1.Label.Visible = false;
            //        //
            //        this.xrChart1.Series.Add(series1);
            //    }
            //}
            //#endregion


            // Ordena de acordo com a volatilidade
            decimal[] keys = new decimal[this.dadosCarteira.Length];
            for (int i = 0; i < this.dadosCarteira.Length; i++) {
                keys[i] = this.dadosCarteira[i].volatilidade.Value;
            }
            // dadosCarteira ordenado por volatilidade
            Array.Sort(keys, this.dadosCarteira);

            #region Adiciona N Série de Fundos

            //int j = 0;
            for (int i = 0; i < this.dadosCarteira.Length; i++) {                
                //Só Adiciona Serie se houver Dados
                if (this.dadosCarteira[i].volatilidade.HasValue && this.dadosCarteira[i].retorno.HasValue) {
                    //j++; // Controla a Serie que vai ser adicionada
                    
                    Series s = new Series(this.dadosCarteira[i].nomeFundo, ViewType.Point);
                    this.xrChart1.Series.Add(s);

                    // Copia a Serie Default
                    //this.xrChart1.Series.Add(this.xrChart1.Series[0]);
                    //this.xrChart1.Series[i + 1].Name = this.dadosCarteira[i].nomeFundo;                    
                    this.xrChart1.Series[i + 1].Points.Add(new SeriesPoint(this.dadosCarteira[i].volatilidade.Value.ToString("n2"),
                                                                           new object[] { this.dadosCarteira[i].retorno.Value.ToString("n2") }));
                    //
                    //this.xrChart1.Series[i + 1].PointOptions.ArgumentNumericOptions.Precision = 2;
                    //this.xrChart1.Series[i + 1].PointOptions.ValueNumericOptions.Precision = 2;                                                
                }
            }
            #endregion

            this.xrChart1.Series[0].ShowInLegend = false;
            this.xrChart1.Series[0].Visible = false;

            //this.xrChart1.SeriesSorting = SortingMode.Ascending;           
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/ReportGraficoRetornoRisco.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportGraficoRetornoRisco.resx";
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.PointOptions pointOptions2 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.PointSeriesView pointSeriesView1 = new DevExpress.XtraCharts.PointSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.PointSeriesView pointSeriesView2 = new DevExpress.XtraCharts.PointSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrTable1,
            this.xrSubreport1,
            this.xrLine1,
            this.xrLabel1,
            this.xrChart1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 1535F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.StylePriority.UseFont = false;
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2455F, 21F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(50F, 127F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(847F, 50F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Período:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell1.Weight = 0.2408500590318772;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "#Data";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell2.Weight = 0.75914994096812283;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBeforePrint);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(50F, 193F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(106F, 21F);
            this.xrSubreport1.Visible = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.SteelBlue;
            this.xrLine1.LineWidth = 7;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(614F, 42F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(50F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(423F, 64F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Retorno X Risco";
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceName = "Terracotta Pie";
            this.xrChart1.AutoBindingSettingsEnabled = false;
            this.xrChart1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            xyDiagram1.AxisX.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.EndText = "%";
            xyDiagram1.AxisX.NumericOptions.Precision = 0;
            xyDiagram1.AxisX.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisX.Title.Text = "Risco % (Vol)";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.GridLines.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            xyDiagram1.AxisY.Label.EndText = "%";
            xyDiagram1.AxisY.VisualRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "Retorno (%)";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYZooming = true;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Dpi = 254F;
            this.xrChart1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.xrChart1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.xrChart1.Legend.BackColor = System.Drawing.Color.Transparent;
            this.xrChart1.Legend.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Legend.MarkerSize = new System.Drawing.Size(15, 15);
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(16F, 238F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.PaletteName = "Civic";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Label = pointSeriesLabel1;
            pointOptions1.ArgumentNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pointOptions1.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;
            pointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            series1.LegendPointOptions = pointOptions1;
            series1.Name = "Retorno X Risco";
            pointOptions2.ArgumentNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            series1.PointOptions = pointOptions2;
            series1.SynchronizePointOptions = false;
            pointSeriesView1.PointMarkerOptions.Kind = DevExpress.XtraCharts.MarkerKind.Square;
            pointSeriesView1.PointMarkerOptions.Size = 10;
            series1.View = pointSeriesView1;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.xrChart1.SeriesTemplate.Label = pointSeriesLabel2;
            pointSeriesView2.ColorEach = true;
            pointSeriesView2.PointMarkerOptions.Size = 10;
            this.xrChart1.SeriesTemplate.View = pointSeriesView2;
            this.xrChart1.SizeF = new System.Drawing.SizeF(1926F, 1249F);
            chartTitle1.Font = new System.Drawing.Font("Tahoma", 16F);
            chartTitle1.Indent = 2;
            chartTitle1.Text = "Análise de Retorno X Risco";
            chartTitle1.TextColor = System.Drawing.Color.Black;
            this.xrChart1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 66F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(254F, 64F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportGraficoRetornoRisco
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportGraficoRetornoRisco.ResourceManager;
        }

        /// <summary>
        /// Preenche a Data do Cabeçalho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataXRTableCell = sender as XRTableCell;
            dataXRTableCell.Text = this.dataInicio.ToString("d") + " à " + this.dataFim.ToString("d");
        }
    }
}