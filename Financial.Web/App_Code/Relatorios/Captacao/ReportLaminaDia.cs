﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportLaminaDia
    /// </summary>
    public class ReportLaminaDia : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        //private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private ReportSemDados reportSemDados1;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport2;
        private SubReportLaminaDia SubReportLaminaDia1;
        private PageHeaderBand PageHeader;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportLaminaDia(int idCarteira, DateTime dataReferencia) {
            this.idCarteira = idCarteira;
            this.dataReferencia = dataReferencia;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            

        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
/*            if (! this.HasData) {
                xrTableCell3.Text = "";
                this.xrSubreport17.Visible = true;
            } */
        }

        /// <summary>
        /// Retorna true se o report tem dados
        /// Se algum subreport possue dados então o report possue dados
        /// </summary>
        /// <returns></returns>
        /*public bool HasData {
            get { 
                return
                    this.subReportAcao1.HasData ||
                    this.subReportOpcaoBolsa1.HasData ||
                    this.subReportTermoBolsa1.HasData ||
                    this.subReportEmprestimoAcao1.HasData ||
                    this.subReportFuturoBMF1.HasData ||
                    this.subReportDisponivelBMF1.HasData ||
                    this.subReportOpcaoDisponivelBMF1.HasData ||
                    this.subReportOpcaoFuturoBMF1.HasData ||
                    this.subReportCotaInvestimento1.HasData ||
                    this.subReportRendaFixa1.HasData ||
                    this.subReportRendaFixaCompromisso1.HasData ||
                    this.subReportSwap1.HasData ||
                    this.subReportLiquidacao1.HasData ||
                    this.subReportLiquidacaoPendente1.HasData ||
                    this.subReportSaldosCC1.HasData ||
                    this.subReportCarteiraResumo1.HasData;
            }
        }*/

        private void PersonalInitialize() {
            // Passa os parametros para os SubReports

            #region Parametros SubReports

            /* SubReportlaminaDia */
            this.SubReportLaminaDia1.PersonalInitialize(this.IdCarteira, this.dataReferencia);
            //this.subReportAcao1.Visible = this.subReportAcao1.HasData;

            #endregion

            #region Pega Campos do resource
            //
            //this.xrTableCell5.Text = Resources.ReportLaminaDia._TituloRelatorio;
            #endregion
        }
       
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/ReportLaminaDia.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "Relatorios/Captacao/ReportLaminaDia.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportLaminaDia.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.SubReportLaminaDia1 = new Financial.Relatorio.SubReportLaminaDia();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubReportLaminaDia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Height = 24;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Location = new System.Drawing.Point(5, 79);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(635, 42);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(635, 42);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Size = new System.Drawing.Size(212, 42);
            this.xrTableCell1.Text = "#Carteira";
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Location = new System.Drawing.Point(212, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Size = new System.Drawing.Size(423, 42);
            this.xrTableCell2.Text = "DataInicio";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.Location = new System.Drawing.Point(1228, 423);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.SubReportLaminaDia1;
            this.xrSubreport2.Size = new System.Drawing.Size(341, 106);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Height = 659;
            this.PageHeader.Name = "PageHeader";
            // 
            // ReportLaminaDia
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(99, 99, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "8.1";
            this.Watermark.Font = new System.Drawing.Font("Verdana", 54F, System.Drawing.FontStyle.Bold);
            this.Watermark.ForeColor = System.Drawing.Color.White;
            this.Watermark.Image = ((System.Drawing.Image)(resources.GetObject("ReportLaminaDia.Watermark.Image")));
            this.Watermark.ImageTransparency = 6;
            this.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Zoom;
            this.Watermark.PageRange = "1";
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubReportLaminaDia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportLaminaDia.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void DataReferenciaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataReferenciaTableCell = sender as XRTableCell;
            dataReferenciaTableCell.Text = this.dataReferencia.ToString("d");
        }

        private void CarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCarteira = sender as XRTableCell;
            xrTableCellCarteira.Text = "";
            // Carrega a Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Nome);
            if(carteira.LoadByPrimaryKey(campos, this.idCarteira)){
                xrTableCellCarteira.Text = this.idCarteira + " - " + carteira.Nome;
            }
        }
        private void TipoRelatorioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            /*XRTableCell tipoRelatorioTableCell = sender as XRTableCell;
            tipoRelatorioTableCell.Text = this.tipoRelatorio == TipoRelatorio.Abertura
                    ? Resources.ReportLaminaDia._Abertura
                    : Resources.ReportLaminaDia._Fechamento;*/
        }
        #endregion
    }
}
