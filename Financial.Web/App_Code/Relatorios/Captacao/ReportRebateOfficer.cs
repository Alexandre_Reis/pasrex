﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using log4net;
using Financial.Captacao;
using Financial.Common;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Captacao.Enums;
using Financial.CRM;
using Financial.Security;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportRebateOfficer
    /// </summary>
    public class ReportRebateOfficer : XtraReport {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportRebateOfficer));
        //
        private int ano; // ano do relatorio
        private int mes; // mes do relatorio
        private int? idOfficer;

        private List<int> mesExecucao; // Lista com os meses de execução do relatorio em ordem crescente
        private List<int> anoExecucao; // Lista com os anos de execução do relatorio em ordem crescente
        private List<DateTime> listaUltimoDiaUtilMes; // Contém o Último Dia Útil do Mês Referente aos Meses e Anos de Execução em ordem Decrescente
        private List<DateTime> listaPrimeiroDiaUtilMes; // Contém o Primeiro Dia Útil do Mês Referente aos Meses e Anos de Execução em ordem Decrescente
        //
        private int numeroLinhasDataTable;
        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell2;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell49;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private SubReportRodape subReportRodape1;
        private CalculatedField calculatedFieldRebateOfficer1;
        private CalculatedField calculatedFieldRebateOfficer2;
        private CalculatedField calculatedFieldRebateOfficer3;
        private CalculatedField calculatedFieldRebateOfficer4;
        private CalculatedField calculatedFieldNomeOfficer;
        private CalculatedField calculatedFieldNomeCliente;
        private TabelaRebateOfficerCollection tabelaRebateOfficerCollection1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportRebateOfficer(int mes, int ano, int? idOfficer)
        {
            this.mes = mes;
            this.ano = ano;
            this.idOfficer = idOfficer;
            //
            #region Define Mes e Ano de execução do Relatório
            this.mesExecucao = new List<int>();
            this.anoExecucao = new List<int>();

            int mesAux = this.mes - 3; // tres meses atras
            // março para tras
            if (mesAux <= 0) {
                #region Janeiro/Fevereiro/Março
                switch (this.mes) {
                    //Janeiro
                    case 1:
                        this.mesExecucao.Add(10); //Outubro
                        this.mesExecucao.Add(11);
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Fevereiro
                    case 2:
                        this.mesExecucao.Add(11); //Novembro
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Março
                    case 3:
                        this.mesExecucao.Add(12); //Dezembro
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.mesExecucao.Add(3);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                }
                #endregion
            }
            else {
                // Adiciona 4 meses do menor para o maior
                for (int i = 3; i >= 0; i--) {
                    this.anoExecucao.Add(this.ano);
                    this.mesExecucao.Add(this.mes - i);
                }
            }
            #endregion

            #region Define os Primeiros Dias Utéis do Mês em Ordem Decrescente
            this.listaPrimeiroDiaUtilMes = new List<DateTime>();

            for (int i = this.mesExecucao.Count - 1; i >= 0; i--) {
                DateTime data = new DateTime(this.anoExecucao[i], this.mesExecucao[i], 1);
                DateTime dataAux = Calendario.RetornaPrimeiroDiaUtilMes(data);
                //
                this.listaPrimeiroDiaUtilMes.Add(dataAux);
            }
            #endregion

            #region Define os Últimos Dias Utéis do Mês em Ordem Decrescente
            this.listaUltimoDiaUtilMes = new List<DateTime>();

            for (int i = this.mesExecucao.Count -1; i >= 0; i--) {
			    DateTime data = new DateTime(this.anoExecucao[i], this.mesExecucao[i], 1);
                DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(data);
                //
                this.listaUltimoDiaUtilMes.Add(dataAux);
			}
            #endregion

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1840);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable3.Visible = false;
                this.xrTable5.Visible = false;
            }
        }

        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;
        }

        private DataView FillDados() 
        {
            // Une as Consultas Horizontalmente            
            this.tabelaRebateOfficerCollection1.CreateColumnsForBinding();
            this.tabelaRebateOfficerCollection1.AddColumn("RebateOfficer1", typeof(System.Decimal));
            this.tabelaRebateOfficerCollection1.AddColumn("RebateOfficer2", typeof(System.Decimal));
            this.tabelaRebateOfficerCollection1.AddColumn("RebateOfficer3", typeof(System.Decimal));
            this.tabelaRebateOfficerCollection1.AddColumn("RebateOfficer4", typeof(System.Decimal));
            this.tabelaRebateOfficerCollection1.AddColumn("NomeOfficer", typeof(System.String));
            this.tabelaRebateOfficerCollection1.AddColumn("NomeCliente", typeof(System.String));
            
            //
            // Realiza 4 Consultas e Une o DataTable na Horizontal
            for (int i = 0; i < 4; i++) {
                int num = i + 1;
                OfficerCollection o = new OfficerCollection();

                if (this.idOfficer.HasValue)
                {
                    o.Query.Where(o.Query.IdOfficer.Equal(this.idOfficer.Value));
                }

                o.Query.Load();
                //
                #region Para Cada Officer
                for (int k = 0; k < o.Count; k++) {
                    // TabelaRebate por Officer
                    TabelaRebateOfficerCollection t = o[k].TabelaRebateOfficerCollectionByIdOfficer;
                    // Lista de IdCarteiras de Cada Officer
                    List<int> listaIdPessoa = new List<int>();
                    // Adiciona as carteiras Univocas(distinct)
                    foreach (TabelaRebateOfficer tabelaRebateOfficer in t) 
                    {
                        if (!listaIdPessoa.Contains(tabelaRebateOfficer.IdPessoa.Value))
                        {
                            listaIdPessoa.Add(tabelaRebateOfficer.IdPessoa.Value);
                        }                    
                    }
                    // Para Cada IdOfficer/IdPessoa Procura o Rebate mais proximo da Data de Referencia
                    for (int j = 0; j < listaIdPessoa.Count; j++) 
                    {
                        TabelaRebateOfficer tAux1 = new TabelaRebateOfficer();
                        //                                              
                        if (tAux1.BuscaRebateOfficer(listaIdPessoa[j], o[k].IdOfficer.Value, this.listaUltimoDiaUtilMes[i]))
                        {
                            // Cria Coluna Virtual
                            tAux1.AddColumn("RebateOfficer" + num, typeof(System.Decimal));
                            tAux1.AddColumn("NomeOfficer", typeof(System.String));
                            tAux1.AddColumn("NomeCliente", typeof(System.String));

                            decimal receitaAdministracao = 0;
                            #region Pega o valor de administracao para a carteira
                            Carteira carteira = new Carteira();
                            if (carteira.LoadByPrimaryKey(listaIdPessoa[j]))
                            {
                                PermissaoCliente permissaoCliente = new PermissaoCliente();
                                if (permissaoCliente.RetornaAcessoCliente(listaIdPessoa[j], HttpContext.Current.User.Identity.Name))
                                {
                                    if (carteira.TipoVisaoFundo == (byte)TipoVisaoFundoRebate.NaoTrata)
                                    {
                                        CalculoAdministracaoHistorico c = new CalculoAdministracaoHistorico();
                                        c.Query.Select(c.Query.ValorDia.Coalesce("0").Sum().As("ValorAdm"))
                                               .Where(c.Query.IdCarteira == listaIdPessoa[j] &
                                                      c.Query.DataHistorico.Between(this.listaPrimeiroDiaUtilMes[i], this.listaUltimoDiaUtilMes[i]));
                                        c.Query.Load();

                                        receitaAdministracao = Convert.ToDecimal(c.GetColumn("ValorAdm"));
                                    }
                                }
                            }
                            #endregion

                            decimal receitaRebateCotista = 0;
                            #region Valor RebateCotista
                            PermissaoCotista permissaoCotista = new PermissaoCotista();

                            try
                            {
                                if (permissaoCotista.RetornaAcessoCotista(listaIdPessoa[j], HttpContext.Current.User.Identity.Name))
                                {
                                    CalculoRebateCotistaQuery calculoRebateCotistaQuery = new CalculoRebateCotistaQuery("C");
                                    PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
                                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                                    CalculoRebateCotista calculoRebateCotista = new CalculoRebateCotista();
                                    
                                    calculoRebateCotistaQuery.Select(
                                            (calculoRebateCotistaQuery.RebateAdministracaoDia +
                                             calculoRebateCotistaQuery.RebatePerformanceDia
                                            ).Coalesce("0").Sum().As("ValorRebateCotista")
                                            );
                                    calculoRebateCotistaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == calculoRebateCotistaQuery.IdCarteira);
                                    calculoRebateCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);
                                    calculoRebateCotistaQuery.Where(calculoRebateCotistaQuery.IdCotista == listaIdPessoa[j] &
                                                                    calculoRebateCotistaQuery.DataCalculo.Between(this.listaPrimeiroDiaUtilMes[i], this.listaUltimoDiaUtilMes[i]),
                                                                    usuarioQuery.Login.Equal(HttpContext.Current.User.Identity.Name));
                                    
                                    calculoRebateCotista.Load(calculoRebateCotistaQuery);

                                    if (!Convert.IsDBNull(calculoRebateCotista.GetColumn("ValorRebateCotista")))
                                    {
                                        receitaRebateCotista = Convert.ToDecimal(calculoRebateCotista.GetColumn("ValorRebateCotista"));
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }                            
                            #endregion

                            decimal receitaCliente = receitaAdministracao + receitaRebateCotista;
	                        decimal valorRebateOfficer = receitaCliente * (tAux1.PercentualRebate.Value / 100);
                            //
                            tAux1.SetColumn("RebateOfficer" +num, valorRebateOfficer);
                            tAux1.SetColumn("NomeOfficer", o[k].str.Apelido.Trim());
                            //
                            #region Carrega Apelido Pessoa
                            Pessoa pessoa = new Pessoa();                            	      
			                List<esQueryItem> campos = new List<esQueryItem>();
                            campos.Add(pessoa.Query.Apelido);
                            //
                            pessoa.LoadByPrimaryKey(campos, listaIdPessoa[j]);
                            #endregion

                            //
                            tAux1.SetColumn("NomeCliente", pessoa.str.Apelido.Trim());
                            //
                           
                            #region Transforma a Collection Horizontalmente
                            // Se Objeto comparado por IdCarteira/IdOfficer não está na Collection
                            if (!this.tabelaRebateOfficerCollection1.Contains(tAux1)) {
                                // Insert na Collection
                                this.tabelaRebateOfficerCollection1.AttachEntity(tAux1);
                            }
                            // Está na Collection - Update
                            else {
                                int index = this.tabelaRebateOfficerCollection1.IndexOf(tAux1.IdPessoa.Value, tAux1.IdOfficer.Value);
                                if (index != -1) {
                                    this.tabelaRebateOfficerCollection1[index].SetColumn("RebateOfficer" + num, valorRebateOfficer);
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion
            }

            this.tabelaRebateOfficerCollection1.Sort = "NomeOfficer ASC";
            return this.tabelaRebateOfficerCollection1.LowLevelBind();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Captacao/ReportRebateOfficer.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportRebateOfficer.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.calculatedFieldRebateOfficer1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.tabelaRebateOfficerCollection1 = new Financial.Captacao.TabelaRebateOfficerCollection();
            this.calculatedFieldRebateOfficer2 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRebateOfficer3 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRebateOfficer4 = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldNomeOfficer = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldNomeCliente = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 46F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell36});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.Text = "Cliente";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.44782608695652176;
            this.xrTableCell39.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer1", "{0:n2}")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.13804347826086957;
            this.xrTableCell21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer2", "{0:n2}")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.13804347826086957;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer3", "{0:n2}")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.13804347826086957;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer4", "{0:n2}")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.13804347826086957;
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 106F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Período:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.StylePriority.UseFont = false;
            this.xrTableCellDataInicio.Text = "Data";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.66614173228346452;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Data Emissão:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable7,
            this.xrTable10,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 251.125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(40.18751F, 200F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1860F, 16F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(92F, 43F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 200F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1840F, 48F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell12,
            this.xrTableCell35});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell38.Weight = 0.44782608695652176;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "Mes1";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.13804347826086957;
            this.xrTableCell28.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes1BeforePrint);
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mes2";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.13804347826086957;
            this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes2BeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Mes3";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell12.Weight = 0.13804347826086957;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes3BeforePrint);
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Mes4";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.13804347826086957;
            this.xrTableCell35.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes4BeforePrint);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(952F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "Rebate por Officer";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 74F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(254F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("calculatedFieldNomeOfficer", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.LightGray;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1840F, 48F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldNomeOfficer")});
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 58F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell27,
            this.xrTableCell37});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 0.44782608695652176;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer1")});
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell23.Summary = xrSummary1;
            this.xrTableCell23.Text = "xrTableCell23";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.13804347826086957;
            this.xrTableCell23.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer2")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell24.Summary = xrSummary2;
            this.xrTableCell24.Text = "xrTableCell24";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.13804347826086957;
            this.xrTableCell24.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer3")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell27.Summary = xrSummary3;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.13804347826086957;
            this.xrTableCell27.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer4")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell37.Summary = xrSummary4;
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.13804347826086957;
            this.xrTableCell37.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 64F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightGray;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1840F, 45F);
            this.xrTable5.StylePriority.UseBackColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell49});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "Total:";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.44782608695652176;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer1")});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell43.Summary = xrSummary5;
            this.xrTableCell43.Text = "xrTableCell43";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.13804347826086957;
            this.xrTableCell43.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer2")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell44.Summary = xrSummary6;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.13804347826086957;
            this.xrTableCell44.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer3")});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell45.Summary = xrSummary7;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.13804347826086957;
            this.xrTableCell45.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldRebateOfficer4")});
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:n2}";
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell49.Summary = xrSummary8;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.13804347826086957;
            this.xrTableCell49.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RebateOfficerBeforePrint);
            // 
            // calculatedFieldRebateOfficer1
            // 
            this.calculatedFieldRebateOfficer1.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldRebateOfficer1.Expression = "[RebateOfficer1]";
            this.calculatedFieldRebateOfficer1.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebateOfficer1.Name = "calculatedFieldRebateOfficer1";
            // 
            // tabelaRebateOfficerCollection1
            // 
            this.tabelaRebateOfficerCollection1.AllowDelete = true;
            this.tabelaRebateOfficerCollection1.AllowEdit = true;
            this.tabelaRebateOfficerCollection1.AllowNew = true;
            this.tabelaRebateOfficerCollection1.EnableHierarchicalBinding = true;
            this.tabelaRebateOfficerCollection1.Filter = "";
            this.tabelaRebateOfficerCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.tabelaRebateOfficerCollection1.Sort = "";
            // 
            // calculatedFieldRebateOfficer2
            // 
            this.calculatedFieldRebateOfficer2.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldRebateOfficer2.Expression = "[RebateOfficer2]";
            this.calculatedFieldRebateOfficer2.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebateOfficer2.Name = "calculatedFieldRebateOfficer2";
            // 
            // calculatedFieldRebateOfficer3
            // 
            this.calculatedFieldRebateOfficer3.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldRebateOfficer3.Expression = "[RebateOfficer3]";
            this.calculatedFieldRebateOfficer3.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebateOfficer3.Name = "calculatedFieldRebateOfficer3";
            // 
            // calculatedFieldRebateOfficer4
            // 
            this.calculatedFieldRebateOfficer4.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldRebateOfficer4.Expression = "[RebateOfficer4]";
            this.calculatedFieldRebateOfficer4.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRebateOfficer4.Name = "calculatedFieldRebateOfficer4";
            // 
            // calculatedFieldNomeOfficer
            // 
            this.calculatedFieldNomeOfficer.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldNomeOfficer.DisplayName = "calculatedFieldOfficerNome";
            this.calculatedFieldNomeOfficer.Expression = "[NomeOfficer]";
            this.calculatedFieldNomeOfficer.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldNomeOfficer.Name = "calculatedFieldNomeOfficer";
            // 
            // calculatedFieldNomeCliente
            // 
            this.calculatedFieldNomeCliente.DataSource = this.tabelaRebateOfficerCollection1;
            this.calculatedFieldNomeCliente.Expression = "[NomeCliente]";
            this.calculatedFieldNomeCliente.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldNomeCliente.Name = "calculatedFieldNomeCliente";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            this.topMarginBand1.StylePriority.UseFont = false;
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportRebateOfficer
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldRebateOfficer1,
            this.calculatedFieldRebateOfficer2,
            this.calculatedFieldRebateOfficer3,
            this.calculatedFieldRebateOfficer4,
            this.calculatedFieldNomeOfficer,
            this.calculatedFieldNomeCliente});
            this.DataSource = this.tabelaRebateOfficerCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportRebateOfficer.ResourceManager;
        }

        #region Funções Internas do Relatorio
        //
        private void DataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellData = sender as XRTableCell;
            //
            // Primeiro Mes
            string mes = Utilitario.RetornaMesString(this.mesExecucao[0]);
            
            // Último Mes            
            string ultimoMes = Utilitario.RetornaMesString(this.mesExecucao[this.mesExecucao.Count - 1]);

            xrTableCellData.Text = mes + "/" + this.anoExecucao[0] + " à " + ultimoMes + "/" + this.anoExecucao[this.anoExecucao.Count-1];
        }
                     
        private void Mes1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[3]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[3];
        }

        private void Mes2BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[2]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[2];
        }

        private void Mes3BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[1]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[1];
        }

        private void Mes4BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[0]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[0];
        }

        private void RebateOfficerBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellRebate = sender as XRTableCell;

            // Comparação Forçada pelo Coalesce
            if (String.IsNullOrEmpty(xrTableCellRebate.Text)) {
                xrTableCellRebate.Text = "  -  ";
            }
        }

        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //TODO: traduzir quando for necessário
            const string REBATE = "Rebate de";
            
            XRTableCell xrTableCellCliente = sender as XRTableCell;

            if (this.numeroLinhasDataTable != 0) {
                string nomeCliente = (string)this.GetCurrentColumnValue("NomeCliente");
                decimal percentualCliente = Convert.ToDecimal(this.GetCurrentColumnValue(TabelaRebateOfficerMetadata.ColumnNames.PercentualRebate));
                //
                xrTableCellCliente.Text = nomeCliente + " (" + REBATE + " " + percentualCliente.ToString("N2") + "%)";
            }
        }

        /// <summary>
        /// Copia A collection com as seguintes colunas Virtuais
        /// RebateOfficer1, RebateOfficer2, RebateOfficer3, RebateOfficer4, NomeOfficer, NomeCliente
        /// </summary>
        /// <param name="t">Collection a Copiar</param>
        /// <returns></returns>
        //private TabelaRebateOfficerCollection CopiaCollection(TabelaRebateOfficerCollection t){
            
        //    TabelaRebateOfficerCollection tabelaRebateOfficerCollectionRetorno = new TabelaRebateOfficerCollection();
        //    tabelaRebateOfficerCollectionRetorno.CreateColumnsForBinding();
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("RebateOfficer1", typeof(System.Decimal));
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("RebateOfficer2", typeof(System.Decimal));
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("RebateOfficer3", typeof(System.Decimal));
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("RebateOfficer4", typeof(System.Decimal));
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("NomeOfficer", typeof(System.String));
        //    tabelaRebateOfficerCollectionRetorno.AddColumn("NomeCliente", typeof(System.String));

        //    for (int i = 0; i < t.Count; i++) {                
                            
        //        TabelaRebateOfficer tAux = t[i];
                
        //        tAux.AddColumn("RebateOfficer1", typeof(System.Decimal));
        //        tAux.AddColumn("RebateOfficer2", typeof(System.Decimal));
        //        tAux.AddColumn("RebateOfficer3", typeof(System.Decimal));
        //        tAux.AddColumn("RebateOfficer4", typeof(System.Decimal));
        //        tAux.AddColumn("NomeOfficer", typeof(System.String));
        //        tAux.AddColumn("NomeCliente", typeof(System.String));
                
        //        tabelaRebateOfficerCollectionRetorno[i].DataReferencia = t[i].DataReferencia;
        //        tabelaRebateOfficerCollectionRetorno[i].IdCarteira = t[i].IdCarteira;
        //        tabelaRebateOfficerCollectionRetorno[i].IdOfficer = t[i].IdOfficer;
        //        tabelaRebateOfficerCollectionRetorno[i].PercentualRebate = t[i].PercentualRebate;
                
        //         Colunas Virtuais
        //        tAux.SetColumn("RebateOfficer1", t[i].GetColumn("RebateOfficer1"));
        //        tAux.SetColumn("RebateOfficer2", t[i].GetColumn("RebateOfficer2"));
        //        tAux.SetColumn("RebateOfficer3", t[i].GetColumn("RebateOfficer3"));
        //        tAux.SetColumn("RebateOfficer4", t[i].GetColumn("RebateOfficer4"));
        //        tAux.SetColumn("NomeOfficer", t[i].GetColumn("NomeOfficer"));
        //        tAux.SetColumn("NomeCliente", t[i].GetColumn("NomeCliente"));

        //        tabelaRebateOfficerCollectionRetorno.AttachEntity(tAux);
        //    }

        //    return tabelaRebateOfficerCollectionRetorno;
        //}

        #endregion
    }
}