/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/08/2011 16:44:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;










































































































































































































namespace Financial.Relatorio
{

    [Serializable]
    abstract public class esTabelaExtratoClienteCollection : esEntityCollection
    {
        public esTabelaExtratoClienteCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "TabelaExtratoClienteCollection";
        }

        #region Query Logic
        protected void InitQuery(esTabelaExtratoClienteQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esTabelaExtratoClienteQuery);
        }
        #endregion

        virtual public TabelaExtratoCliente DetachEntity(TabelaExtratoCliente entity)
        {
            return base.DetachEntity(entity) as TabelaExtratoCliente;
        }

        virtual public TabelaExtratoCliente AttachEntity(TabelaExtratoCliente entity)
        {
            return base.AttachEntity(entity) as TabelaExtratoCliente;
        }

        virtual public void Combine(TabelaExtratoClienteCollection collection)
        {
            base.Combine(collection);
        }

        new public TabelaExtratoCliente this[int index]
        {
            get
            {
                return base[index] as TabelaExtratoCliente;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(TabelaExtratoCliente);
        }
    }



    [Serializable]
    abstract public class esTabelaExtratoCliente : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esTabelaExtratoClienteQuery GetDynamicQuery()
        {
            return null;
        }

        public esTabelaExtratoCliente()
        {

        }

        public esTabelaExtratoCliente(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idSubReport)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idCliente, idSubReport);
            else
                return LoadByPrimaryKeyStoredProcedure(idCliente, idSubReport);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idSubReport)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esTabelaExtratoClienteQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdCliente == idCliente, query.IdSubReport == idSubReport);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idSubReport)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idCliente, idSubReport);
            else
                return LoadByPrimaryKeyStoredProcedure(idCliente, idSubReport);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idSubReport)
        {
            esTabelaExtratoClienteQuery query = this.GetDynamicQuery();
            query.Where(query.IdCliente == idCliente, query.IdSubReport == idSubReport);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idSubReport)
        {
            esParameters parms = new esParameters();
            parms.Add("IdCliente", idCliente); parms.Add("IdSubReport", idSubReport);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdCliente": this.str.IdCliente = (string)value; break;
                        case "IdSubReport": this.str.IdSubReport = (string)value; break;
                        case "PosicaoX": this.str.PosicaoX = (string)value; break;
                        case "PosicaoY": this.str.PosicaoY = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdCliente":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCliente = (System.Int32?)value;
                            break;

                        case "IdSubReport":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdSubReport = (System.Int32?)value;
                            break;

                        case "PosicaoX":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.PosicaoX = (System.Int32?)value;
                            break;

                        case "PosicaoY":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.PosicaoY = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to TabelaExtratoCliente.IdCliente
        /// </summary>
        virtual public System.Int32? IdCliente
        {
            get
            {
                return base.GetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.IdCliente);
            }

            set
            {
                if (base.SetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.IdCliente, value))
                {
                    this._UpToClienteByIdCliente = null;
                }
            }
        }

        /// <summary>
        /// Maps to TabelaExtratoCliente.IdSubReport
        /// </summary>
        virtual public System.Int32? IdSubReport
        {
            get
            {
                return base.GetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.IdSubReport);
            }

            set
            {
                base.SetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.IdSubReport, value);
            }
        }

        /// <summary>
        /// Maps to TabelaExtratoCliente.PosicaoX
        /// </summary>
        virtual public System.Int32? PosicaoX
        {
            get
            {
                return base.GetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.PosicaoX);
            }

            set
            {
                base.SetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.PosicaoX, value);
            }
        }

        /// <summary>
        /// Maps to TabelaExtratoCliente.PosicaoY
        /// </summary>
        virtual public System.Int32? PosicaoY
        {
            get
            {
                return base.GetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.PosicaoY);
            }

            set
            {
                base.SetSystemInt32(TabelaExtratoClienteMetadata.ColumnNames.PosicaoY, value);
            }
        }

        [CLSCompliant(false)]
        internal protected Cliente _UpToClienteByIdCliente;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esTabelaExtratoCliente entity)
            {
                this.entity = entity;
            }


            public System.String IdCliente
            {
                get
                {
                    System.Int32? data = entity.IdCliente;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCliente = null;
                    else entity.IdCliente = Convert.ToInt32(value);
                }
            }

            public System.String IdSubReport
            {
                get
                {
                    System.Int32? data = entity.IdSubReport;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdSubReport = null;
                    else entity.IdSubReport = Convert.ToInt32(value);
                }
            }

            public System.String PosicaoX
            {
                get
                {
                    System.Int32? data = entity.PosicaoX;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.PosicaoX = null;
                    else entity.PosicaoX = Convert.ToInt32(value);
                }
            }

            public System.String PosicaoY
            {
                get
                {
                    System.Int32? data = entity.PosicaoY;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.PosicaoY = null;
                    else entity.PosicaoY = Convert.ToInt32(value);
                }
            }


            private esTabelaExtratoCliente entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esTabelaExtratoClienteQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esTabelaExtratoCliente can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class TabelaExtratoCliente : esTabelaExtratoCliente
    {


        #region UpToClienteByIdCliente - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_TabelaExtratoCliente_Cliente
        /// </summary>

        [XmlIgnore]
        public Cliente UpToClienteByIdCliente
        {
            get
            {
                if (this._UpToClienteByIdCliente == null
                    && IdCliente != null)
                {
                    this._UpToClienteByIdCliente = new Cliente();
                    this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
                    this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
                    this._UpToClienteByIdCliente.Query.Load();
                }

                return this._UpToClienteByIdCliente;
            }

            set
            {
                this.RemovePreSave("UpToClienteByIdCliente");


                if (value == null)
                {
                    this.IdCliente = null;
                    this._UpToClienteByIdCliente = null;
                }
                else
                {
                    this.IdCliente = value.IdCliente;
                    this._UpToClienteByIdCliente = value;
                    this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esTabelaExtratoClienteQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return TabelaExtratoClienteMetadata.Meta();
            }
        }


        public esQueryItem IdCliente
        {
            get
            {
                return new esQueryItem(this, TabelaExtratoClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
            }
        }

        public esQueryItem IdSubReport
        {
            get
            {
                return new esQueryItem(this, TabelaExtratoClienteMetadata.ColumnNames.IdSubReport, esSystemType.Int32);
            }
        }

        public esQueryItem PosicaoX
        {
            get
            {
                return new esQueryItem(this, TabelaExtratoClienteMetadata.ColumnNames.PosicaoX, esSystemType.Int32);
            }
        }

        public esQueryItem PosicaoY
        {
            get
            {
                return new esQueryItem(this, TabelaExtratoClienteMetadata.ColumnNames.PosicaoY, esSystemType.Int32);
            }
        }

    }



    [Serializable]
    [XmlType("TabelaExtratoClienteCollection")]
    public partial class TabelaExtratoClienteCollection : esTabelaExtratoClienteCollection, IEnumerable<TabelaExtratoCliente>
    {
        public TabelaExtratoClienteCollection()
        {

        }

        public static implicit operator List<TabelaExtratoCliente>(TabelaExtratoClienteCollection coll)
        {
            List<TabelaExtratoCliente> list = new List<TabelaExtratoCliente>();

            foreach (TabelaExtratoCliente emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TabelaExtratoClienteMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TabelaExtratoClienteQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new TabelaExtratoCliente(row);
        }

        override protected esEntity CreateEntity()
        {
            return new TabelaExtratoCliente();
        }


        #endregion


        [BrowsableAttribute(false)]
        public TabelaExtratoClienteQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TabelaExtratoClienteQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(TabelaExtratoClienteQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public TabelaExtratoCliente AddNew()
        {
            TabelaExtratoCliente entity = base.AddNewEntity() as TabelaExtratoCliente;

            return entity;
        }

        public TabelaExtratoCliente FindByPrimaryKey(System.Int32 idCliente, System.Int32 idSubReport)
        {
            return base.FindByPrimaryKey(idCliente, idSubReport) as TabelaExtratoCliente;
        }


        #region IEnumerable<TabelaExtratoCliente> Members

        IEnumerator<TabelaExtratoCliente> IEnumerable<TabelaExtratoCliente>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as TabelaExtratoCliente;
            }
        }

        #endregion

        private TabelaExtratoClienteQuery query;
    }


    /// <summary>
    /// Encapsulates the 'TabelaExtratoCliente' table
    /// </summary>

    [Serializable]
    public partial class TabelaExtratoCliente : esTabelaExtratoCliente
    {
        public TabelaExtratoCliente()
        {

        }

        public TabelaExtratoCliente(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TabelaExtratoClienteMetadata.Meta();
            }
        }



        override protected esTabelaExtratoClienteQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TabelaExtratoClienteQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public TabelaExtratoClienteQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TabelaExtratoClienteQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(TabelaExtratoClienteQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private TabelaExtratoClienteQuery query;
    }



    [Serializable]
    public partial class TabelaExtratoClienteQuery : esTabelaExtratoClienteQuery
    {
        public TabelaExtratoClienteQuery()
        {

        }

        public TabelaExtratoClienteQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class TabelaExtratoClienteMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected TabelaExtratoClienteMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(TabelaExtratoClienteMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaExtratoClienteMetadata.PropertyNames.IdCliente;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaExtratoClienteMetadata.ColumnNames.IdSubReport, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaExtratoClienteMetadata.PropertyNames.IdSubReport;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaExtratoClienteMetadata.ColumnNames.PosicaoX, 2, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaExtratoClienteMetadata.PropertyNames.PosicaoX;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaExtratoClienteMetadata.ColumnNames.PosicaoY, 3, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaExtratoClienteMetadata.PropertyNames.PosicaoY;
            c.NumericPrecision = 10;
            _columns.Add(c);


        }
        #endregion

        static public TabelaExtratoClienteMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdCliente = "IdCliente";
            public const string IdSubReport = "IdSubReport";
            public const string PosicaoX = "PosicaoX";
            public const string PosicaoY = "PosicaoY";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdCliente = "IdCliente";
            public const string IdSubReport = "IdSubReport";
            public const string PosicaoX = "PosicaoX";
            public const string PosicaoY = "PosicaoY";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(TabelaExtratoClienteMetadata))
            {
                if (TabelaExtratoClienteMetadata.mapDelegates == null)
                {
                    TabelaExtratoClienteMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (TabelaExtratoClienteMetadata.meta == null)
                {
                    TabelaExtratoClienteMetadata.meta = new TabelaExtratoClienteMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdSubReport", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("PosicaoX", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("PosicaoY", new esTypeMap("int", "System.Int32"));



                meta.Source = "TabelaExtratoCliente";
                meta.Destination = "TabelaExtratoCliente";

                meta.spInsert = "proc_TabelaExtratoClienteInsert";
                meta.spUpdate = "proc_TabelaExtratoClienteUpdate";
                meta.spDelete = "proc_TabelaExtratoClienteDelete";
                meta.spLoadAll = "proc_TabelaExtratoClienteLoadAll";
                meta.spLoadByPrimaryKey = "proc_TabelaExtratoClienteLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private TabelaExtratoClienteMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
