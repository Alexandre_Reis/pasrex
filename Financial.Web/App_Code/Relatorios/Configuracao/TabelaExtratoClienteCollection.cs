﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Drawing;
using Financial.Relatorio.Enums;

namespace Financial.Relatorio
{
    public partial class TabelaExtratoClienteCollection : esTabelaExtratoClienteCollection
    {

        /// <summary>
        /// Dado um cliente retorna um Dicionario com a chave=idSubRelatorio e sua localização na tela
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public Dictionary<int, Point> RetornaPosicaoSubReports(int idCliente)
        {
            Dictionary<int, Point> retorno = new Dictionary<int, Point>();

            /*this.Query.SelectAllExcept(this.Query.IdCliente)
                      .Where(this.Query.IdCliente == idCliente);*/

            this.Query.SelectAllExcept(this.Query.IdCliente);

            this.Query.Load();

            /*
            1- Tabela_RetornoCarteira
            2- Grafico_RetornoAcumulado
            3- Grafico_EvolucaoPL
            4- Grafico_Retorno12meses
            5- Tabela_RiscoRetorno
            6- Grafico_Volatilidade
            7- Grafico_RiscoRetorno
            8- Grafico_AlocacaoGestor
            9- Grafico_AlocacaoDireta
            10- Tabela_PosicaoAtivos
            11- Grafico_Liquidez
            12- Grafico_AlocacaoEstrategia
            13- Tabela_ContaCorrente
            14- Tabela_MovimentoPeriodo
            15- Tabela_RetornoAtivos
            16- Tabela_RetornoEstrategias
            17- Tabela_ResultadoEstrategias
            18- Grafico_DistribuicaoRetorno
            19- Tabela_PosicaoAplicacoes
            */
            if (this.Count == 0) {

                retorno.Add((int)TipoReportExtratoCliente.Tabela_PosicaoAtivos, new Point(0, 0));           /*  posicao 1 */
                //
                retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoCarteira, new Point(0, 100));       /*  posicao 2 */
                //                                
                retorno.Add((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado, new Point(0, 200));     /*  posicao 3 */
                retorno.Add((int)TipoReportExtratoCliente.Grafico_EvolucaoPL, new Point(975, 200));
                //
                retorno.Add((int)TipoReportExtratoCliente.Tabela_RiscoRetorno, new Point(0, 300));          /*  posicao 4 */
                //
                retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoAtivos, new Point(0, 400));         /*  posicao 5 */
                //                
                retorno.Add((int)TipoReportExtratoCliente.Tabela_PosicaoAplicacoes, new Point(0, 500));     /*  posicao 6 */
                //
                retorno.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor, new Point(0, 600));       /*  posicao 7 */
                retorno.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia, new Point(975, 600));
                //
                retorno.Add((int)TipoReportExtratoCliente.Grafico_Retorno12meses, new Point(0, 700));       /*  posicao 8 */
                retorno.Add((int)TipoReportExtratoCliente.Grafico_Liquidez, new Point(975, 700));
                //
                retorno.Add((int)TipoReportExtratoCliente.Grafico_Liquidez2, new Point(0, 800));            /*  posicao 9 */
                //
                retorno.Add((int)TipoReportExtratoCliente.Tabela_ResultadoEstrategias, new Point(0, 900));  /*  posicao 10 */
                //
                retorno.Add((int)TipoReportExtratoCliente.Tabela_MovimentoPeriodo, new Point(0, 1000));      /*  posicao 11 */

                retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoEstrategias, new Point(0, 1100));      /*  posicao 12 */

                retorno.Add((int)TipoReportExtratoCliente.Grafico_FrequenciaRetornos, new Point(0, 1200));      /*  posicao 13 */

                retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos, new Point(0, 1300));         /*  posicao 14 */
                //
                //                
                //retorno.Add(6, new Point(0, 400)); //NAO IMPLEMENTADO
                //retorno.Add(7, new Point(975, 400)); //NAO IMPLEMENTADO                
                //retorno.Add(9, new Point(975, 500)); //NAO IMPLEMENTADO                        
            }
            else
            {
                for (int i = 0; i < this.Count; i++)
                {
                    if (!retorno.ContainsKey(this[i].IdSubReport.Value))
                    {
                        retorno.Add(this[i].IdSubReport.Value, new Point(this[i].PosicaoX.Value, this[i].PosicaoY.Value));
                    }
                }
            }
            return retorno;
        }

        /// <summary>
        /// Dado um cliente retorna um Dicionario com a chave=idSubRelatorio e sua localização na tela
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public Dictionary<int, Point> RetornaPosicaoSubReportsSimulacao()
        {
            Dictionary<int, Point> retorno = new Dictionary<int, Point>();

            retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoCarteira, new Point(0, 0));
                        
            retorno.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia, new Point(0, 100));
            retorno.Add((int)TipoReportExtratoCliente.Grafico_Liquidez, new Point(975, 100));

            retorno.Add((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado, new Point(0, 200));                
            retorno.Add((int)TipoReportExtratoCliente.Grafico_Retorno12meses, new Point(975, 200));

            //retorno.Add((int)TipoReportExtratoCliente.Tabela_RiscoRetorno, new Point(0, 300));
            //retorno.Add((int)TipoReportExtratoCliente.Tabela_RetornoAtivos, new Point(0, 400));
        
            return retorno;
        }

    }
}