﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Common;
using Financial.CRM;
using Financial.Util;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.RendaFixa.Enums;
using EntitySpaces.Core;
using Financial.Common.Enums;

namespace Financial.RendaFixa.Custom1 {
    public class NotaCorretagemRendaFixa {

        #region Privates
        private OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

        private int idCliente;
        private DateTime dataNota;
        private int idAgenteCorretora;

        private LocalCustodiaFixo idCustodia;

        private int? idAssessor;
        private int? idGrupo;
        private int? idPapel;

        private string cnpj;

        /* Header Interno */
        private Header header = new Header();

        /* Footer Interno */
        private Footer footer = new Footer();

        /* Lista interna de Detalhes */
        private List<Detail> lista = new List<Detail>(); 
        #endregion

        /// <summary>
        /// Total de registros dessa nota
        /// </summary>
        /// <returns></returns>
        public int GetQuantidadeRegistros() {
            return this.operacaoRendaFixaCollection.Count;            
        }

        #region Properties
        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public DateTime DataNota {
            get { return dataNota; }
            set { dataNota = value; }
        }

        public int IdAgenteCorretora {
            get { return idAgenteCorretora; }
            set { idAgenteCorretora = value; }
        }
        public LocalCustodiaFixo IdCustodia {
            get { return idCustodia; }
            set { idCustodia = value; }
        }      
        #endregion

        #region Acesso Externo
        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Header GetHeader {
            get { return this.header; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Footer GetFooter {
            get { return this.footer; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public List<Detail> GetListDetail {
            get { return this.lista; }
        }
        #endregion

        #region Construtores
        
        public NotaCorretagemRendaFixa(int idCliente, DateTime dataNota,
                                       int idAgenteCorretora, LocalCustodiaFixo idCustodia,
                                       int? idAssessor, int? idGrupo, int? idPapel, string cnpj) {

            this.idCliente = idCliente;
            //
            this.dataNota = dataNota;
            //
            this.idAgenteCorretora = idAgenteCorretora;
            this.idCustodia = idCustodia;
            //
            this.idAssessor = idAssessor;
            this.idGrupo = idGrupo;
            this.idPapel = idPapel;
            //
           
            this.cnpj = cnpj;
            //

            #region Consulta OperacaoRendaFixa
            this.operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            OperacaoRendaFixaQuery op = new OperacaoRendaFixaQuery("OP");
            TituloRendaFixaQuery t = new TituloRendaFixaQuery("T");
            ClienteRendaFixaQuery crf = new ClienteRendaFixaQuery("C");
            ClienteQuery cliente = new ClienteQuery("C1");
            PapelRendaFixaQuery papelRF = new PapelRendaFixaQuery("P");
            PessoaQuery pessoa = new PessoaQuery("P1");

            op.Select(op, t.DescricaoCompleta, t.Descricao);
            op.InnerJoin(t).On(op.IdTitulo == t.IdTitulo);
            op.InnerJoin(crf).On(op.IdCliente == crf.IdCliente);
            op.InnerJoin(cliente).On(op.IdCliente == cliente.IdCliente);            
            op.InnerJoin(papelRF).On(t.IdPapel == papelRF.IdPapel);
            op.InnerJoin(pessoa).On(cliente.IdPessoa == pessoa.IdPessoa);

            op.Where(op.IdCliente == idCliente,
                     op.DataOperacao == this.dataNota,
                     op.IdAgenteCorretora == idAgenteCorretora,                     
                     op.IdCustodia == (byte)idCustodia,
                     op.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal, (byte)TipoOperacaoTitulo.VendaFinal));

            if (this.idAssessor.HasValue) {
                op.Where(crf.IdAssessor == idAssessor);
            }

            if (this.idGrupo.HasValue) {
                op.Where(cliente.IdGrupoAfinidade == idGrupo);
            }

            if (this.idPapel.HasValue) {
                op.Where(papelRF.IdPapel == idPapel);
            }

            if (!String.IsNullOrEmpty(this.cnpj)) {
                op.Where(pessoa.Cpfcnpj.Like(this.cnpj));
            }
            this.operacaoRendaFixaCollection.Load(op);
            
            #endregion

            this.SetaHeader();
            this.SetaDetail();
            this.SetaFooter();
        }        
        #endregion

        #region Inner Class
        public class Header {

            public Header() {
            }

            public Header(int? a, DateTime b,
                          string c, string d, string e,
                          string f, string g, string h,
                          string i, string j, string k,
                          string l, string m, string n,
                          string o, string p, string q,
                          string r, string s, string t, string u) {

                this.numeroNota = a;
                this.dataPregao = b;
                this.nomeAgente = c;
                this.enderecoCompletoAgente = d;
                this.telefoneCompletoAgente = e;
                this.siteAgente = f;
                this.emailAgente = g;
                this.cnpjAgente = h;
                this.cartaPatente = i;
                this.codigoCliente = j;
                this.cidadeCliente = k;
                this.nomeCliente = l;
                this.enderecoCliente = m;
                this.bairroCliente = n;
                this.cepCliente = o;
                this.ufCliente = p;
                this.telefoneCliente = q;
                this.cpfCNPJCliente = r;
                this.codigoBovespaAgente = s;
                this.codigoAssessor = t;
                this.nomeAssessor = u;
            }

            //Dados da nota
            public int? numeroNota;
            public DateTime dataPregao;
            
            //Dados da corretora
            public string nomeAgente;
            public string enderecoCompletoAgente;
            public string telefoneCompletoAgente;
            public string faxAgente;
            public string siteAgente;
            public string emailAgente;
            public string cnpjAgente;
            public string cartaPatente;
            
            //Dados do cliente
            public string codigoCliente;
            public string nomeCliente;
            public string enderecoCliente;
            public string bairroCliente;
            public string cepCliente;
            public string cidadeCliente;
            public string ufCliente;
            public string telefoneCliente;
            public string cpfCNPJCliente;
            public string codigoBovespaAgente; //Está na seção de cliente pois aparece no quadro de codigo de cliente

            //Dados do Assessor
            public string codigoAssessor;
            public string nomeAssessor;
        }

        public class Detail {
            public Detail() { }


            public Detail(string a, string b, string c, string d, string e,
                          decimal f, decimal g, decimal h) {

                this.tipoOperacao = a;
                this.tipoMercado = b;
                this.prazo = c;
                this.especificacao = d;
                this.observacao = e;
                //
                this.quantidade = f;
                this.preco = g;
                this.valor = h;
            }

            private string tipoOperacao; //C ou V
            private string tipoMercado; //vista
            private string prazo; //hoje em branco
            private string especificacao;
            private string observacao;
            private decimal quantidade;
            private decimal preco;
            private decimal valor;

            #region Properties Necessarias para o Binding do Grid
            public string TipoOperacao {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            public string TipoMercado {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            public string Prazo {
                get { return prazo; }
                set { prazo = value; }
            }

            public string Especificacao {
                get { return especificacao; }
                set { especificacao = value; }
            }

            public string Observacao {
                get { return observacao; }
                set { observacao = value; }
            }

            public decimal Quantidade {
                get { return quantidade; }
                set { quantidade = value; }
            }

            public decimal Preco {
                get { return preco; }
                set { preco = value; }
            }

            public decimal Valor {
                get { return valor; }
                set { valor = value; }
            }
            #endregion
        }

        public class Footer {
            public Footer() { }

            public Footer(decimal a, decimal b, decimal c,
                          decimal d, decimal e, decimal f, decimal g) {
                this.vendasVista = a;
                this.comprasVista = b;
                this.valorLiquido = c;
                this.corretagem = d;
                this.iss = e;
                this.ir = f;
                this.iof = g;
            }

            public decimal vendasVista;
            public decimal comprasVista;
            public decimal valorLiquido;
            public decimal corretagem;
            public decimal emolumentos;
            public decimal iss;
            public decimal ir;
            public decimal iof;
            public DateTime? dataLiquidacao;
            public string cidadeAdministrador = "";
        }

        #endregion
               
        #region Funções
        private void SetaHeader() {

            this.header.dataPregao = this.dataNota;

            #region Dados Corretora
		    AgenteMercado a = new AgenteMercado();
            a.LoadByPrimaryKey(this.idAgenteCorretora);

            #region DDD
            string ddd = "";
            if (!String.IsNullOrEmpty(a.str.Ddd)) {
                ddd = "(" + a.str.Ddd + ") ";
            } 
            #endregion

            #region CidadeEstado
            string cidadeEstado = "";
            string cidade = "";
            if (!String.IsNullOrEmpty(a.str.Cidade)) {
                cidadeEstado += a.str.Cidade;
                cidade = a.str.Cidade;
            }
            if (!String.IsNullOrEmpty(a.str.Uf)) {
                cidadeEstado += " - " + a.str.Uf;
            }
            #endregion

            #region Endereco
            string endereco = "";
            if (!String.IsNullOrEmpty(a.str.Endereco)) {
                endereco += a.str.Endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Bairro)) {
                if (!String.IsNullOrEmpty(a.str.Endereco)) {
                    endereco += ", ";
                }
                endereco += a.str.Bairro;
            }
            #endregion

            #region EnderecoCompleto
            string enderecoCompleto = "";
            if (!String.IsNullOrEmpty(endereco)) {
                enderecoCompleto += endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Cep)) {
                enderecoCompleto += "                      " + a.str.Cep;
            }
            if (!String.IsNullOrEmpty(cidadeEstado)) {

                if (String.IsNullOrEmpty(a.str.Cep)) {
                    enderecoCompleto += "                      " + cidadeEstado;
                }
                else {
                    enderecoCompleto += " " + cidadeEstado;
                }
            }
            // Adapta tamanho maximo
            if (enderecoCompleto.Length >= 97) {
                if (enderecoCompleto.Contains("                      ")) {                                              
                    enderecoCompleto = enderecoCompleto.Replace("                      ", " ");                                                                
                }
            }
            #endregion

            this.footer.cidadeAdministrador = cidade;

            this.header.nomeAgente = a.Nome.Trim();
            this.header.enderecoCompletoAgente = enderecoCompleto;            
            this.header.telefoneCompletoAgente = !String.IsNullOrEmpty(a.str.Telefone) ? ddd + a.str.Telefone : "";
            this.header.faxAgente = !String.IsNullOrEmpty(a.str.Fax) ? ddd + a.str.Fax : "";
            this.header.siteAgente = a.str.Website;
            this.header.emailAgente = a.str.Email;
            this.header.cnpjAgente = a.str.Cnpj == String.Empty ? "" : Utilitario.MascaraCNPJ(a.str.Cnpj);
            this.header.cartaPatente = a.str.CartaPatente;          
	        #endregion       

            #region Dados Cliente
            
            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);

            p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro, p.Cidade, p.Cep, p.Uf, p.IdPessoa);
            p.Where(p.RecebeCorrespondencia == "S" & p.IdPessoa == cliente.IdPessoa);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            string enderecoCompletoCliente = "";
            string cep = "";
            string cidadeEstadoCliente = "";
            string telefone = "";
            //
            if (pessoaEnderecoCollection.HasData) {
                #region EnderecoCompleto
                string enderecoCliente = pessoaEnderecoCollection[0].str.Endereco.Trim();

                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim())) {
                    enderecoCliente += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + "  - " + pessoaEnderecoCollection[0].str.Complemento.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim())) {
                    enderecoCliente += " " + pessoaEnderecoCollection[0].str.Bairro.Trim();
                }

                enderecoCompletoCliente = enderecoCliente; 
                #endregion

                #region Cep
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim())) {
                    cep += Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
                } 
                #endregion

                #region CidadeEstado
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim())) {
                    cidadeEstadoCliente += pessoaEnderecoCollection[0].str.Cidade.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
                    cidadeEstadoCliente += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
                }                
                #endregion

                Pessoa pes = new Pessoa();
                pes.LoadByPrimaryKey(pessoaEnderecoCollection[0].IdPessoa.Value);

                #region Telefone
                if (!String.IsNullOrEmpty(pes.DDD_Fone)) {
                    telefone += pes.DDD_Fone;
                }
                #endregion
            }

            Cliente b = new Cliente();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(b.Query.IdCliente);
            campos.Add(b.Query.Nome);
            b.LoadByPrimaryKey(campos, this.idCliente);

            this.header.codigoCliente = this.idCliente.ToString();
            this.header.nomeCliente = b.Nome;
            this.header.enderecoCliente = enderecoCompletoCliente;
            this.header.telefoneCliente = telefone;
            this.header.cepCliente = cep;
            this.header.cidadeCliente = cidadeEstadoCliente;
            //

            #region CNPJ
            string cpfcnpj = "";
            //
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey((int)b.IdPessoa);
            
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }
            #endregion

            this.header.cpfCNPJCliente = cpfcnpj;
            this.header.codigoBovespaAgente = a.CodigoBovespa.HasValue ? a.CodigoBovespa.Value.ToString() : "";
            if (a.DigitoCodigoBovespa.HasValue)
            {
                this.header.codigoBovespaAgente = this.header.codigoBovespaAgente + "-" + a.DigitoCodigoBovespa.Value.ToString().Substring(0, 1);
            }
            #endregion

            #region Dados Assessor
            ClienteRendaFixa c = new ClienteRendaFixa();
            c.LoadByPrimaryKey(this.idCliente);

            string nomeAssessor = "";
            if (c.IdAssessor.HasValue) {
                Assessor s = new Assessor();
                if(s.LoadByPrimaryKey(c.IdAssessor.Value)){
                    nomeAssessor = s.str.Nome;
                }
                this.header.codigoAssessor = c.IdAssessor.Value.ToString();
                this.header.nomeAssessor = nomeAssessor;
            }            
            #endregion
        }

        private void SetaFooter() {
            decimal totalVendas = 0, totalCompras = 0, totalValorLiquido = 0,
                    totalCorretagem = 0, totalEmolumentos = 0, totalIss = 0, totalIR = 0, totalIOF = 0;

            
            DateTime? dataLiquidacao = null;
    
            for (int i = 0; i < this.operacaoRendaFixaCollection.Count; i++) {

                if (operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal) {
                    totalCompras += operacaoRendaFixaCollection[i].Valor.Value;
                }

                if (operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal) {
                    totalVendas += operacaoRendaFixaCollection[i].Valor.Value;
                }

                totalValorLiquido += operacaoRendaFixaCollection[i].ValorLiquido.Value;
                totalIR += operacaoRendaFixaCollection[i].ValorIR.Value;
                totalIss += operacaoRendaFixaCollection[i].ValorISS.Value;
                totalCorretagem += operacaoRendaFixaCollection[i].ValorCorretagem.Value;
                totalEmolumentos += operacaoRendaFixaCollection[i].Emolumento.Value;
                totalIOF += operacaoRendaFixaCollection[i].ValorIOF.Value;
                dataLiquidacao = operacaoRendaFixaCollection[i].DataLiquidacao;
            }

            this.footer.vendasVista = totalVendas;
            this.footer.comprasVista = totalCompras;
            this.footer.valorLiquido = totalValorLiquido;
            this.footer.corretagem = totalCorretagem;
            this.footer.emolumentos = totalEmolumentos;
            this.footer.iss = totalIss;
            this.footer.ir = totalIR;
            this.footer.iof = totalIOF;
            this.footer.dataLiquidacao = dataLiquidacao;
        }

        private void SetaDetail() {
            for (int i = 0; i < this.operacaoRendaFixaCollection.Count; i++) {
                Detail d = new Detail();
                //
                d.TipoOperacao = operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ? "C" : "V";
                d.TipoMercado = "VISTA";
                d.Prazo = "";
                d.Especificacao = Convert.ToString( operacaoRendaFixaCollection[i].GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao) );
                d.Observacao = "";
                //
                d.Quantidade = operacaoRendaFixaCollection[i].Quantidade.Value;
                d.Preco = operacaoRendaFixaCollection[i].PUOperacao.Value;
                d.Valor = operacaoRendaFixaCollection[i].Valor.Value;
                //
                this.lista.Add(d);
            }
        }
        #endregion
    }
}