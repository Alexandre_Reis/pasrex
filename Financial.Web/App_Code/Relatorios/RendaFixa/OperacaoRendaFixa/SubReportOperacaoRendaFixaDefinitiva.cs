﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.RendaFixa.Enums;
using Financial.RendaFixa;
using Financial.Common;
using Financial.Investidor;
using log4net;
using Financial.Util;
using Financial.Security;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportOperacaoRendaFixaDefinitiva
    /// </summary>
    public class SubReportOperacaoRendaFixaDefinitiva : XtraReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SubReportOperacaoRendaFixaDefinitiva));

        public enum OrigemDados {
            OperacaoRF = 1,
            LiquidacaoRF = 2
        }

        private int? idCliente;

        public int? IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private int? idTitulo;

        public int? IdTitulo
        {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        private int? idPapel;

        public int? IdPapel
        {
            get { return idPapel; }
            set { idPapel = value; }
        }

        private ReportOperacaoRendaFixa.TipoOrdenacao tipoOrdenacao;
        //
        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private int? idLiquidacao;

        public int? IdLiquidacao
        {
            get { return idLiquidacao; }
            set { idLiquidacao = value; }
        }

        private int? idTipoOperacao;

        public int? IdTipoOperacao
        {
            get { return idTipoOperacao; }
            set { idTipoOperacao = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData
        {
            get { return this.numeroLinhasDataTable != 0; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell21;
        private GroupHeaderBand GroupHeader1;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell28;
        private PageFooterBand PageFooter;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell59;
        private PageFooterBand PageFooter1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell2;
        private OperacaoRendaFixaCollection operacaoRendaFixaCollection1;
        private XRTableCell xrTableCell27;
        private CalculatedField calculatedFieldDescricaoTitulo;
        private CalculatedField calculatedFieldIdCliente;
        private CalculatedField calculatedFieldApelidoCliente;
        private CalculatedField calculatedFieldDataVencimentoTitulo;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell12;
        private CalculatedField calculatedFieldDataEmissaoTitulo;
        private CalculatedField calculatedFieldValorLiquido;
        private XRControlStyle xrControlStyle1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportOperacaoRendaFixaDefinitiva()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoOrdenacao">Ordenação do relatorio por Cliente ou por Nome</param>
        public void PersonalInitialize(int? idCliente, DateTime dataInicio, DateTime dataFim, int? idTitulo, int? idPapel, ReportOperacaoRendaFixa.TipoOrdenacao tipoOrdenacao, int? idLiquidacao, int? idTipoOperacao)
        {
            this.idCliente = idCliente;
            this.idTitulo = idTitulo;
            this.idPapel = idPapel;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            this.tipoOrdenacao = tipoOrdenacao;
            this.idTipoOperacao = idTipoOperacao;
            this.IdLiquidacao = idLiquidacao;


            // Consulta do SubRelatorio
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }

        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (this.numeroLinhasDataTable == 0)
            {
                this.xrTable10.Visible = false;
                this.xrTable7.Visible = false;
                this.xrTable2.Visible = false;
            }
        }

        private DataView FillDados()
        {
            #region OperacaoRendaFixa
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            EmissorQuery emissorQuery = new EmissorQuery("E");
            IndiceQuery indiceQuery = new IndiceQuery("I");
            //
            PermissaoClienteQuery p = new PermissaoClienteQuery("p");
            UsuarioQuery u = new UsuarioQuery("u");
            //
            operacaoRendaFixaQuery
                    .Select("< '1' as Origem >",
                            clienteQuery.IdCliente,                             
                            (clienteQuery.IdCliente.Cast(esCastType.String) + " - " + clienteQuery.Apelido).As("ApelidoCliente"),
                            (tituloRendaFixaQuery.IdTitulo.Cast(esCastType.String) + " - " + tituloRendaFixaQuery.Descricao).As("DescricaoTitulo"),
                            tituloRendaFixaQuery.DataVencimento.As("DataVencimentoTitulo"),
                            tituloRendaFixaQuery.Percentual, tituloRendaFixaQuery.DataEmissao,
                            emissorQuery.Nome,
                            indiceQuery.Descricao,
                            operacaoRendaFixaQuery.DataOperacao, operacaoRendaFixaQuery.TipoOperacao,
                            operacaoRendaFixaQuery.DataLiquidacao,
                            operacaoRendaFixaQuery.Quantidade, operacaoRendaFixaQuery.PUOperacao,
                            operacaoRendaFixaQuery.Valor, operacaoRendaFixaQuery.TaxaOperacao,
                            operacaoRendaFixaQuery.DataVolta, operacaoRendaFixaQuery.TaxaVolta,
                            operacaoRendaFixaQuery.PUVolta, operacaoRendaFixaQuery.ValorVolta,
                            operacaoRendaFixaQuery.TipoNegociacao, operacaoRendaFixaQuery.Rendimento,
                            operacaoRendaFixaQuery.ValorIR, operacaoRendaFixaQuery.ValorIOF,
                            operacaoRendaFixaQuery.ValorLiquido);
            //
            operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            operacaoRendaFixaQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
            //
            // Limita Somente quem Tem acesso
            //
            operacaoRendaFixaQuery.InnerJoin(p).On(p.IdCliente == operacaoRendaFixaQuery.IdCliente);
            operacaoRendaFixaQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);

            operacaoRendaFixaQuery.Where(u.Login == HttpContext.Current.User.Identity.Name);
            //        
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));

            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.TipoOperacao.In((int)TipoOperacaoTitulo.CompraFinal, 
                                                                                (int)TipoOperacaoTitulo.VendaFinal,
                                                                                (int)TipoOperacaoTitulo.VendaTotal,
                                                                                (int)TipoOperacaoTitulo.Deposito,
                                                                                (int)TipoOperacaoTitulo.Retirada,
                                                                                (int)TipoOperacaoTitulo.CompraRevenda,
                                                                                (int)TipoOperacaoTitulo.AntecipacaoRevenda,
                                                                                (int)TipoOperacaoTitulo.AntecipacaoRecompra));

            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdIndiceVolta.IsNotNull() | 
                                             (operacaoRendaFixaQuery.IdIndiceVolta.IsNull() & operacaoRendaFixaQuery.TipoOperacao.NotEqual((int)TipoOperacaoTitulo.CompraRevenda)));

            // Adiciona Filtro de Cliente
            if (this.idCliente.HasValue) {
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente == this.idCliente);
            }

            if (this.idTitulo.HasValue)
            {
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdTitulo == this.idTitulo.Value);
            }

            if (this.idPapel.HasValue)
            {
                operacaoRendaFixaQuery.Where(tituloRendaFixaQuery.IdPapel == this.idPapel.Value);
            }

            if (this.IdLiquidacao.HasValue)
            {
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdLiquidacao == this.IdLiquidacao.Value);
            }

            if (this.IdTipoOperacao.HasValue)
            {
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.TipoOperacao == this.idTipoOperacao.Value);
            }

            this.operacaoRendaFixaCollection1.Load(operacaoRendaFixaQuery);

            foreach (OperacaoRendaFixa operacaoRendaFixaAjuste in this.operacaoRendaFixaCollection1)
            {
                if (operacaoRendaFixaAjuste.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    operacaoRendaFixaAjuste.SetColumn("DataVencimentoTitulo", Convert.ToDateTime(operacaoRendaFixaAjuste.DataVolta.Value));
                }

                if (operacaoRendaFixaAjuste.ValorLiquido.Value == 0)
                {
                    operacaoRendaFixaAjuste.ValorLiquido = operacaoRendaFixaAjuste.Valor.Value - operacaoRendaFixaAjuste.ValorIR.Value - operacaoRendaFixaAjuste.ValorIOF.Value;
                }
            }
            #endregion

            #region LiquidacaoRendaFixa
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            clienteQuery = new ClienteQuery("C");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            emissorQuery = new EmissorQuery("E");
            indiceQuery = new IndiceQuery("I");
            
            
            //
            p = new PermissaoClienteQuery("p");
            u = new UsuarioQuery("u");
            //
            liquidacaoRendaFixaQuery
                    .Select("< '2' as Origem >",
                            clienteQuery.IdCliente,                            
                            (clienteQuery.IdCliente.Cast(esCastType.String) + " - " + clienteQuery.Apelido).As("ApelidoCliente"),
                            tituloRendaFixaQuery.IdTitulo,
                            (tituloRendaFixaQuery.IdTitulo.Cast(esCastType.String) + " - " + tituloRendaFixaQuery.Descricao).As("DescricaoTitulo"),                            
                            tituloRendaFixaQuery.DataVencimento.As("DataVencimentoTitulo"),
                            tituloRendaFixaQuery.Percentual, 
                            tituloRendaFixaQuery.DataEmissao,
                            emissorQuery.Nome,
                            indiceQuery.Descricao,
                            liquidacaoRendaFixaQuery.DataLiquidacao, 
                            liquidacaoRendaFixaQuery.TipoLancamento,
                            liquidacaoRendaFixaQuery.Quantidade,
                            liquidacaoRendaFixaQuery.ValorBruto,
                            liquidacaoRendaFixaQuery.Rendimento,
                            liquidacaoRendaFixaQuery.ValorIR,
                            liquidacaoRendaFixaQuery.ValorIOF,
                            liquidacaoRendaFixaQuery.ValorLiquido);
            //
            liquidacaoRendaFixaQuery.InnerJoin(clienteQuery).On(liquidacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
            liquidacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(liquidacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            liquidacaoRendaFixaQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            liquidacaoRendaFixaQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
            //liquidacaoRendaFixaQuery.LeftJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdPosicao = liquidacaoRendaFixaQuery.IdPosicaoResgatada && posicaoRendaFixaQuery.DataHistorico = );
            //liquidacaoRendaFixaQuery.LeftJoin(operacaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdOperacao = 
            //
            // Limita Somente quem Tem acesso
            //
            liquidacaoRendaFixaQuery.InnerJoin(p).On(p.IdCliente == liquidacaoRendaFixaQuery.IdCliente);
            liquidacaoRendaFixaQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
            liquidacaoRendaFixaQuery.Where(u.Login == HttpContext.Current.User.Identity.Name);
            //        
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.DataLiquidacao.Between(this.dataInicio, this.dataFim));

            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.TipoLancamento.NotEqual((byte)TipoLancamentoLiquidacao.Venda));

            // Adiciona Filtro de Cliente
            if (this.idCliente.HasValue) {
                liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdCliente == this.idCliente);
            }

            if (this.idTitulo.HasValue)
            {
                liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdTitulo == this.idTitulo.Value);
            }

            if (this.idPapel.HasValue)
            {
                liquidacaoRendaFixaQuery.Where(tituloRendaFixaQuery.IdPapel == this.idPapel.Value);
            }

            if (this.IdLiquidacao.HasValue)
            {
                OperacaoRendaFixaQuery op = new OperacaoRendaFixaQuery("OP");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("PO");
                posicaoRendaFixaQuery.InnerJoin(op).On(posicaoRendaFixaQuery.IdOperacao == op.IdOperacao);
                posicaoRendaFixaQuery.Where(op.IdLiquidacao == this.idLiquidacao.Value);
                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);

                liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdPosicaoResgatada.In(posicaoRendaFixaQuery));
            }

            if (this.IdTipoOperacao.HasValue)
            {
                OperacaoRendaFixaQuery op = new OperacaoRendaFixaQuery("OP");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("PO");
                posicaoRendaFixaQuery.InnerJoin(op).On(posicaoRendaFixaQuery.IdOperacao == op.IdOperacao);
                posicaoRendaFixaQuery.Where(op.TipoOperacao == this.IdTipoOperacao.Value);
                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);

                liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdPosicaoResgatada.In(posicaoRendaFixaQuery));
            }

            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);
            #endregion

            #region Combine OperacaoRendaFixa e LiquidacaoRendaFixa
            foreach (LiquidacaoRendaFixa l in liquidacaoRendaFixaCollection) {
                OperacaoRendaFixa op = operacaoRendaFixaCollection1.AddNew();
                op.IdCliente = l.IdCliente;
                // Extras
                op.SetColumn("Origem", l.GetColumn("Origem"));
                op.SetColumn("ApelidoCliente", l.GetColumn("ApelidoCliente"));
                op.SetColumn("DescricaoTitulo", l.GetColumn("DescricaoTitulo"));
                op.SetColumn("DataVencimentoTitulo", l.GetColumn("DataVencimentoTitulo"));                
                op.SetColumn("DataEmissao", l.GetColumn("DataEmissao"));
                op.SetColumn("Nome", l.GetColumn("Nome"));                
                //
                op.DataLiquidacao = l.DataLiquidacao;
                op.Quantidade = l.Quantidade;
                op.Rendimento = l.Rendimento;
                op.ValorIR  = l.ValorIR;
                op.ValorIOF = l.ValorIOF;
                //
                if (WebConfigConfiguration.WebConfig.AppSettings.isClienteItau() &&
                    (l.TipoLancamento == (byte)TipoLancamentoLiquidacao.Juros ||
                     l.TipoLancamento == (byte)TipoLancamentoLiquidacao.Amortizacao ||
                     l.TipoLancamento == (byte)TipoLancamentoLiquidacao.PagtoPrincipal))
                {
                    AgendaRendaFixaCollection agendaRendaFixa = new AgendaRendaFixaCollection();
                    agendaRendaFixa.Query
                        .Select(agendaRendaFixa.Query.DataPagamento)
                        .Where(agendaRendaFixa.Query.IdTitulo == l.IdTitulo,
                               agendaRendaFixa.Query.DataEvento == l.DataLiquidacao);
                    switch (l.TipoLancamento)
                    {
                        case (byte)TipoLancamentoLiquidacao.Juros:
                            agendaRendaFixa.Query.Where(agendaRendaFixa.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                            (byte)TipoEventoTitulo.JurosCorrecao,
                                                                                            (byte)TipoEventoTitulo.PagamentoCorrecao,
                                                                                            (byte)TipoEventoTitulo.PagamentoPU));
                            break;
                        case (byte)TipoLancamentoLiquidacao.Amortizacao:
                            agendaRendaFixa.Query.Where(agendaRendaFixa.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                            (byte)TipoEventoTitulo.AmortizacaoCorrigida));
                            break;
                        case (byte)TipoLancamentoLiquidacao.PagtoPrincipal:
                            agendaRendaFixa.Query.Where(agendaRendaFixa.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoPrincipal));
                            break;
                    }

                    if (agendaRendaFixa.Query.Load())
                    {
                        op.DataOperacao = agendaRendaFixa[0].DataPagamento.Value;
                    }
                    else
                    {
                        op.DataOperacao = l.DataLiquidacao;
                    }
                }
                else
                {
                    op.DataOperacao = l.DataLiquidacao;
                }
                op.TipoOperacao = l.TipoLancamento;
                op.Valor = l.ValorBruto;
                op.ValorLiquido = l.ValorLiquido;

                if (!l.PULiquidacao.HasValue || l.PULiquidacao.Value == 0)
                {
                    if (l.Quantidade.Value != 0)
                    {
                        op.PUOperacao = l.ValorBruto.Value / l.Quantidade.Value;
                    }
                }
                else
                {
                    op.PUOperacao = l.PULiquidacao.Value;
                }                

                op.TaxaOperacao = 0;
            }
            #endregion

            return this.operacaoRendaFixaCollection1.LowLevelBind();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/RendaFixa/OperacaoRendaFixa/SubReportOperacaoRendaFixaDefinitiva.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportOperacaoRendaFixaDefinitiva.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter1 = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.operacaoRendaFixaCollection1 = new Financial.RendaFixa.OperacaoRendaFixaCollection();
            this.calculatedFieldDescricaoTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldIdCliente = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldApelidoCliente = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldDataVencimentoTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldDataEmissaoTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldValorLiquido = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 93F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.EvenStyleName = "xrControlStyle1";
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow4});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2488F, 92F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell17,
            this.xrTableCell28,
            this.xrTableCell27,
            this.xrTableCell4});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldApelidoCliente")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.24779116465863449;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldDataEmissaoTitulo", "{0:d}")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "xrTableCell13";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell13.Weight = 0.072389558232931764;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TaxaOperacao", "{0:n4}")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 0.14564086423820283;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "TipoOperacao";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.10486114379392574;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TipoOperacaoBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade", "{0:n2}")});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.11044176706827306;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PUOperacao", "{0:n8}")});
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.12771084337349398;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIR", "{0:n2}")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.08514056224899598;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Rendimento", "{0:n2}")});
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.10522088353413656;
            this.xrTableCell4.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell50,
            this.xrTableCell52,
            this.xrTableCell54,
            this.xrTableCell57,
            this.xrTableCell8});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldDescricaoTitulo")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 0.24779116465863454;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldDataVencimentoTitulo", "{0:d}")});
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "xrTableCell47";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 0.072389558232931778;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.Text = "PercentualIndice";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell48.Weight = 0.14564256782991342;
            this.xrTableCell48.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PercentualIndiceBeforePrint);
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataOperacao", "{0:d}")});
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "xrTableCell50";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 0.10485944020221512;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.11044176706827312;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Valor", "{0:n2}")});
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = "xrTableCell54";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell54.Weight = 0.12771084337349395;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorIOF", "{0:n2}")});
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "xrTableCell57";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.085140562248996;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido", "{0:n2}")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.10522088353413654;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 72F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow3});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2490F, 96F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell1,
            this.xrTableCell15,
            this.xrTableCell2});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "Cliente";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.09558232931726908;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.15301204819277106;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "Emissão";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell26.Weight = 0.072389582745042716;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "Taxa";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell5.Weight = 0.14564076618975902;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Tipo";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell3.Weight = 0.10486121733025854;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell11.Weight = 0.11044176706827313;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Pu";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell1.Weight = 0.127710843373494;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "IR";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.085140562248995938;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Rendimento";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.10522088353413654;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Título";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 0.24859437751004018;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Vencimento";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.0723895827450427;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "Índice";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 0.14564076618975902;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Dt. Operação";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell32.Weight = 0.10486121733025856;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Quantidade";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.11044176706827313;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "Valor";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.127710843373494;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "IOF";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.085140562248996007;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "Valor Líquido";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.10522088353413654;
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(100F, 15F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "Operações Definitivas";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrTable10});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 169F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 50F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(500F, 50F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell59.Weight = 1;
            // 
            // PageFooter1
            // 
            this.PageFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageFooter1.Dpi = 254F;
            this.PageFooter1.HeightF = 19F;
            this.PageFooter1.Name = "PageFooter1";
            this.PageFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(254F, 15F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 1;
            // 
            // operacaoRendaFixaCollection1
            // 
            this.operacaoRendaFixaCollection1.AllowDelete = true;
            this.operacaoRendaFixaCollection1.AllowEdit = true;
            this.operacaoRendaFixaCollection1.AllowNew = true;
            this.operacaoRendaFixaCollection1.EnableHierarchicalBinding = true;
            this.operacaoRendaFixaCollection1.Filter = "";
            this.operacaoRendaFixaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.operacaoRendaFixaCollection1.Sort = "";
            // 
            // calculatedFieldDescricaoTitulo
            // 
            this.calculatedFieldDescricaoTitulo.DataSource = this.operacaoRendaFixaCollection1;
            this.calculatedFieldDescricaoTitulo.Expression = "[DescricaoTitulo]";
            this.calculatedFieldDescricaoTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldDescricaoTitulo.Name = "calculatedFieldDescricaoTitulo";
            // 
            // calculatedFieldIdCliente
            // 
            this.calculatedFieldIdCliente.DataSource = this.operacaoRendaFixaCollection1;
            this.calculatedFieldIdCliente.Expression = "[IdCliente]";
            this.calculatedFieldIdCliente.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldIdCliente.Name = "calculatedFieldIdCliente";
            // 
            // calculatedFieldApelidoCliente
            // 
            this.calculatedFieldApelidoCliente.DataSource = this.operacaoRendaFixaCollection1;
            this.calculatedFieldApelidoCliente.Expression = "[ApelidoCliente]";
            this.calculatedFieldApelidoCliente.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldApelidoCliente.Name = "calculatedFieldApelidoCliente";
            // 
            // calculatedFieldDataVencimentoTitulo
            // 
            this.calculatedFieldDataVencimentoTitulo.DataSource = this.operacaoRendaFixaCollection1;
            this.calculatedFieldDataVencimentoTitulo.Expression = "[DataVencimentoTitulo]";
            this.calculatedFieldDataVencimentoTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
            this.calculatedFieldDataVencimentoTitulo.Name = "calculatedFieldDataVencimentoTitulo";
            // 
            // calculatedFieldDataEmissaoTitulo
            // 
            this.calculatedFieldDataEmissaoTitulo.DataSource = this.operacaoRendaFixaCollection1;
            this.calculatedFieldDataEmissaoTitulo.Expression = "[DataEmissao]";
            this.calculatedFieldDataEmissaoTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
            this.calculatedFieldDataEmissaoTitulo.Name = "calculatedFieldDataEmissaoTitulo";
            // 
            // calculatedFieldValorLiquido
            // 
            this.calculatedFieldValorLiquido.DataSource = this.operacaoRendaFixaCollection1;
            //this.calculatedFieldValorLiquido.Expression = "[Valor] - [ValorIR] - [ValorIOF]";
            this.calculatedFieldValorLiquido.Expression = "[ValorLiquido]";
            this.calculatedFieldValorLiquido.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldValorLiquido.Name = "calculatedFieldValorLiquido";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportOperacaoRendaFixaDefinitiva
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.GroupHeader1,
            this.PageFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldDescricaoTitulo,
            this.calculatedFieldIdCliente,
            this.calculatedFieldApelidoCliente,
            this.calculatedFieldDataVencimentoTitulo,
            this.calculatedFieldDataEmissaoTitulo,
            this.calculatedFieldValorLiquido});
            this.DataSource = this.operacaoRendaFixaCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportOperacaoRendaFixaDefinitiva.ResourceManager;
        }

        #region Funções Internas do Relatorio
        //               
        private void PercentualIndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell indiceXRTableCell = sender as XRTableCell;
            indiceXRTableCell.Text = "";
           
            if (this.numeroLinhasDataTable != 0) {
                int origem = Convert.ToInt32(this.GetCurrentColumnValue("Origem"));

                if (origem == (int)OrigemDados.OperacaoRF) {

                    string descricaoIndice = "";
                    if (!Convert.IsDBNull(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.Descricao))) {
                        descricaoIndice = Convert.ToString(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.Descricao));
                    }
                    decimal? percentual = null;
                    if (!Convert.IsDBNull(this.GetCurrentColumnValue(TituloRendaFixaMetadata.ColumnNames.Percentual))) {
                        percentual = Convert.ToDecimal(this.GetCurrentColumnValue(TituloRendaFixaMetadata.ColumnNames.Percentual));
                    }
                    //
                    if (percentual.HasValue && !String.IsNullOrEmpty(descricaoIndice)) {
                        indiceXRTableCell.Text = percentual.Value.ToString("n2") + "%" + " " + descricaoIndice;
                    }
                    else {
                        decimal percentualFixo = 1M; // Trata caso de MultiLingua
                        indiceXRTableCell.Text = percentualFixo.ToString("P2");
                    }
                }
                else {
                    indiceXRTableCell.Text = "";
                }
            }
        }

        private void TipoOperacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tipoOperacaoXRTableCell = sender as XRTableCell;
            tipoOperacaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {

                byte tipoOperacao = Convert.ToByte(this.GetCurrentColumnValue(OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao));
                //
                int origem = Convert.ToInt32(this.GetCurrentColumnValue("Origem"));
                //
                if (origem == (int)OrigemDados.OperacaoRF) {
                    if (tipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal) {
                        tipoOperacaoXRTableCell.Text = "Compra";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal) {
                        tipoOperacaoXRTableCell.Text = "Venda";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                    {
                        tipoOperacaoXRTableCell.Text = "Venda Total";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.Deposito)
                    {
                        tipoOperacaoXRTableCell.Text = "Depósito";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.Retirada)
                    {
                        tipoOperacaoXRTableCell.Text = "Retirada";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                    {
                        tipoOperacaoXRTableCell.Text = "Compra c/ Revenda";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRevenda)
                    {
                        tipoOperacaoXRTableCell.Text = "Antecipação Revenda";
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                    {
                        tipoOperacaoXRTableCell.Text = "Antecipação Recompra";
                    }
                }
                else { // Liquidacao RF
                    TipoLancamentoLiquidacao t = (TipoLancamentoLiquidacao)Enum.Parse(typeof(TipoLancamentoLiquidacao), tipoOperacao.ToString());
                    //
                    tipoOperacaoXRTableCell.Text = (string)StringEnum.GetStringValue(t);
                }
            }
        }

        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            #region Define a Ordenação do Relatorio
            if (this.tipoOrdenacao == ReportOperacaoRendaFixa.TipoOrdenacao.PorCodigo)
            {
                this.Detail.SortFields.Add(new GroupField(ClienteMetadata.ColumnNames.IdCliente, XRColumnSortOrder.Ascending));
                this.Detail.SortFields.Add(new GroupField(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao, XRColumnSortOrder.Ascending));
            }
            else if (this.tipoOrdenacao == ReportOperacaoRendaFixa.TipoOrdenacao.PorNome)
            {
                // Alias da Coluna do Join
                const string apelidoColumn = "ApelidoCliente";
                //
                this.Detail.SortFields.Add(new GroupField(apelidoColumn, XRColumnSortOrder.Ascending));
                this.Detail.SortFields.Add(new GroupField(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao, XRColumnSortOrder.Ascending));
            }
            #endregion
        }

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e)
        {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try
            {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1)
            {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }
        #endregion

        #endregion
    }
}