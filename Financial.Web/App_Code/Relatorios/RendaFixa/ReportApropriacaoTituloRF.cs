﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using log4net;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Common;
using Financial.Security;
using System.Web;
using Financial.Investidor.Enums;
using EntitySpaces.Core;
using System.Text;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportPosicaoRendaFixa
    /// </summary>
    public class ReportApropriacaoTituloRF : XtraReport
    {
        DataTable dt;

        private static readonly ILog log = LogManager.GetLogger(typeof(ReportApropriacaoTituloRF));

        private int? idCliente;

        public int? IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private int? idTitulo;

        public int? IdTitulo
        {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        private int? idPapel;

        public int? IdPapel
        {
            get { return idPapel; }
            set { idPapel = value; }
        }

        private int? idAssessor;

        public int? IdAssessor
        {
            get { return idAssessor; }
            set { idAssessor = value; }
        }

        private int? idGrupoAfinidade;

        public int? IdGrupoAfinidade
        {
            get { return idGrupoAfinidade; }
            set { idGrupoAfinidade = value; }
        }

        private byte? idCustodia;

        public byte? IdCustodia
        {
            get { return idCustodia; }
            set { idCustodia = value; }
        }

        private DateTime? data;

        public DateTime? Data
        {
            get { return data; }
            set { data = value; }
        }

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private PosicaoRendaFixaCollection posicaoRendaFixaCollection1;
        private PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection1;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell25;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport2;
        private SubReportLogotipo subReportLogotipo1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell9;
        private CalculatedField calculatedFieldIdTitulo;
        private CalculatedField calculatedFieldDescricaoTitulo;
        private CalculatedField calculatedFieldIdCliente;
        private CalculatedField calculatedFieldApelidoCliente;
        private CalculatedField calculatedFieldDataEmissaoTitulo;
        private CalculatedField calculatedFieldPUMercadoRecomposto;
        private CalculatedField calculatedFieldNomeEmissor;
        private CalculatedField calculatedFieldRendimento;
        private CalculatedField calculatedFieldValorLiquido;
        private XRControlStyle xrControlStyle1;
        private CalculatedField calculatedFieldPapelRendaFixa;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell26;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable4;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell46;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell58;
        private XRTable xrTable5;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private CalculatedField calculatedFieldAjusteMTM;
        private CalculatedField calculatedFieldValorBruto;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private CalculatedField calculatedTextHeader;
        private CalculatedField calculatedField1;
        private XRLabel xrLabel10;
        private XRLabel xrLabel11;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell19;
        private XRLabel xrLabel12;
        private XRLabel xrLabel13;
        private XRTableCell xrTableCell63;
        private XRLabel xrLabel1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell20;


        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoOrdenacao"></param>
        public ReportApropriacaoTituloRF(int? idCliente, int? idTitulo, int? idPapel, DateTime? data, int? idAssessor, int? idGrupoAfinidade, byte? idCustodia)
        {
            this.idCliente = idCliente;
            this.idTitulo = idTitulo;
            this.IdPapel = idPapel;
            this.data = data;
            this.idAssessor = idAssessor;
            this.idGrupoAfinidade = idGrupoAfinidade;
            this.idCustodia = idCustodia;

            //
            this.PersonalInitialize();
            this.InitializeComponent();

            #region Controla Visibilidade da DataPosicao no Cabeçalho
            xrTable5.Visible = this.data.HasValue;
            #endregion


            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2490);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (this.numeroLinhasDataTable == 0)
            {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //                
                this.GroupHeader1.Visible = false;
                this.GroupFooter1.Visible = false;
                //
                this.ReportFooter.Visible = false;
                //this.xrTable4.Visible = false;
            }
        }

        /// <summary>
        /// Preenche o DataSource do relatório
        /// </summary>
        private void PersonalInitialize()
        {
            dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
        }

        /// <summary>
        /// Consulta para obter os dados do Relatorio
        /// </summary>
        /// <returns></returns>
        private DataTable FillDados() 
        {
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                          clienteCollection.Query.TipoControle.NotIn((byte)TipoControleCliente.ApenasCotacao,
                                                                                     (byte)TipoControleCliente.Cotista,
                                                                                     (byte)TipoControleCliente.IRRendaVariavel));

            if (this.idCliente.HasValue)
            {
                clienteCollection.Query.Where(clienteCollection.Query.IdCliente.Equal(this.idCliente.Value));
            }
            
            clienteCollection.Query.Load();

            PosicaoRendaFixaCollection posicaoRendaFixaCollectionFinal = new PosicaoRendaFixaCollection();
            // Adiciona Colunas Extras proveniente de outra Tabela
            posicaoRendaFixaCollectionFinal.CreateColumnsForBinding();
            posicaoRendaFixaCollectionFinal.AddColumn("ApelidoCliente", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("DescricaoTitulo", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("Descricao", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("Percentual", typeof(System.Decimal));
            posicaoRendaFixaCollectionFinal.AddColumn("NomeEmissor", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("PapelRendaFixa", typeof(System.String));
            //
            posicaoRendaFixaCollectionFinal.AddColumn("DataEmissaoTitulo", typeof(System.DateTime));
            //
            posicaoRendaFixaCollectionFinal.AddColumn("PUMercadoRecomposto", typeof(System.Decimal));
            //
            posicaoRendaFixaCollectionFinal.AddColumn("AjusteMTM", typeof(System.Decimal));
            
            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            
            int i = 0;
            foreach (Cliente cliente in clienteCollection) {
                bool historico = this.data.HasValue && (this.data.Value < cliente.DataDia.Value);

                #region SQL
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("PH");
                ClienteQuery clienteQuery = new ClienteQuery("C");
                ClienteRendaFixaQuery clienteRendaFixaQuery = new ClienteRendaFixaQuery("CR");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                EmissorQuery emissorQuery = new EmissorQuery("E");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P1");
                IndiceQuery indiceQuery = new IndiceQuery("I");
                //
                PermissaoClienteQuery p = new PermissaoClienteQuery("Permissao");
                UsuarioQuery u = new UsuarioQuery("U");
                //
                if (historico)
                {
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                    #region Consulta em PosicaoRendaFixaHistorico
                    posicaoRendaFixaHistoricoQuery
                            .Select( 
                                    (clienteQuery.IdCliente.Cast(esCastType.String) + " - " + clienteQuery.Apelido).As("ApelidoCliente"),
                        //
                                    (tituloRendaFixaQuery.Descricao).As("DescricaoTitulo"),
                                    tituloRendaFixaQuery.DataEmissao.As("DataEmissaoTitulo"),
                                    tituloRendaFixaQuery.Percentual, 
                                    tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.DataVencimento.As("DataVencimentoTitulo"),
                        //
                                    emissorQuery.Nome.As("NomeEmissor"),
                        //
                                    indiceQuery.Descricao,
                        //
                                    papelRendaFixaQuery.Descricao.As("PapelRendaFixa"),
                        //
                                    posicaoRendaFixaHistoricoQuery.IdPosicao,
                                    posicaoRendaFixaHistoricoQuery.DataVencimento,
                                    posicaoRendaFixaHistoricoQuery.DataOperacao,
                                    posicaoRendaFixaHistoricoQuery.TaxaOperacao,
                                    posicaoRendaFixaHistoricoQuery.PUOperacao,
                                    posicaoRendaFixaHistoricoQuery.Quantidade,
                                    posicaoRendaFixaHistoricoQuery.PUMercado,
                                    posicaoRendaFixaHistoricoQuery.ValorMercado,
                                    posicaoRendaFixaHistoricoQuery.ValorIR,
                                    posicaoRendaFixaHistoricoQuery.ValorIOF,
                                    posicaoRendaFixaHistoricoQuery.PUCurva,
                                    posicaoRendaFixaHistoricoQuery.IdCliente);
                    //
                    posicaoRendaFixaHistoricoQuery.InnerJoin(clienteQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.LeftJoin(clienteRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
                    posicaoRendaFixaHistoricoQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
                    //
                    // Limita Somente quem Tem acesso
                    //
                    posicaoRendaFixaHistoricoQuery.InnerJoin(p).On(p.IdCliente == posicaoRendaFixaHistoricoQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
                    posicaoRendaFixaHistoricoQuery.Where(u.Login == HttpContext.Current.User.Identity.Name,
                                                         posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(this.data.Value),
                                                         posicaoRendaFixaHistoricoQuery.IdCliente.Equal(cliente.IdCliente.Value));

                    if (this.idTitulo.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdTitulo == this.idTitulo.Value);
                    }
                    if (this.idPapel.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(tituloRendaFixaQuery.IdPapel == this.idPapel.Value);
                    }
                    if (this.idAssessor.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(clienteRendaFixaQuery.IdAssessor == this.idAssessor.Value);
                    }
                    if (this.idGrupoAfinidade.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(clienteQuery.IdGrupoAfinidade == this.idGrupoAfinidade.Value);
                    }
                    if (this.idCustodia.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCustodia == this.idCustodia.Value);
                    }

                    posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery); // tem as colunas extras
                    #endregion

                    #region Adiciona Colunas Extras
                    PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                    //
                    posicaoRendaFixaCollectionAux.AddColumn("DescricaoCompleta", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("Descricao", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("Percentual", typeof(System.Decimal));
                    posicaoRendaFixaCollectionAux.AddColumn("ApelidoCliente", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("DescricaoTitulo", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("NomeEmissor", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("PapelRendaFixa", typeof(System.String));
                    posicaoRendaFixaCollectionAux.AddColumn("DataEmissaoTitulo", typeof(System.DateTime));
                    posicaoRendaFixaCollectionAux.AddColumn("PUMercadoRecomposto", typeof(System.Decimal));

                    //
                    for (int j = 0; j < posicaoRendaFixaHistoricoCollection.Count; j++) 
                    {
                        posicaoRendaFixaCollectionAux[j].SetColumn("DescricaoCompleta", posicaoRendaFixaHistoricoCollection[j].GetColumn("DescricaoCompleta"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("Descricao", posicaoRendaFixaHistoricoCollection[j].GetColumn("Descricao"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("Percentual", posicaoRendaFixaHistoricoCollection[j].GetColumn("Percentual"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("ApelidoCliente", posicaoRendaFixaHistoricoCollection[j].GetColumn("ApelidoCliente"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("DescricaoTitulo", posicaoRendaFixaHistoricoCollection[j].GetColumn("DescricaoTitulo"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("NomeEmissor", posicaoRendaFixaHistoricoCollection[j].GetColumn("NomeEmissor"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("PapelRendaFixa", posicaoRendaFixaHistoricoCollection[j].GetColumn("PapelRendaFixa"));
                        posicaoRendaFixaCollectionAux[j].SetColumn("DataEmissaoTitulo", posicaoRendaFixaHistoricoCollection[j].GetColumn("DataEmissaoTitulo"));
                        
                        decimal puMercado = Convert.ToDecimal(posicaoRendaFixaHistoricoCollection[j].GetColumn("PUMercado"));
                        int idTitulo = Convert.ToInt32(posicaoRendaFixaHistoricoCollection[j].GetColumn("IdTitulo"));
                        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                        tituloRendaFixa.LoadByPrimaryKey(idTitulo);
                        decimal puMercadoRecomposto = puMercado + agendaRendaFixa.RetornaTotalAmortizadoPU(tituloRendaFixa, this.data.Value, posicaoRendaFixaCollectionAux[j].DataOperacao.Value);
                        posicaoRendaFixaCollectionAux[j].SetColumn("PUMercadoRecomposto", puMercadoRecomposto);
                    }
                    #endregion

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollectionAux) {
                        posicaoRendaFixaCollectionFinal.AttachEntity(posicaoRendaFixa);
                    }
                }
                else
                {
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    #region Consulta em PosicaoRendaFixa
                    posicaoRendaFixaQuery
                            .Select(
                                    (clienteQuery.IdCliente.Cast(esCastType.String) + " - " + clienteQuery.Apelido).As("ApelidoCliente"),                                    
                        //
                                    (tituloRendaFixaQuery.Descricao).As("DescricaoTitulo"),
                                    tituloRendaFixaQuery.DataEmissao.As("DataEmissaoTitulo"),
                                    tituloRendaFixaQuery.Percentual, 
                                    tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.DataVencimento.As("DataVencimentoTitulo"),
                        //
                                    emissorQuery.Nome.As("NomeEmissor"),
                        //
                                    indiceQuery.Descricao,
                        //
                                    papelRendaFixaQuery.Descricao.As("PapelRendaFixa"),
                        //
                                    posicaoRendaFixaQuery.IdPosicao,
                                    posicaoRendaFixaQuery.DataVencimento,
                                    posicaoRendaFixaQuery.DataOperacao,
                                    posicaoRendaFixaQuery.TaxaOperacao,
                                    posicaoRendaFixaQuery.PUOperacao,
                                    posicaoRendaFixaQuery.Quantidade,
                                    posicaoRendaFixaQuery.PUMercado,
                                    posicaoRendaFixaQuery.ValorMercado,
                                    posicaoRendaFixaQuery.ValorIR,
                                    posicaoRendaFixaQuery.PUCurva,
                                    posicaoRendaFixaQuery.ValorIOF,
                                    posicaoRendaFixaQuery.PUCurva,
                                    posicaoRendaFixaQuery.IdCliente);
                    //
                    posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(posicaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
                    posicaoRendaFixaQuery.LeftJoin(clienteRendaFixaQuery).On(posicaoRendaFixaQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
                    posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
                    posicaoRendaFixaQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
                    //
                    // Limita Somente quem Tem acesso
                    //
                    posicaoRendaFixaQuery.InnerJoin(p).On(p.IdCliente == posicaoRendaFixaQuery.IdCliente);
                    posicaoRendaFixaQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
                    posicaoRendaFixaQuery.Where(u.Login == HttpContext.Current.User.Identity.Name,
                                                posicaoRendaFixaQuery.IdCliente.Equal(cliente.IdCliente.Value));

                    if (this.idTitulo.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdTitulo == this.idTitulo.Value);
                    }
                    if (this.idPapel.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(tituloRendaFixaQuery.IdPapel == this.idPapel.Value);
                    }
                    if (this.idAssessor.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(clienteRendaFixaQuery.IdAssessor == this.idAssessor.Value);
                    }
                    if (this.idGrupoAfinidade.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(clienteQuery.IdGrupoAfinidade == this.idGrupoAfinidade.Value);
                    }
                    if (this.idCustodia.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaQuery.IdCustodia == this.idCustodia.Value);
                    }

                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
                    #endregion

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection) {
                        posicaoRendaFixaCollectionFinal.AttachEntity(posicaoRendaFixa);
                    }
                }
                #endregion                

                i++;
            }

            return posicaoRendaFixaCollectionFinal.LowLevelBind().ToTable("esUtility");
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/RendaFixa/ReportPosicaoRendaFixa.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportApropriacaoTituloRF.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.posicaoRendaFixaCollection1 = new Financial.RendaFixa.PosicaoRendaFixaCollection();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.posicaoRendaFixaHistoricoCollection1 = new Financial.RendaFixa.PosicaoRendaFixaHistoricoCollection();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.calculatedFieldIdTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldDescricaoTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldIdCliente = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldApelidoCliente = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldDataEmissaoTitulo = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldPUMercadoRecomposto = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldNomeEmissor = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldRendimento = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldValorLiquido = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.calculatedFieldPapelRendaFixa = new DevExpress.XtraReports.UI.CalculatedField();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.calculatedFieldAjusteMTM = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldValorBruto = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedTextHeader = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedField1 = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 58.42F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorBruto", "{0:###,##0.00}")});
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(2326.51F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(267.49F, 46.00002F);
            this.xrLabel11.Text = "xrLabel11";
            // 
            // posicaoRendaFixaCollection1
            // 
            this.posicaoRendaFixaCollection1.AllowDelete = true;
            this.posicaoRendaFixaCollection1.AllowEdit = true;
            this.posicaoRendaFixaCollection1.AllowNew = true;
            this.posicaoRendaFixaCollection1.EnableHierarchicalBinding = true;
            this.posicaoRendaFixaCollection1.Filter = "";
            this.posicaoRendaFixaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoRendaFixaCollection1.Sort = "";
            // 
            // xrLabel10
            // 
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido", "{0:###,##0.00}")});
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(2025.547F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(300.9631F, 46.00002F);
            this.xrLabel10.Text = "xrLabel10";
            // 
            // xrLabel9
            // 
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:###,##0.00}")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1219.512F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(209.9651F, 46.00002F);
            this.xrLabel9.Text = "xrLabel9";
            // 
            // xrLabel8
            // 
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DataVencimento", "{0:dd/MM/yyyy}")});
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(1031.595F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(187.9169F, 46.00002F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DataOperacao", "{0:dd/MM/yyyy}")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(804.386F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(220.7614F, 46.00002F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.NomeEmissor")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(363.54F, 0F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(440.846F, 46.00002F);
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "xrLabel6";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DescricaoTitulo")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(104F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(259.54F, 46.00002F);
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "xrLabel5";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF", "{0:###,##0.00}")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1858.377F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(167.1691F, 46.00002F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR", "{0:###,##0.00}")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1679.808F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(178.5076F, 46.00002F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldAjusteMTM", "{0:###,##0.00}")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1429.478F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(250.3306F, 46.00002F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // posicaoRendaFixaHistoricoCollection1
            // 
            this.posicaoRendaFixaHistoricoCollection1.AllowDelete = true;
            this.posicaoRendaFixaHistoricoCollection1.AllowEdit = true;
            this.posicaoRendaFixaHistoricoCollection1.AllowNew = true;
            this.posicaoRendaFixaHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoRendaFixaHistoricoCollection1.Filter = "";
            this.posicaoRendaFixaHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoRendaFixaHistoricoCollection1.Sort = "";
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrPageInfo2,
            this.xrTable8,
            this.xrTable5});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(104F, 190F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(2400F, 41.99998F);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(2267F, 1.999959F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:d}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(2119F, 1.999959F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(1903.958F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.CanGrow = false;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "Data Emissão:";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell25.Weight = 1D;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable5.SizeF = new System.Drawing.SizeF(516.2709F, 42F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell60});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.CanGrow = false;
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.Text = "Data Posição:";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell59.Weight = 0.43049348585190839D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UsePadding = false;
            this.xrTableCell60.Text = "#DataPosicao";
            this.xrTableCell60.Weight = 0.61786085485195519D;
            this.xrTableCell60.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrTable3,
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 238.3542F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(64F, 212F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrSubreport3.Visible = false;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1693F, 64F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.15002953337271116D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "Apropriação de Titulos de Renda Fixa";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.84997046662728881D;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(768F, 142F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2519F, 190F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(63F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 102F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(645F, 100F);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrTable6});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 110.25F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(104F, 48F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell58,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell9,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell61,
            this.xrTableCell62});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "Ativo";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell10.Weight = 0.17537684536424752D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell58.Weight = 0.0034011709494987663D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Emitente";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell11.Weight = 0.30362331393591535D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Data Compra";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell12.Weight = 0.15650776229230759D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Vencimento";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell13.Weight = 0.12944191787722303D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Quantidade";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell14.Weight = 0.14462939097994043D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Ajuste MTM";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.17243410540805984D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "IR";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.12296079823738473D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "IOF";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.11519351264321186D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Valor Liquido";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell61.Weight = 0.20731164978529387D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "Valor Bruto";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell62.Weight = 0.18429703942952752D;
                        // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(104F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell20});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "Cliente:";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell17.Weight = 0.17882097690421261D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell20.Weight = 1.5363565299983979D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ApelidoCliente")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(2230F, 47.99998F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "[calculatedFieldApelidoCliente]";
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 79.3125F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.RepeatEveryPage = true;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(99.60236F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2494F, 48F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell22,
            this.xrTableCell26,
            this.xrTableCell41,
            this.xrTableCell3,
            this.xrTableCell19});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Total p/ Grupo:";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.53408643117391463D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.AjusteMTM", "{0:###,##0.00}")});
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell22.Summary = xrSummary1;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.095551051863704842D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:###,##0.00}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell26.Summary = xrSummary2;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.07667270645080318D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell41.Summary = xrSummary3;
            this.xrTableCell41.Text = "xrTableCell41";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.062153398751255D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:###,##0.00}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell3.Summary = xrSummary4;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.12587679820845885D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorBruto")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:###,##0.00}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell19.Summary = xrSummary5;
            this.xrTableCell19.Text = "xrTableCell19";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.10726603925467496D;
            // 
            // calculatedFieldIdTitulo
            // 
            this.calculatedFieldIdTitulo.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldIdTitulo.Expression = "[IdTitulo]";
            this.calculatedFieldIdTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.Int32;
            this.calculatedFieldIdTitulo.Name = "calculatedFieldIdTitulo";
            // 
            // calculatedFieldDescricaoTitulo
            // 
            this.calculatedFieldDescricaoTitulo.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldDescricaoTitulo.Expression = "[DescricaoTitulo]";
            this.calculatedFieldDescricaoTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldDescricaoTitulo.Name = "calculatedFieldDescricaoTitulo";
            // 
            // calculatedFieldIdCliente
            // 
            this.calculatedFieldIdCliente.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldIdCliente.Expression = "[IdCliente]";
            this.calculatedFieldIdCliente.FieldType = DevExpress.XtraReports.UI.FieldType.Int32;
            this.calculatedFieldIdCliente.Name = "calculatedFieldIdCliente";
            // 
            // calculatedFieldApelidoCliente
            // 
            this.calculatedFieldApelidoCliente.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldApelidoCliente.Expression = "[ApelidoCliente]";
            this.calculatedFieldApelidoCliente.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldApelidoCliente.Name = "calculatedFieldApelidoCliente";
            // 
            // calculatedFieldDataEmissaoTitulo
            // 
            this.calculatedFieldDataEmissaoTitulo.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldDataEmissaoTitulo.Expression = "[DataEmissaoTitulo]";
            this.calculatedFieldDataEmissaoTitulo.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
            this.calculatedFieldDataEmissaoTitulo.Name = "calculatedFieldDataEmissaoTitulo";
            // 
            // calculatedFieldPUMercadoRecomposto
            // 
            this.calculatedFieldPUMercadoRecomposto.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldPUMercadoRecomposto.Expression = "[PUMercadoRecomposto]";
            this.calculatedFieldPUMercadoRecomposto.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldPUMercadoRecomposto.Name = "calculatedFieldPUMercadoRecomposto";
            // 
            // calculatedFieldNomeEmissor
            // 
            this.calculatedFieldNomeEmissor.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldNomeEmissor.Expression = "[NomeEmissor]";
            this.calculatedFieldNomeEmissor.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldNomeEmissor.Name = "calculatedFieldNomeEmissor";
            // 
            // calculatedFieldRendimento
            // 
            this.calculatedFieldRendimento.DataSource = dt;
            this.calculatedFieldRendimento.Expression = "([PUMercadoRecomposto] - [PUOperacao]) * [Quantidade]";
            this.calculatedFieldRendimento.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldRendimento.Name = "calculatedFieldRendimento";
            // 
            // calculatedFieldValorLiquido
            // 
            this.calculatedFieldValorLiquido.DataSource = dt;
            this.calculatedFieldValorLiquido.Expression = "[ValorMercado] - [ValorIR] - [ValorIOF]";
            this.calculatedFieldValorLiquido.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldValorLiquido.Name = "calculatedFieldValorLiquido";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // calculatedFieldPapelRendaFixa
            // 
            this.calculatedFieldPapelRendaFixa.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedFieldPapelRendaFixa.Expression = "[PapelRendaFixa]";
            this.calculatedFieldPapelRendaFixa.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldPapelRendaFixa.Name = "calculatedFieldPapelRendaFixa";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.xrLabel12,
            this.xrTable4});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 55.43751F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel13
            // 
            this.xrLabel13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorBruto")});
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(2340.144F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(253.856F, 47.99998F);
            this.xrLabel13.StylePriority.UseBackColor = false;
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:###,##0.00}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel13.Summary = xrSummary6;
            this.xrLabel13.Text = "xrLabel13";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido")});
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(2013.077F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(313.4331F, 47.99998F);
            this.xrLabel12.StylePriority.UseBackColor = false;
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:###,##0.00}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrLabel12.Summary = xrSummary7;
            this.xrLabel12.Text = "xrLabel12";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1913.077F, 48F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell46});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Total Geral:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.53392635195840843D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.AjusteMTM")});
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:###,##0.00}";
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell7.Summary = xrSummary8;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.10053434563454015D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            xrSummary9.FormatString = "{0:###,##0.00}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell8.Summary = xrSummary9;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.071689676185184487D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            xrSummary10.FormatString = "{0:###,##0.00}";
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell46.Summary = xrSummary10;
            this.xrTableCell46.Text = "xrTableCell46";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.062153460958277409D;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 232F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 151.1875F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // calculatedFieldAjusteMTM
            // 
            this.calculatedFieldAjusteMTM.DataSource = dt;
            this.calculatedFieldAjusteMTM.DisplayName = "calculatedFieldAjusteMTM";
            this.calculatedFieldAjusteMTM.Expression = "([PUMercado] - [PUCurva]) * [Quantidade]";
            this.calculatedFieldAjusteMTM.Name = "calculatedFieldAjusteMTM";
            // 
            // calculatedFieldValorBruto
            // 
            this.calculatedFieldValorBruto.DataSource = dt;
            this.calculatedFieldValorBruto.DisplayName = "calculatedFieldValorBruto";
            this.calculatedFieldValorBruto.Expression = "[PUMercado] * [Quantidade]";
            this.calculatedFieldValorBruto.Name = "calculatedFieldValorBruto";
            // 
            // calculatedTextHeader
            // 
            this.calculatedTextHeader.DataSource = this.posicaoRendaFixaCollection1;
            this.calculatedTextHeader.Expression = "\'Codigo Cliente:\'";
            this.calculatedTextHeader.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedTextHeader.Name = "calculatedTextHeader";
            // 
            // calculatedField1
            // 
            this.calculatedField1.Name = "calculatedField1";
            // 
            
            // ReportApropriacaoTituloRF
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupFooter1,
            this.PageFooter,
            this.PageHeader,
            this.topMarginBand1,
            this.Detail,
            this.GroupHeader1,
            this.ReportFooter,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldIdTitulo,
            this.calculatedFieldDescricaoTitulo,
            this.calculatedFieldIdCliente,
            this.calculatedFieldApelidoCliente,
            this.calculatedFieldDataEmissaoTitulo,
            this.calculatedFieldNomeEmissor,
            this.calculatedFieldRendimento,
            this.calculatedFieldValorLiquido,
            this.calculatedFieldPapelRendaFixa,
            this.calculatedFieldAjusteMTM,
            this.calculatedFieldValorBruto,
            this.calculatedTextHeader,
            this.calculatedField1});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 232, 151);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "15.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.ReportApropriacaoTituloRF.ResourceManager;
        }

        #region Funções Internas do Relatorio
        private void PercentualIndiceBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell indiceXRTableCell = sender as XRTableCell;
            indiceXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0)
            {
                string descricaoIndice = "";
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.Descricao)))
                {
                    descricaoIndice = Convert.ToString(this.GetCurrentColumnValue(IndiceMetadata.ColumnNames.Descricao));
                }
                decimal? percentual = null;
                if (!Convert.IsDBNull(this.GetCurrentColumnValue(TituloRendaFixaMetadata.ColumnNames.Percentual)))
                {
                    percentual = Convert.ToDecimal(this.GetCurrentColumnValue(TituloRendaFixaMetadata.ColumnNames.Percentual));
                }
                //
                if (percentual.HasValue && !String.IsNullOrEmpty(descricaoIndice))
                {
                    indiceXRTableCell.Text = percentual.Value.ToString("n2") + "%" + " " + descricaoIndice;
                }
                else if (!String.IsNullOrEmpty(descricaoIndice))
                {
                    indiceXRTableCell.Text = "100% " + descricaoIndice;
                }
                else
                {
                    indiceXRTableCell.Text = "";
                }
            }
        }

        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoTableCell = sender as XRTableCell;
            if (this.data.HasValue) {
                dataPosicaoTableCell.Text = this.data.Value.ToString("d");
            }
        }
        #endregion
    }
}