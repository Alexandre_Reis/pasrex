﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using log4net;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Common;
using Financial.Security;
using System.Web;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.RendaFixa.Enums;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportPosicaoLiquidaRF
    /// </summary>
    public class ReportPosicaoLiquidaRF : XtraReport
    {
        DataTable dt;

        private static readonly ILog log = LogManager.GetLogger(typeof(ReportPosicaoLiquidaRF));

        private int? idCliente;

        public int? IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private int? idTitulo;

        public int? IdTitulo
        {
            get { return idTitulo; }
            set { idTitulo = value; }
        }


        private int? tipoPessoa;

        public int? TipoPessoa
        {
            get { return tipoPessoa; }
            set { tipoPessoa = value; }
        }

        private int? idClasse;

        public int? IdClasse
        {
            get { return idClasse; }
            set { idClasse = value; }
        }

        private int? idAssessor;

        public int? IdAssessor
        {
            get { return idAssessor; }
            set { idAssessor = value; }
        }

        private int? idGrupoAfinidade;

        public int? IdGrupoAfinidade
        {
            get { return idGrupoAfinidade; }
            set { idGrupoAfinidade = value; }
        }

        private byte? idCustodia;

        public byte? IdCustodia
        {
            get { return idCustodia; }
            set { idCustodia = value; }
        }

        private DateTime? data;

        public DateTime? Data
        {
            get { return data; }
            set { data = value; }
        }

        private int numeroLinhasDataTable;

        /* Baseado no idClasse Define quais Classes de papel vão ser usadas */
        private List<int> classesPapel = new List<int>();

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private PosicaoRendaFixaCollection posicaoRendaFixaCollection1;
        private PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection1;
        private XRPageInfo xrPageInfo1;
        private XRSubreport xrSubreport2;
        private SubReportLogotipo subReportLogotipo1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private GroupHeaderBand GroupHeader1;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTable xrTable6;
        private XRTableRow xrTableRow9;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell9;
        private CalculatedField calculatedFieldAgencia;
        private CalculatedField calculatedFieldPUMercadoRecomposto;
        private CalculatedField calculatedFieldContaCorrente;
        private CalculatedField calculatedFieldClearing;
        private XRControlStyle xrControlStyle1;
        private XRTableCell xrTableCell3;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private CalculatedField calculatedFieldBanco;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell22;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable4;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell46;
        private XRSubreport xrSubreport3;

        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell21;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRPageInfo xrPageInfo2;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell25;
        private CalculatedField calculatedFieldCPFCNPJ;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell45;
        private CalculatedField calculatedFieldValorLiquido;
        private ReportSemDados reportSemDados1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Somente serão usadas essas classes de Papel
        /// </summary>
        /// <returns></returns>
        /// 
        private void DefineClassesPapelValidas()
        {
            this.classesPapel = new List<int>(new int[] {
                                            (int)ClasseRendaFixa.LFT, (int)ClasseRendaFixa.LTN,
                                            (int)ClasseRendaFixa.NTN, (int)ClasseRendaFixa.CCB,
                                            (int)ClasseRendaFixa.CDB, (int)ClasseRendaFixa.CRI, (int)ClasseRendaFixa.CCI,
                                            (int)ClasseRendaFixa.LCI, (int)ClasseRendaFixa.LH,
                                            (int)ClasseRendaFixa.DPGE, (int)ClasseRendaFixa.LC,
                                            (int)ClasseRendaFixa.LF, (int)ClasseRendaFixa.Debenture,
                                            (int)ClasseRendaFixa.CDA, (int)ClasseRendaFixa.CDCA,
                                            (int)ClasseRendaFixa.CPR, (int)ClasseRendaFixa.CRA,
                                            (int)ClasseRendaFixa.LCA }
                                           );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="idPapel"></param>
        /// <param name="data"></param>
        /// <param name="idAssessor"></param>
        /// <param name="idGrupoAfinidade"></param>
        /// <param name="idCustodia"></param>
        public ReportPosicaoLiquidaRF(int? idCliente, int? idTitulo, int? idClasse, DateTime? data, int? idAssessor, int? idGrupoAfinidade, byte? idCustodia, int? tipoPessoa)
        {
            this.idCliente = idCliente;
            this.idTitulo = idTitulo;
            this.idClasse = idClasse;

            // Baseado no idClasse define quais lista de classes vai ser usada
            if (!this.idClasse.HasValue)
            {
                // altera this.classesPapel
                this.DefineClassesPapelValidas(); // Todas as classes dentro desse grupo
            }
            else
            {
                this.classesPapel.Add(this.idClasse.Value);
            }

            this.data = data;
            this.idAssessor = idAssessor;
            this.idGrupoAfinidade = idGrupoAfinidade;
            this.idCustodia = idCustodia;
            this.tipoPessoa = tipoPessoa;
            //
            this.PersonalInitialize();
            this.InitializeComponent();
            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2490);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (this.numeroLinhasDataTable == 0)
            {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //                
                this.GroupHeader1.Visible = false;
                this.GroupFooter1.Visible = false;
                //
                this.ReportFooter.Visible = false;
                //this.xrTable4.Visible = false;
            }
        }

        /// <summary>
        /// Preenche o DataSource do relatório
        /// </summary>
        private void PersonalInitialize()
        {
            dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
        }

        /// <summary>
        /// Consulta para obter os dados do Relatorio
        /// </summary>
        /// <returns></returns>
        private DataTable FillDados()
        {
            #region Cliente
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (this.idCliente.HasValue)
            {
                clienteCollection.Query.Where(clienteCollection.Query.IdCliente.Equal(this.idCliente.Value));
            }

            clienteCollection.Query.Load();
            #endregion

            PosicaoRendaFixaCollection posicaoRendaFixaCollectionFinal = new PosicaoRendaFixaCollection();
            // Adiciona Colunas Extras proveniente de outra Tabela
            posicaoRendaFixaCollectionFinal.CreateColumnsForBinding();
            posicaoRendaFixaCollectionFinal.AddColumn("CPF_CNPJ", typeof(System.String));
            //
            posicaoRendaFixaCollectionFinal.AddColumn("Classe", typeof(System.Int32));
            posicaoRendaFixaCollectionFinal.AddColumn("DescricaoClasse", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("Banco", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("Agencia", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("ContaCorrente", typeof(System.String));
            posicaoRendaFixaCollectionFinal.AddColumn("Clearing", typeof(System.String));

            foreach (Cliente cliente in clienteCollection)
            {
                bool historico = this.data.HasValue && (this.data.Value < cliente.DataDia.Value);

                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("PH");

                ClienteQuery clienteQuery = new ClienteQuery("C");
                ClienteRendaFixaQuery clienteRendaFixaQuery = new ClienteRendaFixaQuery("CR");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P1");
                PessoaQuery pessoaQuery = new PessoaQuery("P2");
                //
                PermissaoClienteQuery p = new PermissaoClienteQuery("Permissao");
                UsuarioQuery u = new UsuarioQuery("U");
                //
                if (historico)
                {
                    #region Historico

                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                    #region Consulta em PosicaoRendaFixaHistorico
                    posicaoRendaFixaHistoricoQuery
                            .Select(posicaoRendaFixaHistoricoQuery.IdCliente,
                                    posicaoRendaFixaHistoricoQuery.IdCustodia,
                                    posicaoRendaFixaHistoricoQuery.Quantidade,
                                    posicaoRendaFixaHistoricoQuery.ValorIR,
                                    posicaoRendaFixaHistoricoQuery.ValorIOF,
                                    posicaoRendaFixaHistoricoQuery.ValorMercado,
                        //
                                    papelRendaFixaQuery.Classe);
                    //
                    posicaoRendaFixaHistoricoQuery.InnerJoin(clienteQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.LeftJoin(clienteRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(pessoaQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
                    //
                    // Limita Somente quem Tem acesso
                    //
                    posicaoRendaFixaHistoricoQuery.InnerJoin(p).On(p.IdCliente == posicaoRendaFixaHistoricoQuery.IdCliente);
                    posicaoRendaFixaHistoricoQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
                    posicaoRendaFixaHistoricoQuery.Where(u.Login == HttpContext.Current.User.Identity.Name,
                                                         posicaoRendaFixaHistoricoQuery.DataHistorico == this.data.Value,
                                                         posicaoRendaFixaHistoricoQuery.IdCliente == cliente.IdCliente.Value);

                    posicaoRendaFixaHistoricoQuery.Where(papelRendaFixaQuery.Classe.In(this.classesPapel));

                    if (this.idTitulo.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdTitulo == this.idTitulo.Value);
                    }
                    if (this.idAssessor.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(clienteRendaFixaQuery.IdAssessor == this.idAssessor.Value);
                    }
                    if (this.idGrupoAfinidade.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(clienteQuery.IdGrupoAfinidade == this.idGrupoAfinidade.Value);
                    }
                    if (this.idCustodia.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCustodia == this.idCustodia.Value);
                    }
                    if (this.tipoPessoa.HasValue)
                    {
                        posicaoRendaFixaHistoricoQuery.Where(pessoaQuery.Tipo == this.tipoPessoa.Value);
                    }

                    posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery); // tem as colunas extras
                    #endregion

                    PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);

                    //
                    posicaoRendaFixaCollectionAux.CreateColumnsForBinding();
                    posicaoRendaFixaCollectionAux.AddColumn("Classe", typeof(System.Int32));

                    // Copia Coluna Extra: Classe do Papel
                    for (int i = 0; i < posicaoRendaFixaHistoricoCollection.Count; i++)
                    {
                        posicaoRendaFixaCollectionAux[i].SetColumn("Classe", posicaoRendaFixaHistoricoCollection[i].GetColumn("Classe"));
                    }

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollectionAux)
                    {
                        posicaoRendaFixaCollectionFinal.AttachEntity(posicaoRendaFixa);
                    }
                    #endregion
                }
                else
                {
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    #region Consulta em PosicaoRendaFixa
                    posicaoRendaFixaQuery
                            .Select(posicaoRendaFixaQuery.IdCliente,
                                    posicaoRendaFixaQuery.IdCustodia,
                                    posicaoRendaFixaQuery.Quantidade,
                                    posicaoRendaFixaQuery.ValorIR,
                                    posicaoRendaFixaQuery.ValorIOF,
                                    posicaoRendaFixaQuery.ValorMercado,
                        //
                                    papelRendaFixaQuery.Classe);
                    //
                    posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(posicaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
                    posicaoRendaFixaQuery.LeftJoin(clienteRendaFixaQuery).On(posicaoRendaFixaQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
                    posicaoRendaFixaQuery.InnerJoin(pessoaQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
                    //
                    // Limita Somente quem Tem acesso
                    //
                    posicaoRendaFixaQuery.InnerJoin(p).On(p.IdCliente == posicaoRendaFixaQuery.IdCliente);
                    posicaoRendaFixaQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
                    posicaoRendaFixaQuery.Where(u.Login == HttpContext.Current.User.Identity.Name,
                                                posicaoRendaFixaQuery.IdCliente == cliente.IdCliente.Value);

                    posicaoRendaFixaQuery.Where(papelRendaFixaQuery.Classe.In(this.classesPapel));

                    if (this.idTitulo.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdTitulo == this.idTitulo.Value);
                    }
                    if (this.idAssessor.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(clienteRendaFixaQuery.IdAssessor == this.idAssessor.Value);
                    }
                    if (this.idGrupoAfinidade.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(clienteQuery.IdGrupoAfinidade == this.idGrupoAfinidade.Value);
                    }
                    if (this.idCustodia.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCustodia == this.idCustodia.Value);
                    }
                    if (this.tipoPessoa.HasValue)
                    {
                        posicaoRendaFixaQuery.Where(pessoaQuery.Tipo == this.tipoPessoa.Value);
                    }

                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
                    //
                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        posicaoRendaFixaCollectionFinal.AttachEntity(posicaoRendaFixa);
                    }
                    #endregion
                }
            }

            #region Adiciona ContaCorrente/Agencia/Banco/TipoPessoa/CPF_CNPJ
            for (int i = 0; i < posicaoRendaFixaCollectionFinal.Count; i++)
            {

                #region CNPJ
                string cpfcnpj = "";
                //
                Pessoa pessoa = new Pessoa();
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(posicaoRendaFixaCollectionFinal[i].IdCliente.Value);
                pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

                if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj))
                {
                    cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
                }
                posicaoRendaFixaCollectionFinal[i].SetColumn("CPF_CNPJ", cpfcnpj);
                #endregion

                ContaCorrenteCollection c = new ContaCorrenteCollection();
                //
                c.Query.Select(c.Query.IdConta, c.Query.IdAgencia, c.Query.IdBanco, c.Query.Numero)
                       .Where(c.Query.IdPessoa == cliente.IdPessoa.Value,
                              c.Query.ContaDefault == "S");

                if (c.Query.Load())
                {
                    posicaoRendaFixaCollectionFinal[i].SetColumn("ContaCorrente", c[0].str.Numero);

                    int? idAgencia = c[0].IdAgencia;
                    int? idBanco = c[0].IdBanco;

                    if (idAgencia.HasValue)
                    {
                        Agencia agencia = new Agencia();
                        if (agencia.LoadByPrimaryKey(idAgencia.Value))
                        {
                            posicaoRendaFixaCollectionFinal[i].SetColumn("Agencia", agencia.str.Codigo.Trim());
                        }
                    }

                    if (idBanco.HasValue)
                    {
                        Banco banco = new Banco();
                        if (banco.LoadByPrimaryKey(idBanco.Value))
                        {
                            posicaoRendaFixaCollectionFinal[i].SetColumn("Banco", banco.str.CodigoCompensacao.Trim());
                        }
                    }
                }

                // Clearing
                string clearing;

                try
                {
                    clearing = StringEnum.GetStringValue((LocalCustodiaFixo)posicaoRendaFixaCollectionFinal[i].IdCustodia.Value);
                }
                catch
                {
                    clearing = string.Empty;
                }
                //
                posicaoRendaFixaCollectionFinal[i].SetColumn("Clearing", clearing);

                //
                //
                // Baseado na Classe int define a Descricao Da Classe
                // TODO: fazer pelo Enum RendaFixa
                //string descricaoClasse = "";
                //switch ( Convert.ToInt32( posicaoRendaFixaCollectionFinal[i].GetColumn("DescricaoClasse") ) ) {
                //    case (int)ClasseRendaFixa.LFT: descricaoClasse = "LFT"; break;
                //    case (int)ClasseRendaFixa.LTN: descricaoClasse = "LTN"; break;
                //    case (int)ClasseRendaFixa.NTN: descricaoClasse = "NTN"; break;
                //    case (int)ClasseRendaFixa.CCB: descricaoClasse = "CCB"; break;
                //    case (int)ClasseRendaFixa.CDB: descricaoClasse = "CDB"; break;
                //    case (int)ClasseRendaFixa.CRI_CCI: descricaoClasse = "CRI"; break;
                //    case (int)ClasseRendaFixa.CRI_CCI_Cetip: descricaoClasse = "CRI"; break;
                //    case (int)ClasseRendaFixa.LCI: descricaoClasse = "LCI"; break;
                //    case (int)ClasseRendaFixa.LH: descricaoClasse = "LH"; break;
                //    case (int)ClasseRendaFixa.DPGE: descricaoClasse = "DPGE"; break;
                //    case (int)ClasseRendaFixa.LC: descricaoClasse = "LC"; break;
                //    case (int)ClasseRendaFixa.LF: descricaoClasse = "LF"; break;
                //    case (int)ClasseRendaFixa.Debenture: descricaoClasse = "Debênture"; break;
                //}

                string descricaoClasse;

                try
                {
                    descricaoClasse = StringEnum.GetStringValue((ClasseRendaFixa)Convert.ToInt32(posicaoRendaFixaCollectionFinal[i].GetColumn("Classe")));
                }
                catch
                {
                    descricaoClasse = string.Empty;
                }

                posicaoRendaFixaCollectionFinal[i].SetColumn("DescricaoClasse", descricaoClasse);
            }
            #endregion

            return posicaoRendaFixaCollectionFinal.LowLevelBind().ToTable("esUtility");
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/RendaFixa/ReportPosicaoLiquidaRF.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportPosicaoLiquidaRF.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.posicaoRendaFixaCollection1 = new Financial.RendaFixa.PosicaoRendaFixaCollection();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.posicaoRendaFixaHistoricoCollection1 = new Financial.RendaFixa.PosicaoRendaFixaHistoricoCollection();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.calculatedFieldAgencia = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldPUMercadoRecomposto = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldContaCorrente = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldClearing = new DevExpress.XtraReports.UI.CalculatedField();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.calculatedFieldBanco = new DevExpress.XtraReports.UI.CalculatedField();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.calculatedFieldCPFCNPJ = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldValorLiquido = new DevExpress.XtraReports.UI.CalculatedField();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 50.37499F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("IdTitulo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.EvenStyleName = "xrControlStyle1";
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2490F, 46F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell20,
            this.xrTableCell40,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell26,
            this.xrTableCell44,
            this.xrTableCell36,
            this.xrTableCell28,
            this.xrTableCell27,
            this.xrTableCell29});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CPFCNPJ")});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.10143068612339982;
            this.xrTableCell3.WordWrap = false;
            // 
            // posicaoRendaFixaCollection1
            // 
            this.posicaoRendaFixaCollection1.AllowDelete = true;
            this.posicaoRendaFixaCollection1.AllowEdit = true;
            this.posicaoRendaFixaCollection1.AllowNew = true;
            this.posicaoRendaFixaCollection1.EnableHierarchicalBinding = true;
            this.posicaoRendaFixaCollection1.Filter = "";
            this.posicaoRendaFixaCollection1.RowStateFilter = ((System.Data.DataViewRowState)(((System.Data.DataViewRowState.Unchanged | System.Data.DataViewRowState.Added)
                        | System.Data.DataViewRowState.ModifiedCurrent)));
            this.posicaoRendaFixaCollection1.Sort = "";
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.CanGrow = false;
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.IdCliente")});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "xrTableCell12";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell20.Weight = 0.098318260836314064;
            this.xrTableCell20.WordWrap = false;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Clearing")});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UsePadding = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.0804550722420934;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:n2}")});
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "xrTableCell23";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.12339372136985438;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado", "{0:n2}")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "xrTableCell24";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.11356676706827304;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.0912524579519249;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.090319631185876326;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido", "{0:n2}")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.12644753819967369;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.CanGrow = false;
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Banco")});
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.050962051330321251;
            this.xrTableCell28.WordWrap = false;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.CanGrow = false;
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Agencia")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.059939660987700816;
            this.xrTableCell27.WordWrap = false;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.CanGrow = false;
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ContaCorrente")});
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.063914152704568281;
            this.xrTableCell29.WordWrap = false;
            // 
            // posicaoRendaFixaHistoricoCollection1
            // 
            this.posicaoRendaFixaHistoricoCollection1.AllowDelete = true;
            this.posicaoRendaFixaHistoricoCollection1.AllowEdit = true;
            this.posicaoRendaFixaHistoricoCollection1.AllowNew = true;
            this.posicaoRendaFixaHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoRendaFixaHistoricoCollection1.Filter = "";
            this.posicaoRendaFixaHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoRendaFixaHistoricoCollection1.Sort = "";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrTable3,
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 241F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(64F, 212F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrSubreport3.Visible = false;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1693F, 64F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell5});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 0.15002953337271116;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "Posições a Liquidar Renda Fixa";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.84997046662728881;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(768F, 142F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2519F, 190F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(63F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrPageInfo2,
            this.xrTable8});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 190F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(2400F, 42F);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:HH:mm:ss}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(360F, 1.999959F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:d}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(212F, 1.999959F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.CanGrow = false;
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "Data Emissão:";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell25.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 102F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(645F, 100F);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 79F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(635F, 42F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell1.Text = "#Carteira";
            this.xrTableCell1.Weight = 0.33385826771653543;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.CanGrow = false;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell2.Text = "DataInicio";
            this.xrTableCell2.Weight = 0.66614173228346452;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.xrTable7});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DescricaoClasse", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 98F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrTable10
            // 
            this.xrTable10.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.KeepTogether = true;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DescricaoClasse")});
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // xrTable7
            // 
            this.xrTable7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.KeepTogether = true;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 50F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell39,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell8,
            this.xrTableCell32,
            this.xrTableCell35,
            this.xrTableCell9,
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "CPF / CNPJ";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell10.Weight = 0.10143068612339985;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Código";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell12.Weight = 0.0983182853484249;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "Clearing";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell39.Weight = 0.080455145778426246;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Quantidade";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell13.Weight = 0.12339362332141074;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Valor";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell14.Weight = 0.1135667670682731;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Valor IR";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell8.Weight = 0.091252510040160661;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Valor IOF";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell32.Weight = 0.090319579097640573;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Valor Líquido";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.12644744015122991;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Banco";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.050962149378765122;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Agência";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.05993966098770076;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "C/C";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.063914152704568281;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 66.52081F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell42,
            this.xrTableCell22,
            this.xrTableCell41,
            this.xrTableCell30,
            this.xrTableCell17,
            this.xrTableCell37,
            this.xrTableCell33,
            this.xrTableCell19,
            this.xrTableCell11});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Total p/ Grupo:";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.19974894695971387;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Weight = 0.08045514577842626;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n2}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell22.Summary = xrSummary1;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.12339364783352166;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell41.Summary = xrSummary2;
            this.xrTableCell41.Text = "xrTableCell41";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.11356681609249494;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell30.Summary = xrSummary3;
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.091252411991716853;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell17.Summary = xrSummary4;
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.09031962812186245;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell37.Summary = xrSummary5;
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.12644773429656128;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.050961855233433724;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.059939562939257021;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.06391425075301202;
            // 
            // calculatedFieldAgencia
            // 
            this.calculatedFieldAgencia.DataSource = dt;
            this.calculatedFieldAgencia.Expression = "[Agencia]";
            this.calculatedFieldAgencia.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldAgencia.Name = "calculatedFieldAgencia";
            // 
            // calculatedFieldPUMercadoRecomposto
            // 
            this.calculatedFieldPUMercadoRecomposto.DataSource = dt;
            this.calculatedFieldPUMercadoRecomposto.Expression = "[PUMercadoRecomposto]";
            this.calculatedFieldPUMercadoRecomposto.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldPUMercadoRecomposto.Name = "calculatedFieldPUMercadoRecomposto";
            // 
            // calculatedFieldContaCorrente
            // 
            this.calculatedFieldContaCorrente.DataSource = dt;
            this.calculatedFieldContaCorrente.Expression = "[ContaCorrente]";
            this.calculatedFieldContaCorrente.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldContaCorrente.Name = "calculatedFieldContaCorrente";
            // 
            // calculatedFieldClearing
            // 
            this.calculatedFieldClearing.DataSource = dt;
            this.calculatedFieldClearing.Expression = "[Clearing]";
            this.calculatedFieldClearing.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldClearing.Name = "calculatedFieldClearing";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            // 
            // calculatedFieldBanco
            // 
            this.calculatedFieldBanco.DataSource = dt;
            this.calculatedFieldBanco.Expression = "[Banco]";
            this.calculatedFieldBanco.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldBanco.Name = "calculatedFieldBanco";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 48F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2490F, 48F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell43,
            this.xrTableCell7,
            this.xrTableCell46,
            this.xrTableCell31,
            this.xrTableCell21,
            this.xrTableCell38,
            this.xrTableCell47,
            this.xrTableCell45,
            this.xrTableCell34});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Total Geral:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.19974892244760292;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.Weight = 0.080454998705760547;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell7.Summary = xrSummary6;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell7.Weight = 0.12339381941829815;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell46.Summary = xrSummary7;
            this.xrTableCell46.Text = "xrTableCell46";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.11356686511671685;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            xrSummary8.FormatString = "{0:n2}";
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell31.Summary = xrSummary8;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.091252411991716839;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            xrSummary9.FormatString = "{0:n2}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell21.Summary = xrSummary9;
            this.xrTableCell21.Text = "xrTableCell21";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.090319677146084312;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "calculatedFieldValorLiquido")});
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            xrSummary10.FormatString = "{0:n2}";
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell38.Summary = xrSummary10;
            this.xrTableCell38.Text = "xrTableCell38";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.12644737887095259;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 0.050962100354543176;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.059939562939257042;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.063914263009067523;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            this.bottomMarginBand1.StylePriority.UseFont = false;
            // 
            // calculatedFieldCPFCNPJ
            // 
            this.calculatedFieldCPFCNPJ.DataSource = dt;
            this.calculatedFieldCPFCNPJ.Expression = "[CPF_CNPJ]";
            this.calculatedFieldCPFCNPJ.FieldType = DevExpress.XtraReports.UI.FieldType.String;
            this.calculatedFieldCPFCNPJ.Name = "calculatedFieldCPFCNPJ";
            // 
            // calculatedFieldValorLiquido
            // 
            this.calculatedFieldValorLiquido.DataSource = dt;
            this.calculatedFieldValorLiquido.Expression = "[ValorMercado] - [ValorIOF] - [ValorIR]";
            this.calculatedFieldValorLiquido.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
            this.calculatedFieldValorLiquido.Name = "calculatedFieldValorLiquido";
            // 
            // ReportPosicaoLiquidaRF
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldBanco,
            this.calculatedFieldAgencia,
            this.calculatedFieldContaCorrente,
            this.calculatedFieldClearing,
            this.calculatedFieldCPFCNPJ,
            this.calculatedFieldValorLiquido});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.ReportPosicaoLiquidaRF.ResourceManager;
        }

        #region Funções Internas do Relatorio

        #endregion
    }
}