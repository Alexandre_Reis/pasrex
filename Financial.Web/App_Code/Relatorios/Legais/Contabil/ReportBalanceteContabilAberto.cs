﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Contabil;
using Financial.Util;
using System.Text.RegularExpressions;
using Financial.Contabil.Enums;
using Financial.Common.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportBalanceteContabilAberto
    /// </summary>
    public class ReportBalanceteContabilAberto : XtraReport {
       
        private DateTime dataInicio;

        public DateTime DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        public DateTime dataFimSaldo;

        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        // Guarda o Cliente
        private Cliente cliente;

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private PageHeaderBand PageHeader;
        private XRSubreport xrSubreport3;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport1;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell3;
        private XRSubreport xrSubreport2;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell10;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell13;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo4;
        private XRPageInfo xrPageInfo5;
        private XRTable xrTable4;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTable xrTable5;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCellDataInicio;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCellDataFim;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell44;


        protected class ValoresFooter {
            public decimal ativo = 0;  // somatório conta 1
            public decimal patrimonio = 0; // somatório conta 6
            public decimal passivo = 0; // somatório conta 4
            public decimal receitas = 0; // somatório conta 7
            public decimal despesas = 0; // somatório conta 8
            public decimal patrimonioLiquido1 = 0; // somatório conta 1 - somatório conta 4
            public decimal patrimonioLiquido2 = 0; // somatório conta 6 + 7 + 8
        }
        //
        private ValoresFooter valoresFooter = new ValoresFooter();

        // Construtor
        public ReportBalanceteContabilAberto(int idCliente, DateTime data_Inicio, DateTime data_Fim) {

            this.dataInicio = data_Inicio;
            this.dataFim = data_Fim;
            this.idCliente = idCliente;
            //
            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            //
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            campos.Add(this.cliente.Query.IdLocal);
            //
            this.cliente.LoadByPrimaryKey(campos, this.idCliente);
            
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2480);
        }

        private void PersonalInitialize() {
            List<Balancete> dt = this.FillDados();
            this.DataSource = dt;

            this.numeroLinhasDataTable = dt.Count;
        }

        /// <summary>
        /// Retorna se Relatório tem Dados
        /// </summary>
        /// <returns></returns>
        public bool RelatorioTemDados() {
            return this.numeroLinhasDataTable != 0;
        }

        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport1.Visible = true;
                //                
                this.xrTable2.Visible = false;                
                this.xrTable7.Visible = false;
                this.xrTable10.Visible = false;
                //
                this.ReportFooter.Visible = false;
            }
        }

        private List<Balancete> FillDados() 
        {
            DateTime dataPrimeiro = this.dataInicio;
            DateTime dataUltimo = this.dataFim;

            ContabSaldo contabSaldo = new ContabSaldo();
            contabSaldo.Query.Select(contabSaldo.Query.Data.Max());
            contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(cliente.IdCliente.Value));
            contabSaldo.Query.Load();

            if (contabSaldo.Data.HasValue)
            {
                if (dataUltimo > contabSaldo.Data.Value)
                {
                    dataUltimo = contabSaldo.Data.Value;
                }
            }

            if (dataUltimo > cliente.DataDia.Value)
            {
                dataUltimo = cliente.DataDia.Value;
            }

            this.dataFimSaldo = dataUltimo;

            #region Consulta
            ContabContaCollection c = new ContabContaCollection();            
            c.Query.Load();
            #endregion

            #region Ordenação
            c.CreateColumnsForBinding();
            //
            c.AddColumn("Ordenacao", typeof(System.Int64));

            //
            for (int i = 0; i < c.Count; i++) {
                string codigo = c[i].Codigo;

                codigo = Regex.Replace(codigo, "[^0-9]+", string.Empty);
                codigo = codigo.PadRight(18, '0');
                //
                Int64 codigoInt = Convert.ToInt64(codigo);
                //
                c[i].SetColumn("Ordenacao", codigoInt);
            }

            c.Sort = "Ordenacao ASC";
            //
            #endregion

            #region Monta Objeto Balancete
            List<Balancete> l = new List<Balancete>();
            //
            for (int i = 0; i < c.Count; i++) {                                                
                Balancete b = new Balancete();
                //
                b.IdConta = c[i].IdConta.Value;
                b.TipoConta = c[i].TipoConta.Trim().ToUpper();
                b.CodigoConta = String.IsNullOrEmpty(c[i].Codigo) ? "" : c[i].Codigo.Trim();
                b.CodigoReduzida = String.IsNullOrEmpty(c[i].CodigoReduzida) ? "" : c[i].CodigoReduzida.Trim();
                b.Descricao = c[i].Descricao.Trim();
                //
                b.ContaDetalhe = this.IsContaDetalhe(c[i].IdConta.Value);

                List<int> listaContaDetalhe = new List<int>();

                if (!b.ContaDetalhe)
                {
                    listaContaDetalhe = RetornaListaContaDetalhe(c[i].IdConta.Value);
                }

                #region Saldos

                b.SaldoAnterior = b.SaldoFinal = 0;

                if (b.ContaDetalhe) {
                    #region Detalhe
                    //
                    ContabSaldo cAnterior = new ContabSaldo();
                    if (cAnterior.LoadByPrimaryKey(this.idCliente, c[i].IdConta.Value, dataPrimeiro)) {
                        b.SaldoAnterior = cAnterior.SaldoAnterior.Value;
                    }

                    ContabSaldo cFinal = new ContabSaldo();
                    if (cFinal.LoadByPrimaryKey(this.idCliente, c[i].IdConta.Value, dataUltimo)) {
                        b.SaldoFinal = cFinal.SaldoFinal.Value;
                    }
                    #endregion
                }
                else {
                    #region Não Detalhe
                    ContabContaCollection cc = new ContabContaCollection();
                    cc.Query.Select(cc.Query.IdConta)
                           .Where(cc.Query.IdConta.In(listaContaDetalhe));

                    cc.Query.Load();

                    // Para cada conta
                    for (int j = 0; j < cc.Count; j++) {
                        ContabSaldo cAnterior1 = new ContabSaldo();
                        if (cAnterior1.LoadByPrimaryKey(this.idCliente, cc[j].IdConta.Value, dataPrimeiro)) {
                            b.SaldoAnterior += cAnterior1.SaldoAnterior.Value;
                        }

                        ContabSaldo cFinal = new ContabSaldo();
                        if (cFinal.LoadByPrimaryKey(this.idCliente, cc[j].IdConta.Value, dataUltimo)) {
                            b.SaldoFinal += cFinal.SaldoFinal.Value;
                        }
                    }

                    #region Armazena os Totais Footer
                    
                    switch (b.CodigoConta) {
                        #region Ativo
                         case "1": valoresFooter.ativo += b.SaldoFinal;
                             break;
                        #endregion
                        #region Patrimonio
                         case "6": valoresFooter.patrimonio += b.SaldoFinal;
                             break;
                        #endregion
                        #region Passivo
                         case "4": valoresFooter.passivo += b.SaldoFinal;
                             break;
                        #endregion
                        #region Receitas
                         case "7": valoresFooter.receitas += b.SaldoFinal;
                             break;
                        #endregion
                        #region Despesas
                         case "8": valoresFooter.despesas += b.SaldoFinal;
                             break;
                         #endregion
                     }
                    #endregion

                    #endregion
                }
                #endregion

                #region Debito/Credito
                b.CreditoMes = b.DebitoMes = 0;

                if (b.ContaDetalhe) {
                    #region Detalhe
                    //
                    ContabSaldo cTotal = new ContabSaldo();
                    cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                        cTotal.Query.TotalDebito.Sum());
                    //
                    cTotal.Query.Where(cTotal.Query.IdCliente == this.idCliente,
                                       cTotal.Query.IdConta == c[i].IdConta.Value,
                                       cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                    //                                
                    cTotal.Query.Load();

                    if (cTotal.es.HasData) {
                        if (cTotal.TotalCredito.HasValue) {
                            b.CreditoMes = cTotal.TotalCredito.Value;
                        }
                        if (cTotal.TotalDebito.HasValue) {
                            b.DebitoMes = cTotal.TotalDebito.Value;
                        }
                    } 
                    #endregion
                }
                else {
                    #region Não Detalhe
                    ContabContaCollection cc1 = new ContabContaCollection();
                    cc1.Query.Select(cc1.Query.IdConta)
                           .Where(cc1.Query.IdConta.In(listaContaDetalhe));

                    cc1.Query.Load();

                    // Para cada conta
                    for (int k = 0; k < cc1.Count; k++) {
                        ContabSaldo cTotal = new ContabSaldo();
                        cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                            cTotal.Query.TotalDebito.Sum());
                        //
                        cTotal.Query.Where(cTotal.Query.IdCliente == this.idCliente,
                                           cTotal.Query.IdConta == cc1[k].IdConta.Value,
                                           cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                        //                                
                        cTotal.Query.Load();

                        if (cTotal.es.HasData) {
                            if (cTotal.TotalCredito.HasValue) {
                                b.CreditoMes += cTotal.TotalCredito.Value;
                            }
                            if (cTotal.TotalDebito.HasValue) {
                                b.DebitoMes += cTotal.TotalDebito.Value;
                            }
                        } 
                    } 
                    #endregion
                }
                #endregion

                if (this.TemSaldoMovimento(b.SaldoAnterior, b.SaldoFinal, b.CreditoMes, b.DebitoMes)){
                    l.Add(b);
                }
            } 
            #endregion

            return l;
        }

        /// <summary>
        /// Retorna true se algum valor for diferente de 0
        /// </summary>
        /// <param name="saldoInicio"></param>
        /// <param name="SaldoFinal"></param>
        /// <param name="net"></param>
        /// <returns></returns>
        private bool TemSaldoMovimento(decimal saldoInicio, decimal saldoFinal, decimal creditoMes, decimal debitoMes) {
            return saldoInicio != 0 ||
                   saldoFinal != 0 ||
                   creditoMes != 0 ||
                   debitoMes != 0;
        }

        /// <summary>
        /// Dado um IdConta determina se é conta detalhe. ultimo nivel
        /// </summary>
        /// <param name="codigoConta"></param>
        /// <returns></returns>
        private bool IsContaDetalhe(int idConta) {

            ContabContaCollection c = new ContabContaCollection();
            //
            c.Query.Select(c.Query.IdConta);
            c.Query.Where(c.Query.IdContaMae == idConta);
            c.Query.Load();
                
            return c.Count == 0;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            string resourceFileName = "ReportBalanceteContabilAberto.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo4 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo5 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataFim = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 42.12501F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.KeepTogether = true;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2480F, 42F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableBeforePrint);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell35,
            this.xrTableCell55,
            this.xrTableCell58,
            this.xrTableCell9,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell8,
            this.xrTableCell13});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CodigoConta")});
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell28.Weight = 0.11612903225806452;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CodigoReduzida")});
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "NumConta";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell35.Weight = 0.057661290322580654;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Descricao")});
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "xrTableCell55";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.26088709677419353;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SaldoAnterior")});
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.Text = "Debitos";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.13427419354838716;
            this.xrTableCell58.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell9.Weight = 0.014112903225806476;
            this.xrTableCell9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoAnteriorDCBeforePrint);
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DebitoMes")});
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.Text = "Creditos";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.13427419354838713;
            this.xrTableCell59.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CreditoMes")});
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell60.StylePriority.UseFont = false;
            xrSummary1.FormatString = "{0:n2}";
            this.xrTableCell60.Summary = xrSummary1;
            this.xrTableCell60.Text = "Saldos";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.13427419354838704;
            this.xrTableCell60.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SaldoFinal")});
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.1342741935483871;
            this.xrTableCell8.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "xrTableCell13";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.014112903225806463;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoFinalDCBeforePrint);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrTable7,
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable1,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 339.6041F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.LightGray;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(99.99997F, 243.6042F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2480F, 47.99997F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "#NomeCNPJ";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell10.Weight = 1;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCNPJ);
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(99.99989F, 291.6041F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2480F, 48.00002F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell25,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrTableCell26,
            this.xrTableCell12,
            this.xrTableCell6,
            this.xrTableCell11});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Conta";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell2.Weight = 0.11612903274790054;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Resumida";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.057661291821876022;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Nome da Conta";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell4.Weight = 0.260887097923642;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "#Saldo";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.13427419372382365;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoInicioBeforePrint);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Weight = 0.014112903401243002;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Débito";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.13427419949080183;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "Crédito";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell12.Weight = 0.13427419159644785;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "#Saldo";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell6.Weight = 0.13427418980842293;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoFimBeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Weight = 0.014112899485842264;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportLogotipo1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(51.45834F, 142.5416F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport1.Visible = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2472F, 26.00003F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(108F, 37.99998F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(884F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1380.958F, 64F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 0.088516746411483258;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "#Titulo";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.737415222460003;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo4,
            this.xrPageInfo5,
            this.xrTable4,
            this.xrTable5,
            this.xrTable6});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 95.60414F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 148F);
            // 
            // xrPageInfo4
            // 
            this.xrPageInfo4.Dpi = 254F;
            this.xrPageInfo4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo4.Format = "{0:HH:mm:ss}";
            this.xrPageInfo4.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo4.Name = "xrPageInfo4";
            this.xrPageInfo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo4.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo4.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo5
            // 
            this.xrPageInfo5.Dpi = 254F;
            this.xrPageInfo5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo5.Format = "{0:d}";
            this.xrPageInfo5.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo5.Name = "xrPageInfo5";
            this.xrPageInfo5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo5.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo5.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.SizeF = new System.Drawing.SizeF(931F, 43F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.CanGrow = false;
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Text = "Cliente:";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell40.Weight = 0.22771213748657357;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.CanGrow = false;
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "NomeCarteira";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell41.Weight = 0.7722878625134264;
            this.xrTableCell41.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCellDataInicio,
            this.xrTableCell43,
            this.xrTableCellDataFim});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.CanGrow = false;
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.Text = "Período:";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell42.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataInicio";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.26614173228346455;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioBeforePrint);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.CanGrow = false;
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Text = "a";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell43.Weight = 0.099212598425196849;
            // 
            // xrTableCellDataFim
            // 
            this.xrTableCellDataFim.CanGrow = false;
            this.xrTableCellDataFim.Dpi = 254F;
            this.xrTableCellDataFim.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataFim.Name = "xrTableCellDataFim";
            this.xrTableCellDataFim.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataFim.Text = "DataFim";
            this.xrTableCellDataFim.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataFim.Weight = 0.30078740157480316;
            this.xrTableCellDataFim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataFimBeforePrint);
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.CanGrow = false;
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.Text = "Data Emissão:";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell44.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
            this.PageFooter.HeightF = 71F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.StylePriority.UseFont = false;
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(528F, 64F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150.4374F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 97.89584F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.BorderWidth = 2;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2480F, 96.00004F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseBorderWidth = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableFooterBeforePrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.BorderWidth = 1;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell36,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell38,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.StylePriority.UseBorderWidth = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Ativo (1):";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell14.Weight = 0.10866096299456132;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.10244450797936038;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UsePadding = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell36.Weight = 0.014961366480179442;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "Passivo (4):";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.10173054414201255;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell17.Weight = 0.13119965867664918;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell38.Weight = 0.010080645536370728;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.Weight = 0.085399849359534347;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell19.Weight = 0.13512258816267683;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Patrimônio Líquido (1-4):";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell20.Weight = 0.16201289812366559;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell21.Weight = 0.13427407905914748;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell22.Weight = 0.014112899485842264;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell37,
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell39,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.StylePriority.UsePadding = false;
            this.xrTableRow4.StylePriority.UseTextAlignment = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Patrimônio (6):";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell23.Weight = 0.10866096299456132;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell24.Weight = 0.1024445572012606;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell37.Weight = 0.014961317258279247;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Receitas (7):";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.10173066719676305;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell29.Weight = 0.13119956023284879;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell39.Weight = 0.010080645536370708;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Despesas (8):";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell30.Weight = 0.085399849359534291;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell31.Weight = 0.13512253894077664;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Patrimônio Líquido (6+7-8):";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell32.Weight = 0.16201289812366559;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UsePadding = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell33.Weight = 0.13427410367009759;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.014112899485842264;
            // 
            // ReportBalanceteContabilAberto
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.ReportFooter});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(101, 101, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportBalanceteContabilAberto.ResourceManager;
        }

        #region Funções Internas do Relatorio

        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataInicioXRTableCell = sender as XRTableCell;
            dataInicioXRTableCell.Text = this.dataInicio.ToString("d");
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataFimXRTableCell = sender as XRTableCell;
            dataFimXRTableCell.Text = this.dataFim.ToString("d");
        }

        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell nomeCliente = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            //
            nomeCliente.Text = this.idCliente + " - " + cliente.Nome;
        }

        private void TituloBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tituloXRTableCell = sender as XRTableCell;
            //

            string cabecalho = "Balancete Demonstrativo referente ";
            if (this.dataInicio == this.dataFim)
                cabecalho += "a data de: " + this.dataInicio.Day + " de " + Utilitario.RetornaMesString(this.dataInicio.Month) + " de " + this.dataInicio.Year;
            else
                cabecalho += "ao período de: " + this.dataInicio.ToString("d") + " a " + this.dataFim.ToString("d");

            tituloXRTableCell.Text = cabecalho;
        }

        private void SaldoInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoInicioXRTableCell = sender as XRTableCell;
            //

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(this.DataInicio, 1, this.cliente.IdLocal.Value, TipoFeriado.Outros);
            saldoInicioXRTableCell.Text = "Saldo Anterior " + dataAnterior.ToString("d");
        }

        private void SaldoFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoFimXRTableCell = sender as XRTableCell;
            //
            saldoFimXRTableCell.Text = "Saldo Atual" + this.dataFimSaldo.ToShortDateString();
        }

        private void NomeCNPJ(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCNPJ = sender as XRTableCell;
            xrTableCellCNPJ.Text = "";
            //
            if (this.cliente.UpToPessoaByIdPessoa != null)
            {
                string cnpj = this.cliente.Nome.Trim().ToUpper() + " - CNPJ: " + this.cliente.UpToPessoaByIdPessoa.Cpfcnpj; // já com mascara
                xrTableCellCNPJ.Text = cnpj;
            }
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
                valor = Math.Abs(valor);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }
             
        #endregion        

        private void TableBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.IsContaDetalhe(Convert.ToInt32(this.GetCurrentColumnValue("IdConta"))))
            {
                XRTable teste = sender as XRTable;
                XRTableRow tableTotalRow0 = teste.Rows[0];

                int colunas = tableTotalRow0.Cells.Count;
                for (int j = 0; j < colunas; j++)
                {

                    string nomeFonte = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Name;
                    float size = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Size;

                    Font font = new Font(nomeFonte, size, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));

                    ((XRTableCell)teste.Rows[0].Cells[j]).Font = font;
                }
            }
            else
            {
                XRTable teste = sender as XRTable;
                XRTableRow tableTotalRow0 = teste.Rows[0];

                int colunas = tableTotalRow0.Cells.Count;
                for (int j = 0; j < colunas; j++)
                {

                    string nomeFonte = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Name;
                    float size = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Size;

                    Font font = new Font(nomeFonte, size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));

                    ((XRTableCell)teste.Rows[0].Cells[j]).Font = font;
                }
            }
        }

        private void SaldoAnteriorDCBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoAnteriorXRTableCell = sender as XRTableCell;
            saldoAnteriorXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldoAnterior = Convert.ToDecimal(this.GetCurrentColumnValue("SaldoAnterior"));
                string tipoConta = Convert.ToString(this.GetCurrentColumnValue(ContabContaMetadata.ColumnNames.TipoConta));

                string cod = "";
                if (tipoConta == TipoContaContabil.Ativo) {
                    cod = saldoAnterior >= 0 ? "D" : "C";
                }
                else if (tipoConta == TipoContaContabil.Passivo) {
                    cod = saldoAnterior >= 0 ? "C" : "D";
                }
                saldoAnteriorXRTableCell.Text = cod;
            }
        }

        private void SaldoFinalDCBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoFinalXRTableCell = sender as XRTableCell;
            saldoFinalXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldoFinal = Convert.ToDecimal(this.GetCurrentColumnValue("SaldoFinal"));
                string tipoConta = Convert.ToString(this.GetCurrentColumnValue(ContabContaMetadata.ColumnNames.TipoConta));

                string cod = "";
                if (tipoConta == TipoContaContabil.Ativo) {
                    cod = saldoFinal >= 0 ? "D" : "C";
                }
                else if (tipoConta == TipoContaContabil.Passivo) {
                    cod = saldoFinal >= 0 ? "C" : "D";
                }
                saldoFinalXRTableCell.Text = cod;
            }
        }

        private List<int> RetornaListaContaDetalhe(int idContaMae)
        {
            List<int> lista = new List<int>();

            ContabContaCollection coll = new ContabContaCollection();
            coll.Query.Select(coll.Query.IdConta);
            coll.Query.Where(coll.Query.IdContaMae == idContaMae);
            coll.Query.Load();

            foreach (ContabConta contabConta in coll)
            {
                if (this.IsContaDetalhe(contabConta.IdConta.Value))
                {
                    lista.Add(contabConta.IdConta.Value);
                }
                else
                {
                    List<int> listaAux = this.RetornaListaContaDetalhe(contabConta.IdConta.Value);

                    foreach (int id in listaAux)
                    {
                        lista.Add(id);
                    }
                }
            }

            return lista;
        }

        private void TableFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable totalXRTableCell = sender as XRTable;

            #region Limpa os Dados da Tabela
            //ReportBase.LimpaDadosTable(totalXRTableCell);
            #endregion

            if (this.numeroLinhasDataTable != 0) {

                this.valoresFooter.patrimonioLiquido1 = this.valoresFooter.ativo - this.valoresFooter.passivo;
                this.valoresFooter.patrimonioLiquido2 = this.valoresFooter.patrimonio + this.valoresFooter.receitas + this.valoresFooter.despesas;
                
                #region Linha 0
                XRTableRow totalRow0 = totalXRTableCell.Rows[0];
                
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[1]), this.valoresFooter.ativo, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow0.Cells[2]).Text = this.valoresFooter.ativo >= 0 ? "D" : "C";
                
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[4]), this.valoresFooter.passivo, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow0.Cells[5]).Text = this.valoresFooter.passivo >= 0 ? "C" : "D";
                
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[9]), this.valoresFooter.patrimonioLiquido1, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                //((XRTableCell)totalRow0.Cells[10]).Text = this.valoresFooter.patrimonioLiquido1 >= 0 ? "C" : "D";
                #endregion

                #region Linha 1
                XRTableRow totalRow1 = totalXRTableCell.Rows[1];
                
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[1]), this.valoresFooter.patrimonio, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow1.Cells[2]).Text = this.valoresFooter.patrimonio >= 0 ? "C" : "D";

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[4]), Math.Abs(this.valoresFooter.receitas), ReportBase.NumeroCasasDecimais.DuasCasasDecimais);               
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[7]), Math.Abs(this.valoresFooter.despesas), ReportBase.NumeroCasasDecimais.DuasCasasDecimais);                                
                
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[9]), this.valoresFooter.patrimonioLiquido2, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                //((XRTableCell)totalRow1.Cells[10]).Text = this.valoresFooter.patrimonioLiquido2 >= 0 ? "C" : "D";
                #endregion
            }
        }
    }
}