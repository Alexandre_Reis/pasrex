﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using Financial.Bolsa.Enums;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Contabil;
using System.Text.RegularExpressions;
using Financial.Contabil.Enums;
using Financial.Common.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportBalanceteContabilFechado
    /// </summary>
    public class ReportBalanceteContabilFechado : XtraReport {

        private DateTime dataInicio;
        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private DateTime dataFim;
        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        private DateTime dataFimSaldo;

        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        // Guarda o Cliente
        private Cliente cliente;

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private SubReportRodape subReportRodape1;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private ReportSemDados reportSemDados1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell14;
        private XRPageInfo xrPageInfo3;
        private XRPageInfo xrPageInfo1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell11;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo4;
        private XRPageInfo xrPageInfo5;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell16;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCellDataInicio;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCellDataFim;
        private XRTable xrTable5;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell20;

        protected class ValoresFooter {
            public decimal ativo = 0;  // somatório conta 1
            public decimal patrimonio = 0; // somatório conta 6
            public decimal passivo = 0; // somatório conta 4
            public decimal receitas = 0; // somatório conta 7
            public decimal despesas = 0; // somatório conta 8
            public decimal patrimonioLiquido1 = 0; // somatório conta 1 - somatório conta 4
            public decimal patrimonioLiquido2 = 0; // somatório conta 6 + 7 + 8
        }
        //
        private ValoresFooter valoresFooter = new ValoresFooter();

        // Construtor com parametros
        public ReportBalanceteContabilFechado(int idCliente, DateTime data_Inicio, DateTime data_Fim) {
            this.dataInicio = data_Inicio;
            this.dataFim = data_Fim;
            this.IdCliente = idCliente;

            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            //
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            campos.Add(this.cliente.Query.IdLocal);
            //
            this.cliente.LoadByPrimaryKey(campos, this.idCliente);

            //
            this.InitializeComponent();
            this.PersonalInitialize();
           
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1860);
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.xrTable1.Visible = false;
                this.xrTable6.Visible = false;      
                //
                this.ReportFooter.Visible = false;
            }
        }

        private void PersonalInitialize() {
            List<Balancete> dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;
        }

        /// <summary>
        /// Retorna se Relatório tem Dados
        /// </summary>
        /// <returns></returns>
        public bool RelatorioTemDados() {
            return this.numeroLinhasDataTable != 0;
        }

        private List<Balancete> FillDados() {

            DateTime dataPrimeiro = this.dataInicio;
            DateTime dataUltimo = this.dataFim;

            ContabSaldo contabSaldo = new ContabSaldo();
            contabSaldo.Query.Select(contabSaldo.Query.Data.Max());
            contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(cliente.IdCliente.Value));
            contabSaldo.Query.Load();

            if (contabSaldo.Data.HasValue)
            {
                if (dataUltimo > contabSaldo.Data.Value)
                {
                    dataUltimo = contabSaldo.Data.Value;
                }
            }

            if (dataUltimo > cliente.DataDia.Value)
            {
                dataUltimo = cliente.DataDia.Value;
            }

            this.dataFimSaldo = dataUltimo;
            
            #region Consulta
            ContabContaCollection c = new ContabContaCollection();            
            c.Query.Load();
            #endregion

            #region Ordenação
            c.CreateColumnsForBinding();
            //
            c.AddColumn("Ordenacao", typeof(System.Int64));

            //
            for (int i = 0; i < c.Count; i++) {
                string codigo = c[i].Codigo;

                codigo = Regex.Replace(codigo, "[^0-9]+", string.Empty);
                codigo = codigo.PadRight(18, '0');
                //
                Int64 codigoInt = Convert.ToInt64(codigo);
                //
                c[i].SetColumn("Ordenacao", codigoInt);
            }

            c.Sort = "Ordenacao ASC";
            //
            #endregion

            #region Monta Objeto Balancete
            List<Balancete> l = new List<Balancete>();
            //
            for (int i = 0; i < c.Count; i++) {
                Balancete b = new Balancete();
                //
                b.IdConta = c[i].IdConta.Value;
                b.TipoConta = c[i].TipoConta.Trim().ToUpper();
                b.CodigoConta = String.IsNullOrEmpty(c[i].Codigo) ? "" : c[i].Codigo.Trim();
                b.CodigoReduzida = String.IsNullOrEmpty(c[i].CodigoReduzida) ? "" : c[i].CodigoReduzida.Trim();
                b.Descricao = c[i].Descricao.Trim();
                //
                b.ContaDetalhe = this.IsContaDetalhe(c[i].IdConta.Value);
                //

                List<int> listaContaDetalhe = new List<int>();

                if (!b.ContaDetalhe) {
                    listaContaDetalhe = RetornaListaContaDetalhe(c[i].IdConta.Value);
                }

                #region Saldos
                b.SaldoAnterior = b.SaldoFinal = 0;

                if (b.ContaDetalhe) {
                    #region Detalhe
                    //
                    ContabSaldo cAnterior = new ContabSaldo(); 
                    if (cAnterior.LoadByPrimaryKey(this.idCliente, c[i].IdConta.Value, dataPrimeiro)) {
                        b.SaldoAnterior = cAnterior.SaldoAnterior.Value;
                    }
                    
                    ContabSaldo cFinal = new ContabSaldo();
                    if (cFinal.LoadByPrimaryKey(this.idCliente, c[i].IdConta.Value, dataUltimo)) {
                        b.SaldoFinal = cFinal.SaldoFinal.Value;
                    }
                    #endregion
                }
                else {
                    #region Não Detalhe
                    ContabContaCollection cc = new ContabContaCollection();
                    cc.Query.Select(cc.Query.IdConta)
                            .Where(cc.Query.IdConta.In(listaContaDetalhe));

                    cc.Query.Load();

                    // Para cada conta
                    for (int j = 0; j < cc.Count; j++) {
                        ContabSaldo cAnterior1 = new ContabSaldo();
                        if (cAnterior1.LoadByPrimaryKey(this.idCliente, cc[j].IdConta.Value, dataPrimeiro)) {
                            b.SaldoAnterior += cAnterior1.SaldoAnterior.Value;
                        }

                        ContabSaldo cFinal = new ContabSaldo();
                        if (cFinal.LoadByPrimaryKey(this.idCliente, cc[j].IdConta.Value, dataUltimo)) {
                            b.SaldoFinal += cFinal.SaldoFinal.Value;
                        }
                    }

                    #region Armazena os Totais Footer

                    switch (b.CodigoConta) {
                        #region Ativo
                        case "1": valoresFooter.ativo += b.SaldoFinal;
                            break;
                        #endregion
                        #region Patrimonio
                        case "6": valoresFooter.patrimonio += b.SaldoFinal;
                            break;
                        #endregion
                        #region Passivo
                        case "4": valoresFooter.passivo += b.SaldoFinal;
                            break;
                        #endregion
                        #region Receitas
                        case "7": valoresFooter.receitas += b.SaldoFinal;
                            break;
                        #endregion
                        #region Despesas
                        case "8": valoresFooter.despesas += b.SaldoFinal;
                            break;
                        #endregion
                    }
                    #endregion
                    #endregion
                }
                #endregion

                #region Debito/Credito
                b.Net = 0;

                if (b.ContaDetalhe) {
                    #region Detalhe
                    //
                    ContabSaldo cTotal = new ContabSaldo();
                    cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                        cTotal.Query.TotalDebito.Sum());
                    //
                    cTotal.Query.Where(cTotal.Query.IdCliente == this.idCliente,
                                       cTotal.Query.IdConta == c[i].IdConta.Value,
                                       cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                    //                                
                    cTotal.Query.Load();

                    if (cTotal.es.HasData) {
                        decimal credito = 0;
                        decimal debito = 0;
                        //
                        if (cTotal.TotalCredito.HasValue) {
                            credito = cTotal.TotalCredito.Value;
                        }
                        if (cTotal.TotalDebito.HasValue) {
                            debito = cTotal.TotalDebito.Value;
                        }
                        //
                        if (b.TipoConta == TipoContaContabil.Ativo)
                            b.Net = debito - credito;
                        else
                            b.Net = credito - debito;
                    }
                    #endregion
                }
                else {
                    #region Não Detalhe
                    ContabContaCollection cc1 = new ContabContaCollection();
                    cc1.Query.Select(cc1.Query.IdConta)
                           .Where(cc1.Query.IdConta.In(listaContaDetalhe));

                    cc1.Query.Load();

                    // Para cada conta
                    for (int k = 0; k < cc1.Count; k++) 
                    {
                        ContabSaldo cTotal = new ContabSaldo();
                        cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                            cTotal.Query.TotalDebito.Sum());
                        //
                        cTotal.Query.Where(cTotal.Query.IdCliente == this.idCliente,
                                           cTotal.Query.IdConta == cc1[k].IdConta.Value,
                                           cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                        //                                
                        cTotal.Query.Load();

                        if (cTotal.es.HasData) 
                        {
                            decimal credito = 0;
                            decimal debito = 0;
                            //
                            if (cTotal.TotalCredito.HasValue) {
                                credito = cTotal.TotalCredito.Value;
                            }
                            if (cTotal.TotalDebito.HasValue) {
                                debito = cTotal.TotalDebito.Value;
                            }
                            //
                            decimal cont = 0;
                            if (b.TipoConta == TipoContaContabil.Ativo)
                                cont = debito - credito;
                            else
                                cont = credito - debito;

                            b.Net += cont;
                        }
                    }
                    #endregion
                }
                #endregion

                //
                // Adiciona se tem Saldo ou Movimento
                if(this.TemSaldoMovimento(b.SaldoAnterior, b.SaldoFinal, b.Net)) {
                    l.Add(b);
                }
            }
            #endregion

            return l;
        }

        /// <summary>
        /// Retorna true se algum valor for diferente de 0
        /// </summary>
        /// <param name="saldoInicio"></param>
        /// <param name="SaldoFinal"></param>
        /// <param name="net"></param>
        /// <returns></returns>
        private bool TemSaldoMovimento(decimal saldoInicio, decimal saldoFinal, decimal net) {
            return saldoInicio!=0 || 
                   saldoFinal!=0 || 
                   net!=0;
        }

        /// <summary>
        /// Dado um IdConta determina se é conta detalhe. ultimo nivel
        /// </summary>
        /// <param name="codigoConta"></param>
        /// <returns></returns>
        private bool IsContaDetalhe(int idConta) {

            ContabContaCollection c = new ContabContaCollection();
            //
            c.Query.Select(c.Query.IdConta);
            c.Query.Where(c.Query.IdContaMae == idConta);
            c.Query.Load();

            return c.Count == 0;
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Bolsa/ReportBalanceteContabilFechado.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportBalanceteContabilFechado.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo4 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo5 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataFim = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 45.0625F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1850.541F, 40F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell4,
            this.xrTableCell13,
            this.xrTableCell6,
            this.xrTableCell14,
            this.xrTableCell11});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CodigoConta")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.12197420992203782;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CodigoReduzida")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "xrTableCell21";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.062600830422233314;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Descricao")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.28341853853036031;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SaldoAnterior")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.15599786486349596;
            this.xrTableCell19.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.018827328575332085;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoAnteriorDCBeforePrint);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Net", "{0:n8}")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.15599786178547195;
            this.xrTableCell13.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.018827329101263036;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NetBeforePrint);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SaldoFinal")});
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.15897870248937202;
            this.xrTableCell14.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.01882737507956278;
            this.xrTableCell11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoFinalDCBeforePrint);
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(783F, 21.00001F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(895.8749F, 64.00002F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            this.xrTableCell55.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloBeforePrint);
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 329F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1850.541F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell1,
            this.xrTableCell28,
            this.xrTableCell2,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell7});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Conta";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.12197420991839646;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Resumida";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.062959440697063829;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Nome da Conta";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell1.Weight = 0.28364265147988688;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "#Saldo";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.15599786486326653;
            this.xrTableCell28.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoInicioBeforePrint);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.018827328653685;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Movto do Mês";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.15599786178524253;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 0.018827329102065891;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "#Saldo";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.15839597918900825;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaldoFimBeforePrint);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.018827374868279446;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 79.33334F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 5F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(661F, 64F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrTable1,
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo2,
            this.xrTable7,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 377F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(1678.875F, 21.00001F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrPageInfo1.Format = "{0:HH:mm:ss}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1828.875F, 21.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(126F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 281F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1859F, 48F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "#NomeCNPJ";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell8.Weight = 1;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCNPJ);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(46.91671F, 108.5F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport3.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(661F, 64F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(1886.541F, 63.00003F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(64F, 43F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 100.5417F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable2.BorderWidth = 2;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow5});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1859F, 96.00004F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseBorderWidth = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableFooterBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.BorderWidth = 1;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.StylePriority.UseBorderWidth = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Ativo (1):";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.10866096299456132;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell37.Weight = 0.107584725334121;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 254F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell38.Weight = 0.016847524972425254;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "Passivo (4):";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell39.Weight = 0.09147600424279001;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell40.Weight = 0.14342424610620133;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UsePadding = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell41.Weight = 0.020483548464102691;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Weight = 0.10097263585867852;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell43.Weight = 0.12788907113640691;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UsePadding = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "Pat. Líquido(1-4):";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell44.Weight = 0.13909166876248513;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell45.Weight = 0.12945671264238565;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UsePadding = false;
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell46.Weight = 0.014112899485842264;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UsePadding = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Património (6):";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell47.Weight = 0.10866096299456132;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UsePadding = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell48.Weight = 0.1075847253076434;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 254F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UsePadding = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell49.Weight = 0.016847524998902883;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UsePadding = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "Receitas (7):";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell50.Weight = 0.091476168337855365;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell51.Weight = 0.14342427070391262;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell52.Weight = 0.020483417214528007;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "Despesas (8):";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell53.Weight = 0.10097261944255255;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell54.Weight = 0.12788903010933087;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "Pat. Líquido(6+7-8):";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell56.Weight = 0.13909166876248513;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell57.Weight = 0.12945671264238565;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell58.Weight = 0.014112899485842264;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo4,
            this.xrPageInfo5,
            this.xrTable3,
            this.xrTable4,
            this.xrTable5});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 108.5F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 148F);
            // 
            // xrPageInfo4
            // 
            this.xrPageInfo4.Dpi = 254F;
            this.xrPageInfo4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo4.Format = "{0:HH:mm:ss}";
            this.xrPageInfo4.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo4.Name = "xrPageInfo4";
            this.xrPageInfo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo4.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo4.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo5
            // 
            this.xrPageInfo5.Dpi = 254F;
            this.xrPageInfo5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo5.Format = "{0:d}";
            this.xrPageInfo5.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo5.Name = "xrPageInfo5";
            this.xrPageInfo5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo5.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo5.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(931F, 43F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell16});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Cliente:";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell12.Weight = 0.22771213748657357;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.CanGrow = false;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "NomeCarteira";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell16.Weight = 0.7722878625134264;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCellDataInicio,
            this.xrTableCell18,
            this.xrTableCellDataFim});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.CanGrow = false;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "Período:";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell17.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "DataInicio";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.26614173228346455;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataInicioBeforePrint);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.CanGrow = false;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "a";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell18.Weight = 0.099212598425196849;
            // 
            // xrTableCellDataFim
            // 
            this.xrTableCellDataFim.CanGrow = false;
            this.xrTableCellDataFim.Dpi = 254F;
            this.xrTableCellDataFim.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataFim.Name = "xrTableCellDataFim";
            this.xrTableCellDataFim.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataFim.Text = "DataFim";
            this.xrTableCellDataFim.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataFim.Weight = 0.30078740157480316;
            this.xrTableCellDataFim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataFimBeforePrint);
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.CanGrow = false;
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "Data Emissão:";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 1;
            // 
            // ReportBalanceteContabilFechado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.ReportFooter});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportBalanceteContabilFechado.ResourceManager;

        }
        
        #region Funções Internas do Relatorio

        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataInicioXRTableCell = sender as XRTableCell;
            dataInicioXRTableCell.Text = this.dataInicio.ToString("d");
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dataFimXRTableCell = sender as XRTableCell;
            dataFimXRTableCell.Text = this.dataFim.ToString("d");
        }

        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell nomeCliente = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            //
            nomeCliente.Text = this.idCliente + " - " + cliente.Nome;
        }

        private void TituloBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tituloXRTableCell = sender as XRTableCell;
            //

            string cabecalho = "Balancete Demonstrativo referente ";
            if (this.dataInicio == this.dataFim)
                cabecalho += "a data de: " + this.dataInicio.Day + " de " + Utilitario.RetornaMesString(this.dataInicio.Month) + " de " + this.dataInicio.Year;
            else
                cabecalho += "ao período de: " + this.dataInicio.ToString("d") + " a " + this.dataFim.ToString("d");

            tituloXRTableCell.Text = cabecalho;
        }

        private void SaldoInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell saldoInicioXRTableCell = sender as XRTableCell;
            //
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(this.DataInicio, 1, this.cliente.IdLocal.Value, TipoFeriado.Outros);
            saldoInicioXRTableCell.Text = "Saldo Anterior " + dataAnterior.ToString("d");
        }

        private void SaldoFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell saldoFimXRTableCell = sender as XRTableCell;
            //
            saldoFimXRTableCell.Text = "Saldo Atual" + this.dataFimSaldo.ToShortDateString();
        }

        private void NomeCNPJ(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCNPJ = sender as XRTableCell;
            xrTableCellCNPJ.Text = "";
            //
            if (this.cliente.UpToPessoaByIdPessoa != null)
            {
                string cnpj = this.cliente.Nome.Trim().ToUpper() + " - CNPJ: " + this.cliente.UpToPessoaByIdPessoa.Cpfcnpj; // já com mascara
                xrTableCellCNPJ.Text = cnpj;
            }
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
                valor = Math.Abs(valor);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }
        #endregion       

        private void TableBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.IsContaDetalhe(Convert.ToInt32(this.GetCurrentColumnValue("IdConta"))))
            {
                XRTable teste = sender as XRTable;
                XRTableRow tableTotalRow0 = teste.Rows[0];

                int colunas = tableTotalRow0.Cells.Count;
                for (int j = 0; j < colunas; j++)
                {

                    string nomeFonte = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Name;
                    float size = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Size;

                    Font font = new Font(nomeFonte, size, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));

                    ((XRTableCell)teste.Rows[0].Cells[j]).Font = font;
                }
            }
            else
            {
                XRTable teste = sender as XRTable;
                XRTableRow tableTotalRow0 = teste.Rows[0];

                int colunas = tableTotalRow0.Cells.Count;
                for (int j = 0; j < colunas; j++)
                {

                    string nomeFonte = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Name;
                    float size = ((XRTableCell)teste.Rows[0].Cells[j]).Font.Size;

                    Font font = new Font(nomeFonte, size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));

                    ((XRTableCell)teste.Rows[0].Cells[j]).Font = font;
                }
            }
        }

        private void SaldoAnteriorDCBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoAnteriorXRTableCell = sender as XRTableCell;
            saldoAnteriorXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldoAnterior = Convert.ToDecimal(this.GetCurrentColumnValue("SaldoAnterior"));
                string tipoConta = Convert.ToString(this.GetCurrentColumnValue(ContabContaMetadata.ColumnNames.TipoConta));

                string cod = "";
                if (tipoConta == TipoContaContabil.Ativo) {
                    cod = saldoAnterior >= 0 ? "D" : "C";
                }
                else if (tipoConta == TipoContaContabil.Passivo) {
                    cod = saldoAnterior >= 0 ? "C" : "D";
                }
                saldoAnteriorXRTableCell.Text = cod;
            }
        }

        private void NetBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell netXRTableCell = sender as XRTableCell;
            netXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal net = Convert.ToDecimal(this.GetCurrentColumnValue("Net"));
                string tipoConta = Convert.ToString(this.GetCurrentColumnValue(ContabContaMetadata.ColumnNames.TipoConta));

                string cod = "";
                if (tipoConta == TipoContaContabil.Ativo) {
                    cod = net >= 0 ? "D" : "C";
                }
                else if (tipoConta == TipoContaContabil.Passivo) {
                    cod = net >= 0 ? "C" : "D";
                }
                netXRTableCell.Text = cod;
            }
        }

        private void SaldoFinalDCBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell saldoFinalXRTableCell = sender as XRTableCell;
            saldoFinalXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal saldoFinal = Convert.ToDecimal(this.GetCurrentColumnValue("SaldoFinal"));
                string tipoConta = Convert.ToString(this.GetCurrentColumnValue(ContabContaMetadata.ColumnNames.TipoConta));

                string cod = "";
                if (tipoConta == TipoContaContabil.Ativo) {
                    cod = saldoFinal >= 0 ? "D" : "C";
                }
                else if (tipoConta == TipoContaContabil.Passivo) {
                    cod = saldoFinal >= 0 ? "C" : "D";
                }
                saldoFinalXRTableCell.Text = cod;
            }
        }

        private List<int> RetornaListaContaDetalhe(int idContaMae)
        {
            List<int> lista = new List<int>();

            ContabContaCollection coll = new ContabContaCollection();
            coll.Query.Select(coll.Query.IdConta);
            coll.Query.Where(coll.Query.IdContaMae == idContaMae);
            coll.Query.Load();

            foreach (ContabConta contabConta in coll)
            {
                if (this.IsContaDetalhe(contabConta.IdConta.Value))
                {
                    lista.Add(contabConta.IdConta.Value);
                }
                else
                {
                    List<int> listaAux = this.RetornaListaContaDetalhe(contabConta.IdConta.Value);

                    foreach (int id in listaAux)
                    {
                        lista.Add(id);
                    }
                }
            }

            return lista;
        }

        private void TableFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable totalXRTableCell = sender as XRTable;

            #region Limpa os Dados da Tabela
            //ReportBase.LimpaDadosTable(totalXRTableCell);
            #endregion

            if (this.numeroLinhasDataTable != 0) {

                this.valoresFooter.patrimonioLiquido1 = this.valoresFooter.ativo - this.valoresFooter.passivo;
                this.valoresFooter.patrimonioLiquido2 = this.valoresFooter.patrimonio + this.valoresFooter.receitas + this.valoresFooter.despesas;

                #region Linha 0
                XRTableRow totalRow0 = totalXRTableCell.Rows[0];

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[1]), this.valoresFooter.ativo, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow0.Cells[2]).Text = this.valoresFooter.ativo >= 0 ? "D" : "C";

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[4]), this.valoresFooter.passivo, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow0.Cells[5]).Text = this.valoresFooter.passivo >= 0 ? "C" : "D";

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[9]), this.valoresFooter.patrimonioLiquido1, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                //((XRTableCell)totalRow0.Cells[10]).Text = this.valoresFooter.patrimonioLiquido1 >= 0 ? "C" : "D";
                #endregion

                #region Linha 1
                XRTableRow totalRow1 = totalXRTableCell.Rows[1];

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[1]), this.valoresFooter.patrimonio, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ((XRTableCell)totalRow1.Cells[2]).Text = this.valoresFooter.patrimonio >= 0 ? "C" : "D";

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[4]), Math.Abs(this.valoresFooter.receitas), ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[7]), Math.Abs(this.valoresFooter.despesas), ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow1.Cells[9]), this.valoresFooter.patrimonioLiquido2, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                //((XRTableCell)totalRow1.Cells[10]).Text = this.valoresFooter.patrimonioLiquido2 >= 0 ? "C" : "D";
                #endregion
            }
        }
    }
}