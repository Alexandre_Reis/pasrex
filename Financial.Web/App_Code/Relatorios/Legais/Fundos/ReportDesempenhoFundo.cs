﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using EntitySpaces.Core;
using System.Text;
using EntitySpaces.Interfaces;
using System.Web;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.Swap;
using System.Drawing.Printing;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for ReportDesempenhoFundo
    /// </summary>
    public class ReportDesempenhoFundo : DevExpress.XtraReports.UI.XtraReport
    {
        //
        private int? idCliente;

        private DateTime _dataInicio;
        private DateTime _dataFim;
        private decimal _valorBruto;

        public int? IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        private Cliente cliente;

        private int numeroLinhasDataTable;

        int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        // Armazena os últimos 5 anos em relação ao ano passado em ordem crescente
        private List<int> ultimos_5_Anos;

        /* Valores da Table1 */
        private List<CalculoMedida.EstatisticaRetornoMensal> listaRetornosMensais;

        /* Valores da Table2 */
        private List<CalculoMedida.EstatisticaRetornoAnual> listaRetornosAnuais;


        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private PageHeaderBand PageHeader;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private PageFooterBand PageFooter;
        private ReportHeaderBand ReportHeader;
        private ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.Parameters.Parameter parameter1;
        private XRRichText xrRichText1;
        private XRLabel xrLabel9;
        private XRRichText xrRichText5;
        private XRRichText xrRichText6;
        private XRRichText xrRichText8;
        private XRRichText xrRichText9;
        private XRRichText xrRichText16;
        private XRRichText xrRichText17;
        private XRRichText xrRichText19;
        private XRRichText xrRichText21;
        private XRRichText xrRichText22;
        private XRRichText xrRichText23;
        private XRRichText xrRichText24;
        private XRTable xrTable7;
        private XRTableRow xrTableRow24;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell55;
        private XRTableRow xrTableRow25;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell60;
        private XRTableRow xrTableRow26;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell65;
        private XRTableRow xrTableRow27;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell70;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell57;
        private XRRichText xrRichText25;
        private XRPanel xrPanel1;
        private XRPanel xrPanel2;
        private XRTable xrTable8;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableRow xrTableRow28;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell67;
        private XRTableRow xrTableRow29;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell69;
        private XRTableCell xrTableCell71;
        private XRTableRow xrTableRow30;
        private XRTableCell xrTableCell73;
        private XRTableCell xrTableCell74;
        private XRTableCell xrTableCell75;
        private XRTableCell xrTableCell76;
        private XRTableRow xrTableRow31;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell78;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell80;
        private XRTableRow xrTableRow32;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableCell xrTableCell83;
        private XRPageBreak xrPageBreak2;
        private XRPageBreak xrPageBreak3;
        private XRPageBreak xrPageBreak4;
        private XRTable xrTable11;
        private XRTableRow xrTableRow52;
        private XRTableCell xrTableCell151;
        private XRTableCell xrTableCell152;
        private XRTableCell xrTableCell153;
        private XRTableCell xrTableCell154;
        private XRTableRow xrTableRow53;
        private XRTableCell xrM1;
        private XRTableCell xrTableCell156;
        private XRTableCell xrTableCell157;
        private XRTableCell xrTableCell158;
        private XRTableRow xrTableRow54;
        private XRTableCell xrM2;
        private XRTableCell xrTableCell160;
        private XRTableCell xrTableCell161;
        private XRTableCell xrTableCell162;
        private XRTableRow xrTableRow55;
        private XRTableCell xrM3;
        private XRTableCell xrTableCell164;
        private XRTableCell xrTableCell165;
        private XRTableCell xrTableCell166;
        private XRTableRow xrTableRow56;
        private XRTableCell xrM4;
        private XRTableCell xrTableCell168;
        private XRTableCell xrTableCell169;
        private XRTableCell xrTableCell170;
        private XRTableRow xrTableRow57;
        private XRTableCell xrM5;
        private XRTableCell xrTableCell172;
        private XRTableCell xrTableCell173;
        private XRTableCell xrTableCell174;
        private XRTableRow xrTableRow58;
        private XRTableCell xrM6;
        private XRTableCell xrTableCell176;
        private XRTableCell xrTableCell177;
        private XRTableCell xrTableCell178;
        private XRTableRow xrTableRow59;
        private XRTableCell xrM7;
        private XRTableCell xrTableCell180;
        private XRTableCell xrTableCell181;
        private XRTableCell xrTableCell182;
        private XRTableRow xrTableRow60;
        private XRTableCell xrM8;
        private XRTableCell xrTableCell184;
        private XRTableCell xrTableCell185;
        private XRTableCell xrTableCell186;
        private XRTableRow xrTableRow61;
        private XRTableCell xrM9;
        private XRTableCell xrTableCell188;
        private XRTableCell xrTableCell189;
        private XRTableCell xrTableCell190;
        private XRTableRow xrTableRow62;
        private XRTableCell xrM10;
        private XRTableCell xrTableCell192;
        private XRTableCell xrTableCell193;
        private XRTableCell xrTableCell194;
        private XRTableRow xrTableRow63;
        private XRTableCell xrM11;
        private XRTableCell xrTableCell196;
        private XRTableCell xrTableCell197;
        private XRTableCell xrTableCell198;
        private XRTableRow xrTableRow64;
        private XRTableCell xrM13;
        private XRTableCell xrTableCell200;
        private XRTableCell xrTableCell201;
        private XRTableCell xrTableCell202;
        private XRTableRow xrTableRow65;
        private XRTableCell xrTableCell203;
        private XRTableCell xrTableCell204;
        private XRTableCell xrTableCell205;
        private XRTableCell xrTableCell206;
        private XRRichText xrRichText2;
        private XRRichText xrRichText32;
        private XRRichText xrRichText33;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable1;
        private XRTableRow xrTableRow33;
        private XRTableCell xrTableCell103;
        private XRTableCell xrTableCell104;
        private XRTableRow xrTableRow34;
        private XRTableCell xrTableCell105;
        private XRTableCell xrTableCell106;
        private XRTableCell xrTableCell107;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell3;
        private XRTableRow xrTableRow37;
        private XRTableCell xrTableCell132;
        private XRTableCell xrTableCell133;
        private XRTableRow xrTableRow38;
        private XRTableCell xrTableCell136;
        private XRTableCell xrTableCell137;
        private XRTableRow xrTableRow14;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell28;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private XRTableRow xrTableRow13;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableRow xrTableRow21;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableRow xrTableRow22;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell48;
        private XRRichText xrRichText3;
        private XRRichText xrRichText4;
        private XRRichText xrRichText7;
        private XRRichText xrRichText10;
        private XRRichText xrRichText11;
        private XRRichText xrRichText12;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRRichText xrRichText13;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void ReportDesempenhoFundoInternal()
        {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Tratamento para Report sem dados
            //this.SetRelatorioSemDados();            

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            //this.DataSource = new DataView();
        }
        #endregion

        #region Construtores Publics
        public ReportDesempenhoFundo()
        {
            this.ReportDesempenhoFundoInternal();
        }

        public ReportDesempenhoFundo(int idCliente, DateTime data)
        {
            this.data = data;
            this.idCliente = idCliente;
            this.ReportDesempenhoFundoInternal();
        }

        #endregion
        #endregion

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados()
        {
            if (this.numeroLinhasDataTable == 0)
            {
                //this.PageHeader.Visible = true;
                //this.xrSubreport1.Visible = true;
            }
        }

        private void PersonalInitialize()
        {
            // Carrega a Cliente
            this.cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.cliente.Query.Nome);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.IdCliente);
            campos.Add(this.cliente.Query.Apelido);
            campos.Add(this.cliente.Query.DataDia);
            //
            this.cliente.LoadByPrimaryKey(campos, this.IdCliente.Value);

            //         
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCliente.Value);
            //
            this.parameter1.Value = "";
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj))
            {
                this.parameter1.Value = pessoa.str.Cpfcnpj;
            }

            // Carrega Lamina
            //DateTime dataUltimoDiaMes = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            //#region Lamina
            //LaminaCollection laminaCollection = new LaminaCollection();

            //// Busca todas as laminas menores que a data de referencia ordenado decrescente pela data
            //laminaCollection.Query
            //     .Where(laminaCollection.Query.InicioVigencia.LessThanOrEqual(dataUltimoDiaMes),
            //            laminaCollection.Query.IdCarteira == this.idCliente)
            //     .OrderBy(laminaCollection.Query.InicioVigencia.Descending);

            //laminaCollection.Query.Load();

            //// Pega a Primeira se existir Lamina
            //bool existeLamina = laminaCollection.HasData;
            //if (existeLamina) {
            //    this.lamina = laminaCollection[0];
            //}
            //#endregion

            #region Rentabilidade Anual/Mensal
            this.ano = this.data.Year;

            this.ultimos_5_Anos = new List<int>(new int[5] { 
                this.ano - 4, this.ano - 3,
                this.ano - 2, this.ano - 1,
                this.ano
            });

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente.Value);

            int mesInicio;
            int mesFinal;
            DateTime dataInicio;
            DateTime dataFim;
            bool isAnoFechado = false;
            if (this.data.Date.Month == 6)
            {
                // 12 meses findos 30 de junho
                mesInicio = 6;
                mesFinal = 6;
                dataInicio = new DateTime(this.ano - 1, mesInicio, 30);
                dataFim = new DateTime(this.ano, mesFinal, 30);
            }
            else
            {
                // 12 meses findos 31 dezembro
                isAnoFechado = true;
                mesInicio = 1;
                mesFinal = 12;
                dataInicio = new DateTime(this.ano, mesInicio, 01);
                dataInicio = dataInicio.AddDays(-1);
                dataFim = new DateTime(this.ano, mesFinal, 01);
            }
            dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataFim, 0);

            DateTime dataDia = this.cliente.DataDia.Value;

            //Ajusta datas pela data de inicio da cota e dataDia do cliente
            if (dataFim > dataDia)
            {
                dataFim = dataDia;
            }

            if (dataInicio < carteira.DataInicioCota.Value)
            {
                dataInicio = carteira.DataInicioCota.Value;
            }

            _dataInicio = dataInicio;
            _dataFim = dataFim;

            int nrDias = Calendario.NumeroDias(_dataInicio, _dataFim, cliente.IdLocal.Value, TipoFeriado.Brasil);

            CalculoMedida calculoMedida = new CalculoMedida(this.idCliente.Value);
            calculoMedida.SetIdIndice(carteira.IdIndiceBenchmark.Value);
            calculoMedida.SetBenchmarkPercentual(true);
            calculoMedida.SetBuscaCotaMaisProxima(true);
            this.listaRetornosMensais = calculoMedida.RetornaListaRetornosMensais(dataInicio, dataFim);

            #region Busca retornos mensais e anuais
            DateTime dataAnteriorAno;

            //if (this.data.Date.Day == 30)
            //{
            //    dataAnteriorAno = new DateTime(dataInicio.Year - 4, 06, 30); 
            //}
            //else
            //{
            dataAnteriorAno = new DateTime(dataInicio.Year - 4, 01, 01);
            //}
            if (dataAnteriorAno < carteira.DataInicioCota.Value)
            {
                dataAnteriorAno = carteira.DataInicioCota.Value;
            }
            #endregion

            this.listaRetornosAnuais = calculoMedida.RetornaListaRetornosAnuais(dataAnteriorAno, dataFim, isAnoFechado);
            #endregion

            #region Busca valores de despesas de taxa administracao, performance e outras despesas

            #region Despesas1
            decimal valorPLMedio = calculoMedida.CalculaPLMedio(dataInicio, dataFim);

            int origemTaxaAdministracao = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao;
            int origemTaxaGestao = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao;
            int origemTaxaPerformance = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance;
            int origemOutrasDespesas = OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros;

            Liquidacao liquidacao = new Liquidacao();

            decimal valorAdministracaoFundo = Math.Abs(liquidacao.RetornaValorLiquidado(this.idCliente.Value, origemTaxaAdministracao, dataInicio, dataFim));
            decimal valorGestaoFundo = Math.Abs(liquidacao.RetornaValorLiquidado(this.idCliente.Value, origemTaxaGestao, dataInicio, dataFim));
            decimal valorPerformanceFundo = Math.Abs(liquidacao.RetornaValorLiquidado(this.idCliente.Value, origemTaxaPerformance, dataInicio, dataFim));
            decimal valorOutrasDespesasFundo = Math.Abs(liquidacao.RetornaValorLiquidado(this.idCliente.Value, origemOutrasDespesas, dataInicio, dataFim));

            PosicaoFundo posicaoFundo = new PosicaoFundo();
            List<int> listaIdsFundos = posicaoFundo.RetornaListaIdsFundosEmPosicao(this.idCliente.Value, dataInicio, dataFim);

            decimal totalValorAdmFundos = 0;
            decimal totalValorPfeeFundos = 0;

            foreach (int idCarteira in listaIdsFundos)
            {
                CalculoMedida calculoMedidaInvestido = new CalculoMedida(idCarteira);
                decimal valorPLMedioInvestido = calculoMedidaInvestido.CalculaPLMedio(dataInicio, dataFim);

                decimal saldoMedio = posicaoFundo.RetornaSaldoMedioFundo(this.idCliente.Value, idCarteira, dataInicio, dataFim);
                decimal fatorSaldo = 0.0M;
                if (valorPLMedio != 0)
                {
                    fatorSaldo = saldoMedio / valorPLMedio;
                }

                decimal valorAdmFundosFundoInvestido = liquidacao.RetornaValorLiquidado(idCarteira, origemTaxaAdministracao, dataInicio, dataFim);
                decimal valorPfeeFundosFundoInvestido = liquidacao.RetornaValorLiquidado(idCarteira, origemTaxaPerformance, dataInicio, dataFim);
                //
                TabelaInformeDesempenho tabelaInformeDesempenhoInvestido = new TabelaInformeDesempenho();
                tabelaInformeDesempenhoInvestido.Query.Select(tabelaInformeDesempenhoInvestido.Query.ValorAdministracaoPaga.Sum(),
                                                             tabelaInformeDesempenhoInvestido.Query.ValorAdministracaoPagaGrupoEconomicoGestor.Sum(),
                                                             tabelaInformeDesempenhoInvestido.Query.ValorPfeePaga.Sum());
                //tabelaInformeDesempenhoInvestido.Query.ValorPfeeGestor.Sum());

                //
                tabelaInformeDesempenhoInvestido.Query.Where(tabelaInformeDesempenhoInvestido.Query.IdCarteira.Equal(idCarteira),
                                                            tabelaInformeDesempenhoInvestido.Query.Data.GreaterThanOrEqual(dataInicio),
                                                            tabelaInformeDesempenhoInvestido.Query.Data.LessThanOrEqual(dataFim));
                //
                tabelaInformeDesempenhoInvestido.Query.Load();

                decimal valorAdmFundosInformado = 0;
                decimal valorPfeeFundosInformado = 0;
                if (tabelaInformeDesempenhoInvestido.ValorAdministracaoPaga.HasValue)
                {
                    valorAdmFundosInformado += tabelaInformeDesempenhoInvestido.ValorAdministracaoPaga.Value;
                    // valorAdmFundosInformado += tabelaInformeDesempenhoInvestido.ValorAdministracaoPagaGrupoEconomicoGestor.Value;

                    valorPfeeFundosInformado += tabelaInformeDesempenhoInvestido.ValorPfeePaga.Value;
                    //valorPfeeFundosInformado += tabelaInformeDesempenhoInvestido.ValorPfeeGestor.Value;
                }

                totalValorAdmFundos += Math.Abs(Math.Round((valorAdmFundosFundoInvestido + valorAdmFundosInformado) * fatorSaldo, 4));
                totalValorPfeeFundos += Math.Abs(Math.Round((totalValorPfeeFundos = valorPfeeFundosInformado) * fatorSaldo, 4));
            }

            decimal despesaTxAdmParteFixa = totalValorAdmFundos + valorAdministracaoFundo + valorGestaoFundo;
            decimal despesaTxAdmParteVariavel = valorPerformanceFundo + totalValorPfeeFundos;

            /* ------------------------------------------------------------------------------------------------------------------- */
            //
            TabelaInformeDesempenho tabelaInformeDesempenho = new TabelaInformeDesempenho();
            tabelaInformeDesempenho.Query.Select(tabelaInformeDesempenho.Query.ValorAdministracaoPaga.Sum(),
                                                 tabelaInformeDesempenho.Query.ValorAdministracaoPagaGrupoEconomicoGestor.Sum(),
                                                 tabelaInformeDesempenho.Query.ValorPfeePaga.Sum(),
                //tabelaInformeDesempenho.Query.ValorPfeeGestor.Sum(),
                                                 tabelaInformeDesempenho.Query.ValorOutrasDespesasPagas.Sum(),
                                                 tabelaInformeDesempenho.Query.ValorDespesasOperacionaisGrupoEconomicoGestor.Sum(),
                //
                                                 tabelaInformeDesempenho.Query.ValorCustodiaPaga.Sum()
                                                 );
            //
            tabelaInformeDesempenho.Query.Where(tabelaInformeDesempenho.Query.IdCarteira.Equal(this.idCliente.Value),
                                                tabelaInformeDesempenho.Query.Data.GreaterThanOrEqual(dataInicio),
                                                tabelaInformeDesempenho.Query.Data.LessThanOrEqual(dataFim));
            tabelaInformeDesempenho.Query.Load();
            //
            if (tabelaInformeDesempenho.ValorAdministracaoPaga.HasValue)
            {
                despesaTxAdmParteFixa += tabelaInformeDesempenho.ValorAdministracaoPaga.Value;
                //despesaTxAdmParteFixa += tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor.Value;

                despesaTxAdmParteVariavel += tabelaInformeDesempenho.ValorPfeePaga.Value;
                //valorPerformance += tabelaInformeDesempenho.ValorPfeeGestor.Value;

                valorOutrasDespesasFundo += tabelaInformeDesempenho.ValorOutrasDespesasPagas.Value;
                // valorOutrasDespesasFundo += tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor.Value;
            }

            decimal? custodia = null; ;
            if (tabelaInformeDesempenho.ValorCustodiaPaga.HasValue)
            {
                custodia = tabelaInformeDesempenho.ValorCustodiaPaga.Value;
            }

            decimal? percAdmFundos, percPfeeFundos, percOutrasDespesas, percCustodia;

            if (valorPLMedio != 0)
            {
                percAdmFundos = Math.Round((despesaTxAdmParteFixa / nrDias) / valorPLMedio, 4);
                percPfeeFundos = Math.Round((despesaTxAdmParteVariavel / nrDias) / valorPLMedio, 4);
                percOutrasDespesas = Math.Round((valorOutrasDespesasFundo / nrDias) / valorPLMedio, 4);
                //
                percCustodia = 0;
                if (custodia.HasValue)
                {
                    percCustodia = Math.Round((custodia.Value / nrDias) / valorPLMedio, 4);
                }

                // Salva Valores da Table1
                this.valoresTable1.despesaFixa = percAdmFundos;
                this.valoresTable1.despesaVariavel = percPfeeFundos;
                this.valoresTable1.outrasDespesas = percOutrasDespesas;
                this.valoresTable1.taxaCustodia = percCustodia;
                //
                this.valoresTable1.totalDespesas = percAdmFundos + percPfeeFundos + percOutrasDespesas + percCustodia;
            }
            #endregion Despesas1

            #region Despesas 2

            decimal valorOutrasDespesas2 = Math.Abs(liquidacao.RetornaValorLiquidado(this.idCliente.Value, origemOutrasDespesas, dataInicio, dataFim));
            //

            TabelaInformeDesempenho tabelaInforme2 = new TabelaInformeDesempenho();
            tabelaInforme2.Query.Select(
                tabelaInforme2.Query.ValorAdministracaoPaga.Sum(),
                tabelaInforme2.Query.ValorPfeePaga.Sum(),
                tabelaInforme2.Query.ValorCustodiaPaga.Sum(),
                tabelaInforme2.Query.ValorOutrasDespesasPagas.Sum(),

                tabelaInforme2.Query.ValorAdministracaoPagaGrupoEconomicoADM.Sum(),
                tabelaInforme2.Query.ValorDespesasOperacionaisGrupoEconomicoADM.Sum(),
                tabelaInforme2.Query.ValorAdministracaoPagaGrupoEconomicoGestor.Sum(),
                tabelaInforme2.Query.ValorDespesasOperacionaisGrupoEconomicoGestor.Sum());
            //
            tabelaInforme2.Query.Where(tabelaInforme2.Query.IdCarteira.Equal(this.idCliente.Value),
                                       tabelaInforme2.Query.Data.GreaterThanOrEqual(dataInicio),
                                       tabelaInforme2.Query.Data.LessThanOrEqual(dataFim));

            tabelaInforme2.Query.Load();
            //
            if(!tabelaInforme2.es.HasData) {
                throw new Exception("Tabela Informe Desempenho - Fundos de Investimento não cadastrado para o cliente " + this.idCliente.Value + " no periodo " + dataInicio.ToString("d") + " - " + dataFim.ToString("d") );
            }

            if (!tabelaInforme2.ValorAdministracaoPaga.HasValue && !tabelaInforme2.ValorPfeePaga.HasValue &&
                !tabelaInforme2.ValorCustodiaPaga.HasValue && !tabelaInforme2.ValorOutrasDespesasPagas.HasValue &&
                !tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoADM.HasValue && !tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoADM.HasValue &&
                !tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoGestor.HasValue && !tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoGestor.HasValue 
              ) {
                  throw new Exception("Tabela Informe Desempenho - Fundos de Investimento não cadastrado para o cliente " + this.idCliente.Value + " no periodo " + dataInicio.ToString("d") + " - " + dataFim.ToString("d"));
            }

            decimal valorAdministracaoPaga = tabelaInforme2.ValorAdministracaoPaga.HasValue ? tabelaInforme2.ValorAdministracaoPaga.Value : 0;
            decimal valorPfeePaga = tabelaInforme2.ValorPfeePaga.HasValue ? tabelaInforme2.ValorPfeePaga.Value : 0;
            decimal valorCustodiaPaga = tabelaInforme2.ValorCustodiaPaga.HasValue ? tabelaInforme2.ValorCustodiaPaga.Value : 0;
            decimal valorOutrasDespesasPagas = tabelaInforme2.ValorOutrasDespesasPagas.HasValue ? tabelaInforme2.ValorOutrasDespesasPagas.Value : 0;

            decimal valorAdministracaoPagaGrupoEconomicoADM = tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoADM.HasValue ? tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoADM.Value : 0;
            decimal valorDespesasOperacionaisGrupoEconomicoADM = tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoADM.HasValue ? tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoADM.Value : 0;
            decimal valorAdministracaoPagaGrupoEconomicoGestor = tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoGestor.HasValue ? tabelaInforme2.ValorAdministracaoPagaGrupoEconomicoGestor.Value : 0;
            decimal valorDespesasOperacionaisGrupoEconomicoGestor = tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoGestor.HasValue ? tabelaInforme2.ValorDespesasOperacionaisGrupoEconomicoGestor.Value : 0;

            decimal totalDespesasFundoMedia = (valorAdministracaoPaga + valorPfeePaga + valorCustodiaPaga + valorOutrasDespesasPagas) / 12m;

            this.valoresTable2.taxaAdministracaoGrupoEconomicoAdministrador = (valorAdministracaoPagaGrupoEconomicoADM / 12m) / totalDespesasFundoMedia;
            this.valoresTable2.despesasGrupoEconomicoAdministrador = (valorDespesasOperacionaisGrupoEconomicoADM / 12m)/ totalDespesasFundoMedia;

            this.valoresTable2.taxaAdministracaoGrupoEconomicoGestor = (valorAdministracaoPagaGrupoEconomicoGestor / 12m)/ totalDespesasFundoMedia;
            this.valoresTable2.despesasGrupoEconomicoGestor = (valorDespesasOperacionaisGrupoEconomicoGestor / 12m)/ totalDespesasFundoMedia;

            this.valoresTable2.totalDespesas = ((valorAdministracaoPagaGrupoEconomicoADM + valorDespesasOperacionaisGrupoEconomicoADM + valorAdministracaoPagaGrupoEconomicoGestor + valorDespesasOperacionaisGrupoEconomicoGestor) / 12m) / ((valorAdministracaoPaga + valorPfeePaga + valorCustodiaPaga + valorOutrasDespesasPagas) / 12m);
            #endregion

            #endregion
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Cotista/ReportListaEtiqueta.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "ReportDesempenhoFundo.resx";
            System.Resources.ResourceManager resources = global::Resources.ReportDesempenhoFundo.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrRichText12 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText11 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText10 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText7 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText4 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText3 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText33 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPageBreak4 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak3 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak2 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText24 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText25 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText9 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText8 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText5 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.parameter1 = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrRichText21 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText22 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText23 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrM13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText19 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrRichText16 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText17 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText6 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrRichText13 = new DevExpress.XtraReports.UI.XRRichText();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrRichText32 = new DevExpress.XtraReports.UI.XRRichText();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText12,
            this.xrRichText11,
            this.xrRichText10,
            this.xrRichText7,
            this.xrRichText4,
            this.xrRichText3,
            this.xrTable2,
            this.xrTable1,
            this.xrRichText33,
            this.xrRichText2,
            this.xrPageBreak4,
            this.xrPageBreak3,
            this.xrPageBreak2,
            this.xrPanel2,
            this.xrRichText9,
            this.xrRichText8,
            this.xrRichText5,
            this.xrLabel9,
            this.xrRichText1,
            this.xrPanel1,
            this.xrTable11,
            this.xrRichText19,
            this.xrTable8,
            this.xrRichText16,
            this.xrRichText17,
            this.xrRichText6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 7395.847F;
            this.Detail.KeepTogether = true;
            this.Detail.MultiColumn.ColumnSpacing = 51F;
            this.Detail.MultiColumn.ColumnWidth = 1016F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrRichText12
            // 
            this.xrRichText12.CanShrink = true;
            this.xrRichText12.Dpi = 254F;
            this.xrRichText12.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 6670.98F);
            this.xrRichText12.Name = "xrRichText12";
            this.xrRichText12.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText12.SerializableRtfString = resources.GetString("xrRichText12.SerializableRtfString");
            this.xrRichText12.SizeF = new System.Drawing.SizeF(1889.927F, 73.93213F);
            this.xrRichText12.StylePriority.UsePadding = false;
            // 
            // xrRichText11
            // 
            this.xrRichText11.CanShrink = true;
            this.xrRichText11.Dpi = 254F;
            this.xrRichText11.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 7056.92F);
            this.xrRichText11.Name = "xrRichText11";
            this.xrRichText11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText11.SerializableRtfString = resources.GetString("xrRichText11.SerializableRtfString");
            this.xrRichText11.SizeF = new System.Drawing.SizeF(1889.927F, 204.6528F);
            this.xrRichText11.StylePriority.UsePadding = false;
            this.xrRichText11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IR6B_PrintOnPage);
            // 
            // xrRichText10
            // 
            this.xrRichText10.CanShrink = true;
            this.xrRichText10.Dpi = 254F;
            this.xrRichText10.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 6776.18F);
            this.xrRichText10.Name = "xrRichText10";
            this.xrRichText10.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText10.SerializableRtfString = resources.GetString("xrRichText10.SerializableRtfString");
            this.xrRichText10.SizeF = new System.Drawing.SizeF(1889.927F, 256.8447F);
            this.xrRichText10.StylePriority.UsePadding = false;
            this.xrRichText10.Visible = false;
            this.xrRichText10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.IR6A_PrintOnPage);
            // 
            // xrRichText7
            // 
            this.xrRichText7.CanShrink = true;
            this.xrRichText7.Dpi = 254F;
            this.xrRichText7.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 6234.9F);
            this.xrRichText7.Name = "xrRichText7";
            this.xrRichText7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText7.SerializableRtfString = resources.GetString("xrRichText7.SerializableRtfString");
            this.xrRichText7.SizeF = new System.Drawing.SizeF(1889.927F, 410.1582F);
            this.xrRichText7.StylePriority.UsePadding = false;
            // 
            // xrRichText4
            // 
            this.xrRichText4.CanShrink = true;
            this.xrRichText4.Dpi = 254F;
            this.xrRichText4.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 6058.75F);
            this.xrRichText4.Name = "xrRichText4";
            this.xrRichText4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText4.SerializableRtfString = resources.GetString("xrRichText4.SerializableRtfString");
            this.xrRichText4.SizeF = new System.Drawing.SizeF(1889.927F, 142.6753F);
            this.xrRichText4.StylePriority.UsePadding = false;
            // 
            // xrRichText3
            // 
            this.xrRichText3.CanShrink = true;
            this.xrRichText3.Dpi = 254F;
            this.xrRichText3.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 5689.51F);
            this.xrRichText3.Name = "xrRichText3";
            this.xrRichText3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText3.SerializableRtfString = resources.GetString("xrRichText3.SerializableRtfString");
            this.xrRichText3.SizeF = new System.Drawing.SizeF(1889.927F, 312.2988F);
            this.xrRichText3.StylePriority.UsePadding = false;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(20.3111F, 2862.045F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow13,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1914.927F, 466F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDespesas2BeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.08561779525054436;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Despesas do fundo pagas ao grupo econômico do administrador (e do gestor, se este" +
                " for diferente)";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.63653547876493755;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Percentual em relação à taxa de despesas em ";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.33364594649646928;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.0585806046693261;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Despesas pagas ao grupo econômico do";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell6.Weight = 0.39012435258309003;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Taxa de administração";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.24641112618184752;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.33364594649646928;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.063086803102964023;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "administrador";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell39.Weight = 0.39012435258309003;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UsePadding = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "Despesas Operacionais e de serviços";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.24641112618184752;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.StylePriority.UsePadding = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.33364594649646928;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.068494247283690454;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "Despesas pagas ao grupo econômico do";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell49.Weight = 0.39012491618855244;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "Taxa de administração";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell42.Weight = 0.24641056257638513;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UsePadding = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.33364594649646928;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.072099223708383314;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.Text = "gestor";
            this.xrTableCell48.Weight = 0.39012431942982756;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UsePadding = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "Despesas operacionais e de serviços";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 0.24641115933510999;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.StylePriority.UsePadding = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.33364594649646928;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow22.Weight = 0.072099223708383314;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            this.xrTableCell46.Text = "Total";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.63653547876493755;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.33364594649646928;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 2308.383F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow1,
            this.xrTableRow37,
            this.xrTableRow14,
            this.xrTableRow38});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1910.238F, 466F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDespesas1BeforePrint);
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.08561779525054436;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "Despesas do Fundo";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell103.Weight = 0.63653547876493755;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UsePadding = false;
            this.xrTableCell104.StylePriority.UseTextAlignment = false;
            this.xrTableCell104.Text = "Percentual em relação ao Patrimônio Líquido diário médio em ";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell104.Weight = 0.33364594649646928;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.0585806046693261;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.StylePriority.UseFont = false;
            this.xrTableCell105.StylePriority.UsePadding = false;
            this.xrTableCell105.StylePriority.UseTextAlignment = false;
            this.xrTableCell105.Text = "Taxas de administração";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell105.Weight = 0.39012435258309003;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell106.StylePriority.UseFont = false;
            this.xrTableCell106.StylePriority.UsePadding = false;
            this.xrTableCell106.StylePriority.UseTextAlignment = false;
            this.xrTableCell106.Text = "Parte Fixa";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell106.Weight = 0.24641112618184752;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.StylePriority.UsePadding = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell107.Weight = 0.33364594649646928;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.063086803102964023;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "(inclui as taxas de administração e de performance, se houver, \r\nde outros fundos" +
                " em que este fundo tenha investido)";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 0.39012435258309003;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Parte Variável (Taxa de Performance)";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.24641112618184752;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.33364594649646928;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell133});
            this.xrTableRow37.Dpi = 254F;
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow37.Weight = 0.068494247283690454;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell132.StylePriority.UseFont = false;
            this.xrTableCell132.StylePriority.UseTextAlignment = false;
            this.xrTableCell132.Text = "Taxa de Custódia";
            this.xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell132.Weight = 0.63653547876493755;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell133.StylePriority.UseFont = false;
            this.xrTableCell133.StylePriority.UsePadding = false;
            this.xrTableCell133.StylePriority.UseTextAlignment = false;
            this.xrTableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell133.Weight = 0.33364594649646928;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell28});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.072099223708383314;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Outras Despesas\r\n(Inclui despesas de serviços, custódia, auditoria, etc)";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.63653547876493755;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.33364594649646928;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow38.Weight = 0.072099223708383314;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell136.StylePriority.UseFont = false;
            this.xrTableCell136.StylePriority.UseTextAlignment = false;
            this.xrTableCell136.Text = "TAXA TOTAL DE DESPESAS";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell136.Weight = 0.63653547876493755;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell137.StylePriority.UseFont = false;
            this.xrTableCell137.StylePriority.UsePadding = false;
            this.xrTableCell137.StylePriority.UseTextAlignment = false;
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell137.Weight = 0.33364594649646928;
            // 
            // xrRichText33
            // 
            this.xrRichText33.CanShrink = true;
            this.xrRichText33.Dpi = 254F;
            this.xrRichText33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12.87162F);
            this.xrRichText33.Name = "xrRichText33";
            this.xrRichText33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText33.SerializableRtfString = resources.GetString("xrRichText33.SerializableRtfString");
            this.xrRichText33.SizeF = new System.Drawing.SizeF(1953.381F, 60.696F);
            this.xrRichText33.StylePriority.UsePadding = false;
            // 
            // xrRichText2
            // 
            this.xrRichText2.CanShrink = true;
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 93.28596F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1957F, 60.696F);
            this.xrRichText2.StylePriority.UsePadding = false;
            this.xrRichText2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco1_PrintOnPage);
            // 
            // xrPageBreak4
            // 
            this.xrPageBreak4.Dpi = 254F;
            this.xrPageBreak4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5555.74F);
            this.xrPageBreak4.Name = "xrPageBreak4";
            // 
            // xrPageBreak3
            // 
            this.xrPageBreak3.Dpi = 254F;
            this.xrPageBreak3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3596.26F);
            this.xrPageBreak3.Name = "xrPageBreak3";
            // 
            // xrPageBreak2
            // 
            this.xrPageBreak2.Dpi = 254F;
            this.xrPageBreak2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2000F);
            this.xrPageBreak2.Name = "xrPageBreak2";
            // 
            // xrPanel2
            // 
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText24,
            this.xrTable7,
            this.xrRichText25});
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(20.3111F, 4419.897F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel2.SizeF = new System.Drawing.SizeF(1914.927F, 1117.775F);
            this.xrPanel2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item9BeforePrint);
            // 
            // xrRichText24
            // 
            this.xrRichText24.CanShrink = true;
            this.xrRichText24.Dpi = 254F;
            this.xrRichText24.LocationFloat = new DevExpress.Utils.PointFloat(29.58573F, 25F);
            this.xrRichText24.Name = "xrRichText24";
            this.xrRichText24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText24.SerializableRtfString = resources.GetString("xrRichText24.SerializableRtfString");
            this.xrRichText24.SizeF = new System.Drawing.SizeF(1885.341F, 500.1162F);
            this.xrRichText24.StylePriority.UsePadding = false;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(34.56927F, 562.9312F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow26,
            this.xrTableRow25,
            this.xrTableRow27});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1880.357F, 316.9995F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableItem9_BeforePrint);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell55,
            this.xrTableCell52});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.574468085106383;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(40, 5, 0, 0, 254F);
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "Simulação das Despesas ";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.74718792127526323;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell55.StylePriority.UsePadding = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "[+3 anos]";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.115370509935462;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "[+5 anos]";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell52.Weight = 0.13744156878927483;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell65,
            this.xrTableCell53});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.574468085106383;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell61.StylePriority.UsePadding = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Saldo bruto acumulado (hipotético - rentabilidade bruta anual de 10%) ";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 0.74718792127526323;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell65.StylePriority.UsePadding = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell65.Weight = 0.115370509935462;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 0.13744156878927483;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell60,
            this.xrTableCell54});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.58510638297872342;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "Despesas previstas (se a TAXA TOTAL DE DESPESAS se mantiver constante)";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell56.Weight = 0.74718792127526323;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell60.StylePriority.UsePadding = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.115370509935462;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell54.StylePriority.UsePadding = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 0.13744156878927483;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell70,
            this.xrTableCell57});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1.6382978723404258;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell66.StylePriority.UsePadding = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "Retorno bruto hipotético após dedução das despesas e do valor do investimento ori" +
                "ginal (antes da incidência de impostos, de taxas de ingresso e/ou saída, ou de t" +
                "axa de performance)";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell66.Weight = 0.74718792127526323;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell70.StylePriority.UsePadding = false;
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell70.Weight = 0.115370509935462;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.13744156878927483;
            // 
            // xrRichText25
            // 
            this.xrRichText25.CanShrink = true;
            this.xrRichText25.Dpi = 254F;
            this.xrRichText25.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 923.0386F);
            this.xrRichText25.Name = "xrRichText25";
            this.xrRichText25.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText25.SerializableRtfString = resources.GetString("xrRichText25.SerializableRtfString");
            this.xrRichText25.SizeF = new System.Drawing.SizeF(1889.927F, 149.1997F);
            this.xrRichText25.StylePriority.UsePadding = false;
            // 
            // xrRichText9
            // 
            this.xrRichText9.CanShrink = true;
            this.xrRichText9.Dpi = 254F;
            this.xrRichText9.LocationFloat = new DevExpress.Utils.PointFloat(37F, 495F);
            this.xrRichText9.Name = "xrRichText9";
            this.xrRichText9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText9.SerializableRtfString = resources.GetString("xrRichText9.SerializableRtfString");
            this.xrRichText9.SizeF = new System.Drawing.SizeF(1913F, 55F);
            this.xrRichText9.StylePriority.UsePadding = false;
            // 
            // xrRichText8
            // 
            this.xrRichText8.CanShrink = true;
            this.xrRichText8.Dpi = 254F;
            this.xrRichText8.LocationFloat = new DevExpress.Utils.PointFloat(37F, 425.95F);
            this.xrRichText8.Name = "xrRichText8";
            this.xrRichText8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText8.SerializableRtfString = resources.GetString("xrRichText8.SerializableRtfString");
            this.xrRichText8.SizeF = new System.Drawing.SizeF(1913.167F, 54.99988F);
            this.xrRichText8.StylePriority.UsePadding = false;
            // 
            // xrRichText5
            // 
            this.xrRichText5.CanShrink = true;
            this.xrRichText5.Dpi = 254F;
            this.xrRichText5.LocationFloat = new DevExpress.Utils.PointFloat(37F, 330.1983F);
            this.xrRichText5.Name = "xrRichText5";
            this.xrRichText5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText5.SerializableRtfString = resources.GetString("xrRichText5.SerializableRtfString");
            this.xrRichText5.SizeF = new System.Drawing.SizeF(1906.795F, 78.48511F);
            this.xrRichText5.StylePriority.UsePadding = false;
            this.xrRichText5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Bloco3_PrintOnPage);
            // 
            // xrLabel9
            // 
            this.xrLabel9.CanShrink = true;
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter1, "Text", "")});
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(4.928293F, 160F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(1948.453F, 61.74199F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabel9.WordWrap = false;
            // 
            // parameter1
            // 
            this.parameter1.Name = "parameter1";
            // 
            // xrRichText1
            // 
            this.xrRichText1.CanShrink = true;
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(33.695F, 247.6377F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1904.039F, 71.1759F);
            this.xrRichText1.StylePriority.UsePadding = false;
            this.xrRichText1.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco2_PrintOnPage);
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText21,
            this.xrRichText22,
            this.xrRichText23});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(27.19323F, 3641.174F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1909F, 760.2725F);
            this.xrPanel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item8BeforePrint);
            // 
            // xrRichText21
            // 
            this.xrRichText21.CanShrink = true;
            this.xrRichText21.Dpi = 254F;
            this.xrRichText21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0009765625F);
            this.xrRichText21.Name = "xrRichText21";
            this.xrRichText21.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText21.SerializableRtfString = resources.GetString("xrRichText21.SerializableRtfString");
            this.xrRichText21.SizeF = new System.Drawing.SizeF(1908.045F, 212.0283F);
            this.xrRichText21.StylePriority.UsePadding = false;
            // 
            // xrRichText22
            // 
            this.xrRichText22.CanShrink = true;
            this.xrRichText22.Dpi = 254F;
            this.xrRichText22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 236.7349F);
            this.xrRichText22.Name = "xrRichText22";
            this.xrRichText22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText22.SerializableRtfString = resources.GetString("xrRichText22.SerializableRtfString");
            this.xrRichText22.SizeF = new System.Drawing.SizeF(1908.045F, 278.428F);
            this.xrRichText22.StylePriority.UsePadding = false;
            this.xrRichText22.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco8A_PrintOnPage);
            // 
            // xrRichText23
            // 
            this.xrRichText23.CanShrink = true;
            this.xrRichText23.Dpi = 254F;
            this.xrRichText23.LocationFloat = new DevExpress.Utils.PointFloat(8.726089F, 530.1311F);
            this.xrRichText23.Name = "xrRichText23";
            this.xrRichText23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText23.SerializableRtfString = resources.GetString("xrRichText23.SerializableRtfString");
            this.xrRichText23.SizeF = new System.Drawing.SizeF(1899.318F, 185.5693F);
            this.xrRichText23.StylePriority.UsePadding = false;
            this.xrRichText23.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco8B_PrintOnPage);
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.Dpi = 254F;
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(33.69497F, 575.47F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow57,
            this.xrTableRow58,
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65});
            this.xrTable11.SizeF = new System.Drawing.SizeF(1901.543F, 677.6763F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item2RentabilidadeMensal_BeforePrint);
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell7});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.06043855536994075;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseFont = false;
            this.xrTableCell151.StylePriority.UseTextAlignment = false;
            this.xrTableCell151.Text = "Mês";
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell151.Weight = 0.09263958566960552;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseFont = false;
            this.xrTableCell152.StylePriority.UseTextAlignment = false;
            this.xrTableCell152.Text = "Rentabilidade(líquida de despesas, mas não de impostos)";
            this.xrTableCell152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell152.Weight = 0.1993419895272836;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseFont = false;
            this.xrTableCell153.StylePriority.UseTextAlignment = false;
            this.xrTableCell153.Text = "Variação Percentual do ";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell153.Weight = 0.21573149196749611;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseFont = false;
            this.xrTableCell154.StylePriority.UseTextAlignment = false;
            this.xrTableCell154.Text = "Desempenho do Fundo como % do ";
            this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell154.Weight = 0.23123417904851079;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Rentabilidade ajustada em função de eventos";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.23123417904851079;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM1,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell8});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow53.Weight = 0.0302192773583879;
            // 
            // xrM1
            // 
            this.xrM1.Dpi = 254F;
            this.xrM1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM1.Name = "xrM1";
            this.xrM1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM1.Text = "Janeiro";
            this.xrM1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM1.Weight = 0.09263958566960552;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell156.Weight = 0.1993419895272836;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell157.StylePriority.UseFont = false;
            this.xrTableCell157.StylePriority.UseTextAlignment = false;
            this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell157.Weight = 0.21573149196749611;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell158.Weight = 0.23123417904851079;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.23123417904851079;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM2,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell9});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow54.Weight = 0.030219276987151211;
            // 
            // xrM2
            // 
            this.xrM2.BackColor = System.Drawing.Color.Transparent;
            this.xrM2.Dpi = 254F;
            this.xrM2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM2.Name = "xrM2";
            this.xrM2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM2.StylePriority.UseBackColor = false;
            this.xrM2.Text = "Fevereiro";
            this.xrM2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM2.Weight = 0.09263958566960552;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell160.StylePriority.UseBackColor = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.StylePriority.UseTextAlignment = false;
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell160.Weight = 0.1993419895272836;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell161.StylePriority.UseFont = false;
            this.xrTableCell161.StylePriority.UseTextAlignment = false;
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell161.Weight = 0.21573149196749611;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.StylePriority.UseTextAlignment = false;
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell162.Weight = 0.23123417904851079;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.23123417904851079;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM3,
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell10});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow55.Weight = 0.030219277098753272;
            // 
            // xrM3
            // 
            this.xrM3.Dpi = 254F;
            this.xrM3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM3.Name = "xrM3";
            this.xrM3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM3.Text = "Março";
            this.xrM3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM3.Weight = 0.09263958566960552;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell164.StylePriority.UseFont = false;
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell164.Weight = 0.1993419895272836;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell165.StylePriority.UseFont = false;
            this.xrTableCell165.StylePriority.UseTextAlignment = false;
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell165.Weight = 0.21573149196749611;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseFont = false;
            this.xrTableCell166.StylePriority.UseTextAlignment = false;
            this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell166.Weight = 0.23123417904851079;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.23123417904851079;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM4,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell13});
            this.xrTableRow56.Dpi = 254F;
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow56.Weight = 0.030219277098753269;
            // 
            // xrM4
            // 
            this.xrM4.BackColor = System.Drawing.Color.Transparent;
            this.xrM4.Dpi = 254F;
            this.xrM4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM4.Name = "xrM4";
            this.xrM4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM4.StylePriority.UseBackColor = false;
            this.xrM4.Text = "Abril";
            this.xrM4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM4.Weight = 0.09263958566960552;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell168.StylePriority.UseBackColor = false;
            this.xrTableCell168.StylePriority.UseFont = false;
            this.xrTableCell168.StylePriority.UseTextAlignment = false;
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell168.Weight = 0.1993419895272836;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell169.Weight = 0.21573149196749611;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseFont = false;
            this.xrTableCell170.StylePriority.UseTextAlignment = false;
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell170.Weight = 0.23123417904851079;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.23123417904851079;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM5,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell14});
            this.xrTableRow57.Dpi = 254F;
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow57.Weight = 0.030219278359501184;
            // 
            // xrM5
            // 
            this.xrM5.Dpi = 254F;
            this.xrM5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM5.Name = "xrM5";
            this.xrM5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM5.StylePriority.UsePadding = false;
            this.xrM5.StylePriority.UseTextAlignment = false;
            this.xrM5.Text = "Maio";
            this.xrM5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM5.Weight = 0.09263958566960552;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell172.StylePriority.UseFont = false;
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell172.Weight = 0.1993419895272836;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell173.StylePriority.UseFont = false;
            this.xrTableCell173.StylePriority.UseTextAlignment = false;
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell173.Weight = 0.21573149196749611;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseFont = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell174.Weight = 0.23123417904851079;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.23123417904851079;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM6,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell15});
            this.xrTableRow58.Dpi = 254F;
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow58.Weight = 0.030219278377746135;
            // 
            // xrM6
            // 
            this.xrM6.BackColor = System.Drawing.Color.Transparent;
            this.xrM6.Dpi = 254F;
            this.xrM6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM6.Name = "xrM6";
            this.xrM6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM6.StylePriority.UseBackColor = false;
            this.xrM6.StylePriority.UsePadding = false;
            this.xrM6.Text = "Junho";
            this.xrM6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM6.Weight = 0.09263958566960552;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell176.StylePriority.UseBackColor = false;
            this.xrTableCell176.StylePriority.UseFont = false;
            this.xrTableCell176.StylePriority.UseTextAlignment = false;
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell176.Weight = 0.1993419895272836;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell177.StylePriority.UseFont = false;
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell177.Weight = 0.21573149196749611;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseFont = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell178.Weight = 0.23123417904851079;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.23123417904851079;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM7,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell16});
            this.xrTableRow59.Dpi = 254F;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow59.Weight = 0.030219278377746128;
            // 
            // xrM7
            // 
            this.xrM7.Dpi = 254F;
            this.xrM7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM7.Name = "xrM7";
            this.xrM7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM7.Text = "Julho";
            this.xrM7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM7.Weight = 0.09263958566960552;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell180.StylePriority.UseFont = false;
            this.xrTableCell180.StylePriority.UseTextAlignment = false;
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell180.Weight = 0.1993419895272836;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell181.StylePriority.UseFont = false;
            this.xrTableCell181.StylePriority.UseTextAlignment = false;
            this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell181.Weight = 0.21573149196749611;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseTextAlignment = false;
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell182.Weight = 0.23123417904851079;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.23123417904851079;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM8,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell17});
            this.xrTableRow60.Dpi = 254F;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow60.Weight = 0.030219278359501174;
            // 
            // xrM8
            // 
            this.xrM8.BackColor = System.Drawing.Color.Transparent;
            this.xrM8.Dpi = 254F;
            this.xrM8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM8.Name = "xrM8";
            this.xrM8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM8.StylePriority.UseBackColor = false;
            this.xrM8.StylePriority.UsePadding = false;
            this.xrM8.Text = "Agosto";
            this.xrM8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM8.Weight = 0.09263958566960552;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell184.StylePriority.UseBackColor = false;
            this.xrTableCell184.StylePriority.UseFont = false;
            this.xrTableCell184.StylePriority.UseTextAlignment = false;
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell184.Weight = 0.1993419895272836;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell185.StylePriority.UseFont = false;
            this.xrTableCell185.StylePriority.UseTextAlignment = false;
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell185.Weight = 0.21573149196749611;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell186.Weight = 0.23123417904851079;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.23123417904851079;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM9,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell18});
            this.xrTableRow61.Dpi = 254F;
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow61.Weight = 0.030219278564460349;
            // 
            // xrM9
            // 
            this.xrM9.Dpi = 254F;
            this.xrM9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM9.Name = "xrM9";
            this.xrM9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM9.StylePriority.UsePadding = false;
            this.xrM9.Text = "Setembro";
            this.xrM9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM9.Weight = 0.09263958566960552;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseTextAlignment = false;
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell188.Weight = 0.1993419895272836;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell189.StylePriority.UseFont = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell189.Weight = 0.21573162355677361;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseFont = false;
            this.xrTableCell190.StylePriority.UseTextAlignment = false;
            this.xrTableCell190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell190.Weight = 0.23123411325387203;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.23123411325387203;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM10,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell19});
            this.xrTableRow62.Dpi = 254F;
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow62.Weight = 0.030219278564460363;
            // 
            // xrM10
            // 
            this.xrM10.BackColor = System.Drawing.Color.Transparent;
            this.xrM10.Dpi = 254F;
            this.xrM10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM10.Name = "xrM10";
            this.xrM10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM10.StylePriority.UseBackColor = false;
            this.xrM10.Text = "Outubro";
            this.xrM10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM10.Weight = 0.09263958566960552;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell192.StylePriority.UseBackColor = false;
            this.xrTableCell192.StylePriority.UseFont = false;
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell192.Weight = 0.19934198978322673;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell193.Weight = 0.2157316411571773;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseFont = false;
            this.xrTableCell194.StylePriority.UseTextAlignment = false;
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell194.Weight = 0.2312341043256986;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.2312341043256986;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM11,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell20});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow63.Weight = 0.030219276987151204;
            // 
            // xrM11
            // 
            this.xrM11.BackColor = System.Drawing.Color.Transparent;
            this.xrM11.Dpi = 254F;
            this.xrM11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM11.Name = "xrM11";
            this.xrM11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM11.Text = "Novembro";
            this.xrM11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM11.Weight = 0.09263958566960552;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell196.StylePriority.UseFont = false;
            this.xrTableCell196.StylePriority.UseTextAlignment = false;
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell196.Weight = 0.19934198978322673;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell197.StylePriority.UseFont = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell197.Weight = 0.2157316411571773;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseFont = false;
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell198.Weight = 0.2312341043256986;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell20.Weight = 0.2312341043256986;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrM13,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell21});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow64.Weight = 0.030219278564460356;
            // 
            // xrM13
            // 
            this.xrM13.BackColor = System.Drawing.Color.Transparent;
            this.xrM13.Dpi = 254F;
            this.xrM13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrM13.Name = "xrM13";
            this.xrM13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrM13.StylePriority.UseBackColor = false;
            this.xrM13.Text = "Dezembro";
            this.xrM13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrM13.Weight = 0.09263958566960552;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell200.BorderColor = System.Drawing.Color.Black;
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell200.StylePriority.UseBackColor = false;
            this.xrTableCell200.StylePriority.UseBorderColor = false;
            this.xrTableCell200.StylePriority.UseFont = false;
            this.xrTableCell200.StylePriority.UseTextAlignment = false;
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell200.Weight = 0.1993419895272836;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell201.Weight = 0.21573149196749611;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.StylePriority.UseFont = false;
            this.xrTableCell202.StylePriority.UseTextAlignment = false;
            this.xrTableCell202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell202.Weight = 0.23123417904851079;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell21.Weight = 0.23123417904851079;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell22});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow65.Weight = 0.032663499977084259;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell203.Text = "12 meses";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell203.Weight = 0.09263958566960552;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell204.StylePriority.UseFont = false;
            this.xrTableCell204.StylePriority.UseTextAlignment = false;
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell204.Weight = 0.1993419895272836;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseTextAlignment = false;
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell205.Weight = 0.21573149196749611;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseFont = false;
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell206.Weight = 0.23123417904851079;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 0.23123417904851079;
            // 
            // xrRichText19
            // 
            this.xrRichText19.CanShrink = true;
            this.xrRichText19.Dpi = 254F;
            this.xrRichText19.LocationFloat = new DevExpress.Utils.PointFloat(27.19311F, 1283F);
            this.xrRichText19.Name = "xrRichText19";
            this.xrRichText19.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText19.SerializableRtfString = resources.GetString("xrRichText19.SerializableRtfString");
            this.xrRichText19.SizeF = new System.Drawing.SizeF(1929.807F, 62.31836F);
            this.xrRichText19.StylePriority.UsePadding = false;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(29.99462F, 1373.091F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32});
            this.xrTable8.SizeF = new System.Drawing.SizeF(1908.806F, 302.3325F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Item2RentabilidadeAnual_BeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell23});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.068125776975298438;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell11.CanGrow = false;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Ano";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.092639552772286152;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "Rentabilidade (líquida de despesas, mas não de impostos)";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.199342022424603;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Variação Percentual do ";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 0.21573145907017671;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "Desempenho do Fundo como % do ";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell59.Weight = 0.23123419549717048;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Rentabilidade ajustada em função de eventos";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.23123419549717048;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell67,
            this.xrTableCell24});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 0.030015169161471053;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell62.Text = "Ano0";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell62.Weight = 0.092639552772286152;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.199342022424603;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.21573145907017671;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.23123419549717048;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell24.Weight = 0.23123419549717048;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell71,
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow29.Weight = 0.030015169691102815;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell68.StylePriority.UseBackColor = false;
            this.xrTableCell68.Text = "Ano1";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.092639552772286152;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell69.StylePriority.UseBackColor = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.199342022424603;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.21573145907017671;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.2312341954971705;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.2312341954971705;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell29});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow30.Weight = 0.030015169542689787;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell73.Text = "Ano2";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell73.Weight = 0.092639552772286152;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell74.Weight = 0.199342022424603;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell75.Weight = 0.21573145907017671;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.23123419549717048;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.23123419549717048;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell33});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow31.Weight = 0.030015170905770591;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell77.StylePriority.UseBackColor = false;
            this.xrTableCell77.Text = "Ano3";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell77.Weight = 0.092639552772286152;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell78.StylePriority.UseBackColor = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell78.Weight = 0.199342022424603;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell79.StylePriority.UseFont = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell79.Weight = 0.21573145907017671;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell80.Weight = 0.23123419549717048;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.23123419549717048;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell32,
            this.xrTableCell34});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow32.Weight = 0.030015170367003484;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell81.StylePriority.UsePadding = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "Ano4";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell81.Weight = 0.092639552772286152;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell82.Weight = 0.199342022424603;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.21573145907017671;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.23123419549717048;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 0.23123419549717048;
            // 
            // xrRichText16
            // 
            this.xrRichText16.CanShrink = true;
            this.xrRichText16.Dpi = 254F;
            this.xrRichText16.LocationFloat = new DevExpress.Utils.PointFloat(20.31102F, 1737.609F);
            this.xrRichText16.Name = "xrRichText16";
            this.xrRichText16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText16.SerializableRtfString = resources.GetString("xrRichText16.SerializableRtfString");
            this.xrRichText16.SizeF = new System.Drawing.SizeF(1918.49F, 264.3915F);
            this.xrRichText16.StylePriority.UsePadding = false;
            // 
            // xrRichText17
            // 
            this.xrRichText17.CanShrink = true;
            this.xrRichText17.Dpi = 254F;
            this.xrRichText17.LocationFloat = new DevExpress.Utils.PointFloat(20.31102F, 2028.65F);
            this.xrRichText17.Name = "xrRichText17";
            this.xrRichText17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText17.SerializableRtfString = resources.GetString("xrRichText17.SerializableRtfString");
            this.xrRichText17.SizeF = new System.Drawing.SizeF(1914.927F, 232.0811F);
            this.xrRichText17.StylePriority.UseFont = false;
            this.xrRichText17.StylePriority.UsePadding = false;
            this.xrRichText17.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Bloco3Despesas_PrintOnPage);
            // 
            // xrRichText6
            // 
            this.xrRichText6.CanShrink = true;
            this.xrRichText6.Dpi = 254F;
            this.xrRichText6.LocationFloat = new DevExpress.Utils.PointFloat(45.31119F, 5568.721F);
            this.xrRichText6.Name = "xrRichText6";
            this.xrRichText6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText6.SerializableRtfString = resources.GetString("xrRichText6.SerializableRtfString");
            this.xrRichText6.SizeF = new System.Drawing.SizeF(1889.927F, 73.93213F);
            this.xrRichText6.StylePriority.UsePadding = false;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(1850F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(87.86926F, 40.00014F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IdPessoa")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(42F, 21F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Endereco")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(42F, 85F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Numero")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(339F, 85F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Complemento")});
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(614F, 85F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Bairro")});
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(42F, 169F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Cidade")});
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(42F, 254F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 64F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Uf")});
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(42F, 339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 63F);
            this.xrLabel7.Text = "xrLabel7";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText13});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 201.8485F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Visible = false;
            // 
            // xrRichText13
            // 
            this.xrRichText13.CanShrink = true;
            this.xrRichText13.Dpi = 254F;
            this.xrRichText13.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 75F);
            this.xrRichText13.Name = "xrRichText13";
            this.xrRichText13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrRichText13.SerializableRtfString = resources.GetString("xrRichText13.SerializableRtfString");
            this.xrRichText13.SizeF = new System.Drawing.SizeF(1953.381F, 60.696F);
            this.xrRichText13.StylePriority.UsePadding = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText32,
            this.xrPageInfo3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 66.27264F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrRichText32
            // 
            this.xrRichText32.CanGrow = false;
            this.xrRichText32.Dpi = 254F;
            this.xrRichText32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrRichText32.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 1.227315F);
            this.xrRichText32.Name = "xrRichText32";
            this.xrRichText32.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
            this.xrRichText32.SerializableRtfString = resources.GetString("xrRichText32.SerializableRtfString");
            this.xrRichText32.SizeF = new System.Drawing.SizeF(1804.626F, 64.99991F);
            this.xrRichText32.StylePriority.UseFont = false;
            this.xrRichText32.StylePriority.UsePadding = false;
            this.xrRichText32.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Rodape_PrintOnPage);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Name = "ReportFooter";
            // 
            // ReportDesempenhoFundo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.PageFooter});
            this.DetailPrintCount = 1;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(101, 78, 150, 0);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter1});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.RequestParameters = false;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region Valores da Table1
        class ValoresTable1
        {
            public decimal? despesaFixa;
            public decimal? despesaVariavel;
            public decimal? outrasDespesas;
            public decimal? totalDespesas;
            public decimal? taxaCustodia;
        }
        private ValoresTable1 valoresTable1 = new ValoresTable1();
        #endregion
        //
        #region Valores da Table2
        class ValoresTable2
        {
            public decimal? taxaAdministracaoGrupoEconomicoAdministrador;
            public decimal? despesasGrupoEconomicoAdministrador;
            //
            public decimal? taxaAdministracaoGrupoEconomicoGestor;
            public decimal? despesasGrupoEconomicoGestor;
            //
            public decimal? totalDespesas;
        }
        private ValoresTable2 valoresTable2 = new ValoresTable2();
        #endregion

        #region Funções Personalizadas

        private decimal CalculaDespesas()
        {
            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilMes(_dataInicio);
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(_dataFim);

            #region
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == this.idCliente,
                                  liquidacaoQuery.DataVencimento.Between(dataInicio, dataFim),
                                  liquidacaoQuery.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaFiscalizacaoCVM,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros
                                                            ));
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;
            return despesas;
        }

        private void Bloco1_PrintOnPage(object sender, PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", this.cliente.Apelido.Trim());
        }

        private void Bloco2_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", this.data.Year.ToString());
        }

        private void Bloco3_PrintOnPage(object sender, PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", this.cliente.Nome.Trim());
        }

        #region Rentabilidade Mensal/Anual

        #region Preenche os valores das tables
        private void Item2RentabilidadeMensal_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[2]).Text += descricaoIndice;

            string texto = "Desempenho do Fundo como % do " + descricaoIndice;
            if (carteira.UpToIndiceByIdIndiceBenchmark != null)
            {
                texto = carteira.UpToIndiceByIdIndiceBenchmark.Tipo == (byte)TipoIndice.Decimal ? "Contribuição em relação ao índice de Referência (Rentabilidade do Fundo - Rentabilidade do índice)" : "Desempenho do Fundo como % do " + descricaoIndice;
            }

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[3]).Text = texto;
            int ano = this.data.Year;
            int anoAnt = ano;
            if (this.data.Month == 6)
            {
                anoAnt = ano - 1;

                ((XRTableCell)((XRTableRow)table.Rows[1]).Cells[0]).Text = string.Format("Jul/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[2]).Cells[0]).Text = string.Format("Ago/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[3]).Cells[0]).Text = string.Format("Set/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[4]).Cells[0]).Text = string.Format("Out/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[5]).Cells[0]).Text = string.Format("Nov/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[6]).Cells[0]).Text = string.Format("Dez/{0}", anoAnt);
                ((XRTableCell)((XRTableRow)table.Rows[7]).Cells[0]).Text = string.Format("Jan/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[8]).Cells[0]).Text = string.Format("Fev/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[9]).Cells[0]).Text = string.Format("Mar/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[10]).Cells[0]).Text = string.Format("Abr/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[11]).Cells[0]).Text = string.Format("Mai/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[12]).Cells[0]).Text = string.Format("Jun/{0}", ano);

            }
            else
            {
                ((XRTableCell)((XRTableRow)table.Rows[1]).Cells[0]).Text = string.Format("Jan/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[2]).Cells[0]).Text = string.Format("Fev/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[3]).Cells[0]).Text = string.Format("Mar/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[4]).Cells[0]).Text = string.Format("Abr/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[5]).Cells[0]).Text = string.Format("Mai/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[6]).Cells[0]).Text = string.Format("Jun/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[7]).Cells[0]).Text = string.Format("Jul/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[8]).Cells[0]).Text = string.Format("Ago/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[9]).Cells[0]).Text = string.Format("Set/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[10]).Cells[0]).Text = string.Format("Out/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[11]).Cells[0]).Text = string.Format("Nov/{0}", ano);
                ((XRTableCell)((XRTableRow)table.Rows[12]).Cells[0]).Text = string.Format("Dez/{0}", ano);
            }
            //
            //

            CalculoMedida.EstatisticaRetornoMensal cJan = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 1; });
            CalculoMedida.EstatisticaRetornoMensal cFev = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 2; });
            CalculoMedida.EstatisticaRetornoMensal cMar = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 3; });
            CalculoMedida.EstatisticaRetornoMensal cAbr = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 4; });
            CalculoMedida.EstatisticaRetornoMensal cMai = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 5; });
            CalculoMedida.EstatisticaRetornoMensal cJun = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 6; });
            CalculoMedida.EstatisticaRetornoMensal cJul = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 7; });
            CalculoMedida.EstatisticaRetornoMensal cAgo = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 8; });
            CalculoMedida.EstatisticaRetornoMensal cSet = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 9; });
            CalculoMedida.EstatisticaRetornoMensal cOut = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 10; });
            CalculoMedida.EstatisticaRetornoMensal cNov = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 11; });
            CalculoMedida.EstatisticaRetornoMensal cDez = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 12; });
                                              
            //
            decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);
            decimal? rentabilidadeAmortizada = CalculaRentabilidadeAmortizada12Meses(listaRetornosMensais);

            #region Preenche com -
            for (int k = 1; k <= 12; k++)
            {
                for (int p = 1; p <= 4; p++)
                {
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                }
            }
            #endregion

            #region Janeiro
            if (cJan != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[1]), cJan.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), cJan.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[3]), cJan.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[4]), cJan.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Fevereiro
            if (cFev != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[1]), cFev.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), cFev.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[3]), cFev.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[4]), cFev.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Março
            if (cMar != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[1]), cMar.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[2]), cMar.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[3]), cMar.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[4]), cMar.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Abril
            if (cAbr != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[1]), cAbr.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[2]), cAbr.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[3]), cAbr.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[4]), cAbr.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Maio
            if (cMai != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), cMai.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[2]), cMai.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[3]), cMai.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[4]), cMai.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Junho
            if (cJun != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[1]), cJun.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[2]), cJun.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[3]), cJun.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[6]).Cells[4]), cJun.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Julho
            if (cJul != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[1]), cJul.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[2]), cJul.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[3]), cJul.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[7]).Cells[4]), cJul.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Agosto
            if (cAgo != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[1]), cAgo.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[2]), cAgo.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[3]), cAgo.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[8]).Cells[4]), cAgo.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Setembro
            if (cSet != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[1]), cSet.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[2]), cSet.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[3]), cSet.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[9]).Cells[4]), cSet.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Outubro
            if (cOut != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[1]), cOut.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[2]), cOut.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[3]), cOut.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[10]).Cells[4]), cOut.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Novembro
            if (cNov != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[1]), cNov.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[2]), cNov.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[3]), cNov.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[11]).Cells[4]), cNov.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Dezembro
            if (cDez != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[1]), cDez.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[2]), cDez.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[3]), cDez.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[12]).Cells[4]), cDez.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion

            decimal? rentabilidade12MesesDiferencial = null;
            if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue)
            {
                if (rentabilidade12MesesIndice != 0)
                {
                    rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
                }
            }

            /* Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial */
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[4]), rentabilidadeAmortizada, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

            #endregion
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12Meses(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades)
        {

            // Lista com Retornos Mensais não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 1; i < rentabilidades.Count; i++)
            {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.Retorno.HasValue)
                {
                    rentabilidadesM.Add(c.Retorno.Value);
                }
            }

            if (rentabilidadesM.Count == 0)
            {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++)
            {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnual = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++)
            {
                if (j == 0)
                {
                    rentabilidadeAnual = rentabilidadesFator[j];
                }
                else
                {
                    rentabilidadeAnual *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnual - 1) * 100;
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidadeAmortizada12Meses(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades)
        {
            decimal? rentab = 1M;

            for (int i = 1; i < rentabilidades.Count; i++)
            {
                rentab *= (rentabilidades[i].RetornoAmortizado + 1M);
            }

            rentab -= 1M;

            return rentab;

        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses do Indice baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12MesesIndice(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades)
        {

            // Lista com Retornos Mensais do Benchamark não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 1; i < rentabilidades.Count; i++)
            {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.RetornoBenchmark.HasValue)
                {
                    rentabilidadesM.Add(c.RetornoBenchmark.Value);
                }
            }

            if (rentabilidadesM.Count == 0)
            {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++)
            {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnualIndice = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++)
            {
                if (j == 0)
                {
                    rentabilidadeAnualIndice = rentabilidadesFator[j];
                }
                else
                {
                    rentabilidadeAnualIndice *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnualIndice - 1) * 100;
        }

        /// <summary>
        /// Tabela rentabilidade Anual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item2RentabilidadeAnual_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[2]).Text += descricaoIndice;

            string texto = "Desempenho do Fundo como % do " + descricaoIndice;
            if (carteira.UpToIndiceByIdIndiceBenchmark != null)
            {
                texto = carteira.UpToIndiceByIdIndiceBenchmark.Tipo == (byte)TipoIndice.Decimal ? "Contribuição em relação ao índice de Referência (Rentabilidade do Fundo - Rentabilidade do índice)" : "Desempenho do Fundo como % do " + descricaoIndice;
            }

            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[3]).Text = texto;

            //
            // Informações Gerais
            ((XRTableCell)((XRTableRow)table.Rows[1]).Cells[0]).Text = this.ultimos_5_Anos[4].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[2]).Cells[0]).Text = this.ultimos_5_Anos[3].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[3]).Cells[0]).Text = this.ultimos_5_Anos[2].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[4]).Cells[0]).Text = this.ultimos_5_Anos[1].ToString();
            ((XRTableCell)((XRTableRow)table.Rows[5]).Cells[0]).Text = this.ultimos_5_Anos[0].ToString();
            //

            #region Preenche com -
            for (int k = 1; k <= 5; k++)
            {
                for (int p = 1; p <= 3; p++)
                {
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                    ((XRTableCell)((XRTableRow)table.Rows[k]).Cells[p]).Text = "-";
                }
            }
            #endregion

            //
            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            #region Ano0
            if (ano0 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[1]), ano0.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), ano0.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[3]), ano0.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[4]), ano0.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano1
            if (ano1 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[1]), ano1.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), ano1.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[3]), ano1.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[4]), ano1.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano2
            if (ano2 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[1]), ano2.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[2]), ano2.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[3]), ano2.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[4]), ano2.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano3
            if (ano3 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[1]), ano3.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[2]), ano3.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[3]), ano3.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[4]), ano3.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #region Ano4
            if (ano4 != null)
            {
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), ano4.Retorno, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[2]), ano4.RetornoBenchmark, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[3]), ano4.RetornoDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[4]), ano4.RetornoAmortizado, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            }
            #endregion
            #endregion
        }

        #endregion
        #endregion

        private void Bloco8A_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRRichText valor = sender as XRRichText;

            string ano ;
            string proxAno ;
            DateTime dataInicio ;
            DateTime dataFim  ;

            if (this.data.Month == 6)
            {
                ano = Convert.ToString(this.data.Year - 1);
                proxAno = Convert.ToString(this.data.Year);

                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(this.data.Year -1, 1, 1));
                dataFim = Calendario.RetornaPrimeiroDiaUtilAno(this.data);
            }
            else
            {
                ano = Convert.ToString(this.data.Year);
                proxAno = Convert.ToString(this.data.Year + 1);

                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(this.data);
                dataFim = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(this.data.Year + 1, 1, 1));
            }
            valor.Rtf = valor.Rtf.Replace("[#1]", ano);
            valor.Rtf = valor.Rtf.Replace("[#2]", proxAno);

            
            //
            HistoricoCota historicoCota1 = new HistoricoCota();
            historicoCota1.LoadByPrimaryKey(dataInicio, this.idCliente.Value);
            decimal cotaFechamentoInicio = historicoCota1.CotaFechamento.HasValue ? historicoCota1.CotaFechamento.Value : 0;
            //
            HistoricoCota historicoCota2 = new HistoricoCota();
            historicoCota2.LoadByPrimaryKey(dataFim, this.idCliente.Value);
            decimal cotaFechamentoFim = historicoCota2.CotaFechamento.HasValue ? historicoCota2.CotaFechamento.Value : 0;

            decimal valorBruto = 0;
            if (historicoCota1.CotaFechamento.HasValue)
            {
                valorBruto = 1000 * (cotaFechamentoFim / cotaFechamentoInicio);
            }
            else
            {
                valorBruto = 1000 * 1;
            }

            decimal valorIR = 0;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            //if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Acoes)
            //{
            //    valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.15M;
            //}
            //else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Isento)
            //{
            //    valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0M;
            //}
            //else
            //{
            //    valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.175M;
            //}

            if (carteira.TipoCarteira.Value == (int)TipoCarteiraFundo.RendaVariavel)
            {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.15M;
            }
            else if (carteira.TipoCarteira == (int)TipoCarteiraFundo.RendaFixa)
            {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.175M;
            }
            else
            {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.2M;
            }

            decimal valorLiq = valorBruto - valorIR;

            valor.Rtf = valor.Rtf.Replace("[#3]", valorLiq.ToString("N2"));
            valor.Rtf = valor.Rtf.Replace("[#4]", valorIR.ToString("N2"));

            _valorBruto = valorBruto;

            //
            valor.Rtf = valor.Rtf.Replace("[#5]", "");
            //Não implementado
            // Se tem Taxa de Entrada/Saida
            //if ( !String.IsNullOrEmpty(lamina.TaxaEntrada) && !String.IsNullOrEmpty(lamina.TaxaSaida) ) {
            //    // TODO ajuste sobre performance individual
            //    string texto = "A taxa de ingresso teria custado R$ " + lamina.TaxaEntrada + ", a taxa de saída teria custado R$ " +lamina.TaxaSaida+ ", e o ajuste sobre performance individual teria custado R$ .";                
            //    valor.Rtf = valor.Rtf.Replace("[#5]", texto);                
            //}
            //else {
            //    valor.Rtf = valor.Rtf.Replace("[#5]", "");
            //}                                    
        }

        private void Bloco8B_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRRichText valor = sender as XRRichText;

            decimal? despesas = 0;
            if (this.valoresTable1.totalDespesas.HasValue)
            {
                despesas = this.valoresTable1.totalDespesas.Value * _valorBruto; ;
            }
            //decimal despesas = this.CalculaDespesas();
            valor.Rtf = valor.Rtf.Replace("[#1]", despesas.Value.ToString("N2"));
        }

        private void Item8BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRPanel panel = sender as XRPanel;

            //Carteira carteira = new Carteira();
            //carteira.LoadByPrimaryKey(this.idCliente.Value);

            //DateTime dataInicio = carteira.DataInicioCota.Value;
            //DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            //// Subtrair maior de menor
            //TimeSpan resultado = dataFim.Subtract(dataInicio);
            //bool maisDeUmAno = (resultado.Days >= 365);
            ////
            //panel.Visible = maisDeUmAno;

            //if (!panel.Visible) {
            //    this.Detail.HeightF = this.Detail.HeightF - 760.27F;
            //}
        }

        private void Item9BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRPanel panel = sender as XRPanel;

            //Carteira carteira = new Carteira();
            //carteira.LoadByPrimaryKey(this.idCliente.Value);

            //DateTime dataInicio = carteira.DataInicioCota.Value;
            //DateTime dataFim = Calendario.RetornaUltimoDiaUtilMes(this.data, 0);

            //// Subtrair maior de menor
            //TimeSpan resultado = dataFim.Subtract(dataInicio);
            //bool maisDeUmAno = (resultado.Days >= 365);
            ////
            //panel.Visible = maisDeUmAno;

            //if (!panel.Visible) {
            //    this.Detail.HeightF = this.Detail.HeightF - 1368.95F;
            //}
        }

        private void TableItem9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable table = sender as XRTable;
            XRTableRow linha1 = table.Rows[1];
            XRTableRow linha2 = table.Rows[2];
            XRTableRow linha3 = table.Rows[3];
            //
            ((XRTableCell)linha1.Cells[1]).Text = "R$ 1.331,00";
            ((XRTableCell)linha1.Cells[2]).Text = "R$ 1.610,51";
            //

            decimal? tresAnos = 0;
            decimal? cincoAnos = 0;

            decimal? bruto1 = 1331m;
            decimal? bruto2 = 1610.51m;
            decimal vl = 1000m;
            decimal total = 0;
            for (int i = 0; i < 5; i++)
            {
                vl = (vl * 1.10m);
                cincoAnos = cincoAnos + (vl * this.valoresTable1.totalDespesas.Value);
                if (i == 2)
                    tresAnos = cincoAnos;
            }

            ((XRTableCell)linha2.Cells[1]).Text = "R$ " + tresAnos.Value.ToString("N2");
            ((XRTableCell)linha2.Cells[2]).Text = "R$ " + cincoAnos.Value.ToString("N2");
            //
            decimal retornoBruto1 = 331M - tresAnos.Value;
            decimal retornoBruto2 = 610.51M - cincoAnos.Value;

            ((XRTableCell)linha3.Cells[1]).Text = "R$ " + retornoBruto1.ToString("N2");
            ((XRTableCell)linha3.Cells[2]).Text = "R$ " + retornoBruto2.ToString("N2");
        }

        private void Bloco3Despesas_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRRichText valor = sender as XRRichText;
            valor.Rtf = valor.Rtf.Replace("[#1]", _dataInicio.ToString("d"));
            valor.Rtf = valor.Rtf.Replace("[#2]", _dataFim.ToString("d"));
        }

        private void IR6A_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);

            valor.Visible = carteira.TipoCarteira == (int)TipoCarteiraFundo.RendaFixa;
        }

        private void IR6B_PrintOnPage(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.idCliente.Value);
            //
            valor.Visible = carteira.TipoCarteira == (int)TipoCarteiraFundo.RendaVariavel;
            //valor.HeightF = this.Detail.HeightF - 1368.95F;

            if (valor.Visible == true)
            {
                valor.LocationFloat = new DevExpress.Utils.PointFloat(25F, 6776.18F);
            }
        }

        #region Despesas
        private void TableDespesas1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[1]).Text += this.ano;
            //
            // Valores
            if (this.valoresTable1.despesaFixa.HasValue) { this.valoresTable1.despesaFixa = Math.Abs(this.valoresTable1.despesaFixa.Value); }
            if (this.valoresTable1.despesaVariavel.HasValue) { this.valoresTable1.despesaVariavel = Math.Abs(this.valoresTable1.despesaVariavel.Value); }
            if (this.valoresTable1.outrasDespesas.HasValue) { this.valoresTable1.outrasDespesas = Math.Abs(this.valoresTable1.outrasDespesas.Value); }
            if (this.valoresTable1.taxaCustodia.HasValue) { this.valoresTable1.taxaCustodia = Math.Abs(this.valoresTable1.taxaCustodia.Value); }

            //
            if (this.valoresTable1.totalDespesas.HasValue) { this.valoresTable1.totalDespesas = Math.Abs(this.valoresTable1.totalDespesas.Value); }

            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), this.valoresTable1.despesaFixa, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), this.valoresTable1.despesaVariavel, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[1]), this.valoresTable1.taxaCustodia, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[1]), this.valoresTable1.outrasDespesas, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            //
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), this.valoresTable1.totalDespesas, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            //            
            #endregion
        }

        private void TableDespesas2BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            #region Preenche Table
            XRTable table = sender as XRTable;
            //
            ((XRTableCell)((XRTableRow)table.Rows[0]).Cells[1]).Text += this.ano;
            //
            // Valores
            if (this.valoresTable2.taxaAdministracaoGrupoEconomicoAdministrador.HasValue) { this.valoresTable2.taxaAdministracaoGrupoEconomicoAdministrador = Math.Abs(this.valoresTable2.taxaAdministracaoGrupoEconomicoAdministrador.Value); }
            if (this.valoresTable2.despesasGrupoEconomicoAdministrador.HasValue) { this.valoresTable2.despesasGrupoEconomicoAdministrador = Math.Abs(this.valoresTable2.despesasGrupoEconomicoAdministrador.Value); }
            //
            if (this.valoresTable2.taxaAdministracaoGrupoEconomicoGestor.HasValue) { this.valoresTable2.taxaAdministracaoGrupoEconomicoGestor = Math.Abs(this.valoresTable2.taxaAdministracaoGrupoEconomicoGestor.Value); }
            if (this.valoresTable2.despesasGrupoEconomicoGestor.HasValue) { this.valoresTable2.despesasGrupoEconomicoGestor = Math.Abs(this.valoresTable2.despesasGrupoEconomicoGestor.Value); }
            //
            if (this.valoresTable2.totalDespesas.HasValue) { this.valoresTable2.totalDespesas = Math.Abs(this.valoresTable2.totalDespesas.Value); }

            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[1]).Cells[2]), this.valoresTable2.taxaAdministracaoGrupoEconomicoAdministrador, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[2]).Cells[2]), this.valoresTable2.despesasGrupoEconomicoAdministrador, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[3]).Cells[2]), this.valoresTable2.taxaAdministracaoGrupoEconomicoGestor, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[4]).Cells[2]), this.valoresTable2.despesasGrupoEconomicoGestor, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            //
            ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[5]).Cells[1]), this.valoresTable2.totalDespesas, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            //            
            #endregion
        }
        #endregion

        private void Rodape_PrintOnPage(object sender, PrintEventArgs e)
        {
            XRRichText valor = sender as XRRichText;

            //#region Dados Cliente

            //PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");

            //p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro, p.Cidade, p.Cep, p.Uf, p.IdPessoa);
            //p.Where(p.RecebeCorrespondencia == "S" & p.IdPessoa == this.idCliente.Value);

            //PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            //pessoaEnderecoCollection.Load(p);

            //string enderecoCompletoCliente = "";
            ////
            //if (pessoaEnderecoCollection.HasData) {
            //    #region EnderecoCompleto
            //    string enderecoCliente = pessoaEnderecoCollection[0].str.Endereco.Trim();

            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim())) {
            //        enderecoCliente += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + "  - " + pessoaEnderecoCollection[0].str.Complemento.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim())) {
            //        enderecoCliente += " " + pessoaEnderecoCollection[0].str.Bairro.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim())) {
            //        enderecoCliente += " " + Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim())) {
            //        enderecoCliente += " " + pessoaEnderecoCollection[0].str.Cidade.Trim();
            //    }
            //    if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
            //        enderecoCliente += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
            //    }

            //    Pessoa pes = new Pessoa();
            //    pes.LoadByPrimaryKey(pessoaEnderecoCollection[0].IdPessoa.Value);

            //    if (!String.IsNullOrEmpty(pes.DDD_Fone)) {
            //        enderecoCliente += " - " + pes.DDD_Fone;
            //    }

            //    enderecoCompletoCliente = enderecoCliente;
            //    #endregion
            //}
            //#endregion

            Carteira b = new Carteira();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(b.Query.IdCarteira);
            campos.Add(b.Query.Nome);
            b.LoadByPrimaryKey(campos, this.idCliente.Value);
            //

            #region CNPJ
            string cpfcnpj = "";
            //
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.idCliente.Value);

            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj))
            {
                cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }
            #endregion

            string texto = b.Nome.Trim();
            if (!String.IsNullOrEmpty(cpfcnpj))
            {
                texto += " - " + cpfcnpj.Trim();
            }

            valor.Rtf = valor.Rtf.Replace("[#1]", !String.IsNullOrEmpty(texto) ? texto.Trim() : "-");
        }

        #endregion
    }
}