﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Drawing;
using Financial.WebConfigConfiguration;
using System.Web.Hosting;
using System.IO;
using Financial.InvestidorCotista;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Util;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportHeaderInformeRendimentosPF
    /// </summary>
    public class SubReportHeaderInformeRendimentosPF : DevExpress.XtraReports.UI.XtraReport {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable2;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell12;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell2;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportHeaderInformeRendimentosPF() {
            InitializeComponent();
        }
       
        private int idCotista;

        public int IdCotista {
            get { return IdCotista; }
            set { IdCotista = value; }
        }

        public void PersonalInitialize(int idCotista) {
            this.idCotista = idCotista;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            string resourceFileName = "SubReportHeaderInformeRendimentosPF.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 150F;
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 150F;
            this.BottomMargin.Name = "BottomMargin";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(4.000039F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(1840F, 232.5417F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrPictureBox1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 232.5417F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(116)))), ((int)(((byte)(189)))));
            this.xrTable2.BorderColor = System.Drawing.Color.Transparent;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrTable2.ForeColor = System.Drawing.Color.White;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(956.0209F, 35.58334F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow6,
            this.xrTableRow7});
            this.xrTable2.SizeF = new System.Drawing.SizeF(862.9791F, 122F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseForeColor = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBackColor = false;
            this.xrTableRow4.Weight = 0.12500002483527262;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(116)))), ((int)(((byte)(189)))));
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Cotista";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 1;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CotistaBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.17578127545615441;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "EnderecoBairro";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 1;
            this.xrTableCell12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.EnderecoBairroBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.17578127545615435;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "CepCidade";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CepCidadeBeforePrint);
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // SubReportHeaderInformeRendimentosPF
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.PageHeader,
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Khmer UI", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(0, 315, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubReportHeaderInformeRendimentosPFBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        /// <summary>
        /// Se existir imagem capa_extrato_cotista carrega
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubReportHeaderInformeRendimentosPFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {

            string imagemHeaderInformeRendimento = "~/imagensPersonalizadas/" + "header_informe_rendimento_" + WebConfig.AppSettings.Cliente + ".png";

            string pathImagem = HostingEnvironment.MapPath(imagemHeaderInformeRendimento);

            // Se existe path carrega imagem Personalizada
            if (File.Exists(pathImagem)) {
                this.xrPictureBox1.ImageUrl = imagemHeaderInformeRendimento;
            }
        }

        private void CotistaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCotista = sender as XRTableCell;
            xrTableCellCotista.Text = "";
            // Carrega o Cotista
            Cotista cotista = new Cotista();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cotista.Query.Nome);
            if (cotista.LoadByPrimaryKey(campos, this.idCotista)) {
                xrTableCellCotista.Text = cotista.Nome.Trim().ToUpper();
            }
        }

        private void EnderecoBairroBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellEndereco = sender as XRTableCell;
            xrTableCellEndereco.Text = "";
            // Carrega o Endereco

            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            CotistaQuery c = new CotistaQuery("C");

            p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro);
            p.InnerJoin(c).On(p.IdPessoa == c.IdCotista);
            p.Where(p.RecebeCorrespondencia == "S" & c.IdCotista == this.idCotista);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            if (pessoaEnderecoCollection.HasData) {
                string endereco = pessoaEnderecoCollection[0].str.Endereco.Trim();
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim())) {
                    endereco += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + " " + pessoaEnderecoCollection[0].str.Complemento.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim())) {
                    endereco += " - " + pessoaEnderecoCollection[0].str.Bairro.Trim();
                }

                //
                xrTableCellEndereco.Text = endereco.Trim().ToUpper();
            }
            else {
                //xrTableCellEndereco.Text = "Endereço Não Cadastrado";
            }
        }

        private void CepCidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellEndereco = sender as XRTableCell;
            xrTableCellEndereco.Text = "";

            // Carrega o Endereco
            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            CotistaQuery c = new CotistaQuery("C");

            p.Select(p.Cep, p.Cidade, p.Uf);
            p.InnerJoin(c).On(p.IdPessoa == c.IdCotista);
            p.Where(p.RecebeCorrespondencia == "S" & c.IdCotista == this.idCotista);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            if (pessoaEnderecoCollection.HasData) {
                string cepCidadeUf = "";

                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim())) {
                    cepCidadeUf = "CEP: " + Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim())) {
                    cepCidadeUf += " - " + pessoaEnderecoCollection[0].str.Cidade.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
                    cepCidadeUf += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
                }
                xrTableCellEndereco.Text = cepCidadeUf.ToUpper();
            }
        }
    }

}