﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Financial.Tributo;
using Financial.CRM;
using Financial.Util;
using Financial.CRM.Enums;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Investidor;

/// <summary>
/// Summary description for ReportDarf
/// </summary>
public class ReportDarf : DevExpress.XtraReports.UI.XtraReport {

    private int idCliente;
    private int ano;
    private int mes;

    private XRTableCell tableCell11;
    private XRTableCell tableCell21;
    private DevExpress.XtraReports.Parameters.Parameter parameter08_ValorMulta;
    private XRTableCell tableCell19;
    private XRTableCell tableCell30;
    private DevExpress.XtraReports.Parameters.Parameter parameter03_CPF_CNPJ;
    private DevExpress.XtraReports.Parameters.Parameter parameter01_Nome;
    private XRTableCell tableCell51;
    private DevExpress.XtraReports.Parameters.Parameter parameter07_ValorPrincipal;
    private XRTableCell tableCell12;
    private DevExpress.XtraReports.Parameters.Parameter parameter05_NumeroReferencia;
    private XRTableRow tableRow8;
    private XRTableCell tableCell23;
    private XRTableCell tableCell38;
    private XRTableCell tableCell24;
    private DevExpress.XtraReports.Parameters.Parameter parameter09_ValorJuros;
    private XRPictureBox pictureBox1;
    private XRTableCell tableCell3;
    private DevExpress.XtraReports.Parameters.Parameter parameter02_PeriodoApuracao;
    private XRTableCell tableCell9;
    private DevExpress.XtraReports.Parameters.Parameter parameter04_CodigoReceita;
    private XRTableCell tableCell31;
    private XRTableRow tableRow3;
    private XRTableCell tableCell8;
    private XRTableCell tableCell33;
    private XRTableRow tableRow26;
    private XRTableCell tableCell64;
    private XRTableCell tableCell5;
    private XRTableCell tableCell57;
    private XRTableCell tableCell65;
    private XRTableCell tableCell49;
    private DevExpress.XtraReports.Parameters.Parameter parameter06_DataVencimento;
    private TopMarginBand TopMargin;
    private XRLabel label13;
    private DevExpress.XtraReports.Parameters.Parameter parameterDataPagamentoDarf;
    private XRPanel panel1;
    private XRLine line3;
    private XRLabel label10;
    private DevExpress.XtraReports.Parameters.Parameter parameterCidadeEstado;
    private XRLabel label9;
    private XRLine line1;
    private XRTable table2;
    private XRTableRow tableRow11;
    private XRTableCell tableCell1;
    private XRTableRow tableRow12;
    private XRTableCell tableCell4;
    private XRTableRow tableRow13;
    private XRTableCell tableCell7;
    private XRTableRow tableRow14;
    private XRTableCell tableCell10;
    private XRTableRow tableRow15;
    private XRTableCell tableCell13;
    private XRLabel label5;
    private XRLabel label3;
    private XRLabel label6;
    private XRLabel label2;
    private XRLabel label4;
    private XRLabel label7;
    private BottomMarginBand BottomMargin;
    private XRTableCell tableCell66;
    private XRLabel label16;
    private XRTableCell tableCell14;
    private XRTableCell tableCell16;
    private XRTableCell tableCell39;
    private XRTableCell tableCell36;
    private XRTableCell tableCell50;
    private XRTableCell tableCell59;
    private XRTableCell tableCell18;
    private XRTableCell tableCell63;
    private XRTableCell tableCell55;
    private XRTableCell tableCell56;
    private XRTableCell tableCell15;
    private XRTableCell tableCell61;
    private DevExpress.XtraReports.Parameters.Parameter parameter10_ValorTotal;
    private XRTable table4;
    private XRTableRow tableRow27;
    private XRTableRow tableRow28;
    private XRTableRow tableRow29;
    private XRTableCell tableCell67;
    private XRTableRow tableRow30;
    private XRTableCell tableCell68;
    private XRTableCell tableCell25;
    private XRPictureBox pictureBox2;
    private XRTableCell tableCell34;
    private XRTableCell tableCell42;
    private XRTableCell tableCell22;
    private XRTableCell tableCell28;
    private XRTableRow tableRow22;
    private XRTableCell tableCell53;
    private XRTableCell tableCell54;
    private XRTableRow tableRow1;
    private XRTableCell tableCell2;
    private XRLabel label1;
    private XRTableCell tableCell47;
    private XRTableCell tableCell20;
    private XRTableCell tableCell41;
    private XRLine line2;
    private XRLabel label8;
    private XRTableCell tableCell37;
    private XRTable table3;
    private XRTableRow tableRow16;
    private XRTableRow tableRow17;
    private XRTableRow tableRow18;
    private XRTableCell tableCell43;
    private XRTableRow tableRow19;
    private XRTableCell tableCell44;
    private XRTableCell tableCell45;
    private XRTableCell tableCell46;
    private XRTableRow tableRow20;
    private XRTableCell tableCell48;
    private XRTableRow tableRow21;
    private XRTableCell tableCell52;
    private XRTableRow tableRow23;
    private XRTableCell tableCell58;
    private XRTableRow tableRow24;
    private XRTableCell tableCell60;
    private XRTableRow tableRow25;
    private XRTableCell tableCell62;
    private XRTableCell tableCell6;
    private XRLine line5;
    private XRPanel panel2;
    private XRLine line4;
    private XRLabel label12;
    private XRLabel label14;
    private XRLabel label15;
    private XRLabel label17;
    private XRLabel label18;
    private XRLabel label19;
    private XRTableCell tableCell26;
    private XRTableRow tableRow6;
    private XRTableCell tableCell17;
    private XRTableRow tableRow10;
    private XRTableCell tableCell29;
    private XRTableCell tableCell40;
    private XRTableRow tableRow7;
    private PageHeaderBand PageHeader;
    private XRLine line6;
    private XRTable table1;
    private XRTableRow tableRow2;
    private XRTableCell tableCell32;
    private XRTableRow tableRow4;
    private XRTableRow tableRow5;
    private XRTableCell tableCell35;
    private XRTableRow tableRow9;
    private XRTableCell tableCell27;
    private DetailBand Detail;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public ReportDarf(int idCliente, int mes, int ano) {
        this.idCliente = idCliente;
        this.mes = mes;
        this.ano = ano;
		//        
        InitializeComponent();
        //
        
        this.PersonalInitialize();
	}
	
    /// <summary>
    /// Inicializações Personalizadas
    /// </summary>
    private void PersonalInitialize() 
    {
        ApuracaoRendaVariavelIR apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
        apuracaoRendaVariavelIR.LoadByPrimaryKey(this.idCliente, this.ano, (byte)this.mes);

        ApuracaoIRImobiliario apuracaoIRImobiliario = new ApuracaoIRImobiliario();
        apuracaoIRImobiliario.LoadByPrimaryKey(this.idCliente, this.ano, (byte)this.mes);

        decimal valorPrincipal = 0;
        if (apuracaoRendaVariavelIR.ImpostoPagar.HasValue)
        {
            valorPrincipal = apuracaoRendaVariavelIR.ImpostoPagar.Value;
        }

        if (apuracaoIRImobiliario.ImpostoPagar.HasValue)
        {
            valorPrincipal += apuracaoIRImobiliario.ImpostoPagar.Value;
        }

        DateTime dataApuracao = Calendario.RetornaUltimoDiaCorridoMes(new DateTime(this.ano, this.mes, 01), 0);
        DateTime dataVencimento = Calendario.RetornaUltimoDiaCorridoMes(new DateTime(this.ano, this.mes, 01), 1);

        DateTime dataInicioContagem = Calendario.AdicionaDiaUtil(dataVencimento, 1);

        DateTime dataHoje = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        DateTime dataPagamentoDARF = Calendario.RetornaUltimoDiaUtilMes(dataHoje, 0);
        DateTime dataUltimoDiaMesAnterior = Calendario.RetornaUltimoDiaUtilMes(dataHoje.AddMonths(-1), 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

        decimal valorMulta = 0;
        decimal valorJuros = 0;
        if (dataHoje > dataVencimento) //Calcula juros e multa devidos
        {
            int numeroDiasMulta = Calendario.NumeroDias(dataInicioContagem, dataHoje) + 1;
            if (numeroDiasMulta < 0)
                numeroDiasMulta = 0;

            valorMulta = Utilitario.Truncate((numeroDiasMulta * (0.33M / 100M)) * valorPrincipal, 2);

            if (valorMulta > Utilitario.Truncate(valorPrincipal * 0.2M, 2))
            {
                valorMulta = Utilitario.Truncate(valorPrincipal * 0.2M, 2);
            }

            decimal taxaSelic = 0;            
            if (dataInicioContagem < dataUltimoDiaMesAnterior)
            {
                DateTime dataAux = dataInicioContagem;
                DateTime dataFinalAux = Calendario.RetornaUltimoDiaCorridoMes(dataAux, 0);
                while (dataAux <= dataUltimoDiaMesAnterior)
                {
                    CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
                    cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.IdIndice.Equal((byte)ListaIndiceFixo.SELIC),
                                                        cotacaoIndiceCollection.Query.Data.GreaterThanOrEqual(dataAux),
                                                        cotacaoIndiceCollection.Query.Data.LessThanOrEqual(dataFinalAux));
                    cotacaoIndiceCollection.Query.Load();
                                        
                    decimal taxaSelicMes = 0;
                    foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
                    {
                        DateTime dataCotacao = cotacaoIndice.Data.Value;
                        taxaSelicMes += (decimal)Math.Pow((double)(cotacaoIndice.Valor.Value / 100M + 1M), (double)(1M / 252M)) - 1M;
                    }

                    taxaSelicMes = Math.Round(taxaSelicMes * 100M, 2);

                    dataAux = Calendario.RetornaPrimeiroDiaCorridoMes(dataAux, 1);
                    dataFinalAux = Calendario.RetornaUltimoDiaCorridoMes(dataAux, 0);

                    valorJuros += valorPrincipal * taxaSelicMes / 100M;
                }
            }

            valorJuros += valorPrincipal * 0.01M;

            valorJuros = Utilitario.Truncate(valorJuros, 2);
        }

        decimal valorTotal = valorPrincipal + valorMulta + valorJuros;

        Pessoa pessoa = new Pessoa();
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.idCliente);
        pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

        string cpfCNPJ = "";
        if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
        {
            cpfCNPJ = Utilitario.MascaraCPF(pessoa.Cpfcnpj.ToString());
        }
        else
        {
            cpfCNPJ = Utilitario.MascaraCNPJ(pessoa.Cpfcnpj.ToString());
        }

        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(cliente.IdPessoa.Value));
        pessoaEnderecoCollection.Query.Load();

        string domicilio = "";
        if (pessoaEnderecoCollection.Count > 0)
        {
            domicilio = pessoaEnderecoCollection[0].Cidade.Trim() + " - " + pessoaEnderecoCollection[0].Uf.Trim();
        }

        // TODO: Preencher Dados
        #region Preenche Parametros

        this.parameter01_Nome.Value = pessoa.Nome.ToUpper().Trim();
        this.parameter02_PeriodoApuracao.Value = dataApuracao;
        this.parameter03_CPF_CNPJ.Value = cpfCNPJ;
        this.parameter04_CodigoReceita.Value = 6015;
        //this.parameter05_NumeroReferencia.Value = 1;
        this.parameter06_DataVencimento.Value = dataVencimento;
        this.parameter07_ValorPrincipal.Value = valorPrincipal;
        this.parameter08_ValorMulta.Value = valorMulta;
        this.parameter09_ValorJuros.Value = valorJuros;
        this.parameter10_ValorTotal.Value = valorTotal;
        this.parameterDataPagamentoDarf.Value = dataPagamentoDARF;
        this.parameterCidadeEstado.Value = domicilio.ToUpper();

        #endregion
    }

	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "ReportDarf.resx";
        System.Resources.ResourceManager resources = global::Resources.ReportDarf.ResourceManager;
        this.tableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter08_ValorMulta = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter03_CPF_CNPJ = new DevExpress.XtraReports.Parameters.Parameter();
        this.parameter01_Nome = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter07_ValorPrincipal = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter05_NumeroReferencia = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter09_ValorJuros = new DevExpress.XtraReports.Parameters.Parameter();
        this.pictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
        this.tableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter02_PeriodoApuracao = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter04_CodigoReceita = new DevExpress.XtraReports.Parameters.Parameter();
        this.tableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter06_DataVencimento = new DevExpress.XtraReports.Parameters.Parameter();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.label13 = new DevExpress.XtraReports.UI.XRLabel();
        this.parameterDataPagamentoDarf = new DevExpress.XtraReports.Parameters.Parameter();
        this.panel1 = new DevExpress.XtraReports.UI.XRPanel();
        this.line3 = new DevExpress.XtraReports.UI.XRLine();
        this.label10 = new DevExpress.XtraReports.UI.XRLabel();
        this.parameterCidadeEstado = new DevExpress.XtraReports.Parameters.Parameter();
        this.label9 = new DevExpress.XtraReports.UI.XRLabel();
        this.line1 = new DevExpress.XtraReports.UI.XRLine();
        this.table2 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label5 = new DevExpress.XtraReports.UI.XRLabel();
        this.label3 = new DevExpress.XtraReports.UI.XRLabel();
        this.label6 = new DevExpress.XtraReports.UI.XRLabel();
        this.label2 = new DevExpress.XtraReports.UI.XRLabel();
        this.label4 = new DevExpress.XtraReports.UI.XRLabel();
        this.label7 = new DevExpress.XtraReports.UI.XRLabel();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.tableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label16 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.parameter10_ValorTotal = new DevExpress.XtraReports.Parameters.Parameter();
        this.table4 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.pictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
        this.tableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label1 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
        this.line2 = new DevExpress.XtraReports.UI.XRLine();
        this.label8 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table3 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.line5 = new DevExpress.XtraReports.UI.XRLine();
        this.panel2 = new DevExpress.XtraReports.UI.XRPanel();
        this.line4 = new DevExpress.XtraReports.UI.XRLine();
        this.label12 = new DevExpress.XtraReports.UI.XRLabel();
        this.label14 = new DevExpress.XtraReports.UI.XRLabel();
        this.label15 = new DevExpress.XtraReports.UI.XRLabel();
        this.label17 = new DevExpress.XtraReports.UI.XRLabel();
        this.label18 = new DevExpress.XtraReports.UI.XRLabel();
        this.label19 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.line6 = new DevExpress.XtraReports.UI.XRLine();
        this.table1 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        ((System.ComponentModel.ISupportInitialize)(this.table2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // tableCell11
        // 
        this.tableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell11.Dpi = 254F;
        this.tableCell11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell11.Name = "tableCell11";
        this.tableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell11.StylePriority.UseBorders = false;
        this.tableCell11.StylePriority.UseFont = false;
        this.tableCell11.StylePriority.UsePadding = false;
        this.tableCell11.Text = "05";
        this.tableCell11.Weight = 0.18618706043283639;
        // 
        // tableCell21
        // 
        this.tableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter08_ValorMulta, "Text", "{0:n2}")});
        this.tableCell21.Dpi = 254F;
        this.tableCell21.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell21.Name = "tableCell21";
        this.tableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell21.StylePriority.UseBorders = false;
        this.tableCell21.StylePriority.UseFont = false;
        this.tableCell21.StylePriority.UsePadding = false;
        this.tableCell21.StylePriority.UseTextAlignment = false;
        this.tableCell21.Text = "tableCell21";
        this.tableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell21.Weight = 1.305988958697667;
        // 
        // parameter08_ValorMulta
        // 
        this.parameter08_ValorMulta.Name = "parameter08_ValorMulta";
        this.parameter08_ValorMulta.Type = typeof(System.Nullable<decimal>);
        // 
        // tableCell19
        // 
        this.tableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell19.Dpi = 254F;
        this.tableCell19.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell19.Name = "tableCell19";
        this.tableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell19.StylePriority.UseBorders = false;
        this.tableCell19.StylePriority.UseFont = false;
        this.tableCell19.StylePriority.UsePadding = false;
        this.tableCell19.Text = "PERÍODO DE APURAÇÃO";
        this.tableCell19.Weight = 1.5078239808694964;
        // 
        // tableCell30
        // 
        this.tableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter03_CPF_CNPJ, "Text", "")});
        this.tableCell30.Dpi = 254F;
        this.tableCell30.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell30.Name = "tableCell30";
        this.tableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell30.StylePriority.UseBorders = false;
        this.tableCell30.StylePriority.UseFont = false;
        this.tableCell30.StylePriority.UsePadding = false;
        this.tableCell30.StylePriority.UseTextAlignment = false;
        this.tableCell30.Text = "tableCell6";
        this.tableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell30.Weight = 1.305988958697667;
        // 
        // parameter03_CPF_CNPJ
        // 
        this.parameter03_CPF_CNPJ.Name = "parameter03_CPF_CNPJ";
        // 
        // parameter01_Nome
        // 
        this.parameter01_Nome.Name = "parameter01_Nome";
        // 
        // tableCell51
        // 
        this.tableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell51.Dpi = 254F;
        this.tableCell51.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell51.Name = "tableCell51";
        this.tableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell51.StylePriority.UseBorders = false;
        this.tableCell51.StylePriority.UseFont = false;
        this.tableCell51.StylePriority.UsePadding = false;
        this.tableCell51.Text = "VALOR PRINCIPAL";
        this.tableCell51.Weight = 1.5078239808694964;
        // 
        // parameter07_ValorPrincipal
        // 
        this.parameter07_ValorPrincipal.Name = "parameter07_ValorPrincipal";
        this.parameter07_ValorPrincipal.Type = typeof(System.Nullable<decimal>);
        // 
        // tableCell12
        // 
        this.tableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter05_NumeroReferencia, "Text", "{0:n0}")});
        this.tableCell12.Dpi = 254F;
        this.tableCell12.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell12.Name = "tableCell12";
        this.tableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell12.StylePriority.UseBorders = false;
        this.tableCell12.StylePriority.UseFont = false;
        this.tableCell12.StylePriority.UsePadding = false;
        this.tableCell12.StylePriority.UseTextAlignment = false;
        this.tableCell12.Text = "tableCell12";
        this.tableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell12.Weight = 1.305988958697667;
        // 
        // parameter05_NumeroReferencia
        // 
        this.parameter05_NumeroReferencia.Name = "parameter05_NumeroReferencia";
        this.parameter05_NumeroReferencia.Type = typeof(System.Nullable<int>);
        // 
        // tableRow8
        // 
        this.tableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell23,
            this.tableCell38,
            this.tableCell24});
        this.tableRow8.Dpi = 254F;
        this.tableRow8.Name = "tableRow8";
        this.tableRow8.Weight = 0.55409733329232269;
        // 
        // tableCell23
        // 
        this.tableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell23.Dpi = 254F;
        this.tableCell23.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell23.Name = "tableCell23";
        this.tableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell23.StylePriority.UseBorders = false;
        this.tableCell23.StylePriority.UseFont = false;
        this.tableCell23.StylePriority.UsePadding = false;
        this.tableCell23.Text = "09";
        this.tableCell23.Weight = 0.18618706043283639;
        // 
        // tableCell38
        // 
        this.tableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell38.Dpi = 254F;
        this.tableCell38.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell38.Name = "tableCell38";
        this.tableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell38.StylePriority.UseBorders = false;
        this.tableCell38.StylePriority.UseFont = false;
        this.tableCell38.StylePriority.UsePadding = false;
        this.tableCell38.Text = "VALOR DOS JUROS E/OU \r\nENCARGOS DL - 1025/69";
        this.tableCell38.Weight = 1.5078239808694964;
        // 
        // tableCell24
        // 
        this.tableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter09_ValorJuros, "Text", "{0:n2}")});
        this.tableCell24.Dpi = 254F;
        this.tableCell24.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell24.Name = "tableCell24";
        this.tableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell24.StylePriority.UseBorders = false;
        this.tableCell24.StylePriority.UseFont = false;
        this.tableCell24.StylePriority.UsePadding = false;
        this.tableCell24.StylePriority.UseTextAlignment = false;
        this.tableCell24.Text = "tableCell24";
        this.tableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell24.Weight = 1.305988958697667;
        // 
        // parameter09_ValorJuros
        // 
        this.parameter09_ValorJuros.Name = "parameter09_ValorJuros";
        this.parameter09_ValorJuros.Type = typeof(System.Nullable<decimal>);
        // 
        // pictureBox1
        // 
        this.pictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.pictureBox1.Dpi = 254F;
        this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
        this.pictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 4F);
        this.pictureBox1.Name = "pictureBox1";
        this.pictureBox1.SizeF = new System.Drawing.SizeF(200.8276F, 232.8045F);
        this.pictureBox1.StylePriority.UseBorders = false;
        // 
        // tableCell3
        // 
        this.tableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter02_PeriodoApuracao, "Text", "{0:d}")});
        this.tableCell3.Dpi = 254F;
        this.tableCell3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell3.Name = "tableCell3";
        this.tableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell3.StylePriority.UseBorders = false;
        this.tableCell3.StylePriority.UseFont = false;
        this.tableCell3.StylePriority.UsePadding = false;
        this.tableCell3.StylePriority.UseTextAlignment = false;
        this.tableCell3.Text = "tableCell3";
        this.tableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell3.Weight = 1.305988958697667;
        // 
        // parameter02_PeriodoApuracao
        // 
        this.parameter02_PeriodoApuracao.Name = "parameter02_PeriodoApuracao";
        this.parameter02_PeriodoApuracao.Type = typeof(System.Nullable<System.DateTime>);
        // 
        // tableCell9
        // 
        this.tableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter04_CodigoReceita, "Text", "{0:n0}")});
        this.tableCell9.Dpi = 254F;
        this.tableCell9.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell9.Name = "tableCell9";
        this.tableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell9.StylePriority.UseBorders = false;
        this.tableCell9.StylePriority.UseFont = false;
        this.tableCell9.StylePriority.UsePadding = false;
        this.tableCell9.StylePriority.UseTextAlignment = false;
        this.tableCell9.Text = "tableCell9";
        this.tableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell9.Weight = 1.305988958697667;
        // 
        // parameter04_CodigoReceita
        // 
        this.parameter04_CodigoReceita.Name = "parameter04_CodigoReceita";
        this.parameter04_CodigoReceita.Type = typeof(System.Nullable<int>);
        // 
        // tableCell31
        // 
        this.tableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell31.Dpi = 254F;
        this.tableCell31.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell31.Name = "tableCell31";
        this.tableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell31.StylePriority.UseBorders = false;
        this.tableCell31.StylePriority.UseFont = false;
        this.tableCell31.StylePriority.UsePadding = false;
        this.tableCell31.Text = "PERÍODO DE APURAÇÃO";
        this.tableCell31.Weight = 1.5078239808694964;
        // 
        // tableRow3
        // 
        this.tableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell8,
            this.tableCell33,
            this.tableCell9});
        this.tableRow3.Dpi = 254F;
        this.tableRow3.Name = "tableRow3";
        this.tableRow3.Weight = 0.55409742229080083;
        // 
        // tableCell8
        // 
        this.tableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell8.Dpi = 254F;
        this.tableCell8.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell8.Name = "tableCell8";
        this.tableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell8.StylePriority.UseBorders = false;
        this.tableCell8.StylePriority.UseFont = false;
        this.tableCell8.StylePriority.UsePadding = false;
        this.tableCell8.Text = "04";
        this.tableCell8.Weight = 0.18618706043283639;
        // 
        // tableCell33
        // 
        this.tableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell33.Dpi = 254F;
        this.tableCell33.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell33.Name = "tableCell33";
        this.tableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell33.StylePriority.UseBorders = false;
        this.tableCell33.StylePriority.UseFont = false;
        this.tableCell33.StylePriority.UsePadding = false;
        this.tableCell33.Text = "CÓDIGO DA RECEITA";
        this.tableCell33.Weight = 1.5078239808694964;
        // 
        // tableRow26
        // 
        this.tableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell64});
        this.tableRow26.Dpi = 254F;
        this.tableRow26.Name = "tableRow26";
        this.tableRow26.Weight = 1.1023622047244095;
        // 
        // tableCell64
        // 
        this.tableCell64.Dpi = 254F;
        this.tableCell64.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell64.Name = "tableCell64";
        this.tableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell64.StylePriority.UseFont = false;
        this.tableCell64.Text = "MINISTÉRIO DA FAZENDA";
        this.tableCell64.Weight = 3;
        // 
        // tableCell5
        // 
        this.tableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell5.Dpi = 254F;
        this.tableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell5.Name = "tableCell5";
        this.tableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell5.StylePriority.UseBorders = false;
        this.tableCell5.StylePriority.UseFont = false;
        this.tableCell5.StylePriority.UsePadding = false;
        this.tableCell5.Text = "03";
        this.tableCell5.Weight = 0.18618706043283639;
        // 
        // tableCell57
        // 
        this.tableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell57.Dpi = 254F;
        this.tableCell57.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell57.Name = "tableCell57";
        this.tableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell57.StylePriority.UseBorders = false;
        this.tableCell57.StylePriority.UseFont = false;
        this.tableCell57.StylePriority.UsePadding = false;
        this.tableCell57.Text = "VALOR DOS JUROS E/OU \r\nENCARGOS DL - 1025/69";
        this.tableCell57.Weight = 1.5078239808694964;
        // 
        // tableCell65
        // 
        this.tableCell65.Dpi = 254F;
        this.tableCell65.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Bold);
        this.tableCell65.Name = "tableCell65";
        this.tableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell65.StylePriority.UseFont = false;
        this.tableCell65.StylePriority.UsePadding = false;
        this.tableCell65.Text = "SECRETARIA DA RECEITA FEDERAL DO BRASIL";
        this.tableCell65.Weight = 3;
        // 
        // tableCell49
        // 
        this.tableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter06_DataVencimento, "Text", "{0:d}")});
        this.tableCell49.Dpi = 254F;
        this.tableCell49.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell49.Name = "tableCell49";
        this.tableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell49.StylePriority.UseBorders = false;
        this.tableCell49.StylePriority.UseFont = false;
        this.tableCell49.StylePriority.UsePadding = false;
        this.tableCell49.StylePriority.UseTextAlignment = false;
        this.tableCell49.Text = "tableCell15";
        this.tableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell49.Weight = 1.305988958697667;
        // 
        // parameter06_DataVencimento
        // 
        this.parameter06_DataVencimento.Name = "parameter06_DataVencimento";
        this.parameter06_DataVencimento.Type = typeof(System.Nullable<System.DateTime>);
        // 
        // TopMargin
        // 
        this.TopMargin.Dpi = 254F;
        this.TopMargin.HeightF = 150F;
        this.TopMargin.Name = "TopMargin";
        // 
        // label13
        // 
        this.label13.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameterDataPagamentoDarf, "Text", "{0:d}")});
        this.label13.Dpi = 254F;
        this.label13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label13.LocationFloat = new DevExpress.Utils.PointFloat(559F, 722.1262F);
        this.label13.Name = "label13";
        this.label13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label13.SizeF = new System.Drawing.SizeF(431.391F, 50F);
        this.label13.StylePriority.UseBorders = false;
        this.label13.StylePriority.UseFont = false;
        this.label13.Text = "label9";
        // 
        // parameterDataPagamentoDarf
        // 
        this.parameterDataPagamentoDarf.Name = "parameterDataPagamentoDarf";
        this.parameterDataPagamentoDarf.Type = typeof(System.Nullable<System.DateTime>);
        // 
        // panel1
        // 
        this.panel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.panel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.line3,
            this.label10,
            this.label9,
            this.line1,
            this.table2,
            this.pictureBox1,
            this.label5,
            this.label3,
            this.label6,
            this.label2,
            this.label4,
            this.label7});
        this.panel1.Dpi = 254F;
        this.panel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69F);
        this.panel1.Name = "panel1";
        this.panel1.SizeF = new System.Drawing.SizeF(1011.417F, 1020F);
        this.panel1.StylePriority.UseBorders = false;
        // 
        // line3
        // 
        this.line3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.line3.Dpi = 254F;
        this.line3.LineWidth = 3;
        this.line3.LocationFloat = new DevExpress.Utils.PointFloat(3.000023F, 665F);
        this.line3.Name = "line3";
        this.line3.SizeF = new System.Drawing.SizeF(1008.417F, 38.50381F);
        this.line3.StylePriority.UseBorders = false;
        // 
        // label10
        // 
        this.label10.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameterCidadeEstado, "Text", "")});
        this.label10.Dpi = 254F;
        this.label10.LocationFloat = new DevExpress.Utils.PointFloat(497.4838F, 780.5461F);
        this.label10.Name = "label10";
        this.label10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label10.SizeF = new System.Drawing.SizeF(500.5435F, 50F);
        this.label10.StylePriority.UseBorders = false;
        this.label10.Text = "label10";
        // 
        // parameterCidadeEstado
        // 
        this.parameterCidadeEstado.Name = "parameterCidadeEstado";
        // 
        // label9
        // 
        this.label9.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameterDataPagamentoDarf, "Text", "{0:d}")});
        this.label9.Dpi = 254F;
        this.label9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label9.LocationFloat = new DevExpress.Utils.PointFloat(565.6364F, 722.1262F);
        this.label9.Name = "label9";
        this.label9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label9.SizeF = new System.Drawing.SizeF(431.391F, 50F);
        this.label9.StylePriority.UseBorders = false;
        this.label9.StylePriority.UseFont = false;
        this.label9.Text = "label9";
        // 
        // line1
        // 
        this.line1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.line1.Dpi = 254F;
        this.line1.LineWidth = 3;
        this.line1.LocationFloat = new DevExpress.Utils.PointFloat(3.000023F, 436.4963F);
        this.line1.Name = "line1";
        this.line1.SizeF = new System.Drawing.SizeF(1008.417F, 38.50381F);
        this.line1.StylePriority.UseBorders = false;
        // 
        // table2
        // 
        this.table2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table2.Dpi = 254F;
        this.table2.LocationFloat = new DevExpress.Utils.PointFloat(205.8276F, 24.99998F);
        this.table2.Name = "table2";
        this.table2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow11,
            this.tableRow12,
            this.tableRow13,
            this.tableRow14,
            this.tableRow15});
        this.table2.SizeF = new System.Drawing.SizeF(805.5889F, 355.0001F);
        this.table2.StylePriority.UseBorders = false;
        this.table2.StylePriority.UseFont = false;
        // 
        // tableRow11
        // 
        this.tableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell1});
        this.tableRow11.Dpi = 254F;
        this.tableRow11.Name = "tableRow11";
        this.tableRow11.Weight = 1.1023622047244095;
        // 
        // tableCell1
        // 
        this.tableCell1.Dpi = 254F;
        this.tableCell1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell1.Name = "tableCell1";
        this.tableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell1.StylePriority.UseFont = false;
        this.tableCell1.Text = "MINISTÉRIO DA FAZENDA";
        this.tableCell1.Weight = 3;
        // 
        // tableRow12
        // 
        this.tableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell4});
        this.tableRow12.Dpi = 254F;
        this.tableRow12.Name = "tableRow12";
        this.tableRow12.Weight = 1.1023622047244095;
        // 
        // tableCell4
        // 
        this.tableCell4.Dpi = 254F;
        this.tableCell4.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Bold);
        this.tableCell4.Name = "tableCell4";
        this.tableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell4.StylePriority.UseFont = false;
        this.tableCell4.StylePriority.UsePadding = false;
        this.tableCell4.Text = "SECRETARIA DA RECEITA FEDERAL DO BRASIL";
        this.tableCell4.Weight = 3;
        // 
        // tableRow13
        // 
        this.tableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell7});
        this.tableRow13.Dpi = 254F;
        this.tableRow13.Name = "tableRow13";
        this.tableRow13.Weight = 1.1023622047244095;
        // 
        // tableCell7
        // 
        this.tableCell7.Dpi = 254F;
        this.tableCell7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
        this.tableCell7.Name = "tableCell7";
        this.tableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell7.StylePriority.UseFont = false;
        this.tableCell7.StylePriority.UsePadding = false;
        this.tableCell7.Text = "Documento de Arrecadação de Receitas Federais";
        this.tableCell7.Weight = 3;
        // 
        // tableRow14
        // 
        this.tableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell10});
        this.tableRow14.Dpi = 254F;
        this.tableRow14.Name = "tableRow14";
        this.tableRow14.Weight = 0.69291338582677164;
        // 
        // tableCell10
        // 
        this.tableCell10.Dpi = 254F;
        this.tableCell10.Name = "tableCell10";
        this.tableCell10.Weight = 3;
        // 
        // tableRow15
        // 
        this.tableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell13});
        this.tableRow15.Dpi = 254F;
        this.tableRow15.Name = "tableRow15";
        this.tableRow15.Weight = 1;
        // 
        // tableCell13
        // 
        this.tableCell13.Dpi = 254F;
        this.tableCell13.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell13.Name = "tableCell13";
        this.tableCell13.StylePriority.UseFont = false;
        this.tableCell13.Text = "DARF";
        this.tableCell13.Weight = 3;
        // 
        // label5
        // 
        this.label5.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter01_Nome, "Text", "")});
        this.label5.Dpi = 254F;
        this.label5.LocationFloat = new DevExpress.Utils.PointFloat(87.41763F, 536.2738F);
        this.label5.Name = "label5";
        this.label5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label5.SizeF = new System.Drawing.SizeF(876.0416F, 45F);
        this.label5.StylePriority.UseBorders = false;
        this.label5.Text = "label5";
        // 
        // label3
        // 
        this.label3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label3.Dpi = 254F;
        this.label3.LocationFloat = new DevExpress.Utils.PointFloat(92.70934F, 491.2738F);
        this.label3.Name = "label3";
        this.label3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label3.SizeF = new System.Drawing.SizeF(669.6666F, 45F);
        this.label3.StylePriority.UseBorders = false;
        this.label3.Text = "NOME / TELEFONE";
        // 
        // label6
        // 
        this.label6.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label6.Dpi = 254F;
        this.label6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.label6.LocationFloat = new DevExpress.Utils.PointFloat(15.98015F, 483.1454F);
        this.label6.Name = "label6";
        this.label6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label6.SizeF = new System.Drawing.SizeF(58.2083F, 53.12836F);
        this.label6.StylePriority.UseBorders = false;
        this.label6.StylePriority.UseFont = false;
        this.label6.Text = "01";
        // 
        // label2
        // 
        this.label2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label2.Dpi = 254F;
        this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label2.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 722.1262F);
        this.label2.Name = "label2";
        this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label2.SizeF = new System.Drawing.SizeF(522F, 50F);
        this.label2.StylePriority.UseBorders = false;
        this.label2.StylePriority.UseFont = false;
        this.label2.Text = "DARF válido para pagamento até";
        // 
        // label4
        // 
        this.label4.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label4.Dpi = 254F;
        this.label4.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 780.5463F);
        this.label4.Name = "label4";
        this.label4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label4.SizeF = new System.Drawing.SizeF(454F, 50F);
        this.label4.StylePriority.UseBorders = false;
        this.label4.Text = "Domicílio tributário informado:";
        // 
        // label7
        // 
        this.label7.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label7.Dpi = 254F;
        this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label7.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 830.5461F);
        this.label7.Name = "label7";
        this.label7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label7.SizeF = new System.Drawing.SizeF(955.9874F, 58.4201F);
        this.label7.StylePriority.UseBorders = false;
        this.label7.StylePriority.UseFont = false;
        this.label7.Text = "NÂO RECEBER COM RASURAS";
        // 
        // BottomMargin
        // 
        this.BottomMargin.Dpi = 254F;
        this.BottomMargin.HeightF = 150F;
        this.BottomMargin.Name = "BottomMargin";
        // 
        // tableCell66
        // 
        this.tableCell66.Dpi = 254F;
        this.tableCell66.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
        this.tableCell66.Name = "tableCell66";
        this.tableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(15, 0, 0, 0, 254F);
        this.tableCell66.StylePriority.UseFont = false;
        this.tableCell66.StylePriority.UsePadding = false;
        this.tableCell66.Text = "Documento de Arrecadação de Receitas Federais";
        this.tableCell66.Weight = 3;
        // 
        // label16
        // 
        this.label16.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label16.Dpi = 254F;
        this.label16.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.label16.LocationFloat = new DevExpress.Utils.PointFloat(15.98015F, 483.1454F);
        this.label16.Name = "label16";
        this.label16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label16.SizeF = new System.Drawing.SizeF(58.2083F, 53.12836F);
        this.label16.StylePriority.UseBorders = false;
        this.label16.StylePriority.UseFont = false;
        this.label16.Text = "01";
        // 
        // tableCell14
        // 
        this.tableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell14.Dpi = 254F;
        this.tableCell14.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell14.Name = "tableCell14";
        this.tableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell14.StylePriority.UseBorders = false;
        this.tableCell14.StylePriority.UseFont = false;
        this.tableCell14.StylePriority.UsePadding = false;
        this.tableCell14.Text = "06";
        this.tableCell14.Weight = 0.18618706043283639;
        // 
        // tableCell16
        // 
        this.tableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell16.Dpi = 254F;
        this.tableCell16.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell16.Name = "tableCell16";
        this.tableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
        this.tableCell16.StylePriority.UseBorders = false;
        this.tableCell16.StylePriority.UseFont = false;
        this.tableCell16.StylePriority.UsePadding = false;
        this.tableCell16.StylePriority.UseTextAlignment = false;
        this.tableCell16.Text = "02";
        this.tableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.tableCell16.Weight = 0.18618706043283639;
        // 
        // tableCell39
        // 
        this.tableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell39.Dpi = 254F;
        this.tableCell39.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell39.Name = "tableCell39";
        this.tableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell39.StylePriority.UseBorders = false;
        this.tableCell39.StylePriority.UseFont = false;
        this.tableCell39.StylePriority.UsePadding = false;
        this.tableCell39.Text = "VALOR TOTAL";
        this.tableCell39.Weight = 1.5078239808694964;
        // 
        // tableCell36
        // 
        this.tableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell36.Dpi = 254F;
        this.tableCell36.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell36.Name = "tableCell36";
        this.tableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell36.StylePriority.UseBorders = false;
        this.tableCell36.StylePriority.UseFont = false;
        this.tableCell36.StylePriority.UsePadding = false;
        this.tableCell36.Text = "VALOR PRINCIPAL";
        this.tableCell36.Weight = 1.5078239808694964;
        // 
        // tableCell50
        // 
        this.tableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell50.Dpi = 254F;
        this.tableCell50.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell50.Name = "tableCell50";
        this.tableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell50.StylePriority.UseBorders = false;
        this.tableCell50.StylePriority.UseFont = false;
        this.tableCell50.StylePriority.UsePadding = false;
        this.tableCell50.Text = "07";
        this.tableCell50.Weight = 0.18618706043283639;
        // 
        // tableCell59
        // 
        this.tableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell59.Dpi = 254F;
        this.tableCell59.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell59.Name = "tableCell59";
        this.tableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell59.StylePriority.UseBorders = false;
        this.tableCell59.StylePriority.UseFont = false;
        this.tableCell59.StylePriority.UsePadding = false;
        this.tableCell59.Text = "10";
        this.tableCell59.Weight = 0.18618706043283639;
        // 
        // tableCell18
        // 
        this.tableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter07_ValorPrincipal, "Text", "{0:n2}")});
        this.tableCell18.Dpi = 254F;
        this.tableCell18.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell18.Name = "tableCell18";
        this.tableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell18.StylePriority.UseBorders = false;
        this.tableCell18.StylePriority.UseFont = false;
        this.tableCell18.StylePriority.UsePadding = false;
        this.tableCell18.StylePriority.UseTextAlignment = false;
        this.tableCell18.Text = "tableCell18";
        this.tableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell18.Weight = 1.305988958697667;
        // 
        // tableCell63
        // 
        this.tableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell63.Dpi = 254F;
        this.tableCell63.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell63.Name = "tableCell63";
        this.tableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell63.StylePriority.UseBorders = false;
        this.tableCell63.StylePriority.UseFont = false;
        this.tableCell63.StylePriority.UsePadding = false;
        this.tableCell63.Text = "AUTENTICAÇÃO BANCÁRIA (Somente nas 1ª e 2ª vias)";
        this.tableCell63.Weight = 2.8138131019118138;
        // 
        // tableCell55
        // 
        this.tableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter08_ValorMulta, "Text", "{0:n2}")});
        this.tableCell55.Dpi = 254F;
        this.tableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell55.Name = "tableCell55";
        this.tableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell55.StylePriority.UseBorders = false;
        this.tableCell55.StylePriority.UseFont = false;
        this.tableCell55.StylePriority.UsePadding = false;
        this.tableCell55.StylePriority.UseTextAlignment = false;
        this.tableCell55.Text = "tableCell21";
        this.tableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell55.Weight = 1.305988958697667;
        // 
        // tableCell56
        // 
        this.tableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell56.Dpi = 254F;
        this.tableCell56.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell56.Name = "tableCell56";
        this.tableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell56.StylePriority.UseBorders = false;
        this.tableCell56.StylePriority.UseFont = false;
        this.tableCell56.StylePriority.UsePadding = false;
        this.tableCell56.Text = "09";
        this.tableCell56.Weight = 0.18618706043283639;
        // 
        // tableCell15
        // 
        this.tableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter06_DataVencimento, "Text", "{0:d}")});
        this.tableCell15.Dpi = 254F;
        this.tableCell15.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell15.Name = "tableCell15";
        this.tableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell15.StylePriority.UseBorders = false;
        this.tableCell15.StylePriority.UseFont = false;
        this.tableCell15.StylePriority.UsePadding = false;
        this.tableCell15.StylePriority.UseTextAlignment = false;
        this.tableCell15.Text = "tableCell15";
        this.tableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell15.Weight = 1.305988958697667;
        // 
        // tableCell61
        // 
        this.tableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter10_ValorTotal, "Text", "{0:n2}")});
        this.tableCell61.Dpi = 254F;
        this.tableCell61.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell61.Name = "tableCell61";
        this.tableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell61.StylePriority.UseBorders = false;
        this.tableCell61.StylePriority.UseFont = false;
        this.tableCell61.StylePriority.UsePadding = false;
        this.tableCell61.StylePriority.UseTextAlignment = false;
        this.tableCell61.Text = "tableCell27";
        this.tableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell61.Weight = 1.305988958697667;
        // 
        // parameter10_ValorTotal
        // 
        this.parameter10_ValorTotal.Name = "parameter10_ValorTotal";
        this.parameter10_ValorTotal.Type = typeof(System.Nullable<decimal>);
        // 
        // table4
        // 
        this.table4.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table4.Dpi = 254F;
        this.table4.LocationFloat = new DevExpress.Utils.PointFloat(205.8276F, 24.99998F);
        this.table4.Name = "table4";
        this.table4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow26,
            this.tableRow27,
            this.tableRow28,
            this.tableRow29,
            this.tableRow30});
        this.table4.SizeF = new System.Drawing.SizeF(805.5889F, 355.0001F);
        this.table4.StylePriority.UseBorders = false;
        this.table4.StylePriority.UseFont = false;
        // 
        // tableRow27
        // 
        this.tableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell65});
        this.tableRow27.Dpi = 254F;
        this.tableRow27.Name = "tableRow27";
        this.tableRow27.Weight = 1.1023622047244095;
        // 
        // tableRow28
        // 
        this.tableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell66});
        this.tableRow28.Dpi = 254F;
        this.tableRow28.Name = "tableRow28";
        this.tableRow28.Weight = 1.1023622047244095;
        // 
        // tableRow29
        // 
        this.tableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell67});
        this.tableRow29.Dpi = 254F;
        this.tableRow29.Name = "tableRow29";
        this.tableRow29.Weight = 0.69291338582677164;
        // 
        // tableCell67
        // 
        this.tableCell67.Dpi = 254F;
        this.tableCell67.Name = "tableCell67";
        this.tableCell67.Weight = 3;
        // 
        // tableRow30
        // 
        this.tableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell68});
        this.tableRow30.Dpi = 254F;
        this.tableRow30.Name = "tableRow30";
        this.tableRow30.Weight = 1;
        // 
        // tableCell68
        // 
        this.tableCell68.Dpi = 254F;
        this.tableCell68.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell68.Name = "tableCell68";
        this.tableCell68.StylePriority.UseFont = false;
        this.tableCell68.Text = "DARF";
        this.tableCell68.Weight = 3;
        // 
        // tableCell25
        // 
        this.tableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell25.Dpi = 254F;
        this.tableCell25.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell25.Name = "tableCell25";
        this.tableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell25.StylePriority.UseBorders = false;
        this.tableCell25.StylePriority.UseFont = false;
        this.tableCell25.StylePriority.UsePadding = false;
        this.tableCell25.Text = "03";
        this.tableCell25.Weight = 0.18618706043283639;
        // 
        // pictureBox2
        // 
        this.pictureBox2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.pictureBox2.Dpi = 254F;
        this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
        this.pictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(7.999976F, 4F);
        this.pictureBox2.Name = "pictureBox2";
        this.pictureBox2.SizeF = new System.Drawing.SizeF(196.8276F, 232.8047F);
        this.pictureBox2.StylePriority.UseBorders = false;
        // 
        // tableCell34
        // 
        this.tableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell34.Dpi = 254F;
        this.tableCell34.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell34.Name = "tableCell34";
        this.tableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell34.StylePriority.UseBorders = false;
        this.tableCell34.StylePriority.UseFont = false;
        this.tableCell34.StylePriority.UsePadding = false;
        this.tableCell34.Text = "NÚMERO DE REFERÊNCIA";
        this.tableCell34.Weight = 1.5078239808694964;
        // 
        // tableCell42
        // 
        this.tableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell42.Dpi = 254F;
        this.tableCell42.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell42.Name = "tableCell42";
        this.tableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell42.StylePriority.UseBorders = false;
        this.tableCell42.StylePriority.UseFont = false;
        this.tableCell42.StylePriority.UsePadding = false;
        this.tableCell42.Text = "CÓDIGO DA RECEITA";
        this.tableCell42.Weight = 1.5078239808694964;
        // 
        // tableCell22
        // 
        this.tableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter02_PeriodoApuracao, "Text", "{0:d}")});
        this.tableCell22.Dpi = 254F;
        this.tableCell22.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell22.Name = "tableCell22";
        this.tableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell22.StylePriority.UseBorders = false;
        this.tableCell22.StylePriority.UseFont = false;
        this.tableCell22.StylePriority.UsePadding = false;
        this.tableCell22.StylePriority.UseTextAlignment = false;
        this.tableCell22.Text = "tableCell3";
        this.tableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell22.Weight = 1.305988958697667;
        // 
        // tableCell28
        // 
        this.tableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell28.Dpi = 254F;
        this.tableCell28.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell28.Name = "tableCell28";
        this.tableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell28.StylePriority.UseBorders = false;
        this.tableCell28.StylePriority.UseFont = false;
        this.tableCell28.StylePriority.UsePadding = false;
        this.tableCell28.Text = "NÚMERO DO CPF OU CNPJ";
        this.tableCell28.Weight = 1.5078239808694964;
        // 
        // tableRow22
        // 
        this.tableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell53,
            this.tableCell54,
            this.tableCell55});
        this.tableRow22.Dpi = 254F;
        this.tableRow22.Name = "tableRow22";
        this.tableRow22.Weight = 0.55409733329232269;
        // 
        // tableCell53
        // 
        this.tableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell53.Dpi = 254F;
        this.tableCell53.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell53.Name = "tableCell53";
        this.tableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell53.StylePriority.UseBorders = false;
        this.tableCell53.StylePriority.UseFont = false;
        this.tableCell53.StylePriority.UsePadding = false;
        this.tableCell53.Text = "08";
        this.tableCell53.Weight = 0.18618706043283639;
        // 
        // tableCell54
        // 
        this.tableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell54.Dpi = 254F;
        this.tableCell54.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell54.Name = "tableCell54";
        this.tableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell54.StylePriority.UseBorders = false;
        this.tableCell54.StylePriority.UseFont = false;
        this.tableCell54.StylePriority.UsePadding = false;
        this.tableCell54.Text = "VALOR DA MULTA";
        this.tableCell54.Weight = 1.5078239808694964;
        // 
        // tableRow1
        // 
        this.tableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell2,
            this.tableCell31,
            this.tableCell3});
        this.tableRow1.Dpi = 254F;
        this.tableRow1.Name = "tableRow1";
        this.tableRow1.Weight = 0.55409738800435515;
        // 
        // tableCell2
        // 
        this.tableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell2.Dpi = 254F;
        this.tableCell2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell2.Name = "tableCell2";
        this.tableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
        this.tableCell2.StylePriority.UseBorders = false;
        this.tableCell2.StylePriority.UseFont = false;
        this.tableCell2.StylePriority.UsePadding = false;
        this.tableCell2.StylePriority.UseTextAlignment = false;
        this.tableCell2.Text = "02";
        this.tableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.tableCell2.Weight = 0.18618706043283639;
        // 
        // label1
        // 
        this.label1.Dpi = 254F;
        this.label1.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.label1.LocationFloat = new DevExpress.Utils.PointFloat(1839.938F, 0F);
        this.label1.Name = "label1";
        this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label1.SizeF = new System.Drawing.SizeF(119.0625F, 58.42F);
        this.label1.StylePriority.UseFont = false;
        this.label1.StylePriority.UseTextAlignment = false;
        this.label1.Text = "1ª via";
        this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        // 
        // tableCell47
        // 
        this.tableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell47.Dpi = 254F;
        this.tableCell47.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell47.Name = "tableCell47";
        this.tableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell47.StylePriority.UseBorders = false;
        this.tableCell47.StylePriority.UseFont = false;
        this.tableCell47.StylePriority.UsePadding = false;
        this.tableCell47.Text = "06";
        this.tableCell47.Weight = 0.18618706043283639;
        // 
        // tableCell20
        // 
        this.tableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell20.Dpi = 254F;
        this.tableCell20.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell20.Name = "tableCell20";
        this.tableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell20.StylePriority.UseBorders = false;
        this.tableCell20.StylePriority.UseFont = false;
        this.tableCell20.StylePriority.UsePadding = false;
        this.tableCell20.Text = "08";
        this.tableCell20.Weight = 0.18618706043283639;
        // 
        // tableCell41
        // 
        this.tableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell41.Dpi = 254F;
        this.tableCell41.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell41.Name = "tableCell41";
        this.tableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell41.StylePriority.UseBorders = false;
        this.tableCell41.StylePriority.UseFont = false;
        this.tableCell41.StylePriority.UsePadding = false;
        this.tableCell41.Text = "04";
        this.tableCell41.Weight = 0.18618706043283639;
        // 
        // line2
        // 
        this.line2.Dpi = 254F;
        this.line2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.line2.LineWidth = 3;
        this.line2.LocationFloat = new DevExpress.Utils.PointFloat(5.000594F, 1154.961F);
        this.line2.Name = "line2";
        this.line2.SizeF = new System.Drawing.SizeF(1954F, 25F);
        // 
        // label8
        // 
        this.label8.Dpi = 254F;
        this.label8.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.label8.LocationFloat = new DevExpress.Utils.PointFloat(1838.355F, 1234.066F);
        this.label8.Name = "label8";
        this.label8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label8.SizeF = new System.Drawing.SizeF(119.0625F, 58.42F);
        this.label8.StylePriority.UseFont = false;
        this.label8.StylePriority.UseTextAlignment = false;
        this.label8.Text = "2ª via";
        this.label8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        // 
        // tableCell37
        // 
        this.tableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell37.Dpi = 254F;
        this.tableCell37.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell37.Name = "tableCell37";
        this.tableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell37.StylePriority.UseBorders = false;
        this.tableCell37.StylePriority.UseFont = false;
        this.tableCell37.StylePriority.UsePadding = false;
        this.tableCell37.Text = "VALOR DA MULTA";
        this.tableCell37.Weight = 1.5078239808694964;
        // 
        // table3
        // 
        this.table3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.table3.Dpi = 254F;
        this.table3.LocationFloat = new DevExpress.Utils.PointFloat(1013.418F, 1307F);
        this.table3.Name = "table3";
        this.table3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow16,
            this.tableRow17,
            this.tableRow18,
            this.tableRow19,
            this.tableRow20,
            this.tableRow21,
            this.tableRow22,
            this.tableRow23,
            this.tableRow24,
            this.tableRow25});
        this.table3.SizeF = new System.Drawing.SizeF(943.5823F, 1011F);
        this.table3.StylePriority.UseBorders = false;
        // 
        // tableRow16
        // 
        this.tableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell16,
            this.tableCell19,
            this.tableCell22});
        this.tableRow16.Dpi = 254F;
        this.tableRow16.Name = "tableRow16";
        this.tableRow16.Weight = 0.55409738800435515;
        // 
        // tableRow17
        // 
        this.tableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell25,
            this.tableCell28,
            this.tableCell30});
        this.tableRow17.Dpi = 254F;
        this.tableRow17.Name = "tableRow17";
        this.tableRow17.Weight = 0.55409736757876893;
        // 
        // tableRow18
        // 
        this.tableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell41,
            this.tableCell42,
            this.tableCell43});
        this.tableRow18.Dpi = 254F;
        this.tableRow18.Name = "tableRow18";
        this.tableRow18.Weight = 0.55409742229080083;
        // 
        // tableCell43
        // 
        this.tableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter04_CodigoReceita, "Text", "{0:n0}")});
        this.tableCell43.Dpi = 254F;
        this.tableCell43.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell43.Name = "tableCell43";
        this.tableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell43.StylePriority.UseBorders = false;
        this.tableCell43.StylePriority.UseFont = false;
        this.tableCell43.StylePriority.UsePadding = false;
        this.tableCell43.StylePriority.UseTextAlignment = false;
        this.tableCell43.Text = "tableCell9";
        this.tableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell43.Weight = 1.305988958697667;
        // 
        // tableRow19
        // 
        this.tableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell44,
            this.tableCell45,
            this.tableCell46});
        this.tableRow19.Dpi = 254F;
        this.tableRow19.Name = "tableRow19";
        this.tableRow19.Weight = 0.55409733329232291;
        // 
        // tableCell44
        // 
        this.tableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell44.Dpi = 254F;
        this.tableCell44.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell44.Name = "tableCell44";
        this.tableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell44.StylePriority.UseBorders = false;
        this.tableCell44.StylePriority.UseFont = false;
        this.tableCell44.StylePriority.UsePadding = false;
        this.tableCell44.Text = "05";
        this.tableCell44.Weight = 0.18618706043283639;
        // 
        // tableCell45
        // 
        this.tableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell45.Dpi = 254F;
        this.tableCell45.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell45.Name = "tableCell45";
        this.tableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell45.StylePriority.UseBorders = false;
        this.tableCell45.StylePriority.UseFont = false;
        this.tableCell45.StylePriority.UsePadding = false;
        this.tableCell45.Text = "NÚMERO DE REFERÊNCIA";
        this.tableCell45.Weight = 1.5078239808694964;
        // 
        // tableCell46
        // 
        this.tableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter05_NumeroReferencia, "Text", "{0:n0}")});
        this.tableCell46.Dpi = 254F;
        this.tableCell46.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell46.Name = "tableCell46";
        this.tableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell46.StylePriority.UseBorders = false;
        this.tableCell46.StylePriority.UseFont = false;
        this.tableCell46.StylePriority.UsePadding = false;
        this.tableCell46.StylePriority.UseTextAlignment = false;
        this.tableCell46.Text = "tableCell12";
        this.tableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell46.Weight = 1.305988958697667;
        // 
        // tableRow20
        // 
        this.tableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell47,
            this.tableCell48,
            this.tableCell49});
        this.tableRow20.Dpi = 254F;
        this.tableRow20.Name = "tableRow20";
        this.tableRow20.Weight = 0.55409742229080083;
        // 
        // tableCell48
        // 
        this.tableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell48.Dpi = 254F;
        this.tableCell48.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell48.Name = "tableCell48";
        this.tableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell48.StylePriority.UseBorders = false;
        this.tableCell48.StylePriority.UseFont = false;
        this.tableCell48.StylePriority.UsePadding = false;
        this.tableCell48.Text = "DATA DE VENCIMENTO";
        this.tableCell48.Weight = 1.5078239808694964;
        // 
        // tableRow21
        // 
        this.tableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell50,
            this.tableCell51,
            this.tableCell52});
        this.tableRow21.Dpi = 254F;
        this.tableRow21.Name = "tableRow21";
        this.tableRow21.Weight = 0.55409742229080106;
        // 
        // tableCell52
        // 
        this.tableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter07_ValorPrincipal, "Text", "{0:n2}")});
        this.tableCell52.Dpi = 254F;
        this.tableCell52.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell52.Name = "tableCell52";
        this.tableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell52.StylePriority.UseBorders = false;
        this.tableCell52.StylePriority.UseFont = false;
        this.tableCell52.StylePriority.UsePadding = false;
        this.tableCell52.StylePriority.UseTextAlignment = false;
        this.tableCell52.Text = "tableCell18";
        this.tableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell52.Weight = 1.305988958697667;
        // 
        // tableRow23
        // 
        this.tableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell56,
            this.tableCell57,
            this.tableCell58});
        this.tableRow23.Dpi = 254F;
        this.tableRow23.Name = "tableRow23";
        this.tableRow23.Weight = 0.55409733329232269;
        // 
        // tableCell58
        // 
        this.tableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter09_ValorJuros, "Text", "{0:n2}")});
        this.tableCell58.Dpi = 254F;
        this.tableCell58.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell58.Name = "tableCell58";
        this.tableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell58.StylePriority.UseBorders = false;
        this.tableCell58.StylePriority.UseFont = false;
        this.tableCell58.StylePriority.UsePadding = false;
        this.tableCell58.StylePriority.UseTextAlignment = false;
        this.tableCell58.Text = "tableCell24";
        this.tableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell58.Weight = 1.305988958697667;
        // 
        // tableRow24
        // 
        this.tableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell59,
            this.tableCell60,
            this.tableCell61});
        this.tableRow24.Dpi = 254F;
        this.tableRow24.Name = "tableRow24";
        this.tableRow24.Weight = 0.55409733329232225;
        // 
        // tableCell60
        // 
        this.tableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell60.Dpi = 254F;
        this.tableCell60.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell60.Name = "tableCell60";
        this.tableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell60.StylePriority.UseBorders = false;
        this.tableCell60.StylePriority.UseFont = false;
        this.tableCell60.StylePriority.UsePadding = false;
        this.tableCell60.Text = "VALOR TOTAL";
        this.tableCell60.Weight = 1.5078239808694964;
        // 
        // tableRow25
        // 
        this.tableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell62,
            this.tableCell63});
        this.tableRow25.Dpi = 254F;
        this.tableRow25.Name = "tableRow25";
        this.tableRow25.Weight = 0.904053944754302;
        // 
        // tableCell62
        // 
        this.tableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell62.Dpi = 254F;
        this.tableCell62.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell62.Name = "tableCell62";
        this.tableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 16, 0, 254F);
        this.tableCell62.StylePriority.UseBorders = false;
        this.tableCell62.StylePriority.UseFont = false;
        this.tableCell62.StylePriority.UsePadding = false;
        this.tableCell62.Text = "11";
        this.tableCell62.Weight = 0.18618689808818634;
        // 
        // tableCell6
        // 
        this.tableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter03_CPF_CNPJ, "Text", "")});
        this.tableCell6.Dpi = 254F;
        this.tableCell6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell6.Name = "tableCell6";
        this.tableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell6.StylePriority.UseBorders = false;
        this.tableCell6.StylePriority.UseFont = false;
        this.tableCell6.StylePriority.UsePadding = false;
        this.tableCell6.StylePriority.UseTextAlignment = false;
        this.tableCell6.Text = "tableCell6";
        this.tableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell6.Weight = 1.305988958697667;
        // 
        // line5
        // 
        this.line5.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.line5.Dpi = 254F;
        this.line5.LineWidth = 3;
        this.line5.LocationFloat = new DevExpress.Utils.PointFloat(3.000023F, 436.4963F);
        this.line5.Name = "line5";
        this.line5.SizeF = new System.Drawing.SizeF(1008.417F, 38.50381F);
        this.line5.StylePriority.UseBorders = false;
        // 
        // panel2
        // 
        this.panel2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.panel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.line4,
            this.label12,
            this.label13,
            this.line5,
            this.table4,
            this.pictureBox2,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19});
        this.panel2.Dpi = 254F;
        this.panel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1307.446F);
        this.panel2.Name = "panel2";
        this.panel2.SizeF = new System.Drawing.SizeF(1012F, 1020F);
        this.panel2.StylePriority.UseBorders = false;
        // 
        // line4
        // 
        this.line4.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.line4.Dpi = 254F;
        this.line4.LineWidth = 3;
        this.line4.LocationFloat = new DevExpress.Utils.PointFloat(3.000023F, 665F);
        this.line4.Name = "line4";
        this.line4.SizeF = new System.Drawing.SizeF(1008.417F, 38.50381F);
        this.line4.StylePriority.UseBorders = false;
        // 
        // label12
        // 
        this.label12.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameterCidadeEstado, "Text", "")});
        this.label12.Dpi = 254F;
        this.label12.LocationFloat = new DevExpress.Utils.PointFloat(498F, 780.5461F);
        this.label12.Name = "label12";
        this.label12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label12.SizeF = new System.Drawing.SizeF(500.5435F, 50F);
        this.label12.StylePriority.UseBorders = false;
        this.label12.Text = "label10";
        // 
        // label14
        // 
        this.label14.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter01_Nome, "Text", "")});
        this.label14.Dpi = 254F;
        this.label14.LocationFloat = new DevExpress.Utils.PointFloat(87.41763F, 536.2738F);
        this.label14.Name = "label14";
        this.label14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label14.SizeF = new System.Drawing.SizeF(876.0416F, 45F);
        this.label14.StylePriority.UseBorders = false;
        this.label14.Text = "label5";
        // 
        // label15
        // 
        this.label15.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label15.Dpi = 254F;
        this.label15.LocationFloat = new DevExpress.Utils.PointFloat(92.70934F, 491.2738F);
        this.label15.Name = "label15";
        this.label15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label15.SizeF = new System.Drawing.SizeF(669.6666F, 45F);
        this.label15.StylePriority.UseBorders = false;
        this.label15.Text = "NOME / TELEFONE";
        // 
        // label17
        // 
        this.label17.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label17.Dpi = 254F;
        this.label17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label17.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 722.1262F);
        this.label17.Name = "label17";
        this.label17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label17.SizeF = new System.Drawing.SizeF(515F, 50F);
        this.label17.StylePriority.UseBorders = false;
        this.label17.StylePriority.UseFont = false;
        this.label17.Text = "DARF válido para pagamento até";
        // 
        // label18
        // 
        this.label18.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label18.Dpi = 254F;
        this.label18.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 780.5463F);
        this.label18.Name = "label18";
        this.label18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label18.SizeF = new System.Drawing.SizeF(454F, 50F);
        this.label18.StylePriority.UseBorders = false;
        this.label18.Text = "Domicílio tributário informado:";
        // 
        // label19
        // 
        this.label19.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.label19.Dpi = 254F;
        this.label19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
        this.label19.LocationFloat = new DevExpress.Utils.PointFloat(43.03981F, 830.5461F);
        this.label19.Name = "label19";
        this.label19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label19.SizeF = new System.Drawing.SizeF(955.9874F, 58.4201F);
        this.label19.StylePriority.UseBorders = false;
        this.label19.StylePriority.UseFont = false;
        this.label19.Text = "NÂO RECEBER COM RASURAS";
        // 
        // tableCell26
        // 
        this.tableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell26.Dpi = 254F;
        this.tableCell26.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell26.Name = "tableCell26";
        this.tableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell26.StylePriority.UseBorders = false;
        this.tableCell26.StylePriority.UseFont = false;
        this.tableCell26.StylePriority.UsePadding = false;
        this.tableCell26.Text = "10";
        this.tableCell26.Weight = 0.18618706043283639;
        // 
        // tableRow6
        // 
        this.tableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell17,
            this.tableCell36,
            this.tableCell18});
        this.tableRow6.Dpi = 254F;
        this.tableRow6.Name = "tableRow6";
        this.tableRow6.Weight = 0.55409742229080106;
        // 
        // tableCell17
        // 
        this.tableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell17.Dpi = 254F;
        this.tableCell17.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell17.Name = "tableCell17";
        this.tableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell17.StylePriority.UseBorders = false;
        this.tableCell17.StylePriority.UseFont = false;
        this.tableCell17.StylePriority.UsePadding = false;
        this.tableCell17.Text = "07";
        this.tableCell17.Weight = 0.18618706043283639;
        // 
        // tableRow10
        // 
        this.tableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell29,
            this.tableCell40});
        this.tableRow10.Dpi = 254F;
        this.tableRow10.Name = "tableRow10";
        this.tableRow10.Weight = 0.91571876644770189;
        // 
        // tableCell29
        // 
        this.tableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell29.Dpi = 254F;
        this.tableCell29.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell29.Name = "tableCell29";
        this.tableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 16, 0, 254F);
        this.tableCell29.StylePriority.UseBorders = false;
        this.tableCell29.StylePriority.UseFont = false;
        this.tableCell29.StylePriority.UsePadding = false;
        this.tableCell29.Text = "11";
        this.tableCell29.Weight = 0.18618689808818634;
        // 
        // tableCell40
        // 
        this.tableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell40.Dpi = 254F;
        this.tableCell40.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell40.Name = "tableCell40";
        this.tableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 16, 0, 254F);
        this.tableCell40.StylePriority.UseBorders = false;
        this.tableCell40.StylePriority.UseFont = false;
        this.tableCell40.StylePriority.UsePadding = false;
        this.tableCell40.Text = "AUTENTICAÇÃO BANCÁRIA (Somente nas 1ª e 2ª vias)";
        this.tableCell40.Weight = 2.8138131019118138;
        // 
        // tableRow7
        // 
        this.tableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell20,
            this.tableCell37,
            this.tableCell21});
        this.tableRow7.Dpi = 254F;
        this.tableRow7.Name = "tableRow7";
        this.tableRow7.Weight = 0.55409733329232269;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.line6,
            this.panel2,
            this.table3,
            this.label8,
            this.line2,
            this.panel1,
            this.label1,
            this.table1});
        this.PageHeader.Dpi = 254F;
        this.PageHeader.HeightF = 2489.911F;
        this.PageHeader.Name = "PageHeader";
        // 
        // line6
        // 
        this.line6.Dpi = 254F;
        this.line6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.line6.LineWidth = 3;
        this.line6.LocationFloat = new DevExpress.Utils.PointFloat(6.417299F, 2350.446F);
        this.line6.Name = "line6";
        this.line6.SizeF = new System.Drawing.SizeF(1950.583F, 25F);
        // 
        // table1
        // 
        this.table1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.table1.Dpi = 254F;
        this.table1.LocationFloat = new DevExpress.Utils.PointFloat(1011.417F, 69.47347F);
        this.table1.Name = "table1";
        this.table1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow1,
            this.tableRow2,
            this.tableRow3,
            this.tableRow4,
            this.tableRow5,
            this.tableRow6,
            this.tableRow7,
            this.tableRow8,
            this.tableRow9,
            this.tableRow10});
        this.table1.SizeF = new System.Drawing.SizeF(947.5836F, 1013F);
        this.table1.StylePriority.UseBorders = false;
        // 
        // tableRow2
        // 
        this.tableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell5,
            this.tableCell32,
            this.tableCell6});
        this.tableRow2.Dpi = 254F;
        this.tableRow2.Name = "tableRow2";
        this.tableRow2.Weight = 0.55409736757876893;
        // 
        // tableCell32
        // 
        this.tableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell32.Dpi = 254F;
        this.tableCell32.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell32.Name = "tableCell32";
        this.tableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell32.StylePriority.UseBorders = false;
        this.tableCell32.StylePriority.UseFont = false;
        this.tableCell32.StylePriority.UsePadding = false;
        this.tableCell32.Text = "NÚMERO DO CPF OU CNPJ";
        this.tableCell32.Weight = 1.5078239808694964;
        // 
        // tableRow4
        // 
        this.tableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell11,
            this.tableCell34,
            this.tableCell12});
        this.tableRow4.Dpi = 254F;
        this.tableRow4.Name = "tableRow4";
        this.tableRow4.Weight = 0.55409733329232291;
        // 
        // tableRow5
        // 
        this.tableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell14,
            this.tableCell35,
            this.tableCell15});
        this.tableRow5.Dpi = 254F;
        this.tableRow5.Name = "tableRow5";
        this.tableRow5.Weight = 0.55409742229080083;
        // 
        // tableCell35
        // 
        this.tableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell35.Dpi = 254F;
        this.tableCell35.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.tableCell35.Name = "tableCell35";
        this.tableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 12, 0, 254F);
        this.tableCell35.StylePriority.UseBorders = false;
        this.tableCell35.StylePriority.UseFont = false;
        this.tableCell35.StylePriority.UsePadding = false;
        this.tableCell35.Text = "DATA DE VENCIMENTO";
        this.tableCell35.Weight = 1.5078239808694964;
        // 
        // tableRow9
        // 
        this.tableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell26,
            this.tableCell39,
            this.tableCell27});
        this.tableRow9.Dpi = 254F;
        this.tableRow9.Name = "tableRow9";
        this.tableRow9.Weight = 0.55409733329232225;
        // 
        // tableCell27
        // 
        this.tableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.parameter10_ValorTotal, "Text", "{0:n2}")});
        this.tableCell27.Dpi = 254F;
        this.tableCell27.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.tableCell27.Name = "tableCell27";
        this.tableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 11, 12, 0, 254F);
        this.tableCell27.StylePriority.UseBorders = false;
        this.tableCell27.StylePriority.UseFont = false;
        this.tableCell27.StylePriority.UsePadding = false;
        this.tableCell27.StylePriority.UseTextAlignment = false;
        this.tableCell27.Text = "tableCell27";
        this.tableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell27.Weight = 1.305988958697667;
        // 
        // Detail
        // 
        this.Detail.Dpi = 254F;
        this.Detail.Expanded = false;
        this.Detail.HeightF = 0F;
        this.Detail.Name = "Detail";
        // 
        // ReportDarf
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.Detail,
            this.BottomMargin,
            this.PageHeader});
        this.Dpi = 254F;
        this.Margins = new System.Drawing.Printing.Margins(98, 98, 150, 150);
        this.PageHeight = 2794;
        this.PageWidth = 2159;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.parameter02_PeriodoApuracao,
            this.parameter03_CPF_CNPJ,
            this.parameter04_CodigoReceita,
            this.parameter05_NumeroReferencia,
            this.parameter06_DataVencimento,
            this.parameter07_ValorPrincipal,
            this.parameter08_ValorMulta,
            this.parameter09_ValorJuros,
            this.parameter10_ValorTotal,
            this.parameter01_Nome,
            this.parameterCidadeEstado,
            this.parameterDataPagamentoDarf});
        this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
        this.Scripts.OnParametersRequestBeforeShow = "Report1_ParametersRequestBeforeShow";
        this.SnapGridSize = 31.75F;
        this.Version = "11.1";
        ((System.ComponentModel.ISupportInitialize)(this.table2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
