﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using Financial.Util;
using Financial.Investidor;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for RelatorioGanhoFII
    /// </summary>
    public class ReportGanhoFII : DevExpress.XtraReports.UI.XtraReport {
        //
        private int ano; // ano do relatorio
        private int mes; // mes do relatorio
        private int idCliente;
        //
        private string nomeCliente;

        private List<int> mesExecucao; // Lista com os meses de execução do relatorio
        private List<int> anoExecucao; // Lista com os anos de execução do relatorio
        //                        
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell4;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell7;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell17;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell20;
        private Financial.Tributo.Relatorio.GanhoFIICollection ganhoFIICollection1;
        private XRTable xrTable12;
        private XRTableRow xrTableRow123;
        private XRTableCell xrTableCell3;
        private XRTableRow xrTableRow124;
        private XRTableCell xrTableCell8;
        private XRTable xrTable13;
        private XRTableRow xrTableRow125;
        private XRTableCell xrTableCell6;
        private XRTable xrTable14;
        private XRTableRow xrTableRow126;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell5;
        private XRTable xrTable19;
        private XRTableRow xrTableRow17;
        private XRTableCell xrTableCell38;
        private XRTableRow xrTableRow18;
        private XRTableCell xrTableCell40;
        private XRTableRow xrTableRow20;
        private XRTableCell xrTableCell41;
        private XRTableRow xrTableRow21;
        private XRTableCell xrTableCell42;
        private XRTableRow xrTableRow22;
        private XRTableCell xrTableCell43;
        private XRTableRow xrTableRow11;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell175;
        private XRTableCell xrTableCell176;
        private XRTableCell xrTableCell177;
        private XRTableCell xrTableCell179;
        private XRTableCell xrTableCell180;
        private XRTableCell xrTableCell181;
        private XRTableCell xrTableCell182;
        private XRTableCell xrTableCell183;
        private XRTableCell xrTableCell184;
        private XRTableCell xrTableCell186;
        private XRTableCell xrTableCell187;
        private XRTableCell xrTableCell188;
        private XRTableCell xrTableCell244;
        private XRTableCell xrTableCell253;
        private XRTableCell xrTableCell255;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell178;
        private XRTableCell xrTableCell185;
        private XRTable xrTable4;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell9;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTable xrTable18;
        private XRTableRow xrTableRow150;
        private XRTableCell xrTableCell248;
        private XRTableRow xrTableRow159;
        private XRTableCell xrTableCell249;
        private XRTableRow xrTableRow160;
        private XRTableCell xrTableCell251;
        private XRTableRow xrTableRow161;
        private XRTableCell xrTableCell252;
        private XRTableRow xrTableRow174;
        private XRTableCell xrTableCell278;
        private XRTableRow xrTableRow175;
        private XRTableCell xrTableCell280;
        private XRTableRow xrTableRow176;
        private XRTableCell xrTableCell282;
        private XRTableRow xrTableRow177;
        private XRTableCell xrTableCell284;
        private XRTableRow xrTableRow178;
        private XRTableCell xrTableCell286;
        private XRTableRow xrTableRow179;
        private XRTableCell xrTableCell288;
        private XRTableRow xrTableRow180;
        private XRTableCell xrTableCell290;
        private XRTable xrTable5;
        private XRTableRow xrTableRow26;
        private XRTableCell xrTableCell46;
        private XRTableRow xrTableRow27;
        private XRTableCell xrTableCell47;
        private XRTableRow xrTableRow28;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableRow xrTableRow29;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableRow xrTableRow43;
        private XRTableCell xrTableCell77;
        private XRTableCell xrTableCell78;
        private XRTableRow xrTableRow44;
        private XRTableCell xrTableCell79;
        private XRTableCell xrTableCell80;
        private XRTableRow xrTableRow45;
        private XRTableCell xrTableCell81;
        private XRTableCell xrTableCell82;
        private XRTableRow xrTableRow46;
        private XRTableCell xrTableCell83;
        private XRTableCell xrTableCell84;
        private XRTableRow xrTableRow47;
        private XRTableCell xrTableCell85;
        private XRTableCell xrTableCell86;
        private XRTableRow xrTableRow48;
        private XRTableCell xrTableCell87;
        private XRTableCell xrTableCell88;
        private XRTableRow xrTableRow71;
        private XRTableCell xrTableCell245;
        private XRTableCell xrTableCell246;
        private XRTable xrTable6;
        private XRTableRow xrTableRow19;
        private XRTableCell xrTableCell39;
        private XRTableRow xrTableRow42;
        private XRTableCell xrTableCell62;
        private XRTableRow xrTableRow49;
        private XRTableCell xrTableCell89;
        private XRTableCell xrTableCell90;
        private XRTableRow xrTableRow50;
        private XRTableCell xrTableCell91;
        private XRTableCell xrTableCell92;
        private XRTableRow xrTableRow63;
        private XRTableCell xrTableCell117;
        private XRTableCell xrTableCell118;
        private XRTableRow xrTableRow64;
        private XRTableCell xrTableCell120;
        private XRTableCell xrTableCell121;
        private XRTableRow xrTableRow66;
        private XRTableCell xrTableCell122;
        private XRTableCell xrTableCell123;
        private XRTableRow xrTableRow67;
        private XRTableCell xrTableCell124;
        private XRTableCell xrTableCell125;
        private XRTableRow xrTableRow68;
        private XRTableCell xrTableCell126;
        private XRTableCell xrTableCell127;
        private XRTableRow xrTableRow69;
        private XRTableCell xrTableCell128;
        private XRTableCell xrTableCell129;
        private XRTableRow xrTableRow70;
        private XRTableCell xrTableCell130;
        private XRTableCell xrTableCell131;
        private XRTable xrTable16;
        private XRTableRow xrTableRow134;
        private XRTableCell xrTableCell198;
        private XRTableRow xrTableRow135;
        private XRTableCell xrTableCell199;
        private XRTableRow xrTableRow136;
        private XRTableCell xrTableCell200;
        private XRTableCell xrTableCell201;
        private XRTableRow xrTableRow137;
        private XRTableCell xrTableCell202;
        private XRTableCell xrTableCell203;
        private XRTableRow xrTableRow88;
        private XRTableCell xrTableCell162;
        private XRTableCell xrTableCell228;
        private XRTableRow xrTableRow151;
        private XRTableCell xrTableCell229;
        private XRTableCell xrTableCell230;
        private XRTableRow xrTableRow152;
        private XRTableCell xrTableCell231;
        private XRTableCell xrTableCell232;
        private XRTableRow xrTableRow153;
        private XRTableCell xrTableCell233;
        private XRTableCell xrTableCell234;
        private XRTableRow xrTableRow154;
        private XRTableCell xrTableCell235;
        private XRTableCell xrTableCell236;
        private XRTableRow xrTableRow155;
        private XRTableCell xrTableCell237;
        private XRTableCell xrTableCell238;
        private XRTableRow xrTableRow156;
        private XRTableCell xrTableCell239;
        private XRTableCell xrTableCell240;
        private XRTable xrTable7;
        private XRTableRow xrTableRow72;
        private XRTableCell xrTableCell132;
        private XRTableRow xrTableRow73;
        private XRTableCell xrTableCell133;
        private XRTableRow xrTableRow74;
        private XRTableCell xrTableCell134;
        private XRTableCell xrTableCell135;
        private XRTableRow xrTableRow75;
        private XRTableCell xrTableCell136;
        private XRTableCell xrTableCell137;
        private XRTableRow xrTableRow65;
        private XRTableCell xrTableCell119;
        private XRTableCell xrTableCell247;
        private XRTableRow xrTableRow89;
        private XRTableCell xrTableCell163;
        private XRTableCell xrTableCell164;
        private XRTableRow xrTableRow90;
        private XRTableCell xrTableCell165;
        private XRTableCell xrTableCell166;
        private XRTableRow xrTableRow91;
        private XRTableCell xrTableCell167;
        private XRTableCell xrTableCell168;
        private XRTableRow xrTableRow92;
        private XRTableCell xrTableCell169;
        private XRTableCell xrTableCell170;
        private XRTableRow xrTableRow93;
        private XRTableCell xrTableCell171;
        private XRTableCell xrTableCell172;
        private XRTableRow xrTableRow94;
        private XRTableCell xrTableCell173;
        private XRTableCell xrTableCell174;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Construtor da classe de Relatorio   
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        public ReportGanhoFII(int idCliente, int mes, int ano) {
            this.idCliente = idCliente;
            this.ano = ano;
            this.mes = mes;
            //
            #region Define Mes e Ano de execução do Relatório
            this.mesExecucao = new List<int>();
            this.anoExecucao = new List<int>();

            int mesAux = this.mes - 3; // tres meses atras
            // março para tras
            if (mesAux <= 0) {
                #region Janeiro/Fevereiro/Março
                switch (this.mes) {
                    //Janeiro
                    case 1:
                        this.mesExecucao.Add(10); //Outubro
                        this.mesExecucao.Add(11);
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Fevereiro
                    case 2:
                        this.mesExecucao.Add(11); //Novembro
                        this.mesExecucao.Add(12);
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                    //Março
                    case 3:
                        this.mesExecucao.Add(12); //Dezembro
                        this.mesExecucao.Add(1);
                        this.mesExecucao.Add(2);
                        this.mesExecucao.Add(3);
                        this.anoExecucao.Add(this.ano - 1);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        this.anoExecucao.Add(this.ano);
                        break;
                }
                #endregion
            }
            else {
                // Adiciona 4 meses do menor para o maior
                for (int i = 3; i >= 0; i--) {
                    this.anoExecucao.Add(this.ano);
                    this.mesExecucao.Add(this.mes - i);
                }
            }
            #endregion

            // Carrega NomeCliente
            #region Carrega NomeCliente
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(this.idCliente)) {
                this.nomeCliente = cliente.Nome;
            }
            #endregion

            this.InitializeComponent();
            this.PersonalInitialize();
            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
        }

        /// <summary>
        ///  Preeenche o DataSet do Relatório  
        /// </summary>
        private void PersonalInitialize() {
            DataTable dt = ganhoFIICollection1.FillDataTable(this.idCliente, this.mesExecucao, this.anoExecucao);
            this.DataSource = dt;

            // Campos do resource que possuem acentos
            //this.xrTableCell25.Text = Resources.ReportGanhoRendaVariavel._IRFonteDaytradeMes;
            //this.xrTableCell47.Text = Resources.ReportGanhoRendaVariavel._Operacoes;
            //this.xrTableCell62.Text = Resources.ReportGanhoRendaVariavel._Operacoes;
            //this.xrTableCell133.Text = Resources.ReportGanhoRendaVariavel._Operacoes;
            //this.xrTableCell199.Text = Resources.ReportGanhoRendaVariavel._Operacoes;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Legais/ImpostoRenda/ReportGanhoRendaVariavel.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportGanhoFII.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow150 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow159 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow160 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow161 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow174 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow175 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow176 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow177 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow178 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow179 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow180 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow134 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow135 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow136 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow137 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow151 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow152 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow153 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow154 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow155 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow156 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow92 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ganhoFIICollection1 = new Financial.Tributo.Relatorio.GanhoFIICollection();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow126 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow125 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow123 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow124 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable3,
            this.xrTable1,
            this.xrTable19,
            this.xrTable18,
            this.xrTable5,
            this.xrTable6,
            this.xrTable16,
            this.xrTable7,
            this.xrTable2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 1182.833F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 781.7291F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2497F, 25F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell9.Weight = 1;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(101F, 175F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2497F, 47F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell17});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "APURAÇÃO DE GANHOS OU PERDAS E DO IMPOSTO";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell13.Weight = 0.63996796155386459;
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubTituloDetailBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.xrTableCell17.Summary = xrSummary1;
            this.xrTableCell17.Text = "Valores em Reais\r\n";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell17.Weight = 0.36003203844613535;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 77F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow4});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2498F, 92F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell4});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.4891304347826087;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "CPF/CNPJ";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell1.Weight = 0.27101681345076062;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell4.Text = "NOME";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell4.Weight = 0.72898318654923944;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.51086956521739135;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "$CNPJ";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell19.Weight = 0.27101681345076062;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CNPJBeforePrint);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell20.Text = "$NomeCliente";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell20.Weight = 0.72898318654923944;
            this.xrTableCell20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeClienteBeforePrint);
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.Dpi = 254F;
            this.xrTable19.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 813.7292F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17,
            this.xrTableRow5,
            this.xrTableRow18,
            this.xrTableRow20,
            this.xrTableRow11,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTable19.SizeF = new System.Drawing.SizeF(2497F, 329F);
            this.xrTable19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell26,
            this.xrTableCell36,
            this.xrTableCell179,
            this.xrTableCell186});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow17.Weight = 0.14285714285714285;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell38.Text = "20 - Total do imposto devido (A + B)";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell38.Weight = 0.27152583099719663;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalImposto", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.19503404084901882;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalImposto1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.17781337605126152;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalImposto2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell179.Text = "xrTableCell179";
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell179.Weight = 0.18101722066479775;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TotalImposto3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell186.Text = "xrTableCell186";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell186.Weight = 0.17460953143772526;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell35,
            this.xrTableCell37,
            this.xrTableCell178,
            this.xrTableCell185});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.14285714285714285;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell25.Text = "21 - IR fonte daytrade do mês";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.27152583099719663;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMes", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell35.Text = "xrTableCell35";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.19503404084901882;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMes1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 0.17781337605126152;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMes2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell178.Text = "xrTableCell178";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell178.Weight = 0.18101722066479775;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMes3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell185.Text = "xrTableCell185";
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell185.Weight = 0.17460953143772526;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell27,
            this.xrTableCell44,
            this.xrTableCell180,
            this.xrTableCell187});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow18.Weight = 0.14285714285714285;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell40.Text = "22 - IR fonte de meses anteriores";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.27152583099719663;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMesAnterior", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.19503404084901882;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMesAnterior1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell44.Text = "xrTableCell44";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell44.Weight = 0.17781337605126152;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMesAnterior2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell180.Text = "xrTableCell180";
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell180.Weight = 0.18101722066479775;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeMesAnterior3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell187.Text = "xrTableCell187";
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell187.Weight = 0.17460953143772526;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell29,
            this.xrTableCell45,
            this.xrTableCell181,
            this.xrTableCell188});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.14285714285714285;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell41.Text = "23 - IR fonte a compensar";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.27152583099719663;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeCompensar", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.19503404084901882;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeCompensar1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell45.Weight = 0.17781337605126152;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeCompensar2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell181.Text = "xrTableCell181";
            this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell181.Weight = 0.18101722066479775;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRDayTradeCompensar3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell188.Text = "xrTableCell188";
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell188.Weight = 0.17460953143772526;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell30,
            this.xrTableCell175,
            this.xrTableCell182,
            this.xrTableCell244});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.14285714285714285;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell32.Text = "24 - IR Fonte (lei 11.033 de 2004) - 0,005%";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell32.Weight = 0.27152583099719663;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRFonteNormal", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.19503404084901882;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRFonteNormal1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell175.Text = "xrTableCell175";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell175.Weight = 0.17781337605126152;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRFonteNormal2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell182.Text = "xrTableCell182";
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell182.Weight = 0.18101722066479775;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IRFonteNormal3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell244.Dpi = 254F;
            this.xrTableCell244.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell244.Text = "xrTableCell244";
            this.xrTableCell244.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell244.Weight = 0.17460953143772526;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell31,
            this.xrTableCell176,
            this.xrTableCell183,
            this.xrTableCell253});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow21.Weight = 0.14285714285714285;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell42.Text = "25 - Imposto a pagar (20 - 21 - 22)";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell42.Weight = 0.27152583099719663;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPagar", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell31.Weight = 0.19503404084901882;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPagar1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell176.Text = "xrTableCell176";
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell176.Weight = 0.17781337605126152;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPagar2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell183.Text = "xrTableCell183";
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell183.Weight = 0.18101722066479775;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPagar3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell253.Dpi = 254F;
            this.xrTableCell253.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell253.Text = "xrTableCell253";
            this.xrTableCell253.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell253.Weight = 0.17460953143772526;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell33,
            this.xrTableCell177,
            this.xrTableCell184,
            this.xrTableCell255});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow22.Weight = 0.14285714285714285;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell43.Text = "26 - Imposto pago";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.27152583099719663;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPago", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell33.Text = "xrTableCell33";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.19503404084901882;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPago1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell177.Text = "xrTableCell177";
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell177.Weight = 0.17781337605126152;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPago2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell184.Text = "xrTableCell184";
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell184.Weight = 0.18101722066479775;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoPago3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell255.Dpi = 254F;
            this.xrTableCell255.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell255.Text = "xrTableCell255";
            this.xrTableCell255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell255.Weight = 0.17460953143772526;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.Dpi = 254F;
            this.xrTable18.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(100F, 230F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow150,
            this.xrTableRow159,
            this.xrTableRow160,
            this.xrTableRow161,
            this.xrTableRow174,
            this.xrTableRow175,
            this.xrTableRow176,
            this.xrTableRow177,
            this.xrTableRow178,
            this.xrTableRow179,
            this.xrTableRow180});
            this.xrTable18.SizeF = new System.Drawing.SizeF(677F, 539F);
            this.xrTable18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailBeforePrint);
            // 
            // xrTableRow150
            // 
            this.xrTableRow150.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell248});
            this.xrTableRow150.Dpi = 254F;
            this.xrTableRow150.Name = "xrTableRow150";
            this.xrTableRow150.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow150.Weight = 0.072529465095194923;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell248.Dpi = 254F;
            this.xrTableCell248.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell248.Multiline = true;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell248.Weight = 1;
            // 
            // xrTableRow159
            // 
            this.xrTableRow159.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell249});
            this.xrTableRow159.Dpi = 254F;
            this.xrTableRow159.Name = "xrTableRow159";
            this.xrTableRow159.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow159.Weight = 0.045330915684496827;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Dpi = 254F;
            this.xrTableCell249.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell249.Multiline = true;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell249.Text = "DISCRIMINAÇÃO";
            this.xrTableCell249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell249.Weight = 1;
            // 
            // xrTableRow160
            // 
            this.xrTableRow160.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251});
            this.xrTableRow160.Dpi = 254F;
            this.xrTableRow160.Name = "xrTableRow160";
            this.xrTableRow160.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow160.Weight = 0.045330915684496827;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Dpi = 254F;
            this.xrTableCell251.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell251.Multiline = true;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell251.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell251.Weight = 1;
            // 
            // xrTableRow161
            // 
            this.xrTableRow161.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell252});
            this.xrTableRow161.Dpi = 254F;
            this.xrTableRow161.Name = "xrTableRow161";
            this.xrTableRow161.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow161.Weight = 0.042611060743427021;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Dpi = 254F;
            this.xrTableCell252.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell252.Text = "01 - Resultado do mês em FII";
            this.xrTableCell252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell252.Weight = 1;
            // 
            // xrTableRow174
            // 
            this.xrTableRow174.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell278});
            this.xrTableRow174.Dpi = 254F;
            this.xrTableRow174.Name = "xrTableRow174";
            this.xrTableRow174.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow174.Weight = 0.027198549410698096;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Dpi = 254F;
            this.xrTableCell278.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell278.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell278.Weight = 1;
            // 
            // xrTableRow175
            // 
            this.xrTableRow175.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell280});
            this.xrTableRow175.Dpi = 254F;
            this.xrTableRow175.Name = "xrTableRow175";
            this.xrTableRow175.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow175.Weight = 0.042611060743427021;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Dpi = 254F;
            this.xrTableCell280.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell280.Text = "14 - RESULTADO LÍQUIDO DO MÊS";
            this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell280.Weight = 1;
            // 
            // xrTableRow176
            // 
            this.xrTableRow176.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282});
            this.xrTableRow176.Dpi = 254F;
            this.xrTableRow176.Name = "xrTableRow176";
            this.xrTableRow176.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow176.Weight = 0.042611060743427021;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Dpi = 254F;
            this.xrTableCell282.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell282.Text = "15 - Resultado Negativo até o mês anterior";
            this.xrTableCell282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell282.Weight = 1;
            // 
            // xrTableRow177
            // 
            this.xrTableRow177.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell284});
            this.xrTableRow177.Dpi = 254F;
            this.xrTableRow177.Name = "xrTableRow177";
            this.xrTableRow177.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow177.Weight = 0.042611060743427021;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Dpi = 254F;
            this.xrTableCell284.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell284.Text = "16 - Base de Cálculo p/Imposto (14-15)";
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell284.Weight = 1;
            // 
            // xrTableRow178
            // 
            this.xrTableRow178.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell286});
            this.xrTableRow178.Dpi = 254F;
            this.xrTableRow178.Name = "xrTableRow178";
            this.xrTableRow178.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow178.Weight = 0.042611060743427021;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Dpi = 254F;
            this.xrTableCell286.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell286.Text = "17 - Prejuízo a compensar";
            this.xrTableCell286.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell286.Weight = 1;
            // 
            // xrTableRow179
            // 
            this.xrTableRow179.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell288});
            this.xrTableRow179.Dpi = 254F;
            this.xrTableRow179.Name = "xrTableRow179";
            this.xrTableRow179.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow179.Weight = 0.042611060743427021;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Dpi = 254F;
            this.xrTableCell288.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell288.Text = "18 - Alíquota de imposto";
            this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell288.Weight = 1;
            // 
            // xrTableRow180
            // 
            this.xrTableRow180.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290});
            this.xrTableRow180.Dpi = 254F;
            this.xrTableRow180.Name = "xrTableRow180";
            this.xrTableRow180.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow180.Weight = 0.042611060743427021;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Dpi = 254F;
            this.xrTableCell290.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell290.Text = "19 - Imposto devido (16 x 18)";
            this.xrTableCell290.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell290.Weight = 1;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(777F, 230F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow71});
            this.xrTable5.SizeF = new System.Drawing.SizeF(487F, 539F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow26.Weight = 0.072529465095194923;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell46.Multiline = true;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell46.Text = "$Mes1";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell46.Weight = 1;
            this.xrTableCell46.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes1BeforePrint);
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow27.Weight = 0.045330915684496827;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell47.Text = "Operações";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell47.Weight = 1;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 0.045330915684496827;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell48.Text = "Comuns";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell48.Weight = 0.50308008213552358;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell49.Multiline = true;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell49.Text = "Daytrade";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 0.49691991786447637;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow29.Weight = 0.042611060743427021;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNormal", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell50.Text = "ResultadoNormal";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.50308008213552358;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell51.Text = "ResultadoDT";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.49691991786447637;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow43.Dpi = 254F;
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow43.Weight = 0.027198549410698096;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell77.Weight = 0.50308008213552358;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell78.Weight = 0.49691991786447637;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow44.Dpi = 254F;
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow44.Weight = 0.042611060743427021;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMes", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell79.Text = "xrTableCell79";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell79.Weight = 0.50308008213552358;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMesDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell80.Text = "xrTableCell80";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell80.Weight = 0.49691991786447637;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow45.Dpi = 254F;
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow45.Weight = 0.042611060743427021;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnterior", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell81.Text = "xrTableCell81";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell81.Weight = 0.50308008213552358;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnteriorDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell82.Text = "xrTableCell82";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell82.Weight = 0.49691991786447637;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell84});
            this.xrTableRow46.Dpi = 254F;
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow46.Weight = 0.042611060743427021;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculo", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell83.Text = "xrTableCell83";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell83.Weight = 0.50308008213552358;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculoDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell84.Text = "xrTableCell84";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell84.Weight = 0.49691991786447637;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow47.Weight = 0.042611060743427021;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensar", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell85.Text = "xrTableCell85";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell85.Weight = 0.50308008213552358;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensarDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell86.Text = "xrTableCell86";
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell86.Weight = 0.49691991786447637;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow48.Weight = 0.042611060743427021;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell87.Text = "20%";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell87.Weight = 0.50308008213552358;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Dpi = 254F;
            this.xrTableCell88.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell88.Text = "20%";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell88.Weight = 0.49691991786447637;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell245,
            this.xrTableCell246});
            this.xrTableRow71.Dpi = 254F;
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow71.Weight = 0.042611060743427021;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculado", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell245.Dpi = 254F;
            this.xrTableCell245.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell245.Text = "xrTableCell245";
            this.xrTableCell245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell245.Weight = 0.50308008213552358;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculadoDT", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell246.Dpi = 254F;
            this.xrTableCell246.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell246.Text = "xrTableCell246";
            this.xrTableCell246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell246.Weight = 0.49691991786447637;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(1265F, 230F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19,
            this.xrTableRow42,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow66,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69,
            this.xrTableRow70});
            this.xrTable6.SizeF = new System.Drawing.SizeF(452F, 540F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow19.Weight = 0.072463768115942032;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Multiline = true;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell39.Text = "$Mes2";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell39.Weight = 1;
            this.xrTableCell39.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes2BeforePrint);
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62});
            this.xrTableRow42.Dpi = 254F;
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow42.Weight = 0.045289855072463768;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell62.Text = "Operações";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 1;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow49.Weight = 0.045289855072463768;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell89.Text = "Comuns";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell89.Weight = 0.504424778761062;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Dpi = 254F;
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell90.Multiline = true;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell90.Text = "Daytrade";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell90.Weight = 0.49557522123893805;
            // 
            // xrTableRow50
            // 
            this.xrTableCell91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNormal1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow50.Weight = 0.042572463768115944;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell91.Text = "xrTableCell91";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell91.Weight = 0.504424778761062;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell92.Text = "xrTableCell92";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell92.Weight = 0.49557522123893805;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow63.Weight = 0.02717391304347826;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell117.Weight = 0.504424778761062;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell118.Weight = 0.49557522123893805;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell121});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow64.Weight = 0.042572463768115944;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMes1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell120.Text = "xrTableCell120";
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell120.Weight = 0.504424778761062;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMesDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell121.Text = "xrTableCell121";
            this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell121.Weight = 0.49557522123893805;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123});
            this.xrTableRow66.Dpi = 254F;
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow66.Weight = 0.042572463768115944;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnterior1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell122.Text = "xrTableCell122";
            this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell122.Weight = 0.504424778761062;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnteriorDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell123.Text = "xrTableCell123";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell123.Weight = 0.49557522123893805;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124,
            this.xrTableCell125});
            this.xrTableRow67.Dpi = 254F;
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow67.Weight = 0.042572463768115944;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculo1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell124.Text = "xrTableCell124";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell124.Weight = 0.504424778761062;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculoDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell125.Text = "xrTableCell125";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell125.Weight = 0.49557522123893805;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127});
            this.xrTableRow68.Dpi = 254F;
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow68.Weight = 0.042572463768115944;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensar1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell126.Text = "xrTableCell126";
            this.xrTableCell126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell126.Weight = 0.504424778761062;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensarDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell127.Text = "xrTableCell127";
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell127.Weight = 0.49557522123893805;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTableCell129});
            this.xrTableRow69.Dpi = 254F;
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow69.Weight = 0.042572463768115944;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell128.Text = "20%";
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell128.Weight = 0.504424778761062;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell129.Text = "20%";
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell129.Weight = 0.49557522123893805;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell131});
            this.xrTableRow70.Dpi = 254F;
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow70.Weight = 0.043478260869565216;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculado1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell130.Text = "xrTableCell130";
            this.xrTableCell130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell130.Weight = 0.504424778761062;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculadoDT1", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell131.Text = "xrTableCell131";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell131.Weight = 0.49557522123893805;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Dpi = 254F;
            this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(2157F, 230F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow134,
            this.xrTableRow135,
            this.xrTableRow136,
            this.xrTableRow137,
            this.xrTableRow88,
            this.xrTableRow151,
            this.xrTableRow152,
            this.xrTableRow153,
            this.xrTableRow154,
            this.xrTableRow155,
            this.xrTableRow156});
            this.xrTable16.SizeF = new System.Drawing.SizeF(441F, 539F);
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow134
            // 
            this.xrTableRow134.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198});
            this.xrTableRow134.Dpi = 254F;
            this.xrTableRow134.Name = "xrTableRow134";
            this.xrTableRow134.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow134.Weight = 0.072529465095194923;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell198.Multiline = true;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell198.Text = "$Mes4";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell198.Weight = 1;
            this.xrTableCell198.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes4BeforePrint);
            // 
            // xrTableRow135
            // 
            this.xrTableRow135.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199});
            this.xrTableRow135.Dpi = 254F;
            this.xrTableRow135.Name = "xrTableRow135";
            this.xrTableRow135.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow135.Weight = 0.045330915684496827;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell199.Multiline = true;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell199.Text = "Operações";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell199.Weight = 1;
            // 
            // xrTableRow136
            // 
            this.xrTableRow136.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201});
            this.xrTableRow136.Dpi = 254F;
            this.xrTableRow136.Name = "xrTableRow136";
            this.xrTableRow136.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow136.Weight = 0.045330915684496827;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell200.Text = "Comuns";
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell200.Weight = 0.49886621315192742;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell201.Multiline = true;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell201.Text = "Daytrade";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell201.Weight = 0.50113378684807253;
            // 
            // xrTableRow137
            // 
            this.xrTableRow137.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell203});
            this.xrTableRow137.Dpi = 254F;
            this.xrTableRow137.Name = "xrTableRow137";
            this.xrTableRow137.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow137.Weight = 0.042611060743427021;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNormal3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell202.Text = "xrTableCell202";
            this.xrTableCell202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell202.Weight = 0.49886621315192742;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell203.Text = "xrTableCell203";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell203.Weight = 0.50113378684807253;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell228});
            this.xrTableRow88.Dpi = 254F;
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow88.Weight = 0.027198549410698096;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell162.Weight = 0.50340136054421769;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Dpi = 254F;
            this.xrTableCell228.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell228.Weight = 0.49659863945578231;
            // 
            // xrTableRow151
            // 
            this.xrTableRow151.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229,
            this.xrTableCell230});
            this.xrTableRow151.Dpi = 254F;
            this.xrTableRow151.Name = "xrTableRow151";
            this.xrTableRow151.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow151.Weight = 0.042611060743427021;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMes3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell229.Dpi = 254F;
            this.xrTableCell229.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell229.Text = "xrTableCell229";
            this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell229.Weight = 0.49886621315192742;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMesDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell230.Dpi = 254F;
            this.xrTableCell230.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell230.Text = "xrTableCell230";
            this.xrTableCell230.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell230.Weight = 0.50113378684807253;
            // 
            // xrTableRow152
            // 
            this.xrTableRow152.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell232});
            this.xrTableRow152.Dpi = 254F;
            this.xrTableRow152.Name = "xrTableRow152";
            this.xrTableRow152.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow152.Weight = 0.042611060743427021;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnterior3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell231.Dpi = 254F;
            this.xrTableCell231.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell231.Text = "xrTableCell231";
            this.xrTableCell231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell231.Weight = 0.49886621315192742;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnteriorDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell232.Dpi = 254F;
            this.xrTableCell232.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell232.Text = "xrTableCell232";
            this.xrTableCell232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell232.Weight = 0.50113378684807253;
            // 
            // xrTableRow153
            // 
            this.xrTableRow153.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234});
            this.xrTableRow153.Dpi = 254F;
            this.xrTableRow153.Name = "xrTableRow153";
            this.xrTableRow153.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow153.Weight = 0.042611060743427021;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculo3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell233.Dpi = 254F;
            this.xrTableCell233.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell233.Text = "xrTableCell233";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell233.Weight = 0.49886621315192742;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculoDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell234.Dpi = 254F;
            this.xrTableCell234.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell234.Text = "xrTableCell234";
            this.xrTableCell234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell234.Weight = 0.50113378684807253;
            // 
            // xrTableRow154
            // 
            this.xrTableRow154.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell236});
            this.xrTableRow154.Dpi = 254F;
            this.xrTableRow154.Name = "xrTableRow154";
            this.xrTableRow154.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow154.Weight = 0.042611060743427021;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensar3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell235.Dpi = 254F;
            this.xrTableCell235.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell235.Text = "xrTableCell235";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell235.Weight = 0.49886621315192742;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensarDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell236.Dpi = 254F;
            this.xrTableCell236.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell236.Text = "xrTableCell236";
            this.xrTableCell236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell236.Weight = 0.50113378684807253;
            // 
            // xrTableRow155
            // 
            this.xrTableRow155.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell237,
            this.xrTableCell238});
            this.xrTableRow155.Dpi = 254F;
            this.xrTableRow155.Name = "xrTableRow155";
            this.xrTableRow155.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow155.Weight = 0.042611060743427021;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Dpi = 254F;
            this.xrTableCell237.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell237.Text = "20%";
            this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell237.Weight = 0.49886621315192742;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Dpi = 254F;
            this.xrTableCell238.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell238.Text = "20%";
            this.xrTableCell238.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell238.Weight = 0.50113378684807253;
            // 
            // xrTableRow156
            // 
            this.xrTableRow156.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240});
            this.xrTableRow156.Dpi = 254F;
            this.xrTableRow156.Name = "xrTableRow156";
            this.xrTableRow156.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow156.Weight = 0.042611060743427021;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculado3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell239.Dpi = 254F;
            this.xrTableCell239.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell239.Text = "xrTableCell239";
            this.xrTableCell239.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell239.Weight = 0.49886621315192742;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculadoDT3", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell240.Dpi = 254F;
            this.xrTableCell240.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell240.Text = "xrTableCell240";
            this.xrTableCell240.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell240.Weight = 0.50113378684807253;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(1718F, 230F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72,
            this.xrTableRow73,
            this.xrTableRow74,
            this.xrTableRow75,
            this.xrTableRow65,
            this.xrTableRow89,
            this.xrTableRow90,
            this.xrTableRow91,
            this.xrTableRow92,
            this.xrTableRow93,
            this.xrTableRow94});
            this.xrTable7.SizeF = new System.Drawing.SizeF(439F, 539F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132});
            this.xrTableRow72.Dpi = 254F;
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow72.Weight = 0.072529465095194923;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell132.Multiline = true;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell132.Text = "$Mes3";
            this.xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell132.Weight = 1;
            this.xrTableCell132.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Mes3BeforePrint);
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133});
            this.xrTableRow73.Dpi = 254F;
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow73.Weight = 0.045330915684496827;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell133.Multiline = true;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell133.Text = "Operações";
            this.xrTableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell133.Weight = 1;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTableCell135});
            this.xrTableRow74.Dpi = 254F;
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow74.Weight = 0.045330915684496827;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell134.Text = "Comuns";
            this.xrTableCell134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell134.Weight = 0.50341685649202739;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell135.Text = "Daytrade";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell135.Weight = 0.49658314350797267;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137});
            this.xrTableRow75.Dpi = 254F;
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow75.Weight = 0.042611060743427021;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell136.Text = "[ResultadoDT2]";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell136.Weight = 0.50341685649202739;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell137.Text = "xrTableCell137";
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell137.Weight = 0.49658314350797267;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell247});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow65.Weight = 0.027198549410698096;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell119.Weight = 0.50341685649202739;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Dpi = 254F;
            this.xrTableCell247.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell247.Weight = 0.49658314350797267;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell164});
            this.xrTableRow89.Dpi = 254F;
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow89.Weight = 0.042611060743427021;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMes2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell163.Text = "xrTableCell163";
            this.xrTableCell163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell163.Weight = 0.50341685649202739;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoLiquidoMesDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell164.Text = "xrTableCell164";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell164.Weight = 0.49658314350797267;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165,
            this.xrTableCell166});
            this.xrTableRow90.Dpi = 254F;
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow90.Weight = 0.042611060743427021;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnterior2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell165.Text = "xrTableCell165";
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell165.Weight = 0.50341685649202739;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ResultadoNegativoAnteriorDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell166.Text = "xrTableCell166";
            this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell166.Weight = 0.49658314350797267;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow91.Dpi = 254F;
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow91.Weight = 0.042611060743427021;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculo2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell167.Text = "xrTableCell167";
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell167.Weight = 0.50341685649202739;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BaseCalculoDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell168.Text = "xrTableCell168";
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell168.Weight = 0.49658314350797267;
            // 
            // xrTableRow92
            // 
            this.xrTableRow92.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell170});
            this.xrTableRow92.Dpi = 254F;
            this.xrTableRow92.Name = "xrTableRow92";
            this.xrTableRow92.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow92.Weight = 0.042611060743427021;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensar2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell169.Text = "xrTableCell169";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell169.Weight = 0.50341685649202739;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PrejuizoCompensarDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell170.Text = "xrTableCell170";
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell170.Weight = 0.49658314350797267;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell172});
            this.xrTableRow93.Dpi = 254F;
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow93.Weight = 0.042611060743427021;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell171.Text = "20%";
            this.xrTableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell171.Weight = 0.50341685649202739;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell172.Text = "20%";
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell172.Weight = 0.49658314350797267;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174});
            this.xrTableRow94.Dpi = 254F;
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow94.Weight = 0.042611060743427021;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculado2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell173.Text = "xrTableCell173";
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell173.Weight = 0.50341685649202739;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ImpostoCalculadoDT2", "{0:#,#.00;(#,#.00);0.00}")});
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableCell174.Text = "xrTableCell174";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell174.Weight = 0.49658314350797267;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(101F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2497F, 56F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "IDENTIFICAÇÃO DO CONTRIBUINTE";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 1;
            this.xrTableCell7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloDetailBeforePrint);
            // 
            // ganhoFIICollection1
            // 
            this.ganhoFIICollection1.AllowDelete = true;
            this.ganhoFIICollection1.AllowEdit = true;
            this.ganhoFIICollection1.AllowNew = true;
            this.ganhoFIICollection1.EnableHierarchicalBinding = true;
            this.ganhoFIICollection1.Filter = "";
            this.ganhoFIICollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.ganhoFIICollection1.Sort = "";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 169F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable14
            // 
            this.xrTable14.Dpi = 254F;
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow126});
            this.xrTable14.SizeF = new System.Drawing.SizeF(2604F, 169F);
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow126
            // 
            this.xrTableRow126.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell2});
            this.xrTableRow126.Dpi = 254F;
            this.xrTableRow126.Name = "xrTableRow126";
            this.xrTableRow126.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow126.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13});
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 0.65322580645161288;
            // 
            // xrTable13
            // 
            this.xrTable13.Dpi = 254F;
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(101F, 42F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow125});
            this.xrTable13.SizeF = new System.Drawing.SizeF(1058F, 106F);
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow125
            // 
            this.xrTableRow125.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow125.Dpi = 254F;
            this.xrTableRow125.Name = "xrTableRow125";
            this.xrTableRow125.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow125.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "RESUMO DE APURAÇÃO DE GANHOS EM FUNDOS IMOBILIÁRIOS";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 1;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloBeforePrint);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 0.34677419354838712;
            // 
            // xrTable12
            // 
            this.xrTable12.Dpi = 254F;
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(169F, 0F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow123,
            this.xrTableRow124});
            this.xrTable12.SizeF = new System.Drawing.SizeF(613F, 169F);
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow123
            // 
            this.xrTableRow123.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow123.Dpi = 254F;
            this.xrTableRow123.Name = "xrTableRow123";
            this.xrTableRow123.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow123.Weight = 0.50295857988165682;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "IMPOSTO DE RENDA";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 1;
            // 
            // xrTableRow124
            // 
            this.xrTableRow124.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow124.Dpi = 254F;
            this.xrTableRow124.Name = "xrTableRow124";
            this.xrTableRow124.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow124.Weight = 0.49704142011834318;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "$AnoCalendario";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 1;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AnoCalendarioBeforePrint);
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 76F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 10F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 10F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportGanhoFII
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.ganhoFIICollection1;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(25, 20, 10, 10);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region Funções Personalizadas
        private void CNPJBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCNPJ = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            if (cliente.UpToPessoaByIdPessoa != null)
            {
                string cnpj = cliente.UpToPessoaByIdPessoa.Cpfcnpj;
                xrTableCellCNPJ.Text = cnpj;
            }
        }

        private void NomeClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellNomeCliente = sender as XRTableCell;
            //
            if (!String.IsNullOrEmpty(this.nomeCliente)) {
                string nome = this.nomeCliente;
                //
                int tamanho = nome.Length;
                if (tamanho > 1) {
                    string primeiraLetra = nome.Substring(0, 1).ToUpper();
                    nome = primeiraLetra + nome.Substring(1, tamanho - 1);
                    xrTableCellNomeCliente.Text = nome;
                }
            }
        }

        private void AnoCalendarioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellAnoCalendario = sender as XRTableCell;
            //            
            // Se mes for março, fevereiro ou janeiro, ano são dois
            int mesAux = this.mes - 3;
            string anoCalendario = "";
            if (mesAux <= 0) {
                anoCalendario = "ANO " + Resources.ReportGanhoRendaVariavel._Calendario + " DE " + (this.ano - 1) + "/" + this.ano;
            }
            else {
                anoCalendario = "ANO " + Resources.ReportGanhoRendaVariavel._Calendario + " DE " + this.ano;
            }

            xrTableCellAnoCalendario.Text = anoCalendario;
        }

        private void Mes1BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[0]);
            xrTableCellMes.Text = mes + "/" + this.anoExecucao[0];
        }

        private void Mes2BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes2 = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[1]);
            xrTableCellMes2.Text = mes + "/" + this.anoExecucao[1]; ;
        }

        private void Mes3BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes3 = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[2]);
            xrTableCellMes3.Text = mes + "/" + this.anoExecucao[2];
        }

        private void Mes4BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellMes4 = sender as XRTableCell;
            //            
            string mes = Utilitario.RetornaMesString(this.mesExecucao[3]);
            xrTableCellMes4.Text = mes + "/" + this.anoExecucao[3];
        }
        #endregion

        private void TituloBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell xrTableCellTitulo = sender as XRTableCell;
            //xrTableCellTitulo.Text = Resources.ReportGanhoRendaVariavel._TituloRelatorio;
        }

        private void TituloDetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell xrTableCellTituloDetail = sender as XRTableCell;
            //xrTableCellTituloDetail.Text = Resources.ReportGanhoRendaVariavel._TituloDetail;
        }

        private void SubTituloDetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell xrTableCellSubTituloDetail = sender as XRTableCell;
            //xrTableCellSubTituloDetail.Text = Resources.ReportGanhoRendaVariavel._SubTituloDetail;
        }

        private void TableDetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable tableDetail = sender as XRTable;
            //
            //((XRTableRow)tableDetail.Rows[1]).Cells[0].Text = Resources.ReportGanhoFII._Discriminacao;
            //((XRTableRow)tableDetail.Rows[3]).Cells[0].Text = Resources.ReportGanhoFII._ResultadoFII;
            //((XRTableRow)tableDetail.Rows[17]).Cells[0].Text = Resources.ReportGanhoFII._ResultadoLiquidoMes;
            //((XRTableRow)tableDetail.Rows[18]).Cells[0].Text = Resources.ReportGanhoFII._ResultadoNegativoAteMesAnterior;
            //((XRTableRow)tableDetail.Rows[19]).Cells[0].Text = Resources.ReportGanhoFII._BaseCaculoImposto;
            //((XRTableRow)tableDetail.Rows[20]).Cells[0].Text = Resources.ReportGanhoFII._PrejuizoACompensar;
            //((XRTableRow)tableDetail.Rows[21]).Cells[0].Text = Resources.ReportGanhoFII._AliquotaImposto;
        }
    }
}