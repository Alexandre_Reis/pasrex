﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using Financial.Util;
using Financial.Investidor;
using Financial.Common;
using System.Collections.Generic;
using Financial.BMF.Enums;
using Financial.BMF;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.InvestidorCotista.Enums;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportInformeContabilPJ
    /// </summary>
    public class ReportInformeContabilPJ : XtraReport {
        
        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private int idCotista;

        public int IdCotista {
            get { return idCotista; }
            set { idCotista = value; }
        }

        private int mes;

        public int Mes {
            get { return mes; }
            set { mes = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        private decimal totalRendimentoBruto;
        private decimal totalIR;
        private decimal totalIOF;

        private DateTime dataInicio;
        private DateTime dataFim;

        private int numeroLinhasDataTable;

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;        
        private ReportSemDados reportSemDados1;
        private PosicaoCotistaAberturaCollection posicaoCotistaCollection1;
        private SubReportRodape subReportRodape1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ReportInformeContabilPJ(int idCarteira, int idCotista, int mes, int ano) {
            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.mes = mes;
            this.ano = ano;
            //
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);
            
            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1860);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();            
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;                
            }
        }
        
        private void PersonalInitialize() 
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, this.idCarteira);
            DateTime dataDia = cliente.DataDia.Value;

            DateTime dataInicio = new DateTime(this.ano, this.mes, 01);
            if (dataInicio.CompareTo(dataDia) > 0)
            {
                dataInicio = new DateTime(dataDia.Year, dataDia.Month, 01);
            }
            this.dataInicio = dataInicio;
            DateTime dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataInicio, 0);
            this.dataFim = dataFim;      

            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Count;
        }

        private DataView FillDados() 
        {
            #region SQL

            // Consulta em PosicaoCotistaAbertura
            PosicaoCotistaAberturaQuery p = new PosicaoCotistaAberturaQuery("P");

            // Query com join
            p.Select(p.IdPosicao, p.DataConversao, p.Quantidade);
            p.Where(p.IdCarteira.Equal(this.idCarteira), p.IdCotista.Equal(this.idCotista),
                    p.DataHistorico.Equal(this.dataFim));
            this.posicaoCotistaCollection1.Load(p);

            #endregion

            return this.posicaoCotistaCollection1.LowLevelBind();
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Legais/ImpostoRenda/ReportInformeContabilPJ.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportInformeContabilPJ.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.posicaoCotistaCollection1 = new Financial.InvestidorCotista.PosicaoCotistaAberturaCollection();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 53F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Data", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1859F, 40F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataConversao", "{0:d}")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.23130715438407748;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "RendaBruta";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.76869284561592255;
            this.xrTableCell15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RendaBrutaBeforePrint);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "ValorIR";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.76869284561592255;
            this.xrTableCell16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorIRBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "ValorIOF";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell17.Weight = 0.76869284561592255;
            this.xrTableCell17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorIOFBeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(106F, 183F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 106F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(217F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(142F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "Período Ref.: ";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "Mes";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.66614173228346463;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MesBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "Data Emissão: ";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(750F, 11F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1200F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "Informe Contábil PJ";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.96076861489191356;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 307F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1859F, 48F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "Aplicação";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.23023130715438409;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Renda Bruta";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell25.Weight = 0.769768692845616;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Valor IR";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.769768692845616;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Valor IOF";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.769768692845616;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 64F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(360F, 64F);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(21F, 307F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(64F, 42F);
            this.xrSubreport3.Visible = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable7,
            this.xrPanel1,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 355F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 11F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(616F, 148F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1863F, 85F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(92F, 42F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // posicaoCotistaCollection1
            // 
            this.posicaoCotistaCollection1.AllowDelete = true;
            this.posicaoCotistaCollection1.AllowEdit = true;
            this.posicaoCotistaCollection1.AllowNew = true;
            this.posicaoCotistaCollection1.EnableHierarchicalBinding = true;
            this.posicaoCotistaCollection1.Filter = "";
            this.posicaoCotistaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoCotistaCollection1.Sort = "";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportInformeContabilPJ
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.posicaoCotistaCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportInformeContabilPJ.ResourceManager;
        }

        #region Funções Internas do Relatorio
        //
        private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell dataInicioXRTableCell = sender as XRTableCell;
            //dataInicioXRTableCell.Text = this.dataInicio.ToString("d");
        }

        private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell dataFimXRTableCell = sender as XRTableCell;
            //dataFimXRTableCell.Text = this.dataFim.ToString("d");
        }
        #endregion

        private void RendaBrutaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) 
        {
            XRTableCell rendaBrutaXRTableCell = sender as XRTableCell;
            rendaBrutaXRTableCell.Text = "";

            decimal rendimentoBruto = 0;
            if (this.numeroLinhasDataTable != 0) {
                int idPosicao = (int)this.GetCurrentColumnValue(PosicaoCotistaAberturaMetadata.ColumnNames.IdPosicao);
                DateTime dataConversao = (DateTime)this.GetCurrentColumnValue(PosicaoCotistaAberturaMetadata.ColumnNames.DataConversao);
                decimal quantidade = (decimal)this.GetCurrentColumnValue(PosicaoCotistaAberturaMetadata.ColumnNames.Quantidade); //TODO PEGAR DE ABERTURA

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCota(this.idCarteira, this.dataFim);
                decimal cotaDia = historicoCota.CotaFechamento.Value;

                DateTime dataMesAnterior = Calendario.RetornaUltimoDiaUtilMes(this.dataInicio, -1);
                historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCota(this.idCarteira, dataMesAnterior);
                decimal cotaMesAnterior = historicoCota.CotaFechamento.Value;

                decimal rendimentoPosicao = Math.Round(quantidade * (cotaDia - cotaMesAnterior), 2);

                DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");

                detalheResgateCotistaQuery.Select(detalheResgateCotistaQuery.IdOperacao, detalheResgateCotistaQuery.Quantidade);
                detalheResgateCotistaQuery.InnerJoin(posicaoCotistaQuery).On(posicaoCotistaQuery.IdPosicao == detalheResgateCotistaQuery.IdPosicaoResgatada);
                detalheResgateCotistaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao == detalheResgateCotistaQuery.IdOperacao);
                detalheResgateCotistaQuery.Where(operacaoCotistaQuery.DataConversao.GreaterThanOrEqual(this.dataInicio),
                                                 operacaoCotistaQuery.DataConversao.LessThanOrEqual(this.dataFim),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCarteira),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCotista),
                                                 operacaoCotistaQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                                      (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                      (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                      (byte)TipoOperacaoCotista.ResgateTotal));

                DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
                detalheResgateCotistaCollection.Load(detalheResgateCotistaQuery);

                decimal totalRendimentoResgate = 0;
                foreach (DetalheResgateCotista detalheResgateCotista in detalheResgateCotistaCollection) {
                    int idOperacao = detalheResgateCotista.IdOperacao.Value;
                    decimal quantidadeResgatada = detalheResgateCotista.Quantidade.Value;

                    OperacaoCotista operacaoCotista = new OperacaoCotista();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(operacaoCotista.Query.DataConversao);
                    operacaoCotista.LoadByPrimaryKey(idOperacao);
                    DateTime dataConversaoResgate = operacaoCotista.DataConversao.Value;

                    historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCota(this.idCarteira, dataConversaoResgate);
                    decimal cotaResgate = historicoCota.CotaFechamento.Value;
                    decimal rendimentoResgate = Math.Round(quantidadeResgatada * (cotaResgate - cotaMesAnterior), 2);

                    totalRendimentoResgate += rendimentoResgate;
                }

                rendimentoBruto = rendimentoPosicao + totalRendimentoResgate;

                this.totalRendimentoBruto += rendimentoBruto;
            }

            ReportBase.ConfiguraSinalNegativo(rendaBrutaXRTableCell, rendimentoBruto);
        }

        private void ValorIRBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell valorIRXRTableCell = sender as XRTableCell;
            valorIRXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0)
            {
                DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");

                detalheResgateCotistaQuery.Select(detalheResgateCotistaQuery.ValorIR.Sum());
                detalheResgateCotistaQuery.InnerJoin(posicaoCotistaQuery).On(posicaoCotistaQuery.IdPosicao == detalheResgateCotistaQuery.IdPosicaoResgatada);
                detalheResgateCotistaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao == detalheResgateCotistaQuery.IdOperacao);
                detalheResgateCotistaQuery.Where(operacaoCotistaQuery.DataConversao.GreaterThanOrEqual(this.dataInicio),
                                                 operacaoCotistaQuery.DataConversao.LessThanOrEqual(this.dataFim),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCarteira),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCotista),
                                                 operacaoCotistaQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                                      (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                      (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                      (byte)TipoOperacaoCotista.ResgateTotal));

                DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                detalheResgateCotista.Load(detalheResgateCotistaQuery);

                decimal valorIR = 0;
                if (detalheResgateCotista.ValorIR.HasValue)
                {
                    valorIR = detalheResgateCotista.ValorIR.Value;
                }

                this.totalIR += valorIR;

                ReportBase.ConfiguraSinalNegativo(valorIRXRTableCell, valorIR);
            }
        }

        private void ValorIOFBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell valorIOFXRTableCell = sender as XRTableCell;
            valorIOFXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0)
            {
                DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");

                detalheResgateCotistaQuery.Select(detalheResgateCotistaQuery.ValorIR.Sum());
                detalheResgateCotistaQuery.InnerJoin(posicaoCotistaQuery).On(posicaoCotistaQuery.IdPosicao == detalheResgateCotistaQuery.IdPosicaoResgatada);
                detalheResgateCotistaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao == detalheResgateCotistaQuery.IdOperacao);
                detalheResgateCotistaQuery.Where(operacaoCotistaQuery.DataConversao.GreaterThanOrEqual(this.dataInicio),
                                                 operacaoCotistaQuery.DataConversao.LessThanOrEqual(this.dataFim),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCarteira),
                                                 operacaoCotistaQuery.IdCarteira.Equal(this.idCotista),
                                                 operacaoCotistaQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                                      (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                      (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                      (byte)TipoOperacaoCotista.ResgateTotal));

                DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                detalheResgateCotista.Load(detalheResgateCotistaQuery);

                decimal valorIOF = 0;
                if (detalheResgateCotista.ValorIR.HasValue)
                {
                    valorIOF = detalheResgateCotista.ValorIR.Value;
                }

                this.totalIOF += valorIOF;

                ReportBase.ConfiguraSinalNegativo(valorIOFXRTableCell, valorIOF);
            }
        }

        private void MesBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell mesXRTableCell = sender as XRTableCell;
            mesXRTableCell.Text = Utilitario.RetornaMesString(mes) + "/ " + ano.ToString();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //DetailBand detail = sender as DetailBand;
            //if (xrTableCell15.Text == "0,00" && xrTableCell16.Text == "0,00" && xrTableCell17.Text == "0,00")
            //{
            //    detail.Visible = false;
            //}
        }
        
    }
}