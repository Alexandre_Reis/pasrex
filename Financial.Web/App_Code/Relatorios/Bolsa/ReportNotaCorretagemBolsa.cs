﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Financial.CRM;
using System.Data;
using Financial.Relatorio;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Custom1;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Tributo.Enums;
using Financial.Tributo;
using Financial.Investidor;
using Financial.WebConfigConfiguration;
using System.IO;
using DevExpress.XtraPrinting;

/// <summary>
/// Summary description for ReportNotaCorretagemBolsa
/// </summary>
public class ReportNotaCorretagemBolsa : DevExpress.XtraReports.UI.XtraReport {
    private XRTableCell tableCell176;
    private XRTableRow tableRow7;
    private XRTableCell tableCell19;
    private XRTableCell tableCell20;
    private XRTableCell tableCell21;
    private XRTableCell tableCell27;
    private XRTableCell tableCell50;
    private XRLabel label1;
    private XRTableCell tableCell107;
    private XRTableCell tableCell119;
    private XRTableCell tableCell131;
    private XRTableCell tableCell66;
    private XRTableCell tableCell125;
    private XRPanel xrPanel9;
    private XRTable table2;
    private XRTableRow tableRow2;
    private XRTableCell tableCell4;
    private XRTableRow tableRow3;
    private XRTableCell tableCell7;
    private XRTableRow tableRow4;
    private XRTableCell tableCell10;
    private XRTableCell tableCell11;
    private XRTableCell tableCell12;
    private XRTableCell tableCell24;
    private XRTableRow tableRow5;
    private XRTableCell tableCell13;
    private XRTableCell tableCell14;
    private XRTableRow tableRow6;
    private XRTableCell tableCell16;
    private XRTableCell tableCell17;
    private XRTableCell tableCell83;
    private XRTableCell tableCell18;
    private XRTableCell tableCell46;
    private XRTableCell tableCell168;
    private XRTableCell tableCell160;
    private XRTable table5;
    private XRTableRow tableRow14;
    private XRTableCell tableCell23;
    private XRTableCell tableCell25;
    private XRTableRow tableRow16;
    private XRTableCell tableCell31;
    private XRTableCell tableCell35;
    private XRTableCell tableCell73;
    private XRLabel label4;
    private XRTable table10;
    private XRTableRow tableRow23;
    private XRTableCell tableCell59;
    private XRTableRow tableRow24;
    private XRTableCell tableCell60;
    private XRTableCell tableCell112;
    private XRTableCell tableCell104;
    private XRTableCell tableCell77;
    private XRTableRow tableRow53;
    private XRTableCell tableCell116;
    private XRTableCell tableCell118;
    private XRTableCell tableCell123;
    private XRTableCell tableCell103;
    private XRTableRow tableRow64;
    private XRTableCell tableCell138;
    private XRTableCell tableCell139;
    private XRTableCell tableCell147;
    private XRTableCell tableCell115;
    private XRTableCell tableCell76;
    private XRTableCell tableCell177;
    private XRTableRow tableRow29;
    private XRTableCell tableCell65;
    private XRTableRow tableRow66;
    private XRTableCell tableCell154;
    private XRTableCell tableCell42;
    private XRTableCell tableCell110;
    private XRTableRow tableRow1;
    private XRTableCell tableCell74;
    private XRTableCell tableCell1;
    private XRTableCell tableCell72;
    private XRTableCell tableCell71;
    private XRTableCell tableCell2;
    private XRTableCell tableCell34;
    private XRTableCell tableCell85;
    private XRTableCell tableCell3;
    private XRTableCell tableCell75;
    private XRTable table16;
    private XRTableRow tableRow46;
    private XRTableCell tableCell79;
    private XRTableRow tableRow69;
    private XRTableCell tableCell151;
    private XRTableCell tableCell152;
    private XRTableCell tableCell153;
    private XRTableCell tableCell109;
    private XRTableRow tableRow20;
    private XRTableCell tableCell159;
    private XRTableRow tableRow31;
    private XRTableCell tableCell182;
    private XRTableRow tableRow71;
    private XRTableCell tableCell161;
    private XRTableCell tableCell162;
    private XRTableCell tableCell163;
    private XRTableCell tableCell174;
    private XRTableRow tableRow15;
    private XRTableCell tableCell36;
    private XRTableCell tableCell41;
    private XRTableCell tableCell101;
    private XRLine line1;
    private XRTable table12;
    private XRTableRow tableRow27;
    private XRTableCell tableCell63;
    private XRTableRow tableRow28;
    private XRTableCell tableCell64;
    private XRTableCell tableCell70;
    private XRTableCell tableCell68;
    private XRLabel label20;
    private XRTableRow tableRow8;
    private XRTableCell tableCell5;
    private XRTableCell tableCell6;
    private XRTableCell tableCell8;
    private XRTableCell tableCell82;
    private XRPanel panel2;
    private XRTable table14;
    private XRTableRow tableRow32;
    private XRTableCell tableCell80;
    private XRTableRow tableRow33;
    private XRTableCell tableCell84;
    private XRTableCell tableCell67;
    private XRTableRow tableRow34;
    private XRTableCell tableCell86;
    private XRTableCell tableCell87;
    private XRTableCell tableCell69;
    private XRTableCell tableCell144;
    private XRTableCell tableCell44;
    private XRTableCell tableCell146;
    private XRLabel label9;
    private XRTableCell tableCell56;
    private XRLabel label5;
    private XRTableCell tableCell142;
    private XRTableCell tableCell52;
    private XRTableRow tableRow19;
    private XRTableCell tableCell43;
    private XRTableCell tableCell45;
    private XRTableCell tableCell102;
    private XRTableCell tableCell126;
    private XRTableCell tableCell155;
    private XRTableCell tableCell57;
    private XRTableRow tableRow44;
    private XRTableCell tableCell114;
    private XRTableCell tableCell22;
    private XRTableCell tableCell129;
    private XRTableCell tableCell99;
    private XRTableCell tableCell97;
    private XRTable table13;
    private XRTableRow tableRow30;
    private XRTableCell tableCell38;
    private XRTableCell tableCell128;
    private XRTableRow tableRow48;
    private XRTableCell tableCell89;
    private XRTableCell tableCell173;
    private XRTableCell tableCell130;
    private XRTableCell tableCell156;
    private XRTableCell tableCell91;
    private XRTable table9;
    private XRTableRow tableRow22;
    private XRTableCell tableCell47;
    private XRTableCell tableCell58;
    private XRTableCell tableCell51;
    private XRTableCell tableCell55;
    private XRTableCell tableCell48;
    private XRTableCell tableCell53;
    private XRTableCell tableCell54;
    private XRTableRow tableRow39;
    private XRTableCell tableCell49;
    private XRTableRow tableRow26;
    private XRTableCell tableCell62;
    private XRTableCell tableCell172;
    private XRTableCell tableCell37;
    private XRTableCell tableCell166;
    private XRTableCell tableCell121;
    private XRTableCell tableCell61;
    private XRTable table22;
    private XRTableRow tableRow54;
    private XRTableCell tableCell95;
    private XRTableRow tableRow56;
    private XRTableCell tableCell124;
    private XRTableRow tableRow57;
    private XRTableCell tableCell127;
    private XRTableCell tableCell143;
    private XRTableRow tableRow58;
    private XRTableCell tableCell149;
    private XRTableCell tableCell150;
    private BottomMarginBand BottomMargin;
    private XRTableCell tableCell170;
    private XRTableRow tableRow75;
    private XRTableCell tableCell175;
    private XRTableRow tableRow17;
    private XRLabel label2;
    private XRTable table25;
    private XRTableRow tableRow76;
    private XRTableCell tableCell33;
    private XRTableCell tableCell136;
    private XRTable table3;
    private XRTableRow tableRow9;
    private XRTableCell tableCell9;
    private XRTableCell tableCell15;
    private XRPageInfo pageInfo1;
    private XRTableCell tableCell171;
    private XRTableCell tableCell28;
    private XRTableRow tableRow45;
    private XRTableCell tableCell117;
    private XRTableCell tableCell111;
    private XRLabel label16;
    private PageHeaderBand PageHeader;
    private XRLabel label13;
    private XRLabel label12;
    private XRLabel label11;
    private XRLabel label10;
    private XRLabel label8;
    private XRLabel label7;
    private XRLabel label6;
    private XRLabel label3;
    private XRTable table11;
    private XRTableRow tableRow25;
    private XRTable table8;
    private XRTableRow tableRow21;
    private XRTable table7;
    private XRTableRow tableRow18;
    private XRTableCell tableCell39;
    private XRTableCell tableCell40;
    private XRTable table6;
    private XRTable table4;
    private XRTableRow tableRow10;
    private XRTableRow tableRow13;
    private XRTableRow tableRow12;
    private XRTableCell tableCell30;
    private XRTableCell tableCell32;
    private XRTableRow tableRow11;
    private XRTableCell tableCell26;
    private XRTableCell tableCell29;
    private XRTable table1;
    private XRTable table17;
    private XRTableRow tableRow47;
    private XRTableCell tableCell81;
    private XRTable table15;
    private XRTableRow tableRow35;
    private XRTableCell tableCell78;
    private XRTableRow tableRow36;
    private XRTableCell tableCell88;
    private XRTableCell tableCell90;
    private XRTableRow tableRow37;
    private XRTableCell tableCell93;
    private XRTableRow tableRow38;
    private XRTableCell tableCell94;
    private XRTableCell tableCell96;
    private XRTableRow tableRow40;
    private XRTableCell tableCell100;
    private XRTableRow tableRow41;
    private XRTableCell tableCell105;
    private XRTableRow tableRow42;
    private XRTableCell tableCell106;
    private XRTableCell tableCell108;
    private XRTableRow tableRow43;
    private XRTableRow tableRow50;
    private XRTableCell tableCell98;
    private XRTableCell tableCell120;
    private XRPanel panel4;
    private XRTable table23;
    private XRTable table20;
    private XRTableRow tableRow55;
    private XRTableCell tableCell137;
    private XRLabel label23;
    private XRLabel label19;
    private XRTableCell tableCell148;
    private XRTableRow tableRow73;
    private XRTableCell tableCell167;
    private XRTableCell tableCell169;
    private XRTableCell tableCell141;
    private XRTableRow tableRow72;
    private XRTableCell tableCell164;
    private XRTableCell tableCell165;
    private XRTableRow tableRow67;
    private XRTableCell tableCell157;
    private XRTableRow tableRow61;
    private XRTableCell tableCell132;
    private XRTableCell tableCell133;
    private XRTable table18;
    private XRTableRow tableRow49;
    private XRTableCell tableCell92;
    private XRTableRow tableRow51;
    private XRTableRow tableRow52;
    private XRTableCell tableCell113;
    private XRTableCell tableCell122;
    private PageFooterBand PageFooter;
    private XRPanel panel1;
    private XRPanel panel3;
    private XRTable table21;
    private XRTableRow tableRow68;
    private XRTable table19;
    private XRTableRow tableRow59;
    private XRTableRow tableRow60;
    private XRTableRow tableRow62;
    private XRTableCell tableCell134;
    private XRTableCell tableCell135;
    private XRTableCell tableCell145;
    private XRTableRow tableRow63;
    private XRTableRow tableRow65;
    private XRTableCell tableCell140;
    private XRTable table24;
    private XRTableRow tableRow70;
    private XRTableCell tableCell158;
    private XRTableRow tableRow74;
    private XRLabel label14;
    private XRLabel label15;
    private XRLabel label17;
    private XRLabel label18;
    private XRLabel label21;
    private XRLabel label22;
    private XRLabel label24;
    private TopMarginBand TopMargin;
    private DetailBand Detail;
        
    private XRControlStyle xrControlStyle1;
    private XRControlStyle xrControlStyle2;

    
    private int numeroLinhasDataTable;

	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;   
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRPictureBox xrPictureBox1;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell6;
    
    /* Dados da Nota */
    //private Financial.RendaFixa.Custom.NotaCorretagemRendaFixa dadosNota;
    private NotaCorretagemBolsa dadosNota;

	//public ReportNotaCorretagemBolsa(Financial.RendaFixa.Custom.NotaCorretagemRendaFixa a) {
    public ReportNotaCorretagemBolsa(NotaCorretagemBolsa a) {
		InitializeComponent();
		//
        #region Teste Dados
        //a.GetHeader.numeroNota = 10989;
        //a.GetHeader.dataPregao = new DateTime(2012,12,17);
        //a.GetHeader.nomeAgente = "CLEAR CTVM LTDA";
        //a.GetHeader.enderecoCompletoAgente = "RUA JOAQUIM LFLORIAN, 413 - BIBI - 04534011 São Paulo -SP";
        //a.GetHeader.telefoneCompletoAgente = "3957-9000";
        //a.GetHeader.siteAgente = "www.clear.com.br";
        //a.GetHeader.emailAgente = "atendimento@clear.com.br";
        //a.GetHeader.cnpjAgente = "15.107.963/0001-66";
        //a.GetHeader.cartaPatente = "cartaPatente";
        //a.GetHeader.codigoCliente = "0011024";
        //a.GetHeader.nomeCliente = "Clube de Investimento Axon";
        //a.GetHeader.enderecoCliente = "AV Brig Faria Lima 1663 - 5 Andar";
        //a.GetHeader.cidadeCliente = "São Paulo";
        //a.GetHeader.bairroCliente = "Jardim Paulistano";
        //a.GetHeader.cepCliente = "01452-001";
        //a.GetHeader.ufCliente = "SP";
        //a.GetHeader.telefoneCliente = "(011) 98182-5484";
        //a.GetHeader.cpfCNPJCliente = "13.628.223/0001-40";
        //a.GetHeader.codigoBovespaAgente = "308-5";
        //a.GetHeader.codigoAssessor = "0";
        ////        
        //a.GetFooter.vendasVista = 10;
        //a.GetFooter.comprasVista = 20;
        //a.GetFooter.valorLiquido = 30;
        //a.GetFooter.corretagem = 40;
        //a.GetFooter.iss = 50;
        //a.GetFooter.ir = 60;

        //
        //List<Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail> dadosDetail = a.GetListDetail;
        List<NotaCorretagemBolsa.Detail> dadosDetail = a.GetListDetail;

        //dadosDetail = new List<Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail>(
        //    new Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail[] { 
        //        new Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail("1","1","1","1","1", 1.21M , 1.43M , 1.54M),
        //        new Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail("2","2","2","2","2", 2.00M , 2M    , 2.1M),
        //        new Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail("3","3","3","3","3", 3.7M  , 3M    , 3.56M),
        //        new Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail("4","4","4","4","4", 4.8M  , 4M    ,4M)
        //    }
        // );
        #endregion
        
        dadosNota = a;
        
        /* DataBinding do Detail */
        this.DataSource = dadosDetail;
        //this.DataSource = new BindingList<Financial.RendaFixa.Custom.NotaCorretagemRendaFixa.Detail>(dadosDetail);

        this.numeroLinhasDataTable = dadosDetail.Count;
	}

	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "ReportNotaCorretagemBolsa.resx";
        this.tableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label1 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrPanel9 = new DevExpress.XtraReports.UI.XRPanel();
        this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
        this.table2 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table5 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label4 = new DevExpress.XtraReports.UI.XRLabel();
        this.table10 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table16 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
        this.line1 = new DevExpress.XtraReports.UI.XRLine();
        this.table12 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label20 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
        this.panel2 = new DevExpress.XtraReports.UI.XRPanel();
        this.table14 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label9 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label5 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table13 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table9 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table22 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.tableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
        this.label2 = new DevExpress.XtraReports.UI.XRLabel();
        this.table25 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table3 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.pageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.tableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label16 = new DevExpress.XtraReports.UI.XRLabel();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.label13 = new DevExpress.XtraReports.UI.XRLabel();
        this.label12 = new DevExpress.XtraReports.UI.XRLabel();
        this.label11 = new DevExpress.XtraReports.UI.XRLabel();
        this.label10 = new DevExpress.XtraReports.UI.XRLabel();
        this.label8 = new DevExpress.XtraReports.UI.XRLabel();
        this.label7 = new DevExpress.XtraReports.UI.XRLabel();
        this.label6 = new DevExpress.XtraReports.UI.XRLabel();
        this.label3 = new DevExpress.XtraReports.UI.XRLabel();
        this.table11 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
        this.table8 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
        this.table7 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table6 = new DevExpress.XtraReports.UI.XRTable();
        this.table4 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table1 = new DevExpress.XtraReports.UI.XRTable();
        this.table17 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table15 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
        this.panel4 = new DevExpress.XtraReports.UI.XRPanel();
        this.table23 = new DevExpress.XtraReports.UI.XRTable();
        this.table20 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
        this.label23 = new DevExpress.XtraReports.UI.XRLabel();
        this.label19 = new DevExpress.XtraReports.UI.XRLabel();
        this.tableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table18 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
        this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
        this.panel1 = new DevExpress.XtraReports.UI.XRPanel();
        this.panel3 = new DevExpress.XtraReports.UI.XRPanel();
        this.table21 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
        this.table19 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
        this.table24 = new DevExpress.XtraReports.UI.XRTable();
        this.tableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
        this.tableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
        this.tableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
        this.label14 = new DevExpress.XtraReports.UI.XRLabel();
        this.label15 = new DevExpress.XtraReports.UI.XRLabel();
        this.label17 = new DevExpress.XtraReports.UI.XRLabel();
        this.label18 = new DevExpress.XtraReports.UI.XRLabel();
        this.label21 = new DevExpress.XtraReports.UI.XRLabel();
        this.label22 = new DevExpress.XtraReports.UI.XRLabel();
        this.label24 = new DevExpress.XtraReports.UI.XRLabel();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
        this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
        ((System.ComponentModel.ISupportInitialize)(this.table2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table10)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table16)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table12)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table14)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table13)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table9)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table22)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table25)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table11)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table8)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table7)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table17)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table15)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table23)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table20)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table18)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table21)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table19)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.table24)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // tableCell176
        // 
        this.tableCell176.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell176.Dpi = 254F;
        this.tableCell176.Name = "tableCell176";
        this.tableCell176.StylePriority.UseBorders = false;
        this.tableCell176.Weight = 0.8573022885606737;
        // 
        // tableRow7
        // 
        this.tableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell19,
            this.tableCell20,
            this.tableCell21,
            this.tableCell27});
        this.tableRow7.Dpi = 254F;
        this.tableRow7.Name = "tableRow7";
        this.tableRow7.Weight = 1;
        // 
        // tableCell19
        // 
        this.tableCell19.Dpi = 254F;
        this.tableCell19.Name = "tableCell19";
        this.tableCell19.Text = "Ouvidoria: Tel.";
        this.tableCell19.Weight = 0.43805350772870855;
        // 
        // tableCell20
        // 
        this.tableCell20.CanGrow = false;
        this.tableCell20.Dpi = 254F;
        this.tableCell20.Name = "tableCell20";
        this.tableCell20.Text = "Ouvidoria";
        this.tableCell20.Weight = 0.73281977900727069;
        this.tableCell20.WordWrap = false;
        // 
        // tableCell21
        // 
        this.tableCell21.Dpi = 254F;
        this.tableCell21.Name = "tableCell21";
        this.tableCell21.Text = "e-mail ouvidoria:";
        this.tableCell21.Weight = 0.48099238672324007;
        // 
        // tableCell27
        // 
        this.tableCell27.CanGrow = false;
        this.tableCell27.Dpi = 254F;
        this.tableCell27.Name = "tableCell27";
        this.tableCell27.Text = "emailOuvidoria";
        this.tableCell27.Weight = 1.3481343265407808;
        this.tableCell27.WordWrap = false;
        // 
        // tableCell50
        // 
        this.tableCell50.Dpi = 254F;
        this.tableCell50.Name = "tableCell50";
        this.tableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell50.StylePriority.UsePadding = false;
        this.tableCell50.StylePriority.UseTextAlignment = false;
        this.tableCell50.Text = "Valor Operação/Ajuste";
        this.tableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.tableCell50.Weight = 0.35768988493435322;
        // 
        // label1
        // 
        this.label1.Dpi = 254F;
        this.label1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
        this.label1.LocationFloat = new DevExpress.Utils.PointFloat(593.5208F, 0F);
        this.label1.Name = "label1";
        this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label1.SizeF = new System.Drawing.SizeF(582.5624F, 58.42001F);
        this.label1.StylePriority.UseFont = false;
        this.label1.StylePriority.UseTextAlignment = false;
        this.label1.Text = "NOTA DE CORRETAGEM";
        this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // tableCell107
        // 
        this.tableCell107.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell107.Dpi = 254F;
        this.tableCell107.Name = "tableCell107";
        this.tableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell107.StylePriority.UseBorders = false;
        this.tableCell107.StylePriority.UsePadding = false;
        this.tableCell107.StylePriority.UseTextAlignment = false;
        this.tableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell107.Weight = 0.7750836329449895;
        this.tableCell107.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell119
        // 
        this.tableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell119.Dpi = 254F;
        this.tableCell119.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell119.Name = "tableCell119";
        this.tableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell119.StylePriority.UseBorders = false;
        this.tableCell119.StylePriority.UseFont = false;
        this.tableCell119.StylePriority.UsePadding = false;
        this.tableCell119.StylePriority.UseTextAlignment = false;
        this.tableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell119.Weight = 0.773360948755962;
        this.tableCell119.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell131
        // 
        this.tableCell131.CanGrow = false;
        this.tableCell131.Dpi = 254F;
        this.tableCell131.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell131.Name = "tableCell131";
        this.tableCell131.StylePriority.UseFont = false;
        this.tableCell131.StylePriority.UseTextAlignment = false;
        this.tableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell131.Weight = 0.14792870912585576;
        this.tableCell131.WordWrap = false;
        this.tableCell131.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell66
        // 
        this.tableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell66.CanGrow = false;
        this.tableCell66.Dpi = 254F;
        this.tableCell66.Name = "tableCell66";
        this.tableCell66.StylePriority.UseBorders = false;
        this.tableCell66.Weight = 3.0197368421052628;
        this.tableCell66.WordWrap = false;
        // 
        // tableCell125
        // 
        this.tableCell125.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell125.Dpi = 254F;
        this.tableCell125.Name = "tableCell125";
        this.tableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell125.StylePriority.UseBorders = false;
        this.tableCell125.StylePriority.UsePadding = false;
        this.tableCell125.StylePriority.UseTextAlignment = false;
        this.tableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell125.Weight = 0.77508365801492063;
        this.tableCell125.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // xrPanel9
        // 
        this.xrPanel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrPanel9.BorderWidth = 1;
        this.xrPanel9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.table2});
        this.xrPanel9.Dpi = 254F;
        this.xrPanel9.LocationFloat = new DevExpress.Utils.PointFloat(4.037221E-05F, 100F);
        this.xrPanel9.Name = "xrPanel9";
        this.xrPanel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.xrPanel9.SizeF = new System.Drawing.SizeF(1915F, 230F);
        // 
        // xrPictureBox1
        // 
        this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrPictureBox1.Dpi = 254F;
        this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(11.58329F, 24.99996F);
        this.xrPictureBox1.Name = "xrPictureBox1";
        this.xrPictureBox1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.xrPictureBox1.SizeF = new System.Drawing.SizeF(564.8959F, 180.0002F);
        this.xrPictureBox1.StylePriority.UseBorders = false;
        this.xrPictureBox1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.LogotipoBeforePrint);
        // 
        // table2
        // 
        this.table2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table2.Dpi = 254F;
        this.table2.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.table2.LocationFloat = new DevExpress.Utils.PointFloat(596F, 3.000015F);
        this.table2.Name = "table2";
        this.table2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow2,
            this.tableRow3,
            this.tableRow4,
            this.tableRow5,
            this.tableRow6,
            this.tableRow7});
        this.table2.SizeF = new System.Drawing.SizeF(1314.833F, 216.205F);
        this.table2.StylePriority.UseBorders = false;
        this.table2.StylePriority.UseFont = false;
        this.table2.StylePriority.UsePadding = false;
        this.table2.StylePriority.UseTextAlignment = false;
        this.table2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.table2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho2_BeforePrint);
        // 
        // tableRow2
        // 
        this.tableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell4});
        this.tableRow2.Dpi = 254F;
        this.tableRow2.Name = "tableRow2";
        this.tableRow2.Weight = 1;
        // 
        // tableCell4
        // 
        this.tableCell4.CanGrow = false;
        this.tableCell4.Dpi = 254F;
        this.tableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
        this.tableCell4.Name = "tableCell4";
        this.tableCell4.StylePriority.UseFont = false;
        this.tableCell4.StylePriority.UseTextAlignment = false;
        this.tableCell4.Text = "Administrador";
        this.tableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell4.Weight = 3;
        this.tableCell4.WordWrap = false;
        // 
        // tableRow3
        // 
        this.tableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell7});
        this.tableRow3.Dpi = 254F;
        this.tableRow3.Name = "tableRow3";
        this.tableRow3.Weight = 1;
        // 
        // tableCell7
        // 
        this.tableCell7.CanGrow = false;
        this.tableCell7.Dpi = 254F;
        this.tableCell7.Name = "tableCell7";
        this.tableCell7.Text = "Endereco";
        this.tableCell7.Weight = 3;
        this.tableCell7.WordWrap = false;
        // 
        // tableRow4
        // 
        this.tableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell10,
            this.tableCell11,
            this.tableCell12,
            this.tableCell24});
        this.tableRow4.Dpi = 254F;
        this.tableRow4.Name = "tableRow4";
        this.tableRow4.Weight = 1;
        // 
        // tableCell10
        // 
        this.tableCell10.Dpi = 254F;
        this.tableCell10.Name = "tableCell10";
        this.tableCell10.Text = "Tel:";
        this.tableCell10.Weight = 0.14804914722845605;
        // 
        // tableCell11
        // 
        this.tableCell11.CanGrow = false;
        this.tableCell11.Dpi = 254F;
        this.tableCell11.Name = "tableCell11";
        this.tableCell11.Text = "Telefone";
        this.tableCell11.Weight = 1.0228241395075233;
        this.tableCell11.WordWrap = false;
        // 
        // tableCell12
        // 
        this.tableCell12.Dpi = 254F;
        this.tableCell12.Name = "tableCell12";
        this.tableCell12.Text = "Fax:";
        this.tableCell12.Weight = 0.16704379425731242;
        // 
        // tableCell24
        // 
        this.tableCell24.CanGrow = false;
        this.tableCell24.Dpi = 254F;
        this.tableCell24.Name = "tableCell24";
        this.tableCell24.Text = "Fax";
        this.tableCell24.Weight = 1.6620829190067088;
        this.tableCell24.WordWrap = false;
        // 
        // tableRow5
        // 
        this.tableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell13,
            this.xrTableCell4,
            this.xrTableCell5,
            this.tableCell14});
        this.tableRow5.Dpi = 254F;
        this.tableRow5.Name = "tableRow5";
        this.tableRow5.Weight = 1;
        // 
        // tableCell13
        // 
        this.tableCell13.Dpi = 254F;
        this.tableCell13.Name = "tableCell13";
        this.tableCell13.Text = "Internet :";
        this.tableCell13.Weight = 0.30066469134140855;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.Dpi = 254F;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Text = "Internet";
        this.xrTableCell4.Weight = 1.216856158171796;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Dpi = 254F;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Text = "e-mail:";
        this.xrTableCell5.Weight = 0.22699801527261665;
        // 
        // tableCell14
        // 
        this.tableCell14.CanGrow = false;
        this.tableCell14.Dpi = 254F;
        this.tableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell14.Name = "tableCell14";
        this.tableCell14.StylePriority.UseFont = false;
        this.tableCell14.Text = "Email";
        this.tableCell14.Weight = 1.2554811352141788;
        this.tableCell14.WordWrap = false;
        // 
        // tableRow6
        // 
        this.tableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell16,
            this.xrTableCell7,
            this.xrTableCell6,
            this.tableCell17});
        this.tableRow6.Dpi = 254F;
        this.tableRow6.Name = "tableRow6";
        this.tableRow6.Weight = 1;
        // 
        // tableCell16
        // 
        this.tableCell16.Dpi = 254F;
        this.tableCell16.Name = "tableCell16";
        this.tableCell16.Text = "C.N.P.J.:";
        this.tableCell16.Weight = 0.32950942240121406;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.Dpi = 254F;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.Text = "CNPJ";
        this.xrTableCell7.Weight = 0.84136401970987817;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.Dpi = 254F;
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.Text = "Carta Patente:";
        this.xrTableCell6.Weight = 0.48099210546369808;
        // 
        // tableCell17
        // 
        this.tableCell17.CanGrow = false;
        this.tableCell17.Dpi = 254F;
        this.tableCell17.Name = "tableCell17";
        this.tableCell17.Text = "CartaPatente";
        this.tableCell17.Weight = 1.34813445242521;
        this.tableCell17.WordWrap = false;
        // 
        // tableCell83
        // 
        this.tableCell83.Dpi = 254F;
        this.tableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell83.Name = "tableCell83";
        this.tableCell83.StylePriority.UseFont = false;
        this.tableCell83.Weight = 0.616438392003286;
        // 
        // tableCell18
        // 
        this.tableCell18.CanGrow = false;
        this.tableCell18.Dpi = 254F;
        this.tableCell18.Name = "tableCell18";
        this.tableCell18.StylePriority.UseTextAlignment = false;
        this.tableCell18.Text = "dtPregao";
        this.tableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell18.Weight = 1.3255813776060594;
        this.tableCell18.WordWrap = false;
        // 
        // tableCell46
        // 
        this.tableCell46.Dpi = 254F;
        this.tableCell46.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell46.Name = "tableCell46";
        this.tableCell46.StylePriority.UseFont = false;
        this.tableCell46.Text = "Agente de Compensação";
        this.tableCell46.Weight = 3.0197368421052628;
        // 
        // tableCell168
        // 
        this.tableCell168.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell168.Dpi = 254F;
        this.tableCell168.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell168.Name = "tableCell168";
        this.tableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell168.StylePriority.UseBorders = false;
        this.tableCell168.StylePriority.UseFont = false;
        this.tableCell168.StylePriority.UsePadding = false;
        this.tableCell168.StylePriority.UseTextAlignment = false;
        this.tableCell168.Text = "X - Box";
        this.tableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell168.Weight = 0.80225876085713743;
        // 
        // tableCell160
        // 
        this.tableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell160.Dpi = 254F;
        this.tableCell160.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell160.Name = "tableCell160";
        this.tableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell160.StylePriority.UseBorders = false;
        this.tableCell160.StylePriority.UseFont = false;
        this.tableCell160.StylePriority.UsePadding = false;
        this.tableCell160.StylePriority.UseTextAlignment = false;
        this.tableCell160.Text = "I - POP";
        this.tableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell160.Weight = 0.8573022885606737;
        // 
        // table5
        // 
        this.table5.Dpi = 254F;
        this.table5.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.table5.LocationFloat = new DevExpress.Utils.PointFloat(1483.249F, 480F);
        this.table5.Name = "table5";
        this.table5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow14,
            this.tableRow16});
        this.table5.SizeF = new System.Drawing.SizeF(430F, 59.99997F);
        this.table5.StylePriority.UseFont = false;
        this.table5.StylePriority.UsePadding = false;
        this.table5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho8_BeforePrint);
        // 
        // tableRow14
        // 
        this.tableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell23,
            this.tableCell25});
        this.tableRow14.Dpi = 254F;
        this.tableRow14.Name = "tableRow14";
        this.tableRow14.Weight = 0.92307692307692313;
        // 
        // tableCell23
        // 
        this.tableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell23.CanGrow = false;
        this.tableCell23.Dpi = 254F;
        this.tableCell23.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell23.Name = "tableCell23";
        this.tableCell23.StylePriority.UseBorders = false;
        this.tableCell23.StylePriority.UseFont = false;
        this.tableCell23.Text = "Custodiante";
        this.tableCell23.Weight = 2.3293816826442422;
        this.tableCell23.WordWrap = false;
        // 
        // tableCell25
        // 
        this.tableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell25.CanGrow = false;
        this.tableCell25.Dpi = 254F;
        this.tableCell25.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell25.Name = "tableCell25";
        this.tableCell25.StylePriority.UseBorders = false;
        this.tableCell25.StylePriority.UseFont = false;
        this.tableCell25.Weight = 0.67061831735575794;
        this.tableCell25.WordWrap = false;
        // 
        // tableRow16
        // 
        this.tableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell31,
            this.tableCell35});
        this.tableRow16.Dpi = 254F;
        this.tableRow16.Name = "tableRow16";
        this.tableRow16.Weight = 0.923076923076923;
        // 
        // tableCell31
        // 
        this.tableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell31.CanGrow = false;
        this.tableCell31.Dpi = 254F;
        this.tableCell31.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell31.Name = "tableCell31";
        this.tableCell31.StylePriority.UseBorders = false;
        this.tableCell31.StylePriority.UseFont = false;
        this.tableCell31.Weight = 2.3293808825187319;
        this.tableCell31.WordWrap = false;
        // 
        // tableCell35
        // 
        this.tableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell35.CanGrow = false;
        this.tableCell35.Dpi = 254F;
        this.tableCell35.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell35.Name = "tableCell35";
        this.tableCell35.StylePriority.UseBorders = false;
        this.tableCell35.StylePriority.UseFont = false;
        this.tableCell35.Weight = 0.67061911748126857;
        this.tableCell35.WordWrap = false;
        // 
        // tableCell73
        // 
        this.tableCell73.CanGrow = false;
        this.tableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Prazo")});
        this.tableCell73.Dpi = 254F;
        this.tableCell73.EvenStyleName = "xrControlStyle1";
        this.tableCell73.Name = "tableCell73";
        this.tableCell73.OddStyleName = "xrControlStyle2";
        this.tableCell73.StylePriority.UseTextAlignment = false;
        this.tableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell73.Weight = 0.12877266869345716;
        this.tableCell73.WordWrap = false;
        // 
        // label4
        // 
        this.label4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label4.BorderWidth = 1;
        this.label4.CanGrow = false;
        this.label4.Dpi = 254F;
        this.label4.LocationFloat = new DevExpress.Utils.PointFloat(41F, 659.9999F);
        this.label4.Name = "label4";
        this.label4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label4.SizeF = new System.Drawing.SizeF(138F, 10F);
        this.label4.StylePriority.UseBorders = false;
        this.label4.StylePriority.UseBorderWidth = false;
        // 
        // table10
        // 
        this.table10.Dpi = 254F;
        this.table10.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table10.LocationFloat = new DevExpress.Utils.PointFloat(772.2294F, 480F);
        this.table10.Name = "table10";
        this.table10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow23,
            this.tableRow24});
        this.table10.SizeF = new System.Drawing.SizeF(270F, 60F);
        this.table10.StylePriority.UseFont = false;
        this.table10.StylePriority.UsePadding = false;
        this.table10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho6_BeforePrint);
        // 
        // tableRow23
        // 
        this.tableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell59});
        this.tableRow23.Dpi = 254F;
        this.tableRow23.Name = "tableRow23";
        this.tableRow23.Weight = 1;
        // 
        // tableCell59
        // 
        this.tableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell59.Dpi = 254F;
        this.tableCell59.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell59.Name = "tableCell59";
        this.tableCell59.StylePriority.UseBorders = false;
        this.tableCell59.StylePriority.UseFont = false;
        this.tableCell59.Text = "Cliente";
        this.tableCell59.Weight = 3.0197368421052628;
        // 
        // tableRow24
        // 
        this.tableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell60});
        this.tableRow24.Dpi = 254F;
        this.tableRow24.Name = "tableRow24";
        this.tableRow24.Weight = 1;
        // 
        // tableCell60
        // 
        this.tableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell60.CanGrow = false;
        this.tableCell60.Dpi = 254F;
        this.tableCell60.Name = "tableCell60";
        this.tableCell60.StylePriority.UseBorders = false;
        this.tableCell60.Weight = 3.0197368421052628;
        this.tableCell60.WordWrap = false;
        // 
        // tableCell112
        // 
        this.tableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell112.Dpi = 254F;
        this.tableCell112.Name = "tableCell112";
        this.tableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell112.StylePriority.UseBorders = false;
        this.tableCell112.StylePriority.UsePadding = false;
        this.tableCell112.Text = "Ajuste de day-Trade";
        this.tableCell112.Weight = 2.2308546619229546;
        // 
        // tableCell104
        // 
        this.tableCell104.Dpi = 254F;
        this.tableCell104.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell104.Name = "tableCell104";
        this.tableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell104.StylePriority.UseFont = false;
        this.tableCell104.StylePriority.UsePadding = false;
        this.tableCell104.Text = "Taxa de Liquidação";
        this.tableCell104.Weight = 2.07698807074394;
        // 
        // tableCell77
        // 
        this.tableCell77.CanGrow = false;
        this.tableCell77.Dpi = 254F;
        this.tableCell77.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell77.Name = "tableCell77";
        this.tableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell77.StylePriority.UseFont = false;
        this.tableCell77.StylePriority.UsePadding = false;
        this.tableCell77.Text = "Cliente";
        this.tableCell77.Weight = 3;
        this.tableCell77.WordWrap = false;
        // 
        // tableRow53
        // 
        this.tableRow53.BackColor = System.Drawing.Color.Gainsboro;
        this.tableRow53.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell116,
            this.tableCell118,
            this.tableCell123});
        this.tableRow53.Dpi = 254F;
        this.tableRow53.Name = "tableRow53";
        this.tableRow53.StylePriority.UseBackColor = false;
        this.tableRow53.StylePriority.UseBorders = false;
        this.tableRow53.Weight = 1.0001081930654219;
        // 
        // tableCell116
        // 
        this.tableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell116.Dpi = 254F;
        this.tableCell116.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell116.Name = "tableCell116";
        this.tableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell116.StylePriority.UseBorders = false;
        this.tableCell116.StylePriority.UseFont = false;
        this.tableCell116.StylePriority.UsePadding = false;
        this.tableCell116.Text = "Total CBLC";
        this.tableCell116.Weight = 2.07698807074394;
        // 
        // tableCell118
        // 
        this.tableCell118.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell118.Dpi = 254F;
        this.tableCell118.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell118.Name = "tableCell118";
        this.tableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell118.StylePriority.UseBorders = false;
        this.tableCell118.StylePriority.UseFont = false;
        this.tableCell118.StylePriority.UsePadding = false;
        this.tableCell118.StylePriority.UseTextAlignment = false;
        this.tableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell118.Weight = 0.7750836329449895;
        this.tableCell118.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell123
        // 
        this.tableCell123.CanGrow = false;
        this.tableCell123.Dpi = 254F;
        this.tableCell123.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell123.Name = "tableCell123";
        this.tableCell123.StylePriority.UseFont = false;
        this.tableCell123.StylePriority.UseTextAlignment = false;
        this.tableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell123.Weight = 0.14792829631107013;
        this.tableCell123.WordWrap = false;
        this.tableCell123.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell103
        // 
        this.tableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell103.Dpi = 254F;
        this.tableCell103.Name = "tableCell103";
        this.tableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell103.StylePriority.UseBorders = false;
        this.tableCell103.StylePriority.UsePadding = false;
        this.tableCell103.Text = "Vendas à futuro";
        this.tableCell103.Weight = 2.2308546619229546;
        // 
        // tableRow64
        // 
        this.tableRow64.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell138,
            this.tableCell139,
            this.tableCell147});
        this.tableRow64.Dpi = 254F;
        this.tableRow64.Name = "tableRow64";
        this.tableRow64.StylePriority.UseBorders = false;
        this.tableRow64.Weight = 1;
        // 
        // tableCell138
        // 
        this.tableCell138.Dpi = 254F;
        this.tableCell138.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell138.Name = "tableCell138";
        this.tableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell138.StylePriority.UseFont = false;
        this.tableCell138.StylePriority.UsePadding = false;
        this.tableCell138.Text = "Outras Bovespa";
        this.tableCell138.Weight = 2.0769887269801242;
        // 
        // tableCell139
        // 
        this.tableCell139.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell139.Dpi = 254F;
        this.tableCell139.Name = "tableCell139";
        this.tableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell139.StylePriority.UseBorders = false;
        this.tableCell139.StylePriority.UsePadding = false;
        this.tableCell139.StylePriority.UseTextAlignment = false;
        this.tableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell139.Weight = 0.775083985236358;
        this.tableCell139.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell147
        // 
        this.tableCell147.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell147.CanGrow = false;
        this.tableCell147.Dpi = 254F;
        this.tableCell147.Name = "tableCell147";
        this.tableCell147.StylePriority.UseBorders = false;
        this.tableCell147.StylePriority.UseTextAlignment = false;
        this.tableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell147.Weight = 0.14792728778351849;
        this.tableCell147.WordWrap = false;
        this.tableCell147.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell115
        // 
        this.tableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell115.Dpi = 254F;
        this.tableCell115.Name = "tableCell115";
        this.tableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell115.StylePriority.UseBorders = false;
        this.tableCell115.StylePriority.UsePadding = false;
        this.tableCell115.Text = "Valor das operações";
        this.tableCell115.Weight = 2.2308546619229546;
        // 
        // tableCell76
        // 
        this.tableCell76.CanGrow = false;
        this.tableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Valor", "{0:n2}")});
        this.tableCell76.Dpi = 254F;
        this.tableCell76.EvenStyleName = "xrControlStyle1";
        this.tableCell76.Name = "tableCell76";
        this.tableCell76.OddStyleName = "xrControlStyle2";
        this.tableCell76.StylePriority.UseTextAlignment = false;
        this.tableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.tableCell76.Weight = 0.39389286722245809;
        this.tableCell76.WordWrap = false;
        // 
        // tableCell177
        // 
        this.tableCell177.CanGrow = false;
        this.tableCell177.Dpi = 254F;
        this.tableCell177.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell177.Name = "tableCell177";
        this.tableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell177.StylePriority.UseFont = false;
        this.tableCell177.StylePriority.UsePadding = false;
        this.tableCell177.StylePriority.UseTextAlignment = false;
        this.tableCell177.Text = "Sac";
        this.tableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        this.tableCell177.Weight = 3;
        this.tableCell177.WordWrap = false;
        this.tableCell177.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SacFooterBeforePrint);
        // 
        // tableRow29
        // 
        this.tableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell65});
        this.tableRow29.Dpi = 254F;
        this.tableRow29.Name = "tableRow29";
        this.tableRow29.Weight = 1;
        // 
        // tableCell65
        // 
        this.tableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell65.Dpi = 254F;
        this.tableCell65.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell65.Name = "tableCell65";
        this.tableCell65.StylePriority.UseBorders = false;
        this.tableCell65.StylePriority.UseFont = false;
        this.tableCell65.Text = "Administrador";
        this.tableCell65.Weight = 3.0197368421052628;
        // 
        // tableRow66
        // 
        this.tableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell154});
        this.tableRow66.Dpi = 254F;
        this.tableRow66.Name = "tableRow66";
        this.tableRow66.Weight = 1;
        // 
        // tableCell154
        // 
        this.tableCell154.CanGrow = false;
        this.tableCell154.Dpi = 254F;
        this.tableCell154.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell154.Name = "tableCell154";
        this.tableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell154.StylePriority.UseFont = false;
        this.tableCell154.StylePriority.UsePadding = false;
        this.tableCell154.Text = "A coluna Q indica liquidação no Agente do Qualificado.";
        this.tableCell154.Weight = 3;
        this.tableCell154.WordWrap = false;
        // 
        // tableCell42
        // 
        this.tableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell42.CanGrow = false;
        this.tableCell42.Dpi = 254F;
        this.tableCell42.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell42.Name = "tableCell42";
        this.tableCell42.StylePriority.UseBorders = false;
        this.tableCell42.StylePriority.UseFont = false;
        this.tableCell42.Weight = 0.67061820425139362;
        this.tableCell42.WordWrap = false;
        // 
        // tableCell110
        // 
        this.tableCell110.Dpi = 254F;
        this.tableCell110.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell110.Name = "tableCell110";
        this.tableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell110.StylePriority.UseFont = false;
        this.tableCell110.StylePriority.UsePadding = false;
        this.tableCell110.Text = "Taxa de Registro";
        this.tableCell110.Weight = 2.07698807074394;
        // 
        // tableRow1
        // 
        this.tableRow1.BackColor = System.Drawing.Color.Transparent;
        this.tableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell74,
            this.tableCell1,
            this.tableCell72,
            this.tableCell71,
            this.tableCell73,
            this.tableCell2,
            this.xrTableCell1,
            this.xrTableCell2,
            this.tableCell34,
            this.tableCell85,
            this.tableCell3,
            this.tableCell76,
            this.tableCell75});
        this.tableRow1.Dpi = 254F;
        this.tableRow1.Name = "tableRow1";
        this.tableRow1.StylePriority.UseBackColor = false;
        this.tableRow1.Weight = 0.98596294878435553;
        // 
        // tableCell74
        // 
        this.tableCell74.CanGrow = false;
        this.tableCell74.Dpi = 254F;
        this.tableCell74.EvenStyleName = "xrControlStyle1";
        this.tableCell74.Name = "tableCell74";
        this.tableCell74.OddStyleName = "xrControlStyle2";
        this.tableCell74.StylePriority.UseTextAlignment = false;
        this.tableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell74.Weight = 0.060598902811866168;
        this.tableCell74.WordWrap = false;
        // 
        // tableCell1
        // 
        this.tableCell1.CanGrow = false;
        this.tableCell1.Dpi = 254F;
        this.tableCell1.EvenStyleName = "xrControlStyle1";
        this.tableCell1.Name = "tableCell1";
        this.tableCell1.OddStyleName = "xrControlStyle2";
        this.tableCell1.StylePriority.UseTextAlignment = false;
        this.tableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell1.Weight = 0.2120961472331826;
        this.tableCell1.WordWrap = false;
        this.tableCell1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NegociacaoBeforePrint);
        // 
        // tableCell72
        // 
        this.tableCell72.CanGrow = false;
        this.tableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TipoOperacao")});
        this.tableCell72.Dpi = 254F;
        this.tableCell72.EvenStyleName = "xrControlStyle1";
        this.tableCell72.Name = "tableCell72";
        this.tableCell72.OddStyleName = "xrControlStyle2";
        this.tableCell72.StylePriority.UseTextAlignment = false;
        this.tableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell72.Weight = 0.0757486275324139;
        this.tableCell72.WordWrap = false;
        // 
        // tableCell71
        // 
        this.tableCell71.CanGrow = false;
        this.tableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TipoMercado")});
        this.tableCell71.Dpi = 254F;
        this.tableCell71.EvenStyleName = "xrControlStyle1";
        this.tableCell71.Name = "tableCell71";
        this.tableCell71.OddStyleName = "xrControlStyle2";
        this.tableCell71.StylePriority.UseTextAlignment = false;
        this.tableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell71.Weight = 0.42419228962220407;
        this.tableCell71.WordWrap = false;
        // 
        // tableCell2
        // 
        this.tableCell2.CanGrow = false;
        this.tableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Especificacao")});
        this.tableCell2.Dpi = 254F;
        this.tableCell2.EvenStyleName = "xrControlStyle1";
        this.tableCell2.Name = "tableCell2";
        this.tableCell2.OddStyleName = "xrControlStyle2";
        this.tableCell2.StylePriority.UseTextAlignment = false;
        this.tableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell2.Weight = 0.30840675505252452;
        this.tableCell2.WordWrap = false;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.CanGrow = false;
        this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Especificacao1")});
        this.xrTableCell1.Dpi = 254F;
        this.xrTableCell1.EvenStyleName = "xrControlStyle1";
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.OddStyleName = "xrControlStyle2";
        this.xrTableCell1.StylePriority.UseTextAlignment = false;
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell1.Weight = 0.1404977030657304;
        this.xrTableCell1.WordWrap = false;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.CanGrow = false;
        this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "EspecificacaoOpcao")});
        this.xrTableCell2.Dpi = 254F;
        this.xrTableCell2.EvenStyleName = "xrControlStyle1";
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.OddStyleName = "xrControlStyle2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 254F);
        this.xrTableCell2.StylePriority.UsePadding = false;
        this.xrTableCell2.StylePriority.UseTextAlignment = false;
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell2.Weight = 0.27828234461925072;
        this.xrTableCell2.WordWrap = false;
        // 
        // tableCell34
        // 
        this.tableCell34.CanGrow = false;
        this.tableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Observacao")});
        this.tableCell34.Dpi = 254F;
        this.tableCell34.EvenStyleName = "xrControlStyle1";
        this.tableCell34.Name = "tableCell34";
        this.tableCell34.OddStyleName = "xrControlStyle2";
        this.tableCell34.StylePriority.UseTextAlignment = false;
        this.tableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell34.Weight = 0.16664697028525166;
        this.tableCell34.WordWrap = false;
        // 
        // tableCell85
        // 
        this.tableCell85.CanGrow = false;
        this.tableCell85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantidade", "{0:n0}")});
        this.tableCell85.Dpi = 254F;
        this.tableCell85.EvenStyleName = "xrControlStyle1";
        this.tableCell85.Name = "tableCell85";
        this.tableCell85.OddStyleName = "xrControlStyle2";
        this.tableCell85.StylePriority.UseTextAlignment = false;
        this.tableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.tableCell85.Weight = 0.348443662043828;
        this.tableCell85.WordWrap = false;
        // 
        // tableCell3
        // 
        this.tableCell3.CanGrow = false;
        this.tableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Preco", "{0:n2}")});
        this.tableCell3.Dpi = 254F;
        this.tableCell3.EvenStyleName = "xrControlStyle1";
        this.tableCell3.Name = "tableCell3";
        this.tableCell3.OddStyleName = "xrControlStyle2";
        this.tableCell3.StylePriority.UseTextAlignment = false;
        this.tableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.tableCell3.Weight = 0.30299449906154691;
        this.tableCell3.WordWrap = false;
        // 
        // tableCell75
        // 
        this.tableCell75.CanGrow = false;
        this.tableCell75.Dpi = 254F;
        this.tableCell75.EvenStyleName = "xrControlStyle1";
        this.tableCell75.Name = "tableCell75";
        this.tableCell75.OddStyleName = "xrControlStyle2";
        this.tableCell75.StylePriority.UseTextAlignment = false;
        this.tableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell75.Weight = 0.060598919556462925;
        this.tableCell75.WordWrap = false;
        this.tableCell75.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CreditoDebitoBeforePrint);
        // 
        // table16
        // 
        this.table16.Dpi = 254F;
        this.table16.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.table16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 35.7143F);
        this.table16.Name = "table16";
        this.table16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow46});
        this.table16.SizeF = new System.Drawing.SizeF(925.0042F, 30F);
        this.table16.StylePriority.UseFont = false;
        // 
        // tableRow46
        // 
        this.tableRow46.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell79});
        this.tableRow46.Dpi = 254F;
        this.tableRow46.Name = "tableRow46";
        this.tableRow46.StylePriority.UseBorders = false;
        this.tableRow46.Weight = 0.47244094488188981;
        // 
        // tableCell79
        // 
        this.tableCell79.CanGrow = false;
        this.tableCell79.Dpi = 254F;
        this.tableCell79.Font = new System.Drawing.Font("Times New Roman", 6.5F);
        this.tableCell79.Name = "tableCell79";
        this.tableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell79.StylePriority.UseFont = false;
        this.tableCell79.StylePriority.UsePadding = false;
        this.tableCell79.Text = "Observação: (1) As operações a futuro não são computadas no líquido da fatura";
        this.tableCell79.Weight = 3;
        this.tableCell79.WordWrap = false;
        // 
        // tableRow69
        // 
        this.tableRow69.BackColor = System.Drawing.Color.Gainsboro;
        this.tableRow69.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell151,
            this.tableCell152,
            this.tableCell153});
        this.tableRow69.Dpi = 254F;
        this.tableRow69.Name = "tableRow69";
        this.tableRow69.StylePriority.UseBackColor = false;
        this.tableRow69.StylePriority.UseBorders = false;
        this.tableRow69.Weight = 0.99429099859819181;
        // 
        // tableCell151
        // 
        this.tableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell151.Dpi = 254F;
        this.tableCell151.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell151.Name = "tableCell151";
        this.tableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell151.StylePriority.UseBorders = false;
        this.tableCell151.StylePriority.UseFont = false;
        this.tableCell151.StylePriority.UsePadding = false;
        this.tableCell151.Text = "Total Bovespa / Soma";
        this.tableCell151.Weight = 1.4703173417033448;
        // 
        // tableCell152
        // 
        this.tableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell152.Dpi = 254F;
        this.tableCell152.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell152.Name = "tableCell152";
        this.tableCell152.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell152.StylePriority.UseBorders = false;
        this.tableCell152.StylePriority.UseFont = false;
        this.tableCell152.StylePriority.UsePadding = false;
        this.tableCell152.StylePriority.UseTextAlignment = false;
        this.tableCell152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell152.Weight = 1.3817546922466144;
        this.tableCell152.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.FooterImprimeContinua_PrintOnPage);
        // 
        // tableCell153
        // 
        this.tableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell153.CanGrow = false;
        this.tableCell153.Dpi = 254F;
        this.tableCell153.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell153.Name = "tableCell153";
        this.tableCell153.StylePriority.UseBorders = false;
        this.tableCell153.StylePriority.UseFont = false;
        this.tableCell153.StylePriority.UseTextAlignment = false;
        this.tableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell153.Weight = 0.14792796605004038;
        this.tableCell153.WordWrap = false;
        this.tableCell153.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell109
        // 
        this.tableCell109.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell109.Dpi = 254F;
        this.tableCell109.Name = "tableCell109";
        this.tableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell109.StylePriority.UseBorders = false;
        this.tableCell109.StylePriority.UsePadding = false;
        this.tableCell109.Text = "Ajuste da posição";
        this.tableCell109.Weight = 2.2308546619229546;
        // 
        // tableRow20
        // 
        this.tableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell46});
        this.tableRow20.Dpi = 254F;
        this.tableRow20.Name = "tableRow20";
        this.tableRow20.StylePriority.UseBorders = false;
        this.tableRow20.Weight = 1;
        // 
        // tableCell159
        // 
        this.tableCell159.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell159.Dpi = 254F;
        this.tableCell159.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell159.Name = "tableCell159";
        this.tableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell159.StylePriority.UseBorders = false;
        this.tableCell159.StylePriority.UseFont = false;
        this.tableCell159.StylePriority.UsePadding = false;
        this.tableCell159.StylePriority.UseTextAlignment = false;
        this.tableCell159.Text = "C - Clubes e Fundos de Ações";
        this.tableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell159.Weight = 0.80225876085713743;
        // 
        // tableRow31
        // 
        this.tableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell77});
        this.tableRow31.Dpi = 254F;
        this.tableRow31.Name = "tableRow31";
        this.tableRow31.Weight = 1.7905053732138871;
        // 
        // tableCell182
        // 
        this.tableCell182.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell182.CanGrow = false;
        this.tableCell182.Dpi = 254F;
        this.tableCell182.Name = "tableCell182";
        this.tableCell182.StylePriority.UseBorders = false;
        this.tableCell182.StylePriority.UseTextAlignment = false;
        this.tableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell182.Weight = 0.14792718830898691;
        this.tableCell182.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow71
        // 
        this.tableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell161,
            this.tableCell162,
            this.tableCell163});
        this.tableRow71.Dpi = 254F;
        this.tableRow71.Name = "tableRow71";
        this.tableRow71.Weight = 1;
        // 
        // tableCell161
        // 
        this.tableCell161.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell161.Dpi = 254F;
        this.tableCell161.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell161.Name = "tableCell161";
        this.tableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell161.StylePriority.UseBorders = false;
        this.tableCell161.StylePriority.UseFont = false;
        this.tableCell161.StylePriority.UsePadding = false;
        this.tableCell161.StylePriority.UseTextAlignment = false;
        this.tableCell161.Text = "# - Negócio direto";
        this.tableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell161.Weight = 1.3404389505821888;
        // 
        // tableCell162
        // 
        this.tableCell162.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell162.Dpi = 254F;
        this.tableCell162.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell162.Name = "tableCell162";
        this.tableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell162.StylePriority.UseBorders = false;
        this.tableCell162.StylePriority.UseFont = false;
        this.tableCell162.StylePriority.UsePadding = false;
        this.tableCell162.StylePriority.UseTextAlignment = false;
        this.tableCell162.Text = "P - Carteira Própria";
        this.tableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell162.Weight = 0.80225876085713743;
        // 
        // tableCell163
        // 
        this.tableCell163.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell163.Dpi = 254F;
        this.tableCell163.Name = "tableCell163";
        this.tableCell163.StylePriority.UseBorders = false;
        this.tableCell163.Weight = 0.8573022885606737;
        // 
        // tableCell174
        // 
        this.tableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell174.Dpi = 254F;
        this.tableCell174.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell174.Name = "tableCell174";
        this.tableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell174.StylePriority.UseBorders = false;
        this.tableCell174.StylePriority.UseFont = false;
        this.tableCell174.StylePriority.UsePadding = false;
        this.tableCell174.StylePriority.UseTextAlignment = false;
        this.tableCell174.Text = "B - Debêntures";
        this.tableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell174.Weight = 1.3404389505821888;
        // 
        // tableRow15
        // 
        this.tableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell36,
            this.tableCell41});
        this.tableRow15.Dpi = 254F;
        this.tableRow15.Name = "tableRow15";
        this.tableRow15.Weight = 0.99999997554681241;
        // 
        // tableCell36
        // 
        this.tableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell36.CanGrow = false;
        this.tableCell36.Dpi = 254F;
        this.tableCell36.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell36.Name = "tableCell36";
        this.tableCell36.StylePriority.UseBorders = false;
        this.tableCell36.StylePriority.UseFont = false;
        this.tableCell36.Text = "Complemento Nome";
        this.tableCell36.Weight = 2.3293808918345102;
        this.tableCell36.WordWrap = false;
        // 
        // tableCell41
        // 
        this.tableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell41.CanGrow = false;
        this.tableCell41.Dpi = 254F;
        this.tableCell41.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell41.Name = "tableCell41";
        this.tableCell41.StylePriority.UseBorders = false;
        this.tableCell41.StylePriority.UseFont = false;
        this.tableCell41.Weight = 0.6706191081654892;
        this.tableCell41.WordWrap = false;
        // 
        // tableCell101
        // 
        this.tableCell101.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell101.Dpi = 254F;
        this.tableCell101.Name = "tableCell101";
        this.tableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell101.StylePriority.UseBorders = false;
        this.tableCell101.StylePriority.UsePadding = false;
        this.tableCell101.StylePriority.UseTextAlignment = false;
        this.tableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell101.Weight = 0.7750836329449895;
        this.tableCell101.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // line1
        // 
        this.line1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.line1.Dpi = 254F;
        this.line1.LineWidth = 3;
        this.line1.LocationFloat = new DevExpress.Utils.PointFloat(120.8387F, 178.5714F);
        this.line1.Name = "line1";
        this.line1.SizeF = new System.Drawing.SizeF(818.5455F, 14.99994F);
        this.line1.StylePriority.UseBorders = false;
        // 
        // table12
        // 
        this.table12.Dpi = 254F;
        this.table12.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table12.LocationFloat = new DevExpress.Utils.PointFloat(1050F, 480F);
        this.table12.Name = "table12";
        this.table12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow27,
            this.tableRow28});
        this.table12.SizeF = new System.Drawing.SizeF(420F, 60F);
        this.table12.StylePriority.UseFont = false;
        this.table12.StylePriority.UsePadding = false;
        this.table12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho7_BeforePrint);
        // 
        // tableRow27
        // 
        this.tableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell63});
        this.tableRow27.Dpi = 254F;
        this.tableRow27.Name = "tableRow27";
        this.tableRow27.Weight = 1;
        // 
        // tableCell63
        // 
        this.tableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell63.Dpi = 254F;
        this.tableCell63.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell63.Name = "tableCell63";
        this.tableCell63.StylePriority.UseBorders = false;
        this.tableCell63.StylePriority.UseFont = false;
        this.tableCell63.Text = "Valor";
        this.tableCell63.Weight = 3.0197368421052628;
        // 
        // tableRow28
        // 
        this.tableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell64,
            this.tableCell70,
            this.tableCell68});
        this.tableRow28.Dpi = 254F;
        this.tableRow28.Name = "tableRow28";
        this.tableRow28.Weight = 1;
        // 
        // tableCell64
        // 
        this.tableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell64.CanGrow = false;
        this.tableCell64.Dpi = 254F;
        this.tableCell64.Name = "tableCell64";
        this.tableCell64.StylePriority.UseBorders = false;
        this.tableCell64.Weight = 2.1569548872180446;
        this.tableCell64.WordWrap = false;
        // 
        // tableCell70
        // 
        this.tableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell70.CanGrow = false;
        this.tableCell70.Dpi = 254F;
        this.tableCell70.Name = "tableCell70";
        this.tableCell70.StylePriority.UseBorders = false;
        this.tableCell70.Weight = 0.50328947368421073;
        this.tableCell70.WordWrap = false;
        // 
        // tableCell68
        // 
        this.tableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell68.CanGrow = false;
        this.tableCell68.Dpi = 254F;
        this.tableCell68.Name = "tableCell68";
        this.tableCell68.StylePriority.UseBorders = false;
        this.tableCell68.Text = "C";
        this.tableCell68.Weight = 0.35949248120300747;
        // 
        // label20
        // 
        this.label20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label20.BorderWidth = 1;
        this.label20.CanGrow = false;
        this.label20.Dpi = 254F;
        this.label20.LocationFloat = new DevExpress.Utils.PointFloat(1076F, 0F);
        this.label20.Name = "label20";
        this.label20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label20.SizeF = new System.Drawing.SizeF(108F, 10F);
        this.label20.StylePriority.UseBorders = false;
        this.label20.StylePriority.UseBorderWidth = false;
        // 
        // tableRow8
        // 
        this.tableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell5,
            this.tableCell6,
            this.tableCell8});
        this.tableRow8.Dpi = 254F;
        this.tableRow8.Name = "tableRow8";
        this.tableRow8.Weight = 0.83333333333333348;
        // 
        // tableCell5
        // 
        this.tableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell5.Dpi = 254F;
        this.tableCell5.Font = new System.Drawing.Font("Times New Roman", 6F);
        this.tableCell5.Name = "tableCell5";
        this.tableCell5.StylePriority.UseBorders = false;
        this.tableCell5.StylePriority.UseFont = false;
        this.tableCell5.Text = "Nr. Nota";
        this.tableCell5.Weight = 0.90697677967160262;
        // 
        // tableCell6
        // 
        this.tableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell6.Dpi = 254F;
        this.tableCell6.Font = new System.Drawing.Font("Times New Roman", 6F);
        this.tableCell6.Name = "tableCell6";
        this.tableCell6.StylePriority.UseBorders = false;
        this.tableCell6.StylePriority.UseFont = false;
        this.tableCell6.Text = "Folha";
        this.tableCell6.Weight = 0.7674418427223384;
        // 
        // tableCell8
        // 
        this.tableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell8.Dpi = 254F;
        this.tableCell8.Font = new System.Drawing.Font("Times New Roman", 6F);
        this.tableCell8.Name = "tableCell8";
        this.tableCell8.StylePriority.UseBorders = false;
        this.tableCell8.StylePriority.UseFont = false;
        this.tableCell8.Text = "Data Pregão";
        this.tableCell8.Weight = 1.3255813776060592;
        // 
        // tableCell82
        // 
        this.tableCell82.CanGrow = false;
        this.tableCell82.Dpi = 254F;
        this.tableCell82.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
        this.tableCell82.Name = "tableCell82";
        this.tableCell82.StylePriority.UseFont = false;
        this.tableCell82.Text = "Carteira";
        this.tableCell82.Weight = 2.3835616079967146;
        this.tableCell82.WordWrap = false;
        // 
        // panel2
        // 
        this.panel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.panel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table14});
        this.panel2.Dpi = 254F;
        this.panel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 340F);
        this.panel2.Name = "panel2";
        this.panel2.SizeF = new System.Drawing.SizeF(1470F, 130F);
        this.panel2.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.panel2.StylePriority.UseBorders = false;
        // 
        // table14
        // 
        this.table14.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table14.Dpi = 254F;
        this.table14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.table14.Name = "table14";
        this.table14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow31,
            this.tableRow32,
            this.tableRow33,
            this.tableRow34});
        this.table14.SizeF = new System.Drawing.SizeF(1460F, 130F);
        this.table14.StylePriority.UseBorders = false;
        this.table14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho3_BeforePrint);
        // 
        // tableRow32
        // 
        this.tableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell80,
            this.tableCell82});
        this.tableRow32.Dpi = 254F;
        this.tableRow32.Name = "tableRow32";
        this.tableRow32.Weight = 2.4415981186015938;
        // 
        // tableCell80
        // 
        this.tableCell80.CanGrow = false;
        this.tableCell80.Dpi = 254F;
        this.tableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell80.Name = "tableCell80";
        this.tableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
        this.tableCell80.StylePriority.UseFont = false;
        this.tableCell80.StylePriority.UsePadding = false;
        this.tableCell80.Text = "IdCarteira";
        this.tableCell80.Weight = 0.61643839200328576;
        this.tableCell80.WordWrap = false;
        // 
        // tableRow33
        // 
        this.tableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell83,
            this.tableCell84,
            this.tableCell67});
        this.tableRow33.Dpi = 254F;
        this.tableRow33.Name = "tableRow33";
        this.tableRow33.Weight = 2.5392620942556046;
        // 
        // tableCell84
        // 
        this.tableCell84.CanGrow = false;
        this.tableCell84.Dpi = 254F;
        this.tableCell84.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell84.Name = "tableCell84";
        this.tableCell84.StylePriority.UseFont = false;
        this.tableCell84.Text = "EnderecoBairro";
        this.tableCell84.Weight = 1.8287670743414917;
        this.tableCell84.WordWrap = false;
        // 
        // tableCell67
        // 
        this.tableCell67.CanGrow = false;
        this.tableCell67.Dpi = 254F;
        this.tableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell67.Name = "tableCell67";
        this.tableCell67.StylePriority.UseFont = false;
        this.tableCell67.Text = "Telefone";
        this.tableCell67.Weight = 0.55479453365522269;
        this.tableCell67.WordWrap = false;
        // 
        // tableRow34
        // 
        this.tableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell86,
            this.tableCell87,
            this.tableCell69});
        this.tableRow34.Dpi = 254F;
        this.tableRow34.Name = "tableRow34";
        this.tableRow34.Weight = 2.5392621310734707;
        // 
        // tableCell86
        // 
        this.tableCell86.Dpi = 254F;
        this.tableCell86.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell86.Name = "tableCell86";
        this.tableCell86.StylePriority.UseFont = false;
        this.tableCell86.Weight = 0.61643831320598907;
        // 
        // tableCell87
        // 
        this.tableCell87.CanGrow = false;
        this.tableCell87.Dpi = 254F;
        this.tableCell87.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell87.Name = "tableCell87";
        this.tableCell87.StylePriority.UseFont = false;
        this.tableCell87.Text = "Cep";
        this.tableCell87.Weight = 0.27786543904339689;
        this.tableCell87.WordWrap = false;
        // 
        // tableCell69
        // 
        this.tableCell69.CanGrow = false;
        this.tableCell69.Dpi = 254F;
        this.tableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell69.Name = "tableCell69";
        this.tableCell69.StylePriority.UseFont = false;
        this.tableCell69.Text = "CidadeEstado";
        this.tableCell69.Weight = 2.1056962477506143;
        this.tableCell69.WordWrap = false;
        // 
        // tableCell144
        // 
        this.tableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell144.CanGrow = false;
        this.tableCell144.Dpi = 254F;
        this.tableCell144.Name = "tableCell144";
        this.tableCell144.StylePriority.UseBorders = false;
        this.tableCell144.StylePriority.UseTextAlignment = false;
        this.tableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell144.Weight = 0.147928366392089;
        this.tableCell144.WordWrap = false;
        this.tableCell144.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell44
        // 
        this.tableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell44.CanGrow = false;
        this.tableCell44.Dpi = 254F;
        this.tableCell44.Name = "tableCell44";
        this.tableCell44.StylePriority.UseBorders = false;
        this.tableCell44.Weight = 1.4210526516563016;
        this.tableCell44.WordWrap = false;
        // 
        // tableCell146
        // 
        this.tableCell146.Dpi = 254F;
        this.tableCell146.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell146.Name = "tableCell146";
        this.tableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell146.StylePriority.UseFont = false;
        this.tableCell146.StylePriority.UsePadding = false;
        this.tableCell146.Text = "Emolumentos";
        this.tableCell146.Weight = 2.0769883759350387;
        // 
        // label9
        // 
        this.label9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label9.BorderWidth = 1;
        this.label9.CanGrow = false;
        this.label9.Dpi = 254F;
        this.label9.LocationFloat = new DevExpress.Utils.PointFloat(1076F, 659.9999F);
        this.label9.Name = "label9";
        this.label9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label9.SizeF = new System.Drawing.SizeF(108F, 10F);
        this.label9.StylePriority.UseBorders = false;
        this.label9.StylePriority.UseBorderWidth = false;
        // 
        // tableCell56
        // 
        this.tableCell56.Dpi = 254F;
        this.tableCell56.Name = "tableCell56";
        this.tableCell56.Text = "Obs.(*)";
        this.tableCell56.Weight = 0.15133032256273904;
        // 
        // label5
        // 
        this.label5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label5.BorderWidth = 1;
        this.label5.CanGrow = false;
        this.label5.Dpi = 254F;
        this.label5.LocationFloat = new DevExpress.Utils.PointFloat(181F, 659.9999F);
        this.label5.Name = "label5";
        this.label5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label5.SizeF = new System.Drawing.SizeF(48F, 10F);
        this.label5.StylePriority.UseBorders = false;
        this.label5.StylePriority.UseBorderWidth = false;
        // 
        // tableCell142
        // 
        this.tableCell142.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell142.CanGrow = false;
        this.tableCell142.Dpi = 254F;
        this.tableCell142.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell142.Name = "tableCell142";
        this.tableCell142.StylePriority.UseBorders = false;
        this.tableCell142.StylePriority.UseFont = false;
        this.tableCell142.StylePriority.UseTextAlignment = false;
        this.tableCell142.Text = "Clear CTVM LTDA";
        this.tableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell142.Weight = 3;
        this.tableCell142.WordWrap = false;
        // 
        // tableCell52
        // 
        this.tableCell52.CanGrow = false;
        this.tableCell52.Dpi = 254F;
        this.tableCell52.Name = "tableCell52";
        this.tableCell52.Text = "Negociação";
        this.tableCell52.Weight = 0.19260223241620461;
        this.tableCell52.WordWrap = false;
        // 
        // tableRow19
        // 
        this.tableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell43,
            this.tableCell44,
            this.tableCell45});
        this.tableRow19.Dpi = 254F;
        this.tableRow19.Name = "tableRow19";
        this.tableRow19.Weight = 1;
        // 
        // tableCell43
        // 
        this.tableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell43.CanGrow = false;
        this.tableCell43.Dpi = 254F;
        this.tableCell43.Name = "tableCell43";
        this.tableCell43.StylePriority.UseBorders = false;
        this.tableCell43.Weight = 0.43421054639314349;
        this.tableCell43.WordWrap = false;
        // 
        // tableCell45
        // 
        this.tableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell45.CanGrow = false;
        this.tableCell45.Dpi = 254F;
        this.tableCell45.Name = "tableCell45";
        this.tableCell45.StylePriority.UseBorders = false;
        this.tableCell45.Weight = 1.1644736440558181;
        this.tableCell45.WordWrap = false;
        // 
        // tableCell102
        // 
        this.tableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell102.Dpi = 254F;
        this.tableCell102.Name = "tableCell102";
        this.tableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell102.StylePriority.UseBorders = false;
        this.tableCell102.StylePriority.UsePadding = false;
        this.tableCell102.StylePriority.UseTextAlignment = false;
        this.tableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell102.Weight = 0.76914533807704522;
        this.tableCell102.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell126
        // 
        this.tableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell126.CanGrow = false;
        this.tableCell126.Dpi = 254F;
        this.tableCell126.Name = "tableCell126";
        this.tableCell126.StylePriority.UseBorders = false;
        this.tableCell126.StylePriority.UseTextAlignment = false;
        this.tableCell126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell126.Weight = 0.14792796605004038;
        this.tableCell126.WordWrap = false;
        this.tableCell126.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell155
        // 
        this.tableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
        this.tableCell155.Dpi = 254F;
        this.tableCell155.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
        this.tableCell155.Name = "tableCell155";
        this.tableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell155.StylePriority.UseBorders = false;
        this.tableCell155.StylePriority.UseFont = false;
        this.tableCell155.StylePriority.UsePadding = false;
        this.tableCell155.StylePriority.UseTextAlignment = false;
        this.tableCell155.Text = "(*) - Observações:";
        this.tableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell155.Weight = 1.3404395290880771;
        // 
        // tableCell57
        // 
        this.tableCell57.Dpi = 254F;
        this.tableCell57.Name = "tableCell57";
        this.tableCell57.StylePriority.UseTextAlignment = false;
        this.tableCell57.Text = "Preço/Ajuste";
        this.tableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.tableCell57.Weight = 0.27514604435082657;
        // 
        // tableRow44
        // 
        this.tableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell112,
            this.tableCell114});
        this.tableRow44.Dpi = 254F;
        this.tableRow44.Name = "tableRow44";
        this.tableRow44.Weight = 1;
        // 
        // tableCell114
        // 
        this.tableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell114.Dpi = 254F;
        this.tableCell114.Name = "tableCell114";
        this.tableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell114.StylePriority.UseBorders = false;
        this.tableCell114.StylePriority.UsePadding = false;
        this.tableCell114.StylePriority.UseTextAlignment = false;
        this.tableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell114.Weight = 0.76914533807704522;
        this.tableCell114.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell22
        // 
        this.tableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell22.CanGrow = false;
        this.tableCell22.Dpi = 254F;
        this.tableCell22.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell22.Name = "tableCell22";
        this.tableCell22.StylePriority.UseBorders = false;
        this.tableCell22.StylePriority.UseFont = false;
        this.tableCell22.Text = "C.P.F./C.N.P.J./C.V.M./C.O.B.";
        this.tableCell22.Weight = 3;
        this.tableCell22.WordWrap = false;
        // 
        // tableCell129
        // 
        this.tableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell129.Dpi = 254F;
        this.tableCell129.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell129.Name = "tableCell129";
        this.tableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell129.StylePriority.UseBorders = false;
        this.tableCell129.StylePriority.UseFont = false;
        this.tableCell129.StylePriority.UsePadding = false;
        this.tableCell129.Text = "Corretagem / Despesas";
        this.tableCell129.Weight = 3;
        // 
        // tableCell99
        // 
        this.tableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell99.Dpi = 254F;
        this.tableCell99.Name = "tableCell99";
        this.tableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell99.StylePriority.UseBorders = false;
        this.tableCell99.StylePriority.UsePadding = false;
        this.tableCell99.StylePriority.UseTextAlignment = false;
        this.tableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell99.Weight = 0.76914533807704522;
        this.tableCell99.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell97
        // 
        this.tableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell97.Dpi = 254F;
        this.tableCell97.Name = "tableCell97";
        this.tableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell97.StylePriority.UseBorders = false;
        this.tableCell97.StylePriority.UsePadding = false;
        this.tableCell97.Text = "Opções - vendas";
        this.tableCell97.Weight = 2.2308546619229546;
        // 
        // table13
        // 
        this.table13.Dpi = 254F;
        this.table13.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table13.LocationFloat = new DevExpress.Utils.PointFloat(1050F, 550F);
        this.table13.Name = "table13";
        this.table13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow29,
            this.tableRow30});
        this.table13.SizeF = new System.Drawing.SizeF(420F, 60F);
        this.table13.StylePriority.UseFont = false;
        this.table13.StylePriority.UsePadding = false;
        this.table13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho11_BeforePrint);
        // 
        // tableRow30
        // 
        this.tableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell66});
        this.tableRow30.Dpi = 254F;
        this.tableRow30.Name = "tableRow30";
        this.tableRow30.Weight = 1;
        // 
        // tableCell38
        // 
        this.tableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell38.CanGrow = false;
        this.tableCell38.Dpi = 254F;
        this.tableCell38.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell38.Name = "tableCell38";
        this.tableCell38.StylePriority.UseBorders = false;
        this.tableCell38.StylePriority.UseFont = false;
        this.tableCell38.Weight = 2.3293817957486063;
        this.tableCell38.WordWrap = false;
        // 
        // tableCell128
        // 
        this.tableCell128.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell128.Dpi = 254F;
        this.tableCell128.Name = "tableCell128";
        this.tableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell128.StylePriority.UseBorders = false;
        this.tableCell128.StylePriority.UsePadding = false;
        this.tableCell128.StylePriority.UseTextAlignment = false;
        this.tableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell128.Weight = 0.77508365801492063;
        this.tableCell128.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow48
        // 
        this.tableRow48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell89,
            this.tableCell173});
        this.tableRow48.Dpi = 254F;
        this.tableRow48.Name = "tableRow48";
        this.tableRow48.StylePriority.UseBorders = false;
        this.tableRow48.Weight = 1.0001080866296379;
        // 
        // tableCell89
        // 
        this.tableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell89.Dpi = 254F;
        this.tableCell89.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell89.Name = "tableCell89";
        this.tableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell89.StylePriority.UseBorders = false;
        this.tableCell89.StylePriority.UseFont = false;
        this.tableCell89.StylePriority.UsePadding = false;
        this.tableCell89.Text = "Resumo Financeiro";
        this.tableCell89.Weight = 2.7484936426445965;
        // 
        // tableCell173
        // 
        this.tableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell173.CanGrow = false;
        this.tableCell173.Dpi = 254F;
        this.tableCell173.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell173.Name = "tableCell173";
        this.tableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell173.StylePriority.UseBorders = false;
        this.tableCell173.StylePriority.UseFont = false;
        this.tableCell173.StylePriority.UsePadding = false;
        this.tableCell173.StylePriority.UseTextAlignment = false;
        this.tableCell173.Text = "D/C";
        this.tableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell173.Weight = 0.25150635735540339;
        this.tableCell173.WordWrap = false;
        // 
        // tableCell130
        // 
        this.tableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell130.Dpi = 254F;
        this.tableCell130.Name = "tableCell130";
        this.tableCell130.StylePriority.UseBorders = false;
        this.tableCell130.Weight = 3;
        // 
        // tableCell156
        // 
        this.tableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Top;
        this.tableCell156.Dpi = 254F;
        this.tableCell156.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell156.Name = "tableCell156";
        this.tableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell156.StylePriority.UseBorders = false;
        this.tableCell156.StylePriority.UseFont = false;
        this.tableCell156.StylePriority.UsePadding = false;
        this.tableCell156.StylePriority.UseTextAlignment = false;
        this.tableCell156.Text = "A- Posição Futuro";
        this.tableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell156.Weight = 0.8022581032089745;
        // 
        // tableCell91
        // 
        this.tableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell91.Dpi = 254F;
        this.tableCell91.Name = "tableCell91";
        this.tableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell91.StylePriority.UseBorders = false;
        this.tableCell91.StylePriority.UsePadding = false;
        this.tableCell91.Text = "Compras à Vista";
        this.tableCell91.Weight = 2.2308546619229546;
        // 
        // table9
        // 
        this.table9.Dpi = 254F;
        this.table9.Font = new System.Drawing.Font("Times New Roman", 6.5F);
        this.table9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 640F);
        this.table9.Name = "table9";
        this.table9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow22});
        this.table9.SizeF = new System.Drawing.SizeF(1915F, 19.99994F);
        this.table9.StylePriority.UseFont = false;
        this.table9.StylePriority.UseTextAlignment = false;
        this.table9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // tableRow22
        // 
        this.tableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell47,
            this.tableCell52,
            this.tableCell58,
            this.tableCell51,
            this.tableCell55,
            this.tableCell48,
            this.tableCell56,
            this.tableCell53,
            this.tableCell57,
            this.tableCell50,
            this.tableCell54});
        this.tableRow22.Dpi = 254F;
        this.tableRow22.Name = "tableRow22";
        this.tableRow22.Weight = 1;
        // 
        // tableCell47
        // 
        this.tableCell47.CanGrow = false;
        this.tableCell47.Dpi = 254F;
        this.tableCell47.Name = "tableCell47";
        this.tableCell47.Text = "Q";
        this.tableCell47.Weight = 0.055029208328198409;
        this.tableCell47.WordWrap = false;
        // 
        // tableCell58
        // 
        this.tableCell58.CanGrow = false;
        this.tableCell58.Dpi = 254F;
        this.tableCell58.Name = "tableCell58";
        this.tableCell58.Text = "C/V";
        this.tableCell58.Weight = 0.068786514712341415;
        this.tableCell58.WordWrap = false;
        // 
        // tableCell51
        // 
        this.tableCell51.CanGrow = false;
        this.tableCell51.Dpi = 254F;
        this.tableCell51.Name = "tableCell51";
        this.tableCell51.Text = "Tipo Mercado";
        this.tableCell51.Weight = 0.385204460227149;
        this.tableCell51.WordWrap = false;
        // 
        // tableCell55
        // 
        this.tableCell55.Dpi = 254F;
        this.tableCell55.Name = "tableCell55";
        this.tableCell55.Text = "Prazo";
        this.tableCell55.Weight = 0.11693707625518711;
        // 
        // tableCell48
        // 
        this.tableCell48.Dpi = 254F;
        this.tableCell48.Name = "tableCell48";
        this.tableCell48.Text = "Especificação do Título";
        this.tableCell48.Weight = 0.66035053257888132;
        // 
        // tableCell53
        // 
        this.tableCell53.Dpi = 254F;
        this.tableCell53.Name = "tableCell53";
        this.tableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell53.StylePriority.UsePadding = false;
        this.tableCell53.StylePriority.UseTextAlignment = false;
        this.tableCell53.Text = "Quantidade";
        this.tableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.tableCell53.Weight = 0.31641795546869889;
        // 
        // tableCell54
        // 
        this.tableCell54.CanGrow = false;
        this.tableCell54.Dpi = 254F;
        this.tableCell54.Font = new System.Drawing.Font("Times New Roman", 6.3F);
        this.tableCell54.Name = "tableCell54";
        this.tableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 254F);
        this.tableCell54.StylePriority.UseFont = false;
        this.tableCell54.StylePriority.UsePadding = false;
        this.tableCell54.StylePriority.UseTextAlignment = false;
        this.tableCell54.Text = "D/C";
        this.tableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        this.tableCell54.Weight = 0.0550292305781959;
        this.tableCell54.WordWrap = false;
        // 
        // tableRow39
        // 
        this.tableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell97,
            this.tableCell99});
        this.tableRow39.Dpi = 254F;
        this.tableRow39.Name = "tableRow39";
        this.tableRow39.Weight = 1;
        // 
        // tableCell49
        // 
        this.tableCell49.CanGrow = false;
        this.tableCell49.Dpi = 254F;
        this.tableCell49.Name = "tableCell49";
        this.tableCell49.Weight = 3.0197368421052628;
        this.tableCell49.WordWrap = false;
        // 
        // tableRow26
        // 
        this.tableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell62});
        this.tableRow26.Dpi = 254F;
        this.tableRow26.Name = "tableRow26";
        this.tableRow26.Weight = 1;
        // 
        // tableCell62
        // 
        this.tableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell62.CanGrow = false;
        this.tableCell62.Dpi = 254F;
        this.tableCell62.Name = "tableCell62";
        this.tableCell62.StylePriority.UseBorders = false;
        this.tableCell62.Weight = 3.0197368421052628;
        this.tableCell62.WordWrap = false;
        // 
        // tableCell172
        // 
        this.tableCell172.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell172.Dpi = 254F;
        this.tableCell172.Name = "tableCell172";
        this.tableCell172.StylePriority.UseBorders = false;
        this.tableCell172.Weight = 0.8573022885606737;
        // 
        // tableCell37
        // 
        this.tableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell37.Dpi = 254F;
        this.tableCell37.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell37.Name = "tableCell37";
        this.tableCell37.StylePriority.UseBorders = false;
        this.tableCell37.StylePriority.UseFont = false;
        this.tableCell37.Text = "Banco";
        this.tableCell37.Weight = 0.43421054639314349;
        // 
        // tableCell166
        // 
        this.tableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell166.Dpi = 254F;
        this.tableCell166.Name = "tableCell166";
        this.tableCell166.StylePriority.UseBorders = false;
        this.tableCell166.Weight = 0.8573022885606737;
        // 
        // tableCell121
        // 
        this.tableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell121.CanGrow = false;
        this.tableCell121.Dpi = 254F;
        this.tableCell121.Name = "tableCell121";
        this.tableCell121.StylePriority.UseBorders = false;
        this.tableCell121.StylePriority.UseTextAlignment = false;
        this.tableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell121.Weight = 0.14792829631107013;
        this.tableCell121.WordWrap = false;
        this.tableCell121.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell61
        // 
        this.tableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell61.Dpi = 254F;
        this.tableCell61.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell61.Name = "tableCell61";
        this.tableCell61.StylePriority.UseBorders = false;
        this.tableCell61.StylePriority.UseFont = false;
        this.tableCell61.Text = "Acionista";
        this.tableCell61.Weight = 3.0197368421052628;
        // 
        // table22
        // 
        this.table22.Dpi = 254F;
        this.table22.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table22.LocationFloat = new DevExpress.Utils.PointFloat(960F, 215F);
        this.table22.Name = "table22";
        this.table22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow54,
            this.tableRow56,
            this.tableRow57,
            this.tableRow58,
            this.tableRow69});
        this.table22.SizeF = new System.Drawing.SizeF(950F, 180F);
        this.table22.StylePriority.UseFont = false;
        this.table22.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer3_BeforePrint);
        // 
        // tableRow54
        // 
        this.tableRow54.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell95});
        this.tableRow54.Dpi = 254F;
        this.tableRow54.Name = "tableRow54";
        this.tableRow54.StylePriority.UseBorders = false;
        this.tableRow54.Weight = 1.0002378514507555;
        // 
        // tableCell95
        // 
        this.tableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell95.Dpi = 254F;
        this.tableCell95.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell95.Name = "tableCell95";
        this.tableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell95.StylePriority.UseBorders = false;
        this.tableCell95.StylePriority.UseFont = false;
        this.tableCell95.StylePriority.UsePadding = false;
        this.tableCell95.Text = "Bovespa / Soma";
        this.tableCell95.Weight = 3;
        // 
        // tableRow56
        // 
        this.tableRow56.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell124,
            this.tableCell125,
            this.tableCell126});
        this.tableRow56.Dpi = 254F;
        this.tableRow56.Name = "tableRow56";
        this.tableRow56.StylePriority.UseBorders = false;
        this.tableRow56.Weight = 1.0002378514507555;
        // 
        // tableCell124
        // 
        this.tableCell124.Dpi = 254F;
        this.tableCell124.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell124.Name = "tableCell124";
        this.tableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell124.StylePriority.UseFont = false;
        this.tableCell124.StylePriority.UsePadding = false;
        this.tableCell124.Text = "Taxa de opções / futuro";
        this.tableCell124.Weight = 2.0769883759350387;
        // 
        // tableRow57
        // 
        this.tableRow57.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell127,
            this.tableCell128,
            this.tableCell143});
        this.tableRow57.Dpi = 254F;
        this.tableRow57.Name = "tableRow57";
        this.tableRow57.StylePriority.UseBorders = false;
        this.tableRow57.Weight = 1.0002378514507555;
        // 
        // tableCell127
        // 
        this.tableCell127.Dpi = 254F;
        this.tableCell127.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell127.Name = "tableCell127";
        this.tableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell127.StylePriority.UseFont = false;
        this.tableCell127.StylePriority.UsePadding = false;
        this.tableCell127.Text = "Taxa A.N.A";
        this.tableCell127.Weight = 2.0769883759350387;
        // 
        // tableCell143
        // 
        this.tableCell143.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell143.CanGrow = false;
        this.tableCell143.Dpi = 254F;
        this.tableCell143.Name = "tableCell143";
        this.tableCell143.StylePriority.UseBorders = false;
        this.tableCell143.StylePriority.UseTextAlignment = false;
        this.tableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell143.Weight = 0.14792796605004038;
        this.tableCell143.WordWrap = false;
        this.tableCell143.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow58
        // 
        this.tableRow58.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell146,
            this.tableCell149,
            this.tableCell150});
        this.tableRow58.Dpi = 254F;
        this.tableRow58.Name = "tableRow58";
        this.tableRow58.StylePriority.UseBorders = false;
        this.tableRow58.Weight = 1.0002378514507555;
        // 
        // tableCell149
        // 
        this.tableCell149.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell149.Dpi = 254F;
        this.tableCell149.Name = "tableCell149";
        this.tableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell149.StylePriority.UseBorders = false;
        this.tableCell149.StylePriority.UsePadding = false;
        this.tableCell149.StylePriority.UseTextAlignment = false;
        this.tableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell149.Weight = 0.77508365801492063;
        this.tableCell149.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell150
        // 
        this.tableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell150.CanGrow = false;
        this.tableCell150.Dpi = 254F;
        this.tableCell150.Name = "tableCell150";
        this.tableCell150.StylePriority.UseBorders = false;
        this.tableCell150.StylePriority.UseTextAlignment = false;
        this.tableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell150.Weight = 0.14792796605004038;
        this.tableCell150.WordWrap = false;
        this.tableCell150.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // BottomMargin
        // 
        this.BottomMargin.Dpi = 254F;
        this.BottomMargin.HeightF = 150F;
        this.BottomMargin.Name = "BottomMargin";
        // 
        // tableCell170
        // 
        this.tableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell170.Dpi = 254F;
        this.tableCell170.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell170.Name = "tableCell170";
        this.tableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell170.StylePriority.UseBorders = false;
        this.tableCell170.StylePriority.UseFont = false;
        this.tableCell170.StylePriority.UsePadding = false;
        this.tableCell170.StylePriority.UseTextAlignment = false;
        this.tableCell170.Text = "F - Cobertura";
        this.tableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell170.Weight = 1.3404389505821888;
        // 
        // tableRow75
        // 
        this.tableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell174,
            this.tableCell175,
            this.tableCell176});
        this.tableRow75.Dpi = 254F;
        this.tableRow75.Name = "tableRow75";
        this.tableRow75.Weight = 0.92400039672851564;
        // 
        // tableCell175
        // 
        this.tableCell175.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell175.Dpi = 254F;
        this.tableCell175.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell175.Name = "tableCell175";
        this.tableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell175.StylePriority.UseBorders = false;
        this.tableCell175.StylePriority.UseFont = false;
        this.tableCell175.StylePriority.UsePadding = false;
        this.tableCell175.StylePriority.UseTextAlignment = false;
        this.tableCell175.Text = "L - Precatório";
        this.tableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell175.Weight = 0.80225876085713743;
        // 
        // tableRow17
        // 
        this.tableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell38,
            this.tableCell42});
        this.tableRow17.Dpi = 254F;
        this.tableRow17.Name = "tableRow17";
        this.tableRow17.Weight = 1.0000000244531875;
        // 
        // label2
        // 
        this.label2.Dpi = 254F;
        this.label2.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.label2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 610F);
        this.label2.Name = "label2";
        this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label2.SizeF = new System.Drawing.SizeF(343.9583F, 27F);
        this.label2.StylePriority.UseFont = false;
        this.label2.Text = "Negócios Realizados";
        // 
        // table25
        // 
        this.table25.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table25.Dpi = 254F;
        this.table25.LocationFloat = new DevExpress.Utils.PointFloat(4.037221E-05F, 904F);
        this.table25.Name = "table25";
        this.table25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.table25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow76});
        this.table25.SizeF = new System.Drawing.SizeF(1913.249F, 35.00012F);
        this.table25.StylePriority.UseBorders = false;
        this.table25.StylePriority.UsePadding = false;
        // 
        // tableRow76
        // 
        this.tableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell177});
        this.tableRow76.Dpi = 254F;
        this.tableRow76.Name = "tableRow76";
        this.tableRow76.Weight = 0.72134038689777968;
        // 
        // tableCell33
        // 
        this.tableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell33.CanGrow = false;
        this.tableCell33.Dpi = 254F;
        this.tableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell33.Name = "tableCell33";
        this.tableCell33.StylePriority.UseBorders = false;
        this.tableCell33.StylePriority.UseFont = false;
        this.tableCell33.StylePriority.UseTextAlignment = false;
        this.tableCell33.Text = "CPF";
        this.tableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell33.Weight = 3;
        this.tableCell33.WordWrap = false;
        // 
        // tableCell136
        // 
        this.tableCell136.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell136.CanGrow = false;
        this.tableCell136.Dpi = 254F;
        this.tableCell136.Name = "tableCell136";
        this.tableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell136.StylePriority.UseBorders = false;
        this.tableCell136.StylePriority.UsePadding = false;
        this.tableCell136.StylePriority.UseTextAlignment = false;
        this.tableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell136.Weight = 0.77336235927889463;
        this.tableCell136.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // table3
        // 
        this.table3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.table3.Dpi = 254F;
        this.table3.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.table3.LocationFloat = new DevExpress.Utils.PointFloat(1485F, 25.00001F);
        this.table3.Name = "table3";
        this.table3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow8,
            this.tableRow9});
        this.table3.SizeF = new System.Drawing.SizeF(430F, 60F);
        this.table3.StylePriority.UseBorders = false;
        this.table3.StylePriority.UseFont = false;
        this.table3.StylePriority.UsePadding = false;
        this.table3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho1_BeforePrint);
        // 
        // tableRow9
        // 
        this.tableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell9,
            this.tableCell15,
            this.tableCell18});
        this.tableRow9.Dpi = 254F;
        this.tableRow9.Name = "tableRow9";
        this.tableRow9.Weight = 1.1666666666666665;
        // 
        // tableCell9
        // 
        this.tableCell9.CanGrow = false;
        this.tableCell9.Dpi = 254F;
        this.tableCell9.Name = "tableCell9";
        this.tableCell9.StylePriority.UseTextAlignment = false;
        this.tableCell9.Text = "Nota";
        this.tableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell9.Weight = 0.9069767796716024;
        this.tableCell9.WordWrap = false;
        // 
        // tableCell15
        // 
        this.tableCell15.CanGrow = false;
        this.tableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pageInfo1});
        this.tableCell15.Dpi = 254F;
        this.tableCell15.Name = "tableCell15";
        this.tableCell15.StylePriority.UseTextAlignment = false;
        this.tableCell15.Text = "Folha";
        this.tableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell15.Weight = 0.7674418427223384;
        this.tableCell15.WordWrap = false;
        // 
        // pageInfo1
        // 
        this.pageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.pageInfo1.Dpi = 254F;
        this.pageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.pageInfo1.Name = "pageInfo1";
        this.pageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.pageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
        this.pageInfo1.SizeF = new System.Drawing.SizeF(110F, 35F);
        this.pageInfo1.StylePriority.UseBorders = false;
        // 
        // tableCell171
        // 
        this.tableCell171.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell171.Dpi = 254F;
        this.tableCell171.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell171.Name = "tableCell171";
        this.tableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell171.StylePriority.UseBorders = false;
        this.tableCell171.StylePriority.UseFont = false;
        this.tableCell171.StylePriority.UsePadding = false;
        this.tableCell171.StylePriority.UseTextAlignment = false;
        this.tableCell171.Text = "Y - Desmanche de Box";
        this.tableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell171.Weight = 0.80225876085713743;
        // 
        // tableCell28
        // 
        this.tableCell28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell28.CanGrow = false;
        this.tableCell28.Dpi = 254F;
        this.tableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell28.Name = "tableCell28";
        this.tableCell28.StylePriority.UseBorders = false;
        this.tableCell28.StylePriority.UseFont = false;
        this.tableCell28.StylePriority.UseTextAlignment = false;
        this.tableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell28.Weight = 0.88167166861135238;
        this.tableCell28.WordWrap = false;
        // 
        // tableRow45
        // 
        this.tableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell115,
            this.tableCell117});
        this.tableRow45.Dpi = 254F;
        this.tableRow45.Name = "tableRow45";
        this.tableRow45.Weight = 1;
        // 
        // tableCell117
        // 
        this.tableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell117.Dpi = 254F;
        this.tableCell117.Name = "tableCell117";
        this.tableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell117.StylePriority.UseBorders = false;
        this.tableCell117.StylePriority.UsePadding = false;
        this.tableCell117.StylePriority.UseTextAlignment = false;
        this.tableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell117.Weight = 0.76914533807704522;
        this.tableCell117.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell111
        // 
        this.tableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell111.Dpi = 254F;
        this.tableCell111.Name = "tableCell111";
        this.tableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell111.StylePriority.UseBorders = false;
        this.tableCell111.StylePriority.UsePadding = false;
        this.tableCell111.StylePriority.UseTextAlignment = false;
        this.tableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell111.Weight = 0.76914533807704522;
        this.tableCell111.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // label16
        // 
        this.label16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label16.BorderWidth = 1;
        this.label16.CanGrow = false;
        this.label16.Dpi = 254F;
        this.label16.LocationFloat = new DevExpress.Utils.PointFloat(181F, 0F);
        this.label16.Name = "label16";
        this.label16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label16.SizeF = new System.Drawing.SizeF(48F, 10F);
        this.label16.StylePriority.UseBorders = false;
        this.label16.StylePriority.UseBorderWidth = false;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label13,
            this.label12,
            this.label11,
            this.label10,
            this.label9,
            this.label8,
            this.label7,
            this.label6,
            this.label5,
            this.label4,
            this.label3,
            this.panel2,
            this.table13,
            this.table12,
            this.table11,
            this.table10,
            this.table9,
            this.table8,
            this.table7,
            this.table6,
            this.table5,
            this.table4,
            this.table3,
            this.label1,
            this.xrPanel9,
            this.label2});
        this.PageHeader.Dpi = 254F;
        this.PageHeader.HeightF = 670F;
        this.PageHeader.Name = "PageHeader";
        // 
        // label13
        // 
        this.label13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label13.BorderWidth = 1;
        this.label13.CanGrow = false;
        this.label13.Dpi = 254F;
        this.label13.LocationFloat = new DevExpress.Utils.PointFloat(1876F, 660F);
        this.label13.Name = "label13";
        this.label13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label13.SizeF = new System.Drawing.SizeF(38F, 10F);
        this.label13.StylePriority.UseBorders = false;
        this.label13.StylePriority.UseBorderWidth = false;
        // 
        // label12
        // 
        this.label12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label12.BorderWidth = 1;
        this.label12.CanGrow = false;
        this.label12.Dpi = 254F;
        this.label12.LocationFloat = new DevExpress.Utils.PointFloat(1615F, 659.9999F);
        this.label12.Name = "label12";
        this.label12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label12.SizeF = new System.Drawing.SizeF(258F, 10F);
        this.label12.StylePriority.UseBorders = false;
        this.label12.StylePriority.UseBorderWidth = false;
        // 
        // label11
        // 
        this.label11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label11.BorderWidth = 1;
        this.label11.CanGrow = false;
        this.label11.Dpi = 254F;
        this.label11.LocationFloat = new DevExpress.Utils.PointFloat(1416F, 659.9999F);
        this.label11.Name = "label11";
        this.label11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label11.SizeF = new System.Drawing.SizeF(198F, 10F);
        this.label11.StylePriority.UseBorders = false;
        this.label11.StylePriority.UseBorderWidth = false;
        // 
        // label10
        // 
        this.label10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label10.BorderWidth = 1;
        this.label10.CanGrow = false;
        this.label10.Dpi = 254F;
        this.label10.LocationFloat = new DevExpress.Utils.PointFloat(1186F, 659.9999F);
        this.label10.Name = "label10";
        this.label10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label10.SizeF = new System.Drawing.SizeF(228F, 10F);
        this.label10.StylePriority.UseBorders = false;
        this.label10.StylePriority.UseBorderWidth = false;
        // 
        // label8
        // 
        this.label8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label8.BorderWidth = 1;
        this.label8.CanGrow = false;
        this.label8.Dpi = 254F;
        this.label8.LocationFloat = new DevExpress.Utils.PointFloat(596F, 659.9999F);
        this.label8.Name = "label8";
        this.label8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label8.SizeF = new System.Drawing.SizeF(478F, 10F);
        this.label8.StylePriority.UseBorders = false;
        this.label8.StylePriority.UseBorderWidth = false;
        // 
        // label7
        // 
        this.label7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label7.BorderWidth = 1;
        this.label7.CanGrow = false;
        this.label7.Dpi = 254F;
        this.label7.LocationFloat = new DevExpress.Utils.PointFloat(511F, 659.9999F);
        this.label7.Name = "label7";
        this.label7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label7.SizeF = new System.Drawing.SizeF(83F, 10F);
        this.label7.StylePriority.UseBorders = false;
        this.label7.StylePriority.UseBorderWidth = false;
        // 
        // label6
        // 
        this.label6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label6.BorderWidth = 1;
        this.label6.CanGrow = false;
        this.label6.Dpi = 254F;
        this.label6.LocationFloat = new DevExpress.Utils.PointFloat(231F, 659.9999F);
        this.label6.Name = "label6";
        this.label6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label6.SizeF = new System.Drawing.SizeF(278F, 10F);
        this.label6.StylePriority.UseBorders = false;
        this.label6.StylePriority.UseBorderWidth = false;
        // 
        // label3
        // 
        this.label3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.label3.BorderWidth = 1;
        this.label3.CanGrow = false;
        this.label3.Dpi = 254F;
        this.label3.LocationFloat = new DevExpress.Utils.PointFloat(1F, 659.9999F);
        this.label3.Name = "label3";
        this.label3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label3.SizeF = new System.Drawing.SizeF(38F, 10F);
        this.label3.StylePriority.UseBorders = false;
        this.label3.StylePriority.UseBorderWidth = false;
        // 
        // table11
        // 
        this.table11.Dpi = 254F;
        this.table11.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table11.LocationFloat = new DevExpress.Utils.PointFloat(772.2296F, 549.9999F);
        this.table11.Name = "table11";
        this.table11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow25,
            this.tableRow26});
        this.table11.SizeF = new System.Drawing.SizeF(270F, 60F);
        this.table11.StylePriority.UseFont = false;
        this.table11.StylePriority.UsePadding = false;
        this.table11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho10_BeforePrint);
        // 
        // tableRow25
        // 
        this.tableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell61});
        this.tableRow25.Dpi = 254F;
        this.tableRow25.Name = "tableRow25";
        this.tableRow25.Weight = 1;
        // 
        // table8
        // 
        this.table8.Dpi = 254F;
        this.table8.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 480F);
        this.table8.Name = "table8";
        this.table8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow20,
            this.tableRow21});
        this.table8.SizeF = new System.Drawing.SizeF(765F, 60F);
        this.table8.StylePriority.UseFont = false;
        this.table8.StylePriority.UsePadding = false;
        this.table8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho5_BeforePrint);
        // 
        // tableRow21
        // 
        this.tableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell49});
        this.tableRow21.Dpi = 254F;
        this.tableRow21.Name = "tableRow21";
        this.tableRow21.StylePriority.UseBorders = false;
        this.tableRow21.Weight = 1;
        // 
        // table7
        // 
        this.table7.Dpi = 254F;
        this.table7.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 550F);
        this.table7.Name = "table7";
        this.table7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow18,
            this.tableRow19});
        this.table7.SizeF = new System.Drawing.SizeF(765F, 60F);
        this.table7.StylePriority.UseFont = false;
        this.table7.StylePriority.UsePadding = false;
        this.table7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho9_BeforePrint);
        // 
        // tableRow18
        // 
        this.tableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell37,
            this.tableCell39,
            this.tableCell40});
        this.tableRow18.Dpi = 254F;
        this.tableRow18.Name = "tableRow18";
        this.tableRow18.Weight = 1;
        // 
        // tableCell39
        // 
        this.tableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell39.Dpi = 254F;
        this.tableCell39.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell39.Name = "tableCell39";
        this.tableCell39.StylePriority.UseBorders = false;
        this.tableCell39.StylePriority.UseFont = false;
        this.tableCell39.Text = "Agência";
        this.tableCell39.Weight = 1.4210526516563016;
        // 
        // tableCell40
        // 
        this.tableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell40.Dpi = 254F;
        this.tableCell40.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell40.Name = "tableCell40";
        this.tableCell40.StylePriority.UseBorders = false;
        this.tableCell40.StylePriority.UseFont = false;
        this.tableCell40.Text = "Conta Corrente";
        this.tableCell40.Weight = 1.1644736440558181;
        // 
        // table6
        // 
        this.table6.Dpi = 254F;
        this.table6.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.table6.LocationFloat = new DevExpress.Utils.PointFloat(1483.249F, 549.9999F);
        this.table6.Name = "table6";
        this.table6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow15,
            this.tableRow17});
        this.table6.SizeF = new System.Drawing.SizeF(430F, 60F);
        this.table6.StylePriority.UseFont = false;
        this.table6.StylePriority.UsePadding = false;
        this.table6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho12_BeforePrint);
        // 
        // table4
        // 
        this.table4.Dpi = 254F;
        this.table4.LocationFloat = new DevExpress.Utils.PointFloat(1483.249F, 340F);
        this.table4.Name = "table4";
        this.table4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.table4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow10,
            this.tableRow13,
            this.tableRow12,
            this.tableRow11});
        this.table4.SizeF = new System.Drawing.SizeF(430F, 130.0001F);
        this.table4.StylePriority.UsePadding = false;
        this.table4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Cabecalho4_BeforePrint);
        // 
        // tableRow10
        // 
        this.tableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell22});
        this.tableRow10.Dpi = 254F;
        this.tableRow10.Name = "tableRow10";
        this.tableRow10.Weight = 1;
        // 
        // tableRow13
        // 
        this.tableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell33});
        this.tableRow13.Dpi = 254F;
        this.tableRow13.Name = "tableRow13";
        this.tableRow13.Weight = 1;
        // 
        // tableRow12
        // 
        this.tableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell30,
            this.tableCell32});
        this.tableRow12.Dpi = 254F;
        this.tableRow12.Name = "tableRow12";
        this.tableRow12.Weight = 1;
        // 
        // tableCell30
        // 
        this.tableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell30.CanGrow = false;
        this.tableCell30.Dpi = 254F;
        this.tableCell30.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell30.Name = "tableCell30";
        this.tableCell30.StylePriority.UseBorders = false;
        this.tableCell30.StylePriority.UseFont = false;
        this.tableCell30.Text = "Código Cliente";
        this.tableCell30.Weight = 1.7668648653252181;
        this.tableCell30.WordWrap = false;
        // 
        // tableCell32
        // 
        this.tableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell32.CanGrow = false;
        this.tableCell32.Dpi = 254F;
        this.tableCell32.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.tableCell32.Name = "tableCell32";
        this.tableCell32.StylePriority.UseBorders = false;
        this.tableCell32.StylePriority.UseFont = false;
        this.tableCell32.Text = "Assessor";
        this.tableCell32.Weight = 1.2331351346747819;
        this.tableCell32.WordWrap = false;
        // 
        // tableRow11
        // 
        this.tableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell26,
            this.tableCell28,
            this.tableCell29});
        this.tableRow11.Dpi = 254F;
        this.tableRow11.Name = "tableRow11";
        this.tableRow11.Weight = 1;
        // 
        // tableCell26
        // 
        this.tableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell26.CanGrow = false;
        this.tableCell26.Dpi = 254F;
        this.tableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell26.Name = "tableCell26";
        this.tableCell26.StylePriority.UseBorders = false;
        this.tableCell26.StylePriority.UseFont = false;
        this.tableCell26.StylePriority.UseTextAlignment = false;
        this.tableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell26.Weight = 0.88519653235612583;
        this.tableCell26.WordWrap = false;
        // 
        // tableCell29
        // 
        this.tableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell29.CanGrow = false;
        this.tableCell29.Dpi = 254F;
        this.tableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.tableCell29.Name = "tableCell29";
        this.tableCell29.StylePriority.UseBorders = false;
        this.tableCell29.StylePriority.UseFont = false;
        this.tableCell29.StylePriority.UseTextAlignment = false;
        this.tableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.tableCell29.Weight = 1.2331317990325217;
        this.tableCell29.WordWrap = false;
        // 
        // table1
        // 
        this.table1.Dpi = 254F;
        this.table1.Font = new System.Drawing.Font("Times New Roman", 7F);
        this.table1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.table1.Name = "table1";
        this.table1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow1});
        this.table1.SizeF = new System.Drawing.SizeF(1915F, 30F);
        this.table1.StylePriority.UseFont = false;
        // 
        // table17
        // 
        this.table17.BackColor = System.Drawing.Color.Gainsboro;
        this.table17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.table17.Dpi = 254F;
        this.table17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.table17.Name = "table17";
        this.table17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow47});
        this.table17.SizeF = new System.Drawing.SizeF(950F, 30F);
        this.table17.StylePriority.UseBackColor = false;
        this.table17.StylePriority.UseBorders = false;
        this.table17.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer5_BeforePrint);
        // 
        // tableRow47
        // 
        this.tableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell81,
            this.tableCell119,
            this.tableCell131});
        this.tableRow47.Dpi = 254F;
        this.tableRow47.Name = "tableRow47";
        this.tableRow47.Weight = 0.47244094488188981;
        // 
        // tableCell81
        // 
        this.tableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell81.CanGrow = false;
        this.tableCell81.Dpi = 254F;
        this.tableCell81.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell81.Name = "tableCell81";
        this.tableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell81.StylePriority.UseBorders = false;
        this.tableCell81.StylePriority.UseFont = false;
        this.tableCell81.StylePriority.UsePadding = false;
        this.tableCell81.Text = "Líquido para data";
        this.tableCell81.Weight = 2.0787103421181818;
        this.tableCell81.WordWrap = false;
        // 
        // table15
        // 
        this.table15.Dpi = 254F;
        this.table15.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 15F);
        this.table15.Name = "table15";
        this.table15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow35,
            this.tableRow36,
            this.tableRow37,
            this.tableRow38,
            this.tableRow39,
            this.tableRow40,
            this.tableRow41,
            this.tableRow42,
            this.tableRow43,
            this.tableRow44,
            this.tableRow45});
        this.table15.SizeF = new System.Drawing.SizeF(950F, 380F);
        this.table15.StylePriority.UseFont = false;
        this.table15.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer1_BeforePrint);
        // 
        // tableRow35
        // 
        this.tableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell78});
        this.tableRow35.Dpi = 254F;
        this.tableRow35.Name = "tableRow35";
        this.tableRow35.Weight = 1;
        // 
        // tableCell78
        // 
        this.tableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell78.Dpi = 254F;
        this.tableCell78.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell78.Name = "tableCell78";
        this.tableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell78.StylePriority.UseBorders = false;
        this.tableCell78.StylePriority.UseFont = false;
        this.tableCell78.StylePriority.UsePadding = false;
        this.tableCell78.Text = "Resumo dos Negócios";
        this.tableCell78.Weight = 3;
        // 
        // tableRow36
        // 
        this.tableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell88,
            this.tableCell90});
        this.tableRow36.Dpi = 254F;
        this.tableRow36.Name = "tableRow36";
        this.tableRow36.Weight = 1;
        // 
        // tableCell88
        // 
        this.tableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell88.Dpi = 254F;
        this.tableCell88.Name = "tableCell88";
        this.tableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell88.StylePriority.UseBorders = false;
        this.tableCell88.StylePriority.UsePadding = false;
        this.tableCell88.Text = "Vendas à Vista";
        this.tableCell88.Weight = 2.2308546545870755;
        // 
        // tableCell90
        // 
        this.tableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell90.Dpi = 254F;
        this.tableCell90.Name = "tableCell90";
        this.tableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell90.StylePriority.UseBorders = false;
        this.tableCell90.StylePriority.UsePadding = false;
        this.tableCell90.StylePriority.UseTextAlignment = false;
        this.tableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell90.Weight = 0.76914534541292456;
        this.tableCell90.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow37
        // 
        this.tableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell91,
            this.tableCell93});
        this.tableRow37.Dpi = 254F;
        this.tableRow37.Name = "tableRow37";
        this.tableRow37.Weight = 1;
        // 
        // tableCell93
        // 
        this.tableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell93.Dpi = 254F;
        this.tableCell93.Name = "tableCell93";
        this.tableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell93.StylePriority.UseBorders = false;
        this.tableCell93.StylePriority.UsePadding = false;
        this.tableCell93.StylePriority.UseTextAlignment = false;
        this.tableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell93.Weight = 0.76914533807704522;
        this.tableCell93.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow38
        // 
        this.tableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell94,
            this.tableCell96});
        this.tableRow38.Dpi = 254F;
        this.tableRow38.Name = "tableRow38";
        this.tableRow38.Weight = 1;
        // 
        // tableCell94
        // 
        this.tableCell94.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell94.Dpi = 254F;
        this.tableCell94.Name = "tableCell94";
        this.tableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell94.StylePriority.UseBorders = false;
        this.tableCell94.StylePriority.UsePadding = false;
        this.tableCell94.Text = "Opções - compras";
        this.tableCell94.Weight = 2.2308546619229546;
        // 
        // tableCell96
        // 
        this.tableCell96.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell96.Dpi = 254F;
        this.tableCell96.Name = "tableCell96";
        this.tableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell96.StylePriority.UseBorders = false;
        this.tableCell96.StylePriority.UsePadding = false;
        this.tableCell96.StylePriority.UseTextAlignment = false;
        this.tableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell96.Weight = 0.76914533807704522;
        this.tableCell96.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow40
        // 
        this.tableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell100,
            this.tableCell102});
        this.tableRow40.Dpi = 254F;
        this.tableRow40.Name = "tableRow40";
        this.tableRow40.Weight = 1;
        // 
        // tableCell100
        // 
        this.tableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell100.Dpi = 254F;
        this.tableCell100.Name = "tableCell100";
        this.tableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell100.StylePriority.UseBorders = false;
        this.tableCell100.StylePriority.UsePadding = false;
        this.tableCell100.Text = "Compras à futuro";
        this.tableCell100.Weight = 2.2308546619229546;
        // 
        // tableRow41
        // 
        this.tableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell103,
            this.tableCell105});
        this.tableRow41.Dpi = 254F;
        this.tableRow41.Name = "tableRow41";
        this.tableRow41.Weight = 1;
        // 
        // tableCell105
        // 
        this.tableCell105.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell105.Dpi = 254F;
        this.tableCell105.Name = "tableCell105";
        this.tableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell105.StylePriority.UseBorders = false;
        this.tableCell105.StylePriority.UsePadding = false;
        this.tableCell105.StylePriority.UseTextAlignment = false;
        this.tableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell105.Weight = 0.76914533807704522;
        this.tableCell105.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow42
        // 
        this.tableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell106,
            this.tableCell108});
        this.tableRow42.Dpi = 254F;
        this.tableRow42.Name = "tableRow42";
        this.tableRow42.Weight = 1;
        // 
        // tableCell106
        // 
        this.tableCell106.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell106.Dpi = 254F;
        this.tableCell106.Name = "tableCell106";
        this.tableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell106.StylePriority.UseBorders = false;
        this.tableCell106.StylePriority.UsePadding = false;
        this.tableCell106.Text = "Ajuste do dia";
        this.tableCell106.Weight = 2.2308546619229546;
        // 
        // tableCell108
        // 
        this.tableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell108.Dpi = 254F;
        this.tableCell108.Name = "tableCell108";
        this.tableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell108.StylePriority.UseBorders = false;
        this.tableCell108.StylePriority.UsePadding = false;
        this.tableCell108.StylePriority.UseTextAlignment = false;
        this.tableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell108.Weight = 0.76914533807704522;
        this.tableCell108.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow43
        // 
        this.tableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell109,
            this.tableCell111});
        this.tableRow43.Dpi = 254F;
        this.tableRow43.Name = "tableRow43";
        this.tableRow43.Weight = 1;
        // 
        // tableRow50
        // 
        this.tableRow50.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell98,
            this.tableCell101,
            this.tableCell120});
        this.tableRow50.Dpi = 254F;
        this.tableRow50.Name = "tableRow50";
        this.tableRow50.StylePriority.UseBorders = false;
        this.tableRow50.Weight = 1.0001080866296379;
        // 
        // tableCell98
        // 
        this.tableCell98.Dpi = 254F;
        this.tableCell98.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell98.Name = "tableCell98";
        this.tableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell98.StylePriority.UseFont = false;
        this.tableCell98.StylePriority.UsePadding = false;
        this.tableCell98.Text = "Valor Líquido das operações";
        this.tableCell98.Weight = 2.07698807074394;
        // 
        // tableCell120
        // 
        this.tableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell120.CanGrow = false;
        this.tableCell120.Dpi = 254F;
        this.tableCell120.Name = "tableCell120";
        this.tableCell120.StylePriority.UseBorders = false;
        this.tableCell120.StylePriority.UseTextAlignment = false;
        this.tableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell120.Weight = 0.14792829631107013;
        this.tableCell120.WordWrap = false;
        this.tableCell120.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // panel4
        // 
        this.panel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.panel4.CanGrow = false;
        this.panel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table23,
            this.table20});
        this.panel4.Dpi = 254F;
        this.panel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 395F);
        this.panel4.Name = "panel4";
        this.panel4.SizeF = new System.Drawing.SizeF(950F, 245F);
        this.panel4.StylePriority.UseBorders = false;
        // 
        // table23
        // 
        this.table23.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table23.Dpi = 254F;
        this.table23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
        this.table23.Name = "table23";
        this.table23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.table23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow66});
        this.table23.SizeF = new System.Drawing.SizeF(619.125F, 27.16406F);
        this.table23.StylePriority.UseBorders = false;
        this.table23.StylePriority.UsePadding = false;
        // 
        // table20
        // 
        this.table20.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table20.Dpi = 254F;
        this.table20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.000039F);
        this.table20.Name = "table20";
        this.table20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.table20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow55});
        this.table20.SizeF = new System.Drawing.SizeF(388.6055F, 33F);
        this.table20.StylePriority.UseBorders = false;
        this.table20.StylePriority.UsePadding = false;
        // 
        // tableRow55
        // 
        this.tableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell137});
        this.tableRow55.Dpi = 254F;
        this.tableRow55.Name = "tableRow55";
        this.tableRow55.Weight = 1;
        // 
        // tableCell137
        // 
        this.tableCell137.CanGrow = false;
        this.tableCell137.Dpi = 254F;
        this.tableCell137.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell137.Name = "tableCell137";
        this.tableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell137.StylePriority.UseFont = false;
        this.tableCell137.StylePriority.UsePadding = false;
        this.tableCell137.Text = "Especificações Diversas";
        this.tableCell137.Weight = 3;
        this.tableCell137.WordWrap = false;
        // 
        // label23
        // 
        this.label23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label23.BorderWidth = 1;
        this.label23.CanGrow = false;
        this.label23.Dpi = 254F;
        this.label23.LocationFloat = new DevExpress.Utils.PointFloat(1615F, 0F);
        this.label23.Name = "label23";
        this.label23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label23.SizeF = new System.Drawing.SizeF(258F, 10F);
        this.label23.StylePriority.UseBorders = false;
        this.label23.StylePriority.UseBorderWidth = false;
        // 
        // label19
        // 
        this.label19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label19.BorderWidth = 1;
        this.label19.CanGrow = false;
        this.label19.Dpi = 254F;
        this.label19.LocationFloat = new DevExpress.Utils.PointFloat(596F, 0F);
        this.label19.Name = "label19";
        this.label19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label19.SizeF = new System.Drawing.SizeF(478F, 10F);
        this.label19.StylePriority.UseBorders = false;
        this.label19.StylePriority.UseBorderWidth = false;
        this.label19.WordWrap = false;
        // 
        // tableCell148
        // 
        this.tableCell148.BackColor = System.Drawing.Color.Transparent;
        this.tableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell148.CanGrow = false;
        this.tableCell148.Dpi = 254F;
        this.tableCell148.Name = "tableCell148";
        this.tableCell148.StylePriority.UseBackColor = false;
        this.tableCell148.StylePriority.UseBorders = false;
        this.tableCell148.StylePriority.UseTextAlignment = false;
        this.tableCell148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell148.Weight = 0.14792728778351849;
        this.tableCell148.WordWrap = false;
        this.tableCell148.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow73
        // 
        this.tableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell167,
            this.tableCell168,
            this.tableCell169});
        this.tableRow73.Dpi = 254F;
        this.tableRow73.Name = "tableRow73";
        this.tableRow73.Weight = 1;
        // 
        // tableCell167
        // 
        this.tableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell167.Dpi = 254F;
        this.tableCell167.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell167.Name = "tableCell167";
        this.tableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell167.StylePriority.UseBorders = false;
        this.tableCell167.StylePriority.UseFont = false;
        this.tableCell167.StylePriority.UsePadding = false;
        this.tableCell167.StylePriority.UseTextAlignment = false;
        this.tableCell167.Text = "D - Day-Trade";
        this.tableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell167.Weight = 1.3404389505821888;
        // 
        // tableCell169
        // 
        this.tableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.tableCell169.Dpi = 254F;
        this.tableCell169.Name = "tableCell169";
        this.tableCell169.StylePriority.UseBorders = false;
        this.tableCell169.Weight = 0.8573022885606737;
        // 
        // tableCell141
        // 
        this.tableCell141.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.tableCell141.Dpi = 254F;
        this.tableCell141.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell141.Name = "tableCell141";
        this.tableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell141.StylePriority.UseBorders = false;
        this.tableCell141.StylePriority.UseFont = false;
        this.tableCell141.StylePriority.UsePadding = false;
        this.tableCell141.StylePriority.UseTextAlignment = false;
        this.tableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell141.Weight = 0.775083985236358;
        this.tableCell141.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow72
        // 
        this.tableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell164,
            this.tableCell165,
            this.tableCell166});
        this.tableRow72.Dpi = 254F;
        this.tableRow72.Name = "tableRow72";
        this.tableRow72.Weight = 1;
        // 
        // tableCell164
        // 
        this.tableCell164.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell164.Dpi = 254F;
        this.tableCell164.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell164.Name = "tableCell164";
        this.tableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell164.StylePriority.UseBorders = false;
        this.tableCell164.StylePriority.UseFont = false;
        this.tableCell164.StylePriority.UsePadding = false;
        this.tableCell164.StylePriority.UseTextAlignment = false;
        this.tableCell164.Text = "8- Liquidação Institucional";
        this.tableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell164.Weight = 1.3404389505821888;
        // 
        // tableCell165
        // 
        this.tableCell165.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell165.Dpi = 254F;
        this.tableCell165.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell165.Name = "tableCell165";
        this.tableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell165.StylePriority.UseBorders = false;
        this.tableCell165.StylePriority.UseFont = false;
        this.tableCell165.StylePriority.UsePadding = false;
        this.tableCell165.StylePriority.UseTextAlignment = false;
        this.tableCell165.Text = "H - Home Broker";
        this.tableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell165.Weight = 0.80225876085713743;
        // 
        // tableRow67
        // 
        this.tableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell155,
            this.tableCell156,
            this.tableCell157});
        this.tableRow67.Dpi = 254F;
        this.tableRow67.Name = "tableRow67";
        this.tableRow67.Weight = 1;
        // 
        // tableCell157
        // 
        this.tableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell157.Dpi = 254F;
        this.tableCell157.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell157.Name = "tableCell157";
        this.tableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell157.StylePriority.UseBorders = false;
        this.tableCell157.StylePriority.UseFont = false;
        this.tableCell157.StylePriority.UsePadding = false;
        this.tableCell157.StylePriority.UseTextAlignment = false;
        this.tableCell157.Text = "T - Liquidação pelo Bruto";
        this.tableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell157.Weight = 0.85730236770294865;
        // 
        // tableRow61
        // 
        this.tableRow61.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell132,
            this.tableCell133,
            this.tableCell144});
        this.tableRow61.Dpi = 254F;
        this.tableRow61.Name = "tableRow61";
        this.tableRow61.StylePriority.UseBorders = false;
        this.tableRow61.Weight = 1;
        // 
        // tableCell132
        // 
        this.tableCell132.Dpi = 254F;
        this.tableCell132.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell132.Name = "tableCell132";
        this.tableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell132.StylePriority.UseFont = false;
        this.tableCell132.StylePriority.UsePadding = false;
        this.tableCell132.Text = "Corretagem";
        this.tableCell132.Weight = 2.0769883633974824;
        // 
        // tableCell133
        // 
        this.tableCell133.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell133.Dpi = 254F;
        this.tableCell133.Name = "tableCell133";
        this.tableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell133.StylePriority.UseBorders = false;
        this.tableCell133.StylePriority.UsePadding = false;
        this.tableCell133.StylePriority.UseTextAlignment = false;
        this.tableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell133.Weight = 0.77508327021042911;
        this.tableCell133.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // table18
        // 
        this.table18.Dpi = 254F;
        this.table18.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table18.LocationFloat = new DevExpress.Utils.PointFloat(960F, 15F);
        this.table18.Name = "table18";
        this.table18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow48,
            this.tableRow49,
            this.tableRow50,
            this.tableRow51,
            this.tableRow52,
            this.tableRow53});
        this.table18.SizeF = new System.Drawing.SizeF(950F, 200F);
        this.table18.StylePriority.UseFont = false;
        this.table18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer2_BeforePrint);
        // 
        // tableRow49
        // 
        this.tableRow49.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell92});
        this.tableRow49.Dpi = 254F;
        this.tableRow49.Name = "tableRow49";
        this.tableRow49.StylePriority.UseBorders = false;
        this.tableRow49.Weight = 1.0001080866296379;
        // 
        // tableCell92
        // 
        this.tableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell92.Dpi = 254F;
        this.tableCell92.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell92.Name = "tableCell92";
        this.tableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell92.StylePriority.UseBorders = false;
        this.tableCell92.StylePriority.UseFont = false;
        this.tableCell92.StylePriority.UsePadding = false;
        this.tableCell92.Text = "CBLC";
        this.tableCell92.Weight = 3;
        // 
        // tableRow51
        // 
        this.tableRow51.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell104,
            this.tableCell107,
            this.tableCell121});
        this.tableRow51.Dpi = 254F;
        this.tableRow51.Name = "tableRow51";
        this.tableRow51.StylePriority.UseBorders = false;
        this.tableRow51.Weight = 1.0001080866296379;
        // 
        // tableRow52
        // 
        this.tableRow52.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell110,
            this.tableCell113,
            this.tableCell122});
        this.tableRow52.Dpi = 254F;
        this.tableRow52.Name = "tableRow52";
        this.tableRow52.StylePriority.UseBorders = false;
        this.tableRow52.Weight = 1.00010816516887;
        // 
        // tableCell113
        // 
        this.tableCell113.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell113.Dpi = 254F;
        this.tableCell113.Name = "tableCell113";
        this.tableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell113.StylePriority.UseBorders = false;
        this.tableCell113.StylePriority.UsePadding = false;
        this.tableCell113.StylePriority.UseTextAlignment = false;
        this.tableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell113.Weight = 0.7750836329449895;
        this.tableCell113.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell122
        // 
        this.tableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell122.CanGrow = false;
        this.tableCell122.Dpi = 254F;
        this.tableCell122.Name = "tableCell122";
        this.tableCell122.StylePriority.UseBorders = false;
        this.tableCell122.StylePriority.UseTextAlignment = false;
        this.tableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell122.Weight = 0.14792829631107013;
        this.tableCell122.WordWrap = false;
        this.tableCell122.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // PageFooter
        // 
        this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel1});
        this.PageFooter.Dpi = 254F;
        this.PageFooter.HeightF = 970F;
        this.PageFooter.Name = "PageFooter";
        // 
        // panel1
        // 
        this.panel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.panel1.CanGrow = false;
        this.panel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel3,
            this.table19,
            this.table24,
            this.table25,
            this.table22,
            this.table18,
            this.panel4,
            this.table15,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label20,
            this.label21,
            this.label22,
            this.label23,
            this.label24,
            this.label19});
        this.panel1.Dpi = 254F;
        this.panel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.panel1.Name = "panel1";
        this.panel1.SizeF = new System.Drawing.SizeF(1915F, 960F);
        this.panel1.StylePriority.UseBorders = false;
        // 
        // panel3
        // 
        this.panel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.panel3.CanGrow = false;
        this.panel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table17,
            this.table16,
            this.line1,
            this.table21});
        this.panel3.Dpi = 254F;
        this.panel3.LocationFloat = new DevExpress.Utils.PointFloat(960F, 640F);
        this.panel3.Name = "panel3";
        this.panel3.SizeF = new System.Drawing.SizeF(950F, 250F);
        this.panel3.StylePriority.UseBorders = false;
        // 
        // table21
        // 
        this.table21.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.table21.Dpi = 254F;
        this.table21.LocationFloat = new DevExpress.Utils.PointFloat(120.8389F, 195.0001F);
        this.table21.Name = "table21";
        this.table21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow68});
        this.table21.SizeF = new System.Drawing.SizeF(762F, 30.00002F);
        this.table21.StylePriority.UseBorders = false;
        this.table21.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer6_BeforePrint);
        // 
        // tableRow68
        // 
        this.tableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell142});
        this.tableRow68.Dpi = 254F;
        this.tableRow68.Name = "tableRow68";
        this.tableRow68.Weight = 0.96477488193520278;
        // 
        // table19
        // 
        this.table19.Dpi = 254F;
        this.table19.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.table19.LocationFloat = new DevExpress.Utils.PointFloat(960F, 395F);
        this.table19.Name = "table19";
        this.table19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow59,
            this.tableRow60,
            this.tableRow61,
            this.tableRow62,
            this.tableRow63,
            this.tableRow64,
            this.tableRow65});
        this.table19.SizeF = new System.Drawing.SizeF(950F, 245F);
        this.table19.StylePriority.UseFont = false;
        this.table19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Footer4_BeforePrint);
        // 
        // tableRow59
        // 
        this.tableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell129});
        this.tableRow59.Dpi = 254F;
        this.tableRow59.Name = "tableRow59";
        this.tableRow59.Weight = 1;
        // 
        // tableRow60
        // 
        this.tableRow60.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell130});
        this.tableRow60.Dpi = 254F;
        this.tableRow60.Name = "tableRow60";
        this.tableRow60.StylePriority.UseBorders = false;
        this.tableRow60.Weight = 1;
        // 
        // tableRow62
        // 
        this.tableRow62.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell134,
            this.tableCell135,
            this.tableCell145});
        this.tableRow62.Dpi = 254F;
        this.tableRow62.Name = "tableRow62";
        this.tableRow62.StylePriority.UseBorders = false;
        this.tableRow62.Weight = 1;
        // 
        // tableCell134
        // 
        this.tableCell134.CanGrow = false;
        this.tableCell134.Dpi = 254F;
        this.tableCell134.Font = new System.Drawing.Font("Times New Roman", 7.5F);
        this.tableCell134.Name = "tableCell134";
        this.tableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell134.StylePriority.UseFont = false;
        this.tableCell134.StylePriority.UsePadding = false;
        this.tableCell134.Text = "ISS (SÃO PAULO)";
        this.tableCell134.Weight = 2.0769883633974824;
        this.tableCell134.WordWrap = false;
        // 
        // tableCell135
        // 
        this.tableCell135.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.tableCell135.Dpi = 254F;
        this.tableCell135.Name = "tableCell135";
        this.tableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
        this.tableCell135.StylePriority.UseBorders = false;
        this.tableCell135.StylePriority.UsePadding = false;
        this.tableCell135.StylePriority.UseTextAlignment = false;
        this.tableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.tableCell135.Weight = 0.77508327021042911;
        this.tableCell135.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableCell145
        // 
        this.tableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.tableCell145.CanGrow = false;
        this.tableCell145.Dpi = 254F;
        this.tableCell145.Name = "tableCell145";
        this.tableCell145.StylePriority.UseBorders = false;
        this.tableCell145.StylePriority.UseTextAlignment = false;
        this.tableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.tableCell145.Weight = 0.147928366392089;
        this.tableCell145.WordWrap = false;
        this.tableCell145.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow63
        // 
        this.tableRow63.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.tableCell136,
            this.tableCell182});
        this.tableRow63.Dpi = 254F;
        this.tableRow63.Name = "tableRow63";
        this.tableRow63.StylePriority.UseBorders = false;
        this.tableRow63.Weight = 1;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.CanGrow = false;
        this.xrTableCell3.Dpi = 254F;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.xrTableCell3.StylePriority.UsePadding = false;
        this.xrTableCell3.Weight = 2.078710452412118;
        this.xrTableCell3.WordWrap = false;
        this.xrTableCell3.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.Footer_PrintOnPage);
        // 
        // tableRow65
        // 
        this.tableRow65.BackColor = System.Drawing.Color.Gainsboro;
        this.tableRow65.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell140,
            this.tableCell141,
            this.tableCell148});
        this.tableRow65.Dpi = 254F;
        this.tableRow65.Name = "tableRow65";
        this.tableRow65.StylePriority.UseBackColor = false;
        this.tableRow65.StylePriority.UseBorders = false;
        this.tableRow65.Weight = 1;
        // 
        // tableCell140
        // 
        this.tableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.tableCell140.Dpi = 254F;
        this.tableCell140.Font = new System.Drawing.Font("Times New Roman", 7.5F, System.Drawing.FontStyle.Bold);
        this.tableCell140.Name = "tableCell140";
        this.tableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell140.StylePriority.UseBorders = false;
        this.tableCell140.StylePriority.UseFont = false;
        this.tableCell140.StylePriority.UsePadding = false;
        this.tableCell140.Text = "Total corretagem / Despesas";
        this.tableCell140.Weight = 2.0769887269801242;
        // 
        // table24
        // 
        this.table24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
        this.table24.Dpi = 254F;
        this.table24.Font = new System.Drawing.Font("Times New Roman", 6F);
        this.table24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 640F);
        this.table24.Name = "table24";
        this.table24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow67,
            this.tableRow70,
            this.tableRow71,
            this.tableRow72,
            this.tableRow73,
            this.tableRow74,
            this.tableRow75});
        this.table24.SizeF = new System.Drawing.SizeF(950.0001F, 250F);
        this.table24.StylePriority.UseBorders = false;
        this.table24.StylePriority.UseFont = false;
        // 
        // tableRow70
        // 
        this.tableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell158,
            this.tableCell159,
            this.tableCell160});
        this.tableRow70.Dpi = 254F;
        this.tableRow70.Name = "tableRow70";
        this.tableRow70.Weight = 1;
        // 
        // tableCell158
        // 
        this.tableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.tableCell158.CanGrow = false;
        this.tableCell158.Dpi = 254F;
        this.tableCell158.Font = new System.Drawing.Font("Times New Roman", 5F);
        this.tableCell158.Name = "tableCell158";
        this.tableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254F);
        this.tableCell158.StylePriority.UseBorders = false;
        this.tableCell158.StylePriority.UseFont = false;
        this.tableCell158.StylePriority.UsePadding = false;
        this.tableCell158.StylePriority.UseTextAlignment = false;
        this.tableCell158.Text = "2 - Corretora ou pessoa Vinculada atuou na contra parte";
        this.tableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.tableCell158.Weight = 1.3404389505821888;
        this.tableCell158.WordWrap = false;
        // 
        // tableRow74
        // 
        this.tableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell170,
            this.tableCell171,
            this.tableCell172});
        this.tableRow74.Dpi = 254F;
        this.tableRow74.Name = "tableRow74";
        this.tableRow74.Weight = 0.92399996948242191;
        // 
        // label14
        // 
        this.label14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label14.BorderWidth = 1;
        this.label14.CanGrow = false;
        this.label14.Dpi = 254F;
        this.label14.LocationFloat = new DevExpress.Utils.PointFloat(1F, 0F);
        this.label14.Name = "label14";
        this.label14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label14.SizeF = new System.Drawing.SizeF(38F, 10F);
        this.label14.StylePriority.UseBorders = false;
        this.label14.StylePriority.UseBorderWidth = false;
        // 
        // label15
        // 
        this.label15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label15.BorderWidth = 1;
        this.label15.CanGrow = false;
        this.label15.Dpi = 254F;
        this.label15.LocationFloat = new DevExpress.Utils.PointFloat(41F, 0F);
        this.label15.Name = "label15";
        this.label15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label15.SizeF = new System.Drawing.SizeF(138F, 10F);
        this.label15.StylePriority.UseBorders = false;
        this.label15.StylePriority.UseBorderWidth = false;
        // 
        // label17
        // 
        this.label17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label17.BorderWidth = 1;
        this.label17.CanGrow = false;
        this.label17.Dpi = 254F;
        this.label17.LocationFloat = new DevExpress.Utils.PointFloat(231F, 0F);
        this.label17.Name = "label17";
        this.label17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label17.SizeF = new System.Drawing.SizeF(278F, 10F);
        this.label17.StylePriority.UseBorders = false;
        this.label17.StylePriority.UseBorderWidth = false;
        // 
        // label18
        // 
        this.label18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label18.BorderWidth = 1;
        this.label18.CanGrow = false;
        this.label18.Dpi = 254F;
        this.label18.LocationFloat = new DevExpress.Utils.PointFloat(511F, 0F);
        this.label18.Name = "label18";
        this.label18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label18.SizeF = new System.Drawing.SizeF(83F, 10F);
        this.label18.StylePriority.UseBorders = false;
        this.label18.StylePriority.UseBorderWidth = false;
        // 
        // label21
        // 
        this.label21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label21.BorderWidth = 1;
        this.label21.CanGrow = false;
        this.label21.Dpi = 254F;
        this.label21.LocationFloat = new DevExpress.Utils.PointFloat(1186F, 0F);
        this.label21.Name = "label21";
        this.label21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label21.SizeF = new System.Drawing.SizeF(228F, 10F);
        this.label21.StylePriority.UseBorders = false;
        this.label21.StylePriority.UseBorderWidth = false;
        // 
        // label22
        // 
        this.label22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label22.BorderWidth = 1;
        this.label22.CanGrow = false;
        this.label22.Dpi = 254F;
        this.label22.LocationFloat = new DevExpress.Utils.PointFloat(1416F, 0F);
        this.label22.Name = "label22";
        this.label22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label22.SizeF = new System.Drawing.SizeF(198F, 10F);
        this.label22.StylePriority.UseBorders = false;
        this.label22.StylePriority.UseBorderWidth = false;
        // 
        // label24
        // 
        this.label24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.label24.BorderWidth = 1;
        this.label24.CanGrow = false;
        this.label24.Dpi = 254F;
        this.label24.LocationFloat = new DevExpress.Utils.PointFloat(1876F, 0F);
        this.label24.Name = "label24";
        this.label24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.label24.SizeF = new System.Drawing.SizeF(38F, 10F);
        this.label24.StylePriority.UseBorders = false;
        this.label24.StylePriority.UseBorderWidth = false;
        // 
        // TopMargin
        // 
        this.TopMargin.Dpi = 254F;
        this.TopMargin.HeightF = 109F;
        this.TopMargin.Name = "TopMargin";
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table1});
        this.Detail.Dpi = 254F;
        this.Detail.HeightF = 30F;
        this.Detail.KeepTogether = true;
        this.Detail.Name = "Detail";
        // 
        // xrControlStyle1
        // 
        this.xrControlStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
        this.xrControlStyle1.Name = "xrControlStyle1";
        this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        // 
        // xrControlStyle2
        // 
        this.xrControlStyle2.BackColor = System.Drawing.Color.Empty;
        this.xrControlStyle2.Name = "xrControlStyle2";
        this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        // 
        // ReportNotaCorretagemBolsa
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.Detail,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
        this.DesignerOptions.ShowExportWarnings = false;
        this.Dpi = 254F;
        this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.Margins = new System.Drawing.Printing.Margins(119, 119, 109, 150);
        this.PageHeight = 2794;
        this.PageWidth = 2159;
        this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
        this.SnapGridSize = 31.75F;
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
        this.Version = "11.1";
        ((System.ComponentModel.ISupportInitialize)(this.table2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table10)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table16)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table12)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table14)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table13)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table9)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table22)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table25)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table11)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table8)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table7)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table17)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table15)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table23)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table20)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table18)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table21)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table19)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.table24)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion

    // region privates para calcular Liquidoda Nota
    private decimal totalResumo = 0.00M;
    private decimal totalBovespaSoma = 0.00M;
    private decimal totalCorretagem = 0.00M;

    #region Funções Internas

    #region Header

    private void Cabecalho1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;
        
        XRTableRow row1 = table.Rows[1];
        ((XRTableCell)row1.Cells[0]).Text = dadosNota.GetHeader.numeroNota.HasValue ? dadosNota.GetHeader.numeroNota.Value.ToString() : "";
        ((XRTableCell)row1.Cells[2]).Text = dadosNota.GetHeader.dataPregao.ToString("dd/MM/yyyy");
    }

    private void Cabecalho2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row1 = table.Rows[0];
        XRTableRow row2 = table.Rows[1];
        XRTableRow row3 = table.Rows[2];
        XRTableRow row4 = table.Rows[3];
        XRTableRow row5 = table.Rows[4];
        XRTableRow row6 = table.Rows[5];
        //                                              
        ((XRTableCell)row1.Cells[0]).Text = dadosNota.GetHeader.nomeAgente;
        //
        ((XRTableCell)row2.Cells[0]).Text = dadosNota.GetHeader.enderecoCompletoAgente;
        //
        ((XRTableCell)row3.Cells[1]).Text = dadosNota.GetHeader.telefoneCompletoAgente;
        ((XRTableCell)row3.Cells[3]).Text = dadosNota.GetHeader.faxAgente;
        //
        ((XRTableCell)row4.Cells[1]).Text = dadosNota.GetHeader.siteAgente;
        ((XRTableCell)row4.Cells[3]).Text = dadosNota.GetHeader.emailAgente;
        //        
        ((XRTableCell)row5.Cells[1]).Text = dadosNota.GetHeader.cnpjAgente;
        ((XRTableCell)row5.Cells[3]).Text = dadosNota.GetHeader.cartaPatente;
        //
        ((XRTableCell)row6.Cells[1]).Text = dadosNota.GetHeader.telefoneOuvidoria;
        ((XRTableCell)row6.Cells[3]).Text = dadosNota.GetHeader.emailOuvidoria;
    }

    private void Cabecalho3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row1 = table.Rows[0];
        XRTableRow row2 = table.Rows[1];
        XRTableRow row3 = table.Rows[2];
        XRTableRow row4 = table.Rows[3];

        string dvCliente = "";
        string codigoBovespaAgenteSemDV = dadosNota.GetHeader.codigoBovespaAgente;
        if (dadosNota.GetHeader.codigoBovespaAgente.Contains("-") && dadosNota.GetHeader.codigoBovespaAgente.Length > 2)
        {
            codigoBovespaAgenteSemDV = dadosNota.GetHeader.codigoBovespaAgente.Substring(0, dadosNota.GetHeader.codigoBovespaAgente.Length - 2);
        }

        if (Utilitario.IsInteger(codigoBovespaAgenteSemDV) && Utilitario.IsInteger(dadosNota.GetHeader.codigoCliente))
        {            
            dvCliente = "-" + Utilitario.CalculaDV(Convert.ToInt32(dadosNota.GetHeader.codigoCliente), Convert.ToInt32(codigoBovespaAgenteSemDV));
        }

        ((XRTableCell)row2.Cells[0]).Text = dadosNota.GetHeader.codigoCliente.PadLeft(7, '0') + dvCliente;

        ((XRTableCell)row2.Cells[1]).Text = dadosNota.GetHeader.nomeCliente;
        //
        ((XRTableCell)row3.Cells[1]).Text = dadosNota.GetHeader.enderecoCliente + " " + dadosNota.GetHeader.bairroCliente;
        ((XRTableCell)row3.Cells[2]).Text = dadosNota.GetHeader.telefoneCliente;
        //
        ((XRTableCell)row4.Cells[1]).Text = dadosNota.GetHeader.cepCliente;
        ((XRTableCell)row4.Cells[2]).Text = dadosNota.GetHeader.cidadeCliente; // já vem cidade-estado
    }

    private void Cabecalho4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row1 = table.Rows[0];
        XRTableRow row2 = table.Rows[1];
        XRTableRow row3 = table.Rows[2];
        XRTableRow row4 = table.Rows[3];

        ((XRTableCell)row2.Cells[0]).Text = dadosNota.GetHeader.cpfCNPJCliente;
        //

        // TODO - verificar codigo duplo
        ((XRTableCell)row4.Cells[0]).Text = dadosNota.GetHeader.codigoBovespaAgente;

        string dvCliente = "";
        string codigoBovespaAgenteSemDV = dadosNota.GetHeader.codigoBovespaAgente;
        if (dadosNota.GetHeader.codigoBovespaAgente.Contains("-") && dadosNota.GetHeader.codigoBovespaAgente.Length > 2)
        {
            codigoBovespaAgenteSemDV = dadosNota.GetHeader.codigoBovespaAgente.Substring(0, dadosNota.GetHeader.codigoBovespaAgente.Length - 2);
        }

        if (Utilitario.IsInteger(codigoBovespaAgenteSemDV) && Utilitario.IsInteger(dadosNota.GetHeader.codigoCliente))
        {
            dvCliente = "-" + Utilitario.CalculaDV(Convert.ToInt32(dadosNota.GetHeader.codigoCliente), Convert.ToInt32(codigoBovespaAgenteSemDV));
        }

        ((XRTableCell)row4.Cells[1]).Text = dadosNota.GetHeader.codigoCliente + dvCliente;

        ((XRTableCell)row4.Cells[2]).Text = dadosNota.GetHeader.codigoAssessor;
    }

    private void Cabecalho5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];
        //
        //((XRTableCell)row2.Cells[0]).Text = "agente";
    }

    private void Cabecalho6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];
        //
        //((XRTableCell)row2.Cells[0]).Text = "cliente";
    }

    private void Cabecalho7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];
        //
        ((XRTableCell)row2.Cells[1]).Text = "0,00";
        ((XRTableCell)row2.Cells[2]).Text = "C";
    }

    private void Cabecalho8_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row1 = table.Rows[0];
        XRTableRow row2 = table.Rows[1];

        ((XRTableCell)row1.Cells[1]).Text = "C.I.";
        //
        //((XRTableCell)row2.Cells[0]).Text = "Custodiante";
        ((XRTableCell)row2.Cells[1]).Text = "N";
    }

    private void Cabecalho9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];

        ContaCorrenteCollection c = new ContaCorrenteCollection();
        //
        c.Query.Select(c.Query.IdConta, c.Query.IdAgencia, c.Query.IdBanco, c.Query.Numero)
               .Where(c.Query.IdPessoa == dadosNota.IdCliente,
                      c.Query.ContaDefault == "N");

        if (c.Query.Load()) {
            ((XRTableCell)row2.Cells[2]).Text = c[0].str.Numero.Trim();

            int? idAgencia = c[0].IdAgencia;
            int? idBanco = c[0].IdBanco;

            if (idAgencia.HasValue) {
                Agencia agencia = new Agencia();
                if (agencia.LoadByPrimaryKey(idAgencia.Value)) {
                    ((XRTableCell)row2.Cells[1]).Text = agencia.str.Codigo.Trim();
                }
            }

            if (idBanco.HasValue) {
                Banco banco = new Banco();
                if (banco.LoadByPrimaryKey(idBanco.Value)) {
                    ((XRTableCell)row2.Cells[0]).Text = banco.str.CodigoCompensacao.Trim();
                }
            }
        }
    }

    private void Cabecalho10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];
        //((XRTableCell)row2.Cells[0]).Text = "Acionista";
    }

    private void Cabecalho11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row2 = table.Rows[1];
        //((XRTableCell)row2.Cells[0]).Text = "Administrador";
    }

    private void Cabecalho12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        XRTableRow row1 = table.Rows[0];
        XRTableRow row2 = table.Rows[1];

        ((XRTableCell)row1.Cells[1]).Text = "P. Vinc";
        //
        ((XRTableCell)row2.Cells[0]).Text = "";
        ((XRTableCell)row2.Cells[1]).Text = dadosNota.GetHeader.pessoaVinculada;
    }
  
    #endregion

    #region Footer
    private void Footer1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        // Muda os Labels - labels Diferentes
        if (this.dadosNota.TipoMercado == TipoMercadoBolsa.MercadoVista || this.dadosNota.TipoMercado == TipoMercadoBolsa.Termo ||
            this.dadosNota.TipoMercado == TipoMercadoBolsa.Imobiliario || this.dadosNota.TipoMercado == "VIS_TER_IMO") {
            ((XRTableCell)table.Rows[1].Cells[0]).Text = "Debêntures";
            ((XRTableCell)table.Rows[2].Cells[0]).Text = "Vendas à Vista";
            ((XRTableCell)table.Rows[3].Cells[0]).Text = "Compras a Vista";
            ((XRTableCell)table.Rows[4].Cells[0]).Text = "Opções - Compras";
            ((XRTableCell)table.Rows[5].Cells[0]).Text = "Opções - Vendas";
            ((XRTableCell)table.Rows[6].Cells[0]).Text = "Operações à Termo";
            ((XRTableCell)table.Rows[7].Cells[0]).Text = "Valor das oper. c/ títulos públ. (v.nom.)";
            ((XRTableCell)table.Rows[8].Cells[0]).Text = "Valor da Operações";
            ((XRTableCell)table.Rows[9].Cells[0]).Text = "";
            ((XRTableCell)table.Rows[10].Cells[0]).Text = "";
            //
            ((XRTableCell)table.Rows[9].Cells[1]).Text = "";
            ((XRTableCell)table.Rows[10].Cells[1]).Text = "";
            /*--------------------------------------------------------------------------------------------*/

            ((XRTableCell)table.Rows[1].Cells[1]).Text = "0,00"; // Debêntures
            ((XRTableCell)table.Rows[2].Cells[1]).Text = dadosNota.GetFooter.vendasVista.ToString("N2");  // Vendas à Vista
            ((XRTableCell)table.Rows[3].Cells[1]).Text = dadosNota.GetFooter.comprasVista.ToString("N2"); // Compras a Vista
            ((XRTableCell)table.Rows[4].Cells[1]).Text = "0,00"; // Opções - Compras
            ((XRTableCell)table.Rows[5].Cells[1]).Text = "0,00"; // Opções - Vendas
            ((XRTableCell)table.Rows[6].Cells[1]).Text = dadosNota.GetFooter.valorTermo.ToString("N2"); // Operações à Termo
            ((XRTableCell)table.Rows[7].Cells[1]).Text = "0,00"; // Valor das oper. c/ títulos públ. (v.nom.)            
            //
            decimal total = dadosNota.GetFooter.vendasVista + dadosNota.GetFooter.comprasVista + dadosNota.GetFooter.valorTermo;
            ((XRTableCell)table.Rows[8].Cells[1]).Text = total.ToString("N2"); // Valor da Operações
        }
        else { // OPC - OPV - OPC_OPV
            ((XRTableCell)table.Rows[1].Cells[1]).Text = "0,00";  //Vendas Vista
            ((XRTableCell)table.Rows[2].Cells[1]).Text = "0,00";  //Compras Vista
            ((XRTableCell)table.Rows[3].Cells[1]).Text = dadosNota.GetFooter.comprasOpcoes.ToString("N2");  //Opções Compras
            ((XRTableCell)table.Rows[4].Cells[1]).Text = dadosNota.GetFooter.vendasOpcoes.ToString("N2");   //Opções Vendas
            ((XRTableCell)table.Rows[5].Cells[1]).Text = "0,00";  //Compras Futuro
            ((XRTableCell)table.Rows[6].Cells[1]).Text = "0,00";  //Vendas Futuro
            ((XRTableCell)table.Rows[7].Cells[1]).Text = "0,00";  //Ajuste Dia
            ((XRTableCell)table.Rows[8].Cells[1]).Text = "0,00";  //Ajuste Posicao
            ((XRTableCell)table.Rows[9].Cells[1]).Text = "0,00";  //Ajuste DayTrade  
            //
            decimal total = dadosNota.GetFooter.comprasOpcoes + dadosNota.GetFooter.vendasOpcoes;
            ((XRTableCell)table.Rows[10].Cells[1]).Text = total.ToString("N2"); //Valor Operacoes
        }
    }

    private void Footer2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        decimal valorLiquido = dadosNota.TipoMercado == TipoMercadoBolsa.OpcaoCompra || 
                               dadosNota.TipoMercado == TipoMercadoBolsa.OpcaoVenda ||
                               dadosNota.TipoMercado == "OPC_OPV"
                            ? dadosNota.GetFooter.vendasOpcoes - dadosNota.GetFooter.comprasOpcoes
                            : dadosNota.GetFooter.vendasVista - dadosNota.GetFooter.comprasVista;
        //
        //
        decimal absoluto = Math.Abs(valorLiquido);
        ((XRTableCell)table.Rows[2].Cells[1]).Text = absoluto.ToString("N2");
        //                
        //
        ((XRTableCell)table.Rows[3].Cells[1]).Text = dadosNota.GetFooter.liquidacaoCBLC.ToString("N2"); // Taxa de Liquidação
        //
        //
        decimal taxaRegistro = dadosNota.GetFooter.registroCBLC + dadosNota.GetFooter.registroBolsa;
        ((XRTableCell)table.Rows[4].Cells[1]).Text = taxaRegistro.ToString("N2"); // Taxa de Registro
        //
        //

        decimal txLiquidacaoCBLC = dadosNota.GetFooter.liquidacaoCBLC * -1;
        decimal txRegistroCBLC = dadosNota.GetFooter.registroCBLC * -1;
        decimal txRegistroBolsa = dadosNota.GetFooter.registroBolsa * -1;

        decimal taxas = txLiquidacaoCBLC + txRegistroCBLC + txRegistroBolsa;
        decimal total = valorLiquido + taxas;

        decimal absTotal = Math.Abs(total);
        ((XRTableCell)table.Rows[5].Cells[1]).Text = absTotal.ToString("N2");
        //
        /* ----------------------------------------------------------------------- */
        /* C/D */
        ((XRTableCell)table.Rows[2].Cells[2]).Text = valorLiquido >=0 ? "C" : "D";
        ((XRTableCell)table.Rows[3].Cells[2]).Text = "D";
        ((XRTableCell)table.Rows[4].Cells[2]).Text = "D";
        ((XRTableCell)table.Rows[5].Cells[2]).Text = total >= 0 ? "C" : "D";

        /* ------------------------------------------------------------------*/
        // Armazena Total
        this.totalResumo = total;

    }

    private void Footer3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        if (this.dadosNota.TipoMercado == TipoMercadoBolsa.MercadoVista || this.dadosNota.TipoMercado == TipoMercadoBolsa.Termo ||
            this.dadosNota.TipoMercado == TipoMercadoBolsa.Imobiliario || this.dadosNota.TipoMercado == "VIS_TER_IMO") {

            ((XRTableCell)table.Rows[1].Cells[0]).Text = "Taxa de Termo/Opções";
        }
        else { // OPC - OPV - OPC_OPV
            ((XRTableCell)table.Rows[1].Cells[0]).Text = "Taxa de Opções/Futuro";
        }

        decimal taxaOpcoes = 0.00M, taxaANA = 0.00M;
        ((XRTableCell)table.Rows[1].Cells[1]).Text = taxaOpcoes.ToString("N2"); // TaxaOpcoes
        ((XRTableCell)table.Rows[2].Cells[1]).Text = taxaANA.ToString("N2"); // Taxa A.N.A   
        ((XRTableCell)table.Rows[3].Cells[1]).Text = dadosNota.GetFooter.emolumentos.ToString("N2"); // Emolumentos
        //
        //
        decimal total = dadosNota.GetFooter.emolumentos + taxaOpcoes + taxaANA;
        ((XRTableCell)table.Rows[4].Cells[1]).Text = total.ToString("N2"); // Total Bovespa/Soma
        //
        ((XRTableCell)table.Rows[1].Cells[2]).Text = "D";
        ((XRTableCell)table.Rows[2].Cells[2]).Text = "D";
        ((XRTableCell)table.Rows[3].Cells[2]).Text = "D";
        ((XRTableCell)table.Rows[4].Cells[2]).Text = "D";


        /* ------------------------------------------------------------------*/
        // Armazena Total
        this.totalBovespaSoma = total * -1;
    }

    private void Footer4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        bool cobraIss = this.CobraIss();
        //
        //
        XRTable table = sender as XRTable;

        ((XRTableCell)table.Rows[3].Cells[0]).Text = "ISS";
        if(!String.IsNullOrEmpty(dadosNota.GetFooter.cidadeAdministrador)) {
            ((XRTableCell)table.Rows[3].Cells[0]).Text += " (" + dadosNota.GetFooter.cidadeAdministrador.ToUpper() + ")";
        }

        decimal corretagemAux = dadosNota.GetFooter.corretagem;
        if (cobraIss)
        {
            corretagemAux -= dadosNota.GetFooter.iss;
        }

        ((XRTableCell)table.Rows[2].Cells[1]).Text = corretagemAux.ToString("N2");
        ((XRTableCell)table.Rows[3].Cells[1]).Text = dadosNota.GetFooter.iss.ToString("N2");        
        //

        /* ------------------------------------------------------------------*/
        decimal valorIRCelula = 0; // Armazena Valor colocado na célula para Cálculo do Total
        //
        // IR Sob Operações Base
        if (dadosNota.TipoMercado == "VIS_TER_IMO" ||
            dadosNota.TipoMercado == TipoMercadoBolsa.MercadoVista ||
            dadosNota.TipoMercado == TipoMercadoBolsa.Termo ||
            dadosNota.TipoMercado == TipoMercadoBolsa.Imobiliario) {

            #region IrFonte Acoes
            IRFonte ir = new IRFonte();
            ir.Query.Select(ir.Query.ValorBase.Sum(), ir.Query.ValorIR.Sum())
                    .Where(ir.Query.IdCliente == this.dadosNota.IdCliente,
                           ir.Query.Data == this.dadosNota.DataNota,
                           ir.Query.IdAgente == this.dadosNota.IdAgenteCorretora,
                           ir.Query.Identificador.In((Int16)IdentificadorIR.IRFonteAcoes, (Int16)IdentificadorIR.IRFonteFII));
            //
            ir.Query.Load();

            decimal valorBase = (ir.ValorBase.HasValue) ? ir.ValorBase.Value : 0;
            decimal valorIR = (ir.ValorIR.HasValue) ? ir.ValorIR.Value : 0;

            if (valorBase != 0 && valorIR != 0) {
                ((XRTableCell)table.Rows[4].Cells[0]).Text = "I.R.R.F s/ operações, base R$ " + valorBase.ToString("N2");
                ((XRTableCell)table.Rows[4].Cells[1]).Text = valorIR.ToString("N2");
                ((XRTableCell)table.Rows[4].Cells[2]).Text = this.DebitaIR() ? "D" : "";
                //
                valorIRCelula = valorIR;
            }
            #endregion

        }
        else { // OPC - OPV - OPC_OPV

            #region IrFonte Opções
            IRFonte ir1 = new IRFonte();
            ir1.Query.Select(ir1.Query.ValorBase.Sum(), ir1.Query.ValorIR.Sum())
                    .Where(ir1.Query.IdCliente == this.dadosNota.IdCliente,
                           ir1.Query.Data == this.dadosNota.DataNota,
                           ir1.Query.IdAgente == this.dadosNota.IdAgenteCorretora,
                           ir1.Query.Identificador == (Int16)IdentificadorIR.IRFonteOpcoes);
            //
            ir1.Query.Load();

            decimal valorBase1 = (ir1.ValorBase.HasValue) ? ir1.ValorBase.Value : 0;
            decimal valorIR1 = (ir1.ValorIR.HasValue) ? ir1.ValorIR.Value : 0;

            if (valorBase1 != 0 && valorIR1 != 0) {
                ((XRTableCell)table.Rows[4].Cells[0]).Text = "I.R.R.F s/ operações, base R$ " + valorBase1.ToString("N2");
                ((XRTableCell)table.Rows[4].Cells[1]).Text = valorIR1.ToString("N2");
                ((XRTableCell)table.Rows[4].Cells[2]).Text = this.DebitaIR() ? "D" : "";
                //
                valorIRCelula = valorIR1;
            }
            #endregion
        }

        //
        ((XRTableCell)table.Rows[5].Cells[1]).Text = dadosNota.GetFooter.ir.ToString("N2"); // 0 sempre - Outras
        //

        decimal total = corretagemAux + dadosNota.GetFooter.ir;
        if (this.DebitaIR()) { // Se Coluna I.R.R.F s/ operações base é D então soma no total
            total += valorIRCelula;
        }

        if (cobraIss) {
            total += dadosNota.GetFooter.iss;
        }

        //
        ((XRTableCell)table.Rows[6].Cells[1]).Text = total.ToString("N2");
        //
        ((XRTableCell)table.Rows[2].Cells[2]).Text = "D"; // Corretagem
        ((XRTableCell)table.Rows[3].Cells[2]).Text = cobraIss ? "D" : ""; // ISS
        ((XRTableCell)table.Rows[5].Cells[2]).Text = "D"; // Outras
        ((XRTableCell)table.Rows[6].Cells[2]).Text = "D"; //Total


        /* ------------------------------------------------------------------*/
        // Armazena Total
        this.totalCorretagem = total * -1;
    }

    private void Footer5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        //decimal totalCorretagem = dadosNota.GetFooter.corretagem + dadosNota.GetFooter.iss + dadosNota.GetFooter.ir;
        //decimal valor = dadosNota.TipoMercado == TipoMercadoBolsa.OpcaoCompra ||
        //                dadosNota.TipoMercado == TipoMercadoBolsa.OpcaoVenda ||
        //                dadosNota.TipoMercado == "OPC_OPV"
        //                ? (dadosNota.GetFooter.vendasOpcoes - dadosNota.GetFooter.comprasOpcoes) - totalCorretagem
        //                : (dadosNota.GetFooter.vendasVista - dadosNota.GetFooter.comprasVista) - totalCorretagem;
        ////
        //decimal valorAbsoluto = Math.Abs(valor);
        //
        DateTime data = new DateTime();
        if (dadosNota.TipoMercado == "VIS_TER_IMO" ||
            dadosNota.TipoMercado == TipoMercadoBolsa.MercadoVista ||
            dadosNota.TipoMercado == TipoMercadoBolsa.Termo ||
            dadosNota.TipoMercado == TipoMercadoBolsa.Imobiliario) {
            
            data = Calendario.AdicionaDiaUtil(dadosNota.GetHeader.dataPregao, 3, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
        else { // OPC - OPV - OPC_OPV
            data = Calendario.AdicionaDiaUtil(dadosNota.GetHeader.dataPregao, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
  
        // Somatoria de resumo + bovespaSoma + Corretagem
        decimal total = this.totalResumo + this.totalBovespaSoma + this.totalCorretagem;
        decimal absTotal = Math.Abs(total);

        ((XRTableCell)table.Rows[0].Cells[0]).Text = "Líquido para " + data.ToString("dd/MM/yyyy");
        //((XRTableCell)table.Rows[0].Cells[1]).Text = valorAbsoluto.ToString("N2");

        ((XRTableCell)table.Rows[0].Cells[1]).Text = absTotal.ToString("N2");
        ((XRTableCell)table.Rows[0].Cells[2]).Text = total >= 0 ? "C" : "D";
    }

    private void Footer6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTable table = sender as XRTable;

        ((XRTableCell)table.Rows[0].Cells[0]).Text = dadosNota.GetHeader.nomeAgente;
    }
    #endregion
                    
    private void Footer_PrintOnPage(object sender, DevExpress.XtraReports.UI.PrintOnPageEventArgs e) {
        XRTableCell valorXRTableCell = sender as XRTableCell;

        if (e.PageIndex != e.PageCount - 1) {
            valorXRTableCell.Text = String.Empty;

            // Zera Totais
            this.totalResumo = 0;
            this.totalBovespaSoma = 0;
            this.totalCorretagem = 0;
        }
    }

    private void FooterImprimeContinua_PrintOnPage(object sender, DevExpress.XtraReports.UI.PrintOnPageEventArgs e) {
        XRTableCell valorXRTableCell = sender as XRTableCell;

        if (e.PageIndex != e.PageCount - 1) {
            valorXRTableCell.Text = "C O N T I N U A . . .";
            valorXRTableCell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        }
    }
    #endregion

    #region Detail
    private void NegociacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTableCell negociacaoXRTableCell = sender as XRTableCell;
        negociacaoXRTableCell.Text = "";

        if (this.numeroLinhasDataTable != 0) {
            negociacaoXRTableCell.Text = "1-BOVESPA";
        }
    }

    private void CreditoDebitoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTableCell creditoDebitoXRTableCell = sender as XRTableCell;
        creditoDebitoXRTableCell.Text = "";
        
        if (this.numeroLinhasDataTable != 0) {            
            string tipoOperacao = ((string)this.GetCurrentColumnValue(OperacaoBolsaMetadata.ColumnNames.TipoOperacao)).Substring(0,1);
            string creditoDebito = tipoOperacao == "C" ? "D" : "C";
            creditoDebitoXRTableCell.Text = creditoDebito;
        }
    }
    #endregion

    /// <summary>
    /// Retorna se tem o Débito na Tabela Corretagem/Despesas
    /// </summary>
    /// <returns></returns>
    private bool CobraIss() 
    {
        PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollection = new PerfilCorretagemBolsaCollection();
        //
        perfilCorretagemBolsaCollection.Query.Select(perfilCorretagemBolsaCollection.Query.Iss);
        perfilCorretagemBolsaCollection.Query.Where(perfilCorretagemBolsaCollection.Query.IdCliente == dadosNota.IdCliente,
                                                   perfilCorretagemBolsaCollection.Query.IdAgente == dadosNota.IdAgenteCorretora,
                                                   perfilCorretagemBolsaCollection.Query.DataReferencia <= dadosNota.DataNota);
        perfilCorretagemBolsaCollection.Query.OrderBy(perfilCorretagemBolsaCollection.Query.DataReferencia.Descending);
        //
        perfilCorretagemBolsaCollection.Query.Load();
        //
        return (perfilCorretagemBolsaCollection.Count != 0 && perfilCorretagemBolsaCollection[0].Iss != 0);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private bool DebitaIR() {
        LiquidacaoQuery l = new LiquidacaoQuery("L");

        l.Select(l.IdLiquidacao);
        l.Where(l.IdCliente == this.dadosNota.IdCliente,
                l.IdAgente == this.dadosNota.IdAgenteCorretora,
                l.DataLancamento == this.dadosNota.DataNota,
                l.Origem == (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao,
                l.Fonte == (byte)FonteLancamentoLiquidacao.Interno);

        LiquidacaoCollection liquidacao = new LiquidacaoCollection();
        liquidacao.Load(l);

        return liquidacao.Count >= 1;
    }

    private void LogotipoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        //const string imagem = "logo_cliente.png";
        string imagem = "logo_" +this.dadosNota.IdAgenteCorretora.ToString()+".png";
        //
        string imagemLogoAgente = "~/imagens/agentes/" + imagem;
        string pathImagem = DiretorioAplicacao.DiretorioBaseAplicacao + "imagens/agentes/" + imagem;

        // Se existe path carrega imagemLogo Personalizada
        if (File.Exists(pathImagem)) {            
            this.xrPictureBox1.ImageUrl = imagemLogoAgente;
            //this.xrPictureBox1.Sizing = ImageSizeMode.ZoomImage;

            int largura = this.xrPictureBox1.Image.Size.Width; // 203 pixels X 0.264583333 = milimeters
            largura = Convert.ToInt32(largura * 0.264583333) + 5;
            int larguraComponente = Convert.ToInt32(this.xrPictureBox1.SizeF.Width); // tenth of a millimeter
            larguraComponente = larguraComponente / 10; // Milimeters

            if (largura > larguraComponente) {
                this.xrPictureBox1.Sizing = ImageSizeMode.StretchImage;
            }
            else {
                this.xrPictureBox1.Sizing = ImageSizeMode.Normal;
            }
        }
    }

    private void SacFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
        XRTableCell sacXRTableCell = sender as XRTableCell;
        sacXRTableCell.Text = "";

        string sac = "";
        if (!String.IsNullOrEmpty(dadosNota.GetHeader.telefoneOuvidoria) || !String.IsNullOrEmpty(dadosNota.GetHeader.emailOuvidoria)) {
            sac = "SAC/Ouvidoria: ";
            if (!String.IsNullOrEmpty(dadosNota.GetHeader.telefoneOuvidoria)) {
                sac += dadosNota.GetHeader.telefoneOuvidoria;
            }
            if (!String.IsNullOrEmpty(dadosNota.GetHeader.emailOuvidoria)) {
                if (String.IsNullOrEmpty(dadosNota.GetHeader.telefoneOuvidoria)) {
                    sac += dadosNota.GetHeader.emailOuvidoria;
                }
                else {
                    sac += " - " +dadosNota.GetHeader.emailOuvidoria;
                }
            }

            sacXRTableCell.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top)));
        }

        sacXRTableCell.Text = sac;
    }
}