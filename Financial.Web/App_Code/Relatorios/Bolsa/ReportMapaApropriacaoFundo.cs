﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Relatorio;

/// <summary>
/// Summary description for ReportMapaApropriacaoFundo
/// </summary>
public class ReportMapaApropriacaoFundo : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public ReportMapaApropriacaoFundo()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    public ReportMapaApropriacaoFundo(DateTime dataHistorico, int? idCliente, bool isByCliente)
        {
            this.DataHistorico = dataHistorico;
            this.IdCliente = idCliente;
            this.MapaOperacaoBolsa();
        }


    private void MapaOperacaoBolsa()
    {
        this.InitializeComponent();
        this.PersonalInitialize();

        // Configura o Relatorio
        ReportBase relatorioBase = new ReportBase(this);

        // Tratamento para Report sem dados
        this.SetRelatorioSemDados();

        // Configura o tamanho da linha do subReport
        this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2480);
    }

    private void PersonalInitialize()
    {
        DataTable dt = this.FillDados();
        this.DataSource = dt;
        this.numeroLinhasDataTable = dt.Rows.Count;

        // Controla se Informação do cliente será visivel
        this.xrTable3.Visible = this.IdCliente.HasValue;

        #region Pega Campos do resource
        this.xrTableCell3.Text = "Apropriação de fundos";// Resources.ReportMapaOperacaoBolsa._TituloRelatorio;
        this.xrTableCell4.Text = Resources.ReportMapaOperacaoBolsa._DataEmissao;
        
        this.xrTableCell49.Text = Resources.ReportMapaOperacaoBolsa._Cliente;
               
        this.xrTableCell8.Text = "Apelido"; // Resources.ReportMapaOperacaoBolsa._Corretora;
        this.xrTableCell9.Text = "Nome"; // Resources.ReportMapaOperacaoBolsa._Codigo;        
        this.xrTableCell42.Text = "Valor Bruto"; //Resources.ReportMapaOperacaoBolsa._Valor;
        this.xrTableCell43.Text = "Valor IR";// Resources.ReportMapaOperacaoBolsa._Corretagem;
        this.xrTableCell23.Text = "Valor IOF";//Resources.ReportMapaOperacaoBolsa._Taxas;
        this.xrTableCell25.Text = "ValorLiquido";// Resources.ReportMapaOperacaoBolsa._ValorLiquido; ValorLiquido
        #endregion
    }

    /// <summary>
    /// Se relatorio não tem dados deixa invisible 
    /// </summary>
    private void SetRelatorioSemDados()
    {
        if (this.numeroLinhasDataTable == 0)
        {
            // Desaparece com as todas as bandas menos o subreport                                
            this.xrSubreport1.Visible = true;
            //                
            this.xrTable8.Visible = false;
            this.xrTable9.Visible = false;
            this.xrTable10.Visible = false;
            this.xrTable11.Visible = false;
            this.xrTable12.Visible = false;
        }
    }

    private DataTable FillDados()
    {
        esUtility u = new esUtility();
        
        string listaOrigemOperacaoBolsa = OrigemOperacaoBolsa.Primaria + ", " +
                                OrigemOperacaoBolsa.ExercicioOpcaoCompra + ", " +
                                OrigemOperacaoBolsa.ExercicioOpcaoVenda;

        
        string listaTipoOperacao = "'" + TipoOperacaoBolsa.Deposito + "'" + ", " +
                                   "'" + TipoOperacaoBolsa.Retirada + "'";       

        esParameters esParams = new esParameters();
        esParams.Add("DataInicio", this.dataInicio);
        esParams.Add("DataFim", this.dataFim);

        #region SQL
        StringBuilder sqlText = new StringBuilder();


        sqlText.Append(" SELECT C.IdCliente as IdCliente, C.Apelido as Cliente, B.Apelido as Fundo, B.NOME as NomeFundo, SUM(A.ValorRendimento) as ValorRendimento, SUM(A.ValorBruto) as ValorBruto,  SUM(A.ValorIR) as ValorIR, SUM(A.ValorIOF) as ValorIOF,  SUM(A.ValorLiquido) as ValorLiquido \n");
        sqlText.Append(" FROM PosicaoFundoHistorico as A INNER JOIN Carteira AS B ON A.IdCarteira = B.IdCarteira \n");
        sqlText.Append(" INNER JOIN Cliente as c on c.IdCliente = A.IdCliente \n");
        sqlText.Append(" WHERE A.DataHistorico = '" + dataHistorico.ToString("yyyyMMdd") + "'\n");
        if (this.idCliente.HasValue)
        {
            sqlText.Append(" and  c.IdCliente = " + this.idCliente + " \n");
        }
        sqlText.Append(" GROUP BY C.IdCliente, C.Apelido, B.Apelido, B.Nome ORDER BY c.IdCliente, c.Apelido");
       
        #endregion
        
        DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString());
        return dt;
    }

    private DateTime dataInicio;

    public DateTime DataInicio
    {
        get { return dataInicio; }
        set { dataInicio = value; }
    }

    private DateTime dataFim;

    public DateTime DataFim
    {
        get { return dataFim; }
        set { dataFim = value; }
    }

    private string cdAtivoBolsa;

    public string CdAtivoBolsa
    {
        get { return cdAtivoBolsa; }
        set { cdAtivoBolsa = value; }
    }

    private int? idCliente;

    public int? IdCliente
    {
        get { return idCliente; }
        set { idCliente = value; }
    }

    private string tipoMercado;

    public string TipoMercado
    {
        get { return tipoMercado; }
        set { tipoMercado = value; }
    }

    private int? idAgenteCorretora;

    public int? IdAgenteCorretora
    {
        get { return idAgenteCorretora; }
        set { idAgenteCorretora = value; }
    }

    private DateTime dataHistorico;

    public DateTime DataHistorico
    {
        get { return dataHistorico; }
        set { dataHistorico = value; }
    }

    private string tipoOperacao;

    private int numeroLinhasDataTable;

    //
    
    private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    private PageHeaderBand PageHeader;
    private GroupHeaderBand GroupHeader1;
    private GroupFooterBand GroupFooter1;
    private ReportFooterBand ReportFooter;
    private XRSubreport xrSubreport3;
    private SubReportLogotipo subReportLogotipo1;
    private XRSubreport xrSubreport1;
    private ReportSemDados reportSemDados1;
    private XRTable xrTable8;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell8;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell11;
    private XRTableCell xrTableCell21;
    private XRTableCell xrTableCell42;
    private XRTableCell xrTableCell43;
    private XRTableCell xrTableCell23;
    private XRTableCell xrTableCell25;
    private XRPageInfo xrPageInfo1;
    private XRPanel xrPanel1;
    private XRTable xrTable7;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell4;
    private XRPageInfo xrPageInfo2;
    private XRPageInfo xrPageInfo3;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell3;
    private XRSubreport xrSubreport2;
    private SubReportRodapeLandScape subReportRodapeLandScape1;
    private XRTable xrTable9;
    private XRTableRow xrTableRow9;
    private XRTableCell xrTableCell26;
    private XRTable xrTable10;
    private XRTableRow xrTableRow10;
    private XRTableCell xrTableCell28;
    private XRTableCell xrTableCell35;
    private XRTableCell xrTableCell55;
    private XRTableCell xrTableCell59;
    private XRTableCell xrTableCell60;
    private XRTableCell xrTableCell61;
    private XRTableCell xrTableCell62;
    private XRTableCell xrTableCell63;
    private XRTable xrTable11;
    private XRTableRow xrTableRow11;
    private XRTableCell xrTableCell64;
    private XRTableCell xrTableCell65;
    private XRTableCell xrTableCell66;
    private XRTableCell xrTableCell67;
    private XRTableCell xrTableCell68;
    private XRTableCell xrTableCell69;
    private XRTableCell xrTableCell70;
    private XRTableCell xrTableCell71;
    private XRTableCell xrTableCell72;
    private XRTableCell xrTableCell73;
    private XRTable xrTable12;
    private XRTableRow xrTableRow12;
    private XRTableCell xrTableCell74;
    private XRTableCell xrTableCell75;
    private XRTableCell xrTableCell76;
    private XRTableCell xrTableCell77;
    private XRTableCell xrTableCell78;
    private XRTableCell xrTableCell79;
    private XRTableCell xrTableCell80;
    private XRTableCell xrTableCell81;
    private XRTableCell xrTableCell82;
    private XRTableCell xrTableCell83;
    private TopMarginBand topMarginBand1;
    private BottomMarginBand bottomMarginBand1;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell49;
    private XRTableCell xrTableCell52;
    private XRTableCell xrTableCell2;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {        
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
        this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
        this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
        this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
        this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
        this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
        this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
        this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
        this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
        this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
        this.Detail.Dpi = 254F;
        this.Detail.HeightF = 50F;
        this.Detail.KeepTogether = true;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("Data", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("TipoOperacao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrTable10
        // 
        this.xrTable10.Dpi = 254F;
        this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrTable10.Name = "xrTable10";
        this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
        this.xrTable10.SizeF = new System.Drawing.SizeF(2480F, 48F);
        this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow10
        // 
        this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell35,
            this.xrTableCell55,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63});
        this.xrTableRow10.Dpi = 254F;
        this.xrTableRow10.Name = "xrTableRow10";
        this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow10.Weight = 1;
        // 
        // xrTableCell28
        // 
        this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Data", "{0:d}")});
        this.xrTableCell28.Dpi = 254F;
        this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell28.Name = "xrTableCell28";
        this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell28.StylePriority.UseFont = false;
        this.xrTableCell28.Text = "xrTableCell1";
        this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell28.Weight = 0.079032258064516123;
        // 
        // xrTableCell35
        // 
        this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Fundo")});
        this.xrTableCell35.Dpi = 254F;
        this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell35.Name = "xrTableCell35";
        this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell35.StylePriority.UseFont = false;
        this.xrTableCell35.Text = "xrTableCell2";
        this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell35.Weight = 0.16969925665086316;
        // 
        // xrTableCell55
        // 
        this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.NomeFundo")});
        this.xrTableCell55.Dpi = 254F;
        this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell55.Name = "xrTableCell55";
        this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell55.StylePriority.UseFont = false;
        this.xrTableCell55.Text = "xrTableCell3";
        this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell55.Weight = 0.23080313897901961;
        // 
        // xrTableCell59
        // 
        this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorRendimento", "{0:n2}")});
        this.xrTableCell59.Dpi = 254F;
        this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell59.Name = "xrTableCell59";
        this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell59.StylePriority.UseFont = false;
        this.xrTableCell59.Text = "esUtility.ValorRendimento";
        this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell59.Weight = 0.14071901382938507;
        // 
        // xrTableCell60
        // 
        this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto", "{0:n2}")});
        this.xrTableCell60.Dpi = 254F;
        this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell60.Name = "xrTableCell60";
        this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell60.StylePriority.UseFont = false;
        xrSummary1.FormatString = "{0:n2}";
        this.xrTableCell60.Summary = xrSummary1;
        this.xrTableCell60.Text = "esUtility.ValorBruto";
        this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell60.Weight = 0.12570564516129029;
        this.xrTableCell60.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell61
        // 
        this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR", "{0:n2}")});
        this.xrTableCell61.Dpi = 254F;
        this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell61.Name = "xrTableCell61";
        this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell61.StylePriority.UseFont = false;
        xrSummary2.FormatString = "{0:n2}";
        this.xrTableCell61.Summary = xrSummary2;
        this.xrTableCell61.Text = "[esUtility.ValorIR]";
        this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell61.Weight = 0.075882105673513042;
        this.xrTableCell61.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell62
        // 
        this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF", "{0:n2}")});
        this.xrTableCell62.Dpi = 254F;
        this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell62.Name = "xrTableCell62";
        this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell62.StylePriority.UseFont = false;
        this.xrTableCell62.Text = "[esUtility.ValorIOF]";
        this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell62.Weight = 0.070413318757087978;
        this.xrTableCell62.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell63
        // 
        this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido", "{0:n2}")});
        this.xrTableCell63.Dpi = 254F;
        this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell63.Name = "xrTableCell63";
        this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell63.StylePriority.UseFont = false;
        this.xrTableCell63.Text = "[esUtility.ValorLiquido]";
        this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell63.Weight = 0.10774526288432459;
        this.xrTableCell63.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrSubreport1,
            this.xrTable8,
            this.xrPageInfo1,
            this.xrPanel1,
            this.xrTable1});
        this.PageHeader.Dpi = 254F;
        this.PageHeader.HeightF = 285.8125F;
        this.PageHeader.Name = "PageHeader";
        this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrSubreport3
        // 
        this.xrSubreport3.Dpi = 254F;
        this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrSubreport3.Name = "xrSubreport3";
        this.xrSubreport3.ReportSource = this.subReportLogotipo1;
        this.xrSubreport3.SizeF = new System.Drawing.SizeF(767F, 64F);
        // 
        // xrSubreport1
        // 
        this.xrSubreport1.Dpi = 254F;
        this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(58.35416F, 236.1042F);
        this.xrSubreport1.Name = "xrSubreport1";
        this.xrSubreport1.ReportSource = this.reportSemDados1;
        this.xrSubreport1.SizeF = new System.Drawing.SizeF(30F, 30F);
        this.xrSubreport1.Visible = false;
        // 
        // xrTable8
        // 
        this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrTable8.Dpi = 254F;
        this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 235F);
        this.xrTable8.Name = "xrTable8";
        this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
        this.xrTable8.SizeF = new System.Drawing.SizeF(2480F, 48F);
        this.xrTable8.StylePriority.UseBorders = false;
        this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell2,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell23,
            this.xrTableCell25});
        this.xrTableRow6.Dpi = 254F;
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow6.Weight = 1;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.Dpi = 254F;
        this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell8.StylePriority.UseFont = false;
        this.xrTableCell8.Text = "Apelido";
        this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        this.xrTableCell8.Weight = 0.19988787767170124;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.Dpi = 254F;
        this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell9.Multiline = true;
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell9.StylePriority.UseFont = false;
        this.xrTableCell9.Text = "Nome\r\n";
        this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        this.xrTableCell9.Weight = 0.185480001870849;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Dpi = 254F;
        this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell2.Multiline = true;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell2.StylePriority.UseFont = false;
        this.xrTableCell2.Text = "Valor Rendimento";
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.xrTableCell2.Weight = 0.11308598811618993;
        // 
        // xrTableCell42
        // 
        this.xrTableCell42.Dpi = 254F;
        this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell42.Multiline = true;
        this.xrTableCell42.Name = "xrTableCell42";
        this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell42.StylePriority.UseFont = false;
        this.xrTableCell42.Text = "Valor Bruto\r\n";
        this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.xrTableCell42.Weight = 0.10102068556955278;
        // 
        // xrTableCell43
        // 
        this.xrTableCell43.Dpi = 254F;
        this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell43.Multiline = true;
        this.xrTableCell43.Name = "xrTableCell43";
        this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell43.StylePriority.UseFont = false;
        this.xrTableCell43.Text = "Valor IR\r\n \r\nIR";
        this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.xrTableCell43.Weight = 0.060981067598920496;
        // 
        // xrTableCell23
        // 
        this.xrTableCell23.Dpi = 254F;
        this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell23.Name = "xrTableCell23";
        this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell23.StylePriority.UseFont = false;
        this.xrTableCell23.Text = "Valor IOF";
        this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.xrTableCell23.Weight = 0.056586197794538134;
        // 
        // xrTableCell25
        // 
        this.xrTableCell25.Dpi = 254F;
        this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell25.Name = "xrTableCell25";
        this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell25.StylePriority.UseFont = false;
        this.xrTableCell25.Text = "#ValorLiquido";
        this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
        this.xrTableCell25.Weight = 0.086587213636312971;
        // 
        // xrPageInfo1
        // 
        this.xrPageInfo1.Dpi = 254F;
        this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2461F, 106F);
        this.xrPageInfo1.Name = "xrPageInfo1";
        this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrPageInfo1.SizeF = new System.Drawing.SizeF(108F, 42F);
        this.xrPageInfo1.StylePriority.UseFont = false;
        this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        // 
        // xrPanel1
        // 
        this.xrPanel1.CanGrow = false;
        this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable3});
        this.xrPanel1.Dpi = 254F;
        this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 84.99998F);
        this.xrPanel1.Name = "xrPanel1";
        this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.xrPanel1.SizeF = new System.Drawing.SizeF(1918.063F, 137F);
        // 
        // xrTable7
        // 
        this.xrTable7.Dpi = 254F;
        this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable7.Name = "xrTable7";
        this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
        this.xrTable7.SizeF = new System.Drawing.SizeF(215F, 42F);
        this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
        this.xrTableRow7.Dpi = 254F;
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow7.Weight = 1;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.CanGrow = false;
        this.xrTableCell4.Dpi = 254F;
        this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell4.StylePriority.UseFont = false;
        this.xrTableCell4.Text = "#DataEmissao";
        this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell4.Weight = 1;
        // 
        // xrPageInfo2
        // 
        this.xrPageInfo2.Dpi = 254F;
        this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrPageInfo2.Format = "{0:HH:mm:ss}";
        this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(365F, 0F);
        this.xrPageInfo2.Name = "xrPageInfo2";
        this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo2.SizeF = new System.Drawing.SizeF(126F, 42F);
        this.xrPageInfo2.StylePriority.UseFont = false;
        this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrPageInfo3
        // 
        this.xrPageInfo3.Dpi = 254F;
        this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrPageInfo3.Format = "{0:d}";
        this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(217F, 0F);
        this.xrPageInfo3.Name = "xrPageInfo3";
        this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
        this.xrPageInfo3.StylePriority.UseFont = false;
        this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable3
        // 
        this.xrTable3.Dpi = 254F;
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 84.00004F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(1737.854F, 42F);
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell52});
        this.xrTableRow3.Dpi = 254F;
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow3.Weight = 1;
        // 
        // xrTableCell49
        // 
        this.xrTableCell49.CanGrow = false;
        this.xrTableCell49.Dpi = 254F;
        this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell49.Name = "xrTableCell49";
        this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell49.StylePriority.UseFont = false;
        this.xrTableCell49.Text = "#Cliente";
        this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell49.Weight = 0.23715846994535519;
        // 
        // xrTableCell52
        // 
        this.xrTableCell52.CanGrow = false;
        this.xrTableCell52.Dpi = 254F;
        this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell52.Name = "xrTableCell52";
        this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell52.StylePriority.UseFont = false;
        this.xrTableCell52.Text = "Cliente";
        this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell52.Weight = 1.6621357902151641;
        this.xrTableCell52.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteCabecalhoBeforePrint);
        // 
        // xrTable1
        // 
        this.xrTable1.Dpi = 254F;
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(884F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
        this.xrTable1.SizeF = new System.Drawing.SizeF(1672F, 64F);
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3});
        this.xrTableRow1.Dpi = 254F;
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow1.Weight = 1;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.Dpi = 254F;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell1.Weight = 0.088516746411483258;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.Dpi = 254F;
        this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell3.StylePriority.UseFont = false;
        this.xrTableCell3.Text = "#Titlulo";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell3.Weight = 0.91148325358851678;
        // 
        // PageFooter
        // 
        this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
        this.PageFooter.Dpi = 254F;
        this.PageFooter.HeightF = 71F;
        this.PageFooter.Name = "PageFooter";
        this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrSubreport2
        // 
        this.xrSubreport2.Dpi = 254F;
        this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrSubreport2.Name = "xrSubreport2";
        this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
        this.xrSubreport2.SizeF = new System.Drawing.SizeF(528F, 64F);
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
        this.GroupHeader1.Dpi = 254F;
        this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("IdCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
        this.GroupHeader1.HeightF = 50F;
        this.GroupHeader1.KeepTogether = true;
        this.GroupHeader1.Name = "GroupHeader1";
        this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.GroupHeader1.RepeatEveryPage = true;
        this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable9
        // 
        this.xrTable9.BackColor = System.Drawing.Color.LightGray;
        this.xrTable9.Dpi = 254F;
        this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrTable9.Name = "xrTable9";
        this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
        this.xrTable9.SizeF = new System.Drawing.SizeF(2480F, 48F);
        this.xrTable9.StylePriority.UseBackColor = false;
        this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow9
        // 
        this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
        this.xrTableRow9.Dpi = 254F;
        this.xrTableRow9.Name = "xrTableRow9";
        this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow9.Weight = 1;
        // 
        // xrTableCell26
        // 
        this.xrTableCell26.Dpi = 254F;
        this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell26.Name = "xrTableCell26";
        this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell26.StylePriority.UseFont = false;
        this.xrTableCell26.Text = "Cliente";
        this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell26.Weight = 1;
        this.xrTableCell26.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
        this.GroupFooter1.Dpi = 254F;
        this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
        this.GroupFooter1.HeightF = 53F;
        this.GroupFooter1.KeepTogether = true;
        this.GroupFooter1.Name = "GroupFooter1";
        this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrTable11
        // 
        this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
        this.xrTable11.Dpi = 254F;
        this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrTable11.Name = "xrTable11";
        this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
        this.xrTable11.SizeF = new System.Drawing.SizeF(2480F, 48F);
        this.xrTable11.StylePriority.UseBorders = false;
        this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow11
        // 
        this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73});
        this.xrTableRow11.Dpi = 254F;
        this.xrTableRow11.Name = "xrTableRow11";
        this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow11.Weight = 1;
        // 
        // xrTableCell64
        // 
        this.xrTableCell64.Dpi = 254F;
        this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell64.Name = "xrTableCell64";
        this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell64.StylePriority.UseFont = false;
        this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomJustify;
        this.xrTableCell64.Weight = 0.079032258064516123;
        // 
        // xrTableCell65
        // 
        this.xrTableCell65.Dpi = 254F;
        this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell65.Name = "xrTableCell65";
        this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell65.StylePriority.UseFont = false;
        this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell65.Weight = 0.14516129032258066;
        // 
        // xrTableCell66
        // 
        this.xrTableCell66.Dpi = 254F;
        this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell66.Name = "xrTableCell66";
        this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell66.StylePriority.UseFont = false;
        this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell66.Weight = 0.10241935483870968;
        // 
        // xrTableCell67
        // 
        this.xrTableCell67.Dpi = 254F;
        this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell67.Name = "xrTableCell67";
        this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell67.StylePriority.UseFont = false;
        this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell67.Weight = 0.10241935483870968;
        // 
        // xrTableCell68
        // 
        this.xrTableCell68.Dpi = 254F;
        this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell68.Name = "xrTableCell68";
        this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell68.StylePriority.UseFont = false;
        this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell68.Weight = 0.0939516129032258;
        // 
        // xrTableCell69
        // 
        this.xrTableCell69.Dpi = 254F;
        this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell69.Name = "xrTableCell69";
        this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell69.StylePriority.UseFont = false;
        this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell69.Weight = 0.10241935483870968;
        // 
        // xrTableCell70
        // 
        this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Valor")});
        this.xrTableCell70.Dpi = 254F;
        this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell70.Name = "xrTableCell70";
        this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell70.StylePriority.UseFont = false;
        xrSummary3.FormatString = "{0:n2}";
        xrSummary3.IgnoreNullValues = true;
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell70.Summary = xrSummary3;
        this.xrTableCell70.Text = "xrTableCell70";
        this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell70.Weight = 0.12055609918409778;
        this.xrTableCell70.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell71
        // 
        this.xrTableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIR")});
        this.xrTableCell71.Dpi = 254F;
        this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell71.Name = "xrTableCell71";
        this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell71.StylePriority.UseFont = false;
        xrSummary4.FormatString = "{0:n2}";
        xrSummary4.IgnoreNullValues = true;
        xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell71.Summary = xrSummary4;
        this.xrTableCell71.Text = "[esUtility.ValorIR]";
        this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell71.Weight = 0.0758821056735131;
        this.xrTableCell71.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell72
        // 
        this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorIOF")});
        this.xrTableCell72.Dpi = 254F;
        this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell72.Name = "xrTableCell72";
        this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell72.StylePriority.UseFont = false;
        xrSummary5.FormatString = "{0:n2}";
        xrSummary5.IgnoreNullValues = true;
        xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell72.Summary = xrSummary5;
        this.xrTableCell72.Text = "[esUtility.ValorIOF]";
        this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell72.Weight = 0.070413306451612909;
        this.xrTableCell72.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell73
        // 
        this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Valorliquido")});
        this.xrTableCell73.Dpi = 254F;
        this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell73.Name = "xrTableCell73";
        this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell73.StylePriority.UseFont = false;
        xrSummary6.FormatString = "{0:n2}";
        xrSummary6.IgnoreNullValues = true;
        xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell73.Summary = xrSummary6;
        this.xrTableCell73.Text = "xrTableCell73";
        this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell73.Weight = 0.10774526288432459;
        this.xrTableCell73.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
        this.ReportFooter.Dpi = 254F;
        this.ReportFooter.HeightF = 58F;
        this.ReportFooter.KeepTogether = true;
        this.ReportFooter.Name = "ReportFooter";
        this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
        this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable12
        // 
        this.xrTable12.Borders = DevExpress.XtraPrinting.BorderSide.Top;
        this.xrTable12.Dpi = 254F;
        this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrTable12.Name = "xrTable12";
        this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
        this.xrTable12.SizeF = new System.Drawing.SizeF(2480F, 48F);
        this.xrTable12.StylePriority.UseBorders = false;
        this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTable12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
        // 
        // xrTableRow12
        // 
        this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83});
        this.xrTableRow12.Dpi = 254F;
        this.xrTableRow12.Name = "xrTableRow12";
        this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
        this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow12.Weight = 1;
        // 
        // xrTableCell74
        // 
        this.xrTableCell74.Dpi = 254F;
        this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell74.Name = "xrTableCell74";
        this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell74.StylePriority.UseFont = false;
        this.xrTableCell74.Text = "Total";
        this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell74.Weight = 0.13024193548387097;
        // 
        // xrTableCell75
        // 
        this.xrTableCell75.Dpi = 254F;
        this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell75.Name = "xrTableCell75";
        this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell75.StylePriority.UseFont = false;
        this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell75.Weight = 0.0939516129032258;
        // 
        // xrTableCell76
        // 
        this.xrTableCell76.Dpi = 254F;
        this.xrTableCell76.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell76.Name = "xrTableCell76";
        this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell76.StylePriority.UseFont = false;
        this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell76.Weight = 0.10241935483870968;
        // 
        // xrTableCell77
        // 
        this.xrTableCell77.Dpi = 254F;
        this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell77.Name = "xrTableCell77";
        this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell77.StylePriority.UseFont = false;
        this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell77.Weight = 0.10241935483870968;
        // 
        // xrTableCell78
        // 
        this.xrTableCell78.Dpi = 254F;
        this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell78.Name = "xrTableCell78";
        this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell78.StylePriority.UseFont = false;
        this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell78.Weight = 0.0939516129032258;
        // 
        // xrTableCell79
        // 
        this.xrTableCell79.Dpi = 254F;
        this.xrTableCell79.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell79.Name = "xrTableCell79";
        this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell79.StylePriority.UseFont = false;
        this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell79.Weight = 0.10241935483870968;
        // 
        // xrTableCell80
        // 
        this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Valor")});
        this.xrTableCell80.Dpi = 254F;
        this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell80.Name = "xrTableCell80";
        this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell80.StylePriority.UseFont = false;
        xrSummary7.FormatString = "{0:n2}";
        xrSummary7.IgnoreNullValues = true;
        xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell80.Summary = xrSummary7;
        this.xrTableCell80.Text = "xrTableCell80";
        this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell80.Weight = 0.12055609918409778;
        this.xrTableCell80.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell81
        // 
        this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Corretagem")});
        this.xrTableCell81.Dpi = 254F;
        this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell81.Name = "xrTableCell81";
        this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell81.StylePriority.UseFont = false;
        xrSummary8.FormatString = "{0:n2}";
        xrSummary8.IgnoreNullValues = true;
        xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell81.Summary = xrSummary8;
        this.xrTableCell81.Text = "xrTableCell81";
        this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell81.Weight = 0.0758821056735131;
        this.xrTableCell81.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell82
        // 
        this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Taxas")});
        this.xrTableCell82.Dpi = 254F;
        this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell82.Name = "xrTableCell82";
        this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell82.StylePriority.UseFont = false;
        xrSummary9.FormatString = "{0:n2}";
        xrSummary9.IgnoreNullValues = true;
        xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell82.Summary = xrSummary9;
        this.xrTableCell82.Text = "xrTableCell82";
        this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell82.Weight = 0.070413306451612909;
        this.xrTableCell82.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // xrTableCell83
        // 
        this.xrTableCell83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Valorliquido")});
        this.xrTableCell83.Dpi = 254F;
        this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8F);
        this.xrTableCell83.Name = "xrTableCell83";
        this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
        this.xrTableCell83.StylePriority.UseFont = false;
        xrSummary10.FormatString = "{0:n2}";
        xrSummary10.IgnoreNullValues = true;
        xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell83.Summary = xrSummary10;
        this.xrTableCell83.Text = "xrTableCell83";
        this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        this.xrTableCell83.Weight = 0.10774526288432459;
        this.xrTableCell83.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
        // 
        // topMarginBand1
        // 
        this.topMarginBand1.Dpi = 254F;
        this.topMarginBand1.HeightF = 150F;
        this.topMarginBand1.Name = "topMarginBand1";
        // 
        // bottomMarginBand1
        // 
        this.bottomMarginBand1.Dpi = 254F;
        this.bottomMarginBand1.HeightF = 150F;
        this.bottomMarginBand1.Name = "bottomMarginBand1";
        // 
        // ReportMapaOperacaoBolsa
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
        this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
        this.Dpi = 254F;
        this.ExportOptions.Html.RemoveSecondarySymbols = true;
        this.ExportOptions.Mht.RemoveSecondarySymbols = true;
        this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(101, 0, 150, 150);
        this.PageHeight = 2159;
        this.PageWidth = 2794;
        this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
        this.Version = "11.1";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }


    #endregion

    #region Funções Internas do Relatorio
    //
    private void DataInicioBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTableCell dataInicioXRTableCell = sender as XRTableCell;
        dataInicioXRTableCell.Text = this.dataInicio.ToString("d");
    }

    private void DataFimBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTableCell dataFimXRTableCell = sender as XRTableCell;
        dataFimXRTableCell.Text = this.dataFim.ToString("d");
    }

    private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTableCell clienteXRTableCell = sender as XRTableCell;
        clienteXRTableCell.Text = "";
        if (this.numeroLinhasDataTable != 0)
        {
            int idCliente = (int)this.GetCurrentColumnValue(ClienteMetadata.ColumnNames.IdCliente);
            string nome = (string)this.GetCurrentColumnValue("Cliente");
            string cliente = idCliente.ToString() + " - " + nome;
            //
            clienteXRTableCell.Text = cliente;
        }
    }

    private void TipoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTableCell tipoXRTableCell = sender as XRTableCell;
        tipoXRTableCell.Text = "";
        
    }

    private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTable summaryFinal = sender as XRTable;

        /* TotalValor*/
        XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
        ((XRTableCell)summaryFinalRow0.Cells[0]).Text = this.numeroLinhasDataTable != 0
                                            ? Resources.ReportMapaOperacaoBolsa._TotalGeral
                                            : "";
    }

    private void ClienteCabecalhoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        XRTableCell clienteXRTableCell = sender as XRTableCell;

        if (this.IdCliente.HasValue)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            cliente.LoadByPrimaryKey(campos, this.idCliente.Value);

            string nome = !String.IsNullOrEmpty(cliente.Nome) ? cliente.Nome : "";
            clienteXRTableCell.Text = nome;
        }
    }

    /// <summary>
    /// Aplica o formato na Célula com 2 duas Casas Decimais
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CustomFormat(object sender, PrintOnPageEventArgs e)
    {
        XRTableCell valorXRTableCell = sender as XRTableCell;
        decimal valor = 0.00M;
        try
        {
            valor = Convert.ToDecimal(valorXRTableCell.Text);
        }
        catch (Exception e1)
        {            
        }

        ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
    }

    #endregion
}
