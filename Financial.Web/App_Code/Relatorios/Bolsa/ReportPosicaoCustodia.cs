﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using Financial.Bolsa.Enums;
using Financial.Relatorio;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Reflection;
using Financial.WebConfigConfiguration;
using Financial.Bolsa;
using System.Text;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security;
using System.Web;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportPosicaoCustodia
    /// </summary>
    public class ReportPosicaoCustodia : XtraReport {
        //
        private DateTime dataPosicao;

        public DateTime DataPosicao {
            get { return dataPosicao; }
            set { dataPosicao = value; }
        }

        private string cdAtivoBolsa;

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        private enum TipoPesquisa {
            PosicaoBolsa = 0,
            PosicaoBolsaHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoBolsa;

        private int? idCliente;
        //
        private int numeroLinhasDataTable;

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRLabel xrLabel1;
        private GroupHeaderBand GroupHeader2;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell6;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell7;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCellDataPosicao;
        private XRTable xrTable6;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell23;
        private XRPageInfo xrPageInfo1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell19;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell24;
        private XRPageInfo xrPageInfo2;
        private Financial.Bolsa.PosicaoBolsaCollection posicaoBolsaCollection1;
        private Financial.Bolsa.PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection1;
        private GroupFooterBand GroupFooter2;
        private XRSubreport xrSubreport1;
        
        private PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection1;
        private XRSubreport xrSubreport2;
        
        private XRSubreport xrSubreport3;
        
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTable xrTable8;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        private SubReportLogotipo subReportLogotipo1;
        private ReportSemDados reportSemDados1;
        private SubReportRodape subReportRodape1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Construtores
        #region Construtores Privates
        private void PosicaoCustodia() {
            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            //this.reportSemDados1.w = false;

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();

            // Configura o tamanho da linha do subReport
            this.subReportRodape1.PersonalizaLinhaRodape(1855);
        }

        private void PosicaoCustodia(DateTime dataPosicao) {
            this.dataPosicao = dataPosicao;
            //
            this.PosicaoCustodia();
        }
        #endregion

        #region Construtores Publics
        public ReportPosicaoCustodia(DateTime dataPosicao) {
            this.PosicaoCustodia(dataPosicao);
        }

        public ReportPosicaoCustodia(int? idCliente, DateTime dataPosicao) {
            this.idCliente = idCliente;
            this.PosicaoCustodia(dataPosicao);
        }

        public ReportPosicaoCustodia(int? idCliente, DateTime dataPosicao, string cdAtivoBolsa) {
            this.idCliente = idCliente;
            this.cdAtivoBolsa = cdAtivoBolsa.Trim();
            this.PosicaoCustodia(dataPosicao);
        }
        #endregion
        #endregion

        /// <summary>
        ///  Preeenche o DataSet do Relatório  
        /// </summary>
        private void PersonalInitialize() {
            DataView dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Table.Rows.Count;

            #region Pega Campos do resource
            this.xrLabel1.Text = Resources.ReportPosicaoCustodia._TituloRelatorio;
            this.xrTableCell21.Text = Resources.ReportPosicaoCustodia._DataPosicao;
            this.xrTableCell13.Text = Resources.ReportPosicaoCustodia._Codigo;
            this.xrTableCell14.Text = Resources.ReportPosicaoCustodia._Especificacao;
            this.xrTableCell15.Text = Resources.ReportPosicaoCustodia._QtdeAbertura;
            this.xrTableCell16.Text = Resources.ReportPosicaoCustodia._Entradas;
            this.xrTableCell17.Text = Resources.ReportPosicaoCustodia._Saidas;
            this.xrTableCell18.Text = Resources.ReportPosicaoCustodia._QtdeFechamento;
            this.xrTableCell23.Text = Resources.ReportPosicaoCustodia._DataEmissao;
            //
            this.xrTableCell20.Text = Resources.ReportPosicaoCustodia._PuCusto;
            this.xrTableCell22.Text = Resources.ReportPosicaoCustodia._PuMercado;
            this.xrTableCell25.Text = Resources.ReportPosicaoCustodia._ValorMercado;
            #endregion
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                                
                this.xrSubreport1.Visible = true;
                //
                this.xrTable3.Visible = false;
                this.xrTable7.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable2.Visible = false;
                this.xrTable8.Visible = false;
            }
        }

        private DataView FillDados() {
            if (!this.idCliente.HasValue) {
                #region Consulta com PosicaoBolsa
                return this.ConsultaPosicaoBolsa();
                #endregion
            }

        /* IdCliente Especifico 
         * - Verifica a dataDia da Carteira para saber se Consulta em PosicaoBolsaHistórico ou PosicaoBolsa
         */
            else {
                #region Consulta em PosicaoBolsaHistorico ou PosicaoBolsa

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(campos, this.idCliente.Value);

                if (cliente.DataDia.Value.CompareTo(this.dataPosicao) == 0) {
                    return this.ConsultaPosicaoBolsa();
                }
                else {
                    // Consulta na posicaoBolsaHistorico
                    return this.ConsultaPosicaoBolsaHistorico();
                }

                #endregion
            }
        }

        /// <summary>
        /// Realiza a consulta na PosicaoBolsa
        /// </summary>
        /// <returns></returns>
        private DataView ConsultaPosicaoBolsa() {
            this.tipoPesquisa = TipoPesquisa.PosicaoBolsa;

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            #region SQL

            this.posicaoBolsaCollection1.QueryReset();

            /* Query com join */
            ClienteQuery cliente = new ClienteQuery("C");
            PosicaoBolsaQuery posicaoBolsa = new PosicaoBolsaQuery("P");
            AgenteMercadoQuery agenteMercado = new AgenteMercadoQuery("A");
            PermissaoClienteQuery permissaoCliente = new PermissaoClienteQuery("P1");            
            //
            posicaoBolsa.Select(posicaoBolsa.IdPosicao, posicaoBolsa.IdCliente, posicaoBolsa.CdAtivoBolsa,
                                posicaoBolsa.IdAgente, posicaoBolsa.PUCustoLiquido, posicaoBolsa.PUMercado,
                                posicaoBolsa.ValorMercado,
                                cliente.Nome.As("NomeCliente"),
                                agenteMercado.Nome.As("NomeAgenteMercado"));
            //
            posicaoBolsa.InnerJoin(cliente).On(posicaoBolsa.IdCliente == cliente.IdCliente);
            posicaoBolsa.InnerJoin(agenteMercado).On(posicaoBolsa.IdAgente == agenteMercado.IdAgente);
            //
            posicaoBolsa.InnerJoin(permissaoCliente).On(permissaoCliente.IdCliente == cliente.IdCliente);            
            posicaoBolsa.Where(permissaoCliente.IdUsuario == idUsuario);

            // Adiciona Filtros
            if (this.idCliente.HasValue) {
                posicaoBolsa.Where(posicaoBolsa.IdCliente == this.idCliente);
            }
            if (!String.IsNullOrEmpty(this.cdAtivoBolsa)) {
                posicaoBolsa.Where(posicaoBolsa.CdAtivoBolsa.Like(this.cdAtivoBolsa + "%"));
            }

            posicaoBolsa.OrderBy(posicaoBolsa.CdAtivoBolsa.Ascending);
            //
            this.posicaoBolsaCollection1.Load(posicaoBolsa);

            #endregion
            //
            // Union da PosicaoBolsa Com posicaoBolsa Abertura
            /* Para cada PosicaoBolsaAbertura que não está na posicaoBolsa adiciono na Posicão Bolsa
             */
            #region Union com PosicaoBolsaAbertura
            this.posicaoBolsaAberturaCollection1.QueryReset();

            PosicaoBolsaAberturaQuery posicaoBolsaAbertura = new PosicaoBolsaAberturaQuery("P");
            ClienteQuery cliente1 = new ClienteQuery("C");            
            PermissaoClienteQuery permissaoCliente1 = new PermissaoClienteQuery("P1");

            posicaoBolsaAbertura.Select(posicaoBolsaAbertura.IdPosicao,
                                        posicaoBolsaAbertura.IdCliente,
                                        posicaoBolsaAbertura.CdAtivoBolsa,
                                        posicaoBolsaAbertura.IdAgente,
                                        posicaoBolsaAbertura.PUCustoLiquido,
                                        posicaoBolsaAbertura.PUMercado,
                                        posicaoBolsaAbertura.ValorMercado
                                       );

            posicaoBolsaAbertura.InnerJoin(cliente1).On(posicaoBolsaAbertura.IdCliente == cliente1.IdCliente);
            posicaoBolsaAbertura.InnerJoin(permissaoCliente1).On(permissaoCliente1.IdCliente == cliente1.IdCliente);
            //
            posicaoBolsaAbertura.Where(permissaoCliente1.IdUsuario == idUsuario,
                                       posicaoBolsaAbertura.DataHistorico == this.dataPosicao);

            // Adiciona Filtros
            if (this.idCliente.HasValue) {
                posicaoBolsaAbertura.Where(posicaoBolsaAbertura.IdCliente == this.idCliente);
            }

            if (!String.IsNullOrEmpty(this.cdAtivoBolsa)) {
                posicaoBolsaAbertura.Where(posicaoBolsaAbertura.CdAtivoBolsa.Like(this.cdAtivoBolsa + "%"));
            }

            posicaoBolsaAbertura.OrderBy(posicaoBolsaAbertura.CdAtivoBolsa.Ascending);

            this.posicaoBolsaAberturaCollection1.Load(posicaoBolsaAbertura);

            for (int i = 0; i < this.posicaoBolsaAberturaCollection1.Count; i++) {
                PosicaoBolsaAbertura posicaoBolsaAberturaAux = new PosicaoBolsaAbertura();
                posicaoBolsaAberturaAux = posicaoBolsaAberturaCollection1[i];

                // se nao existe na posicaoBolsa então adiciono
                // Zera o Filtro
                this.posicaoBolsaCollection1.Filter = "";
                // Atualiza o filtro
                StringBuilder filtro = new StringBuilder();
                filtro.Append(PosicaoBolsaMetadata.ColumnNames.IdCliente).Append("= ").Append(posicaoBolsaAberturaAux.IdCliente).Append(" AND ");
                filtro.Append(PosicaoBolsaMetadata.ColumnNames.IdAgente).Append("= ").Append(posicaoBolsaAberturaAux.IdAgente).Append(" AND ");
                filtro.Append(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa).Append("= '").Append(posicaoBolsaAberturaAux.CdAtivoBolsa).Append("'");
                //
                this.posicaoBolsaCollection1.Filter = filtro.ToString();
                // Adiciono PosicaoBolsaAbertura na PosicaoBolsa
                if (this.posicaoBolsaCollection1.Count == 0) {
                    PosicaoBolsa posicaoBolsaAux = new PosicaoBolsa();
                    posicaoBolsaAux.IdCliente = posicaoBolsaAberturaAux.IdCliente;
                    posicaoBolsaAux.IdAgente = posicaoBolsaAberturaAux.IdAgente;
                    posicaoBolsaAux.CdAtivoBolsa = posicaoBolsaAberturaAux.CdAtivoBolsa;
                    posicaoBolsaAux.NomeCliente = posicaoBolsaAux.UpToClienteByIdCliente.Nome;
                    posicaoBolsaAux.NomeAgenteMercado = posicaoBolsaAux.UpToAgenteMercadoByIdAgente.Nome;
                    //
                    posicaoBolsaAux.PUCustoLiquido = 0;
                    posicaoBolsaAux.PUMercado = 0;
                    posicaoBolsaAux.ValorMercado = 0;
                    //
                    this.posicaoBolsaCollection1.AttachEntity(posicaoBolsaAux);

                    // Preenche os valores das Colunas Virtuais no DataTable
                    this.posicaoBolsaCollection1[0].SetColumn("NomeCliente", posicaoBolsaAux.NomeCliente);
                    this.posicaoBolsaCollection1[0].SetColumn("NomeAgenteMercado", posicaoBolsaAux.NomeAgenteMercado);
                }
            }
            // Zera o Filtro
            this.posicaoBolsaCollection1.Filter = "";

            #endregion

            // Ordena Por CdAtivoBolsa
            this.posicaoBolsaCollection1.Sort = PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa + " ASC";

            for (int i = this.posicaoBolsaCollection1.Count - 1; i >= 0; i--)
            {
                PosicaoBolsa posicaoBolsaDel = posicaoBolsaCollection1[i];
                int idCliente = posicaoBolsaDel.IdCliente.Value;
                Cliente clienteStatus = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteStatus.Query.DataDia);
                campos.Add(clienteStatus.Query.StatusAtivo);
                clienteStatus.LoadByPrimaryKey(campos, idCliente);

                if (!clienteStatus.IsAtivo || this.dataPosicao > clienteStatus.DataDia.Value)
                {
                    this.posicaoBolsaCollection1.DetachEntity(posicaoBolsaDel);
                }
            }

            return this.posicaoBolsaCollection1.LowLevelBind();
        }

        /// <summary>
        /// Realiza a consulta na PosicaoBolsaHistorico
        /// </summary>
        /// <returns></returns>
        private DataView ConsultaPosicaoBolsaHistorico() {
            this.tipoPesquisa = TipoPesquisa.PosicaoBolsaHistorico;

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            #region SQL

            this.posicaoBolsaHistoricoCollection1.QueryReset();

            /* Query com join */
            ClienteQuery cliente = new ClienteQuery("C");
            PosicaoBolsaHistoricoQuery posicaoBolsaHistorico = new PosicaoBolsaHistoricoQuery("P");
            AgenteMercadoQuery agenteMercado = new AgenteMercadoQuery("A");
            PermissaoClienteQuery permissaoCliente = new PermissaoClienteQuery("P1");

            posicaoBolsaHistorico.Select(posicaoBolsaHistorico.IdPosicao,
                         posicaoBolsaHistorico.IdCliente,
                         posicaoBolsaHistorico.CdAtivoBolsa,
                         posicaoBolsaHistorico.IdAgente,
                         posicaoBolsaHistorico.PUCustoLiquido, posicaoBolsaHistorico.PUMercado,
                         posicaoBolsaHistorico.ValorMercado,
                         cliente.Nome.As("NomeCliente"),
                         agenteMercado.Nome.As("NomeAgenteMercado"));

            posicaoBolsaHistorico.InnerJoin(cliente).On(posicaoBolsaHistorico.IdCliente == cliente.IdCliente);
            posicaoBolsaHistorico.InnerJoin(agenteMercado).On(posicaoBolsaHistorico.IdAgente == agenteMercado.IdAgente);
            posicaoBolsaHistorico.InnerJoin(permissaoCliente).On(permissaoCliente.IdCliente == cliente.IdCliente);

            posicaoBolsaHistorico.Where(posicaoBolsaHistorico.DataHistorico == this.dataPosicao,
                                        posicaoBolsaHistorico.IdCliente == this.idCliente,
                                        permissaoCliente.IdUsuario == idUsuario);

            if (!String.IsNullOrEmpty(this.cdAtivoBolsa)) {
                posicaoBolsaHistorico.Where(posicaoBolsaHistorico.CdAtivoBolsa.Like(this.cdAtivoBolsa + "%"));
            }

            posicaoBolsaHistorico.OrderBy(posicaoBolsaHistorico.CdAtivoBolsa.Ascending);
           
            this.posicaoBolsaHistoricoCollection1.Load(posicaoBolsaHistorico);
           
            #endregion
            //
            // Union da PosicaoBolsaHistorico Com posicaoBolsa Abertura
            /* Para cada PosicaoBolsaAbertura que não está na posicaoBolsaistorico adiciono na Posicão Bolsa
             */
            #region Union com PosicaoBolsaAbertura
            this.posicaoBolsaAberturaCollection1.QueryReset();

            PosicaoBolsaAberturaQuery posicaoBolsaAbertura = new PosicaoBolsaAberturaQuery("P");
            ClienteQuery cliente1 = new ClienteQuery("C");
            PermissaoClienteQuery permissaoCliente1 = new PermissaoClienteQuery("P1");

            posicaoBolsaAbertura.Select(posicaoBolsaAbertura.IdPosicao,
                            posicaoBolsaAbertura.IdCliente,
                            posicaoBolsaAbertura.CdAtivoBolsa,
                            posicaoBolsaAbertura.IdAgente,
                            posicaoBolsaAbertura.PUCustoLiquido,
                            posicaoBolsaAbertura.PUMercado,
                            posicaoBolsaAbertura.ValorMercado
                           );

            posicaoBolsaAbertura.InnerJoin(cliente1).On(posicaoBolsaAbertura.IdCliente == cliente1.IdCliente);
            posicaoBolsaAbertura.InnerJoin(permissaoCliente1).On(permissaoCliente1.IdCliente == cliente1.IdCliente);
            //
            posicaoBolsaAbertura.Where(permissaoCliente1.IdUsuario == idUsuario,
                                       posicaoBolsaAbertura.DataHistorico == this.dataPosicao,
                                       posicaoBolsaAbertura.IdCliente == this.idCliente
                                       );
                        
            if (!String.IsNullOrEmpty(this.cdAtivoBolsa)) {
                posicaoBolsaAbertura.Where(posicaoBolsaAbertura.CdAtivoBolsa.Like(this.cdAtivoBolsa + "%"));
            }

            posicaoBolsaAbertura.OrderBy(posicaoBolsaAbertura.CdAtivoBolsa.Ascending);
           
            this.posicaoBolsaAberturaCollection1.Load(posicaoBolsaAbertura);
            
            for (int i = 0; i < this.posicaoBolsaAberturaCollection1.Count; i++) {
                PosicaoBolsaAbertura posicaoBolsaAberturaAux = new PosicaoBolsaAbertura();
                posicaoBolsaAberturaAux = posicaoBolsaAberturaCollection1[i];

                // se nao existe na posicaoBolsaHistorico então adiciono
                // Zera o Filtro
                this.posicaoBolsaHistoricoCollection1.Filter = "";
                // Atualiza o filtro
                StringBuilder filtro = new StringBuilder();
                filtro.Append(PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente).Append("= ").Append(posicaoBolsaAberturaAux.IdCliente).Append(" AND ");
                filtro.Append(PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente).Append("= ").Append(posicaoBolsaAberturaAux.IdAgente).Append(" AND ");
                filtro.Append(PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa).Append("= '").Append(posicaoBolsaAberturaAux.CdAtivoBolsa).Append("'");
                //
                this.posicaoBolsaHistoricoCollection1.Filter = filtro.ToString();
                // Adiciono PosicaoBolsaAbertura na PosicaoBolsaHistorico
                if (this.posicaoBolsaHistoricoCollection1.Count == 0) {
                    PosicaoBolsaHistorico posicaoBolsaHistoricoAux = new PosicaoBolsaHistorico();
                    posicaoBolsaHistoricoAux.IdCliente = posicaoBolsaAberturaAux.IdCliente;
                    posicaoBolsaHistoricoAux.IdAgente = posicaoBolsaAberturaAux.IdAgente;
                    posicaoBolsaHistoricoAux.CdAtivoBolsa = posicaoBolsaAberturaAux.CdAtivoBolsa;
                    //
                    posicaoBolsaHistoricoAux.PUCustoLiquido = 0;
                    posicaoBolsaHistoricoAux.PUMercado = 0;
                    posicaoBolsaHistoricoAux.ValorMercado = 0;
                    //
                    posicaoBolsaHistoricoAux.NomeCliente = posicaoBolsaHistoricoAux.UpToClienteByIdCliente.Nome;
                    posicaoBolsaHistoricoAux.NomeAgenteMercado = posicaoBolsaHistoricoAux.UpToAgenteMercadoByIdAgente.Nome;
                    //
                    this.posicaoBolsaHistoricoCollection1.AttachEntity(posicaoBolsaHistoricoAux);

                    // Preenche os valores das Colunas Virtuais no DataTable
                    this.posicaoBolsaHistoricoCollection1[0].SetColumn("NomeCliente", posicaoBolsaHistoricoAux.NomeCliente);
                    this.posicaoBolsaHistoricoCollection1[0].SetColumn("NomeAgenteMercado", posicaoBolsaHistoricoAux.NomeAgenteMercado);
                }
            }
            // Zera o Filtro
            this.posicaoBolsaHistoricoCollection1.Filter = "";

            #endregion
            // Ordena Por CdAtivoBolsa
            this.posicaoBolsaHistoricoCollection1.Sort = PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa + " ASC";

            for (int i = this.posicaoBolsaHistoricoCollection1.Count - 1; i >= 0; i--)
            {
                PosicaoBolsaHistorico posicaoBolsaDel = posicaoBolsaHistoricoCollection1[i];
                int idCliente = posicaoBolsaDel.IdCliente.Value;
                Cliente clienteStatus = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteStatus.Query.DataDia);
                campos.Add(clienteStatus.Query.StatusAtivo);
                clienteStatus.LoadByPrimaryKey(campos, idCliente);

                if (!clienteStatus.IsAtivo || this.dataPosicao > clienteStatus.DataDia.Value)
                {
                    this.posicaoBolsaHistoricoCollection1.DetachEntity(posicaoBolsaDel);
                }
            }

            return this.posicaoBolsaHistoricoCollection1.LowLevelBind();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Bolsa/ReportPosicaoCustodia.resx";
        */
        private void InitializeComponent() {
            string resourceFileName = "ReportPosicaoCustodia.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataPosicao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.posicaoBolsaCollection1 = new Financial.Bolsa.PosicaoBolsaCollection();
            this.posicaoBolsaAberturaCollection1 = new Financial.Bolsa.PosicaoBolsaAberturaCollection();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.posicaoBolsaHistoricoCollection1 = new Financial.Bolsa.PosicaoBolsaHistoricoCollection();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.subReportRodape1 = new Financial.Relatorio.SubReportRodape();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 50F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1850F, 50F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell4,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell6,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CdAtivoBolsa")});
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.08216216216216217;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Especificacao";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.093513513513513488;
            this.xrTableCell4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.EspecificacaoBeforePrint);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "QuantidadeAbertura";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 0.11243243243243248;
            this.xrTableCell2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeAberturaBeforePrint);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Entradas";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 0.11567567567567572;
            this.xrTableCell5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.EntradasBeforePrint);
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Saidas";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.1140540540540541;
            this.xrTableCell3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SaidasBeforePrint);
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "QuantidadeFechamento";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.1262162162162162;
            this.xrTableCell6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeFechamentoBeforePrint);
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PUCustoLiquido", "{0:n8}")});
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell26.Weight = 0.1258108108108108;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PUMercado", "{0:n8}")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.12614864864864867;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorMercado", "{0:n2}")});
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.10398648648648649;
            this.xrTableCell28.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrPanel1,
            this.xrLabel1,
            this.xrTable3});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 280F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportLogotipo1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(725F, 64F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(47.08332F, 225F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.reportSemDados1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrSubreport1.Visible = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1820F, 116F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(127F, 42F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrTable5,
            this.xrPageInfo2,
            this.xrTable6});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 111F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1042F, 100F);
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(217F, 5F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(146F, 40F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(661F, 42F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCellDataPosicao});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.CanShrink = true;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "#DataPosicao";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell21.Weight = 0.32829046898638425;
            // 
            // xrTableCellDataPosicao
            // 
            this.xrTableCellDataPosicao.CanShrink = true;
            this.xrTableCellDataPosicao.Dpi = 254F;
            this.xrTableCellDataPosicao.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataPosicao.Name = "xrTableCellDataPosicao";
            this.xrTableCellDataPosicao.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataPosicao.StylePriority.UseFont = false;
            this.xrTableCellDataPosicao.Text = "DataPosicao";
            this.xrTableCellDataPosicao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataPosicao.Weight = 0.67170953101361575;
            this.xrTableCellDataPosicao.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataPosicaoBeforePrint);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(370F, 3F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(217F, 42F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.CanShrink = true;
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "#DataEmissao";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell23.Weight = 1;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(847F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1101F, 85F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "#TituloRelatorio";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 225F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1850F, 55F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell22,
            this.xrTableCell25});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.CanGrow = false;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.Text = "#Codigo";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell13.Weight = 0.082162162162162142;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.CanGrow = false;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = "#Especificacao";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell14.Weight = 0.093513513513513488;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.CanGrow = false;
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.Text = "#QtdeAbertura";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell15.Weight = 0.11243243243243248;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.CanGrow = false;
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.Text = "#Entradas";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell16.Weight = 0.11567567567567572;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.CanGrow = false;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "#Saidas";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell17.Weight = 0.1140540540540541;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.CanGrow = false;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.Text = "#QtdeFechamento";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell18.Weight = 0.1262162162162162;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "#PuCusto";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell20.Weight = 0.1258108108108108;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "#PuMercado";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell22.Weight = 0.12614864864864867;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "#ValorMercado";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell25.Weight = 0.10398648648648649;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 85F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subReportRodape1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(254F, 64F);
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeCliente", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader2.HeightF = 45F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader2.RepeatEveryPage = true;
            this.GroupHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderClienteBeforePrint);
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable4.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1850F, 45F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.StylePriority.UseBorderColor = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "Cliente";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell19.Weight = 1;
            this.xrTableCell19.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClienteBeforePrint);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NomeAgenteMercado", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 50F;
            this.GroupHeader1.Level = 2;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GroupHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderAgenteMercadoBeforePrint);
            // 
            // xrTable7
            // 
            this.xrTable7.BackColor = System.Drawing.Color.LightGray;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1850F, 50F);
            this.xrTable7.StylePriority.UseBackColor = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NomeAgenteMercado")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell24.Weight = 1;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1850F, 50F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.08216216216216217;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.093513513513513488;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "QuantidadeTotal";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell9.Weight = 0.11243243243243248;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "EntradasTotal";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell10.Weight = 0.11567567567567572;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "SaidasTotal";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.1140540540540541;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "QuantidadeFechamentoTotal";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.1262162162162162;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PUCustoLiquido")});
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:n8}";
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell29.Summary = xrSummary1;
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.1258108108108108;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PUMercado")});
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n8}";
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell30.Summary = xrSummary2;
            this.xrTableCell30.Text = "xrTableCell30";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.12614864864864867;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorMercado")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell31.Summary = xrSummary3;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.10398648648648649;
            this.xrTableCell31.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 77F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 10F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(1850F, 50F);
            this.xrTable8.StylePriority.UseBackColor = false;
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalAgenteMercadoBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow8.BorderColor = System.Drawing.SystemColors.ControlText;
            this.xrTableRow8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.StylePriority.UseBackColor = false;
            this.xrTableRow8.StylePriority.UseBorderColor = false;
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "#Total";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell32.Weight = 0.16270270270270271;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell33.Weight = 0.01297297297297295;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "TotalAgenteMercado";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.11243243243243248;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "TotalAgenteMercado";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.11567567567567572;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "TotalAgenteMercado";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.1140540540540541;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "TotalAgenteMercado";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.1262162162162162;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n8}";
            this.xrTableCell38.Summary = xrSummary4;
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.1258108108108108;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            xrSummary5.FormatString = "{0:n8}";
            this.xrTableCell39.Summary = xrSummary5;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.12614864864864867;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ValorMercado")});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            xrSummary6.FormatString = "{0:n2}";
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell40.Summary = xrSummary6;
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell40.Weight = 0.10398648648648649;
            this.xrTableCell40.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // posicaoBolsaCollection1
            // 
            this.posicaoBolsaCollection1.AllowDelete = true;
            this.posicaoBolsaCollection1.AllowEdit = true;
            this.posicaoBolsaCollection1.AllowNew = true;
            this.posicaoBolsaCollection1.EnableHierarchicalBinding = true;
            this.posicaoBolsaCollection1.Filter = "";
            this.posicaoBolsaCollection1.RowStateFilter = ((System.Data.DataViewRowState)(((System.Data.DataViewRowState.Unchanged | System.Data.DataViewRowState.Added)
                        | System.Data.DataViewRowState.ModifiedCurrent)));
            this.posicaoBolsaCollection1.Sort = "";
            // 
            // posicaoBolsaAberturaCollection1
            // 
            this.posicaoBolsaAberturaCollection1.AllowDelete = true;
            this.posicaoBolsaAberturaCollection1.AllowEdit = true;
            this.posicaoBolsaAberturaCollection1.AllowNew = true;
            this.posicaoBolsaAberturaCollection1.EnableHierarchicalBinding = true;
            this.posicaoBolsaAberturaCollection1.Filter = "";
            this.posicaoBolsaAberturaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoBolsaAberturaCollection1.Sort = "";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter2.Dpi = 254F;
            this.GroupFooter2.HeightF = 77F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // posicaoBolsaHistoricoCollection1
            // 
            this.posicaoBolsaHistoricoCollection1.AllowDelete = true;
            this.posicaoBolsaHistoricoCollection1.AllowEdit = true;
            this.posicaoBolsaHistoricoCollection1.AllowNew = true;
            this.posicaoBolsaHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoBolsaHistoricoCollection1.Filter = "";
            this.posicaoBolsaHistoricoCollection1.RowStateFilter = ((System.Data.DataViewRowState)(((System.Data.DataViewRowState.Unchanged | System.Data.DataViewRowState.Added)
                        | System.Data.DataViewRowState.ModifiedCurrent)));
            this.posicaoBolsaHistoricoCollection1.Sort = "";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportPosicaoCustodia
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader2,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupFooter2,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.posicaoBolsaCollection1;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 150, 150);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportPosicaoCustodia.ResourceManager;
        }

        #region Variaveis Internas do Relatorio
        protected class ValoresTotais {
            public decimal totalQuantidadeAbertura = 0;
            public decimal totalQuantidadeFechamento = 0;
            public decimal totalEntradas = 0;
            public decimal totalSaidas = 0;
        }
        //
        private ValoresTotais valoresTotal = new ValoresTotais();
        private ValoresTotais valoresTotalPorAgenteMercado = new ValoresTotais();
        #endregion

        private string agenteMercadoCorrente = "";

        // Controla a Quebra de Pagina do Grupo IdCliente 
        private int idClienteGrupo = 0;

        // Controla a Quebra de Pagina do Grupo AgenteMercado 
        private string nomeAgenteMercadoGrupo = "";

        #region Funções Customizadas

        // Pega a Data da posição
        private void DataPosicaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataPosicaoTableCell = sender as XRTableCell;
            dataPosicaoTableCell.Text = this.dataPosicao.ToString("d");
        }

        private void ClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellCliente = sender as XRTableCell;
            xrTableCellCliente.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                int idCliente = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);
                string nomeCliente = (string)this.GetCurrentColumnValue("NomeCliente");
                //
                xrTableCellCliente.Text = idCliente + " - " + nomeCliente;
            }
        }

        private void EspecificacaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellEspecificacao = sender as XRTableCell;
            xrTableCellEspecificacao.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.Especificacao);
                //
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                string especificacao = "";
                if (ativoBolsa.es.HasData) {
                    especificacao = !String.IsNullOrEmpty(ativoBolsa.Especificacao) ? ativoBolsa.Especificacao : "";
                }
                //
                xrTableCellEspecificacao.Text = especificacao;
            }
        }

        private void EntradasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellEntradas = sender as XRTableCell;
            xrTableCellEntradas.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                int idAgenteLiquidacao = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdAgente);
                int idClienteAux = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);

                #region Busca o Sum da quantidade de operacaoBolsa por cliente,igAgenteLiquidacao,data,tipoOperacao, cdAtivoBolsa
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.QueryReset();
                operacaoBolsa.Query
                     .Select(operacaoBolsa.Query.Quantidade.Sum())
                     .Where(operacaoBolsa.Query.Data.Equal(this.dataPosicao),
                            operacaoBolsa.Query.IdCliente == idClienteAux,
                            operacaoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa,
                            operacaoBolsa.Query.IdAgenteLiquidacao == idAgenteLiquidacao,
                            operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Deposito));
                operacaoBolsa.Query.Load();
                #endregion

                decimal entradas = 0;
                if (operacaoBolsa.es.HasData) {
                    entradas = operacaoBolsa.Quantidade.HasValue ? operacaoBolsa.Quantidade.Value : 0;
                }
                this.valoresTotal.totalEntradas += entradas;
                this.valoresTotalPorAgenteMercado.totalEntradas += entradas;
                //
                xrTableCellEntradas.Text = entradas.ToString("n0");
            }
        }

        private void SaidasBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellSaidas = sender as XRTableCell;
            xrTableCellSaidas.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                int idAgenteLiquidacao = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdAgente);
                int idClienteAux = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);

                #region Busca o Sum da quantidade de operacaoBolsa por cliente,igAgenteLiquidacao,data,tipoOperacao,cdAtivoBolsa
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.QueryReset();
                operacaoBolsa.Query
                     .Select(operacaoBolsa.Query.Quantidade.Sum())
                     .Where(operacaoBolsa.Query.Data.Equal(this.dataPosicao),
                            operacaoBolsa.Query.IdCliente == idClienteAux,
                            operacaoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa,
                            operacaoBolsa.Query.IdAgenteLiquidacao == idAgenteLiquidacao,
                            operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.Retirada));
                operacaoBolsa.Query.Load();
                #endregion

                decimal saidas = 0;
                if (operacaoBolsa.es.HasData) {
                    saidas = operacaoBolsa.Quantidade.HasValue ? operacaoBolsa.Quantidade.Value : 0;
                }
                this.valoresTotal.totalSaidas += saidas;
                this.valoresTotalPorAgenteMercado.totalSaidas += saidas;
                //
                xrTableCellSaidas.Text = saidas.ToString("n0");
            }
        }

        private void QuantidadeAberturaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellQuantidadeAbertura = sender as XRTableCell;
            xrTableCellQuantidadeAbertura.Text = "";

            /* Procura na PosicaoBolsaAbertura por idCliente, cdativoBolsa e IdAgente e dataHistorico
               Se nao achar, quantidadeAbertura = 0
             */
            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                int idAgente = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdAgente);
                int idClienteAux = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);

                #region Busca PosicaoBolsaAbertura por idCliente, cdativoBolsa e IdAgente
                PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();
                posicaoBolsaAbertura.QueryReset();
                posicaoBolsaAbertura.Query
                     .Select(posicaoBolsaAbertura.Query.Quantidade)
                     .Where(posicaoBolsaAbertura.Query.IdCliente == idClienteAux,
                            posicaoBolsaAbertura.Query.CdAtivoBolsa == cdAtivoBolsa,
                            posicaoBolsaAbertura.Query.IdAgente == idAgente,
                            posicaoBolsaAbertura.Query.DataHistorico == this.dataPosicao);
                posicaoBolsaAbertura.Query.Load();
                #endregion

                decimal qtdAbertura = 0;
                if (posicaoBolsaAbertura.es.HasData) {
                    qtdAbertura = posicaoBolsaAbertura.Quantidade.HasValue ? posicaoBolsaAbertura.Quantidade.Value : 0;
                }

                this.valoresTotal.totalQuantidadeAbertura += qtdAbertura;
                this.valoresTotalPorAgenteMercado.totalQuantidadeAbertura += qtdAbertura;
                //
                ReportBase.ConfiguraSinalNegativo(xrTableCellQuantidadeAbertura, qtdAbertura, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
            }
        }

        private void QuantidadeFechamentoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell xrTableCellQuantidadeFechamento = sender as XRTableCell;
            xrTableCellQuantidadeFechamento.Text = "";

            /* Procura na PosicaoBolsa por idCliente, cdativoBolsa e IdAgente
               Se nao achar, quantidadeFechamento = 0
             */
            if (this.numeroLinhasDataTable != 0) {
                string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                int idAgente = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdAgente);
                int idClienteAux = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);

                decimal qtdFechamento = 0;

                // Busca na PosicaoBolsa                
                if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsa) {

                    Cliente clienteAux = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(clienteAux.Query.DataDia);
                    clienteAux.LoadByPrimaryKey(campos, idClienteAux);

                    if (clienteAux.DataDia.Value == this.dataPosicao) {
                        #region Se o Cliente do Grupo atual está na data dia

                        #region Busca PosicaoBolsa por idCliente, cdativoBolsa e IdAgente
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.QueryReset();
                        posicaoBolsa.Query
                             .Select(posicaoBolsa.Query.Quantidade)
                             .Where(posicaoBolsa.Query.IdCliente == idClienteAux,
                                    posicaoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa,
                                    posicaoBolsa.Query.IdAgente == idAgente);
                        posicaoBolsa.Query.Load();
                        #endregion

                        if (posicaoBolsa.es.HasData) {
                            qtdFechamento = posicaoBolsa.Quantidade.HasValue ? posicaoBolsa.Quantidade.Value : 0;
                        }

                        #endregion
                    }
                    else {
                        #region Se o Cliente do Grupo atual não está na data dia

                        #region Busca PosicaoBolsaHistorico por idCliente, cdativoBolsa e IdAgente
                        PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                        posicaoBolsaHistorico.QueryReset();
                        posicaoBolsaHistorico.Query
                             .Select(posicaoBolsaHistorico.Query.Quantidade)
                             .Where(posicaoBolsaHistorico.Query.IdCliente == idClienteAux,
                                    posicaoBolsaHistorico.Query.CdAtivoBolsa == cdAtivoBolsa,
                                    posicaoBolsaHistorico.Query.IdAgente == idAgente,
                                    posicaoBolsaHistorico.Query.DataHistorico == this.dataPosicao);
                        posicaoBolsaHistorico.Query.Load();
                        #endregion

                        if (posicaoBolsaHistorico.es.HasData) {
                            qtdFechamento = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                        }

                        #endregion
                    }

                    this.valoresTotal.totalQuantidadeFechamento += qtdFechamento;
                    //
                    this.valoresTotalPorAgenteMercado.totalQuantidadeFechamento += qtdFechamento;
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico) {
                    #region Busca PosicaoBolsaHistorico por idCliente, cdativoBolsa e IdAgente
                    PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    posicaoBolsaHistorico.QueryReset();
                    posicaoBolsaHistorico.Query
                         .Select(posicaoBolsaHistorico.Query.Quantidade)
                         .Where(posicaoBolsaHistorico.Query.IdCliente == idClienteAux,
                                posicaoBolsaHistorico.Query.CdAtivoBolsa == cdAtivoBolsa,
                                posicaoBolsaHistorico.Query.IdAgente == idAgente,
                                posicaoBolsaHistorico.Query.DataHistorico == this.dataPosicao);
                    posicaoBolsaHistorico.Query.Load();
                    #endregion

                    if (posicaoBolsaHistorico.es.HasData) {
                        qtdFechamento = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                    }

                    this.valoresTotal.totalQuantidadeFechamento += qtdFechamento;
                    this.valoresTotalPorAgenteMercado.totalQuantidadeFechamento += qtdFechamento;
                    //
                }

                ReportBase.ConfiguraSinalNegativo(xrTableCellQuantidadeFechamento, qtdFechamento, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
            }
        }

        private void GroupHeaderClienteBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                // Só Zerar quando não for quebra de Pagina
                int controleIdClienteGrupo = (int)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.IdCliente);

                if (controleIdClienteGrupo != this.idClienteGrupo) {
                    // Salva o IdCliente do Grupo Atual
                    this.idClienteGrupo = controleIdClienteGrupo;
                    //
                    this.valoresTotal.totalEntradas = 0;
                    this.valoresTotal.totalSaidas = 0;
                    this.valoresTotal.totalQuantidadeAbertura = 0;
                    this.valoresTotal.totalQuantidadeFechamento = 0;
                }
            }
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            
            XRTable totalXRTableCell = sender as XRTable;

            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(totalXRTableCell);
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha 0 
                /* 2-QuantidadeAbertura
                   3-Entradas
                   4-Saidas
                   5-QuantidadeFechamento               
                 */

                #region Linha 0
                XRTableRow totalRow0 = totalXRTableCell.Rows[0];
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[2]), this.valoresTotal.totalQuantidadeAbertura, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                //
                ((XRTableCell)totalRow0.Cells[3]).Text = this.valoresTotal.totalEntradas.ToString("n0");
                ((XRTableCell)totalRow0.Cells[4]).Text = this.valoresTotal.totalSaidas.ToString("n0");
                //
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[5]), this.valoresTotal.totalQuantidadeFechamento, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                #endregion

                // Total AgenteMercado = Soma Totais Parciais dos Clientes
                //this.valoresTotalPorAgenteMercado.totalQuantidadeAbertura += this.valoresTotal.totalQuantidadeAbertura;
                //this.valoresTotalPorAgenteMercado.totalEntradas += this.valoresTotal.totalEntradas;
                //this.valoresTotalPorAgenteMercado.totalSaidas += this.valoresTotal.totalSaidas;
                //this.valoresTotalPorAgenteMercado.totalQuantidadeFechamento += this.valoresTotal.totalQuantidadeFechamento;
            }
        }

        private void TableTotalAgenteMercadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {

            XRTable totalXRTableCell = sender as XRTable;

            #region Limpa os Dados da Tabela
            ReportBase.LimpaDadosTable(totalXRTableCell);
            #endregion

            if (this.numeroLinhasDataTable != 0) {
                // Prenche Linha 0 
                /* 2-QuantidadeAbertura
                   3-Entradas
                   4-Saidas
                   5-QuantidadeFechamento               
                 */

                #region Linha 0
                XRTableRow totalRow0 = totalXRTableCell.Rows[0];
                //                
                ((XRTableCell)totalRow0.Cells[0]).Text = Resources.ReportPosicaoCustodia._Total + ": " + agenteMercadoCorrente;
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[2]), this.valoresTotalPorAgenteMercado.totalQuantidadeAbertura, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                //
                ((XRTableCell)totalRow0.Cells[3]).Text = this.valoresTotalPorAgenteMercado.totalEntradas.ToString("n0");
                ((XRTableCell)totalRow0.Cells[4]).Text = this.valoresTotalPorAgenteMercado.totalSaidas.ToString("n0");
                //
                ReportBase.ConfiguraSinalNegativo(((XRTableCell)totalRow0.Cells[5]), this.valoresTotalPorAgenteMercado.totalQuantidadeFechamento, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
                #endregion
            }
        }

        private void GroupHeaderAgenteMercadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {           
            if (this.numeroLinhasDataTable != 0) {
                // Salva o Agente Mercado Corrente
                this.agenteMercadoCorrente = (string)this.GetCurrentColumnValue("NomeAgenteMercado");

                // Só Zerar quando não for quebra de Pagina
                string controleAgenteMercadoGrupo = (string)this.GetCurrentColumnValue("NomeAgenteMercado");

                if (controleAgenteMercadoGrupo.Trim() != this.nomeAgenteMercadoGrupo.Trim()) {
                    // Salva o AgenteMercado do Grupo Atual
                    this.nomeAgenteMercadoGrupo = controleAgenteMercadoGrupo;
                    //
                    this.valoresTotalPorAgenteMercado.totalEntradas = 0;
                    this.valoresTotalPorAgenteMercado.totalSaidas = 0;
                    this.valoresTotalPorAgenteMercado.totalQuantidadeAbertura = 0;
                    this.valoresTotalPorAgenteMercado.totalQuantidadeFechamento = 0;
                    //
                    // Quando trocar AgenteMercado zera também os totais de Cliente
                    this.valoresTotal.totalEntradas = 0;
                    this.valoresTotal.totalSaidas = 0;
                    this.valoresTotal.totalQuantidadeAbertura = 0;
                    this.valoresTotal.totalQuantidadeFechamento = 0;
                }
            }

        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        #endregion        
    }
}