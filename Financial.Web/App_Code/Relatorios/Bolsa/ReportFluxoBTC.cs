﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Globalization;
using Financial.Fundo;
using Financial.Fundo.Exceptions;
using Financial.InvestidorCotista;
using Financial.Util;
using Financial.Common.Enums;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using EntitySpaces.Core;
using System.Text;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for ReportFluxoBTC
    /// </summary>
    public class ReportFluxoBTC : XtraReport {
        private int idCliente;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private enum TipoPesquisa {
            PosicaoEmprestimoBolsa = 0,
            PosicaoEmprestimoBolsaHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoEmprestimoBolsa;
        //
        private enum TipoPesquisaBolsa {
            PosicaoBolsa = 0,
            PosicaoBolsaHistorico = 1
        }
        //
        private List<DateTime> diasExecucao; // Contém os dias D0-D1-D2-D3 para projeção dos empréstimos
        //
        private List<DateTime> datasDisponivel; // Contém três dias p/ trás em relação a data  de Referencia para Calculo do Disponivel

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell34;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTableCell xrTableCell2;
        private XRPanel xrPanel1;
        private XRPageInfo xrPageInfo3;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCellDataInicio;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRPageInfo xrPageInfo1;
        private XRPageInfo xrPageInfo2;
        private XRSubreport xrSubreport1;
        private SubReportLogotipo subReportLogotipo1;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable8;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell66;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell53;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell40;
        private XRTableCell xrTableCell41;
        private Financial.Bolsa.PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection1;
        private Financial.Bolsa.PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection1;
        private GroupFooterBand GroupFooter1;
        private XRSubreport xrSubreport2;
        private XRSubreport xrSubreport3;
        private ReportSemDados reportSemDados1;
        private SubReportRodapeLandScape subReportRodapeLandScape1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Construtor com parametros
        public ReportFluxoBTC(int idCliente, DateTime data) {
            this.idCliente = idCliente;
            this.data = data;
            //

            #region Define D0-D1-D2-D3- Acima de D3
            this.diasExecucao = new List<DateTime>(new DateTime[] { 
                data,
                Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),
                Calendario.AdicionaDiaUtil(data, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),
                Calendario.AdicionaDiaUtil(data, 3, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),
                Calendario.AdicionaDiaUtil(data, 4, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)
            });
            #endregion

            #region Define D-3,D-2,D-1,D0-D1-D2-D3,Acima de D3
            this.datasDisponivel = new List<DateTime>(new DateTime[] { 
                Calendario.SubtraiDiaUtil(data, 3, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),  // D-3
                Calendario.SubtraiDiaUtil(data, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),  // D-2
                Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil),  // D-1
                data,                                                                              // D0
                Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil), // D1
                Calendario.AdicionaDiaUtil(data, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil), // D2
                Calendario.AdicionaDiaUtil(data, 3, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil), // D3
                Calendario.AdicionaDiaUtil(data, 4, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil), // Acima de D3
            });
            #endregion

            this.InitializeComponent();
            this.PersonalInitialize();

            // Configura o Relatorio
            ReportBase relatorioBase = new ReportBase(this);

            // Configura o tamanho da linha do subReport
            this.subReportRodapeLandScape1.PersonalizaLinhaRodape(2385);

            // Tratamento para Report sem dados
            this.SetRelatorioSemDados();
        }

        private void PersonalInitialize() {
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;
        }

        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                // Desaparece com as todas as bandas menos o subreport                
                this.xrSubreport3.Visible = true;
                //
                this.xrTable7.Visible = false;
                this.xrTable6.Visible = false;
            }
        }

        private DataTable FillDados() {
            // Define se faz Consulta PosicaoEmprestimoBolsa ou PosicaoEmprestimoBolsaHistorico
            Cliente cliente = new Cliente();

            this.tipoPesquisa = cliente.IsClienteNaData(this.idCliente, this.data)
                                ? TipoPesquisa.PosicaoEmprestimoBolsa
                                : TipoPesquisa.PosicaoEmprestimoBolsaHistorico;

            DataTable dataTable = new DataTable();

            #region SQL
            if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                #region PosicaoEmprestimoBolsa
                this.posicaoEmprestimoBolsaCollection1.QueryReset();
                //
                this.posicaoEmprestimoBolsaCollection1.Query.es.Distinct = true;

                this.posicaoEmprestimoBolsaCollection1.Query
                     .Select(this.posicaoEmprestimoBolsaCollection1.Query.CdAtivoBolsa)
                     .Where(this.posicaoEmprestimoBolsaCollection1.Query.IdCliente == this.idCliente)
                     .OrderBy(this.posicaoEmprestimoBolsaCollection1.Query.CdAtivoBolsa.Ascending);

                dataTable = this.posicaoEmprestimoBolsaCollection1.Query.LoadDataTable();
                #endregion
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                #region PosicaoEmprestimoBolsaHistorico
                this.posicaoEmprestimoBolsaHistoricoCollection1.QueryReset();
                //
                this.posicaoEmprestimoBolsaHistoricoCollection1.Query.es.Distinct = true;

                this.posicaoEmprestimoBolsaHistoricoCollection1.Query
                     .Select(this.posicaoEmprestimoBolsaHistoricoCollection1.Query.CdAtivoBolsa)
                     .Where(this.posicaoEmprestimoBolsaHistoricoCollection1.Query.IdCliente == this.idCliente,
                            this.posicaoEmprestimoBolsaHistoricoCollection1.Query.DataHistorico == this.data)
                     .OrderBy(this.posicaoEmprestimoBolsaHistoricoCollection1.Query.CdAtivoBolsa.Ascending);

                dataTable = this.posicaoEmprestimoBolsaHistoricoCollection1.Query.LoadDataTable();
                #endregion
            }
            #endregion
            //            
            return dataTable;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ReportFluxoBTC.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "ReportFluxoBTC.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDataInicio = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.reportSemDados1 = new Financial.Relatorio.ReportSemDados();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportLogotipo1 = new Financial.Relatorio.SubReportLogotipo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subReportRodapeLandScape1 = new Financial.Relatorio.SubReportRodapeLandScape();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.posicaoEmprestimoBolsaCollection1 = new Financial.Bolsa.PosicaoEmprestimoBolsaCollection();
            this.posicaoEmprestimoBolsaHistoricoCollection1 = new Financial.Bolsa.PosicaoEmprestimoBolsaHistoricoCollection();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.xrTable5,
            this.xrTable4});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 146F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable8
            // 
            this.xrTable8.Dpi = 254F;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(100F, 95F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable8.SizeF = new System.Drawing.SizeF(2385F, 46F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            this.xrTableRow6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DadosDisponivelBeforePrint);
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.Text = "Disponível";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 0.064570230607966461;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.Text = "Abertura";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.079664570230607967;
            this.xrTableCell56.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell57.Text = "Liquidação";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.080083857442348014;
            this.xrTableCell57.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell58.Text = "Saldo";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.080083857442348014;
            this.xrTableCell58.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell59.Text = "Liquidação";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.088469601677148846;
            this.xrTableCell59.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "Saldo";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.088888888888888892;
            this.xrTableCell60.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.Text = "Liquidação";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.097693920335429771;
            this.xrTableCell61.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "Saldo";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell62.Weight = 0.088469601677148846;
            this.xrTableCell62.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell63.Text = "Liquidação";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell63.Weight = 0.080083857442348014;
            this.xrTableCell63.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.Text = "Saldo";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell64.Weight = 0.088888888888888892;
            this.xrTableCell64.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell65.Text = "Liquidação";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell65.Weight = 0.079664570230607967;
            this.xrTableCell65.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell66.Text = "Saldo";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell66.Weight = 0.083438155136268344;
            this.xrTableCell66.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTable5
            // 
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 47F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2385F, 46F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            this.xrTableRow5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DadosTomadoBeforePrint);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Text = "Tomado";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell42.Weight = 0.064570230607966461;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.Text = "Tomado";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.079664570230607967;
            this.xrTableCell43.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TomadoD0BeforePrint);
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.Text = "Liquidação";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell44.Weight = 0.080083857442348014;
            this.xrTableCell44.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.Text = "Saldo";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.080083857442348014;
            this.xrTableCell45.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.Text = "Liquidação";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.088469601677148846;
            this.xrTableCell46.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Saldo";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.088888888888888892;
            this.xrTableCell47.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.Text = "Liquidação";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.097693920335429771;
            this.xrTableCell48.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "Saldo";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.088469601677148846;
            this.xrTableCell49.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell50.Text = "Liquidação";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.080083857442348014;
            this.xrTableCell50.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell51.Text = "Saldo";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.088888888888888892;
            this.xrTableCell51.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell52.Text = "Liquidação";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.079664570230607967;
            this.xrTableCell52.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.Text = "Saldo";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.083438155136268344;
            this.xrTableCell53.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2385F, 46F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            this.xrTableRow4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DadosDoadoBeforePrint);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "Doado";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.Weight = 0.064570230607966461;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.Text = "Doado";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.079664570230607967;
            this.xrTableCell30.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DoadoD0BeforePrint);
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell31.Text = "Liquidação";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.080083857442348014;
            this.xrTableCell31.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell32.Text = "Saldo";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.080083857442348014;
            this.xrTableCell32.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell33.Text = "Liquidação";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.088469601677148846;
            this.xrTableCell33.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "Saldo";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.088888888888888892;
            this.xrTableCell35.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "Liquidação";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.097693920335429771;
            this.xrTableCell36.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "Saldo";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.088469601677148846;
            this.xrTableCell37.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "Liquidação";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.080083857442348014;
            this.xrTableCell38.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.Text = "Saldo";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.088888888888888892;
            this.xrTableCell39.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.Text = "Liquidação";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell40.Weight = 0.079664570230607967;
            this.xrTableCell40.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "Saldo";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell41.Weight = 0.083438155136268344;
            this.xrTableCell41.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTable6
            // 
            this.xrTable6.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2385F, 46F);
            this.xrTable6.StylePriority.UseBackColor = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell13,
            this.xrTableCell12,
            this.xrTableCell6,
            this.xrTableCell14,
            this.xrTableCell22,
            this.xrTableCell11,
            this.xrTableCell17,
            this.xrTableCell16,
            this.xrTableCell18});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CdAtivoBolsa")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.064570230607966461;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "Abertura";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.079664570230607967;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "Liquidação";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.080083857442348014;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Saldo";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.080083857442348014;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Liquidação";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.088469601677148846;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Saldo";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 0.088888888888888892;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "Liquidação";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.097693920335429771;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Saldo";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.088469601677148846;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "Liquidação";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.080083857442348014;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "Saldo";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.088888888888888892;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.Text = "Liquidação";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell16.Weight = 0.079664570230607967;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "Saldo";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.083438155136268344;
            // 
            // xrPanel1
            // 
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo3,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(100F, 87F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1037F, 148F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 254F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo2.Format = "{0:HH:mm:ss}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(360F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(127F, 40F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo3.Format = "{0:d}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(212F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(148F, 40F);
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(931F, 43F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "Carteira:";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.22771213748657357;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.CanGrow = false;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.Text = "NomeCarteira";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 0.7722878625134264;
            this.xrTableCell8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.NomeCarteiraBeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 42F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(635F, 42F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCellDataInicio});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "Data Posição:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell4.Weight = 0.33385826771653543;
            // 
            // xrTableCellDataInicio
            // 
            this.xrTableCellDataInicio.CanGrow = false;
            this.xrTableCellDataInicio.Dpi = 254F;
            this.xrTableCellDataInicio.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDataInicio.Name = "xrTableCellDataInicio";
            this.xrTableCellDataInicio.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCellDataInicio.Text = "Data";
            this.xrTableCellDataInicio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCellDataInicio.Weight = 0.66614173228346452;
            this.xrTableCellDataInicio.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(212F, 42F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "Data Emissão:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 1;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport3,
            this.xrTable7,
            this.xrSubreport1,
            this.xrPageInfo1,
            this.xrTable10,
            this.xrPanel1});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 302F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Dpi = 254F;
            this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(50F, 270F);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.reportSemDados1;
            this.xrSubreport3.SizeF = new System.Drawing.SizeF(5F, 5F);
            this.xrSubreport3.Visible = false;
            // 
            // xrTable7
            // 
            this.xrTable7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 255F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2385F, 46F);
            this.xrTable7.StylePriority.UseBackColor = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CabecalhoDataBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell23,
            this.xrTableCell27,
            this.xrTableCell24,
            this.xrTableCell34,
            this.xrTableCell2,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 0.064570230607966461;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "D+0";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell28.Weight = 0.079664570230607967;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "D+0";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.080083857442348014;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.080083857442348014;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "D+1";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.088469601677148846;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.088888888888888892;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.Text = "D+2";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell27.Weight = 0.097693920335429771;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.088469601677148846;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "D+3";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.080083857442348014;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell2.Weight = 0.088888888888888892;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Acima D+3";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell9.Weight = 0.13333333333333333;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell10.Weight = 0.029769392033542976;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 254F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(101F, 21F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subReportLogotipo1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(767F, 64F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(2413F, 87F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(79F, 43F);
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable10
            // 
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(889F, 21F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1588F, 64F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell20.Weight = 0.14672544080604533;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "Demonstrativo Fluxo de Ativos BTC";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.85327455919395467;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 64F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 254F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subReportRodapeLandScape1;
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(296F, 64F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 46F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // posicaoEmprestimoBolsaCollection1
            // 
            this.posicaoEmprestimoBolsaCollection1.AllowDelete = true;
            this.posicaoEmprestimoBolsaCollection1.AllowEdit = true;
            this.posicaoEmprestimoBolsaCollection1.AllowNew = true;
            this.posicaoEmprestimoBolsaCollection1.EnableHierarchicalBinding = true;
            this.posicaoEmprestimoBolsaCollection1.Filter = "";
            this.posicaoEmprestimoBolsaCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoEmprestimoBolsaCollection1.Sort = "";
            // 
            // posicaoEmprestimoBolsaHistoricoCollection1
            // 
            this.posicaoEmprestimoBolsaHistoricoCollection1.AllowDelete = true;
            this.posicaoEmprestimoBolsaHistoricoCollection1.AllowEdit = true;
            this.posicaoEmprestimoBolsaHistoricoCollection1.AllowNew = true;
            this.posicaoEmprestimoBolsaHistoricoCollection1.EnableHierarchicalBinding = true;
            this.posicaoEmprestimoBolsaHistoricoCollection1.Filter = "";
            this.posicaoEmprestimoBolsaHistoricoCollection1.RowStateFilter = System.Data.DataViewRowState.None;
            this.posicaoEmprestimoBolsaHistoricoCollection1.Sort = "";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportFluxoBTC
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.DataSource = this.posicaoEmprestimoBolsaCollection1;
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(150, 150, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportSemDados1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogotipo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportRodapeLandScape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.ReportFluxoBTC.ResourceManager;
        }

        #region Funções Internas do Relatorio
        //
        #region Cabecalho
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell dataXRTableCell = sender as XRTableCell;
            dataXRTableCell.Text = this.data.ToString("d");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NomeCarteiraBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell nomeCliente = sender as XRTableCell;
            //
            Cliente cliente = new Cliente();

            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            cliente.LoadByPrimaryKey(this.idCliente);
            //
            nomeCliente.Text = this.idCliente + " - " + cliente.Apelido;
        }
        #endregion

        /// <summary>
        /// Preenche o Cabeçalho de Datas - D0-D1-D2-D3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CabecalhoDataBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable table = sender as XRTable;
            /*
             * [1] - D0 
             * [2] - D0
             * [4] - D1
             * [6] - D2
             * [8] - D3
             * [10] - Acima D3
             */
            XRTableRow linha0 = table.Rows[0];
            //
            ((XRTableCell)linha0.Cells[1]).Text = this.diasExecucao[0].ToString("d");
            ((XRTableCell)linha0.Cells[2]).Text = this.diasExecucao[0].ToString("d");
            ((XRTableCell)linha0.Cells[4]).Text = this.diasExecucao[1].ToString("d");
            ((XRTableCell)linha0.Cells[6]).Text = this.diasExecucao[2].ToString("d");
            ((XRTableCell)linha0.Cells[8]).Text = this.diasExecucao[3].ToString("d");
            ((XRTableCell)linha0.Cells[10]).Text = "Acima de " + this.diasExecucao[3].ToString("d");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Obsolete("Para o Calculo do Saldo é necessario essa variavel presente na linha de Dados ")]
        private void DoadoD0BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell doadoXRTableCell = sender as XRTableCell;
            //doadoXRTableCell.Text = "";

            //if (this.numeroLinhasDataTable != 0) {
            //    string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            //    PosicaoEmprestimoBolsaAbertura p = new PosicaoEmprestimoBolsaAbertura();
            //    p.Query.Select(p.Query.Quantidade.Sum())
            //           .Where(p.Query.IdCliente == this.idCliente &&
            //                  p.Query.CdAtivoBolsa == cdAtivoBolsa && 
            //                  p.Query.DataHistorico == this.diasExecucao[0] &&
            //                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
            //    //
            //    decimal quantidadeDoado = p.Query.Load()
            //                              ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
            //                              : 0.00M;

            //    doadoXRTableCell.Text = quantidadeDoado.ToString("n2");

            //    //XRTableCell liquidacao = doadoXRTableCell.NextCell;
            //    //XRTableCell saldo = liquidacao.NextCell;

            //    //saldo.Text = quantidadeDoado.ToString("n2") + liquidacao.Text;
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Obsolete("Para o Calculo do Saldo é necessario essa variavel presente na linha de Dados ")]
        private void TomadoD0BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            //XRTableCell tomadoXRTableCell = sender as XRTableCell;
            //tomadoXRTableCell.Text = "";

            //if (this.numeroLinhasDataTable != 0) {
            //    string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            //    PosicaoEmprestimoBolsaAbertura p = new PosicaoEmprestimoBolsaAbertura();
            //    p.Query.Select(p.Query.Quantidade.Sum())
            //           .Where(p.Query.IdCliente == this.idCliente &&
            //                  p.Query.CdAtivoBolsa == cdAtivoBolsa && 
            //                  p.Query.DataHistorico == this.diasExecucao[0] &&
            //                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
            //    //
            //    decimal quantidadeTomado = p.Query.Load() 
            //                               ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
            //                               : 0.00M;

            //    tomadoXRTableCell.Text = quantidadeTomado.ToString("n2");
            //}
        }

        /// <summary>
        /// Calcula Todos os Dados da Linha de Doado em D0-D1-D2 e Acima de D3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DadosDoadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableRow row0 = sender as XRTableRow;

            // Limpa Toda a Linha Menos a Primeira Coluna            
            int colunas = ((XRTableRow)row0.Table.Rows[0]).Cells.Count;
            for (int j = 1; j < colunas; j++) {
                ((XRTableCell)row0.Table.Rows[0].Cells[j]).Text = "";
            }

            #region Zera os Campos da Table
            //((XRTableCell)row0.Cells[2]).Text = "";
            //((XRTableCell)row0.Cells[4]).Text = "";
            //((XRTableCell)row0.Cells[6]).Text = "";
            //((XRTableCell)row0.Cells[8]).Text = "";
            //((XRTableCell)row0.Cells[10]).Text = "";
            #endregion
            //
            // Define se faz Consulta PosicaoEmprestimoBolsa ou PosicaoEmprestimoBolsaHistorico
            Cliente cliente = new Cliente();

            TipoPesquisa tipoPesquisa = cliente.IsClienteNaData(this.idCliente, this.data)
                                        ? TipoPesquisa.PosicaoEmprestimoBolsa
                                        : TipoPesquisa.PosicaoEmprestimoBolsaHistorico;
            //
            string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            //            
            decimal liquidacaoD0 = 0.00M;
            decimal liquidacaoD1 = 0.00M;
            decimal liquidacaoD2 = 0.00M;
            decimal liquidacaoD3 = 0.00M;
            decimal liquidacaoAcimaD3 = 0.00M;

            #region D0
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[0] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                //
                decimal entradaDoadoD0 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaDoado1 = 0.00M;
                decimal saidaDoado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[0] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[0] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[0] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[0] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaDoado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD0 = entradaDoadoD0 - saidaDoado1 - saidaDoado2;
                #endregion
            }
            #endregion

            #region D1
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[1] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                //
                decimal entradaDoadoD1 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaDoado1 = 0.00M;
                decimal saidaDoado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[1] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[1] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[1] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[1] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaDoado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD1 = entradaDoadoD1 - saidaDoado1 - saidaDoado2;
                #endregion
            }
            #endregion

            #region D2
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[2] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                //
                decimal entradaDoadoD2 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaDoado1 = 0.00M;
                decimal saidaDoado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[2] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[2] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[2] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[2] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaDoado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD2 = entradaDoadoD2 - saidaDoado1 - saidaDoado2;
                #endregion
            }
            #endregion

            #region D3
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[3] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                //
                decimal entradaDoadoD3 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaDoado1 = 0.00M;
                decimal saidaDoado2 = 0.00M;

                #region Saida1
                if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[3] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[3] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[3] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                }
                else if (this.tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[3] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaDoado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD3 = entradaDoadoD3 - saidaDoado1 - saidaDoado2;
                #endregion
            }
            #endregion

            #region Acima de D3
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro >= this.diasExecucao[4] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                //
                decimal entradaDoadoAcimaD3 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaDoado1 = 0.00M;
                decimal saidaDoado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento >= this.diasExecucao[4] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento >= this.diasExecucao[4] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);

                    saidaDoado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data >= this.diasExecucao[4] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data >= this.diasExecucao[4] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Doador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaDoado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoAcimaD3 = entradaDoadoAcimaD3 - saidaDoado1 - saidaDoado2;
                #endregion
            }
            #endregion

            #region Exibe Liquidação
            ((XRTableCell)row0.Cells[2]).Text = liquidacaoD0.ToString("N2"); // D0
            ((XRTableCell)row0.Cells[4]).Text = liquidacaoD1.ToString("N2"); // D1
            ((XRTableCell)row0.Cells[6]).Text = liquidacaoD2.ToString("N2"); // D2
            ((XRTableCell)row0.Cells[8]).Text = liquidacaoD3.ToString("N2"); // D3
            ((XRTableCell)row0.Cells[10]).Text = liquidacaoAcimaD3.ToString("N2"); // >D3
            #endregion

            #region Exibe Doado Abertura
            PosicaoEmprestimoBolsaAbertura pDoado = new PosicaoEmprestimoBolsaAbertura();
            pDoado.Query.Select(pDoado.Query.Quantidade.Sum())
                   .Where(pDoado.Query.IdCliente == this.idCliente &&
                          pDoado.Query.CdAtivoBolsa == cdAtivoBolsa &&
                          pDoado.Query.DataHistorico == this.diasExecucao[0] &&
                          pDoado.Query.PontaEmprestimo == PontaEmprestimoBolsa.Doador);
            //
            decimal quantidadeDoado = pDoado.Query.Load()
                                      ? pDoado.Quantidade != null ? pDoado.Quantidade.Value : 0.00M
                                      : 0.00M;

            ((XRTableCell)row0.Cells[1]).Text = quantidadeDoado.ToString("N2");
            #endregion

            #region Exibe Saldos
            decimal saldo1 = quantidadeDoado + liquidacaoD0;
            decimal saldo2 = saldo1 + liquidacaoD1;
            decimal saldo3 = saldo2 + liquidacaoD2;
            decimal saldo4 = saldo3 + liquidacaoD3;
            decimal saldo5 = saldo4 + liquidacaoAcimaD3;
            //
            ((XRTableCell)row0.Cells[3]).Text = saldo1.ToString("N2");
            ((XRTableCell)row0.Cells[5]).Text = saldo2.ToString("N2");
            ((XRTableCell)row0.Cells[7]).Text = saldo3.ToString("N2");
            ((XRTableCell)row0.Cells[9]).Text = saldo4.ToString("N2");
            ((XRTableCell)row0.Cells[11]).Text = saldo5.ToString("N2");
            #endregion
        }

        /// <summary>
        /// Calcula Todos os Dados da linha de Tomado em D0-D1-D2 e Acima de D3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DadosTomadoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableRow row0 = sender as XRTableRow;

            // Limpa Toda a Linha Menos a Primeira Coluna            
            int colunas = ((XRTableRow)row0.Table.Rows[0]).Cells.Count;
            for (int j = 1; j < colunas; j++) {
                ((XRTableCell)row0.Table.Rows[0].Cells[j]).Text = "";
            }

            #region Zera os Campos da Table
            //((XRTableCell)row0.Cells[2]).Text = "";
            //((XRTableCell)row0.Cells[4]).Text = "";
            //((XRTableCell)row0.Cells[6]).Text = "";
            //((XRTableCell)row0.Cells[8]).Text = "";
            //((XRTableCell)row0.Cells[10]).Text = "";
            #endregion
            //
            // Define se faz Consulta PosicaoEmprestimoBolsa ou PosicaoEmprestimoBolsaHistorico
            Cliente cliente = new Cliente();

            TipoPesquisa tipoPesquisa = cliente.IsClienteNaData(this.idCliente, this.data)
                                        ? TipoPesquisa.PosicaoEmprestimoBolsa
                                        : TipoPesquisa.PosicaoEmprestimoBolsaHistorico;
            //
            string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            //            
            decimal liquidacaoD0 = 0.00M;
            decimal liquidacaoD1 = 0.00M;
            decimal liquidacaoD2 = 0.00M;
            decimal liquidacaoD3 = 0.00M;
            decimal liquidacaoAcimaD3 = 0.00M;

            #region D0
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[0] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                //
                decimal entradaTomadoD0 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaTomado1 = 0.00M;
                decimal saidaTomado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[0] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[0] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[0] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[0] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaTomado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD0 = entradaTomadoD0 - saidaTomado1 - saidaTomado2;
                #endregion
            }
            #endregion

            #region D1
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[1] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                //
                decimal entradaTomadoD1 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaTomado1 = 0.00M;
                decimal saidaTomado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[1] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[1] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[1] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[1] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaTomado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD1 = entradaTomadoD1 - saidaTomado1 - saidaTomado2;
                #endregion
            }
            #endregion

            #region D2
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[2] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                //
                decimal entradaTomadoD2 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaTomado1 = 0.00M;
                decimal saidaTomado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[2] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[2] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[2] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[2] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaTomado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD2 = entradaTomadoD2 - saidaTomado1 - saidaTomado2;
                #endregion
            }
            #endregion

            #region D3
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro == this.diasExecucao[3] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                //
                decimal entradaTomadoD3 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaTomado1 = 0.00M;
                decimal saidaTomado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento == this.diasExecucao[3] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento == this.diasExecucao[3] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[3] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data == this.diasExecucao[3] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaTomado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoD3 = entradaTomadoD3 - saidaTomado1 - saidaTomado2;
                #endregion
            }
            #endregion

            #region Acima de D3
            if (this.numeroLinhasDataTable != 0) {

                #region Entradas
                OperacaoEmprestimoBolsa oper = new OperacaoEmprestimoBolsa();
                oper.Query.Select(oper.Query.Quantidade.Sum())
                       .Where(oper.Query.IdCliente == this.idCliente &&
                              oper.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              oper.Query.DataRegistro >= this.diasExecucao[4] &&
                              oper.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                //
                decimal entradaTomadoAcimaD3 = oper.Query.Load()
                                           ? oper.Quantidade != null ? oper.Quantidade.Value : 0.00M
                                           : 0.00M;
                #endregion

                #region Saidas
                //
                decimal saidaTomado1 = 0.00M;
                decimal saidaTomado2 = 0.00M;

                #region Saida1
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataVencimento >= this.diasExecucao[4] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                    p.Query.Select(p.Query.Quantidade.Sum())
                           .Where(p.Query.IdCliente == this.idCliente &&
                                  p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                                  p.Query.DataHistorico == this.data &&
                                  p.Query.DataVencimento >= this.diasExecucao[4] &&
                                  p.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);

                    saidaTomado1 = p.Query.Load()
                                  ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                                  : 0.00M;
                }
                #endregion

                #region Saida2
                LiquidacaoEmprestimoBolsaQuery l = new LiquidacaoEmprestimoBolsaQuery("L");
                //
                if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsa) {
                    PosicaoEmprestimoBolsaQuery p = new PosicaoEmprestimoBolsaQuery("P");
                    //                    
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data >= this.diasExecucao[4] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
                }
                else if (tipoPesquisa == TipoPesquisa.PosicaoEmprestimoBolsaHistorico) {
                    PosicaoEmprestimoBolsaHistoricoQuery p = new PosicaoEmprestimoBolsaHistoricoQuery("P");
                    //
                    l.Select(l.Quantidade.Sum());
                    //
                    l.InnerJoin(p).On(l.IdPosicao == p.IdPosicao);
                    //
                    l.Where(l.IdCliente == this.IdCliente &&
                            l.CdAtivoBolsa == cdAtivoBolsa &&
                            l.Data >= this.diasExecucao[4] &&
                            p.PontaEmprestimo == PontaEmprestimoBolsa.Tomador &&
                            p.DataHistorico == this.data);
                }

                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

                if (liquidacaoEmprestimoBolsaCollection.Load(l)) {
                    if (liquidacaoEmprestimoBolsaCollection[0].Quantidade != null) {
                        saidaTomado2 = liquidacaoEmprestimoBolsaCollection[0].Quantidade.Value;
                    }
                }
                #endregion

                #endregion

                #region Entradas - Saidas
                liquidacaoAcimaD3 = entradaTomadoAcimaD3 - saidaTomado1 - saidaTomado2;
                #endregion
            }
            #endregion

            #region Exibe Liquidação
            ((XRTableCell)row0.Cells[2]).Text = liquidacaoD0.ToString("N2");  // D0
            ((XRTableCell)row0.Cells[4]).Text = liquidacaoD1.ToString("N2");  // D1
            ((XRTableCell)row0.Cells[6]).Text = liquidacaoD2.ToString("N2");  // D2
            ((XRTableCell)row0.Cells[8]).Text = liquidacaoD3.ToString("N2");  // D3
            ((XRTableCell)row0.Cells[10]).Text = liquidacaoAcimaD3.ToString("N2"); // >D3
            #endregion

            #region Exibe Tomado Abertura
            PosicaoEmprestimoBolsaAbertura pTomado = new PosicaoEmprestimoBolsaAbertura();
            pTomado.Query.Select(pTomado.Query.Quantidade.Sum())
                   .Where(pTomado.Query.IdCliente == this.idCliente &&
                          pTomado.Query.CdAtivoBolsa == cdAtivoBolsa &&
                          pTomado.Query.DataHistorico == this.diasExecucao[0] &&
                          pTomado.Query.PontaEmprestimo == PontaEmprestimoBolsa.Tomador);
            //
            decimal quantidadeTomado = pTomado.Query.Load()
                                       ? pTomado.Quantidade != null ? pTomado.Quantidade.Value : 0.00M
                                       : 0.00M;

            ((XRTableCell)row0.Cells[1]).Text = quantidadeTomado.ToString("N2");
            #endregion

            #region Exibe Saldos
            decimal saldo1 = quantidadeTomado + liquidacaoD0;
            decimal saldo2 = saldo1 + liquidacaoD1;
            decimal saldo3 = saldo2 + liquidacaoD2;
            decimal saldo4 = saldo3 + liquidacaoD3;
            decimal saldo5 = saldo4 + liquidacaoAcimaD3;
            //
            ((XRTableCell)row0.Cells[3]).Text = saldo1.ToString("N2");
            ((XRTableCell)row0.Cells[5]).Text = saldo2.ToString("N2");
            ((XRTableCell)row0.Cells[7]).Text = saldo3.ToString("N2");
            ((XRTableCell)row0.Cells[9]).Text = saldo4.ToString("N2");
            ((XRTableCell)row0.Cells[11]).Text = saldo5.ToString("N2");
            #endregion
        }

        /// <summary>
        /// Calcula Todos os Dados da Linha de Disponível em D0-D1-D2 e Acima de D3 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DadosDisponivelBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableRow row0 = sender as XRTableRow;

            // Limpa Toda a Linha Menos a Primeira Coluna
            int colunas = ((XRTableRow)row0.Table.Rows[0]).Cells.Count;
            for (int j = 1; j < colunas; j++) {
                ((XRTableCell)row0.Table.Rows[0].Cells[j]).Text = "";
            }
            //
            // Define se faz Consulta PosicaoBolsa ou PosicaoBolsaHistorico
            Cliente cliente = new Cliente();

            TipoPesquisaBolsa tipoPesquisaBolsa = cliente.IsClienteNaData(this.idCliente, this.data)
                                        ? TipoPesquisaBolsa.PosicaoBolsa
                                        : TipoPesquisaBolsa.PosicaoBolsaHistorico;
            //
            string cdAtivoBolsa = (string)this.GetCurrentColumnValue(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);

            #region Calcula Abertura Disponível D0
            //
            decimal quantidadePosicao = 0.00M;

            #region QuantidadePosicao
            if (tipoPesquisaBolsa == TipoPesquisaBolsa.PosicaoBolsa) {
                PosicaoBolsa p = new PosicaoBolsa();

                p.Query.Select(p.Query.Quantidade.Sum())
                       .Where(p.Query.IdCliente == this.idCliente &&
                              p.Query.CdAtivoBolsa == cdAtivoBolsa);

                quantidadePosicao = p.Query.Load()
                              ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                              : 0.00M;
            }
            else if (tipoPesquisaBolsa == TipoPesquisaBolsa.PosicaoBolsaHistorico) {
                PosicaoBolsaHistorico p = new PosicaoBolsaHistorico();

                p.Query.Select(p.Query.Quantidade.Sum())
                       .Where(p.Query.IdCliente == this.idCliente &&
                              p.Query.CdAtivoBolsa == cdAtivoBolsa &&
                              p.Query.DataHistorico == this.data);

                quantidadePosicao = p.Query.Load()
                              ? p.Quantidade != null ? p.Quantidade.Value : 0.00M
                              : 0.00M;
            }
            #endregion

            #region Net = (Operações Compra - Operações Venda)
            OperacaoBolsa operCompras = new OperacaoBolsa();
            operCompras.Query.Select(operCompras.Query.Quantidade.Sum())
                   .Where(operCompras.Query.IdCliente == this.idCliente &&
                          operCompras.Query.CdAtivoBolsa == cdAtivoBolsa &&
                          operCompras.Query.TipoOperacao == TipoOperacaoBolsa.Compra &&
                          operCompras.Query.Data.Between(this.datasDisponivel[0], this.datasDisponivel[2]));

            decimal quantidadeCompras = operCompras.Query.Load()
                          ? operCompras.Quantidade != null ? operCompras.Quantidade.Value : 0.00M
                          : 0.00M;
            //
            OperacaoBolsa operVendas = new OperacaoBolsa();
            operVendas.Query.Select(operVendas.Query.Quantidade.Sum())
                   .Where(operVendas.Query.IdCliente == this.idCliente &&
                          operVendas.Query.CdAtivoBolsa == cdAtivoBolsa &&
                          operVendas.Query.TipoOperacao == TipoOperacaoBolsa.Venda &&
                          operVendas.Query.Data.Between(this.datasDisponivel[0], this.datasDisponivel[2]));

            decimal quantidadeVendas = operVendas.Query.Load()
                          ? operVendas.Quantidade != null ? operVendas.Quantidade.Value : 0.00M
                          : 0.00M;

            decimal net = quantidadeCompras - quantidadeVendas;

            #endregion

            decimal quantidadeDisponivelAbertura = quantidadePosicao - net;

            #endregion

            #region Exibe Disponivel Abertura
            ((XRTableCell)row0.Cells[1]).Text = quantidadeDisponivelAbertura.ToString("N2");
            #endregion

            #region Calcula Liquidacao D0/D1/D2/D3/Acima de D3
            //            
            #region Metodo com várias Consultas Deprecated
            //OperacaoBolsa operComprasD0 = new OperacaoBolsa();
            //operComprasD0.Query.Select(operComprasD0.Query.Quantidade.Sum())
            //       .Where(operComprasD0.Query.IdCliente == this.idCliente &&
            //              operComprasD0.Query.CdAtivoBolsa == cdAtivoBolsa &&
            //              operComprasD0.Query.TipoOperacao == TipoOperacaoBolsa.Compra &&
            //              operComprasD0.Query.Data == this.datasDisponivel[0]);

            //decimal quantidadeComprasD0 = operComprasD0.Query.Load()
            //              ? operComprasD0.Quantidade != null ? operComprasD0.Quantidade.Value : 0.00M
            //              : 0.00M;
            ////
            //OperacaoBolsa operVendasD0 = new OperacaoBolsa();
            //operVendasD0.Query.Select(operVendasD0.Query.Quantidade.Sum())
            //       .Where(operVendasD0.Query.IdCliente == this.idCliente &&
            //              operVendasD0.Query.CdAtivoBolsa == cdAtivoBolsa &&
            //              operVendasD0.Query.TipoOperacao == TipoOperacaoBolsa.Venda &&
            //              operVendasD0.Query.Data == this.datasDisponivel[0]);

            //decimal quantidadeVendasD0 = operVendasD0.Query.Load()
            //              ? operVendasD0.Quantidade != null ? operVendasD0.Quantidade.Value : 0.00M
            //              : 0.00M;

            //OperacaoBolsa operDepositosD0 = new OperacaoBolsa();
            //operDepositosD0.Query.Select(operDepositosD0.Query.Quantidade.Sum())
            //       .Where(operDepositosD0.Query.IdCliente == this.idCliente &&
            //              operDepositosD0.Query.CdAtivoBolsa == cdAtivoBolsa &&
            //              operDepositosD0.Query.TipoOperacao == TipoOperacaoBolsa.Deposito &&
            //              operDepositosD0.Query.Data == this.datasDisponivel[3]);

            //decimal quantidadeDepositosD0 = operDepositosD0.Query.Load()
            //              ? operDepositosD0.Quantidade != null ? operDepositosD0.Quantidade.Value : 0.00M
            //              : 0.00M;

            //OperacaoBolsa operRetiradaD0 = new OperacaoBolsa();
            //operRetiradaD0.Query.Select(operRetiradaD0.Query.Quantidade.Sum())
            //       .Where(operRetiradaD0.Query.IdCliente == this.idCliente &&
            //              operRetiradaD0.Query.CdAtivoBolsa == cdAtivoBolsa &&
            //              operRetiradaD0.Query.TipoOperacao == TipoOperacaoBolsa.Retirada &&
            //              operRetiradaD0.Query.Data == this.datasDisponivel[3]);

            //decimal quantidadeRetiradasD0 = operRetiradaD0.Query.Load()
            //              ? operRetiradaD0.Quantidade != null ? operRetiradaD0.Quantidade.Value : 0.00M
            //              : 0.00M;

            #endregion

            OperacaoBolsaCollection operPrincipal = new OperacaoBolsaCollection();
            operPrincipal.Query.Select(operPrincipal.Query.Quantidade, operPrincipal.Query.TipoOperacao,
                                       operPrincipal.Query.Data)
                   .Where(operPrincipal.Query.IdCliente == this.idCliente &&
                          operPrincipal.Query.CdAtivoBolsa == cdAtivoBolsa);

            operPrincipal.Query.Load();

            #region CalculaD0
            #region Operacao Compra
            // Aplica o Filtro de TipoOperacao e Data
            StringBuilder filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[0] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeComprasD0 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Venda
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[0] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeVendasD0 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Depositos
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[3] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeDepositosD0 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Retiradas
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[3] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeRetiradasD0 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion
            #endregion

            #region CalculaD1
            #region Operacao Compra
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[1] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeComprasD1 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Venda
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[1] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeVendasD1 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Depositos
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[4] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeDepositosD1 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Retiradas
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[4] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeRetiradasD1 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion
            #endregion

            #region CalculaD2
            #region Operacao Compra
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[2] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeComprasD2 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Venda
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[2] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeVendasD2 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Depositos
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[5] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeDepositosD2 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Retiradas
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[5] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeRetiradasD2 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion
            #endregion

            #region CalculaD3
            #region Operacao Compra
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[3] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeComprasD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Venda
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[3] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeVendasD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Depositos
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[6] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeDepositosD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Retiradas
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");
            filtro.Append(" AND Data = '" + this.datasDisponivel[6] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeRetiradasD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion
            #endregion

            #region Calcula Acima de D3
            #region Operacao Compra
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Compra + "'");
            filtro.Append(" AND Data >= '" + this.datasDisponivel[4] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeComprasAcimaD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Venda
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Venda + "'");
            filtro.Append(" AND Data >= '" + this.datasDisponivel[4] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeVendasAcimaD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Depositos
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Deposito + "'");
            filtro.Append(" AND Data >= '" + this.datasDisponivel[7] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeDepositosAcimaD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion

            #region Operacao Retiradas
            // Aplica o Filtro de TipoOperacao e Data
            filtro = new StringBuilder();
            filtro.Append("TipoOperacao = '" + TipoOperacaoBolsa.Retirada + "'");
            filtro.Append(" AND Data >= '" + this.datasDisponivel[7] + "'");
            //                         
            operPrincipal.Filter = filtro.ToString();

            // Faz a Somatorio
            decimal quantidadeRetiradasAcimaD3 = this.SumQuantidade(operPrincipal);

            // Zera o Filtro
            operPrincipal.Filter = "";
            #endregion
            #endregion

            decimal quantidadeNetOperacaoD0 = quantidadeComprasD0 + quantidadeDepositosD0 - quantidadeVendasD0 - quantidadeRetiradasD0;
            decimal quantidadeNetOperacaoD1 = quantidadeComprasD1 + quantidadeDepositosD1 - quantidadeVendasD1 - quantidadeRetiradasD1;
            decimal quantidadeNetOperacaoD2 = quantidadeComprasD2 + quantidadeDepositosD2 - quantidadeVendasD2 - quantidadeRetiradasD2;
            decimal quantidadeNetOperacaoD3 = quantidadeComprasD3 + quantidadeDepositosD3 - quantidadeVendasD3 - quantidadeRetiradasD3;
            decimal quantidadeNetOperacaoAcimaD3 = quantidadeComprasAcimaD3 + quantidadeDepositosAcimaD3 - quantidadeVendasAcimaD3 - quantidadeRetiradasAcimaD3;

            #endregion

            #region Exibe Liquidação
            ((XRTableCell)row0.Cells[2]).Text = quantidadeNetOperacaoD0.ToString("N2");       // D0
            ((XRTableCell)row0.Cells[4]).Text = quantidadeNetOperacaoD1.ToString("N2");       // D1
            ((XRTableCell)row0.Cells[6]).Text = quantidadeNetOperacaoD2.ToString("N2");       // D2
            ((XRTableCell)row0.Cells[8]).Text = quantidadeNetOperacaoD3.ToString("N2");       // D3
            ((XRTableCell)row0.Cells[10]).Text = quantidadeNetOperacaoAcimaD3.ToString("N2"); // >D3
            #endregion

            #region Exibe Saldos
            decimal saldo1 = quantidadeDisponivelAbertura + quantidadeNetOperacaoD0;
            decimal saldo2 = saldo1 + quantidadeNetOperacaoD1;
            decimal saldo3 = saldo2 + quantidadeNetOperacaoD2;
            decimal saldo4 = saldo3 + quantidadeNetOperacaoD3;
            decimal saldo5 = saldo4 + quantidadeNetOperacaoAcimaD3;
            //
            ((XRTableCell)row0.Cells[3]).Text = saldo1.ToString("N2");
            ((XRTableCell)row0.Cells[5]).Text = saldo2.ToString("N2");
            ((XRTableCell)row0.Cells[7]).Text = saldo3.ToString("N2");
            ((XRTableCell)row0.Cells[9]).Text = saldo4.ToString("N2");
            ((XRTableCell)row0.Cells[11]).Text = saldo5.ToString("N2");
            #endregion
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }


        /// <summary>
        /// Somatoria do campo Quantidade da operacaoBolsa
        /// </summary>
        /// <param name="oper"></param>
        /// <returns></returns>
        private decimal SumQuantidade(OperacaoBolsaCollection oper) {
            decimal quantidade = 0.00M;

            for (int i = 0; i < oper.Count; i++) {
                quantidade += oper[i].Quantidade.Value;
            }
            return quantidade;
        }
        #endregion
    }
}