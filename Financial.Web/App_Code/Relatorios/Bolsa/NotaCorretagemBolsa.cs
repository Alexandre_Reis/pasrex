﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Common;
using Financial.CRM;
using Financial.Util;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.RendaFixa.Enums;
using EntitySpaces.Core;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa.Custom1 {
    public class NotaCorretagemBolsa {

        #region Privates
        private OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();

        private int idCliente;
        private DateTime dataNota;
        private int idAgenteCorretora;
        
        private string tipoMercado;

        /* Header Interno */
        private Header header = new Header();

        /* Footer Interno */
        private Footer footer = new Footer();

        /* Lista interna de Detalhes */
        private List<Detail> lista = new List<Detail>(); 
        #endregion

        /// <summary>
        /// Total de registros dessa nota
        /// </summary>
        /// <returns></returns>
        public int GetQuantidadeRegistros() {
            return this.operacaoBolsaCollection.Count;            
        }

        #region Properties
        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public DateTime DataNota {
            get { return dataNota; }
            set { dataNota = value; }
        }

        public int IdAgenteCorretora {
            get { return idAgenteCorretora; }
            set { idAgenteCorretora = value; }
        }

        public string TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }
        #endregion

        #region Acesso Externo
        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Header GetHeader {
            get { return this.header; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Footer GetFooter {
            get { return this.footer; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public List<Detail> GetListDetail {
            get { return this.lista; }
        }
        #endregion

        #region Construtores
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataNota"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="tipoMercado">VIS_TER_IMO, OPC_OPV, VIS, OPC, OPV, TER, IMO</param>
        public NotaCorretagemBolsa(int idCliente, DateTime dataNota, int idAgenteCorretora, string tipoMercado) {

            this.idCliente = idCliente;
            //
            this.dataNota = dataNota;
            //
            this.idAgenteCorretora = idAgenteCorretora;
            //
            this.tipoMercado = tipoMercado;

            #region Consulta OperacaoBolsa
            this.operacaoBolsaCollection = new OperacaoBolsaCollection();

            OperacaoBolsaQuery op = new OperacaoBolsaQuery("OP");
            ClienteBolsaQuery cb = new ClienteBolsaQuery("C");
            ClienteQuery cliente = new ClienteQuery("C1");
            AtivoBolsaQuery ativoBolsa = new AtivoBolsaQuery("A");
            PessoaQuery pessoa = new PessoaQuery("P1");

            op.Select(op, ativoBolsa.Descricao, ativoBolsa.DataVencimento, ativoBolsa.Especificacao, ativoBolsa.CdAtivoBolsaObjeto);
            op.InnerJoin(cb).On(op.IdCliente == cb.IdCliente);
            op.InnerJoin(cliente).On(op.IdCliente == cliente.IdCliente);
            op.InnerJoin(ativoBolsa).On(op.CdAtivoBolsa == ativoBolsa.CdAtivoBolsa);
            op.InnerJoin(pessoa).On(cliente.IdPessoa == pessoa.IdPessoa);

            op.Where(op.IdCliente == idCliente,
                     op.Data == this.dataNota,
                     op.IdAgenteCorretora == idAgenteCorretora,                                          
                     op.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda, 
                                        TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade));

            if (this.tipoMercado == "VIS_TER_IMO") {
                op.Where(op.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Termo, TipoMercadoBolsa.Imobiliario));
            }
            else if (this.tipoMercado == "OPC_OPV") {
                op.Where(op.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            }
            else {
                op.Where(op.TipoMercado == this.tipoMercado);
            }
            //
            this.operacaoBolsaCollection.Load(op);
            
            #endregion

            this.SetaHeader();
            this.SetaDetail();
            this.SetaFooter();
        }        
        #endregion

        #region Inner Class
        public class Header {

            public Header() {
            }

            public Header(int? a, DateTime b,
                          string c, string d, string e,
                          string f, string g, string h,
                          string i, string j, string k,
                          string l, string m, string n,
                          string o, string p, string q,
                          string r, string s, string t, string u) {

                this.numeroNota = a;
                this.dataPregao = b;
                this.nomeAgente = c;
                this.enderecoCompletoAgente = d;
                this.telefoneCompletoAgente = e;
                this.siteAgente = f;
                this.emailAgente = g;
                this.cnpjAgente = h;
                this.cartaPatente = i;
                this.codigoCliente = j;
                this.cidadeCliente = k;
                this.nomeCliente = l;
                this.enderecoCliente = m;
                this.bairroCliente = n;
                this.cepCliente = o;
                this.ufCliente = p;
                this.telefoneCliente = q;
                this.cpfCNPJCliente = r;
                this.codigoBovespaAgente = s;
                this.codigoAssessor = t;
                this.nomeAssessor = u;
            }

            //Dados da nota
            public int? numeroNota;
            public DateTime dataPregao;
            
            //Dados da corretora
            public string nomeAgente;
            public string enderecoCompletoAgente;
            public string telefoneCompletoAgente;
            public string faxAgente;
            public string siteAgente;
            public string emailAgente;
            public string cnpjAgente;
            public string cartaPatente;
            public string telefoneOuvidoria;
            public string emailOuvidoria;
            
            //Dados do cliente
            public string codigoCliente;
            public string nomeCliente;
            public string enderecoCliente;
            public string bairroCliente;
            public string cepCliente;
            public string cidadeCliente;
            public string ufCliente;
            public string telefoneCliente;
            public string cpfCNPJCliente;
            public string codigoBovespaAgente; //Está na seção de cliente pois aparece no quadro de codigo de cliente            

            public string pessoaVinculada;

            //Dados do Assessor
            public string codigoAssessor;
            public string nomeAssessor;
        }

        public class Detail {
            public Detail() { }


            public Detail(string a, string b, string c, string d, string e, string f, string g,
                          decimal h, decimal i, decimal j) {

                this.tipoOperacao = a;
                this.tipoMercado = b;
                this.prazo = c;
                this.especificacao = d;
                this.especificacao1 = e;
                this.especificacaoOpcao = f;
                this.observacao = g;
                //
                this.quantidade = h;
                this.preco = i;
                this.valor = j;
            }

            private string tipoOperacao;
            private string tipoMercado; 
            private string prazo;
            private string especificacao;
            private string especificacao1; // PNA-PNB
            private string especificacaoOpcao; // para Mercado de Opções - 4 primeiras letras do ativo ou vazio
            private string observacao;
            private decimal quantidade;
            private decimal preco;
            private decimal valor;

            #region Properties Necessarias para o Binding do Grid
            public string TipoOperacao {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            public string TipoMercado {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            public string Prazo {
                get { return prazo; }
                set { prazo = value; }
            }

            public string Especificacao {
                get { return especificacao; }
                set { especificacao = value; }
            }

            public string Especificacao1 {
                get { return especificacao1; }
                set { especificacao1 = value; }
            }

            public string EspecificacaoOpcao {
                get { return especificacaoOpcao; }
                set { especificacaoOpcao = value; }
            }

            public string Observacao {
                get { return observacao; }
                set { observacao = value; }
            }

            public decimal Quantidade {
                get { return quantidade; }
                set { quantidade = value; }
            }

            public decimal Preco {
                get { return preco; }
                set { preco = value; }
            }

            public decimal Valor {
                get { return valor; }
                set { valor = value; }
            }
            #endregion
        }

        public class Footer {
            public Footer() { }

            public Footer(decimal a, decimal b, decimal c,
                          decimal d, decimal e, decimal f) {
                this.vendasVista = a;
                this.comprasVista = b;
                this.valorLiquido = c;
                this.corretagem = d;
                this.iss = e;
                this.ir = f;
            }

            // Para OPC - OPV - OPC_OPV
            public decimal vendasOpcoes;
            public decimal comprasOpcoes;
            //
            public decimal valorTermo;
            //
            public decimal vendasVista;
            public decimal comprasVista;
            public decimal valorLiquido;
            public decimal corretagem;
            public decimal iss;
            public decimal ir;
            //
            public string cidadeAdministrador = "";
            //
            public decimal emolumentos;
            //
            public decimal liquidacaoCBLC;
            public decimal registroCBLC;
            public decimal registroBolsa;
        }

        #endregion
               
        #region Funções
        private void SetaHeader() {

            this.header.dataPregao = this.dataNota;

            if (this.GetQuantidadeRegistros() > 0) {
                if (this.operacaoBolsaCollection[0].NumeroNota.HasValue) {
                    this.header.numeroNota = this.operacaoBolsaCollection[0].NumeroNota.Value;
                }
            }

            #region Dados Corretora
		    AgenteMercado a = new AgenteMercado();
            a.LoadByPrimaryKey(this.idAgenteCorretora);

            #region DDD
            string ddd = "";
            if (!String.IsNullOrEmpty(a.str.Ddd)) {
                ddd = "(" + a.str.Ddd + ") ";
            } 
            #endregion

            #region CidadeEstado
            string cidadeEstado = "";
            string cidade = "";
            if (!String.IsNullOrEmpty(a.str.Cidade)) {
                cidadeEstado += a.str.Cidade;
                cidade = a.str.Cidade;
            }
            if (!String.IsNullOrEmpty(a.str.Uf)) {
                cidadeEstado += " - " + a.str.Uf;
            }
            #endregion

            #region Endereco
            string endereco = "";
            if (!String.IsNullOrEmpty(a.str.Endereco)) {
                endereco += a.str.Endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Bairro)) {
                if (!String.IsNullOrEmpty(a.str.Endereco)) {
                    endereco += ", ";
                }
                endereco += a.str.Bairro;
            }
            #endregion

            #region EnderecoCompleto
            string enderecoCompleto = "";
            if (!String.IsNullOrEmpty(endereco)) {
                enderecoCompleto += endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Cep)) {
                enderecoCompleto += "                      " + a.str.Cep;
            }
            if (!String.IsNullOrEmpty(cidadeEstado)) {

                if (String.IsNullOrEmpty(a.str.Cep)) {
                    enderecoCompleto += "                      " + cidadeEstado;                                        
                }
                else {
                    enderecoCompleto += " " + cidadeEstado;
                }
            }
            #endregion
            // Adapta tamanho maximo
            if (enderecoCompleto.Length >= 97) {
                if (enderecoCompleto.Contains("                      ")) {
                    enderecoCompleto = enderecoCompleto.Replace("                      ", " ");
                }
            }
            //
            this.footer.cidadeAdministrador = cidade;

            //this.header.nomeAgente = enderecoCompleto.Length.ToString();
            //
            this.header.nomeAgente = a.Nome.Trim();            
            this.header.enderecoCompletoAgente = enderecoCompleto;            
            this.header.telefoneCompletoAgente = !String.IsNullOrEmpty(a.str.Telefone) ? ddd + a.str.Telefone : "";
            this.header.faxAgente = !String.IsNullOrEmpty(a.str.Fax) ? ddd + a.str.Fax : "";
            this.header.siteAgente = a.str.Website;
            this.header.emailAgente = a.str.Email;
            this.header.cnpjAgente = a.str.Cnpj == String.Empty ? "" : Utilitario.MascaraCNPJ(a.str.Cnpj);
            this.header.cartaPatente = a.str.CartaPatente;
            this.header.telefoneOuvidoria = a.str.TelOuvidoria;
            this.header.emailOuvidoria = a.str.EmailOuvidoria;
	        #endregion       

            #region Dados Cliente
            
            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
            Cliente cli = new Cliente();
            cli.LoadByPrimaryKey(this.idCliente);
            p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro, p.Cidade, p.Cep, p.Uf, p.IdPessoa);
            p.Where(p.RecebeCorrespondencia == "S" & p.IdPessoa == cli.IdPessoa);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            string enderecoCompletoCliente = "";
            string cep = "";
            string cidadeEstadoCliente = "";
            string telefone = "";
            //
            if (pessoaEnderecoCollection.HasData) {
                #region EnderecoCompleto
                string enderecoCliente = pessoaEnderecoCollection[0].str.Endereco.Trim();

                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim())) {
                    enderecoCliente += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + "  - " + pessoaEnderecoCollection[0].str.Complemento.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim())) {
                    enderecoCliente += " " + pessoaEnderecoCollection[0].str.Bairro.Trim();
                }

                enderecoCompletoCliente = enderecoCliente; 
                #endregion

                #region Cep
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim())) {
                    cep += Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
                } 
                #endregion

                #region CidadeEstado
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim())) {
                    cidadeEstadoCliente += pessoaEnderecoCollection[0].str.Cidade.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
                    cidadeEstadoCliente += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
                }                
                #endregion

                Pessoa pes = new Pessoa();
                pes.LoadByPrimaryKey(pessoaEnderecoCollection[0].IdPessoa.Value);
               
                #region Telefone
                if (!String.IsNullOrEmpty(pes.DDD_Fone)) {
                    telefone += pes.DDD_Fone;
                }
                #endregion
            }

            Pessoa b = new Pessoa();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(b.Query.IdPessoa);
            campos.Add(b.Query.Nome);
            campos.Add(b.Query.PessoaVinculada);
            b.LoadByPrimaryKey(campos, this.idCliente);

            this.header.pessoaVinculada = b.PessoaVinculada;
            this.header.codigoCliente = this.idCliente.ToString();
            this.header.nomeCliente = b.Nome;
            this.header.enderecoCliente = enderecoCompletoCliente;
            this.header.telefoneCliente = telefone;
            this.header.cepCliente = cep;
            this.header.cidadeCliente = cidadeEstadoCliente;
            //

            #region CNPJ
            string cpfcnpj = "";
            //
            Pessoa pessoa = new Pessoa();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);
            
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }
            #endregion

            this.header.cpfCNPJCliente = cpfcnpj;
            this.header.codigoBovespaAgente = a.CodigoBovespa.HasValue ? a.CodigoBovespa.Value.ToString() : "";            
            if (a.DigitoCodigoBovespa.HasValue)
            {
                this.header.codigoBovespaAgente = this.header.codigoBovespaAgente + "-" + a.DigitoCodigoBovespa.Value.ToString().Substring(0, 1);
            }
            #endregion

            #region Dados Assessor
            ClienteBolsa c = new ClienteBolsa();
            c.LoadByPrimaryKey(this.idCliente);

            string nomeAssessor = "";
            if (c.IdAssessor.HasValue) {
                Assessor s = new Assessor();
                if(s.LoadByPrimaryKey(c.IdAssessor.Value)){
                    nomeAssessor = s.str.Nome;
                }
                this.header.codigoAssessor = c.IdAssessor.Value.ToString();
                this.header.nomeAssessor = nomeAssessor;
            }            
            #endregion
        }

        private void SetaFooter() {
            decimal totalVendas = 0, totalCompras = 0, totalValorLiquido = 0,
                    totalCorretagem = 0, totalIss = 0, totalIR = 0,
                    totalVendasOpcoes = 0, totalComprasOpcoes = 0,
                    totalValorTermo = 0, totalEmolumentos = 0,
                    totalLiquidacaoCBLC = 0, totalRegistroCBLC = 0, totalRegistroBolsa = 0;

            #region VIS - TER - IMO
            if (this.tipoMercado == "VIS_TER_IMO" ||
                this.tipoMercado == TipoMercadoBolsa.MercadoVista ||
                this.tipoMercado == TipoMercadoBolsa.Termo ||
                this.tipoMercado == TipoMercadoBolsa.Imobiliario) {

                for (int i = 0; i < this.operacaoBolsaCollection.Count; i++) {
                    string tipoMercadoOper = operacaoBolsaCollection[i].TipoMercado; // Somente VIS/TER/Imobilario

                    if (tipoMercadoOper == TipoMercadoBolsa.MercadoVista || tipoMercadoOper == TipoMercadoBolsa.Imobiliario) {

                        if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Compra || operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.CompraDaytrade) {
                            totalCompras += operacaoBolsaCollection[i].Valor.Value;
                        }

                        if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Venda || operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.VendaDaytrade) {
                            totalVendas += operacaoBolsaCollection[i].Valor.Value;
                        }
                    }

                    if (tipoMercadoOper == TipoMercadoBolsa.Termo) {
                        totalValorTermo += operacaoBolsaCollection[i].Valor.Value;
                    }

                    totalEmolumentos += operacaoBolsaCollection[i].Emolumento.Value;
                    //totalIR += operacaoRendaFixaCollection[i].ValorIR.Value;
                    totalIss += operacaoBolsaCollection[i].ValorISS.Value;
                    totalCorretagem += operacaoBolsaCollection[i].Corretagem.Value;
                    //
                    totalLiquidacaoCBLC += operacaoBolsaCollection[i].LiquidacaoCBLC.Value;
                    totalRegistroCBLC += operacaoBolsaCollection[i].RegistroBolsa.Value;
                    totalRegistroBolsa += operacaoBolsaCollection[i].RegistroCBLC.Value;
                }
            } 
            #endregion

            #region Opcoes
            else { // OPC - OPV - OPC_OPV
                
                for (int i = 0; i < this.operacaoBolsaCollection.Count; i++) {
                    //string tipoMercadoOper = operacaoBolsaCollection[i].TipoMercado; // Somente OPC - OPV

                    if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Compra || operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.CompraDaytrade) {
                        totalComprasOpcoes += operacaoBolsaCollection[i].Valor.Value;
                    }

                    if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Venda || operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.VendaDaytrade) {
                        totalVendasOpcoes += operacaoBolsaCollection[i].Valor.Value;
                    }

                    totalEmolumentos += operacaoBolsaCollection[i].Emolumento.Value;
                    //totalValorLiquido += operacaoRendaFixaCollection[i].ValorLiquido.Value;
                    //totalIR += operacaoRendaFixaCollection[i].ValorIR.Value;
                    totalIss += operacaoBolsaCollection[i].ValorISS.Value;
                    totalCorretagem += operacaoBolsaCollection[i].Corretagem.Value;
                    //
                    totalLiquidacaoCBLC += operacaoBolsaCollection[i].LiquidacaoCBLC.Value;                    
                    totalRegistroCBLC += operacaoBolsaCollection[i].RegistroBolsa.Value;
                    totalRegistroBolsa += operacaoBolsaCollection[i].RegistroCBLC.Value;
                }                 
            }
            #endregion

            // OPC - OPV
            this.footer.comprasOpcoes = totalComprasOpcoes;
            this.footer.vendasOpcoes = totalVendasOpcoes;
            //
                   
            // VIS - TER - IMO
            this.footer.valorTermo = totalValorTermo;
            //
            this.footer.vendasVista = totalVendas;
            this.footer.comprasVista = totalCompras;
            //
            //this.footer.valorLiquido = totalValorLiquido;
            this.footer.corretagem = totalCorretagem;
            this.footer.iss = totalIss;
            //this.footer.ir = totalIR;

            this.footer.emolumentos = totalEmolumentos;
            //
            this.footer.liquidacaoCBLC = totalLiquidacaoCBLC;
            this.footer.registroCBLC = totalRegistroCBLC;
            this.footer.registroBolsa = totalRegistroBolsa;
        }

        private void SetaDetail() {
            for (int i = 0; i < this.operacaoBolsaCollection.Count; i++) {
                Detail d = new Detail();
                //
                d.TipoOperacao = operacaoBolsaCollection[i].TipoOperacao.Substring(0, 1);
                //d.TipoMercado = TipoMercadoBolsa.str.RetornaTexto(operacaoBolsaCollection[i].TipoMercado);

                string tipoMercadoAux = operacaoBolsaCollection[i].TipoMercado;
                switch (tipoMercadoAux) {
                    case TipoMercadoBolsa.MercadoVista:
                    case TipoMercadoBolsa.Imobiliario: d.TipoMercado = "VISTA"; 
                                                       break;
                    case TipoMercadoBolsa.OpcaoCompra: d.TipoMercado = "OPCAO COMPRA"; break;
                    case TipoMercadoBolsa.OpcaoVenda: d.TipoMercado = "OPCAO VENDA"; break;
                    case TipoMercadoBolsa.Termo: d.TipoMercado = "TERMO"; break;
                }
                
                //d.TipoMercado = TipoMercadoBolsa.str.RetornaTexto(operacaoBolsaCollection[i].TipoMercado);

                d.Prazo = "";

                DateTime? dataVencimento = null;
                if (!Convert.IsDBNull(operacaoBolsaCollection[i].GetColumn(AtivoBolsaMetadata.ColumnNames.DataVencimento))) {
                    dataVencimento = Convert.ToDateTime(operacaoBolsaCollection[i].GetColumn(AtivoBolsaMetadata.ColumnNames.DataVencimento));
                }

                d.Prazo = ""; // Vista / IMO
                
                string prazo = "";
                switch (tipoMercadoAux) {
                    case TipoMercadoBolsa.Termo: // Dias Corridos do Pregão até a dataVencimento
                                            d.Prazo = "";
                                            if (dataVencimento.HasValue && this.dataNota <=dataVencimento.Value) {
                                                int numeroDias = Calendario.NumeroDias(this.dataNota, dataVencimento.Value);
                                                d.Prazo = numeroDias.ToString();
                                            }
                                            break;
                    case TipoMercadoBolsa.OpcaoCompra:
                    case TipoMercadoBolsa.OpcaoVenda:
                                            if (dataVencimento.HasValue) {
                                                prazo = dataVencimento.Value.ToString("MM/yy");
                                            }
                                            d.Prazo = prazo;
                                            break;
                }

                if (tipoMercadoAux == TipoMercadoBolsa.OpcaoCompra || tipoMercadoAux == TipoMercadoBolsa.OpcaoVenda)
                {
                    d.Especificacao = operacaoBolsaCollection[i].CdAtivoBolsa;
                }
                else
                {
                    d.Especificacao = Convert.ToString(operacaoBolsaCollection[i].GetColumn(AtivoBolsaMetadata.ColumnNames.Descricao));
                }

                d.Especificacao1 = Convert.ToString(operacaoBolsaCollection[i].GetColumn(AtivoBolsaMetadata.ColumnNames.Especificacao)).ToUpper().Trim();

                d.EspecificacaoOpcao = "";
                if (tipoMercadoAux == TipoMercadoBolsa.OpcaoCompra || tipoMercadoAux == TipoMercadoBolsa.OpcaoVenda) 
                {
                    d.EspecificacaoOpcao = operacaoBolsaCollection[i].CdAtivoBolsa.Substring(0,4);
                }

                d.Observacao = "";
                //
                d.Quantidade = operacaoBolsaCollection[i].Quantidade.Value;
                d.Preco = operacaoBolsaCollection[i].Pu.Value;
                d.Valor = operacaoBolsaCollection[i].Valor.Value;
                //
                this.lista.Add(d);
            }
        }
        #endregion
    }
}