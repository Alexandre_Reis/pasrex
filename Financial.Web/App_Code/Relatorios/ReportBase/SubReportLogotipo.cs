﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Web.Configuration;
using System.Web;
using DevExpress.XtraPrinting;
using Financial.Security;
using Financial.Investidor;
using System.Reflection;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportLogotipo
    /// Conversão Imagem: Necessário trocar o report Unit Do relátorio para poder ver as Unidades Corretas
    /// 208 X 37 pixels
    /// - 2.17 - 0.39  Inchs - http://www.classical-webdesigns.co.uk/resources/pixelinchconvert.html
    /// - 55.03 - 9.7895 milimeters - http://www.unitconversion.org/typography/pixels-x-to-millimeters-conversion.html
    /// </summary>
    public class SubReportLogotipo : DevExpress.XtraReports.UI.XtraReport {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRPictureBox xrPictureBox1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRControlStyle xrControlStyle1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportLogotipo() {
            InitializeComponent();
            //
            this.PersonalInitialize();
        }

        private void PersonalInitialize() { 
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/ReportBase/SubReportLogotipo.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportLogotipo.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.Detail.HeightF = 55F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(297F, 42F);
            this.xrPictureBox1.StylePriority.UseBorders = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BorderColor = System.Drawing.Color.DarkRed;
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrControlStyle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // SubReportLogotipo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 67;
            this.PageWidth = 298;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubReportLogotipoBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportLogotipo.ResourceManager;
        }

        #region funções Internas
        private void SubReportLogotipoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            
            //const string imagem = "logo_cliente.png";
            string imagem = this.DefineImagem();

            string imagemLogoCliente = "~/imagensPersonalizadas/"+imagem;
            string pathImagem = DiretorioAplicacao.DiretorioBaseAplicacao + "imagensPersonalizadas/"+imagem;

            //string imagemLogoCliente = "~/imagensPersonalizadas/logoTeste.png";
            //string pathImagem = DiretorioAplicacao.DiretorioBaseAplicacao + "imagensPersonalizadas/logoTeste.png";

            //
            // Se existe path carrega imagemLogo Personalizada
            if (File.Exists(pathImagem)) {
                this.xrPictureBox1.ImageUrl = imagemLogoCliente;

                //this.xrPictureBox1.Sizing = ImageSizeMode.ZoomImage;

                // Pega o tamanho da imagem
                //float largura = this.xrPictureBox1.Image.Size.Width;
                //if (largura >= IMAGEM_LARGURA_MAX) {
                //    this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
                //}

                this.PersonalXRPictureBox();
            }
        }

        /// <summary>
        /// Baseado no ReportPai define qual a imagem será colocado no logotipo
        /// </summary>
        /// <returns>string com o nome da imagem .png</returns>
        private string DefineImagem() {

            string img = "logo_cliente.png"; // Default para garantir em casos normais
                        
            if (this.MasterReport != null) { // Se é usado como SubReport 

                #region  Relatorios com idCliente Obrigatorio
                /*
                 * ReportSaldoAplicacaoFundo e ReportSaldoAplicacaoFundoView2  Sempre tem idcliente pois relatorios são anexados
                 * ReportSaldoAplicacaoCotista e ReportSaldoAplicacaoCotistaView2 Sempre tem idcarteira pois relatorios são anexados
                 * ReportExtratoCotista sempre tem um IdCarteira pois relatorios são anexados
                 * 
                 */
                if (this.MasterReport is ReportComposicaoCarteira || this.MasterReport is ReportComposicaoCarteiraSemResumo || this.MasterReport is ReportComposicaoCarteiraMasa ||
                    this.MasterReport is ReportFluxoCaixaAnalitico ||
                    this.MasterReport is ReportHistoricoCota || this.MasterReport is ReportHistoricoCotaSintetico ||
                    this.MasterReport is ReportMapaResultado ||
                    this.MasterReport is ReportAnaliseResultados ||
                    this.MasterReport is ReportExtratoContaCorrente ||
                    this.MasterReport is ReportMemoriaPfee ||
                    this.MasterReport is ReportFluxoCaixaSintetico ||
                    this.MasterReport is ReportSaldoAplicacaoFundo || this.MasterReport is ReportSaldoAplicacaoFundoView2 ||
                    this.MasterReport is ReportSaldoAplicacaoCotista || this.MasterReport is ReportSaldoAplicacaoCotistaView2 ||
                    this.MasterReport is ReportExtratoCotista ||
                    this.MasterReport is ReportNotaAplicacao || this.MasterReport is ReportNotaResgate
                   ) {

                    object valor = null;
                    if (this.MasterReport.GetType().GetProperty("IdCliente") != null)
                        valor = this.MasterReport.GetType().GetProperty("IdCliente").GetValue(this.MasterReport, null);

                    if (valor == null) 
                    {
                        try 
                        {
                            valor = this.MasterReport.GetType().GetProperty("IdCarteira").GetValue(this.MasterReport, null);
                        }
                        catch (Exception) {
                        }    
                    }

                    if (valor != null) 
                    {
                        img = this.VerificaClienteUsuario(Convert.ToInt32(valor));
                    }
                }
                #endregion

                #region  Relatorios sem IdCliente Definido - pode ou não ter IdCliente
                else if (this.MasterReport is ReportFluxoCaixaSinteticoGeral ||
                         this.MasterReport is ReportMovimentacaoFundo
                        ) {

                    object valor = null;
                    try {
                        valor = this.MasterReport.GetType().GetProperty("IdCliente").GetValue(this.MasterReport, null);
                    }
                    catch (Exception) {
                    }                    
                    if (valor != null) {
                        img = this.VerificaClienteUsuario(Convert.ToInt32(valor));
                    }
                    else {
                        img = this.VerificaUsuario();     
                    }                   
                }
                #endregion

                #region  Relatorios sem IdCarteira Definido - pode ou não ter IdCarteira
                else if (this.MasterReport is ReportMovimentacaoCotista
              ) {

                    object valor = null;
                    try {
                        valor = this.MasterReport.GetType().GetProperty("IdCarteira").GetValue(this.MasterReport, null);
                    }
                    catch (Exception) {
                    }

                    if (valor != null) {
                        img = this.VerificaClienteUsuario(Convert.ToInt32(valor));
                    }
                    else {
                        img = this.VerificaUsuario();
                    }
                }
                #endregion
            }
                    
            return img; 
        }

        /// <summary>
        /// Baseado no campo Logotipo do Cliente e do Usuario retorna a imagem a ser carregada caso exista
        /// Caso não exista imagem, retorna a imagem default logo_cliente.png
        /// </summary>
        /// <returns></returns>
        private string VerificaClienteUsuario(int idCliente) {

            string img = "logo_cliente.png";

            #region Verifica se existe logotipo no Cliente
            Cliente c = new Cliente();
            c.Query.Select(c.Query.IdCliente, c.Query.Logotipo)
                   .Where(c.Query.IdCliente == idCliente);

            if (c.Query.Load()) {
                if (!String.IsNullOrEmpty(c.Logotipo)) {
                    // Cliente tem Logotipo Definido - Carrega o Logotipo do Cliente                                        
                    img = c.Logotipo.Trim();
                    return img;
                }
            }
            
            #endregion

            #region Verifica se existe logotipo no Usuario
            Usuario u = new Usuario();
            u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
                   .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

            if (u.Query.Load()) {
                if (!String.IsNullOrEmpty(u.Logotipo)) {
                    // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                        
                    img = u.Logotipo.Trim();
                    return img;
                }
            }
            #endregion

            return img;
        }


        /// <summary>
        /// Baseado no campo Logotipo do Usuario retorna a imagem a ser carregada caso exista
        /// Caso não exista imagem, retorna a imagem default logo_cliente.png
        /// </summary>
        /// <returns></returns>
        private string VerificaUsuario() {
            string img = "logo_cliente.png";

            #region Verifica se existe logotipo no Usuario
            Usuario u = new Usuario();
            u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
                   .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

            if (u.Query.Load()) {
                if (!String.IsNullOrEmpty(u.Logotipo)) {
                    // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                        
                    img = u.Logotipo.Trim();
                    return img;
                }
            }
            #endregion

            return img;
        }

        /// <summary>
        /// Se o Pai do Logotipo for ComposicaoCarteira ou ComposicaoCarteiraSemResumo então corrige 
        /// alinhamento da imagem a direita.
        /// </summary>
        private void PersonalXRPictureBox() {
            if (this.MasterReport != null) { // Se é usado como SubReport 

                if (this.MasterReport is ReportComposicaoCarteira || this.MasterReport is ReportComposicaoCarteiraSemResumo || this.MasterReport is ReportComposicaoCarteiraMasa) {
                   this.AlignImageToRight();
                }
            }
        }

        /// <summary>
        /// Alinha Imagem a Direita.
        /// Considera tamanho maximo como 298
        /// </summary>
        private void AlignImageToRight() {
            /* Tamanho máximo da imagem = tamanho da pictutebox */
            //const int IMAGEM_LARGURA_MAX = 298;
            //const int IMAGEM_LARGURA_MAX = 755;
            //const int IMAGEM_LARGURA_MAX = 317;
            //int IMAGEM_LARGURA_MAX = 298;

            // Pega o tamanho da imagem
            //int largura = this.xrPictureBox1.Image.Size.Width;
            //
            //float deslocamentoDireita = IMAGEM_LARGURA_MAX - largura;

            //int deslocamentoDireita = IMAGEM_LARGURA_MAX - largura;
            
            // Se deslocamento necessário para direita for maior que 90 faz o ajuste

            //if (deslocamentoDireita >= 90) {
            //    int auxDesc = deslocamentoDireita;

            //    auxDesc = auxDesc - 5;

            //    if (auxDesc >= 180) {
            //        auxDesc = 140;
            //    }

            //    Point location = new Point(auxDesc, 0);
            //    this.xrPictureBox1.Location = location;
            //    //
            //    int height = this.xrPictureBox1.Image.Size.Height;
            //    int width = this.xrPictureBox1.Image.Size.Width + 5;

            //    Size size = new Size(width, height);
            //    //
            //    this.xrPictureBox1.Size = size;
            //    //                
            //}

            /*--------------------------------------------------------*/
            //int aux = 298 - this.xrPictureBox1.Image.Size.Width - 10;
            //if (aux >= 160) {
            //    aux = 140; // Limite maximo
            //}
            
            //Point location = new Point(aux, 0);
            //this.xrPictureBox1.Location = location;

            //int height = this.xrPictureBox1.Image.Size.Height;
            //int width = this.xrPictureBox1.Image.Size.Width + 5;

            //Size size = new Size(width, height);
            ////
            //this.xrPictureBox1.Size = size;
            /*--------------------------------------------------------*/


            /* Tamanho máximo da imagem = tamanho da pictutebox */
            int IMAGEM_LARGURA_MAX = 298;

            // Pega o tamanho da imagem
            int largura = this.xrPictureBox1.Image.Size.Width;
            //
            int deslocamentoDireita = IMAGEM_LARGURA_MAX - largura;

            // Se deslocamento necessário para direita for maior que 90 faz o ajuste

            if (deslocamentoDireita >= 60) {
                deslocamentoDireita = deslocamentoDireita - 5;

                PointF location = new PointF( (float)deslocamentoDireita, 0.0f);
                this.xrPictureBox1.LocationF = location;
                //
                float height = (float)this.xrPictureBox1.Image.Size.Height;
                float width = (float)this.xrPictureBox1.Image.Size.Width + 25.0f;

                SizeF size = new SizeF(width, height);
                //
                this.xrPictureBox1.SizeF = size;
                //                
            }
        }

        #endregion
    }
}