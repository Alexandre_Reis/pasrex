﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Web.Configuration;
using System.Web;
using System.Configuration;
using System.Web.Hosting;
using Financial.Investidor;
using Financial.Security;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for SubReportLogotipo1
    /// Conversão Imagem: Necessário trocar o report Unit Do relátorio para poder ver as Unidades Corretas
    /// 208 X 37 pixels
    /// - 2.17 - 0.39  Inchs - http://www.classical-webdesigns.co.uk/resources/pixelinchconvert.html
    /// - 55.03 - 9.7895 milimeters - http://www.unitconversion.org/typography/pixels-x-to-millimeters-conversion.html
    /// </summary>
    public class SubReportLogotipo1 : DevExpress.XtraReports.UI.XtraReport
    {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private XRPictureBox xrPictureBox1;
        private string logoImageName = "logo_cliente.png";
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SubReportLogotipo1()
        {
            InitializeComponent();
            //
            this.PersonalInitialize();
        }

        private void PersonalInitialize()
        {
            this.PersonalizaXRPictureBox1();
            this.PersonalizaLogoImageName();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/ReportBase/SubReportLogotipo1.resx";  */
        private void InitializeComponent()
        {
            string resourceFileName = "SubReportLogotipo1.resx";
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
            this.Detail.HeightF = 43F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(80F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(217F, 39F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportLogotipo1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Margins = new System.Drawing.Printing.Margins(0, 35, 0, 0);
            this.PageHeight = 67;
            this.PageWidth = 335;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SubReportLogotipoBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager()
        {
            return Resources.SubReportLogotipo1.ResourceManager;
        }

        #region funções Internas
        private void SubReportLogotipoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {

            if(!this.HasLogoFullWidth)  {
                this.logoImageName = this.DefineImagem();
            }

            string imagemLogoCliente = "~/imagensPersonalizadas/" + this.logoImageName;

            string pathImagem = HostingEnvironment.MapPath(imagemLogoCliente);

            // Se existe path carrega imagemLogo Personalizada
            if (File.Exists(pathImagem)) {
                this.xrPictureBox1.ImageUrl = imagemLogoCliente;
                //this.xrPictureBox1.ImageUrl = pathImagem;
            }
        }

        private void PersonalizaXRPictureBox1() {
            string logoFullWidthDimensions = ConfigurationManager.AppSettings["LogoFullWidthDimensions"];
            if (!String.IsNullOrEmpty(logoFullWidthDimensions))
            {
                string[] split = logoFullWidthDimensions.Split('x');
                int width = Convert.ToInt32(split[0]);
                int height = Convert.ToInt32(split[1]);

                this.xrPictureBox1.Size = new System.Drawing.Size(width, height);
                this.xrPictureBox1.Location = new System.Drawing.Point(0, 0);
                this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Normal;
            }
        }

        private string GetLogoFullWidthImageName()
        {
            return ConfigurationManager.AppSettings["LogoFullWidthImageName"];
        }

        public bool HasLogoFullWidth
        {
            get
            {
                return !String.IsNullOrEmpty(this.GetLogoFullWidthImageName());
            }
        }

        private void PersonalizaLogoImageName()
        {
            string logoFullWidthImageName = this.GetLogoFullWidthImageName();
            if (!String.IsNullOrEmpty(logoFullWidthImageName))
            {
                this.logoImageName = logoFullWidthImageName;
            }
        }


        /// <summary>
        /// Baseado no ReportPai define qual a imagem será colocado no logotipo
        /// </summary>
        /// <returns>string com o nome da imagem .png</returns>
        private string DefineImagem() {

            string img = "logo_cliente.png"; // Default para garantir em casos normais

            if (this.MasterReport != null) { // Se é usado como SubReport 

                #region  Relatorios com idCarteira/idCliente Obrigatorio
                if (this.MasterReport is ReportLamina ||
                    this.MasterReport is ReportExtratoCliente) {

                    object valor = null;
                    try {
                        valor = this.MasterReport.GetType().GetProperty("IdCarteira").GetValue(this.MasterReport, null);
                    }
                    catch (Exception) {
                    }

                    if (valor != null) {
                        img = this.VerificaClienteUsuario(Convert.ToInt32(valor));
                    }
                }
                #endregion
            }

            return img;
        }

        /// <summary>
        /// Baseado no campo Logotipo do Cliente e do Usuario retorna a imagem a ser carregada caso exista
        /// Caso não exista imagem, retorna a imagem default logo_cliente.png
        /// </summary>
        /// <returns></returns>
        private string VerificaClienteUsuario(int idCliente) {

            string img = "logo_cliente.png";

            #region Verifica se existe logotipo no Cliente
            Cliente c = new Cliente();
            c.Query.Select(c.Query.IdCliente, c.Query.Logotipo)
                   .Where(c.Query.IdCliente == idCliente);

            if (c.Query.Load()) {
                if (!String.IsNullOrEmpty(c.Logotipo)) {
                    // Cliente tem Logotipo Definido - Carrega o Logotipo do Cliente                                        
                    img = c.Logotipo.Trim();
                    return img;
                }
            }

            #endregion

            #region Verifica se existe logotipo no Usuario
            Usuario u = new Usuario();
            u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
                   .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

            if (u.Query.Load()) {
                if (!String.IsNullOrEmpty(u.Logotipo)) {
                    // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                        
                    img = u.Logotipo.Trim();
                    return img;
                }
            }
            #endregion

            return img;
        }


        /// <summary>
        /// Baseado no campo Logotipo do Usuario retorna a imagem a ser carregada caso exista
        /// Caso não exista imagem, retorna a imagem default logo_cliente.png
        /// </summary>
        /// <returns></returns>
        private string VerificaUsuario() {
            string img = "logo_cliente.png";

            #region Verifica se existe logotipo no Usuario
            Usuario u = new Usuario();
            u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
                   .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

            if (u.Query.Load()) {
                if (!String.IsNullOrEmpty(u.Logotipo)) {
                    // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                        
                    img = u.Logotipo.Trim();
                    return img;
                }
            }
            #endregion

            return img;
        }

        #endregion
    }
}