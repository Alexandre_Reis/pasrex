﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DevExpress.XtraReports.UI;
using System.Reflection;
using Financial.Util;
using System.Drawing;
using System.Collections.Generic;
using Financial.WebConfigConfiguration;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts;
using Financial.Util.ConfiguracaoSistema;

namespace Financial.Relatorio
{

    /// <summary>
    /// Summary description for RelatorioBase
    /// </summary>
    public class ReportBase
    {
        static Color POSITIVO_FORECOLOR = Color.Black;
        static Color NEGATIVO_FORECOLOR = Color.Red;
        private static string positivoName = "Positivo";
        private static string negativoName = "Negativo";

        public string POSITIVO_NAME
        {
            get
            {
                return positivoName;
            }
        }

        public string NEGATIVO_NAME
        {
            get
            {
                return negativoName;
            }
        }

        // Report que será Configurado
        private XtraReport report;


        /// <summary>
        /// Define os formatos de exibição de números decimais
        /// </summary>
        public enum NumeroCasasDecimais
        {
            /// <summary>
            /// formato com nenhuma casa Decimal
            /// </summary>
            [StringValue("n0")]
            ZeroCasasDecimais = 0,

            /// <summary>
            /// formato com duas casas Decimais
            /// </summary>
            [StringValue("n2")]
            DuasCasasDecimais = 1,

            /// <summary>
            /// formato com três casas Decimais
            /// </summary>
            [StringValue("n3")]
            TresCasasDecimais = 2,

            /// <summary>
            /// formato com oito casas Decimais
            /// </summary>
            [StringValue("n8")]
            OitoCasasDecimais = 3,

            /// <summary>
            /// formato em porcentagem com 2 casas Decimais
            /// </summary>
            [StringValue("P2")]
            DuasCasasDecimaisPorcentagem = 4,

            /// <summary>
            /// formato em porcentagem com 4 casas Decimais
            /// </summary>
            [StringValue("P4")]
            QuatroCasasDecimaisPorcentagem = 5,

            /// <summary>
            /// formato com 4 casas Decimais
            /// </summary>
            [StringValue("n4")]
            QuatroCasasDecimais = 6,
        }

        // Tipos de Fontes Suportado nos Relatorios
        private enum FonteRelatorio
        {
            [StringValue("Times New Roman")]
            Times = 1,

            [StringValue("Arial")]
            Arial = 2,

            [StringValue("Courier New")]
            Courier = 3,

            [StringValue("Arial Narrow")]
            ArialNarrow = 4
        }

        private XRControlStyle header1Style = new XRControlStyle();
        private XRControlStyle header2Style = new XRControlStyle();
        private XRControlStyle header3Style = new XRControlStyle();
        private XRControlStyle oddRowStyle = new XRControlStyle();
        private XRControlStyle evenRowStyle = new XRControlStyle();
        private XRControlStyle line1Style = new XRControlStyle();
        private XRControlStyle headerFieldStyle = new XRControlStyle();
        private XRControlStyle positivoStyle = new XRControlStyle();
        private XRControlStyle negativoStyle = new XRControlStyle();

        private IDictionary<string, XRControlStyle> xrControlStyleDic = new Dictionary<string, XRControlStyle>();

        // Lista com todas as fontes suportadas
        private List<string> fontesSuportadas = new List<string>();

        // Constante para a fonte de Relatorio - Times new Roman - Fonte Padrao
        private string fonteRelatorio = StringEnum.GetStringValue(FonteRelatorio.Times);

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="report"></param>
        public ReportBase(XtraReport report)
        {
            this.report = report;
            //

            this.InitializeXRControlStyles();

            this.fontesSuportadas.Add(StringEnum.GetStringValue(FonteRelatorio.Times));
            this.fontesSuportadas.Add(StringEnum.GetStringValue(FonteRelatorio.Arial));
            this.fontesSuportadas.Add(StringEnum.GetStringValue(FonteRelatorio.Courier));
            this.fontesSuportadas.Add(StringEnum.GetStringValue(FonteRelatorio.ArialNarrow));
            //
            this.ConfiguraEstilosRelatorio();
            //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xrControl"></param>
        /// <param name="xrControlStyle"></param>
        private void ApplyStyle(XRControl xrControl, XRControlStyle xrControlStyle)
        {   //Para uso de elementos que tem Odd e Even Rows
            string key = xrControlStyle.Name;
            if (xrControlStyleDic.ContainsKey(key))
            {
                XRControlStyle xrControlStyleDicEntry = xrControlStyleDic[key];
                xrControlStyle.BackColor = xrControlStyleDicEntry.BackColor;
                xrControlStyle.ForeColor = xrControlStyleDicEntry.ForeColor;
                //xrControl.StylePriority.UseBackColor = false;
                xrControl.StylePriority.UseForeColor = true;

                //xrControl.ForeColor = xrControlStyleDicEntry.ForeColor;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xrControl"></param>
        private void ApplyStyle(XRControl xrControl)
        {
            //Para uso de elementos que NAO tem Odd e Even Rows
            string key = xrControl.StyleName;
            if (xrControlStyleDic.ContainsKey(key))
            {
                XRControlStyle xrControlStyleDicEntry = xrControlStyleDic[key];
                xrControl.BackColor = xrControlStyleDicEntry.BackColor;
                xrControl.ForeColor = xrControlStyleDicEntry.ForeColor;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ConfiguraEstilosRelatorio()
        {
            FieldInfo[] variaveisPrivates = this.report.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance |
                                                   BindingFlags.DeclaredOnly);

            string fonteWebConfig = WebConfig.AppSettings.TipoFonteRelatorio;

            /* Qualquer fonte diferente das validas é desconsiderada 
               Nesse caso é usado a Times New Roman
             */
            if (this.fontesSuportadas.Contains(fonteWebConfig))
            {
                this.fonteRelatorio = fonteWebConfig;
            }

            // Verifica se é do tipo XRLabel ou XRTableCell
            foreach (FieldInfo variavel in variaveisPrivates)
            {
                // Tratamento se campo for tableCell
                #region TableCell
                if (variavel.FieldType.Equals(typeof(XRTableCell)))
                {
                    // valor velho
                    XRTableCell xrTableCell = (XRTableCell)variavel.GetValue(this.report);

                    if (xrTableCell != null)
                    {
                        // Pega o tamanho atual e Style atual
                        float tamanho = xrTableCell.Font.SizeInPoints;
                        // Se fonte for Courier New Diminui o tamanho da letra em -1
                        //if (this.IsTipoFonteCourierNew()) {
                        //    tamanho = tamanho - 0.5f;                    
                        //}
                        if (this.IsTipoFonteArialNarrow())
                        {
                            tamanho = tamanho + 1f;
                        }

                        FontStyle fontStyle = xrTableCell.Font.Style;

                        Font font = new Font(this.fonteRelatorio, tamanho, fontStyle, GraphicsUnit.Point, ((byte)(0)));

                        // Muda a fonte - novo valor
                        xrTableCell.Font = font;

                        if (!string.IsNullOrEmpty(xrTableCell.EvenStyleName))
                        {
                            this.ApplyStyle(xrTableCell, xrTableCell.Styles.EvenStyle);
                        }
                        if (!string.IsNullOrEmpty(xrTableCell.OddStyleName))
                        {

                            this.ApplyStyle(xrTableCell, xrTableCell.Styles.OddStyle);
                        }

                        if (!string.IsNullOrEmpty(xrTableCell.StyleName))
                        {
                            this.ApplyStyle(xrTableCell);
                        }


                        // Altera o novo valor
                        variavel.SetValue(this.report, xrTableCell);
                    }
                }
                #endregion

                // Tratamento de chart
                #region Chart
                else if (variavel.FieldType.Equals(typeof(XRChart)))
                {
                    // valor velho
                    XRChart xrChart = (XRChart)variavel.GetValue(this.report);

                    FontStyle fontStyle = xrChart.Font.Style;
                    
                    Font fontLegend = new Font(this.fonteRelatorio, 7.0F, fontStyle, GraphicsUnit.Point, ((byte)(0)));
                    Font fontEixoX = new Font(this.fonteRelatorio, 8.0F, fontStyle, GraphicsUnit.Point, ((byte)(0)));
                    Font fontEixoY = new Font(this.fonteRelatorio, 7.5F, fontStyle, GraphicsUnit.Point, ((byte)(0)));

                    // Muda a fonte - novo valor
                    if (typeof(XYDiagram) == xrChart.Diagram.GetType())
                    {
                        XYDiagram diagram = (XYDiagram)xrChart.Diagram;
                        diagram.AxisX.Label.Font = fontEixoX;
                        diagram.AxisY.Label.Font = fontEixoY;
                    }
                    /*else if (typeof(SimpleDiagram3D) == xrChart.Diagram.GetType())
                    {//IMPLEMENTAR !!! - Cuidado com Series
                        SimpleDiagram3D diagram = (SimpleDiagram3D)xrChart.Diagram;
                        diagram.Dimension
                        diagram.AxisX.Label.Font = font;
                        diagram.AxisY.Label.Font = font;
                    }*/

                    if (xrChart.Legend != null) {
                        xrChart.Legend.Font = fontLegend;
                    }
                }
                #endregion

                // Tratamento de Line (usado em rodape, por ex)
                #region Line
                else if (variavel.FieldType.Equals(typeof(XRLine)))
                {
                    XRLine xrLine = (XRLine)variavel.GetValue(this.report);
                    if (!string.IsNullOrEmpty(xrLine.StyleName))
                    {
                        this.ApplyStyle(xrLine);
                    }
                }
                #endregion

                // Tratamento se campo for label
                #region Label
                else if (variavel.FieldType.Equals(typeof(XRLabel)))
                {
                    // valor velho
                    XRLabel xrLabel = (XRLabel)variavel.GetValue(this.report);

                    // Pega o tamanho atual e Style atual
                    float tamanho = xrLabel.Font.SizeInPoints;
                    if (this.IsTipoFonteArialNarrow())
                    {
                        tamanho = tamanho + 1f;
                    }

                    FontStyle fontStyle = xrLabel.Font.Style;
                    Font font = new Font(this.fonteRelatorio, tamanho, fontStyle, GraphicsUnit.Point, ((byte)(0)));

                    // Muda a fonte - novo valor
                    xrLabel.Font = font;

                    if (!string.IsNullOrEmpty(xrLabel.StyleName))
                    {
                        this.ApplyStyle(xrLabel);
                    }


                    // Altera o novo valor
                    variavel.SetValue(this.report, xrLabel);
                }
                #endregion
                // Tratamento se campo for PageInfo
                #region PageInfo
                else if (variavel.FieldType.Equals(typeof(XRPageInfo)))
                {
                    // valor velho
                    XRPageInfo xrPageInfo = (XRPageInfo)variavel.GetValue(this.report);

                    // Pega o tamanho atual e Style atual
                    float tamanho = xrPageInfo.Font.SizeInPoints;
                    if (this.IsTipoFonteArialNarrow())
                    {
                        tamanho = tamanho + 1f;
                    }

                    FontStyle fontStyle = xrPageInfo.Font.Style;
                    Font font = new Font(this.fonteRelatorio, tamanho, fontStyle, GraphicsUnit.Point, ((byte)(0)));

                    // Muda a fonte - novo valor
                    xrPageInfo.Font = font;

                    if (!string.IsNullOrEmpty(xrPageInfo.StyleName))
                    {
                        this.ApplyStyle(xrPageInfo);
                    }

                    // Altera o novo valor
                    variavel.SetValue(this.report, xrPageInfo);
                }
                #endregion
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeXRControlStyles()
        {
            header1Style.Name = "Header1";
            header2Style.Name = "Header2";
            header3Style.Name = "Header3";
            oddRowStyle.Name = "OddRow";
            evenRowStyle.Name = "EvenRow";
            line1Style.Name = "Line1";
            headerFieldStyle.Name = "HeaderField";
            positivoStyle.Name = POSITIVO_NAME;
            negativoStyle.Name = NEGATIVO_NAME;

            List<XRControlStyle> xrControlStyleList = new List<XRControlStyle>();
            xrControlStyleList.Add(header1Style);
            xrControlStyleList.Add(header2Style);
            xrControlStyleList.Add(header3Style);
            xrControlStyleList.Add(oddRowStyle);
            xrControlStyleList.Add(evenRowStyle);
            xrControlStyleList.Add(line1Style);
            xrControlStyleList.Add(headerFieldStyle);
            xrControlStyleList.Add(positivoStyle);
            xrControlStyleList.Add(negativoStyle);

            header1Style.ForeColor = Color.White;
            header1Style.BackColor = ColorTranslator.FromHtml("#262626");

            header2Style.ForeColor = Color.Black;
            header2Style.BackColor = ColorTranslator.FromHtml("#d3d3d3");

            header3Style.ForeColor = Color.Black;
            header3Style.BackColor = ColorTranslator.FromHtml("#e3e3e3");

            oddRowStyle.ForeColor = Color.Black;
            oddRowStyle.BackColor = Color.White;

            evenRowStyle.ForeColor = Color.Black;
            evenRowStyle.BackColor = ColorTranslator.FromHtml("#f3f3f3");

            line1Style.ForeColor = Color.Black;
            line1Style.BackColor = Color.White;

            headerFieldStyle.ForeColor = Color.Black;
            headerFieldStyle.BackColor = Color.White;

            positivoStyle.ForeColor = POSITIVO_FORECOLOR;
            positivoStyle.BackColor = Color.White;

            negativoStyle.ForeColor = NEGATIVO_FORECOLOR;
            negativoStyle.BackColor = Color.White;

            bool usaCoresConfig = String.IsNullOrEmpty(ConfigurationManager.AppSettings["UsaCoresConfig"]) ? false : Convert.ToBoolean(ConfigurationManager.AppSettings["UsaCoresConfig"]);
            
            string colorsReportBaseConfig = "";
            colorsReportBaseConfig = ConfigurationManager.AppSettings["ColorsReportBase"];
            if (usaCoresConfig)
            {
                colorsReportBaseConfig = ConfigurationManager.AppSettings["ColorsReportBase"];
            }
            else
            {
                colorsReportBaseConfig =
                        !String.IsNullOrEmpty(ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato)
                        ? ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato
                        : "#ffffff, #002940, #002940, #D7CFB9, #002940, #F9F2E2, #002940, #FDFAF3, #002940, #F9F2E2, #002940, #ffffff, #002940, #ffffff, #002940, #ffffff, #ff0000, #ffffff";
            }

            if (!string.IsNullOrEmpty(colorsReportBaseConfig))
            {
                string[] colorsResportsBase = colorsReportBaseConfig.Split(',');

                for (int i = 0, j = 0; (i < colorsResportsBase.Length / 2); i++, j++)
                {
                    XRControlStyle xrControlStyle = xrControlStyleList[i];
                    xrControlStyle.ForeColor = ColorTranslator.FromHtml(colorsResportsBase[j]);
                    j++;
                    xrControlStyle.BackColor = ColorTranslator.FromHtml(colorsResportsBase[j]);
                }
            }

            foreach (XRControlStyle xrControlStyle in xrControlStyleList)
            {
                xrControlStyleDic.Add(xrControlStyle.Name, xrControlStyle);
            }
        }

        public static void ConfiguraSinalNegativo(XRTableCell tableCell, decimal valor)
        {
            ReportBase.ConfiguraSinalNegativo(tableCell, valor, NumeroCasasDecimais.DuasCasasDecimais);
        }

        /// <summary>
        /// De acordo com o AppSettings configura o Sinal Negativo dos campos do relatorio
        /// Se WebConfig.RelatoriosConfiguraSinalNegativo = true
        ///     - exibe numero negativo com parenteses e em cor vermelha
        ///     - Se valor for null exibe um traço
        ///     - Exibe o numero com duas casas Decimais e em Porcentagem
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor">valor em decimal a ser colocado na tabela</param>
        public static void ConfiguraSinalNegativo(XRTableCell tableCell, decimal? valor)
        {
            ReportBase.ConfiguraSinalNegativo(tableCell, valor, true, NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
        }

        /// <summary>
        /// De acordo com o AppSettings configura o Sinal Negativo dos campos do relatorio
        /// Se WebConfig.RelatoriosConfiguraSinalNegativo = true
        ///     - exibe numero negativo com parenteses e em cor vermelha
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor">valor em decimal a ser colocado na tabela</param>
        /// <param name="numeroCasasDecimais"></param>
        public static void ConfiguraSinalNegativo(XRTableCell tableCell, decimal valor, NumeroCasasDecimais numeroCasasDecimais)
        {
            ReportBase.ConfiguraSinalNegativo(tableCell, valor, false, numeroCasasDecimais);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor"></param>
        /// <param name="exibeTraco"></param>
        /// <param name="numeroCasasDecimais"></param>
        public static void ConfiguraSinalNegativo(XRTableCell tableCell, decimal? valor, bool exibeTraco, NumeroCasasDecimais numeroCasasDecimais)
        {
            ConfiguraSinalNegativo(tableCell, valor, exibeTraco, numeroCasasDecimais, POSITIVO_FORECOLOR, NEGATIVO_FORECOLOR);
        }

        /// <summary>
        /// De acordo com o AppSettings configura o Sinal Negativo dos campos do relatorio
        /// Se WebConfig.RelatoriosConfiguraSinalNegativo = true
        ///     - exibe numero negativo com parenteses e em cor vermelha
        ///     - Se valor for null, baseado na propriedade exibeTraco, decide se vai exibir o campo
        ///         em branco ou um traço
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor">valor em decimal a ser colocado na tabela</param>
        /// <param name="exibeTraco"></param>
        /// <param name="numeroCasasDecimais"></param>
        public static void ConfiguraSinalNegativo(XRTableCell tableCell, decimal? valor, bool exibeTraco, NumeroCasasDecimais numeroCasasDecimais, Color positivoForeColor, Color negativoForeColor)
        {

            if (!valor.HasValue)
            {
                #region Valor == null
                tableCell.Text = exibeTraco == true ? " - " : "";
                //tableCell.TextAlignment = TextAlignment.MiddleCenter;
                tableCell.ForeColor = positivoForeColor;
                #endregion
            }
            else
            {
                #region Valor Diferente de Null
                bool sinalNegativo = Convert.ToBoolean(WebConfig.AppSettings.RelatoriosFormatoNumeroNegativo);
                decimal valorAux = valor.Value;

                string valorString = "";
                string numeroCasasDecimaisString = StringEnum.GetStringValue(numeroCasasDecimais);
                // Tratamento Sinal Negativo
                #region Tratamento Sinal Negativo
                if (sinalNegativo)
                {
                    valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                }
                // Parenteses
                else
                {
                    #region Coloca numero negativo com parenteses
                    if (valorAux < 0)
                    {
                        decimal valorAux1 = valorAux * -1;
                        valorString = "(" + valorAux1.ToString("" + numeroCasasDecimaisString + "") + ")";
                    }
                    else
                    {
                        valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                    }
                    #endregion
                }
                #endregion

                // Tratamento para Cor do numero Negativo e Positivo
                #region Tratamento da cor para números Negativos
                tableCell.ForeColor = valorAux < 0 ? negativoForeColor : positivoForeColor;
                #endregion
                //
                tableCell.Text = valorString;
                #endregion
            }
        }

        /// <summary>
        /// De acordo com o AppSettings configura o Sinal Negativo dos campos do relatorio
        /// Se WebConfig.RelatoriosConfiguraSinalNegativo = true
        ///     - exibe numero negativo com parenteses
        ///     - Se valor for null, baseado na propriedade exibeTraco, decide se vai exibir o campo
        ///         em branco ou um traço
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor">valor em decimal a ser colocado na tabela</param>
        /// <param name="exibeTraco"></param>
        /// <param name="numeroCasasDecimais"></param>
        public static void ConfiguraSinalNegativoDesprezaCorPositivoNegativo(XRTableCell tableCell, decimal? valor, bool exibeTraco, NumeroCasasDecimais numeroCasasDecimais) {

            if (!valor.HasValue) {
                #region Valor == null
                tableCell.Text = exibeTraco == true ? " - " : "";
                #endregion
            }
            else {
                #region Valor Diferente de Null
                bool sinalNegativo = Convert.ToBoolean(WebConfig.AppSettings.RelatoriosFormatoNumeroNegativo);
                decimal valorAux = valor.Value;

                string valorString = "";
                string numeroCasasDecimaisString = StringEnum.GetStringValue(numeroCasasDecimais);
                // Tratamento Sinal Negativo
                #region Tratamento Sinal Negativo
                if (sinalNegativo) {
                    valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                }
                // Parenteses
                else {
                    #region Coloca numero negativo com parenteses
                    if (valorAux < 0) {
                        decimal valorAux1 = valorAux * -1;
                        valorString = "(" + valorAux1.ToString("" + numeroCasasDecimaisString + "") + ")";
                    }
                    else {
                        valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                    }
                    #endregion
                }
                #endregion

                //
                tableCell.Text = valorString;
                #endregion
            }
        }

        /// <summary>
        /// Dezpreza webConfig - Sinal Negativo vem sempre com menos na frente
        /// </summary>
        /// <param name="tableCell"></param>
        /// <param name="valor">valor em decimal a ser colocado na tabela</param>
        /// <param name="exibeTraco"></param>
        /// <param name="numeroCasasDecimais"></param>
        public static void ConfiguraSinalNegativoDesprezaWebConfig(XRTableCell tableCell, decimal? valor, bool exibeTraco, NumeroCasasDecimais numeroCasasDecimais, Color positivoForeColor, Color negativoForeColor) {

            if (!valor.HasValue) {
                #region Valor == null
                tableCell.Text = exibeTraco == true ? " - " : "";
                //tableCell.TextAlignment = TextAlignment.MiddleCenter;
                tableCell.ForeColor = positivoForeColor;
                #endregion
            }
            else {
                #region Valor Diferente de Null
                //bool sinalNegativo = Convert.ToBoolean(WebConfig.AppSettings.RelatoriosFormatoNumeroNegativo);
                bool sinalNegativo = true;
                decimal valorAux = valor.Value;

                string valorString = "";
                string numeroCasasDecimaisString = StringEnum.GetStringValue(numeroCasasDecimais);
                // Tratamento Sinal Negativo
                #region Tratamento Sinal Negativo
                if (sinalNegativo) {
                    valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                }
                // Parenteses
                else {
                    #region Coloca numero negativo com parenteses
                    if (valorAux < 0) {
                        decimal valorAux1 = valorAux * -1;
                        valorString = "(" + valorAux1.ToString("" + numeroCasasDecimaisString + "") + ")";
                    }
                    else {
                        valorString = valorAux.ToString("" + numeroCasasDecimaisString + "");
                    }
                    #endregion
                }
                #endregion

                // Tratamento para Cor do numero Negativo e Positivo
                #region Tratamento da cor para números Negativos
                tableCell.ForeColor = valorAux < 0 ? negativoForeColor : positivoForeColor;
                #endregion
                //
                tableCell.Text = valorString;
                #endregion
            }
        }

        /// <summary>
        /// Limpa todos os dados da Table
        /// </summary>
        /// <param name="xrTable"></param>
        public static void LimpaDadosTable(XRTable xrTable)
        {
            #region Limpa os Dados da Tabela
            // 
            for (int i = 0; i < xrTable.Rows.Count; i++)
            {
                int colunas = ((XRTableRow)xrTable.Rows[i]).Cells.Count;
                for (int j = 0; j < colunas; j++)
                {
                    ((XRTableCell)xrTable.Rows[i].Cells[j]).Text = "";
                }
            }
            #endregion
        }

        /// <summary>
        /// Define as Cores dos Graficos das Series
        /// Usado para Line Series
        /// </summary>
        /// <param name="xrChart">Gráfico já com as Series</param>
        public static void DefineCoresSeries(XRChart xrChart)
        {
            #region Define as Cores dos Graficos

            // Controla as Cores das Séries de Gráficos
            // Define 30 cores padrão - a Primeira não é usada
            Color[] colors = { new Color(), Color.Blue, Color.Red, 
                                   Color.YellowGreen, Color.Black, Color.DarkOrange,
                                   Color.DarkGray, Color.Purple, Color.SteelBlue, 
                                   Color.Wheat, Color.Sienna, Color.PeachPuff,
                                   Color.PapayaWhip, Color.Aqua, Color.SeaShell,
                                   Color.Navy, Color.Brown, Color.Peru,
                                   Color.Magenta, Color.Coral, Color.Moccasin,
                                   Color.Gold, Color.DarkKhaki, Color.Maroon,
                                   Color.Bisque, Color.DarkOrchid, Color.Lime,
                                   Color.Beige, Color.Salmon, Color.Linen };
            //
            MarkerKind[] kinds = {new MarkerKind(), MarkerKind.Diamond, MarkerKind.Cross, MarkerKind.Circle, 
                                  MarkerKind.Star, MarkerKind.InvertedTriangle, MarkerKind.Plus,
                                  MarkerKind.Pentagon };
            //
            SeriesCollection series = xrChart.Series;

            int j = 0;
            for (int i = 0; i < series.Count; i++)
            {
                series[i].View.Color = colors[i];
                ((LineSeriesView)series[i].View).LineMarkerOptions.FillStyle.FillMode = FillMode.Solid;

                // Marcadores com Lista Circular. Acabando o Tamanho Começa Novamente - Multiplos de 7
                if (i % 7 == 0)
                {
                    j = 0;
                }
                ((LineSeriesView)series[i].View).LineMarkerOptions.Kind = kinds[j];
                j++;
            }
            #endregion
        }

        /// <summary>
        /// Define as Cores dos Graficos das Series
        /// Usado para Points Series
        /// </summary>
        /// <param name="xrChart">Gráfico já com as Series</param>
        public static void PersonalizaCoresPointSeries(XRChart xrChart)
        {
            #region Define as Cores dos Graficos

            // Controla as Cores das Séries de Gráficos
            // Define 30 cores padrão - a Primeira não é usada
            Color[] colors = { new Color(), Color.Blue, Color.Red, 
                                   Color.YellowGreen, Color.Black, Color.DarkOrange,
                                   Color.DarkGray, Color.Purple, Color.SteelBlue, 
                                   Color.Wheat, Color.Sienna, Color.PeachPuff,
                                   Color.PapayaWhip, Color.Aqua, Color.SeaShell,
                                   Color.Navy, Color.Brown, Color.Peru,
                                   Color.Magenta, Color.Coral, Color.Moccasin,
                                   Color.Gold, Color.DarkKhaki, Color.Maroon,
                                   Color.Bisque, Color.DarkOrchid, Color.Lime,
                                   Color.Beige, Color.Salmon, Color.Linen };
            //
            MarkerKind[] kinds = {new MarkerKind(), MarkerKind.Diamond, MarkerKind.Cross, MarkerKind.Circle, 
                                  MarkerKind.Star, MarkerKind.InvertedTriangle, MarkerKind.Plus,
                                  MarkerKind.Pentagon };
            //
            SeriesCollection series = xrChart.Series;

            int j = 0;
            for (int i = 0; i < series.Count; i++)
            {
                series[i].View.Color = colors[i];
                ((PointSeriesView)series[i].View).PointMarkerOptions.FillStyle.FillMode = FillMode.Solid;
                //
                ((PointSeriesView)series[i].View).PointMarkerOptions.Size = 10;

                // Marcadores com Lista Circular. Acabando o Tamanho Começa Novamente - Multiplos de 7
                if (i % 7 == 0)
                {
                    j = 0;
                }
                ((PointSeriesView)series[i].View).PointMarkerOptions.Kind = kinds[j];
                j++;
            }
            #endregion
        }

        /// <summary>
        /// Seta a fonte de uma XRTableCell
        /// </summary>
        /// <param name="xrTable"></param>
        public static void SetFontXRTableCell(XRTableCell xrTableCell)
        {
            string nomeFonte = "Times New Roman";
            float size = 8.00F;
            Font font = new Font(nomeFonte, size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            xrTableCell.Font = font;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Fonte é Times New Roman</returns>
        private bool IsTipoFonteTimesNewRoman()
        {
            return this.fonteRelatorio == StringEnum.GetStringValue(FonteRelatorio.Times);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Fonte é Arial</returns>
        private bool IsTipoFonteArial()
        {
            return this.fonteRelatorio == StringEnum.GetStringValue(FonteRelatorio.Arial);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Fonte é Courier New</returns>
        private bool IsTipoFonteCourierNew()
        {
            return this.fonteRelatorio == StringEnum.GetStringValue(FonteRelatorio.Courier);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Fonte é Arial Narrow</returns>
        private bool IsTipoFonteArialNarrow()
        {
            return this.fonteRelatorio == StringEnum.GetStringValue(FonteRelatorio.ArialNarrow);
        }
        
        public XRControlStyle getControlStyle(string styleName)
        {

            return xrControlStyleDic[styleName];
        }

        public static string FormataPercent(decimal number)
        {
            string percentFormat = Financial.Util.StringEnum.GetStringValue(Financial.Relatorio.ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            return FormataPercent(number, percentFormat);
        }

        public static string FormataPercent(decimal number, string percentFormat)
        {
            return (number / 100M).ToString(percentFormat);
        }

        public static void FillTableRows(XRTable table, List<string> lineTexts)
        {
            for (int lineCount = 0; lineCount < (table.Rows.Count - 1); lineCount++)
            {
                string text = "";
                XRTableRow row = table.Rows[lineCount + 1];
                if (lineCount <= lineTexts.Count)
                {
                    text = lineTexts[lineCount];
                }
                ((XRTableCell)row.Cells[0]).Text = text;
            }
        }

        public static string DashIfNull(string text)
        {
            return String.IsNullOrEmpty(text) ? "-" : text;
        }
    }
}