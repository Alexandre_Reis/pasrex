﻿//using System;
//using System.Web;
//using System.Collections;
//using System.Web.Services;
//using System.Web.Services.Protocols;
//using System.Web.UI.WebControls;
//using System.Collections.Generic;
//using Financial.Investidor;
//using DevExpress.Web;
//using EntitySpaces.Interfaces;
//using Financial.Investidor.Controller;
//using Financial.Investidor.Enums;
//using System.Web.Script.Services;
//using Financial.Util;
//using System.Threading;


///// <summary>
///// Summary description for Processamento
///// </summary>
//[WebService(Namespace = "http://tempuri.org/")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//[ScriptService]
//public class Processamento : System.Web.Services.WebService {

//    public Processamento () {

//        //Uncomment the following line if using designed components 
//        //InitializeComponent(); 
//    }

//    [WebMethod]
//    public string GetCliente()
//    {
//        string clienteProcesso = Context.User.Identity.Name + ":ClienteProcessamento";
//        return Context.Cache[clienteProcesso] as string;
//    }

//    [WebMethod]
//    public string GetProgresso()
//    {
//        string progresso = Context.User.Identity.Name + ":ProgressoProcessamento";
//        return Context.Cache[progresso] as string;
//    }

//    [WebMethod]
//    public void ProcessaDiario(int tipoProcessamento, string listaClientes)
//    {
//        char[] caracter = new char[] {'|'};
//        StringTokenizer token = new StringTokenizer(listaClientes, caracter);

//        string clienteProcesso = Context.User.Identity.Name + ":ClienteProcessamento";
//        string progresso = Context.User.Identity.Name + ":ProgressoProcessamento";

//        int totalProcessado = 0;
//        int totalProcessar = 100;
//        int tempoPorCliente = (int) totalProcessar / token.Count();

//        Context.Cache[progresso] = 0;
        
//        foreach (string item in token)
//        {
//            int idCliente = Convert.ToInt32(item);

//            Cliente cliente = new Cliente();
//            List<esQueryItem> campos = new List<esQueryItem>();
//            campos.Add(cliente.Query.DataDia);
//            campos.Add(cliente.Query.Status);
//            campos.Add(cliente.Query.Apelido);
//            cliente.LoadByPrimaryKey(campos, idCliente);
//            DateTime dataDia = cliente.DataDia.Value;
//            int status = cliente.Status.Value;
//            string apelido = cliente.Apelido;

//            Context.Cache[clienteProcesso] = apelido;
            
//            switch (tipoProcessamento)
//            {
//                case 0: //Rotina de Cálculo
//                    ProcessaCalculo(idCliente, dataDia);                    
//                    break;
//                case 1: //Fechamento
//                    ProcessaFechamento(idCliente, dataDia);
//                    break;
//                case 2: //Abertura
//                    ProcessaAbertura(idCliente, dataDia);
//                    break;
//            }

//            totalProcessado += tempoPorCliente;
//            Context.Cache[progresso] = Convert.ToString(totalProcessado);
//        }

//        totalProcessado = 100;
//        Context.Cache[progresso] = Convert.ToString(totalProcessado);

//    }
        
//    private void ProcessaAbertura(int idCliente, DateTime dataAbertura)
//    {
//        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
//        controllerInvestidor.ExecutaAbertura(idCliente, dataAbertura);
//    }

//    private void ProcessaFechamento(int idCliente, DateTime dataFechamento)
//    {
//        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
//        controllerInvestidor.ExecutaDivulgacao(idCliente, dataFechamento);
//    }

//    private void ProcessaCalculo(int idCliente, DateTime dataCalculo)
//    {
//        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
//        //controllerInvestidor.ExecutaFechamento(idCliente, dataCalculo);
//    }

//    private void ProcessaRetroacao(int idCliente, DateTime dataRetroacao, int tipoRetroacao)
//    {
//        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();

//        if (tipoRetroacao == 0) //Termina a retroação com o cliente aberto
//        {
//            controllerInvestidor.ReprocessaPosicao(idCliente, dataRetroacao, TipoReprocessamento.Abertura);
//        }
//        else //Termina a retroação com o cliente fechado
//        {
//            controllerInvestidor.ReprocessaPosicao(idCliente, dataRetroacao, TipoReprocessamento.Fechamento);
//        }
//    }

//    private void ProcessaPeriodo(int idCliente, DateTime dataFinal)
//    {
//        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
//        //controllerInvestidor.ExecutaCalculoPeriodo(idCliente, dataFinal, this.pro);
//    }
    
//}

