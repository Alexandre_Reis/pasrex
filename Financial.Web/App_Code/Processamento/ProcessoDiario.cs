﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Enums;
using Financial.Investidor.Controller;
using Financial.Util;
using Financial.Investidor.Enums;
using Financial.Security.Enums;
using Financial.Util.Enums;

namespace Financial.Processamento
{
    /// <summary>
    /// Summary description for ProcessoDiario
    /// </summary>
    public class ProcessoDiario
    {
        public void ProcessaDiario(int idCliente, TipoProcessamento tipoProcessamento, ParametrosProcessamento parametrosProcessamento)
        {

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.StatusRealTime);
            campos.Add(cliente.Query.CalculaRealTime);
            cliente.LoadByPrimaryKey(campos, idCliente);

            DateTime dataDia = cliente.DataDia.Value;
            int status = cliente.Status.Value;
            string apelido = cliente.Apelido;
            int subOrigem;
            bool liberarClienteAoFinal = true;
            string detalhesErro = "";
            HistoricoLog historicoLog = new HistoricoLog();
            DateTime dataInicio = DateTime.Now;

            if (tipoProcessamento == TipoProcessamento.Fechamento)
            {
                subOrigem = (int)HistoricoLogSubOrigem.FechamentoSucesso;
            }
            else if (tipoProcessamento == TipoProcessamento.Abertura)
            {
                subOrigem = (int)HistoricoLogSubOrigem.AberturaSucesso;
            }
            else
            {
                //Fechamento
                subOrigem = (int)HistoricoLogSubOrigem.Divulgacao;
            }

            try
            {
                if (cliente.IsProcessando == "S")
                {
                    liberarClienteAoFinal = false;
                    throw new Exception("Cliente " + cliente.IdCliente.Value.ToString() +
                                    " - " + cliente.Apelido + " está sendo processado no momento. Processo cancelado.");
                }

                //Apaga último log de processamento
                this.clearLogProcessamento(idCliente, dataDia);

                //Inicia o processamento mudando o status IsProcessando
                cliente.IsProcessando = "S";
                cliente.Save();
                //

                ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
                switch (tipoProcessamento)
                {
                    case TipoProcessamento.Fechamento: //Fechamento
                        historicoLog.Descricao = "Fechamento do cliente " + idCliente.ToString() + " (" + apelido + ")" + " - Data: " + dataDia.ToShortDateString();
                        controllerInvestidor.ExecutaFechamento(idCliente, dataDia, parametrosProcessamento);

                        //Atualiza StatusRealTime para NaoExecutar***********                                    
                        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                        cliente.Save();
                        //***************************************************
                        break;
                    case TipoProcessamento.Divulgacao: //Divulgacao
                        historicoLog.Descricao = "Divulgacao do cliente " + idCliente.ToString() + " (" + apelido + ")" + " - Data: " + dataDia.ToShortDateString();
                        controllerInvestidor.ExecutaDivulgacao(idCliente, dataDia, true);

                        //Atualiza StatusRealTime para NaoExecutar***********
                        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                        cliente.Save();
                        IntegracaoAnbid.ExportarXml(idCliente, dataDia);
                        //***************************************************
                        break;
                    case TipoProcessamento.Abertura: //Abertura
                        historicoLog.Descricao = "Abertura do cliente " + idCliente.ToString() + " (" + apelido + ")" + " - Data: " + dataDia.ToShortDateString();
                        controllerInvestidor.ExecutaAbertura(idCliente, dataDia, parametrosProcessamento);
                        break;
                }

                this.setLogProcessamento(idCliente, dataDia, dataInicio, "", 0);
                this.clearLogReprocesamento(idCliente, dataDia);

                //
                
                //chama integracao cyrnel
            }
            catch (Exception ex)
            {
                if (subOrigem == (int)HistoricoLogSubOrigem.AberturaSucesso)
                {
                    subOrigem = (int)HistoricoLogSubOrigem.AberturaErro;
                }
                else if (subOrigem == (int)HistoricoLogSubOrigem.FechamentoSucesso)
                {
                    subOrigem = (int)HistoricoLogSubOrigem.FechamentoErro;
                }
                else
                {
                    //Fechamento
                    subOrigem = (int)HistoricoLogSubOrigem.DivulgacaoComErro;
                }

                detalhesErro = ex.Message + "#STACKTRACE#" + ex.StackTrace;

                historicoLog.Descricao = "Erro " + historicoLog.Descricao + " - Detalhes: " + detalhesErro;

                this.setLogProcessamento(idCliente, dataDia, dataInicio, ex.Message, 1);

                throw new Exception(detalhesErro);
            }
            finally
            {
                if (liberarClienteAoFinal)
                {
                    cliente.IsProcessando = "N";
                    cliente.Save();
                }

                //Log do Processo
                DateTime dataFim = DateTime.Now;
                historicoLog.Login = HttpContext.Current.User.Identity.Name;
                historicoLog.Maquina = Utilitario.GetLocalIp();
                historicoLog.Cultura = "";
                historicoLog.DataInicio = dataInicio;
                historicoLog.DataFim = dataFim;
                historicoLog.Origem = (int)HistoricoLogOrigem.Processamento;
                historicoLog.SubOrigem = subOrigem;
                historicoLog.IdCliente = idCliente;
                historicoLog.Descricao = StringExt.Truncate(historicoLog.Descricao, 8000);
                historicoLog.Save();
            }
        }

        private void setLogProcessamento(int idCliente, DateTime dataDia, DateTime dataInicio, String mensagem, int erro)
        {
            LogProcessamento logProcessamento = new LogProcessamento();
            logProcessamento.IdCliente = idCliente;
            logProcessamento.Data = dataDia;
            logProcessamento.Login = HttpContext.Current.User.Identity.Name;
            logProcessamento.Tipo = (int)TipoPerfilProcessamento.Manual;
            logProcessamento.DataInicio = dataInicio;
            logProcessamento.DataFim = DateTime.Now;
            logProcessamento.DataInicialPeriodo = dataDia;
            logProcessamento.DataFinalPeriodo = dataDia;
            logProcessamento.Erro = erro;
            logProcessamento.Mensagem = mensagem;
            logProcessamento.Save();
        }

        private void clearLogProcessamento(int idCliente, DateTime dataDia)
        {
            LogProcessamentoQuery logProcessamentoQuery = new LogProcessamentoQuery();
            logProcessamentoQuery.Where(logProcessamentoQuery.IdCliente.Equal(idCliente));
            logProcessamentoQuery.Where(logProcessamentoQuery.Data.Equal(dataDia));

            LogProcessamentoCollection logProcessamentoCollection = new LogProcessamentoCollection();
            logProcessamentoCollection.Load(logProcessamentoQuery);
            logProcessamentoCollection.MarkAllAsDeleted();
            logProcessamentoCollection.Save();
        }

        private void clearLogReprocesamento(int idCliente, DateTime dataDia)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            boletoRetroativo.LoadByPrimaryKey(idCliente);
            if (boletoRetroativo.DataBoleto.Equals(dataDia))
            {
                boletoRetroativo.MarkAsDeleted();
                boletoRetroativo.Save();
            }
        }
            
    }
}
