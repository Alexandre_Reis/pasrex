﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Enums;
using Financial.Investidor.Controller;
using Financial.Util;
using Financial.Investidor.Enums;
using Financial.Security.Enums;
using Financial.Util.Enums;
using Financial.Common.Enums;

namespace Financial.Processamento
{
    /// <summary>
    /// Summary description for ProcessoPeriodo
    /// </summary>
    public class ProcessoPeriodo
    {
        public void ProcessaPeriodo(int idCliente, TipoProcessamento tipoProcessamento, ParametrosProcessamento parametrosProcessamento,
                                    DateTime dataFinal)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.StatusRealTime);
            campos.Add(cliente.Query.CalculaRealTime);
            cliente.LoadByPrimaryKey(campos, idCliente);

            DateTime dataDia = cliente.DataDia.Value;
            int status = cliente.Status.Value;
            string apelido = cliente.Apelido;
            int subOrigem;
            bool liberarClienteAoFinal = true;
            string detalhesErro = "";
            HistoricoLog historicoLog = new HistoricoLog();
            DateTime dataInicioProcessamento = DateTime.Now;

            if (tipoProcessamento == TipoProcessamento.AvancoPeriodo)
            {
                subOrigem = (int)HistoricoLogSubOrigem.AvancoPeriodoSucesso;
            }
            else
            {
                //Retroacao
                subOrigem = (int)HistoricoLogSubOrigem.RetroacaoSucesso;
            }

            try
            {
                if (cliente.IsProcessando == "S")
                {
                    liberarClienteAoFinal = false;
                    throw new Exception("Cliente " + cliente.IdCliente.Value.ToString() +
                                    " - " + cliente.Apelido + " está sendo processado no momento. Processo cancelado.");
                }

                //Inicia o processamento mudando o status IsProcessando
                cliente.IsProcessando = "S";
                cliente.Save();
                //

                ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
                switch (tipoProcessamento)
                {
                    case TipoProcessamento.AvancoPeriodo:

                        //Apaga último log de processamento
                        this.clearLogProcessamento(idCliente, dataDia, dataFinal);

                        historicoLog.Descricao = "Avança Período do cliente " + idCliente.ToString() + " (" + apelido + ")" + " - Data Final: " + dataFinal.ToShortDateString();
                        controllerInvestidor.ExecutaCalculoPeriodo(idCliente, dataFinal, parametrosProcessamento);

                        //Atualiza StatusRealTime para NaoExecutar***********                                    
                        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                        cliente.Save();
                        //***************************************************
                        
                        IntegracaoAnbid.ExportarXml(idCliente, dataDia);


                        //Log Processamento
                        this.setLogProcessamento(idCliente, dataDia, dataInicioProcessamento, dataFinal, "", false);

                        break;
                    case TipoProcessamento.Retroacao:

                        //Apaga último log de processamento
                        this.clearLogProcessamento(idCliente, dataFinal, dataDia);

                        historicoLog.Descricao = "Reprocessamento do cliente " + idCliente.ToString() + " (" + apelido + ")" + " - Data Final: " + dataFinal.ToShortDateString();
                        controllerInvestidor.ReprocessaPosicao(idCliente, dataFinal, TipoReprocessamento.Fechamento, parametrosProcessamento.MantemFuturo);

                        //Atualiza StatusRealTime para NaoExecutar***********
                        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                        cliente.Save();
                        //***************************************************
                        break;                    
                }

                IntegracaoAnbid.ExportarXml(idCliente, dataDia);

            }
            catch (Exception ex)
            {
                if (subOrigem == (int)HistoricoLogSubOrigem.AvancoPeriodoSucesso)
                {
                    subOrigem = (int)HistoricoLogSubOrigem.AvancoPeriodoErro;
                }
                else
                {
                    //Retroacao
                    subOrigem = (int)HistoricoLogSubOrigem.RetroacaoErro;
                }

                detalhesErro = ex.Message + " - Processo cancelado.#STACKTRACE#" + ex.StackTrace;

                historicoLog.Descricao = "Erro " + historicoLog.Descricao + " - Detalhes: " + detalhesErro;

                //Log Processamento
                this.setLogProcessamento(idCliente, dataDia, dataInicioProcessamento, dataFinal, ex.Message, true);

                throw new Exception(detalhesErro);
            }

            finally
            {
                if (liberarClienteAoFinal)
                {
                    cliente.IsProcessando = "N";
                    cliente.Save();
                }

                //Log do Processo
                DateTime dataFim = DateTime.Now;
                historicoLog.Login = HttpContext.Current.User.Identity.Name;
                historicoLog.Maquina = Utilitario.GetLocalIp();
                historicoLog.Cultura = "";
                historicoLog.DataInicio = dataInicioProcessamento;
                historicoLog.DataFim = dataFim;
                historicoLog.Origem = (int)HistoricoLogOrigem.Processamento;
                historicoLog.SubOrigem = subOrigem;
                historicoLog.IdCliente = idCliente;
                historicoLog.Descricao = StringExt.Truncate(historicoLog.Descricao, 8000);
                historicoLog.Save();
            }
        }

        private void setLogProcessamento(int idCliente, DateTime dataDia, DateTime dataInicioProcessamento, DateTime dataFinal, String mensagem, Boolean erro)
        {
            DateTime dataAux = dataDia;

            BoletoRetroativoQuery boletoRetroativoQuery = new BoletoRetroativoQuery();
            BoletoRetroativoCollection boletoRetroativoCollection = new BoletoRetroativoCollection();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            
            while (DateTime.Compare(cliente.DataDia.Value, dataAux) >= 0)
            {
                LogProcessamento logProcessamento = new LogProcessamento();
                logProcessamento.IdCliente = idCliente;
                logProcessamento.Data = dataAux;
                logProcessamento.Login = HttpContext.Current.User.Identity.Name;
                logProcessamento.Tipo = (int)TipoPerfilProcessamento.Manual;
                logProcessamento.DataInicio = dataInicioProcessamento;
                logProcessamento.DataFim = DateTime.Now;
                logProcessamento.DataInicialPeriodo = dataDia;
                logProcessamento.DataFinalPeriodo = dataFinal;

                if (erro && DateTime.Compare(cliente.DataDia.Value, dataAux) == 0)
                {
                    logProcessamento.Erro = 1;
                    logProcessamento.Mensagem = mensagem;
                }
                else
                {
                    logProcessamento.Erro = 0;
                    logProcessamento.Mensagem = "";
                }

                logProcessamento.Save();

                if (cliente.IdLocal == LocalFeriadoFixo.Brasil)
                {
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, cliente.IdLocal.Value, TipoFeriado.Outros);
                }
            }

            //Limpa controle de reprocessamento para o Cliente.
            boletoRetroativoQuery.Where(boletoRetroativoQuery.IdCliente == idCliente);
            boletoRetroativoQuery.Where(boletoRetroativoQuery.DataBoleto <= dataAux);
            boletoRetroativoCollection.Load(boletoRetroativoQuery);

            boletoRetroativoCollection.MarkAllAsDeleted();
            boletoRetroativoCollection.Save();
            
        }

        private void clearLogProcessamento(int idCliente, DateTime dataDia, DateTime dataFinal)
        {
            LogProcessamentoQuery logProcessamentoQuery = new LogProcessamentoQuery();
            logProcessamentoQuery.Where(logProcessamentoQuery.IdCliente.Equal(idCliente));
            logProcessamentoQuery.Where(logProcessamentoQuery.Data.Between(dataDia, dataFinal));

            LogProcessamentoCollection logProcessamentoCollection = new LogProcessamentoCollection();
            logProcessamentoCollection.Load(logProcessamentoQuery);
            logProcessamentoCollection.MarkAllAsDeleted();
            logProcessamentoCollection.Save();
        }

    }
}
