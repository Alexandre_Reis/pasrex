using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Export;
using System.Collections.Generic;
using System.IO;
using Financial.Investidor;
using EntitySpaces.Interfaces;

/// <summary>
/// Summary description for IntegracaoCyrnel
/// </summary>
public class IntegracaoAnbid
{
    public IntegracaoAnbid()
    {   
        //
        // TODO: Add constructor logic here
        //
    }

    public static void ExportarXml(int idCliente, DateTime inicio)
    {
        string path = System.Configuration.ConfigurationSettings.AppSettings["IntegracaoAnbid"];
        if (string.IsNullOrEmpty(path))
            return;

        TabelaInterfaceCliente t = new TabelaInterfaceCliente();
        t.LoadByPrimaryKey(idCliente, 601);

        if (t.IdCliente == null)
            return;

        XMLPosicaoAnbid xmlPosicaoAnbid = new XMLPosicaoAnbid();
        List<int> idsCliente = new List<int>();
        idsCliente.Add(idCliente);

        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(campos, idCliente);

        DateTime fim = cliente.DataDia.Value;



        for (DateTime dt = inicio; dt <= fim; dt = dt.AddDays(1))
        {
            MemoryStream ms = new MemoryStream();
            string nome = "";
            xmlPosicaoAnbid.ExportaCarteiraXMLAnbid(dt, idsCliente, out ms, out nome);
            ms.Position = 0;            
            using (FileStream fileStream = System.IO.File.Create(Path.Combine(path,nome),Convert.ToInt32(ms.Length)))
                CopyStream(ms, fileStream);       

        }



        
    }

    public static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[16 * 1024];
        int read;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, read);
        }
    }
}
