﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace Financial.InterfacesCustom.GeracaoFuturo
{
    /// <summary>
    /// Summary description for ValidateLogin
    /// </summary>
    public class ValidateLogin : SoapHeader
    {
        #region Properties
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
        #endregion
    }

}