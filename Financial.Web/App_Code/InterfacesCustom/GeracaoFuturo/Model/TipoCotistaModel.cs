﻿using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista.Enums;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// TipoCotistaModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "TipoCotista", Namespace = "")]
    public class TipoCotistaModel 
    {
        #region Fields
        private List<TipoCotistaViewModel> _tipoCotistaList;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoCotistaModel"/> class.
        /// </summary>
        public TipoCotistaModel() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoCotistaModel"/> class.
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        public TipoCotistaModel(OrigemTipoCotista? tipo)
        {
            _tipoCotistaList = new List<TipoCotistaViewModel>();

            if (tipo == null || tipo == OrigemTipoCotista.CVM)
            {
                var tipoCvmList = Enum.GetValues(typeof(Financial.InvestidorCotista.Enums.TipoCotistaCvm));
                foreach (var item in tipoCvmList)
                {
                    var e = (TipoCotistaCvm)item;
                    _tipoCotistaList.Add(new TipoCotistaViewModel
                    {
                        Tipo = OrigemTipoCotista.CVM,
                        Codigo = (int)e,
                        Descricao = CotEnumUtil.GetEnumDescription(e)
                    });
                }
            }
            if (tipo == null || tipo == OrigemTipoCotista.Anbima)
            {
                var tipoAnbimaList = Enum.GetValues(typeof(Financial.InvestidorCotista.Enums.TipoCotistaAnbima));
                foreach (var item in tipoAnbimaList)
                {
                    var e = (TipoCotistaAnbima)item;
                    _tipoCotistaList.Add(new TipoCotistaViewModel
                    {
                        Tipo = OrigemTipoCotista.Anbima,
                        Codigo = (int)e,
                        Descricao = CotEnumUtil.GetEnumDescription(e)
                    });
                }
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public StandardResponse Response { get; set; }

        /// <summary>
        /// Gets or sets the tipo cotista.
        /// </summary>
        /// <value>
        /// The tipo cotista.
        /// </value>
        [XmlArray(ElementName = "TipoCotista")]
        [XmlArrayItem(ElementName = "Tipo")]
        public List<TipoCotistaViewModel> TipoCotista
        {
            get
            {
                return _tipoCotistaList;
            }
            set { _tipoCotistaList = value; }
        }

        #endregion
    }

}