﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Financial.InvestidorCotista;
using Financial.Security;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// Summary description for CadastrosCotista
    /// </summary>
    public class CadastrosCotista : CadastroCotistaList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CadastrosCotista"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public CadastrosCotista()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CadastrosCotista"/> class.
        /// </summary>
        /// <param name="nome">The nome.</param>
        /// <param name="nomeabreviado">The nomeabreviado.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        public CadastrosCotista(string nome, string nomeabreviado, int idUsuario)
        {
            var cot = new CotistaQuery("cot");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");
            cot.Select(cot);
            cot.InnerJoin(perCotistaQuery).On(cot.IdCotista.Equal(perCotistaQuery.IdCotista));
            if (!string.IsNullOrEmpty(nome))
            {
                cot.Where(cot.Nome.Like(nome));
            }
            else if (!string.IsNullOrEmpty(nomeabreviado))
            {
                cot.Where(cot.Nome.Like(nomeabreviado));
            }
            
            cot.Where(perCotistaQuery.IdUsuario.Equal(idUsuario));

            CotistaCollection cotistas = new CotistaCollection();
            cotistas.Load(cot);

            foreach (var cotista in cotistas)
            {
                this.Add(new CadastroCotista(cotista));
            }
        }
    }
}