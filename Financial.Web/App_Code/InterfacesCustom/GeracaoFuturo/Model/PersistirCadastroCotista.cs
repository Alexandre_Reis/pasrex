﻿using Financial.CRM.Enums;
using Financial.Integracao.Excel;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InvestidorCotista;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Financial.Security;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// Summary description for PersistirCadastroCotista
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "PersistirCadastroCotista", Namespace = "")]
    public class PersistirCadastroCotista : CadastroCotista
    {
        #region Properties
        /// <summary>
        /// Gets or sets the opcao.
        /// </summary>
        /// <value>
        /// The opcao.
        /// </value>
        [XmlElement(ElementName = "Opcao")]
        public OpcaoPersistirCotista Opcao { get; set; }

        /// <summary>
        /// Gets or sets the identifier cotista.
        /// </summary>
        /// <value>
        /// The identifier cotista.
        /// </value>
        [XmlElement(ElementName = "IdCotista")]
        public int IdCotista { get; set; }

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        [XmlElement(ElementName = "Response")]
        public StandardResponse Response { get; set; }

        #endregion

        #region public Methods
        public CadastroCotista PersistirCotista(int idUsuario)
        {
            this.Response = new StandardResponse { CodigoErro = 0, MsgErro = "OK" };

            var item = new ValoresExcelCadastroCotista();

            var usuario = new Usuario();
            if (usuario.LoadByPrimaryKey(idUsuario))
            {
                item.CriadoPor = usuario.Login;
            }

            if (Opcao == OpcaoPersistirCotista.Inclusao)
            {
                var cotista = new Cotista();
                if (cotista.LoadByPrimaryKey(IdCotista))
                {
                    this.Response.CodigoErro = 1;
                    Response.MsgErro = string.Format("Cotista codigo {0} já	existente", IdCotista);
                    throw new PersistirCadastroException(Response.MsgErro);
                }
            }

            if (Opcao == OpcaoPersistirCotista.Alteracao)
            {
                var cotista = new Cotista();
                if (!cotista.LoadByPrimaryKey(IdCotista))
                {
                    this.Response = new StandardResponse();
                    this.Response.CodigoErro = 2;
                    Response.MsgErro = string.Format("Cotista codigo {0} inexistente", IdCotista);
                    throw new PersistirCadastroException(Response.MsgErro);
                }
            }

            item.IdCotista = IdCotista;
            item.Opcao = Opcao == OpcaoPersistirCotista.Alteracao ? "A" : "I";

            // informacoes basicas
            item.Nome = this.InformacoesBasicas.NomePessoa;
            item.Apelido = InformacoesBasicas.NomeAbreviadoPessoa;
            if (InformacoesBasicas.Tipo != null) item.TipoPessoa = (CRM.Enums.TipoPessoa)InformacoesBasicas.Tipo;
            item.Cnpj = InformacoesBasicas.CPfCnpj;

            // informacoes adicionais
            item.Sexo = Enums.Util.GetEnumDescription(InformacoesAdicionais.Sexo);
            item.EstadoCivilCotista = InformacoesAdicionais.EstadoCivilPessoa;
            item.DataNascimento = InformacoesAdicionais.DataNascimento;
            item.Profissao = InformacoesAdicionais.Profissao;
            item.PaisNacionalidade = (byte?)InformacoesAdicionais.PaisNacionalidade;
            item.CodigoInterface = InformacoesAdicionais.CodigoInterface;
            item.PoliticamenteExposta = Enums.Util.GetEnumDescription(InformacoesAdicionais.PoliticamenteExposta);
            item.PessoaVinculada = Enums.Util.GetEnumDescription(InformacoesAdicionais.PessoaVinculada);
            item.UFNaturalidade = InformacoesAdicionais.UfNaturalidade;
            if (InformacoesAdicionais.SituacaoLegal != null) item.SituacaoLegal = (SituacaoLegalPessoa)InformacoesAdicionais.SituacaoLegal;
            item.NomePai = InformacoesAdicionais.NomePai;
            item.NomeMae = InformacoesAdicionais.NomeMae;
            if (InformacoesAdicionais.DataVencimentoCadastro != null)
                item.DataVencimentoCadastro = (DateTime)InformacoesAdicionais.DataVencimentoCadastro;

            // cotista
            item.StatusCotista = Cotista.Ativo;
            item.IsentoIR = Enums.Util.GetEnumDescription(Cotista.IsentoIr);
            item.IsentoIOF = Enums.Util.GetEnumDescription(Cotista.IsentoIof);
            item.TipoTributacaoCotista = Cotista.TipoTributacao;
            if (Cotista.TipoCotistaCvm != null)
                item.TipoCotistaCVM = (InvestidorCotista.Enums.TipoCotista)Cotista.TipoCotistaCvm;
            if (Cotista.TipoCotistaAnbima != null)
                item.TipoCotistaAnbima = (InvestidorCotista.Enums.TipoCotistaAnbima)Cotista.TipoCotistaAnbima;
            item.CodigoInterfaceCotista = Cotista.CodigoInterface;
            item.PendenciaCadastral = Cotista.PendenciaCadastral;
            if (Cotista.DataExpiracao != null) item.DataExpiracaoCotista = (DateTime)Cotista.DataExpiracao;

            //enderecos
            item.PessoaEnderecoList = new System.Collections.Generic.List<CRM.PessoaEndereco>();
            Enderecos.ForEach(t => item.PessoaEnderecoList.Add(new CRM.PessoaEndereco
            {
                TipoEndereco = Enums.Util.GetEnumDescription(t.Tipo),
                RecebeCorrespondencia = Enums.Util.GetEnumDescription(t.Correspondencia),
                Endereco = t.Logradouro,
                Numero = t.Numero,
                Complemento = t.Complemento,
                Bairro = t.Bairro,
                Cidade = t.Cidade,
                Uf = t.UF,
                Cep = t.CEP,
                Pais = t.Pais,
                IdEndereco = t.IdEndereco
            }));

            // email
            item.PessoaEmailList = new System.Collections.Generic.List<CRM.PessoaEmail>();
            Emails.ForEach(t => item.PessoaEmailList.Add(new CRM.PessoaEmail
            {
                IdEmail = t.IdEmail,
                Email = t.Email
            }));

            // telefone
            item.PessoaTelefoneList = new System.Collections.Generic.List<CRM.PessoaTelefone>();

            Telefones.ForEach(t => item.PessoaTelefoneList.Add(new CRM.PessoaTelefone
            {
                TipoTelefone = (byte)t.TipoTelefone,
                Ddi = t.DDI,
                Ddd = t.DDD,
                Numero = t.Numero,
                Ramal = t.Ramal,
                IdTelefone = t.IdTelefone

            }));

            var basePage = new ImportacaoBasePage
            {
                valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista> { item }
            };

            basePage.CarregaCadastroCotista();

            return null;

        }
        #endregion
    }
}