﻿using Financial.CRM;
using Financial.Fundo;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.Investidor;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    [Serializable]
    [XmlRoot(ElementName = "ConsultaFundo", Namespace = "")]
    public class ConsultaFundoModel : BaseModel<Carteira, ConsultaFundoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoModel"/> class.
        /// </summary>
        public ConsultaFundoModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoModel"/> class.
        /// </summary>
        /// <param name="idFundo">The identifier fundo.</param>
        public ConsultaFundoModel(int? idFundo)
        {
            if (idFundo != null)
            {
                var consisteFundo = new Carteira();
                if (!consisteFundo.LoadByPrimaryKey((int)idFundo))
                {
                    throw new InvalidFundException(String.Format("Codigo do fundo: {0}, nao encontrado.", idFundo));
                }
            }

            var carteira = new CarteiraQuery("car");
            var cliente = new ClienteQuery("cli");
            var pessoa = new PessoaQuery("P");

            carteira.Select(carteira,
                            pessoa.Cpfcnpj.As("Cnpj"));
            carteira.InnerJoin(cliente).On(cliente.IdCliente == carteira.IdCarteira);
            carteira.InnerJoin(pessoa).On(pessoa.IdPessoa == cliente.IdPessoa);
            carteira.Where(cliente.IdTipo.In(200, 500, 510));

            if (idFundo != null)
            {
                carteira.Where(carteira.IdCarteira == idFundo);
            }

            var carteiraList = new CarteiraCollection();
            carteiraList.Load(carteira);
            LoadViewModelList(carteiraList);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the consulta fundo.
        /// </summary>
        /// <value>
        /// The consulta fundo.
        /// </value>
        [XmlArray(ElementName = "Fundos")]
        [XmlArrayItem(ElementName = "Fundo")]
        public List<ConsultaFundoViewModel> ConsultaFundo
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}