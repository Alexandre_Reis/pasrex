﻿using Financial.CRM.Enums;
using Financial.Integracao.Excel;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InvestidorCotista;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Financial.InvestidorCotista.Enums;
using Financial.Security;
using Financial.Fundo;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// Summary description for PersistirMovimentoCotista
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "PersistirMovimentoCotista", Namespace = "")]
    public class PersistirMovimentoCotista
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PersistirMovimentoCotista"/> class.
        /// </summary>
        public PersistirMovimentoCotista()
        {
            this.Response = new StandardResponse { CodigoErro = 0, MsgErro = "OK" };
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the opcao.
        /// </summary>
        /// <value>
        /// The opcao.
        /// </value>
        [XmlElement(ElementName = "Opcao")]
        public OpcaoPersistirMovimentoCotista? Opcao { get; set; }

        /// <summary>
        /// Gets or sets the identifier cotista.
        /// </summary>
        /// <value>
        /// The identifier cotista.
        /// </value>
        [XmlElement(ElementName = "IdCotista")]
        public int IdCotista { get; set; }

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        [XmlElement(ElementName = "Response")]
        public StandardResponse Response { get; set; }

        /// <summary>
        /// Gets or sets the codigo fundo.
        /// </summary>
        /// <value>
        /// The codigo fundo.
        /// </value>
        [XmlElement(ElementName = "CodigoFundo")]
        public int CodigoFundo { get; set; }

        /// <summary>
        /// Gets or sets the data operacao.
        /// </summary>
        /// <value>
        /// The data operacao.
        /// </value>
        [XmlElement(ElementName = "DataOperacao")]
        public DateTime DataOperacao { get; set; }

        /// <summary>
        /// Gets or sets the tipo operacao.
        /// </summary>
        /// <value>
        /// The tipo operacao.
        /// </value>
        [XmlElement(ElementName = "TipoOperacao")]
        public TipoOperacaoCotista TipoOperacao { get; set; }

        /// <summary>
        /// Gets or sets the criterio resgate.
        /// </summary>
        /// <value>
        /// The criterio resgate.
        /// </value>
        [XmlElement(ElementName = "CriterioResgate")]
        public TipoResgateCotista? CriterioResgate { get; set; }

        /// <summary>
        /// Gets or sets the nota resgatada.
        /// </summary>
        /// <value>
        /// The nota resgatada.
        /// </value>
        [XmlElement(ElementName = "NotaResgatada")]
        public int? NotaResgatada { get; set; }

        /// <summary>
        /// Gets or sets the data cotizacao.
        /// </summary>
        /// <value>
        /// The data cotizacao.
        /// </value>
        [XmlElement(ElementName = "DataCotizacao")]
        public DateTime DataCotizacao { get; set; }

        /// <summary>
        /// Gets or sets the data liquidacao financeira.
        /// </summary>
        /// <value>
        /// The data liquidacao financeira.
        /// </value>
        [XmlElement(ElementName = "DataLiquidacaoFinanceira")]
        public DateTime DataLiquidacaoFinanceira { get; set; }

        /// <summary>
        /// Gets or sets the valor quantidade.
        /// </summary>
        /// <value>
        /// The valor quantidade.
        /// </value>
        [XmlElement(ElementName = "ValorQuantidade")]
        public decimal ValorQuantidade { get; set; }

        /// <summary>
        /// Gets or sets the tipo liquidacao.
        /// </summary>
        /// <value>
        /// The tipo liquidacao.
        /// </value>
        [XmlElement(ElementName = "TipoLiquidacao")]
        public int TipoLiquidacao { get; set; }

        /// <summary>
        /// Gets or sets the local negociacao.
        /// </summary>
        /// <value>
        /// The local negociacao.
        /// </value>
        [XmlElement(ElementName = "LocalNegociacao")]
        public int LocalNegociacao { get; set; }

        /// <summary>
        /// Gets or sets the dados conta.
        /// </summary>
        /// <value>
        /// The dados conta.
        /// </value>
        [XmlElement(ElementName = "DadosConta")]
        public int? DadosConta { get; set; }

        /// <summary>
        /// Gets or sets the observacao.
        /// </summary>
        /// <value>
        /// The observacao.
        /// </value>
        [XmlElement(ElementName = "Observacao")]
        public string Observacao { get; set; }

        #endregion

        #region public Methods

        /// <summary>
        /// Excluirs the movimento.
        /// </summary>
        public void ExcluirMovimento(int idUsuario)
        {
            Opcao = null;

            if (!ConsistirMovimento(idUsuario))
                return;

            var operacaoCotista = new OperacaoCotista();
            var operacao = new OperacaoCotistaQuery("o");
            operacao.Select(operacao);
            operacao.Where(operacao.IdCotista == IdCotista);
            operacao.Where(operacao.IdCarteira == CodigoFundo);
            operacao.Where(operacao.DataOperacao == DataOperacao);
            operacao.Where(operacao.TipoOperacao == TipoOperacao);
            if (CriterioResgate != null)
                operacao.Where(operacao.TipoResgate == CriterioResgate);
            if (NotaResgatada != null)
                operacao.Where(operacao.IdOperacaoResgatada == NotaResgatada);

            operacao.Where(operacao.DataConversao == DataCotizacao);
            operacao.Where(operacao.DataLiquidacao == DataLiquidacaoFinanceira);

            switch (TipoOperacao)
            {
                case TipoOperacaoCotista.ResgateCotas:
                case TipoOperacaoCotista.ComeCotas:
                case TipoOperacaoCotista.ResgateCotasEspecial:
                case TipoOperacaoCotista.AplicacaoCotas:
                    operacao.Where(operacao.Quantidade == ValorQuantidade);
                    break;

                case TipoOperacaoCotista.ResgateBruto:
                case TipoOperacaoCotista.ResgateLiquido:
                    operacao.Where(operacao.ValorBruto == ValorQuantidade);
                    break;

                default:
                    operacao.Where(operacao.ValorLiquido == ValorQuantidade);
                    break;
            }

            operacao.Where(operacao.IdFormaLiquidacao == TipoLiquidacao);
            operacao.Where(operacao.IdLocalNegociacao == LocalNegociacao);
            if (DadosConta != null)
                operacao.Where(operacao.IdConta == DadosConta);

            if (!string.IsNullOrEmpty(Observacao))
                operacao.Where(operacao.Observacao == Observacao);

            if (!operacaoCotista.Load(operacao))
            {
                Response.MsgErro = "Movimento nao encontrado.";
                Response.CodigoErro = 5;
                throw new InvalidFundException(Response.MsgErro);
            }

            operacaoCotista.MarkAsDeleted();
            operacaoCotista.Save();
        }

        /// <summary>
        /// Persistirs the movimento.
        /// </summary>
        /// <param name="idusuario">The idusuario.</param>
        public void PersistirMovimento(int idusuario)
        {
            if (!ConsistirMovimento(idusuario))
                return;

            try
            {
                var item = new ValoresExcelOperacaoCotista
                {
                    IdCotista = IdCotista,
                    IdCarteira = CodigoFundo,
                    DataOperacao = DataOperacao,
                    DataConversao = DataCotizacao,
                    DataLiquidacao = DataLiquidacaoFinanceira,
                    TipoOperacao = (byte)TipoOperacao
                };

                if (CriterioResgate != null)
                    item.TipoResgate = (byte)CriterioResgate;

                switch (TipoOperacao)
                {
                    case TipoOperacaoCotista.ResgateCotas:
                    case TipoOperacaoCotista.ComeCotas:
                    case TipoOperacaoCotista.ResgateCotasEspecial:
                    case TipoOperacaoCotista.AplicacaoCotas:
                        item.Quantidade = ValorQuantidade;
                        break;

                    case TipoOperacaoCotista.ResgateBruto:
                    case TipoOperacaoCotista.ResgateLiquido:
                        item.ValorLiquido = ValorQuantidade;
                        break;

                    default:
                        item.ValorBruto = ValorQuantidade;
                        break;
                }

                //item.CotaOperacao = CotaOperacao;

                //item.ValorIR = ValorIR;
                //item.ValorIOF = ValorIOF;
                //item.ValorPerformance = ValorPerformance;
                //item.RendimentoResgate = RendimentoResgate;

                var basePage = new ImportacaoBasePage
                {
                    valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista> { item }
                };

                var operacoes = basePage.CarregaOperacaoCotista(false);
            }
            catch (Exception ex)
            {
                Response.CodigoErro = 9;
                throw;
            }

        }

        /// <summary>
        /// Consistirs the movimento.
        /// </summary>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        /// <exception cref="Financial.InterfacesCustom.GeracaoFuturo.InvalidShareholderException"></exception>
        /// <exception cref="Financial.InterfacesCustom.GeracaoFuturo.InvalidFundException">
        /// </exception>
        public bool ConsistirMovimento(int idUsuario)
        {
            var msg = ValidarCotista(IdCotista, idUsuario);
            if (!string.IsNullOrEmpty(msg))
            {
                this.Response.CodigoErro = 1;
                throw new InvalidShareholderException(msg);
            }

            var consisteFundo = new Carteira();
            if (!consisteFundo.LoadByPrimaryKey(CodigoFundo))
            {
                Response.CodigoErro = 2;
                throw new InvalidFundException(String.Format("Codigo do fundo: {0}, nao encontrado.", CodigoFundo));
            }

            var dataFundo = new DataPosicaoFundoModel(CodigoFundo);
            if (dataFundo.DataPosicaoFundo == null || dataFundo.DataPosicaoFundo.Count == 0)
            {
                Response.CodigoErro = -1;
                throw new InvalidFundException(String.Format("Nao foi encontrada informacoes sobre data de posicao para o fundo: {0}.", CodigoFundo));
            }

            if (dataFundo.DataPosicaoFundo[0].Descricao == Investidor.Enums.StatusCliente.Divulgado)
            {
                Response.CodigoErro = 4;
                throw new InvalidFundException(String.Format("Fundo codigo {0} fechado.", CodigoFundo));
            }

            if (DadosConta != null)
            {
                var contacorrente = new Financial.Investidor.ContaCorrente();
                if (!contacorrente.LoadByPrimaryKey((int)DadosConta))
                {
                    Response.CodigoErro = 6;
                    throw new InvalidFundException(String.Format("Conta corrente codigo {0} nao encontrada.", DadosConta));
                }

                var cotista = new Cotista();
                cotista.LoadByPrimaryKey(IdCotista);
                if (contacorrente.IdPessoa != cotista.IdPessoa)
                {
                    Response.CodigoErro = 6;
                    throw new InvalidFundException(String.Format("Conta corrente codigo {0} pertence a outro cotista.", DadosConta));
                }
            }

            if (Opcao == OpcaoPersistirMovimentoCotista.Inclusao || Opcao == OpcaoPersistirMovimentoCotista.Alteracao)
            {
                var posicaoQuery = new PosicaoCotistaQuery("p");
                var posicaoCotista = new PosicaoCotista();

                posicaoQuery.Select(posicaoQuery.ValorBruto.Sum().As("VlBruto"));
                posicaoQuery.Select(posicaoQuery.ValorLiquido.Sum().As("VlLiquido"));
                posicaoQuery.Select(posicaoQuery.Quantidade.Sum().As("QtCotas"));
                posicaoQuery.Where(posicaoQuery.IdCotista == IdCotista);
                posicaoQuery.Where(posicaoQuery.IdCarteira == CodigoFundo);
                if (!posicaoCotista.Load(posicaoQuery))
                {
                    Response.CodigoErro = 7;
                    throw new InvalidFundException(String.Format("Posicao nao encontrada para o Cotista {0} no fundo {1} dia {2}.", IdCotista, CodigoFundo, DataOperacao));
                }
                var valorBruto = posicaoCotista.GetColumn("VlBruto");
                var valorLiq = posicaoCotista.GetColumn("VlLiquido");
                var qtCotas = posicaoCotista.GetColumn("QtCotas");

                if (valorBruto == null || valorBruto is System.DBNull || valorLiq == null || valorLiq is System.DBNull || qtCotas == null || qtCotas is System.DBNull)
                {
                    Response.CodigoErro = 7;
                    throw new InvalidFundException(String.Format("Posicao nao encontrada para o Cotista {0} no fundo {1} dia {2}.", IdCotista, CodigoFundo, DataOperacao));
                }

                switch (TipoOperacao)
                {
                    case TipoOperacaoCotista.ResgateBruto:
                    case TipoOperacaoCotista.ResgateTotal:
                    case TipoOperacaoCotista.Retirada:
                        if (ValorQuantidade > (decimal)valorBruto)
                        {
                            Response.CodigoErro = 7;
                            throw new InvalidFundException(String.Format("Saldo	insuficiente. Cotista {0} no fundo {1} dia {2}.", IdCotista, CodigoFundo, DataOperacao));
                        }
                        break;

                    case TipoOperacaoCotista.ResgateLiquido:
                        if (ValorQuantidade > (decimal)valorLiq)
                        {
                            Response.CodigoErro = 7;
                            throw new InvalidFundException(String.Format("Saldo	insuficiente. Cotista {0} no fundo {1} dia {2}.", IdCotista, CodigoFundo, DataOperacao));
                        }
                        break;
                    case TipoOperacaoCotista.ResgateCotas:
                    case TipoOperacaoCotista.ResgateCotasEspecial:
                        if (ValorQuantidade > (decimal)qtCotas)
                        {
                            Response.CodigoErro = 7;
                            throw new InvalidFundException(String.Format("Saldo	insuficiente. Cotista {0} no fundo {1} dia {2}.", IdCotista, CodigoFundo, DataOperacao));
                        }
                        break;
                }
            }
            return true;
        }

        #endregion
        #region private methods
        /// <summary>
        /// Validars the cotista.
        /// </summary>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        protected string ValidarCotista(int codigoCotista, int idUsuario)
        {
            var cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(codigoCotista))
            {
                return String.Format("Codigo de cotista: {0}, nao encontrado.", codigoCotista);
            }

            var cot = new CotistaQuery("cot");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            cot.Select(cot);
            cot.InnerJoin(perCotistaQuery).On(cot.IdCotista.Equal(perCotistaQuery.IdCotista));
            cot.Where(perCotistaQuery.IdCotista.Equal(codigoCotista));
            cot.Where(perCotistaQuery.IdUsuario.Equal(idUsuario));

            CotistaCollection cotistas = new CotistaCollection();
            if (!cotistas.Load(cot))
            {
                return String.Format("Usuario: {0} nao possui permissao de acesso ao cotista codigo: {1}", idUsuario, codigoCotista);
            }

            return null;
        }
        #endregion
    }
}