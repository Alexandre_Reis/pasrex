﻿using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// CotistaMovimentoConsistidoModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel{Financial.InvestidorCotista.OperacaoCotista, Financial.InterfacesCustom.GeracaoFuturo.ViewModel.CotistaMovimentoViewModel}" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaMovimentoConsistido", Namespace = "")]
    public class CotistaMovimentoConsistidoModel : BaseModel<OperacaoCotista, CotistaMovimentoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoConsistidoModel"/> class.
        /// </summary>
        public CotistaMovimentoConsistidoModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoConsistidoModel"/> class.
        /// </summary>
        /// <param name="dataMovimento">The data movimento.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        public CotistaMovimentoConsistidoModel(DateTime dataMovimento, int idUsuario)
        {
            var movimento = new OperacaoCotistaQuery("MOV");
            var cotista = new CotistaQuery("CO");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            movimento.Select(movimento);
            movimento.InnerJoin(perCotistaQuery).On(perCotistaQuery.IdCotista == movimento.IdCotista && perCotistaQuery.IdUsuario == idUsuario);
            movimento.Where(movimento.DataOperacao == dataMovimento);
            var movimentos = new OperacaoCotistaCollection();

            movimentos.Load(movimento);

            ViewModelList = new List<CotistaMovimentoViewModel>();
            foreach (var item in movimentos)
            {
                ViewModelList.Add(new CotistaMovimentoViewModel(item));
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the movimento consistido list.
        /// </summary>
        /// <value>
        /// The movimento consistido list.
        /// </value>
        [XmlArray(ElementName = "MovimentosConsistido")]
        [XmlArrayItem(ElementName = "MovimentoConsistido")]
        public List<CotistaMovimentoViewModel> MovimentoConsistidoList
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}