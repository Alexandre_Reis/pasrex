﻿using Financial.CRM;
using Financial.Fundo;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// PosicaoCotista
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "PosicaoCotista", Namespace = "")]
    public class PosicaoCotistaModel : BaseModel<PosicaoCotistaHistorico, CotistaPosicaoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PosicaoCotistaModel"/> class.
        /// </summary>
        public PosicaoCotistaModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PosicaoCotistaModel"/> class.
        /// </summary>
        /// <param name="dataPosicao">The data posicao.</param>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <exception cref="System.Exception"></exception>
        public PosicaoCotistaModel(DateTime dataPosicao, int codigoCotista, int idUsuario)
        {
            var msg = ValidarCotista(codigoCotista, idUsuario);
            if (!string.IsNullOrEmpty(msg))
                throw new InvalidShareholderException(msg);

            var posicao = new PosicaoCotistaHistoricoQuery("PH");
            var carteira = new CarteiraQuery("CT");
            var cotista = new CotistaQuery("CO");
            var historicoCota = new HistoricoCotaQuery("HC");
            var pessoa = new PessoaQuery("P");

            posicao.Select(posicao,
                                 carteira.TipoCota.As("TipoCota"),
                                 cotista.Nome.As("NomeCotista"),
                                 cotista.Apelido.As("NomeAbreviado"),
                                 historicoCota.CotaAbertura.As("CotaAbertura"),
                                 historicoCota.CotaFechamento.As("CotaFechamento"),
                                 pessoa.Tipo.As("TipoPessoa"));
            posicao.InnerJoin(historicoCota).On(historicoCota.Data == posicao.DataHistorico && historicoCota.IdCarteira == posicao.IdCarteira);
            posicao.InnerJoin(carteira).On(carteira.IdCarteira == posicao.IdCarteira);
            posicao.InnerJoin(cotista).On(cotista.IdCotista == posicao.IdCotista);
            posicao.InnerJoin(pessoa).On(pessoa.IdPessoa == cotista.IdPessoa);
            posicao.Where(posicao.DataHistorico == dataPosicao);
            posicao.Where(posicao.IdCotista == codigoCotista);

            var posicaoCotista = new PosicaoCotistaHistoricoCollection();
            posicaoCotista.Load(posicao);
            LoadViewModelList(posicaoCotista);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the posicoes cotista.
        /// </summary>
        /// <value>
        /// The posicoes cotista.
        /// </value>
        [XmlArray(ElementName = "Posicoes")]
        [XmlArrayItem(ElementName = "Posicao")]
        public List<CotistaPosicaoViewModel> PosicoesCotista
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }
    
}