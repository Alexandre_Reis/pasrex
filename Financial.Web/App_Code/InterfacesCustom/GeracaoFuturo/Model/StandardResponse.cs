﻿using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// Summary description for StandardResponse
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "StandardResponse", Namespace = "")]
    public class StandardResponse
    {
        #region Properties
        /// <summary>
        /// Gets or sets the codigo erro.
        /// </summary>
        /// <value>
        /// The codigo erro.
        /// </value>
        public int CodigoErro { get; set; }
        /// <summary>
        /// Gets or sets the MSG erro.
        /// </summary>
        /// <value>
        /// The MSG erro.
        /// </value>
        public string MsgErro { get; set; } 
        #endregion
    }
}