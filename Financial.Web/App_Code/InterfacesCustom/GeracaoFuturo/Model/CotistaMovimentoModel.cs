﻿using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// CotistaMovimentoModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaMovimento", Namespace = "")]
    public class CotistaMovimentoModel : BaseModel<OperacaoCotista, CotistaMovimentoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoModel"/> class.
        /// </summary>
        public CotistaMovimentoModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoModel"/> class.
        /// </summary>
        /// <param name="dataMovimento">The data movimento.</param>
        /// <param name="hora"></param>
        /// <param name="idUsuario">The identifier usuario.</param>
        public CotistaMovimentoModel(DateTime dataMovimento, string hora, int idUsuario)
        {
            if (string.IsNullOrEmpty(hora))
            {
                throw new Exception("horario nao informado.");
            }
            var movimento = new OperacaoCotistaQuery("MOV");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            var dataHora = dataMovimento.Add(TimeSpan.Parse(hora));

            movimento.Select(movimento);
            movimento.InnerJoin(perCotistaQuery).On(perCotistaQuery.IdCotista == movimento.IdCotista && perCotistaQuery.IdUsuario == idUsuario);
            movimento.Where(movimento.DataOperacao == dataMovimento);
            movimento.Where(movimento.UserTimeStamp > dataHora);
            var movimentos = new OperacaoCotistaCollection();

            movimentos.Load(movimento);
            LoadViewModelList(movimentos);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the resgate list.
        /// </summary>
        /// <value>
        /// The resgate list.
        /// </value>
        [XmlArray(ElementName = "Movimentos")]
        [XmlArrayItem(ElementName = "Movimento")]
        public List<CotistaMovimentoViewModel> MovimentoList
        {
            get
            {
                return ViewModelList;
            }
            set { ViewModelList = value; }
        }

        #endregion
    }

}