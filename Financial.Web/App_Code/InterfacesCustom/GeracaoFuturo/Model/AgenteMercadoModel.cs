﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// AgenteMercadoModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel{Financial.Common.AgenteMercado, Financial.InterfacesCustom.GeracaoFuturo.ViewModel.AgenteMercadoViewModel}" />
    [Serializable]
    [XmlRoot(ElementName = "AgenteMercado", Namespace = "")]
    public class AgenteMercadoModel : BaseModel<AgenteMercado, AgenteMercadoViewModel>
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="AgenteMercadoModel"/> class.
        /// </summary>
        public AgenteMercadoModel() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ClearingModel"/> class.
        /// </summary>
        public AgenteMercadoModel(TipoAgenteMercado? tipoAgente)
        {
            var agenteList = new AgenteMercadoCollection();
            if (tipoAgente == null)
            {
                agenteList.LoadAll();
            }else
            {
                var agenteMercado = new AgenteMercadoQuery("a");
                agenteMercado.Select(agenteMercado);
                switch (tipoAgente)
                {
                    case TipoAgenteMercado.Administrador:
                        agenteMercado.Where(agenteMercado.FuncaoAdministrador == "S");
                        break;
                    case TipoAgenteMercado.Corretora:
                        agenteMercado.Where(agenteMercado.FuncaoCorretora== "S");
                        break;
                    case TipoAgenteMercado.Gestor:
                        agenteMercado.Where(agenteMercado.FuncaoGestor== "S");
                        break;
                    case TipoAgenteMercado.Liquidante:
                        agenteMercado.Where(agenteMercado.FuncaoLiquidante== "S");
                        break;
                    case TipoAgenteMercado.Custodiante:
                        agenteMercado.Where(agenteMercado.FuncaoCustodiante== "S");
                        break;
                    case TipoAgenteMercado.Distribuidor:
                        agenteMercado.Where(agenteMercado.FuncaoDistribuidor== "S");
                        break;
                    case TipoAgenteMercado.Securitizada:
                        agenteMercado.Where(agenteMercado.EmpSecuritizada== "S");
                        break;
                    
                    default:
                        break;
                }

                agenteList.Load(agenteMercado);

            }
            LoadViewModelList(agenteList);
            
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the agente mercado list.
        /// </summary>
        /// <value>
        /// The agente mercado list.
        /// </value>
        [XmlArray(ElementName = "AgentesMercado")]
        [XmlArrayItem(ElementName = "AgenteMercado")]
        public List<AgenteMercadoViewModel> AgenteMercadoList
        {
            get
            {
                return ViewModelList;
            }
            set { }
        }

        #endregion
    }

}