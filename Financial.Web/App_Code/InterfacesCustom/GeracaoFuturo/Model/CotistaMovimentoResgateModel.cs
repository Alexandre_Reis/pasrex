﻿using Financial.Fundo;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// CotistaMovimentoResgateModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaMovimentoResgate", Namespace = "")]
    public class CotistaMovimentoResgateModel : BaseModel<DetalheResgateCotista, CotistaMovimentoResgateViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoResgateModel"/> class.
        /// </summary>
        public CotistaMovimentoResgateModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoResgateModel"/> class.
        /// </summary>
        /// <param name="dataMovimento">The data movimento.</param>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idfundo">The idfundo.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <exception cref="System.Exception"></exception>
        public CotistaMovimentoResgateModel(DateTime dataMovimento, int? idCotista, int? idfundo, int idUsuario)
        {
            if (idCotista != null)
            {
                var msg = ValidarCotista((int)idCotista, idUsuario);
                if (!string.IsNullOrEmpty(msg))
                    throw new InvalidShareholderException(msg);
            }
 
            var resgate = new DetalheResgateCotistaQuery("PH");
            var movimento = new OperacaoCotistaQuery("MOV");
            var carteira = new CarteiraQuery("CT");
            var cotista = new CotistaQuery("CO");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            resgate.Select(resgate,
                                 carteira.Nome.As("NomeFundo"),
                                 cotista.Nome.As("NomeCotista"),
                                 movimento.IdFormaLiquidacao.As("FormaLiquidacao"),
                                 movimento.IdConta.As("Conta"),
                                 movimento.DataConversao.As("DataCotizacao"),
                                 movimento.DataLiquidacao.As("DataLiquidacaoFinanceira"));
            resgate.InnerJoin(movimento).On(movimento.IdOperacao == resgate.IdOperacao);
            resgate.InnerJoin(carteira).On(carteira.IdCarteira == movimento.IdCarteira);
            resgate.InnerJoin(cotista).On(cotista.IdCotista == resgate.IdCotista);

            resgate.Where(movimento.DataOperacao == dataMovimento);

            if (idCotista == null)
            {
                resgate.InnerJoin(perCotistaQuery).On(perCotistaQuery.IdCotista == resgate.IdCotista && perCotistaQuery.IdUsuario == idUsuario);
            }
            else
            {
                resgate.Where(resgate.IdCotista == idCotista);
            }

            if (idfundo != null)
            {
                resgate.Where(resgate.IdCarteira == idfundo);
            }

            var resgates = new DetalheResgateCotistaCollection();

            resgates.Load(resgate);
            LoadViewModelList(resgates);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoResgateModel"/> class.
        /// </summary>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idfundo">The idfundo.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <param name="dtLiquidacao">The dt liquidacao.</param>
        /// <exception cref="System.Exception"></exception>
        public CotistaMovimentoResgateModel(int? idCotista, int? idfundo, int idUsuario, DateTime dtLiquidacao)
        {
            if (idCotista != null)
            {
                var msg = ValidarCotista((int)idCotista, idUsuario);
                if (!string.IsNullOrEmpty(msg))
                    throw new InvalidShareholderException(msg);
            }

            var resgate = new DetalheResgateCotistaQuery("PH");
            var movimento = new OperacaoCotistaQuery("MOV");
            var carteira = new CarteiraQuery("CT");
            var cotista = new CotistaQuery("CO");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            resgate.Select(resgate,
                                 carteira.Nome.As("NomeFundo"),
                                 cotista.Nome.As("NomeCotista"),
                                 movimento.IdFormaLiquidacao.As("FormaLiquidacao"),
                                 movimento.IdConta.As("Conta"),
                                 movimento.DataConversao.As("DataCotizacao"),
                                 movimento.DataLiquidacao.As("DataLiquidacaoFinanceira"));
            resgate.InnerJoin(movimento).On(movimento.IdOperacao == resgate.IdOperacao);
            resgate.InnerJoin(carteira).On(carteira.IdCarteira == movimento.IdCarteira);
            resgate.InnerJoin(cotista).On(cotista.IdCotista == resgate.IdCotista);

            resgate.Where(movimento.DataLiquidacao == dtLiquidacao);

            if (idCotista == null)
            {
                resgate.InnerJoin(perCotistaQuery).On(perCotistaQuery.IdCotista == resgate.IdCotista && perCotistaQuery.IdUsuario == idUsuario);
            }
            else
            {
                resgate.Where(resgate.IdCotista == idCotista);
            }

            if (idfundo != null)
            {
                resgate.Where(resgate.IdCarteira == idfundo);
            }

            var resgates = new DetalheResgateCotistaCollection();

            resgates.Load(resgate);
            LoadViewModelList(resgates);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the resgate list.
        /// </summary>
        /// <value>
        /// The resgate list.
        /// </value>
        [XmlArray(ElementName = "Resgates")]
        [XmlArrayItem(ElementName = "Resgate")]
        public List<CotistaMovimentoResgateViewModel> ResgateList
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}