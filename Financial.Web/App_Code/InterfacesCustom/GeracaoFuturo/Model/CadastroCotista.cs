﻿using Financial.CRM;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InterfacesCustom.GeracaoFuturo.Xml;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// Summary description for CadastroCotista
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "CadastroCotista", Namespace = "")]
    public class CadastroCotista
    {
        #region Fields
        private InformacoesBasicasViewModel _informacoesBasicas;
        private InformacoesAdicionaisViewModel _informacoesAdicionais;
        private CotistaViewModel _cotista;
        private SuitabilityViewModel _suitability;
        private List<EnderecoViewModel> _enderecos;
        private List<TelefoneViewModel> _telefones;
        private List<EmailViewModel> _emails;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CadastroCotista"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public CadastroCotista()
        {
            Response = new StandardResponse { CodigoErro = 0 };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CadastroCotista"/> class.
        /// </summary>
        /// <param name="cotista">The cotista.</param>
        public CadastroCotista(Cotista cotista)
        {
            Load(cotista);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CadastroCotista"/> class.
        /// </summary>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <exception cref="System.Exception">
        /// </exception>
        public CadastroCotista(int codigoCotista, int idUsuario)
        {
            var msg = ValidarCotista(codigoCotista, idUsuario);
            if (!string.IsNullOrEmpty(msg))
                throw new InvalidShareholderException(msg);

            var cotista = new Cotista();
            cotista.LoadByPrimaryKey(codigoCotista);
            Load(cotista);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public StandardResponse Response { get; set; }

        /// <summary>
        /// Gets or sets the informacoes basicas.
        /// </summary>
        /// <value>
        /// The informacoes basicas.
        /// </value>
        [XmlElement(ElementName = "InformacoesBasicas")]
        public InformacoesBasicasViewModel InformacoesBasicas
        {
            get
            {
                return _informacoesBasicas;
            }
            set { _informacoesBasicas = value; }
        }

        /// <summary>
        /// Gets or sets the informacoes adicionais.
        /// </summary>
        /// <value>
        /// The informacoes adicionais.
        /// </value>
        [XmlElement(ElementName = "InformacoesAdicionais")]
        public InformacoesAdicionaisViewModel InformacoesAdicionais
        {
            get
            {
                return _informacoesAdicionais;
            }
            set { _informacoesAdicionais = value; }
        }
        /// <summary>
        /// Gets or sets the cotista.
        /// </summary>
        /// <value>
        /// The cotista.
        /// </value>
        [XmlElement(ElementName = "Cotista")]
        public CotistaViewModel Cotista
        {
            get
            {
                return _cotista;
            }
            set { _cotista = value; }
        }
        /// <summary>
        /// Gets or sets the suitability.
        /// </summary>
        /// <value>
        /// The suitability.
        /// </value>
        [XmlElement(ElementName = "Suitability")]
        public SuitabilityViewModel Suitability
        {
            get
            {
                return _suitability;
            }
            set { _suitability = value; }
        }
        /// <summary>
        /// Gets or sets the enderecos.
        /// </summary>
        /// <value>
        /// The enderecos.
        /// </value>
        [XmlArray(ElementName = "Enderecos")]
        [XmlArrayItem(ElementName = "Endereco")]
        public List<EnderecoViewModel> Enderecos
        {
            get
            {
                return _enderecos;
            }
            set { _enderecos = value; }
        }
        /// <summary>
        /// Gets or sets the telefones.
        /// </summary>
        /// <value>
        /// The telefones.
        /// </value>
        [XmlArray(ElementName = "Telefones")]
        [XmlArrayItem(ElementName = "Telefone")]
        public List<TelefoneViewModel> Telefones
        {
            get
            {
                return _telefones;
            }
            set { _telefones = value; }
        }
        /// <summary>
        /// Gets or sets the emails.
        /// </summary>
        /// <value>
        /// The emails.
        /// </value>
        [XmlArray(ElementName = "Emails")]
        [XmlArrayItem(ElementName = "Email")]
        public List<EmailViewModel> Emails
        {
            get
            {
                return _emails;
            }
            set { _emails = value; }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Loads the specified cotista.
        /// </summary>
        /// <param name="cotista">The cotista.</param>
        private void Load(Cotista cotista)
        {
            _enderecos = new List<EnderecoViewModel>();
            _telefones = new List<TelefoneViewModel>();
            _emails = new List<EmailViewModel>();

            // cotista
            _cotista = new CotistaViewModel(cotista);

            if (cotista.IdPessoa.HasValue)
            {
                // informacoes basicas/adicionais
                var pessoa = new Pessoa();
                if (pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value))
                {
                    _informacoesBasicas = new InformacoesBasicasViewModel(pessoa);
                    _informacoesAdicionais = new InformacoesAdicionaisViewModel(pessoa);
                }

                // suitability
                var pessoaSuitability = new PessoaSuitability();
                if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
                {
                    _suitability = new SuitabilityViewModel(pessoaSuitability);
                }

                // enderecos
                var pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa == cotista.IdPessoa.Value);
                pessoaEnderecoCollection.Query.Load();

                foreach (var pessoaEndereco in pessoaEnderecoCollection)
                {
                    _enderecos.Add(new EnderecoViewModel(pessoaEndereco));
                }

                //Telefones
                var pessoaTelefoneCollection = new PessoaTelefoneCollection();
                pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa == cotista.IdPessoa.Value);
                pessoaTelefoneCollection.Query.Load();

                foreach (var pessoaTelefone in pessoaTelefoneCollection)
                {
                    _telefones.Add(new TelefoneViewModel(pessoaTelefone));
                }
                //email
                var pessoaEmailCollection = new PessoaEmailCollection();
                pessoaEmailCollection.Query.Where(pessoaEmailCollection.Query.IdPessoa == cotista.IdPessoa.Value);
                pessoaEmailCollection.Query.Load();

                foreach (var pessoaEmail in pessoaEmailCollection)
                {
                    _emails.Add(new EmailViewModel(pessoaEmail));
                }
            }
        }

        #endregion

        #region private methods
        /// <summary>
        /// Validars the cotista.
        /// </summary>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        protected string ValidarCotista(int codigoCotista, int idUsuario)
        {
            var cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(codigoCotista))
            {
                return String.Format("Codigo de cotista: {0}, nao encontrado.", codigoCotista);
            }

            var cot = new CotistaQuery("cot");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            cot.Select(cot);
            cot.InnerJoin(perCotistaQuery).On(cot.IdCotista.Equal(perCotistaQuery.IdCotista));
            cot.Where(perCotistaQuery.IdCotista.Equal(codigoCotista));
            cot.Where(perCotistaQuery.IdUsuario.Equal(idUsuario));

            CotistaCollection cotistas = new CotistaCollection();
            if (!cotistas.Load(cot))
            {
                return String.Format("Usuario: {0} nao possui permissao de acesso ao cotista codigo: {1}", idUsuario, codigoCotista);
            }

            return null;
        }
        #endregion
    }

    /// <summary>
    /// CadastroCotista collection
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Xml.XmlSerializableList{Financial.InterfacesCustom.GeracaoFuturo.Model.CadastroCotista}" />
    [XmlRoot(ElementName = "CadastrosCotista", Namespace = "")]
    public class CadastroCotistaList : XmlSerializableList<CadastroCotista>
    {
        public CadastroCotistaList()
        {
            Response = new StandardResponse { CodigoErro = 0 };
        }

        public StandardResponse Response { get; set; }
    }

}