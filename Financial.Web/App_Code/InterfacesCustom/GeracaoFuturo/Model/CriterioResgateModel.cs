﻿using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.InvestidorCotista.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// TipoMovimentoCotistaModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "CriterioResgate", Namespace = "")]
    public class CriterioResgateModel
    {
        #region Fields
        private List<CriterioResgateViewModel> _tipoList;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoMovimentoCotistaModel"/> class.
        /// </summary>
        public CriterioResgateModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoMovimentoCotistaModel"/> class.
        /// </summary>
        public CriterioResgateModel(string tipo)
        {
            _tipoList = new List<CriterioResgateViewModel>();
            var enumList = Enum.GetValues(typeof(Financial.InvestidorCotista.Enums.TipoResgateCotista));
            foreach (var e in enumList.Cast<TipoResgateCotista>())
            {
                _tipoList.Add(new CriterioResgateViewModel
                {
                    Codigo = (int)e,
                    Descricao = CotEnumUtil.GetEnumDescription(e)
                });
            }

        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public StandardResponse Response { get; set; }

        /// <summary>
        /// Gets or sets the tipo cotista.
        /// </summary>
        /// <value>
        /// The tipo cotista.
        /// </value>
        [XmlArray(ElementName = "CriteriosResgate")]
        [XmlArrayItem(ElementName = "Criterio")]
        public List<CriterioResgateViewModel> CriterioResgate
        {
            get
            {
                return _tipoList;
            }
            set { _tipoList = value; }
        }

        #endregion
    }

}