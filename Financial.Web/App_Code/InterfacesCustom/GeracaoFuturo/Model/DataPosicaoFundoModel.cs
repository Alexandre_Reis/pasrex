﻿using Financial.Fundo;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.Investidor;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "DataPosicaoFundo", Namespace = "")]
    public class DataPosicaoFundoModel : BaseModel<Cliente, DataPosicaoFundoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoModel"/> class.
        /// </summary>
        public DataPosicaoFundoModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoModel"/> class.
        /// </summary>
        /// <param name="idFundo">The identifier fundo.</param>
        public DataPosicaoFundoModel(int? idFundo)
        {
            if(idFundo != null)
            {
                var consisteFundo = new Carteira();
                if (!consisteFundo.LoadByPrimaryKey((int)idFundo))
                {
                    throw new InvalidFundException(String.Format("Codigo do fundo: {0}, nao encontrado.", idFundo));
                }
            }

            var cliente = new ClienteQuery("cli");
            cliente.Select(cliente);
            cliente.Where(cliente.IdTipo.In(200, 500, 510));

            if (idFundo != null)
            {
                cliente.Where(cliente.IdCliente == idFundo);
            }
            var clienteList = new ClienteCollection();
            clienteList.Load(cliente);
            LoadViewModelList(clienteList);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the data posicao fundo.
        /// </summary>
        /// <value>
        /// The data posicao fundo.
        /// </value>
        [XmlArray(ElementName = "DatasPosicaoFundo")]
        [XmlArrayItem(ElementName = "DataPosicaoFundo")]
        public List<DataPosicaoFundoViewModel> DataPosicaoFundo
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}