﻿using Financial.CRM;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    [Serializable]
    [XmlRoot(ElementName = "ContaCorrente", Namespace = "")]
    public class ContaCorrenteModel : BaseModel<Financial.Investidor.ContaCorrente, ContaCorrenteViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContaCorrenteModel"/> class.
        /// </summary>
        public ContaCorrenteModel()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ContaCorrenteModel"/> class.
        /// </summary>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        public ContaCorrenteModel(int? idCotista, int idUsuario)
        {
            if (idCotista != null)
            {
                var msg = ValidarCotista((int)idCotista, idUsuario);
                if (!string.IsNullOrEmpty(msg))
                    throw new InvalidShareholderException(msg);
            }

            var perCotistaQuery = new PermissaoCotistaQuery("permissao");
            var conta = new ContaCorrenteQuery("C");
            var pessoa = new PessoaQuery("P");
            var cotista = new CotistaQuery("cot");

            conta.Select(conta,
                         cotista.IdCotista.As("CotistaId"));
            conta.InnerJoin(pessoa).On(pessoa.IdPessoa == conta.IdPessoa);
            conta.InnerJoin(cotista).On(cotista.IdCotista == pessoa.IdPessoa);

            if (idCotista == null)
            {
                conta.InnerJoin(perCotistaQuery).On(perCotistaQuery.IdCotista == cotista.IdCotista);
                conta.Where(perCotistaQuery.IdUsuario == idUsuario);
            }
            else
            {
                var msg = ValidarCotista((int)idCotista, idUsuario);
                if (!string.IsNullOrEmpty(msg))
                    throw new Exception(msg);


                conta.Where(cotista.IdCotista == idCotista);
            }

            var contaList = new ContaCorrenteCollection();
            contaList.Load(conta);
            LoadViewModelList(contaList);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the clearing list.
        /// </summary>
        /// <value>
        /// The clearing list.
        /// </value>
        [XmlArray(ElementName = "Contas")]
        [XmlArrayItem(ElementName = "Conta")]
        public List<ContaCorrenteViewModel> ClearingList
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}