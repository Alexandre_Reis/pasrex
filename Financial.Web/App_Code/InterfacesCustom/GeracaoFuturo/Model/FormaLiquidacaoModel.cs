﻿using Financial.ContaCorrente;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// FormaLiquidacaoModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "FormaLiquidacao", Namespace = "")]
    public class FormaLiquidacaoModel : BaseModel<FormaLiquidacao, FormaLiquidacaoViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FormaLiquidacaoModel"/> class.
        /// </summary>
        public FormaLiquidacaoModel()
        {
            var formaLiq = new FormaLiquidacaoCollection();

            formaLiq.LoadAll();
            LoadViewModelList(formaLiq);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the forma liquidacao list.
        /// </summary>
        /// <value>
        /// The forma liquidacao list.
        /// </value>
        [XmlArray(ElementName = "FormasLiquidacao")]
        [XmlArrayItem(ElementName = "FormaLiquidacao")]
        public List<FormaLiquidacaoViewModel> FormaLiquidacaoList
        {
            get
            {
                return ViewModelList;
            }
            set {  }
        }

        #endregion
    }

}