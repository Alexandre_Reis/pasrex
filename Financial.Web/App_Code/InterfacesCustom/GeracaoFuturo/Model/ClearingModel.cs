﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.ViewModel;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// ClearingModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Model.BaseModel" />
    [Serializable]
    [XmlRoot(ElementName = "Clearing", Namespace = "")]
    public class ClearingModel : BaseModel<Clearing, ClearingViewModel>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearingModel"/> class.
        /// </summary>
        public ClearingModel()
        {
            var clearingList = new ClearingCollection();
            clearingList.LoadAll();
            LoadViewModelList(clearingList);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the clearing list.
        /// </summary>
        /// <value>
        /// The clearing list.
        /// </value>
        [XmlArray(ElementName = "Clearings")]
        [XmlArrayItem(ElementName = "Clearing")]
        public List<ClearingViewModel> ClearingList
        {
            get
            {
                return ViewModelList;
            }
            set { ViewModelList = value; }
        }
        #endregion
    }

}