﻿using System;
using System.Diagnostics;
using System.Text;
using System.Web;

namespace Financial.InterfacesCustom.GeracaoFuturo
{
    /// <summary>
    /// Summary description for WsLogger
    /// </summary>
    public class WsLogger
    {
        #region Debug
        /// <summary>
        /// Debugs the specified mensagem.
        /// </summary>
        /// <param name="mensagem">The mensagem.</param>
        public void Debug(string mensagem)
        {
            return;
            Log(mensagem, EventLogEntryType.Information, WsPassivoConstantes.WsPassivo);
        }
        #endregion

        #region error
        /// <summary>
        /// Errors the specified mensagem.
        /// </summary>
        /// <param name="mensagem">The mensagem.</param>
        /// <param name="ex">The ex.</param>
        public void Error(string mensagem, Exception ex)
        {
            return;
            Log(string.Format("{0}\n{1}\n{2}", mensagem, ex.Message, ex.StackTrace), EventLogEntryType.Error, WsPassivoConstantes.WsPassivo);
        }

        /// <summary>
        /// Errors the specified mensagem.
        /// </summary>
        /// <param name="mensagem">The mensagem.</param>
        /// <param name="metodo">The metodo.</param>
        public void Error(string mensagem, string metodo)
        {
            return;
            Log(mensagem, EventLogEntryType.Error, metodo);
        }

        #endregion

        #region Log
        /// <summary>
        /// Logs the specified mensagem.
        /// </summary>
        /// <param name="mensagem">The mensagem.</param>
        /// <param name="metodo">The metodo.</param>
        public void Log(string mensagem, string metodo)
        {
            return;
            Log(mensagem, EventLogEntryType.Information, metodo);
        }

        /// <summary>
        /// Logs the specified mensagem.
        /// </summary>
        /// <param name="mensagem">The mensagem.</param>
        /// <param name="objTipo">The object tipo.</param>
        /// <param name="logName">Name of the log.</param>
        private static void Log(string mensagem, EventLogEntryType objTipo, string logName)
        {
            return;
            EventLog objLog = new EventLog();

            try
            {
                objLog.Source = logName;

                if (!EventLog.SourceExists(logName))
                    EventLog.CreateEventSource(logName, logName);

                objLog.WriteEntry(mensagem, objTipo, 0, 0);
            }
            catch (Exception ex)
            {
                objLog.Source = WsPassivoConstantes.WsPassivo;
                objLog.WriteEntry(ex.Message + ex.StackTrace, EventLogEntryType.Error, 0, 0);
            }
        }
        #endregion

        #region EndLog
        /// <summary>
        /// Ends the log.
        /// </summary>
        /// <param name="metodo">The metodo.</param>
        public void EndLog(string metodo)
        {
            return;
            StringBuilder strLog = new StringBuilder();

            try
            {
                strLog.AppendFormat("Metodo:{0}", metodo);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("IP:{0}", HttpContext.Current.Request.UserHostAddress);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("Cliente:{0}", HttpContext.Current.Request.UserHostName);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("Data/Hora:{0}", DateTime.Now.ToString("yyyyMMdd hh:mm:ss"));

                Log(strLog.ToString(), metodo);
            }
            catch (Exception ex)
            {
                Error("gravaLog", ex);
            }
        }
        #endregion

        #region BeginLog
        /// <summary>
        /// Logs the specified log name.
        /// </summary>
        /// <param name="logName">Name of the log.</param>
        /// <param name="metodo">The metodo.</param>
        /// <param name="parm">The parm.</param>
        public void BeginLog(string logName, System.Reflection.MethodBase metodo, params object[] parm)
        {
            return;
            StringBuilder strLog = new StringBuilder();

            try
            {
                System.Reflection.ParameterInfo[] parametros = metodo.GetParameters();
                strLog.AppendFormat("Metodo:{0}", metodo.Name);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("IP:{0}", HttpContext.Current.Request.UserHostAddress);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("Cliente:{0}", HttpContext.Current.Request.UserHostName);
                strLog.Append(Environment.NewLine);
                strLog.AppendFormat("Data/Hora:{0}", DateTime.Now.ToString("yyyyMMdd hh:mm:ss"));
                strLog.Append(Environment.NewLine);
                strLog.Append("Parametros:");
                strLog.Append(Environment.NewLine);

                if (parametros.Length != parm.Length)
                {
                    strLog.Append("parametro invalido");
                    Error(strLog.ToString(), metodo.Name);
                    return;
                }

                for (int i = 0; i < parametros.Length; i++)
                {
                    if (parm[i] == null)
                        strLog.AppendFormat("{0} : NULL", parametros[i].Name, parm[i]);
                    else
                        strLog.AppendFormat("{0} : {1}", parametros[i].Name, parm[i]);

                    strLog.AppendLine();
                }

                Log(strLog.ToString(), logName);
            }
            catch (Exception ex)
            {
                Error("WsLogger.log", ex);
            }
        }
        #endregion
    }
}