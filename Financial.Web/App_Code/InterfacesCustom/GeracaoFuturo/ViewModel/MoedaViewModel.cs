﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// MoedaViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Common.Moeda}" />
    [Serializable]
    [XmlRoot(ElementName = "Moeda", Namespace = "")]
    public class MoedaViewModel : EntityViewModel<Moeda>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MoedaViewModel"/> class.
        /// </summary>
        public MoedaViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MoedaViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public MoedaViewModel(Moeda modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier moeda.
        /// </summary>
        /// <value>
        /// The identifier moeda.
        /// </value>
        public short? IdMoeda
        {
            get
            {
                return ModelEntity.IdMoeda;
            }
            set
            {
                ModelEntity.IdMoeda = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo iso.
        /// </summary>
        /// <value>
        /// The codigo iso.
        /// </value>
        public string CodigoISO
        {
            get
            {
                return ModelEntity.CodigoISO;
            }
            set
            {
                ModelEntity.CodigoISO = value;
            }
        }
        #endregion
    }
}
