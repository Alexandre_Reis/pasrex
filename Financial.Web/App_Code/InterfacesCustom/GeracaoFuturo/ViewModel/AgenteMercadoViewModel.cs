﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// AgenteMercadoModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Common.AgenteMercado}" />
    [Serializable]
    [XmlRoot(ElementName = "AgenteMercado", Namespace = "")]
    public class AgenteMercadoViewModel : EntityViewModel<AgenteMercado>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AgenteMercadoViewModel"/> class.
        /// </summary>
        public AgenteMercadoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AgenteMercadoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public AgenteMercadoViewModel(AgenteMercado modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier agente.
        /// </summary>
        /// <value>
        /// The identifier agente.
        /// </value>
        public int? IdAgente
        {
            get
            {
                return ModelEntity.IdAgente;
            }
            set
            {
                ModelEntity.IdAgente = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        /// <summary>
        /// </summary>
        /// <value>
        /// The CNPJ.
        /// </value>
        public string Cnpj
        {
            get
            {
                return ModelEntity.Cnpj;
            }
            set
            {
                ModelEntity.Cnpj = value;
            }
        }
        /// <summary>
        /// Gets or sets the funcao corretora.
        /// </summary>
        /// <value>
        /// The funcao corretora.
        /// </value>
        public FuncaoCorretora? FuncaoCorretora
        {
            get
            {
                return ModelEntity.FuncaoCorretora == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoCorretora, typeof(FuncaoCorretora), true) as FuncaoCorretora?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoCorretora = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the funcao administrador.
        /// </summary>
        /// <value>
        /// The funcao administrador.
        /// </value>
        public FuncaoAdministrador? FuncaoAdministrador
        {
            get
            {
                return ModelEntity.FuncaoAdministrador == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoAdministrador, typeof(FuncaoAdministrador), true) as FuncaoAdministrador?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoAdministrador = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the funcao gestor.
        /// </summary>
        /// <value>
        /// The funcao gestor.
        /// </value>
        public FuncaoGestor? FuncaoGestor
        {
            get
            {
                return ModelEntity.FuncaoGestor == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoGestor, typeof(FuncaoGestor), true) as FuncaoGestor?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoGestor = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the funcao liquidante.
        /// </summary>
        /// <value>
        /// The funcao liquidante.
        /// </value>
        public FuncaoLiquidante? FuncaoLiquidante
        {
            get
            {
                return ModelEntity.FuncaoLiquidante == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoLiquidante, typeof(FuncaoLiquidante), true) as FuncaoLiquidante?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoLiquidante = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the funcao custodiante.
        /// </summary>
        /// <value>
        /// The funcao custodiante.
        /// </value>
        public FuncaoCustodiante? FuncaoCustodiante
        {
            get
            {
                return ModelEntity.FuncaoCustodiante == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoCustodiante, typeof(FuncaoCustodiante), true) as FuncaoCustodiante?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoCustodiante = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the funcao distribuidor.
        /// </summary>
        /// <value>
        /// The funcao distribuidor.
        /// </value>
        public FuncaoDistribuidor? FuncaoDistribuidor
        {
            get
            {
                return ModelEntity.FuncaoDistribuidor == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.FuncaoDistribuidor, typeof(FuncaoDistribuidor), true) as FuncaoDistribuidor?;
            }
            set
            {
                if (value != null)
                    ModelEntity.FuncaoDistribuidor = Enums.Util.GetEnumDescription(value);
            }
        }
        #endregion
    }
}
