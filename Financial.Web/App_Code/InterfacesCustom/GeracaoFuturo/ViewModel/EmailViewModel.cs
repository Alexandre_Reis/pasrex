﻿using Financial.CRM;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for Telefone
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Email", Namespace = "")]
    public class EmailViewModel : EntityViewModel<PessoaEmail>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public EmailViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public EmailViewModel(PessoaEmail modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier email.
        /// </summary>
        /// <value>
        /// The identifier email.
        /// </value>
        public int? IdEmail
        {
            get
            {
                return ModelEntity.IdEmail;
            }
            set
            {
                ModelEntity.IdEmail = value;
            }
        }
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email
        {
            get
            {
                return ModelEntity.Email;
            }
            set
            {
                ModelEntity.Email = value;
            }
        }
       
        #endregion
    }
}
