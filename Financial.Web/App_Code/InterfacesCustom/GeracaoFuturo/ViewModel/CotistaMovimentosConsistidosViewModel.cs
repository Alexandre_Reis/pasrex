﻿using Financial.ContaCorrente;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// CotistaMovimentosConsistidosViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Base.EntityViewModel{Financial.InvestidorCotista.OperacaoCotista}" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaMovimentosConsistidos", Namespace = "")]
    public class CotistaMovimentosConsistidosViewModel : EntityViewModel<OperacaoCotista>
    {
        #region fields
        private FormaLiquidacaoViewModel _formaLiquidacao;
        private ContaCorrenteViewModel _conta;
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentosConsistidosViewModel"/> class.
        /// </summary>
        public CotistaMovimentosConsistidosViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentosConsistidosViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public CotistaMovimentosConsistidosViewModel(OperacaoCotista modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null && modelEntity.GetColumn("FormaLiquidacao") != null)
            {
                var liq = new FormaLiquidacao();
                if (liq.LoadByPrimaryKey((byte)modelEntity.GetColumn("FormaLiquidacao")))
                {
                    _formaLiquidacao = new FormaLiquidacaoViewModel(liq);
                }
            }

            if (modelEntity != null && modelEntity.GetColumn("Conta") != null && !(modelEntity.GetColumn("Conta") is System.DBNull))
            {
                var conta = new Investidor.ContaCorrente();
                if (conta.LoadByPrimaryKey((byte)modelEntity.GetColumn("Conta")))
                {
                    _conta = new ContaCorrenteViewModel(conta);
                }
            }

        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the data movimento.
        /// </summary>
        /// <value>
        /// The data movimento.
        /// </value>
        public DateTime? DataMovimento
        {
            get
            {
                return ModelEntity.DataOperacao;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the codigo fundo.
        /// </summary>
        /// <value>
        /// The codigo fundo.
        /// </value>
        public int? CodigoFundo
        {
            get
            {
                return ModelEntity.IdCarteira;
            }
            set
            {
                ModelEntity.IdCarteira = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo cotista.
        /// </summary>
        /// <value>
        /// The codigo cotista.
        /// </value>
        public int? CodigoCotista
        {
            get
            {
                return ModelEntity.IdCotista;
            }
            set
            {
                ModelEntity.IdCotista  = value;
            }
        }
        /// <summary>
        /// Gets or sets the data registro.
        /// </summary>
        /// <value>
        /// The data registro.
        /// </value>
        public DateTime? DataRegistro
        {
            get
            {
                return ModelEntity.DataRegistro;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the tipo movimento.
        /// </summary>
        /// <value>
        /// The tipo movimento.
        /// </value>
        public TipoOperacaoCotista? TipoMovimento
        {
            get
            {
                return (TipoOperacaoCotista?)ModelEntity.TipoOperacao;
            }
            set
            {
                
            }
        }

        /// <summary>
        /// Gets or sets the quantidade cotas.
        /// </summary>
        /// <value>
        /// The quantidade cotas.
        /// </value>
        public decimal? QuantidadeCotas
        {
            get
            {
                return ModelEntity.Quantidade;
            }
            set
            {
                ModelEntity.Quantidade = value;
            }
        }

        /// <summary>
        /// Gets or sets the valor bruto.
        /// </summary>
        /// <value>
        /// The valor bruto.
        /// </value>
        public decimal? ValorBruto
        {
            get
            {
                return ModelEntity.ValorBruto;
            }
            set
            {
                ModelEntity.ValorBruto = value;
            }
        }

        /// <summary>
        /// Gets or sets the numero nota.
        /// </summary>
        /// <value>
        /// The numero nota.
        /// </value>
        public int? NumeroNota
        {
            get
            {
                return ModelEntity.IdOperacao;
            }
            set
            {
                
            }
        }
        #endregion
    }
}
