﻿using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// TipoCotistaViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "TipoMovimentoCotista", Namespace = "")]
    public class TipoMovimentoCotistaViewModel
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoMovimentoCotistaViewModel"/> class.
        /// </summary>
        public TipoMovimentoCotistaViewModel()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public int Codigo
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get; set;
        }
        #endregion
    }
}