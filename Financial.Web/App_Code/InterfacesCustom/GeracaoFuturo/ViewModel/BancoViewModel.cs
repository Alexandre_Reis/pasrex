﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{

    /// <summary>
    /// BancoViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Investidor.Banco}" />
    [Serializable]
    [XmlRoot(ElementName = "Banco", Namespace = "")]
    public class BancoViewModel : EntityViewModel<Banco>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BancoViewModel"/> class.
        /// </summary>
        public BancoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BancoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public BancoViewModel(Banco modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier banco.
        /// </summary>
        /// <value>
        /// The identifier banco.
        /// </value>
        public int? IdBanco
        {
            get
            {
                return ModelEntity.IdBanco;
            }
            set
            {
                ModelEntity.IdBanco = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public string Codigo
        {
            get
            {
                return ModelEntity.CodigoCompensacao;
            }
            set
            {
                ModelEntity.CodigoCompensacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        #endregion
    }
}
