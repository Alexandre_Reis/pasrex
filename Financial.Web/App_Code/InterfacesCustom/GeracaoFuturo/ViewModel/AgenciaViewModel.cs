﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Investidor.Agencia}" />
    [Serializable]
    [XmlRoot(ElementName = "Agencia", Namespace = "")]
    public class AgenciaViewModel : EntityViewModel<Agencia>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AgenciaViewModel"/> class.
        /// </summary>
        public AgenciaViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AgenciaViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public AgenciaViewModel(Agencia modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier agencia.
        /// </summary>
        /// <value>
        /// The identifier agencia.
        /// </value>
        public int? IdAgencia
        {
            get
            {
                return ModelEntity.IdAgencia;
            }
            set
            {
                ModelEntity.IdAgencia = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier banco.
        /// </summary>
        /// <value>
        /// The identifier banco.
        /// </value>
        public int? IdBanco
        {
            get
            {
                return ModelEntity.IdBanco;
            }
            set
            {
                ModelEntity.IdBanco = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public string Codigo
        {
            get
            {
                return ModelEntity.Codigo;
            }
            set
            {
                ModelEntity.Codigo = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier local.
        /// </summary>
        /// <value>
        /// The identifier local.
        /// </value>
        public short? IdLocal
        {
            get
            {
                return ModelEntity.IdLocal;
            }
            set
            {
                ModelEntity.IdLocal = value;
            }
        }
        #endregion
    }
}
