﻿using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// CriterioResgateViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "CriterioResgate", Namespace = "")]
    public class CriterioResgateViewModel
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CriterioResgateViewModel"/> class.
        /// </summary>
        public CriterioResgateViewModel()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public int Codigo
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get; set;
        }
        #endregion
    }
}