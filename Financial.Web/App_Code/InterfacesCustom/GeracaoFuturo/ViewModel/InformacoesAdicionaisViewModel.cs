﻿using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for InformacoesAdicionais
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "InformacoesAdicionais", Namespace = "")]
    public class InformacoesAdicionaisViewModel : EntityViewModel<Pessoa>
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="InformacoesAdicionaisViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public InformacoesAdicionaisViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InformacoesAdicionaisViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public InformacoesAdicionaisViewModel(Pessoa modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the sexo.
        /// </summary>
        /// <value>
        /// The sexo.
        /// </value>
        public Sexo? Sexo
        {
            get
            {
                return ModelEntity.Sexo == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.Sexo, typeof(Sexo), true) as Sexo?;
            }
            set
            {
                if (value != null)
                    ModelEntity.Sexo = Enums.Util.GetEnumDescription(value);
            }
        }

        /// <summary>
        /// Gets or sets the estado civil pessoa.
        /// </summary>
        /// <value>
        /// The estado civil pessoa.
        /// </value>
        public EstadoCivilPessoa? EstadoCivilPessoa
        {
            get
            {
                return (EstadoCivilPessoa?)ModelEntity.EstadoCivil ;
            }
            set
            {
                if (value != null)
                    ModelEntity.EstadoCivil = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets the data nascimento.
        /// </summary>
        /// <value>
        /// The data nascimento.
        /// </value>
        public DateTime? DataNascimento
        {
            get
            {
                return ModelEntity.DataNascimento;
            }
            set
            {
                ModelEntity.DataNascimento = value;
            }
        }

        /// <summary>
        /// Gets or sets the profissao.
        /// </summary>
        /// <value>
        /// The profissao.
        /// </value>
        public string Profissao
        {
            get
            {
                return ModelEntity.Profissao;
            }
            set
            {
                ModelEntity.Profissao = value;
            }
        }
        /// <summary>
        /// Gets or sets the pais nacionalidade.
        /// </summary>
        /// <value>
        /// The pais nacionalidade.
        /// </value>
        public Pais? PaisNacionalidade
        {
            get
            {
                return  (Pais?)ModelEntity.Nacionalidade;
            }
            set
            {
                ModelEntity.Nacionalidade = (byte)value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo interface.
        /// </summary>
        /// <value>
        /// The codigo interface.
        /// </value>
        public string CodigoInterface
        {
            get
            {
                return ModelEntity.CodigoInterface;
            }
            set
            {
                ModelEntity.CodigoInterface = value;
            }
        }
        /// <summary>
        /// Gets or sets the politicamente exposta.
        /// </summary>
        /// <value>
        /// The politicamente exposta.
        /// </value>
        public PoliticamenteExposta? PoliticamenteExposta
        {
            get
            {
                return ModelEntity.PessoaPoliticamenteExposta == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.PessoaPoliticamenteExposta, typeof(PoliticamenteExposta), true) as PoliticamenteExposta?;
            }
            set
            {
                if(value!=null)
                    ModelEntity.PessoaPoliticamenteExposta = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the pessoa vinculada.
        /// </summary>
        /// <value>
        /// The pessoa vinculada.
        /// </value>
        public PessoVinculada? PessoaVinculada
        {
            get
            {
                return ModelEntity.PessoaVinculada == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.PessoaVinculada, typeof(PessoVinculada), true) as PessoVinculada?;
            }
            set
            {
                ModelEntity.PessoaVinculada = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the naturalidade.
        /// </summary>
        /// <value>
        /// The naturalidade.
        /// </value>
        public string UfNaturalidade
        {
            get
            {
                return ModelEntity.UFNaturalidade;
            }
            set
            {
                ModelEntity.UFNaturalidade = value;
            }
        }
        /// <summary>
        /// Gets or sets the situacao legal.
        /// </summary>
        /// <value>
        /// The situacao legal.
        /// </value>
        public SituacaoLegalPessoa? SituacaoLegal
        {
            get
            {
                return (SituacaoLegalPessoa?)ModelEntity.SituacaoLegal;
            }
            set
            {
                if (value != null)
                    ModelEntity.SituacaoLegal = (byte)value;
            }
        }
        /// <summary>
        /// Gets or sets the nome pai.
        /// </summary>
        /// <value>
        /// The nome pai.
        /// </value>
        public string NomePai
        {
            get
            {
                return ModelEntity.FiliacaoNomePai;
            }
            set
            {
                ModelEntity.FiliacaoNomePai = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome mae.
        /// </summary>
        /// <value>
        /// The nome mae.
        /// </value>
        public string NomeMae
        {
            get
            {
                return ModelEntity.FiliacaoNomeMae;
            }
            set
            {
                ModelEntity.FiliacaoNomeMae = value;
            }
        }
        /// <summary>
        /// Gets or sets the data vencimento cadastro.
        /// </summary>
        /// <value>
        /// The data vencimento cadastro.
        /// </value>
        public DateTime? DataVencimentoCadastro
        {
            get
            {
                return ModelEntity.DataVencimentoCadastro;
            }
            set
            {
                ModelEntity.DataVencimentoCadastro = value;
            }
        }

        /// <summary>
        /// Gets or sets the identifier pessoa.
        /// </summary>
        /// <value>
        /// The identifier pessoa.
        /// </value>
        public int? IdPessoa
        {
            get
            {
                return ModelEntity.IdPessoa;
            }
            set
            {
                ModelEntity.IdPessoa = value;
            }
        }

        #endregion
    }
}