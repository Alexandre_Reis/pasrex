﻿using Financial.CRM;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for Suitability
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Suitability", Namespace = "")]
    public class SuitabilityViewModel : EntityViewModel<PessoaSuitability>
    {
        #region Fields
        private SuitabilityPerfilInvestidorViewModel _perfil;
        private SuitabilityValidacaoViewModel _validacao;
        private SuitabilityTipoDispensaViewModel _tipoDispensa;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public SuitabilityViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public SuitabilityViewModel(PessoaSuitability modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null)
            {
                if (modelEntity.Perfil.HasValue)
                {
                    var suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();

                    if (suitabilityPerfilInvestidor.LoadByPrimaryKey(modelEntity.Perfil.Value))
                    {
                        _perfil = new SuitabilityPerfilInvestidorViewModel(suitabilityPerfilInvestidor);
                    }
                }

                if (modelEntity.IdValidacao.HasValue)
                {
                    var SuitabilityValidacao = new SuitabilityValidacao();

                    if (SuitabilityValidacao.LoadByPrimaryKey(modelEntity.IdValidacao.Value))
                    {
                        _validacao = new SuitabilityValidacaoViewModel(SuitabilityValidacao);
                    }
                }
                if (modelEntity.Dispensa.HasValue)
                {
                    var suitabilityTipoDispensa = new SuitabilityTipoDispensa();

                    if (suitabilityTipoDispensa.LoadByPrimaryKey(modelEntity.IdValidacao.Value))
                    {
                        _tipoDispensa = new SuitabilityTipoDispensaViewModel(suitabilityTipoDispensa);
                    }
                }
            }
        }
        #endregion

        #region properties

        /// <summary>
        /// Gets or sets the perfil.
        /// </summary>
        /// <value>
        /// The perfil.
        /// </value>
        public SuitabilityPerfilInvestidorViewModel Perfil
        {
            get
            {
                return _perfil;
            }
            set
            {
                _perfil = value;
            }
        }

        /// <summary>
        /// Gets or sets the recusa.
        /// </summary>
        /// <value>
        /// The recusa.
        /// </value>
        public Recusa? Recusa
        {
            get
            {
                return ModelEntity.Recusa == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.Recusa, typeof(Recusa), true) as Recusa?;
            }
            set
            {
                if (value != null)
                    ModelEntity.Recusa = Enums.Util.GetEnumDescription(value);
            }
        }

        /// <summary>
        /// Gets or sets the tipo de validacao.
        /// </summary>
        /// <value>
        /// The tipo de validacao.
        /// </value>
        public SuitabilityValidacaoViewModel TipoDeValidacao
        {
            get
            {
                return _validacao;
            }
            set
            {
                _validacao = value;
            }
        }

        /// <summary>
        /// Gets or sets the ultima alteracao.
        /// </summary>
        /// <value>
        /// The ultima alteracao.
        /// </value>
        public DateTime? UltimaAlteracao
        {
            get
            {
                return ModelEntity.UltimaAlteracao;
            }
            set
            {
                ModelEntity.UltimaAlteracao = value;
            }
        }
        /// <summary>
        /// Gets or sets the dispensado.
        /// </summary>
        /// <value>
        /// The dispensado.
        /// </value>
        public Dispensado? Dispensado
        {
            get
            {
                return ModelEntity.Dispensado == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.Dispensado, typeof(Dispensado), true) as Dispensado?;
            }
            set
            {
                if (value != null)
                    ModelEntity.Dispensado = Enums.Util.GetEnumDescription(value);
            }
        }

        /// <summary>
        /// Gets or sets the tipo de dispensa.
        /// </summary>
        /// <value>
        /// The tipo de dispensa.
        /// </value>
        public SuitabilityTipoDispensaViewModel TipoDeDispensa
        {
            get
            {
                return _tipoDispensa;
            }
            set
            {
                _tipoDispensa = value;
            }
        }
        #endregion

    }
}