﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// LocalFeriadoViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Common.Moeda}" />
    [Serializable]
    [XmlRoot(ElementName = "LocalFeriado", Namespace = "")]
    public class LocalFeriadoViewModel : EntityViewModel<LocalFeriado>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalFeriadoViewModel"/> class.
        /// </summary>
        public LocalFeriadoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalFeriadoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public LocalFeriadoViewModel(LocalFeriado modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier local.
        /// </summary>
        /// <value>
        /// The identifier local.
        /// </value>
        public short? IdLocal
        {
            get
            {
                return ModelEntity.IdLocal;
            }
            set
            {
                ModelEntity.IdLocal = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        #endregion
    }
}
