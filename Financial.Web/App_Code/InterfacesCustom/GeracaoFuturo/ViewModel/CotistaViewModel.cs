﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for Cotista
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Cotista", Namespace = "")]
    public class CotistaViewModel : EntityViewModel<Cotista>
    {
        #region constructor

        public CotistaViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public CotistaViewModel(Cotista modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the ativo.
        /// </summary>
        /// <value>
        /// The ativo.
        /// </value>
        public StatusAtivoCotista Ativo
        {
            get
            {
                return (StatusAtivoCotista)ModelEntity.StatusAtivo;
            }
            set
            {
                ModelEntity.StatusAtivo = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets the isento ir.
        /// </summary>
        /// <value>
        /// The isento ir.
        /// </value>
        public IsentoIr? IsentoIr
        {
            get
            {
                return ModelEntity.IsentoIR == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.IsentoIR, typeof(IsentoIr), true) as IsentoIr?;
            }
            set
            {
                if (value != null)
                    ModelEntity.IsentoIR = Enums.Util.GetEnumDescription(value);
            }
        }

        /// <summary>
        /// Gets or sets the isento iof.
        /// </summary>
        /// <value>
        /// The isento iof.
        /// </value>
        public IsentoIof? IsentoIof
        {
            get
            {
                return ModelEntity.IsentoIOF == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.IsentoIOF, typeof(IsentoIof), true) as IsentoIof?;
            }
            set
            {
                if (value != null)
                    ModelEntity.IsentoIOF = Enums.Util.GetEnumDescription(value);
            }
        }


        /// <summary>
        /// Gets or sets the tipo tributacao.
        /// </summary>
        /// <value>
        /// The tipo tributacao.
        /// </value>
        public TipoTributacaoCotista TipoTributacao
        {
            get
            {
                return (TipoTributacaoCotista)ModelEntity.TipoTributacao;
            }
            set
            {
                if(value!=null)
                 ModelEntity.TipoTributacao = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets the tipo cotista CVM.
        /// </summary>
        /// <value>
        /// The tipo cotista CVM.
        /// </value>
        public TipoCotistaCvm? TipoCotistaCvm
        {
            get
            {
                if (ModelEntity.TipoCotistaCVM!=null)
                    return (TipoCotistaCvm)ModelEntity.TipoCotistaCVM;

                return null;
            }
            set
            {
                if(value!=null)
                    ModelEntity.TipoCotistaCVM = (byte)value;
            }
        }
        /// <summary>
        /// Gets or sets the tipo cotista anbima.
        /// </summary>
        /// <value>
        /// The tipo cotista anbima.
        /// </value>
        public TipoCotistaAnbima? TipoCotistaAnbima
        {
            get
            {
                if (ModelEntity.TipoCotistaAnbima!=null)
                    return (TipoCotistaAnbima)ModelEntity.TipoCotistaAnbima;

                return null;
            }
            set
            {
                if(value!=null)
                    ModelEntity.TipoCotistaAnbima = (byte)value;
            }
        }
        /// <summary>
        /// Gets or sets the carteira.
        /// </summary>
        /// <value>
        /// The carteira.
        /// </value>
        public int? Carteira
        {
            get
            {
                return ModelEntity.IdCarteira;
            }
            set
            {
                ModelEntity.IdCarteira = value;
            }
        }
        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        public string Login
        {
            get
            {
                return "";
            }
            set
            {
                
            }
        }

        /// <summary>
        /// Gets or sets the codigo interface.
        /// </summary>
        /// <value>
        /// The codigo interface.
        /// </value>
        public string CodigoInterface
        {
            get
            {
                return ModelEntity.CodigoInterface;
            }
            set
            {
                ModelEntity.CodigoInterface = value;
            }
        }

        /// <summary>
        /// Gets or sets the data expiracao.
        /// </summary>
        /// <value>
        /// The data expiracao.
        /// </value>
        public DateTime? DataExpiracao
        {
            get
            {
                return ModelEntity.DataExpiracao;
            }
            set
            {
                ModelEntity.DataExpiracao = value;
            }
        }
        /// <summary>
        /// Gets or sets the pendencia cadastral.
        /// </summary>
        /// <value>
        /// The pendencia cadastral.
        /// </value>
        public string PendenciaCadastral
        {
            get
            {
                return ModelEntity.PendenciaCadastral;
            }
            set
            {
                ModelEntity.PendenciaCadastral = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier cotista.
        /// </summary>
        /// <value>
        /// The identifier cotista.
        /// </value>
        public int? IdCotista
        {
            get
            {
                return ModelEntity.IdCotista;
            }
            set
            {
                ModelEntity.IdCotista = value;
            }
        }
        #endregion
    }
}