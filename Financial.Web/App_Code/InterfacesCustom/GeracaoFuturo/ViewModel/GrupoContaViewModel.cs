﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// GrupoContaViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Investidor.GrupoConta}" />
    [Serializable]
    [XmlRoot(ElementName = "GrupoConta", Namespace = "")]
    public class GrupoContaViewModel : EntityViewModel<GrupoConta>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GrupoContaViewModel"/> class.
        /// </summary>
        public GrupoContaViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GrupoContaViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public GrupoContaViewModel(GrupoConta modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier grupo.
        /// </summary>
        /// <value>
        /// The identifier grupo.
        /// </value>
        public int? IdGrupo
        {
            get
            {
                return ModelEntity.IdGrupo;
            }
            set
            {
                ModelEntity.IdGrupo = value;
            }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get
            {
                return ModelEntity.Descricao;
            }
            set
            {
                ModelEntity.Descricao = value;
            }
        }
        #endregion
    }
}
