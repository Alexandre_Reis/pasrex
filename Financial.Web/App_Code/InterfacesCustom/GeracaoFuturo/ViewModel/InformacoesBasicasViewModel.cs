﻿using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for InformacoesBasicas
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "InformacoesBasicas", Namespace = "")]
    public class InformacoesBasicasViewModel : EntityViewModel<Pessoa>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InformacoesBasicasViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public InformacoesBasicasViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InformacoesBasicasViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public InformacoesBasicasViewModel(Pessoa modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the nome pessoa.
        /// </summary>
        /// <value>
        /// The nome pessoa.
        /// </value>
        public string NomePessoa
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome abreviado pessoa.
        /// </summary>
        /// <value>
        /// The nome abreviado pessoa.
        /// </value>
        public string NomeAbreviadoPessoa
        {
            get
            {
                return ModelEntity.Apelido;
            }
            set
            {
                ModelEntity.Apelido = value;
            }
        }

        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public TipoPessoa? Tipo
        {
            get
            {
                return (TipoPessoa)ModelEntity.Tipo;
            }
            set
            {
                ModelEntity.Tipo = (byte)value;
            }
        }
        /// <summary>
        /// </summary>
        /// <value>
        /// The c pf CNPJ.
        /// </value>
        public string CPfCnpj
        {
            get
            {
                return ModelEntity.Cpfcnpj;
            }
            set
            {
                ModelEntity.Cpfcnpj = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier pessoa.
        /// </summary>
        /// <value>
        /// The identifier pessoa.
        /// </value>
        public int? IdPessoa
        {
            get
            {
                return ModelEntity.IdPessoa;
            }
            set
            {
                ModelEntity.IdPessoa = value;
            }
        }
        #endregion
    }
}