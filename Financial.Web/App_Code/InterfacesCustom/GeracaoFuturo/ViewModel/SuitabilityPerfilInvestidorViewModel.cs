﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for SuitabilityPerfilInvestidorViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "SuitabilityPerfilInvestidor", Namespace = "")]
    public class SuitabilityPerfilInvestidorViewModel : EntityViewModel<SuitabilityPerfilInvestidor>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityPerfilInvestidorViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public SuitabilityPerfilInvestidorViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityPerfilInvestidorViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public SuitabilityPerfilInvestidorViewModel(SuitabilityPerfilInvestidor modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier perfil investidor.
        /// </summary>
        /// <value>
        /// The identifier perfil investidor.
        /// </value>
        public int? IdPerfilInvestidor
        {
            get
            {
                return ModelEntity.IdPerfilInvestidor;
            }
            set
            {
                ModelEntity.IdPerfilInvestidor = value;
            }
        }
        /// <summary>
        /// Gets or sets the nivel.
        /// </summary>
        /// <value>
        /// The nivel.
        /// </value>
        public int? Nivel
        {
            get
            {
                return ModelEntity.Nivel;
            }
            set
            {
                ModelEntity.Nivel = value;
            }
        }
        /// <summary>
        /// Gets or sets the perfil.
        /// </summary>
        /// <value>
        /// The perfil.
        /// </value>
        public string Perfil
        {
            get
            {
                return ModelEntity.Perfil;
            }
            set
            {
                ModelEntity.Perfil = value;
            }
        }
        #endregion
    }
}