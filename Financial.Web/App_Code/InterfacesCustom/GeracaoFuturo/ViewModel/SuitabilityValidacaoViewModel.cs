﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for SuitabilityPerfilInvestidorViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "SuitabilityValidacao", Namespace = "")]
    public class SuitabilityValidacaoViewModel : EntityViewModel<SuitabilityValidacao>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityValidacaoViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public SuitabilityValidacaoViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityValidacaoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public SuitabilityValidacaoViewModel(SuitabilityValidacao modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier validacao.
        /// </summary>
        /// <value>
        /// The identifier validacao.
        /// </value>
        public int? IdValidacao
        {
            get
            {
                return ModelEntity.IdValidacao;
            }
            set
            {
                ModelEntity.IdValidacao = value;
            }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get
            {
                return ModelEntity.Descricao;
            }
            set
            {
                ModelEntity.Descricao = value;
            }
        }
        #endregion
    }
}