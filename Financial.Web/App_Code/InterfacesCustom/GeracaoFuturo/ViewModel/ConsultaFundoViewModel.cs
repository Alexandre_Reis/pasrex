﻿using Financial.Common;
using Financial.Fundo;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Fundo.Carteira}" />
    [Serializable]
    [XmlRoot(ElementName = "ConsultaFundo", Namespace = "")]
    public class ConsultaFundoViewModel : EntityViewModel<Carteira>
    {
        #region
        private AgenteMercadoViewModel _administrador;
        private AgenteMercadoViewModel _gestor;
        private AgenteMercadoViewModel _custodiante;
        #endregion
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsultaFundoViewModel"/> class.
        /// </summary>
        public ConsultaFundoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsultaFundoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public ConsultaFundoViewModel(Carteira modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null && modelEntity.IdAgenteAdministrador.HasValue)
            {
                _administrador = new AgenteMercadoViewModel(modelEntity.UpToAgenteMercadoByIdAgenteAdministrador);
            }
            if (modelEntity != null && modelEntity.IdAgenteGestor.HasValue)
            {
                _gestor = new AgenteMercadoViewModel(modelEntity.UpToAgenteMercadoByIdAgenteGestor);
            }
            if (modelEntity != null && modelEntity.IdAgenteCustodiante.HasValue)
            {
                var agente = new AgenteMercado();
                agente.LoadByPrimaryKey(modelEntity.IdAgenteCustodiante.Value);
                _custodiante = new AgenteMercadoViewModel(agente);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the codigo fundo.
        /// </summary>
        /// <value>
        /// The codigo fundo.
        /// </value>
        public int? CodigoFundo
        {
            get
            {
                return ModelEntity.IdCarteira;
            }
            set
            {
                ModelEntity.IdCarteira = value;
            }
        }
        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get
            {
                return ModelEntity.Nome;
            }
            set
            {
                ModelEntity.Nome = value;
            }
        }
        /// <summary>
        /// </summary>
        /// <value>
        /// The CNPJ.
        /// </value>
        public string Cnpj
        {
            get
            {
                if (ModelEntity.GetColumn("Cnpj") == null || ModelEntity.GetColumn("Cnpj") is System.DBNull)
                    return null;

                return ModelEntity.GetColumn("Cnpj").ToString();
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the codigo CVM.
        /// </summary>
        /// <value>
        /// The codigo CVM.
        /// </value>
        public int? CodigoCvm
        {
            get
            {
                return ModelEntity.CodigoCDA;
            }
            set
            {
                ModelEntity.CodigoCDA = value;
            }
        }
        /// <summary>
        /// Gets or sets the codi codigo anbima.
        /// </summary>
        /// <value>
        /// The codi codigo anbima.
        /// </value>
        public string CodiCodigoAnbima
        {
            get
            {
                return ModelEntity.CodigoAnbid;
            }
            set
            {
                ModelEntity.CodigoAnbid = value;
            }
        }
        /// <summary>
        /// Gets or sets the administrador.
        /// </summary>
        /// <value>
        /// The administrador.
        /// </value>
        public AgenteMercadoViewModel Administrador
        {
            get
            {
                return _administrador;
            }
            set
            {
                _administrador = value;
            }
        }
        /// <summary>
        /// Gets or sets the custodiante.
        /// </summary>
        /// <value>
        /// The custodiante.
        /// </value>
        public AgenteMercadoViewModel Custodiante
        {
            get
            {
                return _custodiante;
            }
            set
            {
                _custodiante = value;
            }
        }
        /// <summary>
        /// Gets or sets the gestor.
        /// </summary>
        /// <value>
        /// The gestor.
        /// </value>
        public AgenteMercadoViewModel Gestor
        {
            get
            {
                return _gestor;
            }
            set
            {
                _gestor = value;
            }
        }
        #endregion
    }
}
