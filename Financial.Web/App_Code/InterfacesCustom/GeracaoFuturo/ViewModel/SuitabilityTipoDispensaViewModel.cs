﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for SuitabilityPerfilInvestidorViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "SuitabilityTipoDispensa", Namespace = "")]
    public class SuitabilityTipoDispensaViewModel : EntityViewModel<SuitabilityTipoDispensa>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityTipoDispensaViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public SuitabilityTipoDispensaViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SuitabilityTipoDispensaViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public SuitabilityTipoDispensaViewModel(SuitabilityTipoDispensa modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier dispensa.
        /// </summary>
        /// <value>
        /// The identifier dispensa.
        /// </value>
        public int? IdDispensa
        {
            get
            {
                return ModelEntity.IdDispensa;
            }
            set
            {
                ModelEntity.IdDispensa = value;
            }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get
            {
                return ModelEntity.Descricao;
            }
            set
            {
                ModelEntity.Descricao = value;
            }
        }
        #endregion
    }
}