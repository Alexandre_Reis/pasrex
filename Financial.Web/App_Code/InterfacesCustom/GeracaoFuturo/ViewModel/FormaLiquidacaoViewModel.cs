﻿using Financial.Common.Enums;
using Financial.ContaCorrente;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{

    [Serializable]
    [XmlRoot(ElementName = "FormaLiquidacao", Namespace = "")]
    public class FormaLiquidacaoViewModel : EntityViewModel<FormaLiquidacao>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FormaLiquidacaoViewModel"/> class.
        /// </summary>
        public FormaLiquidacaoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormaLiquidacaoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public FormaLiquidacaoViewModel(FormaLiquidacao modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier forma liquidacao.
        /// </summary>
        /// <value>
        /// The identifier forma liquidacao.
        /// </value>
        public byte? IdFormaLiquidacao
        {
            get
            {
                return ModelEntity.IdFormaLiquidacao;
            }
            set
            {
                ModelEntity.IdFormaLiquidacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get
            {
                return ModelEntity.Descricao;
            }
            set
            {
                ModelEntity.Descricao = value;
            }
        }
        /// <summary>
        /// Gets or sets the tipo liquidacao.
        /// </summary>
        /// <value>
        /// The tipo liquidacao.
        /// </value>
        public TipoLiquidacao? TipoLiquidacao
        {
            get
            {
                return (TipoLiquidacao?)ModelEntity.TipoLiquidacao;
            }
            set
            {
                if (value != null)
                    ModelEntity.TipoLiquidacao = (byte)value;
            }
        }
        #endregion
    }
}
