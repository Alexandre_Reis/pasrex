﻿using Financial.CRM.Enums;
using Financial.Fundo.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InvestidorCotista;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{

    /// <summary>
    /// CotistaPosicaoViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.InvestidorCotista.PosicaoCotistaHistorico}" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaPosicao", Namespace = "")]
    public class CotistaPosicaoViewModel : EntityViewModel<PosicaoCotistaHistorico>
    {
        #region Fields
        byte? _tipoPessoa;
        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaPosicaoViewModel"/> class.
        /// </summary>
        public CotistaPosicaoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaPosicaoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public CotistaPosicaoViewModel(PosicaoCotistaHistorico modelEntity)
            : base(modelEntity)
        {
            _tipoPessoa = ModelEntity.GetColumn("TipoPessoa") as byte?;
        }
        #endregion

        #region properties
        /// <summary>
        /// Gets or sets the numero nota.
        /// </summary>
        /// <value>
        /// The numero nota.
        /// </value>
        public int? NumeroNota
        {
            get
            {
                return ModelEntity.IdOperacao;
            }
            set
            {
                ModelEntity.IdOperacao = value;
            }
        }

        /// <summary>
        /// Gets or sets the codigo fundo.
        /// </summary>
        /// <value>
        /// The codigo fundo.
        /// </value>
        public int? CodigoFundo
        {
            get
            {
                return ModelEntity.IdCarteira;
            }
            set
            {
                ModelEntity.IdCarteira = value;
            }
        }
        /// <summary>
        /// Gets or sets the data aplicacao.
        /// </summary>
        /// <value>
        /// The data aplicacao.
        /// </value>
        public DateTime? DataAplicacao
        {
            get
            {
                return ModelEntity.DataAplicacao;
            }
            set
            {
                ModelEntity.DataAplicacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the se liberado.
        /// </summary>
        /// <value>
        /// The se liberado.
        /// </value>
        public Liberada SeLiberado
        {
            get
            {
                return ModelEntity.DataHistorico >= ModelEntity.DataConversao ? Liberada.Sim : Liberada.Nao;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the quantidade cotas.
        /// </summary>
        /// <value>
        /// The quantidade cotas.
        /// </value>
        public decimal? QuantidadeCotas
        {
            get
            {
                return ModelEntity.Quantidade;
            }
            set
            {
                ModelEntity.Quantidade = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor aplicacao.
        /// </summary>
        /// <value>
        /// The valor aplicacao.
        /// </value>
        public decimal? ValorAplicacao
        {
            get
            {
                return ModelEntity.ValorAplicacao;
            }
            set
            {
                ModelEntity.ValorAplicacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the data cotizacao.
        /// </summary>
        /// <value>
        /// The data cotizacao.
        /// </value>
        public DateTime? DataCotizacao
        {
            get
            {
                return ModelEntity.DataConversao;
            }
            set
            {
                ModelEntity.DataConversao = value;
            }
        }
        /// <summary>
        /// Gets or sets the cota aplicacao.
        /// </summary>
        /// <value>
        /// The cota aplicacao.
        /// </value>
        public decimal? CotaAplicacao
        {
            get
            {
                return ModelEntity.CotaAplicacao;
            }
            set
            {
                ModelEntity.CotaAplicacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor bruto.
        /// </summary>
        /// <value>
        /// The valor bruto.
        /// </value>
        public decimal? ValorBruto
        {
            get
            {
                return ModelEntity.ValorBruto;
            }
            set
            {
                ModelEntity.ValorBruto = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor provisao ir.
        /// </summary>
        /// <value>
        /// The valor provisao ir.
        /// </value>
        public decimal? ValorProvisaoIr
        {
            get
            {
                return ModelEntity.ValorIR;
            }
            set
            {
                ModelEntity.ValorIR = value;
            }
        }
        /// <summary>
        /// Gets or sets the valorprovisao iof.
        /// </summary>
        /// <value>
        /// The valorprovisao iof.
        /// </value>
        public decimal? ValorprovisaoIof
        {
            get
            {
                return ModelEntity.ValorIOF;
            }
            set
            {
                ModelEntity.ValorIOF = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor resgate liquido.
        /// </summary>
        /// <value>
        /// The valor resgate liquido.
        /// </value>
        public decimal? ValorResgateLiquido
        {
            get
            {
                return ModelEntity.ValorLiquido;
            }
            set
            {
                ModelEntity.ValorLiquido = value;
            }
        }
        /// <summary>
        /// Gets or sets the data ultimo aniverario corrido.
        /// </summary>
        /// <value>
        /// The data ultimo aniverario corrido.
        /// </value>
        public DateTime? DataUltimoAniverarioCorrido
        {
            get
            {
                return null;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the data ultimo aniversario utility.
        /// </summary>
        /// <value>
        /// The data ultimo aniversario utility.
        /// </value>
        public DateTime? DataUltimoAniversarioUtil
        {
            get
            {
                return null;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the data proximo aniversario corrido.
        /// </summary>
        /// <value>
        /// The data proximo aniversario corrido.
        /// </value>
        public DateTime? DataProximoAniversarioCorrido
        {
            get
            {
                return null;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the data proximo aniversario utility.
        /// </summary>
        /// <value>
        /// The data proximo aniversario utility.
        /// </value>
        public DateTime? DataProximoAniversarioUtil
        {
            get
            {
                return null;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the quantidade dias aniversario.
        /// </summary>
        /// <value>
        /// The quantidade dias aniversario.
        /// </value>
        public decimal? QuantidadeDiasAniversario
        {
            get
            {
                return null;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the quantidade dias carencia.
        /// </summary>
        /// <value>
        /// The quantidade dias carencia.
        /// </value>
        public int? QuantidadeDiasCarencia
        {
            get { return 0; }
            set { }
        }
        /// <summary>
        /// Gets or sets the valor performance.
        /// </summary>
        /// <value>
        /// The valor performance.
        /// </value>
        public decimal? ValorPerformance
        {
            get
            {
                return ModelEntity.ValorPerformance;
            }
            set
            {
                ModelEntity.ValorPerformance = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor penalty fee.
        /// </summary>
        /// <value>
        /// The valor penalty fee.
        /// </value>
        public decimal? ValorPenaltyFee
        {
            get { return 0; }
            set { }
        }

        /// <summary>
        /// Gets or sets the nome cotista.
        /// </summary>
        /// <value>
        /// The nome cotista.
        /// </value>
        public string NomeCotista
        {
            get
            {
                if (ModelEntity.GetColumn("NomeCotista") == null)
                    return null;

                return ModelEntity.GetColumn("NomeCotista").ToString();
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the tipo pessoa.
        /// </summary>
        /// <value>
        /// The tipo pessoa.
        /// </value>
        [XmlElement(ElementName = "TipoPessoa")]
        public TipoPessoa? TipoPessoa
        {
            get
            {
                if (_tipoPessoa == null)
                    return null;

                return (TipoPessoa?)_tipoPessoa;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the nome abreviado cotista.
        /// </summary>
        /// <value>
        /// The nome abreviado cotista.
        /// </value>
        public string NomeAbreviadoCotista
        {
            get
            {
                if (ModelEntity.GetColumn("NomeAbreviado") == null)
                    return null;

                return ModelEntity.GetColumn("NomeAbreviado").ToString();
            }
            set { }
        }

        /// <summary>
        /// Gets the codigo distribuidor.
        /// </summary>
        /// <value>
        /// The codigo distribuidor.
        /// </value>
        public string CodigoDistribuidor { get { return null; } }

        /// <summary>
        /// Gets or sets the tipo calculo cota.
        /// </summary>
        /// <value>
        /// The tipo calculo cota.
        /// </value>
        public TipoCotaFundo? TipoCalculoCota
        {
            get
            {
                if (ModelEntity.GetColumn("TipoCota") == null)
                    return null;
                var tipoCota = ModelEntity.GetColumn("TipoCota") as byte?;

                return (TipoCotaFundo?)tipoCota;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the tipo posicao.
        /// </summary>
        /// <value>
        /// The tipo posicao.
        /// </value>
        public TipoPosicao? TipoPosicao
        {
            get
            {
                if (ModelEntity.GetColumn("TipoPosicao") == null)
                    return null;

                var tipoPos = ModelEntity.GetColumn("TipoPosicao").ToString();

                return Enums.Util.GetEnumDescriptionValue(tipoPos, typeof(TipoPosicao), true) as TipoPosicao?;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the valor cota abertura.
        /// </summary>
        /// <value>
        /// The valor cota abertura.
        /// </value>
        public decimal? ValorCotaAbertura
        {
            get
            {
                if (ModelEntity.GetColumn("CotaAbertura") == null)
                    return null;

                return (decimal)ModelEntity.GetColumn("CotaAbertura");
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the valor cota fechamento.
        /// </summary>
        /// <value>
        /// The valor cota fechamento.
        /// </value>
        public decimal? ValorCotaFechamento
        {
            get
            {
                if (ModelEntity.GetColumn("CotaFechamento") == null)
                    return null;

                return (decimal)ModelEntity.GetColumn("CotaFechamento");
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the indicador ir automatico.
        /// </summary>
        /// <value>
        /// The indicador ir automatico.
        /// </value>
        public IrAutomatico? IndicadorIrAutomatico
        {
            get
            {
                if (ModelEntity.GetColumn("IrAutomatico") == null)
                    return null;

                var tipoPos = ModelEntity.GetColumn("IrAutomatico").ToString();

                return Enums.Util.GetEnumDescriptionValue(tipoPos, typeof(IrAutomatico), true) as IrAutomatico?;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the indicador fundo FMP FGTS.
        /// </summary>
        /// <value>
        /// The indicador fundo FMP FGTS.
        /// </value>
        public FundoFmpFgts? IndicadorFundoFmpFgts
        {
            get
            {
                if (ModelEntity.GetColumn("FundoFmpFgts") == null)
                    return null;

                var tipoPos = ModelEntity.GetColumn("FundoFmpFgts").ToString();

                return Enums.Util.GetEnumDescriptionValue(tipoPos, typeof(FundoFmpFgts), true) as FundoFmpFgts?;
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the tipo liquidacao.
        /// </summary>
        /// <value>
        /// The tipo liquidacao.
        /// </value>
        public string TipoLiquidacao
        {
            get
            {
                if (ModelEntity.GetColumn("TipoLiquidacao") == null)
                    return null;

                return ModelEntity.GetColumn("TipoLiquidacao").ToString();
            }
            set { }
        }
        #endregion

    }

    //[XmlRoot(ElementName = "PosicoesCotista", Namespace = "")]
    //public class PosicaoCotistaList : XmlSerializableList<CotistaPosicaoViewModel> { }

}
