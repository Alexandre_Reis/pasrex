﻿using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Investidor.Cliente}" />
    [Serializable]
    [XmlRoot(ElementName = "DataPosicaoFundo", Namespace = "")]
    public class DataPosicaoFundoViewModel : EntityViewModel<Cliente>
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoViewModel"/> class.
        /// </summary>
        public DataPosicaoFundoViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPosicaoFundoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public DataPosicaoFundoViewModel(Cliente modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        public int? CodigoFundo
        {
            get
            {
                return ModelEntity.IdCliente;
            }
            set
            {
                ModelEntity.IdCliente = value;
            }
        }
        /// <summary>
        /// Gets or sets the data posicao.
        /// </summary>
        /// <value>
        /// The data posicao.
        /// </value>
        public DateTime? DataPosicao
        {
            get
            {
                return ModelEntity.DataDia;
            }
            set
            {
                ModelEntity.DataDia = value;
            }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public StatusCliente? Descricao
        {
            get
            {
                return (StatusCliente?)ModelEntity.Status;
            }
            set
            {
                if (value != null)
                    ModelEntity.Status = (byte)value;
            }
        }
        
        #endregion
    }
}
