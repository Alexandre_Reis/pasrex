﻿using Financial.ContaCorrente;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InvestidorCotista;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// CotistaMovimentoResgateViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.InvestidorCotista.DetalheResgateCotista}" />
    [Serializable]
    [XmlRoot(ElementName = "CotistaMovimentoResgate", Namespace = "")]
    public class CotistaMovimentoResgateViewModel : EntityViewModel<DetalheResgateCotista>
    {
        #region fields
        private FormaLiquidacaoViewModel _formaLiquidacao;
        private ContaCorrenteViewModel _conta;
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoResgateViewModel"/> class.
        /// </summary>
        public CotistaMovimentoResgateViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CotistaMovimentoResgateViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public CotistaMovimentoResgateViewModel(DetalheResgateCotista modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null && modelEntity.GetColumn("FormaLiquidacao") != null)
            {
                var liq = new FormaLiquidacao();
                if (liq.LoadByPrimaryKey((byte)modelEntity.GetColumn("FormaLiquidacao")))
                {
                    _formaLiquidacao = new FormaLiquidacaoViewModel(liq);
                }
            }

            if (modelEntity != null && modelEntity.GetColumn("Conta") != null && !(modelEntity.GetColumn("Conta") is System.DBNull))
            {
                var conta = new Investidor.ContaCorrente();
                if (conta.LoadByPrimaryKey((byte)modelEntity.GetColumn("Conta")))
                {
                    _conta = new ContaCorrenteViewModel(conta);
                }
            }

        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the nome cotista.
        /// </summary>
        /// <value>
        /// The nome cotista.
        /// </value>
        public string NomeCotista
        {
            get
            {
                if (ModelEntity.GetColumn("NomeCotista") == null)
                    return null;

                return ModelEntity.GetColumn("NomeCotista").ToString();
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the identifier carteira.
        /// </summary>
        /// <value>
        /// The identifier carteira.
        /// </value>
        public int? CodigoFundo
        {
            get
            {
                return ModelEntity.IdCarteira;
            }
            set
            {
                ModelEntity.IdCarteira = value;
            }
        }

        /// <summary>
        /// Gets or sets the nome fundo.
        /// </summary>
        /// <value>
        /// The nome fundo.
        /// </value>
        public string NomeFundo
        {
            get
            {
                if (ModelEntity.GetColumn("NomeFundo") == null)
                    return null;

                return ModelEntity.GetColumn("NomeFundo").ToString();
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the quantidade cotas.
        /// </summary>
        /// <value>
        /// The quantidade cotas.
        /// </value>
        public decimal? QuantidadeCotas
        {
            get
            {
                return ModelEntity.Quantidade;
            }
            set
            {
                ModelEntity.Quantidade = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor bruto.
        /// </summary>
        /// <value>
        /// The valor bruto.
        /// </value>
        public decimal? ValorBruto
        {
            get
            {
                return ModelEntity.ValorBruto;
            }
            set
            {
                ModelEntity.ValorBruto = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor ir.
        /// </summary>
        /// <value>
        /// The valor ir.
        /// </value>
        public decimal? ValorIr
        {
            get
            {
                return ModelEntity.ValorIR;
            }
            set
            {
                ModelEntity.ValorIR = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor iof.
        /// </summary>
        /// <value>
        /// The valor iof.
        /// </value>
        public decimal? ValorIof
        {
            get
            {
                return ModelEntity.ValorIOF;
            }
            set
            {
                ModelEntity.ValorIOF = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor liquido.
        /// </summary>
        /// <value>
        /// The valor liquido.
        /// </value>
        public decimal? ValorLiquido
        {
            get
            {
                return ModelEntity.ValorLiquido;
            }
            set
            {
                ModelEntity.ValorLiquido = value;
            }
        }
        /// <summary>
        /// Gets or sets the valor rendimento.
        /// </summary>
        /// <value>
        /// The valor rendimento.
        /// </value>
        public decimal? ValorRendimento
        {
            get
            {
                return ModelEntity.RendimentoResgate;
            }
            set
            {
                ModelEntity.RendimentoResgate = value;
            }
        }
        /// <summary>
        /// Gets or sets the numero nota.
        /// </summary>
        /// <value>
        /// The numero nota.
        /// </value>
        public int? NumeroNota
        {
            get
            {
                return ModelEntity.IdPosicaoResgatada;
            }
            set
            {
                ModelEntity.IdPosicaoResgatada = value;
            }
        }
        /// <summary>
        /// Gets or sets the data cotizacao.
        /// </summary>
        /// <value>
        /// The data cotizacao.
        /// </value>
        public DateTime? DataCotizacao
        {
            get
            {
                if (ModelEntity.GetColumn("DataCotizacao") == null)
                    return null;

                return (DateTime)ModelEntity.GetColumn("DataCotizacao");
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the data liquidacao financeira.
        /// </summary>
        /// <value>
        /// The data liquidacao financeira.
        /// </value>
        public DateTime? DataLiquidacaoFinanceira
        {
            get
            {
                if (ModelEntity.GetColumn("DataLiquidacaoFinanceira") == null)
                    return null;

                return (DateTime)ModelEntity.GetColumn("DataLiquidacaoFinanceira");
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the tipo liquidacao.
        /// </summary>
        /// <value>
        /// The tipo liquidacao.
        /// </value>
        public FormaLiquidacaoViewModel TipoLiquidacao
        {
            get
            {
                return _formaLiquidacao;
            }
            set
            {
                _formaLiquidacao = value;
            }
        }

        /// <summary>
        /// Gets or sets the conta.
        /// </summary>
        /// <value>
        /// The conta.
        /// </value>
        public ContaCorrenteViewModel Conta
        {
            get
            {
                return _conta;
            }
            set
            {
                _conta = value;
            }
        }
        #endregion
    }
}
