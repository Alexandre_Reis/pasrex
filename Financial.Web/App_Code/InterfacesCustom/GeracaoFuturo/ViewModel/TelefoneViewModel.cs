﻿using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for Telefone
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Telefone", Namespace = "")]
    public class TelefoneViewModel : EntityViewModel<PessoaTelefone>
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="TelefoneViewModel"/> class.
        /// parameterless constructor to web service serialization.
        /// </summary>
        public TelefoneViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="TelefoneViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public TelefoneViewModel(PessoaTelefone modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the tipo telefone.
        /// </summary>
        /// <value>
        /// The tipo telefone.
        /// </value>
        public TipoTelefonePessoa TipoTelefone
        {
            get
            {
                return (TipoTelefonePessoa)ModelEntity.TipoTelefone;
            }
            set
            {
                ModelEntity.TipoTelefone = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets the ddi.
        /// </summary>
        /// <value>
        /// The ddi.
        /// </value>
        public string DDI
        {
            get
            {
                return ModelEntity.Ddi;
            }
            set
            {
                ModelEntity.Ddi = value;
            }
        }

        /// <summary>
        /// </summary>
        /// <value>
        /// The DDD.
        /// </value>
        public string DDD
        {
            get
            {
                return ModelEntity.Ddd;
            }
            set
            {
                ModelEntity.Ddd = value;
            }
        }
        /// <summary>
        /// Gets or sets the numero.
        /// </summary>
        /// <value>
        /// The numero.
        /// </value>
        public string Numero
        {
            get
            {
                return ModelEntity.Numero;
            }
            set
            {
                ModelEntity.Numero = value;
            }
        }
        /// <summary>
        /// Gets or sets the ramal.
        /// </summary>
        /// <value>
        /// The ramal.
        /// </value>
        public string Ramal
        {
            get
            {
                return ModelEntity.Ramal;
            }
            set
            {
                ModelEntity.Ramal = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier telefone.
        /// </summary>
        /// <value>
        /// The identifier telefone.
        /// </value>
        public int? IdTelefone
        {
            get
            {
                return ModelEntity.IdTelefone;
            }
            set
            {
                ModelEntity.IdTelefone = value;
            }
        }
        #endregion
    }
}
