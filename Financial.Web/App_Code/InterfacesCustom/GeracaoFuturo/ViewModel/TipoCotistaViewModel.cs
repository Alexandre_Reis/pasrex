﻿using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// TipoCotistaViewModel
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "TipoCotista", Namespace = "")]
    public class TipoCotistaViewModel
    {
        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoCotistaViewModel"/> class.
        /// </summary>
        public TipoCotistaViewModel()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public OrigemTipoCotista Tipo
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public int Codigo
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get; set;
        }
        #endregion
    }
}