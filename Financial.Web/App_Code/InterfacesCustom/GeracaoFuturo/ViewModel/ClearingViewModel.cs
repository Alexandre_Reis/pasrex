﻿using Financial.Common;
using Financial.ContaCorrente;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    [Serializable]
    [XmlRoot(ElementName = "Clearing", Namespace = "")]
    public class ClearingViewModel : EntityViewModel<Clearing>
    {
        #region fields
        private FormaLiquidacaoViewModel _formaLiquidacao;
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearingViewModel"/> class.
        /// </summary>
        public ClearingViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearingViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public ClearingViewModel(Clearing modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null && ModelEntity.IdFormaLiquidacao.HasValue)
            {
                var liq = new FormaLiquidacao();
                if (liq.LoadByPrimaryKey(ModelEntity.IdFormaLiquidacao.Value))
                {
                    _formaLiquidacao = new FormaLiquidacaoViewModel(liq);
                }
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier clearing.
        /// </summary>
        /// <value>
        /// The identifier clearing.
        /// </value>
        public int? IdClearing
        {
            get
            {
                return ModelEntity.IdClearing;
            }
            set
            {
                ModelEntity.IdClearing = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public string Codigo
        {
            get
            {
                return ModelEntity.Codigo;
            }
            set
            {
                ModelEntity.Codigo = value;
            }
        }
        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get
            {
                return ModelEntity.Descricao;
            }
            set
            {
                ModelEntity.Descricao = value;
            }
        }
        /// <summary>
        /// </summary>
        /// <value>
        /// The CNPJ.
        /// </value>
        public string Cnpj
        {
            get
            {
                return ModelEntity.Cnpj;
            }
            set
            {
                ModelEntity.Cnpj = value;
            }
        }
        /// <summary>
        /// Gets or sets the forma liquidacao.
        /// </summary>
        /// <value>
        /// The forma liquidacao.
        /// </value>
        public FormaLiquidacaoViewModel FormaLiquidacao
        {
            get
            {
                return _formaLiquidacao;
            }
            set
            {
                _formaLiquidacao = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier local feriado.
        /// </summary>
        /// <value>
        /// The identifier local feriado.
        /// </value>
        public short? IdLocalFeriado
        {
            get
            {
                return ModelEntity.IdLocalFeriado;
            }
            set
            {
                ModelEntity.IdLocalFeriado = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo cetip.
        /// </summary>
        /// <value>
        /// The codigo cetip.
        /// </value>
        public string CodigoCETIP
        {
            get
            {
                return ModelEntity.CodigoCETIP;
            }
            set
            {
                ModelEntity.CodigoCETIP = value;
            }
        }
        /// <summary>
        /// Gets or sets the codigo selic.
        /// </summary>
        /// <value>
        /// The codigo selic.
        /// </value>
        public string CodigoSELIC
        {
            get
            {
                return ModelEntity.CodigoSELIC;
            }
            set
            {
                ModelEntity.CodigoSELIC = value;
            }
        }
        #endregion
    }
}
