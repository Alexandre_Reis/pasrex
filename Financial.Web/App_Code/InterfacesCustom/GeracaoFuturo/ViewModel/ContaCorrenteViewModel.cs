﻿using Financial.Common;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.Investidor;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{

    /// <summary>
    /// ContaCorrenteViewModel
    /// </summary>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.ViewModel.BaseViewModel{Financial.Investidor.ContaCorrente}" />
    [Serializable]
    [XmlRoot(ElementName = "ContaCorrente", Namespace = "")]
    public class ContaCorrenteViewModel : EntityViewModel<Financial.Investidor.ContaCorrente>
    {
        #region fields
        private BancoViewModel _banco;
        private AgenciaViewModel _agencia;
        private MoedaViewModel _moeda;
        private LocalFeriadoViewModel _local;
        private GrupoContaViewModel _grupo;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContaCorrenteViewModel"/> class.
        /// </summary>
        public ContaCorrenteViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContaCorrenteViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public ContaCorrenteViewModel(Financial.Investidor.ContaCorrente modelEntity)
            : base(modelEntity)
        {
            if (modelEntity != null && modelEntity.IdBanco.HasValue)
            {
                var banco = new Banco();
                if (banco.LoadByPrimaryKey(modelEntity.IdBanco.Value))
                {
                    _banco = new BancoViewModel(banco);
                }
            }
            if (modelEntity != null && modelEntity.IdAgencia.HasValue)
            {
                var agencia = new Agencia();
                if (agencia.LoadByPrimaryKey(modelEntity.IdAgencia.Value))
                {
                    _agencia = new AgenciaViewModel(agencia);
                }
            }
            if (modelEntity != null && modelEntity.IdMoeda.HasValue)
            {
                var moeda = new Moeda();
                if (moeda.LoadByPrimaryKey((short)modelEntity.IdMoeda.Value))
                {
                    _moeda = new MoedaViewModel(moeda);
                }
            }
            if (modelEntity != null && modelEntity.IdLocal.HasValue)
            {
                var local = new LocalFeriado();
                if (local.LoadByPrimaryKey((short)modelEntity.IdLocal.Value))
                {
                    _local = new LocalFeriadoViewModel(local);
                }
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the banco.
        /// </summary>
        /// <value>
        /// The banco.
        /// </value>
        [XmlElement(ElementName = "Banco")]
        private BancoViewModel Banco
        {
            get
            {
                return _banco;
            }
            set
            {
                _banco = value;
            }
        }
        /// <summary>
        /// Gets or sets the agencia.
        /// </summary>
        /// <value>
        /// The agencia.
        /// </value>
        [XmlElement(ElementName = "Agencia")]
        public AgenciaViewModel Agencia
        {
            get
            {
                return _agencia;
            }
            set
            {
                _agencia = value;
            }
        }

        /// <summary>
        /// Gets or sets the identifier conta.
        /// </summary>
        /// <value>
        /// The identifier conta.
        /// </value>
        public int? IdConta
        {
            get
            {
                return ModelEntity.IdConta;
            }
            set
            {
                ModelEntity.IdConta = value;
            }
        }
        /// <summary>
        /// Gets or sets the numero.
        /// </summary>
        /// <value>
        /// The numero.
        /// </value>
        public string Numero
        {
            get
            {
                return ModelEntity.Numero;
            }
            set
            {
                ModelEntity.Numero = value;
            }
        }
        /// <summary>
        /// Gets or sets the digito conta.
        /// </summary>
        /// <value>
        /// The digito conta.
        /// </value>
        public string DigitoConta
        {
            get
            {
                return ModelEntity.DigitoConta;
            }
            set
            {
                ModelEntity.DigitoConta = value;
            }
        }
        /// <summary>
        /// Gets or sets the moeda.
        /// </summary>
        /// <value>
        /// The moeda.
        /// </value>
        public MoedaViewModel Moeda
        {
            get
            {
                return _moeda;
            }
            set
            {
                _moeda = value;
            }
        }
        /// <summary>
        /// Gets or sets the local.
        /// </summary>
        /// <value>
        /// The local.
        /// </value>
        public LocalFeriadoViewModel Local
        {
            get
            {
                return _local;
            }
            set
            {
                _local = value;
            }
        }
        /// <summary>
        /// Gets or sets the grupo.
        /// </summary>
        /// <value>
        /// The grupo.
        /// </value>
        public GrupoContaViewModel Grupo
        {
            get
            {
                return _grupo;
            }
            set
            {
                _grupo = value;
            }
        }
        /// <summary>
        /// Gets or sets the conta default.
        /// </summary>
        /// <value>
        /// The conta default.
        /// </value>
        public ContaDefault? ContaDefault
        {
            get
            {
                return ModelEntity.ContaDefault == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.ContaDefault, typeof(ContaDefault), true) as ContaDefault?;
            }
            set
            {
                if (value != null)
                    ModelEntity.ContaDefault = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the conta default cliente.
        /// </summary>
        /// <value>
        /// The conta default cliente.
        /// </value>
        public ContaDefaultCliente? ContaDefaultCliente
        {
            get
            {
                return ModelEntity.ContaDefaultCliente == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.ContaDefaultCliente, typeof(ContaDefaultCliente), true) as ContaDefaultCliente?;
            }
            set
            {
                if (value != null)
                    ModelEntity.ContaDefaultCliente = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the conta default cotista.
        /// </summary>
        /// <value>
        /// The conta default cotista.
        /// </value>
        public ContaDefaultCotista? ContaDefaultCotista
        {
            get
            {
                return ModelEntity.ContaDefaultCotista == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.ContaDefaultCotista, typeof(ContaDefaultCotista), true) as ContaDefaultCotista?;
            }
            set
            {
                if (value != null)
                    ModelEntity.ContaDefaultCotista = Enums.Util.GetEnumDescription(value);
            }
        }

        /// <summary>
        /// Gets or sets the identifier cotista.
        /// </summary>
        /// <value>
        /// The identifier cotista.
        /// </value>
        public int? IdCotista
        {
            get
            {
                if (ModelEntity.GetColumn("CotistaId") == null || ModelEntity.GetColumn("CotistaId") is System.DBNull)
                    return null;

                return (int)ModelEntity.GetColumn("CotistaId");
            }
            set { }
        }
        /// <summary>
        /// Gets or sets the identifier pessoa.
        /// </summary>
        /// <value>
        /// The identifier pessoa.
        /// </value>
        public int? IdPessoa
        {
            get
            {
                return ModelEntity.IdPessoa;
            }
            set
            {
                ModelEntity.IdPessoa = value;
            }
        }
        #endregion
    }
}
