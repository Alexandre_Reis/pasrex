﻿using Financial.CRM;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using System;
using System.Xml.Serialization;

namespace Financial.InterfacesCustom.GeracaoFuturo.ViewModel
{
    /// <summary>
    /// Summary description for Enderecos
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Endereco", Namespace = "")]
    public class EnderecoViewModel : EntityViewModel<PessoaEndereco>
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="EnderecoViewModel"/> class.
        ///  parameterless constructor to web service serialization.
        /// </summary>
        public EnderecoViewModel()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="EnderecoViewModel"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public EnderecoViewModel(PessoaEndereco modelEntity)
            : base(modelEntity)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public TipoEndereco? Tipo
        {
            get
            {
                return ModelEntity.TipoEndereco == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.TipoEndereco, typeof(TipoEndereco), true) as TipoEndereco?;
            }
            set
            {
                if (value != null)
                    ModelEntity.TipoEndereco = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the correspondencia.
        /// </summary>
        /// <value>
        /// The correspondencia.
        /// </value>
        public RecebeCorrespondencia? Correspondencia
        {
            get
            {
                return ModelEntity.RecebeCorrespondencia == null ? null : Enums.Util.GetEnumDescriptionValue(ModelEntity.RecebeCorrespondencia, typeof(RecebeCorrespondencia), true) as RecebeCorrespondencia?;
            }
            set
            {
                if (value != null)
                    ModelEntity.RecebeCorrespondencia = Enums.Util.GetEnumDescription(value);
            }
        }
        /// <summary>
        /// Gets or sets the logradouro.
        /// </summary>
        /// <value>
        /// The logradouro.
        /// </value>
        public string Logradouro
        {
            get
            {
                return ModelEntity.Endereco;
            }
            set
            {
                ModelEntity.Endereco = value;
            }
        }
        /// <summary>
        /// Gets or sets the numero.
        /// </summary>
        /// <value>
        /// The numero.
        /// </value>
        public string Numero
        {
            get
            {
                return ModelEntity.Numero;
            }
            set
            {
                ModelEntity.Numero = value;
            }
        }
        /// <summary>
        /// Gets or sets the complemento.
        /// </summary>
        /// <value>
        /// The complemento.
        /// </value>
        public string Complemento
        {
            get
            {
                return ModelEntity.Complemento;
            }
            set
            {
                ModelEntity.Complemento = value;
            }
        }
        /// <summary>
        /// Gets or sets the bairro.
        /// </summary>
        /// <value>
        /// The bairro.
        /// </value>
        public string Bairro
        {
            get
            {
                return ModelEntity.Bairro;
            }
            set
            {
                ModelEntity.Bairro = value;
            }
        }
        /// <summary>
        /// Gets or sets the cidade.
        /// </summary>
        /// <value>
        /// The cidade.
        /// </value>
        public string Cidade
        {
            get
            {
                return ModelEntity.Cidade;
            }
            set
            {
                ModelEntity.Cidade = value;
            }
        }
        /// <summary>
        /// Gets or sets the uf.
        /// </summary>
        /// <value>
        /// The uf.
        /// </value>
        public string UF
        {
            get
            {
                return ModelEntity.Uf;
            }
            set
            {
                ModelEntity.Uf = value;
            }
        }
        /// <summary>
        /// Gets or sets the cep.
        /// </summary>
        /// <value>
        /// The cep.
        /// </value>
        public string CEP
        {
            get
            {
                return ModelEntity.Cep;
            }
            set
            {
                ModelEntity.Cep = value;
            }
        }
        /// <summary>
        /// Gets or sets the pais.
        /// </summary>
        /// <value>
        /// The pais.
        /// </value>
        public string Pais
        {
            get
            {
                return ModelEntity.Pais;
            }
            set
            {
                ModelEntity.Pais = value;
            }
        }
        /// <summary>
        /// Gets or sets the identifier endereco.
        /// </summary>
        /// <value>
        /// The identifier endereco.
        /// </value>
        public int? IdEndereco
        {
            get
            {
                return ModelEntity.IdEndereco;
            }
            set
            {
                ModelEntity.IdEndereco = value;
            }
        }
        #endregion
    }
}