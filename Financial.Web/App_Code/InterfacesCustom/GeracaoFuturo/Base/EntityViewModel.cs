﻿using EntitySpaces.Core;
using System;
using System.Reflection;

namespace Financial.InterfacesCustom.GeracaoFuturo.Base
{
    /// <summary>
    /// EntityViewModel
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="Financial.InterfacesCustom.GeracaoFuturo.Base.BaseViewModel" />
    public class EntityViewModel<TEntity> : BaseViewModel where TEntity : esEntity, new()
    {
        /// <summary>
        /// Gets the model entity.
        /// </summary>
        /// <value>
        /// The model entity.
        /// </value>
        protected TEntity ModelEntity { get; private set; }

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityViewModel{TEntity}"/> class.
        /// </summary>
        /// <param name="modelEntity">The model entity.</param>
        public EntityViewModel(TEntity modelEntity)
        {
            ModelEntity = modelEntity;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityViewModel{TEntity}"/> class.
        /// </summary>
        public EntityViewModel()
        {
            ModelEntity = new TEntity();
        }
        #endregion
    }
}