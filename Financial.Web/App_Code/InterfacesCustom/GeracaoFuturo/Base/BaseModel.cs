﻿using EntitySpaces.Core;
using Financial.InterfacesCustom.GeracaoFuturo.Base;
using Financial.InvestidorCotista;
using Financial.Security;
using System;
using System.Collections.Generic;

namespace Financial.InterfacesCustom.GeracaoFuturo.Model
{
    /// <summary>
    /// BaseModel
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TViewModel">The type of the view model.</typeparam>
    public class BaseModel<TEntity, TViewModel>
          where TEntity : esEntity, new()
         where TViewModel : EntityViewModel<TEntity>
    {

        #region Properties
        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public StandardResponse Response { get; set; }
        #endregion

        /// <summary>
        /// Gets or sets the view model list.
        /// </summary>
        /// <value>
        /// The view model list.
        /// </value>
        protected List<TViewModel> ViewModelList { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseModel{TEntity, TViewModel}"/> class.
        /// </summary>
        public BaseModel()
        {
            Response = new StandardResponse { CodigoErro = 0 };
        }

        /// <summary>
        /// Loads the view model list.
        /// </summary>
        /// <param name="list">The list.</param>
        protected void LoadViewModelList(esEntityCollection list)
        {
            ViewModelList = new List<TViewModel>();

            foreach (var item in list)
            {
                TEntity newItem = item as TEntity;
                TViewModel viewModel = BaseViewModel.CreateViewModel<TViewModel, TEntity>(newItem);
                ViewModelList.Add(viewModel);
            }
        }

        /// <summary>
        /// Validars the cotista.
        /// </summary>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        protected string ValidarCotista(int codigoCotista, int idUsuario)
        {
            var cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(codigoCotista))
            {
                return String.Format("Codigo de cotista: {0}, nao encontrado.", codigoCotista);
            }

            var cot = new CotistaQuery("cot");
            var perCotistaQuery = new PermissaoCotistaQuery("permissao");

            cot.Select(cot);
            cot.InnerJoin(perCotistaQuery).On(cot.IdCotista.Equal(perCotistaQuery.IdCotista));
            cot.Where(perCotistaQuery.IdCotista.Equal(codigoCotista));
            cot.Where(perCotistaQuery.IdUsuario.Equal(idUsuario));

            CotistaCollection cotistas = new CotistaCollection();
            if (!cotistas.Load(cot))
            {
                return String.Format("Usuario: {0} nao possui permissao de acesso ao cotista codigo: {1}", idUsuario, codigoCotista);
            }

            return null;
        }
    }
}