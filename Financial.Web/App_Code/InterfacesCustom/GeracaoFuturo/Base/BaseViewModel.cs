﻿using EntitySpaces.Core;
using System;
using System.Reflection;

namespace Financial.InterfacesCustom.GeracaoFuturo.Base
{
    /// <summary>
    /// Summary description for BaseViewModel
    /// </summary>
    public abstract class BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseViewModel"/> class.
        /// </summary>
        public BaseViewModel()
        {
        } 
        #endregion

        #region Factory Members

        /// <summary>
        /// Creates the view model.
        /// </summary>
        /// <typeparam name="TViewModel">The type of the view model.</typeparam>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="modelEntity">The model entity.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Unable to find suitable constructor for type:  + viewmodelType.FullName</exception>
        public static TViewModel CreateViewModel<TViewModel, TEntity>(TEntity modelEntity)
        {
            Type viewmodelType = typeof(TViewModel);
            Type entityType = typeof(TEntity);

            ConstructorInfo ctor = viewmodelType.GetConstructor(new Type[] { entityType });

            if (ctor == null)
                throw new Exception("Unable to find suitable constructor for type: " + viewmodelType.FullName);

            Object viewmodel = ctor.Invoke(new Object[] { modelEntity });

            return (TViewModel)viewmodel;
        }

        #endregion
    }
}