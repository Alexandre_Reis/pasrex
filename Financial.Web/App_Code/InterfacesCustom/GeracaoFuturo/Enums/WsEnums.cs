﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Financial.InterfacesCustom.GeracaoFuturo.Enums
{
    #region Utility enum class
    /// <summary>
    /// Utility enum class
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Gets the enum description.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static String GetEnumDescription(Enum e)
        {
            FieldInfo fieldInfo = e.GetType().GetField(e.ToString());

            DescriptionAttribute[] enumAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (enumAttributes.Length > 0)
            {
                return enumAttributes[0].Description;
            }
            return e.ToString();
        }

        /// <summary>
        /// Gets the enum string value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="enumType">Type of the enum.</param>
        /// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">enumType should be a valid enum</exception>
        public static object GetEnumDescriptionValue(string value, Type enumType, bool ignoreCase)
        {
            object result = null;
            string enumStringValue = null;
            if (!enumType.IsEnum)
                throw new ArgumentException("enumType should be a valid enum");

            foreach (FieldInfo fieldInfo in enumType.GetFields())
            {
                //var attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false)  as StringValueAttribute[];
                var attribs = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                //Get the StringValueAttribute for each enum member 
                if (attribs.Length > 0)
                    enumStringValue = attribs[0].Description;

                if (string.Compare(enumStringValue, value, ignoreCase) == 0)
                    result = Enum.Parse(enumType, fieldInfo.Name);
            }
            return result;
        }
    }
    #endregion

    #region enums

    /// <summary>
    /// OpcaoPersistirCotista
    /// </summary>
    public enum OpcaoPersistirCotista
    {
        Inclusao,
        Alteracao
    }

    /// <summary>
    /// OpcaoPersistirMovimentoCotista
    /// </summary>
    public enum OpcaoPersistirMovimentoCotista
    {
        Inclusao,
        Alteracao
    }

    /// <summary>
    /// OrigemTipoCotista
    /// </summary>
    public enum OrigemTipoCotista
    {
        CVM,
        Anbima
    }

    /// <summary>
    /// TipoAgenteMercado
    /// </summary>
    public enum TipoAgenteMercado
    {
        Corretora,
        Administrador,
        Gestor,
        Liquidante,
        Custodiante,
        Distribuidor,
        Securitizada
    }

    /// <summary>
    /// PessoVinculada
    /// </summary>
    public enum PessoVinculada
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }

    /// <summary>
    /// FuncaoCorretora
    /// </summary>
    public enum FuncaoCorretora
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// FuncaoAdministrador
    /// </summary>
    public enum FuncaoAdministrador
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// FuncaoGestor
    /// </summary>
    public enum FuncaoGestor
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// FuncaoLiquidante
    /// </summary>
    public enum FuncaoLiquidante
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// FuncaoCustodiante
    /// </summary>
    public enum FuncaoCustodiante
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// FuncaoDistribuidor
    /// </summary>
    public enum FuncaoDistribuidor
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }


    /// <summary>
    /// CriterioResgate
    /// </summary>
    public enum CriterioResgate
    {
        [Description("Especifico")]
        Especifico = 1,
        [Description("FIFO")]
        FIFO = 2,
        [Description("LIFO")]
        LIFO,
        [Description("MenorImposto")]
        MenorImposto
    }

    /// <summary>
    /// ContaDefault
    /// </summary>
    public enum ContaDefault
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// ContaDefaultCliente
    /// </summary>
    public enum ContaDefaultCliente
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// ContaDefaultCotista
    /// </summary>
    public enum ContaDefaultCotista
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    /// <summary>
    /// TipoCalculoCota
    /// </summary>
    public enum TipoCalculoCota
    {
        [Description("A")]
        Abertura,
        [Description("F")]
        Fechamento,
    }

    public enum TipoPosicao
    {
        [Description("A")]
        Abertura,
        [Description("F")]
        Fechamento,
    }

    /// <summary>
    /// Liberada
    /// </summary>
    public enum Liberada
    {
        [Description("S")]
        Sim,
        [Description("N")]
        Nao,
    }
    public enum IrAutomatico
    {
        [Description("S")]
        Sim,
        [Description("N")]
        Nao,
    }

    public enum FundoFmpFgts
    {
        [Description("S")]
        Sim,
        [Description("N")]
        Nao,
    }

    /// <summary>
    /// SimOuNao
    /// </summary>
    public enum SimOuNao
    {
        [Description("S")]
        Sim,
        [Description("N")]
        Nao,
    }

    public enum PoliticamenteExposta
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    public enum IsentoIof
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    public enum Dispensado
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    public enum Recusa
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    public enum IsentoIr
    {
        [Description("S")]
        Sim = 1,
        [Description("N")]
        Nao = 2
    }
    public enum Sexo
    {
        [Description("M")]
        Masculino,
        [Description("F")]
        Feminino
    }

    /// <summary>
    /// RecebeCorrespondencia
    /// </summary>
    public enum RecebeCorrespondencia
    {
        [Description("S")]
        Sim,
        [Description("N")]
        Nao,
    }

    /// <summary>
    /// TipoEndereco
    /// </summary>
    public enum TipoEndereco
    {
        [Description("C")]
        Comercial,
        [Description("R")]
        Residencial
    }

    /// <summary>
    /// Paises
    /// </summary>
    public enum Pais
    {
        AFEGANISTAO = 13,
        ANTILHAS_HOLANDESAS = 47,
        ARGENTINA = 63,
        AUSTRIA = 72,
        BURUNDI = 31,
        BELGICA = 87,
        BENIN = 229,
        BANGLADESH = 81,
        BAHREIN_ILHAS = 80,
        BELIZE = 88,
        BERMUDAS = 90,
        BARBADOS = 83,
        BOTSUANA = 101,
        CHILE = 158,
        CHINA_REPUBLICA_POPULAR = 160,
        UCRANIA = 831,
        COSTA_DO_MARFIM = 193,
        CAMAROES = 145,
        CONGO = 177,
        COLOMBIA = 169,
        SAARA_OCIDENTAL = 685,
        CUBA = 199,
        CHIPRE = 163,
        DINAMARCA = 232,
        DOMINICA_ILHA = 235,
        REPUBLICA_DOMINICANA = 647,
        ARGELIA = 59,
        EQUADOR = 239,
        COSTA_RICA = 196,
        EGITO = 240,
        DJIBOUTI = 783,
        ESPANHA = 245,
        ETIOPIA = 253,
        EMIRADOS_ARABES_UNIDOS = 244,
        FIDJI = 870,
        FRANCA = 275,
        GANA = 289,
        GAMBIA = 285,
        GRECIA = 301,
        GUATEMALA = 317,
        GUIANA = 337,
        HONG_KONG = 351,
        FORMOSA_TAIWAN = 161,
        BURKINA_FASO = 115,
        CANARIAS_ILHAS = 151,
        GEORGIA_REPUBLICA_DA = 291,
        GRANADA = 297,
        INDONESIA = 365,
        INDIA = 361,
        IRA_REPUBLICA_ISLAMICA_DO = 372,
        IRLANDA = 375,
        SAMOA_OCIDENTAL = 690,
        ISLANDIA = 379,
        ITALIA = 386,
        ESTONIA_REPUBLICA_DA = 251,
        VIRGENS_AMERICANAS_ILHAS = 866,
        JAMAICA = 391,
        JAPAO = 399,
        FINLANDIA = 271,
        COREIA_REPUBLICA_DA = 190,
        LAOS_REPUBLICA_POPULAR_DEMOCRATICA = 420,
        LIBERIA = 434,
        LIBIA = 438,
        LITUANIA_REPUBLICA_DA = 442,
        SRI_LANKA = 750,
        MACAU = 447,
        MARROCOS = 474,
        MADAGASCAR = 450,
        MALTA = 467,
        MONACO = 495,
        MAURICIO = 485,
        MALAVI = 458,
        NAURU = 508,
        NIGER = 525,
        NICARAGUA = 521,
        GROELANDIA = 305,
        NORUEGA = 538,
        PANAMA_ = 580,
        PARAGUAI = 586,
        PERU = 589,
        FILIPINAS = 267,
        POLONIA_REPUBLICA_DA = 603,
        PORTUGAL = 607,
        GABAO = 281,
        REUNIAO_ILHA = 660,
        RUANDA = 675,
        ARABIA_SAUDITA = 53,
        BELARUS_REPUBLICA_DA = 85,
        SENEGAL = 728,
        CINGAPURA = 741,
        SIRIA_REPUBLICA_ARABE_DA = 744,
        SERRA_LEOA = 735,
        EL_SALVADOR = 687,
        SAN_MARINO = 697,
        SANTA_LUCIA = 715,
        SUECIA = 764,
        TOGO = 800,
        TAILANDIA = 776,
        TONGA = 810,
        TRINIDAD_E_TOBAGO = 815,
        TURQUIA = 827,
        TANZANIA_REPUBLICA_UNIDA_DA = 780,
        URUGUAI = 845,
        ESTADOS_UNIDOS = 249,
        VENEZUELA = 850,
        VIRGENS_BRITANICAS_ILHAS = 863,
        ZAMBIA = 890,
        ALEMANHA = 23,
        AUSTRALIA = 69,
        BAHAMAS_ILHAS = 77,
        BOLIVIA = 97,
        BRASIL = 105,
        CANADA = 149,
        GIBRALTAR = 293,
        GUADALUPE = 309,
        HAITI = 341,
        HUNGRIA_REPUBLICA_DA = 355,
        INGLATERRA = 367,
        IRAQUE = 369,
        ISRAEL = 383,
        IUGOSLAVIA_REPUBLICA_FEDERATIVA = 388,
        JORDANIA = 403,
        LIBANO = 431,
        LIECHTENSTEIN = 440,
        LUXEMBURGO = 445,
        MALASIA = 455,
        MALI = 464,
        MARTINICA = 477,
        MEXICO = 493,
        NEPAL = 517,
        NIUE_ILHA = 531,
        NOVA_ZELANDIA = 548,
        PAQUISTAO = 576,
        PORTO_RICO = 611,
        QUENIA = 623,
        ROMENIA = 670,
        RUSSIA_FEDERACAO_DA = 676,
        SUDAO = 759,
        SUICA = 767,
        TCHECA_REPUBLICA = 791,
        TUNISIA = 820,
        UGANDA = 833,
        VANUATUPACIFIC = 551,
        ZAIRE = 888,
        ZIMBABUE = 665,
        REPUBLICA_CENTRO_AFRICANA = 640,
        AFRICA_DO_SUL = 756,
        ALBANIA_REPUBLICA_DA = 17,
        ANDORRA = 37,
        ANGOLA = 40,
        ANGUILLA = 41,
        ANTIGUA_BARBUDA = 43,
        ARMENIA_REPUBLICA_DA = 64,
        ARUBA = 65,
        ARZEBAIJAO_REPUBLICA_DO = 73,
        BOSNIA_HERZEGOVINIA = 98,
        BRUNEI = 108,
        BULGARIA_REPUBLICA_DA = 111,
        BUTAO = 119,
        CABO_VERDE_REPUBLICA_DE = 127,
        CAMBOJA = 141,
        CASAQUISTAO_REPUBLICA_DO = 153,
        CATAR = 154,
        CAYMAN_ILHAS = 137,
        CHADE = 788,
        CHRISTMAS_ILHAS = 511,
        COCOS_KEELING_ILHAS = 165,
        COMORES_ILHAS = 173,
        COOK_ILHAS = 183,
        COREIA_REPUBLICA_POPULAR_DEMOCRATICA = 187,
        COVEITE = 198,
        CROACIA_REPUBLICA_DA = 195,
        DUBAI = 237,
        ESLOVACA_REPUBLICA = 247,
        ESLOVENIA_REPUBLICA = 246,
        FALKLAND_MALVINAS = 255,
        FEROE_ILHAS = 259,
        FEZZAN = 263,
        GUAM = 313,
        GUIANA_FRANCESA = 325,
        GUINE = 329,
        GUINE_BISSAU = 334,
        GUINE_EQUATORIAL = 331,
        HONDURAS = 345,
        IEMEN = 357,
        JOHNSTON_ILHAS = 396,
        KIRIBATI = 411,
        LEBUAN_ILHAS = 423,
        LESOTO = 426,
        LETONIA_REPUBLICA_DA = 427,
        MACEDONIA = 449,
        MALDIVAS = 461,
        MARIANAS_DO_NORTE = 472,
        MARSHALL_ILHAS = 476,
        MAURITANIA_ = 488,
        MIANMAR_BIRMANIA = 93,
        MICRONESIA = 499,
        MIDWAY_ILHAS = 490,
        MOCAMBIQUE = 505,
        MOLDOVA_REPUBLICA_DA = 494,
        MONGOLIA = 497,
        MONTSERRAT_ILHAS = 501,
        NAMIBIA = 507,
        NORFOLK_ILHA = 535,
        NIGERIA = 528,
        NOVA_CALEDONIA = 542,
        OMA = 556,
        PACIFICO_ADM_EUA_ILHAS_DO = 563,
        PACIFICO_POSSE_EUA_ILHAS_DO = 566,
        PAISES_BAIXOS_HOLANDA = 573,
        PALAU = 575,
        PAPUA_NOVA_GUINE = 545,
        PITCAIM_ILHA_DE = 593,
        POLINESIA_FRANCESA = 599,
        QUIRGUIZ_REPUBLICA_DA = 625,
        REINO_UNIDO = 628,
        SALOMAO_ILHAS = 677,
        SAMOA_AMERICANA = 691,
        SANTA_HELENA_ = 710,
        SAO_CRISTOVAO_E_NEVES_ILHAS = 695,
        SAO_PEDRO_E_MIQUELON = 700,
        SAO_TOME_E_PRINCIPE_ILHAS = 720,
        SAO_VICENTE_E_GRANADINAS = 705,
        SEYCHELLES = 731,
        SOMALIA = 748,
        SUAZILANDIA = 754,
        SURINAME = 770,
        TADJIQUISTAO = 772,
        TERRITORIO_BRITANICO_OCEANO_INDICO = 782,
        TIMOR_LESTE = 795,
        TOQUELAU_ILHAS = 805,
        TURCAS_E_CAICOS = 823,
        TURCOMENISTAO_REPUBLICA_DO = 824,
        TUVALU = 828,
        UZBEQUISTAO_REPUBLICA_DO = 847,
        VATICANO_ESTADO_DA_CIDADE_DO = 848,
        VIETNA = 858,
        WALLIS_E_FUTUNA_ILHAS = 875,

    }
    #endregion
}