﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace Financial.InterfacesCustom.GeracaoFuturo.Xml
{
    /// <summary>
    /// The MS generic lists can not be serialized/deserialized by the XmlSerializer.
    /// This list can because it implements IXmlSerializable.
    /// The XmlSerializer is alternate serializer for WCF and other network
    /// client frameworks. So why use XmlSerializer and not DataContractSerializer?
    /// The DCS is slightly faster (10%) than the XMLS because it is less flexible 
    /// and works off WCF specific assumptions, which complicates using non-WCF
    /// services. Why MS, why?
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class XmlSerializableList<TItem> : IList<TItem>, IXmlSerializable
    {
        #region Construction

        public XmlSerializableList()
        {
            _internalList = new List<TItem>();
        }

        #endregion

        #region Fields

        private List<TItem> _internalList = new List<TItem>();

        #endregion

        #region IXmlSerializable

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            // ensure the internal list exists
            if (_internalList == null)
                _internalList = new List<TItem>();

            // use an XmlSerializer
            XmlSerializer serializer = new XmlSerializer(typeof(TItem));

            String typeName = typeof(TItem).FullName;

            // read the first element
            reader.Read();

            // process all the items
            while ((reader.NodeType != XmlNodeType.EndElement) && (reader.NodeType != XmlNodeType.None))
            {
                TItem item = (TItem)serializer.Deserialize(reader);

                if (item != null)
                    _internalList.Add(item);
            }

            // ensure the end of the stream is reached
            if (reader.NodeType == XmlNodeType.EndElement)
            {
                try
                {
                    reader.ReadEndElement();
                }
                catch (Exception ex)
                {

                }

            }
        }
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            // ensure the internal list contains items to write
            if ((_internalList != null) && (_internalList.Count > 0))
            {
                // use an XmlSerializer
                XmlSerializer serializer = new XmlSerializer(typeof(TItem));

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", ""); // declare an empty namespace

                // write each item in the list
                foreach (TItem item in _internalList)
                {
                    serializer.Serialize(writer, item, ns);
                }
            }
        }

        #endregion

        #region IList<T> Members

        public int IndexOf(TItem item)
        {
            return _internalList.IndexOf(item);
        }
        public void Insert(int index, TItem item)
        {
            _internalList.Insert(index, item);
        }
        public void RemoveAt(int index)
        {
            _internalList.RemoveAt(index);
        }
        public TItem this[int index]
        {
            get { return _internalList[index]; }
            set { _internalList[index] = value; }
        }
        public void Add(TItem item)
        {
            _internalList.Add(item);
        }
        public void Clear()
        {
            _internalList.Clear();
        }
        public bool Contains(TItem item)
        {
            return _internalList.Contains(item);
        }
        public void CopyTo(TItem[] array, int arrayIndex)
        {
            _internalList.CopyTo(array, arrayIndex);
        }
        public int Count
        {
            get { return _internalList.Count; }
        }
        public bool IsReadOnly
        {
            get { return false; }
        }
        public bool Remove(TItem item)
        {
            return _internalList.Remove(item);
        }
        public IEnumerator<TItem> GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }

        #endregion
    }
}
