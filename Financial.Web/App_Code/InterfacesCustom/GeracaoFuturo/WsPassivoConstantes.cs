﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WsPassivoConstantes
/// </summary>
public class WsPassivoConstantes
{
    public const string WsPassivo = "WsPassivo";
    public const string CotistaConsultar = "ConsCotista";
    public const string CotistaConsultarPosicaoOnLine = "ConsOnlinePosicao";
    public const string CotistaConsultarPosicaoOffLine = "ConsOfflinePosicao";
    public const string PASSIVO_CONSULTAADMINISTRADOR = "ConsAdministrador";
    public const string PASSIVO_CONSULTACADASTRO = "ConsCadastro";
    public const string PASSIVO_CONSULTACUSTODIANTE = "ConsCustodiante";
    public const string PASSIVO_CONSULTADISTRIBUIDOR = "ConsDistribuidor";
    public const string PASSIVO_CONSULTAEMPRESA = "ConsEmpresa";
    public const string PASSIVO_CONSULTAGESTOR = "ConsGestor";
    public const string PASSIVO_CONSULTAOPERADOR = "ConsOperador";
    public const string PASSIVO_CONSULTACLEARING = "ConsClearing";
    public const string PASSIVO_CONSULTACONTAEXTERNA = "ConsContaExterna";
    public const string PASSIVO_CONSULTACOTISTA = "ConsPassivoCotista";
    public const string PASSIVO_CONSULTACRITERIOAGENDAMENTO = "ConsAgendamentoCriterio";
    public const string PASSIVO_CONSULTACRITERIORESGATE = "ConsCriterioResgate";
    public const string PASSIVO_CONSULTADATAPOSICAOFUNDO = "ConsDataPosicaoFundo";
    public const string PASSIVO_CONSULTARESGATE = "ConsResgate";
    public const string PASSIVO_CONSULTARMOVIMENTORESGATES = "ConsMovResg";
    public const string PASSIVO_CONSULTAFORMALIQUIDACAO = "ConsFormaLiquidacao";
    public const string PASSIVO_CONSULTAFUNDOS = "ConsFundos";
    public const string PASSIVO_CONSULTATIPOLIQUIDACAO = "ConsLiquidacaoTipo";
    public const string PASSIVO_CONSULTATIPOMOVIMENTO = "ConsTipoMovimento";
    public const string PASSIVO_INSERIRMOVIMENTO = "InserirMovimento";
    public const string PASSIVO_DELETARMOVIMENTO = "DeletarMovimento";
    public const string PASSIVO_PERSISTIRCLIENTE = "ClientePersistir";
    public const string PASSIVO_PERSISTIRCOTISTA = "CotistaPersistir";
    public const string PASSIVO_PERSISTIRENDERECO = "EnderecoPersistir";
    public const string CreditoFiscal = "CreditoFiscal";
    public const string BuscaNota = "LocalizaNota";

    public const string PassivoConsultarMovimento = "ConcMovimentos";
    public const string PassivoConsistirMovimento = "ConstitirMovimentos";
    public const string ObterTipoCotista = "ObtemTipoCotista";
}