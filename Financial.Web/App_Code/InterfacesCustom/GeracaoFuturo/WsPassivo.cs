﻿using Financial.InterfacesCustom.GeracaoFuturo.Enums;
using Financial.InterfacesCustom.GeracaoFuturo.Model;
using Financial.Security;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Web.Services;
using System.Web.Services.Protocols;

[assembly: log4net.Config.XmlConfiguratorAttribute(Watch = true)]
namespace Financial.InterfacesCustom.GeracaoFuturo
{
    /// <summary>
    /// Summary description for WsPassivo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class WsPassivo : WebService
    {
        #region Fields
        private static readonly WsLogger Logger = new WsLogger();
        #endregion

        #region Properties
        public ValidateLogin Authentication { get; set; }
        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="WsPassivo"/> class.
        /// </summary>
        public WsPassivo()
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += (sender, args) =>
            {
                var e = (Exception)args.ExceptionObject;
                Logger.Error("Unhandled exception", e);
            };
        }
        #endregion

        #region WebMethods
        #region Passivo_ConsultaCadastro
        /// <summary>
        /// Passivo_s the consulta cadastro.
        /// </summary>
        /// <param name="codigoCotista">The codigo cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CadastroCotista Passivo_ConsultaCadastro(int codigoCotista, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), codigoCotista, idUsuario);

            try
            {
                Authenticate();
                var cadastroCotista = new CadastroCotista(codigoCotista, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return cadastroCotista;
            }
            catch (InvalidShareholderException ex)
            {
                return new CadastroCotista { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new CadastroCotista { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CadastroCotista { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CadastroCotista { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarCotista
        /// <summary>
        /// Passivo_s the consultar cotista.
        /// </summary>
        /// <param name="nome">The nome.</param>
        /// <param name="nomeAbreviado">The nome abreviado.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CadastroCotistaList Passivo_ConsultarCotista(string nome, string nomeAbreviado, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), nome, nomeAbreviado, idUsuario);

            try
            {
                Authenticate();
                var cadastrosCotista = new CadastrosCotista(nome, nomeAbreviado, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return cadastrosCotista;
            }
            catch (InvalidShareholderException ex)
            {
                return new CadastroCotistaList { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new CadastroCotistaList { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CadastroCotistaList { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CadastroCotistaList { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region ConsultarPosicaoOffLine
        /// <summary>
        /// Consultars the posicao off line.
        /// </summary>
        /// <param name="dataPosicao">The data posicao.</param>
        /// <param name="cotista">The cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public PosicaoCotistaModel ConsultarPosicaoOffLine(DateTime dataPosicao, int cotista, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), dataPosicao, cotista);

            try
            {
                Authenticate();
                var posicaoCotista = new PosicaoCotistaModel(dataPosicao, cotista, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return posicaoCotista;
            }
            catch (InvalidShareholderException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region ConsultarPosicaoOnLine
        /// <summary>
        /// Consultars the posicao on line.
        /// </summary>
        /// <param name="dataPosicao">The data posicao.</param>
        /// <param name="cotista">The cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public PosicaoCotistaModel ConsultarPosicaoOnLine(DateTime dataPosicao, int cotista, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), dataPosicao, cotista, idUsuario);

            try
            {
                Authenticate();
                var posicaoCotista = new PosicaoCotistaModel(dataPosicao, cotista, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return posicaoCotista;
            }
            catch (InvalidShareholderException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new PosicaoCotistaModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultaMovimentoResgates
        /// <summary>
        /// Passivo_s the consulta movimento resgates.
        /// </summary>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idFundo">The identifier fundo.</param>
        /// <param name="dtResgate">The dt resgate.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CotistaMovimentoResgateModel Passivo_ConsultaMovimentoResgates(int? idCotista, int? idFundo, DateTime dtResgate, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), idCotista, idFundo, dtResgate, idUsuario);

            try
            {
                Authenticate();
                var resgates = new CotistaMovimentoResgateModel(dtResgate, idCotista, idFundo, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return resgates;
            }
            catch (InvalidShareholderException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultaResgate
        /// <summary>
        /// Passivo_s the consulta resgate.
        /// </summary>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idFundo">The identifier fundo.</param>
        /// <param name="dtLiquidacao">The dt liquidacao.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CotistaMovimentoResgateModel Passivo_ConsultaResgate(int? idCotista, int? idFundo, DateTime dtLiquidacao, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), idCotista, idFundo, dtLiquidacao, idUsuario);

            try
            {
                Authenticate();
                var resgates = new CotistaMovimentoResgateModel(idCotista, idFundo, idUsuario, dtLiquidacao);
                Logger.EndLog(GetCurrentMethod());

                return resgates;
            }
            catch (InvalidShareholderException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CotistaMovimentoResgateModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarClearing
        /// <summary>
        /// Passivo_s the consultar clearing.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public ClearingModel Passivo_ConsultarClearing()
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var clearings = new ClearingModel();
                Logger.EndLog(GetCurrentMethod());

                return clearings;
            }
            catch (InvalidCredentialException ex)
            {
                return new ClearingModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new ClearingModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new ClearingModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarContaExterna
        /// <summary>
        /// Passivo_s the consultar conta externa.
        /// </summary>
        /// <param name="idCotista">The identifier cotista.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public ContaCorrenteModel Passivo_ConsultarContaExterna(int? idCotista, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var contas = new ContaCorrenteModel(idCotista, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return contas;
            }
            catch (InvalidShareholderException ex)
            {
                return new ContaCorrenteModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new ContaCorrenteModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new ContaCorrenteModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new ContaCorrenteModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarFormaLiquidacao
        /// <summary>
        /// Passivo_s the consultar forma liquidacao.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public FormaLiquidacaoModel Passivo_ConsultarFormaLiquidacao()
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var formaLiq = new FormaLiquidacaoModel();
                Logger.EndLog(GetCurrentMethod());

                return formaLiq;
            }
            catch (InvalidCredentialException ex)
            {
                return new FormaLiquidacaoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new FormaLiquidacaoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new FormaLiquidacaoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarDataPosicaoFundo
        /// <summary>
        /// Passivo_s the consultar data posicao fundo.
        /// </summary>
        /// <param name="idFundo">The identifier fundo.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public DataPosicaoFundoModel Passivo_ConsultarDataPosicaoFundo(int? idFundo)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var dataPosicao = new DataPosicaoFundoModel(idFundo);
                Logger.EndLog(GetCurrentMethod());

                return dataPosicao;
            }
            catch (InvalidFundException ex)
            {
                return new DataPosicaoFundoModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new DataPosicaoFundoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new DataPosicaoFundoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new DataPosicaoFundoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarFundos
        /// <summary>
        /// Passivo_s the consultar fundos.
        /// </summary>
        /// <param name="idFundo">The identifier fundo.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public ConsultaFundoModel Passivo_ConsultarFundos(int? idFundo)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var fundo = new ConsultaFundoModel(idFundo);
                Logger.EndLog(GetCurrentMethod());

                return fundo;
            }
            catch (InvalidFundException ex)
            {
                return new ConsultaFundoModel { Response = new StandardResponse { CodigoErro = 1, MsgErro = ex.Message } };
            }
            catch (InvalidCredentialException ex)
            {
                return new ConsultaFundoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new ConsultaFundoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new ConsultaFundoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarMovimentos
        /// <summary>
        /// Passivo_s the consultar movimentos.
        /// </summary>
        /// <param name="dataMovimento">The data movimento.</param>
        /// <param name="hora">The hora.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CotistaMovimentoModel Passivo_ConsultarMovimentos(DateTime dataMovimento, string hora, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), dataMovimento, hora, idUsuario);

            try
            {
                Authenticate();
                var movimentos = new CotistaMovimentoModel(dataMovimento, hora, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return movimentos;
            }
            catch (InvalidCredentialException ex)
            {
                return new CotistaMovimentoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CotistaMovimentoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CotistaMovimentoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_MovimentosConsistidos
        /// <summary>
        /// Passivo_s the movimentos consistidos.
        /// </summary>
        /// <param name="dataMovimento">The data movimento.</param>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CotistaMovimentoConsistidoModel Passivo_MovimentosConsistidos(DateTime dataMovimento, int idUsuario)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), dataMovimento, idUsuario);

            try
            {
                Authenticate();
                var movimentos = new CotistaMovimentoConsistidoModel(dataMovimento, idUsuario);
                Logger.EndLog(GetCurrentMethod());

                return movimentos;
            }
            catch (InvalidCredentialException ex)
            {
                return new CotistaMovimentoConsistidoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CotistaMovimentoConsistidoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CotistaMovimentoConsistidoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region ConsultarAgenteMercado
        /// <summary>
        /// Consultars the agente mercado.
        /// </summary>
        /// <param name="tipoAgenteMercado">The tipo agente mercado.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public AgenteMercadoModel ConsultarAgenteMercado(TipoAgenteMercado? tipoAgenteMercado)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), tipoAgenteMercado);

            try
            {
                Authenticate();
                var agente = new AgenteMercadoModel(tipoAgenteMercado);
                Logger.EndLog(GetCurrentMethod());

                return agente;
            }
            catch (InvalidCredentialException ex)
            {
                return new AgenteMercadoModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new AgenteMercadoModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new AgenteMercadoModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region ObtemTipoCotista
        /// <summary>
        /// Obtems the tipo cotista.
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public TipoCotistaModel ObtemTipoCotista(OrigemTipoCotista? tipo)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), tipo);

            try
            {
                Authenticate();
                var tipoCotista = new TipoCotistaModel(tipo);
                Logger.EndLog(GetCurrentMethod());

                return tipoCotista;
            }
            catch (InvalidCredentialException ex)
            {
                return new TipoCotistaModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new TipoCotistaModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new TipoCotistaModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarTipoMovimento
        /// <summary>
        /// Passivo_s the consultar tipo movimento.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public TipoMovimentoCotistaModel Passivo_ConsultarTipoMovimento()
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var tipoMovCotista = new TipoMovimentoCotistaModel("tipo");
                Logger.EndLog(GetCurrentMethod());

                return tipoMovCotista;
            }
            catch (InvalidCredentialException ex)
            {
                return new TipoMovimentoCotistaModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new TipoMovimentoCotistaModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new TipoMovimentoCotistaModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_ConsultarCriterioResgate
        /// <summary>
        /// Passivo_s the consultar tipo movimento.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public CriterioResgateModel Passivo_ConsultarCriterioResgate()
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod());

            try
            {
                Authenticate();
                var criterioResgate = new CriterioResgateModel("tipo");
                Logger.EndLog(GetCurrentMethod());

                return criterioResgate;
            }
            catch (InvalidCredentialException ex)
            {
                return new CriterioResgateModel { Response = new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } };
            }
            catch (MissingCredentialException ex)
            {
                return new CriterioResgateModel { Response = new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } };
            }
            catch (Exception ex)
            {
                return new CriterioResgateModel { Response = new StandardResponse { CodigoErro = -1, MsgErro = ex.Message } };
            }
        }

        #endregion

        #region Passivo_PersistirCotista
        /// <summary>
        /// Passivo_s the persistir cotista.
        /// </summary>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <param name="cadastroCotista">The cadastro cotista.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public StandardResponse Passivo_PersistirCotista(int idUsuario,  PersistirCadastroCotista cadastroCotista)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), cadastroCotista);

            try
            {
                Authenticate();
                if (cadastroCotista != null)
                    cadastroCotista.PersistirCotista(idUsuario);

                Logger.EndLog(GetCurrentMethod());
                if (cadastroCotista != null) return cadastroCotista.Response;

                return new StandardResponse { CodigoErro = -1, MsgErro = "Nao houve retorno" };
            }
            catch (InvalidCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -2, MsgErro = ex.Message } ;
            }
            catch (MissingCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -3, MsgErro = ex.Message } ;
            }
            catch (PersistirCadastroException ex)
            {
                return cadastroCotista != null ? cadastroCotista.Response : new StandardResponse { CodigoErro = -1, MsgErro = ex.Message };
            }
            catch (Exception ex)
            {
                return new StandardResponse { CodigoErro = -1, MsgErro = ex.Message };
            }
        }

        #endregion

        #region Passivo_InserirMovimento
        /// <summary>
        /// Passivo_s the inserir movimento.
        /// </summary>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <param name="movimentoCotista">The movimento cotista.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public StandardResponse Passivo_InserirMovimento(int idUsuario, PersistirMovimentoCotista movimentoCotista)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), movimentoCotista);

            try
            {
                if(movimentoCotista== null)
                    return new StandardResponse { CodigoErro = -1, MsgErro = "Parametro de entrada invalido" };

                Authenticate();
                movimentoCotista.PersistirMovimento(idUsuario);

                Logger.EndLog(GetCurrentMethod());
                return movimentoCotista.Response;
               
            }
            catch(InvalidShareholderException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidFundException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidWithdrawException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -2, MsgErro = ex.Message };
            }
            catch (MissingCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -3, MsgErro = ex.Message };
            }
           
            catch (Exception ex)
            {
                return new StandardResponse { CodigoErro = -1, MsgErro = ex.Message };
            }
        }

        #endregion

        #region Passivo_DeletarMovimento

        /// <summary>
        /// Passivo_s the deletar movimento.
        /// </summary>
        /// <param name="idUsuario">The identifier usuario.</param>
        /// <param name="movimentoCotista">The movimento cotista.</param>
        /// <returns></returns>
        [WebMethod]
        [SoapHeader("Authentication")]
        public StandardResponse Passivo_DeletarMovimento(int idUsuario, PersistirMovimentoCotista movimentoCotista)
        {
            Logger.BeginLog(GetCurrentMethod(), System.Reflection.MethodBase.GetCurrentMethod(), idUsuario, movimentoCotista);

            if (movimentoCotista == null)
                return new StandardResponse { CodigoErro = -1, MsgErro = "Parametro de entrada invalido" };

            try
            {
                Authenticate();
                var movCotista = new PersistirMovimentoCotista();
                movCotista.ExcluirMovimento(idUsuario);

                Logger.EndLog(GetCurrentMethod());
                return new StandardResponse { CodigoErro = 0, MsgErro = "OK" };

            }
            catch (InvalidShareholderException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidFundException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidWithdrawException ex)
            {
                return new StandardResponse { CodigoErro = movimentoCotista.Response.CodigoErro, MsgErro = ex.Message };
            }
            catch (InvalidCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -2, MsgErro = ex.Message };
            }
            catch (MissingCredentialException ex)
            {
                return new StandardResponse { CodigoErro = -3, MsgErro = ex.Message };
            }

            catch (Exception ex)
            {
                return new StandardResponse { CodigoErro = -1, MsgErro = ex.Message };
            }
        }

        #endregion


        #endregion

        #region Private methods

        /// <summary>
        /// Gets the current method.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        private string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        /// <summary>
        /// Authenticates this instance.
        /// </summary>
        /// <exception cref="Financial.InterfacesCustom.GeracaoFuturo.MissingCredentialException"></exception>
        /// <exception cref="Financial.InterfacesCustom.GeracaoFuturo.InvalidCredentialException"></exception>
        private void Authenticate()
        {
            if (Authentication == null)
            {
                const string msgErro = "Credenciais de autenticação não foram informadas";
                Logger.Error(msgErro, GetCurrentMethod());
                throw new MissingCredentialException(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                const string msgErro = "Usuário/Senha Invalidos";
                Logger.Error(msgErro, GetCurrentMethod());
                throw new InvalidCredentialException(msgErro);
            }
        }

        /// <summary>
        /// Checks the authentication.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        private bool CheckAuthentication(string username, string password)
        {
            var financialMembershipProvider = new FinancialMembershipProvider();
            var listaValores = new NameValueCollection {{"passwordFormat", "Encrypted"}};
            financialMembershipProvider.Initialize(null, listaValores);

            var autenticado = financialMembershipProvider.ValidateUser(username, password);
            return autenticado;
        }
        #endregion

    }

    #region Custom exceptions

    /// <summary>
    /// PersistirCadastroException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class PersistirCadastroException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersistirCadastroException"/> class.
        /// </summary>
        public PersistirCadastroException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistirCadastroException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public PersistirCadastroException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistirCadastroException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public PersistirCadastroException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    /// <summary>
    /// MissingCredentialException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class MissingCredentialException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MissingCredentialException"/> class.
        /// </summary>
        public MissingCredentialException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public MissingCredentialException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public MissingCredentialException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    /// <summary>
    /// InvalidCredentialException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class InvalidCredentialException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidCredentialException"/> class.
        /// </summary>
        public InvalidCredentialException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidCredentialException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidCredentialException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public InvalidCredentialException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    /// <summary>
    /// InvalidShareholderException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class InvalidShareholderException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidShareholderException"/> class.
        /// </summary>
        public InvalidShareholderException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidShareholderException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidShareholderException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidShareholderException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public InvalidShareholderException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    /// <summary>
    /// InvalidFundException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class InvalidFundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidFundException"/> class.
        /// </summary>
        public InvalidFundException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidFundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidFundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidFundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public InvalidFundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    /// <summary>
    /// InvalidWithdrawException
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class InvalidWithdrawException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidWithdrawException"/> class.
        /// </summary>
        public InvalidWithdrawException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidWithdrawException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidWithdrawException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidWithdrawException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public InvalidWithdrawException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    #endregion
}
