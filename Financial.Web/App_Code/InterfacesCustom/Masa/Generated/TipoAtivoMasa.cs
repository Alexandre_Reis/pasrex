/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/04/2014 10:42:55
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesCustom
{

	[Serializable]
	abstract public class esTipoAtivoMasaCollection : esEntityCollection
	{
		public esTipoAtivoMasaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoAtivoMasaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoAtivoMasaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoAtivoMasaQuery);
		}
		#endregion
		
		virtual public TipoAtivoMasa DetachEntity(TipoAtivoMasa entity)
		{
			return base.DetachEntity(entity) as TipoAtivoMasa;
		}
		
		virtual public TipoAtivoMasa AttachEntity(TipoAtivoMasa entity)
		{
			return base.AttachEntity(entity) as TipoAtivoMasa;
		}
		
		virtual public void Combine(TipoAtivoMasaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoAtivoMasa this[int index]
		{
			get
			{
				return base[index] as TipoAtivoMasa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoAtivoMasa);
		}
	}



	[Serializable]
	abstract public class esTipoAtivoMasa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoAtivoMasaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoAtivoMasa()
		{

		}

		public esTipoAtivoMasa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String cdAtivo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTipoAtivoMasaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.CdAtivo == cdAtivo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivo);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivo)
		{
			esTipoAtivoMasaQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivo == cdAtivo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivo)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivo",cdAtivo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdAtivo": this.str.CdAtivo = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoAtivoMasa.CdAtivo
		/// </summary>
		virtual public System.String CdAtivo
		{
			get
			{
				return base.GetSystemString(TipoAtivoMasaMetadata.ColumnNames.CdAtivo);
			}
			
			set
			{
				base.SetSystemString(TipoAtivoMasaMetadata.ColumnNames.CdAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoAtivoMasa.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(TipoAtivoMasaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(TipoAtivoMasaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoAtivoMasa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdAtivo
			{
				get
				{
					System.String data = entity.CdAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivo = null;
					else entity.CdAtivo = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esTipoAtivoMasa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoAtivoMasaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoAtivoMasa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoAtivoMasa : esTipoAtivoMasa
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoAtivoMasaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoAtivoMasaMetadata.Meta();
			}
		}	
		

		public esQueryItem CdAtivo
		{
			get
			{
				return new esQueryItem(this, TipoAtivoMasaMetadata.ColumnNames.CdAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, TipoAtivoMasaMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoAtivoMasaCollection")]
	public partial class TipoAtivoMasaCollection : esTipoAtivoMasaCollection, IEnumerable<TipoAtivoMasa>
	{
		public TipoAtivoMasaCollection()
		{

		}
		
		public static implicit operator List<TipoAtivoMasa>(TipoAtivoMasaCollection coll)
		{
			List<TipoAtivoMasa> list = new List<TipoAtivoMasa>();
			
			foreach (TipoAtivoMasa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoAtivoMasaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoAtivoMasaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoAtivoMasa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoAtivoMasa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoAtivoMasaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoAtivoMasaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoAtivoMasaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoAtivoMasa AddNew()
		{
			TipoAtivoMasa entity = base.AddNewEntity() as TipoAtivoMasa;
			
			return entity;
		}

		public TipoAtivoMasa FindByPrimaryKey(System.String cdAtivo)
		{
			return base.FindByPrimaryKey(cdAtivo) as TipoAtivoMasa;
		}


		#region IEnumerable<TipoAtivoMasa> Members

		IEnumerator<TipoAtivoMasa> IEnumerable<TipoAtivoMasa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoAtivoMasa;
			}
		}

		#endregion
		
		private TipoAtivoMasaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoAtivoMasa' table
	/// </summary>

	[Serializable]
	public partial class TipoAtivoMasa : esTipoAtivoMasa
	{
		public TipoAtivoMasa()
		{

		}
	
		public TipoAtivoMasa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoAtivoMasaMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoAtivoMasaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoAtivoMasaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoAtivoMasaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoAtivoMasaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoAtivoMasaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoAtivoMasaQuery query;
	}



	[Serializable]
	public partial class TipoAtivoMasaQuery : esTipoAtivoMasaQuery
	{
		public TipoAtivoMasaQuery()
		{

		}		
		
		public TipoAtivoMasaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoAtivoMasaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoAtivoMasaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoAtivoMasaMetadata.ColumnNames.CdAtivo, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoAtivoMasaMetadata.PropertyNames.CdAtivo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoAtivoMasaMetadata.ColumnNames.Tipo, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TipoAtivoMasaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoAtivoMasaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdAtivo = "CdAtivo";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdAtivo = "CdAtivo";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoAtivoMasaMetadata))
			{
				if(TipoAtivoMasaMetadata.mapDelegates == null)
				{
					TipoAtivoMasaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoAtivoMasaMetadata.meta == null)
				{
					TipoAtivoMasaMetadata.meta = new TipoAtivoMasaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CdAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "TipoAtivoMasa";
				meta.Destination = "TipoAtivoMasa";
				
				meta.spInsert = "proc_TipoAtivoMasaInsert";				
				meta.spUpdate = "proc_TipoAtivoMasaUpdate";		
				meta.spDelete = "proc_TipoAtivoMasaDelete";
				meta.spLoadAll = "proc_TipoAtivoMasaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoAtivoMasaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoAtivoMasaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
