﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Net;

using Financial.Fundo.Exportacao;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.RendaFixa;
using System.Runtime.Remoting.Contexts;
using Financial.Investidor.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using Financial.Fundo.Enums;
using System.IO;
using System.Xml;
using System.Web.Configuration;
using Financial.CRM;
using Financial.ContaCorrente;
using Financial.Util;

namespace Financial.InterfacesCustom
{
    public class CaixaOnline
    {

        public CaixaOnline() { }

        public CaixaOnline(string fundo, int idCliente, decimal SaldoInicial, decimal aplicacoes,
            decimal resgates, decimal compras, decimal vendas, decimal SaldoAtualizado,
            decimal over, decimal SaldoFinal)
        {

            this.fundo = fundo;
            this.idCliente = idCliente;
            this.SaldoInicial = SaldoInicial;
            this.aplicacoes = aplicacoes;
            this.resgates = resgates;
            this.compras = compras;
            this.vendas = vendas;
            this.SaldoAtualizado = SaldoAtualizado;
            this.over = over;
            this.SaldoFinal = SaldoFinal;
        }

        private static DataTable dataTableCaixaOnLine = new DataTable();

        private string fundo;

        public string Fundo
        {
            get { return fundo; }
            set { fundo = value; }
        }

        private int idCliente;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private decimal saldoInicial;

        public decimal SaldoInicial
        {
            get { return saldoInicial; }
            set { saldoInicial = value; }
        }

        private decimal aplicacoes;

        public decimal Aplicacoes
        {
            get { return aplicacoes; }
            set { aplicacoes = value; }
        }

        private decimal resgates;

        public decimal Resgates
        {
            get { return resgates; }
            set { resgates = value; }
        }

        private decimal compras;

        public decimal Compras
        {
            get { return compras; }
            set { compras = value; }
        }

        private decimal vendas;

        public decimal Vendas
        {
            get { return vendas; }
            set { vendas = value; }
        }

        private decimal saldoAtualizado;

        public decimal SaldoAtualizado
        {
            get { return saldoAtualizado; }
            set { saldoAtualizado = value; }
        }

        private decimal over;

        public decimal Over
        {
            get { return over; }
            set { over = value; }
        }

        private decimal saldoFinal;

        public decimal SaldoFinal
        {
            get { return saldoFinal; }
            set { saldoFinal = value; }
        }

        public BindingList<CaixaOnline> CarregaGridCaixaOnline()
        {
            BindingList<CaixaOnline> listaCaixaOnline = new BindingList<CaixaOnline>();

            for (int c = 0; c < dataTableCaixaOnLine.Rows.Count; c++)
            {
                CaixaOnline caixaOnline = new CaixaOnline();
                caixaOnline.Fundo = dataTableCaixaOnLine.Rows[c]["Fundo"].ToString();
                caixaOnline.IdCliente = Convert.ToInt32(dataTableCaixaOnLine.Rows[c]["IdCliente"]);
                caixaOnline.Aplicacoes = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["Aplicacoes"]);
                caixaOnline.Compras = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["Compras"]);
                caixaOnline.Over = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["Over"]);
                caixaOnline.Resgates = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["Resgates"]);
                caixaOnline.SaldoAtualizado = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["SaldoAtualizado"]);
                caixaOnline.SaldoFinal = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["SaldoFinal"]);
                caixaOnline.SaldoInicial = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["SaldoInicial"]);
                caixaOnline.Vendas = Convert.ToDecimal(dataTableCaixaOnLine.Rows[c]["Vendas"]);

                listaCaixaOnline.Add(caixaOnline);
            }

            return listaCaixaOnline;
        }


        #region Classes caixa on line itau
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class CAIXAONLINESALDO
        {

            /// <remarks/>
            public MENSAGEM MENSAGEM;

        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class MENSAGEM
        {

            /// <remarks/>
            public HEADER HEADER;

            /// <remarks/>
            public INFORMACOES INFORMACOES;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class HEADER
        {

            /// <remarks/>
            public EMISSOR EMISSOR;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class EMISSOR
        {

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string login;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string senha;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public EMISSORTipovalidacao tipovalidacao;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public enum EMISSORTipovalidacao
        {

            /// <remarks/>
            G,

            /// <remarks/>
            E,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class INFORMACOES
        {

            /// <remarks/>
            public REGISTRO REGISTRO;
            public ERRO ERRO;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CAIXAONLINESALDOFUNDO
        {

            /// <remarks/>
            public string SIGLA_FUNDO;

            /// <remarks/>
            public string NOME_FUNDO;

            /// <remarks/>
            public string SALDO;

            /// <remarks/>
            public string DISPONIVEL;

            /// <remarks/>
            public string TOTAL;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class REGISTRO
        {

            /// <remarks/>
            public string GRUPO;

            /// <remarks/>
            public TPSOLIC TPSOLIC;

            /// <remarks/>
            public string IP;

            /// <remarks/>
            public string MAQUINA;
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public enum TPSOLIC
        {

            /// <remarks/>
            SN,

            /// <remarks/>
            RV,

            /// <remarks/>
            TS,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class ERRO
        {

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string codigo;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string descricao;

        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //[System.Xml.Serialization.XmlRootAttribute( Namespace = "", IsNullable = false)]
        [XmlRoot("CAIXAONLINESALDO")]
        public partial class CAIXAONLINESALDORETORNO
        {
            /// <remarks/>
            public string DATA;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
            public System.DateTime HORA;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("FUNDO", IsNullable = false)]
            public CAIXAONLINESALDOFUNDO[] DADOS;
        }

        public void LimparDataTableCaixaOnline()
        {
            dataTableCaixaOnLine.Rows.Clear();
        }

        public void CriarDataTableCaixaOnLine()
        {
            dataTableCaixaOnLine = new DataTable();

            dataTableCaixaOnLine.Columns.Add(new DataColumn("Fundo", typeof(string)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("IdCliente", typeof(int)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("SaldoInicial", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("Aplicacoes", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("Resgates", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("Compras", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("Vendas", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("SaldoAtualizado", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("Over", typeof(Decimal)));
            dataTableCaixaOnLine.Columns.Add(new DataColumn("SaldoFinal", typeof(Decimal)));

        }

        public void PopulaDataTableCaixaOnLine(DateTime data)
        {
            Usuario usuario = new Usuario();
            usuario.BuscaUsuario( HttpContext.Current.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            PessoaQuery pessoaQuery = new PessoaQuery("M");

            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            CarteiraCollection carteiraCollection = new CarteiraCollection();

            carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, pessoaQuery.CodigoInterface);
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(pessoaQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

            carteiraCollection.Load(carteiraQuery);

            carteiraQuery = new CarteiraQuery("C");
            clienteQuery = new ClienteQuery("L");
            pessoaQuery = new PessoaQuery("M");

            carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, pessoaQuery.CodigoInterface);
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(pessoaQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

            CarteiraCollection coll2 = new CarteiraCollection();
            coll2.Load(carteiraQuery);

            carteiraCollection.Combine(coll2);

            // chama web service itau p obter Saldos iniciais

            // CAIXAONLINESALDORETORNO SaldosIniciais =  PopulaSaldoInicialCaixaOnLine(data);            
            
            // buscar Saldos web service itau
            //if (SaldosIniciais != null)
            //{
                foreach (Carteira carteira in carteiraCollection)
                {
                    SaldoCaixa saldoCaixa = new SaldoCaixa();
                    saldoCaixa.BuscaSaldoCaixa(carteira.IdCarteira.Value, data);

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
                    liquidacao.Query.Where(liquidacao.Query.DataLancamento.LessThan(data),
                                           liquidacao.Query.DataVencimento.Equal(data),
                                           liquidacao.Query.IdCliente.Equal(carteira.IdCarteira.Value));
                    liquidacao.Query.Load();

                    decimal valorLiquidaD0 = liquidacao.Valor.HasValue ? liquidacao.Valor.Value : 0;

                    if (saldoCaixa.SaldoAbertura.HasValue)
                    {
                        DataRow dataRow = dataTableCaixaOnLine.NewRow();
                        dataRow["Fundo"] = carteira.Nome;
                        dataRow["IdCliente"] = carteira.IdCarteira.Value;
                        dataRow["SaldoInicial"] = saldoCaixa.SaldoAbertura.Value + valorLiquidaD0;
                        //for (int i = 0; i < SaldosIniciais.DADOS.Length; i++)
                        //{
                        //    if (SaldosIniciais.DADOS[i].SIGLA_FUNDO == Convert.ToString(carteira.GetColumn(PessoaMetadata.ColumnNames.CodigoInterface)))
                        //    {
                        //        dataRow["SaldoInicial"] = SaldosIniciais.DADOS[i].TOTAL;
                        //    }
                        //}
                        dataRow["Aplicacoes"] = 0;
                        dataRow["Resgates"] = 0;
                        dataRow["Compras"] = 0;
                        dataRow["Vendas"] = 0;
                        dataRow["SaldoAtualizado"] = 0;
                        dataRow["Over"] = 0;
                        dataRow["SaldoFinal"] = 0;

                        dataTableCaixaOnLine.Rows.Add(dataRow);
                    }
                }
            //}
        }

        public void ProcessaCaixaOnLine(DateTime data)
        {
            foreach (DataRow row in dataTableCaixaOnLine.Rows) // Loop over the rows.
            {
                #region Compras/vendas Bolsa
                decimal valorCompraBolsa = 0;
                decimal valorVendaBolsa = 0;
                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();

                // compras Bolsa
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoBolsaCompra.Query.Data.Equal(data),
                                                operacaoBolsaCompra.Query.DataLiquidacao.Equal(data),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra,
                                                                                          TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                valorCompraBolsa = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                // Vendas Bolsa
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoBolsaVenda.Query.Data.Equal(data),
                                                operacaoBolsaVenda.Query.DataLiquidacao.Equal(data),
                                                operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda,
                                                                                         TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                valorVendaBolsa = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                #endregion

                #region Compas/Vendas Renda Fixa
                decimal valorCompraRF = 0;
                decimal valorVendaRF = 0;
                OperacaoRendaFixa operacaoRendaFixaCompra = new OperacaoRendaFixa();
                OperacaoRendaFixa operacaoRendaFixaVenda = new OperacaoRendaFixa();

                // Compras RF
                operacaoRendaFixaCompra.Query.Select(operacaoRendaFixaCompra.Query.Valor.Sum());
                operacaoRendaFixaCompra.Query.Where(operacaoRendaFixaCompra.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoRendaFixaCompra.Query.DataOperacao.Equal(data),
                                                operacaoRendaFixaCompra.Query.DataLiquidacao.Equal(data),
                                                operacaoRendaFixaCompra.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal,
                                                                                              (byte)TipoOperacaoTitulo.CompraCasada));
                operacaoRendaFixaCompra.Query.Load();

                valorCompraRF = operacaoRendaFixaCompra.Valor.HasValue ? operacaoRendaFixaCompra.Valor.Value : 0;

                // Vendas RF
                operacaoRendaFixaVenda.Query.Select(operacaoRendaFixaVenda.Query.Valor.Sum());
                operacaoRendaFixaVenda.Query.Where(operacaoRendaFixaVenda.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoRendaFixaVenda.Query.DataOperacao.Equal(data),
                                                operacaoRendaFixaVenda.Query.DataLiquidacao.Equal(data),
                                                operacaoRendaFixaVenda.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                             (byte)TipoOperacaoTitulo.VendaTotal,
                                                                                             (byte)TipoOperacaoTitulo.VendaCasada));
                operacaoRendaFixaVenda.Query.Load();

                valorVendaRF = operacaoRendaFixaVenda.Valor.HasValue ? operacaoRendaFixaVenda.Valor.Value : 0;

                #endregion

                #region compromissadas
                decimal valorCompraRevenda = 0;
                decimal valorVendaRecompra = 0;
                OperacaoRendaFixa operacaoRendaFixaCompraRevenda = new OperacaoRendaFixa();
                OperacaoRendaFixa operacaoRendaFixaVendaRecompra = new OperacaoRendaFixa();

                // Compras RF
                operacaoRendaFixaCompraRevenda.Query.Select(operacaoRendaFixaCompraRevenda.Query.ValorVolta.Sum());
                operacaoRendaFixaCompraRevenda.Query.Where(operacaoRendaFixaCompraRevenda.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoRendaFixaCompraRevenda.Query.DataVolta.Equal(data),
                                                operacaoRendaFixaCompraRevenda.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda));
                operacaoRendaFixaCompraRevenda.Query.Load();

                valorCompraRevenda = operacaoRendaFixaCompraRevenda.ValorVolta.HasValue ? operacaoRendaFixaCompraRevenda.ValorVolta.Value : 0;

                // Vendas RF
                operacaoRendaFixaVendaRecompra.Query.Select(operacaoRendaFixaVendaRecompra.Query.ValorVolta.Sum());
                operacaoRendaFixaVendaRecompra.Query.Where(operacaoRendaFixaVendaRecompra.Query.IdCliente.Equal(row["IdCliente"]),
                                                operacaoRendaFixaVendaRecompra.Query.DataVolta.Equal(data),
                                                operacaoRendaFixaVendaRecompra.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaRecompra));
                operacaoRendaFixaVendaRecompra.Query.Load();

                valorVendaRecompra = operacaoRendaFixaVendaRecompra.ValorVolta.HasValue ? operacaoRendaFixaVendaRecompra.ValorVolta.Value : 0;
                #endregion

                #region Aplicacoes/resgates
                OperacaoFundo operacaoFundoAplicacao = new OperacaoFundo();
                OperacaoFundo operacaoFundoResgate = new OperacaoFundo();
                decimal valorAplicacao = 0;
                decimal valorResgate = 0;
                operacaoFundoAplicacao.Query.Select(operacaoFundoAplicacao.Query.ValorBruto.Sum());
                operacaoFundoAplicacao.Query.Where(operacaoFundoAplicacao.Query.IdCliente.Equal(row["IdCliente"]),
                                                   operacaoFundoAplicacao.Query.DataOperacao.Equal(data), 
                                                   operacaoFundoAplicacao.Query.DataLiquidacao.Equal(data),
                                                   operacaoFundoAplicacao.Query.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao,
                                                                                                   (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial));

                operacaoFundoAplicacao.Query.Load();

                valorAplicacao = operacaoFundoAplicacao.ValorBruto.HasValue ? operacaoFundoAplicacao.ValorBruto.Value : 0;

                operacaoFundoResgate.Query.Select(operacaoFundoResgate.Query.ValorBruto.Sum());
                operacaoFundoResgate.Query.Where(operacaoFundoResgate.Query.IdCliente.Equal(row["IdCliente"]),
                                                 operacaoFundoResgate.Query.DataOperacao.Equal(data),    
                                                 operacaoFundoResgate.Query.DataLiquidacao.Equal(data),
                                                 operacaoFundoResgate.Query.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                       (byte)TipoOperacaoFundo.ResgateCotas,
                                                       (byte)TipoOperacaoFundo.ResgateLiquido,
                                                       (byte)TipoOperacaoFundo.ResgateTotal,
                                                       (byte)TipoOperacaoFundo.ComeCotas));

                operacaoFundoResgate.Query.Load();

                valorResgate = operacaoFundoResgate.ValorBruto.HasValue ? operacaoFundoResgate.ValorBruto.Value : 0;
                #endregion

                decimal valorSaldoInicial = (decimal)row["SaldoInicial"];

                row["Compras"] = valorCompraRF + valorCompraBolsa;
                row["Vendas"] = valorVendaRF + valorVendaBolsa;
                row["Over"] = valorCompraRevenda - valorVendaRecompra;
                row["Aplicacoes"] = valorAplicacao;
                row["Resgates"] = valorResgate;

                row["SaldoAtualizado"] = valorSaldoInicial + valorAplicacao - valorResgate - (valorCompraRF + valorCompraBolsa) + (valorVendaRF + valorVendaBolsa);
                row["SaldoFinal"] = (valorSaldoInicial + valorAplicacao - valorResgate - (valorCompraRF + valorCompraBolsa) + (valorVendaRF + valorVendaBolsa)) - (valorCompraRevenda - valorVendaRecompra);

            }

            foreach (DataRow row in dataTableCaixaOnLine.Rows) // Loop over the rows.
            {
                decimal? valor = row["Compras"] == null ? null : (decimal?)row["Compras"];
                if (valor > 0)
                {
                    Console.WriteLine("aaa");
                }
            }
        }

        public HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://ww73.itau.com.br/plinternet/Consultas/CaixaOnLine/XML/XMLCaixaOnlineSaldo.asp");
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            string proxyInfo = System.Configuration.ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo))
            {
                //Utilizar padrao de proxy definido no Windows
                webRequest.Proxy = System.Net.WebProxy.GetDefaultProxy();
                webRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default")
                {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    webRequest.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1)
                    {
                        //Foram passadas tambem as credenciais de rede
                        webRequest.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }
            return webRequest;
        }

        public CAIXAONLINESALDORETORNO PopulaSaldoInicialCaixaOnLine(DateTime data)
        {
            EMISSOR emissor = new EMISSOR();

            string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
            string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;

            emissor.login = configUsuarioIntegracao;
            emissor.senha = configSenhaIntegracao;

            emissor.tipovalidacao = EMISSORTipovalidacao.E;

            REGISTRO registro = new REGISTRO();
            registro.GRUPO = "";
            registro.MAQUINA = Environment.MachineName;
            registro.IP = "";
            registro.TPSOLIC = TPSOLIC.TS;

            HEADER header = new HEADER();
            header.EMISSOR = emissor;

            INFORMACOES informacoes = new INFORMACOES();
            informacoes.REGISTRO = registro;

            MENSAGEM mensagem = new MENSAGEM();
            mensagem.HEADER = header;
            mensagem.INFORMACOES = informacoes;

            CAIXAONLINESALDO caixaOnlineSaldo = new CAIXAONLINESALDO();
            caixaOnlineSaldo.MENSAGEM = mensagem;

            StringWriter writer = new StringWriter();

            new System.Xml.Serialization.XmlSerializer(typeof(CAIXAONLINESALDO)).Serialize(writer, caixaOnlineSaldo);

            writer.Close();

            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(writer.ToString());

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            string soapResult;


            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }
            }

            //using (StreamReader rd = new StreamReader(@"C:\temp\retorno_servico_itau.xml"))
            //{
            //    soapResult = rd.ReadToEnd();
            //}

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CAIXAONLINESALDORETORNO));

                using (TextReader reader = new StringReader(soapResult))
                {
                    CAIXAONLINESALDORETORNO msgResponse = serializer.Deserialize(reader) as CAIXAONLINESALDORETORNO;
                    return msgResponse;
                }

            }
            catch (Exception)
            {

                XmlSerializer serializer = new XmlSerializer(typeof(MENSAGEM));

                using (TextReader reader = new StringReader(soapResult))
                {
                    // caso ocorra algum Erro na chamada, o retorno é do tipo Mensagem
                    MENSAGEM msgResponse = serializer.Deserialize(reader) as MENSAGEM;
                    return null;
                }
            }
        }

        #endregion

    }
}