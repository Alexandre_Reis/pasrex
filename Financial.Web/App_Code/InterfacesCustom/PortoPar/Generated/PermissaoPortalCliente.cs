/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/05/2014 17:14:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InterfacesCustom
{

	[Serializable]
	abstract public class esPermissaoPortalClienteCollection : esEntityCollection
	{
		public esPermissaoPortalClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PermissaoPortalClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esPermissaoPortalClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPermissaoPortalClienteQuery);
		}
		#endregion
		
		virtual public PermissaoPortalCliente DetachEntity(PermissaoPortalCliente entity)
		{
			return base.DetachEntity(entity) as PermissaoPortalCliente;
		}
		
		virtual public PermissaoPortalCliente AttachEntity(PermissaoPortalCliente entity)
		{
			return base.AttachEntity(entity) as PermissaoPortalCliente;
		}
		
		virtual public void Combine(PermissaoPortalClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PermissaoPortalCliente this[int index]
		{
			get
			{
				return base[index] as PermissaoPortalCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PermissaoPortalCliente);
		}
	}



	[Serializable]
	abstract public class esPermissaoPortalCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPermissaoPortalClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esPermissaoPortalCliente()
		{

		}

		public esPermissaoPortalCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPermissaoPortalClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo)
		{
			esPermissaoPortalClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "PodeBoletar": this.str.PodeBoletar = (string)value; break;							
						case "PodeDistribuir": this.str.PodeDistribuir = (string)value; break;							
						case "PodeAprovar": this.str.PodeAprovar = (string)value; break;							
						case "AcessoSeguranca": this.str.AcessoSeguranca = (string)value; break;
                        case "AcessoPGBL": this.str.AcessoPGBL = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PermissaoPortalCliente.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(PermissaoPortalClienteMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(PermissaoPortalClienteMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoPortalCliente.PodeBoletar
		/// </summary>
		virtual public System.String PodeBoletar
		{
			get
			{
				return base.GetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar);
			}
			
			set
			{
				base.SetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoPortalCliente.PodeDistribuir
		/// </summary>
		virtual public System.String PodeDistribuir
		{
			get
			{
				return base.GetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir);
			}
			
			set
			{
				base.SetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoPortalCliente.PodeAprovar
		/// </summary>
		virtual public System.String PodeAprovar
		{
			get
			{
				return base.GetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar);
			}
			
			set
			{
				base.SetSystemString(PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoPortalCliente.AcessoSeguranca
		/// </summary>
		virtual public System.String AcessoSeguranca
		{
			get
			{
				return base.GetSystemString(PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca);
			}
			
			set
			{
				base.SetSystemString(PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca, value);
			}
		}

        /// <summary>
        /// Maps to PermissaoPortalCliente.AcessoPGBL
        /// </summary>
        virtual public System.String AcessoPGBL
        {
            get
            {
                return base.GetSystemString(PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL);
            }

            set
            {
                base.SetSystemString(PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL, value);
            }
        }
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPermissaoPortalCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String PodeBoletar
			{
				get
				{
					System.String data = entity.PodeBoletar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PodeBoletar = null;
					else entity.PodeBoletar = Convert.ToString(value);
				}
			}
				
			public System.String PodeDistribuir
			{
				get
				{
					System.String data = entity.PodeDistribuir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PodeDistribuir = null;
					else entity.PodeDistribuir = Convert.ToString(value);
				}
			}
				
			public System.String PodeAprovar
			{
				get
				{
					System.String data = entity.PodeAprovar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PodeAprovar = null;
					else entity.PodeAprovar = Convert.ToString(value);
				}
			}
				
			public System.String AcessoSeguranca
			{
				get
				{
					System.String data = entity.AcessoSeguranca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AcessoSeguranca = null;
					else entity.AcessoSeguranca = Convert.ToString(value);
				}
			}

            public System.String AcessoPGBL
            {
                get
                {
                    System.String data = entity.AcessoPGBL;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.AcessoPGBL = null;
                    else entity.AcessoPGBL = Convert.ToString(value);
                }
            }
			

			private esPermissaoPortalCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPermissaoPortalClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPermissaoPortalCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PermissaoPortalCliente : esPermissaoPortalCliente
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPermissaoPortalClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoPortalClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PodeBoletar
		{
			get
			{
				return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar, esSystemType.String);
			}
		} 
		
		public esQueryItem PodeDistribuir
		{
			get
			{
				return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir, esSystemType.String);
			}
		} 
		
		public esQueryItem PodeAprovar
		{
			get
			{
				return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar, esSystemType.String);
			}
		} 
		
		public esQueryItem AcessoSeguranca
		{
			get
			{
				return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca, esSystemType.String);
			}
		}

        public esQueryItem AcessoPGBL
        {
            get
            {
                return new esQueryItem(this, PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL, esSystemType.String);
            }
        } 
		
	}



	[Serializable]
	[XmlType("PermissaoPortalClienteCollection")]
	public partial class PermissaoPortalClienteCollection : esPermissaoPortalClienteCollection, IEnumerable<PermissaoPortalCliente>
	{
		public PermissaoPortalClienteCollection()
		{

		}
		
		public static implicit operator List<PermissaoPortalCliente>(PermissaoPortalClienteCollection coll)
		{
			List<PermissaoPortalCliente> list = new List<PermissaoPortalCliente>();
			
			foreach (PermissaoPortalCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PermissaoPortalClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoPortalClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PermissaoPortalCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PermissaoPortalCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PermissaoPortalClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoPortalClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PermissaoPortalClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PermissaoPortalCliente AddNew()
		{
			PermissaoPortalCliente entity = base.AddNewEntity() as PermissaoPortalCliente;
			
			return entity;
		}

		public PermissaoPortalCliente FindByPrimaryKey(System.Int32 idGrupo)
		{
			return base.FindByPrimaryKey(idGrupo) as PermissaoPortalCliente;
		}


		#region IEnumerable<PermissaoPortalCliente> Members

		IEnumerator<PermissaoPortalCliente> IEnumerable<PermissaoPortalCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PermissaoPortalCliente;
			}
		}

		#endregion
		
		private PermissaoPortalClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PermissaoPortalCliente' table
	/// </summary>

	[Serializable]
	public partial class PermissaoPortalCliente : esPermissaoPortalCliente
	{
		public PermissaoPortalCliente()
		{

		}
	
		public PermissaoPortalCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoPortalClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esPermissaoPortalClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoPortalClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PermissaoPortalClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoPortalClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PermissaoPortalClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PermissaoPortalClienteQuery query;
	}



	[Serializable]
	public partial class PermissaoPortalClienteQuery : esPermissaoPortalClienteQuery
	{
		public PermissaoPortalClienteQuery()
		{

		}		
		
		public PermissaoPortalClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PermissaoPortalClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PermissaoPortalClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.PodeBoletar;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.PodeDistribuir;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.PodeAprovar;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.AcessoSeguranca;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c);

            c = new esColumnMetadata(PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL, 5, typeof(System.String), esSystemType.String);
            c.PropertyName = PermissaoPortalClienteMetadata.PropertyNames.AcessoPGBL;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            _columns.Add(c); 
			
				
		}
		#endregion
	
		static public PermissaoPortalClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string PodeBoletar = "PodeBoletar";
			 public const string PodeDistribuir = "PodeDistribuir";
			 public const string PodeAprovar = "PodeAprovar";
			 public const string AcessoSeguranca = "AcessoSeguranca";
            public const string AcessoPGBL = "AcessoPGBL";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string PodeBoletar = "PodeBoletar";
			 public const string PodeDistribuir = "PodeDistribuir";
			 public const string PodeAprovar = "PodeAprovar";
			 public const string AcessoSeguranca = "AcessoSeguranca";
            public const string AcessoPGBL = "AcessoPGBL";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PermissaoPortalClienteMetadata))
			{
				if(PermissaoPortalClienteMetadata.mapDelegates == null)
				{
					PermissaoPortalClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PermissaoPortalClienteMetadata.meta == null)
				{
					PermissaoPortalClienteMetadata.meta = new PermissaoPortalClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PodeBoletar", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PodeDistribuir", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PodeAprovar", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("AcessoSeguranca", new esTypeMap("char", "System.String"));
                meta.AddTypeMap("AcessoPGBL", new esTypeMap("char", "System.String"));			
				
				
				meta.Source = "PermissaoPortalCliente";
				meta.Destination = "PermissaoPortalCliente";
				
				meta.spInsert = "proc_PermissaoPortalClienteInsert";				
				meta.spUpdate = "proc_PermissaoPortalClienteUpdate";		
				meta.spDelete = "proc_PermissaoPortalClienteDelete";
				meta.spLoadAll = "proc_PermissaoPortalClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_PermissaoPortalClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PermissaoPortalClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
