/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 13:34:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InterfacesCustom
{

	[Serializable]
	abstract public class esTabelaBandaCarteiraCollection : esEntityCollection
	{
		public esTabelaBandaCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaBandaCarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaBandaCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaBandaCarteiraQuery);
		}
		#endregion
		
		virtual public TabelaBandaCarteira DetachEntity(TabelaBandaCarteira entity)
		{
			return base.DetachEntity(entity) as TabelaBandaCarteira;
		}
		
		virtual public TabelaBandaCarteira AttachEntity(TabelaBandaCarteira entity)
		{
			return base.AttachEntity(entity) as TabelaBandaCarteira;
		}
		
		virtual public void Combine(TabelaBandaCarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaBandaCarteira this[int index]
		{
			get
			{
				return base[index] as TabelaBandaCarteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaBandaCarteira);
		}
	}



	[Serializable]
	abstract public class esTabelaBandaCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaBandaCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaBandaCarteira()
		{

		}

		public esTabelaBandaCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, dataReferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, dataReferencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaBandaCarteiraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.DataReferencia == dataReferencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, dataReferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, dataReferencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			esTabelaBandaCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.DataReferencia == dataReferencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("DataReferencia",dataReferencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "LimiteSuperior": this.str.LimiteSuperior = (string)value; break;							
						case "LimiteInferior": this.str.LimiteInferior = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "LimiteSuperior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteSuperior = (System.Decimal?)value;
							break;
						
						case "LimiteInferior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteInferior = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaBandaCarteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaBandaCarteira.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaBandaCarteira.LimiteSuperior
		/// </summary>
		virtual public System.Decimal? LimiteSuperior
		{
			get
			{
				return base.GetSystemDecimal(TabelaBandaCarteiraMetadata.ColumnNames.LimiteSuperior);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaBandaCarteiraMetadata.ColumnNames.LimiteSuperior, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaBandaCarteira.LimiteInferior
		/// </summary>
		virtual public System.Decimal? LimiteInferior
		{
			get
			{
				return base.GetSystemDecimal(TabelaBandaCarteiraMetadata.ColumnNames.LimiteInferior);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaBandaCarteiraMetadata.ColumnNames.LimiteInferior, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaBandaCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String LimiteSuperior
			{
				get
				{
					System.Decimal? data = entity.LimiteSuperior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteSuperior = null;
					else entity.LimiteSuperior = Convert.ToDecimal(value);
				}
			}
				
			public System.String LimiteInferior
			{
				get
				{
					System.Decimal? data = entity.LimiteInferior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteInferior = null;
					else entity.LimiteInferior = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaBandaCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaBandaCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaBandaCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaBandaCarteira : esTabelaBandaCarteira
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaBandaCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaBandaCarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem LimiteSuperior
		{
			get
			{
				return new esQueryItem(this, TabelaBandaCarteiraMetadata.ColumnNames.LimiteSuperior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem LimiteInferior
		{
			get
			{
				return new esQueryItem(this, TabelaBandaCarteiraMetadata.ColumnNames.LimiteInferior, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaBandaCarteiraCollection")]
	public partial class TabelaBandaCarteiraCollection : esTabelaBandaCarteiraCollection, IEnumerable<TabelaBandaCarteira>
	{
		public TabelaBandaCarteiraCollection()
		{

		}
		
		public static implicit operator List<TabelaBandaCarteira>(TabelaBandaCarteiraCollection coll)
		{
			List<TabelaBandaCarteira> list = new List<TabelaBandaCarteira>();
			
			foreach (TabelaBandaCarteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaBandaCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaBandaCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaBandaCarteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaBandaCarteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaBandaCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaBandaCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaBandaCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaBandaCarteira AddNew()
		{
			TabelaBandaCarteira entity = base.AddNewEntity() as TabelaBandaCarteira;
			
			return entity;
		}

		public TabelaBandaCarteira FindByPrimaryKey(System.Int32 idCarteira, System.DateTime dataReferencia)
		{
			return base.FindByPrimaryKey(idCarteira, dataReferencia) as TabelaBandaCarteira;
		}


		#region IEnumerable<TabelaBandaCarteira> Members

		IEnumerator<TabelaBandaCarteira> IEnumerable<TabelaBandaCarteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaBandaCarteira;
			}
		}

		#endregion
		
		private TabelaBandaCarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaBandaCarteira' table
	/// </summary>

	[Serializable]
	public partial class TabelaBandaCarteira : esTabelaBandaCarteira
	{
		public TabelaBandaCarteira()
		{

		}
	
		public TabelaBandaCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaBandaCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaBandaCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaBandaCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaBandaCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaBandaCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaBandaCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaBandaCarteiraQuery query;
	}



	[Serializable]
	public partial class TabelaBandaCarteiraQuery : esTabelaBandaCarteiraQuery
	{
		public TabelaBandaCarteiraQuery()
		{

		}		
		
		public TabelaBandaCarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaBandaCarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaBandaCarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaBandaCarteiraMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaBandaCarteiraMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaBandaCarteiraMetadata.ColumnNames.LimiteSuperior, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaBandaCarteiraMetadata.PropertyNames.LimiteSuperior;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaBandaCarteiraMetadata.ColumnNames.LimiteInferior, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaBandaCarteiraMetadata.PropertyNames.LimiteInferior;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaBandaCarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataReferencia = "DataReferencia";
			 public const string LimiteSuperior = "LimiteSuperior";
			 public const string LimiteInferior = "LimiteInferior";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataReferencia = "DataReferencia";
			 public const string LimiteSuperior = "LimiteSuperior";
			 public const string LimiteInferior = "LimiteInferior";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaBandaCarteiraMetadata))
			{
				if(TabelaBandaCarteiraMetadata.mapDelegates == null)
				{
					TabelaBandaCarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaBandaCarteiraMetadata.meta == null)
				{
					TabelaBandaCarteiraMetadata.meta = new TabelaBandaCarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("LimiteSuperior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("LimiteInferior", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaBandaCarteira";
				meta.Destination = "TabelaBandaCarteira";
				
				meta.spInsert = "proc_TabelaBandaCarteiraInsert";				
				meta.spUpdate = "proc_TabelaBandaCarteiraUpdate";		
				meta.spDelete = "proc_TabelaBandaCarteiraDelete";
				meta.spLoadAll = "proc_TabelaBandaCarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaBandaCarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaBandaCarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
