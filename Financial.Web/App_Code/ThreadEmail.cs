﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.Web.Configuration;
using System.Web;
using Financial.Relatorio;
using Financial.Investidor;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace Financial.Web.ThreadEmail {
    
    /// <summary>
    ///  Classe que contem os metodos que serão executados pela Thread
    /// </summary>
    [Obsolete("Thread para envio de email: não Funcionou")]
    public class ThreadEmail {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="reports">Reports a serem Atachados</param>
        /// <param name="fileNames">Nomes dos Arquvos Atachados</param>
        [Obsolete("Thread para envio de email: não Funcionou")]
        public void EnviaEmailPorCliente(int idCliente, List<ReportMaster> reports, List<string> fileNames) {
            #region Pega as informações de BookEmail
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            string listaEmail = !String.IsNullOrEmpty(cliente.BookEmail) ? cliente.BookEmail.ToString() : "";
            List<string> destinatarios = new List<string>();

            if (listaEmail != "") {
                listaEmail = listaEmail.Replace(",", ";");
                string[] emails = listaEmail.Split(new Char[] { ';' });
                foreach (string email in emails) {
                    destinatarios.Add(email);
                }
            }

            string subject = !String.IsNullOrEmpty(cliente.BookAssunto) ? cliente.BookAssunto.ToString() : "";
            string body = !String.IsNullOrEmpty(cliente.BookMensagem) ? cliente.BookMensagem.ToString() : "";

            #endregion

            // Prepara os Atachments
            List<Attachment> attachments = this.MontaAttachments(reports, fileNames);

            this.SendMessage(destinatarios, attachments, subject, body);
        }

        /// <summary>
        /// Dado uma lista de Reports Monta Attachments para serem incluidos num email
        /// </summary>
        /// <param name="listaReports"></param>
        /// <param name="filenames">Nomes dos arquivos a serem atachados</param>
        /// <returns>Lista de Arquivos Atachados</returns>
        /// <exception cref="ArgumentException">Se tamanho de listaReports for diferente do tamanho de filenames</exception>
        private List<Attachment> MontaAttachments(List<ReportMaster> listaReports, List<string> filenames) {
            if (listaReports.Count != filenames.Count) {
                throw new ArgumentException("ListaReports e Filenames devem ter o mesmo numero de elementos");
            }

            List<Attachment> listaAttachment = new List<Attachment>();

            for (int i = 0; i < listaReports.Count; i++) {
                string fileName = filenames[i] + ".pdf";
                ReportMaster report = listaReports[i];
                //
                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Attachment att = new Attachment(ms, fileName, "application/pdf");

                listaAttachment.Add(att);
            }

            return listaAttachment;
        }

        /// <summary>
        /// Envia uma Mensagem de Email
        /// </summary>
        /// <param name="listaEmail">Emails Destino para quem se quer mandar a mensagem</param>
        /// <param name="attachments">Arquivos Attachments a serem mandados na mensagem</param>
        /// <param name="subject">Assunto da Mensagem</param>
        /// <param name="body">Corpo da Mensagem</param>
        private void SendMessage(List<string> listaEmail, List<Attachment> attachments, string subject, string body) {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            // Create a new message and attach the PDF report to it.
            MailMessage mail = new MailMessage();
            for (int i = 0; i < attachments.Count; i++) {
                mail.Attachments.Add(attachments[i]);
            }

            if (listaEmail.Count != 0) {
                for (int j = 0; j < listaEmail.Count; j++) {
                    mail.To.Add(new MailAddress(listaEmail[j]));
                }
                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress(settings.Smtp.From.ToString());

                // Specify other e-mail options.
                mail.Subject = subject;
                mail.Body = body;

                // Send the e-mail message via the specified SMTP server.
                SmtpClient smtp = new SmtpClient();
                //smtp.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                smtp.EnableSsl = Convert.ToBoolean(WebConfig.AppSettings.SMTPEnableSSL);

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
                if (!string.IsNullOrEmpty(smtpUsername))
                {
                    string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
                    smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                }

                smtp.Send(mail);
                //this.controleMensagensEnviadas++;
            }
        }
    }
}
