﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Web.Configuration;
using System.Globalization;
using Financial.Web.Util;
using Financial.WebConfigConfiguration;
using System.IO;
using Financial.Security;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Net.Configuration;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Financial.Util;

public partial class LembrarSenha : Financial.Web.Common.BasePage {
    protected override void InitializeCulture() {
        PersonalizeCulture.InicializaCulturePersonalizada();
        base.InitializeCulture();
    }

    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.Title = String.IsNullOrEmpty(ConfigurationManager.AppSettings["TituloPagina"]) ? "Financial" : ConfigurationManager.AppSettings["TituloPagina"];

        if (!Page.IsPostBack) {
            this.InitializeCulture();
        }

        // HACK - Pegar do Resource
        UserName.Focus();

    }

    protected void callbackSenha_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuarioPorEmail(UserName.Text)) {
            e.Result = "Email não cadastrado!";
            return;
        }
        else {
            EnviaSenha(usuario);
            e.Result = "email|" + usuario.Email.ToString();
        }
    }

    /// <summary>
    /// Envia email.
    /// Usa ConfigurationManager.AppSettings["SMTPServer"] para capturar o SMTP server.
    /// </summary>
    /// <param name="in_Body"></param>
    /// <param name="in_Subject"></param>
    /// <param name="in_From"></param>
    /// <param name="in_To"></param>
    /// <param name="in_Bcc"></param>
    private void SendMail(string in_Body, string in_Subject, string in_From, string in_To) 
    {
        EmailUtil emailUtil = new EmailUtil();
        emailUtil.SendMail(in_Body, in_Subject, in_From, in_To);
    }

    private void EnviaSenha(Usuario usuario) {
        string nome = "";
        string email = "";
        string senha = "";

        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        nome = usuario.Nome;
        email = usuario.Email;
        senha = usuario.Senha;

        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);
        senha = financialMembershipProvider.UnEncodePassword(senha);


        string from = settings.Smtp.From.ToString();
        string to = email;
        string body = "Caro(a) " + nome + " , conforme solicitado estamos enviando sua senha. \n\n Senha atual: " + senha;
        string cliente = WebConfig.AppSettings.Cliente;
        if (!String.IsNullOrEmpty(cliente)) {
            cliente = "(" + cliente + ")";
        }
        string subject = "Acesso ao Financial " + cliente + " - Envio de senha";

        SendMail(body, subject, from, to);
    }

    protected void BackButton_Click(object sender, EventArgs e) {
        Response.Redirect("~/Login/LoginInit.aspx");
    }
}