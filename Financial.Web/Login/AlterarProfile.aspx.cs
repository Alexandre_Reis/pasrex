﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Text;

using Financial.Util;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;

using DevExpress.Web;

public partial class AlterarProfile : System.Web.UI.Page
{
    //    
    protected void Page_Load(object sender, EventArgs e)
    {
        txtEmail.Focus();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackSalvar_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        string login = HttpContext.Current.User.Identity.Name;

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuario(login))
        {
            e.Result = "1§Usuário não está logado! Favor relogar no sistema.";
            return;
        }

        bool update = false;

        if (txtEmail.Text != "")
        {
            update = true;
            usuario.Email = txtEmail.Text;
        }

        if (txtSenha.Text != "")
        {
            update = true;

            try
            {
                usuario.ChangePassword(usuario.IdUsuario.Value, txtSenha.Text, txtConfirmaSenha.Text, null);
            }
            catch (Exception exception)
            {
                e.Result = "1§" + exception.Message;
                return;
            }
        }

        if (update)
        {
            usuario.Save();
            e.Result = "0§";
        }
    }
}