using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Financial.Security;

public partial class Login_TrocarSenha : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.IsNullOrEmpty(ConfigurationManager.AppSettings["TituloPagina"]) ? "Financial" : ConfigurationManager.AppSettings["TituloPagina"];

        this.Password.Focus();
        //
        //this.Password.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + this.LoginButton.UniqueID + "','')");
        //this.Password.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) alert('teste'); ");
        //
        this.Password.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) { callbackSenha.SendCallback(); return false; }");
        this.PasswordConfirmation.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) { callbackSenha.SendCallback(); return false; } ");
    }

    protected void callbackSenha_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        string login = HttpContext.Current.User.Identity.Name;
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        try {
            usuario.ChangePassword(usuario.IdUsuario.Value, Password.Text, PasswordConfirmation.Text, null);
        }
        catch (Exception exception) {
            e.Result = exception.Message;
            return;
        }

        //Sucesso
        e.Result = String.Format("ReturnURL={0}", Page.ResolveUrl(Request.Params["ReturnUrl"]));
    }
}