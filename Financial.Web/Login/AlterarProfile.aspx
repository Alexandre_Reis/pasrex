﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlterarProfile.aspx.cs" Inherits="AlterarProfile" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="../js/util/ExtDevExpressAdapter.js" charset="iso-8859-1"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackSalvar" runat="server" OnCallback="callbackSalvar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                var splitResult = e.result.split('§');
                var errorCode = parseInt(splitResult[0],10);
                var errorMessage = splitResult[1];
                if(errorCode === 0){
                    alert('Dados alterados com sucesso!');
                }else{
                    alert(errorMessage);
                }
                if(window.ExtDevExpressAdapter){
                    window.ExtDevExpressAdapter.aftersave({errorCode: errorCode, errorMessage: errorMessage});
                }
            }             
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Alteração de Perfil de Usuário" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlPerfil" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px"
                                                        Width="659px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="labelEmail" runat="server" CssClass="labelNormal" Text="Email:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="textLongo5" MaxLength="200" />
                                                                            <asp:Label ID="labelInfoEmail" runat="server" CssClass="labelNormal" Text="(Deixe em branco, se não for editar)"
                                                                                Style="color: #58607C" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="labelSenha" runat="server" CssClass="labelNormal" Text="Nova Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" CssClass="textLongo5"
                                                                                MaxLength="255" />
                                                                            <asp:Label ID="labelInfoSenha" runat="server" CssClass="labelNormal" Text="(Deixe em branco, se não for editar)"
                                                                                Style="color: #58607C" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: right">
                                                                            <asp:Label ID="labelConfirmaSenha" runat="server" CssClass="labelNormal" Text="Confirma Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtConfirmaSenha" runat="server" TextMode="Password" CssClass="textLongo5" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 50pt">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            OnClientClick="callbackSalvar.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="Salvar" /><div></div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>