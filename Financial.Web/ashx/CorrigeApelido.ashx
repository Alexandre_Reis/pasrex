<%@ WebHandler Language="C#" Class="CorrigeApelido" %>

using System;
using System.Web;
using Financial.CRM;
using Financial.Investidor;
using Financial.Fundo;
using Financial.InvestidorCotista;
using System.Collections.Generic;

public class CorrigeApelido : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        //return;
        
        Cliente cliente = new Cliente();
        Pessoa pessoa = new Pessoa();
        Carteira carteira = new Carteira();
        Cotista cotista = new Cotista();
        List<int> listaClientesAtualizados = new List<int>();
        
        ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
        clienteInterfaceCollection.LoadAll();
                
        foreach(ClienteInterface clienteInterface in clienteInterfaceCollection){
            string codigoYMF = clienteInterface.CodigoYMF == null ? "" : clienteInterface.CodigoYMF;
            int idCliente = (int)clienteInterface.IdCliente;
            if (codigoYMF.Length>0)
            {
                if (!listaClientesAtualizados.Contains(idCliente))
                {
                    cliente = new Cliente();
                    pessoa = new Pessoa();
                    carteira = new Carteira();
                    cotista = new Cotista();
                    if (cliente.LoadByPrimaryKey(idCliente))
                    {
                        string apelidoNovo = cliente.Apelido + " - " + codigoYMF;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = cliente.Apelido.Substring(0, 40 - 3 - codigoYMF.Length) + " - " + codigoYMF;
                        if (!cliente.Apelido.Contains(" - " + codigoYMF))
                        {
                            cliente.Apelido = apelidoNovo;
                            cliente.Save();
                        }
                    }
                    if (pessoa.LoadByPrimaryKey(idCliente))
                    {
                        string apelidoNovo = pessoa.Apelido + " - " + codigoYMF;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = pessoa.Apelido.Substring(0, 40 - 3 - codigoYMF.Length) + " - " + codigoYMF;
                        if (!pessoa.Apelido.Contains(" - " + codigoYMF))
                        {
                            pessoa.Apelido = apelidoNovo;
                            pessoa.Save();
                        }
                    }
                    if (cotista.LoadByPrimaryKey(idCliente))
                    {
                        string apelidoNovo = cotista.Apelido + " - " + codigoYMF;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = cotista.Apelido.Substring(0, 40 - 3 - codigoYMF.Length) + " - " + codigoYMF;
                        if (!cotista.Apelido.Contains(" - " + codigoYMF))
                        {
                            cotista.Apelido = apelidoNovo;
                            cotista.Save();
                        }
                    }
                    if (carteira.LoadByPrimaryKey(idCliente))
                    {
                        string apelidoNovo = carteira.Apelido + " - " + codigoYMF;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = carteira.Apelido.Substring(0, 40 - 3 - codigoYMF.Length) + " - " + codigoYMF;
                        if (!carteira.Apelido.Contains(" - " + codigoYMF))
                        {
                            carteira.Apelido = apelidoNovo;
                            carteira.Save();
                        }
                    }
                    listaClientesAtualizados.Add(idCliente);
                }
            }
        }

        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.LoadAll();

        foreach (Cotista cotistaCad in cotistaCollection)
        {
            string codigoInterface = cotistaCad.CodigoInterface == null ? "" : cotistaCad.CodigoInterface; 
            int idCotista = (int)cotistaCad.IdCotista;
            if (codigoInterface.Length > 0)
            {
                if (!listaClientesAtualizados.Contains(idCotista))
                {
                    cliente = new Cliente();
                    pessoa = new Pessoa();
                    carteira = new Carteira();
                    cotista = new Cotista();
                    if (cliente.LoadByPrimaryKey(idCotista))
                    {
                        string apelidoNovo = cliente.Apelido + " - " + codigoInterface;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = cliente.Apelido.Substring(0, 40 - 3 - codigoInterface.Length) + " - " + codigoInterface;
                        if (!cliente.Apelido.Contains(" - " + codigoInterface))
                        {
                            cliente.Apelido = apelidoNovo;
                            cliente.Save();
                        }
                    }
                    if (pessoa.LoadByPrimaryKey(idCotista))
                    {
                        string apelidoNovo = pessoa.Apelido + " - " + codigoInterface;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = pessoa.Apelido.Substring(0, 40 - 3 - codigoInterface.Length) + " - " + codigoInterface;
                        if (!pessoa.Apelido.Contains(" - " + codigoInterface))
                        {
                            pessoa.Apelido = apelidoNovo;
                            pessoa.Save();
                        }
                    }
                    if (cotista.LoadByPrimaryKey(idCotista))
                    {
                        string apelidoNovo = cotista.Apelido + " - " + codigoInterface;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = cotista.Apelido.Substring(0, 40 - 3 - codigoInterface.Length) + " - " + codigoInterface;
                        if (!cotista.Apelido.Contains(" - " + codigoInterface))
                        {
                            cotista.Apelido = apelidoNovo;
                            cotista.Save();
                        }
                    }
                    if (carteira.LoadByPrimaryKey(idCotista))
                    {
                        string apelidoNovo = carteira.Apelido + " - " + codigoInterface;
                        if (apelidoNovo.Length > 40)
                            apelidoNovo = carteira.Apelido.Substring(0, 40 - 3 - codigoInterface.Length) + " - " + codigoInterface;
                        if (!carteira.Apelido.Contains(" - " + codigoInterface))
                        {
                            carteira.Apelido = apelidoNovo;
                            carteira.Save();
                        }
                    }
                    listaClientesAtualizados.Add(idCotista);
                }
            }
        }
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("Apelidos alterados com sucesso");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}