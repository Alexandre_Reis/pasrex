<%@ WebHandler Language="C#" Class="AppSettings" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Financial;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Investidor;
using FinancialDesk.BO;
using Financial.Util;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.Security;

public class AppSettings : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        JsonResponse jsonResponse = new JsonResponse(context);

        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
        jsonSerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.XmlIgnoreContractResolver();

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.None, jsonSerializerSettings);
        //string json = "{\"success\":true,\"erros\":[],\"indices\":[{\"IdIndice\":1,\"Descricao\":\"CDI\",\"Tipo\":1,\"TipoDivulgacao\":1,\"Taxa\":null,\"Percentual\":null,\"IdIndiceBase\":null},{\"IdIndice\":21,\"Descricao\":\"D�LAR\",\"Tipo\":2,\"TipoDivulgacao\":1,\"Taxa\":null,\"Percentual\":null,\"IdIndiceBase\":null},{\"IdIndice\":70,\"Descricao\":\"IBOVESPA\",\"Tipo\":2,\"TipoDivulgacao\":1,\"Taxa\":null,\"Percentual\":null,\"IdIndiceBase\":null}],\"idIndiceDefault\":70,\"nomeCliente\":\"COFIBENS I - CLUBE DE INVESTIMENTO\",\"applicationPath\":\"/Financial/Financial.Web\",\"tipoAtivo\":[{\"IdTipo\":1,\"Tipo\":\"Acao\",\"Singular\":\"A��o\",\"Plural\":\"A��es\"},{\"IdTipo\":2,\"Tipo\":\"OpcaoBolsa\",\"Singular\":\"Op��o Bolsa\",\"Plural\":\"Op��es Bolsa\"},{\"IdTipo\":3,\"Tipo\":\"TermoBolsa\",\"Singular\":\"Termo Bolsa\",\"Plural\":\"Termos Bolsa\"},{\"IdTipo\":10,\"Tipo\":\"FuturoBMF\",\"Singular\":\"Futuro BMF\",\"Plural\":\"Futuros BMF\"},{\"IdTipo\":10,\"Tipo\":\"OpcaoBMF\",\"Singular\":\"Op��o BMF\",\"Plural\":\"Op��es BMF\"},{\"IdTipo\":20,\"Tipo\":\"RendaFixa\",\"Singular\":\"Renda Fixa\",\"Plural\":\"Renda Fixa\"},{\"IdTipo\":30,\"Tipo\":\"CotaInvestimento\",\"Singular\":\"Fundo de Investimento\",\"Plural\":\"Fundos de Investimento\"},{\"IdTipo\":40,\"Tipo\":\"Caixa\",\"Singular\":\"Saldo em CC\",\"Plural\":\"Saldo em CC\"},{\"IdTipo\":50,\"Tipo\":\"Liquidar\",\"Singular\":\"Liquida��o Proj.\",\"Plural\":\"Liquida��o Proj.\"}],\"relatorioInfo\":[{\"Url\":\"\",\"Singular\":\"Carteira/Caixa\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportComposicaoCarteira.aspx\",\"Singular\":\"Composi��o Carteira\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportExtratoCliente.aspx\",\"Singular\":\"Extrato Consolidado\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportFluxoCaixaAnalitico.aspx\",\"Singular\":\"Fluxo de Caixa Anal�tico\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportFluxoCaixaSintetico.aspx\",\"Singular\":\"Fluxo de Caixa Sint�tico\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportDiarioCaixaMercado.aspx\",\"Singular\":\"Caixa por Mercado\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportHistoricoCota.aspx\",\"Singular\":\"Hist�rico Cota/PL\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportDemonstrativoTaxas.aspx\",\"Singular\":\"Demonstrativo Taxas\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportDemonstrativoProvisoes.aspx\",\"Singular\":\"Demonstrativo Provis�es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportSaldoAplicacaoFundo.aspx\",\"Singular\":\"Saldos de Aplica��es (Fundos)\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportMovimentacaoFundo.aspx\",\"Singular\":\"Movimento Aplic/Resgate\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportMapaResultado.aspx\",\"Singular\":\"Mapa de Resultado\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportLiquidacaoCarteira.aspx\",\"Singular\":\"Liquida��o Carteira\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Gest�oc/An�lises\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportDemonstrativoConsolidadoCarteiras.aspx\",\"Singular\":\"Demonstrativo Geral Carteiras\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportQuadroRetorno.aspx\",\"Singular\":\"Quadro Retornos\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportDemonstrativoReceita.aspx\",\"Singular\":\"Demonstrativo Geral Receitas\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Fundo/FiltroReportAlocacaoDistribuicaoFundos.aspx\",\"Singular\":\"Aloca��o x Distribui��o\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Gr�ficos\",\"IsLeaf\":false,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportEvolucaoFundo.aspx\",\"Singular\":\"Evolu��o do Fundo\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportJanelaMovel.aspx\",\"Singular\":\"Janela M�vel\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportMatrizCorrelacao.aspx\",\"Singular\":\"Matriz de Correla��o\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportGraficoCorrelacao.aspx\",\"Singular\":\"Janela de Correla��o\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportGraficoRetornoRisco.aspx\",\"Singular\":\"Retorno x Risco\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"\",\"Singular\":\"Cotista\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportSaldoAplicacaoCotista.aspx\",\"Singular\":\"Saldos de Aplica��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportMovimentacaoCotista.aspx\",\"Singular\":\"Movimento Aplic/Resgate\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportNotasAplicacaoResgate.aspx\",\"Singular\":\"Notas de Aplic/Resgate\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportExtratoCotista.aspx\",\"Singular\":\"Extrato de Movimenta��o\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportListaEtiqueta.aspx\",\"Singular\":\"Lista de Etiquetas\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportFinanceiroCotista.aspx\",\"Singular\":\"Financeiro por Cotista\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Cotista/FiltroReportDetalheResgateCotista.aspx\",\"Singular\":\"Detalhamento de Resgates\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Rebates de Cliente/Fundo\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportVolumeGestor.aspx\",\"Singular\":\"Volume Gestor\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportMovimentoGestor.aspx\",\"Singular\":\"Movimento Gestor\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportRebateGestor.aspx\",\"Singular\":\"Rebate Gestor\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportRebateOfficer.aspx\",\"Singular\":\"Rebate Officer\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportTotalReceita.aspx\",\"Singular\":\"Total Receita\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Captacao/FiltroReportRebateDistribuidor.aspx\",\"Singular\":\"Rebate Distribuidor\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Conta Corrente\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/ContaCorrente/FiltroReportExtratoContaCorrente.aspx\",\"Singular\":\"Extrato Consolidado\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Bolsa\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportMapaOperacaoBolsa.aspx\",\"Singular\":\"Mapa de Opera��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportMapaMovimentacaoCustodiaBolsa.aspx\",\"Singular\":\"Mapa de Movimento Cust�dia\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportPosicaoCustodia.aspx\",\"Singular\":\"Posi��o Geral de Cust�dia\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportEventosBolsa.aspx\",\"Singular\":\"Lista de Eventos Bovespa\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportMapaEmprestimoAcoes.aspx\",\"Singular\":\"Mapa de Empr�stimo A��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportPosicaoEmprestimoAcoes.aspx\",\"Singular\":\"Posi��o em Emprestimo A��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Bolsa/FiltroReportFluxoCustodia.aspx\",\"Singular\":\"Fluxo de Cust�dia\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"BMF\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/BMF/FiltroReportMapaOperacaoBMF.aspx\",\"Singular\":\"Mapa de Opera��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/BMF/FiltroReportMapaMovimentacaoCustodiaBMF.aspx\",\"Singular\":\"Mapa de Movimento Cust�dia\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Gerencial\",\"IsLeaf\":true,\"Level\":0},{\"Url\":\"\",\"Singular\":\"Renda Fixa\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/RendaFixa/FiltroReportOperacaoRendaFixa.aspx\",\"Singular\":\"Mapa de Opera��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Swap\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Swap/FiltroReportMapaOperacaoSwap.aspx\",\"Singular\":\"Mapa de Opera��es\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Swap/FiltroReportPosicaoSwap.aspx\",\"Singular\":\"Posi��o Geral de Clientes\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Enquadramento\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"/Financial/Financial.Web/Relatorios/Enquadra/FiltroReportResultadoConsolidado.aspx\",\"Singular\":\"Resultado Consolidado\",\"IsLeaf\":true,\"Level\":1},{\"Url\":\"\",\"Singular\":\"Legais\",\"IsLeaf\":false,\"Level\":0},{\"Url\":\"\",\"Singular\":\"Clubes\",\"IsLeaf\":false,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Legais/Clubes/FiltroReportInformeMensalClubes.aspx\",\"Singular\":\"Informe Mensal de Clubes\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"\",\"Singular\":\"ImpostoRenda\",\"IsLeaf\":false,\"Level\":1},{\"Url\":\"/Financial/Financial.Web/Relatorios/Legais/ImpostoRenda/FiltroReportInformeRendimentos.aspx\",\"Singular\":\"Informe de Rendimentos\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Legais/ImpostoRenda/FiltroReportGanhoRendaVariavel.aspx\",\"Singular\":\"Ganhos Renda Variavel\",\"IsLeaf\":true,\"Level\":2},{\"Url\":\"/Financial/Financial.Web/Relatorios/Legais/ImpostoRenda/FiltroReportInformeContabilPJ.aspx\",\"Singular\":\"Informe Cont�bil PJ\",\"IsLeaf\":true,\"Level\":2}]}";
        context.Response.ContentType = "text/html";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JsonResponse
    {
        public bool success;
        public List<string> erros = new List<string>();
        public DataTable indices;
        public int idIndiceDefault;
        public string nomeCliente;
        public bool acessaCarteiraSimulada;
        public string observacaoExtrato;
        public int idCliente;
        public bool isCarteiraSimulada;
        public string dataCliente;
        public string applicationPath;
        public string helpPath;
        public bool travaCliente;
        public bool explodeFundos;
        public List<FinancialDeskTipoAtivo> tipoAtivo = new List<FinancialDeskTipoAtivo>();
        public List<RelatorioInfo> relatorioInfo = new List<RelatorioInfo>();


        public JsonResponse(HttpContext context)
        {

            applicationPath = context.Request.ApplicationPath;
            //helpPath = ConfigurationManager.AppSettings["DiretorioBaseHelp"];
            helpPath = "help";

            observacaoExtrato = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.ObservacaoExtrato;
            if (!string.IsNullOrEmpty(observacaoExtrato))
            {
                observacaoExtrato = Regex.Replace(observacaoExtrato, @"<[^>]*>", String.Empty).Replace("\n", "").Trim();
            }

            Financial.Security.PermissaoCliente permissaoCliente = new Financial.Security.PermissaoCliente();

            int? idCliente;
            string idClienteParam = context.Request["idCliente"];
            if (String.IsNullOrEmpty(idClienteParam))
            {
                idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);
            }
            else
            {
                idCliente = Int32.Parse(idClienteParam);
            }

            if (idCliente == null)
            {
                erros.Add("N�o existem clientes associados a este login.");
                this.success = false;
            }
            else
            {

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente.Value);
                DateTime dataProcCli = cliente.DataDia.Value;
                
                Financial.Common.Indice indice = new Financial.Common.Indice();

                indices = indice.RetornaDataTableIndicesComCotacoes(dataProcCli);

                EstruturaTipos estruturaTipos = new EstruturaTipos();
                foreach (KeyValuePair<ListaTipoFixa, FinancialDeskTipoAtivo> kvp in estruturaTipos.listaTiposNomeada)
                {
                    tipoAtivo.Add(kvp.Value);
                }


                
                this.nomeCliente = cliente.Nome;

                this.idCliente = cliente.IdCliente.Value;
                this.isCarteiraSimulada = (cliente.TipoControle == (byte)TipoControleCliente.CarteiraSimulada);

                Usuario usuario = new Usuario();
                usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

                if (!usuario.AcessaCliente(idCliente.Value))
                {
                    erros.Add("Este usu�rio n�o tem acesso a esta carteira");
                    this.success = false;
                }
                else
                {
                    this.acessaCarteiraSimulada = usuario.AcessaCarteiraSimulada();
                    travaCliente = (usuario.TipoTrava.Value == (int)Financial.Security.Enums.TipoTravaUsuario.TravaCliente);


                    DateTime dataClienteTmp = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;

                    if (!(status == (byte)StatusCliente.Divulgado || status == (byte)StatusCliente.Fechado))
                    {
                        dataClienteTmp = Calendario.SubtraiDiaUtil(dataClienteTmp, 1);
                    }

                    dataCliente = String.Format("{0}-{1}-{2}", dataClienteTmp.Year.ToString(), dataClienteTmp.Month.ToString(), dataClienteTmp.Day.ToString());

                    Carteira carteira = new Carteira();
                    if (!carteira.LoadByPrimaryKey(idCliente.Value))
                    {
                        erros.Add("N�o existem carteiras associadas a este login. Verificar cadastro do usu�rio.");
                        this.success = false;
                    }
                    else
                    {
                        idIndiceDefault = carteira.IdIndiceBenchmark.Value;
                        explodeFundos = carteira.ExplodeCotasDeFundos == "S" ? true : false;

                        #region Checa permissionamento aos relatorios
                        SiteMapNode rootNode = SiteMap.RootNode;

                        SiteMapNode rootNodeRelatorio = null;

                        foreach (SiteMapNode node in rootNode.ChildNodes)
                        {
                            if (Int32.Parse(node["menuID"]) == (int)Financial.Relatorio.Enums.MenuId.Relatorios)
                            {
                                rootNodeRelatorio = node;
                                break;
                            }
                        }

                        if (rootNodeRelatorio != null)
                        {
                            PopulaRelatorioInfo(context, rootNodeRelatorio, 0);
                        }
                        #endregion

                        this.success = true;
                    }
                }

            }
        }



        private void PopulaRelatorioInfo(HttpContext context, SiteMapNode parentNode, int level)
        {
            foreach (SiteMapNode node in parentNode.ChildNodes)
            {
                if (node.IsAccessibleToUser(context) && node["isRelatorio"] != "false")
                {
                    if (!node.HasChildNodes)
                    {
                        relatorioInfo.Add(new RelatorioInfo(node.Url, node.Title, true, level));
                    }
                    else
                    {
                        relatorioInfo.Add(new RelatorioInfo(node.Url, node.Title, false, level));

                        PopulaRelatorioInfo(context, node, level + 1);
                    }
                }
            }
        }
    }
}