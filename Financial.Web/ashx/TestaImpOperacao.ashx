<%@ WebHandler Language="C#" Class="TestaImpOperacao" %>

using System;
using System.Web;
using Financial.Investidor.Controller;
using Financial.Integracao.Excel;
using Financial.RendaFixa;
using Financial.Common;

public class TestaImpOperacao : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string msgerro;
        this.ImportaOperacaoRendaFixa(288, new DateTime(2013, 8, 27), "V", 200, 300, "123", 3, 3, 100, 50, 900, 222, out msgerro);
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World : " + DateTime.Now);
    }

    public void ImportaOperacaoRendaFixa(int IdCliente, DateTime DataOperacao, string TipoOperacao, decimal Quantidade, decimal PUOperacao, string CodigoIsin, byte IdCustodia, byte IdLiquidacao, decimal ValorCorretagem, int NumeroNota, decimal ValorISS, decimal Emolumento, out string MsgErro)
    {
        MsgErro = "";

        //this.InitConnection();

        
        ValoresExcelOperacaoRendaFixa operacaoExcel = new ValoresExcelOperacaoRendaFixa();

        operacaoExcel.IdCliente = IdCliente;
        operacaoExcel.DataOperacao = DataOperacao;
        if (TipoOperacao == "C")
        {
            operacaoExcel.TipoOperacao = "CompraFinal";
        }
        else if (TipoOperacao == "V")
        {
            operacaoExcel.TipoOperacao = "VendaFinal";
        }
        else
        {
            string msgErro = "Tipo de Opera��o n�o suportada: " + TipoOperacao;
            throw new Exception(msgErro);
        }

        operacaoExcel.Quantidade = Quantidade;
        operacaoExcel.PuOperacao = PUOperacao;
        operacaoExcel.Valor = Quantidade * PUOperacao;

        //Localizar titulo pelo CodigoIsin
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        if (!tituloRendaFixa.BuscaPorCodigoIsin(CodigoIsin))
        {
            string msgErro = "T�tulo com C�digo ISIN:" + CodigoIsin + " n�o encontrado";
            MsgErro = msgErro;
            return;
        }
        operacaoExcel.IdTitulo = tituloRendaFixa.IdTitulo.Value;

        operacaoExcel.IdCustodia = IdCustodia;
        operacaoExcel.IdLiquidacao = IdLiquidacao;
        operacaoExcel.ValorCorretagem = ValorCorretagem;
        operacaoExcel.NumeroNota = NumeroNota;
        operacaoExcel.ValorISS = ValorISS;
        operacaoExcel.Emolumento = Emolumento;

        int codigoBovespaDefault = Convert.ToInt32(Financial.Util.ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
        AgenteMercado agenteMercado = new AgenteMercado();
        int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

        operacaoExcel.IdAgenteCorretora = idAgenteMercado;


        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelOperacaoRendaFixa = new System.Collections.Generic.List<ValoresExcelOperacaoRendaFixa>();
        basePage.valoresExcelOperacaoRendaFixa.Add(operacaoExcel);

        basePage.CarregaOperacaoRendaFixa(false);


    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}