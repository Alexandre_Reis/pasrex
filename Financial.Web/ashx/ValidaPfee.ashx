<%@ WebHandler Language="C#" Class="ValidaPfee" %>

using System;
using System.Web;
using Financial.Fundo;

public class ValidaPfee : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
        calculoPerformanceHistoricoCollection.Query.OrderBy(calculoPerformanceHistoricoCollection.Query.DataHistorico.Descending);
        calculoPerformanceHistoricoCollection.Query.Load();

        System.Text.StringBuilder msg = new System.Text.StringBuilder();
        foreach (CalculoPerformanceHistorico calculoPerformanceHistorico in calculoPerformanceHistoricoCollection)
        {
            DateTime dataFimApropriacao = calculoPerformanceHistorico.DataFimApropriacao.Value;
            DateTime dataAnterior = Financial.Util.Calendario.SubtraiDiaUtil(dataFimApropriacao, 1);

            CalculoPerformanceHistorico calculoPerformanceHistoricoCheck = new CalculoPerformanceHistorico();
            if (calculoPerformanceHistoricoCheck.LoadByPrimaryKey(dataAnterior, calculoPerformanceHistorico.IdTabela.Value))
            {
                if (dataFimApropriacao != calculoPerformanceHistoricoCheck.DataFimApropriacao.Value && calculoPerformanceHistorico.ValorAcumulado != 0)
                {
                    msg.Append("Carteira " + calculoPerformanceHistorico.IdCarteira.Value.ToString() + " na data: " + dataFimApropriacao.ToShortDateString() + "<br />");
                }
            }
        }

        string mensagem = msg.ToString();
        
        context.Response.ContentType = "text/html";
        context.Response.Write("Resultado: <br />" + mensagem);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}