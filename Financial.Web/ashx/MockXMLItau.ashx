<%@ WebHandler Language="C#" Class="MockXMLItau" %>

using System;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

public class MockXMLItau : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        ItauMsg itaumsg = new ItauMsg();
        
        itaumsg.parameter.Add(new Param("AGENCIA", "990009", ParamTypes.number, 6));
        itaumsg.parameter.Add(new Param("NMCLI", "Cotista 503", ParamTypes.text, 30));
        itaumsg.parameter.Add(new Param("DDD", "011", ParamTypes.number, 4));
        itaumsg.parameter.Add(new Param("NMCLI6", "Vou capar o nome desse cara", ParamTypes.text, 6));
        itaumsg.parameter.Add(new Param("DDD6", "", ParamTypes.number, 6));
        
        //Nao se preocupar com as linhas abaixo:
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.OmitXmlDeclaration = true;

        MemoryStream ms = new MemoryStream();
        XmlWriter writer = XmlWriter.Create(ms, settings);

        XmlSerializerNamespaces names = new XmlSerializerNamespaces();
        names.Add("", "");

        XmlSerializer x = new XmlSerializer(itaumsg.GetType());
        
        x.Serialize(writer, itaumsg, names);
        
        ms.Flush();
        ms.Seek(0, SeekOrigin.Begin);
        StreamReader sr = new StreamReader(ms);
        string xml = sr.ReadToEnd();
        
        string response = xml.ToString();
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(response);
    }
 
    
    public bool IsReusable {
        get {
            return false;
        }
    }
}

[XmlRoot(ElementName = "itaumsg")]
public class ItauMsg
{
    public List<Param> parameter = new List<Param>();
    public ItauMsg()
    {
        
    }
}

public enum ParamTypes
{
    number,
    text
}

[XmlType(TypeName = "param")]
public class Param
{
    [XmlAttribute]
    public string id;

    [XmlAttribute]
    public string value;
    public Param()
    {
    }
    public Param(string idParam, string valueParam, ParamTypes paramType, int paramLength)
    {
        this.id = idParam.PadRight(11,' ');

        string formattedValue = "";
        if (valueParam.Length > paramLength)
        {
            //Nao eh necessario preencher com fillers, apenas capar no tamanho passado
            formattedValue = valueParam.Substring(0, paramLength);
        }
        else
        {
            //Precisamos preencher com 0s ou brancos
            if (paramType == ParamTypes.number)
            {
                formattedValue = valueParam.PadLeft(paramLength, '0');
            }
            else if (paramType == ParamTypes.text)
            {
                formattedValue = valueParam.PadRight(paramLength, ' ');
            }
            else
            {
                throw new Exception("Tipo de par�metro n�o suportado");
            }
        }
        
        this.value = formattedValue;
    }
}