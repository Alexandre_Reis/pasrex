<%@ WebHandler Language="C#" Class="ImportaFundosBaseCentral" %>

using System;
using System.Web;
using System.IO;
using Financial.Fundo;
using System.Collections.Generic;

public class ImportaFundosBaseCentral : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        StreamReader reader = new StreamReader(File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + "/ashx/fundos_ativos.csv"));
        List<string> listA = new List<string>();
        List<string> erros = new List<string>();
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(';');

            string codigoFundo = values[0];
            Carteira carteira = new Carteira();
            int idCarteira = Convert.ToInt32(codigoFundo);
            if (!carteira.LoadByPrimaryKey(idCarteira))
            {
                
                //Importar carteira
                try
                {
                    carteira.ImportaFundoSIAnbid(codigoFundo);
                }
                catch (Exception e)
                {
                    string message = codigoFundo + ";" + e.Message + ";" + e.InnerException + ";" + e.Source;
                    erros.Add(message);
                }
            }
            //importar apenas 1
            break;
        }
        
        context.Response.ContentType = "text/html";
        context.Response.Write("Fim:" + DateTime.Now + "<br />" + String.Join("<br />", erros.ToArray()));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}