<%@ WebHandler Language="C#" Class="TestaProxy" %>

using System;
using System.Web;
using System.Net;

public class TestaProxy : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        Uri uri = WebRequest.GetSystemWebProxy().GetProxy(new Uri("http://www.example.org/"));

        //IWebProxy proxy = WebRequest.GetSystemWebProxy();
        //IWebProxy proxy = new WebProxy("108.62.72.204", 11169);

        Uri exampleUri = new Uri("http://www.example.org/");
//WebProxy webProxy = new WebProxy(
        IWebProxy defaultProxy = WebRequest.GetSystemWebProxy();

        bool isBypassed = defaultProxy.IsBypassed(exampleUri);
        // ... false

        Uri proxyUri = defaultProxy.GetProxy(exampleUri);


        WebProxy proxyTry = (WebProxy)WebProxy.GetDefaultProxy();
        if (proxyTry.Address != null)
        {
            throw new Exception("here");
        }

        HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
        IWebProxy proxy = myWebRequest.Proxy;
        // Print the Proxy Url to the console.
        if (proxy != null)
        {
            string debug = proxy.GetProxy(myWebRequest.RequestUri).ToString();
        }

        //IWebProxy proxy = WebRequest.DefaultWebProxy;

        if (proxy != null)
        {
            //proxy.Credentials = CredentialCache.DefaultCredentials;
            //proxy.UseDefaultCredentials = true;

            ICredentials credentials = new NetworkCredential("efreitas", "password");
            proxy = new WebProxy(new Uri("http://108.62.72.204:11169"), false, null, credentials);

        }

        WebClient webClient = new WebClient();
        webClient.Proxy = proxy;
        //        string location = Server.MapPath("~/downloads");


        webClient.DownloadFile("http://oglobo2.globo.com/img/clubeSouMaisRio.jpg", @"c:\inetpub\wwwroot\financial\financial.web\downloads\file.jpg");

        context.Response.ContentType = "text/plain";
        context.Response.Write(webClient.DownloadString("http://whatismyipaddress.com//"));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}