<%@ WebHandler Language="C#" Class="AtivoCarteiraSimulada" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.Fundo;
using Newtonsoft.Json;


public class AtivoCarteiraSimulada : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        TabelaCarteiraSimuladaViewModel tabelaCarteiraSimuladaViewModel = new TabelaCarteiraSimuladaViewModel();

        string action = context.Request["action"];
        string pk = context.Request["pk"];

        JSONRetorno jsonRetorno = new JSONRetorno();
        if(action == "save"){
            try
            {
                tabelaCarteiraSimuladaViewModel = this.Save(context, pk);
            }
            catch (Exception e)
            {
                jsonRetorno.erros.Add(e.Message);
            }
        }
        else if (action == "read")
        {
            tabelaCarteiraSimuladaViewModel = this.Read(pk);
        }

        else if (action == "delete")
        {
            tabelaCarteiraSimuladaViewModel = this.Delete(pk);
        }

        jsonRetorno.success = (jsonRetorno.erros.Count == 0);
        jsonRetorno.data = tabelaCarteiraSimuladaViewModel;
        string json = JsonConvert.SerializeObject(jsonRetorno, Formatting.None);

        context.Response.ContentType = "application/json";
        context.Response.Write(json);
        
    }

    public class JSONRetorno
    {
        public bool success;
        public List<string> erros = new List<string>();
        public TabelaCarteiraSimuladaViewModel data;
    }
    
    private TabelaCarteiraSimulada LoadTabelaCarteiraSimulada(string pk)
    {
        TabelaCarteiraSimulada tabelaCarteiraSimulada = new TabelaCarteiraSimulada();
        
        string[] splittedPK = pk.Split('|');
        int idCarteira = Convert.ToInt32(splittedPK[0]);
        int tipoAtivo = Convert.ToInt32(splittedPK[1]);
        string codigoAtivo = splittedPK[2];
        
        tabelaCarteiraSimulada.LoadByPrimaryKey(idCarteira, tipoAtivo, codigoAtivo);
        return tabelaCarteiraSimulada;
    }
    private TabelaCarteiraSimuladaViewModel Read(string pk)
    {
        TabelaCarteiraSimulada tabelaCarteiraSimulada = this.LoadTabelaCarteiraSimulada(pk);
        TabelaCarteiraSimuladaViewModel tabelaCarteiraSimuladaViewModel = new TabelaCarteiraSimuladaViewModel(tabelaCarteiraSimulada);
        return tabelaCarteiraSimuladaViewModel;
    }

    private TabelaCarteiraSimuladaViewModel Delete(string pk)
    {
        TabelaCarteiraSimulada tabelaCarteiraSimulada = this.LoadTabelaCarteiraSimulada(pk);
        TabelaCarteiraSimuladaViewModel tabelaCarteiraSimuladaViewModel = new TabelaCarteiraSimuladaViewModel(tabelaCarteiraSimulada);
        tabelaCarteiraSimulada.MarkAsDeleted();
        tabelaCarteiraSimulada.Save();
        
        return tabelaCarteiraSimuladaViewModel;
    }
    
    private TabelaCarteiraSimuladaViewModel Save(HttpContext context, string pk)
    {
        TabelaCarteiraSimuladaViewModel tabelaCarteiraSimuladaViewModel = new TabelaCarteiraSimuladaViewModel();
        tabelaCarteiraSimuladaViewModel.CodigoAtivo = context.Request["codigoAtivo"];
        tabelaCarteiraSimuladaViewModel.TipoAtivo = Convert.ToInt32(context.Request["tipoAtivo"]);
        tabelaCarteiraSimuladaViewModel.IdCarteira = Convert.ToInt32(context.Request["idCarteira"]);

        Decimal.TryParse(context.Request["valor"], out tabelaCarteiraSimuladaViewModel.Valor);
                
        TabelaCarteiraSimulada tabelaCarteiraSimulada = new TabelaCarteiraSimulada();
        if (!string.IsNullOrEmpty(pk))
        {
            tabelaCarteiraSimulada = LoadTabelaCarteiraSimulada(pk);
        }
            
        tabelaCarteiraSimulada.CodigoAtivo = tabelaCarteiraSimuladaViewModel.CodigoAtivo;
        tabelaCarteiraSimulada.IdCarteira = tabelaCarteiraSimuladaViewModel.IdCarteira;
        tabelaCarteiraSimulada.TipoAtivo = tabelaCarteiraSimuladaViewModel.TipoAtivo;
        tabelaCarteiraSimulada.Valor = tabelaCarteiraSimuladaViewModel.Valor;
        tabelaCarteiraSimulada.Save();

        return tabelaCarteiraSimuladaViewModel;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

public class TabelaCarteiraSimuladaViewModel
{
    public string CodigoAtivo;
    public int IdCarteira;
    public decimal Valor;
    public int TipoAtivo;
    
    public TabelaCarteiraSimuladaViewModel(){
    
    }

    public TabelaCarteiraSimuladaViewModel(TabelaCarteiraSimulada tabelaCarteiraSimulada)
    {
        this.CodigoAtivo = tabelaCarteiraSimulada.CodigoAtivo;
        this.IdCarteira = tabelaCarteiraSimulada.IdCarteira.Value;
        this.Valor = tabelaCarteiraSimulada.Valor.Value;
        this.TipoAtivo = tabelaCarteiraSimulada.TipoAtivo.Value;
    }
}