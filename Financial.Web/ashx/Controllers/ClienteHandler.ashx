<%@ WebHandler Language="C#" Class="ClienteHandler" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.Fundo;
using Newtonsoft.Json;
using Financial.Investidor;

public class ClienteHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        string action = context.Request["action"];
        string pk = context.Request["pk"];

        JSONRetorno jsonRetorno = new JSONRetorno();
        if (action == "list")
        {
            List<ClienteViewModel> clienteViewModelList = this.List();
            jsonRetorno.collection = clienteViewModelList;
        }

        jsonRetorno.success = (jsonRetorno.erros.Count == 0);
        //string json = JsonConvert.SerializeObject(jsonRetorno, Formatting.None, new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter());
        string json = JsonConvert.SerializeObject(jsonRetorno, Formatting.None, new Newtonsoft.Json.Converters.IsoDateTimeConverter());

        context.Response.ContentType = "application/json";
        context.Response.Write(json);
    }

    public class JSONRetorno
    {
        public bool success;
        public List<string> erros = new List<string>();
        public List<ClienteViewModel> collection;
    }

    private List<ClienteViewModel> List()
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);
        List<ClienteViewModel> clienteViewModelList = new List<ClienteViewModel>();
        foreach (Financial.Investidor.Cliente cliente in clienteCollection)
        {
            Cliente clienteCompleto = new Cliente();
            clienteCompleto.LoadByPrimaryKey(cliente.IdCliente.Value);
            ClienteViewModel clienteViewModel = new ClienteViewModel(clienteCompleto);
            clienteViewModelList.Add(clienteViewModel);
        }
        return clienteViewModelList;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

public class ClienteViewModel
{
    public int IdCliente;
    public string Nome;
    public DateTime DataDia;
    
    public ClienteViewModel(){
    
    }

    public ClienteViewModel(Financial.Investidor.Cliente cliente)
    {
        this.IdCliente = cliente.IdCliente.Value;
        this.Nome = cliente.Apelido;
        this.DataDia = cliente.DataDia.Value;
    }
}