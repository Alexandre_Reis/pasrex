<%@ WebHandler Language="C#" Class="TrataIncorporacao" %>

using System;
using System.Web;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista;

public class TrataIncorporacao : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {

        IncorporacaoFundoCollection incorporacaoFundoCollection = new IncorporacaoFundoCollection();
        incorporacaoFundoCollection.LoadAll();

        foreach (IncorporacaoFundo incorporacaoFundo in incorporacaoFundoCollection)
        {
            int idCarteiraOrigem = incorporacaoFundo.IdCarteiraOrigem.Value;
            DateTime dataIncorporacao = incorporacaoFundo.Data.Value;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoTributacao);
            carteira.LoadByPrimaryKey(campos, idCarteiraOrigem);

            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Acoes ||
                carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Isento)
            {
                continue;
            }

            HistoricoIncorporacaoPosicaoCollection historicoIncorporacaoPosicaoCollection = new HistoricoIncorporacaoPosicaoCollection();
                
            PosicaoFundoAberturaCollection posicaoFundoAberturaCollection = new PosicaoFundoAberturaCollection();
            posicaoFundoAberturaCollection.Query.Where(posicaoFundoAberturaCollection.Query.IdCarteira.Equal(idCarteiraOrigem),
                                                       posicaoFundoAberturaCollection.Query.DataHistorico.Equal(dataIncorporacao));
            posicaoFundoAberturaCollection.Query.Load();

            foreach (PosicaoFundoAbertura posicaoFundoAbertura in posicaoFundoAberturaCollection)
            {
                HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = historicoIncorporacaoPosicaoCollection.AddNew();
                historicoIncorporacaoPosicao.DataConversao = posicaoFundoAbertura.DataConversao.Value;
                historicoIncorporacaoPosicao.DataIncorporacao = dataIncorporacao;
                historicoIncorporacaoPosicao.IdCarteiraDestino = incorporacaoFundo.IdCarteiraDestino.Value;
                historicoIncorporacaoPosicao.IdCarteiraOrigem = incorporacaoFundo.IdCarteiraOrigem.Value;
                historicoIncorporacaoPosicao.IdPosicao = posicaoFundoAbertura.IdPosicao.Value;
            }

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteiraOrigem),
                                                         posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(dataIncorporacao));
            posicaoCotistaAberturaCollection.Query.Load();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
            {
                HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = historicoIncorporacaoPosicaoCollection.AddNew();
                historicoIncorporacaoPosicao.DataConversao = posicaoCotistaAbertura.DataConversao.Value;
                historicoIncorporacaoPosicao.DataIncorporacao = dataIncorporacao;
                historicoIncorporacaoPosicao.IdCarteiraDestino = incorporacaoFundo.IdCarteiraDestino.Value;
                historicoIncorporacaoPosicao.IdCarteiraOrigem = incorporacaoFundo.IdCarteiraOrigem.Value;
                historicoIncorporacaoPosicao.IdPosicao = posicaoCotistaAbertura.IdPosicao.Value;
            }

            historicoIncorporacaoPosicaoCollection.Save();
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}