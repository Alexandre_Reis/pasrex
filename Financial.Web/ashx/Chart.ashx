<%@ WebHandler Language="C#" Class="ChartHandler" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using FinancialDesk.BO.Series;
using Financial.Security;

public class ChartHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        string[] dataParam = context.Request.Params["date"].Split('-');
        DateTime data = new DateTime(Convert.ToInt16(dataParam[0]), Convert.ToInt16(dataParam[1]), Convert.ToInt16(dataParam[2]));

        /*Financial.Security.PermissaoCliente permissaoCliente = new Financial.Security.PermissaoCliente();
        int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);*/
        int? idCliente = Int32.Parse(context.Request.Params["idCliente"]);
        bool? explodeFundos = bool.Parse(context.Request.Params["explodeFundos"]);

        explodeFundos = explodeFundos ?? false;

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

        if (!usuario.AcessaCliente(idCliente.Value))
        {
            throw new Exception("Este usu�rio n�o tem acesso a esta carteira");
        }
        
        Financial.Investidor.Cliente cliente = new Financial.Investidor.Cliente();
        cliente.LoadByPrimaryKey(idCliente.Value);

        Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
        data = carteira.RetornaDataReferencia(idCliente.Value, data);

        short idBenchmark = Convert.ToInt16(context.Request.Params["idindice"]);

        string chartType = context.Request.Params["chartType"];
        Chart chart = new Chart();

        try
        {
            if (chartType == "Alocacao")
            {
                SingleSerieValues serieAlocacao = new SingleSerieValues(idCliente.Value, data);
                serieAlocacao.MontaChartAlocacao((bool)explodeFundos);
                chart = serieAlocacao.Chart;
            }
            else if (chartType == "Liquidez")
            {
                SingleSerieValues serieLiquidez = new SingleSerieValues(idCliente.Value, data);
                serieLiquidez.MontaChartLiquidez((bool)explodeFundos);
                chart = serieLiquidez.Chart;
            }
            else if (chartType == "Gestor")
            {
                SingleSerieValues serieGestor = new SingleSerieValues(idCliente.Value, data);
                serieGestor.MontaChartGestor((bool)explodeFundos);
                chart = serieGestor.Chart;
            }
            else if (chartType == "Ativo")
            {
                SingleSerieValues serieAtivo = new SingleSerieValues(idCliente.Value, data);
                serieAtivo.MontaChartAtivo((bool)explodeFundos);
                chart = serieAtivo.Chart;
            }
            else if (chartType == "Industria")
            {
                SingleSerieValues serieIndustria = new SingleSerieValues(idCliente.Value, data);
                serieIndustria.MontaChartIndustria();
                chart = serieIndustria.Chart;
            }
            else if (chartType == "Risco")
            {
                SingleSerieValues serieRisco = new SingleSerieValues(idCliente.Value, data);
                serieRisco.MontaChartRisco();
                chart = serieRisco.Chart;

            }
            else if (chartType == "RetornoAcumulado")
            {
                MultipleSerieValues serieRetornoAcumulado = new MultipleSerieValues(idCliente.Value, data, idBenchmark);
                bool ok = serieRetornoAcumulado.MontaChartRetornoAcumulado();
                chart = serieRetornoAcumulado.Chart;
                //chart = null;
            }
            else if (chartType == "EvolucaoPL")
            {
                MultipleSerieValues serieEvolucaoPL = new MultipleSerieValues(idCliente.Value, data, idBenchmark);
                bool ok = serieEvolucaoPL.MontaChartEvolucaoPL(cliente.DataImplantacao.Value, data);
                chart = serieEvolucaoPL.Chart;
                //chart = null;
            }
            else if (chartType == "RetornoEfetivo")
            {
                MultipleSerieValues serieRetornoEfetivo = new MultipleSerieValues(idCliente.Value, data, idBenchmark);
                bool ok = serieRetornoEfetivo.MontaChartRetornoEfetivo(5, 1);
                chart = serieRetornoEfetivo.Chart;
            }
            else if (chartType == "RetornoMeses")
            {
                MultipleSerieValues serieRetornoMeses = new MultipleSerieValues(idCliente.Value, data, idBenchmark);
                bool ok = serieRetornoMeses.MontaChartRetornoMeses(12, false);
                chart = serieRetornoMeses.Chart;
            }
            else if (chartType == "Volatilidade")
            {
                MultipleSerieValues serieVolatilidade = new MultipleSerieValues(idCliente.Value, data, idBenchmark);
                bool ok = serieVolatilidade.MontaChartVolatilidade(20, 1);
                chart = serieVolatilidade.Chart;
            }

        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }

        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
        jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        string json = JsonConvert.SerializeObject(chart, Formatting.None, jsonSerializerSettings);

        context.Response.ContentType = "text/html";
        context.Response.Write(json);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}