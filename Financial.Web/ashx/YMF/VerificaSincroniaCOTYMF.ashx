<%@ WebHandler Language="C#" Class="VerificaSincroniaCOTYMF" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.Fundo;
using System.Data.SqlClient;
using Financial.Investidor;
using Financial.InvestidorCotista;
using System.Data;
using System.Text;
using Financial.Fundo.Enums;

public class VerificaSincroniaCOTYMF : IHttpHandler
{


    public void ProcessRequest(HttpContext context)
    {
        const int NAO_VALIDADO = -2;
        const int VALIDADO = -1;
        const string DELIMITADOR = ";";

        SqlConnection conn = new Carteira().CreateSqlConnectionYMFCOT();

        bool validaIR = false;
        bool excluiFIDCFechado = false;

        if (context.Request["validaIR"] == "1")
        {
            validaIR = true;
        }

        if (context.Request["excluiFIDCFechado"] == "1")
        {
            excluiFIDCFechado = true;
        }


        CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(false);

        string output = "";
        List<string> fundosComErro = new List<string>();

        List<int> fundosIgnorados = new List<int>();
        /*fundosIgnorados.Add(9907);
        fundosIgnorados.Add(7893);
        fundosIgnorados.Add(10243);
        fundosIgnorados.Add(9926);
        fundosIgnorados.Add(9933);
        fundosIgnorados.Add(9112);
        fundosIgnorados.Add(9788);
        fundosIgnorados.Add(9790);
        */

        int countFundo = 0;
        foreach (Carteira fundo in fundos)
        {
            countFundo++;

            if (fundosIgnorados.Contains(fundo.IdCarteira.Value))
            {
                continue;
            }

            if (excluiFIDCFechado && fundo.IsFDIC() && fundo.TipoFDIC == TipoFDIC.Fechado)
            {
                continue;
            }

            
            int idCliente = fundo.IdCarteira.Value;
            //idCliente = 7893;//, 9922, 9933, 9788
            //idCliente = 9249;//sem posicao na YMF

            //idCliente = 9950;//parece cancelado na ymf
            //idCliente = 9241;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            
            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(idCliente);
            string codFundo = clienteInterface.CodigoYMF;

            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            dataTable.TableName = "COT_POSICAO_NOTA_HISTORICO";

            cmd.CommandText = String.Format(
                "select * from {0} where cd_fundo = '{1}' and dt_posicao = '{2}'",
                dataTable.TableName, codFundo, cliente.DataDia.Value.ToString("yyyy-MM-dd"));

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);


            //Jogar posicoes YMF numa lista de PosicaoCotistaValidar pra facilitar comparacao
            List<PosicaoCotista> posicoesYMF = new List<PosicaoCotista>();
            CotistaCollection cotistaCollection = new CotistaCollection();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                string cdCotista = (string)dataRow["CD_COTISTA"];
                Cotista cotista = cotistaCollection.BuscaCotistaPorCodigoInterface(cdCotista);

                PosicaoCotista posicaoYMF = new PosicaoCotista();
                if (cotista != null)
                {
                    posicaoYMF.IdCotista = cotista.IdCotista;
                }
                posicaoYMF.DataAplicacao = (DateTime)dataRow["DT_APLICACAO"];
                posicaoYMF.ValorAplicacao = Convert.ToDecimal(dataRow["VL_APLICACAO"]);
                posicaoYMF.CotaAplicacao = Convert.ToDecimal(dataRow["VL_COTA_APLICACAO"]);
                posicaoYMF.Quantidade = Convert.ToDecimal(dataRow["QT_COTAS"]);
                posicaoYMF.ValorBruto = Convert.ToDecimal(dataRow["VL_BRUTO"]);
                posicaoYMF.ValorIR = Convert.ToDecimal(dataRow["VL_IR"]);
                posicaoYMF.ValorIOF = Convert.ToDecimal(dataRow["VL_IOF"]);
                posicaoYMF.ValorPerformance = Convert.ToDecimal(dataRow["VL_PERFORMANCE"]);
                posicaoYMF.ValorIOFVirtual = Convert.ToDecimal(dataRow["VL_IOF_VIRTUAL"]);
                posicaoYMF.IdPosicao = NAO_VALIDADO;
                posicoesYMF.Add(posicaoYMF);
            }
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCliente));
            posicaoCotistaCollection.Load(posicaoCotistaCollection.Query);
            foreach (PosicaoCotista posicaoFinancial in posicaoCotistaCollection)
            {
                posicaoFinancial.IdPosicao = NAO_VALIDADO;

                //Tentar localizacao posicao identica na YMF
                foreach (PosicaoCotista posicaoYMF in posicoesYMF)
                {
                    if (posicaoYMF.IdPosicao == NAO_VALIDADO &&
                        posicaoYMF.IdCotista == posicaoFinancial.IdCotista &&
                        posicaoYMF.DataAplicacao == posicaoFinancial.DataAplicacao)
                    {
                        if (posicaoYMF.Quantidade == posicaoFinancial.Quantidade &&
                            (!validaIR || (Math.Abs(posicaoYMF.ValorIR.Value - posicaoFinancial.ValorIR.Value) < 0.10M))
                            // posicaoYMF.ValorBruto == posicaoFinancial.ValorBruto// &&
                            //(Math.Abs(posicaoYMF.ValorIR.Value - posicaoFinancial.ValorIR.Value) < 0.10M) &&
                            //posicaoYMF.ValorIOF == posicaoFinancial.ValorIOF //&&
                            //posicaoYMF.ValorPerformance == posicaoFinancial.ValorPerformance &&
                            //posicaoYMF.ValorIOFVirtual == posicaoFinancial.ValorIOFVirtual
                        )
                        {
                            posicaoYMF.IdPosicao = VALIDADO;
                            posicaoFinancial.IdPosicao = VALIDADO;
                            break; //S� podemos validar UM FUNDO DE cada vez... Se n�o saimos fora, podemos processar duas vezes uma operacao parecida (mesma quantidade, mesmo dia, mesmo cotista, etc.)
                        }
                        else
                        {
                            bool achouAlgoDiferente = true;
                        }
                    }
                }
                if (posicaoFinancial.IdPosicao == NAO_VALIDADO)
                {
                    bool esseNaoValidou = true;
                }
            }

            cmd.Connection.Close();

            //Verificar se ficou algum nao validado
            StringBuilder csvYMF = new StringBuilder();
            bool faltouValidar = false;
            foreach (PosicaoCotista posicaoYMF in posicoesYMF)
            {
                if (posicaoYMF.IdPosicao != VALIDADO)
                {
                    faltouValidar = true;
                    csvYMF.AppendLine(String.Format("{0}{10}{1}{10}{2}{10}{3}{10}{4}{10}{5}{10}{6}{10}{7}{10}{8}{10}{9}{10}",
                        posicaoYMF.IdCotista,
                        posicaoYMF.DataAplicacao,
                        posicaoYMF.ValorAplicacao,
                        posicaoYMF.CotaAplicacao,
                        posicaoYMF.Quantidade,
                        posicaoYMF.ValorBruto,
                        posicaoYMF.ValorIR,
                        posicaoYMF.ValorIOF,
                        posicaoYMF.ValorPerformance,
                        posicaoYMF.ValorIOFVirtual,
                        DELIMITADOR
                    ));
                }
            }

            StringBuilder csvFinancial = new StringBuilder();
            foreach (PosicaoCotista posicaoFinancial in posicaoCotistaCollection)
            {
                if (posicaoFinancial.IdPosicao != VALIDADO)
                {
                    faltouValidar = true;
                    csvFinancial.AppendLine(String.Format("{0}{10}{1}{10}{2}{10}{3}{10}{4}{10}{5}{10}{6}{10}{7}{10}{8}{10}{9}{10}",
                posicaoFinancial.IdCotista,
                posicaoFinancial.DataAplicacao,
                posicaoFinancial.ValorAplicacao,
                posicaoFinancial.CotaAplicacao,
                posicaoFinancial.Quantidade,
                posicaoFinancial.ValorBruto,
                posicaoFinancial.ValorIR,
                posicaoFinancial.ValorIOF,
                posicaoFinancial.ValorPerformance,
                posicaoFinancial.ValorIOFVirtual,
                DELIMITADOR
            ));
                }
            }


            if (faltouValidar)
            {
                fundosComErro.Add(fundo.IdCarteira.Value + ": " + cliente.Nome + "(" + codFundo + ") implanta��o: " + cliente.DataImplantacao.Value.Date + " data dia: " + cliente.DataDia.Value.Date);

                if (false)
                {
                    continue;
                }

                string header = String.Format("IdCotista{0}DataAplicacao{0}ValorAplicacao{0}CotaAplicacao{0}Quantidade{0}ValorBruto{0}ValorIR{0}ValorIOF{0}ValorPerformance{0}ValorIOFVirtual\n", DELIMITADOR);
                output += "\n\nCliente: " + cliente.IdCliente + " " + cliente.Nome + " codymf:" + clienteInterface.CodigoYMF + " " + countFundo + "/" + fundos.Count +
                    ";;;;;;;;;\nYMF\n" + header + csvYMF.ToString() + "\nFinancial\n" + header + csvFinancial.ToString() + "\n\n";


                /*context.Response.Clear();
                context.Response.ClearContent();
                context.Response.ClearHeaders();

                context.Response.ContentType = "text/csv";
                context.Response.ContentEncoding = System.Text.Encoding.Unicode;
                context.Response.AddHeader("Content-Disposition", "attachment; filename=verifica_cot.csv");
                context.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                string header = String.Format("IdCotista{0}DataAplicacao{0}ValorAplicacao{0}CotaAplicacao{0}Quantidade{0}ValorBruto{0}ValorIR{0}ValorIOF{0}ValorPerformance{0}ValorIOFVirtual\n", DELIMITADOR);
                context.Response.Write("Cliente: " + cliente.IdCliente + " " + cliente.Nome + " " + countFundo + "/" + fundos.Count +
                    ";;;;;;;;;\nYMF\n" + header + csvYMF.ToString() + "\nFinancial\n" + header + csvFinancial.ToString());
                return;*/
            }

        }

        context.Response.ContentType = "text/html";

        if (fundosComErro.Count > 0)
        {
            if (false)
            {
                context.Response.Write("Fundos com erro:" + fundosComErro.Count + "<br /><br />" + String.Join("<br />", fundosComErro.ToArray()));
            }
            else
            {
                context.Response.Clear();
                context.Response.ClearContent();
                context.Response.ClearHeaders();

                context.Response.ContentType = "text/csv";
                context.Response.ContentEncoding = System.Text.Encoding.Unicode;
                context.Response.AddHeader("Content-Disposition", "attachment; filename=verifica_posicao.csv");
                context.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                context.Response.Write(output);

            }
        }
        else
        {
            context.Response.Write("Nenhum erro encontrado");
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}