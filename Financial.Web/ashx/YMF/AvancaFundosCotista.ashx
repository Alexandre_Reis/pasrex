<%@ WebHandler Language="C#" Class="AvancaFundosCotista" %>

using System;
using Financial.Fundo;
using System.Web;
using Financial.Investidor;
using Financial.Processamento;
using Financial.Investidor.Controller;

public class AvancaFundosCotista : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {


        Financial.Fundo.CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(true);

        
        foreach (Carteira fundo in fundos)
        {
            
            //Encontrar cota mais recente do fundo
            DateTime dataUltimaCota = new HistoricoCota().BuscaDataMaxCota(fundo.IdCarteira.Value).Value;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataUltimaCota, fundo.IdCarteira.Value);
            
            //Checar se a cota fechamento � maior do que zero (impedir levar pra uma data que tenha uma cota de fundo aberto na YMF)
            if(historicoCota.CotaFechamento == 0){
                continue;    
            }
            
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(fundo.IdCarteira.Value);
            
            //Checar se essa data � maior do que a data do cliente
            if (dataUltimaCota.Date <= cliente.DataDia.Value.Date)
            {
                continue;
            }

            ParametrosProcessamento parametrosProcessamento = new ParametrosProcessamento();
            parametrosProcessamento.ProcessaBolsa = true;
            parametrosProcessamento.ProcessaBMF = true;
            parametrosProcessamento.ProcessaRendaFixa = true;
            parametrosProcessamento.ProcessaSwap = true;

            parametrosProcessamento.IntegraBolsa = false;
            parametrosProcessamento.IntegraBTC = false;
            parametrosProcessamento.IntegraBMF = false;
            parametrosProcessamento.IntegraCC = false;
            parametrosProcessamento.IgnoraOperacoes = false;
            parametrosProcessamento.MantemFuturo = false;
            
            ProcessoPeriodo processoPeriodo = new ProcessoPeriodo();
            processoPeriodo.ProcessaPeriodo(fundo.IdCarteira.Value, 
                Financial.Investidor.Enums.TipoProcessamento.AvancoPeriodo, 
                parametrosProcessamento, 
                dataUltimaCota);
        }
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("Fundos atualizados com sucesso");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}