<%@ WebHandler Language="C#" Class="VerificaNumeroOperacoesYMF" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.Fundo;
using System.Data.SqlClient;
using Financial.Investidor;
using Financial.InvestidorCotista;
using System.Data;
using System.Text;

public class VerificaNumeroOperacoesYMF : IHttpHandler
{

    public class ComparaOperacao{
        public int NumeroOperacoesYMF;
        public int NumeroOperacoesFinancial;
        public string InfoFundo;
        public ComparaOperacao()
        {
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        SqlConnection conn = new Carteira().CreateSqlConnectionYMFCOT();

        CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(false);

        List<ComparaOperacao> infoOperacoes = new List<ComparaOperacao>();
        
        foreach (Carteira fundo in fundos)
        {
            
            int idCliente = fundo.IdCarteira.Value;
            //idCliente = 9757;
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(idCliente);
            string codFundo = clienteInterface.CodigoYMF;

            OperacaoCotistaCollection operacoesFinancial = new OperacaoCotistaCollection();
            operacoesFinancial.Query.Where(operacoesFinancial.Query.IdCarteira == idCliente);
            operacoesFinancial.Load(operacoesFinancial.Query);
            ComparaOperacao infoOperacao = new ComparaOperacao();
            infoOperacao.InfoFundo = idCliente + " " + cliente.Nome + "(" + codFundo + ") data implantação: " + cliente.DataImplantacao.Value.Date + " data dia: " + cliente.DataDia.Value.Date;
            infoOperacao.NumeroOperacoesFinancial = operacoesFinancial.Count;
            
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            dataTable.TableName = "COT_MOVIMENTO";

            cmd.CommandText = String.Format(
                "select * from {0} where cd_fundo = '{1}' and CD_TIPO not in ('RD', 'RJ', 'TR', 'RA', 'VR') ",
                dataTable.TableName, codFundo);

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);

            infoOperacao.NumeroOperacoesYMF = dataTable.Rows.Count;

            infoOperacoes.Add(infoOperacao);
            
        }

        string output = "";
        int numeroErros = 0;
        foreach (ComparaOperacao operacao in infoOperacoes)
        {
            if (operacao.NumeroOperacoesFinancial != operacao.NumeroOperacoesYMF)
            {
                numeroErros++;
                output += operacao.NumeroOperacoesFinancial + " " + operacao.NumeroOperacoesYMF + " - " + operacao.InfoFundo + " <br />";
            }
        }
        
        context.Response.ContentType = "text/html";

        
            context.Response.Write("num erros: " + numeroErros + "<br/ ><br/ >" + output);
        

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}