<%@ WebHandler Language="C#" Class="CalculaCotaTIR" %>

using System;
using System.Web;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Fundo.Controller;
using Financial.Investidor.Enums;
using Financial.Util;

public class CalculaCotaTIR : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {


        ClienteQuery clienteQuery = new ClienteQuery("C");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        
        clienteQuery.Select(clienteQuery.IdCliente,
                            clienteQuery.Status,
                            clienteQuery.DataDia);
        clienteQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.Completo),
                           clienteQuery.IdCliente.NotEqual(69));
        clienteQuery.OrderBy(clienteQuery.IdCliente.Ascending);

        clienteQuery.es.Distinct = true;

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        foreach (Cliente cliente in coll)
        {
            int idCliente = cliente.IdCliente.Value;
            
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.DataHistorico);
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente));
            posicaoFundoHistoricoCollection.Query.OrderBy(posicaoFundoHistoricoCollection.Query.DataHistorico.Ascending);
            posicaoFundoHistoricoCollection.Query.es.Distinct = true;
            posicaoFundoHistoricoCollection.Query.Load();

            if (posicaoFundoHistoricoCollection.Count == 0)
            {
                continue;
            }

            DateTime dataInicio = posicaoFundoHistoricoCollection[0].DataHistorico.Value;

            DateTime dataDia = cliente.DataDia.Value;
            DateTime dataAux = dataInicio;

            while (dataAux <= dataDia || (dataAux == dataDia && cliente.Status.Value == (byte)StatusCliente.Divulgado))
            {
                ControllerFundo c = new ControllerFundo();
                c.CalculaCotaPorTIR(idCliente, dataAux, true);

                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
            }
        }
        
        //string mensagem = msg.ToString();
        
        //context.Response.ContentType = "text/html";
        //context.Response.Write("Resultado: <br />" + mensagem);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}