<%@ WebHandler Language="C#" Class="VerificaCotacaoDebenture" %>

using System;
using System.Web;
using Financial.RendaFixa;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

public class VerificaCotacaoDebenture : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
    //    carregar todo cotacaomercadodebenture in memory com <codigo papel>
//carregar in memory tabela de titulo com <idtitulo, codigopapel>, apenas para titulo papel debenture
//varrer posicaorendafixahistorico, tentar encontrar idtitulo na tabela de , tentar encontrar 
//pra cada idtitulo cujo tipo papel in lista debenture, carregar dictionary com <idtitulo, <

        CotacaoMercadoDebentureCollection cotacoes = new CotacaoMercadoDebentureCollection();
        cotacoes.LoadAll();

        List<string> listaCotacoes = new List<string>();
        foreach (CotacaoMercadoDebenture cotacao in cotacoes)
        {
            listaCotacoes.Add(cotacao.CodigoPapel + "#" + cotacao.DataReferencia.Value.ToString("yyyy-MM-dd"));
      
        }

        TituloRendaFixaCollection titulos = new TituloRendaFixaCollection();
        titulos.LoadAll();

        Dictionary<int, string> tituloCodigoPapel = new Dictionary<int, string>();
        foreach (TituloRendaFixa titulo in titulos)
        {
            if (titulo.IdPapel == 58 || titulo.IdPapel == 2 || titulo.IdPapel == 3)
            {
                string[] descricaoTokens = titulo.Descricao.Split(' ');
                if (descricaoTokens.Length == 0)
                {
                    throw new Exception("debenture sem desc: " + titulo.Descricao);
                }

                string codigoPapel = descricaoTokens[descricaoTokens.Length - 1].Replace("(", "").Replace(")", "").Trim();
                tituloCodigoPapel.Add(titulo.IdTitulo.Value, codigoPapel);
            }
        }
        
        PosicaoRendaFixaHistoricoCollection posicoes = new PosicaoRendaFixaHistoricoCollection();
        posicoes.LoadAll();

        List<string> erros = new List<string>();
        foreach (PosicaoRendaFixaHistorico posicao in posicoes)
        {
            if (!tituloCodigoPapel.ContainsKey(posicao.IdTitulo.Value))
            {
                continue;
            }

            string codigoPapel = tituloCodigoPapel[posicao.IdTitulo.Value];
            string element = codigoPapel + "#" + posicao.DataHistorico.Value.ToString("yyyy-MM-dd");
            if (!listaCotacoes.Contains(element))
            {
                erros.Add(element);
            }
            else
            {
                string debug = "";
            }
        }
        
        context.Response.ContentType = "text/html";
        context.Response.Write("Resultado:<br />" + String.Join("<br />" , erros.ToArray()));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}