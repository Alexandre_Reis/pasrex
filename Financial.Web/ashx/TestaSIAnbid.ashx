<%@ WebHandler Language="C#" Class="TestaSIAnbid" %>

using System;
using System.Web;
using Financial.Fundo;
using Financial.Interfaces.Import.RendaFixa;
using System.Data;
using System.Collections.Generic;

public class TestaSIAnbid : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            bool useProxy = (context.Request["useproxy"] == "1");

            Financial.Interfaces.SIAnbidWebService.SIAnbid service = new Financial.Interfaces.SIAnbidWebService.SIAnbid(); ;
            service.Url = "http://atatika.financialonline.com.br/Financial.WebServices.SIAnbid/Service.asmx";

            if (useProxy)
            {
                /*System.Net.NetworkCredential nc = new System.Net.NetworkCredential("hsimpson", "duffbeer", "homersdomain");
                System.Net.WebProxy proxy = new System.Net.WebProxy("http://192.168.0.1:80");
                 //System.Net.WebProxy proxy = new System.Net.WebProxy("http://192.168.0.1", 80);
                proxy.Credentials = nc;
                service.Credentials = nc;
                service.Proxy = proxy;*/

                /*
                 service.Proxy = new WebProxy(); 
                 service.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                */

                /*
                  service.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                */

                service.Proxy = System.Net.WebProxy.GetDefaultProxy();
                service.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //service.Credentials = System.Net.CredentialCache.DefaultCredentials;//Talvez desnecessario!!!
            }

            string selectColumns = "codfundo,cnpj,fantasia";

            DataTable dataTable = service.BuscaFundos("", "advis", "", selectColumns);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);

            context.Response.ContentType = "text/html";
            context.Response.Write(json);

        }
        catch (Exception e)
        {
            context.Response.ContentType = "text/html";
            context.Response.Write(e.Message + "<br />" + e.StackTrace + "<br />" + e.InnerException + "<br />" + e.TargetSite + "<br />"
                + e.Source);
        }
        

        /*carteira.LoadByPrimaryKey(9000);
        carteira.ImportaCotasSIAnbid();*/

        /*string cnpj = context.Request["cnpj"];
        string codFundo = context.Request["codfundo"];
        string nomeFantasia = context.Request["nomeFantasia"];
        string selectColumns = context.Request["selectcolumns"];

        SIAnbid sianbid = new SIAnbid();
        System.Data.DataSet dataset = sianbid.BuscaFundos(cnpj, nomeFantasia, codFundo, selectColumns);

        List<string> retorno = new List<string>();
        if (dataset != null && dataset.Tables.Count > 0)
        {
            DataTable table = dataset.Tables[0];
            foreach (DataRow row in table.Rows)
            {
                retorno.Add(String.Format("Anbid: {0} - CNPJ: {1} - {2}", row["codfundo"], row["cnpj"], row["fantasia"]));
            }
        }

        context.Response.ContentType = "text/html";
        context.Response.Write(String.Join("<br />", retorno.ToArray()));
*/
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}