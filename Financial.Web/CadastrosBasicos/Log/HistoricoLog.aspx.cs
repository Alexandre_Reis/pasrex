﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;

public partial class CadastrosBasicos_HistoricoLog : CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasEditControl = false;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSHistoricoLog_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        HistoricoLogCollection coll = new HistoricoLogCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        
        if (textDataInicio.Text != "") 
        {
            DateTime dataInicio = new DateTime(Convert.ToDateTime(textDataInicio.Text).Year, Convert.ToDateTime(textDataInicio.Text).Month, Convert.ToDateTime(textDataInicio.Text).Day);
            
            coll.Query.Where(coll.Query.DataInicio.GreaterThan(dataInicio));
        }

        if (textDataFim.Text != "") 
        {
            DateTime dataFim = new DateTime(Convert.ToDateTime(textDataFim.Text).Year, Convert.ToDateTime(textDataFim.Text).Month, Convert.ToDateTime(textDataFim.Text).Day);
            dataFim = Calendario.AdicionaDiaUtil(dataFim, 1);
            coll.Query.Where(coll.Query.DataFim.LessThan(dataFim));
        }

        if (ConfigurationManager.AppSettings["Cliente"] == "Itau")
        {
            coll.Query.es.Top = 500000;
        }
        else
        {
            coll.Query.OrderBy(coll.Query.DataInicio.Descending);
        }

        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {        
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {        
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text);
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text);
        }
        
        labelFiltro.Text = texto.ToString();
    }
        
}