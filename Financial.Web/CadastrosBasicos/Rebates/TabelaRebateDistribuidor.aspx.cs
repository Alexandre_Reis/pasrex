﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Captacao;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaRebateDistribuidor : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaRebateDistribuidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaRebateDistribuidorQuery tabelaRebateDistribuidorQuery = new TabelaRebateDistribuidorQuery("T");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        tabelaRebateDistribuidorQuery.Select(tabelaRebateDistribuidorQuery, carteiraQuery.Apelido.As("Apelido"),
                                             agenteMercadoQuery);
        tabelaRebateDistribuidorQuery.InnerJoin(carteiraQuery).On(tabelaRebateDistribuidorQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaRebateDistribuidorQuery.InnerJoin(agenteMercadoQuery).On(tabelaRebateDistribuidorQuery.IdAgenteDistribuidor == agenteMercadoQuery.IdAgente);

        tabelaRebateDistribuidorQuery.OrderBy(tabelaRebateDistribuidorQuery.DataReferencia.Ascending,
                                          agenteMercadoQuery.Nome.Ascending,
                                          carteiraQuery.Apelido.Ascending,
                                          tabelaRebateDistribuidorQuery.Faixa.Ascending);

        TabelaRebateDistribuidorCollection coll = new TabelaRebateDistribuidorCollection();
        coll.Load(tabelaRebateDistribuidorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoDistribuidor.Equal("S"));
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgenteDistribuidor_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropAgenteDistribuidor = gridCadastro.FindEditFormTemplateControl("dropAgenteDistribuidor") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropAgenteDistribuidor);
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(textFaixa);
        controles.Add(textPercentualRebateAdministracao);
        controles.Add(textPercentualRebatePerformance);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idAgenteDistribuidor = Convert.ToInt32(dropAgenteDistribuidor.SelectedItem.Value);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            decimal faixa = Convert.ToDecimal(textFaixa.Text);

            TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();
            if (tabelaRebateDistribuidor.LoadByPrimaryKey(dataReferencia, idCarteira, idAgenteDistribuidor, faixa)) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxComboBox dropAgenteDistribuidor = gridCadastro.FindEditFormTemplateControl("dropAgenteDistribuidor") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idAgenteDistribuidor = Convert.ToInt32(dropAgenteDistribuidor.SelectedItem.Value);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();

        if (tabelaRebateDistribuidor.LoadByPrimaryKey(dataReferencia, idCarteira, idAgenteDistribuidor, faixa)) {
            tabelaRebateDistribuidor.PercentualRebateAdministracao = percentualRebateAdministracao;
            tabelaRebateDistribuidor.PercentualRebatePerformance = percentualRebatePerformance;
            tabelaRebateDistribuidor.Save();
            
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaRebateDistribuidor - Operacao: Update TabelaRebateDistribuidor: " + dataReferencia + "; " + idCarteira + "; " + idAgenteDistribuidor + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateDistribuidor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxComboBox dropAgenteDistribuidor = gridCadastro.FindEditFormTemplateControl("dropAgenteDistribuidor") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idAgenteDistribuidor = Convert.ToInt32(dropAgenteDistribuidor.SelectedItem.Value);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();

        tabelaRebateDistribuidor.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaRebateDistribuidor.IdAgenteDistribuidor = idAgenteDistribuidor;
        tabelaRebateDistribuidor.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaRebateDistribuidor.Faixa = Convert.ToDecimal(textFaixa.Text);
        tabelaRebateDistribuidor.PercentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        tabelaRebateDistribuidor.PercentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);
        tabelaRebateDistribuidor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaRebateDistribuidor - Operacao: Insert TabelaRebateDistribuidor: " + tabelaRebateDistribuidor.DataReferencia + "; " + tabelaRebateDistribuidor.IdCarteira + "; " + tabelaRebateDistribuidor.IdAgenteDistribuidor + "; " + tabelaRebateDistribuidor.Faixa + UtilitarioWeb.ToString(tabelaRebateDistribuidor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesIdAgenteDistribuidor = gridCadastro.GetSelectedFieldValues(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor);
            List<object> keyValuesFaixa = gridCadastro.GetSelectedFieldValues(TabelaRebateDistribuidorMetadata.ColumnNames.Faixa);
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);
                int idAgenteDistribuidor = Convert.ToInt32(keyValuesIdAgenteDistribuidor[i]);
                decimal faixa = Convert.ToDecimal(keyValuesFaixa[i]);

                TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();
                if (tabelaRebateDistribuidor.LoadByPrimaryKey(data, idCarteira, idAgenteDistribuidor, faixa)) {

                    TabelaRebateDistribuidor tabelaRebateDistribuidorClone = (TabelaRebateDistribuidor)Utilitario.Clone(tabelaRebateDistribuidor);
                    //
                    
                    tabelaRebateDistribuidor.MarkAsDeleted();
                    tabelaRebateDistribuidor.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaRebateDistribuidor - Operacao: Delete TabelaRebateDistribuidor: " + data + "; " + idCarteira + "; " + idAgenteDistribuidor + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateDistribuidorClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira));
            string idAgenteDistribuidor = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor));
            string faixa = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateDistribuidorMetadata.ColumnNames.Faixa));
            e.Value = data + idCarteira + idAgenteDistribuidor + faixa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira", "textPercentualRebateAdministracao");
        base.gridCadastro_PreRender(sender, e);
    }
}