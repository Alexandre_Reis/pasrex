﻿using Financial.Web.Common;
using System;
using DevExpress.Web;
using Financial.Captacao;
using Financial.Common;
using Financial.Fundo;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Security;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.CRM;

public partial class CadastrosBasicos_TabelaRebateOfficer : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupPessoa = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaRebateOfficer_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
        OfficerQuery officerQuery = new OfficerQuery("O");
        PessoaQuery pessoaQuery = new PessoaQuery("P");

        tabelaRebateOfficerQuery.Select(tabelaRebateOfficerQuery,
                                      pessoaQuery.Apelido.As("Apelido"),
                                      officerQuery);
        //
        tabelaRebateOfficerQuery.InnerJoin(pessoaQuery).On(tabelaRebateOfficerQuery.IdPessoa == pessoaQuery.IdPessoa);
        tabelaRebateOfficerQuery.InnerJoin(officerQuery).On(tabelaRebateOfficerQuery.IdOfficer == officerQuery.IdOfficer);
        //
        tabelaRebateOfficerQuery.OrderBy(tabelaRebateOfficerQuery.DataReferencia.Ascending,
                                           officerQuery.Nome.Ascending,
                                           pessoaQuery.Apelido.Ascending);

        TabelaRebateOfficerCollection coll = new TabelaRebateOfficerCollection();
        coll.Load(tabelaRebateOfficerQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOfficer_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        OfficerCollection coll = new OfficerCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region Disable Primary Keys in Edit Mode
    protected void textDataReferencia_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void dropOfficer_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void textPercentualRebate_Load(object sender, EventArgs e) {
        (sender as ASPxSpinEdit).DecimalPlaces = 2;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                nome = pessoa.Apelido;
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropOfficer = gridCadastro.FindEditFormTemplateControl("dropOfficer") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textPercentualRebate = gridCadastro.FindEditFormTemplateControl("textPercentualRebate") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropOfficer);
        controles.Add(btnEditCodigoPessoa);
        controles.Add(textDataReferencia);
        controles.Add(textPercentualRebate);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idOfficer = Convert.ToInt32(dropOfficer.SelectedItem.Value);
            int idPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);

            TabelaRebateOfficer tabelaRebateOfficer = new TabelaRebateOfficer();
            if (tabelaRebateOfficer.LoadByPrimaryKey(dataReferencia, idPessoa, idOfficer))
            {
                e.Result = "Registro já existente";
            }
        }
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaRebateOfficerMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdPessoa = gridCadastro.GetSelectedFieldValues(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa);
            List<object> keyValuesIdOfficer = gridCadastro.GetSelectedFieldValues(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer);
            //
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idPessoa = Convert.ToInt32(keyValuesIdPessoa[i]);
                int idOfficer = Convert.ToInt32(keyValuesIdOfficer[i]);

                TabelaRebateOfficer tabelaRebateOfficer = new TabelaRebateOfficer();
                if (tabelaRebateOfficer.LoadByPrimaryKey(data, idOfficer, idPessoa))
                {

                    TabelaRebateOfficer tabelaRebateOfficerClone = (TabelaRebateOfficer)Utilitario.Clone(tabelaRebateOfficer);
                    
                    tabelaRebateOfficer.MarkAsDeleted();
                    tabelaRebateOfficer.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaRebateOfficer - Operacao: Delete TabelaRebateOfficer: " + data + "; " + idPessoa + "; " + idOfficer + UtilitarioWeb.ToString(tabelaRebateOfficerClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Salvar(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        this.Salvar(false);
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        Salvar(true);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Salvar(bool insert)
    {
        ASPxComboBox dropOfficer = gridCadastro.FindEditFormTemplateControl("dropOfficer") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textPercentualRebate = gridCadastro.FindEditFormTemplateControl("textPercentualRebate") as ASPxSpinEdit;

        int idOfficer = Convert.ToInt32(dropOfficer.SelectedItem.Value);
        int idPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal percentualRebate = Convert.ToDecimal(textPercentualRebate.Text);
        //
        TabelaRebateOfficer tabelaRebateOfficer = new TabelaRebateOfficer();
        //
        if (insert)
        {
            tabelaRebateOfficer.IdPessoa = idPessoa;
            tabelaRebateOfficer.DataReferencia = dataReferencia;
            tabelaRebateOfficer.IdOfficer = idOfficer;
        }
        else
        {
            tabelaRebateOfficer.LoadByPrimaryKey(dataReferencia, idOfficer, idPessoa);
        }
                
        tabelaRebateOfficer.PercentualRebate = percentualRebate;
        tabelaRebateOfficer.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaRebateOfficer - Operacao: Insert TabelaRebateOfficer: " + dataReferencia + "; " + idPessoa + "; " + idOfficer + UtilitarioWeb.ToString(tabelaRebateOfficer),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
            e.Properties["btnEditCodigoPessoa"] = btnEditCodigoPessoa.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateOfficerMetadata.ColumnNames.DataReferencia));
            string idPessoa = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa));
            string idOfficer = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer));
            e.Value = data + idPessoa + idOfficer;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoPessoa", "textPercentualRebate");
        base.gridCadastro_PreRender(sender, e);
    }
}