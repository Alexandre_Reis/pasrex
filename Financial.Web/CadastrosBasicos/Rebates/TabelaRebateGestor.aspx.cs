﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Captacao;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaRebateGestor : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTemplateRebateGestor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaRebateGestorQuery tabelaRebateGestorQuery = new TabelaRebateGestorQuery("T");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

        tabelaRebateGestorQuery.Select(tabelaRebateGestorQuery, agenteMercadoQuery.Nome.As("Nome"));
        tabelaRebateGestorQuery.InnerJoin(agenteMercadoQuery).On(tabelaRebateGestorQuery.IdAgenteGestor == agenteMercadoQuery.IdAgente);

        tabelaRebateGestorQuery.OrderBy(tabelaRebateGestorQuery.DataReferencia.Ascending,
                                        agenteMercadoQuery.Nome.Ascending,
                                        tabelaRebateGestorQuery.Faixa.Ascending);

        TabelaRebateGestorCollection coll = new TabelaRebateGestorCollection();
        coll.Load(tabelaRebateGestorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoGestor.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgenteGestor_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropAgenteGestor = gridCadastro.FindEditFormTemplateControl("dropAgenteGestor") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropAgenteGestor);
        controles.Add(textDataReferencia);
        controles.Add(textFaixa);
        controles.Add(textPercentualRebateAdministracao);
        controles.Add(textPercentualRebatePerformance);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            decimal faixa = Convert.ToDecimal(textFaixa.Text);

            TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();
            if (tabelaRebateGestor.LoadByPrimaryKey(dataReferencia, idAgenteGestor, faixa)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxComboBox dropAgenteGestor = gridCadastro.FindEditFormTemplateControl("dropAgenteGestor") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();

        tabelaRebateGestor.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaRebateGestor.IdAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);
        tabelaRebateGestor.Faixa = Convert.ToDecimal(textFaixa.Text);
        tabelaRebateGestor.PercentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        tabelaRebateGestor.PercentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);
        tabelaRebateGestor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaRebateGestor - Operacao: Insert TabelaRebateGestor: " + dataReferencia + "; " + idAgenteGestor + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateGestor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxComboBox dropAgenteGestor = gridCadastro.FindEditFormTemplateControl("dropAgenteGestor") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();

        if (tabelaRebateGestor.LoadByPrimaryKey(dataReferencia, idAgenteGestor, faixa)) {
            tabelaRebateGestor.PercentualRebateAdministracao = percentualRebateAdministracao;
            tabelaRebateGestor.PercentualRebatePerformance = percentualRebatePerformance;
            tabelaRebateGestor.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaRebateGestor - Operacao: Update TabelaRebateGestor: " + dataReferencia + "; " + idAgenteGestor + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateGestor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaRebateGestorMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdAgenteGestor = gridCadastro.GetSelectedFieldValues(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor);
            List<object> keyValuesFaixa = gridCadastro.GetSelectedFieldValues(TabelaRebateGestorMetadata.ColumnNames.Faixa);
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idAgenteGestor = Convert.ToInt32(keyValuesIdAgenteGestor[i]);
                decimal faixa = Convert.ToDecimal(keyValuesFaixa[i]);

                TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();
                if (tabelaRebateGestor.LoadByPrimaryKey(data, idAgenteGestor, faixa)) {

                    TabelaRebateGestor tabelaRebateGestorClone = (TabelaRebateGestor)Utilitario.Clone(tabelaRebateGestor);
                    //
                    
                    tabelaRebateGestor.MarkAsDeleted();
                    tabelaRebateGestor.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaRebateGestor - Operacao: Delete TabelaRebateGestor: " + data + "; " + idAgenteGestor + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateGestorClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxComboBox dropAgenteGestor = gridCadastro.FindEditFormTemplateControl("dropAgenteGestor") as ASPxComboBox;
            e.Properties["cpDropAgenteGestor"] = dropAgenteGestor.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateGestorMetadata.ColumnNames.DataReferencia));
            string idAgenteGestor = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor));
            string faixa = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateGestorMetadata.ColumnNames.Faixa));
            e.Value = data + idAgenteGestor + faixa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("dropAgenteGestor", "textPercentualRebateAdministracao");
        base.gridCadastro_PreRender(sender, e);
    }
}