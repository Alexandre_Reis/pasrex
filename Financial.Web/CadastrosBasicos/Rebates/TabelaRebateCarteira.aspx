﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaRebateCarteira.aspx.cs" Inherits="CadastrosBasicos_TabelaRebateCarteira" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />   
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        var editor = ASPxClientControl.GetControlCollection().Get('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_btnEditCodigoCarteira');
        editor.SetValue(data);        
        ASPxCallback1.SendCallback(editor.GetValue());
        popupCarteira.HideWindow();
        editor.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
            var btnEditCodigoCarteira = ASPxClientControl.GetControlCollection().Get('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_btnEditCodigoCarteira');                        
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                  
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Rebate de Carteira"></asp:Label>
    </div>
           
    <div id="mainContent">
        
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSTemplateRebateCarteira"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" Caption="Id Carteira" Width="8%" VisibleIndex="1" CellStyle-HorizontalAlign="left">
                    <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" Width="20%" VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Faixa" VisibleIndex="6" Width="19%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="PercentualRebateAdministracao" Caption="% Rebate Adm" VisibleIndex="8" Width="19%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="PercentualRebatePerformance" Caption="% Rebate Pfee" VisibleIndex="10" Width="19%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                
                <Templates>
                <EditForm>
                     
                    <div class="editForm">    
                            
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCarteira"
                                            Text='<%# Eval("IdCarteira") %>' OnLoad="btnEditCodigo_Load"
                                            MaxLength="10" NumberType="Integer">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td colspan="2">
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo" Enabled="False" Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                </td>
                            </tr>
                        
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref:" />
                                </td> 
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnLoad="textDataReferencia_Load" />
                                </td>  
                            
                                <td align="right">
                                    <asp:Label ID="labelFaixa" runat="server" CssClass="labelRequired" Text="Valor Faixa:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textFaixa" runat="server" ClientInstanceName="textValor" 
                                            Text='<%# Eval("Faixa") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                            OnLoad="textFaixa_Load" CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>
                                </td> 
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelPercentualRebateAdministracao" runat="server" CssClass="labelRequired" Text="% Rebate Adm:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textPercentualRebateAdministracao" runat="server" ClientInstanceName="textValor" 
                                            Text='<%# Eval("PercentualRebateAdministracao") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                            CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>
                                </td> 
                            
                                <td align="right">
                                    <asp:Label ID="labelPercentualRebatePerformance" runat="server" CssClass="labelRequired" Text="% Rebate Pfee:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textPercentualRebatePerformance" runat="server" ClientInstanceName="textValor" 
                                            Text='<%# Eval("PercentualRebatePerformance") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                            CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>
                                </td> 
                            </tr>                            
                            
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal4" runat="server" Text="OK+"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>
                                                       
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>
        
        </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTemplateRebateCarteira" runat="server" OnesSelect="EsDSTemplateRebateCarteira_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        
    </form>
</body>
</html>