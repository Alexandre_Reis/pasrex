﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Captacao;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaRebateCarteira : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTemplateRebateCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaRebateCarteiraQuery tabelaRebateCarteiraQuery = new TabelaRebateCarteiraQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        tabelaRebateCarteiraQuery.Select(tabelaRebateCarteiraQuery, carteiraQuery.Apelido.As("Apelido"));
        tabelaRebateCarteiraQuery.InnerJoin(carteiraQuery).On(tabelaRebateCarteiraQuery.IdCarteira == carteiraQuery.IdCarteira);

        tabelaRebateCarteiraQuery.OrderBy(tabelaRebateCarteiraQuery.DataReferencia.Ascending,
                                          tabelaRebateCarteiraQuery.IdCarteira.Ascending,
                                          tabelaRebateCarteiraQuery.Faixa.Ascending);

        TabelaRebateCarteiraCollection coll = new TabelaRebateCarteiraCollection();
        coll.Load(tabelaRebateCarteiraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(textFaixa);
        controles.Add(textPercentualRebateAdministracao);
        controles.Add(textPercentualRebatePerformance);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            decimal faixa = Convert.ToDecimal(textFaixa.Text);

            TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();
            if (tabelaRebateCarteira.LoadByPrimaryKey(dataReferencia, idCarteira, faixa)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();

        tabelaRebateCarteira.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaRebateCarteira.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaRebateCarteira.Faixa = Convert.ToDecimal(textFaixa.Text);
        tabelaRebateCarteira.PercentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        tabelaRebateCarteira.PercentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);
        tabelaRebateCarteira.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaRebateCarteira - Operacao: Insert TabelaRebateCarteira: " + tabelaRebateCarteira.DataReferencia + "; " + tabelaRebateCarteira.IdCarteira + "; " + tabelaRebateCarteira.Faixa + UtilitarioWeb.ToString(tabelaRebateCarteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFaixa = gridCadastro.FindEditFormTemplateControl("textFaixa") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebateAdministracao = gridCadastro.FindEditFormTemplateControl("textPercentualRebateAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textPercentualRebatePerformance = gridCadastro.FindEditFormTemplateControl("textPercentualRebatePerformance") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal faixa = Convert.ToDecimal(textFaixa.Text);
        decimal percentualRebateAdministracao = Convert.ToDecimal(textPercentualRebateAdministracao.Text);
        decimal percentualRebatePerformance = Convert.ToDecimal(textPercentualRebatePerformance.Text);

        TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();

        if (tabelaRebateCarteira.LoadByPrimaryKey(dataReferencia, idCarteira, faixa)) {
            tabelaRebateCarteira.PercentualRebateAdministracao = percentualRebateAdministracao;
            tabelaRebateCarteira.PercentualRebatePerformance = percentualRebatePerformance;
            tabelaRebateCarteira.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaRebateCarteira - Operacao: Update TabelaRebateCarteira: " + dataReferencia + "; " + idCarteira + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateCarteira),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaRebateCarteiraMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues(TabelaRebateCarteiraMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesFaixa = gridCadastro.GetSelectedFieldValues(TabelaRebateCarteiraMetadata.ColumnNames.Faixa);
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);
                decimal faixa = Convert.ToDecimal(keyValuesFaixa[i]);
                
                TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();
                if (tabelaRebateCarteira.LoadByPrimaryKey(data, idCarteira, faixa)) {

                    TabelaRebateCarteira tabelaRebateCarteiraClone = (TabelaRebateCarteira)Utilitario.Clone(tabelaRebateCarteira);
                    //
                    
                    tabelaRebateCarteira.MarkAsDeleted();
                    tabelaRebateCarteira.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaRebateCarteira - Operacao: Delete TabelaRebateCarteira: " + data + "; " + idCarteira + "; " + faixa + UtilitarioWeb.ToString(tabelaRebateCarteiraClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCarteiraMetadata.ColumnNames.DataReferencia));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCarteiraMetadata.ColumnNames.IdCarteira));
            string faixa = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCarteiraMetadata.ColumnNames.Faixa));
            e.Value = data + idCarteira + faixa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira", "textPercentualRebateAdministracao");
        base.gridCadastro_PreRender(sender, e);
    }
}