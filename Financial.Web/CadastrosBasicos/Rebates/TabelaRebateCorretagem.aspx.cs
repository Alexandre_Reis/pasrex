﻿using Financial.Web.Common;
using System;
using DevExpress.Web;
using Financial.Captacao;
using Financial.Common;
using Financial.Fundo;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Security;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class CadastrosBasicos_TabelaRebateCorretagem : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaRebateCorretagem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        TabelaRebateCorretagemQuery tabelaRebateCorretagemQuery = new TabelaRebateCorretagemQuery("T");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        tabelaRebateCorretagemQuery.Select(tabelaRebateCorretagemQuery, clienteQuery.Apelido.As("Apelido"),
                                             agenteMercadoQuery);
        tabelaRebateCorretagemQuery.InnerJoin(clienteQuery).On(tabelaRebateCorretagemQuery.IdCliente == clienteQuery.IdCliente);
        tabelaRebateCorretagemQuery.InnerJoin(agenteMercadoQuery).On(tabelaRebateCorretagemQuery.IdAgenteCorretora == agenteMercadoQuery.IdAgente);
        tabelaRebateCorretagemQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == tabelaRebateCorretagemQuery.IdCliente);
        tabelaRebateCorretagemQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaRebateCorretagemQuery.OrderBy(tabelaRebateCorretagemQuery.DataReferencia.Ascending,
                                          agenteMercadoQuery.Nome.Ascending,
                                          clienteQuery.Apelido.Ascending);

        TabelaRebateCorretagemCollection coll = new TabelaRebateCorretagemCollection();
        coll.Load(tabelaRebateCorretagemQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);

        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region Disable Primary Keys in Edit Mode
    protected void textDataReferencia_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void dropAgenteMercado_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                                    ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //        
        ASPxSpinEdit textPercentualRebate = gridCadastro.FindEditFormTemplateControl("textPercentualRebate") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropAgenteMercado);
        controles.Add(btnEditCodigoCliente);
        controles.Add(textDataReferencia);
        //
        controles.Add(textPercentualRebate);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idAgenteCorretora = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            //
            TabelaRebateCorretagem tabelaRebateCorretagem = new TabelaRebateCorretagem();
            if (tabelaRebateCorretagem.LoadByPrimaryKey(dataReferencia, idCliente, idAgenteCorretora)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //
        ASPxSpinEdit textPercentualRebate = gridCadastro.FindEditFormTemplateControl("textPercentualRebate") as ASPxSpinEdit;
        //                
        TabelaRebateCorretagem tabelaRebateCorretagem = new TabelaRebateCorretagem();
        //
        tabelaRebateCorretagem.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaRebateCorretagem.IdAgenteCorretora = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
        tabelaRebateCorretagem.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        tabelaRebateCorretagem.PercentualRebate = Convert.ToDecimal(textPercentualRebate.Text);
        //        
        tabelaRebateCorretagem.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaRebateCorretagem - Operacao: Insert TabelaRebateCorretagem: " + tabelaRebateCorretagem.DataReferencia + "; " + tabelaRebateCorretagem.IdCliente + "; " + tabelaRebateCorretagem.IdAgenteCorretora + UtilitarioWeb.ToString(tabelaRebateCorretagem),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //        
        ASPxSpinEdit textPercentualRebate = gridCadastro.FindEditFormTemplateControl("textPercentualRebate") as ASPxSpinEdit;
        //
        int idAgenteMercado = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        //
        decimal percentualRebate = Convert.ToDecimal(textPercentualRebate.Text);

        TabelaRebateCorretagem tabelaRebateCorretagem = new TabelaRebateCorretagem();

        if (tabelaRebateCorretagem.LoadByPrimaryKey(dataReferencia, idCliente, idAgenteMercado)) {
            tabelaRebateCorretagem.PercentualRebate = percentualRebate;
            //
            tabelaRebateCorretagem.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaRebateCorretagem - Operacao: Update TabelaRebateCorretagem: " + dataReferencia + "; " + idCliente + "; " + idAgenteMercado + UtilitarioWeb.ToString(tabelaRebateCorretagem),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente);
            List<object> keyValuesIdAgenteCorretora = gridCadastro.GetSelectedFieldValues(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora);
            //

            TabelaRebateCorretagemCollection t = new TabelaRebateCorretagemCollection();
            //
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int idAgenteCorretora = Convert.ToInt32(keyValuesIdAgenteCorretora[i]);

                TabelaRebateCorretagem tabelaRebateCorretagem = new TabelaRebateCorretagem();
                if (tabelaRebateCorretagem.LoadByPrimaryKey(data, idCliente, idAgenteCorretora)) {
                    t.AttachEntity(tabelaRebateCorretagem);
                }
            }

            TabelaRebateCorretagemCollection tabelaRebateCorretagemClone = (TabelaRebateCorretagemCollection)Utilitario.Clone(t);
            //
            // Delete com Transação
            t.MarkAllAsDeleted();
            t.Save();

            foreach (TabelaRebateCorretagem tAux in tabelaRebateCorretagemClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaRebateCorretagem - Operacao: Delete TabelaRebateCorretagem: " + tAux.DataReferencia + "; " + tAux.IdCliente + "; " + tAux.IdAgenteCorretora + UtilitarioWeb.ToString(tAux),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }            
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia));
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente));
            string idAgenteCorretora = Convert.ToString(e.GetListSourceFieldValue(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora));

            e.Value = data + idCliente + idAgenteCorretora;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente", "textPercentualRebate");
        base.gridCadastro_PreRender(sender, e);
    }
}