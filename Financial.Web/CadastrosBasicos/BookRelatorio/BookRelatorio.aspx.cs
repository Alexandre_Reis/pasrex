using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Common.Enums;
using Financial.Enquadra;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Web.Common;

public partial class CadastrosBasicos_BookRelatorio : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                 new List<string>(new string[] { BookRelatorioMetadata.ColumnNames.IdRelatorio }));
    }

    #region DataSources
    protected void EsDSBookRelatorio_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BookRelatorioCollection coll = new BookRelatorioCollection();
        coll.Query.OrderBy(coll.Query.IdBook.Ascending, coll.Query.IdRelatorio.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSBook_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BookCollection coll = new BookCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callBack1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        if (e.Parameter != "")
        {
            int idBook = Convert.ToInt32(e.Parameter);
            Book book = new Book();
            book.LoadByPrimaryKey(idBook);

            if (book.Tipo == (byte)TipoBook.BookGeral)
            {
                e.Result = "Geral";
            }
            else if (book.Tipo == (byte)TipoBook.BookPorCarteira)
            {
                e.Result = "Carteira";
            }
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idBook = Convert.ToString(e.GetListSourceFieldValue("IdBook"));
            string idRelatorio = Convert.ToString(e.GetListSourceFieldValue("IdRelatorio"));
            e.Value = idBook + "|" + idRelatorio;
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        BookRelatorio bookRelatorio = new BookRelatorio();
        int idBook = (int)e.OldValues["IdBook"];
        int idRelatorio = (int)e.OldValues["IdRelatorio"];

        if (bookRelatorio.LoadByPrimaryKey(idBook, idRelatorio))
        {
            bookRelatorio.IdBook = Convert.ToInt32(e.NewValues["IdBook"]);
            bookRelatorio.IdRelatorio = Convert.ToInt32(e.NewValues["IdRelatorio"]);
            bookRelatorio.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BookRelatorio - Operacao: Update BookRelatorio: " + idBook + "; " + idRelatorio + UtilitarioWeb.ToString(bookRelatorio),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        BookRelatorio bookRelatorio = new BookRelatorio();

        if (!bookRelatorio.LoadByPrimaryKey(Convert.ToInt32(e.NewValues["IdBook"]),
                                            Convert.ToInt32(e.NewValues["IdRelatorio"])))
        {
            bookRelatorio.IdBook = Convert.ToInt32(e.NewValues["IdBook"]);
            bookRelatorio.IdRelatorio = Convert.ToInt32(e.NewValues["IdRelatorio"]);
            bookRelatorio.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BookRelatorio - Operacao: Insert BookRelatorio: " + bookRelatorio.IdBook + "; " + bookRelatorio.IdRelatorio + UtilitarioWeb.ToString(bookRelatorio),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
    
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();    
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdBook = gridCadastro.GetSelectedFieldValues("IdBook");
            List<object> keyValuesIdRelatorio = gridCadastro.GetSelectedFieldValues("IdRelatorio");
            for (int i = 0; i < keyValuesIdBook.Count; i++)
            {
                int idBook = Convert.ToInt32(keyValuesIdBook[i]);
                int idRelatorio = Convert.ToInt32(keyValuesIdRelatorio[i]);

                BookRelatorio bookRelatorio = new BookRelatorio();
                if (bookRelatorio.LoadByPrimaryKey(idBook, idRelatorio))
                {
                    BookRelatorio bookRelatorioClone = (BookRelatorio)Utilitario.Clone(bookRelatorio);
                    //

                    bookRelatorio.MarkAsDeleted();
                    bookRelatorio.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de BookRelatorio - Operacao: Delete BookRelatorio: " + bookRelatorioClone.IdBook + "; " +bookRelatorioClone.IdRelatorio + UtilitarioWeb.ToString(bookRelatorioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;

        if (!gridCadastro.IsNewRowEditing)
        {
            e.Editor.Enabled = false;
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;            
        }
    }
}

