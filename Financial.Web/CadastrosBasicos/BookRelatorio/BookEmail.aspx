﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookEmail.aspx.cs" Inherits="CadastrosBasicos_BookEmail" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;    
    </script>
</head>

<body>
    <form id="form1" runat="server">
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);
            }
            else {
                gridCadastro.UpdateEdit();
            }
        }
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Book de Relatórios - Email"></asp:Label>
    </div>
           
    <div id="mainContent">

            <div class="linkButton" >               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"  OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomCallback="gridCadastro_CustomCallback" >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id" Width="10%" VisibleIndex="1" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" Width="30%" VisibleIndex="1"/>
                    <dxwgv:GridViewDataTextColumn FieldName="BookEmail" Caption="Email" Width="25%" VisibleIndex="1"/>
                    <dxwgv:GridViewDataTextColumn FieldName="BookAssunto" Caption="Assunto do Email" Width="30%" VisibleIndex="2"/>
                </Columns>
                
                <Templates>
                <EditForm>
                
                    <div class="editForm">    
                            
                        <table border="0">
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCliente") %>' Enabled="false" 
                                            MaxLength="10" NumberType="Integer">
                                    <Buttons>                                           
                                        <dxe:EditButton />
                                    </Buttons>                    
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False" Text='<%# Eval("Apelido") %>'/>
                                </td>                                
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelEmail" runat="server" CssClass="labelNormal" Text="Email:"/>
                                </td>
                                <td colspan="2">
                                    <dxe:ASPxTextBox ID="textEmail" runat="server" CssClass="textDescricao" Text='<%# Eval("BookEmail") %>' />
                                </td>
                                
                                <td>
                                    <dxe:ASPxCheckBox ID="replicaEmail" ClientInstanceName="replicaEmail" runat="server" Width="10px" ToolTip="Replica o Email para todos os Clientes"/>
                                </td>
                                <td style="width:2px">
                                <td>
                                    <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Replica Email" />
                                </td>

                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAssunto" runat="server" CssClass="labelNormal" Text="Assunto:"/>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textAssunto" runat="server" CssClass="textDescricao" Text='<%# Eval("BookAssunto") %>' />
                                </td>
                                
                                <td>
                                    <dxe:ASPxCheckBox ID="replicaAssunto" runat="server" ClientInstanceName="replicaAssunto" Width="10px" ToolTip="Replica Assunto da Mensagem para todos os Clientes"/>
                                </td>
                                <td style="width:2px">
                                <td>
                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Replica Assunto" />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelMensagem" runat="server" CssClass="labelNormal" Text="Mensagem:"/>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textMensagem" runat="server" CssClass="textDescricao" Height="80px" TextMode="MultiLine" Rows="6" Text='<%# Eval("BookMensagem") %>' />
                                </td>
                                
                                <td>
                                    <dxe:ASPxCheckBox ID="replicaMensagem" runat="server" ClientInstanceName="replicaMensagem" Width="10px" ToolTip="Replica a Mensagem para todos os Clientes"/>
                                </td>
                                <td style="width:2px">
                                <td>
                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Replica Mensagem" />
                                </td>

                            </tr>                            
                            
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" OnClientClick="callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>                                                  

                </EditForm>
                
                </Templates>
                
                <SettingsPopup EditForm-Width="640px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>     
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>