﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_BookCliente : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.AllowUpdate = false;
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                 new List<string>(new string[] { BookClienteMetadata.ColumnNames.Imprime }));
    }

    #region DataSources
    protected void EsDSBookCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        int idUsuario = 0;
        Usuario usuario = new Usuario();
        if (usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name)) {
            idUsuario = usuario.IdUsuario.Value;
        }

        BookClienteQuery bookClienteQuery = new BookClienteQuery("B");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        bookClienteQuery.Select(bookClienteQuery.IdCliente, bookClienteQuery.IdBook, bookClienteQuery.Imprime, clienteQuery.Apelido);
        bookClienteQuery.InnerJoin(clienteQuery).On(bookClienteQuery.IdCliente == clienteQuery.IdCliente);
        bookClienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        bookClienteQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                               permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        BookClienteCollection coll = new BookClienteCollection();
        coll.Load(bookClienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSBook_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        BookCollection coll = new BookCollection();
        coll.Query.Where(coll.Query.Tipo.Equal((byte)TipoBook.BookPorCarteira));
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropBook = gridCadastro.FindEditFormTemplateControl("dropBook") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(dropBook);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            int idBook = Convert.ToInt32(dropBook.SelectedItem.Value);

            BookCliente bookCliente = new BookCliente();
            if (bookCliente.LoadByPrimaryKey(idBook, idCliente)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idBook = Convert.ToString(e.GetListSourceFieldValue("IdBook"));
            string idCliente = Convert.ToString(e.GetListSourceFieldValue("IdCliente"));
            e.Value = idBook + idCliente;
        }
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();        
    }

    private void SalvarNovo()
    {
        BookCliente bookCliente = new BookCliente();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropBook = gridCadastro.FindEditFormTemplateControl("dropBook") as ASPxComboBox;

        int idBook = Convert.ToInt32(dropBook.SelectedItem.Value);
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        bookCliente.IdBook = idBook;
        bookCliente.IdCliente = idCliente;
        bookCliente.Imprime = "S";
        bookCliente.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de BookCliente - Operacao: Insert BookCliente: " + idBook + "; " + idCliente + UtilitarioWeb.ToString(bookCliente),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyValuesIdBook = gridCadastro.GetSelectedFieldValues("IdBook");
            for (int i = 0; i < keyValuesIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int idBook = Convert.ToInt32(keyValuesIdBook[i]);

                BookCliente bookCliente = new BookCliente();
                if (bookCliente.LoadByPrimaryKey(idBook, idCliente)) {

                    BookCliente bookClienteClone = (BookCliente)Utilitario.Clone(bookCliente);
                    //
                    
                    bookCliente.MarkAsDeleted();
                    bookCliente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de BookCliente - Operacao: Delete BookCliente: " + idBook + "; "+idCliente + UtilitarioWeb.ToString(bookClienteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnAtivaImprime" || e.Parameters == "btnDesativaImprime") {
            List<object> keyIdCliente = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyIdBook = gridCadastro.GetSelectedFieldValues("IdBook");
            for (int i = 0; i < keyIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyIdCliente[i]);
                int idBook = Convert.ToInt32(keyIdBook[i]);

                BookCliente bookCliente = new BookCliente();
                bookCliente.LoadByPrimaryKey(idBook, idCliente);

                if (e.Parameters == "btnAtivaImprime") {
                    bookCliente.Imprime = "S";
                }
                else {
                    bookCliente.Imprime = "N";
                }

                bookCliente.Save();

                #region Log do Processo
                string mensagem = e.Parameters == "btnAtivaImprime"
                                 ? "Cadastro de BookCliente - Operacao: Ativa Impressão: " + idBook + "; " + idCliente + UtilitarioWeb.ToString(bookCliente)
                                 : "Cadastro de BookCliente - Operacao: Desativa Impressão: " + idBook + "; " + idCliente + UtilitarioWeb.ToString(bookCliente);

                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                mensagem,
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            ASPxComboBox dropBook = gridCadastro.FindEditFormTemplateControl("dropBook") as ASPxComboBox;
            e.Properties["cpDropBook"] = dropBook.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
    }   
}