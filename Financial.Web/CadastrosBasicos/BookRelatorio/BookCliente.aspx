﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookCliente.aspx.cs" Inherits="CadastrosBasicos_BookCliente" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">                        
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                    
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }        
        "/>        
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Book de Relatórios por Cliente"></asp:Label>
    </div>
           
    <div id="mainContent">

            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
               <asp:LinkButton ID="btnDesativaImprime" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPageDelete" OnClientClick=" if (confirm('Tem certeza que quer desativar a impressão?')==true) gridCadastro.PerformCallback('btnDesativaImprime');return false;"><asp:Literal ID="Literal3" runat="server" Text="Desativa"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnAtivaImprime" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnEdit" OnClientClick=" if (confirm('Tem certeza que quer ativar a impressão?')==true) gridCadastro.PerformCallback('btnAtivaImprime');return false;"><asp:Literal ID="Literal7" runat="server" Text="Ativa"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSBookCliente"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                     >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left">
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="30%">                    
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Book" FieldName="IdBook" VisibleIndex="4" Width="40%">
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox DataSourceID="EsDSBook" TextField="Descricao" ValueField="IdBook">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Imprime" VisibleIndex="5" Width="10%" ExportWidth="100">
                    <EditFormSettings Visible="False" />
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                    </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                </Columns>
                
                <Templates>
                <EditForm>
                    
                    <div class="editForm">    
                            
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                            ClientInstanceName="btnEditCodigoCliente" Text='<%# Eval("IdCliente") %>' MaxLength="10"
                                            NumberType="Integer">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) { OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente); }"
                                            />                                                                                               
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False" Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelBook" runat="server" CssClass="labelRequired" Text="Book:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropBook" runat="server" ClientInstanceName="dropBook"
                                                        DataSourceID="EsDSBook" ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownListLongo" TextField="Descricao" ValueField="IdBook"
                                                        Text='<%#Eval("IdBook")%>'>                                      
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK+"/><div></div></asp:LinkButton>                                               
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"  
	                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>
                    
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="480px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>     
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
    LeftMargin = "50"
    RightMargin = "50"
    />
    
    <cc1:esDataSource ID="EsDSBookCliente" runat="server" OnesSelect="EsDSBookCliente_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSBook" runat="server" OnesSelect="EsDSBook_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>