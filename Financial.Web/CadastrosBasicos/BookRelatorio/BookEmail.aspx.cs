﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_BookEmail : Financial.Web.Common.CadastroBasePage {
    
    new protected void Page_Load(object sender, EventArgs e) {
        this.AllowDelete = false;
        base.Page_Load(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {       
        e.Collection = this.retornaClientesAcesso();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxTextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as ASPxTextBox;
        string email = Convert.ToString(textEmail.Text).Trim();

        if (!String.IsNullOrEmpty(email)) {
            string emailAux = email.Replace(",", ";").Replace(" ", ";");
            string[] emailLista = emailAux.Split(';');

            foreach (string emails  in emailLista) {
                if (emails != "") {
                    if (!Utilitario.IsEmailValid(emails.Trim())) {
                        e.Result = "Email Inválido: " + emails.Trim();
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idCliente = (int)e.Keys[0];

        ASPxTextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as ASPxTextBox;
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textMensagem = gridCadastro.FindEditFormTemplateControl("textMensagem") as TextBox;
        //
        ASPxCheckBox replicaEmail = gridCadastro.FindEditFormTemplateControl("replicaEmail") as ASPxCheckBox;
        ASPxCheckBox replicaAssunto = gridCadastro.FindEditFormTemplateControl("replicaAssunto") as ASPxCheckBox;
        ASPxCheckBox replicaMensagem = gridCadastro.FindEditFormTemplateControl("replicaMensagem") as ASPxCheckBox;

        Cliente cliente = new Cliente();

        if (cliente.LoadByPrimaryKey(idCliente)) {
            cliente.BookEmail = Convert.ToString(textEmail.Text).Trim();
            cliente.BookAssunto = Convert.ToString(textAssunto.Text).Trim();
            cliente.BookMensagem = Convert.ToString(textMensagem.Text).Trim();
            cliente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BookEmail - Operacao: Update BookEmail: " + idCliente + UtilitarioWeb.ToString(cliente),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Replica Email/Assunto/Mensagem para todos os clientes
            ClienteCollection clientes = this.retornaClientesAcesso();
            if ( Convert.ToBoolean(replicaEmail.Value) ) {
                for (int i = 0; i < clientes.Count; i++) {
                    clientes[i].BookEmail = Convert.ToString(textEmail.Text).Trim();
                }
            }
            if ( Convert.ToBoolean(replicaAssunto.Value) ) {
                for (int i = 0; i < clientes.Count; i++) {
                    clientes[i].BookAssunto = Convert.ToString(textAssunto.Text).Trim();
                }
            }            
            if ( Convert.ToBoolean(replicaMensagem.Value) ) {
                for (int i = 0; i < clientes.Count; i++) {
                    clientes[i].BookMensagem = Convert.ToString(textMensagem.Text).Trim();
                }
            }
            clientes.Save();
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        //ASPxGridView gridView = sender as ASPxGridView;
        //if (gridView.IsEditing) {
        //    TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        //    e.Properties["cpTextEmail"] = textEmail.ClientID;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridCadastro.DataBind();
        gridCadastro.CancelEdit();
    }
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textEmail");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    ///  Retorna Lista de Clientes que tem permissão de Acesso
    /// </summary>
    /// <returns></returns>
    private ClienteCollection retornaClientesAcesso() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        //
        clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido,
                            clienteQuery.BookEmail, clienteQuery.BookAssunto, 
                            clienteQuery.BookMensagem);
        //
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario,
                           clienteQuery.TipoControle != (byte)TipoControleCliente.ApenasCotacao,
                           clienteQuery.StatusAtivo == (byte)StatusAtivoCliente.Ativo);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        return coll;
    }
}