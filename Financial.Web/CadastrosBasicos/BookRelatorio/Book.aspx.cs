﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_Book : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {        
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { BookMetadata.ColumnNames.Tipo }));
    }
    
    protected void EsDSBook_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        BookCollection coll = new BookCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void dropTipo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        TextBox textEmailFrom = gridCadastro.FindEditFormTemplateControl("textEmailFrom") as TextBox;
        TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textMensagem = gridCadastro.FindEditFormTemplateControl("textMensagem") as TextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropTipo);
        controles.Add(textEmailFrom);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (!Utilitario.IsEmailValid(textEmailFrom.Text))
        {
            e.Result = "Email Origem Inválido";
            return;
        }
        
        if (gridCadastro.IsEditing) {
            byte tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            if (tipo == (byte)TipoBook.BookPorCarteira &&
                (textEmail.Text != "")) {
                e.Result = "Para Books do tipo Por Carteira, email deve ser preenchido na associação com a carteira.";
                return;
            }
            else if (tipo == (byte)TipoBook.BookGeral &&
                (textEmail.Text == "" || textAssunto.Text == "" || textMensagem.Text == "")) {
                e.Result = "Para Books do tipo Geral, email, assunto e mensagem devem ser preenchidos.";
                return;
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        TextBox textEmailFrom = gridCadastro.FindEditFormTemplateControl("textEmailFrom") as TextBox;
        TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textMensagem = gridCadastro.FindEditFormTemplateControl("textMensagem") as TextBox;

        Book book = new Book();
        int idBook = Convert.ToInt32(e.Keys[0]);

        if (book.LoadByPrimaryKey(idBook)) {
            book.Descricao = textDescricao.Text;
            book.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            book.EmailFrom = textEmailFrom.Text;
            book.Email = textEmail.Text;
            book.Assunto = textAssunto.Text;
            book.Mensagem = textMensagem.Text;
            book.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Book - Operacao: Update Book: " + idBook + UtilitarioWeb.ToString(book),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
       
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        TextBox textEmailFrom = gridCadastro.FindEditFormTemplateControl("textEmailFrom") as TextBox;
        TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textMensagem = gridCadastro.FindEditFormTemplateControl("textMensagem") as TextBox;

        Book book = new Book();
        book.Descricao = textDescricao.Text;
        book.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        book.EmailFrom = textEmailFrom.Text;
        book.Email = textEmail.Text;
        book.Assunto = textAssunto.Text;
        book.Mensagem = textMensagem.Text;
        book.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Book - Operacao: Insert Book: " + book.IdBook + UtilitarioWeb.ToString(book),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdBook");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idBook = Convert.ToInt32(keyValuesId[i]);

                //Deleta BookCliente para o idBook
                BookClienteCollection bookClienteCollection = new BookClienteCollection();
                bookClienteCollection.Query.Where(bookClienteCollection.Query.IdBook.Equal(idBook));
                bookClienteCollection.Query.Load();
                bookClienteCollection.MarkAllAsDeleted();
                bookClienteCollection.Save();
                //

                //Deleta BookRelatorio para o idBook
                BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
                bookRelatorioCollection.Query.Where(bookRelatorioCollection.Query.IdBook.Equal(idBook));
                bookRelatorioCollection.Query.Load();
                bookRelatorioCollection.MarkAllAsDeleted();
                bookRelatorioCollection.Save();
                //

                Book book = new Book();
                if (book.LoadByPrimaryKey(idBook)) {

                    Book bookClone = (Book)Utilitario.Clone(book);

                    book.MarkAsDeleted();
                    book.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Book - Operacao: Delete Book: " + idBook + UtilitarioWeb.ToString(bookClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) 
    {
        base.gridCadastro_CustomJSProperties(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }    
}