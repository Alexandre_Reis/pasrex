﻿using System;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Collections.Generic;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;

using log4net;

public partial class ProcessaIncorporacaoFundo1 : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(ProcessaIncorporacaoFundo));

    /* Guarda a mensagem de Erro do Processamento */
    private string mensagemErro = "";

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditFundoOrigem.Focus();

        this.HasPopupCarteira = true;
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        //TrataTravamentoCampos();
    }

    #region DataSource

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        if( this.TrataErros() ) {
            e.Result = this.mensagemErro;
        }
    }

   /// <summary>
   /// 
   /// </summary>
    [Obsolete("")]
    private void TrataTravamentoCampos() {
        //Usuario usuario = new Usuario();
        //usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        //int tipoTrava = usuario.TipoTrava.Value;

        //if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
        //    btnEditCodigoCliente.Enabled = false;

        //    PermissaoCliente permissaoCliente = new PermissaoCliente();
        //    int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

        //    if (idCliente.HasValue) {
        //        Cliente cliente = new Cliente();

        //        List<esQueryItem> campos = new List<esQueryItem>();
        //        campos.Add(cliente.Query.Apelido);
        //        cliente.LoadByPrimaryKey(campos, idCliente.Value);
        //        string apelido = cliente.Apelido;

        //        btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
        //        textNomeCliente.Text = apelido;
        //    }
        //}
    }

    /// <summary>
    /// Trata Callback Fundo Origem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                               ? carteira.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// Trata Callback Fundo Destino
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                               ? carteira.str.Apelido : "no_access";                                                        
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// Processamento para Incorporação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnProcessa_Click(object sender, EventArgs e) {
        if (!this.TrataErros()) {
            int origem = Convert.ToInt32(this.btnEditFundoOrigem.Text);
            int destino = Convert.ToInt32(this.btnEditFundoDestino.Text);
            DateTime data = Convert.ToDateTime(this.textDataIncorporacao.Text);

            Thread.Sleep(500);

            //                    
            HistoricoCota h = new HistoricoCota();
            //h.ProcessaIncorporacao(origem, destino, data);
        }            
    }

    /// <summary>
    /// Indica se Houve algum erro antes do Processamento
    /// </summary>
    /// <returns>booleano indicando se houve erro</returns>
    /// <modifies>modifica variavel private mensagemErro com a mensagem de erro</modifies>
    private bool TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditFundoOrigem, this.btnEditFundoDestino, this.textDataIncorporacao });

        if (base.TestaObrigatorio(controles) != "") {
            this.mensagemErro = "Campos com * são obrigatórios!";
            return true;
        }
        #endregion

        #region Fundo Origem = Fundo Destino
        if (Convert.ToInt32(this.btnEditFundoOrigem.Text) == Convert.ToInt32(this.btnEditFundoDestino.Text)) {
            this.mensagemErro = "Fundo Origem Igual ao Fundo Destino.";
            return true;
        }
        #endregion

        #region Compara DataDia
        PosicaoFundoCollection p = new PosicaoFundoCollection();
        p.Query.es.Distinct = true;
        //
        p.Query.Select(p.Query.IdCliente)
               .Where(p.Query.IdCarteira == Convert.ToInt32(this.btnEditFundoOrigem.Text))
               .OrderBy(p.Query.IdCliente.Ascending);

        p.Query.Load();

        bool erro = false;
        Cliente c = new Cliente();
        for (int i = 0; i < p.Count; i++) {
            c = p[i].UpToClienteByIdCliente;
            if (c.DataDia != Convert.ToDateTime(this.textDataIncorporacao.Text)) {
                erro = true;
                break;
            }
        }

        if (erro) {
            this.mensagemErro = String.Format("Processo de Incorporação não Executado. Cliente {0} fora da Data Dia: {1}", c.IdCliente.Value, c.DataDia.Value.ToString("d"));
            return true;
        }
        #endregion

        // Não ocorreu erro
        return false;
    }
}