﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TesteASPX_Scheduler.aspx.cs" Inherits="CadastrosBasicos_Scheduler" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>


<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>
    
<%@ Register Assembly="DevExpress.XtraScheduler.v15.2.Core, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraScheduler" TagPrefix="dx" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    
        <table width="100%" border=2><tr><td>
    <div id="container">
    
        <div id="header">
            <asp:Label ID="lblHeader" runat="server" Text="Teste Scheduler"/>
        </div>
        
        <div id="mainContent">
                
      <dx:ASPxScheduler ID="ASPxScheduler1" runat="server" OnAppointmentsInserted="OnSchedulerControlAppointmentsInserted" OnAppointmentCollectionCleared="OnSchedulerControlAppointmentCollectionCleared">
        <Views>
            <DayView>
                <TimeRulers>
                    <dx:TimeRuler />
                </TimeRulers>
            </DayView>
            <WorkWeekView>
                <TimeRulers>
                    <dx:TimeRuler />
                </TimeRulers>
            </WorkWeekView>
            <TimelineView />
        </Views>
        <Storage EnableReminders="false">
        </Storage>
    </dx:ASPxScheduler>
    
    <asp:ObjectDataSource ID="appointmentDataSource" runat="server" DataObjectTypeName="CustomEvent"
        TypeName="CustomEventDataSource" 
        DeleteMethod="DeleteMethodHandler" 
        SelectMethod="SelectMethodHandler"
        InsertMethod="InsertMethodHandler" 
        UpdateMethod="UpdateMethodHandler" 
        OnObjectCreated="appointmentsDataSource_ObjectCreated" 
        OnInserted="OnAppointmentDataSourceInserted"/>
            
        
        </div>
    
    </div>
    
    </td></tr></table>
    
    </div>        
                           
    </form>
</body>
</html>