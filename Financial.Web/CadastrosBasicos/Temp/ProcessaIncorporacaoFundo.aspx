﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessaIncorporacaoFundo.aspx.cs" Inherits="ProcessaIncorporacaoFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function OnGetDataCliente(values) {
    btnEditFundoOrigem.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditFundoOrigem.GetValue());        
    btnEditFundoOrigem.Focus();    
}

function OnGetDataCarteira(values) {
    btnEditFundoDestino.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback2.SendCallback(btnEditFundoDestino.GetValue());        
    btnEditFundoDestino.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditFundoOrigem, document.getElementById('textNomeCliente'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditFundoDestino, document.getElementById('textNomeCarteira'));
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                CallbackPanel.PerformCallback();
            }
        }        
        "/>
    </dxcb:ASPxCallback>        
                
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <div class="divPanel">
    
    <table width="100%">
    <tr><td>
    
    <div id="container_small">
    
        <div id="header"><asp:Label ID="label3" runat="server" Text="Processa Incorporação Fundo" /></div>
        
        <div id="mainContentSpace">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
                    
            <div class="reportFilter">
            
            <div style=height:20px></div>                              
                                            
            <table border="0">

                <tr>
                <td class="td_Label">
                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Fundo Origem:"></asp:Label>
                </td>
                
                <td>
                    <dxe:ASPxSpinEdit ID="btnEditFundoOrigem" runat="server" CssClass="textButtonEdit" EnableClientSideAPI="True" ClientInstanceName="btnEditFundoOrigem" MaxLength="9" NumberType="Integer" AllowMouseWheel="false">
                    <Buttons>
                        <dxe:EditButton></dxe:EditButton>                                
                    </Buttons>        
                    <ClientSideEvents
                             KeyPress="function(s, e) {document.getElementById('textNomeCliente').value = '';}" 
                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditFundoOrigem);}"
                    />                            
                    </dxe:ASPxSpinEdit>                
                </td>                          
                
                <td colspan="2" width=300>
                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                                
                </td>
                </tr>
                
                <tr>
                <td class="td_Label">                
                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Fundo Destino:"/>
                </td>
                
                <td>
                    <dxe:ASPxSpinEdit ID="btnEditFundoDestino" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditFundoDestino" MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                    <Buttons>
                        <dxe:EditButton></dxe:EditButton>                                
                    </Buttons>        
                    <ClientSideEvents
                             KeyPress="function(s, e) {document.getElementById('textNomeCarteira').value = '';}" 
                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                             LostFocus="function(s, e) {popupMensagemCarteira.HideWindow(); ASPxCallback2.SendCallback(btnEditFundoDestino.GetValue());}"
                    />                            
                    </dxe:ASPxSpinEdit>                
                </td>                          
                
                <td colspan="2" width=300>
                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
                </td>
                </tr>
                                                  
                <tr>
                <td class="td_Label" >
                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data Incorporação:"/>
                </td>
                
                <td colspan="3">
                    <dxe:ASPxDateEdit ID="textDataIncorporacao" runat="server" ClientInstanceName="textDataIncorporacao"/>
                </td>                                                                
                </tr>
                                                             
            </table>                                                                                 
            
            </div>
            
            <div id="reportLinkButton" class="linkButton linkButtonNoBorder">

               <asp:LinkButton ID="btnProcessar" runat="server" Font-Overline="false" 
                    CssClass="btnOK" OnClick="btnProcessa_Click" 
                    OnClientClick=" { callbackErro.SendCallback(); }"                
               >
               <asp:Literal ID="Literal1" runat="server" Text="Processar"/><div></div>
               </asp:LinkButton>
               
            </div>
            
            <dxcb:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel1" ClientInstanceName="CallbackPanel" LoadingPanelText="Carregando... ">
            <PanelCollection>
                <dxp:PanelContent ID="PanelContent3" runat="server">                                                                                                                            
                    <table border="0" id="PanelForLoading" style="width:350px;height:100px;">
                    <tr>
                        <td></td>
                     </tr>                                                                                                                                                               
                    </table>
              </dxp:PanelContent>
            </PanelCollection>
            </dxcb:ASPxCallbackPanel>
            
 </ContentTemplate>
    </asp:UpdatePanel>
            
        </div>
        
    </div>
    
    </td></tr>
    </table>
    
    </div>
                                       
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
   </form>
</body>
</html>