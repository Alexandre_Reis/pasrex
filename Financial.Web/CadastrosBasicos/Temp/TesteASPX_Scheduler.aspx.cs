﻿using Financial.Web.Common;
using System;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;
using System.Collections.Generic;

public partial class CadastrosBasicos_Scheduler : BasePage {
    //ASPxSchedulerStorage Storage { get { return ASPxScheduler1.Storage; } }
    //List<Object> lastInsertedIdList = new List<object>();

    //protected void Page_Load(object sender, EventArgs e) {
    //    SetupMappings();
    //    SchedulerDemoUtils.FillResources(Storage, 5);

    //    ASPxScheduler1.AppointmentDataSource = appointmentDataSource;
    //    ASPxScheduler1.DataBind();
    //}

    protected void OnSchedulerControlAppointmentCollectionCleared(object sender, EventArgs e) {
        //    this.lastInsertedIdList.Clear();
    }
    protected void OnAppointmentDataSourceInserted(object sender, ObjectDataSourceStatusEventArgs e) {
        //    this.lastInsertedIdList.Add(e.ReturnValue);
    }
    protected void OnSchedulerControlAppointmentsInserted(object sender, PersistentObjectsEventArgs e) {
        //    ASPxSchedulerStorage storage = (ASPxSchedulerStorage)sender;
        //    int count = e.Objects.Count;
        //    System.Diagnostics.Debug.Assert(count == this.lastInsertedIdList.Count);
        //    for(int i = 0; i < count; i++) { //B184873
        //        Appointment apt = (Appointment)e.Objects[i];
        //        storage.SetAppointmentId(apt, this.lastInsertedIdList[i]);
        //    }
        //    this.lastInsertedIdList.Clear();
    }

    //void SetupMappings() {
    //    ASPxAppointmentMappingInfo mappings = Storage.Appointments.Mappings;
    //    Storage.BeginUpdate();
    //    try {
    //        mappings.AppointmentId = "Id";
    //        mappings.Start = "StartTime";
    //        mappings.End = "EndTime";
    //        mappings.Subject = "Subject";
    //        mappings.AllDay = "AllDay";
    //        mappings.Description = "Description";
    //        mappings.Label = "Label";
    //        mappings.Location = "Location";
    //        mappings.RecurrenceInfo = "RecurrenceInfo";
    //        mappings.ReminderInfo = "ReminderInfo";
    //        mappings.ResourceId = "OwnerId";
    //        mappings.Status = "Status";
    //        mappings.Type = "EventType";
    //    } finally {
    //        Storage.EndUpdate();
    //    }
    //}

    //// Populating ObjectDataSource
    protected void appointmentsDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e) {
        //    e.ObjectInstance = new CustomEventDataSource(GetCustomEvents());
    }
    //CustomEventList GetCustomEvents() {
    //    CustomEventList events = Session["ListBoundModeObjects"] as CustomEventList;
    //    if(events == null) {
    //        events = GenerateCustomEventList();
    //        Session["ListBoundModeObjects"] = events;
    //    }
    //    return events;
    //}

    //#region Random events generation
    //CustomEventList GenerateCustomEventList() {
    //    CustomEventList eventList = new CustomEventList();
    //    int count = Storage.Resources.Count;
    //    for(int i = 0; i < count; i++) {
    //        Resource resource = Storage.Resources[i];
    //        string subjPrefix = resource.Caption + "'s ";

    //        eventList.Add(CreateEvent(resource.Id, subjPrefix + "meeting", 2, 5));
    //        eventList.Add(CreateEvent(resource.Id, subjPrefix + "travel", 3, 6));
    //        eventList.Add(CreateEvent(resource.Id, subjPrefix + "phone call", 0, 10));
    //    }
    //    return eventList;
    //}
    //CustomEvent CreateEvent(object resourceId, string subject, int status, int label) {
    //    CustomEvent customEvent = new CustomEvent();
    //    customEvent.Subject = subject;
    //    customEvent.OwnerId = resourceId;
    //    Random rnd = SchedulerDemoUtils.RandomInstance;
    //    int rangeInHours = 48;
    //    customEvent.StartTime = DateTime.Today + TimeSpan.FromHours(rnd.Next(0, rangeInHours));
    //    customEvent.EndTime = customEvent.StartTime + TimeSpan.FromHours(rnd.Next(0, rangeInHours / 8));
    //    customEvent.Status = status;
    //    customEvent.Label = label;
    //    customEvent.Id = "ev" + customEvent.GetHashCode();
    //    return customEvent;
    //}
    //#endregion
}