﻿
using System;
using DevExpress.XtraScheduler;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxScheduler;
using System.Data.SqlClient;
public partial class _DefaultScheduler : System.Web.UI.Page {
    int lastInsertedAppointmentId;

    protected void Page_Load(object sender, EventArgs e) {
    }

    protected void Page_Init(object sender, EventArgs e) {
        ASPxScheduler1.Start = new DateTime(2008, 07, 13);
    }

    protected void ASPxScheduler1_AppointmentRowInserting(object sender, DevExpress.Web.ASPxScheduler.ASPxSchedulerDataInsertingEventArgs e) {
        e.NewValues.Remove("ID");

    }

    protected void ASPxScheduler1_AppointmentRowInserted(object sender, DevExpress.Web.ASPxScheduler.ASPxSchedulerDataInsertedEventArgs e) {
        e.KeyFieldValue = this.lastInsertedAppointmentId;

    }
    protected void ASPxScheduler1_AppointmentsInserted(object sender, PersistentObjectsEventArgs e) {
        int count = e.Objects.Count;
        //System.Diagnostics.Debug.Assert(count == 1);
        Appointment apt = (Appointment)e.Objects[0];
        ASPxSchedulerStorage storage = (ASPxSchedulerStorage)sender;
        storage.SetAppointmentId(apt, lastInsertedAppointmentId);
    }

    protected void CarsSchedulingDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e) {
        SqlConnection connection = (SqlConnection)e.Command.Connection;
        using (SqlCommand cmd = new SqlCommand("SELECT IDENT_CURRENT('CarScheduling')", connection)) {
            this.lastInsertedAppointmentId = Convert.ToInt32(cmd.ExecuteScalar());
        }
    }
}

