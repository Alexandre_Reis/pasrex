﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

using Financial.Contabil;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;

using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using System.Threading;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_ContabLancamento : CadastroBasePage {
       
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { ContabLancamentoMetadata.ColumnNames.Origem }));
    }

    #region DataSources
    protected void EsDSContabLancamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("CL");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        contabLancamentoQuery.Select(contabLancamentoQuery, clienteQuery.Apelido);
        contabLancamentoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == contabLancamentoQuery.IdCliente);
        contabLancamentoQuery.InnerJoin(clienteQuery).On(contabLancamentoQuery.IdCliente == clienteQuery.IdCliente);
        contabLancamentoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            contabLancamentoQuery.Where(contabLancamentoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento.LessThanOrEqual(textDataFim.Text));
        }

        contabLancamentoQuery.OrderBy(contabLancamentoQuery.Descricao.Ascending);
        //
        ContabLancamentoCollection coll = new ContabLancamentoCollection();
        coll.Load(contabLancamentoQuery);
        //        
        e.Collection = coll;
    }

    protected void EsDSPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }

    protected void EsDSCentroCusto_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabCentroCustoCollection coll = new ContabCentroCustoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);        

        e.Collection = coll;
    }

    /// <summary>
    /// PopUp ContabRoteiro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSContabRoteiro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        ContabRoteiroQuery contabRoteiroQuery = new ContabRoteiroQuery("R");
        ContabContaQuery contabContaDebitoQuery = new ContabContaQuery("D");
        ContabContaQuery contabContaCreditoQuery = new ContabContaQuery("C");

        contabRoteiroQuery.Select(contabRoteiroQuery,
                                  contabContaDebitoQuery,
                                  contabContaCreditoQuery,
                                  contabContaDebitoQuery.Codigo.As("CodigoDebito"),
                                  contabContaCreditoQuery.Codigo.As("CodigoCredito"),
                                  contabContaDebitoQuery.CodigoReduzida.As("CodigoReduzidaDebito"),
                                  contabContaCreditoQuery.CodigoReduzida.As("CodigoReduzidaCredito"));
        contabRoteiroQuery.InnerJoin(contabContaDebitoQuery).On(contabContaDebitoQuery.IdConta == contabRoteiroQuery.IdContaDebito);
        contabRoteiroQuery.InnerJoin(contabContaCreditoQuery).On(contabContaCreditoQuery.IdConta == contabRoteiroQuery.IdContaCredito);

        coll.Load(contabRoteiroQuery);
        //
        e.Collection = coll;
    }

    protected void EsDSPlanoPopup_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    #region CallBacks
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") 
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }

            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;        
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxButtonEdit textEvento = gridCadastro.FindEditFormTemplateControl("btnEditEvento") as ASPxButtonEdit;        

        #region Campos obrigatórios

        List<Control> controles = new List<Control>(new Control[] { 
            btnEditCodigoCliente, textDataRegistro, textValor, textEvento });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    #endregion

    #region Grid Cadastro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ContabLancamento contabLancamento = new ContabLancamento();
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        //
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        //        
        ASPxTextBox textContaCredito = gridCadastro.FindEditFormTemplateControl("textContaCredito") as ASPxTextBox;
        ASPxTextBox textContaDebito = gridCadastro.FindEditFormTemplateControl("textContaDebito") as ASPxTextBox;
        //ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxButtonEdit descricao = gridCadastro.FindEditFormTemplateControl("btnEditEvento") as ASPxButtonEdit;        
        //
        ASPxComboBox dropOrigem = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;
        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;
        //ASPxComboBox dropCentroCusto = gridCadastro.FindEditFormTemplateControl("dropCentroCusto") as ASPxComboBox;

        ASPxTextBox textIdContaCredito = gridCadastro.FindEditFormTemplateControl("textIdContaCredito") as ASPxTextBox;
        ASPxTextBox textIdContaDebito = gridCadastro.FindEditFormTemplateControl("textIdContaDebito") as ASPxTextBox;
        //        
        int idLancamento = (int)e.Keys[0];

        if (contabLancamento.LoadByPrimaryKey(idLancamento)) {
            contabLancamento.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            //
            contabLancamento.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
            contabLancamento.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            //
            contabLancamento.Valor = Convert.ToDecimal(textValor.Text);
            //       
            contabLancamento.ContaCredito = Convert.ToString(textContaCredito.Text).Trim();
            contabLancamento.ContaDebito = Convert.ToString(textContaDebito.Text).Trim();
            contabLancamento.Descricao = Convert.ToString(descricao.Text).Trim();
            //
            contabLancamento.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value);
            contabLancamento.IdPlano = Convert.ToInt32(dropPlano.SelectedItem.Value);
            //
            contabLancamento.IdContaCredito = Convert.ToInt32(textIdContaCredito.Text);
            contabLancamento.IdContaDebito = Convert.ToInt32(textIdContaDebito.Text);
            //
            contabLancamento.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabLancamento - Update: " + idLancamento + UtilitarioWeb.ToString(contabLancamento),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ContabLancamento contabLancamento = new ContabLancamento();
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        //
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        //        
        ASPxTextBox textContaCredito = gridCadastro.FindEditFormTemplateControl("textContaCredito") as ASPxTextBox;
        ASPxTextBox textContaDebito = gridCadastro.FindEditFormTemplateControl("textContaDebito") as ASPxTextBox;
        // ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxButtonEdit descricao = gridCadastro.FindEditFormTemplateControl("btnEditEvento") as ASPxButtonEdit;
        //
        ASPxComboBox dropOrigem = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;
        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;
        //ASPxComboBox dropCentroCusto = gridCadastro.FindEditFormTemplateControl("dropCentroCusto") as ASPxComboBox;
        //
        ASPxTextBox textIdContaCredito = gridCadastro.FindEditFormTemplateControl("textIdContaCredito") as ASPxTextBox;
        ASPxTextBox textIdContaDebito = gridCadastro.FindEditFormTemplateControl("textIdContaDebito") as ASPxTextBox;
        //    
        contabLancamento.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        contabLancamento.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
        contabLancamento.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        //
        contabLancamento.Valor = Convert.ToDecimal(textValor.Text);
        //       
        contabLancamento.ContaCredito = Convert.ToString(textContaCredito.Text).Trim();
        contabLancamento.ContaDebito = Convert.ToString(textContaDebito.Text).Trim();
        contabLancamento.Descricao = Convert.ToString(descricao.Text).Trim();
        //
        contabLancamento.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value);
        contabLancamento.IdPlano = Convert.ToInt32(dropPlano.SelectedItem.Value);
        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Manual;
        //
        contabLancamento.IdCentroCusto = null;
        //
        contabLancamento.IdContaCredito = Convert.ToInt32(textIdContaCredito.Text);
        contabLancamento.IdContaDebito = Convert.ToInt32(textIdContaDebito.Text);

        //if (dropCentroCusto.SelectedItem != null) {
        //    contabLancamento.IdCentroCusto = Convert.ToInt32(dropCentroCusto.SelectedItem.Value);
        //}
        //
        contabLancamento.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabLançamento - Insert: " + contabLancamento.IdLancamento + UtilitarioWeb.ToString(contabLancamento),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ContabLancamentoMetadata.ColumnNames.IdLancamento);

            for (int i = 0; i < keyValuesId.Count; i++) {
                int idLancamento = Convert.ToInt32(keyValuesId[i]);

                ContabLancamento c = new ContabLancamento();
                if (c.LoadByPrimaryKey(idLancamento)) {
                    contabLancamentoCollection.AttachEntity(c);
                }
            }
            //
            ContabLancamentoCollection contabLancamentoCollectionClone = (ContabLancamentoCollection)Utilitario.Clone(contabLancamentoCollection);
            //

            contabLancamentoCollection.MarkAllAsDeleted();
            contabLancamentoCollection.Save();

            foreach (ContabLancamento cl in contabLancamentoCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ContabLancamento - Delete: " + cl.IdLancamento + UtilitarioWeb.ToString(cl),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente", "textValor");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Lançamento) >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Lançamento) <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    } 
    #endregion

    #region Grid ContabRoteiro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idEvento = (int)gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdEvento);        
        //
        string contaDebito = Convert.ToString(gridView.GetRowValues(visibleIndex, "CodigoDebito"));
        string contaCredito = Convert.ToString(gridView.GetRowValues(visibleIndex, "CodigoCredito"));
        //
        string descricao = Convert.ToString(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Descricao));
        //
        int origem = Convert.ToInt32(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Origem));
        int idPlano = Convert.ToInt32(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdPlano));
        //
        int idContaCredito = Convert.ToInt32(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdContaCredito));
        int idContaDebito = Convert.ToInt32(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdContaDebito));
        //
        e.Result = idEvento.ToString() + "|" +
                   contaDebito + "|" +
                   contaCredito + "|" +
                   origem + "|" +
                   descricao + "|" +
                   idPlano + "|" +
                   idContaCredito + "|" +
                   idContaDebito;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridContabRoteiro.DataBind();
    }
   
    #endregion

    #region Modo Update/Insert

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditContabRoteiro_OnInit(object sender, EventArgs e) {
        //// Modo Update
        //if (!gridCadastro.IsNewRowEditing) {
        //    (sender as ASPxSpinEdit).ClientEnabled = false;
        //    (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        //}

        // Modo Inserção
        //if (gridCadastro.IsNewRowEditing) {
        //    (sender as ASPxSpinEdit).ReadOnly = true;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataRegistro_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).ClientEnabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataLancamento_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) { // Update
            (sender as ASPxDateEdit).ClientEnabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion
}