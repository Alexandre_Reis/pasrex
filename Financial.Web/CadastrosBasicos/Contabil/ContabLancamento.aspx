﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContabLancamento.aspx.cs" Inherits="CadastrosBasicos_ContabLancamento" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }        
    
    function OnGetDataClienteFiltro(data){
        if (popupFiltro.IsVisible()) {
            btnEditCodigoClienteFiltro.SetValue(data);        
            ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
            }
    }
        
    // PopContabRoteiro
    function OnGetDataContabRoteiro(data) {
        /* idEvento, contaDebito, contaCredito, origem, descricao, idPlano, idContaCredito, idContaDebito */
                
        var resultSplit = data.split('|');
        btnEditEvento.SetValue(resultSplit[4]);
        //                                
        textContaDebito.SetValue(resultSplit[1]);
        textContaCredito.SetValue(resultSplit[2]);
        //        
        dropOrigem.SetValue(resultSplit[3]);
        dropPlano.SetValue(resultSplit[5]);
        //
        textIdContaCredito.SetValue(resultSplit[6]);
        textIdContaDebito.SetValue(resultSplit[7]);        
        //
        popupContabRoteiro.HideWindow();
        btnEditEvento.Focus();        
    }
    </script>
    
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');                
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {                
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];            
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);

                if (gridCadastro.cp_EditVisibleIndex == 'new') {            
                    if (resultSplit[1] != null && resultSplit[1] != '') {
                        var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                        textDataLancamento.SetValue(newDate);
                        textDataRegistro.SetValue(newDate);
                    }                    
                    else {
                        textDataLancamento.SetValue(null);
                        textDataRegistro.SetValue(null);
                    }                
                }
            }        
        }
        "/>                       
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupContabRoteiro" ClientInstanceName="popupContabRoteiro" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridContabRoteiro" runat="server" Width="100%"
                    ClientInstanceName="gridContabRoteiro"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSContabRoteiro" KeyFieldName="IdEvento"
                    OnCustomDataCallback="gridContabRoteiro_CustomDataCallback" 
                    OnCustomCallback="gridContabRoteiro_CustomCallback"
                    OnHtmlRowCreated="gridContabRoteiro_HtmlRowCreated">               
            <Columns>
            
                    <dxwgv:GridViewDataTextColumn FieldName="IdConta" Visible="false"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="15%" VisibleIndex="0" Settings-AutoFilterCondition="Contains">                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="Conta Débito" Width="9%" VisibleIndex="1" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="Conta Crédito" Width="9%" VisibleIndex="2" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Identificador" Caption="Ativo" Width="8%" VisibleIndex="3"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Origem" VisibleIndex="4" Width="25%" ExportWidth="200">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="100" Text="<div title='Bolsa - Valorização Ação'>Bolsa - Valorização Ação</div>" />
                            <dxe:ListEditItem Value="101" Text="<div title='Bolsa - Reversão Valorização Ação'>Bolsa - Reversão Valorização Ação</div>" />
                            <dxe:ListEditItem Value="102" Text="<div title='Bolsa - Desvalorização Ação'>Bolsa - Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="103" Text="<div title='Bolsa - Reversão Desvalorização Ação'>Bolsa - Reversão Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="200" Text="<div title='Bolsa - Valorização Ação Short'>Bolsa - Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="201" Text="<div title='Bolsa - Reversão Valorização Ação Short'>Bolsa - Reversão Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="202" Text="<div title='Bolsa - Desvalorização Ação Short'>Bolsa - Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="203" Text="<div title='Bolsa - Reversão Desvalorização Ação Short'>Bolsa - Reversão Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="500" Text="<div title='Bolsa - Valorização OpçãoCompra Comprada'>Bolsa - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="501" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Comprada'>Bolsa - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="502" Text="<div title='Bolsa - Desvalorização OpçãoCompra Comprada'>Bolsa - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="503" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Comprada'>Bolsa - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="600" Text="<div title='Bolsa - Valorização OpçãoCompra Vendida'>Bolsa - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="601" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Vendida'>Bolsa - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="602" Text="<div title='Bolsa - Desvalorização OpçãoCompra Vendida'>Bolsa - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="603" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Vendida'>Bolsa - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="1000" Text="<div title='Bolsa - Valorização OpçãoVenda Comprada'>Bolsa - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1001" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Comprada'>Bolsa - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1002" Text="<div title='Bolsa - Desvalorização OpçãoVenda Comprada'>Bolsa - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1003" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Comprada'>Bolsa - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1100" Text="<div title='Bolsa - Valorização OpçãoVenda Vendida'>Bolsa - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1101" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Vendida'>Bolsa - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1102" Text="<div title='Bolsa - Desvalorização OpçãoVenda Vendida'>Bolsa - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1103" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Vendida'>Bolsa - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1500" Text="<div title='Bolsa - Compra Ação'>Bolsa - Compra Ação</div>" />
                            <dxe:ListEditItem Value="1501" Text="<div title='Bolsa - Compra Ação (Corretagem)'>Bolsa - Compra Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="1502" Text="<div title='Bolsa - Compra Ação (Emolumento)'>Bolsa - Compra Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="1503" Text="<div title='Bolsa - Compra Ação (Taxas CBLC)'>Bolsa - Compra Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2000" Text="<div title='Bolsa - Venda Ação'>Bolsa - Venda Ação</div>" />
                            <dxe:ListEditItem Value="2001" Text="<div title='Bolsa - Venda Ação (Corretagem)'>Bolsa - Venda Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2002" Text="<div title='Bolsa - Venda Ação (Emolumento)'>Bolsa - Venda Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2003" Text="<div title='Bolsa - Venda Ação (Taxas CBLC)'>Bolsa - Venda Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2500" Text="<div title='Bolsa - Compra OpçãoCompra'>Bolsa - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="2501" Text="<div title='Bolsa - Compra OpçãoCompra (Corretagem)'>Bolsa - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2502" Text="<div title='Bolsa - Compra OpçãoCompra (Emolumento)'>Bolsa - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2503" Text="<div title='Bolsa - Compra OpçãoCompra (Taxas CBLC)'>Bolsa - Compra OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3000" Text="<div title='Bolsa - Venda OpçãoCompra'>Bolsa - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="3001" Text="<div title='Bolsa - Venda OpçãoCompra (Corretagem)'>Bolsa - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3002" Text="<div title='Bolsa - Venda OpçãoCompra (Emolumento)'>Bolsa - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3003" Text="<div title='Bolsa - Venda OpçãoCompra (Taxas CBLC)'>Bolsa - Venda OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3500" Text="<div title='Bolsa - Compra OpçãoVenda'>Bolsa - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="3501" Text="<div title='Bolsa - Compra OpçãoVenda (Corretagem)'>Bolsa - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3502" Text="<div title='Bolsa - Compra OpçãoVenda (Emolumento)'>Bolsa - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3503" Text="<div title='Bolsa - Compra OpçãoVenda (Taxas CBLC)'>Bolsa - Compra OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4000" Text="<div title='Bolsa - Venda OpçãoVenda'>Bolsa - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4001" Text="<div title='Bolsa - Venda OpçãoVenda (Corretagem)'>Bolsa - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="4002" Text="<div title='Bolsa - Venda OpçãoVenda (Emolumento)'>Bolsa - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="4003" Text="<div title='Bolsa - Venda OpçãoVenda (Taxas CBLC)'>Bolsa - Venda OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4500" Text="<div title='Bolsa - Lucro Ação'>Bolsa - Lucro Ação</div>" />
                            <dxe:ListEditItem Value="4501" Text="<div title='Bolsa - Prejuízo Ação'>Bolsa - Prejuízo Ação</div>" />
                            <dxe:ListEditItem Value="4600" Text="<div title='Bolsa - Lucro Compra OpçãoCompra'>Bolsa - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4601" Text="<div title='Bolsa - Prejuízo Compra OpçãoCompra'>Bolsa - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4650" Text="<div title='Bolsa - Lucro Venda OpçãoCompra'>Bolsa - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4651" Text="<div title='Bolsa - Prejuízo Venda OpçãoCompra'>Bolsa - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4700" Text="<div title='Bolsa - Lucro Compra OpçãoVenda'>Bolsa - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4701" Text="<div title='Bolsa - Prejuízo Compra OpçãoVenda'>Bolsa - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4750" Text="<div title='Bolsa - Lucro Venda OpçãoVenda'>Bolsa - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4751" Text="<div title='Bolsa - Prejuízo Venda OpçãoVenda'>Bolsa - Prejuízo Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="5000" Text="<div title='Bolsa - Lucro Ação DayTrade'>Bolsa - Lucro Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5001" Text="<div title='Bolsa - Prejuízo Ação DayTrade'>Bolsa - Prejuízo Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5100" Text="<div title='Bolsa - Lucro Opção DayTrade'>Bolsa - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="5101" Text="<div title='Bolsa - Prejuízo Opção DayTrade'>Bolsa - Prejuízo Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="5200" Text="<div title='Bolsa - Lucro OpçãoVenda DayTrade'>Bolsa - Lucro OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="5201" Text="<div title='Bolsa - Prejuízo OpçãoVenda DayTrade'>Bolsa - Prejuízo OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="5500" Text="<div title='Bolsa - Expiração OpçãoCompra Comprada'>Bolsa - Expiração OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="5600" Text="<div title='Bolsa - Expiração OpçãoCompra Vendida'>Bolsa - Expiração OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="5700" Text="<div title='Bolsa - Expiração OpçãoVenda Comprada'>Bolsa - Expiração OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="5800" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />                            
                            <dxe:ListEditItem Value="6000" Text="<div title='Bolsa - Exercício OpçãoCompra Comprada'>Bolsa - Exercício OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="6100" Text="<div title='Bolsa - Exercício OpçãoCompra Vendida'>Bolsa - Exercício OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="6200" Text="<div title='Bolsa - Exercício OpçãoVenda Comprada'>Bolsa - Exercício OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="6300" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="7000" Text="<div title='Bolsa - Liquidação a Pagar'>Bolsa - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="7001" Text="<div title='Bolsa - Liquidação a Receber'>Bolsa - Liquidação a Receber</div>" />
                            <dxe:ListEditItem Value="8000" Text="<div title='Bolsa - Provisão Dividendos Receber'>Bolsa - Provisão Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8100" Text="<div title='Bolsa - Pagamento Dividendos Receber'>Bolsa - Pagamento Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8200" Text="<div title='Bolsa - Provisão Dividendos Pagar'>Bolsa - Provisão Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8300" Text="<div title='Bolsa - Pagamento Dividendos Pagar'>Bolsa - Pagamento Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8400" Text="<div title='Bolsa - Provisão Juros s/ Capital Receber'>Bolsa - Provisão Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8500" Text="<div title='Bolsa - Pagamento Juros s/ Capital Receber'>Bolsa - Pagamento Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8600" Text="<div title='Bolsa - Provisão Juros s/ Capital Pagar'>Bolsa - Provisão Juros s/ Capital Pagar</div>" />
                            <dxe:ListEditItem Value="8700" Text="<div title='Bolsa - Pagamento Juros s/ Capital Pagar'>Bolsa - Pagamento Juros s/ Capital Pagar</div>" />                            
                            <dxe:ListEditItem Value="8800" Text="<div title='Bolsa - Provisão Rendimentos Receber'>Bolsa - Provisão Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="8900" Text="<div title='Bolsa - Pagamento Rendimentos Receber'>Bolsa - Pagamento Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="9000" Text="<div title='Bolsa - Provisão Rendimentos Pagar'>Bolsa - Provisão Rendimentos Pagar</div>" />
                            <dxe:ListEditItem Value="9100" Text="<div title='Bolsa - Pagamento Rendimentos Pagar'>Bolsa - Pagamento Rendimentos Pagar</div>" />                                                        
                            <dxe:ListEditItem Value="10000" Text="<div title='Bolsa - BTC Tomado'>Bolsa - BTC Tomado</div>" />
                            <dxe:ListEditItem Value="10100" Text="<div title='Bolsa - BTC Tomado Devolução'>Bolsa - BTC Tomado Devolução</div>" />
                            <dxe:ListEditItem Value="10200" Text="<div title='Bolsa - BTC Doado'>Bolsa - BTC Doado</div>" />
                            <dxe:ListEditItem Value="10300" Text="<div title='Bolsa - BTC Doado Devolução'>Bolsa - BTC Doado Devolução</div>" />
                            <dxe:ListEditItem Value="10400" Text="<div title='Bolsa - BTC Tomado Variação Positiva'>Bolsa - BTC Tomado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10401" Text="<div title='Bolsa - BTC Tomado Variação Positiva (Reversão)'>Bolsa - BTC Tomado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10500" Text="<div title='Bolsa - BTC Tomado Variação Negativa'>Bolsa - BTC Tomado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10501" Text="<div title='Bolsa - BTC Tomado Variação Negativa (Reversão)'>Bolsa - BTC Tomado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10600" Text="<div title='Bolsa - BTC Doado Variação Positiva'>Bolsa - BTC Doado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10601" Text="<div title='Bolsa - BTC Doado Variação Positiva (Reversão)'>Bolsa - BTC Doado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10700" Text="<div title='Bolsa - BTC Doado Variação Negativa'>Bolsa - BTC Doado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10701" Text="<div title='Bolsa - BTC Doado Variação Negativa (Reversão)'>Bolsa - BTC Doado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10800" Text="<div title='Bolsa - BTC Tomado Provisão Taxas'>Bolsa - BTC Tomado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="10900" Text="<div title='Bolsa - BTC Tomado Pagto Taxas'>Bolsa - BTC Tomado Pagto Taxas</div>" />
                            <dxe:ListEditItem Value="11000" Text="<div title='Bolsa - BTC Doado Provisão Taxas'>Bolsa - BTC Doado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="11100" Text="<div title='Bolsa - BTC Doado Pagto Taxas'>Bolsa - BTC Doado Pagto Taxas</div>" />                            
                            <dxe:ListEditItem Value="14000" Text="<div title='Bolsa - Compra Termo'>Bolsa - Compra Termo</div>" />
                            <dxe:ListEditItem Value="14001" Text="<div title='Bolsa - Compra Termo (Corretagem)'>Bolsa - Compra Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14002" Text="<div title='Bolsa - Compra Termo (Emolumento)'>Bolsa - Compra Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14003" Text="<div title='Bolsa - Compra Termo (Taxas CBLC)'>Bolsa - Compra Termo (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="14500" Text="<div title='Bolsa - Venda Termo'>Bolsa - Venda Termo</div>"/>
                            <dxe:ListEditItem Value="14501" Text="<div title='Bolsa - Venda Termo (Corretagem)'>Bolsa - Venda Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14502" Text="<div title='Bolsa - Venda Termo (Emolumento)'>Bolsa - Venda Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14503" Text="<div title='Bolsa - Venda Termo (Taxas CBLC)'>Bolsa - Venda Termo (Taxas CBLC)</div>" />                            
                            <dxe:ListEditItem Value="14700" Text="<div title='Bolsa - Liquidação Física Termo Comprado'>Bolsa - Liquidação Física Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14750" Text="<div title='Bolsa - Liquidação Física Termo Vendido'>Bolsa - Liquidação Física Termo Vendido</div>" />
                            <dxe:ListEditItem Value="14800" Text="<div title='Bolsa - Liquidação Financeira Termo Comprado'>Bolsa - Liquidação Financeira Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14850" Text="<div title='Bolsa - Liquidação Financeira Termo Vendido'>Bolsa - Liquidação Financeira Termo Vendido</div>" />                            
                            <dxe:ListEditItem Value="15000" Text="<div title='Bolsa - Valorização Termo Comprado'>Bolsa - Valorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15010" Text="<div title='Bolsa - Desvalorização Termo Comprado'>Bolsa - Desvalorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15050" Text="<div title='Bolsa - Valorização Termo Vendido'>Bolsa - Valorização Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15060" Text="<div title='Bolsa - Desvalorização Termo Vendido'>Bolsa - Desvalorização Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15100" Text="<div title='Bolsa - Juros Termo Comprado'>Bolsa - Juros Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15110" Text="<div title='Bolsa - Juros Termo Vendido'>Bolsa - Juros Termo Vendido</div>" />
                            <dxe:ListEditItem Value="20000" Text="<div title='BMF - Valorização OpçãoCompra Comprada'>BMF - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20001" Text="<div title='BMF - Reversão Valorização OpçãoCompra Comprada'>BMF - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20002" Text="<div title='BMF - Desvalorização OpçãoCompra Comprada'>BMF - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20003" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Comprada'>BMF - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20100" Text="<div title='BMF - Valorização OpçãoCompra Vendida'>BMF - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20101" Text="<div title='BMF - Reversão Valorização OpçãoCompra Vendida'>BMF - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20102" Text="<div title='BMF - Desvalorização OpçãoCompra Vendida'>BMF - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20103" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Vendida'>BMF - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20500" Text="<div title='BMF - Valorização OpçãoVenda Comprada'>BMF - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20501" Text="<div title='BMF - Reversão Valorização OpçãoVenda Comprada'>BMF - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20502" Text="<div title='BMF - Desvalorização OpçãoVenda Comprada'>BMF - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20503" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Comprada'>BMF - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20600" Text="<div title='BMF - Valorização OpçãoVenda Vendida'>BMF - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20601" Text="<div title='BMF - Reversão Valorização OpçãoVenda Vendida'>BMF - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20602" Text="<div title='BMF - Desvalorização OpçãoVenda Vendida'>BMF - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20603" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Vendida'>BMF - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="21000" Text="<div title='BMF - Ajuste Positivo Futuros'>BMF - Ajuste Positivo Futuros</div>" />
                            <dxe:ListEditItem Value="21001" Text="<div title='BMF - Ajuste Negativo Futuros'>BMF - Ajuste Negativo Futuros</div>" />
                            <dxe:ListEditItem Value="21100" Text="<div title='BMF - Futuros (Corretagem)'>BMF - Futuros (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21101" Text="<div title='BMF - Futuros (Emolumento)'>BMF - Futuros (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21102" Text="<div title='BMF - Futuros (Registro)'>BMF - Futuros (Registro)</div>" />
                            <dxe:ListEditItem Value="21500" Text="<div title='BMF - Compra OpçãoCompra'>BMF - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="21600" Text="<div title='BMF - Compra OpçãoCompra (Corretagem)'>BMF - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21601" Text="<div title='BMF - Compra OpçãoCompra (Emolumento)'>BMF - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21602" Text="<div title='BMF - Compra OpçãoCompra (Registro)'>BMF - Compra OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22000" Text="<div title='BMF - Venda OpçãoCompra'>BMF - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="22100" Text="<div title='BMF - Venda OpçãoCompra (Corretagem)'>BMF - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22101" Text="<div title='BMF - Venda OpçãoCompra (Emolumento)'>BMF - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22102" Text="<div title='BMF - Venda OpçãoCompra (Registro)'>BMF - Venda OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22500" Text="<div title='BMF - Compra OpçãoVenda'>BMF - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="22600" Text="<div title='BMF - Compra OpçãoVenda (Corretagem)'>BMF - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22601" Text="<div title='BMF - Compra OpçãoVenda (Emolumento)'>BMF - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22602" Text="<div title='BMF - Compra OpçãoVenda (Registro)'>BMF - Compra OpçãoVenda (Registro)</div>" />
                            <dxe:ListEditItem Value="23000" Text="<div title='BMF - Venda OpçãoVenda'>BMF - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="23100" Text="<div title='BMF - Venda OpçãoVenda (Corretagem)'>BMF - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="23101" Text="<div title='BMF - Venda OpçãoVenda (Emolumento)'>BMF - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="23102" Text="<div title='BMF - Venda OpçãoVenda (Registro)'>BMF - Venda OpçãoVenda (Registro)</div>" />                            
                            <dxe:ListEditItem Value="23500" Text="<div title='BMF - Lucro Compra OpçãoCompra'>BMF - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23501" Text="<div title='BMF - Prejuízo Compra OpçãoCompra'>BMF - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23550" Text="<div title='BMF - Lucro Venda OpçãoCompra'>BMF - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23551" Text="<div title='BMF - Prejuízo Venda OpçãoCompra'>BMF - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="24000" Text="<div title='BMF - Lucro Compra OpçãoVenda'>BMF - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24001" Text="<div title='BMF - Prejuízo Compra OpçãoVenda'>BMF - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24050" Text="<div title='BMF - Lucro Venda OpçãoVenda'>BMF - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24051" Text="<div title='BMF - Prejuízo Venda OpçãoVenda'>BMF - Prejuízo Venda OpçãoVenda</div>" />                                    
                            <dxe:ListEditItem Value="25000" Text="<div title='BMF - Lucro Futuro DayTrade'>BMF - Lucro Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25001" Text="<div title='BMF - Prejuízo Futuro DayTrade'>BMF - Prejuízo Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25100" Text="<div title='BMF - Lucro Opção DayTrade'>BMF - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="25101" Text="<div title='BMF - Prejuízo Opção DayTrade'>BMF - Prejuízo Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="25200" Text="<div title='BMF - Lucro OpçãoVenda DayTrade'>BMF - Lucro OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="25201" Text="<div title='BMF - Prejuízo OpçãoVenda DayTrade'>BMF - Prejuízo OpçãoVenda DayTrade</div>" />                            
                            <dxe:ListEditItem Value="26000" Text="<div title='BMF - Liquidação a Pagar'>BMF - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="26001" Text="<div title='BMF - Liquidação a Receber'>BMF - Liquidação a Receber</div>" />
                            <dxe:ListEditItem Value="27000" Text="<div title='BMF - Provisão Taxa de Permanência'>BMF - Provisão Taxa de Permanência</div>" />
                            <dxe:ListEditItem Value="27001" Text="<div title='BMF - Pagamento Taxa de Permanência'>BMF - Pagamento Taxa de Permanência</div>" />
                            <dxe:ListEditItem Value="30000" Text="<div title='Renda Fixa - Compra Final'>Renda Fixa - Compra Final</div>" />
                            <dxe:ListEditItem Value="30500" Text="<div title='Renda Fixa - Venda Final'>Renda Fixa - Venda Final</div>" />
                            <dxe:ListEditItem Value="30600" Text="<div title='Renda Fixa - Lucro Venda Final'>Renda Fixa - Lucro Venda Final</div>" />
                            <dxe:ListEditItem Value="30601" Text="<div title='Renda Fixa - Prejuízo Venda Final'>Renda Fixa - Prejuízo Venda Final</div>" />
                            <dxe:ListEditItem Value="31000" Text="<div title='Renda Fixa - Compra Compromissada'>Renda Fixa - Compra Compromissada</div>" />
                            <dxe:ListEditItem Value="31100" Text="<div title='Renda Fixa - Revenda'>Renda Fixa - Revenda</div>" />
                            <dxe:ListEditItem Value="31200" Text="<div title='Renda Fixa - Lucro Revenda'>Renda Fixa - Lucro Revenda</div>" />
                            <dxe:ListEditItem Value="32000" Text="<div title='Renda Fixa - Ajuste MTM Positivo'>Renda Fixa - Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32001" Text="<div title='Renda Fixa - Ajuste MTM Negativo'>Renda Fixa - Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32002" Text="<div title='Renda Fixa - Reversão Ajuste MTM Positivo'>Renda Fixa - Reversão Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32003" Text="<div title='Renda Fixa - Reversão Ajuste MTM Negativo'>Renda Fixa - Reversão Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32500" Text="<div title='Renda Fixa - Renda Positiva'>Renda Fixa - Renda Positiva</div>" />
                            <dxe:ListEditItem Value="32501" Text="<div title='Renda Fixa - Renda Negativa'>Renda Fixa - Renda Negativa</div>" />
                            <dxe:ListEditItem Value="33000" Text="<div title='Renda Fixa - Vencimento'>Renda Fixa - Vencimento</div>" />
                            <dxe:ListEditItem Value="34000" Text="<div title='Renda Fixa - Pagamento Juros'>Renda Fixa - Pagamento Juros</div>" />
                            <dxe:ListEditItem Value="34100" Text="<div title='Renda Fixa - Pagamento Amortização'>Renda Fixa - Pagamento Amortização</div>" />
                            <dxe:ListEditItem Value="40000" Text="<div title='Swap - Ajuste Positivo'>Swap - Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40001" Text="<div title='Swap - Reversão Ajuste Positivo'>Swap - Reversão Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40100" Text="<div title='Swap - Ajuste Negativo'>Swap - Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="40101" Text="<div title='Swap - Reversão Ajuste Negativo'>Swap - Reversão Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="41000" Text="<div title='Swap - Liquidação Pagar'>Swap - Liquidação Pagar</div>" />
                            <dxe:ListEditItem Value="41001" Text="<div title='Swap - Liquidação Receber'>Swap - Liquidação Receber</div>" />
                            <dxe:ListEditItem Value="50000" Text="<div title='Fundos - Aplicação'>Fundos - Aplicação</div>" />
                            <dxe:ListEditItem Value="51000" Text="<div title='Fundos - Resgate'>Fundos - Resgate</div>" />
                            <dxe:ListEditItem Value="51100" Text="<div title='Fundos - Liquidação Resgate'>Fundos - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="52000" Text="<div title='Fundos - Valorização'>Fundos - Valorização</div>" />
                            <dxe:ListEditItem Value="52001" Text="<div title='Fundos - Desvalorização'>Fundos - Desvalorização</div>" />
                            <dxe:ListEditItem Value="60000" Text="<div title='Cotista - Aplicação Pessoa Física'>Cotista - Aplicação Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60500" Text="<div title='Cotista - Resgate (Custo) Pessoa Física'>Cotista - Resgate (Custo) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Física'>Cotista - Resgate (Variação Positiva) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Física'>Cotista - Resgate (Variação Negativa) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Física'>Cotista - Resgate (Provisão IR) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Física'>Cotista - Resgate (Provisão IOF) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="62000" Text="<div title='Cotista - Aplicação Pessoa Jurídica'>Cotista - Aplicação Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62500" Text="<div title='Cotista - Resgate (Custo) Pessoa Jurídica'>Cotista - Resgate (Custo) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Jurídica'>Cotista - Resgate (Variação Positiva) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Jurídica'>Cotista - Resgate (Variação Negativa) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Jurídica'>Cotista - Resgate (Provisão IR) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Jurídica'>Cotista - Resgate (Provisão IOF) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="64000" Text="<div title='Cotista - Liquidação Resgate'>Cotista - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="64100" Text="<div title='Cotista - Liquidação Resgate IR'>Cotista - Liquidação Resgate IR</div>" />
                            <dxe:ListEditItem Value="64200" Text="<div title='Cotista - Liquidação Resgate IOF'>Cotista - Liquidação Resgate IOF</div>" />
                            <dxe:ListEditItem Value="80000" Text="<div title='Despesas - Provisão'>Despesas - Provisão</div>" />
                            <dxe:ListEditItem Value="80100" Text="<div title='Despesas - Pagamento'>Despesas - Pagamento</div>" />
                            <dxe:ListEditItem Value="999999" Text="<div title='Outros'>Outros</div>" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdPlano" Caption="Plano" VisibleIndex="5" Width="10%">
                    <PropertiesComboBox DataSourceID="EsDSPlanoPopup" TextField="Descricao" ValueField="IdPlano"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaDebitoReduzida" Caption="Conta Débito Reduzida" Width="9%" VisibleIndex="7" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <dxwgv:GridViewDataTextColumn FieldName="ContaCreditoReduzida" Caption="Conta Crédito Reduzida" Width="9%" VisibleIndex="8" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"/>
                                                                    
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) { 
                                                gridContabRoteiro.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataContabRoteiro);
                              }" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Roteiro Contábil" />
            
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridContabRoteiro.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Lançamento Contábil"></asp:Label>
    </div>
           
    <div id="mainContent">
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                            </td>  
                            
                            <td colspan="2">
                                <table>
                                    <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                    <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                                </table>
                            </td>                                                                   
                        </tr>                        
                    </table>
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
             
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"  CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnCustomFields" OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Mais Campos"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdLancamento" DataSourceID="EsDSContabLancamento"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnPreRender="gridCadastro_PreRender"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>

                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="0" Width="8%" CellStyle-HorizontalAlign="left" ShowInCustomizationForm="false"/>                        
                    <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="1" Width="16%" ShowInCustomizationForm="false"/>
                                        
                    <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="2" Width="9%" ExportWidth="90" ShowInCustomizationForm="false"/>
                                        
                    <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="3" Width="10%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" ShowInCustomizationForm="false">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="C.Débito" Width="7%" VisibleIndex="4" ShowInCustomizationForm="false">
                    <PropertiesTextEdit MaxLength="22"/>
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="C.Crédito" Width="7%" VisibleIndex="5" ShowInCustomizationForm="false">
                    <PropertiesTextEdit MaxLength="22"/>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Origem" Visible="false" Width="22%" ShowInCustomizationForm="true">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="100" Text="<div title='Bolsa - Valorização Ação'>Bolsa - Valorização Ação</div>" />
                            <dxe:ListEditItem Value="101" Text="<div title='Bolsa - Reversão Valorização Ação'>Bolsa - Reversão Valorização Ação</div>" />
                            <dxe:ListEditItem Value="102" Text="<div title='Bolsa - Desvalorização Ação'>Bolsa - Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="103" Text="<div title='Bolsa - Reversão Desvalorização Ação'>Bolsa - Reversão Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="200" Text="<div title='Bolsa - Valorização Ação Short'>Bolsa - Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="201" Text="<div title='Bolsa - Reversão Valorização Ação Short'>Bolsa - Reversão Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="202" Text="<div title='Bolsa - Desvalorização Ação Short'>Bolsa - Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="203" Text="<div title='Bolsa - Reversão Desvalorização Ação Short'>Bolsa - Reversão Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="500" Text="<div title='Bolsa - Valorização OpçãoCompra Comprada'>Bolsa - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="501" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Comprada'>Bolsa - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="502" Text="<div title='Bolsa - Desvalorização OpçãoCompra Comprada'>Bolsa - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="503" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Comprada'>Bolsa - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="600" Text="<div title='Bolsa - Valorização OpçãoCompra Vendida'>Bolsa - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="601" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Vendida'>Bolsa - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="602" Text="<div title='Bolsa - Desvalorização OpçãoCompra Vendida'>Bolsa - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="603" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Vendida'>Bolsa - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="1000" Text="<div title='Bolsa - Valorização OpçãoVenda Comprada'>Bolsa - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1001" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Comprada'>Bolsa - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1002" Text="<div title='Bolsa - Desvalorização OpçãoVenda Comprada'>Bolsa - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1003" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Comprada'>Bolsa - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1100" Text="<div title='Bolsa - Valorização OpçãoVenda Vendida'>Bolsa - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1101" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Vendida'>Bolsa - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1102" Text="<div title='Bolsa - Desvalorização OpçãoVenda Vendida'>Bolsa - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1103" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Vendida'>Bolsa - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1500" Text="<div title='Bolsa - Compra Ação'>Bolsa - Compra Ação</div>" />
                            <dxe:ListEditItem Value="1501" Text="<div title='Bolsa - Compra Ação (Corretagem)'>Bolsa - Compra Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="1502" Text="<div title='Bolsa - Compra Ação (Emolumento)'>Bolsa - Compra Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="1503" Text="<div title='Bolsa - Compra Ação (Taxas CBLC)'>Bolsa - Compra Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2000" Text="<div title='Bolsa - Venda Ação'>Bolsa - Venda Ação</div>" />
                            <dxe:ListEditItem Value="2001" Text="<div title='Bolsa - Venda Ação (Corretagem)'>Bolsa - Venda Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2002" Text="<div title='Bolsa - Venda Ação (Emolumento)'>Bolsa - Venda Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2003" Text="<div title='Bolsa - Venda Ação (Taxas CBLC)'>Bolsa - Venda Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2500" Text="<div title='Bolsa - Compra OpçãoCompra'>Bolsa - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="2501" Text="<div title='Bolsa - Compra OpçãoCompra (Corretagem)'>Bolsa - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2502" Text="<div title='Bolsa - Compra OpçãoCompra (Emolumento)'>Bolsa - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2503" Text="<div title='Bolsa - Compra OpçãoCompra (Taxas CBLC)'>Bolsa - Compra OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3000" Text="<div title='Bolsa - Venda OpçãoCompra'>Bolsa - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="3001" Text="<div title='Bolsa - Venda OpçãoCompra (Corretagem)'>Bolsa - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3002" Text="<div title='Bolsa - Venda OpçãoCompra (Emolumento)'>Bolsa - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3003" Text="<div title='Bolsa - Venda OpçãoCompra (Taxas CBLC)'>Bolsa - Venda OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3500" Text="<div title='Bolsa - Compra OpçãoVenda'>Bolsa - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="3501" Text="<div title='Bolsa - Compra OpçãoVenda (Corretagem)'>Bolsa - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3502" Text="<div title='Bolsa - Compra OpçãoVenda (Emolumento)'>Bolsa - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3503" Text="<div title='Bolsa - Compra OpçãoVenda (Taxas CBLC)'>Bolsa - Compra OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4000" Text="<div title='Bolsa - Venda OpçãoVenda'>Bolsa - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4001" Text="<div title='Bolsa - Venda OpçãoVenda (Corretagem)'>Bolsa - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="4002" Text="<div title='Bolsa - Venda OpçãoVenda (Emolumento)'>Bolsa - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="4003" Text="<div title='Bolsa - Venda OpçãoVenda (Taxas CBLC)'>Bolsa - Venda OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4500" Text="<div title='Bolsa - Lucro Ação'>Bolsa - Lucro Ação</div>" />
                            <dxe:ListEditItem Value="4501" Text="<div title='Bolsa - Prejuízo Ação'>Bolsa - Prejuízo Ação</div>" />
                            <dxe:ListEditItem Value="4600" Text="<div title='Bolsa - Lucro Compra OpçãoCompra'>Bolsa - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4601" Text="<div title='Bolsa - Prejuízo Compra OpçãoCompra'>Bolsa - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4650" Text="<div title='Bolsa - Lucro Venda OpçãoCompra'>Bolsa - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4651" Text="<div title='Bolsa - Prejuízo Venda OpçãoCompra'>Bolsa - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4700" Text="<div title='Bolsa - Lucro Compra OpçãoVenda'>Bolsa - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4701" Text="<div title='Bolsa - Prejuízo Compra OpçãoVenda'>Bolsa - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4750" Text="<div title='Bolsa - Lucro Venda OpçãoVenda'>Bolsa - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4751" Text="<div title='Bolsa - Prejuízo Venda OpçãoVenda'>Bolsa - Prejuízo Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="5000" Text="<div title='Bolsa - Lucro Ação DayTrade'>Bolsa - Lucro Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5001" Text="<div title='Bolsa - Prejuízo Ação DayTrade'>Bolsa - Prejuízo Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5100" Text="<div title='Bolsa - Lucro Opção DayTrade'>Bolsa - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="5101" Text="<div title='Bolsa - Prejuízo Opção DayTrade'>Bolsa - Prejuízo Opção DayTrade</div>" />                            
                            <dxe:ListEditItem Value="5200" Text="<div title='Bolsa - Lucro OpçãoVenda DayTrade'>Bolsa - Lucro OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="5201" Text="<div title='Bolsa - Prejuízo OpçãoVenda DayTrade'>Bolsa - Prejuízo OpçãoVenda DayTrade</div>" />                            
                            <dxe:ListEditItem Value="5500" Text="<div title='Bolsa - Expiração OpçãoCompra Comprada'>Bolsa - Expiração OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="5600" Text="<div title='Bolsa - Expiração OpçãoCompra Vendida'>Bolsa - Expiração OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="5700" Text="<div title='Bolsa - Expiração OpçãoVenda Comprada'>Bolsa - Expiração OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="5800" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />                            
                            <dxe:ListEditItem Value="6000" Text="<div title='Bolsa - Exercício OpçãoCompra Comprada'>Bolsa - Exercício OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="6100" Text="<div title='Bolsa - Exercício OpçãoCompra Vendida'>Bolsa - Exercício OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="6200" Text="<div title='Bolsa - Exercício OpçãoVenda Comprada'>Bolsa - Exercício OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="6300" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />                            
                            <dxe:ListEditItem Value="7000" Text="<div title='Bolsa - Liquidação a Pagar'>Bolsa - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="7001" Text="<div title='Bolsa - Liquidação a Receber'>Bolsa - Liquidação a Receber</div>" />
                            <dxe:ListEditItem Value="8000" Text="<div title='Bolsa - Provisão Dividendos Receber'>Bolsa - Provisão Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8100" Text="<div title='Bolsa - Pagamento Dividendos Receber'>Bolsa - Pagamento Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8200" Text="<div title='Bolsa - Provisão Dividendos Pagar'>Bolsa - Provisão Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8300" Text="<div title='Bolsa - Pagamento Dividendos Pagar'>Bolsa - Pagamento Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8400" Text="<div title='Bolsa - Provisão Juros s/ Capital Receber'>Bolsa - Provisão Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8500" Text="<div title='Bolsa - Pagamento Juros s/ Capital Receber'>Bolsa - Pagamento Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8600" Text="<div title='Bolsa - Provisão Juros s/ Capital Pagar'>Bolsa - Provisão Juros s/ Capital Pagar</div>" />
                            <dxe:ListEditItem Value="8700" Text="<div title='Bolsa - Pagamento Juros s/ Capital Pagar'>Bolsa - Pagamento Juros s/ Capital Pagar</div>" />                            
                            <dxe:ListEditItem Value="8800" Text="<div title='Bolsa - Provisão Rendimentos Receber'>Bolsa - Provisão Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="8900" Text="<div title='Bolsa - Pagamento Rendimentos Receber'>Bolsa - Pagamento Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="9000" Text="<div title='Bolsa - Provisão Rendimentos Pagar'>Bolsa - Provisão Rendimentos Pagar</div>" />
                            <dxe:ListEditItem Value="9100" Text="<div title='Bolsa - Pagamento Rendimentos Pagar'>Bolsa - Pagamento Rendimentos Pagar</div>" />                                                        
                            <dxe:ListEditItem Value="10000" Text="<div title='Bolsa - BTC Tomado'>Bolsa - BTC Tomado</div>" />
                            <dxe:ListEditItem Value="10100" Text="<div title='Bolsa - BTC Tomado Devolução'>Bolsa - BTC Tomado Devolução</div>" />
                            <dxe:ListEditItem Value="10200" Text="<div title='Bolsa - BTC Doado'>Bolsa - BTC Doado</div>" />
                            <dxe:ListEditItem Value="10300" Text="<div title='Bolsa - BTC Doado Devolução'>Bolsa - BTC Doado Devolução</div>" />
                            <dxe:ListEditItem Value="10400" Text="<div title='Bolsa - BTC Tomado Variação Positiva'>Bolsa - BTC Tomado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10401" Text="<div title='Bolsa - BTC Tomado Variação Positiva (Reversão)'>Bolsa - BTC Tomado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10500" Text="<div title='Bolsa - BTC Tomado Variação Negativa'>Bolsa - BTC Tomado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10501" Text="<div title='Bolsa - BTC Tomado Variação Negativa (Reversão)'>Bolsa - BTC Tomado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10600" Text="<div title='Bolsa - BTC Doado Variação Positiva'>Bolsa - BTC Doado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10601" Text="<div title='Bolsa - BTC Doado Variação Positiva (Reversão)'>Bolsa - BTC Doado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10700" Text="<div title='Bolsa - BTC Doado Variação Negativa'>Bolsa - BTC Doado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10701" Text="<div title='Bolsa - BTC Doado Variação Negativa (Reversão)'>Bolsa - BTC Doado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10800" Text="<div title='Bolsa - BTC Tomado Provisão Taxas'>Bolsa - BTC Tomado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="10900" Text="<div title='Bolsa - BTC Tomado Pagto Taxas'>Bolsa - BTC Tomado Pagto Taxas</div>" />
                            <dxe:ListEditItem Value="11000" Text="<div title='Bolsa - BTC Doado Provisão Taxas'>Bolsa - BTC Doado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="11100" Text="<div title='Bolsa - BTC Doado Pagto Taxas'>Bolsa - BTC Doado Pagto Taxas</div>" />                            
                            <dxe:ListEditItem Value="14000" Text="<div title='Bolsa - Compra Termo'>Bolsa - Compra Termo</div>" />
                            <dxe:ListEditItem Value="14001" Text="<div title='Bolsa - Compra Termo (Corretagem)'>Bolsa - Compra Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14002" Text="<div title='Bolsa - Compra Termo (Emolumento)'>Bolsa - Compra Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14003" Text="<div title='Bolsa - Compra Termo (Taxas CBLC)'>Bolsa - Compra Termo (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="14500" Text="<div title='Bolsa - Venda Termo'>Bolsa - Venda Termo</div>"/>
                            <dxe:ListEditItem Value="14501" Text="<div title='Bolsa - Venda Termo (Corretagem)'>Bolsa - Venda Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14502" Text="<div title='Bolsa - Venda Termo (Emolumento)'>Bolsa - Venda Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14503" Text="<div title='Bolsa - Venda Termo (Taxas CBLC)'>Bolsa - Venda Termo (Taxas CBLC)</div>" />                            
                            <dxe:ListEditItem Value="14700" Text="<div title='Bolsa - Liquidação Física Termo Comprado'>Bolsa - Liquidação Física Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14750" Text="<div title='Bolsa - Liquidação Física Termo Vendido'>Bolsa - Liquidação Física Termo Vendido</div>" />
                            <dxe:ListEditItem Value="14800" Text="<div title='Bolsa - Liquidação Financeira Termo Comprado'>Bolsa - Liquidação Financeira Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14850" Text="<div title='Bolsa - Liquidação Financeira Termo Vendido'>Bolsa - Liquidação Financeira Termo Vendido</div>" />                            
                            <dxe:ListEditItem Value="15000" Text="<div title='Bolsa - Valorização Termo Comprado'>Bolsa - Valorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15010" Text="<div title='Bolsa - Desvalorização Termo Comprado'>Bolsa - Desvalorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15050" Text="<div title='Bolsa - Valorização Termo Vendido'>Bolsa - Valorização Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15060" Text="<div title='Bolsa - Desvalorização Termo Vendido'>Bolsa - Desvalorização Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15100" Text="<div title='Bolsa - Juros Termo Comprado'>Bolsa - Juros Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15110" Text="<div title='Bolsa - Juros Termo Vendido'>Bolsa - Juros Termo Vendido</div>" />
                            <dxe:ListEditItem Value="20000" Text="<div title='BMF - Valorização OpçãoCompra Comprada'>BMF - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20001" Text="<div title='BMF - Reversão Valorização OpçãoCompra Comprada'>BMF - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20002" Text="<div title='BMF - Desvalorização OpçãoCompra Comprada'>BMF - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20003" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Comprada'>BMF - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20100" Text="<div title='BMF - Valorização OpçãoCompra Vendida'>BMF - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20101" Text="<div title='BMF - Reversão Valorização OpçãoCompra Vendida'>BMF - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20102" Text="<div title='BMF - Desvalorização OpçãoCompra Vendida'>BMF - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20103" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Vendida'>BMF - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20500" Text="<div title='BMF - Valorização OpçãoVenda Comprada'>BMF - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20501" Text="<div title='BMF - Reversão Valorização OpçãoVenda Comprada'>BMF - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20502" Text="<div title='BMF - Desvalorização OpçãoVenda Comprada'>BMF - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20503" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Comprada'>BMF - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20600" Text="<div title='BMF - Valorização OpçãoVenda Vendida'>BMF - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20601" Text="<div title='BMF - Reversão Valorização OpçãoVenda Vendida'>BMF - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20602" Text="<div title='BMF - Desvalorização OpçãoVenda Vendida'>BMF - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20603" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Vendida'>BMF - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="21000" Text="<div title='BMF - Ajuste Positivo Futuros'>BMF - Ajuste Positivo Futuros</div>" />
                            <dxe:ListEditItem Value="21001" Text="<div title='BMF - Ajuste Negativo Futuros'>BMF - Ajuste Negativo Futuros</div>" />
                            <dxe:ListEditItem Value="21100" Text="<div title='BMF - Futuros (Corretagem)'>BMF - Futuros (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21101" Text="<div title='BMF - Futuros (Emolumento)'>BMF - Futuros (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21102" Text="<div title='BMF - Futuros (Registro)'>BMF - Futuros (Registro)</div>" />
                            <dxe:ListEditItem Value="21500" Text="<div title='BMF - Compra OpçãoCompra'>BMF - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="21600" Text="<div title='BMF - Compra OpçãoCompra (Corretagem)'>BMF - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21601" Text="<div title='BMF - Compra OpçãoCompra (Emolumento)'>BMF - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21602" Text="<div title='BMF - Compra OpçãoCompra (Registro)'>BMF - Compra OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22000" Text="<div title='BMF - Venda OpçãoCompra'>BMF - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="22100" Text="<div title='BMF - Venda OpçãoCompra (Corretagem)'>BMF - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22101" Text="<div title='BMF - Venda OpçãoCompra (Emolumento)'>BMF - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22102" Text="<div title='BMF - Venda OpçãoCompra (Registro)'>BMF - Venda OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22500" Text="<div title='BMF - Compra OpçãoVenda'>BMF - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="22600" Text="<div title='BMF - Compra OpçãoVenda (Corretagem)'>BMF - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22601" Text="<div title='BMF - Compra OpçãoVenda (Emolumento)'>BMF - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22602" Text="<div title='BMF - Compra OpçãoVenda (Registro)'>BMF - Compra OpçãoVenda (Registro)</div>" />
                            <dxe:ListEditItem Value="23000" Text="<div title='BMF - Venda OpçãoVenda'>BMF - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="23100" Text="<div title='BMF - Venda OpçãoVenda (Corretagem)'>BMF - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="23101" Text="<div title='BMF - Venda OpçãoVenda (Emolumento)'>BMF - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="23102" Text="<div title='BMF - Venda OpçãoVenda (Registro)'>BMF - Venda OpçãoVenda (Registro)</div>" />                            
                            <dxe:ListEditItem Value="23500" Text="<div title='BMF - Lucro Compra OpçãoCompra'>BMF - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23501" Text="<div title='BMF - Prejuízo Compra OpçãoCompra'>BMF - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23550" Text="<div title='BMF - Lucro Venda OpçãoCompra'>BMF - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23551" Text="<div title='BMF - Prejuízo Venda OpçãoCompra'>BMF - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="24000" Text="<div title='BMF - Lucro Compra OpçãoVenda'>BMF - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24001" Text="<div title='BMF - Prejuízo Compra OpçãoVenda'>BMF - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24050" Text="<div title='BMF - Lucro Venda OpçãoVenda'>BMF - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24051" Text="<div title='BMF - Prejuízo Venda OpçãoVenda'>BMF - Prejuízo Venda OpçãoVenda</div>" />                                    
                            <dxe:ListEditItem Value="25000" Text="<div title='BMF - Lucro Futuro DayTrade'>BMF - Lucro Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25001" Text="<div title='BMF - Prejuízo Futuro DayTrade'>BMF - Prejuízo Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25100" Text="<div title='BMF - Lucro Opção DayTrade'>BMF - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="25101" Text="<div title='BMF - Prejuízo Opção DayTrade'>BMF - Prejuízo Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="25200" Text="<div title='BMF - Lucro OpçãoVenda DayTrade'>BMF - Lucro OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="25201" Text="<div title='BMF - Prejuízo OpçãoVenda DayTrade'>BMF - Prejuízo OpçãoVenda DayTrade</div>" />
                            <dxe:ListEditItem Value="26000" Text="<div title='BMF - Liquidação a Pagar'>BMF - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="26001" Text="<div title='BMF - Liquidação a Receber'>BMF - Liquidação a Receber</div>" />
                            <dxe:ListEditItem Value="27000" Text="<div title='BMF - Provisão Taxa de Permanência'>BMF - Provisão Taxa de Permanência</div>" />
                            <dxe:ListEditItem Value="27001" Text="<div title='BMF - Pagamento Taxa de Permanência'>BMF - Pagamento Taxa de Permanência</div>" />
                            <dxe:ListEditItem Value="30000" Text="<div title='Renda Fixa - Compra Final'>Renda Fixa - Compra Final</div>" />
                            <dxe:ListEditItem Value="30500" Text="<div title='Renda Fixa - Venda Final'>Renda Fixa - Venda Final</div>" />
                            <dxe:ListEditItem Value="30600" Text="<div title='Renda Fixa - Lucro Venda Final'>Renda Fixa - Lucro Venda Final</div>" />
                            <dxe:ListEditItem Value="30601" Text="<div title='Renda Fixa - Prejuízo Venda Final'>Renda Fixa - Prejuízo Venda Final</div>" />
                            <dxe:ListEditItem Value="31000" Text="<div title='Renda Fixa - Compra Compromissada'>Renda Fixa - Compra Compromissada</div>" />
                            <dxe:ListEditItem Value="31100" Text="<div title='Renda Fixa - Revenda'>Renda Fixa - Revenda</div>" />
                            <dxe:ListEditItem Value="31200" Text="<div title='Renda Fixa - Lucro Revenda'>Renda Fixa - Lucro Revenda</div>" />
                            <dxe:ListEditItem Value="32000" Text="<div title='Renda Fixa - Ajuste MTM Positivo'>Renda Fixa - Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32001" Text="<div title='Renda Fixa - Ajuste MTM Negativo'>Renda Fixa - Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32002" Text="<div title='Renda Fixa - Reversão Ajuste MTM Positivo'>Renda Fixa - Reversão Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32003" Text="<div title='Renda Fixa - Reversão Ajuste MTM Negativo'>Renda Fixa - Reversão Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32500" Text="<div title='Renda Fixa - Renda Positiva'>Renda Fixa - Renda Positiva</div>" />
                            <dxe:ListEditItem Value="32501" Text="<div title='Renda Fixa - Renda Negativa'>Renda Fixa - Renda Negativa</div>" />
                            <dxe:ListEditItem Value="33000" Text="<div title='Renda Fixa - Vencimento'>Renda Fixa - Vencimento</div>" />
                            <dxe:ListEditItem Value="34000" Text="<div title='Renda Fixa - Pagamento Juros'>Renda Fixa - Pagamento Juros</div>" />
                            <dxe:ListEditItem Value="34100" Text="<div title='Renda Fixa - Pagamento Amortização'>Renda Fixa - Pagamento Amortização</div>" />
                            <dxe:ListEditItem Value="40000" Text="<div title='Swap - Ajuste Positivo'>Swap - Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40001" Text="<div title='Swap - Reversão Ajuste Positivo'>Swap - Reversão Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40100" Text="<div title='Swap - Ajuste Negativo'>Swap - Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="40101" Text="<div title='Swap - Reversão Ajuste Negativo'>Swap - Reversão Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="41000" Text="<div title='Swap - Liquidação Pagar'>Swap - Liquidação Pagar</div>" />
                            <dxe:ListEditItem Value="41001" Text="<div title='Swap - Liquidação Receber'>Swap - Liquidação Receber</div>" />
                            <dxe:ListEditItem Value="50000" Text="<div title='Fundos - Aplicação'>Fundos - Aplicação</div>" />
                            <dxe:ListEditItem Value="51000" Text="<div title='Fundos - Resgate'>Fundos - Resgate</div>" />
                            <dxe:ListEditItem Value="51100" Text="<div title='Fundos - Liquidação Resgate'>Fundos - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="52000" Text="<div title='Fundos - Valorização'>Fundos - Valorização</div>" />
                            <dxe:ListEditItem Value="52001" Text="<div title='Fundos - Desvalorização'>Fundos - Desvalorização</div>" />
                            <dxe:ListEditItem Value="60000" Text="<div title='Cotista - Aplicação Pessoa Física'>Cotista - Aplicação Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60500" Text="<div title='Cotista - Resgate (Custo) Pessoa Física'>Cotista - Resgate (Custo) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Física'>Cotista - Resgate (Variação Positiva) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Física'>Cotista - Resgate (Variação Negativa) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Física'>Cotista - Resgate (Provisão IR) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Física'>Cotista - Resgate (Provisão IOF) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="62000" Text="<div title='Cotista - Aplicação Pessoa Jurídica'>Cotista - Aplicação Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62500" Text="<div title='Cotista - Resgate (Custo) Pessoa Jurídica'>Cotista - Resgate (Custo) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Jurídica'>Cotista - Resgate (Variação Positiva) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Jurídica'>Cotista - Resgate (Variação Negativa) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Jurídica'>Cotista - Resgate (Provisão IR) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Jurídica'>Cotista - Resgate (Provisão IOF) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="64000" Text="<div title='Cotista - Liquidação Resgate'>Cotista - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="64100" Text="<div title='Cotista - Liquidação Resgate IR'>Cotista - Liquidação Resgate IR</div>" />
                            <dxe:ListEditItem Value="64200" Text="<div title='Cotista - Liquidação Resgate IOF'>Cotista - Liquidação Resgate IOF</div>" />
                            <dxe:ListEditItem Value="80000" Text="<div title='Despesas - Provisão'>Despesas - Provisão</div>" />
                            <dxe:ListEditItem Value="80100" Text="<div title='Despesas - Pagamento'>Despesas - Pagamento</div>" />
                            <dxe:ListEditItem Value="999999" Text="<div title='Outros'>Outros</div>" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>                    
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="22%" VisibleIndex="8" ShowInCustomizationForm="false">                                        
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdPlano" Caption="Plano" VisibleIndex="9" Width="10%" ShowInCustomizationForm="false">
                    <PropertiesComboBox DataSourceID="EsDSPlano" TextField="Descricao" ValueField="IdPlano"/>
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" Caption="Registro" VisibleIndex="11" Width="9%" ShowInCustomizationForm="false"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Fonte" Visible="false" ShowInCustomizationForm="true" Width="15%">                        
                        <PropertiesComboBox>
                        <Items>                        
                            <dxe:ListEditItem Value="1" Text="Interno" />
                            <dxe:ListEditItem Value="2" Text="Manual" />
                            <dxe:ListEditItem Value="3" Text="Liquidação" />                            
                        </Items>
                        </PropertiesComboBox>                        
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="IdCentroCusto" Visible="false" ShowInCustomizationForm="false"/>
                    <dxwgv:GridViewDataTextColumn FieldName="IdContaDebito" Visible="false" ShowInCustomizationForm="false"/>
                    <dxwgv:GridViewDataTextColumn FieldName="IdContaCredito" Visible="false" ShowInCustomizationForm="false"/>

                </Columns>
                
                <Templates>
            <EditForm>
                                
                <div class="editForm">
                    <dxe:ASPxTextBox ID="textIdContaCredito" ClientInstanceName="textIdContaCredito" runat="server" ClientVisible="false" Text='<%#Eval("IdContaCredito")%>'></dxe:ASPxTextBox>
                    <dxe:ASPxTextBox ID="textIdContaDebito" ClientInstanceName="textIdContaDebito" runat="server" ClientVisible="false" Text='<%#Eval("IdContaDebito")%>'></dxe:ASPxTextBox>
                    
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"/>
                            </td>             
                            
                            <td>                                                                         
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>'                                                    
                                            MaxLength="10" NumberType="Integer" OnLoad="btnEditCodigoCliente_Load" >            
                                <Buttons>                                           
                                    <dxe:EditButton/>
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                        />                                                                                               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td colspan="2">
                                <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' />
                            </td>
                        </tr>                                                                                                                    
                                                   
                        <tr>                            
                            <td  class="td_Label">
                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Lançamento:" />
                            </td>                                     
                            <td>
                                <dxe:ASPxDateEdit ID="textDataLancamento" runat="server" ClientInstanceName="textDataLancamento" Value='<%#Eval("DataLancamento")%>' OnInit="textDataLancamento_Init" />
                            </td> 
                            
                            <td  class="td_Label">
                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Registro:" />
                            </td>                                     
                            <td>
                                <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro" Value='<%#Eval("DataRegistro")%>' OnInit="textDataRegistro_Init" />
                            </td> 

                        </tr>     

                        <tr>
                            <td  class="td_Label">
                                <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"/>
                            </td>
                            <td colspan="3">
                                <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                 MaxLength="12" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('Valor')%>"/>
                            </td>    
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelEvento" runat="server" CssClass="labelRequired" Text="Evento:" />
                            </td>
                            <td colspan="3">
                                <dxe:ASPxButtonEdit ID="btnEditEvento" runat="server" ReadOnly="true"
                                                    Text='<%#Eval("Descricao")%>' ClientInstanceName="btnEditEvento"
                                                    CssClass="textButtonEditLongo3"
                                                    OnInit="btnEditContabRoteiro_OnInit">
                                <Buttons>
                                    <dxe:EditButton></dxe:EditButton>                
                                </Buttons>
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupContabRoteiro.ShowAtElementByID(s.name);}"                                                  
                                        />                                                                   
                                </dxe:ASPxButtonEdit>
                                                                                                                                                                
                            </td>                                                               
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelContaCredito" runat="server" CssClass="labelNormal" Text="Conta Crédito:" />
                            </td>
                            <td colspan="3">                                                        
                                <dxe:ASPxTextBox ID="textContaCredito" ClientInstanceName="textContaCredito" runat="server" ClientEnabled="false" BackColor="#EBEBEB"
                                 MaxLength="22" Text='<%#Eval("ContaCredito")%>'/>
                            </td>                                                        
                        </tr>                            

                        <tr>
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelContaDebito" runat="server" CssClass="labelNormal" Text="Conta Débito:" />
                            </td>
                            <td colspan="3">                                                        
                                <dxe:ASPxTextBox ID="textContaDebito" ClientInstanceName="textContaDebito" runat="server" ClientEnabled="false" BackColor="#EBEBEB"
                                MaxLength="22" Text='<%#Eval("ContaDebito")%>'/>
                            </td>                                                        
                        </tr>                            
                                                
                        <tr>                                        
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelOrigem" runat="server" CssClass="labelNormal" Text="Origem:" />
                            </td>
                                                
                             <td colspan="3">
                                <dxe:ASPxComboBox ID="dropOrigem" ClientInstanceName="dropOrigem" runat="server" ShowShadow="false" CssClass="dropDownListLongo5" ClientEnabled="false"
                                                    DropDownStyle="DropDownList" Text='<%# Eval("Origem") %>'>
                                
                                <Items>
                                    <dxe:ListEditItem Value="100" Text="Bolsa - Valorização Ação" />
                                    <dxe:ListEditItem Value="101" Text="Bolsa - Reversão Valorização Ação" />
                                    <dxe:ListEditItem Value="102" Text="Bolsa - Desvalorização Ação" />
                                    <dxe:ListEditItem Value="103" Text="Bolsa - Reversão Desvalorização Ação" />
                                    <dxe:ListEditItem Value="200" Text="Bolsa - Valorização Ação Short" />
                                    <dxe:ListEditItem Value="201" Text="Bolsa - Reversão Valorização Ação Short" />
                                    <dxe:ListEditItem Value="202" Text="Bolsa - Desvalorização Ação Short" />
                                    <dxe:ListEditItem Value="203" Text="Bolsa - Reversão Desvalorização Ação Short" />
                                    <dxe:ListEditItem Value="500" Text="Bolsa - Valorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="501" Text="Bolsa - Reversão Valorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="502" Text="Bolsa - Desvalorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="503" Text="Bolsa - Reversão Desvalorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="600" Text="Bolsa - Valorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="601" Text="Bolsa - Reversão Valorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="602" Text="Bolsa - Desvalorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="603" Text="Bolsa - Reversão Desvalorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="1000" Text="Bolsa - Valorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="1001" Text="Bolsa - Reversão Valorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="1002" Text="Bolsa - Desvalorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="1003" Text="Bolsa - Reversão Desvalorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="1100" Text="Bolsa - Valorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="1101" Text="Bolsa - Reversão Valorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="1102" Text="Bolsa - Desvalorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="1103" Text="Bolsa - Reversão Desvalorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="1500" Text="Bolsa - Compra Ação" />
                                    <dxe:ListEditItem Value="1501" Text="Bolsa - Compra Ação (Corretagem)" />
                                    <dxe:ListEditItem Value="1502" Text="Bolsa - Compra Ação (Emolumento)" />
                                    <dxe:ListEditItem Value="1503" Text="Bolsa - Compra Ação (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="2000" Text="Bolsa - Venda Ação" />
                                    <dxe:ListEditItem Value="2001" Text="Bolsa - Venda Ação (Corretagem)" />
                                    <dxe:ListEditItem Value="2002" Text="Bolsa - Venda Ação (Emolumento)" />
                                    <dxe:ListEditItem Value="2003" Text="Bolsa - Venda Ação (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="2500" Text="Bolsa - Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="2501" Text="Bolsa - Compra OpçãoCompra (Corretagem)" />
                                    <dxe:ListEditItem Value="2502" Text="Bolsa - Compra OpçãoCompra (Emolumento)" />
                                    <dxe:ListEditItem Value="2503" Text="Bolsa - Compra OpçãoCompra (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="3000" Text="Bolsa - Venda OpçãoCompra" />
                                    <dxe:ListEditItem Value="3001" Text="Bolsa - Venda OpçãoCompra (Corretagem)" />
                                    <dxe:ListEditItem Value="3002" Text="Bolsa - Venda OpçãoCompra (Emolumento)" />
                                    <dxe:ListEditItem Value="3003" Text="Bolsa - Venda OpçãoCompra (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="3500" Text="Bolsa - Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="3501" Text="Bolsa - Compra OpçãoVenda (Corretagem)" />
                                    <dxe:ListEditItem Value="3502" Text="Bolsa - Compra OpçãoVenda (Emolumento)" />
                                    <dxe:ListEditItem Value="3503" Text="Bolsa - Compra OpçãoVenda (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="4000" Text="Bolsa - Venda OpçãoVenda" />
                                    <dxe:ListEditItem Value="4001" Text="Bolsa - Venda OpçãoVenda (Corretagem)" />
                                    <dxe:ListEditItem Value="4002" Text="Bolsa - Venda OpçãoVenda (Emolumento)" />
                                    <dxe:ListEditItem Value="4003" Text="Bolsa - Venda OpçãoVenda (Taxas CBLC)" />
                                    <dxe:ListEditItem Value="4500" Text="Bolsa - Lucro Ação" />
                                    <dxe:ListEditItem Value="4501" Text="Bolsa - Prejuízo Ação" />
                                    <dxe:ListEditItem Value="4600" Text="Bolsa - Lucro Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="4601" Text="Bolsa - Prejuízo Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="4650" Text="Bolsa - Lucro Venda OpçãoCompra" />
                                    <dxe:ListEditItem Value="4651" Text="Bolsa - Prejuízo Venda OpçãoCompra" />                            
                                    <dxe:ListEditItem Value="4700" Text="Bolsa - Lucro Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="4701" Text="Bolsa - Prejuízo Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="4750" Text="Bolsa - Lucro Venda OpçãoVenda" />
                                    <dxe:ListEditItem Value="4751" Text="Bolsa - Prejuízo Venda OpçãoVenda" />
                                    <dxe:ListEditItem Value="5000" Text="Bolsa - Lucro Ação DayTrade" />
                                    <dxe:ListEditItem Value="5001" Text="Bolsa - Prejuízo Ação DayTrade" />
                                    <dxe:ListEditItem Value="5100" Text="Bolsa - Lucro Opção DayTrade" />
                                    <dxe:ListEditItem Value="5101" Text="Bolsa - Prejuízo Opção DayTrade" />
                                    <dxe:ListEditItem Value="5200" Text="Bolsa - Lucro OpçãoVenda DayTrade" />
                                    <dxe:ListEditItem Value="5201" Text="Bolsa - Prejuízo OpçãoVenda DayTrade" />
                                    <dxe:ListEditItem Value="5500" Text="Bolsa - Expiração OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="5600" Text="Bolsa - Expiração OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="5700" Text="Bolsa - Expiração OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="5800" Text="Bolsa - Expiração OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="7000" Text="Bolsa - Liquidação a Pagar" />
                                    <dxe:ListEditItem Value="7001" Text="Bolsa - Liquidação a Receber" />                        
                                    <dxe:ListEditItem Value="8000" Text="Bolsa - Provisão Dividendos Receber" />
                                    <dxe:ListEditItem Value="8100" Text="Bolsa - Pagamento Dividendos Receber" />
                                    <dxe:ListEditItem Value="8200" Text="Bolsa - Provisão Dividendos Pagar" />
                                    <dxe:ListEditItem Value="8300" Text="Bolsa - Pagamento Dividendos Pagar" />                            
                                    <dxe:ListEditItem Value="8400" Text="Bolsa - Provisão Juros s/ Capital Receber" />
                                    <dxe:ListEditItem Value="8500" Text="Bolsa - Pagamento Juros s/ Capital Receber" />
                                    <dxe:ListEditItem Value="8600" Text="Bolsa - Provisão Juros s/ Capital Pagar" />
                                    <dxe:ListEditItem Value="8700" Text="Bolsa - Pagamento Juros s/ Capital Pagar" />                                    
                                    <dxe:ListEditItem Value="8800" Text="Bolsa - Provisão Juros s/ Capital Receber" />
                                    <dxe:ListEditItem Value="8900" Text="Bolsa - Pagamento Juros s/ Capital Receber" />
                                    <dxe:ListEditItem Value="9000" Text="Bolsa - Provisão Juros s/ Capital Pagar" />
                                    <dxe:ListEditItem Value="9100" Text="Bolsa - Pagamento Juros s/ Capital Pagar" />                                    
                                    <dxe:ListEditItem Value="10000" Text="Bolsa - BTC Tomado" />
                                    <dxe:ListEditItem Value="10100" Text="Bolsa - BTC Tomado Devolução" />
                                    <dxe:ListEditItem Value="10200" Text="Bolsa - BTC Doado" />
                                    <dxe:ListEditItem Value="10300" Text="Bolsa - BTC Doado Devolução" />
                                    <dxe:ListEditItem Value="10400" Text="Bolsa - BTC Tomado Variação Positiva" />
                                    <dxe:ListEditItem Value="10401" Text="Bolsa - BTC Tomado Variação Positiva (Reversão)" />
                                    <dxe:ListEditItem Value="10500" Text="Bolsa - BTC Tomado Variação Negativa" />
                                    <dxe:ListEditItem Value="10501" Text="Bolsa - BTC Tomado Variação Negativa (Reversão)" />
                                    <dxe:ListEditItem Value="10600" Text="Bolsa - BTC Doado Variação Positiva" />
                                    <dxe:ListEditItem Value="10601" Text="Bolsa - BTC Doado Variação Positiva (Reversão)" />
                                    <dxe:ListEditItem Value="10700" Text="Bolsa - BTC Doado Variação Negativa" />
                                    <dxe:ListEditItem Value="10701" Text="Bolsa - BTC Doado Variação Negativa (Reversão)" />
                                    <dxe:ListEditItem Value="10800" Text="Bolsa - BTC Tomado Provisão Taxas" />
                                    <dxe:ListEditItem Value="10900" Text="Bolsa - BTC Tomado Pagto Taxas" />
                                    <dxe:ListEditItem Value="11000" Text="Bolsa - BTC Doado Provisão Taxas" />
                                    <dxe:ListEditItem Value="11100" Text="Bolsa - BTC Doado Pagto Taxas" />
                                    <dxe:ListEditItem Value="14000" Text="Bolsa - Compra Termo" />
                                    <dxe:ListEditItem Value="14001" Text="Bolsa - Compra Termo (Corretagem)" />
                                    <dxe:ListEditItem Value="14002" Text="Bolsa - Compra Termo (Emolumento)" />
                                    <dxe:ListEditItem Value="14003" Text="Bolsa - Compra Termo (Taxas CBLC)" />                                    
                                    <dxe:ListEditItem Value="14500" Text="Bolsa - Venda Termo" />
                                    <dxe:ListEditItem Value="14501" Text="Bolsa - Venda Termo (Corretagem)" />
                                    <dxe:ListEditItem Value="14502" Text="Bolsa - Venda Termo (Emolumento)" />
                                    <dxe:ListEditItem Value="14503" Text="Bolsa - Venda Termo (Taxas CBLC)" />                                    
                                    <dxe:ListEditItem Value="14700" Text="Bolsa - Liquidação Física Termo Comprado" />
                                    <dxe:ListEditItem Value="14750" Text="Bolsa - Liquidação Física Termo Vendido" />
                                    <dxe:ListEditItem Value="14800" Text="Bolsa - Liquidação Financeira Termo Comprado" />
                                    <dxe:ListEditItem Value="14850" Text="Bolsa - Liquidação Financeira Termo Vendido" />
                                    <dxe:ListEditItem Value="15000" Text="Bolsa - Valorização Termo Comprado" />
                                    <dxe:ListEditItem Value="15010" Text="Bolsa - Desvalorização Termo Comprado" />
                                    <dxe:ListEditItem Value="15050" Text="Bolsa - Valorização Termo Vendido" />
                                    <dxe:ListEditItem Value="15060" Text="Bolsa - Desvalorização Termo Vendido" />                            
                                    <dxe:ListEditItem Value="15100" Text="Bolsa - Juros Termo Comprado" />
                                    <dxe:ListEditItem Value="15110" Text="Bolsa - Juros Termo Vendido" />                            
                                    <dxe:ListEditItem Value="20000" Text="BMF - Valorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="20001" Text="BMF - Reversão Valorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="20002" Text="BMF - Desvalorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="20003" Text="BMF - Reversão Desvalorização OpçãoCompra Comprada" />
                                    <dxe:ListEditItem Value="20100" Text="BMF - Valorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="20101" Text="BMF - Reversão Valorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="20102" Text="BMF - Desvalorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="20103" Text="BMF - Reversão Desvalorização OpçãoCompra Vendida" />
                                    <dxe:ListEditItem Value="20500" Text="BMF - Valorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="20501" Text="BMF - Reversão Valorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="20502" Text="BMF - Desvalorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="20503" Text="BMF - Reversão Desvalorização OpçãoVenda Comprada" />
                                    <dxe:ListEditItem Value="20600" Text="BMF - Valorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="20601" Text="BMF - Reversão Valorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="20602" Text="BMF - Desvalorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="20603" Text="BMF - Reversão Desvalorização OpçãoVenda Vendida" />
                                    <dxe:ListEditItem Value="21000" Text="BMF - Ajuste Positivo Futuros" />
                                    <dxe:ListEditItem Value="21001" Text="BMF - Ajuste Negativo Futuros" />
                                    <dxe:ListEditItem Value="21100" Text="BMF - Futuros (Corretagem)" />
                                    <dxe:ListEditItem Value="21101" Text="BMF - Futuros (Emolumento)" />
                                    <dxe:ListEditItem Value="21102" Text="BMF - Futuros (Registro)" />
                                    <dxe:ListEditItem Value="21500" Text="BMF - Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="21600" Text="BMF - Compra OpçãoCompra (Corretagem)" />
                                    <dxe:ListEditItem Value="21601" Text="BMF - Compra OpçãoCompra (Emolumento)" />
                                    <dxe:ListEditItem Value="21602" Text="BMF - Compra OpçãoCompra (Registro)" />
                                    <dxe:ListEditItem Value="22000" Text="BMF - Venda OpçãoCompra" />
                                    <dxe:ListEditItem Value="22100" Text="BMF - Venda OpçãoCompra (Corretagem)" />
                                    <dxe:ListEditItem Value="22101" Text="BMF - Venda OpçãoCompra (Emolumento)" />
                                    <dxe:ListEditItem Value="22102" Text="BMF - Venda OpçãoCompra (Registro)" />
                                    <dxe:ListEditItem Value="22500" Text="BMF - Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="22600" Text="BMF - Compra OpçãoVenda (Corretagem)" />
                                    <dxe:ListEditItem Value="22601" Text="BMF - Compra OpçãoVenda (Emolumento)" />
                                    <dxe:ListEditItem Value="22602" Text="BMF - Compra OpçãoVenda (Registro)" />
                                    <dxe:ListEditItem Value="23000" Text="BMF - Venda OpçãoVenda" />
                                    <dxe:ListEditItem Value="23100" Text="BMF - Venda OpçãoVenda (Corretagem)" />
                                    <dxe:ListEditItem Value="23101" Text="BMF - Venda OpçãoVenda (Emolumento)" />
                                    <dxe:ListEditItem Value="23102" Text="BMF - Venda OpçãoVenda (Registro)" />
                                    <dxe:ListEditItem Value="23500" Text="BMF - Lucro Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="23501" Text="BMF - Prejuízo Compra OpçãoCompra" />
                                    <dxe:ListEditItem Value="23550" Text="BMF - Lucro Venda OpçãoCompra" />
                                    <dxe:ListEditItem Value="23551" Text="BMF - Prejuízo Venda OpçãoCompra" />                            
                                    <dxe:ListEditItem Value="24000" Text="BMF - Lucro Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="24001" Text="BMF - Prejuízo Compra OpçãoVenda" />
                                    <dxe:ListEditItem Value="24050" Text="BMF - Lucro Venda OpçãoVenda" />
                                    <dxe:ListEditItem Value="24051" Text="BMF - Prejuízo Venda OpçãoVenda" />                                    
                                    <dxe:ListEditItem Value="25000" Text="BMF - Lucro Futuro DayTrade" />
                                    <dxe:ListEditItem Value="25001" Text="BMF - Prejuízo Futuro DayTrade" />
                                    <dxe:ListEditItem Value="25100" Text="BMF - Lucro Opção DayTrade" />
                                    <dxe:ListEditItem Value="25101" Text="BMF - Prejuízo Opção DayTrade" />
                                    <dxe:ListEditItem Value="25200" Text="BMF - Lucro OpçãoVenda DayTrade" />
                                    <dxe:ListEditItem Value="25201" Text="BMF - Prejuízo OpçãoVenda DayTrade" />
                                    <dxe:ListEditItem Value="26000" Text="BMF - Liquidação a Pagar" />
                                    <dxe:ListEditItem Value="26001" Text="BMF - Liquidação a Receber" />
                                    <dxe:ListEditItem Value="27000" Text="BMF - Provisão Taxa de Permanência" />
                                    <dxe:ListEditItem Value="27001" Text="BMF - Pagamento Taxa de Permanência" />
                                    <dxe:ListEditItem Value="30000" Text="Renda Fixa - Compra Final" />
                                    <dxe:ListEditItem Value="30500" Text="Renda Fixa - Venda Final" />
                                    <dxe:ListEditItem Value="30600" Text="Renda Fixa - Lucro Venda Final" />
                                    <dxe:ListEditItem Value="30601" Text="Renda Fixa - Prejuízo Venda Final" />
                                    <dxe:ListEditItem Value="31000" Text="Renda Fixa - Compra Compromissada" />
                                    <dxe:ListEditItem Value="31100" Text="Renda Fixa - Revenda" />
                                    <dxe:ListEditItem Value="31200" Text="Renda Fixa - Lucro Revenda" />
                                    <dxe:ListEditItem Value="32000" Text="Renda Fixa - Ajuste MTM Positivo" />
                                    <dxe:ListEditItem Value="32001" Text="Renda Fixa - Ajuste MTM Negativo" />
                                    <dxe:ListEditItem Value="32002" Text="Renda Fixa - Reversão Ajuste MTM Positivo" />
                                    <dxe:ListEditItem Value="32003" Text="Renda Fixa - Reversão Ajuste MTM Negativo" />
                                    <dxe:ListEditItem Value="32500" Text="Renda Fixa - Renda Positiva" />
                                    <dxe:ListEditItem Value="32501" Text="Renda Fixa - Renda Negativa" />
                                    <dxe:ListEditItem Value="33000" Text="Renda Fixa - Vencimento" />
                                    <dxe:ListEditItem Value="34000" Text="Renda Fixa - Pagamento Juros" />
                                    <dxe:ListEditItem Value="34100" Text="Renda Fixa - Pagamento Amortização" />
                                    <dxe:ListEditItem Value="40000" Text="Swap - Ajuste Positivo" />
                                    <dxe:ListEditItem Value="40001" Text="Swap - Reversão Ajuste Positivo" />
                                    <dxe:ListEditItem Value="40100" Text="Swap - Ajuste Negativo" />
                                    <dxe:ListEditItem Value="40101" Text="Swap - Reversão Ajuste Negativo" />
                                    <dxe:ListEditItem Value="41000" Text="Swap - Liquidação Pagar" />
                                    <dxe:ListEditItem Value="41001" Text="Swap - Liquidação Receber" />
                                    <dxe:ListEditItem Value="50000" Text="Fundos - Aplicação" />
                                    <dxe:ListEditItem Value="51000" Text="Fundos - Resgate" />
                                    <dxe:ListEditItem Value="51100" Text="Fundos - Liquidação Resgate" />
                                    <dxe:ListEditItem Value="52000" Text="Fundos - Valorização" />
                                    <dxe:ListEditItem Value="52001" Text="Fundos - Desvalorização" />
                                    <dxe:ListEditItem Value="60000" Text="Cotista - Aplicação Pessoa Física" />
                                    <dxe:ListEditItem Value="60500" Text="Cotista - Resgate (Custo) Pessoa Física" />
                                    <dxe:ListEditItem Value="60600" Text="Cotista - Resgate (Variação Positiva) Pessoa Física" />
                                    <dxe:ListEditItem Value="60601" Text="Cotista - Resgate (Variação Negativa) Pessoa Física" />
                                    <dxe:ListEditItem Value="60700" Text="Cotista - Resgate (Provisão IR) Pessoa Física" />
                                    <dxe:ListEditItem Value="60800" Text="Cotista - Resgate (Provisão IOF) Pessoa Física" />
                                    <dxe:ListEditItem Value="62000" Text="Cotista - Aplicação Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="62500" Text="Cotista - Resgate (Custo) Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="62600" Text="Cotista - Resgate (Variação Positiva) Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="62601" Text="Cotista - Resgate (Variação Negativa) Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="62700" Text="Cotista - Resgate (Provisão IR) Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="62800" Text="Cotista - Resgate (Provisão IOF) Pessoa Jurídica" />
                                    <dxe:ListEditItem Value="64000" Text="Cotista - Liquidação Resgate" />
                                    <dxe:ListEditItem Value="64100" Text="Cotista - Liquidação Resgate IR" />
                                    <dxe:ListEditItem Value="64200" Text="Cotista - Liquidação Resgate IOF" />
                                    <dxe:ListEditItem Value="80000" Text="Despesas - Provisão" />
                                    <dxe:ListEditItem Value="80100" Text="Despesas - Pagamento" />
                                    <dxe:ListEditItem Value="999999" Text="Outros" />
                                </Items>                                                                                     
                                </dxe:ASPxComboBox>             
                            </td>
                        </tr>
                                                                                                                                                                                         
                        <tr>                                        
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelPlano" runat="server" CssClass="labelNormal" Text="Plano:" />
                            </td>
                                                
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropPlano" runat="server" DataSourceID="EsDSPlano" ClientEnabled="false"
                                                 ValueField="IdPlano" TextField="Descricao" ClientInstanceName="dropPlano"
                                                 CssClass="dropDownList" Text='<%#Eval("IdPlano")%>' />
                            </td>
                        </tr>        
                                                                                             
                       </table>
                
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal7" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>                        
                </div>       
                                
            </EditForm>
            
            <StatusBar>
                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
            </StatusBar>      
            
            </Templates>
            
            <SettingsPopup EditForm-Width="370px" />
            <SettingsBehavior EnableCustomizationWindow ="true" />
            <SettingsText CustomizationWindowCaption="Lista de campos" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
            
            </dxwgv:ASPxGridView>
            </div>    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape= "true" LeftMargin="20" RightMargin="20" />
        
    <cc1:esDataSource ID="EsDSContabLancamento" runat="server" OnesSelect="EsDSContabLancamento_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSPlano" runat="server" OnesSelect="EsDSPlano_esSelect" />
    <cc1:esDataSource ID="EsDSCentroCusto" runat="server" OnesSelect="EsDSCentroCusto_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    
    <cc1:esDataSource ID="EsDSContabRoteiro" runat="server" OnesSelect="EsDSContabRoteiro_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSPlanoPopup" runat="server" OnesSelect="EsDSPlanoPopup_esSelect" />
    
    </form>
</body>
</html>