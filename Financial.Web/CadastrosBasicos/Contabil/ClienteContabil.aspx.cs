﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Contabil;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_ClienteContabil : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.Query.Load();
        //
        e.Collection = coll;
    }

    protected void EsDSContabilPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabPlanoCollection coll = new ContabPlanoCollection();
        coll.Query.Load();
        //
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteCollection coll = new ClienteCollection();

        ClienteQuery clienteQuery = new ClienteQuery("C");
        TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("L");
        ContabPlanoQuery contabPlanoQuery = new ContabPlanoQuery("P");
        ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("I");
        
        clienteQuery.Select(clienteQuery, contabPlanoQuery, clienteInterfaceQuery);
        clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
        clienteQuery.LeftJoin(contabPlanoQuery).On(contabPlanoQuery.IdPlano == clienteQuery.IdPlano);
        clienteQuery.LeftJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Faz Update dos Clientes Selecionados em Lote
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        bool ok = true;

        List<object> clientesSelecionados = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (clientesSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Clientes.";
        }
        else
        {
            if (this.dropCalculaContabilLote.SelectedIndex == -1 &&
                this.dropContabilPlanoLote.SelectedIndex == -1 &&
                this.dropContabilLiquidacaoLote.SelectedIndex == -1 &&
                this.dropContabilAcoesLote.SelectedIndex == -1 &&
                this.dropContabilFundosLote.SelectedIndex == -1 &&
                this.dropContabilBMFLote.SelectedIndex == -1 &&
                this.dropContabilDaytradeLote.SelectedIndex == -1 &&
                this.textCategoriaContabilLote.Text == "")
            {
                e.Result = "Prencha algum campo.";
                return;
            }
            
            for (int i = 0; i < clientesSelecionados.Count; i++)
            {
                int idCliente = Convert.ToInt32(clientesSelecionados[i]);

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);

                ClienteInterface clienteInterface = new ClienteInterface();
                if (dropCalculaContabilLote.Text != "" && Convert.ToString(dropCalculaContabilLote.SelectedItem.Value) == "S")
                {
                    cliente.CalculaContabil = "S";
                    if (dropContabilPlanoLote.SelectedIndex > -1)
                    {
                        cliente.IdPlano = Convert.ToInt32(dropContabilPlanoLote.SelectedItem.Value);
                    }

                    if (dropContabilLiquidacaoLote.SelectedIndex > -1)
                    {
                        cliente.ContabilLiquidacao = Convert.ToByte(dropContabilLiquidacaoLote.SelectedItem.Value);
                    }

                    if (dropContabilAcoesLote.SelectedIndex > -1)
                    {
                        cliente.ContabilAcoes = Convert.ToByte(dropContabilAcoesLote.SelectedItem.Value);
                    }

                    if (dropContabilFundosLote.SelectedIndex > -1)
                    {
                        cliente.ContabilFundos = Convert.ToByte(dropContabilFundosLote.SelectedItem.Value);
                    }

                    if (dropContabilBMFLote.SelectedIndex > -1)
                    {
                        cliente.ContabilBMF = Convert.ToByte(dropContabilBMFLote.SelectedItem.Value);
                    }

                    if (dropContabilDaytradeLote.SelectedIndex > -1)
                    {
                        cliente.ContabilDaytrade = Convert.ToByte(dropContabilDaytradeLote.SelectedItem.Value);
                    }


                    if (!clienteInterface.LoadByPrimaryKey(idCliente))
                    {
                        clienteInterface = new ClienteInterface();
                    }

                    if (textCategoriaContabilLote.Text != "")
                    {
                        clienteInterface.CategoriaContabil = Convert.ToInt32(textCategoriaContabilLote.Text);
                    }

                    clienteInterface.Save();
                }
                else
                {
                    cliente.CalculaContabil = "N";
                    cliente.IdPlano = null;
                    cliente.ContabilLiquidacao = null;
                    cliente.ContabilAcoes = null;
                    cliente.ContabilFundos = null;
                    cliente.ContabilBMF = null;
                    cliente.ContabilDaytrade = null;
                }

                if (clienteInterface.LoadByPrimaryKey(idCliente))
                {
                    if (textCategoriaContabilLote.Text != "")
                    {
                        clienteInterface.CategoriaContabil = Convert.ToInt32(textCategoriaContabilLote.Text);
                    }

                    clienteInterface.Save();
                }

                cliente.Save();
                
            }
            e.Result = ok ? "Processo executado com sucesso." : "Processo executado parcialmente.";            
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";
        ASPxComboBox dropCalculaContabil = gridCadastro.FindEditFormTemplateControl("dropCalculaContabil") as ASPxComboBox;

        if (dropCalculaContabil.Text != "" && Convert.ToString(dropCalculaContabil.SelectedItem.Value) == "S")
        {
            ASPxComboBox dropContabilPlano = gridCadastro.FindEditFormTemplateControl("dropContabilPlano") as ASPxComboBox;
            ASPxComboBox dropContabilLiquidacao = gridCadastro.FindEditFormTemplateControl("dropContabilLiquidacao") as ASPxComboBox;
            ASPxComboBox dropContabilAcoes = gridCadastro.FindEditFormTemplateControl("dropContabilAcoes") as ASPxComboBox;
            ASPxComboBox dropContabilFundos = gridCadastro.FindEditFormTemplateControl("dropContabilFundos") as ASPxComboBox;
            ASPxComboBox dropContabilBMF = gridCadastro.FindEditFormTemplateControl("dropCalculaContabil") as ASPxComboBox;
            ASPxComboBox dropContabilDayTrade = gridCadastro.FindEditFormTemplateControl("dropContabilDayTrade") as ASPxComboBox;
            
            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(dropContabilPlano);
            controles.Add(dropContabilLiquidacao);
            controles.Add(dropContabilAcoes);
            controles.Add(dropContabilFundos);
            controles.Add(dropContabilBMF);
            controles.Add(dropContabilDayTrade);            

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos de métodos de contabilização são obrigatórios para o caso de ter marcado Calcula Contábil = Sim.";
                return;
            }
            #endregion
        }
    }
  
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int id = (int)e.Keys[0];

        Cliente cliente = new Cliente();
        ClienteInterface clienteInterface = new ClienteInterface();

        ASPxComboBox dropCalculaContabil = gridCadastro.FindEditFormTemplateControl("dropCalculaContabil") as ASPxComboBox;
        ASPxComboBox dropContabilPlano = gridCadastro.FindEditFormTemplateControl("dropContabilPlano") as ASPxComboBox;
        ASPxComboBox dropContabilLiquidacao = gridCadastro.FindEditFormTemplateControl("dropContabilLiquidacao") as ASPxComboBox;
        ASPxComboBox dropContabilAcoes = gridCadastro.FindEditFormTemplateControl("dropContabilAcoes") as ASPxComboBox;
        ASPxComboBox dropContabilFundos = gridCadastro.FindEditFormTemplateControl("dropContabilFundos") as ASPxComboBox;
        ASPxComboBox dropContabilBMF = gridCadastro.FindEditFormTemplateControl("dropContabilBMF") as ASPxComboBox;
        ASPxComboBox dropContabilDayTrade = gridCadastro.FindEditFormTemplateControl("dropContabilDayTrade") as ASPxComboBox;
        ASPxTextBox textCodigoContabil = gridCadastro.FindEditFormTemplateControl("textCodigoContabil") as ASPxTextBox;
        ASPxSpinEdit textCategoriaContabil = gridCadastro.FindEditFormTemplateControl("textCategoriaContabil") as ASPxSpinEdit;
        ASPxDateEdit textDataTrava = gridCadastro.FindEditFormTemplateControl("textDataTrava") as ASPxDateEdit;

        if (cliente.LoadByPrimaryKey(id))
        {
            if (dropCalculaContabil.Text != "" && Convert.ToString(dropCalculaContabil.SelectedItem.Value) == "S")
            {
                cliente.CalculaContabil = "S";
                cliente.IdPlano = Convert.ToInt32(dropContabilPlano.SelectedItem.Value);
                cliente.ContabilLiquidacao = Convert.ToByte(dropContabilLiquidacao.SelectedItem.Value);
                cliente.ContabilAcoes = Convert.ToByte(dropContabilAcoes.SelectedItem.Value);
                cliente.ContabilFundos = Convert.ToByte(dropContabilFundos.SelectedItem.Value);
                cliente.ContabilBMF = Convert.ToByte(dropContabilBMF.SelectedItem.Value);
                cliente.ContabilDaytrade = Convert.ToByte(dropContabilDayTrade.SelectedItem.Value);

                if (textDataTrava.Text != "")
                {
                    cliente.ContabilDataTrava = Convert.ToDateTime(textDataTrava.Text);
                }
                else
                {
                    cliente.ContabilDataTrava = null;
                }
            }
            else
            {
                cliente.CalculaContabil = "N";
                cliente.IdPlano = null;
                cliente.ContabilLiquidacao = null;
                cliente.ContabilAcoes = null;
                cliente.ContabilFundos = null;
                cliente.ContabilBMF = null;
                cliente.ContabilDaytrade = null;
                cliente.ContabilDataTrava = null;
            }

            cliente.Save();

            if (textCodigoContabil.Text != "" || textCategoriaContabil.Text != "")
            {
                if (!clienteInterface.LoadByPrimaryKey(id))
                {
                    clienteInterface = new ClienteInterface();
                    clienteInterface.IdCliente = id;
                }

                if (cliente.CalculaContabil == "N")
                {
                    clienteInterface.CodigoContabil = null;
                }
                else
                {
                    clienteInterface.CodigoContabil = textCodigoContabil.Text;
                }

                if (textCategoriaContabil.Text != "")
                {
                    clienteInterface.CategoriaContabil = Convert.ToInt32(textCategoriaContabil.Text);
                }

                clienteInterface.Save();                
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Cliente (Contabil) - Operacao: Update Cliente: " + id,
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
    
}