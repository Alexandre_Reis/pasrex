﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Contabil;

public partial class CadastrosBasicos_ImplantaSaldoContabil : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasPopupCliente = true;
        this.HasPopupContabConta = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSContabSaldo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ContabSaldoQuery contabSaldoQuery = new ContabSaldoQuery("S");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Q");
        ContabContaQuery contabContaQuery = new ContabContaQuery("D");

        contabSaldoQuery.Select(contabSaldoQuery, contabContaQuery, clienteQuery.Apelido.As("Apelido"));
        contabSaldoQuery.InnerJoin(clienteQuery).On(contabSaldoQuery.IdCliente == clienteQuery.IdCliente);
        contabSaldoQuery.InnerJoin(contabContaQuery).On(contabSaldoQuery.IdConta == contabContaQuery.IdConta);
        contabSaldoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        contabSaldoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            contabSaldoQuery.Where(contabSaldoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            contabSaldoQuery.Where(contabSaldoQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            contabSaldoQuery.Where(contabSaldoQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        contabSaldoQuery.OrderBy(contabSaldoQuery.IdCliente.Ascending, contabContaQuery.Codigo.Ascending);

        ContabSaldoCollection coll = new ContabSaldoCollection();
        coll.Load(contabSaldoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContabConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabContaQuery contabContaQuery = new ContabContaQuery("C");
        ContabPlanoQuery contabPlanoQuery = new ContabPlanoQuery("P");
        //
        contabContaQuery.Select(contabContaQuery.IdConta,
                                contabContaQuery.Descricao,
                                contabContaQuery.Codigo,
                                contabContaQuery.CodigoReduzida,
                                contabPlanoQuery.Descricao.As("DescricaoPlano"));
        contabContaQuery.InnerJoin(contabPlanoQuery).On(contabPlanoQuery.IdPlano == contabContaQuery.IdPlano);
        contabContaQuery.OrderBy(contabContaQuery.IdPlano.Ascending,
                                 contabContaQuery.Codigo.Ascending);
        //
        ContabContaCollection coll = new ContabContaCollection();
        coll.Load(contabContaQuery);
        //     
        e.Collection = coll;
    }

    protected void EsDSPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(hiddenIdConta);
        controles.Add(textValor);
        controles.Add(textData);
        
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) 
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            Cliente cliente = new Cliente();
            cliente.Query.Select(cliente.Query.IdPlano);
            cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
            cliente.Query.Load();
            
            int idConta = Convert.ToInt32(hiddenIdConta.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            ContabConta contabConta = new ContabConta();
            contabConta.LoadByPrimaryKey(idConta);

            if (!cliente.IdPlano.HasValue)
            {
                e.Result = "Cliente não tem plano de contas associado.";
                return;
            }

            if (cliente.IdPlano.Value != contabConta.IdPlano.Value)
            {
                e.Result = "Plano da conta escolhida é diferente do plano contábil do cliente.";
            }

            ContabSaldo contabSaldo = new ContabSaldo();
            if (contabSaldo.LoadByPrimaryKey(idCliente, idConta, data))
            {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallbackConta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idConta = Convert.ToInt32(e.Parameter);
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.Codigo,
                                               contabContaCollection.Query.CodigoReduzida);
            contabContaCollection.Query.Where(contabContaCollection.Query.IdConta == idConta);

            contabContaCollection.Query.Load();

            if (contabContaCollection.HasData)
            {
                string codigo = contabContaCollection[0].Codigo;
                string codigoReduzida = contabContaCollection[0].CodigoReduzida;
                texto = codigo.ToString() + "|" + codigoReduzida;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackAtualizacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (textListaClientes.Text == "")
        {
            e.Result = "Ao menos um Id deve ser preenchido na lista de Ids.";
            return;
        }

        if (textDataAtualizacao.Text == "")
        {
            e.Result = "A data deve ser preenchida para atualização do saldo contábil de fechamento.";
            return;
        }

        string listaIds = textListaClientes.Text.Replace(",", ";");

        string[] ids = listaIds.Split(new Char[] { ';' });
        foreach (string id in ids)
        {
            if (!Utilitario.IsInteger(id))
            {
                e.Result = "Id informado de forma incorreta. " + id + ". Processo executado com problemas.";
                return;
            }

            ContabSaldo contabSaldo = new ContabSaldo();
            contabSaldo.ImplantaSaldo(Convert.ToInt32(id), Convert.ToDateTime(textDataAtualizacao.Text));
        }

        e.Result = "Processo executado ok.";
        return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditConta_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxButtonEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(ContabSaldoMetadata.ColumnNames.IdCliente));
            string data = Convert.ToString(e.GetListSourceFieldValue(ContabSaldoMetadata.ColumnNames.Data));
            string idConta = Convert.ToString(e.GetListSourceFieldValue(ContabSaldoMetadata.ColumnNames.IdConta));
            e.Value = idCliente + data + idConta;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        ContabSaldo contabSaldo = new ContabSaldo();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        int idConta = Convert.ToInt32(hiddenIdConta.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);

        if (contabSaldo.LoadByPrimaryKey(idCliente, idConta, data))
        {
            contabSaldo.SaldoFinal = valor;
            contabSaldo.SaldoAnterior = 0;
            contabSaldo.TotalCredito = 0;
            contabSaldo.TotalDebito = 0;

            contabSaldo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabSaldo - Operacao: Update ContabSaldo: " + idCliente + "; " + data + "; " + idConta + UtilitarioWeb.ToString(contabSaldo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() 
    {
        ContabSaldo contabSaldo = new ContabSaldo();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime data = Convert.ToDateTime(textData.Text);        
        int idConta = Convert.ToInt32(hiddenIdConta.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);

        ContabConta contabConta = new ContabConta();
        contabConta.LoadByPrimaryKey(idConta);

        contabSaldo.IdCliente = idCliente;
        contabSaldo.Data = data;
        contabSaldo.IdConta = idConta;
        contabSaldo.SaldoFinal = valor;
        contabSaldo.IdPlano = contabConta.IdPlano.Value;
        contabSaldo.SaldoAnterior = 0;
        contabSaldo.TotalCredito = 0;
        contabSaldo.TotalDebito = 0;        

        contabSaldo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabSaldo - Operacao: Insert ContabSaldo: " + contabSaldo.IdCliente.Value + "; " + contabSaldo.IdConta.Value + "; " + contabSaldo.Data.Value + "; " + contabSaldo.SaldoFinal.Value + UtilitarioWeb.ToString(contabSaldo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(ContabSaldoMetadata.ColumnNames.IdCliente);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(ContabSaldoMetadata.ColumnNames.Data);
            List<object> keyValuesIdConta = gridCadastro.GetSelectedFieldValues(ContabSaldoMetadata.ColumnNames.IdConta);
            
            for (int i = 0; i < keyValuesIdCliente.Count; i++) 
            {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idConta = Convert.ToInt32(keyValuesIdConta[i]);

                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, idConta, data))
                {
                    //
                    ContabSaldo contabSaldoClone = (ContabSaldo)Utilitario.Clone(contabSaldo);
                    //

                    contabSaldo.MarkAsDeleted();
                    contabSaldo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ContabSaldo - Operacao: Delete ContabSaldo: " + contabSaldoClone.IdCliente + "; " + contabSaldoClone.IdConta + "; " + contabSaldoClone.Data + "; " + UtilitarioWeb.ToString(contabSaldoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
            e.Properties["cpHiddenIdConta"] = hiddenIdConta.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }        

        labelFiltro.Text = texto.ToString();
    }
}