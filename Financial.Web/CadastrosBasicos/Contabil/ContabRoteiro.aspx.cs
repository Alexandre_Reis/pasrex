﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

using Financial.Contabil;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;

using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_ContabRoteiro : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasPopupContabConta = true;        
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { ContabRoteiroMetadata.ColumnNames.Origem }));
    }

    #region DataSources
    protected void EsDSContabRoteiro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        ContabRoteiroQuery contabRoteiroQuery = new ContabRoteiroQuery("R");
        ContabContaQuery contabContaDebitoQuery = new ContabContaQuery("D");
        ContabContaQuery contabContaCreditoQuery = new ContabContaQuery("C");

        contabRoteiroQuery.Select(contabRoteiroQuery,
                                  contabContaDebitoQuery,
                                  contabContaCreditoQuery,
                                  contabContaDebitoQuery.IdConta.As("IdCodigoDebito"),
                                  contabContaCreditoQuery.IdConta.As("IdCodigoCredito"),
                                  contabContaDebitoQuery.Codigo.As("CodigoDebito"),
                                  contabContaCreditoQuery.Codigo.As("CodigoCredito"),
                                  contabContaDebitoQuery.CodigoReduzida.As("CodigoReduzidaDebito"),
                                  contabContaCreditoQuery.CodigoReduzida.As("CodigoReduzidaCredito"));  
        contabRoteiroQuery.InnerJoin(contabContaDebitoQuery).On(contabContaDebitoQuery.IdConta == contabRoteiroQuery.IdContaDebito);
        contabRoteiroQuery.InnerJoin(contabContaCreditoQuery).On(contabContaCreditoQuery.IdConta == contabRoteiroQuery.IdContaCredito);

        coll.Load(contabRoteiroQuery);
        //
        e.Collection = coll;
    }

    protected void EsDSContabConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabContaQuery contabContaQuery = new ContabContaQuery("C");
        ContabPlanoQuery contabPlanoQuery = new ContabPlanoQuery("P");
        //
        contabContaQuery.Select(contabContaQuery.IdConta,
                                contabContaQuery.Descricao,
                                contabContaQuery.Codigo,
                                contabContaQuery.CodigoReduzida,
                                contabPlanoQuery.Descricao.As("DescricaoPlano"));
        contabContaQuery.InnerJoin(contabPlanoQuery).On(contabPlanoQuery.IdPlano == contabContaQuery.IdPlano);
        contabContaQuery.OrderBy(contabContaQuery.IdPlano.Ascending,
                                 contabContaQuery.Codigo.Ascending);
        //
        ContabContaCollection coll = new ContabContaCollection();
        coll.Load(contabContaQuery);
        //     
        e.Collection = coll;
    }

    protected void EsDSPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }

    protected void EsDSCentroCusto_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabCentroCustoCollection coll = new ContabCentroCustoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallbackConta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idConta = Convert.ToInt32(e.Parameter);
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.Codigo,
                                               contabContaCollection.Query.CodigoReduzida);
            contabContaCollection.Query.Where(contabContaCollection.Query.IdConta == idConta);

            contabContaCollection.Query.Load();

            if (contabContaCollection.HasData)
            {
                string codigo = contabContaCollection[0].Codigo;
                string codigoReduzida = contabContaCollection[0].CodigoReduzida;
                texto = codigo.ToString() + "|" + codigoReduzida;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxComboBox textDescricao = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;
        TextBox hiddenIdContaDebito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaDebito") as TextBox;
        TextBox hiddenIdContaCredito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaCredito") as TextBox;
        ASPxComboBox dropOrigem = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;        
                
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { textDescricao, hiddenIdContaDebito, hiddenIdContaCredito, dropOrigem });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        ContabConta contabContaCredito = new ContabConta();
        contabContaCredito.LoadByPrimaryKey(Convert.ToInt32(hiddenIdContaCredito.Text));

        ContabConta contabContaDebito = new ContabConta();
        contabContaDebito.LoadByPrimaryKey(Convert.ToInt32(hiddenIdContaDebito.Text));

        if (contabContaCredito.IdPlano.Value != contabContaDebito.IdPlano.Value)
        {
            e.Result = "O plano contábil deve ser igual para as contas débito e crédito.";
            return;
        }

        ContabContaCollection contabContaCollection = new ContabContaCollection();
        contabContaCollection.Query.Select(contabContaCollection.Query.IdConta);
        contabContaCollection.Query.Where(contabContaCollection.Query.IdContaMae.In(Convert.ToInt32(hiddenIdContaCredito.Text), Convert.ToInt32(hiddenIdContaDebito.Text)));
        contabContaCollection.Query.Load();

        if (contabContaCollection.Count > 0)
        {
            e.Result = "Não pode ser escolhida uma conta mãe para associação com roteiro contábil.";
            return;
        }

    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ContabRoteiro contabRoteiro = new ContabRoteiro();

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        TextBox hiddenIdContaDebito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaDebito") as TextBox;
        TextBox hiddenIdContaCredito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaCredito") as TextBox;
        ASPxTextBox textContaCreditoReduzida = gridCadastro.FindEditFormTemplateControl("textContaCreditoReduzida") as ASPxTextBox;
        ASPxTextBox textContaDebitoReduzida = gridCadastro.FindEditFormTemplateControl("textContaDebitoReduzida") as ASPxTextBox;
        ASPxTextBox textIdentificador = gridCadastro.FindEditFormTemplateControl("textIdentificador") as ASPxTextBox;
        ASPxComboBox dropOrigem = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;
                       
        int idEvento = (int)e.Keys[0];

        if (contabRoteiro.LoadByPrimaryKey(idEvento)) 
        {
            contabRoteiro.IdContaCredito = Convert.ToInt32(hiddenIdContaCredito.Text);
            contabRoteiro.IdContaDebito = Convert.ToInt32(hiddenIdContaDebito.Text);            
            contabRoteiro.Descricao = Convert.ToString(textDescricao.Text).Trim();            
            contabRoteiro.Identificador = Convert.ToString(textIdentificador.Text).Trim();

            ContabConta contabContaCredito = new ContabConta();
            contabContaCredito.LoadByPrimaryKey(contabRoteiro.IdContaCredito.Value);

            ContabConta contabContaDebito = new ContabConta();
            contabContaDebito.LoadByPrimaryKey(contabRoteiro.IdContaDebito.Value);

            contabRoteiro.ContaCredito = contabContaCredito.Codigo;
            contabRoteiro.ContaDebito = contabContaDebito.Codigo;

            if (!String.IsNullOrEmpty(textContaCreditoReduzida.Text))
            {
                contabRoteiro.ContaCreditoReduzida = Convert.ToString(textContaCreditoReduzida.Text).Trim();
            }
            if (!String.IsNullOrEmpty(textContaDebitoReduzida.Text))
            {
                contabRoteiro.ContaDebitoReduzida = Convert.ToString(textContaDebitoReduzida.Text).Trim();
            }
            
            contabRoteiro.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value);
            contabRoteiro.IdPlano = contabContaCredito.IdPlano.Value; //pode pegar o IdPlano de qq uma das 2 contas, pq serão sempre iguais

            //
            contabRoteiro.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabRoteiro - Update: " + idEvento + UtilitarioWeb.ToString(contabRoteiro),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ContabRoteiro contabRoteiro = new ContabRoteiro();

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        TextBox hiddenIdContaDebito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaDebito") as TextBox;
        TextBox hiddenIdContaCredito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaCredito") as TextBox;
        ASPxTextBox textContaCreditoReduzida = gridCadastro.FindEditFormTemplateControl("textContaCreditoReduzida") as ASPxTextBox;
        ASPxTextBox textContaDebitoReduzida = gridCadastro.FindEditFormTemplateControl("textContaDebitoReduzida") as ASPxTextBox;
        ASPxTextBox textIdentificador = gridCadastro.FindEditFormTemplateControl("textIdentificador") as ASPxTextBox;        //
        ASPxComboBox dropOrigem = gridCadastro.FindEditFormTemplateControl("dropOrigem") as ASPxComboBox;
        //                          
        contabRoteiro.IdContaCredito = Convert.ToInt32(hiddenIdContaCredito.Text);
        contabRoteiro.IdContaDebito = Convert.ToInt32(hiddenIdContaDebito.Text);

        ContabConta contabContaCredito = new ContabConta();
        contabContaCredito.LoadByPrimaryKey(contabRoteiro.IdContaCredito.Value);

        ContabConta contabContaDebito = new ContabConta();
        contabContaDebito.LoadByPrimaryKey(contabRoteiro.IdContaDebito.Value);

        contabRoteiro.ContaCredito = contabContaCredito.Codigo;
        contabRoteiro.ContaDebito = contabContaDebito.Codigo;

        contabRoteiro.Descricao = Convert.ToString(textDescricao.Text).Trim();

        if (!String.IsNullOrEmpty(textIdentificador.Text)) {
            contabRoteiro.Identificador = Convert.ToString(textIdentificador.Text).Trim();
        }

        if (!String.IsNullOrEmpty(textContaCreditoReduzida.Text)) {
            contabRoteiro.ContaCreditoReduzida = Convert.ToString(textContaCreditoReduzida.Text).Trim();
        }
        if (!String.IsNullOrEmpty(textContaDebitoReduzida.Text)) {
            contabRoteiro.ContaDebitoReduzida = Convert.ToString(textContaDebitoReduzida.Text).Trim();
        }

        contabRoteiro.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value);
        contabRoteiro.IdPlano = contabContaCredito.IdPlano.Value; //Pode pegar o IdPlano de qq uma das 2 contas pq ele será sempre o mesmo para as 2

        //
        contabRoteiro.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabRoteiro - Insert: " + contabRoteiro.IdEvento + UtilitarioWeb.ToString(contabRoteiro),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            TextBox hiddenIdContaDebito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaDebito") as TextBox;
            e.Properties["cpHiddenIdContaDebito"] = hiddenIdContaDebito.ClientID;

            TextBox hiddenIdContaCredito = gridCadastro.FindEditFormTemplateControl("hiddenIdContaCredito") as TextBox;
            e.Properties["cpHiddenIdContaCredito"] = hiddenIdContaCredito.ClientID;
        }
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ContabRoteiroMetadata.ColumnNames.IdEvento);
            //
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idEvento = Convert.ToInt32(keyValuesId[i]);

                ContabRoteiro c = new ContabRoteiro();
                if (c.LoadByPrimaryKey(idEvento)) {
                    contabRoteiroCollection.AttachEntity(c);
                }
            }
            //
            //
            ContabRoteiroCollection contabRoteiroCollectionClone = (ContabRoteiroCollection)Utilitario.Clone(contabRoteiroCollection);
            //

            contabRoteiroCollection.MarkAllAsDeleted();
            contabRoteiroCollection.Save();

            foreach (ContabRoteiro cr in contabRoteiroCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ContabRoteiro - Delete: " + cr.IdEvento + UtilitarioWeb.ToString(cr),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}