using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Contabil;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;

public partial class CadastrosBasicos_ContabPlano : CadastroBasePage
{
    #region DataSources
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSContabPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabPlanoCollection coll = new ContabPlanoCollection();
        //
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ContabPlano contabPlano = new ContabPlano();
        int idPlano = Convert.ToInt32(e.Keys[0]);

        if (contabPlano.LoadByPrimaryKey(idPlano))
        {
            contabPlano.Descricao = Convert.ToString(e.NewValues[ContabPlanoMetadata.ColumnNames.Descricao]);
            contabPlano.ValidaDigitoConta = Convert.ToString(e.NewValues[ContabPlanoMetadata.ColumnNames.ValidaDigitoConta]);
            contabPlano.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabPlano - Operacao: Update ContabPlano: " + idPlano + UtilitarioWeb.ToString(contabPlano),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Insert
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ContabPlano contabPlano = new ContabPlano();

        contabPlano.Descricao = Convert.ToString(e.NewValues[ContabPlanoMetadata.ColumnNames.Descricao]);
        contabPlano.ValidaDigitoConta = Convert.ToString(e.NewValues[ContabPlanoMetadata.ColumnNames.ValidaDigitoConta]);
        contabPlano.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabPlano - Operacao: Insert ContabPlano: " + contabPlano.IdPlano + UtilitarioWeb.ToString(contabPlano),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ContabPlanoMetadata.ColumnNames.IdPlano);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPlano = Convert.ToInt32(keyValuesId[i]);

                ContabPlano contabPlano = new ContabPlano();
                if (contabPlano.LoadByPrimaryKey(idPlano))
                {

                    //
                    ContabPlano contabPlanoClone = (ContabPlano)Utilitario.Clone(contabPlano);
                    //

                    contabPlano.MarkAsDeleted();
                    contabPlano.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ContabPlano - Operacao: Delete ContabPlano: " + idPlano + UtilitarioWeb.ToString(contabPlanoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (e.Column.FieldName == ContabPlanoMetadata.ColumnNames.IdPlano)
        {
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
            //e.Editor.Enabled = false;

            //(e.Editor as ASPxSpinEdit).ValidationSettings.RequiredField.IsRequired = false;
        }
    }
}