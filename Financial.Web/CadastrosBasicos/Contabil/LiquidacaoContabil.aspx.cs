﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Contabil;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_LiquidacaoContabil : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasEditControl = false;
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        //        
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoMetadata.ColumnNames.Fonte }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        ContabRoteiroQuery contabRoteiroQuery = new ContabRoteiroQuery("R");

        liquidacaoQuery.Select(liquidacaoQuery, 
                               clienteQuery.Apelido.As("Apelido"),
                               contabRoteiroQuery.Descricao.As("DescricaoEvento"));
        liquidacaoQuery.InnerJoin(clienteQuery).On(liquidacaoQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.LeftJoin(contabRoteiroQuery).On(contabRoteiroQuery.IdEvento == liquidacaoQuery.IdEvento);
        liquidacaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                              clienteQuery.CalculaContabil.Equal("S"));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicioLancamento.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.GreaterThanOrEqual(textDataInicioLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataFimLancamento.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.LessThanOrEqual(textDataFimLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textDescricaoFiltro.Text))
        {
            liquidacaoQuery.Where(liquidacaoQuery.Descricao.Like("%" + textDescricaoFiltro.Text + "%"));
        }

        liquidacaoQuery.Where(liquidacaoQuery.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao),
                              liquidacaoQuery.Fonte.NotEqual(FonteLancamentoLiquidacao.Interno));
        liquidacaoQuery.OrderBy(liquidacaoQuery.DataVencimento.Descending, liquidacaoQuery.DataLancamento.Descending);

        LiquidacaoCollection coll = new LiquidacaoCollection();
        coll.Load(liquidacaoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContabRoteiro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        ContabRoteiroQuery contabRoteiroQuery = new ContabRoteiroQuery("R");
        ContabContaQuery contabContaDebitoQuery = new ContabContaQuery("D");
        ContabContaQuery contabContaCreditoQuery = new ContabContaQuery("C");

        contabRoteiroQuery.Select(contabRoteiroQuery,
                                  contabContaDebitoQuery,
                                  contabContaCreditoQuery,
                                  contabContaDebitoQuery.Codigo.As("CodigoDebito"),
                                  contabContaCreditoQuery.Codigo.As("CodigoCredito"),
                                  contabContaDebitoQuery.CodigoReduzida.As("CodigoReduzidaDebito"),
                                  contabContaCreditoQuery.CodigoReduzida.As("CodigoReduzidaCredito"));
        contabRoteiroQuery.InnerJoin(contabContaDebitoQuery).On(contabContaDebitoQuery.IdConta == contabRoteiroQuery.IdContaDebito);
        contabRoteiroQuery.InnerJoin(contabContaCreditoQuery).On(contabContaCreditoQuery.IdConta == contabRoteiroQuery.IdContaCredito);

        coll.Load(contabRoteiroQuery);
        //
        e.Collection = coll;
    }

    protected void EsDSPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;
                            //

                            nome = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        if (textIdEvento.Text != "") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdLiquidacao");            

            if (keyValuesId.Count > 0) {
                for (int i = 0; i < keyValuesId.Count; i++) {
                    int idLiquidacao = Convert.ToInt32(keyValuesId[i]);
                    int idEvento = Convert.ToInt32(textIdEvento.Text);
                    int? idEventoVencimento = null;
                    if (textIdEventoVencimento.Text != "")
                        idEventoVencimento = Convert.ToInt32(textIdEventoVencimento.Text);

                    Liquidacao liquidacao = new Liquidacao();
                    if (liquidacao.LoadByPrimaryKey(idLiquidacao))
                    {
                        liquidacao.IdEvento = idEvento;
                        if (liquidacao.DataVencimento == liquidacao.DataLancamento)
                            idEventoVencimento = null;
                        liquidacao.IdEventoVencimento = idEventoVencimento;
                        liquidacao.Save();
                    }

                    LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                    liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.IdLiquidacao,
                                                               liquidacaoHistoricoCollection.Query.DataHistorico,
                                                               liquidacaoHistoricoCollection.Query.IdEvento,
                                                               liquidacaoHistoricoCollection.Query.IdEventoVencimento);
                    liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(liquidacao.IdCliente.Value),
                                                              liquidacaoHistoricoCollection.Query.DataLancamento.Equal(liquidacao.DataLancamento.Value),
                                                              liquidacaoHistoricoCollection.Query.DataVencimento.Equal(liquidacao.DataVencimento.Value),
                                                              liquidacaoHistoricoCollection.Query.Descricao.Equal(liquidacao.Descricao));
                    liquidacaoHistoricoCollection.Query.Load();

                    foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection) {
                        liquidacaoHistorico.IdEvento = idEvento;
                        liquidacaoHistorico.IdEventoVencimento = idEventoVencimento;
                    }

                    liquidacaoHistoricoCollection.Save();
                }

                e.Result = "Processo executado com sucesso.";
            }
            else {
                e.Result = "Ao menos 1 linha precisa ser selecionada.";
            }
        }
        else{
            e.Result = "O evento deve ser escolhido.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        string descricao = Convert.ToString(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Descricao));
        int idEvento = Convert.ToInt32(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdEvento));
        //
        e.Result = idEvento.ToString() + "|" + descricao;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridContabRoteiro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridContabRoteiro.DataBind();
    }    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.FieldName == "Valor")
        {
            decimal value = (decimal)e.GetValue("Valor");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        if (e.DataColumn.FieldName == "Descricao")
        {
            e.Cell.Attributes.Add("title", e.CellValue.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {

        base.gridCadastro_PreRender(sender, e);
        //
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicioLancamento.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Lacto) >= ").Append(textDataInicioLancamento.Text.Substring(0, 10));
        }
        if (textDataFimLancamento.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Lacto) <= ").Append(textDataFimLancamento.Text.Substring(0, 10));
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Vcto) >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Vcto) <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textDescricaoFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }

            texto.Append(" Descrição Like %").Append(textDescricaoFiltro.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

}