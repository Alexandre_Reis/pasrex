﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

using Financial.Contabil;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;

using DevExpress.Web;

using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using System.Threading;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_ContabConta : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-us";
        this.UICulture = "en-us";
        this.HasPopupContabConta = true;
        base.Page_Load(sender, e);

        gridCadastro.DataBind();

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { ContabContaMetadata.ColumnNames.TipoConta }));
    }

    #region DataSources
    protected void EsDSContabConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabContaQuery contabPlanoContasFilhaQuery = new ContabContaQuery("Filha");
        ContabContaQuery contabPlanoContasMaeQuery = new ContabContaQuery("Mae");

        //
        ContabPlanoQuery contabPlanoQuery = new ContabPlanoQuery("C");
        //
        contabPlanoContasFilhaQuery.Select(contabPlanoContasFilhaQuery,
                                           contabPlanoContasFilhaQuery.Descricao.As("DescricaoConta"), // Para o Binding do ASPxGridLookup
                                           contabPlanoContasMaeQuery.Codigo.As("CodigoMae"), // Descricao da Mae por Auto-Relacionamento
                                           contabPlanoQuery.Descricao.As("DescricaoPlano"));
        //
        contabPlanoContasFilhaQuery.LeftJoin(contabPlanoContasMaeQuery).On(contabPlanoContasFilhaQuery.IdContaMae == contabPlanoContasMaeQuery.IdConta);
        contabPlanoContasFilhaQuery.InnerJoin(contabPlanoQuery).On(contabPlanoContasFilhaQuery.IdPlano == contabPlanoQuery.IdPlano);
        //
        contabPlanoContasFilhaQuery.OrderBy(contabPlanoContasFilhaQuery.IdPlano.Ascending,
                                            contabPlanoContasFilhaQuery.Codigo.Ascending);
        //
        ContabContaCollection coll = new ContabContaCollection();
        coll.Load(contabPlanoContasFilhaQuery);
        //     
        e.Collection = coll;
    }

    protected void EsDSPlano_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabPlanoCollection coll = new ContabPlanoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;
        ASPxComboBox dropTipoConta = gridCadastro.FindEditFormTemplateControl("dropTipoConta") as ASPxComboBox;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropFuncaoConta = gridCadastro.FindEditFormTemplateControl("dropFuncaoConta") as ASPxComboBox;
        ASPxCallbackPanel codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        ASPxTextBox textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCodigoReduzida = gridCadastro.FindEditFormTemplateControl("textCodigoReduzida") as ASPxTextBox;
        //
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] {
            dropPlano, dropTipoConta, textDescricao, dropFuncaoConta });

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (textCodigo.Text.Trim() == "" && textCodigoReduzida.Text.Trim() == "")
        {
            e.Result = "Deve ser informado o Código da conta ou o Código da reduzida.";
            return;
        }

        // Se for Update - IdConta não pode ser o mesmo que idContaMae
        ASPxTextBox textIdConta = gridCadastro.FindEditFormTemplateControl("textIdConta") as ASPxTextBox;
        if (gridCadastro.IsEditing)
        {
            if (!String.IsNullOrEmpty(hiddenIdConta.Text.Trim()))
            {
                if (hiddenIdConta.Text.Trim() == textIdConta.Text.Trim())
                {
                    e.Result = "Conta Filha e Conta Mãe não podem ser iguais.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallbackConta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idConta = Convert.ToInt32(e.Parameter);
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.Codigo);
            contabContaCollection.Query.Where(contabContaCollection.Query.IdConta == idConta);

            contabContaCollection.Query.Load();

            if (contabContaCollection.HasData)
            {
                string codigo = contabContaCollection[0].Codigo;
                texto = codigo.ToString();
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ContabConta contabConta = new ContabConta();
        //
        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;
        ASPxComboBox dropTipoConta = gridCadastro.FindEditFormTemplateControl("dropTipoConta") as ASPxComboBox;
        ASPxComboBox dropFuncaoConta = gridCadastro.FindEditFormTemplateControl("dropFuncaoConta") as ASPxComboBox;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        //
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
        //
        ASPxCallbackPanel codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        ASPxTextBox textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCodigoReduzida = gridCadastro.FindEditFormTemplateControl("textCodigoReduzida") as ASPxTextBox;

        int idConta = (int)e.Keys[0];

        if (contabConta.LoadByPrimaryKey(idConta))
        {
            contabConta.IdPlano = Convert.ToInt32(dropPlano.SelectedItem.Value);
            contabConta.TipoConta = Convert.ToString(dropTipoConta.SelectedItem.Value);
            contabConta.Descricao = Convert.ToString(textDescricao.Text).Trim();
            contabConta.FuncaoConta = Convert.ToInt32(dropFuncaoConta.SelectedItem.Value);
            //
            contabConta.IdContaMae = null;
            contabConta.Codigo = null;
            contabConta.CodigoReduzida = null;

            if (!String.IsNullOrEmpty(hiddenIdConta.Text))
            {
                contabConta.IdContaMae = Convert.ToInt32(hiddenIdConta.Text);
            }
            if (!String.IsNullOrEmpty(textCodigo.Text))
            {
                contabConta.Codigo = textCodigo.Text.Replace(" ", "0");
            }

            if (!String.IsNullOrEmpty(textCodigoReduzida.Text))
            {
                contabConta.CodigoReduzida = textCodigoReduzida.Text.Trim();
            }

            string validaDigito = dropPlano.SelectedItem.GetValue("ValidaDigitoConta").ToString();

            if (validaDigito == "S")
            {
                ASPxCallbackPanel digitoPanel = gridCadastro.FindEditFormTemplateControl("digitoPanel") as ASPxCallbackPanel;
                ASPxTextBox textDigito = digitoPanel.FindControl("textDigito") as ASPxTextBox;
                contabConta.Digito = textDigito.Text;
            }
            else
            {
                contabConta.Digito = null;
            }

            //
            contabConta.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabConta - Update: " + idConta + UtilitarioWeb.ToString(contabConta),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        ContabConta contabConta = new ContabConta();
        //
        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;
        ASPxComboBox dropTipoConta = gridCadastro.FindEditFormTemplateControl("dropTipoConta") as ASPxComboBox;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropFuncaoConta = gridCadastro.FindEditFormTemplateControl("dropFuncaoConta") as ASPxComboBox;
        //
        TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
        //
        ASPxCallbackPanel codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        ASPxTextBox textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCodigoReduzida = gridCadastro.FindEditFormTemplateControl("textCodigoReduzida") as ASPxTextBox;

        contabConta.IdPlano = Convert.ToInt32(dropPlano.SelectedItem.Value);
        contabConta.TipoConta = Convert.ToString(dropTipoConta.SelectedItem.Value);
        contabConta.Descricao = Convert.ToString(textDescricao.Text).Trim();
        contabConta.FuncaoConta = Convert.ToInt32(dropFuncaoConta.SelectedItem.Value);

        if (!String.IsNullOrEmpty(hiddenIdConta.Text))
        {
            contabConta.IdContaMae = Convert.ToInt32(hiddenIdConta.Text);
        }
        if (!String.IsNullOrEmpty(textCodigo.Text))
        {
            contabConta.Codigo = textCodigo.Text.Replace(" ", "0").Replace(",", ".");
        }
        if (!String.IsNullOrEmpty(textCodigoReduzida.Text))
        {
            contabConta.CodigoReduzida = textCodigoReduzida.Text.Trim();
        }

        string validaDigito = dropPlano.SelectedItem.GetValue("ValidaDigitoConta").ToString();

        if (validaDigito == "S")
        {
            ASPxCallbackPanel digitoPanel = gridCadastro.FindEditFormTemplateControl("digitoPanel") as ASPxCallbackPanel;
            ASPxTextBox textDigito = digitoPanel.FindControl("textDigito") as ASPxTextBox;
            contabConta.Digito = textDigito.Text;
        }
        else
        {
            contabConta.Digito = null;
        }

        //
        contabConta.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabConta - Insert: " + contabConta.IdConta + UtilitarioWeb.ToString(contabConta),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

    }

    protected void dropContaMaeOnInit(object sender, EventArgs e)
    {
        (sender as ASPxGridLookup).GridView.Width = Unit.Point(400);
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ContabContaMetadata.ColumnNames.IdConta);

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idConta = Convert.ToInt32(keyValuesId[i]);

                ContabConta c = new ContabConta();
                if (c.LoadByPrimaryKey(idConta))
                {
                    contabContaCollection.AttachEntity(c);
                }
            }
            //
            ContabContaCollection contabContaCollectionClone = (ContabContaCollection)Utilitario.Clone(contabContaCollection);
            //

            try
            {
                contabContaCollection.MarkAllAsDeleted();
                contabContaCollection.Save();
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível excluir alguma(s) da(s) conta(s) por ter evento relacionado no roteiro contábil.");
            }


            foreach (ContabConta cl in contabContaCollectionClone)
            {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ContabConta - Delete: " + cl.IdConta + UtilitarioWeb.ToString(cl),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            TextBox hiddenIdConta = gridCadastro.FindEditFormTemplateControl("hiddenIdConta") as TextBox;
            e.Properties["cpHiddenIdConta"] = hiddenIdConta.ClientID;
        }
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);

        var codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        if (codigoPanel == null) return;

        var textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;
        if (textCodigo == null) return;

        ASPxCallbackPanel digitoPanel = gridCadastro.FindEditFormTemplateControl("digitoPanel") as ASPxCallbackPanel;
        if (digitoPanel == null) return;

        var textDigito = digitoPanel.FindControl("textDigito") as ASPxTextBox;
        var dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;

        if (dropPlano == null || dropPlano.Text == null)
            return;

        var plano = new ContabPlano();
        int idPlano;
        if (Int32.TryParse(dropPlano.Text, out idPlano))
        {
            if (plano.LoadByPrimaryKey(idPlano))
            {
                var validaDigito = plano.ValidaDigitoConta;

                if (validaDigito == "S")
                {
                    textCodigo.MaskSettings.Mask = "9.9.9.99.99.999";
                    textCodigo.MaskSettings.PromptChar = '0';
                    textCodigo.MaskSettings.IncludeLiterals = MaskIncludeLiteralsMode.All;
                    if (string.IsNullOrEmpty(textCodigo.Text)) return;

                    var contabConta = new ContabConta();
                    var conta = textCodigo.Text.Replace(" ", "").Replace(".", "").Replace(",", "");
                    var digito = contabConta.CalculaDigitoContaCOFI(conta.PadRight(10, '0'));
                    textDigito.Text = digito.ToString();
                }
                else
                {
                    if (textDigito != null) textDigito.Text = null;
                    textCodigo.MaskSettings.Mask = null;
                }
            }
        }
    }

    protected void digitoPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        ASPxTextBox textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;

        ASPxCallbackPanel digitoPanel = gridCadastro.FindEditFormTemplateControl("digitoPanel") as ASPxCallbackPanel;
        ASPxTextBox textDigito = digitoPanel.FindControl("textDigito") as ASPxTextBox;

        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;

        if (dropPlano.SelectedItem == null)
            return;

        string validaDigito = dropPlano.SelectedItem.GetValue("ValidaDigitoConta").ToString();

        if (validaDigito == "S" && !string.IsNullOrEmpty(textCodigo.Text))
        {
            // calcula digito
            ContabConta contabConta = new ContabConta();
            string conta = textCodigo.Text.Replace(" ", "").Replace(".", "").Replace(",", "");
            int digito = contabConta.CalculaDigitoContaCOFI(conta.PadRight(10, '0'));

            textDigito.Text = digito.ToString();
        }
        else
        {
            textDigito.Text = null;
        }
    }

    protected void codigoPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel codigoPanel = gridCadastro.FindEditFormTemplateControl("codigoPanel") as ASPxCallbackPanel;
        ASPxTextBox textCodigo = codigoPanel.FindControl("textCodigo") as ASPxTextBox;
        ASPxCallbackPanel digitoPanel = gridCadastro.FindEditFormTemplateControl("digitoPanel") as ASPxCallbackPanel;
        ASPxTextBox textDigito = digitoPanel.FindControl("textDigito") as ASPxTextBox;
        ASPxComboBox dropPlano = gridCadastro.FindEditFormTemplateControl("dropPlano") as ASPxComboBox;

        if (dropPlano.SelectedItem == null)
            return;

        string validaDigito = dropPlano.SelectedItem.GetValue("ValidaDigitoConta").ToString();

        if (validaDigito == "S")
        {
            textCodigo.MaskSettings.Mask ="9.9.9.99.99.999";
            textCodigo.MaskSettings.PromptChar = '0';
            textCodigo.MaskSettings.IncludeLiterals = MaskIncludeLiteralsMode.All;
            if (!string.IsNullOrEmpty(textCodigo.Text))
            {
                ContabConta contabConta = new ContabConta();
                string conta = textCodigo.Text.Replace(" ", "").Replace(".", "");
                int digito = contabConta.CalculaDigitoContaCOFI(conta.PadRight(10, '0'));
                textDigito.Text = digito.ToString();
            }
        }
        else
        {
            textDigito.Text = null;
            textCodigo.MaskSettings.Mask = null;
        }
    }
}