using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Contabil;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;

public partial class CadastrosBasicos_ContabCentroCusto : CadastroBasePage {
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSContabCentroCusto_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabCentroCustoCollection coll = new ContabCentroCustoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ContabCentroCusto contabCentroCusto = new ContabCentroCusto();
        int idCentroCusto = Convert.ToInt32(e.Keys[0]);

        if (contabCentroCusto.LoadByPrimaryKey(idCentroCusto)) {
            contabCentroCusto.Descricao = Convert.ToString(e.NewValues[ContabCentroCustoMetadata.ColumnNames.Descricao]);
            contabCentroCusto.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContabCentroCusto - Operacao: Update ContabCentroCusto: " + idCentroCusto + UtilitarioWeb.ToString(contabCentroCusto),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ContabCentroCusto contabCentroCusto = new ContabCentroCusto();

        contabCentroCusto.Descricao = Convert.ToString(e.NewValues[ContabCentroCustoMetadata.ColumnNames.Descricao]);
        contabCentroCusto.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContabCentroCusto - Operacao: Insert ContabCentroCusto: " + contabCentroCusto.IdCentroCusto + UtilitarioWeb.ToString(contabCentroCusto),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ContabCentroCustoMetadata.ColumnNames.IdCentroCusto);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCentroCusto = Convert.ToInt32(keyValuesId[i]);

                ContabCentroCusto contabCentroCusto = new ContabCentroCusto();
                if (contabCentroCusto.LoadByPrimaryKey(idCentroCusto)) {

                    //
                    ContabCentroCusto contabCentroCustoClone = (ContabCentroCusto)Utilitario.Clone(contabCentroCusto);
                    //
                    
                    contabCentroCusto.MarkAsDeleted();
                    contabCentroCusto.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ContabCentroCusto - Operacao: Delete ContabCentroCusto: " + idCentroCusto + UtilitarioWeb.ToString(contabCentroCustoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}