﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxaRepasseBMF.aspx.cs" Inherits="CadastrosBasicos_TaxaRepasseBMF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head6" runat="server">
    <title></title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataAtivoBMF(data) 
    {
        var resultSplit = data.split('-');
    
        btnEditAtivoBMF.SetValue(resultSplit[0]);        
        popupAtivoBMF.HideWindow();
        btnEditAtivoBMF.Focus();
    }                  
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                 
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Taxa de Clearing" />
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal2" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal3" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdTabela"
                                        DataSourceID="EsDSTaxaRepasseBMF" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="5%" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTabela" Visible="true" VisibleIndex="0" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdPerfilTaxaClearingBMF" Caption="Perfil" VisibleIndex="1"
                                                Width="10%">
                                                <PropertiesComboBox DataSourceID="EsDSPerfilTaxaClearingBMF" TextField="Descricao" ValueField="IdPerfil">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>                                                
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="3" Width="20%"
                                                ExportWidth="130">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Disponivel'>Disponivel</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Futuro'>Futuro</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Opção Disponivel'>Opção Disponivel</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Opção Futuro'>Opção Futuro</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Termo'>Termo</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="CdAtivoBMF" Caption="Código" VisibleIndex="3"
                                                Width="5%" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataTextColumn FieldName="FaixaVencimento" Caption="Vencimento" UnboundType="Integer"
                                                Visible="true" VisibleIndex="5" />
                                            <dxwgv:GridViewDataCheckColumn FieldName="UltimoVencimento" Caption="Ultimo Vencimento"
                                                UnboundType="Boolean" Visible="true" VisibleIndex="6" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorFixoAgenteCustodia" Caption="Valor Fixo Ag. Custódia"
                                                UnboundType="decimal" Visible="true" VisibleIndex="7" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorFixoCorretora" Caption="Valor Fixo Corretora"
                                                UnboundType="decimal" Visible="true" VisibleIndex="8" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Perfil:" />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxComboBox ID="dropPerfil" runat="server" ClientInstanceName="dropPerfil"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListLongo" 
                                                                         DataSourceID="EsDSPerfilTaxaClearingBMF" TextField="Descricao" ValueField="IdPerfil"
                                                                         AllowMouseWheel="true" Text='<%#Eval("IdPerfilTaxaClearingBMF") %>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBMF" runat="server" CssClass="textAtivoCurto"
                                                                        MaxLength="20" ClientInstanceName="btnEditAtivoBMF">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) { gridAtivoBMF.GetColumnByField('Serie').Visible=false; popupAtivoBMF.ShowAtElementByID(s.name);}" />
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelNormal" AssociatedControlID="dropTipoMercado"
                                                                        Text="Mercado:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropTipoMercado"
                                                                        Text='<%#Eval("TipoMercado")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="0" Text="" />
                                                                            <dxe:ListEditItem Value="1" Text="Disponível" />
                                                                            <dxe:ListEditItem Value="2" Text="Futuro" />
                                                                            <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                                                                            <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                                                                            <dxe:ListEditItem Value="5" Text="Termo" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Vencimento:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textVencimento" runat="server" Number="0" MaxValue="99999"
                                                                        MinValue="0" OnInit="textVencimento_Init" ClientInstanceName="textVencimento"
                                                                        Text='<%#Eval("FaixaVencimento") %>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="label6" runat="server" CssClass="labelRequired" Text="Utilizar Vencimentos Posteriores:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxCheckBox ID="blnUltimoVencimento" runat="server" ClientInstanceName="blnUltimoVencimento"
                                                                        ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Value='<%# Eval("UltimoVencimento") %>'>
                                                                    </dxe:ASPxCheckBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Valor Fixo Ag Custódia:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorFixoAgenteCustodia" runat="server" Number="0" MaxValue="99999"
                                                                        NumberType="Float" MinValue="0" OnInit="textValorFixoAgenteCustodia_Init" ClientInstanceName="textValorFixoAgenteCustodia"
                                                                        Text='<%#Eval("ValorFixoAgenteCustodia") %>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Valor Fixo Corretora:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorFixoCorretora" runat="server" Number="0" MaxValue="99999"
                                                                        NumberType="Float" MinValue="0" OnInit="textValorFixoCorretora_Init" ClientInstanceName="textValorFixoCorretora"
                                                                        Text='<%#Eval("ValorFixoCorretora") %>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="40" RightMargin="40" />
        <cc1:esDataSource ID="EsDSTaxaRepasseBMF" runat="server" OnesSelect="EsDSTaxaRepasseBMF_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilTaxaClearingBMF" runat="server" OnesSelect="EsDSPerfilTaxaClearingBMF_esSelect" />
    </form>
</body>
</html>
