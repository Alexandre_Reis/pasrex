﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;

using System.Collections;
using System.Collections.Generic;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using System.Web.UI.HtmlControls;

using Financial.Common;

using Financial.Util;

using DevExpress.Web;

using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.BMF;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_PerfilCorretagemBMF : CadastroBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado,
                                                  PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem,
                                                  PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT
                  }));
    }

    #region DataSources
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfilCorretagemBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PerfilCorretagemBMFQuery perfilCorretagemBMFQuery = new PerfilCorretagemBMFQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Q");

        perfilCorretagemBMFQuery.Select(perfilCorretagemBMFQuery, clienteQuery.Apelido.As("Apelido"));
        //
        perfilCorretagemBMFQuery.InnerJoin(clienteQuery).On(perfilCorretagemBMFQuery.IdCliente == clienteQuery.IdCliente);
        perfilCorretagemBMFQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        perfilCorretagemBMFQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);
        //
        perfilCorretagemBMFQuery.OrderBy(perfilCorretagemBMFQuery.IdCliente.Ascending, 
                                         perfilCorretagemBMFQuery.IdAgente.Ascending,
                                         perfilCorretagemBMFQuery.DataReferencia.Ascending,
                                         perfilCorretagemBMFQuery.TipoMercado.Ascending,
                                         perfilCorretagemBMFQuery.CdAtivoBMF.Ascending);

        PerfilCorretagemBMFCollection coll = new PerfilCorretagemBMFCollection();
        coll.Load(perfilCorretagemBMFQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    #region CallBacks
    /// <summary>
    /// Popup de Cliente
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                        
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// Tratatamento de Erros
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        //
        ASPxButtonEdit btnCdAtivo = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxButtonEdit btnGrupoAtivo = gridCadastro.FindEditFormTemplateControl("btnGrupoAtivo") as ASPxButtonEdit;
        //
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagem = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagem") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagemDT = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagemDT") as ASPxComboBox;
        
        #region Campos obrigatórios
        List<Control> controles = new List<Control>( new Control[] { 
                    textDataReferencia, btnEditCodigoCliente, dropAgente, dropTipoMercado, dropTipoCorretagem, dropTipoCorretagemDT}); 

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();
            perfilCorretagemBMFCollection.Query
                        .Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem)
                        .Where(perfilCorretagemBMFCollection.Query.DataReferencia == Convert.ToDateTime(textDataReferencia.Text) &&
                               perfilCorretagemBMFCollection.Query.IdCliente == Convert.ToInt32(btnEditCodigoCliente.Text) &&
                               perfilCorretagemBMFCollection.Query.IdAgente == Convert.ToInt32(dropAgente.SelectedItem.Value) &&
                               perfilCorretagemBMFCollection.Query.CdAtivoBMF == btnCdAtivo.Text.Trim().ToUpper() &&
                               perfilCorretagemBMFCollection.Query.TipoMercado == Convert.ToByte(dropTipoMercado.SelectedItem.Value) &&
                               perfilCorretagemBMFCollection.Query.GrupoAtivo == btnGrupoAtivo.Text.Trim() &&
                               perfilCorretagemBMFCollection.Query.TipoCorretagem == Convert.ToByte(dropTipoCorretagem.SelectedItem.Value) &&
                               perfilCorretagemBMFCollection.Query.TipoCorretagemDT == Convert.ToByte(dropTipoCorretagemDT.SelectedItem.Value));

            if (perfilCorretagemBMFCollection.Query.Load()) {
                e.Result = "Registro já existente";
            }
        }
    }
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }
    private void SalvarNovo()
    {
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        //
        ASPxButtonEdit btnCdAtivo = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxButtonEdit btnGrupoAtivo = gridCadastro.FindEditFormTemplateControl("btnGrupoAtivo") as ASPxButtonEdit;
        //
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagem = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagem") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagemDT = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagemDT") as ASPxComboBox;


        PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();

        perfilCorretagemBMF.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        perfilCorretagemBMF.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        perfilCorretagemBMF.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        perfilCorretagemBMF.CdAtivoBMF = btnCdAtivo.Text.Trim().ToUpper();
        perfilCorretagemBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
        perfilCorretagemBMF.GrupoAtivo = btnGrupoAtivo.Text.Trim();
        perfilCorretagemBMF.TipoCorretagem = Convert.ToByte(dropTipoCorretagem.SelectedItem.Value);
        perfilCorretagemBMF.TipoCorretagemDT = Convert.ToByte(dropTipoCorretagemDT.SelectedItem.Value);

        perfilCorretagemBMF.Save();

        //cotacaoIndice.Save();

       #region Log do Processo
       HistoricoLog historicoLog = new HistoricoLog();
       historicoLog.InsereHistoricoLog(DateTime.Now,
                                       DateTime.Now,
                                       "Cadastro de perfilCorretagemBMF - Operacao: Insert IdCliente: " + btnEditCodigoCliente.Text,
                                       HttpContext.Current.User.Identity.Name,
                                       UtilitarioWeb.GetIP(Request),
                                       "",
                                       HistoricoLogOrigem.Outros);
       #endregion
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackClone_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        TextBox textIdPerfilCorretagem = gridCadastro.FindEditFormTemplateControl("textIdPerfilCorretagem") as TextBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnCdAtivo = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagem = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagem") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagemDT = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagemDT") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxButtonEdit btnGrupoAtivo = gridCadastro.FindEditFormTemplateControl("btnGrupoAtivo") as ASPxButtonEdit;

        int idPerfilCorretagem = Convert.ToInt32(textIdPerfilCorretagem.Text);
        int idClienteBase = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        string cdAtivo = Convert.ToString(btnCdAtivo.Text);
        string grupoAtivo = Convert.ToString(btnGrupoAtivo.Text);
        byte tipoCorretagem = Convert.ToByte(dropTipoCorretagem.SelectedItem.Value);
        byte tipoCorretagemDT = Convert.ToByte(dropTipoCorretagemDT.SelectedItem.Value);
        byte tipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
        
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
        clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                      clienteCollection.Query.IdCliente.NotEqual(idClienteBase));
        clienteCollection.Query.Load();

        foreach (Cliente cliente in clienteCollection)
        {
            int idCliente = cliente.IdCliente.Value;

            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();
            perfilCorretagemBMFCollection.Query.Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem);
            perfilCorretagemBMFCollection.Query.Where(perfilCorretagemBMFCollection.Query.IdCliente.Equal(idCliente),
                                                      perfilCorretagemBMFCollection.Query.IdAgente.Equal(idAgente),
                                                      perfilCorretagemBMFCollection.Query.DataReferencia.Equal(dataReferencia),
                                                      perfilCorretagemBMFCollection.Query.CdAtivoBMF.Equal(cdAtivo),
                                                      perfilCorretagemBMFCollection.Query.GrupoAtivo.Equal(grupoAtivo),
                                                      perfilCorretagemBMFCollection.Query.TipoCorretagem.Equal(tipoCorretagem),
                                                      perfilCorretagemBMFCollection.Query.TipoCorretagemDT.Equal(tipoCorretagemDT),
                                                      perfilCorretagemBMFCollection.Query.TipoMercado.Equal(tipoMercado));
            perfilCorretagemBMFCollection.Query.Load();

            if (perfilCorretagemBMFCollection.Count == 0)
            {
                PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();
                perfilCorretagemBMF.IdCliente = idCliente;
                perfilCorretagemBMF.CdAtivoBMF = cdAtivo;
                perfilCorretagemBMF.DataReferencia = dataReferencia;
                perfilCorretagemBMF.GrupoAtivo = grupoAtivo;
                perfilCorretagemBMF.IdAgente = idAgente;
                perfilCorretagemBMF.IdPerfilCorretagem = idPerfilCorretagem;
                perfilCorretagemBMF.TipoCorretagem = tipoCorretagem;
                perfilCorretagemBMF.TipoCorretagemDT = tipoCorretagemDT;
                perfilCorretagemBMF.TipoMercado = tipoMercado;
                perfilCorretagemBMF.Save();
            }
        }
    }
    #endregion

    #region Modo Edicao

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgente_Load(object sender, EventArgs e) {
        //if (!gridCadastro.IsNewRowEditing) {
        //    (sender as ASPxComboBox).Enabled = false;
        //    (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        //}
    }
    #endregion

    #region Grid
    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {        
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idPerfilCorretagem = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem));
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia));
            e.Value = idPerfilCorretagem + dataReferencia;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        // Chaves
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textIdPerfilCorretagem = gridCadastro.FindEditFormTemplateControl("textIdPerfilCorretagem") as TextBox;
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        //
        ASPxButtonEdit btnCdAtivo = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxButtonEdit btnGrupoAtivo = gridCadastro.FindEditFormTemplateControl("btnGrupoAtivo") as ASPxButtonEdit;
        //
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagem = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagem") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagemDT = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagemDT") as ASPxComboBox;
        //
        PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();
        //
        // Chaves
        int idPerfilCorretagem = Convert.ToInt32(textIdPerfilCorretagem.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);

        if (perfilCorretagemBMF.LoadByPrimaryKey(idPerfilCorretagem, dataReferencia)) {
            //
            perfilCorretagemBMF.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            perfilCorretagemBMF.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            perfilCorretagemBMF.CdAtivoBMF = btnCdAtivo.Text.Trim().ToUpper();
            perfilCorretagemBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
            perfilCorretagemBMF.GrupoAtivo = btnGrupoAtivo.Text.Trim();
            perfilCorretagemBMF.TipoCorretagem = Convert.ToByte(dropTipoCorretagem.SelectedItem.Value);
            perfilCorretagemBMF.TipoCorretagemDT = Convert.ToByte(dropTipoCorretagemDT.SelectedItem.Value);
            //
            perfilCorretagemBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PerfilCorretagemBMF - Operacao: Update PerfilCorretagemBMF: " + idPerfilCorretagem + "; " + dataReferencia + UtilitarioWeb.ToString(perfilCorretagemBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
       
        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();                
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        //
        ASPxButtonEdit btnCdAtivo = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxButtonEdit btnGrupoAtivo = gridCadastro.FindEditFormTemplateControl("btnGrupoAtivo") as ASPxButtonEdit;
        //
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;        
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagem = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagem") as ASPxComboBox;
        ASPxComboBox dropTipoCorretagemDT = gridCadastro.FindEditFormTemplateControl("dropTipoCorretagemDT") as ASPxComboBox;
        //
        PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();
        //
        perfilCorretagemBMF.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        perfilCorretagemBMF.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        perfilCorretagemBMF.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        perfilCorretagemBMF.CdAtivoBMF = btnCdAtivo.Text.Trim().ToUpper();
        perfilCorretagemBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
        perfilCorretagemBMF.GrupoAtivo = btnGrupoAtivo.Text.Trim();
        perfilCorretagemBMF.TipoCorretagem = Convert.ToByte(dropTipoCorretagem.SelectedItem.Value);
        perfilCorretagemBMF.TipoCorretagemDT = Convert.ToByte(dropTipoCorretagemDT.SelectedItem.Value);
        //
        perfilCorretagemBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilCorretagemBMF - Operacao: Insert PerfilCorretagemBMF: " + perfilCorretagemBMF.IdPerfilCorretagem + "; " + perfilCorretagemBMF.DataReferencia + UtilitarioWeb.ToString(perfilCorretagemBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        const string Delete = "btnDelete";

        if (e.Parameters == Delete) {
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();

            List<object> keyValuesIdPerfilCorretagem = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem);
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia);

            for (int i = 0; i < keyValuesIdPerfilCorretagem.Count; i++) {
                // Chaves
                int idPerfilCorretagem = Convert.ToInt32(keyValuesIdPerfilCorretagem[i]);                
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                //
                PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();
                //
                if (perfilCorretagemBMF.LoadByPrimaryKey(idPerfilCorretagem, dataReferencia)) {
                    perfilCorretagemBMFCollection.AttachEntity(perfilCorretagemBMF);
                }
            }

            //
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollectionClone = (PerfilCorretagemBMFCollection)Utilitario.Clone(perfilCorretagemBMFCollection);
            //

            // Delete
            perfilCorretagemBMFCollection.MarkAllAsDeleted();
            perfilCorretagemBMFCollection.Save();

            foreach (PerfilCorretagemBMF p in perfilCorretagemBMFCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PerfilCorretagemBMF - Operacao: Delete PerfilCorretagemBMF: " + p.IdPerfilCorretagem + "; " + p.DataReferencia + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion   
            }            
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente", "dropAgente");
        base.gridCadastro_PreRender(sender, e);
    }

    #endregion
}