﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using Financial.Common;
using Financial.Util;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.BMF;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_PerfilRepasseBMF : CadastroBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load ( sender, e );
    }

    #region Properties

    int? CodigoPerfil {

        get {
            try {

                return int.Parse ( ( ( gridCadastro.FindEditFormTemplateControl ( "textCodigoPerfil" ) as ASPxSpinEdit ).Text ) );
            }
            catch {
                return null;
            }
        }
    }

    string DescricaoPerfil {
        get {

            try {

                return ( gridCadastro.FindEditFormTemplateControl ( "textDescricaoPerfil" ) as ASPxTextBox ).Text;
            }
            catch {
                return string.Empty;
            }
        }
    }


    #endregion

    #region Log
    void Log (string method ) {

        HistoricoLog historicoLog = new HistoricoLog ( );
        historicoLog.InsereHistoricoLog (DateTime.Now,
                                         DateTime.Now,
                                         string.Format("Cadastro de Perfis de Repasse  - Operacao: {0} --> Codigo: {1}", method,this.CodigoPerfil.ToString()),
                                         HttpContext.Current.User.Identity.Name,
                                         UtilitarioWeb.GetIP ( Request ),
                                         "",
                                         HistoricoLogOrigem.Outros );
    }

    #endregion

    #region DataSources

    protected void EsDSPerfilRepasseBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        PerfilTaxaClearingBMFQuery query = new PerfilTaxaClearingBMFQuery ( );
        //
        query.OrderBy ( query.Descricao.Ascending );
        //
        PerfilTaxaClearingBMFCollection items = new PerfilTaxaClearingBMFCollection ( );
        //
        items.Load(query);
        //
        e.Collection = items;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void textCodigoPerfil_Init ( object sender, EventArgs e ) {

        if ( !this.gridCadastro.IsNewRowEditing )
            ( sender as ASPxSpinEdit ).Enabled = true;
    }

    protected void textDescricaoPerfil_Init ( object sender, EventArgs e ) {

        if ( !this.gridCadastro.IsNewRowEditing )
            ( sender as ASPxTextBox ).Enabled = true;
    }

    #region CallBacks
   

    /// <summary>
    /// Tratatamento de Erros
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        #region Campos obrigatórios
        if ( null == this.CodigoPerfil ){
            e.Result = "Campo Código do Perfil é obrigatório!";
            return;
        }

        if ( null == this.DescricaoPerfil ){

            e.Result = "";
            return;
        }

        if ( null == this.CodigoPerfil ){
            e.Result = "Campo Descrição do Perfil é obrigatório!";
            return;
        }
       
        #endregion

        if(gridCadastro.IsNewRowEditing) {

            PerfilTaxaClearingBMF perfilCodigo = new PerfilTaxaClearingBMF();
            if (perfilCodigo.LoadByPrimaryKey(this.CodigoPerfil.Value)) {
                e.Result = "Código de perfil já existe!";
                return;
            }
            PerfilTaxaClearingBMFCollection perfiDescricao = new PerfilTaxaClearingBMFCollection();
            perfiDescricao.Query.Where(perfiDescricao.Query.Descricao.Equal(this.DescricaoPerfil));
            if (perfiDescricao.Query.Load()) {
                e.Result = "Descrição de perfil já existe!";
                return;
            }

            
            //PerfilTaxaClearingBMFCollection q = new PerfilTaxaClearingBMFCollection ( );
            //q.Query.Select ( q.Query.IdPerfil )
            //       .Where (  q.Query.IdPerfil == this.CodigoPerfil && 
            //                 q.Query.Descricao == this.DescricaoPerfil );

            //if( q.Query.Load( ) ) {
            //    e.Result = "Registro já existe!";
            //}
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Salvar ( );
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    void Salvar ( ) {

        PerfilTaxaClearingBMF entity = new PerfilTaxaClearingBMF ( );
        entity.IdPerfil = this.CodigoPerfil;
        entity.Descricao = this.DescricaoPerfil;
        entity.Save ( );
        this.Log ( "INSERT" );
    }
  
    #endregion

    #region Modo Edicao

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
   

    #endregion

    #region Grid
    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {        
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string IdPerfil = Convert.ToString ( e.GetListSourceFieldValue ( PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil ) );
            string CodigoPerfil = Convert.ToString ( e.GetListSourceFieldValue ( PerfilTaxaClearingBMFMetadata.ColumnNames.Descricao ) );
            e.Value = IdPerfil + DescricaoPerfil;
        }
    }

    protected void gridCadastro_RowUpdating ( object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e ) {
        int IdPerfil = 0;
        PerfilTaxaClearingBMFQuery q = new PerfilTaxaClearingBMFQuery ( );
        q.Where ( q.IdPerfil == this.CodigoPerfil );
        PerfilTaxaClearingBMFCollection items = new PerfilTaxaClearingBMFCollection ( );
        items.Load ( q );

        if ( null != items ) {
            foreach ( PerfilTaxaClearingBMF item in items )
            {
                if ( item.LoadByPrimaryKey ( item.IdPerfil.Value ) ) {

                    PerfilTaxaClearingBMF clone = ( PerfilTaxaClearingBMF )Utilitario.Clone ( item );
                    clone.IdPerfil = this.CodigoPerfil;
                    clone.Descricao = this.DescricaoPerfil;
                    clone.Save ( );
                    this.Log ( "UPDATED" );
                }
            }
        }
        e.Cancel = true;
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    protected void gridCadastro_RowInserting ( object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e ) {

        PerfilTaxaClearingBMF entity = new PerfilTaxaClearingBMF ( );
        entity.IdPerfil = this.CodigoPerfil;
        entity.Descricao = this.DescricaoPerfil;
        entity.Save ( );
        this.Log ( "INSERT" );
        e.Cancel = true;
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        //const string Delete = "btnDelete";
        //
        this.RemoverPerfilSelecionado ( );
        //
        this.gridCadastro.DataBind ( );
        //
        this.gridCadastro.Selection.UnselectAll ( );
        //
        this.gridCadastro.CancelEdit ( );
    }

    void RemoverPerfilSelecionado ( ) {

        List<object> entityIds = this.gridCadastro.GetSelectedFieldValues(PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil);
        //
        entityIds.ForEach ( delegate ( object objId )
        {
            int Id = Convert.ToInt32 ( objId.ToString ( ) );
            PerfilTaxaClearingBMF entity = new PerfilTaxaClearingBMF ( );
            if ( entity.LoadByPrimaryKey ( Id ) ) {
                PerfilTaxaClearingBMF cloneObj = ( PerfilTaxaClearingBMF )Utilitario.Clone ( entity );
                cloneObj.MarkAsDeleted ( );
                cloneObj.Save ( );
                this.Log ( "DELETED" );
            }
        } );
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;

        if ( gridView.IsEditing ) {

            ASPxSpinEdit codigoPerfil = this.gridCadastro.FindEditFormTemplateControl ( "textCodigoPerfil" ) as ASPxSpinEdit;
            e.Properties["cpCodigoPerfil"] = codigoPerfil.ClientID;

            ASPxTextBox descricaoPerfil = this.gridCadastro.FindEditFormTemplateControl ( "textDescricaoPerfil" ) as ASPxTextBox;
            e.Properties["cpDescricaoPerfil"] = descricaoPerfil.ClientID;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        base.gridCadastro_PreRender(sender, e);
        ASPxGridView grid = ( sender as ASPxGridView );
    }

    #endregion
}