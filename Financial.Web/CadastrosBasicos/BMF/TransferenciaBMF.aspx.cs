﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using DevExpress.Web;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.BMF;
using Financial.Web.Common;

using Financial.ContaCorrente.Enums;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TransferenciaBMF : CadastroBasePage {    
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        this.HasPopupAtivoBMF = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTransferenciaBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        if (usuario.BuscaUsuario(Context.User.Identity.Name)) {
            int idUsuario = usuario.IdUsuario.Value;

            TransferenciaBMFQuery transferenciaBMFQuery = new TransferenciaBMFQuery("T");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            transferenciaBMFQuery.Select(transferenciaBMFQuery, clienteQuery.Apelido.As("Apelido"));
            transferenciaBMFQuery.InnerJoin(clienteQuery).On(transferenciaBMFQuery.IdCliente == clienteQuery.IdCliente);
            transferenciaBMFQuery.InnerJoin(permissaoClienteQuery).On(transferenciaBMFQuery.IdCliente == permissaoClienteQuery.IdCliente);
            transferenciaBMFQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

            if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
                transferenciaBMFQuery.Where(transferenciaBMFQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
            }

            if (!String.IsNullOrEmpty(textDataInicio.Text)) {
                transferenciaBMFQuery.Where(transferenciaBMFQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
            }

            if (!String.IsNullOrEmpty(textDataFim.Text)) {
                transferenciaBMFQuery.Where(transferenciaBMFQuery.Data.LessThanOrEqual(textDataFim.Text));
            }

            transferenciaBMFQuery.OrderBy(transferenciaBMFQuery.Data.Descending);

            TransferenciaBMFCollection coll = new TransferenciaBMFCollection();
            coll.Load(transferenciaBMFQuery);

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            e.Collection = new TransferenciaBMFCollection();
        }

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCustodiante == 'S');
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
            ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
            ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            
            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditAtivoBMF);
            controles.Add(dropAgenteOrigem);
            controles.Add(dropAgenteDestino);
            controles.Add(textQuantidade);
            controles.Add(textPU);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) {
                e.Result = "Data não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;

        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {
                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;

                            nome = nome + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else {
                            nome = "no_access";
                        }
                    }
                    else {
                        nome = "no_active";
                    }
                }
                else {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string cdAtivoBMF = "";
        string serie = "";
        //string peso = "";
        //string tipoMercado = "";
        e.Result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(e.Parameter));
            string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(e.Parameter));
            ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

            if (ativoBMF.str.CdAtivoBMF != "")
            {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                e.Result = cdAtivoBMF + serie;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTransferencia = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
        ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        TransferenciaBMF transferenciaBMF = new TransferenciaBMF();
        if (transferenciaBMF.LoadByPrimaryKey(idTransferencia))
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            transferenciaBMF.IdCliente = idCliente;
            transferenciaBMF.CdAtivoBMF = Convert.ToString(btnEditAtivoBMF.Text).ToUpper().Substring(0, 3);
            transferenciaBMF.Serie = Convert.ToString(btnEditAtivoBMF.Text).ToUpper().Substring(4);
            transferenciaBMF.Data = Convert.ToDateTime(textData.Text);
            transferenciaBMF.IdAgenteOrigem = Convert.ToInt32(dropAgenteOrigem.SelectedItem.Value);
            transferenciaBMF.IdAgenteDestino = Convert.ToInt32(dropAgenteDestino.SelectedItem.Value);
            transferenciaBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);

            decimal? pu = null;
            if (!String.IsNullOrEmpty(textPU.Text)) {
                pu = Convert.ToDecimal(textPU.Text);
            }
            transferenciaBMF.Pu = pu;

            transferenciaBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TransferenciaBMF - Operacao: Update TransferenciaBMF: " + idTransferencia + UtilitarioWeb.ToString(transferenciaBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();
        
        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
        ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        AtivoBMF ativoBMF = new AtivoBMF();
        string cdAtivoBMF = "";
        string serie = "";
        string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text));
        string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text));
        ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

        if (ativoBMF.str.CdAtivoBMF != "")
        {
            cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
            serie = ativoBMF.str.Serie; 
        }

        TransferenciaBMF transferenciaBMF = new TransferenciaBMF();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);        

        transferenciaBMF.IdCliente = idCliente;
        transferenciaBMF.CdAtivoBMF = cdAtivoBMF;
        transferenciaBMF.Serie = serie; //Convert.ToString(btnEditAtivoBMF.Text).ToUpper().Substring(4);
        transferenciaBMF.Data = Convert.ToDateTime(textData.Text);
        transferenciaBMF.IdAgenteOrigem = Convert.ToInt32(dropAgenteOrigem.SelectedItem.Value);
        transferenciaBMF.IdAgenteDestino = Convert.ToInt32(dropAgenteDestino.SelectedItem.Value);
        transferenciaBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);

        if (!String.IsNullOrEmpty(textPU.Text)) {
            transferenciaBMF.Pu = Convert.ToDecimal(textPU.Text);
        }

        transferenciaBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TransferenciaBMF - Operacao: Insert TransferenciaBMF: " + transferenciaBMF.IdTransferencia + UtilitarioWeb.ToString(transferenciaBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTransferencia");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idTranferencia = Convert.ToInt32(keyValuesId[i]);

                TransferenciaBMF transferenciaBMF = new TransferenciaBMF();
                if (transferenciaBMF.LoadByPrimaryKey(idTranferencia))
                {
                    int idCliente = transferenciaBMF.IdCliente.Value;

                    TransferenciaBMF transferenciaBMFClone = (TransferenciaBMF)Utilitario.Clone(transferenciaBMF);
                    //

                    transferenciaBMF.MarkAsDeleted();
                    transferenciaBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TransferenciaBMF - Operacao: Delete TransferenciaBMF: " + idTranferencia + UtilitarioWeb.ToString(transferenciaBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);                              
        //        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CdAtivoBMF" }));
            string serie = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Serie" }));

            btnEditAtivoBMF.Text = cdAtivoBMF + serie;
        }
    }
}