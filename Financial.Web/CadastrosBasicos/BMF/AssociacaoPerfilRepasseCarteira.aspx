﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssociacaoPerfilRepasseCarteira.aspx.cs" Inherits="AssociacaoPerfilRepasseCarteira" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript">
    
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    // <![CDATA[
        function AddSelectedItems() {
            MoveSelectedItems(lbAvailable, lbChoosen, false);
            UpdateButtonState();
        }
        function AddAllItems() {
            MoveAllItems(lbAvailable, lbChoosen, false);
            UpdateButtonState();
        }
        function RemoveSelectedItems() {
            MoveSelectedItems(lbChoosen, lbAvailable, true);
            UpdateButtonState();
        }
        function RemoveAllItems() {
            MoveAllItems(lbChoosen, lbAvailable, true);
            UpdateButtonState();
        }
        function MoveSelectedItems(srcListBox, dstListBox, removeEspaco) {
            srcListBox.BeginUpdate();
            dstListBox.BeginUpdate();
            var items = srcListBox.GetSelectedItems();
            var contaEspaco = 0;
            for(var i = items.length - 1; i >= 0; i = i - 1) {
                if (items[i].value == 999 || items[i].value == 9991 || items[i].value == 9992 ||
                    items[i].value == 9993 || items[i].value == 9994) {
                    if (removeEspaco)
                        srcListBox.RemoveItem(items[i].index);
                    else {
                        contaEspaco++;
                        dstListBox.AddItem(items[i].text, (items[i].value*10)+contaEspaco);
                        srcListBox.RemoveItem(items[i].index);
                        srcListBox.AddItem(items[i].text, items[i].value);
                    }
                }
                else {
                    dstListBox.AddItem(items[i].text, items[i].value);
                    srcListBox.RemoveItem(items[i].index);
                }
            }
            srcListBox.EndUpdate();
            dstListBox.EndUpdate();
        }
        function MoveAllItems(srcListBox, dstListBox, removeEspaco) {
            srcListBox.BeginUpdate();
            var count = srcListBox.GetItemCount();
            for(var i = 0; i < count; i++) {
                var item = srcListBox.GetItem(i);
                if (item.value != 999 && item.value != 9991 && item.value != 9992 &&
                    item.value != 9993 && item.value != 9994) {
                    dstListBox.AddItem(item.text, item.value);
                }
            }
            srcListBox.EndUpdate();
            srcListBox.ClearItems();
            if (!removeEspaco) {
                srcListBox.AddItem("Espaço em Branco", 999);
            }
        }
        function UpdateButtonState() {
            btnMoveAllItemsToRight.SetEnabled(lbAvailable.GetItemCount() > 0);
            btnMoveAllItemsToLeft.SetEnabled(lbChoosen.GetItemCount() > 0);
            btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
            btnMoveSelectedItemsToLeft.SetEnabled(lbChoosen.GetSelectedItems().length > 0);
        }
        
    // ]]> 
    </script>
</head>

<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

  <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
             

            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Associação Perfil Clearing x Carteiras" />
                            </div>
                            
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    
                                                                
                                                     <dxrp:ASPxRoundPanel ID="pnlFront" runat="server" HeaderText="" Width="800px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">                                                                                                                           
                                                                <table align="center" cellpadding="0" cellspacing="0" width="800px">
                                                                    <tr>
                                                                        <td valign="top" style="width: 38%">
                                                                            <div class="BottomPadding">
                                                                                <asp:Label ID="lblListaPerfisRepasse" runat="server" Text="Perfil Repasse:" />
                                                                            </div>
                                                                            <dxe:ASPxComboBox ID="dropPerfilRepasse" 
                                                                                              runat="server" 
                                                                                              ClientInstanceName="dropPerfilRepasse"
                                                                                              AutoPostBack="true"
                                                                                              ShowShadow="false"
                                                                                              DropDownStyle="DropDownList"
                                                                                              CssClass="dropDownListLongo"
                                                                                              OnLoad="dropPerfilRepasse_OnLoad"
                                                                                              AllowMouseWheel="true"
                                                                                              OnSelectedIndexChanged="dropPerfilRepasse_SelectedChanged">
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="3"><br />   
                                                                            <b><asp:Label ID="label10" runat="server" Text="Seleção de Carteiras" Font-Size="Small" /></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" style="width: 38%">
                                                                            <div class="BottomPadding">
                                                                                <asp:Label ID="lblAvailable" runat="server" Text="Disponíveis:" />
                                                                            </div>
                                                                            <dxe:ASPxListBox ID="lbAvailable" Enabled="false" runat="server" ClientInstanceName="lbAvailable" Width="100%" Height="466px" SelectionMode="CheckColumn" OnPreRender="lbAvailable_OnPreRender">                                                                            
                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                                                                            </dxe:ASPxListBox>
                                                                        </td>
                                                                        <td valign="middle" align="center" style="padding: 10px; width: 26%">
                                                                            <div>
                                                                                <dxe:ASPxButton ID="btnMoveSelectedItemsToRight" runat="server" ClientInstanceName="btnMoveSelectedItemsToRight"
                                                                                    AutoPostBack="False" Text="Adicionar >" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) 
                                                                                    { 
                                                                                        if( null == dropPerfilRepasse.GetValue() ){
                                                                                            alert('Selecione um perfil para continuar!');
                                                                                            return;
                                                                                        }
                                                                                        //
                                                                                        AddSelectedItems(); 
                                                                                        
                                                                                    }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div class="TopPadding">
                                                                                <dxe:ASPxButton ID="btnMoveAllItemsToRight" runat="server" ClientInstanceName="btnMoveAllItemsToRight"
                                                                                    AutoPostBack="False" Text="Adicionar todos >>" Width="130px" Height="23px">
                                                                                    <ClientSideEvents Click="function(s, e) 
                                                                                    { 
                                                                                        if( null == dropPerfilRepasse.GetValue() ){
                                                                                            alert('Selecione um perfil para continuar!');
                                                                                            return;
                                                                                        }
                                                                                        //
                                                                                        AddAllItems(); 
                                                                                    }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div style="height: 32px">
                                                                            </div>
                                                                            <div>
                                                                                <dxe:ASPxButton ID="btnMoveSelectedItemsToLeft" runat="server" ClientInstanceName="btnMoveSelectedItemsToLeft"
                                                                                    AutoPostBack="False" Text="< Remover" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) 
                                                                                    { 
                                                                                        if( null == dropPerfilRepasse.GetValue() ){
                                                                                            alert('Selecione um perfil para continuar!');
                                                                                            return;
                                                                                        }
                                                                                        //                                                                                        
                                                                                        RemoveSelectedItems(); 
                                                                                    }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div class="TopPadding">
                                                                                <dxe:ASPxButton ID="btnMoveAllItemsToLeft" runat="server" ClientInstanceName="btnMoveAllItemsToLeft"
                                                                                    AutoPostBack="False" Text="<< Remover todos" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) 
                                                                                    {
                                                                                       if( null == dropPerfilRepasse.GetValue() ){
                                                                                            alert('Selecione um perfil para continuar!');
                                                                                            return;
                                                                                        }
                                                                                        //
                                                                                        RemoveAllItems(); 
                                                                                     }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top" style="width: 38%">
                                                                            <div class="BottomPadding"><asp:Label ID="lblChosen" runat="server" Text="Escolhidos:" /></div>
                                                                            <dxe:ASPxListBox ID="lbChoosen" Enabled="false" runat="server" ClientInstanceName="lbChoosen" Width="100%" Height="466px" SelectionMode="CheckColumn" OnPreRender="lbChoosen_OnPreRender">
                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                { 
                                                                                    if( null == dropPerfilRepasse.GetValue() ){
                                                                                            alert('Selecione um perfil para continuar!');
                                                                                            return;
                                                                                        }
                                                                                        //
                                                                                        UpdateButtonState(); 
                                                                                    
                                                                                 }" />
                                                                            </dxe:ASPxListBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                
                                                                </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                            
                                                                <table align="left" border="0" width="600px"  cellpadding="2" cellspacing="2">
                                                                <tr>
                                                                </tr>
                                                                </table>
                                                                <br /><br />
                                                                <div class="linkButton linkButtonTbar" style="margin: 0px 0 0 0px !important;">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" 
                                                                                        runat="server" 
                                                                                        Font-Overline="false" 
                                                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div></div>
                                                                            
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <br />
                                                </td>
                                        </table>                
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
    </form>
</body>
</html>