﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaTOB.aspx.cs" Inherits="CadastrosBasicos_TabelaTOB" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
                       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataClasseBMF(data) {
        btnCdAtivo.SetValue(data);
        popupClasseBMF.HideWindow();             
    }             
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <dxpc:ASPxPopupControl ID="popupClasseBMF" ClientInstanceName="popupClasseBMF" runat="server" Width="100px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        
        <div>
            <dxwgv:ASPxGridView ID="gridClasseBMF" runat="server" Width="100%"
                    ClientInstanceName="gridClasseBMF"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSClasseBMF" KeyFieldName="CdAtivoBMF"
                    OnCustomDataCallback="gridClasseBMF_CustomDataCallback" 
                    OnCustomCallback="gridClasseBMF_CustomCallback"
                    OnHtmlRowCreated="gridClasseBMF_HtmlRowCreated">               

                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" Caption="Ativo" VisibleIndex="0" ReadOnly="true" Width="10%"/>
                </Columns>            
                <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
                <SettingsBehavior ColumnResizeMode="Disabled" />
                <ClientSideEvents RowDblClick="function(s, e) {gridClasseBMF.GetValuesOnCustomCallback(e.visibleIndex, 
                                                        OnGetDataClasseBMF);}" 
                                  Init="function(s, e) {e.cancel = true;}"
	            />
    	        
                <SettingsDetail ShowDetailButtons="False" />
                <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                </Styles>
                <Images>
                    <PopupEditFormWindowClose Height="17px" Width="17px" />
                </Images>
                <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Ativo BMF" />
            </dxwgv:ASPxGridView>            
        </div>      
        </dxpc:PopupControlContentControl>
        </ContentCollection>
        
        <ClientSideEvents CloseUp="function(s, e) {gridClasseBMF.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Tabela TOB (BMF)" />
    </div>
    
    <div id="mainContent">
       
        <div class="linkButton">
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>

        <div class="divDataGrid">
        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
            KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaTOB"
            OnCustomCallback="gridCadastro_CustomCallback"
            OnRowInserting="gridCadastro_RowInserting"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnBeforeGetCallbackResult="gridCadastro_PreRender"
            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">        
                
        <Columns>           
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="5%" ShowClearFilterButton="True">
                <HeaderTemplate>
                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                </HeaderTemplate>
            </dxwgv:GridViewCommandColumn>
                                            
            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />

            <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="0" Width="10%"/>
            
            <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" VisibleIndex="1" Caption="Ativo BMF" Width="5%" Settings-AutoFilterCondition="Contains" />
            
            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="2" Width="10%">
                <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Disponível'>Disponível</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Futuro'>Futuro</div>" />
                        <dxe:ListEditItem Value="3" Text="<div title='Opção Disponível'>Opção Disponível</div>" />
                        <dxe:ListEditItem Value="4" Text="<div title='Opção Futuro'>Opção Futuro</div>" />
                        <dxe:ListEditItem Value="5" Text="<div title='Termo'>Termo</div>" />                            
                    </Items>
                </PropertiesComboBox>
            </dxwgv:GridViewDataComboBoxColumn>                               
            
            <dxwgv:GridViewDataSpinEditColumn FieldName="TOB" VisibleIndex="3" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>

            <dxwgv:GridViewDataSpinEditColumn FieldName="TOBDayTrade" Caption="TOB DayTrade" VisibleIndex="4" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>

            <dxwgv:GridViewDataSpinEditColumn FieldName="TOBMinima" Caption="TOB Mínima" VisibleIndex="5" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>

            <dxwgv:GridViewDataSpinEditColumn FieldName="TOBDayTradeMinima" Caption="TOB Day. Mínima" VisibleIndex="6" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>

            <dxwgv:GridViewDataSpinEditColumn FieldName="TOBExercicio" Caption="TOB Exercício" VisibleIndex="7" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>

            <dxwgv:GridViewDataSpinEditColumn FieldName="TOBExercicioCasado" Caption="TOB Exercício Casado" VisibleIndex="8" Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
            </dxwgv:GridViewDataSpinEditColumn>
                                                                                                                                                         
        </Columns>
        
        <Templates>
            <EditForm>                
                <div class="editForm">       
				                            
                    <table>
                        <tr>
                            <td  class="td_Label_Longo">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Referência:"  /></td> 
                            <td colspan="3">
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init"/>	                            
                            </td>  
                        </tr>
                    
                    	<tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelRequired" Text="Tipo Mercado:" />
                            </td>                    
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" ShowShadow="false" CssClass="dropDownListCurto"
			                            DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
			                            Text='<%# Eval("TipoMercado") %>' OnLoad="dropTipoMercado_OnLoad">
	                            <Items>
	                                <dxe:ListEditItem Value="1" Text="Disponível" />
	                                <dxe:ListEditItem Value="2" Text="Futuro" />
	                                <dxe:ListEditItem Value="3" Text="Opção Disponível" />
	                                <dxe:ListEditItem Value="4" Text="Opção Futuro" />
	                                <dxe:ListEditItem Value="5" Text="Termo" />                                    
	                            </Items>                                                 
                                </dxe:ASPxComboBox>             
                            </td>
                		
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCdAtivoBMF" runat="server" CssClass="labelRequired" Text="Ativo BMF:" />
                            </td>
                            <td>                                                                                            
                                <dxe:ASPxButtonEdit ID="btnCdAtivo" runat="server" CssClass="textAtivoBMFCurto" MaxLength="3" 
                                                        ClientInstanceName="btnCdAtivo" Text="<%#Bind('CdAtivoBMF')%>"
                                                        OnLoad="cdAtivoBMF_Load">            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents    
                                         ButtonClick="function(s, e) {popupClasseBMF.ShowAtElementByID(s.name);}"                                          
                                    />                    
                                </dxe:ASPxButtonEdit>                                                                                                                                                                                                  
                            </td>                                                               
                        </tr>	                        	                        	                                                    	    
                                                                                                                                                                                                                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOB" runat="server" CssClass="labelRequired" Text="TOB:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOB" runat="server" CssClass="labelNormal" ClientInstanceName="textTOB"
                                                                Text='<%# Eval("TOB") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                            
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOBDayTrade" runat="server" CssClass="labelRequired" Text="TOB DayTrade:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOBDayTrade" runat="server" CssClass="labelNormal" ClientInstanceName="textTOBDayTrade"
                                                                Text='<%# Eval("TOBDayTrade") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOBMinima" runat="server" CssClass="labelRequired" Text="TOB Mínima:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOBMinima" runat="server" CssClass="labelNormal" ClientInstanceName="textTOBMinima"
                                                                Text='<%# Eval("TOBMinima") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                            
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOBDayTradeMinima" runat="server" CssClass="labelRequired" Text="TOB DayTrade Mínima:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOBDayTradeMinima" runat="server" CssClass="labelNormal" ClientInstanceName="textTOBDayTradeMinima"
                                                                Text='<%# Eval("TOBDayTradeMinima") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOBExercicio" runat="server" CssClass="labelRequired" Text="TOB Exercício:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOBExercicio" runat="server" CssClass="labelNormal" ClientInstanceName="textTOBExercicio"
                                                                Text='<%# Eval("TOBExercicio") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                            
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTOBExercicioCasado" runat="server" CssClass="labelRequired" Text="TOB Exercício Casado:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textTOBExercicioCasado" runat="server" CssClass="labelNormal" ClientInstanceName="textTOBExercicioCasado"
                                                                Text='<%# Eval("TOBExercicioCasado") %>' Width="85%"
                                                                MaxLength="13" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>                                                                            
                            </td>
                        </tr>
                	    
                    </table>
                    	
                    <div class="linhaH"></div>

                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
			                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                </div>                
            </EditForm>
        </Templates>
        
        <SettingsPopup EditForm-Width="500px" />
        <SettingsCommandButton>
            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
        </SettingsCommandButton>
        
    </dxwgv:ASPxGridView>            
    </div>

    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" 
    LeftMargin = "40" RightMargin = "40"
    />
        
    <cc1:esDataSource ID="EsDSTabelaTOB" runat="server" OnesSelect="EsDSTabelaTOB_esSelect" LowLevelBind="true" />            
    <cc1:esDataSource ID="EsDSClasseBMF" runat="server" OnesSelect="EsDSClasseBMF_esSelect" />
        
    </form>
</body>
</html>