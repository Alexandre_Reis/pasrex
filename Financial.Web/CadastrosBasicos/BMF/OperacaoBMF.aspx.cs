﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_OperacaoBMF : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBMF = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { OperacaoBMFMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        //
        AgenteMercadoQuery agenteQuery = new AgenteMercadoQuery("A");
        //
        operacaoBMFQuery.Select(operacaoBMFQuery, clienteQuery.Apelido.As("Apelido"), agenteQuery.Nome);
        operacaoBMFQuery.InnerJoin(clienteQuery).On(operacaoBMFQuery.IdCliente == clienteQuery.IdCliente);
        operacaoBMFQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoBMFQuery.InnerJoin(agenteQuery).On(operacaoBMFQuery.IdAgenteCorretora == agenteQuery.IdAgente);
        //        
        operacaoBMFQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        operacaoBMFQuery.Where(operacaoBMFQuery.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            operacaoBMFQuery.Where(operacaoBMFQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            operacaoBMFQuery.Where(operacaoBMFQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text))
        {
            operacaoBMFQuery.Where(operacaoBMFQuery.CdAtivoBMF.Like("%" + textCdAtivo.Text + "%"));
        }

        if (!String.IsNullOrEmpty(textSerie.Text))
        {
            operacaoBMFQuery.Where(operacaoBMFQuery.Serie.Like("%" + textSerie.Text + "%"));
        }

        operacaoBMFQuery.OrderBy(operacaoBMFQuery.Data.Descending,
                                 agenteQuery.Nome.Ascending);

        OperacaoBMFCollection coll = new OperacaoBMFCollection();
        coll.Load(operacaoBMFQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);        

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }
			
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {

        if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            textData.ClientEnabled = true;
        }

        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        if (!multiConta)
        {
            labelConta.Visible = false;
            dropConta.Visible = false;
        }

        bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;

        if (!agenteLiquidacaoExclusivo)
        {
            Label labelAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("labelAgenteLiquidacao") as Label;
            ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
            labelAgenteLiquidacao.Visible = false;
            dropAgenteLiquidacao.Visible = false;
        }

        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            //TipoMercado, Peso
            string cdAtivoBMF = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBMF").ToString();
            string serie = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Serie").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
            HiddenField hiddenIdTipoMercado = gridCadastro.FindEditFormTemplateControl("hiddenIdTipoMercado") as HiddenField;
            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;

            AtivoBMF ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

            string tipoMercado = TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(ativoBMF.TipoMercado));
            string peso = ativoBMF.Peso.ToString();

            textTipoMercado.Text = tipoMercado;
            textPeso.Text = peso;
            hiddenIdTipoMercado.Value = ativoBMF.TipoMercado.ToString();
            //

            //CheckBox de custos informados            
            ASPxCheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as ASPxCheckBox;
            string calculaDespesas = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CalculaDespesas").ToString();

            if (calculaDespesas == "S") {
                chkCustosInformados.Checked = false;
            }
            else {
                chkCustosInformados.Checked = true;
            }
            //

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;	
		    ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            HiddenField hiddenIdTipoMercado = gridCadastro.FindEditFormTemplateControl("hiddenIdTipoMercado") as HiddenField;
            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropAgenteCorretora);
            controles.Add(dropTipoOperacao);
            controles.Add(btnEditAtivoBMF);
            controles.Add(textQuantidade);
            controles.Add(dropLocalNegociacao);
            controles.Add(dropLocalCustodia);
            controles.Add(dropClearing);
            controles.Add(textDataOperacao);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            string[] ativo = btnEditAtivoBMF.Text.Split('-');
            if ((ativo[0] == "DI1" || ativo[0] == "DDI") && Convert.ToDecimal(textPU.Text) == 0)
            {
                if (gridCadastro.IsNewRowEditing)
                {
                    controles.Add(textTaxa);
                }
                else
                {
                    controles.Add(textPU);
                    controles.Add(textValor);
                }
            }
            else
            {
                controles.Add(textPU);
                controles.Add(textValor);
            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data da operação não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                DateTime dataInicio = cliente.DataInicio.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataInicio > data)
                {
                    e.Result = "Data informada não pode ser anterior a data de início do Cliente(" + dataInicio.ToString("dd/MM/yyyy") + ").";
                    return;
                }
                //else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                //    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                //    return;
                //}
            }

            // Se Data está Preenchida e Existe Ativo compara a Data de Vencimento
            if (!String.IsNullOrEmpty(textData.Text)) {
                AtivoBMF a = new AtivoBMF();

                string[] ativoBMF = btnEditAtivoBMF.Text.Split('-');

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(a.Query.DataVencimento);
                string cdAtivo = ativo[0];
                string serie = ativo[1];

                if (a.LoadByPrimaryKey(campos, cdAtivo, serie)) {
                    if (a.DataVencimento.HasValue && a.DataVencimento < Convert.ToDateTime(textData.Text)) {
                        e.Result = "Ativo Vencido em: " + a.DataVencimento.Value.ToString("d");
                        return;
                    }
                }
            }
            
            #region Validação Ingressos e retiradas
            if ((hiddenIdTipoMercado.Value != "1" && hiddenIdTipoMercado.Value != "3" && hiddenIdTipoMercado.Value != "4") &&
                (dropTipoOperacao.Value.ToString() == "RE" || dropTipoOperacao.Value.ToString() == "DE"))
            {
                e.Result = "Operação Invalida! Apenas os mercados: Disponível, Opções de Futuros e Opções de Disponível podem ser do tipo Depósito ou Retirada";
            }

            #endregion

            #region Operação Retroativa
            if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
            {
                if (DateTime.Compare(data, cliente.DataDia.Value) < 0)
                {
                    e.Result = "Data de registro anterior a data do Cliente. | Favor processar a carteira novamente a partir de " + data.ToString("dd/MM/yyyy");
                    return;
                }
            }
            #endregion
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {
                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBMF = "";
        string serie = "";
        string peso = "";
        string tipoMercado = "";
        e.Result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") 
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(e.Parameter));
            string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(e.Parameter));
            ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

            if (ativoBMF.str.CdAtivoBMF != "") {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                tipoMercado = TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(ativoBMF.TipoMercado));
                peso = ativoBMF.Peso.ToString();
                int idLocalCustodia = (int)ativoBMF.IdLocalCustodia;
                int idLocalNegociacao = (int)ativoBMF.IdLocalNegociacao;
                int idClearing = (int)ativoBMF.IdClearing;
                e.Result = cdAtivoBMF + serie + "|" + tipoMercado + "|" + peso + "|" + idLocalCustodia + "|" + idLocalNegociacao + "|" + idClearing + "|" + ativoBMF.TipoMercado + "|" + cdAtivoBMF;
            }            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCustos_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        if (btnEditCodigoCustos.Text == "" ||
            textCorretagem.Text == "" ||
            textTaxas.Text == "") {
            e.Result = "Os campos precisam ser preenchidos corretamente.";
            return;
        }

        int idCliente = Convert.ToInt32(btnEditCodigoCustos.Text);
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.Status);
        if (!cliente.LoadByPrimaryKey(campos, idCliente)) {
            e.Result = "Cliente " + idCliente.ToString() + " inexistente.";
            return;
        }
        else if (cliente.Status.Value == (byte)StatusCliente.Divulgado) {
            e.Result = "Cliente " + idCliente.ToString() + " está fechado. Processo não pode ser executado.";
            return;
        }

        decimal corretagem = Convert.ToDecimal(textCorretagem.Text);
        decimal taxas = Convert.ToDecimal(textTaxas.Text);
        DateTime data = cliente.DataDia.Value;
        
        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
        List<int> listaIds = new List<int>();
        for (int i = 0; i < keyValuesId.Count; i++)
        {
            int idOperacao = Convert.ToInt32(keyValuesId[i]);
            OperacaoBMF operacaoBMFCheck = new OperacaoBMF();
            operacaoBMFCheck.LoadByPrimaryKey(idOperacao);
            if (operacaoBMFCheck.Data.Value != data)
            {
                e.Result = "Existem operações selecionadas com data diferente da data atual do cliente. Processo não pode ser executado.";
                return;
            }

            listaIds.Add(idOperacao);
        }

        OperacaoBMF operacaoBMF = new OperacaoBMF();
        if (listaIds.Count == 0)
        {
            operacaoBMF.RateiaTaxasTudo(idCliente, data, corretagem, taxas);
        }
        else
        {
            operacaoBMF.RateiaTaxasTudoPorIds(listaIds, corretagem, taxas);
        }
        
        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        ASPxCheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as ASPxCheckBox;
        ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;

        ASPxSpinEdit textTaxaClearingVlFixo = gridCadastro.FindEditFormTemplateControl("textTaxaClearingVlFixo") as ASPxSpinEdit;
        ASPxSpinEdit textEmolumentos = gridCadastro.FindEditFormTemplateControl("textEmolumentos") as ASPxSpinEdit;
        ASPxSpinEdit textRegistro = gridCadastro.FindEditFormTemplateControl("textRegistro") as ASPxSpinEdit;
        ASPxSpinEdit textCorretagem = gridCadastro.FindEditFormTemplateControl("textCorretagem") as ASPxSpinEdit;

        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        //
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        OperacaoBMF operacaoBMF = new OperacaoBMF();
        operacaoBMF.IdCliente = idCliente;
        //
        AtivoBMF ativoBMF = new AtivoBMF();
        //
        string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
        string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();

        decimal taxa = 0;
        if (!string.IsNullOrEmpty(textTaxa.Text))
            taxa = Convert.ToDecimal(textTaxa.Text);

        if (dropAgenteLiquidacao.SelectedIndex > -1)
        {
            operacaoBMF.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
        }
        else
        {
            operacaoBMF.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
        }

        operacaoBMF.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
        operacaoBMF.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
        operacaoBMF.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
        operacaoBMF.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);

        if (dropTrader.SelectedIndex > -1)
            operacaoBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            operacaoBMF.IdTrader = null;
        
        ativoBMF = new AtivoBMF();
        ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
        byte tipoMercado = ativoBMF.TipoMercado.Value;

        operacaoBMF.CdAtivoBMF = cdAtivoBMF;
        operacaoBMF.Serie = serie;
        operacaoBMF.TipoMercado = tipoMercado;
        operacaoBMF.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
        operacaoBMF.Data = Convert.ToDateTime(textData.Text);
        operacaoBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);

        if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro && (ativoBMF.CdAtivoBMF == "DI1" || ativoBMF.CdAtivoBMF == "DDI") && taxa > 0)
        {
            AtivoBMF ativoBMF_Aux = new AtivoBMF();
            if (ativoBMF.CdAtivoBMF == "DI1")
            {
                int numeroDiasVencimento = ativoBMF_Aux.RetornaPrazoDiasUteis(ativoBMF.CdAtivoBMF, ativoBMF.Serie, operacaoBMF.Data.Value);
                decimal fator = 1M + (taxa / 100M);
                decimal expoente = numeroDiasVencimento / 252M;
                decimal fatorDesconto = (decimal)Math.Pow((double)fator, (double)expoente);
                decimal puCalculado = Math.Round(100000M / fatorDesconto, 2);
                operacaoBMF.Pu = puCalculado;
            }
            else if (ativoBMF.CdAtivoBMF == "DDI")
            {
                int numeroDiasVencimento = ativoBMF_Aux.RetornaPrazoDiasCorridos(ativoBMF.CdAtivoBMF, ativoBMF.Serie, operacaoBMF.Data.Value);

                decimal puCalculado = Math.Round(100000M / (1M + (taxa * numeroDiasVencimento / 36000M)), 2);
                operacaoBMF.Pu = puCalculado;
            }

            if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.Compra || operacaoBMF.TipoOperacao == TipoOperacaoBMF.Venda)
                operacaoBMF.TipoOperacao = operacaoBMF.TipoOperacao == TipoOperacaoBMF.Compra ? TipoOperacaoBMF.Venda : TipoOperacaoBMF.Compra;
            else if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade || operacaoBMF.TipoOperacao == TipoOperacaoBMF.VendaDaytrade)
                operacaoBMF.TipoOperacao = operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade ? TipoOperacaoBMF.VendaDaytrade : TipoOperacaoBMF.CompraDaytrade;
            else
                operacaoBMF.TipoOperacao = operacaoBMF.TipoOperacao == TipoOperacaoBMF.Deposito ? TipoOperacaoBMF.Retirada : TipoOperacaoBMF.Deposito;

            operacaoBMF.Valor = operacaoBMF.Quantidade.Value * operacaoBMF.Pu.Value;
            operacaoBMF.Taxa = taxa;
        }
        else
        {
            operacaoBMF.Pu = Convert.ToDecimal(textPU.Text);
            operacaoBMF.Valor = Convert.ToDecimal(textValor.Text);
            operacaoBMF.Taxa = null;
        }

        
        operacaoBMF.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);        
        operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
        operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Manual;

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
        {
            operacaoBMF.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            operacaoBMF.IdCategoriaMovimentacao = null;
        }

        //Trata Custos Informados
        operacaoBMF.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";
        operacaoBMF.IdMoeda = ativoBMF.IdMoeda.Value;

        if (!string.IsNullOrEmpty(textTaxaClearingVlFixo.Text))
            operacaoBMF.TaxaClearingVlFixo = Convert.ToDecimal(textTaxaClearingVlFixo.Text);
        else
            operacaoBMF.TaxaClearingVlFixo = 0;

        if (!string.IsNullOrEmpty(textEmolumentos.Text))
            operacaoBMF.Emolumento = Convert.ToDecimal(textEmolumentos.Text);
        else
            operacaoBMF.Emolumento = 0;

        if (!string.IsNullOrEmpty(textRegistro.Text))
            operacaoBMF.Registro = Convert.ToDecimal(textRegistro.Text);
        else
            operacaoBMF.Registro = 0;

        if (!string.IsNullOrEmpty(textCorretagem.Text))
            operacaoBMF.Corretagem = Convert.ToDecimal(textCorretagem.Text);
        else
            operacaoBMF.Corretagem = 0;


        if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
        {
            operacaoBMF.IdConta = Convert.ToInt32(dropConta.Value);
        }


        operacaoBMF.Save();

        #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        if (DateTime.Compare(cliente.DataDia.Value, operacaoBMF.Data.Value) > 0)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            if (boletoRetroativo.LoadByPrimaryKey(idCliente))
            {
                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoBMF.Data.Value) > 0)
                {
                    boletoRetroativo.DataBoleto = operacaoBMF.Data.Value;
                }
            }
            else
            {
                boletoRetroativo.IdCliente = idCliente;
                boletoRetroativo.DataBoleto = operacaoBMF.Data.Value;
                boletoRetroativo.TipoMercado = (int)TipoMercado.BMF;
            }

            boletoRetroativo.Save();
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoBMF - Operacao: Insert OperacaoBMF: " + operacaoBMF.IdOperacao + UtilitarioWeb.ToString(operacaoBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        /* Atualiza StatusRealTime para executar */
        cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        ASPxCheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as ASPxCheckBox;
        ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;

        ASPxSpinEdit textTaxaClearingVlFixo = gridCadastro.FindEditFormTemplateControl("textTaxaClearingVlFixo") as ASPxSpinEdit;        
        ASPxSpinEdit textEmolumentos = gridCadastro.FindEditFormTemplateControl("textEmolumentos") as ASPxSpinEdit;
        ASPxSpinEdit textRegistro = gridCadastro.FindEditFormTemplateControl("textRegistro") as ASPxSpinEdit;
        ASPxSpinEdit textCorretagem = gridCadastro.FindEditFormTemplateControl("textCorretagem") as ASPxSpinEdit;

        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        OperacaoBMF operacaoBMF = new OperacaoBMF();
        if (operacaoBMF.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoBMF.IdCliente = idCliente;
            AtivoBMF ativoBMF = new AtivoBMF();
            string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
            string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();

            if (ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo)
            {
                operacaoBMF.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
            }
            else
            {
                operacaoBMF.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            }

            operacaoBMF.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            operacaoBMF.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
            operacaoBMF.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
            operacaoBMF.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
            operacaoBMF.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);

            if (dropTrader.SelectedIndex > -1)
                operacaoBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                operacaoBMF.IdTrader = null;

            ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
            byte tipoMercado = ativoBMF.TipoMercado.Value;

            operacaoBMF.CdAtivoBMF = cdAtivoBMF;
            operacaoBMF.Serie = serie;
            operacaoBMF.TipoMercado = tipoMercado;
            operacaoBMF.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);

            decimal pu = Convert.ToDecimal(textPU.Text);
            if (operacaoBMF.Pu.Value != pu)
                operacaoBMF.Taxa = null;

            operacaoBMF.Pu = Convert.ToDecimal(textPU.Text);
            operacaoBMF.Valor = Convert.ToDecimal(textValor.Text);
            operacaoBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);
            operacaoBMF.Data = Convert.ToDateTime(textData.Text);

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                operacaoBMF.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                operacaoBMF.IdCategoriaMovimentacao = null;
            }

            //Trata Custos Informados
            operacaoBMF.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";
            operacaoBMF.IdMoeda = ativoBMF.IdMoeda.Value;

            if (!string.IsNullOrEmpty(textTaxaClearingVlFixo.Text))
                operacaoBMF.TaxaClearingVlFixo = Convert.ToDecimal(textTaxaClearingVlFixo.Text);
            else
                operacaoBMF.TaxaClearingVlFixo = 0;

            if (!string.IsNullOrEmpty(textEmolumentos.Text))
                operacaoBMF.Emolumento = Convert.ToDecimal(textEmolumentos.Text);
            else
                operacaoBMF.Emolumento = 0;

            if (!string.IsNullOrEmpty(textRegistro.Text))
                operacaoBMF.Registro = Convert.ToDecimal(textRegistro.Text);
            else
                operacaoBMF.Registro = 0;

            if (!string.IsNullOrEmpty(textCorretagem.Text))
                operacaoBMF.Corretagem = Convert.ToDecimal(textCorretagem.Text);
            else
                operacaoBMF.Corretagem = 0;

            if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
            {
                operacaoBMF.IdConta = Convert.ToInt32(dropConta.Value);
            }

            operacaoBMF.Save();

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            if (DateTime.Compare(cliente.DataDia.Value, operacaoBMF.Data.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(idCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoBMF.Data.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacaoBMF.Data.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = idCliente;
                    boletoRetroativo.DataBoleto = operacaoBMF.Data.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.BMF;
                }

                boletoRetroativo.Save();
            }
            #endregion

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoBMF - Operacao: Update OperacaoBMF: " + operacaoBMF.IdOperacao + UtilitarioWeb.ToString(operacaoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoBMF operacaoBMF = new OperacaoBMF();
                if (operacaoBMF.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoBMF.IdCliente.Value;

                    //
                    OperacaoBMF operacaoBMFClone = (OperacaoBMF)Utilitario.Clone(operacaoBMF);
                    //

                    operacaoBMF.MarkAsDeleted();
                    operacaoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoBMF - Operacao: Delete OperacaoBMF: " + idOperacao + UtilitarioWeb.ToString(operacaoBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;
            e.Properties["cpTextPeso"] = textPeso.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        e.NewValues["PercentualDesconto"] = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e) {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CdAtivoBMF" }));
            string serie = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Serie" }));

            btnEditAtivoBMF.Text = cdAtivoBMF + "-" + serie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }


    //testa para saber se é deposito ou retirada unicas operações onde é possivel editar a data de operação
    protected void dropTipoOperacao_OnDataBound(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = sender as ASPxComboBox;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;

        //primeiro testa para saber se é valido e possui valor depois testa para saber se é deposito ou retirada unicas operações onde é possivel editar a data de operação.
        if (dropTipoOperacao != null && dropTipoOperacao.Value != null)
        {
            if(dropTipoOperacao.Value.ToString() == "DE" || dropTipoOperacao.Value.ToString() == "RE")
            {
                textDataOperacao.ClientEnabled = true;
            }
            else
            {
                textDataOperacao.ClientEnabled = false;
            }
        }
    }

    protected void textTaxa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
    }

    protected void chkCustosInformados_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int editingRowVisibleIndex = gridCadastro.EditingRowVisibleIndex;
            string calculaDespesas = gridCadastro.GetRowValues(editingRowVisibleIndex, "CalculaDespesas").ToString();

            ASPxCheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as ASPxCheckBox;
            chkCustosInformados.Checked = calculaDespesas.Equals("N") ? true : false;

        }
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
            return;

        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
        {
            e.Collection = new ContaCorrenteCollection();
            return;
        }

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCarteira != null && btnEditCodigoCarteira.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdPessoa);
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                ContaCorrenteCollection coll = new ContaCorrenteCollection();

                coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa.Value) | coll.Query.IdCliente.Equal(idCliente));
                coll.Query.OrderBy(coll.Query.Numero.Ascending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }

        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }
}