﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using Financial.Common;
using Financial.Util;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_TaxaRepasseBMF : CadastroBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBMF = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load ( sender, e );       
    }

    #region Properties

    ASPxButtonEdit _btnEditAtivoBMF;
    String AtivoBMF
    {
        get
        {
            if (null == this._btnEditAtivoBMF)
                this._btnEditAtivoBMF = this.gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            return (_btnEditAtivoBMF != null ? _btnEditAtivoBMF.Text : string.Empty);
        }
        set
        {
            if (null == this._btnEditAtivoBMF)
                this._btnEditAtivoBMF = this.gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            this._btnEditAtivoBMF.Text = value;
        }

    }

    ASPxComboBox _dropTipoMercado;
    ASPxComboBox dropTipoMercado
    {

        get
        {

            if (null == this._dropTipoMercado)
                this._dropTipoMercado = this.gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
            //
            return this._dropTipoMercado;
        }
    }


    ASPxComboBox _dropPerfil;
    ASPxComboBox dropPerfil {

        get {

            if ( null == this._dropPerfil )
                this._dropPerfil = this.gridCadastro.FindEditFormTemplateControl ( "dropPerfil" ) as ASPxComboBox;
            //
            return this._dropPerfil;
        }
    }


    PerfilTaxaClearingBMFCollection _perfis;
    PerfilTaxaClearingBMFCollection getPerfis
    {

        get {

            if ( null == this._perfis ) {
                PerfilTaxaClearingBMFQuery query = new PerfilTaxaClearingBMFQuery ( );
                this._perfis = new PerfilTaxaClearingBMFCollection ( );
                query.OrderBy ( query.Descricao.Ascending );
                this._perfis.Load ( query );
            }
            return this._perfis;
        }
    }

    #endregion

    #region Log
    void Log (string method ) {

        HistoricoLog historicoLog = new HistoricoLog ( );
        historicoLog.InsereHistoricoLog ( DateTime.Now,
                                         DateTime.Now,
                                         string.Format ( "Cadastro de Taxa Clearing  - Operacao: {0}", method ),
                                         HttpContext.Current.User.Identity.Name,
                                         UtilitarioWeb.GetIP ( Request ),   
                                         "",
                                         HistoricoLogOrigem.Outros );
    }

    #endregion

    #region DataSources

    protected void EsDSTaxaRepasseBMF_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e )
    {
        TabelaTaxaClearingBMFQuery query = new TabelaTaxaClearingBMFQuery ( );
        query.OrderBy ( query.TipoMercado.Ascending );
        TabelaTaxaClearingBMFCollection items = new TabelaTaxaClearingBMFCollection ( );
        items.Load ( query );
        e.Collection = items;
    }


    protected void EsDSPerfilTaxaClearingBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilTaxaClearingBMFCollection coll = new PerfilTaxaClearingBMFCollection();
        coll.LoadAll();
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e )
    {
        AtivoBMFCollection coll = new AtivoBMFCollection ( );
        coll.Query.es.Distinct = true;
        coll.Query.Select(coll.Query.CdAtivoBMF, coll.Query.CdAtivoBMF.As("Serie"));
        coll.Query.OrderBy ( coll.Query.CdAtivoBMF.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    #endregion


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }


    protected void textVencimento_Init ( object sender, EventArgs e ) {

        if ( !this.gridCadastro.IsNewRowEditing )
            ( sender as ASPxSpinEdit ).Enabled = true;
    }

    protected void textValorFixoAgenteCustodia_Init ( object sender, EventArgs e ) {

        if ( !this.gridCadastro.IsNewRowEditing )
            ( sender as ASPxSpinEdit ).Enabled = true;
    }

    protected void textValorFixoCorretora_Init ( object sender, EventArgs e ) {

        if ( !this.gridCadastro.IsNewRowEditing )
            ( sender as ASPxSpinEdit ).Enabled = true;
    }

    #region CallBacks

    protected void dropPerfil_OnInit ( object sender, EventArgs e ) {

        ASPxComboBox objDropPerfil = sender as ASPxComboBox;
        //
        foreach ( PerfilTaxaClearingBMF perfil in this.getPerfis )
            objDropPerfil.Items.Add ( new ListEditItem ( perfil.Descricao, perfil.IdPerfil ) );
    }
    
    /// <summary>
    /// Tratatamento de Erros
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        #region Campos obrigatórios
        if ( null == this.dropPerfil.SelectedItem ) {
            e.Result = "Selecione um perfil!";
            return;
        }

        if ( null == this.AtivoBMF ) {
               e.Result = "Selecione um ativo!";
               return;
        }
            
        ASPxSpinEdit vencimento = ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textVencimento" ) );
        ASPxSpinEdit valorFixoAgenteCustodia = ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoAgenteCustodia" ) );
        ASPxSpinEdit valorFixoCorretora = ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoCorretora" ) );

        if ( string.IsNullOrEmpty ( vencimento.Text ) ) {
            e.Result = "O campo Vencimento é obrigatório!";
            return;
        }

        if ( string.IsNullOrEmpty ( valorFixoAgenteCustodia.Text ) ) {
            e.Result = "O campo Valor Fixo Ag Custódia é obrigatório!";
            return;
        }

        if ( string.IsNullOrEmpty ( valorFixoCorretora.Text ) ) {
            e.Result = "O campo Valor Fixo Corretora é obrigatório!";
            return;
        }
       
        #endregion

        if ( this.gridCadastro.IsNewRowEditing ) 
        {

            TabelaTaxaClearingBMFCollection result = new TabelaTaxaClearingBMFCollection ( );
            result.Query.Where(result.Query.CdAtivoBMF == this.AtivoBMF &
                               result.Query.IdPerfilTaxaClearingBMF == Convert.ToInt32(this._dropPerfil.SelectedItem.Value) &
                               result.Query.FaixaVencimento == int.Parse(vencimento.Text));

            if (dropTipoMercado.SelectedIndex > 0)
                result.Query.Where(result.Query.TipoMercado.Equal(Convert.ToByte(dropTipoMercado.SelectedItem.Value)));
            else
                result.Query.Where(result.Query.TipoMercado.IsNull());

            if (result.Query.Load())
            {
                PerfilTaxaClearingBMF perfil = new PerfilTaxaClearingBMF();
                perfil.LoadByPrimaryKey(result[0].IdPerfilTaxaClearingBMF.Value);

                string mensagem = string.Format("Não é permitido cadastrar o mesmo PERFIL:{2}, VENCIMENTO:{0} para o ATIVO:{1}", vencimento.Text, this.AtivoBMF, perfil.Descricao);

                if (dropTipoMercado.SelectedIndex > 0)
                    mensagem = string.Format(mensagem + " e MERCADO:" + TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(dropTipoMercado.SelectedItem.Value)));
                else
                    mensagem = string.Format(mensagem + " sem mercado específico ");

                e.Result = mensagem;
                return;
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Salvar ( null );
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    void Salvar ( TabelaTaxaClearingBMF entity )
    {

        string method = null == entity ? "INSERT" : "UPDATED";
        string codAtivo = string.Empty;
        string codSerie = string.Empty;
        if ( null == entity )
            entity = new TabelaTaxaClearingBMF ( );
        //


        entity.CdAtivoBMF = this.AtivoBMF;

        if (dropTipoMercado.SelectedIndex > 0)
            entity.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
        else
            entity.TipoMercado = null;
        entity.IdPerfilTaxaClearingBMF = int.Parse ( this.dropPerfil.SelectedItem.Value.ToString ( ) );
        entity.FaixaVencimento = int.Parse ( ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textVencimento" ) ).Text );
        entity.ValorFixoAgenteCustodia = decimal.Parse ( ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoAgenteCustodia" ) ).Text );
        entity.ValorFixoCorretora = decimal.Parse ( ( ( ASPxSpinEdit )this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoCorretora" ) ).Text );
        entity.UltimoVencimento = ( ( ASPxCheckBox )this.gridCadastro.FindEditFormTemplateControl ( "blnUltimoVencimento" ) ).Checked ? "1" : "0";
        entity.Save ( );
        //
        this.Log ( method );
    }
  
    #endregion

    #region Modo Edicao

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
   

    #endregion

    #region Grid

    protected void gridCadastro_RowUpdating ( object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e ) {

        String strId = Convert.ToString ( gridCadastro.GetRowValues ( gridCadastro.EditingRowVisibleIndex, new string[ ] { "IdTabela" } ) );
        int Id = int.Parse ( strId );

        TabelaTaxaClearingBMF entity = new TabelaTaxaClearingBMF ( );
        if ( entity.LoadByPrimaryKey ( Id ) )
            this.Salvar ( ( TabelaTaxaClearingBMF )Utilitario.Clone ( entity ) );
        e.Cancel = true;
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    protected void gridCadastro_RowInserting ( object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e ) {

        this.Salvar ( null );
        e.Cancel = true;
        this.gridCadastro.CancelEdit ( );
        this.gridCadastro.DataBind ( );
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        //const string Delete = "btnDelete";
        //
        this.RemoverPerfilSelecionado ( );
        //
        this.gridCadastro.DataBind ( );
        //
        this.gridCadastro.Selection.UnselectAll ( );
        //
        this.gridCadastro.CancelEdit ( );
    }


    void RemoverPerfilSelecionado ( ) {

        List<object> entityIds = this.gridCadastro.GetSelectedFieldValues ( TabelaTaxaClearingBMFMetadata.ColumnNames.IdTabela );
        //
        entityIds.ForEach ( delegate ( object objId )
        {
            int Id = Convert.ToInt32 ( objId.ToString ( ) );
            TabelaTaxaClearingBMF entity = new TabelaTaxaClearingBMF ( );
            if ( entity.LoadByPrimaryKey ( Id ) ) {
                TabelaTaxaClearingBMF cloneObj = ( TabelaTaxaClearingBMF )Utilitario.Clone ( entity );
                cloneObj.MarkAsDeleted ( );
                cloneObj.Save ( );
                this.Log ( "DELETED" );
            }
        } );
    }

  
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;

        if ( gridView.IsEditing ) {

            ASPxComboBox perfil = this.gridCadastro.FindEditFormTemplateControl ( "dropPerfil" ) as ASPxComboBox;
            e.Properties["cpPerfil"] = perfil.ClientID;

            ASPxSpinEdit vencimento = this.gridCadastro.FindEditFormTemplateControl ( "textVencimento" ) as ASPxSpinEdit;
            e.Properties["cpVencimento"] = vencimento.ClientID;

            ASPxSpinEdit valorFixoAgenteCustodia = this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoAgenteCustodia" ) as ASPxSpinEdit;
            e.Properties["cpValorFixoAgenteCustodia"] = valorFixoAgenteCustodia.ClientID;

            ASPxSpinEdit valorFixoCorretora = this.gridCadastro.FindEditFormTemplateControl ( "textValorFixoCorretora" ) as ASPxSpinEdit;
            e.Properties["cpValorFixoCorretora"] = valorFixoCorretora.ClientID;

        }
    }

    new protected void panelEdicao_Load ( object sender, EventArgs e )
    {
        if ( gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString ( Session["FormLoad"] ) != "S" )
        {
            Session["FormLoad"] = "S";
            //TipoMercado, Peso
            string cdAtivoBMF = gridCadastro.GetRowValues ( gridCadastro.EditingRowVisibleIndex, "CdAtivoBMF" ).ToString ( );
            //

        }
    }

    protected void gridCadastro_HtmlRowPrepared ( object sender, ASPxGridViewTableRowEventArgs e )
    {
        if ( gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing )
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl ( "btnEditAtivoBMF" ) as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString ( gridCadastro.GetRowValues ( gridCadastro.EditingRowVisibleIndex, new string[ ] { "CdAtivoBMF" } ) );

            btnEditAtivoBMF.Text = cdAtivoBMF;

            this.AtivoBMF = cdAtivoBMF;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        base.gridCadastro_PreRender(sender, e);
        ASPxGridView grid = ( sender as ASPxGridView );
    }

    #endregion
   
}