﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrdemBMF.aspx.cs" Inherits="CadastrosBasicos_OrdemBMF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">


                       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    function OnGetDataAtivoBMF(data) {
        btnEditAtivoBMF.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
        popupAtivoBMF.HideWindow();
        btnEditAtivoBMF.Focus();
    }    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {            
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }                     
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            textQuantidade.SetValue(0);
            textTaxa.SetValue(0);
            textPU.SetValue(0);
            textValor.SetValue(0);
            alert('Operação feita com sucesso.');
            textQuantidade.Focus();
        }        
        "/>
    </dxcb:ASPxCallback> 
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                            
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                  
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {                                        
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                if (gridCadastro.cp_EditVisibleIndex == 'new')
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textData);                                
                }
                else
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
                }
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0];
            
            if (e.result == '' && btnEditAtivoBMF.GetValue() != null)
            {
                popupMensagemAtivoBMF.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBMF.ShowAtElementByID(btnEditAtivoBMF.name);
                btnEditAtivoBMF.SetValue(''); 
                btnEditAtivoBMF.Focus();                 
            }
                                    
            var textPeso = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textPeso');
            
            if (textPeso != null)
            {
                if (resultSplit[1] != null)            
                {
                    textPeso.value = resultSplit[1];
                }
                else
                {
                    textPeso.value = '';                
                }
                
                if (resultSplit[2] != null && resultSplit[3] != null)            
                {
                    if((resultSplit[2] == 'DI1' || resultSplit[2] == 'DDI') && resultSplit[3] == '2')
                    {                    
                        textTaxa.SetEnabled(true);                                       
                    }
                    else                    
                    { 
                        textTaxa.SetEnabled(false);                    
                        textPU.SetEnabled(true);                     
                        textValor.SetEnabled(true); 
                        textPU.SetValue(null);
                        textValor.SetValue(null);
                    }                           
                }
            }
            
            if (resultSplit[4] != null)
            {
                var idLocalCustodia = resultSplit[4];
                dropLocalCustodia.SetValue(idLocalCustodia);
            }
            
            if (resultSplit[5] != null)
            {
                var idLocalNegociacao = resultSplit[5];
                dropLocalNegociacao.SetValue(idLocalNegociacao); 
            } 
            
            if (resultSplit[6] != null)
            {            
                var idClearing = resultSplit[6];                
                dropClearing.SetValue(idClearing);
            }   
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackCustos" runat="server" OnCallback="callBackCustos_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            alert(e.result);
            btnEditCodigoCustos.Focus();                    
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            alert(e.result);
            gridCadastro.PerformCallback('btnRefresh'); 
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ordens de BMF"></asp:Label>
    </div>
        
    <div id="mainContent">
       
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2">
                                <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                            </td>  
                            
                            <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                      
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo/Série:"></asp:Label>
                            </td>
                                           
                            <td colspan="2">
                                <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textCurto"></asp:TextBox>
                                <asp:TextBox ID="textSerie" runat="server" CssClass="textCurto"></asp:TextBox>
                            </td>    
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelTraderFiltro" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                            </td>
                            
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropTraderFiltro" runat="server" ClientInstanceName="dropTraderFiltro"
                                                    DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"   
                                                    ShowShadow="false" DropDownStyle="DropDown"
                                                    CssClass="dropDownList" TextField="Nome" ValueField="IdTrader">
                                  <ClientSideEvents LostFocus="function(s, e) { 
                                                                    if(s.GetSelectedIndex() == -1)
                                                                        s.SetText(null); } "/>                  
                                </dxe:ASPxComboBox>         
                            </td>
                        </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>          
            
            <dxpc:ASPxPopupControl ID="popupCustos" AllowDragging="true" PopupElementID="popupCustos" EnableClientSideAPI="True"
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Custos informados" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCustos" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCustos" MaxLength="10" NumberType="Integer">
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Corretagem:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="textCorretagem" runat="server" CssClass="textValor" 
                                            ClientInstanceName="textCorretagem" MaxLength="10" NumberType="Float" DecimalPlaces="2">
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Taxas:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="textTaxas" runat="server" CssClass="textValor" 
                                            ClientInstanceName="textTaxas" MaxLength="10" NumberType="Float" DecimalPlaces="2" ClientVisible="false">
                                </dxe:ASPxSpinEdit>
                            </td>
                        </tr>    
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKCustos" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="callBackCustos.SendCallback(); return false;"><asp:Literal ID="Literal14" runat="server" Text="Processar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>    
            
            <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote" EnableClientSideAPI="True"
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Alteração em Lote" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelAgenteLote" runat="server" CssClass="labelNormal" Text="Liquidante:"> </asp:Label>
                            </td>                
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropAgenteLote" runat="server" ClientInstanceName="dropAgenteLote"
                                                    DataSourceID="EsDSAgenteMercadoLiquidacao" IncrementalFilteringMode="Contains"   
                                                    ShowShadow="false" DropDownStyle="DropDown"
                                                    CssClass="dropDownList" TextField="Nome" ValueField="IdAgente">
                                  <ClientSideEvents LostFocus="function(s, e) { 
                                                                    if(s.GetSelectedIndex() == -1)
                                                                        s.SetText(null); } "/>                  
                                </dxe:ASPxComboBox>         
                            </td>
                            
                            <td class="td_Label">
                               <asp:Label ID="labelTraderLote" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                            </td>
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropTraderLote" runat="server" ClientInstanceName="dropTraderLote"
                                                    DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"   
                                                    ShowShadow="false" DropDownStyle="DropDown"
                                                    CssClass="dropDownListLongo" TextField="Nome" ValueField="IdTrader">
                                  <ClientSideEvents LostFocus="function(s, e) { 
                                                                    if(s.GetSelectedIndex() == -1)
                                                                        s.SetText(null); } "/>                  
                                </dxe:ASPxComboBox>         
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label7" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="%Desconto:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="textPercentualLote" runat="server" CssClass="textCurto" 
                                            ClientInstanceName="textTaxas" MaxLength="10" NumberType="Float" DecimalPlaces="2">
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Informa Custos:"></asp:Label>
                            </td>
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropInformaCustos" runat="server" 
                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                    <Items>
                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                    <dxe:ListEditItem Value="N" Text="Não" />                                        
                                    </Items>                        
                                </dxe:ASPxComboBox>
                            </td>   
                        </tr>    
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="callBackLote.SendCallback(); return false;"><asp:Literal ID="Literal15" runat="server" Text="Processar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>  
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnCustos" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnEdit" OnClientClick="popupCustos.ShowWindow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Custos"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnLote" OnClientClick="popupLote.ShowWindow(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Altera em Lote"/><div></div></asp:LinkButton>           
               <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnCustomFields" OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Mais Campos"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdOrdem" DataSourceID="EsDSOrdemBMF"                        
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"     
                        OnInitNewRow="gridCadastro_InitNewRow"
                        OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"         
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"               
                        >     
                    
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOrdem" Caption="Id.Ordem" VisibleIndex="2" Width="7%" CellStyle-HorizontalAlign="left">                    
                        </dxwgv:GridViewDataColumn> 
                        <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id.Cliente" VisibleIndex="2" Width="7%" CellStyle-HorizontalAlign="left">                    
                        </dxwgv:GridViewDataColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="14%">                    
                        </dxwgv:GridViewDataColumn>
                        
                        <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="4" Width="10%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Corretora" FieldName="IdAgenteCorretora" VisibleIndex="5" Width="18%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Trader" FieldName="IdTrader" VisibleIndex="6" Width="10%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSTrader" TextField="Nome" ValueField="IdTrader">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="CdAtivoBMF" Caption="Código" VisibleIndex="7" Width="5%">                    
                        </dxwgv:GridViewDataColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="Serie" Caption="Série" VisibleIndex="8" Width="5%">                    
                        </dxwgv:GridViewDataColumn>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOrdem" Caption="Tipo" VisibleIndex="9" Width="8%">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="C " Text="<div title='Compra'>Compra</div>" />
                            <dxe:ListEditItem Value="V " Text="<div title='Venda'>Venda</div>" />                            
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="10" Width="8%"                                 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>        
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PU" VisibleIndex="11" Width="10%"
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOrdem" Visible="false" ShowInCustomizationForm="false">                    
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewDataColumn FieldName="Valor" Visible="false" ShowInCustomizationForm="false">                    
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewDataColumn FieldName="CalculaDespesas" Visible="false" ShowInCustomizationForm="false">                    
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewDataColumn FieldName="IdAgenteLiquidacao" Visible="false" ShowInCustomizationForm="false">                    
                        </dxwgv:GridViewDataColumn>                        
                        <dxwgv:GridViewDataColumn FieldName="PercentualDesconto" Caption="%Desconto" Visible="false" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        </dxwgv:GridViewDataColumn>
                    </Columns>
                    
                    <Templates>
                    <EditForm>
                        
                        <asp:TextBox ID="textPeso" runat="server" CssClass="hiddenField" />
                        
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm">                            
                            
                            <table>
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                    </td>        

                                    <td>                                        
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>'                                                    
                                                    MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>

                                    <td colspan="2">
                                        <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                    </td>
                                </tr>
                           
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelAgenteCorretora" runat="server" CssClass="labelRequired" Text="Corretora:"> </asp:Label>
                                    </td>                
                                    <td colspan="3">
                                        <dxe:ASPxComboBox ID="dropAgenteCorretora" runat="server" ClientInstanceName="dropAgenteCorretora"
                                                            DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="Contains" 
                                                            ShowShadow="false" DropDownStyle="DropDown"
                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdAgente"
                                                            Text='<%#Eval("IdAgenteCorretora")%>'>
                                                <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } "/>
                                        </dxe:ASPxComboBox>         
                                    </td>
                                </tr>  
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelAgenteLiquidacao" runat="server" CssClass="labelRequired" Text="Liquidante:"> </asp:Label>
                                    </td>                
                                    <td colspan="3">
                                        <dxe:ASPxComboBox ID="dropAgenteLiquidacao" runat="server" ClientInstanceName="dropAgenteLiquidacao"
                                                            DataSourceID="EsDSAgenteMercadoLiquidacao" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown"
                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdAgente"
                                                            Text='<%#Eval("IdAgenteLiquidacao")%>'>
                                          <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } "/>                  
                                        </dxe:ASPxComboBox>         
                                    </td>
                                </tr>  
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="% Desconto:"></asp:Label>
                                    </td>        
                                    
                                    <td colspan="3">                                                                                                                
                                        <dxe:ASPxSpinEdit ID="textPercentual" runat="server" CssClass="textValor_5" 
                                                    ClientInstanceName="textTaxas" MaxLength="10" NumberType="Float" DecimalPlaces="2"
                                                    Text='<%#Eval("PercentualDesconto")%>'>
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                          
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
                                    </td>                                     
                                    <td>                                    
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>'/>                                    
                                    </td> 
                                
                                    <td class="td_Label">
                                        <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                            DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown"
                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdTrader"
                                                            Text='<%#Eval("IdTrader")%>'>
                                          <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } "/>                  
                                        </dxe:ASPxComboBox>         
                                    </td>
                                </tr>   
                            
                                <tr>                                        
                                    <td  class="td_Label">
                                        <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                    </td>
                                    <td>
                                        <dxe:ASPxButtonEdit ID="btnEditAtivoBMF" runat="server" CssClass="textAtivoBMF" MaxLength="20" 
                                                            ClientInstanceName="btnEditAtivoBMF">            
                                        <Buttons>
                                            <dxe:EditButton></dxe:EditButton>                
                                        </Buttons>           
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupAtivoBMF.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemAtivoBMF.HideWindow();
                                                                        if (btnEditAtivoBMF.GetValue() != null)
                                                                        {
                                                                            ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
                                                                        }
                                                                        }"
                                        />                    
                                        </dxe:ASPxButtonEdit>
                                    </td>
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOrdem" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                    </td>                    
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTipoOrdem" runat="server" 
                                                            ShowShadow="false" DropDownStyle="DropDown" IncrementalFilteringMode="Contains" 
                                                            CssClass="dropDownListCurto" Text='<%#Eval("TipoOrdem")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="C " Text="Compra" />
                                        <dxe:ListEditItem Value="V " Text="Venda" />                                        
                                        </Items>             
                                        <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } "/>                                    
                                        </dxe:ASPxComboBox>             
                                    </td>                                                                                       
                                </tr>
                                
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade" DisplayFormatString="N"
                                                                         MaxLength="12" NumberType="Integer" Text="<%#Bind('Quantidade')%>">            
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                         }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelPU" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textPU" runat="server" CssClass="textValor_5" ClientInstanceName="textPU" DisplayFormatString="N"
                                                                         MaxLength="12" NumberType="Float" DecimalPlaces="8" Text="<%#Bind('PU')%>">            
                                        
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {                                                                        
                                                                        if(!(s.GetValue() === '' || s.GetValue() == null))
                                                                        {
                                                                            textTaxa.SetEnabled(false); 
                                                                            textTaxa.SetValue(0); 
                                                                        }
                                                                        var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                       }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>  
                                    <td class="td_Label">
                                        <asp:Label ID="labelTaxa" runat="server" CssClass="labelRequired" Text="Taxa:"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                            <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" Style="float: left;" OnLoad="textTaxa_Load"
                                                ClientInstanceName="textTaxa" MaxLength="25" NumberType="Float" DecimalPlaces="16" ClientEnabled="false"
                                                Text='<%#Eval("Taxa")%>'>
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {                                              
                                                                        if(s.GetValue() === '' || s.GetValue() == null)                                                                                                                   
                                                                        {                                                                        
                                                                            textPU.SetEnabled(true); 
                                                                            textValor.SetEnabled(true);
                                                                        }
                                                                        else
                                                                        {
                                                                            textPU.SetEnabled(false); 
                                                                            textValor.SetEnabled(false);
                                                                            textPU.SetValue(0); 
                                                                            textValor.SetValue(0); 
                                                                        }
                                                                                                                                                        
                                                                       }" />                                                
                                            </dxe:ASPxSpinEdit>
                                     </td>   
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor" DisplayFormatString="N"
                                                                         MaxLength="12" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('Valor')%>">            
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                         }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>     
                                </tr>
                                <tr>
                                  <td class="td_Label">
                                      <asp:Label ID="lblLocalCustodia" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalCustodia"
                                          Text="Local Custódia:">
                                      </asp:Label>
                                  </td>
                                  <td colspan="3">
                                      <dxe:ASPxComboBox ID="dropLocalCustodia" runat="server" DataSourceID="EsDSLocalCustodia" ClientInstanceName="dropLocalCustodia"
                                          ValueField="IdLocalCustodia" TextField="Descricao" IncrementalFilteringMode="StartsWith" ShowShadow="false" DropDownStyle="DropDown"
                                          CssClass="dropDownListLongo" Text='<%#Eval("IdLocalCustodia")%>'> 
                                      </dxe:ASPxComboBox>
                                  </td>                                    
                               </tr> 
							     <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="lblClearing" runat="server" CssClass="labelRequired" AssociatedControlID="dropClearing"
                                            Text="Clearing:">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxComboBox ID="dropClearing" runat="server" DataSourceID="EsDSClearing" ClientInstanceName = "dropClearing"
                                            ValueField="IdClearing" TextField="Descricao" IncrementalFilteringMode="StartsWith" ShowShadow="false" DropDownStyle="DropDown"
                                            CssClass="dropDownListLongo" Text='<%#Eval("IdClearing")%>'> 
                                        </dxe:ASPxComboBox>
                                    </td>                                    
                                 </tr>
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>                                 
                                  </tr>		
                                      <td class="td_Label">
                                          <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                              Text="Local Negociação:">
                                          </asp:Label>
                                      </td>
                                      <td colspan="3">
                                          <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao" ClientInstanceName="dropLocalNegociacao"
                                              ValueField="IdLocalNegociacao" TextField="Descricao" IncrementalFilteringMode="StartsWith" ShowShadow="false" DropDownStyle="DropDown"
                                              CssClass="dropDownListLongo" Text='<%#Eval("IdLocalNegociacao")%>'> 
                                          </dxe:ASPxComboBox>
                                      </td>
                                </tr>                                
                                <tr>
                                   <td class="td_Label">
                                       <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Mov:"> </asp:Label>
                                   </td>                
                                   <td colspan="3">
                                       <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                           DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                           ShowShadow="false" DropDownStyle="DropDown"
                                                           CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                           Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                       </dxe:ASPxComboBox>         
                                   </td> 
                                </tr>                                    
                                <tr> 
                                    <td class="td_Label">
                                        <asp:Label ID="labelCheck" runat="server" CssClass="labelNormal" Text="Informa custos:"></asp:Label>
                                    </td>
                                    
                                    <td colspan="3">
                                        <asp:CheckBox ID="chkCustosInformados" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="lblTaxaClearingVlFixo" runat="server" CssClass="labelNormal" Text="Taxa Clearing Vl.Fixo:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textTaxaClearingVlFixo" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaClearingVlFixo" DisplayFormatString="N"
                                                                         MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("TaxaClearingVlFixo")%>'>             
                                        </dxe:ASPxSpinEdit>
                                    </td>        
                                    <td class="td_Label">
                                        <asp:Label ID="lblCorretagem" runat="server" CssClass="labelNormal" Text="Corretagem:"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <dxe:ASPxSpinEdit ID="textCorretagem" runat="server" CssClass="textValor_5" ClientInstanceName="textCorretagem" DisplayFormatString="N"
                                                                         MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Corretagem")%>'>            
                                        </dxe:ASPxSpinEdit>
                                    </td>                                     
                                </tr>								
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="lblEmolumentos" runat="server" CssClass="labelNormal" Text="Emolumentos:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textEmolumentos" runat="server" CssClass="textValor_5" ClientInstanceName="textEmolumentos" DisplayFormatString="N"
                                                                         MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Emolumento")%>'>             
                                        </dxe:ASPxSpinEdit>
                                    </td>     
                                    <td class="td_Label">
                                        <asp:Label ID="lblRegistro" runat="server" CssClass="labelNormal" Text="Registro:"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <dxe:ASPxSpinEdit ID="textRegistro" runat="server" CssClass="textValor_5" ClientInstanceName="textRegistro" DisplayFormatString="N"
                                                                         MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Registro")%>'>            
                                        </dxe:ASPxSpinEdit>
                                    </td>     
                                </tr>                                   
                            </table>
                            
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK+"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="550px" />
                    <SettingsBehavior EnableCustomizationWindow ="true" />
                    <SettingsText CustomizationWindowCaption="Lista de campos" />
                    
                </dxwgv:ASPxGridView>            
            </div>                           
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"
    LeftMargin = "50"
    RightMargin = "50"
    ></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSOrdemBMF" runat="server" OnesSelect="EsDSOrdemBMF_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />    
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />            
    <cc1:esDataSource ID="EsDSAgenteMercadoLiquidacao" runat="server" OnesSelect="EsDSAgenteMercadoLiquidacao_esSelect" />        
    <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />        
    <cc1:esDataSource ID="EsDSLocalCustodia" runat="server" OnesSelect="EsDSLocalCustodia_esSelect" />
    <cc1:esDataSource ID="EsDSClearing" runat="server" OnesSelect="EsDSClearing_esSelect" />
    <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />	
    <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
    </form>
</body>
</html>