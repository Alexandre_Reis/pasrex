﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;

using System.Collections;
using System.Collections.Generic;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using DevExpress.Web;

using Financial.Web.Common;
using Financial.BMF;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Util;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_TabelaCorretagemBMF : CadastroBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {        
        base.Page_Load(sender, e);
    }

    #region DataSources
    /// <summary>
    /// Consulta para Grid Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaCorretagemBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaCorretagemBMFQuery t = new TabelaCorretagemBMFQuery("T");
        PerfilCorretagemBMFQuery p = new PerfilCorretagemBMFQuery("P");
        AgenteMercadoQuery a = new AgenteMercadoQuery("A");
        ClienteQuery c = new ClienteQuery("C");

        t.Select(t, 
                 p.CdAtivoBMF, p.GrupoAtivo, p.TipoMercado, p.TipoCorretagem, p.TipoCorretagemDT, p.IdAgente,
                 c.IdCliente, c.Apelido,
                 a.Nome);
        //
        t.InnerJoin(p).On(t.IdPerfilCorretagem == p.IdPerfilCorretagem &&
                          t.DataReferencia == p.DataReferencia);
        t.InnerJoin(c).On(c.IdCliente == p.IdCliente);
        t.InnerJoin(a).On(p.IdAgente == a.IdAgente);                
        //
        t.OrderBy(t.DataReferencia.Ascending,t.FaixaValor.Ascending);
        //
        TabelaCorretagemBMFCollection coll = new TabelaCorretagemBMFCollection();
        //
        coll.Load(t);
        //       
        e.Collection = coll;
    }

    /// <summary>
    /// PopUp Perfil Corretagem
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfilCorretagemBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PerfilCorretagemBMFQuery perfilCorretagemBMFQuery = new PerfilCorretagemBMFQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Q");

        perfilCorretagemBMFQuery.Select(perfilCorretagemBMFQuery, clienteQuery.Apelido.As("Apelido"));
        //
        perfilCorretagemBMFQuery.InnerJoin(clienteQuery).On(perfilCorretagemBMFQuery.IdCliente == clienteQuery.IdCliente);
        perfilCorretagemBMFQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        perfilCorretagemBMFQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);
        //
        perfilCorretagemBMFQuery.OrderBy(perfilCorretagemBMFQuery.IdCliente.Ascending,
                                         perfilCorretagemBMFQuery.IdAgente.Ascending,
                                         perfilCorretagemBMFQuery.DataReferencia.Ascending,
                                         perfilCorretagemBMFQuery.TipoMercado.Ascending,
                                         perfilCorretagemBMFQuery.CdAtivoBMF.Ascending);

        PerfilCorretagemBMFCollection coll = new PerfilCorretagemBMFCollection();
        coll.Load(perfilCorretagemBMFQuery);
        
        e.Collection = coll;
    }

    /// <summary>
    /// PopUp Perfil Corretagem para linkar a Corretora
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.IdAgente.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }    
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    #region CallBacks
    /// <summary>
    /// Tratatamento de Erros
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit idPerfilCorretagem = gridCadastro.FindEditFormTemplateControl("btnEditPerfilCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit faixaVencimento = gridCadastro.FindEditFormTemplateControl("textFaixaVencimento") as ASPxSpinEdit;
        ASPxSpinEdit faixaValor = gridCadastro.FindEditFormTemplateControl("textFaixaValor") as ASPxSpinEdit;
        //
        ASPxSpinEdit taxaCorretagem = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit taxaCorretagemDT = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagemDT") as ASPxSpinEdit;
        ASPxSpinEdit taxaVencimento = gridCadastro.FindEditFormTemplateControl ( "textTaxaVencimento" ) as ASPxSpinEdit;
        //        
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
                    textDataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor, taxaCorretagem, taxaCorretagemDT, taxaVencimento} );

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        // Se estiver Incluindo
        if (gridCadastro.IsNewRowEditing) {
            TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
            //
            if (tabelaCorretagemBMF.LoadByPrimaryKey(Convert.ToDateTime(textDataReferencia.Text),
                                                     Convert.ToInt32(idPerfilCorretagem.Text),
                                                     Convert.ToInt32(faixaVencimento.Text),
                                                     Convert.ToDecimal(faixaValor.Text))) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackClone_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditPerfilCorretagem = gridCadastro.FindEditFormTemplateControl("btnEditPerfilCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit textIdCliente = gridCadastro.FindEditFormTemplateControl("textIdCliente") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textFaixaVencimento = gridCadastro.FindEditFormTemplateControl("textFaixaVencimento") as ASPxSpinEdit;
        ASPxSpinEdit textFaixaValor = gridCadastro.FindEditFormTemplateControl("textFaixaValor") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCorretagem = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCorretagemDT = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagemDT") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaVencimento = gridCadastro.FindEditFormTemplateControl ( "textTaxaVencimento" ) as ASPxSpinEdit;
        ASPxCheckBox blnexistsFaixaVencimento = gridCadastro.FindEditFormTemplateControl("blnFaixaVencimentoPosterior") as ASPxCheckBox;

        int idPerfilCorretagem = Convert.ToInt32(btnEditPerfilCorretagem.Text);
        int idClienteBase = Convert.ToInt32(textIdCliente.Text);
        DateTime dataReferencia = Convert.ToDateTime(textData.Text);
        int faixaVencimento = Convert.ToInt32(textFaixaVencimento.Text);
        decimal faixaValor = Convert.ToDecimal(textFaixaValor.Text);
        decimal taxaCorretagem = Convert.ToDecimal(textTaxaCorretagem.Text);
        decimal taxaCorretagemDT = Convert.ToDecimal(textTaxaCorretagemDT.Text);
        decimal taxaVencimento = Convert.ToDecimal ( textTaxaVencimento.Text );
        string existsFaixaVencimento = "";
        if (null != blnexistsFaixaVencimento)
            existsFaixaVencimento = blnexistsFaixaVencimento.Checked ? "S" : "N";

        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
        clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                      clienteCollection.Query.IdCliente.NotEqual(idClienteBase));
        clienteCollection.Query.Load();

        foreach (Cliente cliente in clienteCollection)
        {
            int idCliente = cliente.IdCliente.Value;

            PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();
            perfilCorretagemBMF.LoadByPrimaryKey(idPerfilCorretagem, dataReferencia);

            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();
            perfilCorretagemBMFCollection.Query.Where(perfilCorretagemBMFCollection.Query.CdAtivoBMF.Equal(perfilCorretagemBMF.CdAtivoBMF),
                                                      perfilCorretagemBMFCollection.Query.DataReferencia.Equal(dataReferencia),
                                                      perfilCorretagemBMFCollection.Query.GrupoAtivo.Equal(perfilCorretagemBMF.GrupoAtivo),
                                                      perfilCorretagemBMFCollection.Query.IdAgente.Equal(perfilCorretagemBMF.IdAgente.Value),
                                                      perfilCorretagemBMFCollection.Query.IdCliente.Equal(idCliente),
                                                      perfilCorretagemBMFCollection.Query.TipoCorretagem.Equal(perfilCorretagemBMF.TipoCorretagem.Value),
                                                      perfilCorretagemBMFCollection.Query.TipoCorretagemDT.Equal(perfilCorretagemBMF.TipoCorretagemDT.Value));

            if (perfilCorretagemBMF.TipoMercado.HasValue)
            {
                perfilCorretagemBMFCollection.Query.Where(perfilCorretagemBMFCollection.Query.TipoMercado.Equal(perfilCorretagemBMF.TipoMercado.Value));
            }
            perfilCorretagemBMFCollection.Query.Load();

            int idPerfilCorretagemNovo = 0;
            if (perfilCorretagemBMFCollection.Count != 0)
            {
                idPerfilCorretagemNovo = perfilCorretagemBMFCollection[0].IdPerfilCorretagem.Value;
            }
            else
            {
                PerfilCorretagemBMF perfilCorretagemBMFNovo = new PerfilCorretagemBMF();
                perfilCorretagemBMFNovo.CdAtivoBMF = perfilCorretagemBMF.CdAtivoBMF;
                perfilCorretagemBMFNovo.DataReferencia = dataReferencia;
                perfilCorretagemBMFNovo.GrupoAtivo = perfilCorretagemBMF.GrupoAtivo;
                perfilCorretagemBMFNovo.IdAgente = perfilCorretagemBMF.IdAgente.Value;
                perfilCorretagemBMFNovo.IdCliente = idCliente;
                perfilCorretagemBMFNovo.TipoCorretagem = perfilCorretagemBMF.TipoCorretagem.Value;
                perfilCorretagemBMFNovo.TipoCorretagemDT = perfilCorretagemBMF.TipoCorretagemDT.Value;
                perfilCorretagemBMFNovo.TipoMercado = perfilCorretagemBMF.TipoMercado;
                perfilCorretagemBMFNovo.Save();

                idPerfilCorretagemNovo = perfilCorretagemBMFNovo.IdPerfilCorretagem.Value;
            }


            TabelaCorretagemBMFCollection tabelaCorretagemBMFCollection = new TabelaCorretagemBMFCollection();
            tabelaCorretagemBMFCollection.Query.Where(tabelaCorretagemBMFCollection.Query.DataReferencia.Equal(dataReferencia),
                                                      tabelaCorretagemBMFCollection.Query.IdPerfilCorretagem.Equal(idPerfilCorretagemNovo),
                                                      tabelaCorretagemBMFCollection.Query.FaixaValor.Equal(faixaValor),
                                                      tabelaCorretagemBMFCollection.Query.FaixaVencimento.Equal(faixaVencimento));
            tabelaCorretagemBMFCollection.Query.Load();

            if (tabelaCorretagemBMFCollection.Count == 0)
            {
                TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
                tabelaCorretagemBMF.DataReferencia = dataReferencia;
                tabelaCorretagemBMF.FaixaValor = faixaValor;
                tabelaCorretagemBMF.FaixaVencimento = faixaVencimento;
                tabelaCorretagemBMF.IdPerfilCorretagem = idPerfilCorretagemNovo;
                tabelaCorretagemBMF.TaxaCorretagem = taxaCorretagem;
                tabelaCorretagemBMF.TaxaCorretagemDT = taxaCorretagemDT;
                tabelaCorretagemBMF.TaxaVencimento = taxaVencimento;
                tabelaCorretagemBMF.FaixaVencimentoValidoPosteriores = existsFaixaVencimento;

                tabelaCorretagemBMF.Save();
            }
        }
    }
    #endregion

    #region Modo Edicao

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditPerfilCorretagem_OnInit(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).ClientEnabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
        
        // Modo Inserção
        if (gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).ReadOnly = true;
        }
    }

    protected void blnFaixaVencimentoPosterior_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            string chave = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CompositeKey" }));

            string[] chaves = chave.Split(new Char[] { '|' });

            // Chaves
            int idPerfilCorretagem = Convert.ToInt32(chaves[0]);
            DateTime dataReferencia = Convert.ToDateTime(chaves[1]);
            int faixaVencimento = Convert.ToInt32(chaves[2]);
            decimal faixaValor = Convert.ToDecimal(chaves[3]);

            TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
            tabelaCorretagemBMF.LoadByPrimaryKey(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor);

            (sender as ASPxCheckBox).Checked = tabelaCorretagemBMF.FaixaVencimentoValidoPosteriores.Equals("S");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).ClientEnabled = false;
        }
        // Modo Inserção
        if (gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).ClientEnabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixaVencimento_Load(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixaValor_Load(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCdAtivoBMF_Load(object sender, EventArgs e) {       
        (sender as ASPxTextBox).ClientEnabled = false;
        (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textGrupoAtivo_Load(object sender, EventArgs e) {
        (sender as ASPxTextBox).ClientEnabled = false;
        (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
    }
    #endregion

    #region Grid
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfilCorretagemBMF_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idPerfilCorretagem = (int)gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem);
        string dataReferencia = Convert.ToDateTime(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia)).ToShortDateString();
        string cdAtivoBMF = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF));
        string grupoAtivo = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo));
        string idCliente = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.IdCliente));
        string tipoMercado = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado));
        string tipoCorretagem = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem));
        string tipoCorretagemDT = Convert.ToString(gridView.GetRowValues(visibleIndex, PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT));
        int corretora = Convert.ToInt32(gridView.GetRowValues(visibleIndex, AgenteMercadoMetadata.ColumnNames.IdAgente));

        //string tipoMercadoTexto = "", tipoCorretagemTexto = "", tipoCorretagemDTTexto = "";

        //#region Tradução para textos
        //switch (tipoMercado) {
        //    case "1": tipoMercadoTexto = "Disponível"; 
        //        break;	 	  
        //    case "2": tipoMercadoTexto = "Futuro";	
        //        break;
        //    case "3": tipoMercadoTexto = "Opção Disponível";	
        //        break;
        //    case "4": tipoMercadoTexto = "Opção Futuro";	
        //        break;
        //    case "5": tipoMercadoTexto = "Termo";	
        //        break;
        //}

        //switch (tipoCorretagem) {
        //    case "1": tipoCorretagemTexto = "TOB"; 
        //        break;	 	  
        //    case "2": tipoCorretagemTexto = "VL. Fixo";	
        //        break;
        //}

        //switch (tipoCorretagemDT) {
        //    case "1": tipoCorretagemDTTexto = "TOB"; 
        //        break;	 	  
        //    case "2": tipoCorretagemDTTexto = "VL. Fixo";	
        //        break;
        //}
        //#endregion

        AgenteMercado a = new AgenteMercado();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(a.Query.Nome);
        //
        string corretoraTexto = "";
        if (a.LoadByPrimaryKey(campos, corretora)) {
            corretoraTexto = a.str.Nome;
        }
        
        e.Result = idPerfilCorretagem.ToString() + "|" +
                   dataReferencia + "|" +
                   cdAtivoBMF + "|" +
                   grupoAtivo + "|" +
                   idCliente + "|" +
                   tipoMercado + "|" +
                   tipoCorretagem + "|" +
                   tipoCorretagemDT + "|" +
                   corretoraTexto + "|" + Thread.CurrentThread.CurrentCulture.Name;

        //e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem) + "|" +
        //           Convert.ToDateTime(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia)).ToShortDateString() + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF) + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo) + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.IdCliente) + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado) + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem) + "|" +
        //           Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT)
        //           ))))));       
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfilCorretagemBMF_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfilCorretagemBMF_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridPerfilCorretagemBMF.DataBind();
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idPerfilCorretagem = Convert.ToString(e.GetListSourceFieldValue(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem));
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia));
            string faixaVencimento = Convert.ToString(e.GetListSourceFieldValue(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento));
            string faixaValor = Convert.ToString(e.GetListSourceFieldValue(TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor));
            //            
            e.Value = idPerfilCorretagem + "|" + dataReferencia + "|" + faixaVencimento + "|" + faixaValor;
        }

        //if (e.Column.FieldName == PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo) {
        //    e.Value = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo));
        //}

        //if (e.Column.FieldName == PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF) {
        //    e.Value = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF));
        //}
    }

    /// <summary>
    /// Realiza o Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        string chave = (string)e.Keys[0];
        /* posicao 0 - idPerfilCorretagem
         * posicao 1 - dataReferencia
         * posicao 2 - faixaVencimento
         * posicao 3 - faixaValor
         */
        string[] chaves = chave.Split(new Char[] { '|' });
        //        
        ASPxSpinEdit textTaxaCorretagem = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCorretagemDT = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagemDT") as ASPxSpinEdit;
        string textTaxaVencimento = ( gridCadastro.FindEditFormTemplateControl ( "textTaxaVencimento" ) as ASPxSpinEdit ).Text;

        ASPxCheckBox blnexistsFaixaVencimento = gridCadastro.FindEditFormTemplateControl ( "blnFaixaVencimentoPosterior" ) as ASPxCheckBox;
        string existsFaixaVencimento = "";
        if ( null != blnexistsFaixaVencimento )
            existsFaixaVencimento = blnexistsFaixaVencimento.Checked ? "S" : "N";


        // Chaves
        int idPerfilCorretagem = Convert.ToInt32(chaves[0]);
        DateTime dataReferencia = Convert.ToDateTime(chaves[1]);
        int faixaVencimento = Convert.ToInt32(chaves[2]);
        decimal faixaValor = Convert.ToDecimal(chaves[3]);

        TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();

        if (tabelaCorretagemBMF.LoadByPrimaryKey(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor) ) {
            tabelaCorretagemBMF.TaxaCorretagem = Convert.ToDecimal(textTaxaCorretagem.Text);
            tabelaCorretagemBMF.TaxaCorretagemDT = Convert.ToDecimal(textTaxaCorretagemDT.Text);
            tabelaCorretagemBMF.TaxaVencimento = Convert.ToDecimal ( textTaxaVencimento );
            tabelaCorretagemBMF.FaixaVencimentoValidoPosteriores = existsFaixaVencimento;
            //
            tabelaCorretagemBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaCorretagemBMF - Operacao: Update TabelaCorretagemBMF: " + dataReferencia + "; " + idPerfilCorretagem + "; " + faixaVencimento + "; " + faixaValor + UtilitarioWeb.ToString(tabelaCorretagemBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
       
        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();                
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditPerfilCorretagem = gridCadastro.FindEditFormTemplateControl("btnEditPerfilCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit textFaixaVencimento = gridCadastro.FindEditFormTemplateControl("textFaixaVencimento") as ASPxSpinEdit;
        ASPxSpinEdit textFaixaValor = gridCadastro.FindEditFormTemplateControl("textFaixaValor") as ASPxSpinEdit;
        //
        ASPxSpinEdit textTaxaCorretagem = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagem") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCorretagemDT = gridCadastro.FindEditFormTemplateControl("textTaxaCorretagemDT") as ASPxSpinEdit;
        string textTaxaVencimento = ( gridCadastro.FindEditFormTemplateControl ( "textTaxaVencimento" ) as ASPxSpinEdit ).Text;
        ASPxCheckBox blnexistsFaixaVencimento = gridCadastro.FindEditFormTemplateControl ( "blnFaixaVencimentoPosterior" ) as ASPxCheckBox;
        string existsFaixaVencimento = "";
        if ( null != blnexistsFaixaVencimento )
            existsFaixaVencimento = blnexistsFaixaVencimento.Checked ? "S" : "N";

        TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
        //
        tabelaCorretagemBMF.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaCorretagemBMF.IdPerfilCorretagem = Convert.ToInt32(btnEditPerfilCorretagem.Text);
        tabelaCorretagemBMF.FaixaVencimento = Convert.ToInt32(textFaixaVencimento.Text);
        tabelaCorretagemBMF.FaixaValor = Convert.ToDecimal(textFaixaValor.Text);
        tabelaCorretagemBMF.TaxaCorretagem = Convert.ToDecimal(textTaxaCorretagem.Text);
        tabelaCorretagemBMF.TaxaCorretagemDT = Convert.ToDecimal(textTaxaCorretagemDT.Text);
        tabelaCorretagemBMF.TaxaVencimento = Convert.ToDecimal ( textTaxaVencimento );
        tabelaCorretagemBMF.FaixaVencimentoValidoPosteriores = existsFaixaVencimento;

        //
        tabelaCorretagemBMF.Save();
        //
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaCorretagemBMF - Operacao: Insert TabelaCorretagemBMF: " + tabelaCorretagemBMF.DataReferencia + "; " + tabelaCorretagemBMF.IdPerfilCorretagem + "; " + tabelaCorretagemBMF.FaixaVencimento + "; " + tabelaCorretagemBMF.FaixaValor + UtilitarioWeb.ToString(tabelaCorretagemBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Faz o Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        const string Delete = "btnDelete";

        if (e.Parameters == Delete) {
            TabelaCorretagemBMFCollection tabelaCorretagemBMFCollection = new TabelaCorretagemBMFCollection();

            List<object> keyValuesIdPerfilCorretagem = gridCadastro.GetSelectedFieldValues(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem);
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesFaixaVencimento = gridCadastro.GetSelectedFieldValues(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento);
            List<object> keyValuesFaixaValor = gridCadastro.GetSelectedFieldValues(TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor);

            for (int i = 0; i < keyValuesIdPerfilCorretagem.Count; i++) {
                // Chaves
                int idPerfilCorretagem = Convert.ToInt32(keyValuesIdPerfilCorretagem[i]);
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                int faixaVencimento = Convert.ToInt32(keyValuesFaixaVencimento[i]);
                decimal faixaValor = Convert.ToDecimal(keyValuesFaixaValor[i]);
                //
                TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
                //
                if (tabelaCorretagemBMF.LoadByPrimaryKey(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor) ) {
                    tabelaCorretagemBMFCollection.AttachEntity(tabelaCorretagemBMF);
                }
            }

            // Delete
            TabelaCorretagemBMFCollection tabelaCorretagemBMFCollectionClone = (TabelaCorretagemBMFCollection)Utilitario.Clone(tabelaCorretagemBMFCollection);
            //

            tabelaCorretagemBMFCollection.MarkAllAsDeleted();
            tabelaCorretagemBMFCollection.Save();

            foreach (TabelaCorretagemBMF t in tabelaCorretagemBMFCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaCorretagemBMF - Operacao: Delete TabelaCorretagemBMF: " + t.DataReferencia + "; " + t.IdPerfilCorretagem + "; " + t.FaixaVencimento + "; " + t.FaixaValor + UtilitarioWeb.ToString(t),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion   
            }            
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditPerfilCorretagem", "textTaxaCorretagem");
        base.gridCadastro_PreRender(sender, e);
    }

    #endregion
}