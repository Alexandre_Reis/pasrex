using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.BMF;

public partial class CadastrosBasicos_TabelaCustosBMF : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasEditControl = false;
        base.Page_Load(sender, e);
    }

    protected void EsDSTabelaCustosBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TabelaCustosBMFCollection coll = new TabelaCustosBMFCollection();

        coll.Query.OrderBy(coll.Query.DataReferencia.Descending,
                           coll.Query.CdAtivoBMF.Ascending,
                           coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue("DataReferencia"));
            string cdAtivoBMF = Convert.ToString(e.GetListSourceFieldValue("CdAtivoBMF"));
            string serie = Convert.ToString(e.GetListSourceFieldValue("Serie"));
            e.Value = dataReferencia + "|" + cdAtivoBMF + "|" + serie;
        }
    }

}

