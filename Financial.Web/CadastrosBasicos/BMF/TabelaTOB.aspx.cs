﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DevExpress.Web;

using System.Collections.Generic;

using Financial.Web.Common;

using Financial.BMF;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;


public partial class CadastrosBasicos_TabelaTOB : CadastroBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { TabelaTOBMetadata.ColumnNames.TipoMercado }));

        this.HasPopupAtivoBMF = true;
    }

    #region DataSources
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaTOB_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTOBCollection coll = new TabelaTOBCollection();
        coll.Query.OrderBy(coll.Query.DataReferencia.Ascending,
                           coll.Query.TipoMercado.Ascending,
                           coll.Query.CdAtivoBMF.Ascending);
        coll.LoadAll();
        //
        e.Collection = coll;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSClasseBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClasseBMFCollection coll = new ClasseBMFCollection();        
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    #region CallBacks
    /// <summary>
    /// Tratatamento de Erros
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxButtonEdit cdAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxComboBox tipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        //
        ASPxSpinEdit tob = gridCadastro.FindEditFormTemplateControl("textTOB") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytrade = gridCadastro.FindEditFormTemplateControl("textTOBDayTrade") as ASPxSpinEdit;
        ASPxSpinEdit tobMinima = gridCadastro.FindEditFormTemplateControl("textTOBMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytradeMinima = gridCadastro.FindEditFormTemplateControl("textTOBDayTradeMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicio = gridCadastro.FindEditFormTemplateControl("textTOBExercicio") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicioCasado = gridCadastro.FindEditFormTemplateControl("textTOBExercicioCasado") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
                    textDataReferencia, cdAtivoBMF, tipoMercado, 
                    tob, tobDaytrade, tobMinima,
                    tobDaytradeMinima, tobExercicio, tobExercicioCasado
        });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Testa CodigoBMF
        ClasseBMF classeBMF = new ClasseBMF();
        if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF.Text.Trim().ToUpper())) {
            e.Result = "Ativo BMF não Existe";
            return;
        }
        #endregion

        // Se estiver Incluindo
        if (gridCadastro.IsNewRowEditing) {
            TabelaTOB tabelaTOB = new TabelaTOB();
            //
            if (tabelaTOB.LoadByPrimaryKey(Convert.ToDateTime(textDataReferencia.Text),
                                           cdAtivoBMF.Text.Trim().ToUpper(),
                                           Convert.ToByte(tipoMercado.SelectedItem.Value)) ) {
                
                e.Result = "Registro já existente";
            }
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxDateEdit).Enabled = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoMercado_OnLoad(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }

        // Modo Inserção
        //if (gridCadastro.IsNewRowEditing) {
        //    (sender as ASPxComboBox).SelectedItem.Index = 1;
        //}
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void cdAtivoBMF_Load(object sender, EventArgs e) {
        // Modo Update
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }        

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridClasseBMF_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), TabelaTOBMetadata.ColumnNames.CdAtivoBMF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridClasseBMF_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridClasseBMF_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridClasseBMF.DataBind();
    }
    
    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(TabelaTOBMetadata.ColumnNames.DataReferencia));
            string cdAtivoBMF = Convert.ToString(e.GetListSourceFieldValue(TabelaTOBMetadata.ColumnNames.CdAtivoBMF));            
            string tipoMercado = Convert.ToString(e.GetListSourceFieldValue(TabelaTOBMetadata.ColumnNames.TipoMercado));

            e.Value = dataReferencia + "|" + cdAtivoBMF + "|" + tipoMercado;
        }
    }

    /// <summary>
    /// Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        // Chaves
        string chave = (string)e.Keys[0];
        /* posicao 0 - dataReferencia
         * posicao 1 - cdAtivoBMF
         * posicao 2 - tipoMercado
         */
        string[] chaves = chave.Split(new Char[] { '|' });
        //        
        ASPxSpinEdit tob = gridCadastro.FindEditFormTemplateControl("textTOB") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytrade = gridCadastro.FindEditFormTemplateControl("textTOBDayTrade") as ASPxSpinEdit;
        ASPxSpinEdit tobMinima = gridCadastro.FindEditFormTemplateControl("textTOBMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytradeMinima = gridCadastro.FindEditFormTemplateControl("textTOBDayTradeMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicio = gridCadastro.FindEditFormTemplateControl("textTOBExercicio") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicioCasado = gridCadastro.FindEditFormTemplateControl("textTOBExercicioCasado") as ASPxSpinEdit;

        // Chaves
        DateTime dataReferencia = Convert.ToDateTime(chaves[0]);
        string cdAtivoBMF = Convert.ToString(chaves[1]);
        int tipoMercado = Convert.ToInt32(chaves[2]);

        TabelaTOB tabelaTOB = new TabelaTOB();

        if (tabelaTOB.LoadByPrimaryKey(dataReferencia, cdAtivoBMF, (byte)tipoMercado)) {
            tabelaTOB.Tob = Convert.ToDecimal(tob.Text);
            tabelaTOB.TOBDayTrade = Convert.ToDecimal(tobDaytrade.Text);
            tabelaTOB.TOBMinima = Convert.ToDecimal(tobMinima.Text);
            tabelaTOB.TOBDayTradeMinima = Convert.ToDecimal(tobDaytradeMinima.Text);
            tabelaTOB.TOBExercicio = Convert.ToDecimal(tobExercicio.Text);
            tabelaTOB.TOBExercicioCasado = Convert.ToDecimal(tobExercicioCasado.Text);
            //
            tabelaTOB.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaTOB - Operacao: Update TabelaTOB: " + dataReferencia + " " + cdAtivoBMF + " " + tipoMercado + " " + UtilitarioWeb.ToString(tabelaTOB),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
       
        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();                
    }

    /// <summary>
    /// Insert
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxButtonEdit cdAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnCdAtivo") as ASPxButtonEdit;
        ASPxComboBox tipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;        
        //
        ASPxSpinEdit tob = gridCadastro.FindEditFormTemplateControl("textTOB") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytrade = gridCadastro.FindEditFormTemplateControl("textTOBDayTrade") as ASPxSpinEdit;
        ASPxSpinEdit tobMinima = gridCadastro.FindEditFormTemplateControl("textTOBMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobDaytradeMinima = gridCadastro.FindEditFormTemplateControl("textTOBDayTradeMinima") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicio = gridCadastro.FindEditFormTemplateControl("textTOBExercicio") as ASPxSpinEdit;
        ASPxSpinEdit tobExercicioCasado = gridCadastro.FindEditFormTemplateControl("textTOBExercicioCasado") as ASPxSpinEdit;

        TabelaTOB tabelaTOB = new TabelaTOB();
        //
        tabelaTOB.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaTOB.CdAtivoBMF = Convert.ToString(cdAtivoBMF.Text);
        tabelaTOB.TipoMercado = Convert.ToByte(tipoMercado.SelectedItem.Value);
        //
        tabelaTOB.Tob = Convert.ToDecimal(tob.Text);
        tabelaTOB.TOBDayTrade = Convert.ToDecimal(tobDaytrade.Text);
        tabelaTOB.TOBMinima = Convert.ToDecimal(tobMinima.Text);
        tabelaTOB.TOBDayTradeMinima = Convert.ToDecimal(tobDaytradeMinima.Text);
        tabelaTOB.TOBExercicio = Convert.ToDecimal(tobExercicio.Text);
        tabelaTOB.TOBExercicioCasado = Convert.ToDecimal(tobExercicioCasado.Text);
        //
        tabelaTOB.Save();
        //
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaTOB - Operacao: Insert TabelaTOB: " + tabelaTOB.CdAtivoBMF + " " + tabelaTOB.DataReferencia + " " + tabelaTOB.TipoMercado + " " + UtilitarioWeb.ToString(tabelaTOB),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Delete de TabelaTob
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        const string Delete = "btnDelete";

        if (e.Parameters == Delete) {
            TabelaTOBCollection tabelaTobCollection = new TabelaTOBCollection();

            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(TabelaTOBMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesCdAtivoBMF = gridCadastro.GetSelectedFieldValues(TabelaTOBMetadata.ColumnNames.CdAtivoBMF);
            List<object> keyValuesTipoMercado = gridCadastro.GetSelectedFieldValues(TabelaTOBMetadata.ColumnNames.TipoMercado);

            for (int i = 0; i < keyValuesDataReferencia.Count; i++) {
                // Chaves
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                string cdAtivoBMF = Convert.ToString(keyValuesCdAtivoBMF[i]);
                int tipoMercado = Convert.ToInt32(keyValuesTipoMercado[i]);                
                //
                TabelaTOB tabelaTOB = new TabelaTOB();
                //
                if (tabelaTOB.LoadByPrimaryKey(dataReferencia, cdAtivoBMF, (byte)tipoMercado)) {
                    tabelaTobCollection.AttachEntity(tabelaTOB);
                }
            }

            TabelaTOBCollection tabelaTobCollectionClone = (TabelaTOBCollection)Utilitario.Clone(tabelaTobCollection);

            // Delete
            tabelaTobCollection.MarkAllAsDeleted();
            tabelaTobCollection.Save();
            //
            foreach (TabelaTOB c in tabelaTobCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaTOB - Operacao: Delete TabelaTOB: " + c.DataReferencia + " " + c.CdAtivoBMF + " " + c.TipoMercado + " " + UtilitarioWeb.ToString(c),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textDataReferencia", "textTOB");
        base.gridCadastro_PreRender(sender, e);
    }
}