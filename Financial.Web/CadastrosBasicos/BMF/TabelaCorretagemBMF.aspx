﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaCorretagemBMF.aspx.cs" Inherits="CadastrosBasicos_TabelaCorretagemBMF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataPerfilCorretagemBMF(data) {        
        /* idPerfilCorretagem, DataReferencia, CdAtivoBMF */
        var resultSplit = data.split('|');
        btnEditPerfilCorretagem.SetValue(resultSplit[0]);
        //                                
        var newDate = LocalizedData(resultSplit[1], resultSplit[9]);                       
        textData.SetValue(newDate);
        //        
        textCdAtivoBMF.SetValue(resultSplit[2]);
        textGrupoAtivo.SetValue(resultSplit[3]);
        textIdCliente.SetValue(resultSplit[4]);
        //
        dropTipoMercado.SetValue(resultSplit[5]);                    
        dropTipoCorretagem.SetValue(resultSplit[6]);
        dropTipoCorretagemDT.SetValue(resultSplit[7]);        
        dropCorretora.SetValue(resultSplit[8]);
        //
        popupPerfilCorretagemBMF.HideWindow();
        btnEditPerfilCorretagem.Focus();        
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackClone" runat="server" OnCallback="callbackClone_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {    
            alert('Clonagem executada');
            gridCadastro.UpdateEdit();
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <dxpc:ASPxPopupControl ID="popupPerfilCorretagemBMF" ClientInstanceName="popupPerfilCorretagemBMF" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPerfilCorretagemBMF" runat="server" Width="100%"
                    ClientInstanceName="gridPerfilCorretagemBMF"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPerfilCorretagemBMF" KeyFieldName="IdPerfilCorretagem"
                    OnCustomDataCallback="gridPerfilCorretagemBMF_CustomDataCallback" 
                    OnCustomCallback="gridPerfilCorretagemBMF_CustomCallback"
                    OnHtmlRowCreated="gridPerfilCorretagemBMF_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdPerfilCorretagem" VisibleIndex="0" ReadOnly="true" Width="10%"/>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="1" Width="10%">
                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}"  />                    
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Cliente" VisibleIndex="2" ReadOnly="true" Width="25%"/>
                                               
                <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" Caption="Ativo" VisibleIndex="3" ReadOnly="true" Width="8%" Settings-AutoFilterCondition="Contains" />
                <dxwgv:GridViewDataTextColumn FieldName="GrupoAtivo" Caption="Grupo Ativo" VisibleIndex="4" ReadOnly="true" Width="8%"/>                
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="5" Width="11%">
                    <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente" />
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="6" Width="11%" ReadOnly="true">
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Disponível" />
                        <dxe:ListEditItem Value="2" Text="Futuro" />
                        <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                        <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                        <dxe:ListEditItem Value="5" Text="Termo" />                            
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>             
                                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagem" VisibleIndex="7" Width="11%" ReadOnly="true">
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="TOB" />
                        <dxe:ListEditItem Value="2" Text="VL. Fixo" />
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
            
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagemDT" VisibleIndex="8" Width="12%" ReadOnly="true">
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="TOB" />
                        <dxe:ListEditItem Value="2" Text="VL. Fixo" />
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>                                                               
            </Columns>
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) { 
            gridPerfilCorretagemBMF.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataPerfilCorretagemBMF);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Perfil de Corretagem BMF" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridPerfilCorretagemBMF.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Tabelas de Corretagem (BMF)" />
    </div>
    
    <div id="mainContent">
              
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {gridCadastro.PerformCallback('btnDelete');} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
    
            <div class="divDataGrid">
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
                KeyFieldName="CompositeKey"
                DataSourceID="EsDSTabelaCorretagemBMF"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">        
                            
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="13%" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>

                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" VisibleIndex="0" />                                
                <dxwgv:GridViewDataTextColumn FieldName="IdPerfilCorretagem" Visible="false" />
                
                <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id" VisibleIndex="2" Width="7%" CellStyle-HorizontalAlign="left"/>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="25%"/>
                
                 <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="3" Width="25%" >
                    <PropertiesComboBox TextField="Nome" ValueField="IdAgente" DataSourceID="EsDSAgenteMercado"/>
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="4" Width="10%"/>                                    
                <dxwgv:GridViewDataTextColumn FieldName="GrupoAtivo" VisibleIndex="5" Width="9%" Visible="true" />
                <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" VisibleIndex="6" Width="7%" Visible="true" Settings-AutoFilterCondition="Contains" />
                                                             
                <dxwgv:GridViewDataSpinEditColumn FieldName="FaixaVencimento" VisibleIndex="7"  Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right"/>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="FaixaValor" VisibleIndex="8" Width="9%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                </dxwgv:GridViewDataSpinEditColumn>
                                
                <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaCorretagem" Caption="Taxa Corretagem" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                </dxwgv:GridViewDataSpinEditColumn>
                    
                <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaCorretagemDT" Caption="Taxa Corretagem DT" VisibleIndex="10" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                </dxwgv:GridViewDataSpinEditColumn>
                                                                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="11" Width="11%" Visible="false">
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Disponível" />
                        <dxe:ListEditItem Value="2" Text="Futuro" />
                        <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                        <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                        <dxe:ListEditItem Value="5" Text="Termo" />                            
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>             
                                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagem" VisibleIndex="12" Width="11%" Visible="false" >
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="TOB" />
                        <dxe:ListEditItem Value="2" Text="VL. Fixo" />
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
            
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagemDT" VisibleIndex="13" Width="12%" Visible="false" >
                <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="TOB" />
                        <dxe:ListEditItem Value="2" Text="VL. Fixo" />
                    </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                

                                                       
            </Columns>
            
            <Templates>
                <EditForm>
                                        
                    <div class="editForm">        
                         
                         <table> 
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelPerfilCorretagem" runat="server" CssClass="labelRequired" Text="Perfil Corretagem:" />
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="btnEditPerfilCorretagem" runat="server" MaxLength="10"
                                                        Text='<%#Eval("IdPerfilCorretagem")%>' ClientInstanceName="btnEditPerfilCorretagem"                                                         
                                                        NumberType="Integer" CssClass="textButtonEdit"
                                                        OnInit="btnEditPerfilCorretagem_OnInit">
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>
                                            <ClientSideEvents                              
                                                 ButtonClick="function(s, e) {popupPerfilCorretagemBMF.ShowAtElementByID(s.name);}"                                                  
                                            />                                                                   
                                    </dxe:ASPxSpinEdit>
                                </td>                                                               
                            </tr>
                                                       
                            <tr>                                                                                                        
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Referência:" />
                                </td>
                                                        
                                <td colspan="3">       
                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init"/>                                                 
                                </td>                                                                
                            </tr>
                            
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelIdCliente" runat="server" CssClass="labelNormal" Text="Id Cliente:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textIdCliente" runat="server" CssClass="textNormal_5" 
                                        ClientInstanceName="textIdCliente" Text='<%# Eval("IdCliente") %>' ClientEnabled="false" />
                                </td>

                                <td class="td_Label_Curto">
                                    <asp:Label ID="CdAtivoBMF" runat="server" CssClass="labelNormal" Text="Ativo BMF:" />
                                </td>
                                <td>                                    
                                    <dxe:ASPxTextBox ID="textCdAtivoBMF" runat="server" ClientInstanceName="textCdAtivoBMF" 
                                    CssClass="textNormal_5" Text='<%# Eval("CdAtivoBMF") %>' OnLoad="textCdAtivoBMF_Load" />
                                </td>                                                                                    
                            </tr>

                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="GrupoAtivo" runat="server" CssClass="labelNormal" Text="Grupo Ativo:" />
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxTextBox ID="textGrupoAtivo" runat="server" ClientInstanceName="textGrupoAtivo" 
                                    Text='<%# Eval("GrupoAtivo") %>' CssClass="textDescricao" OnLoad="textGrupoAtivo_Load" />
                                </td>                                                        
                            </tr>
                            
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="TipoMercado" runat="server" CssClass="labelNormal" Text="TipoMercado:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" ClientInstanceName="dropTipoMercado" ClientEnabled="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoMercado")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Disponível" />
                                            <dxe:ListEditItem Value="2" Text="Futuro" />
                                            <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                                            <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                                            <dxe:ListEditItem Value="5" Text="Termo" />                            
                                        </Items>                                                                                    
                                    </dxe:ASPxComboBox>                                                                                            
                                </td>
                                
                                <td class="td_Label_Curto">
                                    <asp:Label ID="Corretora" runat="server" CssClass="labelNormal" Text="Corretora:" />
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropCorretora" runat="server" ClientInstanceName="dropCorretora" 
                                    ClientEnabled="false" CssClass="dropDownListCurto" ValueField="IdAgente" TextField="Nome" Text='<%#Eval("Nome")%>' />
                                </td>             
                                                                                     
                            </tr>
                                                                                     
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="TipoCorretagem" runat="server" CssClass="labelNormal" Text="Tipo Corretagem:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoCorretagem" runat="server" ClientInstanceName="dropTipoCorretagem" ClientEnabled="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoCorretagem")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="TOB" />
                                            <dxe:ListEditItem Value="2" Text="VL. Fixo" />                          
                                        </Items>                                                                                    
                                    </dxe:ASPxComboBox>                                                                                                                                        
                                </td>                                                        

                                <td class="td_Label_Curto">
                                    <asp:Label ID="TipoCorretagemDT" runat="server" CssClass="labelNormal" Text="Tipo Corretagem DT:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoCorretagemDT" runat="server" ClientInstanceName="dropTipoCorretagemDT" ClientEnabled="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoCorretagemDT")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="TOB" />
                                            <dxe:ListEditItem Value="2" Text="VL. Fixo" />                          
                                        </Items>                                                                                    
                                    </dxe:ASPxComboBox>
                                </td>                                                                                                                                                                                        
                            </tr>
                                                                                                                                                                            
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelFaixaVencimento" runat="server" CssClass="labelRequired" Text="Fx. Vencimento:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textFaixaVencimento" runat="server" CssClass="textValor_5" ClientInstanceName="textFaixaVencimento"
                                                                    Text='<%# Eval("FaixaVencimento") %>' OnLoad="textFaixaVencimento_Load"
                                                                    MaxLength="9" NumberType="Integer">
                                    </dxe:ASPxSpinEdit>                                                                            
                                </td>

                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelFaixaValor" runat="server" CssClass="labelRequired" Text="Fx. Valor:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textFaixaValor" runat="server" CssClass="textValor_5" ClientInstanceName="textFaixaValor" OnLoad="textFaixaValor_Load"
                                                                    Text='<%# Eval("FaixaValor") %>' 
                                                                    MaxLength="14" NumberType="Float" DecimalPlaces="2">
                                    </dxe:ASPxSpinEdit>                                                                            
                                </td>
                                
                            </tr>
                            
                            <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelTaxaCorretagem" runat="server" CssClass="labelRequired" Text="Taxa Corretagem:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textTaxaCorretagem" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaCorretagem"
                                                                    Text='<%# Eval("TaxaCorretagem") %>'
                                                                    MaxLength="14" NumberType="Float" DecimalPlaces="8">
                                    </dxe:ASPxSpinEdit>                                                                            
                                </td>                                                        

                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelTaxaCorretagemDT" runat="server" CssClass="labelRequired" Text="Taxa Corretagem DT:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textTaxaCorretagemDT" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaCorretagemDT"
                                                                    Text='<%# Eval("TaxaCorretagemDT") %>'
                                                                    MaxLength="14" NumberType="Float" DecimalPlaces="8">
                                    </dxe:ASPxSpinEdit>                                            
                                </td>                                                        
                                
                            </tr>
                            
                             <tr>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelTaxaVencimento" runat="server" CssClass="labelRequired" Text="Taxa Vencimento:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textTaxaVencimento" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaVencimento"
                                                                    Text='<%# Eval("[TaxaVencimento]") %>'
                                                                    MaxLength="14" NumberType="Float" DecimalPlaces="8">
                                    </dxe:ASPxSpinEdit>                                                                            
                                </td>                                                        

                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelFaixaVencimentoPosterior" runat="server" CssClass="labelRequired" Text="Utilizar para Vencimentos Posteriores:" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox ID="blnFaixaVencimentoPosterior" runat="server" ClientInstanceName="checkFaixaVencimentoPosterior" OnInit="blnFaixaVencimentoPosterior_Init"></dxe:ASPxCheckBox>
                                </td>                                                        

                            </tr>

                        </table>
                        
                        <div class="linkButton linkButtonNoBorder" style="margin-left:80px;">
                            <asp:LinkButton ID="btnClone" runat="server" Font-Overline="false" CssClass="btnCopy" OnInit="btnClone_Init"
                                                    OnClientClick="callbackClone.SendCallback(); return false;">
                                    <asp:Literal ID="Literal8" runat="server" Text="Clonar p/ Clientes Ativos"/><div></div></asp:LinkButton>
                        </div>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>   
                    </div>
                   
                </EditForm>
            </Templates>
            
            <SettingsPopup EditForm-Width="380px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
                        
        </dxwgv:ASPxGridView>            
        </div>
                            
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />

    <cc1:esDataSource ID="EsDSTabelaCorretagemBMF" runat="server" OnesSelect="EsDSTabelaCorretagemBMF_esSelect" LowLevelBind="true" />    
    <cc1:esDataSource ID="EsDSPerfilCorretagemBMF" runat="server" OnesSelect="EsDSPerfilCorretagemBMF_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />    
    </form>
</body>
</html>