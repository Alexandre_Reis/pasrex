﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerfilRepasseBMF.aspx.cs" Inherits="CadastrosBasicos_PerfilRepasseBMF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head4" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
          
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                 
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    
  <%--  <dxcb:ASPxCallback ID="callbackClone" runat="server" OnCallback="callbackClone_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {    
            alert('Clonagem executada');
            gridCadastro.UpdateEdit();
        }        
        "/>
    </dxcb:ASPxCallback>--%>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Perfis de Clearing (BMF)" />
    </div>
    
    <div id="mainContent">
               
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>

        <div class="divDataGrid">
        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
            KeyFieldName="CompositeKey" DataSourceID="EsDSPerfilRepasseBMF"
            OnCustomCallback="gridCadastro_CustomCallback"
            OnRowInserting="gridCadastro_RowInserting"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnBeforeGetCallbackResult="gridCadastro_PreRender"
            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
            OnCustomJSProperties="gridCadastro_CustomJSProperties" >        
                
        <Columns>  
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="5%" ShowClearFilterButton="True">
                <HeaderTemplate>
                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                </HeaderTemplate>
            </dxwgv:GridViewCommandColumn>                
            
            <dxwgv:GridViewDataTextColumn FieldName="Id" Visible="false" />
            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
            <dxwgv:GridViewDataTextColumn FieldName="IdPerfil" Caption="Código" UnboundType="Integer" VisibleIndex="0" Visible="true" Width="3%" />
            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição Perfil" UnboundType="String" Visible="true" VisibleIndex="1" />
                                           
        </Columns>
        
        <Templates>
            <EditForm>
                <div class="editForm">       
                                       
                    <table>
                        
                        <tr>
                            <td class="td_Label_Lzongo">
                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Código Perfíl:" />
                            </td>        

                            <td class="td_label_lzongo">
                                 <dxe:ASPxSpinEdit ID="textCodigoPerfil" runat="server" Number="0" MaxValue="99999" MinValue="0" OnInit="textCodigoPerfil_Init" ClientInstanceName="textCodigoPerfil" Text='<%#Eval("IdPerfil") %>'></dxe:ASPxSpinEdit>
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="td_Label_Lzongo">
                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Descrição Perfíl:" />
                            </td>        

                            <td>
                                <dxe:ASPxTextBox ID="textDescricaoPerfil" runat="server" CssClass="" Text='<%# Eval("Descricao") %>' ClientInstanceName="textDescricaoPerfil" OnInit="textDescricaoPerfil_Init"></dxe:ASPxTextBox>
                            </td>                               
                        </tr>                        
                                                                                                                                                                        
                    </table>
                    
                    <div class="linhaH"></div>
            
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"  OnInit="btnOKAdd_Init"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                </div>
                                
            </EditForm>                
                            
        </Templates>
        
        <SettingsPopup EditForm-Width="500px" />
        <SettingsCommandButton>
            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
        </SettingsCommandButton>
        
    </dxwgv:ASPxGridView>            
    </div>                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"  Landscape = "true"
    LeftMargin = "40"
    RightMargin = "40"    
    />
        
    <cc1:esDataSource ID="EsDSPerfilRepasseBMF" runat="server" OnesSelect="EsDSPerfilRepasseBMF_esSelect" />    
        
    </form>
</body>
</html>