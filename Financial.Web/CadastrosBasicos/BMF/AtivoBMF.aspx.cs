﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;

using DevExpress.Web;

using Financial.BMF;
using Financial.Common.Enums;
using Financial.Web.Common;
using System.Drawing;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.BMF.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_AtivoBMF : CadastroBasePage {

    new protected void Page_Load(object sender, EventArgs e) {               
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                          new List<string>(new string[] { AtivoBMFMetadata.ColumnNames.TipoMercado, 
                                                                          AtivoBMFMetadata.ColumnNames.TipoSerie
                                                                        }));
    }

    #region DataSources
    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBMFCollection coll = new AtivoBMFCollection();

        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.IdMoeda.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EstrategiaCollection coll = new EstrategiaCollection();
        EstrategiaQuery estrategiaQuery = new EstrategiaQuery("estrategia");

        StringBuilder descricaoField = new StringBuilder();
        descricaoField.Append("< cast(estrategia.[IdEstrategia] as varchar(20)) + ' - ' + estrategia.descricao as Descricao >");

        estrategiaQuery.Select(descricaoField.ToString(), estrategiaQuery.IdEstrategia);
        estrategiaQuery.OrderBy(estrategiaQuery.IdEstrategia.Ascending);
        coll.Load(estrategiaQuery);

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textCdAtivoBMF = gridCadastro.FindEditFormTemplateControl("textCdAtivoBMF") as ASPxTextBox;
        ASPxTextBox textSerie = gridCadastro.FindEditFormTemplateControl("textSerie") as ASPxTextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxSpinEdit textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as ASPxSpinEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textCdAtivoBMF);
        controles.Add(textSerie);
        controles.Add(dropTipoMercado);
        controles.Add(textPeso);
        controles.Add(dropMoeda);
        controles.Add(dropLocalCustodia);
        controles.Add(dropLocalNegociacao);
        controles.Add(dropClearing);
        controles.Add(dropEstrategia);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        string cdAtivoBMF = Convert.ToString(textCdAtivoBMF.Text);
        string serie = Convert.ToString(textSerie.Text);

        if (gridCadastro.IsNewRowEditing)
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            if (ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
            {
                e.Result = "Registro já existente";
            }
        }

        if (Convert.ToString(textDataVencimento.Text) != "")
        {
            if (Convert.ToInt32(dropMoeda.SelectedItem.Value) == (byte)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataVencimento.Text), LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                {
                    e.Result = "Data de vencimento não é dia útil.";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataVencimento.Text), LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data de vencimento não é dia útil.";
                    return;
                }
            }
        }
        else
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            if (ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
            {
                if (ativoBMF.TipoMercado != (byte)TipoMercadoBMF.Disponivel)
                {
                    e.Result = "Para mercado diferente de disponível, a data de vencimento é obrigatória.";
                }
            }
        }

        if (textCodigoCusip.Text.Length < 9 && !string.IsNullOrEmpty(textCodigoCusip.Text))
        {
            e.Result = "Código Cusip deve possuir 9 caracteres.";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCdAtivoBMF_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxTextBox).Enabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }
        else
        {
            //(sender as ASPxTextBox).Attributes.Add("style", "text-transform:uppercase;"); 
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textSerie_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxTextBox).Enabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string cdAtivoBMF = Convert.ToString(e.GetListSourceFieldValue(AtivoBMFMetadata.ColumnNames.CdAtivoBMF));
            string serie = Convert.ToString(e.GetListSourceFieldValue(AtivoBMFMetadata.ColumnNames.Serie));
            e.Value = cdAtivoBMF + "-" + serie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxTextBox textCdAtivoBMF = gridCadastro.FindEditFormTemplateControl("textCdAtivoBMF") as ASPxTextBox;
        ASPxTextBox textSerie = gridCadastro.FindEditFormTemplateControl("textSerie") as ASPxTextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoSerie = gridCadastro.FindEditFormTemplateControl("dropTipoSerie") as ASPxComboBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textPrecoExercicio = gridCadastro.FindEditFormTemplateControl("textPrecoExercicio") as ASPxSpinEdit;
        ASPxSpinEdit textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as ASPxSpinEdit;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxTextBox textCodigoFeeder = gridCadastro.FindEditFormTemplateControl("textCodigoFeeder") as ASPxTextBox;
        TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        ASPxSpinEdit textCodigoCDA = gridCadastro.FindEditFormTemplateControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;

        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;

        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        string cdAtivoBMF = textCdAtivoBMF.Text.ToString();
        string serie = textSerie.Text.ToString();

        //Verifica se já existe a classeBMF criada, se não existe, cria agora
        ClasseBMF classeBMF = new ClasseBMF();
        if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF)) {
            classeBMF.CdAtivoBMF = cdAtivoBMF;
            classeBMF.Save();
        }
        //

        AtivoBMF ativoBMF = new AtivoBMF();

        ativoBMF.CdAtivoBMF = cdAtivoBMF.Trim().ToUpper();
        ativoBMF.Serie = serie.Trim().ToUpper();
        ativoBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
        ativoBMF.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

        if (dropClearing.SelectedIndex > -1)
        {
            ativoBMF.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
        }
        else
        {
            ativoBMF.IdClearing = null;
        }

        if (dropLocalNegociacao.SelectedIndex > -1)
        {
            ativoBMF.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
        }
        else
        {
            ativoBMF.IdLocalNegociacao = null;
        }

        if (dropTipoSerie.SelectedIndex != -1) {
            ativoBMF.TipoSerie = Convert.ToString(dropTipoSerie.SelectedItem.Value);
        }

        if (textDataVencimento.Text.ToString() != "") {
            ativoBMF.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        if (textPrecoExercicio.Text.ToString() != "") {
            ativoBMF.PrecoExercicio = Convert.ToDecimal(textPrecoExercicio.Text);
        }

        if (!String.IsNullOrEmpty(textCodigoFeeder.Text.ToString())) {
            ativoBMF.CodigoFeeder = textCodigoFeeder.Text.ToString().Trim();
        }

        ativoBMF.Peso = Convert.ToDecimal(textPeso.Text);
        ativoBMF.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);
        //

        if (textCodigoIsin.Text.ToString() != "") {
            ativoBMF.CodigoIsin = textCodigoIsin.Text.ToString().Trim();
        }

        if (dropEstrategia.Text != "")
        {
            ativoBMF.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
        }
        else
        {
            ativoBMF.IdEstrategia = null;
        }

        if (textCodigoCusip.Text.ToString() != "") 
            ativoBMF.CodigoCusip = textCodigoCusip.Text;
        
        int codicoCda = 0;
        if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
            ativoBMF.CodigoCDA = codicoCda;

        int codigoBds = 0;
        if (int.TryParse(textCodigoBDS.Text.ToString(), out codigoBds))
            ativoBMF.CodigoBDS = codigoBds;

        if (dropInvestimentoColetivoCvm.SelectedIndex != -1)
        {
            ativoBMF.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
        }

        ativoBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AtivoBMF - Operacao: Insert AtivoBMF: " + ativoBMF.CdAtivoBMF + "; " + ativoBMF.Serie + UtilitarioWeb.ToString(ativoBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textCdAtivoBMF = gridCadastro.FindEditFormTemplateControl("textCdAtivoBMF") as ASPxTextBox;
        ASPxTextBox textSerie = gridCadastro.FindEditFormTemplateControl("textSerie") as ASPxTextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropTipoSerie = gridCadastro.FindEditFormTemplateControl("dropTipoSerie") as ASPxComboBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textPrecoExercicio = gridCadastro.FindEditFormTemplateControl("textPrecoExercicio") as ASPxSpinEdit;
        ASPxSpinEdit textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as ASPxSpinEdit;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
        ASPxTextBox textCodigoFeeder = gridCadastro.FindEditFormTemplateControl("textCodigoFeeder") as ASPxTextBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = gridCadastro.FindEditFormTemplateControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        string cdAtivoBMF = textCdAtivoBMF.Text.ToString();
        string serie = textSerie.Text.ToString();
        //
        AtivoBMF ativoBMF = new AtivoBMF();

        if (ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie)) 
        {

            if (dropClearing.SelectedIndex > -1)
            {
                ativoBMF.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
            }
            else
            {
                ativoBMF.IdClearing = null;
            }

            if (dropLocalNegociacao.SelectedIndex > -1)
            {
                ativoBMF.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
            }
            else
            {
                ativoBMF.IdLocalNegociacao = null;
            }

            if (dropTipoSerie.SelectedIndex != -1) {
                ativoBMF.TipoSerie = Convert.ToString(dropTipoSerie.SelectedItem.Value);
            }

            if (Convert.ToDateTime(textDataVencimento.Text) != ativoBMF.DataVencimento.Value ||
                Convert.ToByte(dropTipoMercado.SelectedItem.Value) != ativoBMF.TipoMercado.Value)
            {
                #region Posicao BMF
                PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                         posicaoBMFCollection.Query.Serie.Equal(serie));

                posicaoBMFCollection.Query.Load();

                foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
                {
                    posicaoBMF.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                    posicaoBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
                }
                posicaoBMFCollection.Save();
                #endregion

                #region Posicao RendaFixaHistorico
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                         posicaoBMFHistoricoCollection.Query.Serie.Equal(serie));

                posicaoBMFHistoricoCollection.Query.Load();

                foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
                {
                    posicaoBMFHistorico.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                    posicaoBMFHistorico.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
                }
                posicaoBMFHistoricoCollection.Save();
                #endregion

                #region Posicao BMFAbertura
                PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
                posicaoBMFAberturaCollection.Query.Where(posicaoBMFAberturaCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                         posicaoBMFAberturaCollection.Query.Serie.Equal(serie));

                posicaoBMFAberturaCollection.Query.Load();

                foreach (PosicaoBMFAbertura posicaoBMFAbertura in posicaoBMFAberturaCollection)
                {
                    posicaoBMFAbertura.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                    posicaoBMFAbertura.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
                }
                posicaoBMFAberturaCollection.Save();

                #endregion
            }

            ativoBMF.TipoMercado = Convert.ToByte(dropTipoMercado.SelectedItem.Value);
            ativoBMF.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

            if (textDataVencimento.Text.ToString() != "")
            {
                ativoBMF.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            }

            if (textPrecoExercicio.Text.ToString() != "") {
                ativoBMF.PrecoExercicio = Convert.ToDecimal(textPrecoExercicio.Text);
            }

            if (!String.IsNullOrEmpty(textCodigoFeeder.Text.ToString())) {
                ativoBMF.CodigoFeeder = textCodigoFeeder.Text.ToString().Trim();
            }
            else {
                ativoBMF.CodigoFeeder = null;
            }

            ativoBMF.Peso = Convert.ToDecimal(textPeso.Text);
            ativoBMF.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);

            if (textCodigoIsin.Text.ToString() != "") {
                ativoBMF.CodigoIsin = textCodigoIsin.Text.Trim();
            }

            if (dropEstrategia.Text != "")
            {
                ativoBMF.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
            }
            else
            {
                ativoBMF.IdEstrategia = null;
            }
            
            if (textCodigoCusip.Text.ToString() != "") 
                ativoBMF.CodigoCusip = textCodigoCusip.Text;

            int codicoCda = 0;
            if (int.TryParse(textCodigoCDA.Text.ToString(),out codicoCda))
                ativoBMF.CodigoCDA = codicoCda;

            int codigoBds = 0;
            if (int.TryParse(textCodigoBDS.Text.ToString(), out codigoBds))
                ativoBMF.CodigoBDS = codigoBds;

            if (dropInvestimentoColetivoCvm.SelectedIndex != -1)
            {
                ativoBMF.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
            }

            ativoBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AtivoBMF - Operacao: Update AtivoBMF: " + cdAtivoBMF + "; " + serie + UtilitarioWeb.ToString(ativoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(AtivoBMFMetadata.ColumnNames.CdAtivoBMF);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(AtivoBMFMetadata.ColumnNames.Serie);
            for (int i = 0; i < keyValues1.Count; i++)
            {
                string cdAtivoBMF = Convert.ToString(keyValues1[i]);
                string serie = Convert.ToString(keyValues2[i]);

                AtivoBMF ativoBMF = new AtivoBMF();
                if (ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                {
                    bool achou = false;
                    PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
                    posicaoBMFAberturaCollection.Query.Where(posicaoBMFAberturaCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                             posicaoBMFAberturaCollection.Query.Serie.Equal(serie));
                    if (posicaoBMFAberturaCollection.Query.Load())
                    {
                        achou = true;
                    }
                    else
                    {
                        OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
                        operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                          operacaoBMFCollection.Query.Serie.Equal(serie));
                        if (operacaoBMFCollection.Query.Load())
                        {
                            achou = true;
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Ativo " + cdAtivoBMF + serie + " não pode ser excluído por ter posições e/ou movimentos relacionadas.");
                    }

                    AtivoBMF ativoBMFClone = (AtivoBMF)Utilitario.Clone(ativoBMF);
                    //
                    ativoBMF.MarkAsDeleted();
                    ativoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AtivoBMF - Operacao: Delete AtivoBMF: " + cdAtivoBMF + "; " + serie + UtilitarioWeb.ToString(ativoBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxTextBox textCdAtivoBMF = gridCadastro.FindEditFormTemplateControl("textCdAtivoBMF") as ASPxTextBox;
            e.Properties["cpTextCdAtivoBMF"] = textCdAtivoBMF.ClientID;

            ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
            e.Properties["cpTextDataVencimento"] = textDataVencimento.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textCdAtivoBMF", "textDataVencimento");
        base.gridCadastro_PreRender(sender, e);
    }
}
