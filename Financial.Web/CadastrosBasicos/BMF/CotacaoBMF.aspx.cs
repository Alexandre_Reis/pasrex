﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Web.Common;

using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Interfaces.Import.Offshore;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_CotacaoBMF : CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBMF = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotacaoBMFCollection coll = new CotacaoBMFCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        TextBox textCdAtivo = popupFiltro.FindControl("textCdAtivo") as TextBox;
        TextBox textSerie = popupFiltro.FindControl("textSerie") as TextBox;

        if (textDataInicio.Text != "") 
        {
            coll.Query.Where(coll.Query.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") 
        {
            coll.Query.Where(coll.Query.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (textCdAtivo.Text != "")
        {
            coll.Query.Where(coll.Query.CdAtivoBMF.Equal(textCdAtivo.Text));
        }

        if (textSerie.Text != "")
        {
            coll.Query.Where(coll.Query.Serie.Equal(textSerie.Text));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending, coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        string cdAtivoBMF = "";
        string serie = "";
        e.Result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(e.Parameter));
            string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(e.Parameter));
            ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

            if (ativoBMF.str.CdAtivoBMF != "")
            {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                e.Result = cdAtivoBMF + "|" + serie;
            }            
        }

    }
    
    protected void callbackImportar_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textDataInicioImporta.Value == "")
        {
            e.Result = "Preencha a Data Inicio.";
            return;
        }
        else
            if (textDataFimImporta.Value == "")
            {
                e.Result = "Preencha a Data Fim.";
                return;
            }
            else
            {
                string[] lines = textImportacao.Text.Split(new string[] { "\n" }, StringSplitOptions.None);

                List<string> ativos = new List<string>();
                List<string> especs = new List<string>();

                for (int c = 0; c < lines.Length; c++)
                {
                    string[] str = lines[c].Split(';');

                    for (int d = 0; d < str.Length; d++)
                    {
                        AtivoBMF ativoBMF = new AtivoBMF();
                        ativoBMF.LoadByPrimaryKey(str[d].Substring(0, 3), str[d].Substring(3, str[d].Length - 3));

                        string[] codigoFeeder = ativoBMF.CodigoFeeder.Split(':');
                        ativos.Add(codigoFeeder[0]);
                        especs.Add(codigoFeeder[1]);
                    }
                }

                CotacaoBloomberg cotacaoBloombergExec = new CotacaoBloomberg();
                List<CotacaoBloomberg> listaCotacao = cotacaoBloombergExec.CarregaCotacaoBloomberg(ativos, especs, Convert.ToDateTime(textDataInicioImporta.Value), Convert.ToDateTime(textDataFimImporta.Value));

                CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();

                for (int c = 0; c < listaCotacao.Count; c++)
                {
                    CotacaoBMF cotBMF = new CotacaoBMF();
                    if (!cotBMF.LoadByPrimaryKey(listaCotacao[c].Data, listaCotacao[c].CdAtivo.Substring(0, 3), listaCotacao[c].CdAtivo.Substring(3, listaCotacao[c].CdAtivo.Length - 3)))
                    {
                        CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();

                        cotacaoBMF.Data = listaCotacao[c].Data;
                        cotacaoBMF.CdAtivoBMF = listaCotacao[c].CdAtivo.Substring(0, 3);
                        cotacaoBMF.Serie = listaCotacao[c].CdAtivo.Substring(3, listaCotacao[c].CdAtivo.Length - 3);
                        cotacaoBMF.PUMedio = listaCotacao[c].Pu;
                        cotacaoBMF.PUFechamento = listaCotacao[c].Pu;
                    }
                }
                cotacaoBMFCollection.Save();
            }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBMF);
        controles.Add(textData);
        controles.Add(textPUFechamento);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            AtivoBMF ativoBMF = new AtivoBMF();
            string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text));
            string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text));
            DateTime data = Convert.ToDateTime(textData.Text);

            CotacaoBMF cotacaoBMF = new CotacaoBMF();
            if (cotacaoBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).ClientEnabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditAtivoBMF_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).ClientEnabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textPUGerencial_Load(object sender, EventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.CalculaGerencial);
        clienteCollection.Query.Where(clienteCollection.Query.CalculaGerencial.Equal("S"));
        clienteCollection.Query.Load();

        if (gridCadastro.IsNewRowEditing || clienteCollection.Count == 0)
        {
            (sender as ASPxSpinEdit).Visible = false;

            Label labelPUGerencial = gridCadastro.FindEditFormTemplateControl("labelPUGerencial") as Label;
            labelPUGerencial.Visible = false;
        }        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(CotacaoBMFMetadata.ColumnNames.Data));
            string cdAtivoBMF = Convert.ToString(e.GetListSourceFieldValue(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF));
            string serie = Convert.ToString(e.GetListSourceFieldValue(CotacaoBMFMetadata.ColumnNames.Serie));
            e.Value = data + cdAtivoBMF + serie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CotacaoBMF cotacaoBMF = new CotacaoBMF();

        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUMedio = gridCadastro.FindEditFormTemplateControl("textPUMedio") as ASPxSpinEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textPUCorrigido = gridCadastro.FindEditFormTemplateControl("textPUCorrigido") as ASPxSpinEdit;
        ASPxSpinEdit textPUGerencial = gridCadastro.FindEditFormTemplateControl("textPUGerencial") as ASPxSpinEdit;

        AtivoBMF ativoBMF = new AtivoBMF();
        string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text));
        string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text));
        DateTime data = Convert.ToDateTime(textData.Text);

        decimal PUMedio = textPUMedio.Text.ToString() != ""
                    ? Convert.ToDecimal(textPUMedio.Text) : 0;
        //
        decimal PUFechamento = textPUFechamento.Text.ToString() != ""
                              ? Convert.ToDecimal(textPUFechamento.Text) : 0;
        //
        decimal PUCorrigido = textPUCorrigido.Text.ToString() != ""
            ? Convert.ToDecimal(textPUCorrigido.Text) : 0;

        if (cotacaoBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie)) {
            cotacaoBMF.PUMedio = PUMedio;
            cotacaoBMF.PUFechamento = PUFechamento;
            cotacaoBMF.PUCorrigido = PUCorrigido;

            if (textPUGerencial != null && !String.IsNullOrEmpty(textPUGerencial.Text))
            {
                cotacaoBMF.PUGerencial = Convert.ToDecimal(textPUGerencial.Text);
            }

            cotacaoBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoBMF - Operacao: Update CotacaoBMF: " + data + "; " + cdAtivoBMF + "; " + serie + UtilitarioWeb.ToString(cotacaoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SalvarNovo()
    {
        CotacaoBMF cotacaoBMF = new CotacaoBMF();

        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUMedio = gridCadastro.FindEditFormTemplateControl("textPUMedio") as ASPxSpinEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textPUCorrigido = gridCadastro.FindEditFormTemplateControl("textPUCorrigido") as ASPxSpinEdit;

        AtivoBMF ativoBMF = new AtivoBMF();
        string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text));
        string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text));
        DateTime data = Convert.ToDateTime(textData.Text);

        decimal PUMedio = textPUMedio.Text.ToString() != ""
            ? Convert.ToDecimal(textPUMedio.Text) : 0;
        //
        decimal PUFechamento = textPUFechamento.Text.ToString() != ""
                              ? Convert.ToDecimal(textPUFechamento.Text) : 0;
        //
        decimal PUCorrigido = textPUCorrigido.Text.ToString() != ""
            ? Convert.ToDecimal(textPUCorrigido.Text) : 0;

        cotacaoBMF.Data = data;
        cotacaoBMF.CdAtivoBMF = cdAtivoBMF.Trim().ToUpper();
        cotacaoBMF.Serie = serie.Trim().ToUpper();
        cotacaoBMF.PUMedio = PUMedio;
        cotacaoBMF.PUFechamento = PUFechamento;
        cotacaoBMF.PUCorrigido = PUCorrigido;
        cotacaoBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoBMF - Operacao: Insert CotacaoBMF: " + data + "; " + cdAtivoBMF + "; " + serie + UtilitarioWeb.ToString(cotacaoBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesCdAtivo = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF);
            List<object> keyValuesSerie = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.Serie);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++) {
                string cdAtivoBMF = Convert.ToString(keyValuesCdAtivo[i]);
                string serie = Convert.ToString(keyValuesSerie[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                if (cotacaoBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie)) {

                    //
                    CotacaoBMF cotacaoBMFClone = (CotacaoBMF)Utilitario.Clone(cotacaoBMF);
                    //
                    
                    cotacaoBMF.MarkAsDeleted();
                    cotacaoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoBMF - Operacao: Delete CotacaoBMF: " + data + "; " + cdAtivoBMF + "; " + serie + UtilitarioWeb.ToString(cotacaoBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnCopy")
        {
            List<object> keyValuesCdAtivo = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF);
            List<object> keyValuesSerie = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.Serie);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoBMFMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++)
            {                
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                string cdAtivo = keyValuesCdAtivo[i].ToString();
                string serie = keyValuesSerie[i].ToString();
                DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1);

                CotacaoBMF cotacaoBMFOrig = new CotacaoBMF();
                cotacaoBMFOrig.LoadByPrimaryKey(data, cdAtivo, serie);

                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                if (!cotacaoBMF.LoadByPrimaryKey(dataProxima, cdAtivo, serie))
                {
                    cotacaoBMF = new CotacaoBMF();
                    foreach (esColumnMetadata colCotacaoBMF in cotacaoBMF.es.Meta.Columns)
                    {
                        if (colCotacaoBMF.Name.ToUpper().Equals("DATA"))
                            cotacaoBMF.Data = dataProxima;
                        else
                            cotacaoBMF.SetColumn(colCotacaoBMF.Name, cotacaoBMFOrig.GetColumn(colCotacaoBMF.Name));
                    }

                    cotacaoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoBMF - Operacao: Copy CotacaoBMF: " + data + "; " + UtilitarioWeb.ToString(cotacaoBMF),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
                else
                {
                    throw new Exception("Ativo " + cotacaoBMF.CdAtivoBMF + "-" + cotacaoBMF.Serie + " já tem cotação definida na data " + dataProxima.ToShortDateString() + ".");
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {
        this.FocaCampoGrid("textData", "textPUFechamento");
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        
        if (textDataFim.Text != "") 
        {
            if (texto.ToString() != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        if (textCdAtivo.Text != "")
        {
            if (texto.ToString() != "")
                texto.Append(" |");

            texto.Append(" Ativo = ").Append(textCdAtivo.Text);
        }

        if (textSerie.Text != "")
        {
            if (texto.ToString() != "")
                texto.Append(" |");

            texto.Append(" Série = ").Append(textSerie.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
            e.Properties["cpTextPUFechamento"] = textPUFechamento.ClientID;
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e) {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CdAtivoBMF" }));
            string serie = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Serie" }));

            btnEditAtivoBMF.Text = cdAtivoBMF + "-" + serie;
        }
    }
}