﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_OrdemBMF : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBMF = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { OrdemBMFMetadata.ColumnNames.TipoOrdem }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOrdemBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxComboBox dropTraderFiltro = popupFiltro.FindControl("dropTraderFiltro") as ASPxComboBox;

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OrdemBMFQuery ordemBMFQuery = new OrdemBMFQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        //
        AgenteMercadoQuery agenteQuery = new AgenteMercadoQuery("A");
        TraderQuery traderQuery = new TraderQuery("T");
        //
        ordemBMFQuery.Select(ordemBMFQuery, clienteQuery.Apelido.As("Apelido"), agenteQuery.Nome, traderQuery.Nome);
        //
        ordemBMFQuery.InnerJoin(clienteQuery).On(ordemBMFQuery.IdCliente == clienteQuery.IdCliente);
        ordemBMFQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        ordemBMFQuery.InnerJoin(agenteQuery).On(ordemBMFQuery.IdAgenteCorretora == agenteQuery.IdAgente);
        ordemBMFQuery.LeftJoin(traderQuery).On(ordemBMFQuery.IdTrader == traderQuery.IdTrader);

        ordemBMFQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            ordemBMFQuery.Where(ordemBMFQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            ordemBMFQuery.Where(ordemBMFQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            ordemBMFQuery.Where(ordemBMFQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text))
        {
            ordemBMFQuery.Where(ordemBMFQuery.CdAtivoBMF.Like("%" + textCdAtivo.Text + "%"));
        }

        if (!String.IsNullOrEmpty(textSerie.Text))
        {
            ordemBMFQuery.Where(ordemBMFQuery.Serie.Like("%" + textSerie.Text + "%"));
        }

        if (dropTraderFiltro.SelectedIndex != -1) {
            ordemBMFQuery.Where(ordemBMFQuery.IdTrader.Equal(dropTraderFiltro.SelectedItem.Value));
        }

        ordemBMFQuery.OrderBy(ordemBMFQuery.Data.Descending,
                              agenteQuery.Nome.Ascending,
                              traderQuery.Nome.Ascending);

        OrdemBMFCollection coll = new OrdemBMFCollection();
        coll.Load(ordemBMFQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoLiquidante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        //Busca do web.config o flag de agente liquidação exclusivo
        //bool agenteLiquidacaoExclusivo = Convert.ToBoolean(WebConfig.AppSettings.AgenteLiquidacaoExclusivo);
        bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;

        if (!agenteLiquidacaoExclusivo) {
            Label labelAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("labelAgenteLiquidacao") as Label;
            ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
            labelAgenteLiquidacao.Visible = false;
            dropAgenteLiquidacao.Visible = false;
        }

        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            //TipoMercado, Peso
            string cdAtivoBMF = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBMF").ToString();
            string serie = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Serie").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;

            AtivoBMF ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

            string tipoMercado = TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(ativoBMF.TipoMercado));
            string peso = ativoBMF.Peso.ToString();

            textPeso.Text = peso;
            //

            //CheckBox de custos informados            
            CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
            string calculaDespesas = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CalculaDespesas").ToString();
            
            chkCustosInformados.Checked = calculaDespesas == "S" ? false : true;

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropAgenteCorretora);
            controles.Add(dropTipoOrdem);
            controles.Add(textData);
            controles.Add(btnEditAtivoBMF);
            controles.Add(textQuantidade);
            controles.Add(dropLocalNegociacao);
            controles.Add(dropClearing);
            controles.Add(dropLocalCustodia);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            string[] ativo = btnEditAtivoBMF.Text.Split('-');
            if ((ativo[0] == "DI1" || ativo[0] == "DDI") && Convert.ToDecimal(textPU.Text) == 0)
            {
                if (gridCadastro.IsNewRowEditing)
                {
                    controles.Add(textTaxa);
                }
                else
                {
                    controles.Add(textPU);
                    controles.Add(textValor);
                }
            }
            else
            {
                controles.Add(textPU);
                controles.Add(textValor);
            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data da operação não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }
            // Se Data está Preenchida e Existe Ativo compara a Data de Vencimento
            if (!String.IsNullOrEmpty(textData.Text)) {
                AtivoBMF a = new AtivoBMF();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(a.Query.DataVencimento);
                string cdAtivo = ativo[0];
                string serie = ativo[1];

                if (a.LoadByPrimaryKey(campos, cdAtivo, serie)) {
                    if (a.DataVencimento.HasValue && a.DataVencimento < Convert.ToDateTime(textData.Text)) {
                        e.Result = "Ativo Vencido em: " + a.DataVencimento.Value.ToString("d");
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {
                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                        ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                        : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBMF = "";
        string serie = "";
        string peso = "";
        e.Result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") 
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(e.Parameter));
            string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(e.Parameter));
            ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

            if (ativoBMF.str.CdAtivoBMF != "") {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                peso = ativoBMF.Peso.ToString();

                string idLocalCustodia = ativoBMF.IdLocalCustodia.Value.ToString();
                string idLocalNegociacao = ativoBMF.IdLocalNegociacao.Value.ToString();
                string idClearing = ativoBMF.IdClearing.Value.ToString();

                e.Result = cdAtivoBMF + serie + "|" + peso + "|" + cdAtivoBMF + "|" + ativoBMF.TipoMercado.Value + "|" + idLocalCustodia + "|" + idLocalNegociacao + "|" + idClearing;
            }            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCustos_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        if (btnEditCodigoCustos.Text == "" ||
            textCorretagem.Text == "" ||
            textTaxas.Text == "") {
            e.Result = "Os campos precisam ser preenchidos corretamente.";
            return;
        }

        int idCliente = Convert.ToInt32(btnEditCodigoCustos.Text);
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.Status);
        if (!cliente.LoadByPrimaryKey(campos, idCliente)) {
            e.Result = "Cliente " + idCliente.ToString() + " inexistente.";
            return;
        }
        else if (cliente.Status.Value == (byte)StatusCliente.Divulgado) {
            e.Result = "Cliente " + idCliente.ToString() + " está fechado. Processo não pode ser executado.";
            return;
        }

        decimal corretagem = Convert.ToDecimal(textCorretagem.Text);
        decimal taxas = Convert.ToDecimal(textTaxas.Text);
        DateTime data = cliente.DataDia.Value;

        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOrdem");
        List<int> listaIds = new List<int>();
        for (int i = 0; i < keyValuesId.Count; i++)
        {
            int idOrdem = Convert.ToInt32(keyValuesId[i]);
            OrdemBMF ordemBMFCheck = new OrdemBMF();
            ordemBMFCheck.LoadByPrimaryKey(idOrdem);
            if (ordemBMFCheck.Data.Value != data)
            {
                e.Result = "Existem ordens selecionadas com data diferente da data atual do cliente. Processo não pode ser executado.";
                return;
            }

            listaIds.Add(idOrdem);
        }

        OrdemBMF ordemBMF = new OrdemBMF();
        if (listaIds.Count == 0)
        {
            ordemBMF.RateiaTaxasTudo(idCliente, data, corretagem, taxas);
        }
        else
        {
            ordemBMF.RateiaTaxasTudoPorIds(listaIds, corretagem, taxas);
        }

        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        bool ok = true;
        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOrdem");
        for (int i = 0; i < keyValuesId.Count; i++) {
            int idOrdem = Convert.ToInt32(keyValuesId[i]);

            OrdemBMF ordemBMF = new OrdemBMF();
            if (ordemBMF.LoadByPrimaryKey(idOrdem)) {
                int idCliente = ordemBMF.IdCliente.Value;
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                campos.Add(cliente.Query.Status);
                cliente.LoadByPrimaryKey(campos, idCliente);
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;

                if (DateTime.Compare(dataDia, ordemBMF.Data.Value) > 0 || status == (byte)StatusCliente.Divulgado)
                {
                    ok = false;
                }
                else {
                    if (dropAgenteLote.SelectedIndex > -1) {
                        ordemBMF.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLote.SelectedItem.Value);
                    }
                    if (dropTraderLote.SelectedIndex > -1) {
                        ordemBMF.IdTrader = Convert.ToInt32(dropTraderLote.SelectedItem.Value);
                    }
                    if (textPercentualLote.Text != "") {
                        ordemBMF.PercentualDesconto = Convert.ToDecimal(textPercentualLote.Text);
                    }
                    if (dropInformaCustos.SelectedIndex > -1)
                    {
                        if (dropInformaCustos.SelectedItem.Value.ToString() == "S")
                        {
                            ordemBMF.CalculaDespesas = "N";
                        }
                        else
                        {
                            ordemBMF.CalculaDespesas = "S";
                        }
                    }
                }

                ordemBMF.Save();
            }
        }

        if (ok) {
            e.Result = "Processo executado com sucesso.";
        }
        else {
            e.Result = "Cliente(s) com data dia posterior à data da ordem a ser alterada ou com status fechado. Processo executado parcialmente.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

        ASPxSpinEdit textTaxaClearingVlFixo = gridCadastro.FindEditFormTemplateControl("textTaxaClearingVlFixo") as ASPxSpinEdit;
        ASPxSpinEdit textEmolumentos = gridCadastro.FindEditFormTemplateControl("textEmolumentos") as ASPxSpinEdit;
        ASPxSpinEdit textRegistro = gridCadastro.FindEditFormTemplateControl("textRegistro") as ASPxSpinEdit;
        ASPxSpinEdit textCorretagem = gridCadastro.FindEditFormTemplateControl("textCorretagem") as ASPxSpinEdit;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;

        OrdemBMF ordemBMF = new OrdemBMF();
        if (ordemBMF.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ordemBMF.IdCliente = idCliente;
            AtivoBMF ativoBMF = new AtivoBMF();
            string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
            string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
            ordemBMF.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);

            //Busca do web.config o flag de agente liquidação exclusivo
            bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;

            ordemBMF.IdAgenteLiquidacao = agenteLiquidacaoExclusivo
                                          ? Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value)
                                          : Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
            byte tipoMercado = ativoBMF.TipoMercado.Value;

            ordemBMF.CdAtivoBMF = cdAtivoBMF;
            ordemBMF.Serie = serie;
            ordemBMF.TipoMercado = tipoMercado;
            ordemBMF.TipoOrdem = Convert.ToString(dropTipoOrdem.SelectedItem.Value);

            decimal pu = Convert.ToDecimal(textPU.Text);
            if (ordemBMF.Pu.Value != pu)
                ordemBMF.Taxa = null;

            ordemBMF.Pu = pu;
            ordemBMF.Valor = Convert.ToDecimal(textValor.Text);
            ordemBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);

            DateTime data = Convert.ToDateTime(textData.Text);
            ordemBMF.Data = Convert.ToDateTime(textData.Text.ToString());
            ordemBMF.Origem = (byte)OrigemOrdemBMF.Primaria;
            ordemBMF.Fonte = (byte)FonteOrdemBMF.Manual;

            if (dropTrader.SelectedItem != null) {
                ordemBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            }
            else {
                ordemBMF.IdTrader = null; // Vazio
            }

            ordemBMF.PercentualDesconto = textPercentual.Text != "" ? Convert.ToDecimal(textPercentual.Text): 0;

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                ordemBMF.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                ordemBMF.IdCategoriaMovimentacao = null;
            }

            //Trata Custos Informados
            ordemBMF.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";
            ordemBMF.IdMoeda = ativoBMF.IdMoeda.Value;

            ordemBMF.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
            ordemBMF.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
            ordemBMF.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
            ordemBMF.DataOperacao = Convert.ToDateTime(textData.Text.ToString());

            if (!string.IsNullOrEmpty(textTaxaClearingVlFixo.Text))
                ordemBMF.TaxaClearingVlFixo = Convert.ToDecimal(textTaxaClearingVlFixo.Text);
            else
                ordemBMF.TaxaClearingVlFixo = 0;

            if (!string.IsNullOrEmpty(textEmolumentos.Text))
                ordemBMF.Emolumento = Convert.ToDecimal(textEmolumentos.Text);
            else
                ordemBMF.Emolumento = 0;

            if (!string.IsNullOrEmpty(textRegistro.Text))
                ordemBMF.Registro = Convert.ToDecimal(textRegistro.Text);
            else
                ordemBMF.Registro = 0;

            if (!string.IsNullOrEmpty(textCorretagem.Text))
                ordemBMF.Corretagem = Convert.ToDecimal(textCorretagem.Text);
            else
                ordemBMF.Corretagem = 0;

            ordemBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemBMF - Operacao: Update OrdemBMF: " + idOperacao + UtilitarioWeb.ToString(ordemBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

        ASPxSpinEdit textTaxaClearingVlFixo = gridCadastro.FindEditFormTemplateControl("textTaxaClearingVlFixo") as ASPxSpinEdit;
        ASPxSpinEdit textEmolumentos = gridCadastro.FindEditFormTemplateControl("textEmolumentos") as ASPxSpinEdit;
        ASPxSpinEdit textRegistro = gridCadastro.FindEditFormTemplateControl("textRegistro") as ASPxSpinEdit;
        ASPxSpinEdit textCorretagem = gridCadastro.FindEditFormTemplateControl("textCorretagem") as ASPxSpinEdit;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        OrdemBMF ordemBMF = new OrdemBMF();
        ordemBMF.IdCliente = idCliente;
        AtivoBMF ativoBMF = new AtivoBMF();
        string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
        string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
        ordemBMF.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);

        //Busca do web.config o flag de agente liquidação exclusivo
        //bool agenteLiquidacaoExclusivo = Convert.ToBoolean(WebConfig.AppSettings.AgenteLiquidacaoExclusivo);
        bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;

        ordemBMF.IdAgenteLiquidacao = agenteLiquidacaoExclusivo 
                        ? Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value)
                        : Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);

        ativoBMF = new AtivoBMF();
        ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
        byte tipoMercado = ativoBMF.TipoMercado.Value;

        ordemBMF.CdAtivoBMF = cdAtivoBMF;
        ordemBMF.Serie = serie;
        ordemBMF.TipoMercado = tipoMercado;
        ordemBMF.TipoOrdem = Convert.ToString(dropTipoOrdem.SelectedItem.Value).Trim();
        ordemBMF.Pu = Convert.ToDecimal(textPU.Text);
        ordemBMF.Valor = Convert.ToDecimal(textValor.Text);
        ordemBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);

        DateTime data = Convert.ToDateTime(textData.Text);
        ordemBMF.Data = data;

        decimal taxa = 0;
        if(!string.IsNullOrEmpty(textTaxa.Text))
            taxa = Convert.ToDecimal(textTaxa.Text);

        if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro && (ativoBMF.CdAtivoBMF == "DI1" || ativoBMF.CdAtivoBMF == "DDI") && taxa > 0)
        {            
            if (ativoBMF.CdAtivoBMF == "DI1")
            {
                AtivoBMF ativoBMFPrazo = new AtivoBMF();
                int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasUteisBrasil(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);
                decimal fator = 1M + (taxa / 100M);
                decimal expoente = numeroDiasVencimento / 252M;
                decimal fatorDesconto = (decimal)Math.Pow((double)fator, (double)expoente);
                decimal puCalculado = Math.Round(100000M / fatorDesconto, 2);
                ordemBMF.Pu = puCalculado;
            }
            else if (ativoBMF.CdAtivoBMF == "DDI")
            {
                AtivoBMF ativoBMFPrazo = new AtivoBMF();
                int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasCorridos(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);

                decimal puCalculado = Math.Round(100000M / (1M + (taxa * numeroDiasVencimento / 36000M)), 2);
                ordemBMF.Pu = puCalculado;
            }

            ordemBMF.TipoOrdem = ordemBMF.TipoOrdem == TipoOrdemBMF.Compra ? TipoOrdemBMF.Venda : TipoOrdemBMF.Compra;
            ordemBMF.Valor = ordemBMF.Quantidade.Value * ordemBMF.Pu.Value;
            ordemBMF.Taxa = taxa;
        }
        else
        {
            ordemBMF.Pu = Convert.ToDecimal(textPU.Text);
            ordemBMF.Valor = Convert.ToDecimal(textValor.Text);
            ordemBMF.Taxa = null;
        }
                      
        ordemBMF.Origem = (byte)OrigemOrdemBMF.Primaria;
        ordemBMF.Fonte = (byte)FonteOrdemBMF.Manual;

        if (dropTrader.SelectedItem != null) {
            ordemBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        }
        else {
            ordemBMF.IdTrader = null; // Vazio
        }

        ordemBMF.QuantidadeDayTrade = 0;
        ordemBMF.CustoWtr = 0;
        ordemBMF.PontaEstrategia = 0;
        ordemBMF.IdMoeda = ativoBMF.IdMoeda.Value;
        ordemBMF.PercentualDesconto = textPercentual.Text != "" ? Convert.ToDecimal(textPercentual.Text) : 0;

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
        {
            ordemBMF.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            ordemBMF.IdCategoriaMovimentacao = null;
        }

        //Trata Custos Informados
        ordemBMF.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";

        ordemBMF.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
        ordemBMF.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
        ordemBMF.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
        ordemBMF.DataOperacao = Convert.ToDateTime(textData.Text.ToString());

        if (!string.IsNullOrEmpty(textTaxaClearingVlFixo.Text))
            ordemBMF.TaxaClearingVlFixo = Convert.ToDecimal(textTaxaClearingVlFixo.Text);
        else
            ordemBMF.TaxaClearingVlFixo = 0;

        if(!string.IsNullOrEmpty(textEmolumentos.Text))
            ordemBMF.Emolumento = Convert.ToDecimal(textEmolumentos.Text);
        else
            ordemBMF.Emolumento = 0;

        if(!string.IsNullOrEmpty(textRegistro.Text))
            ordemBMF.Registro = Convert.ToDecimal(textRegistro.Text);
        else
            ordemBMF.Registro = 0;

        if (!string.IsNullOrEmpty(textCorretagem.Text))
            ordemBMF.Corretagem = Convert.ToDecimal(textCorretagem.Text);
        else
            ordemBMF.Corretagem = 0;

        ordemBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OrdemBMF - Operacao: Insert OrdemBMF: " + ordemBMF.IdOrdem + UtilitarioWeb.ToString(ordemBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(OrdemBMFMetadata.ColumnNames.IdOrdem);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOrdem = Convert.ToInt32(keyValuesId[i]);

                OrdemBMF ordemBMF = new OrdemBMF();
                if (ordemBMF.LoadByPrimaryKey(idOrdem)) {
                    int idCliente = ordemBMF.IdCliente.Value;

                    //
                    OrdemBMF ordemBMFClone = (OrdemBMF)Utilitario.Clone(ordemBMF);
                    //

                    ordemBMF.MarkAsDeleted();
                    ordemBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OrdemBMF - Operacao: Delete OrdemBMF: " + idOrdem + UtilitarioWeb.ToString(ordemBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;
            e.Properties["cpTextPeso"] = textPeso.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        //e.NewValues["PercentualDesconto"] = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e) {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CdAtivoBMF" }));
            string serie = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Serie" }));

            btnEditAtivoBMF.Text = cdAtivoBMF + "-" + serie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;
        ASPxComboBox dropTraderFiltro = popupFiltro.FindControl("dropTraderFiltro") as ASPxComboBox;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTraderFiltro.SelectedIndex != -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Trader = ").Append(dropTraderFiltro.SelectedItem.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void textTaxa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
    }
    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

}