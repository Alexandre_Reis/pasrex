﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CopiaOperacoes.aspx.cs" Inherits="CopiaOperacoes" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cópia Operações" />
                            </div>
                            
                            <div id="mainContentSpace">
                                <br />
                                
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" align="center">
                                                                                                                                   
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Operação Bolsa" HeaderStyle-Font-Size="11px" Width="400px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">                                                                                                                                        
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Cliente Origem:"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropClienteOrigem" ClientInstanceName="dropClienteOrigem" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Cliente Destino:"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropClienteDestino" ClientInstanceName="dropClienteDestino" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false" />                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>
                                                                                                                                        
                                                                    <tr>
                                                                        <td class="td_Label">                
                                                                            <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                                        </td>                        
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataInicioBolsa" runat="server" ClientInstanceName="dataInicioBolsa"/>
                                                                        </td>  
                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Fim:"/>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataFimBolsa" runat="server" ClientInstanceName="dataFimBolsa"/>                                                                        
                                                                        </td>                                                                                  
                                                                    </tr>

                                                                    <tr>
                                                                    <td>
                                                                        <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo Oper.:"/>
                                                                    </td>
                                                                                 
                                                                    <td colspan="3">
                                                                        <dxe:ASPxComboBox ID="dropTipoOperacaoBolsa" runat="server" CssClass="dropDownListCurto">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="C" Text="Compra" />
                                                                            <dxe:ListEditItem Value="V" Text="Venda" />
                                                                            <dxe:ListEditItem Value="CD" Text="Compra DT" />
                                                                            <dxe:ListEditItem Value="VD" Text="Venda DT" />
                                                                            <dxe:ListEditItem Value="DE" Text="Depósito" />
                                                                            <dxe:ListEditItem Value="RE" Text="Retirada" />                                        
                                                                        </Items>                                        
                                                                        </dxe:ASPxComboBox>                 
                                                                    </td>
                                                                    </tr>                                                                    
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelNormal" Text="Mercado:"></asp:Label>
                                                                        </td>

                                                                        <td colspan="3">
                                                                            <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" CssClass="dropDownListCurto">
                                                                                <Items>
                                                                                <dxe:ListEditItem Value="VIS" Text="Vista" />
                                                                                <dxe:ListEditItem Value="OPC" Text="Opção Compra" />
                                                                                <dxe:ListEditItem Value="OPV" Text="Opção Venda" />
                                                                                <dxe:ListEditItem Value="TER" Text="Termo" />
                                                                                <dxe:ListEditItem Value="FUT" Text="Futuro" />
                                                                                <dxe:ListEditItem Value="IMO" Text="Imobilário" />
                                                                                </Items>                                                                                    
                                                                            </dxe:ASPxComboBox>                            
                                                                        </td>
                                                                                                               
                                                                   </tr>
                                                                   
                                                                   <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                                                                        </td>
                                                                                                                                                
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropCodigoAtivo" ClientInstanceName="dropCodigoAtivo" runat="server" KeyFieldName="CdAtivoBolsa" DataSourceID="EsDSAtivoBolsa"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" >
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="CdAtivoBolsa" Width="10%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Descricao" CellStyle-Font-Size="X-Small" Width="25%"/>
                                                                                 <dx:GridViewDataColumn FieldName="TipoMercado" CellStyle-Font-Size="X-Small" Width="10%"/>
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>                                                                    
                                                                    
                                                                    <tr>
                                                                    <td colspan="4">
                                                                        <div class="linkButton linkButtonNoBorder" style="padding-left:200pt" >
                                                                            <div class="linkButtonWrapper">
                                                                                    <asp:LinkButton ID="copyBolsa" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                        OnClick="btnCopiarBolsa_Click"><asp:Literal ID="Literal1" runat="server" Text="Copiar" /><div></div></asp:LinkButton>
                                                                            </div>     
                                                                        </div>                                                                    
                                                                    </td>                                                                    
                                                                    </tr>                                                                                                                                        
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>                                                                                                    
                                                </td>
                                                <td width="20"></td>
                                                <td>                                                    
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Operação Renda Fixa" HeaderStyle-Font-Size="11px" Width="400px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <%--<div style="height:2px"></div>--%>
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">

                                                                      <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Cliente Origem:"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropClienteRendaFixaOrigem" ClientInstanceName="dropClienteRendaFixaOrigem" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Cliente Destino:"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropClienteRendaFixaDestino" ClientInstanceName="dropClienteRendaFixaDestino" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false" />                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="td_Label">                
                                                                            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                                        </td>                        
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataInicioRendaFixa" runat="server" ClientInstanceName="textDataInicio"/>
                                                                        </td>  
                                                                        
                                                                        <td>                                                                        
                                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Fim:"/>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataFimRendaFixa" runat="server" ClientInstanceName="textDataFim"/>
                                                                        </td>                                                                                  
                                                                    </tr>
                                                                    
                                                                     <tr>                                                                    
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Id Titulo:"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3">

                                                                            <dx:ASPxGridLookup ID="dropTitulo" ClientInstanceName="dropTitulo" runat="server" SelectionMode="Multiple" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixa"
                                                                                               Width="250px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" AllowUserInput="true">
                                                                                 <Columns>
                                                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                     <dx:GridViewDataColumn FieldName="IdTitulo" Caption="Id" Width="10%" CellStyle-Font-Size="X-Small"/>                         
                                                                                     <dx:GridViewDataColumn FieldName="Descricao" CellStyle-Font-Size="X-Small" Width="75%" />
                                                                                     <dx:GridViewDataDateColumn FieldName="DataVencimento" CellStyle-Font-Size="X-Small" Width="10%"/>
                                                                                </Columns>
                                                                                
                                                                                <GridViewProperties>
                                                                                    <Settings ShowFilterRow="True" />
                                                                                </GridViewProperties>
                                                                                
                                                                            </dx:ASPxGridLookup>

                                                                        </td>
                                                                    </tr>
                                                                                                                                                                                                            
                                                                    <tr>                                                                    
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Tipo Oper.:"/>
                                                                        </td>

                                                                        <td colspan="3">
                                                                            <dxe:ASPxComboBox ID="tipoOperacaoRendaFixa" runat="server" ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto">
                                                                            <Items>
                                                                                <dxe:ListEditItem Value="1" Text="Compra Final" />
                                                                                <dxe:ListEditItem Value="2" Text="Venda Final" />
                                                                            </Items>                                        
                                                                            </dxe:ASPxComboBox>                 
                                                                        </td>                                                                    
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <div class="linkButton linkButtonNoBorder" style="padding-left:200pt" >
                                                                                <div class="linkButtonWrapper">
                                                                                        <asp:LinkButton ID="copyRendaFixa" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                            OnClick="btnCopiarRendaFixa_Click"><asp:Literal ID="Literal2" runat="server" Text="Copiar" /><div></div></asp:LinkButton>
                                                                                </div>     
                                                                            </div>                                                                    
                                                                        </td>                                                                    
                                                                    </tr>
                                                                                                                                                                                                                                                                          
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>                                                                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <div style="height:12px"></div>
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderStyle-Font-Bold="true"                                                    
                                                        HeaderText="Operação Fundo" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                                                                    
                                                                       <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Carteira Origem:"/>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropCarteiraOrigemFundo" ClientInstanceName="dropCarteiraOrigemFundo" runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>                                            

                                                                     <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label12" runat="server" CssClass="labelRequired" Text="Carteira Destino:"/>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <dx:ASPxGridLookup ID="dropCarteiraDestinoFundo" ClientInstanceName="dropCarteiraDestinoFundo" runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dx:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dx:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                                        </td>
                                                                    </tr>                                            
                                                                                                                
                                                                    <tr>
                                                                        <td class="td_Label">                
                                                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"/>
                                                                        </td>                        
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataInicioFundo" runat="server" ClientInstanceName="dataInicioFundo"/>
                                                                        </td>  
                                                                        
                                                                        <td>                                                                        
                                                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="dataFimFundo" runat="server" ClientInstanceName="dataFimFundo"/>                                                                        
                                                                        </td>                                                                                  
                                                                    </tr>
                                    
                                                                   <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>                    
                                                                        </td>                    
                                                                        <td colspan="3">      
                                                                            <dxe:ASPxComboBox ID="dropTipoOperacaoFundo" runat="server" ClientInstanceName="dropTipoOperacaoFundo" 
                                                                                                ShowShadow="true" DropDownStyle="DropDownList" 
                                                                                                CssClass="dropDownListCurto" >
                                                                            <Items>
                                                                                <dxe:ListEditItem Value="1" Text="Aplicação" />
                                                                                <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                                                                                <dxe:ListEditItem Value="3" Text="Resgate Líquido" />
                                                                                <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                                                                                <dxe:ListEditItem Value="5" Text="Resgate Total" />
                                                                                <dxe:ListEditItem Value="10" Text="Aplic. Especial" />
                                                                                <dxe:ListEditItem Value="11" Text="Aplic. Ações" />
                                                                                <dxe:ListEditItem Value="12" Text="Resg. Especial" />
                                                                                <dxe:ListEditItem Value="20" Text="Come Cotas" />                        
                                                                                <dxe:ListEditItem Value="40" Text="Ajuste Posição" />
                                                                            </Items>
                                                                            </dxe:ASPxComboBox>                 
                                                                        </td>
                                                                    </tr>                
                                                                                                                                                                                                                                                                                      
                                                                    <tr>
                                                                    <td colspan="4">
                                                                        <div class="linkButton linkButtonNoBorder" style="padding-left:200pt" >                                            
                                                                                    <asp:LinkButton ID="btnSalvar" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                        OnClick="btnCopiarFundo_Click"><asp:Literal ID="Literal5" runat="server" Text="Copiar" /><div></div></asp:LinkButton>                                            
                                                                        </div>
                                                                    
                                                                    </td>                                                                    
                                                                    </tr>                                                                       
                                                                </table>                                                                
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>                                                    
                                                </td>
                                                <td width="20"></td>
                                                <td></td>
                                            </tr>                                            
                                        </table>                                                                                
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                
                                <br />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <cc1:esDataSource ID="EsDSTituloRendaFixa" runat="server" OnesSelect="EsDSTituloRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
        <!-- Fundo -->
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>