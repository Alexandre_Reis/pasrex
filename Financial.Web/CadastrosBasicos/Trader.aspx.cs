﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;

using Financial.Common;
using Financial.Util;

using DevExpress.Web;

using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Gerencial;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_Trader : CadastroBasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TraderQuery traderQuery = new TraderQuery("trader");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        TraderCollection coll = new TraderCollection();

        traderQuery.Select(traderQuery, carteiraQuery.Apelido.As("ApelidoCarteira"));
        traderQuery.LeftJoin(carteiraQuery).On(traderQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));

        traderQuery.OrderBy(traderQuery.Nome.Ascending);
        coll.Load(traderQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Trader trader = new Trader();
        int idTrader = Convert.ToInt32(e.Keys[0]);

        if (trader.LoadByPrimaryKey(idTrader)) {

            ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
            ASPxComboBox dropAlgorithmic = gridCadastro.FindEditFormTemplateControl("dropAlgorithmic") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

            trader.Nome = textNome.Text;
            trader.AlgoTrader = Convert.ToString(dropAlgorithmic.SelectedItem.Value);
            
			if(!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
				trader.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
			else
				trader.IdCarteira = null;

            trader.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Trader - Operacao: Update Trader: " + idTrader + UtilitarioWeb.ToString(trader),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTrader");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idTrader = Convert.ToInt32(keyValuesId[i]);

                Trader trader = new Trader();
                if (trader.LoadByPrimaryKey(idTrader)) {
                    OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
                    ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdTrader.Equal(idTrader));
                    if (ordemBolsaCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }
                    OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
                    ordemBMFCollection.Query.Where(ordemBolsaCollection.Query.IdTrader.Equal(idTrader));
                    if (ordemBMFCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }
                    CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
                    calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdTrader.Equal(idTrader));
                    if (calculoGerencialCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }

                    //
                    Trader traderClone = (Trader)Utilitario.Clone(trader);
                    //

                    trader.MarkAsDeleted();
                    trader.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Trader - Operacao: Delete Trader: " + idTrader + UtilitarioWeb.ToString(traderClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        Carteira carteira = new Carteira();

        string resultado = "";
        DateTime? dataDia = null;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {                    
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        resultado = carteira.str.Apelido;
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;

        ASPxTextBox hiddenIdTrader = gridCadastro.FindEditFormTemplateControl("hiddenIdTrader") as ASPxTextBox;
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxComboBox dropAlgorithmic = gridCadastro.FindEditFormTemplateControl("dropAlgorithmic") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textNome);
        controles.Add(dropAlgorithmic);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }


        if(!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
        {
            TraderCollection traderColl = new TraderCollection();
            traderColl.Query.Where(traderColl.Query.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));

            if (!string.IsNullOrEmpty(hiddenIdTrader.Text))
                traderColl.Query.Where(traderColl.Query.IdTrader.NotEqual(Convert.ToInt32(hiddenIdTrader.Text)));

            if (traderColl.Query.Load())
            {
                e.Result = "Já existe trader vinculado a Carteira - " + btnEditCodigoCarteira.Text + "!";
                return;
            }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        //
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        //
        carteiraQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));

        coll.Load(carteiraQuery);        

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
        
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxComboBox dropAlgorithmic = gridCadastro.FindEditFormTemplateControl("dropAlgorithmic") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        Trader trader = new Trader();

        trader.Nome = textNome.Text;
        trader.AlgoTrader = Convert.ToString(dropAlgorithmic.SelectedItem.Value);
		
		if(!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
			trader.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
		else
			trader.IdCarteira = null;
		
        trader.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Trader - Operacao: Insert Trader: " + trader.IdTrader + UtilitarioWeb.ToString(trader),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

}