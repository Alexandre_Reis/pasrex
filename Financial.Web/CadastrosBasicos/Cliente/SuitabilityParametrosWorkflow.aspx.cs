﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using Financial.Investidor;
using System.Globalization;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

public partial class CadastrosBasicos_Cliente_SuitabilityParametrosWorkflow : BasePage 
{
    SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();

    new protected void Page_Load(object sender, EventArgs e)
    {
        pnlSeguranca.HeaderText = String.Empty;
        suitabilityParametrosWorkflow.LoadByPrimaryKey(1);
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityParametrosWorkflowHistoricoCollection coll = new SuitabilityParametrosWorkflowHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e) {
        this.TrataErros();

        suitabilityParametrosWorkflow.AtivaWorkFlow = Convert.ToString(this.dropAtivaWorkflowSuitability.Value);

        if (suitabilityParametrosWorkflow.AtivaWorkFlow.Equals("N"))
        {
            suitabilityParametrosWorkflow.VerificaEnquadramento = null;
            suitabilityParametrosWorkflow.ResponsavelNome = null;
            suitabilityParametrosWorkflow.ResponsavelEmail = null;
            suitabilityParametrosWorkflow.ValorPeriodo = null;
            suitabilityParametrosWorkflow.GrandezaPeriodo = null;

            ASPxComboBox dropVerificacaoEnquadramento = pnlSeguranca.FindControl("dropVerificacaoEnquadramento") as ASPxComboBox;

            dropVerificacaoEnquadramento.SelectedIndex = -1;
            this.textNome.Text = "";
            this.textEmail.Text = "";
            this.txtValorPeriodo.Text = "";
            this.dropGrandezaPeriodo.Value = null;

        }
        else
        {
            suitabilityParametrosWorkflow.VerificaEnquadramento = Convert.ToInt16(this.dropVerificacaoEnquadramento.Value);
            suitabilityParametrosWorkflow.ResponsavelNome = this.textNome.Text;
            suitabilityParametrosWorkflow.ResponsavelEmail = this.textEmail.Text;
            suitabilityParametrosWorkflow.ValorPeriodo = Convert.ToInt16(this.txtValorPeriodo.Text);
            suitabilityParametrosWorkflow.GrandezaPeriodo = Convert.ToInt16(this.dropGrandezaPeriodo.Value);
        }

        
        // Salva todas as Configurações
        try {
            suitabilityParametrosWorkflow.Save();

            #region Atualiza Historico
            SuitabilityParametrosWorkflowHistorico.createSuitabilityPerfilInvestidorHistorico(suitabilityParametrosWorkflow, TipoOperacaoBanco.Alteração);
            #endregion
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);


        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }

    /// <summary>
    /// Tratamento para Campos Obrigatórios
    /// </summary>
    /// <exception cref="Exception"></exception>
    private void TrataErros() {

        if (this.dropAtivaWorkflowSuitability.Value.Equals("S"))
        {
            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { 
                this.dropVerificacaoEnquadramento,
                this.textNome,
                this.textEmail,
                this.txtValorPeriodo,
                this.dropVerificacaoEnquadramento
            });

            if (base.TestaObrigatorio(controles) != "")
            {
                throw new Exception("Favor informar todos os atributos.");
            }
            #endregion
        }
    }
           
   
    protected void dropAtivaWorkflowSuitability_OnPreRender(object sender, EventArgs e)
    {
        this.dropAtivaWorkflowSuitability.SelectedIndex = suitabilityParametrosWorkflow.AtivaWorkFlow == "S" ? 0 : 1;
    }

    protected void dropVerificacaoEnquadramento_OnPreRender(object sender, EventArgs e) 
    {
        if (suitabilityParametrosWorkflow.VerificaEnquadramento.HasValue)
        {
            int verificacaoEnquadramento = suitabilityParametrosWorkflow.VerificaEnquadramento.Value;

            this.dropVerificacaoEnquadramento.Text = verificacaoEnquadramento.ToString();
        }
    }

    protected void textNome_OnPrerender(object sender, EventArgs e)
    {
        this.textNome.Text = Convert.ToString(suitabilityParametrosWorkflow.ResponsavelNome);
    }

    protected void textEmail_OnPrerender(object sender, EventArgs e)
    {
        this.textEmail.Text = Convert.ToString(suitabilityParametrosWorkflow.ResponsavelEmail);
    }

    protected void txtValorPeriodo_OnPrerender(object sender, EventArgs e)
    {
        this.txtValorPeriodo.Text = Convert.ToString(suitabilityParametrosWorkflow.ValorPeriodo);
    }

    protected void dropGrandezaPeriodo_OnPreRender(object sender, EventArgs e)
    {
        if(suitabilityParametrosWorkflow.GrandezaPeriodo.HasValue)
        {
            int grandezaPeriodo = suitabilityParametrosWorkflow.GrandezaPeriodo.Value;

            this.dropGrandezaPeriodo.Text = grandezaPeriodo.ToString();
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

}