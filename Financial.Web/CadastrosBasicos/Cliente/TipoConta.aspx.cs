using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;

public partial class CadastrosBasicos_TipoConta : CadastroBasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTipoConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TipoContaCollection coll = new TipoContaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TipoConta tipoConta = new TipoConta();
        int idTipoConta = (int)e.Keys[0];

        if (tipoConta.LoadByPrimaryKey(idTipoConta))
        {
            tipoConta.Descricao = Convert.ToString(e.NewValues[TipoContaMetadata.ColumnNames.Descricao]);

            tipoConta.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TipoConta - Operacao: Update TipoConta: " + idTipoConta + UtilitarioWeb.ToString(tipoConta),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        TipoConta tipoConta = new TipoConta();

        tipoConta.Descricao = Convert.ToString(e.NewValues[TipoContaMetadata.ColumnNames.Descricao]);
        tipoConta.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TipoConta - Operacao: Insert TipoConta: " + tipoConta.IdTipoConta + UtilitarioWeb.ToString(tipoConta),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TipoContaMetadata.ColumnNames.IdTipoConta);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTipoConta = Convert.ToInt32(keyValuesId[i]);

                TipoConta tipoConta = new TipoConta();
                if (tipoConta.LoadByPrimaryKey(idTipoConta))
                {

                    TipoConta tipoContaClone = (TipoConta)Utilitario.Clone(tipoConta);
                    //
                    tipoConta.MarkAsDeleted();
                    tipoConta.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TipoConta - Operacao: Delete TipoConta: " + idTipoConta + UtilitarioWeb.ToString(tipoContaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}