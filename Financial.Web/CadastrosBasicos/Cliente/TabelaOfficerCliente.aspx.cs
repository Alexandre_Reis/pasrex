﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Captacao;

public partial class CadastrosBasicos_TabelaOfficerCliente : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        clienteQuery.Select();
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        clienteQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                           clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                           clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOfficer_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        OfficerCollection coll = new OfficerCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Faz Update em Lote.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        List<object> clientesSelecionados = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (clientesSelecionados.Count == 0) {
            e.Result = "Selecione um ou mais Clientes.";
        }
        else 
        {
            for (int i = 0; i < clientesSelecionados.Count; i++) 
            {
                int idCliente = Convert.ToInt32(clientesSelecionados[i]);

                Cliente cliente = new Cliente();
                //
                if (cliente.LoadByPrimaryKey(idCliente)) 
                {
                    if (dropOfficer.SelectedIndex != -1)
                    {
                        cliente.IdOfficer = Convert.ToInt32(dropOfficer.SelectedItem.Value);
                    }
                    else
                    {
                        cliente.IdOfficer = null;
                    }

                    cliente.Save();
                }
            }
            e.Result = "Processo executado com sucesso.";
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Cliente cliente = new Cliente();
        int idCliente = Convert.ToInt32(e.Keys[0]);

        if (cliente.LoadByPrimaryKey(idCliente)) 
        {
            if (e.NewValues[ClienteMetadata.ColumnNames.IdOfficer] != null)
            {
                cliente.IdOfficer = Convert.ToInt32(e.NewValues[ClienteMetadata.ColumnNames.IdOfficer]);
            }
            else
            {
                cliente.IdOfficer = null;
            }
            
            cliente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Cliente - Operacao: Update Cliente (IdOfficer): " + cliente.IdOfficer, HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridCadastro.DataBind();
        gridCadastro.CancelEdit();
    }
}