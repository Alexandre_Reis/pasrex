﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaOfficerCliente.aspx.cs" Inherits="CadastrosBasicos_TabelaOfficerCliente" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            if (e.result != '') 
            {
                alert(e.result);
                popupLote.Hide();
                gridCadastro.PerformCallback('btnRefresh');
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote" EnableClientSideAPI="True"
                                PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                Width="400" Left="250" Top="70" HeaderText="Alteração de Officer" runat="server"
                                HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
            
            <table>        
                <tr>
                    <td class="td_Label">
                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Escolha o Officer:"/>
                    </td>
                                                                                                                                                             
                    <td colspan="3">                       
                        <dxe:ASPxComboBox ID="dropOfficer" runat="server" DataSourceID="EsDSOfficer"
                                         ValueField="IdOfficer" TextField="Apelido" CssClass="dropDownList">
                        </dxe:ASPxComboBox>
                    </td>
                </tr>    
            </table>        
            
            <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização?')==true) callBackLote.SendCallback(); return false;"><asp:Literal ID="Literal15" runat="server" Text="Processa Atualização"/><div></div></asp:LinkButton>
            </div>                    
        </dxpc:PopupControlContentControl></ContentCollection>                             
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cliente -> Officer"></asp:Label>
    </div>
           
    <div id="mainContent">

            <div class="linkButton" >               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel"  OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="popupLote.ShowWindow(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Altera Officer em Lote"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomCallback="gridCadastro_CustomCallback"                     
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id" Width="10%" VisibleIndex="1" CellStyle-HorizontalAlign="left" ReadOnly="true" />
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" Width="60%" VisibleIndex="2" ReadOnly="true" />
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Officer" FieldName="IdOfficer" VisibleIndex="3" Width="25%">
                    <EditFormSettings Visible="False" />                        
                    <PropertiesComboBox DataSourceID="EsDSOfficer" TextField="Apelido" ValueField="IdOfficer" DropDownStyle="DropDown">
                        <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                </Columns>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>     
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />    
    <cc1:esDataSource ID="EsDSOfficer" runat="server" OnesSelect="EsDSOfficer_esSelect" /> 
        
    </form>
</body>
</html>