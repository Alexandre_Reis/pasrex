﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SuitabilityParametrosWorkflow.aspx.cs" Inherits="CadastrosBasicos_Cliente_SuitabilityParametrosWorkflow" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" /> 
    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    
    function showHistorico()
    {
        gridHistorico.PerformCallback();
        popupHistorico.Show();
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração de Parâmetros de Workflow - Suitability" />
                            </div>
                            <div id="mainContentSpace">
                            
                            
                                <dxpc:ASPxPopupControl ID="popupHistorico" runat="server" Width="1000px" HeaderText="" MaxWidth="1000px"
                                    ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    AllowDragging="True">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <div>
                                                <dxwgv:ASPxGridView ID="gridHistorico" runat="server" Width="100%" ClientInstanceName="gridHistorico" EnableCallBacks="true"
                                                    AutoGenerateColumns="true" DataSourceID="EsDSSuitabilityHistorico" KeyFieldName="DataHistorico"
                                                    OnCustomCallback="gridHistorico_CustomCallback">

                                                    <Settings ShowTitlePanel="True" UseFixedTableLayout="true"/>
                                                    <SettingsBehavior ColumnResizeMode="Disabled"  />
                                                    <SettingsDetail ShowDetailButtons="False" />
                                                    <Styles AlternatingRow-Enabled="True" Cell-Wrap="False" >
                                                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                    </Styles>
                                                    <Images>
                                                        <PopupEditFormWindowClose Height="17px" Width="17px" />
                                                    </Images>
                                                    <SettingsText EmptyDataRow="0 Registros" Title="Histórico" />
                                                </dxwgv:ASPxGridView>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                               </dxpc:ASPxPopupControl>
                            
                            
                            
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlSeguranca" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px"
                                                        Width="459px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Ativa workflow de Suitability:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <dxe:ASPxComboBox ID="dropAtivaWorkflowSuitability" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropAtivaWorkflowSuitability" OnPreRender="dropAtivaWorkflowSuitability_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Verificação de Enquadramento:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <dxe:ASPxComboBox ID="dropVerificacaoEnquadramento" runat="server" CssClass="dropDownListCurto_1"
                                                                                ClientInstanceName="dropVerificacaoEnquadramento" OnPreRender="dropVerificacaoEnquadramento_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Exato" />
                                                                                    <dxe:ListEditItem Value="2" Text="Risco Menor" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Nome Responsável Monitoramento:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textNome" runat="server" TextMode="SingleLine" CssClass="textNormal10" OnPreRender="textNome_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Email Responsável Monitoramento:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textEmail" runat="server" TextMode="SingleLine" CssClass="textNormal10" OnPreRender="textEmail_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Validade:" />
                                                                        </td>
                                                                        <td style="width: 10px;">
                                                                            <dxe:ASPxSpinEdit ID="txtValorPeriodo" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtValorPeriodo" MaxLength="3" OnPreRender="txtValorPeriodo_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropGrandezaPeriodo" runat="server" CssClass="dropDownListCurto_1"
                                                                                ClientInstanceName="dropGrandezaPeriodo" OnPreRender="dropGrandezaPeriodo_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Dia(s)" />
                                                                                    <dxe:ListEditItem Value="2" Text="Mês(es)" />
                                                                                    <dxe:ListEditItem Value="3" Text="Ano(s)" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>                                                                    
                                                                </table>
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 90pt">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnCustomFields"
                                                                            OnClientClick="showHistorico(); return false;">
                                                                            <asp:Literal ID="Literal12" runat="server" Text="Historico Parâmetros" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <cc1:esDataSource ID="EsDSSuitabilityHistorico" runat="server" OnesSelect="EsDSSuitabilityHistorico_esSelect" />
    </form>
</body>
</html>
