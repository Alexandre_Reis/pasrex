﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityPerfilInvestidor : CadastroBasePage
{
    protected void EsDSSuitabilityPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();

        coll.Query.OrderBy(coll.Query.IdPerfilInvestidor.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorHistoricoCollection coll = new SuitabilityPerfilInvestidorHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();

        suitabilityPerfilInvestidor.Nivel = Convert.ToInt16(e.NewValues["Nivel"]);
        suitabilityPerfilInvestidor.Perfil = Convert.ToString(e.NewValues["Perfil"]);
        suitabilityPerfilInvestidor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SuitabilityPerfilInvestidor - Operacao: Insert: " + suitabilityPerfilInvestidor.IdPerfilInvestidor + UtilitarioWeb.ToString(suitabilityPerfilInvestidor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityPerfilInvestidorHistorico.createSuitabilityPerfilInvestidorHistorico(suitabilityPerfilInvestidor, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
        gridHistorico.DataBind();
    }
    
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilInvestidor");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdPerfil = Convert.ToInt16(keyValuesId[i]);

                SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                if (suitabilityPerfilInvestidor.LoadByPrimaryKey(IdPerfil))
                {
                    //
                    SuitabilityPerfilInvestidor suitabilityPerfilInvestidorClone = (SuitabilityPerfilInvestidor)Utilitario.Clone(suitabilityPerfilInvestidor);
                    //

                    suitabilityPerfilInvestidor.MarkAsDeleted();
                    suitabilityPerfilInvestidor.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityPerfilInvestidor - Operacao: Delete suitabilityPerfilInvestidor: " + IdPerfil + UtilitarioWeb.ToString(suitabilityPerfilInvestidorClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityPerfilInvestidorHistorico.createSuitabilityPerfilInvestidorHistorico(suitabilityPerfilInvestidorClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
        gridHistorico.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
        short IdPerfil = Convert.ToInt16(e.Keys[0]);

        if (suitabilityPerfilInvestidor.LoadByPrimaryKey(IdPerfil))
        {
            suitabilityPerfilInvestidor.Nivel = Convert.ToInt16(e.NewValues["Nivel"]);
            suitabilityPerfilInvestidor.Perfil = Convert.ToString(e.NewValues["Perfil"]);
            suitabilityPerfilInvestidor.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityPerfilInvestidor: " + IdPerfil + UtilitarioWeb.ToString(suitabilityPerfilInvestidor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityPerfilInvestidorHistorico.createSuitabilityPerfilInvestidorHistorico(suitabilityPerfilInvestidor, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
        gridHistorico.DataBind();
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }
}
