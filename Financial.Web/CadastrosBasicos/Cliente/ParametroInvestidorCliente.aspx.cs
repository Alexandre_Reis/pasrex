﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;

public partial class CadastrosBasicos_ParametroInvestidorCliente : CadastroBasePage 
{
    #region DataSources
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();

        coll.Query.Select(coll.Query.IdCliente,
                          coll.Query.Nome,
                          coll.Query.InvestidorProfissional,
                          coll.Query.InvestidorQualificado);
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Cliente cliente = new Cliente();
        int idCliente = (int)e.Keys[0];

        if (cliente.LoadByPrimaryKey(idCliente))
        {
            cliente.InvestidorProfissional = Convert.ToString(e.NewValues[ClienteMetadata.ColumnNames.InvestidorProfissional]);
            cliente.InvestidorQualificado = Convert.ToString(e.NewValues[ClienteMetadata.ColumnNames.InvestidorQualificado]);

            cliente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Parametrização de Investidores - Cliente - Operacao: Update Cliente: " + idCliente + UtilitarioWeb.ToString(cliente),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}