﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;

using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityOpcao : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
               new List<string>(new string[] { SuitabilityOpcaoMetadata.ColumnNames.IdQuestao}));
    }

    protected void EsDSSuitabilityOpcao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityOpcaoCollection coll = new SuitabilityOpcaoCollection();

        coll.Query.OrderBy(coll.Query.IdQuestao.Ascending, coll.Query.IdOpcao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityQuestao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityQuestaoQuery suitabilityQuestaoQuery = new SuitabilityQuestaoQuery("SQ");
        SuitabilityValidacaoQuery suitabilityValidacaoQuery = new SuitabilityValidacaoQuery("SV");
        suitabilityQuestaoQuery.Select(suitabilityQuestaoQuery.IdQuestao, suitabilityQuestaoQuery.Descricao, suitabilityValidacaoQuery.Descricao.As("Validacao"));
        suitabilityQuestaoQuery.LeftJoin(suitabilityValidacaoQuery).On(suitabilityQuestaoQuery.IdValidacao.Equal(suitabilityValidacaoQuery.IdValidacao));
        SuitabilityQuestaoCollection coll = new SuitabilityQuestaoCollection();

        coll.Load(suitabilityQuestaoQuery);

        foreach (SuitabilityQuestao suitabilityQuestao in coll)
        {
            string validacao = "";
            if (suitabilityQuestao.GetColumn("Validacao").ToString() != "") validacao = suitabilityQuestao.GetColumn("Validacao").ToString() + " - ";

            suitabilityQuestao.Descricao = "<div title='" + validacao + suitabilityQuestao.Descricao + "'>" + validacao + suitabilityQuestao.Descricao + "</div>";
        }

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityOpcaoHistoricoCollection coll = new SuitabilityOpcaoHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityOpcao suitabilityOpcao = new SuitabilityOpcao();

        suitabilityOpcao.IdQuestao = Convert.ToInt32(e.NewValues["IdQuestao"]);
        suitabilityOpcao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        suitabilityOpcao.Pontos = Convert.ToInt32(e.NewValues["Pontos"]);

        suitabilityOpcao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SuitabilityOpcao - Operacao: Insert: " + suitabilityOpcao.IdOpcao + UtilitarioWeb.ToString(suitabilityOpcao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityOpcaoHistorico.createSuitabilityOpcaoHistorico(suitabilityOpcao, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOpcao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdOpcao = Convert.ToInt16(keyValuesId[i]);

                SuitabilityOpcao suitabilityOpcao = new SuitabilityOpcao();
                if (suitabilityOpcao.LoadByPrimaryKey(IdOpcao))
                {
                    //
                    SuitabilityOpcao suitabilityOpcaoClone = (SuitabilityOpcao)Utilitario.Clone(suitabilityOpcao);
                    //

                    suitabilityOpcao.MarkAsDeleted();
                    suitabilityOpcao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityOpcao - Operacao: Delete suitabilityOpcao: " + IdOpcao + UtilitarioWeb.ToString(suitabilityOpcaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityOpcaoHistorico.createSuitabilityOpcaoHistorico(suitabilityOpcaoClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityOpcao suitabilityOpcao = new SuitabilityOpcao();
        short idOpcao = Convert.ToInt16(e.Keys[0]);

        if (suitabilityOpcao.LoadByPrimaryKey(idOpcao))
        {
            suitabilityOpcao.IdQuestao = Convert.ToInt32(e.NewValues["IdQuestao"]);
            suitabilityOpcao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            suitabilityOpcao.Pontos = Convert.ToInt32(e.NewValues["Pontos"]);
            suitabilityOpcao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityOpcao: " + idOpcao + UtilitarioWeb.ToString(suitabilityOpcao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityOpcaoHistorico.createSuitabilityOpcaoHistorico(suitabilityOpcao, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }
}
