﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityTipoDispensa : CadastroBasePage
{
    protected void EsDSSuitabilityTipoDispensa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityTipoDispensaCollection coll = new SuitabilityTipoDispensaCollection();

        coll.Query.OrderBy(coll.Query.IdDispensa.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityTipoDispensaHistoricoCollection coll = new SuitabilityTipoDispensaHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityTipoDispensa suitabilityTipoDispensa = new SuitabilityTipoDispensa();

        suitabilityTipoDispensa.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        suitabilityTipoDispensa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SuitabilityTipoDispensa - Operacao: Insert: " + suitabilityTipoDispensa.IdDispensa + UtilitarioWeb.ToString(suitabilityTipoDispensa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityTipoDispensaHistorico.createSuitabilityTipoDispensaHistorico(suitabilityTipoDispensa, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
        gridHistorico.DataBind();
    }
    
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdDispensa");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdDispensa = Convert.ToInt16(keyValuesId[i]);

                SuitabilityTipoDispensa suitabilityTipoDispensa = new SuitabilityTipoDispensa();
                if (suitabilityTipoDispensa.LoadByPrimaryKey(IdDispensa))
                {
                    //
                    SuitabilityTipoDispensa suitabilityTipoDispensaClone = (SuitabilityTipoDispensa)Utilitario.Clone(suitabilityTipoDispensa);
                    //

                    suitabilityTipoDispensa.MarkAsDeleted();
                    suitabilityTipoDispensa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityTipoDispensa - Operacao: Delete suitabilityTipoDispensa: " + IdDispensa + UtilitarioWeb.ToString(suitabilityTipoDispensaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityTipoDispensaHistorico.createSuitabilityTipoDispensaHistorico(suitabilityTipoDispensaClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
        gridHistorico.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityTipoDispensa suitabilityTipoDispensa = new SuitabilityTipoDispensa();
        short IdDispensa = Convert.ToInt16(e.Keys[0]);

        if (suitabilityTipoDispensa.LoadByPrimaryKey(IdDispensa))
        {
            suitabilityTipoDispensa.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            suitabilityTipoDispensa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityDispensa: " + IdDispensa + UtilitarioWeb.ToString(suitabilityTipoDispensa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityTipoDispensaHistorico.createSuitabilityTipoDispensaHistorico(suitabilityTipoDispensa, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
        gridHistorico.DataBind();
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }
}
