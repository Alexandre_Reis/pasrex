using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista.Perfil;

public partial class CadastrosBasicos_Cliente_SuitabilityConsultaLog : CadastroBasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasEditControl = false;
        this.AllowDelete = false;
        this.AllowUpdate = false;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);
    }

    #region data sources
    protected void EsDSSuitabilityAlertas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityLogMensagensQuery suitabilityLogMensagensQuery = new SuitabilityLogMensagensQuery("SLM");
        CotistaQuery cotistaQuery = new CotistaQuery("C");

        suitabilityLogMensagensQuery.Select(suitabilityLogMensagensQuery.IdCotista,
                            cotistaQuery.Nome, 
                            suitabilityLogMensagensQuery.Mensagem,
                            suitabilityLogMensagensQuery.Resposta,
                            suitabilityLogMensagensQuery.Data);
        suitabilityLogMensagensQuery.InnerJoin(cotistaQuery).On(suitabilityLogMensagensQuery.IdCotista.Equal(cotistaQuery.IdCotista));

        if (!String.IsNullOrEmpty(btnEditCodigoCotistaFiltro.Text))
        {
            suitabilityLogMensagensQuery.Where(suitabilityLogMensagensQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotistaFiltro.Text)));
        }

        if (textDataInicio.Text != "")
        {
            suitabilityLogMensagensQuery.Where(suitabilityLogMensagensQuery.Data.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
        }

        if (textDataFim.Text != "")
        {
            suitabilityLogMensagensQuery.Where(suitabilityLogMensagensQuery.Data.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
        }

        suitabilityLogMensagensQuery.OrderBy(suitabilityLogMensagensQuery.Data.Descending);

        SuitabilityLogMensagensCollection coll = new SuitabilityLogMensagensCollection();

        coll.Load(suitabilityLogMensagensQuery);

        e.Collection = coll;

    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}
