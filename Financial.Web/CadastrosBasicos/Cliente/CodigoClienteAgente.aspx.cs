﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CodigoClienteAgente : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCodigoClienteAgente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        CodigoClienteAgenteQuery codigoClienteAgenteQuery = new CodigoClienteAgenteQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");

        codigoClienteAgenteQuery.Select(codigoClienteAgenteQuery, clienteQuery);
        codigoClienteAgenteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == codigoClienteAgenteQuery.IdCliente);
        codigoClienteAgenteQuery.InnerJoin(clienteQuery).On(codigoClienteAgenteQuery.IdCliente == clienteQuery.IdCliente);
        codigoClienteAgenteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        CodigoClienteAgenteCollection coll = new CodigoClienteAgenteCollection();
        coll.Load(codigoClienteAgenteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).Enabled = false;
    }

    protected void dropAgente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoBovespa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBovespa") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoBMF = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBMF") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(dropAgente);
        controles.Add(btnEditCodigoBovespa);
        controles.Add(btnEditCodigoBMF);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
        //
        codigoClienteAgenteCollection.Query.Where(
                                        codigoClienteAgenteCollection.Query.IdCliente == Convert.ToInt32(btnEditCodigoCliente.Text) &&
                                        codigoClienteAgenteCollection.Query.IdAgente == Convert.ToInt32(dropAgente.SelectedItem.Value) &&
                                        ( codigoClienteAgenteCollection.Query.CodigoClienteBovespa == Convert.ToInt32(btnEditCodigoBovespa.Text) | 
                                          codigoClienteAgenteCollection.Query.CodigoClienteBMF == Convert.ToInt32(btnEditCodigoBMF.Text) 
                                        )
                                      );
        codigoClienteAgenteCollection.Query.Load();

        if (codigoClienteAgenteCollection.Count > 0) {
            e.Result = "Já existe uma conta igual (Bovespa ou BMF) associada a este cliente!";
            return;
        }
    }
  
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int id = (int)e.Keys[0];

        CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();

        if (codigoClienteAgente.LoadByPrimaryKey(id)) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoBovespa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBovespa") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoBMF = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBMF") as ASPxSpinEdit;

            codigoClienteAgente.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            codigoClienteAgente.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            codigoClienteAgente.CodigoClienteBovespa = Convert.ToInt32(btnEditCodigoBovespa.Text);
            codigoClienteAgente.CodigoClienteBMF = Convert.ToInt32(btnEditCodigoBMF.Text);

            codigoClienteAgente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CodigoClienteAgente - Operacao: Update CodigoClienteAgente: " + id + UtilitarioWeb.ToString(codigoClienteAgente),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoBovespa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBovespa") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoBMF = gridCadastro.FindEditFormTemplateControl("btnEditCodigoBMF") as ASPxSpinEdit;

        CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();

        codigoClienteAgente.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        codigoClienteAgente.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        codigoClienteAgente.CodigoClienteBovespa = Convert.ToInt32(btnEditCodigoBovespa.Text);
        codigoClienteAgente.CodigoClienteBMF = Convert.ToInt32(btnEditCodigoBMF.Text);

        codigoClienteAgente.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CodigoClienteAgente - Operacao: Insert CodigoClienteAgente: " + codigoClienteAgente.Identificador + UtilitarioWeb.ToString(codigoClienteAgente),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("Identificador");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int id = Convert.ToInt32(keyValuesId[i]);

                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                if (codigoClienteAgente.LoadByPrimaryKey(id)) {

                    //
                    CodigoClienteAgente codigoClienteAgenteClone = (CodigoClienteAgente)Utilitario.Clone(codigoClienteAgente);
                    //

                    codigoClienteAgente.MarkAsDeleted();
                    codigoClienteAgente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CodigoClienteAgente - Operacao: Delete CodigoClienteAgente: " + id + UtilitarioWeb.ToString(codigoClienteAgenteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente", "btnEditCodigoBovespa");
        base.gridCadastro_PreRender(sender, e);
    }
}