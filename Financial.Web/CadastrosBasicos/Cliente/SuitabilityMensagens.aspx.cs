﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Contabil;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_Cliente_SuitabilityMensagens : CadastroBasePage
{

    #region Load dos esDataSources

    protected void EsDSSuitabilityMensagens_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityMensagensCollection coll = new SuitabilityMensagensCollection();
        coll.LoadAll();
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityMensagensHistoricoCollection coll = new SuitabilityMensagensHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    #endregion

    #region Insert/Update/Delete
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        int idMensagem = (int)e.Keys[0];
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropConcordancia = gridCadastro.FindEditFormTemplateControl("dropConcordancia") as ASPxComboBox;

        SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
        suitabilityMensagens.LoadByPrimaryKey(idMensagem);

        suitabilityMensagens.Assunto = textAssunto.Text;
        suitabilityMensagens.Descricao = textDescricao.Text;
        suitabilityMensagens.ControlaConcordancia = Convert.ToString(dropConcordancia.Value);

        suitabilityMensagens.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Mensagens Suitability - Operacao: Update suitabilityMensagens: " + idMensagem + UtilitarioWeb.ToString(suitabilityMensagens),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityMensagensHistorico.createSuitabilityMensagensHistorico(suitabilityMensagens, TipoOperacaoBanco.Alteração);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropConcordancia = gridCadastro.FindEditFormTemplateControl("dropConcordancia") as ASPxComboBox;

        SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();

        suitabilityMensagens.Assunto = textAssunto.Text;
        suitabilityMensagens.Descricao = textDescricao.Text;
        suitabilityMensagens.ControlaConcordancia = Convert.ToString(dropConcordancia.Value);

        suitabilityMensagens.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Mensagens Suitability - Operacao: Insert suitabilityMensagens: " + suitabilityMensagens.IdMensagem + UtilitarioWeb.ToString(suitabilityMensagens),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityMensagensHistorico.createSuitabilityMensagensHistorico(suitabilityMensagens, TipoOperacaoBanco.Inclusão);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(SuitabilityMensagensMetadata.ColumnNames.IdMensagem);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idMensagem = Convert.ToInt32(keyValuesId[i]);

                SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
                if (suitabilityMensagens.LoadByPrimaryKey(idMensagem))
                {
                    SuitabilityMensagens suitabilityMensagensClone = (SuitabilityMensagens)Utilitario.Clone(suitabilityMensagens);
                    //

                    suitabilityMensagens.MarkAsDeleted();
                    suitabilityMensagens.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Mensagens suitability - Operacao: Delete suitabilityMensagens: " + idMensagem + UtilitarioWeb.ToString(suitabilityMensagensClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityMensagensHistorico.createSuitabilityMensagensHistorico(suitabilityMensagensClone, TipoOperacaoBanco.Exclusão);
                    #endregion

                }

            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    #endregion


    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        TextBox textAssunto = gridCadastro.FindEditFormTemplateControl("textAssunto") as TextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropConcordancia = gridCadastro.FindEditFormTemplateControl("dropConcordancia") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropConcordancia);
        controles.Add(textDescricao);
        controles.Add(textAssunto);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

}