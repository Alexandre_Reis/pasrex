﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.CRM;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using DevExpress.Web.Data;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_TabelaProcessamento : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {        
        this.HasPopupPessoa = true;
        base.Page_Load(sender, e);
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTabelaProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("Q");
        TabelaProcessamentoQuery tabelaProcessamentoQuery = new TabelaProcessamentoQuery("R");

        clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido, clienteQuery.IdTipo, grupoProcessamentoQuery.IdGrupoProcessamento, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(tabelaProcessamentoQuery).On(tabelaProcessamentoQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(tabelaProcessamentoQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);
                        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
 
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropGrupoProcessamento = gridCadastro.FindEditFormTemplateControl("dropGrupoProcessamento") as ASPxComboBox;

        TabelaProcessamento tabelaProcessamento = new TabelaProcessamento();

        if (tabelaProcessamento.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoPessoa.Value),Convert.ToInt16(dropGrupoProcessamento.Value)))
        {
            e.Result = "O Cliente " + Convert.ToInt32(btnEditCodigoPessoa.Value).ToString() + " já está vinculado a este grupo de processamento";
            return;
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropGrupoProcessamento = gridCadastro.FindEditFormTemplateControl("dropGrupoProcessamento") as ASPxComboBox;

        TabelaProcessamento tabelaProcessamento = new TabelaProcessamento();

        tabelaProcessamento.IdCliente = Convert.ToInt32(btnEditCodigoPessoa.Text);
        tabelaProcessamento.IdGrupoProcessamento = Convert.ToInt16(dropGrupoProcessamento.SelectedItem.Value);
        
        tabelaProcessamento.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Cliente Grupo Processamento - Operacao: Insert Cliente: " + tabelaProcessamento.IdCliente + UtilitarioWeb.ToString(tabelaProcessamento),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.IdCliente));
            string idGrupoprocessamento = Convert.ToString(e.GetListSourceFieldValue(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento));
            e.Value = idCliente + "-" + idGrupoprocessamento;
        }
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(TabelaProcessamentoMetadata.ColumnNames.IdCliente);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento);
            for (int i = 0; i < keyValues1.Count; i++)
            {
                int idCliente = Convert.ToInt32(keyValues1[i]);
                short idGrupoProcessamento = Convert.ToInt16(keyValues2[i]);
                                
                TabelaProcessamento tabelaProcessamento = new TabelaProcessamento();
                if (tabelaProcessamento.LoadByPrimaryKey(idCliente, idGrupoProcessamento))
                {
                    tabelaProcessamento.MarkAsDeleted();
                    tabelaProcessamento.Save();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_Init(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
    }
}
