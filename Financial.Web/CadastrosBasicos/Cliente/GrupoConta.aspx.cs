using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;

public partial class CadastrosBasicos_GrupoConta : CadastroBasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSGrupoConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoContaCollection coll = new GrupoContaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        GrupoConta grupoConta = new GrupoConta();
        int idGrupoConta = (int)e.Keys[0];

        if (grupoConta.LoadByPrimaryKey(idGrupoConta))
        {
            grupoConta.Descricao = Convert.ToString(e.NewValues[GrupoContaMetadata.ColumnNames.Descricao]);

            grupoConta.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoConta - Operacao: Update GrupoConta: " + idGrupoConta + UtilitarioWeb.ToString(grupoConta),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        GrupoConta grupoConta = new GrupoConta();

        grupoConta.Descricao = Convert.ToString(e.NewValues[GrupoContaMetadata.ColumnNames.Descricao]);
        grupoConta.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoConta - Operacao: Insert GrupoConta: " + grupoConta.IdGrupo + UtilitarioWeb.ToString(grupoConta),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(GrupoContaMetadata.ColumnNames.IdGrupo);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupoConta = Convert.ToInt32(keyValuesId[i]);

                GrupoConta grupoConta = new GrupoConta();
                if (grupoConta.LoadByPrimaryKey(idGrupoConta))
                {

                    GrupoConta grupoContaClone = (GrupoConta)Utilitario.Clone(grupoConta);
                    //
                    grupoConta.MarkAsDeleted();
                    grupoConta.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoConta - Operacao: Delete GrupoConta: " + idGrupoConta + UtilitarioWeb.ToString(grupoContaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}