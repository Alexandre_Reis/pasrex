﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.CRM;
using Financial.Common.Enums;

using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityPerfil : CadastroBasePage
{
    protected void EsDSSuitabilityPerfil_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilCollection coll = new SuitabilityPerfilCollection();

        coll.Query.OrderBy(coll.Query.IdPerfilInvestidor.Ascending);

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilInvestidorCollection coll = new PerfilInvestidorCollection();

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();

        coll.Query.OrderBy(coll.Query.IdTipo.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilHistoricoCollection coll = new SuitabilityPerfilHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_Init(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityPerfil suitabilityPerfil = new SuitabilityPerfil();

        suitabilityPerfil.IdPerfilInvestidor = Convert.ToByte(e.NewValues["IdPerfilInvestidor"]);
        suitabilityPerfil.Data = Convert.ToDateTime(e.NewValues["Data"]);
        suitabilityPerfil.Pontos = Convert.ToInt32(e.NewValues["Pontos"]);

        suitabilityPerfil.PerfilInvestidor = Convert.ToInt32(e.NewValues["PerfilInvestidor"]);
        suitabilityPerfil.Definicao = Convert.ToString(e.NewValues["Definicao"]);
        suitabilityPerfil.FaixaPontuacao = Convert.ToInt16(e.NewValues["FaixaPontuacao"]);
        suitabilityPerfil.TipoCliente = Convert.ToInt16(e.NewValues["TipoCliente"]);

        suitabilityPerfil.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de suitabilityPerfil - Operacao: Insert: " + suitabilityPerfil.IdPerfilInvestidor + UtilitarioWeb.ToString(suitabilityPerfil),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityPerfilHistorico.createSuitabilityPerfilHistorico(suitabilityPerfil, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

}
