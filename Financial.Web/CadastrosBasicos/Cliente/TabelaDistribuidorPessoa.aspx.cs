﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Captacao;

public partial class CadastrosBasicos_TabelaDistribuidorPessoa : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoDistribuidor.Equal("S"));
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Faz Update em Lote.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        List<object> selecionados = gridCadastro.GetSelectedFieldValues(PessoaMetadata.ColumnNames.IdPessoa);

        if (selecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Pessoas.";
        }
        else 
        {
            for (int i = 0; i < selecionados.Count; i++) 
            {
                int idPessoa = Convert.ToInt32(selecionados[i]);

                Pessoa pessoa = new Pessoa();
                //
                if (pessoa.LoadByPrimaryKey(idPessoa)) 
                {
                    if (dropDistribuidor.SelectedIndex != -1)
                    {
                        pessoa.IdAgenteDistribuidor = Convert.ToInt32(dropDistribuidor.SelectedItem.Value);
                    }
                    else
                    {
                        pessoa.IdAgenteDistribuidor = null;
                    }

                    pessoa.Save();
                }
            }
            e.Result = "Processo executado com sucesso.";
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Pessoa pessoa = new Pessoa();
        int idPessoa = Convert.ToInt32(e.Keys[0]);

        if (pessoa.LoadByPrimaryKey(idPessoa)) 
        {
            if (e.NewValues[PessoaMetadata.ColumnNames.IdAgenteDistribuidor] != null)
            {
                pessoa.IdAgenteDistribuidor = Convert.ToInt32(e.NewValues[PessoaMetadata.ColumnNames.IdAgenteDistribuidor]);
            }
            else
            {
                pessoa.IdAgenteDistribuidor = null;
            }

            pessoa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Pessoa - Operacao: Update Pessoa (IdAgenteDistribuidor): " + pessoa.IdAgenteDistribuidor, HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridCadastro.DataBind();
        gridCadastro.CancelEdit();
    }
}