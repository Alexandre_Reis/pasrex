﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using Financial.Export;
using System.Xml;
using Financial.CRM.Enums;
using Financial.Interfaces.Export;

public partial class CadastrosBasicos_PessoaVinculo : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {

        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSPessoaVinculo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaVinculoQuery pessoaVinculoQuery = new PessoaVinculoQuery("V");

        PessoaQuery pessoaQuery = new PessoaQuery("P");

        PessoaQuery pessoaVinculadaQuery = new PessoaQuery("Q");

        pessoaVinculoQuery.InnerJoin(pessoaQuery).On(pessoaVinculoQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));
        pessoaVinculoQuery.InnerJoin(pessoaVinculadaQuery).On(pessoaVinculoQuery.IdPessoaVinculada.Equal(pessoaVinculadaQuery.IdPessoa));

        pessoaVinculoQuery.Select(pessoaVinculoQuery.IdPessoa, pessoaQuery.Nome, 
                                pessoaVinculoQuery.IdPessoaVinculada, 

        pessoaVinculadaQuery.Nome.As("NomeVinculada"), pessoaVinculoQuery.TipoVinculo);


        PessoaVinculoCollection coll = new PessoaVinculoCollection();
        coll.Load(pessoaVinculoQuery);

        e.Collection = coll;
    }

    protected void EsPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();

        coll.LoadAll();
        
        e.Collection = coll;
    }
    #endregion
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idPessoa = Convert.ToString(e.GetListSourceFieldValue(PessoaVinculoMetadata.ColumnNames.IdPessoa)).PadLeft(5, '0');
            string idPessoaVinculada = Convert.ToString(e.GetListSourceFieldValue(PessoaVinculoMetadata.ColumnNames.IdPessoaVinculada)).PadLeft(5, '0');
            e.Value = idPessoa + idPessoaVinculada;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropPessoaOnInit(object sender, EventArgs e)
    {
        // Se for Modo Update, fica desabilitada
        if (!gridCadastro.IsNewRowEditing)
        {
            if ((sender as ASPxGridLookup).ClientInstanceName == "dropPessoa")
            {
                (sender as ASPxGridLookup).ClientEnabled = false;
                (sender as ASPxGridLookup).BackColor = Color.FromName("#EBEBEB");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropPessoaVinculadaOnInit(object sender, EventArgs e)
    {
        // Se for Modo Update, fica desabilitada
        if (!gridCadastro.IsNewRowEditing)
        {
            if ((sender as ASPxGridLookup).ClientInstanceName == "dropPessoaVinculada")
            {
                (sender as ASPxGridLookup).ClientEnabled = false;
                (sender as ASPxGridLookup).BackColor = Color.FromName("#EBEBEB");
            }
        }
    }
        

    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
                
        ASPxGridLookup dropPessoa = gridCadastro.FindEditFormTemplateControl("dropPessoa") as ASPxGridLookup;
        ASPxGridLookup dropPessoaVinculada = gridCadastro.FindEditFormTemplateControl("dropPessoaVinculada") as ASPxGridLookup;
        
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            dropPessoa, dropPessoaVinculada});

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
                
        if (dropPessoa.Text.Trim() == dropPessoaVinculada.Text.Trim())
        {
            e.Result = "Pessoa não pode ser igual à Pessoa Cotitular.";
            return;
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    private void SalvarNovo() {
        ASPxGridLookup dropPessoa = gridCadastro.FindEditFormTemplateControl("dropPessoa") as ASPxGridLookup;
        ASPxGridLookup dropPessoaVinculada = gridCadastro.FindEditFormTemplateControl("dropPessoaVinculada") as ASPxGridLookup;
        
        PessoaVinculo pessoaVinculo = new PessoaVinculo();

        pessoaVinculo.IdPessoa = Convert.ToInt32(dropPessoa.Value);

        pessoaVinculo.IdPessoaVinculada = Convert.ToInt32(dropPessoaVinculada.Value);

        pessoaVinculo.TipoVinculo = Convert.ToByte(TipoVinculoPessoa.Cotitular);

        pessoaVinculo.Save();
        
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PessoaVinculo - Operacao: Insert PessoaVinculo: Pessoa " + pessoaVinculo.IdPessoa + " com " + pessoaVinculo.IdPessoaVinculada + " " + UtilitarioWeb.ToString(pessoaVinculo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Enviar para o itau
        #region CCO_XML
        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
        if (integraCotistaItau)
        {
            Itau itau = new Itau();
            itau.EnviaCco_XML(pessoaVinculo.IdPessoaVinculada.Value, "I");

        }
        #endregion
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxGridLookup dropPessoa = gridCadastro.FindEditFormTemplateControl("dropPessoa") as ASPxGridLookup;
        ASPxGridLookup dropPessoaVinculada = gridCadastro.FindEditFormTemplateControl("dropPessoaVinculada") as ASPxGridLookup;
        
        PessoaVinculo pessoaVinculo = new PessoaVinculo();

        pessoaVinculo.LoadByPrimaryKey(Convert.ToInt32(dropPessoa.Value), Convert.ToInt32(dropPessoaVinculada.Value));

        pessoaVinculo.IdPessoa = Convert.ToInt32(dropPessoa.Value);

        pessoaVinculo.IdPessoaVinculada = Convert.ToInt32(dropPessoaVinculada.Value);

        pessoaVinculo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PessoaVinculo - Operacao: Update PessoaVinculo: Pessoa " + pessoaVinculo.IdPessoa + " com " + pessoaVinculo.IdPessoaVinculada + " " + UtilitarioWeb.ToString(pessoaVinculo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();      
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete")
        {
            PessoaVinculoCollection pessoaVinculoCollection = new PessoaVinculoCollection();

            List<object> keyValuesIdPessoa = gridCadastro.GetSelectedFieldValues("IdPessoa");
            List<object> keyValuesIdPessoaVinculada = gridCadastro.GetSelectedFieldValues("IdPessoaVinculada");

            for (int i = 0; i < keyValuesIdPessoa.Count; i++)
            {
                int idPessoa = Convert.ToInt32(keyValuesIdPessoa[i]);
                int idPessoaVinculada = Convert.ToInt32(keyValuesIdPessoaVinculada[i]);

                PessoaVinculo pessoaVinculo = new PessoaVinculo();
                if (pessoaVinculo.LoadByPrimaryKey(idPessoa, idPessoaVinculada))
                {
                    pessoaVinculo.MarkAsDeleted();
                    pessoaVinculoCollection.AttachEntity(pessoaVinculo);
                }
            }
            //
            pessoaVinculoCollection.Save();

            foreach (PessoaVinculo pessoaVinculo in pessoaVinculoCollection)
            {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de pessoaVinculo - Delete: " + pessoaVinculo.IdPessoa + "-" + pessoaVinculo.IdPessoaVinculada + UtilitarioWeb.ToString(pessoaVinculo),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {        
        base.gridCadastro_PreRender(sender, e);
    }

}