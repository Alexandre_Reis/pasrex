﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SuitabilityVerificaPerfilCotista.aspx.cs" Inherits="CadastrosBasicos_Cliente_SuitabilityVerificaPerfilCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
       
    <style type="text/css">p{text-indent: 50px;} .linhaContorno {border: 1px solid black;} .centralizar { width: 50%; margin: 0 auto;} </style>

               
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript"> 
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    
    var respostas = [];
    
    function imprimir()
    {
        window.print();
    }
    
    function showHistorico()
    {
        gridHistorico.PerformCallback();
        popupHistorico.Show();
    }
    
    function OnGetDataCotista(values) {
        btnEditCodigoCotista.SetValue(values);                    
        popupCotista.HideWindow();
        ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
    }
    
    function showPerfil(perfil)
    {
        if(perfil == "")
            return;
    
        var jsonPerfil = JSON.parse(perfil);
        var text = "";
        
        if(jsonPerfil["Status"] == "Recusado")
        {
            text = "<h1>Cotista recusou responder questionário em "+ jsonPerfil["DtUltAtualizacao"]+"+</h1>";
        }
        else
        {
        
            if(jsonPerfil["DtUltAtualizacao"] == "" &&
               jsonPerfil["Validade"] == "" &&
               jsonPerfil["Perfil"] == "" &&
               jsonPerfil["Definicao"] == "")
            {
                text = "<h1>Favor parametrizar informação de validação no cadastro de pessoas, pasta Suitability.</h1>";
            }
            else
            {        
                text = "<table style='border-collapse: collapse;'><tr><td class='linhaContorno'><h3>Última Atualização</h3></td>";
                text +="    <td class='linhaContorno'><h3>Validade</h3></td>";
                text +="    <td class='linhaContorno'><h3>Perfil Apurado</h3></td>";
                text +="    <td class='linhaContorno'><h3>Descrição do Perfil</h3></td></tr>";
                text +="<tr><td class='linhaContorno'><h4>"+jsonPerfil["DtUltAtualizacao"]+"</h4></td>";
                text +="    <td class='linhaContorno'><h4>"+jsonPerfil["Validade"]+"</h4></td>";
                text +="    <td class='linhaContorno'><h4>"+jsonPerfil["Perfil"]+"</h4></td>";
                text += "   <td class='linhaContorno'><h4>"+jsonPerfil["Definicao"]+"</h4></td></tr></table>";
            }
        }
        
        document.getElementById("perfil").innerHTML = text;
    }
    
    function showQuestions(questions)
    {
        var jsonQuestion = JSON.parse(questions); 
        var text = "";
        var idQuestao = 0;
        for (i = 0; i < jsonQuestion.length; i++) 
        {
            if(idQuestao != jsonQuestion[i]["QuestaoId"])
            {
                if(idQuestao > 0) text +="</div>";
                
                text += "<div class='questao'>";
                text += "<h1>"+jsonQuestion[i]["QuestaoDescricao"]+"</h1>";
                idQuestao = jsonQuestion[i]["QuestaoId"];
            }
        
            text += "<p style='font-size: large;'><input type='radio'";
            if(jsonQuestion[i]["OpcaoId"] == jsonQuestion[i]["OpcaoResposta"]) text += " checked ";
            text += "name='questao"+jsonQuestion[i]["QuestaoId"]+"' value='"+ jsonQuestion[i]["OpcaoId"]+"'>"+jsonQuestion[i]["OpcaoDescricao"]+"</p>";

        }
        

        if(text != "")
        {   
            text +="</div>";
            text += "<button style='margin-left: 10px;' type='button' id='atualiza' onclick='atualizar()'>Atualizar</button>"
            text += "<button style='margin-left: 10px;' type='button' id='aceita'   onclick='aceitar()' disabled>Aceito</button>"
            text += "<button style='margin-left: 10px;' type='button' onclick='recusar()'>Recuso</button>";
            text += "<button style='margin-left: 10px;' type='button' onclick='imprimir()'>Imprimir</button>";
        }
        
        document.getElementById("questionario").innerHTML = text;
        
    }
    
    function atualizar()
    {
        //document.getElementById("atualiza").disabled = true;
        
        var questionario = document.getElementById("questionario");
        var elements = document.getElementsByClassName("questao")

        //Valida questionario
        for (i=0;i<elements.length;i++) {
          if(!checkRadio(elements[i].childNodes[1].getElementsByTagName("input")[0]["name"]))
          {
            alert("Favor preencher todas as questões.");
            document.getElementById("atualiza").disabled = false;
            return;
          }
        }
        
        respostas = [];
        
        //Seleciona questoes corretas
        for (i=0;i<elements.length;i++) {

            var nomeQuestao = elements[i].childNodes[1].getElementsByTagName("input")[0]["name"];
            var idQuestao = nomeQuestao.substr(7);
            var questao = document.getElementsByName(nomeQuestao);

            for(j = 0; j < questao.length; j++) {
               if(questao[j].checked) {
                    respostas.push({idCotista:btnEditCodigoCotista.GetValue(), 
                                    idQuestao:idQuestao, 
                                    idOpcao:questao[j].value});
               }
            }
        }
        
        atualiza.SendCallback(JSON.stringify(respostas));
        
    }
     
    function aceitar()
    {
        mensagemAceita.SendCallback();
    }
    
    function recusar()
    {
        mensagemRecusa.SendCallback();
    }
    
    function showMensagemRecusa(mensagem)
    {
        var jsonMensagem = JSON.parse(mensagem); 
        
        if(jsonMensagem["ControlaConcordancia"] == "S")
        {
            if(confirm(jsonMensagem["Descricao"]))
            {
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:jsonMensagem["IdMensagem"], mensagem:jsonMensagem["Descricao"], resposta:'OK' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
                recusa.SendCallback(btnEditCodigoCotista.GetValue());
            }
            else
            {
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:jsonMensagem["IdMensagem"], mensagem:jsonMensagem["Descricao"], resposta:'Cancelar' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
            }
        }
    }
    
    function showMensagemAceita(mensagem)
    {
        var jsonMensagem = JSON.parse(mensagem); 
        
        if(jsonMensagem["ControlaConcordancia"] == "S")
        {
            if(confirm(jsonMensagem["Descricao"]))
            {
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:jsonMensagem["IdMensagem"], mensagem:jsonMensagem["Descricao"], resposta:'OK' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
                aceita.SendCallback(JSON.stringify(respostas));

            }
            else
            {
                document.getElementById("atualiza").disabled = false;
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:jsonMensagem["IdMensagem"], mensagem:jsonMensagem["Descricao"], resposta:'Cancelar' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
            }
        }
    }
    
    function showNovoPerfil(novoperfil)
    {
        if(novoperfil == "")
            return;
            
        var jsonPerfil = JSON.parse(novoperfil);
        var text = "";
        
        text = "<table style='border-collapse: collapse;'><tr><td class='linhaContorno'><h3>Última Atualização</h3></td>";
        text +="    <td class='linhaContorno'><h3>Validade</h3></td>";
        text +="    <td class='linhaContorno'><h3>Perfil Apurado</h3></td>";
        text +="    <td class='linhaContorno'><h3>Descrição do Perfil</h3></td></tr>";
        text +="<tr><td class='linhaContorno'><h4>"+jsonPerfil["DtUltAtualizacao"]+"</h4></td>";
        text +="    <td class='linhaContorno'><h4>"+jsonPerfil["Validade"]+"</h4></td>";
        text +="    <td class='linhaContorno'><h4>"+jsonPerfil["Perfil"]+"</h4></td>";
        text += "   <td class='linhaContorno'><h4>"+jsonPerfil["Definicao"]+"</h4></td>";
        text += "   <td class='linhaContorno'><h4>Novo Perfil. Clique em aceito para confirmar novo perfil.</h4></td></tr></table>";
        
        document.getElementById("perfil").innerHTML = text;
        
    }
    
    function checkRadio(radioName)
    {
        var elements = document.getElementsByName(radioName);
        for (x=0;x<elements.length;x++) {
          if(elements[x].checked) {
            return true;
          }
        }
        return false;       
    }
    
    function selecionarResposta(radioName)
    {
        var elements = document.getElementsByName(radioName);
        for (x=0;x<elements.length;x++) {
          if(elements[x].value == "true") {
            return true;
          }
        }
        return false;       
    }
    </script>
</head>

<body>

    <form id="form1" runat="server">
    
    <asp:ScriptManager id="ScriptManager" runat="server"></asp:ScriptManager>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, document.getElementById('textNomeCotista'));
            updatePerfil.SendCallback(btnEditCodigoCotista.GetValue());
            updatePanel.SendCallback(btnEditCodigoCotista.GetValue());
            return false;
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="updatePanel" runat="server" OnCallback="updatePanel_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if(e.result == 'S')
            {
                alert('Cliente dispensado de apuração de perfil de Suitability');
            }
            else
            {
                showQuestions(e.result);
            }
        }
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="updatePerfil" runat="server" OnCallback="updatePerfil_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            showPerfil(e.result);
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="mensagemRecusa" runat="server" OnCallback="mensagemRecusa_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if(e.result == '')
                alert('Mensagem não cadastrada para o evento - Recusa do preenchimento do formulário.');
            else
                showMensagemRecusa(e.result);
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="recusa" runat="server" OnCallback="recusa_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            alert('Questionário Recusado.');
        }
        "/>
    </dxcb:ASPxCallback>   
    
    <dxcb:ASPxCallback ID="atualiza" runat="server" OnCallback="atualiza_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {           
            if(e.result == '')
            {
                alert('Não foi possivel definir perfil do cotista. Favor verificar se foi cadastrada a parametrização de perfil (Cliente/Suitability/Perfil).');
            }
            else
            {
                alert('Novo Perfil. Clique em aceito para confirmar novo perfil.');
                document.getElementById('aceita').disabled = false;
                showNovoPerfil(e.result);            
            }
        }
        "/>
    </dxcb:ASPxCallback>  
    
    <dxcb:ASPxCallback ID="aceita" runat="server" OnCallback="aceita_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            alert('Perfil Atualizado.');
            document.getElementById('perfil').innerHTML = '';
            document.getElementById('questionario').innerHTML = '';
            btnEditCodigoCotista.SetValue(null);
            return false;
        }
        "/>
    </dxcb:ASPxCallback>  
    
    <dxcb:ASPxCallback ID="mensagemAceita" runat="server" OnCallback="mensagemAceita_callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if(e.result == '')
                alert('Mensagem não cadastrada para o evento - Aceite do perfil apurado.');
            else
                showMensagemAceita(e.result);
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="logMensagem" runat="server" OnCallback="logMensagem_callback">
    </dxcb:ASPxCallback>
    
    
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                               
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Suitability Verifica Perfil Cotista"></asp:Label>
                        </div>

                        <div id="mainContent">
                        
                            <div>
                                <table cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                                        </td>
                                        
                                        <td>
                                            <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit"
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCotista" 
                                                                SpinButtons-ShowIncrementButtons="false">  
                                            <Buttons>
                                                <dxe:EditButton></dxe:EditButton>
                                            </Buttons>
                                            <ClientSideEvents
                                                     KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                                                     ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                                            />
                                            </dxe:ASPxSpinEdit>               
                                        </td>
                                        
                                        <td>
                                            <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
                                        </td>
                                    </tr>

                               </table>
                               
                               <div id="perfil" class="centralizar">
                               </div>
                               
                                            
                               <div id="questionario" class="centralizar">
                               </div>
                               
                               
                               
<%--                               <div id="mocapePerfil" >
                                    <table class="tableR">
                                        <tr>
                                            <th>
                                                Última Atualização
                                            </th>
                                            <th>
                                                Validade
                                            </th>
                                            <th>
                                                Perfil Apurado
                                            </th>
                                            <th>
                                                Descrição do Perfil
                                            </th>
                                        </tr>
                                        <tr><td>1</td><td>2</td><td>3</td><td>4</td></tr>
                                    </table>
                               </div>
                               
                               
                               <div id="mocapeQuestionario">
                                    <h1>Pergunta1</h1>
                                    <h2><p>Resposta1</p></h2>
                                    <h2><p>Resposta2</p></h2>
                                    <h2><p>Resposta3</p></h2>
                                    <h2><p>Resposta4</p></h2>
                                    
                                    <h1>Pergunta2</h1>
                                    <h2><p>Resposta1</p></h2>
                                    <h2><p>Resposta2</p></h2>
                                    <h2><p>Resposta3</p></h2>
                                    <h2><p>Resposta4</p></h2>
                                    
                                    <button type="button" onclick="alert('Hello world!')">Click Me!</button>
                                    <button type="button" onclick="alert('Hello world!')">Click Me!</button>
                                    <button type="button" onclick="alert('Hello world!')">Click Me!</button>
                                    
                               <div>
                                      --%>  
                            </div>
                    
<%--                           <div class="linkButton" >               
                               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
                               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
                               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
                               <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnCustomFields" OnClientClick="showHistorico(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Histórico"/><div></div></asp:LinkButton>
                            </div>--%>
                    
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>        

    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    <cc1:esDataSource ID="EsDSSuitabilityHistorico" runat="server" OnesSelect="EsDSSuitabilityHistorico_esSelect" />
    </form>
</body>
</html>