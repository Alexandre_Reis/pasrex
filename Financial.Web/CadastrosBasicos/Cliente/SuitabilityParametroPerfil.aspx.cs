﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.CRM;
using Financial.Common.Enums;

using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityParametroPerfil : CadastroBasePage
{
    protected void EsDSSuitabilityParametroPerfil_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityParametroPerfilCollection coll = new SuitabilityParametroPerfilCollection();

        coll.Query.OrderBy(coll.Query.IdParametroPerfil.Ascending);

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSValidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityValidacaoCollection coll = new SuitabilityValidacaoCollection();

        coll.Query.OrderBy(coll.Query.IdValidacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityParametroPerfilHistoricoCollection coll = new SuitabilityParametroPerfilHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityParametroPerfil suitabilityParametroPerfil = new SuitabilityParametroPerfil();

        suitabilityParametroPerfil.PerfilInvestidor = Convert.ToInt32(e.NewValues["PerfilInvestidor"]);
        suitabilityParametroPerfil.Definicao = Convert.ToString(e.NewValues["Definicao"]);
        suitabilityParametroPerfil.FaixaPontuacao = Convert.ToInt16(e.NewValues["FaixaPontuacao"]);
        suitabilityParametroPerfil.IdValidacao = Convert.ToInt16(e.NewValues["IdValidacao"]);

        SuitabilityParametroPerfilCollection suitabilityParametroPerfilCollection = new SuitabilityParametroPerfilCollection();
        suitabilityParametroPerfilCollection.Query.Where(suitabilityParametroPerfilCollection.Query.PerfilInvestidor.Equal(suitabilityParametroPerfil.PerfilInvestidor) &
                                                         suitabilityParametroPerfilCollection.Query.IdValidacao.Equal(suitabilityParametroPerfil.IdValidacao));
        suitabilityParametroPerfilCollection.Query.Load();
        if (suitabilityParametroPerfilCollection.Count > 0)
        {
            e.Cancel = true;
            return;
        }

        suitabilityParametroPerfil.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de suitabilityParametroPerfil - Operacao: Insert: " + suitabilityParametroPerfil.IdParametroPerfil + UtilitarioWeb.ToString(suitabilityParametroPerfil),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityParametroPerfilHistorico.createSuitabilityParametroPerfilHistorico(suitabilityParametroPerfil, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdParametroPerfil");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdParametroPerfil = Convert.ToInt16(keyValuesId[i]);

                SuitabilityParametroPerfil suitabilityParametroPerfil = new SuitabilityParametroPerfil();
                if (suitabilityParametroPerfil.LoadByPrimaryKey(IdParametroPerfil))
                {
                    //
                    SuitabilityParametroPerfil suitabilityParametroPerfilClone = (SuitabilityParametroPerfil)Utilitario.Clone(suitabilityParametroPerfil);
                    //

                    suitabilityParametroPerfil.MarkAsDeleted();
                    suitabilityParametroPerfil.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityParametroPerfil - Operacao: Delete suitabilityParametroPerfil: " + IdParametroPerfil + UtilitarioWeb.ToString(suitabilityParametroPerfilClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityParametroPerfilHistorico.createSuitabilityParametroPerfilHistorico(suitabilityParametroPerfilClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityParametroPerfil suitabilityParametroPerfil = new SuitabilityParametroPerfil();
        short idParametroPerfil = Convert.ToInt16(e.Keys[0]);

        if (suitabilityParametroPerfil.LoadByPrimaryKey(idParametroPerfil))
        {
            suitabilityParametroPerfil.PerfilInvestidor = Convert.ToInt32(e.NewValues["PerfilInvestidor"]);
            suitabilityParametroPerfil.Definicao = Convert.ToString(e.NewValues["Definicao"]);
            suitabilityParametroPerfil.FaixaPontuacao = Convert.ToInt16(e.NewValues["FaixaPontuacao"]);
            suitabilityParametroPerfil.IdValidacao = Convert.ToInt16(e.NewValues["IdValidacao"]);

            SuitabilityParametroPerfilCollection suitabilityParametroPerfilCollection = new SuitabilityParametroPerfilCollection();
            suitabilityParametroPerfilCollection.Query.Where(suitabilityParametroPerfilCollection.Query.PerfilInvestidor.Equal(suitabilityParametroPerfil.PerfilInvestidor) &
                                                             suitabilityParametroPerfilCollection.Query.IdValidacao.Equal(suitabilityParametroPerfil.IdValidacao) &
                                                             suitabilityParametroPerfilCollection.Query.IdParametroPerfil.NotEqual(suitabilityParametroPerfil.IdParametroPerfil));
            suitabilityParametroPerfilCollection.Query.Load();
            if (suitabilityParametroPerfilCollection.Count > 0)
            {
                e.Cancel = true;
                return;
            }


            suitabilityParametroPerfil.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityParametroPerfil: " + idParametroPerfil + UtilitarioWeb.ToString(suitabilityParametroPerfil),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityParametroPerfilHistorico.createSuitabilityParametroPerfilHistorico(suitabilityParametroPerfil, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

}
