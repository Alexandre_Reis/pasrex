using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityValidacao : CadastroBasePage
{
    protected void EsDSSuitabilityValidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityValidacaoCollection coll = new SuitabilityValidacaoCollection();

        coll.Query.OrderBy(coll.Query.IdValidacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();

        suitabilityValidacao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        suitabilityValidacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SuitabilityValidacao - Operacao: Insert: " + suitabilityValidacao.IdValidacao + UtilitarioWeb.ToString(suitabilityValidacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
    
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdValidacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdValidacao = Convert.ToInt16(keyValuesId[i]);

                SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
                if (suitabilityValidacao.LoadByPrimaryKey(IdValidacao))
                {
                    //
                    SuitabilityValidacao suitabilityValidacaoClone = (SuitabilityValidacao)Utilitario.Clone(suitabilityValidacao);
                    //

                    suitabilityValidacao.MarkAsDeleted();
                    suitabilityValidacao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityValidacao - Operacao: Delete suitabilityValidacao: " + IdValidacao + UtilitarioWeb.ToString(suitabilityValidacaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
        short IdValidacao = Convert.ToInt16(e.Keys[0]);

        if (suitabilityValidacao.LoadByPrimaryKey(IdValidacao))
        {
            suitabilityValidacao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            suitabilityValidacao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityValidacao: " + IdValidacao + UtilitarioWeb.ToString(suitabilityValidacao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

}
