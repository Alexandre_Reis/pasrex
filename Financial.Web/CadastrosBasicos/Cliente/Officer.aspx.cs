﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Captacao;
using Financial.Web.Common;

public partial class CadastrosBasicos_Officer : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupPessoa = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OfficerMetadata.ColumnNames.Tipo }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOfficer_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        OfficerCollection coll = new OfficerCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoPessoa);
        controles.Add(dropTipo);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idOfficer = Convert.ToInt32(btnEditCodigoPessoa.Text);

        if (gridCadastro.IsNewRowEditing) {
            Officer officer = new Officer();
            if (officer.LoadByPrimaryKey(idOfficer)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }
    private void SalvarNovo()
    {
        //ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        //ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        //ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        TextBox textApelido = gridCadastro.FindEditFormTemplateControl("textApelido") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        Officer officer = new Officer();

        officer.IdOfficer = Convert.ToInt32(btnEditCodigoPessoa.Value);
        officer.Apelido = Convert.ToString(textApelido.Text);
        officer.Nome = Convert.ToString(textApelido.Text);
        officer.Tipo = Convert.ToByte(dropTipo.Value);

        officer.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Officers - Operacao: Insert IdOfficer: " + btnEditCodigoPessoa.Value,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa)) {
                nome = pessoa.Apelido;
            }
        }
        e.Result = nome;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOfficer = (int)e.Keys[0];

        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        //        
        Officer officer = new Officer();
        if (officer.LoadByPrimaryKey(idOfficer)) {
            officer.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            officer.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Officer - Operacao: Update Officer: " + idOfficer + UtilitarioWeb.ToString(officer),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxSpinEdit textIdOfficer = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        TextBox textApelido = gridCadastro.FindEditFormTemplateControl("textApelido") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        //        
        Officer officer = new Officer();
        //
        officer.IdOfficer = Convert.ToInt32(textIdOfficer.Text);
        officer.Nome = textApelido.Text;
        officer.Apelido = textApelido.Text;
        officer.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        //

        officer.Save();

        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Officer - Operacao: Insert Officer: " + officer.IdOfficer + UtilitarioWeb.ToString(officer),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOfficer");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOfficer = Convert.ToInt32(keyValuesId[i]);

                Officer officer = new Officer();
                if (officer.LoadByPrimaryKey(idOfficer)) {
                    bool achou = false;
                    TabelaRebateOfficerCollection tabelaRebateOfficerCollection = new TabelaRebateOfficerCollection();
                    tabelaRebateOfficerCollection.Query.Where(tabelaRebateOfficerCollection.Query.IdOfficer.Equal(idOfficer));
                    if (tabelaRebateOfficerCollection.Query.Load()) {
                        achou = true;
                    }

                    if (achou) {
                        throw new Exception("Officer " + officer.Nome + " não pode ser excluído por ter tabelas de rebate relacionadas.");
                    }

                    //
                    Officer officerClone = (Officer)Utilitario.Clone(officer);
                    //

                    officer.MarkAsDeleted();
                    officer.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Officer - Operacao: Delete Officer: " + idOfficer + UtilitarioWeb.ToString(officerClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoPessoa"] = btnEditCodigoPessoa.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoPessoa");
        base.gridCadastro_PreRender(sender, e);
    }
}