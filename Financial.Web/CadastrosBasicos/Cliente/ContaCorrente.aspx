﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContaCorrente.aspx.cs" Inherits="CadastrosBasicos_ContaCorrente" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    

     
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataPessoa(data) {
        btnEditCodigoPessoa.SetValue(data);
        popupPessoa.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoPessoa.GetValue());
        btnEditCodigoPessoa.Focus();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">                        
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                    
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }        
        "/>        
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }
        "/>
    </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
            {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
            OnCallBackCompleteCliente(s, e, popupMensagemPessoa, btnEditCodigoPessoa, textNome);
        }  
            
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Conta Corrente"></asp:Label>
    </div>
           
    <div id="mainContent">
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdConta" DataSourceID="EsDSContaCorrente"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>

                    <dxwgv:GridViewDataColumn FieldName="IdConta" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="left"/>

                    <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="30%" UnboundType="String"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Banco" FieldName="IdBanco" VisibleIndex="3" Width="15%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSBanco" TextField="Nome" ValueField="IdBanco"></PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Agência" FieldName="IdAgencia" VisibleIndex="4" Width="15%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSAgencia" TextField="Nome" ValueField="IdAgencia"></PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Numero" Caption="Número" Width="10%" VisibleIndex="5">
                    <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="C/C Principal" FieldName="ContaDefault" VisibleIndex="5" Width="8%" ExportWidth="100">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                            <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />                            
                        </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Moeda" FieldName="IdMoeda" VisibleIndex="5" Width="8%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSMoeda" TextField="Nome" ValueField="IdMoeda">
                            </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>                                       
                </Columns>
                
                <Templates>
                <EditForm>                   
                    <div class="editForm">    
                            
                        <table>
                            <tr>
                                <td class="td_Label_Longo">
                                    <asp:Label ID="lblContaCorrente" runat="server" CssClass="labelNormal" Text="Id.Conta:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxTextBox ID="textIdConta" ClientInstanceName="textIdConta" runat="server" CssClass="textCurto" ClientEnabled="False" Text='<%# Eval("IdConta") %>'></dxe:ASPxTextBox>
                                </td>
                            </tr>
                                <td class="td_Label_Longo">
                                    <asp:Label ID="labelPessoa" runat="server" CssClass="labelRequired" Text="Pessoa:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoPessoa" Text='<%# Eval("IdPessoa") %>'  
                                            MaxLength="10" NumberType="Integer" OnInit="btnEditCodigoPessoa_Init">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupPessoa.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemPessoa, ASPxCallback1, btnEditCodigoPessoa);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>                                                                        
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="False" Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                </td>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelBanco" runat="server" CssClass="labelNormal" Text="Banco:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropBanco" runat="server" ClientInstanceName="dropBanco"
                                                        DataSourceID="EsDSBanco" IncrementalFilteringMode="startsWith"  
                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdBanco"
                                                        Text='<%#Eval("IdBanco")%>'>
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                        if(s.GetSelectedIndex() == -1)
                                                                        {
                                                                            s.SetText(null);
                                                                        }
                                                                        else
                                                                        {
                                                                            dropAgencia.PerformCallback();
                                                                        }                                                                        
                                                                        } " />
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>  
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Agência:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropAgencia" runat="server" ClientInstanceName="dropAgencia"
                                                        DataSourceID="EsDSAgencia" IncrementalFilteringMode="startswith"  
                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdAgencia"
                                                        Text='<%#Eval("IdAgencia")%>'>
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                        if(s.GetSelectedIndex() == -1)
                                                                            s.SetText(null); } "/>
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Local :"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropLocal" runat="server" ClientInstanceName="dropLocal"
                                                        DataSourceID="EsDSLocalFeriado" IncrementalFilteringMode="startswith"  
                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdLocal"
                                                        Text='<%#Eval("IdLocal")%>'>
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                        if(s.GetSelectedIndex() == -1)
                                                                            s.SetText(null); } "/>
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropTipoConta" runat="server" ClientInstanceName="dropTipoConta"
                                                        DataSourceID="EsDSTipoConta" IncrementalFilteringMode="startswith"  
                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Descricao" ValueField="IdTipoConta"
                                                        Text='<%#Eval("IdTipoConta")%>'>
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                        if(s.GetSelectedIndex() == -1)
                                                                            s.SetText(null); } "/>
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelNumero" runat="server" CssClass="labelRequired" Text="Número:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textNumero" runat="server" MaxLength="50" CssClass="textNormal" Text='<%# Eval("Numero") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="DV:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textDigito" runat="server" MaxLength="2" CssClass="textCurto" Text='<%# Eval("DigitoConta") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Descrição:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textDescricao" runat="server" MaxLength="50" Width="320px" Text='<%# Eval("DescricaoCodigo") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Grupo:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropGrupo" runat="server" ClientInstanceName="dropGrupo"
                                                        DataSourceID="EsDSGrupoConta" IncrementalFilteringMode="startswith"  
                                                        ShowShadow="false" DropDownStyle="DropDownlist"
                                                        CssClass="dropDownList" TextField="Descricao" ValueField="IdGrupo"
                                                        Text='<%#Eval("IdGrupo")%>'>                                            
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelContaDefault" runat="server" CssClass="labelRequired" Text="C/C Principal:"></asp:Label>
                                </td>
                                <td colspan="2">      
                                    <dxe:ASPxComboBox ID="dropContaDefault" runat="server" ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("ContaDefault")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                    <dxe:ListEditItem Value="N" Text="Não" />
                                    </Items>                                                            
                                    </dxe:ASPxComboBox>                                                                 
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Moeda:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropMoeda" runat="server" ClientInstanceName="dropMoeda"
                                                        DataSourceID="EsDSMoeda" IncrementalFilteringMode="startswith"  
                                                        ShowShadow="false" DropDownStyle="DropDownlist"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdMoeda"
                                                        Text='<%#Eval("IdMoeda")%>'>                                            
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                                                
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>                                                                      
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                                        
            </dxwgv:ASPxGridView>            
            </div>            
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" 
    LeftMargin = "40"
    RightMargin = "35"
    />
    
    <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
    <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect" />
    <cc1:esDataSource ID="EsDSBanco" runat="server" OnesSelect="EsDSBanco_esSelect" />
    <cc1:esDataSource ID="EsDSAgencia" runat="server" OnesSelect="EsDSAgencia_esSelect" />
    <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
    <cc1:esDataSource ID="EsDSLocalFeriado" runat="server" OnesSelect="EsDSLocalFeriado_esSelect" />
    <cc1:esDataSource ID="EsDSTipoConta" runat="server" OnesSelect="EsDSTipoConta_esSelect" />
    <cc1:esDataSource ID="EsDSGrupoConta" runat="server" OnesSelect="EsDSGrupoConta_esSelect" />
        
    </form>
</body>
</html>