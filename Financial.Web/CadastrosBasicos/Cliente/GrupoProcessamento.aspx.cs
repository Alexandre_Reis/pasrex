﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class CadastrosBasicos_GrupoProcessamento : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        GrupoProcessamento grupoProcessamento = new GrupoProcessamento();
        short idGrupoProcessamento = (short)e.Keys[0];

        if (grupoProcessamento.LoadByPrimaryKey(idGrupoProcessamento))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            grupoProcessamento.Descricao = textDescricao.Text;
            grupoProcessamento.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoProcessamento - Operacao: Update GrupoProcessamento: " + idGrupoProcessamento + UtilitarioWeb.ToString(grupoProcessamento),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        GrupoProcessamento grupoProcessamento = new GrupoProcessamento();
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        grupoProcessamento.Descricao = textDescricao.Text;
        grupoProcessamento.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoProcessamento - Operacao: Insert GrupoProcessamento: " + grupoProcessamento.IdGrupoProcessamento + UtilitarioWeb.ToString(grupoProcessamento),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        GrupoProcessamento grupoProcessamento = new GrupoProcessamento();
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        grupoProcessamento.Descricao = textDescricao.Text;
        grupoProcessamento.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoProcessamento - Operacao: Insert GrupoProcessamento: " + grupoProcessamento.IdGrupoProcessamento + UtilitarioWeb.ToString(grupoProcessamento),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdGrupoProcessamento");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupoProcessamento = Convert.ToInt32(keyValuesId[i]);

                GrupoProcessamento grupoProcessamento = new GrupoProcessamento();
                if (grupoProcessamento.LoadByPrimaryKey((short)idGrupoProcessamento))
                {
                    ClienteCollection clienteCollection = new ClienteCollection();
                    clienteCollection.Query.Where(clienteCollection.Query.IdGrupoProcessamento.Equal(idGrupoProcessamento));
                    if (clienteCollection.Query.Load())
                    {
                        throw new Exception("Grupo de processamento " + grupoProcessamento.Descricao + " não pode ser excluído por ter clientes relacionados.");
                    }

                    //
                    GrupoProcessamento grupoProcessamentoClone = (GrupoProcessamento)Utilitario.Clone(grupoProcessamento);
                    //
                    grupoProcessamento.MarkAsDeleted();
                    grupoProcessamento.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoProcessamento - Operacao: Delete GrupoProcessamento: " + idGrupoProcessamento + UtilitarioWeb.ToString(grupoProcessamentoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

}

