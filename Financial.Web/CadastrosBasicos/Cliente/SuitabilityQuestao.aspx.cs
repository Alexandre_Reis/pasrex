﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;


public partial class CadastrosBasicos_Cliente_Suitability : CadastroBasePage
{
    protected void EsDSSuitabilityQuestao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityQuestaoCollection coll = new SuitabilityQuestaoCollection();

        coll.Query.OrderBy(coll.Query.IdQuestao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSValidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityValidacaoCollection coll = new SuitabilityValidacaoCollection();

        coll.Query.OrderBy(coll.Query.IdValidacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityQuestaoHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityQuestaoHistoricoCollection coll = new SuitabilityQuestaoHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityQuestao suitabilityQuestao = new SuitabilityQuestao();

        suitabilityQuestao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        suitabilityQuestao.Fator = Convert.ToInt32(e.NewValues["Fator"]);
        suitabilityQuestao.IdValidacao = Convert.ToInt32(e.NewValues["IdValidacao"]);
        if (suitabilityQuestao.IdValidacao == 0)
        {
            throw new Exception("O campo Tipo Validação possui preenchimento obrigatório");
        }
            
        suitabilityQuestao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SuitabilityOpcao - Operacao: Insert: " + suitabilityQuestao.IdQuestao + UtilitarioWeb.ToString(suitabilityQuestao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityQuestaoHistorico.createSuitabilityQuestaoHistorico(suitabilityQuestao, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
    
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdQuestao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdQuestao = Convert.ToInt16(keyValuesId[i]);

                SuitabilityQuestao suitabilityQuestao = new SuitabilityQuestao();
                if (suitabilityQuestao.LoadByPrimaryKey(IdQuestao))
                {
                    //
                    SuitabilityQuestao suitabilityQuestaoClone = (SuitabilityQuestao)Utilitario.Clone(suitabilityQuestao);
                    //

                    suitabilityQuestao.MarkAsDeleted();
                    suitabilityQuestao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityQuestao - Operacao: Delete suitabilityQuestao: " + IdQuestao + UtilitarioWeb.ToString(suitabilityQuestaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityQuestaoHistorico.createSuitabilityQuestaoHistorico(suitabilityQuestaoClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityQuestao suitabilityQuestao = new SuitabilityQuestao();
        short IdQuestao = Convert.ToInt16(e.Keys[0]);

        if (suitabilityQuestao.LoadByPrimaryKey(IdQuestao))
        {
            suitabilityQuestao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            suitabilityQuestao.Fator = Convert.ToInt32(e.NewValues["Fator"]);
            suitabilityQuestao.IdValidacao = Convert.ToInt32(e.NewValues["IdValidacao"]);
            if (suitabilityQuestao.IdValidacao == 0)
            {
                throw new Exception("O campo Tipo Validação possui preenchimento obrigatório");
            }
            suitabilityQuestao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityQuestao: " + IdQuestao + UtilitarioWeb.ToString(suitabilityQuestao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityQuestaoHistorico.createSuitabilityQuestaoHistorico(suitabilityQuestao, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }


}
