﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Configuration;
using System.Web.Configuration;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using Financial.InvestidorCotista;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.InvestidorCotista.Perfil;

using DevExpress.Web;
using DevExpress.Utils;


using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class CadastrosBasicos_Cliente_SuitabilityVerificaPerfilCotista : BasePage
{

    private PlaceHolder ph1;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.ph1 = new PlaceHolder();
        ASPxPopupControl popup = this.PopupCotista();
        ASPxPopupControl popupMensagem = this.PopupMensagemCotista();
        ph1.Controls.Add(popupMensagem);
        ph1.Controls.Add(popup);

        this.FindControl("form1").Controls.Add(this.ph1);
            
        base.Page_Load(sender, e);
        
    }


    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    protected SuitabilityQuestaoCollection getQuestionario(int idCotista)
    {
        SuitabilityQuestaoCollection coll = new SuitabilityQuestaoCollection();
        
        //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
        //para selecao das informações de suitability do cadastro de cliente
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
        {
            if (pessoaSuitability.IdValidacao.HasValue)
            {
                int tipoValidacao = pessoaSuitability.IdValidacao.Value;

                SuitabilityQuestaoQuery suitabilityQuestaoQuery = new SuitabilityQuestaoQuery("SQ");
                SuitabilityOpcaoQuery suitabilityOpcaoQuery = new SuitabilityOpcaoQuery("SO");
                SuitabilityRespostaCotistaDetalheQuery suitabilityRespostaCotistaDetalheQuery = new SuitabilityRespostaCotistaDetalheQuery("SRC");

                suitabilityQuestaoQuery.Select(suitabilityQuestaoQuery.IdQuestao,
                                               suitabilityQuestaoQuery.Descricao.As("Questao"),
                                               suitabilityOpcaoQuery.IdOpcao,
                                               suitabilityOpcaoQuery.Descricao.As("Opcao"),
                                               suitabilityRespostaCotistaDetalheQuery.Data,
                                               suitabilityRespostaCotistaDetalheQuery.IdOpcao.As("Resposta"));
                suitabilityQuestaoQuery.InnerJoin(suitabilityOpcaoQuery).On(suitabilityQuestaoQuery.IdQuestao.Equal(suitabilityOpcaoQuery.IdQuestao));
                suitabilityQuestaoQuery.LeftJoin(suitabilityRespostaCotistaDetalheQuery).On(suitabilityRespostaCotistaDetalheQuery.IdQuestao.Equal(suitabilityQuestaoQuery.IdQuestao) &
                                                                                            suitabilityRespostaCotistaDetalheQuery.IdOpcao.Equal(suitabilityOpcaoQuery.IdOpcao) &
                                                                                            suitabilityRespostaCotistaDetalheQuery.IdCotista.Equal(idCotista) &
                                                                                            suitabilityRespostaCotistaDetalheQuery.Data.Equal("'" + pessoaSuitability.UltimaAlteracao.Value.ToString("yyyyMMdd HH:mm:ss.fff") + "'"));
                suitabilityQuestaoQuery.Where(suitabilityQuestaoQuery.IdValidacao.Equal(tipoValidacao));
                suitabilityQuestaoQuery.OrderBy(suitabilityQuestaoQuery.IdQuestao.Ascending, suitabilityOpcaoQuery.IdOpcao.Ascending);

                coll.Load(suitabilityQuestaoQuery);


                ////Seleciona ultima data da resposta do questionário do cliente
                //SuitabilityRespostaCotistaCollection suitabilityRespostaCotistaCollection = new SuitabilityRespostaCotistaCollection();
                //suitabilityRespostaCotistaCollection.Query.Where(suitabilityRespostaCotistaCollection.Query.IdCotista.Equal(idCotista));
                //suitabilityRespostaCotistaCollection.Query.OrderBy(suitabilityRespostaCotistaCollection.Query.Data.Descending);
                //suitabilityRespostaCotistaCollection.Query.es.Top = 1;
                //suitabilityRespostaCotistaCollection.Query.Load();
                //if (suitabilityRespostaCotistaCollection.Count > 0)
                //{
                //    SuitabilityQuestaoQuery suitabilityQuestaoQuery = new SuitabilityQuestaoQuery("SQ");
                //    SuitabilityOpcaoQuery suitabilityOpcaoQuery = new SuitabilityOpcaoQuery("SO");
                //    SuitabilityRespostaCotistaQuery suitabilityRespostaCotistaQuery = new SuitabilityRespostaCotistaQuery("SRC");
                //    SuitabilityRespostaCotistaDetalheQuery suitabilityRespostaCotistaDetalheQuery = new SuitabilityRespostaCotistaDetalheQuery("SRC");

                //    suitabilityQuestaoQuery.Select(suitabilityQuestaoQuery.IdQuestao,
                //                                   suitabilityQuestaoQuery.Descricao.As("Questao"),
                //                                   suitabilityOpcaoQuery.IdOpcao,
                //                                   suitabilityOpcaoQuery.Descricao.As("Opcao"),
                //                                   suitabilityRespostaCotistaQuery.Data,
                //                                   suitabilityRespostaCotistaDetalheQuery.IdOpcao.As("Resposta"));
                //    suitabilityQuestaoQuery.InnerJoin(suitabilityOpcaoQuery).On(suitabilityQuestaoQuery.IdQuestao.Equal(suitabilityOpcaoQuery.IdQuestao));
                //    suitabilityQuestaoQuery.LeftJoin(suitabilityRespostaCotistaDetalheQuery).On(suitabilityRespostaCotistaDetalheQuery.IdQuestao.Equal(suitabilityQuestaoQuery.IdQuestao) &
                //                                                                                suitabilityRespostaCotistaDetalheQuery.IdOpcao.Equal(suitabilityOpcaoQuery.IdOpcao) &
                //                                                                                suitabilityRespostaCotistaDetalheQuery.IdCotista.Equal(idCotista) &
                //                                                                                suitabilityRespostaCotistaDetalheQuery.Data.Equal("'" + suitabilityRespostaCotistaCollection[0].Data.Value.ToString("yyyyMMdd HH:mm:ss.fff") + "'"));
                //    suitabilityQuestaoQuery.Where(suitabilityQuestaoQuery.IdValidacao.Equal(tipoValidacao));
                //    suitabilityQuestaoQuery.OrderBy(suitabilityQuestaoQuery.IdQuestao.Ascending, suitabilityOpcaoQuery.IdOpcao.Ascending);

                //    coll.Load(suitabilityQuestaoQuery);
                //}
                //else
                //{
                //    SuitabilityQuestaoQuery suitabilityQuestaoQuery = new SuitabilityQuestaoQuery("SQ");
                //    SuitabilityOpcaoQuery suitabilityOpcaoQuery = new SuitabilityOpcaoQuery("SO");
                //    SuitabilityRespostaCotistaQuery suitabilityRespostaCotistaQuery = new SuitabilityRespostaCotistaQuery("SRC");
                //    SuitabilityRespostaCotistaDetalheQuery suitabilityRespostaCotistaDetalheQuery = new SuitabilityRespostaCotistaDetalheQuery("SRC");

                //    suitabilityQuestaoQuery.Select(suitabilityQuestaoQuery.IdQuestao,
                //                                   suitabilityQuestaoQuery.Descricao.As("Questao"),
                //                                   suitabilityOpcaoQuery.IdOpcao,
                //                                   suitabilityOpcaoQuery.Descricao.As("Opcao"),
                //                                   suitabilityRespostaCotistaQuery.Data,
                //                                   suitabilityRespostaCotistaDetalheQuery.IdOpcao.As("Resposta"));
                //    suitabilityQuestaoQuery.InnerJoin(suitabilityOpcaoQuery).On(suitabilityQuestaoQuery.IdQuestao.Equal(suitabilityOpcaoQuery.IdQuestao));
                //    suitabilityQuestaoQuery.LeftJoin(suitabilityRespostaCotistaDetalheQuery).On(suitabilityRespostaCotistaDetalheQuery.IdQuestao.Equal(suitabilityQuestaoQuery.IdQuestao) &
                //                                                                                suitabilityRespostaCotistaDetalheQuery.IdOpcao.Equal(suitabilityOpcaoQuery.IdOpcao) &
                //                                                                                suitabilityRespostaCotistaDetalheQuery.IdCotista.Equal(idCotista));
                //    suitabilityQuestaoQuery.Where(suitabilityQuestaoQuery.IdValidacao.Equal(tipoValidacao));
                //    suitabilityQuestaoQuery.OrderBy(suitabilityQuestaoQuery.IdQuestao.Ascending, suitabilityOpcaoQuery.IdOpcao.Ascending);
                //    coll.Load(suitabilityQuestaoQuery);
                //}

            }
        }
        return coll;
    }


    protected void EsDSSuitabilityRespostaCotistaDetalhe_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityRespostaCotistaDetalheCollection coll = new SuitabilityRespostaCotistaDetalheCollection();

        coll.Query.OrderBy(coll.Query.IdQuestao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityEventosHistoricoCollection coll = new SuitabilityEventosHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }
    
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void updatePanel_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int idCotista = Convert.ToInt32(e.Parameter);

        //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
        //para selecao das informações de suitability do cadastro de cliente
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
        {
            if (pessoaSuitability.Dispensado.Equals("S"))
            {
                e.Result = "S";
            }
            else
            {
                List<Quiz> list = new List<Quiz>();

                foreach (SuitabilityQuestao suitabilityQuestao in getQuestionario(idCotista))
                {
                    list.Add(new Quiz(suitabilityQuestao.GetColumn("Questao").ToString(),
                                      suitabilityQuestao.IdQuestao.Value.ToString(),
                                      suitabilityQuestao.GetColumn("Opcao").ToString(),
                                      suitabilityQuestao.GetColumn("IdOpcao").ToString(),
                                      suitabilityQuestao.GetColumn("Resposta").ToString()));
                }
                JavaScriptSerializer jss = new JavaScriptSerializer();
                string output = jss.Serialize(list);

                e.Result = output;
            }
        }
    }

    protected void updatePerfil_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int idCotista = Convert.ToInt32(e.Parameter);

        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        e.Result = controllerPerfilCotista.updatePerfil(idCotista);
    }

    protected void mensagemRecusa_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = selecionaMensagemEvento(7);
    }

    protected void mensagemAceita_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = selecionaMensagemEvento(3);
    }

    protected string selecionaMensagemEvento(int idMensagem)
    {
        SuitabilityEventos suitabilityEventos = new SuitabilityEventos();
        suitabilityEventos.LoadByPrimaryKey(idMensagem);

        string output = "";

        if (suitabilityEventos.IdMensagem.HasValue)
        {
            SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
            suitabilityMensagens.LoadByPrimaryKey(suitabilityEventos.IdMensagem.Value);

            Mensagem mensagem = new Mensagem();
            mensagem.IdMensagem = suitabilityMensagens.IdMensagem.Value.ToString();
            mensagem.ControlaConcordancia = suitabilityMensagens.ControlaConcordancia;
            mensagem.Descricao = suitabilityMensagens.Descricao;

            JavaScriptSerializer jss = new JavaScriptSerializer();
            output = jss.Serialize(mensagem);
        }

        return output;
    }

    protected void recusa_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int idCotista = Convert.ToInt32(e.Parameter);

        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        controllerPerfilCotista.recusaPerfil(idCotista);
        
    }

    protected void aceita_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        List<ControllerPerfilCotista.Resposta> respostas = JsonConvert.DeserializeObject<List<ControllerPerfilCotista.Resposta>>(e.Parameter);
        int idCotista = Convert.ToInt32(respostas[0].idCotista);

        string login = HttpContext.Current.User.Identity.Name;

        controllerPerfilCotista.aceitePerfil(idCotista, respostas, login);

    }

    protected void atualiza_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        List<ControllerPerfilCotista.Resposta> respostas = JsonConvert.DeserializeObject<List<ControllerPerfilCotista.Resposta>>(e.Parameter);

        ControllerPerfilCotista.PerfilCotista perfilCotista = controllerPerfilCotista.apuraPerfil(respostas);

        if (perfilCotista.Perfil == null)
        {
            e.Result = "";
        }
        else
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            e.Result = jss.Serialize(perfilCotista);
        }
        
    }

    protected void logMensagem_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista.LogMensagem logMensagem = JsonConvert.DeserializeObject<ControllerPerfilCotista.LogMensagem>(e.Parameter);

        SuitabilityLogMensagens.createSuitabilityLogMensagens(HttpContext.Current.User.Identity.Name,
                                                              Convert.ToInt32(logMensagem.IdCotista),
                                                              Convert.ToInt32(logMensagem.IdMensagem),
                                                              logMensagem.Mensagem,
                                                              logMensagem.Resposta);

    }

    protected void enviaEmail(string body, string subject, string from, string to)
    {
        EmailUtil emailUtil = new EmailUtil();
        emailUtil.SendMail(body, subject, from, to);
    }

    public ASPxPopupControl PopupCotista()
    {
        ASPxPopupControl popupCotista = new ASPxPopupControl();
        popupCotista = SetCommonSettings(popupCotista); //Seta properties comuns a todos as popups
        popupCotista.ClientInstanceName = "popupCotista";
        popupCotista.HeaderText = " ";
        popupCotista.ClientSideEvents.CloseUp = "function(s, e) {gridCotista.ClearFilter(); }";

        ASPxGridView gridCotista = new ASPxGridView();
        gridCotista = SetCommonSettings(gridCotista); //Seta properties comuns a todos os grids
        gridCotista.ClientInstanceName = "gridCotista";
        gridCotista.DataSourceID = "EsDSCotista";
        gridCotista.KeyFieldName = "IdCotista";
        gridCotista.SettingsText.Title = "Pesquisa Cotista";

        StringBuilder str = new StringBuilder();
        str.Append("function(s, e) {gridCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCotista); }");
        gridCotista.ClientSideEvents.RowDblClick = str.ToString();

        //Colunas do grid
        GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
        IdColumn.FieldName = "IdCotista";
        IdColumn.ReadOnly = true;
        IdColumn.VisibleIndex = 0;
        IdColumn.Width = Unit.Percentage(20);

        GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
        ApelidoColumn.FieldName = "Apelido";
        ApelidoColumn.VisibleIndex = 1;
        ApelidoColumn.Width = Unit.Percentage(80);

        gridCotista.Columns.Add(IdColumn);
        gridCotista.Columns.Add(ApelidoColumn);
        //

        //Troca de Like pelo Contains no filtro
        UtilitarioGrid.GridFilterContains(gridCotista);

        //Eventos de server-side
        gridCotista.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
        gridCotista.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCotista_CustomDataCallBack);

        //Adiciona na popup e retorna a a popup
        popupCotista.Controls.Add(gridCotista);
        return popupCotista;
    }

    public ASPxPopupControl PopupMensagemCotista()
    {
        ASPxPopupControl popupMensagem = new ASPxPopupControl();
        popupMensagem.ID = "popupMensagemCotista";
        popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
        popupMensagem.PopupElementID = "btnEditCodigoCotista";

        return popupMensagem;
    }

    private ASPxGridView SetCommonSettings(ASPxGridView grid)
    {
        grid.Width = Unit.Percentage(100);
        grid.AutoGenerateColumns = false;
        //grid.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
        //grid.CssPostfix = "Glass";
        grid.Settings.ShowFilterRow = true;
        grid.Settings.ShowTitlePanel = true;
        grid.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
        grid.SettingsDetail.ShowDetailButtons = false;
        grid.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        grid.Styles.Cell.Wrap = DefaultBoolean.False;
        //grid.Styles.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
        //grid.Styles.CssPostfix = "Glass";
        //grid.Images.ImageFolder = "~/App_Themes/Glass/{0}/";
        grid.Images.PopupEditFormWindowClose.Height = Unit.Pixel(17);
        grid.Images.PopupEditFormWindowClose.Width = Unit.Pixel(17);
        grid.SettingsText.EmptyDataRow = "0 Registros";
        grid.ClientSideEvents.Init = "function(s, e) {e.cancel = true;} ";

        return grid;
    }

    private ASPxPopupControl SetCommonSettings(ASPxPopupControl popup)
    {
        popup.Width = Unit.Pixel(500);
        popup.ContentStyle.VerticalAlign = VerticalAlign.Top;
        popup.PopupVerticalAlign = PopupVerticalAlign.Middle;
        popup.PopupHorizontalAlign = PopupHorizontalAlign.OutsideRight;
        popup.AllowDragging = true;

        return popup;
    }

    private void SetaCoresGrid(ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    private void Grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        this.SetaCoresGrid(e);
    }

    public void gridCotista_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
    }

    private ASPxPopupControl SetCommonSettingsMensagem(ASPxPopupControl popup)
    {
        popup.CloseAction = CloseAction.MouseOut;
        popup.ForeColor = Color.Red;
        popup.PopupVerticalAlign = PopupVerticalAlign.Middle;
        popup.PopupHorizontalAlign = PopupHorizontalAlign.OutsideRight;
        popup.Left = 100;
        popup.PopupAction = PopupAction.None;
        popup.HeaderText = "";

        return popup;
    }

    public class Quiz
    {
        public string QuestaoDescricao;
        public string QuestaoId;
        public string OpcaoDescricao;
        public string OpcaoId;
        public string OpcaoResposta;

        public Quiz(string QuestaoDescricao, string QuestaoId, string OpcaoDescricao, string OpcaoId, string OpcaoResposta)
        {
            this.QuestaoDescricao = QuestaoDescricao;
            this.QuestaoId = QuestaoId;
            this.OpcaoDescricao = OpcaoDescricao;
            this.OpcaoId = OpcaoId;
            this.OpcaoResposta = OpcaoResposta;
        }
    }

    public class Mensagem
    {
        public string IdMensagem;
        public string Descricao;
        public string ControlaConcordancia;
    }

}

