﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;

using DevExpress.Web;

public partial class CadastrosBasicos_Cliente_SuitabilityEventoMensagem : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
               new List<string>(new string[] { SuitabilityOpcaoMetadata.ColumnNames.IdQuestao}));
    }

    protected void EsDSSuitabilityEvento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityEventosCollection coll = new SuitabilityEventosCollection();

        coll.Query.OrderBy(coll.Query.IdSuitabilityEventos.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityMensagem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityMensagensCollection coll = new SuitabilityMensagensCollection();

        coll.Query.OrderBy(coll.Query.IdMensagem.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityEventosHistoricoCollection coll = new SuitabilityEventosHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }
    
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityEventos suitabilityEventos = new SuitabilityEventos();
        short idEvento = Convert.ToInt16(e.Keys[0]);

        if (suitabilityEventos.LoadByPrimaryKey(idEvento))
        {
            suitabilityEventos.IdMensagem = Convert.ToInt32(e.NewValues["IdMensagem"]);
            suitabilityEventos.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityEventos: " + idEvento + UtilitarioWeb.ToString(suitabilityEventos),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityEventosHistorico.createSuitabilityEventosHistorico(suitabilityEventos, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }
}
