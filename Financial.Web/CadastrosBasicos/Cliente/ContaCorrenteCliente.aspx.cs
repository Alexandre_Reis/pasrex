﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.CRM;

public partial class CadastrosBasicos_ContaCorrenteCliente : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        this.AllowUpdate = false;
        base.Page_Load(sender, e);
    }

    protected void callbackCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            string nome = "";
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = idCliente.ToString() + " - " + cliente.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
            e.Result = nome;
        }
    }

    #region EsSelect


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContaCorrenteCollection coll = new ContaCorrenteCollection();
        e.Collection = coll;

        int idClienteFiltro = 0;
        if (btnEditCodigoClienteFiltro != null && !string.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
            idClienteFiltro = Convert.ToInt32(btnEditCodigoClienteFiltro.Text);

        if (idClienteFiltro != 0)
        {
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("ccQuery");
            BancoQuery bancoQuery = new BancoQuery("bancoQuery");
            AgenciaQuery agenciaQuery = new AgenciaQuery("agenciaQuery");
            MoedaQuery moedaQuery = new MoedaQuery("moedaQuery");
            ClienteQuery clienteQuery = new ClienteQuery("clienteQuery");

            StringBuilder contaDefaultField = new StringBuilder();
            contaDefaultField.Append("< CASE WHEN ccQuery.ContaDefaultCliente = 'S' AND ccQuery.[idCliente] <> ").Append(idClienteFiltro);
            contaDefaultField.Append(" THEN 'N'");
            contaDefaultField.Append(" ELSE ccQuery.ContaDefaultCliente");
            contaDefaultField.Append(" END as ContaDefaultCliente>");

            contaCorrenteQuery.Select(contaCorrenteQuery.IdConta,
                                      bancoQuery.Nome.As("Banco"),
                                      agenciaQuery.Nome.As("Agencia"),
                                      contaCorrenteQuery.Numero,
                                      contaCorrenteQuery.DigitoConta,
                                      moedaQuery.Nome.As("Moeda"),
                                      contaCorrenteQuery.IdCliente,
                                      contaDefaultField.ToString());
            contaCorrenteQuery.LeftJoin(bancoQuery).On(bancoQuery.IdBanco.Equal(contaCorrenteQuery.IdBanco));
            contaCorrenteQuery.LeftJoin(agenciaQuery).On(agenciaQuery.IdAgencia.Equal(contaCorrenteQuery.IdAgencia));
            contaCorrenteQuery.LeftJoin(moedaQuery).On(moedaQuery.IdMoeda.Equal(contaCorrenteQuery.IdMoeda));
            contaCorrenteQuery.LeftJoin(clienteQuery).On(clienteQuery.IdPessoa.Equal(contaCorrenteQuery.IdPessoa));

            contaCorrenteQuery.Where(clienteQuery.IdCliente.Equal(idClienteFiltro));

            if (coll.Load(contaCorrenteQuery))
                e.Collection = coll;
        }
    }
    #endregion

    #region GridCadastro

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnVincular" || e.Parameters == "btnDesvincular")
        {
            int? idCliente = null;

            List<ContaCorrente> lstContasDefault = new List<ContaCorrente>();
            if (e.Parameters == "btnVincular")
                idCliente = Convert.ToInt32(btnEditCodigoClienteFiltro.Text);

            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdConta");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                #region Busca CheckBox atualizado
                int rowIndex = (sender as ASPxGridView).FindVisibleIndexByKeyValue(keyValuesId[i]);
                ASPxCheckBox checkContaDefault = gridCadastro.FindRowCellTemplateControl(rowIndex, null, "cbContaDefault") as ASPxCheckBox;
                ((IPostBackDataHandler)checkContaDefault).LoadPostData("", Request.Params);
                #endregion

                ContaCorrente contaCorrente = new ContaCorrente();

                int idConta = Convert.ToInt32(keyValuesId[i]);

                if (contaCorrente.LoadByPrimaryKey(idConta))
                {
                    ContaCorrente contaCorrenteClone = (ContaCorrente)Utilitario.Clone(contaCorrente);
                    //

                    if (idCliente.HasValue)
                    {
                        if (contaCorrente.IdCliente.HasValue && contaCorrente.IdCliente.Value != idCliente.Value)
                            throw new Exception("Conta Corrente Id - " + idConta + " já foi está vinculada a outro cliente!");

                        #region Valida Conta Default
                        ContaCorrenteCollection collContasDefault = new ContaCorrenteCollection();
                        collContasDefault.Query.Where(collContasDefault.Query.IdCliente.Equal(idCliente)
                                                     & collContasDefault.Query.ContaDefaultCliente.Equal("S"));

                        if (collContasDefault.Query.Load())
                            lstContasDefault = (List<ContaCorrente>)collContasDefault;

                        if (lstContasDefault.Count > 0 && checkContaDefault.Checked)
                        {
                            ContaCorrente contaDefault = lstContasDefault.Find(delegate(ContaCorrente x) { return x.IdMoeda == contaCorrente.IdMoeda && x.IdCliente == idCliente && x.IdConta != contaCorrente.IdConta; });
                            if (contaDefault != null && contaDefault.IdConta.GetValueOrDefault(0) != 0)
                            {
                                Moeda moeda = new Moeda();
                                moeda.LoadByPrimaryKey((short)contaDefault.IdMoeda.Value);
                                throw new Exception("Já existe conta default cadastrada para o Cliente e Moeda selecionados! \n" + " Id.Conta: " + contaCorrente.IdConta + " Moeda: " + moeda.Nome);
                            }
                        }
                        #endregion

                        contaCorrente.ContaDefaultCliente = checkContaDefault.Checked ? "S" : "N";
                    }
                    else
                    {
                        if (contaCorrente.IdCliente.HasValue && contaCorrente.IdCliente.Value != Convert.ToInt32(btnEditCodigoClienteFiltro.Text))
                            throw new Exception("A conta já foi vinculada anteriormente e não pode ser desvinculada por outro cliente!");

                        contaCorrente.ContaDefaultCliente = "N";
                    }

                    contaCorrente.IdCliente = idCliente;
                    

                    contaCorrente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Associação CC X Cliente - Operacao: Update ContaCorrente: " + idConta + UtilitarioWeb.ToString(contaCorrenteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion
}