﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Investidor.Controller;
using Financial.Fundo.Enums;
using Financial.Relatorio;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using Financial.Tributo;
using Financial.Enquadra;
using Financial.Captacao;
using EntitySpaces.Core;
using System.IO;

public partial class CadastrosBasicos_Cliente : CadastroBasePage
{
    private Dictionary<int, int> ClienteBloqueadoSinacor;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupPessoa = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { ClienteMetadata.ColumnNames.StatusAtivo }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClienteBolsaQuery clienteBolsaQuery = new ClienteBolsaQuery("B");
        ClienteBMFQuery clienteBMFQuery = new ClienteBMFQuery("M");
        ClienteRendaFixaQuery clienteRendaFixaQuery = new ClienteRendaFixaQuery("R");
        ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("I");
        PessoaQuery pessoaQuery = new PessoaQuery("Pe");

        clienteQuery.Select(clienteQuery, clienteBolsaQuery.TipoCotacao.As("TipoCotacaoBolsa"),
                                          clienteBolsaQuery.IsentoIR.As("IsentoIR_Bolsa"),
                                          clienteBolsaQuery.CodigoSinacor.As("CodigoSinacorBolsa"),
                                          clienteBolsaQuery.CodigoSinacor2.As("CodigoSinacorBolsa2"),
                                          clienteBolsaQuery.IdAssessor.As("IdAssessorBolsa"),
                                          clienteBolsaQuery.TipoCusto.As("TipoCustoBolsa"),
                                          clienteBolsaQuery.IdCustodianteAcoes,
                                          clienteBolsaQuery.IdCustodianteOpcoes,
                                          clienteBMFQuery.Socio, clienteBMFQuery.InvestidorInstitucional,
                                          clienteBMFQuery.TipoCotacao.As("TipoCotacaoBMF"),
                                          clienteBMFQuery.CodigoSinacor.As("CodigoSinacorBMF"),
                                          clienteBMFQuery.IdAssessor.As("IdAssessorBMF"),
                                          clienteBMFQuery.TipoPlataforma.As("TipoPlataforma"),
                                          clienteBMFQuery.IdCustodianteBmf,
                                          clienteRendaFixaQuery.UsaCustoMedio.As("UsaCustoMedioRF"),
                                          clienteRendaFixaQuery.IsentoIR.As("IsentoIR_RF"),
                                          clienteRendaFixaQuery.IsentoIOF.As("IsentoIOF_RF"),
                                          clienteRendaFixaQuery.CodigoInterface.As("CodigoInterfaceRF"),
                                          clienteRendaFixaQuery.IdAssessor.As("IdAssessorRendaFixa"),
                                          clienteRendaFixaQuery.CodigoCetip.As("CodigoCetip"),
                                          clienteRendaFixaQuery.AtivoCetip.As("AtivoCetip"),
                                          clienteInterfaceQuery.CodigoCVM, clienteInterfaceQuery.CodigoYMF,
                                          clienteInterfaceQuery.CodigoGazeta, clienteInterfaceQuery.CodigoRetencaoDIRF,
                                          clienteInterfaceQuery.CodigoSwift, clienteInterfaceQuery.RegistroBovespa,
                                          clienteInterfaceQuery.InterfaceSinacorCC,
                                          clienteInterfaceQuery.CodigoIsin, clienteInterfaceQuery.CodigoCusip,
                                          pessoaQuery.Apelido.As("ApelidoPessoa"));

        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.LeftJoin(clienteBolsaQuery).On(clienteQuery.IdCliente == clienteBolsaQuery.IdCliente);
        clienteQuery.LeftJoin(clienteBMFQuery).On(clienteQuery.IdCliente == clienteBMFQuery.IdCliente);
        clienteQuery.LeftJoin(clienteRendaFixaQuery).On(clienteQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
        clienteQuery.LeftJoin(clienteInterfaceQuery).On(clienteQuery.IdCliente == clienteInterfaceQuery.IdCliente);
        clienteQuery.LeftJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);

        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        //Query sem vinculo de permissionamento (fundos somente Cotação)
        clienteQuery = new ClienteQuery("C");
        clienteBolsaQuery = new ClienteBolsaQuery("B");
        clienteBMFQuery = new ClienteBMFQuery("M");
        clienteRendaFixaQuery = new ClienteRendaFixaQuery("R");
        clienteInterfaceQuery = new ClienteInterfaceQuery("I");
        pessoaQuery = new PessoaQuery("Pe");

        clienteQuery.Select(clienteQuery, clienteBolsaQuery.TipoCotacao.As("TipoCotacaoBolsa"),
                                          clienteBolsaQuery.IsentoIR.As("IsentoIR_Bolsa"),
                                          clienteBolsaQuery.CodigoSinacor.As("CodigoSinacorBolsa"),
                                          clienteBolsaQuery.CodigoSinacor2.As("CodigoSinacorBolsa2"),
                                          clienteBolsaQuery.IdAssessor.As("IdAssessorBolsa"),
                                          clienteBolsaQuery.TipoCusto.As("TipoCustoBolsa"),
                                          clienteBolsaQuery.IdCustodianteAcoes,
                                          clienteBolsaQuery.IdCustodianteOpcoes,
                                          clienteBMFQuery.Socio, clienteBMFQuery.InvestidorInstitucional,
                                          clienteBMFQuery.TipoCotacao.As("TipoCotacaoBMF"),
                                          clienteBMFQuery.CodigoSinacor.As("CodigoSinacorBMF"),
                                          clienteBMFQuery.IdAssessor.As("IdAssessorBMF"),
                                          clienteBMFQuery.TipoPlataforma.As("TipoPlataforma"),
                                          clienteRendaFixaQuery.UsaCustoMedio.As("UsaCustoMedioRF"),
                                          clienteRendaFixaQuery.IsentoIR.As("IsentoIR_RF"),
                                          clienteRendaFixaQuery.IsentoIOF.As("IsentoIOF_RF"),
                                          clienteRendaFixaQuery.CodigoInterface.As("CodigoInterfaceRF"),
                                          clienteRendaFixaQuery.IdAssessor.As("IdAssessorRendaFixa"),
                                          clienteRendaFixaQuery.CodigoCetip.As("CodigoCetip"),
                                          clienteRendaFixaQuery.AtivoCetip.As("AtivoCetip"),
                                          clienteInterfaceQuery.CodigoCVM, clienteInterfaceQuery.CodigoYMF,
                                          clienteInterfaceQuery.CodigoGazeta, clienteInterfaceQuery.CodigoRetencaoDIRF,
                                          clienteInterfaceQuery.CodigoSwift, clienteInterfaceQuery.RegistroBovespa,
                                          clienteInterfaceQuery.CodigoIsin, clienteInterfaceQuery.CodigoCusip,
                                          pessoaQuery.Apelido.As("ApelidoPessoa"));

        clienteQuery.LeftJoin(clienteBolsaQuery).On(clienteQuery.IdCliente == clienteBolsaQuery.IdCliente);
        clienteQuery.LeftJoin(clienteBMFQuery).On(clienteQuery.IdCliente == clienteBMFQuery.IdCliente);
        clienteQuery.LeftJoin(clienteRendaFixaQuery).On(clienteQuery.IdCliente == clienteRendaFixaQuery.IdCliente);
        clienteQuery.LeftJoin(clienteInterfaceQuery).On(clienteQuery.IdCliente == clienteInterfaceQuery.IdCliente);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
        clienteQuery.LeftJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void OnWindowCallback()
    {
        throw new Exception("aviso");
    }

    protected void EsDSTipoClienteLote_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        TipoCliente t = new TipoCliente();
        t.IdTipo = null;
        t.Descricao = "";

        // Attach
        coll.AttachEntity(t);
        coll.Sort = TipoClienteMetadata.ColumnNames.Descricao + " ASC";

        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocal_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();
        coll.LoadAll();
        coll.DetachEntity(coll.FindByPrimaryKey(LocalFeriadoFixo.Bovespa));

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAssessor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AssessorCollection coll = new AssessorCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoAfinidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoAfinidadeCollection coll = new GrupoAfinidadeCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCustodiante_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Select(coll.Query.IdAgente, coll.Query.Nome)
                  .Where(coll.Query.FuncaoCustodiante == 'S');
        //
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        //
        coll.Query.Load();
        //

        AgenteMercado a = new AgenteMercado();
        a.IdAgente = null;
        a.Nome = "";

        // Attach
        coll.AttachEntity(a);
        coll.Sort = AgenteMercadoMetadata.ColumnNames.Nome + " ASC";

        e.Collection = coll;
    }
    #endregion

    #region CallBacks
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        //
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropTipoControle = pageControl.FindControl("dropTipoControle") as ASPxComboBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropGrupoProcessamento = pageControl.FindControl("dropGrupoProcessamento") as ASPxComboBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocal = pageControl.FindControl("dropLocal") as ASPxComboBox;
        ASPxComboBox dropApuraGanhoRV = pageControl.FindControl("dropApuraGanhoRV") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
        ASPxDateEdit textDataImplantacao = pageControl.FindControl("textDataImplantacao") as ASPxDateEdit;
        ASPxComboBox dropIsentoIR = pageControl.FindControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropZeraCaixa = pageControl.FindControl("dropZeraCaixa") as ASPxComboBox;
        ASPxTextBox textRegime = pageControl.FindControl("textRegime") as ASPxTextBox;
        ASPxComboBox dropLocalNegociacao = pageControl.FindControl("dropLocalNegociacao") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoPessoa);
        controles.Add(dropStatusAtivo);
        controles.Add(dropTipoControle);
        controles.Add(dropTipo);
        controles.Add(dropGrupoProcessamento);
        controles.Add(dropMoeda);
        controles.Add(dropLocal);
        controles.Add(dropApuraGanhoRV);
        controles.Add(dropIsentoIOF);
        controles.Add(textDataImplantacao);
        controles.Add(dropIsentoIR);
        controles.Add(dropZeraCaixa);

        ASPxComboBox dropUsaCustoMedio = pageControl.FindControl("dropUsaCustoMedio") as ASPxComboBox;
        ASPxComboBox dropIsentoIR_RF = pageControl.FindControl("dropIsentoIR_RF") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF_RF = pageControl.FindControl("dropIsentoIOF_RF") as ASPxComboBox;
        ASPxComboBox dropAssessorRF = pageControl.FindControl("dropAssessorRF") as ASPxComboBox;
        ASPxComboBox dropAtivoCetip = pageControl.FindControl("dropAtivoCetip") as ASPxComboBox;


        if (dropUsaCustoMedio.SelectedIndex > -1 ||
                     dropIsentoIOF_RF.SelectedIndex > -1 ||
                     dropIsentoIR_RF.SelectedIndex > -1 ||
                     dropAtivoCetip.SelectedIndex > -1
                    )
        {
            controles.Add(dropUsaCustoMedio);
            controles.Add(dropIsentoIOF_RF);
            controles.Add(dropIsentoIR_RF);
            controles.Add(dropAtivoCetip);
        }

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        if (Convert.ToInt32(dropTipo.SelectedItem.Value) == (int)TipoClienteFixo.Fundo)
        {
            controles = new List<Control>();
            controles.Add(dropLocalNegociacao);
            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
        }
        #endregion

        #region Registro já existente
        ASPxTextBox textIdCliente = pageControl.FindControl("textIdCliente") as ASPxTextBox;
        int idCliente = Convert.ToInt32(textIdCliente.Text);

        if (gridCadastro.IsNewRowEditing)
        {
            Cliente Cliente = new Cliente();
            if (Cliente.LoadByPrimaryKey(idCliente))
            {
                e.Result = "O Id Cliente reservado foi utilizado por outro usuário, o campo foi atualizado para o próximo ID disponível!" + "|" + "updateIdCliente";
            }
        }
        #endregion

        #region Confere Login
        if (gridCadastro.IsNewRowEditing)
        {
            TextBox textLogin = pageControl.FindControl("textLogin") as TextBox;
            string login = RetornaLoginAssociado(idCliente);

            if (!String.IsNullOrEmpty(login) && login.Trim() != textLogin.Text.Trim())
            {
                string descricaoGrupo = WebConfig.AppSettings.GrupoCliente;
                GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
                grupoUsuarioCollection.Query.Select(grupoUsuarioCollection.Query.IdGrupo);
                grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.Descricao.Equal(descricaoGrupo));
                if (!grupoUsuarioCollection.Query.Load())
                {
                    e.Result = "Não existe grupo definido para login automático, ou não está devidamente associado no config.";
                    return;
                }

                Usuario usuario = new Usuario();
                if (usuario.BuscaUsuario(textLogin.Text.Trim()))
                {
                    e.Result = "Login já existente! Escolha outro login para associar com o cliente.";
                    return;
                }
            }
        }
        #endregion

        #region Validações tabPage - ClienteBolsa
        ASPxTextBox textCodigoSinacorBolsa = pageControl.FindControl("textCodigoSinacorBolsa") as ASPxTextBox;
        TextBox textCodigoSinacorBolsa2 = pageControl.FindControl("textCodigoSinacorBolsa2") as TextBox;
        //
                
        // Confere CodigoSinacor = CodigoSinacor2 para o mesmo Cliente
        if (textCodigoSinacorBolsa.Text.Trim() == textCodigoSinacorBolsa2.Text.Trim())
        {
            // Se for diferente de branco/vazio
            if (!String.IsNullOrEmpty(textCodigoSinacorBolsa.Text.Trim()))
            {
                e.Result = "Código Sinacor e Código Sinacor 2 não podem ser iguais";
                return;
            }
        }
        #endregion

        #region Validações tabPage - ClienteBMF
        TextBox textCodigoSinacorBMF = pageControl.FindControl("textCodigoSinacorBMF") as TextBox;

        // Confere CodigoSinacor
        if (!String.IsNullOrEmpty(textCodigoSinacorBMF.Text.Trim()))
        {
            ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
            clienteBMFCollection.Query.Select(clienteBMFCollection.Query.IdCliente)
                              .Where(clienteBMFCollection.Query.CodigoSinacor == textCodigoSinacorBMF.Text.Trim() &
                                     clienteBMFCollection.Query.IdCliente != idCliente);
            clienteBMFCollection.Query.Load();

            if (clienteBMFCollection.Count > 0)
            {
                e.Result = "Código Sinacor BMF já existente";
                return;
            }
        }
        #endregion
    }

    /// <summary>
    /// Preenche o controle com o nome do Cliente de acordo com o IdSelecionado
    /// </summary>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();

            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(pessoa.Query.Apelido);
            if (pessoa.LoadByPrimaryKey(campos, idPessoa))
            {
                if (pessoa.str.Apelido != "")
                {
                    nome = pessoa.str.Apelido;
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// Faz Update dos Clientes Selecionados em Lote
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        bool ok = true;

        ASPxComboBox dropReset = PopupControlContentControl3.FindControl("dropReset") as ASPxComboBox;

        List<object> clientesSelecionados = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (clientesSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Clientes.";
        }
        else
        {
            #region Se tem Cliente Selecionado
            if (this.dropReset.SelectedIndex == 0 &&
                this.textDataImplantacao.Text == "" &&
                this.textDataProcessamentoBloqueio.Text == "" &&
                this.dropTipoControle.SelectedIndex == 0 &&
                this.dropTipoClienteLote.SelectedIndex == 0 &&
                this.dropGrupoProcessamentoLote.SelectedIndex == -1 &&
                this.dropDescontoTributoLote.SelectedIndex == 0 &&
                this.dropGrossUPLote.SelectedIndex == 0 &&
                this.dropZeraCaixaLote.SelectedIndex == 0 &&
                this.dropIsentoIRLote.SelectedIndex == 0 &&
                this.dropIsentoIOFLote.SelectedIndex == 0)
            {
                e.Result = "Prencha algum campo.";
                return;
            }

            for (int i = 0; i < clientesSelecionados.Count; i++)
            {
                int idCliente = Convert.ToInt32(clientesSelecionados[i]);

                Cliente cliente = new Cliente();
                Carteira carteira = new Carteira();
                //
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    carteira.LoadByPrimaryKey(idCliente);

                    DateTime? dataImplantacaoNew = null;
                    #region Calcula dataImplantacaoNew (que será a nova dataDia do cliente)
                    if (textDataImplantacao.Text != "")
                    {
                        dataImplantacaoNew = Convert.ToDateTime(textDataImplantacao.Text);
                    }

                    if (dropReset.SelectedIndex > 0)
                    {
                        if (Convert.ToByte(dropReset.SelectedItem.Value) == 1) // Pela Movimentacao
                        {
                            List<DateTime> minDates = this.RetornaListaMinDates(idCliente);

                            if (minDates.Count > 0)
                            {
                                DateTime minDate = DateTime.MaxValue;
                                foreach (DateTime date in minDates)
                                {
                                    if (date < minDate)
                                    {
                                        minDate = date;
                                    }
                                }
                                dataImplantacaoNew = minDate;
                            }
                        }
                        else if (Convert.ToByte(dropReset.SelectedItem.Value) == 2) // Pela Data de Implantação
                        {
                            if (!dataImplantacaoNew.HasValue)
                            {
                                dataImplantacaoNew = cliente.DataImplantacao.Value;
                            }
                        }
                    }
                    #endregion

                    if (dropReset.SelectedIndex > 0)
                    {
                        #region Remover tudo de PosicaoCotista, PosicaoCotistaHistorico, PosicaoCotistaAbertura
                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCliente));
                        posicaoCotistaCollection.Query.Load();
                        posicaoCotistaCollection.MarkAllAsDeleted();
                        posicaoCotistaCollection.Save();

                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCliente));
                        posicaoCotistaHistoricoCollection.Query.Load();
                        posicaoCotistaHistoricoCollection.MarkAllAsDeleted();
                        posicaoCotistaHistoricoCollection.Save();

                        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                        posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCliente));
                        posicaoCotistaAberturaCollection.Query.Load();
                        posicaoCotistaAberturaCollection.MarkAllAsDeleted();
                        posicaoCotistaAberturaCollection.Save();
                        #endregion

                        #region Remover tudo de OperacaoCotista - AplicacaoCotasEspecial, ResgateCotasEspecial, FonteOperacaoCotista.In(ZeraCaixa,ResgateTributos) e HistoricoCota
                        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                        operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
                        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira == idCliente &
                            (operacaoCotistaCollection.Query.TipoOperacao == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial
                                | operacaoCotistaCollection.Query.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial
                                | operacaoCotistaCollection.Query.Fonte.In((byte)FonteOperacaoCotista.ZeraCaixa, (byte)FonteOperacaoCotista.ResgateTributos))
                            );
                        operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);

                        operacaoCotistaCollection.MarkAllAsDeleted();
                        operacaoCotistaCollection.Save();

                        if (carteira.DataInicioCota.HasValue)
                        {
                            carteira.RemoveCotas();
                        }
                        #endregion

                        if (dataImplantacaoNew.HasValue)
                        {
                            #region Remover SaldoCaixa, Liquidacao, LiquidacaoAbertura, LiquidacaoHistorico (deleta com data < dataImplantacaoNew)
                            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCliente),
                                                             saldoCaixaCollection.Query.Data.LessThan(dataImplantacaoNew));
                            saldoCaixaCollection.Query.Load();
                            saldoCaixaCollection.MarkAllAsDeleted();
                            saldoCaixaCollection.Save();

                            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                                             liquidacaoCollection.Query.DataVencimento.LessThan(dataImplantacaoNew));
                            liquidacaoCollection.Query.Load();
                            liquidacaoCollection.MarkAllAsDeleted();
                            liquidacaoCollection.Save();

                            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
                            liquidacaoAberturaCollection.Query.Where(liquidacaoAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                                     liquidacaoAberturaCollection.Query.DataVencimento.LessThan(dataImplantacaoNew));
                            liquidacaoAberturaCollection.Query.Load();
                            liquidacaoAberturaCollection.MarkAllAsDeleted();
                            liquidacaoAberturaCollection.Save();

                            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                      liquidacaoHistoricoCollection.Query.DataVencimento.LessThan(dataImplantacaoNew));
                            liquidacaoHistoricoCollection.Query.Load();
                            liquidacaoHistoricoCollection.MarkAllAsDeleted();
                            liquidacaoHistoricoCollection.Save();
                            #endregion
                        }
                    }

                    if (dataImplantacaoNew.HasValue)
                    {
                        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();

                        cliente.DataImplantacao = dataImplantacaoNew;
                        cliente.DataInicio = dataImplantacaoNew;
                        cliente.DataDia = dataImplantacaoNew;

                        if (carteira.DataInicioCota.HasValue)
                        {
                            carteira.DataInicioCota = dataImplantacaoNew;
                            carteira.Save();
                        }
                    }

                    if (dropTipoControle.SelectedIndex >= 1)
                    {
                        cliente.TipoControle = Convert.ToByte(dropTipoControle.SelectedItem.Value);
                    }

                    /*---------------------------------------------------------------------------------------------------*/

                    if (this.dropTipoClienteLote.SelectedIndex >= 1)
                    {
                        cliente.IdTipo = Convert.ToInt16(this.dropTipoClienteLote.SelectedItem.Value);
                    }
                    if (this.dropIsentoIRLote.SelectedIndex >= 1)
                    {
                        cliente.IsentoIR = (string)this.dropIsentoIRLote.SelectedItem.Value;
                    }
                    if (this.dropIsentoIOFLote.SelectedIndex >= 1)
                    {
                        cliente.IsentoIOF = (string)this.dropIsentoIOFLote.SelectedItem.Value;
                    }
                    if (this.dropDescontoTributoLote.SelectedIndex >= 1)
                    {
                        cliente.DescontaTributoPL = Convert.ToByte(this.dropDescontoTributoLote.SelectedItem.Value);
                    }
                    if (this.dropGrossUPLote.SelectedIndex >= 1)
                    {
                        cliente.GrossUP = Convert.ToByte(this.dropGrossUPLote.SelectedItem.Value);
                    }
                    if (this.dropZeraCaixaLote.SelectedIndex >= 1)
                    {
                        cliente.ZeraCaixa = (string)this.dropZeraCaixaLote.SelectedItem.Value;
                    }
                    if (this.dropGrupoProcessamentoLote.SelectedIndex >= 0)
                    {
                        cliente.IdGrupoProcessamento = Convert.ToInt16(this.dropGrupoProcessamentoLote.SelectedItem.Value);
                    }

                    if (this.textDataProcessamentoBloqueio.Text != "")
                    {
                        cliente.DataBloqueioProcessamento = Convert.ToDateTime(this.textDataProcessamentoBloqueio.Text);
                    }
                    else
                    {
                        cliente.DataBloqueioProcessamento = null;
                    }

                    cliente.DataAtualizacao = DateTime.Now;
                    //
                    cliente.Save();
                }
            }
            e.Result = ok ? "Processo executado com sucesso." : "Processo executado parcialmente.";
            #endregion
        }
    }

    /// <summary>
    /// Copia de IdOrigem para IdDestino.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackAlteraId_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (textIdOrigem.Text == "" || textIdDestino.Text == "")
        {
            e.Result = "Id Origem e Destino precisam ser preenchidos!";
            return;
        }

        int idOrigem = Convert.ToInt32(textIdOrigem.Text);
        int idDestino = Convert.ToInt32(textIdDestino.Text);

        Cliente clienteOrigem = new Cliente();
        if (!clienteOrigem.LoadByPrimaryKey(idOrigem))
        {
            e.Result = "Cliente com Id Origem não existe!";
            return;
        }
        Cliente clienteDestino = new Cliente();
        if (!clienteDestino.LoadByPrimaryKey(idDestino))
        {
            e.Result = "Cliente com Id Destino não existe!";
            return;
        }

        Pessoa pessoaOrigem = new Pessoa();
        if (!pessoaOrigem.LoadByPrimaryKey(idOrigem))
        {
            e.Result = "Pessoa com Id Origem não existe!";
            return;
        }
        Pessoa pessoaDestino = new Pessoa();
        if (!pessoaDestino.LoadByPrimaryKey(idDestino))
        {
            e.Result = "Pessoa com Id Destino não existe!";
            return;
        }

        #region CodigoClienteAgente, ProventoBolsaCliente, PerfilCorretagemBolsa, PerfilCorretagemBMF
        CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
        codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdCliente.Equal(idOrigem));
        codigoClienteAgenteCollection.Query.Load();
        foreach (CodigoClienteAgente codigoClienteAgente in codigoClienteAgenteCollection)
        {
            codigoClienteAgente.IdCliente = idDestino;
        }
        codigoClienteAgenteCollection.Save();

        ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
        proventoBolsaClienteCollection.Query.Where(proventoBolsaClienteCollection.Query.IdCliente.Equal(idOrigem));
        proventoBolsaClienteCollection.Query.Load();
        foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
        {
            proventoBolsaCliente.IdCliente = idDestino;
        }
        proventoBolsaClienteCollection.Save();

        PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollectionDestino = new PerfilCorretagemBolsaCollection();
        PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollectionOrigem = new PerfilCorretagemBolsaCollection();
        perfilCorretagemBolsaCollectionOrigem.Query.Where(perfilCorretagemBolsaCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        perfilCorretagemBolsaCollectionOrigem.Query.Load();
        foreach (PerfilCorretagemBolsa perfilCorretagemBolsa in perfilCorretagemBolsaCollectionOrigem)
        {
            PerfilCorretagemBolsa perfilCorretagemBolsaNew = perfilCorretagemBolsaCollectionDestino.AddNew();
            perfilCorretagemBolsaNew.IdCliente = idDestino;
            perfilCorretagemBolsaNew.DataReferencia = perfilCorretagemBolsa.DataReferencia;
            perfilCorretagemBolsaNew.IdAgente = perfilCorretagemBolsa.IdAgente;
            perfilCorretagemBolsaNew.IdTemplate = perfilCorretagemBolsa.IdTemplate;
            perfilCorretagemBolsaNew.PercentualDesconto = perfilCorretagemBolsa.PercentualDesconto;
            perfilCorretagemBolsaNew.TipoMercado = perfilCorretagemBolsa.TipoMercado;
        }
        try
        {
            perfilCorretagemBolsaCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        perfilCorretagemBolsaCollectionOrigem.MarkAllAsDeleted();
        perfilCorretagemBolsaCollectionOrigem.Save();

        PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();
        perfilCorretagemBMFCollection.Query.Where(perfilCorretagemBMFCollection.Query.IdCliente.Equal(idOrigem));
        perfilCorretagemBMFCollection.Query.Load();
        foreach (PerfilCorretagemBMF perfilCorretagemBMF in perfilCorretagemBMFCollection)
        {
            perfilCorretagemBMF.IdCliente = idDestino;
        }
        perfilCorretagemBMFCollection.Save();
        #endregion

        #region BookCliente, PermissaoCliente, ListaBenchmark, TemplateCarteira, Usuario
        BookClienteCollection bookClienteCollectionDestino = new BookClienteCollection();
        BookClienteCollection bookClienteCollectionOrigem = new BookClienteCollection();
        bookClienteCollectionOrigem.Query.Where(bookClienteCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        bookClienteCollectionOrigem.Query.Load();
        foreach (BookCliente bookCliente in bookClienteCollectionOrigem)
        {
            BookCliente bookClienteNew = bookClienteCollectionDestino.AddNew();
            bookClienteNew.IdCliente = idDestino;
            bookClienteNew.IdBook = bookCliente.IdBook.Value;
            bookClienteNew.Imprime = bookCliente.Imprime;
        }

        try
        {
            bookClienteCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        bookClienteCollectionOrigem.MarkAllAsDeleted();
        bookClienteCollectionOrigem.Save();

        PermissaoClienteCollection permissaoClienteCollectionDestino = new PermissaoClienteCollection();
        PermissaoClienteCollection permissaoClienteCollectionOrigem = new PermissaoClienteCollection();
        permissaoClienteCollectionOrigem.Query.Where(permissaoClienteCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        permissaoClienteCollectionOrigem.Query.Load();
        foreach (PermissaoCliente permissaoCliente in permissaoClienteCollectionOrigem)
        {
            PermissaoCliente permissaoClienteNew = permissaoClienteCollectionDestino.AddNew();
            permissaoClienteNew.IdCliente = idDestino;
            permissaoClienteNew.IdUsuario = permissaoCliente.IdUsuario.Value;
        }

        try
        {
            permissaoClienteCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        permissaoClienteCollectionOrigem.MarkAllAsDeleted();
        permissaoClienteCollectionOrigem.Save();

        ListaBenchmarkCollection listaBenchmarkCollectionDestino = new ListaBenchmarkCollection();
        ListaBenchmarkCollection listaBenchmarkCollectionOrigem = new ListaBenchmarkCollection();
        listaBenchmarkCollectionOrigem.Query.Where(listaBenchmarkCollectionOrigem.Query.IdCarteira.Equal(idOrigem));
        listaBenchmarkCollectionOrigem.Query.Load();
        foreach (ListaBenchmark listaBenchmark in listaBenchmarkCollectionOrigem)
        {
            ListaBenchmark listaBenchmarkNew = listaBenchmarkCollectionDestino.AddNew();
            listaBenchmarkNew.IdCarteira = idDestino;
            listaBenchmarkNew.IdIndice = listaBenchmark.IdIndice.Value;
        }

        try
        {
            listaBenchmarkCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        listaBenchmarkCollectionOrigem.MarkAllAsDeleted();
        listaBenchmarkCollectionOrigem.Save();

        TemplateCarteiraCollection templateCarteiraCollectionDestino = new TemplateCarteiraCollection();
        TemplateCarteiraCollection templateCarteiraCollectionOrigem = new TemplateCarteiraCollection();
        templateCarteiraCollectionOrigem.Query.Where(templateCarteiraCollectionOrigem.Query.IdCarteira.Equal(idOrigem));
        templateCarteiraCollectionOrigem.Query.Load();
        foreach (TemplateCarteira templateCarteira in templateCarteiraCollectionOrigem)
        {
            TemplateCarteira templateCarteiraNew = templateCarteiraCollectionDestino.AddNew();
            templateCarteiraNew.IdCarteira = idDestino;
            templateCarteiraNew.Descricao = templateCarteira.Descricao;
        }

        try
        {
            templateCarteiraCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        templateCarteiraCollectionOrigem.MarkAllAsDeleted();
        templateCarteiraCollectionOrigem.Save();

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Query.Where(usuarioCollection.Query.IdCliente.Equal(idOrigem));
        usuarioCollection.Query.Load();

        foreach (Usuario usuario in usuarioCollection)
        {
            usuario.IdCliente = idDestino;
        }
        usuarioCollection.Save();
        #endregion

        #region Ordem, Operacao, Transferencia, TabelaCONR
        OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
        ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdCliente.Equal(idOrigem));
        ordemBolsaCollection.Query.Load();
        foreach (OrdemBolsa ordemBolsa in ordemBolsaCollection)
        {
            ordemBolsa.IdCliente = idDestino;
        }
        ordemBolsaCollection.Save();

        OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
        ordemBMFCollection.Query.Where(ordemBMFCollection.Query.IdCliente.Equal(idOrigem));
        ordemBMFCollection.Query.Load();
        foreach (OrdemBMF ordemBMF in ordemBMFCollection)
        {
            ordemBMF.IdCliente = idDestino;
        }
        ordemBMFCollection.Save();

        OrdemRendaFixaCollection ordemRendaFixaCollection = new OrdemRendaFixaCollection();
        ordemRendaFixaCollection.Query.Where(ordemRendaFixaCollection.Query.IdCliente.Equal(idOrigem));
        ordemRendaFixaCollection.Query.Load();
        foreach (OrdemRendaFixa ordemRendaFixa in ordemRendaFixaCollection)
        {
            ordemRendaFixa.IdCliente = idDestino;
        }
        ordemRendaFixaCollection.Save();

        OrdemFundoCollection ordemFundoCollection = new OrdemFundoCollection();
        ordemFundoCollection.Query.Where(ordemFundoCollection.Query.IdCliente.Equal(idOrigem));
        ordemFundoCollection.Query.Load();
        foreach (OrdemFundo ordemFundo in ordemFundoCollection)
        {
            ordemFundo.IdCliente = idDestino;
        }
        ordemFundoCollection.Save();

        OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
        ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCarteira.Equal(idOrigem));
        ordemCotistaCollection.Query.Load();
        foreach (OrdemCotista ordemCotista in ordemCotistaCollection)
        {
            ordemCotista.IdCarteira = idDestino;
        }
        ordemCotistaCollection.Save();

        OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
        operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        operacaoBolsaCollection.Query.Load();
        foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
        {
            operacaoBolsa.IdCliente = idDestino;
        }
        operacaoBolsaCollection.Save();

        OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
        operacaoEmprestimoBolsaCollection.Query.Where(operacaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        operacaoEmprestimoBolsaCollection.Query.Load();
        foreach (OperacaoEmprestimoBolsa operacaoEmprestimoBolsa in operacaoEmprestimoBolsaCollection)
        {
            operacaoEmprestimoBolsa.IdCliente = idDestino;
        }
        operacaoEmprestimoBolsaCollection.Save();

        OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
        operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idOrigem));
        operacaoBMFCollection.Query.Load();
        foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
        {
            operacaoBMF.IdCliente = idDestino;
        }
        operacaoBMFCollection.Save();

        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(idOrigem));
        operacaoRendaFixaCollection.Query.Load();
        foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
        {
            operacaoRendaFixa.IdCliente = idDestino;
        }
        operacaoRendaFixaCollection.Save();

        OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
        operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idOrigem));
        operacaoFundoCollection.Query.Load();
        foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
        {
            operacaoFundo.IdCliente = idDestino;
        }
        operacaoFundoCollection.Save();

        OperacaoSwapCollection operacaoSwapCollection = new OperacaoSwapCollection();
        operacaoSwapCollection.Query.Where(operacaoSwapCollection.Query.IdCliente.Equal(idOrigem));
        operacaoSwapCollection.Query.Load();
        foreach (OperacaoSwap operacaoSwap in operacaoSwapCollection)
        {
            operacaoSwap.IdCliente = idDestino;
        }
        operacaoSwapCollection.Save();

        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idOrigem));
        operacaoCotistaCollection.Query.Load();
        foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
        {
            operacaoCotista.IdCarteira = idDestino;
        }
        operacaoCotistaCollection.Save();

        operacaoCotistaCollection = new OperacaoCotistaCollection();
        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idOrigem));
        operacaoCotistaCollection.Query.Load();
        foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
        {
            operacaoCotista.IdCotista = idDestino;
        }
        operacaoCotistaCollection.Save();

        TransferenciaBolsaCollection transferenciaBolsaCollection = new TransferenciaBolsaCollection();
        transferenciaBolsaCollection.Query.Where(transferenciaBolsaCollection.Query.IdCliente.Equal(idOrigem));
        transferenciaBolsaCollection.Query.Load();
        foreach (TransferenciaBolsa transferenciaBolsa in transferenciaBolsaCollection)
        {
            transferenciaBolsa.IdCliente = idDestino;
        }
        transferenciaBolsaCollection.Save();

        TransferenciaBMFCollection transferenciaBMFCollection = new TransferenciaBMFCollection();
        transferenciaBMFCollection.Query.Where(transferenciaBMFCollection.Query.IdCliente.Equal(idOrigem));
        transferenciaBMFCollection.Query.Load();
        foreach (TransferenciaBMF transferenciaBMF in transferenciaBMFCollection)
        {
            transferenciaBMF.IdCliente = idDestino;
        }
        transferenciaBMFCollection.Save();

        TransferenciaCotaCollection transferenciaCotaCollection = new TransferenciaCotaCollection();
        transferenciaCotaCollection.Query.Where(transferenciaCotaCollection.Query.IdCarteira.Equal(idOrigem));
        transferenciaCotaCollection.Query.Load();
        foreach (TransferenciaCota transferenciaCota in transferenciaCotaCollection)
        {
            transferenciaCota.IdCarteira = idDestino;
        }
        transferenciaCotaCollection.Save();

        TabelaCONRCollection tabelaCONRCollection = new TabelaCONRCollection();
        tabelaCONRCollection.Query.Where(tabelaCONRCollection.Query.IdCliente.Equal(idOrigem));
        tabelaCONRCollection.Query.Load();
        foreach (TabelaCONR tabelaCONR in tabelaCONRCollection)
        {
            tabelaCONR.IdCliente = idDestino;
        }
        tabelaCONRCollection.Save();
        #endregion

        #region Posicao, PosicaoHistorico, PosicaoAbertura
        PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
        posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBolsaCollection.Query.Load();
        foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
        {
            posicaoBolsa.IdCliente = idDestino;
        }
        posicaoBolsaCollection.Save();

        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
        posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoEmprestimoBolsaCollection.Query.Load();
        foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
        {
            posicaoEmprestimoBolsa.IdCliente = idDestino;
        }
        posicaoEmprestimoBolsaCollection.Save();

        PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
        posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoTermoBolsaCollection.Query.Load();
        foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
        {
            posicaoTermoBolsa.IdCliente = idDestino;
        }
        posicaoTermoBolsaCollection.Save();

        PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
        posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBMFCollection.Query.Load();
        foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
        {
            posicaoBMF.IdCliente = idDestino;
        }
        posicaoBMFCollection.Save();

        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoFundoCollection.Query.Load();
        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
        {
            posicaoFundo.IdCliente = idDestino;
        }
        posicaoFundoCollection.Save();

        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoRendaFixaCollection.Query.Load();
        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
        {
            posicaoRendaFixa.IdCliente = idDestino;
        }
        posicaoRendaFixaCollection.Save();

        PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
        posicaoSwapCollection.Query.Where(posicaoSwapCollection.Query.IdCliente.Equal(idOrigem));
        posicaoSwapCollection.Query.Load();
        foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
        {
            posicaoSwap.IdCliente = idDestino;
        }
        posicaoSwapCollection.Save();

        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idOrigem));
        posicaoCotistaCollection.Query.Load();
        foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
        {
            posicaoCotista.IdCarteira = idDestino;
        }
        posicaoCotistaCollection.Save();

        PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
        posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBolsaHistoricoCollection.Query.Load();
        foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaHistoricoCollection)
        {
            posicaoBolsa.IdCliente = idDestino;
        }
        posicaoBolsaHistoricoCollection.Save();

        PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
        posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoEmprestimoBolsaHistoricoCollection.Query.Load();
        foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsa in posicaoEmprestimoBolsaHistoricoCollection)
        {
            posicaoEmprestimoBolsa.IdCliente = idDestino;
        }
        posicaoEmprestimoBolsaHistoricoCollection.Save();

        PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
        posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoTermoBolsaHistoricoCollection.Query.Load();
        foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsa in posicaoTermoBolsaHistoricoCollection)
        {
            posicaoTermoBolsa.IdCliente = idDestino;
        }
        posicaoTermoBolsaHistoricoCollection.Save();

        PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
        posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBMFHistoricoCollection.Query.Load();
        foreach (PosicaoBMFHistorico posicaoBMF in posicaoBMFHistoricoCollection)
        {
            posicaoBMF.IdCliente = idDestino;
        }
        posicaoBMFHistoricoCollection.Save();

        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
        posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoFundoHistoricoCollection.Query.Load();
        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoHistoricoCollection)
        {
            posicaoFundo.IdCliente = idDestino;
        }
        posicaoFundoHistoricoCollection.Save();

        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
        posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoRendaFixaHistoricoCollection.Query.Load();
        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaHistoricoCollection)
        {
            posicaoRendaFixa.IdCliente = idDestino;
        }
        posicaoRendaFixaHistoricoCollection.Save();

        PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
        posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        posicaoSwapHistoricoCollection.Query.Load();
        foreach (PosicaoSwapHistorico posicaoSwap in posicaoSwapHistoricoCollection)
        {
            posicaoSwap.IdCliente = idDestino;
        }
        posicaoSwapHistoricoCollection.Save();

        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idOrigem));
        posicaoCotistaHistoricoCollection.Query.Load();
        foreach (PosicaoCotistaHistorico posicaoCotista in posicaoCotistaHistoricoCollection)
        {
            posicaoCotista.IdCarteira = idDestino;
        }
        posicaoCotistaHistoricoCollection.Save();

        PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
        posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBolsaAberturaCollection.Query.Load();
        foreach (PosicaoBolsaAbertura posicaoBolsa in posicaoBolsaAberturaCollection)
        {
            posicaoBolsa.IdCliente = idDestino;
        }
        posicaoBolsaAberturaCollection.Save();

        PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
        posicaoEmprestimoBolsaAberturaCollection.Query.Where(posicaoEmprestimoBolsaAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoEmprestimoBolsaAberturaCollection.Query.Load();
        foreach (PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsa in posicaoEmprestimoBolsaAberturaCollection)
        {
            posicaoEmprestimoBolsa.IdCliente = idDestino;
        }
        posicaoEmprestimoBolsaAberturaCollection.Save();

        PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
        posicaoTermoBolsaAberturaCollection.Query.Where(posicaoTermoBolsaAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoTermoBolsaAberturaCollection.Query.Load();
        foreach (PosicaoTermoBolsaAbertura posicaoTermoBolsa in posicaoTermoBolsaAberturaCollection)
        {
            posicaoTermoBolsa.IdCliente = idDestino;
        }
        posicaoTermoBolsaAberturaCollection.Save();

        PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
        posicaoBMFAberturaCollection.Query.Where(posicaoBMFAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoBMFAberturaCollection.Query.Load();
        foreach (PosicaoBMFAbertura posicaoBMF in posicaoBMFAberturaCollection)
        {
            posicaoBMF.IdCliente = idDestino;
        }
        posicaoBMFAberturaCollection.Save();

        PosicaoFundoAberturaCollection posicaoFundoAberturaCollection = new PosicaoFundoAberturaCollection();
        posicaoFundoAberturaCollection.Query.Where(posicaoFundoAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoFundoAberturaCollection.Query.Load();
        foreach (PosicaoFundoAbertura posicaoFundo in posicaoFundoAberturaCollection)
        {
            posicaoFundo.IdCliente = idDestino;
        }
        posicaoFundoAberturaCollection.Save();

        PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
        posicaoRendaFixaAberturaCollection.Query.Where(posicaoRendaFixaAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoRendaFixaAberturaCollection.Query.Load();
        foreach (PosicaoRendaFixaAbertura posicaoRendaFixa in posicaoRendaFixaAberturaCollection)
        {
            posicaoRendaFixa.IdCliente = idDestino;
        }
        posicaoRendaFixaAberturaCollection.Save();

        PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
        posicaoSwapAberturaCollection.Query.Where(posicaoSwapAberturaCollection.Query.IdCliente.Equal(idOrigem));
        posicaoSwapAberturaCollection.Query.Load();
        foreach (PosicaoSwapAbertura posicaoSwap in posicaoSwapAberturaCollection)
        {
            posicaoSwap.IdCliente = idDestino;
        }
        posicaoSwapAberturaCollection.Save();

        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
        posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idOrigem));
        posicaoCotistaAberturaCollection.Query.Load();
        foreach (PosicaoCotistaAbertura posicaoCotista in posicaoCotistaAberturaCollection)
        {
            posicaoCotista.IdCarteira = idDestino;
        }
        posicaoCotistaAberturaCollection.Save();
        #endregion

        #region ApuracaoRendaVariavelIR, ApuracaoIRImobiliario, IRFonte
        ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollectionDestino = new ApuracaoRendaVariavelIRCollection();
        ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollectionOrigem = new ApuracaoRendaVariavelIRCollection();
        apuracaoRendaVariavelIRCollectionOrigem.Query.Where(apuracaoRendaVariavelIRCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        apuracaoRendaVariavelIRCollectionOrigem.Query.Load();
        foreach (ApuracaoRendaVariavelIR apuracaoRendaVariavelIR in apuracaoRendaVariavelIRCollectionOrigem)
        {
            ApuracaoRendaVariavelIR apuracaoRendaVariavelIRNew = apuracaoRendaVariavelIRCollectionDestino.AddNew();
            apuracaoRendaVariavelIRNew.Ano = apuracaoRendaVariavelIR.Ano;
            apuracaoRendaVariavelIRNew.BaseCalculo = apuracaoRendaVariavelIR.BaseCalculo;
            apuracaoRendaVariavelIRNew.BaseCalculoDT = apuracaoRendaVariavelIR.BaseCalculoDT;
            apuracaoRendaVariavelIRNew.IdCliente = idDestino;
            apuracaoRendaVariavelIRNew.ImpostoCalculado = apuracaoRendaVariavelIR.ImpostoCalculado;
            apuracaoRendaVariavelIRNew.ImpostoCalculadoDT = apuracaoRendaVariavelIR.ImpostoCalculadoDT;
            apuracaoRendaVariavelIRNew.ImpostoPagar = apuracaoRendaVariavelIR.ImpostoPagar;
            apuracaoRendaVariavelIRNew.ImpostoPago = apuracaoRendaVariavelIR.ImpostoPago;
            apuracaoRendaVariavelIRNew.IRDayTradeCompensar = apuracaoRendaVariavelIR.IRDayTradeCompensar;
            apuracaoRendaVariavelIRNew.IRDayTradeMes = apuracaoRendaVariavelIR.IRDayTradeMes;
            apuracaoRendaVariavelIRNew.IRDayTradeMesAnterior = apuracaoRendaVariavelIR.IRDayTradeMesAnterior;
            apuracaoRendaVariavelIRNew.IRFonteNormal = apuracaoRendaVariavelIR.IRFonteNormal;
            apuracaoRendaVariavelIRNew.Mes = apuracaoRendaVariavelIR.Mes;
            apuracaoRendaVariavelIRNew.PrejuizoCompensar = apuracaoRendaVariavelIR.PrejuizoCompensar;
            apuracaoRendaVariavelIRNew.PrejuizoCompensarDT = apuracaoRendaVariavelIR.PrejuizoCompensarDT;
            apuracaoRendaVariavelIRNew.ResultadoAcoes = apuracaoRendaVariavelIR.ResultadoAcoes;
            apuracaoRendaVariavelIRNew.ResultadoAcoesDT = apuracaoRendaVariavelIR.ResultadoAcoesDT;
            apuracaoRendaVariavelIRNew.ResultadoFuturoDolar = apuracaoRendaVariavelIR.ResultadoFuturoDolar;
            apuracaoRendaVariavelIRNew.ResultadoFuturoDolarDT = apuracaoRendaVariavelIR.ResultadoFuturoDolarDT;
            apuracaoRendaVariavelIRNew.ResultadoFuturoIndice = apuracaoRendaVariavelIR.ResultadoFuturoIndice;
            apuracaoRendaVariavelIRNew.ResultadoFuturoIndiceDT = apuracaoRendaVariavelIR.ResultadoFuturoIndiceDT;
            apuracaoRendaVariavelIRNew.ResultadoFuturoJuros = apuracaoRendaVariavelIR.ResultadoFuturoJuros;
            apuracaoRendaVariavelIRNew.ResultadoFuturoJurosDT = apuracaoRendaVariavelIR.ResultadoFuturoJurosDT;
            apuracaoRendaVariavelIRNew.ResultadoFuturoOutros = apuracaoRendaVariavelIR.ResultadoFuturoOutros;
            apuracaoRendaVariavelIRNew.ResultadoFuturoOutrosDT = apuracaoRendaVariavelIR.ResultadoFuturoOutrosDT;
            apuracaoRendaVariavelIRNew.ResultadoOpcoes = apuracaoRendaVariavelIR.ResultadoOpcoes;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesDT = apuracaoRendaVariavelIR.ResultadoOpcoesDT;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesNaoBolsa = apuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsa;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesNaoBolsaDT = apuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsaDT;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesOuro = apuracaoRendaVariavelIR.ResultadoOpcoesOuro;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesOuroDT = apuracaoRendaVariavelIR.ResultadoOpcoesOuroDT;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesOutros = apuracaoRendaVariavelIR.ResultadoOpcoesOutros;
            apuracaoRendaVariavelIRNew.ResultadoOpcoesOutrosDT = apuracaoRendaVariavelIR.ResultadoOpcoesOutrosDT;
            apuracaoRendaVariavelIRNew.ResultadoOuro = apuracaoRendaVariavelIR.ResultadoOuro;
            apuracaoRendaVariavelIRNew.ResultadoOuroDT = apuracaoRendaVariavelIR.ResultadoOuroDT;
            apuracaoRendaVariavelIRNew.ResultadoOuroNaoBolsa = apuracaoRendaVariavelIR.ResultadoOuroNaoBolsa;
            apuracaoRendaVariavelIRNew.ResultadoOuroNaoBolsaDT = apuracaoRendaVariavelIR.ResultadoOuroNaoBolsaDT;
            apuracaoRendaVariavelIRNew.ResultadoTermo = apuracaoRendaVariavelIR.ResultadoTermo;
            apuracaoRendaVariavelIRNew.ResultadoTermoDT = apuracaoRendaVariavelIR.ResultadoTermoDT;
            apuracaoRendaVariavelIRNew.ResultadoTermoOutros = apuracaoRendaVariavelIR.ResultadoTermoOutros;
            apuracaoRendaVariavelIRNew.ResultadoTermoOutrosDT = apuracaoRendaVariavelIR.ResultadoTermoOutrosDT;
            apuracaoRendaVariavelIRNew.ResultadoLiquidoMes = apuracaoRendaVariavelIR.ResultadoLiquidoMes;
            apuracaoRendaVariavelIRNew.ResultadoLiquidoMesDT = apuracaoRendaVariavelIR.ResultadoLiquidoMesDT;
            apuracaoRendaVariavelIRNew.ResultadoNegativoAnterior = apuracaoRendaVariavelIR.ResultadoNegativoAnterior;
            apuracaoRendaVariavelIRNew.ResultadoNegativoAnteriorDT = apuracaoRendaVariavelIR.ResultadoNegativoAnteriorDT;
            apuracaoRendaVariavelIRNew.TotalImposto = apuracaoRendaVariavelIR.TotalImposto;
        }

        try
        {
            apuracaoRendaVariavelIRCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        apuracaoRendaVariavelIRCollectionOrigem.MarkAllAsDeleted();
        apuracaoRendaVariavelIRCollectionOrigem.Save();

        ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollectionDestino = new ApuracaoIRImobiliarioCollection();
        ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollectionOrigem = new ApuracaoIRImobiliarioCollection();
        apuracaoIRImobiliarioCollectionOrigem.Query.Where(apuracaoIRImobiliarioCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        apuracaoIRImobiliarioCollectionOrigem.Query.Load();
        foreach (ApuracaoIRImobiliario apuracaoIRImobiliario in apuracaoIRImobiliarioCollectionOrigem)
        {
            ApuracaoIRImobiliario apuracaoIRImobiliarioNew = apuracaoIRImobiliarioCollectionDestino.AddNew();
            apuracaoIRImobiliarioNew.Ano = apuracaoIRImobiliario.Ano;
            apuracaoIRImobiliarioNew.BaseCalculo = apuracaoIRImobiliario.BaseCalculo;
            apuracaoIRImobiliarioNew.BaseCalculoDT = apuracaoIRImobiliario.BaseCalculoDT;
            apuracaoIRImobiliarioNew.IdCliente = idDestino;
            apuracaoIRImobiliarioNew.ImpostoCalculado = apuracaoIRImobiliario.ImpostoCalculado;
            apuracaoIRImobiliarioNew.ImpostoCalculadoDT = apuracaoIRImobiliario.ImpostoCalculadoDT;
            apuracaoIRImobiliarioNew.ImpostoPagar = apuracaoIRImobiliario.ImpostoPagar;
            apuracaoIRImobiliarioNew.ImpostoPago = apuracaoIRImobiliario.ImpostoPago;
            apuracaoIRImobiliarioNew.IRDayTradeCompensar = apuracaoIRImobiliario.IRDayTradeCompensar;
            apuracaoIRImobiliarioNew.IRDayTradeMes = apuracaoIRImobiliario.IRDayTradeMes;
            apuracaoIRImobiliarioNew.IRDayTradeMesAnterior = apuracaoIRImobiliario.IRDayTradeMesAnterior;
            apuracaoIRImobiliarioNew.IRFonteNormal = apuracaoIRImobiliario.IRFonteNormal;
            apuracaoIRImobiliarioNew.Mes = apuracaoIRImobiliario.Mes;
            apuracaoIRImobiliarioNew.PrejuizoCompensar = apuracaoIRImobiliario.PrejuizoCompensar;
            apuracaoIRImobiliarioNew.PrejuizoCompensarDT = apuracaoIRImobiliario.PrejuizoCompensarDT;
            apuracaoIRImobiliarioNew.ResultadoDT = apuracaoIRImobiliario.ResultadoDT;
            apuracaoIRImobiliarioNew.ResultadoLiquidoMes = apuracaoIRImobiliario.ResultadoLiquidoMes;
            apuracaoIRImobiliarioNew.ResultadoLiquidoMesDT = apuracaoIRImobiliario.ResultadoLiquidoMesDT;
            apuracaoIRImobiliarioNew.ResultadoNegativoAnterior = apuracaoIRImobiliario.ResultadoNegativoAnterior;
            apuracaoIRImobiliarioNew.ResultadoNegativoAnteriorDT = apuracaoIRImobiliario.ResultadoNegativoAnteriorDT;
            apuracaoIRImobiliarioNew.ResultadoNormal = apuracaoIRImobiliario.ResultadoNormal;
            apuracaoIRImobiliarioNew.TotalImposto = apuracaoIRImobiliario.TotalImposto;
        }

        try
        {
            apuracaoIRImobiliarioCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        apuracaoIRImobiliarioCollectionOrigem.MarkAllAsDeleted();
        apuracaoIRImobiliarioCollectionOrigem.Save();

        IRFonteCollection irFonteCollectionDestino = new IRFonteCollection();
        IRFonteCollection irFonteCollectionOrigem = new IRFonteCollection();
        irFonteCollectionOrigem.Query.Where(irFonteCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        irFonteCollectionOrigem.Query.Load();
        foreach (IRFonte irFonte in irFonteCollectionOrigem)
        {
            IRFonte irFonteNew = irFonteCollectionDestino.AddNew();
            irFonteNew.Data = irFonte.Data;
            irFonteNew.IdAgente = irFonte.IdAgente;
            irFonteNew.IdCliente = idDestino;
            irFonteNew.Identificador = irFonte.Identificador;
            irFonteNew.ValorBase = irFonte.ValorBase;
            irFonteNew.ValorIR = irFonte.ValorIR;
        }

        try
        {
            irFonteCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        irFonteCollectionOrigem.MarkAllAsDeleted();
        irFonteCollectionOrigem.Save();
        #endregion

        #region ContaCorrente, SaldoCaixa, Liquidacao
        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
        contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(idOrigem));
        contaCorrenteCollection.Query.Load();
        foreach (ContaCorrente contaCorrente in contaCorrenteCollection)
        {
            contaCorrente.IdPessoa = idDestino;
        }
        contaCorrenteCollection.Save();

        SaldoCaixaCollection saldoCaixaCollectionDestino = new SaldoCaixaCollection();
        SaldoCaixaCollection saldoCaixaCollectionOrigem = new SaldoCaixaCollection();
        saldoCaixaCollectionOrigem.Query.Where(saldoCaixaCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        saldoCaixaCollectionOrigem.Query.Load();
        foreach (SaldoCaixa saldoCaixa in saldoCaixaCollectionOrigem)
        {
            SaldoCaixa saldoCaixaNew = saldoCaixaCollectionDestino.AddNew();
            saldoCaixaNew.Data = saldoCaixa.Data.Value;
            saldoCaixaNew.IdCliente = idDestino;
            saldoCaixaNew.IdConta = saldoCaixa.IdConta.Value;
            saldoCaixaNew.SaldoAbertura = saldoCaixa.SaldoAbertura;
            saldoCaixaNew.SaldoFechamento = saldoCaixa.SaldoFechamento;
        }

        try
        {
            saldoCaixaCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        saldoCaixaCollectionOrigem.MarkAllAsDeleted();
        saldoCaixaCollectionOrigem.Save();

        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
        liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoCollection.Query.Load();
        foreach (Liquidacao liquidacao in liquidacaoCollection)
        {
            liquidacao.IdCliente = idDestino;
        }
        liquidacaoCollection.Save();

        LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoHistoricoCollection.Query.Load();
        foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection)
        {
            liquidacaoHistorico.IdCliente = idDestino;
        }
        liquidacaoHistoricoCollection.Save();

        LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
        liquidacaoAberturaCollection.Query.Where(liquidacaoAberturaCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoAberturaCollection.Query.Load();
        foreach (LiquidacaoAbertura liquidacaoAbertura in liquidacaoAberturaCollection)
        {
            liquidacaoAbertura.IdCliente = idDestino;
        }
        liquidacaoAberturaCollection.Save();

        LiquidacaoFuturoCollection liquidacaoFuturoCollection = new LiquidacaoFuturoCollection();
        liquidacaoFuturoCollection.Query.Where(liquidacaoFuturoCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoFuturoCollection.Query.Load();
        foreach (LiquidacaoFuturo liquidacaoFuturo in liquidacaoFuturoCollection)
        {
            liquidacaoFuturo.IdCliente = idDestino;
        }
        liquidacaoFuturoCollection.Save();
        #endregion

        #region LiquidacaoRendaFixa, LiquidacaoTermoBolsa, LiquidacaoEmprestimoBolsa, LiquidacaoSwap
        LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
        liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoRendaFixaCollection.Query.Load();
        foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
        {
            liquidacaoRendaFixa.IdCliente = idDestino;
        }
        liquidacaoRendaFixaCollection.Save();

        LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
        liquidacaoTermoBolsaCollection.Query.Where(liquidacaoTermoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoTermoBolsaCollection.Query.Load();
        foreach (LiquidacaoTermoBolsa liquidacaoTermoBolsa in liquidacaoTermoBolsaCollection)
        {
            liquidacaoTermoBolsa.IdCliente = idDestino;
        }
        liquidacaoTermoBolsaCollection.Save();

        LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
        liquidacaoEmprestimoBolsaCollection.Query.Where(liquidacaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoEmprestimoBolsaCollection.Query.Load();
        foreach (LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa in liquidacaoEmprestimoBolsaCollection)
        {
            liquidacaoEmprestimoBolsa.IdCliente = idDestino;
        }
        liquidacaoEmprestimoBolsaCollection.Save();

        LiquidacaoSwapCollection liquidacaoSwapCollection = new LiquidacaoSwapCollection();
        liquidacaoSwapCollection.Query.Where(liquidacaoSwapCollection.Query.IdCliente.Equal(idOrigem));
        liquidacaoSwapCollection.Query.Load();
        foreach (LiquidacaoSwap liquidacaoSwap in liquidacaoSwapCollection)
        {
            liquidacaoSwap.IdCliente = idDestino;
        }
        liquidacaoSwapCollection.Save();
        #endregion

        #region DetalheResgateFundo, DetalheResgateCotista, PrejuizoFundo, PrejuizoCotista
        DetalheResgateFundoCollection detalheResgateFundoCollection = new DetalheResgateFundoCollection();
        detalheResgateFundoCollection.Query.Where(detalheResgateFundoCollection.Query.IdCliente.Equal(idOrigem));
        detalheResgateFundoCollection.Query.Load();
        foreach (DetalheResgateFundo detalheResgateFundo in detalheResgateFundoCollection)
        {
            detalheResgateFundo.IdCliente = idDestino;
        }
        detalheResgateFundoCollection.Save();

        DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
        detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdCarteira.Equal(idOrigem));
        detalheResgateCotistaCollection.Query.Load();
        foreach (DetalheResgateCotista detalheResgateCotista in detalheResgateCotistaCollection)
        {
            detalheResgateCotista.IdCarteira = idDestino;
        }
        detalheResgateCotistaCollection.Save();

        detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
        detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdCotista.Equal(idOrigem));
        detalheResgateCotistaCollection.Query.Load();
        foreach (DetalheResgateCotista detalheResgateCotista in detalheResgateCotistaCollection)
        {
            detalheResgateCotista.IdCotista = idDestino;
        }
        detalheResgateCotistaCollection.Save();

        PrejuizoFundoCollection prejuizoFundoCollectionDestino = new PrejuizoFundoCollection();
        PrejuizoFundoCollection prejuizoFundoCollectionOrigem = new PrejuizoFundoCollection();
        prejuizoFundoCollectionOrigem.Query.Where(prejuizoFundoCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        prejuizoFundoCollectionOrigem.Query.Load();
        foreach (PrejuizoFundo prejuizoFundo in prejuizoFundoCollectionOrigem)
        {
            PrejuizoFundo prejuizoFundoNew = prejuizoFundoCollectionDestino.AddNew();
            prejuizoFundoNew.Data = prejuizoFundoNew.Data;
            prejuizoFundoNew.DataLimiteCompensacao = prejuizoFundoNew.DataLimiteCompensacao;
            prejuizoFundoNew.IdCarteira = prejuizoFundoNew.IdCarteira;
            prejuizoFundoNew.IdCliente = idDestino;
            prejuizoFundoNew.ValorPrejuizo = prejuizoFundoNew.ValorPrejuizo;
        }

        try
        {
            prejuizoFundoCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        prejuizoFundoCollectionOrigem.MarkAllAsDeleted();
        prejuizoFundoCollectionOrigem.Save();

        PrejuizoCotistaCollection prejuizoCotistaCollectionDestino = new PrejuizoCotistaCollection();
        PrejuizoCotistaCollection prejuizoCotistaCollectionOrigem = new PrejuizoCotistaCollection();
        prejuizoCotistaCollectionOrigem.Query.Where(prejuizoCotistaCollectionOrigem.Query.IdCarteira.Equal(idOrigem));
        prejuizoCotistaCollectionOrigem.Query.Load();
        foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollectionOrigem)
        {
            PrejuizoCotista prejuizoCotistaNew = prejuizoCotistaCollectionDestino.AddNew();
            prejuizoCotistaNew.Data = prejuizoCotistaNew.Data;
            prejuizoCotistaNew.DataLimiteCompensacao = prejuizoCotistaNew.DataLimiteCompensacao;
            prejuizoCotistaNew.IdCarteira = idDestino;
            prejuizoCotistaNew.IdCotista = prejuizoCotistaNew.IdCotista;
            prejuizoCotistaNew.ValorPrejuizo = prejuizoCotistaNew.ValorPrejuizo;
        }

        try
        {
            prejuizoCotistaCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        prejuizoCotistaCollectionOrigem.MarkAllAsDeleted();
        prejuizoCotistaCollectionOrigem.Save();

        PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoCollectionDestino = new PrejuizoFundoHistoricoCollection();
        PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoCollectionOrigem = new PrejuizoFundoHistoricoCollection();
        prejuizoFundoHistoricoCollectionOrigem.Query.Where(prejuizoFundoHistoricoCollectionOrigem.Query.IdCliente.Equal(idOrigem));
        prejuizoFundoHistoricoCollectionOrigem.Query.Load();
        foreach (PrejuizoFundoHistorico prejuizoFundo in prejuizoFundoHistoricoCollectionOrigem)
        {
            PrejuizoFundoHistorico prejuizoFundoNew = prejuizoFundoHistoricoCollectionDestino.AddNew();
            prejuizoFundoNew.Data = prejuizoFundoNew.Data;
            prejuizoFundoNew.DataHistorico = prejuizoFundoNew.DataHistorico;
            prejuizoFundoNew.DataLimiteCompensacao = prejuizoFundoNew.DataLimiteCompensacao;
            prejuizoFundoNew.IdCarteira = prejuizoFundoNew.IdCarteira;
            prejuizoFundoNew.IdCliente = idDestino;
            prejuizoFundoNew.ValorPrejuizo = prejuizoFundoNew.ValorPrejuizo;
        }

        try
        {
            prejuizoFundoHistoricoCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        prejuizoFundoHistoricoCollectionOrigem.MarkAllAsDeleted();
        prejuizoFundoHistoricoCollectionOrigem.Save();

        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollectionDestino = new PrejuizoCotistaHistoricoCollection();
        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollectionOrigem = new PrejuizoCotistaHistoricoCollection();
        prejuizoCotistaHistoricoCollectionOrigem.Query.Where(prejuizoCotistaHistoricoCollectionOrigem.Query.IdCarteira.Equal(idOrigem));
        prejuizoCotistaHistoricoCollectionOrigem.Query.Load();
        foreach (PrejuizoCotistaHistorico prejuizoCotista in prejuizoCotistaHistoricoCollectionOrigem)
        {
            PrejuizoCotistaHistorico prejuizoCotistaNew = prejuizoCotistaHistoricoCollectionDestino.AddNew();
            prejuizoCotistaNew.Data = prejuizoCotistaNew.Data;
            prejuizoCotistaNew.DataHistorico = prejuizoCotistaNew.DataHistorico;
            prejuizoCotistaNew.DataLimiteCompensacao = prejuizoCotistaNew.DataLimiteCompensacao;
            prejuizoCotistaNew.IdCarteira = idDestino;
            prejuizoCotistaNew.IdCotista = prejuizoCotistaNew.IdCotista;
            prejuizoCotistaNew.ValorPrejuizo = prejuizoCotistaNew.ValorPrejuizo;
        }

        try
        {
            prejuizoCotistaHistoricoCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        prejuizoCotistaHistoricoCollectionOrigem.MarkAllAsDeleted();
        prejuizoCotistaHistoricoCollectionOrigem.Save();
        #endregion

        #region AgendaFundo, EnquadraItemFundo, ContabLancamento
        AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
        agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idOrigem));
        agendaFundoCollection.Query.Load();
        foreach (AgendaFundo agendaFundo in agendaFundoCollection)
        {
            agendaFundo.IdCarteira = idDestino;
        }
        agendaFundoCollection.Save();

        EnquadraItemFundoCollection enquadraItemFundoCollection = new EnquadraItemFundoCollection();
        enquadraItemFundoCollection.Query.Where(enquadraItemFundoCollection.Query.IdCarteira.Equal(idOrigem));
        enquadraItemFundoCollection.Query.Load();
        foreach (EnquadraItemFundo enquadraItemFundo in enquadraItemFundoCollection)
        {
            enquadraItemFundo.IdCarteira = idDestino;
        }
        enquadraItemFundoCollection.Save();

        ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
        contabLancamentoCollection.Query.Where(contabLancamentoCollection.Query.IdCliente.Equal(idOrigem));
        contabLancamentoCollection.Query.Load();
        foreach (ContabLancamento contabLancamento in contabLancamentoCollection)
        {
            contabLancamento.IdCliente = idDestino;
        }
        contabLancamentoCollection.Save();
        #endregion

        #region HistoricoCota, Tabela >> TaxaAdministracao, TaxaPerformance, Provisao, Calculo >> Administracao, Performance, Performance
        HistoricoCotaCollection historicoCotaCollectionDestino = new HistoricoCotaCollection();
        HistoricoCotaCollection historicoCotaCollectionOrigem = new HistoricoCotaCollection();
        historicoCotaCollectionOrigem.Query.Where(historicoCotaCollectionOrigem.Query.IdCarteira.Equal(idOrigem));
        historicoCotaCollectionOrigem.Query.Load();
        foreach (HistoricoCota historicoCota in historicoCotaCollectionOrigem)
        {
            HistoricoCota historicoCotaNew = historicoCotaCollectionDestino.AddNew();
            historicoCotaNew.AjustePL = historicoCota.AjustePL;
            historicoCotaNew.CotaAbertura = historicoCota.CotaAbertura;
            historicoCotaNew.CotaBruta = historicoCota.CotaBruta;
            historicoCotaNew.CotaFechamento = historicoCota.CotaFechamento;
            historicoCotaNew.Data = historicoCota.Data;
            historicoCotaNew.IdCarteira = idDestino;
            historicoCotaNew.PatrimonioBruto = historicoCota.PatrimonioBruto;
            historicoCotaNew.PLAbertura = historicoCota.PLAbertura;
            historicoCotaNew.PLFechamento = historicoCota.PLFechamento;
            historicoCotaNew.QuantidadeFechamento = historicoCota.QuantidadeFechamento;
        }

        try
        {
            historicoCotaCollectionDestino.Save();
        }
        catch (Exception)
        {
        }

        historicoCotaCollectionOrigem.MarkAllAsDeleted();
        historicoCotaCollectionOrigem.Save();

        TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
        tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira.Equal(idOrigem));
        tabelaTaxaAdministracaoCollection.Query.Load();
        foreach (TabelaTaxaAdministracao tabelaTaxaAdministracao in tabelaTaxaAdministracaoCollection)
        {
            tabelaTaxaAdministracao.IdCarteira = idDestino;
        }
        tabelaTaxaAdministracaoCollection.Save();

        TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
        tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idOrigem));
        tabelaTaxaPerformanceCollection.Query.Load();
        foreach (TabelaTaxaPerformance tabelaTaxaPerformance in tabelaTaxaPerformanceCollection)
        {
            tabelaTaxaPerformance.IdCarteira = idDestino;
        }
        tabelaTaxaPerformanceCollection.Save();

        TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
        tabelaProvisaoCollection.Query.Where(tabelaProvisaoCollection.Query.IdCarteira.Equal(idOrigem));
        tabelaProvisaoCollection.Query.Load();
        foreach (TabelaProvisao tabelaProvisao in tabelaProvisaoCollection)
        {
            tabelaProvisao.IdCarteira = idDestino;
        }
        tabelaProvisaoCollection.Save();

        CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
        calculoAdministracaoCollection.Query.Where(calculoAdministracaoCollection.Query.IdCarteira.Equal(idOrigem));
        calculoAdministracaoCollection.Query.Load();
        foreach (CalculoAdministracao calculoAdministracao in calculoAdministracaoCollection)
        {
            calculoAdministracao.IdCarteira = idDestino;
        }
        calculoAdministracaoCollection.Save();

        CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
        calculoAdministracaoHistoricoCollection.Query.Where(calculoAdministracaoHistoricoCollection.Query.IdCarteira.Equal(idOrigem));
        calculoAdministracaoHistoricoCollection.Query.Load();
        foreach (CalculoAdministracaoHistorico calculoAdministracao in calculoAdministracaoHistoricoCollection)
        {
            calculoAdministracao.IdCarteira = idDestino;
        }
        calculoAdministracaoHistoricoCollection.Save();

        CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
        calculoPerformanceCollection.Query.Where(calculoPerformanceCollection.Query.IdCarteira.Equal(idOrigem));
        calculoPerformanceCollection.Query.Load();
        foreach (CalculoPerformance calculoPerformance in calculoPerformanceCollection)
        {
            calculoPerformance.IdCarteira = idDestino;
        }
        calculoPerformanceCollection.Save();

        CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
        calculoPerformanceHistoricoCollection.Query.Where(calculoPerformanceHistoricoCollection.Query.IdCarteira.Equal(idOrigem));
        calculoPerformanceHistoricoCollection.Query.Load();
        foreach (CalculoPerformanceHistorico calculoPerformance in calculoPerformanceHistoricoCollection)
        {
            calculoPerformance.IdCarteira = idDestino;
        }
        calculoPerformanceHistoricoCollection.Save();

        CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
        calculoProvisaoCollection.Query.Where(calculoProvisaoCollection.Query.IdCarteira.Equal(idOrigem));
        calculoProvisaoCollection.Query.Load();
        foreach (CalculoProvisao calculoProvisao in calculoProvisaoCollection)
        {
            calculoProvisao.IdCarteira = idDestino;
        }
        calculoProvisaoCollection.Save();

        CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
        calculoProvisaoHistoricoCollection.Query.Where(calculoProvisaoHistoricoCollection.Query.IdCarteira.Equal(idOrigem));
        calculoProvisaoHistoricoCollection.Query.Load();
        foreach (CalculoProvisaoHistorico calculoProvisao in calculoProvisaoHistoricoCollection)
        {
            calculoProvisao.IdCarteira = idDestino;
        }
        calculoProvisaoHistoricoCollection.Save();
        #endregion

        #region EnquadraRegra, EnquadraResultado, EnquadraResultadoDetalhe
        EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
        enquadraRegraCollection.Query.Where(enquadraRegraCollection.Query.IdCarteira.Equal(idOrigem));
        enquadraRegraCollection.Query.Load();
        foreach (EnquadraRegra enquadraRegra in enquadraRegraCollection)
        {
            enquadraRegra.IdCarteira = idDestino;
        }
        enquadraRegraCollection.Save();

        EnquadraResultadoCollection enquadraResultadoCollection = new EnquadraResultadoCollection();
        enquadraResultadoCollection.Query.Where(enquadraResultadoCollection.Query.IdCarteira.Equal(idOrigem));
        enquadraResultadoCollection.Query.Load();
        foreach (EnquadraResultado enquadraResultado in enquadraResultadoCollection)
        {
            enquadraResultado.IdCarteira = idDestino;
        }
        enquadraResultadoCollection.Save();

        EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();
        enquadraResultadoDetalheCollection.Query.Where(enquadraResultadoDetalheCollection.Query.IdCarteira.Equal(idOrigem));
        enquadraResultadoDetalheCollection.Query.Load();
        foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
        {
            enquadraResultadoDetalhe.IdCarteira = idDestino;
        }
        enquadraResultadoDetalheCollection.Save();
        #endregion

        #region TabelaRebate >> Carteira Corretagem, Distribuidor, Officer -- CalculoRebate >> Carteira, Cotista, Officer

        #endregion


        #region Deleção de Carteira, Cliente, Pessoa Origem
        Carteira carteiraOrigem = new Carteira();
        if (carteiraOrigem.LoadByPrimaryKey(idOrigem))
        {
            carteiraOrigem.MarkAsDeleted();
            carteiraOrigem.Save();
        }
        if (clienteOrigem != null)
        {
            clienteOrigem.MarkAsDeleted();
            clienteOrigem.Save();
        }
        if (pessoaOrigem != null)
        {
            pessoaOrigem.MarkAsDeleted();
            pessoaOrigem.Save();
        }
        #endregion

        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// Chamada a store Procedure para copiar dados de um Cliente para outro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCriaCopia_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textIdClienteOrigem, this.textIdClienteDestino });

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        bool ckBolsa = this.chkBolsa.Value != null ? (bool)this.chkBolsa.Value : false;
        bool ckBMF = this.chkBMF.Value != null ? (bool)this.chkBMF.Value : false;
        bool ckRendaFixa = this.chkRendaFixa.Value != null ? (bool)this.chkRendaFixa.Value : false;
        bool ckLiquidacao = this.chkLiquidacao.Value != null ? (bool)this.chkLiquidacao.Value : false;
        bool ckFundo = this.chkFundo.Value != null ? (bool)this.chkFundo.Value : false;
        bool ckHistoricoCota = this.chkHistoricoCota.Value != null ? (bool)this.chkHistoricoCota.Value : false;

        bool[] paramMercados = { ckBolsa, ckBMF, ckRendaFixa, ckLiquidacao, ckFundo, ckHistoricoCota };

        // se todos forem false ao mesmo tempo
        bool todos = !ckBolsa && !ckBMF && !ckRendaFixa && !ckLiquidacao && !ckFundo && !ckHistoricoCota;

        if (todos)
        {
            e.Result = "Escolher algum mercado!";
            return;
        }

        int idOrigem = Convert.ToInt32(this.textIdClienteOrigem.Text.Trim());
        int idDestino = Convert.ToInt32(this.textIdClienteDestino.Text.Trim());

        #region Conferencia Cliente/Pessoa Existente
        if (!new Cliente().LoadByPrimaryKey(idOrigem))
        {
            e.Result = "Cliente com Id Origem não existe!";
            return;
        }

        if (!new Pessoa().LoadByPrimaryKey(idOrigem))
        {
            e.Result = "Pessoa com Id Origem não existe!";
            return;
        }

        if (idOrigem == idDestino)
        {
            e.Result = "Cliente Origem não pode ser igual ao Cliente Destino!";
            return;
        }

        #endregion

        string key = esConfigSettings.DefaultConnection.ConnectionString;
        string[] aux = key.Split(';');
        string[] aux1 = Regex.Split(aux[1], "Initial Catalog=");
        //
        string catalog = aux1[1].Trim();
        //
        string bancoOrigem = catalog;
        string bancoDestino = catalog;
        //
        int idClienteOrigem = idOrigem;
        int idClienteDestino = idDestino;
        //

        try
        {
            this.CallStoreProcedure(bancoOrigem, bancoDestino, idClienteOrigem, idClienteDestino, paramMercados);

            bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idClienteDestino))
            {
                #region Se tem permisão automática para usuários internos, salva a permissão
                if (permissaoInternoAuto && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
                {
                    GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                    usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                    usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Load(usuarioQuery);

                    PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                    foreach (Usuario usuarioPermissao in usuarioCollection)
                    {
                        int idUsuario = usuarioPermissao.IdUsuario.Value;

                        PermissaoCliente permissaoClienteExiste = new PermissaoCliente();

                        if (!permissaoClienteExiste.LoadByPrimaryKey(idUsuario, idClienteDestino))
                        {
                            PermissaoCliente permissaoCliente = new PermissaoCliente();
                            permissaoCliente.IdCliente = idClienteDestino;
                            permissaoCliente.IdUsuario = idUsuario;
                            permissaoClienteCollection.AttachEntity(permissaoCliente);
                        }
                    }
                    permissaoClienteCollection.Save();
                }
                #endregion
            }
        }
        catch (Exception e1)
        {
            e.Result = "Erro. Processo não executado: " + e1.Message;
            return;
        }

        e.Result = "Processo executado com sucesso.";
    }
    #endregion

    /* Constante que Indica o Nome da StoreProcedureName */
    private const string StoreProcedureNome = "CopyTables";

    /// <summary>
    /// Faz chamada a StoreProcedure CopyTables 
    /// </summary>
    /// <param name="bancoOrigem">Nome do Banco de Dados Origem</param>
    /// <param name="bancoDestino">Nome do Banco de Dados Destino</param>
    /// <param name="idClienteOrigem">idCliente Origem</param>
    /// <param name="idClienteDestino">idCliente Destino</param>
    /// <param name="paramMercados">Vetor com os Parâmetros do Mercado
    /// [0] = Bolsa
    /// [1] = BMF
    /// [2] = RendaFixa
    /// [3] = Liquidacao
    /// [4] = Fundo
    /// [5] = HistoricoCota
    /// </param>
    /// <returns></returns>
    private void CallStoreProcedure(string bancoOrigem, string bancoDestino, int idClienteOrigem, int idClienteDestino, bool[] paramMercados)
    {
        esParameters parms = new esParameters();
        //
        parms.Add("Banco_Origem", bancoOrigem);
        parms.Add("Banco_Destino", bancoDestino);
        parms.Add("idClienteOrigem", idClienteOrigem);
        parms.Add("idClienteDestino", idClienteDestino);
        //
        parms.Add("mercadoBolsa", paramMercados[0], esParameterDirection.Input, System.Data.DbType.Boolean, 1);
        parms.Add("mercadoBMF", paramMercados[1], esParameterDirection.Input, System.Data.DbType.Boolean, 1);
        parms.Add("mercadoRendaFixa", paramMercados[2], esParameterDirection.Input, System.Data.DbType.Boolean, 1);
        parms.Add("mercadoLiquidacao", paramMercados[3], esParameterDirection.Input, System.Data.DbType.Boolean, 1);
        parms.Add("mercadoFundo", paramMercados[4], esParameterDirection.Input, System.Data.DbType.Boolean, 1);
        parms.Add("mercadoHistoricoCota", paramMercados[5], esParameterDirection.Input, System.Data.DbType.Boolean, 1);

        esUtility u = new esUtility();

        u.ExecuteNonQuery(esQueryType.StoredProcedure, StoreProcedureNome, parms);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textLogin_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as TextBox).Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void labelLogin_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    #region GridCadastro
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            int idCliente = (int)e.Keys[0];

            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxCallbackPanel cbPanel = pageControl.FindControl("cbPanel") as ASPxCallbackPanel;
            //

            #region tabPage - Informações Básicas
            ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
            ASPxComboBox dropTipoControle = pageControl.FindControl("dropTipoControle") as ASPxComboBox;
            ASPxComboBox dropGrupoProcessamento = pageControl.FindControl("dropGrupoProcessamento") as ASPxComboBox;
            ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
            ASPxComboBox dropLocal = pageControl.FindControl("dropLocal") as ASPxComboBox;
            ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
            ASPxComboBox dropIsentoIR = pageControl.FindControl("dropIsentoIR") as ASPxComboBox;
            ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
            ASPxComboBox dropApuraGanhoRV = pageControl.FindControl("dropApuraGanhoRV") as ASPxComboBox;
            ASPxDateEdit textDataImplantacao = pageControl.FindControl("textDataImplantacao") as ASPxDateEdit;
            ASPxComboBox dropRegimeEspecialTributacao = pageControl.FindControl("dropRegimeEspecialTributacao") as ASPxComboBox;
            ASPxComboBox dropLocalNegociacao = pageControl.FindControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxTextBox textNomeCliente = pageControl.FindControl("textNomeCliente") as ASPxTextBox;
            ASPxTextBox textApelidoCliente = pageControl.FindControl("textApelidoCliente") as ASPxTextBox;
            ASPxComboBox dropAmortizacaoRendimentoJuros = pageControl.FindControl("dropAmortizacaoRendimentoJuros") as ASPxComboBox;

            Cliente cliente = new Cliente();
            bool clienteExiste = cliente.LoadByPrimaryKey(idCliente);
            if (clienteExiste)
            {
                cliente.TipoControle = Convert.ToByte(dropTipoControle.SelectedItem.Value);
                cliente.IdGrupoProcessamento = Convert.ToInt16(dropGrupoProcessamento.SelectedItem.Value);
                cliente.IsentoIR = (string)dropIsentoIR.SelectedItem.Value;
                cliente.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
                cliente.IdTipo = Convert.ToInt16(dropTipo.SelectedItem.Value);
                cliente.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);
                cliente.IdLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
                cliente.IsentoIOF = (string)dropIsentoIOF.SelectedItem.Value;
                cliente.ApuraGanhoRV = (string)dropApuraGanhoRV.SelectedItem.Value;
                cliente.DataInicio = Convert.ToDateTime(textDataImplantacao.Text);
                cliente.Nome = textNomeCliente.Text;

                if (!textApelidoCliente.Text.Equals(cliente.Apelido))
                {
                    cliente.Apelido = textApelidoCliente.Text;

                    Carteira carteiraAux = new Carteira();
                    if (carteiraAux.LoadByPrimaryKey(cliente.IdCliente.Value))
                    {
                        carteiraAux.Apelido = cliente.Apelido;
                        carteiraAux.Save();

                        gridCadastro.JSProperties["cpMessage"] = "O Apelido da Carteira também foi alterado";
                    }
                }

                cliente.AmortizacaoRendimentoJuros = (string)dropAmortizacaoRendimentoJuros.SelectedItem.Value;

                if (Convert.ToInt32(dropTipo.SelectedItem.Value) == (int)TipoClienteFixo.Fundo)
                {
                    cliente.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
                }
                else
                {
                    cliente.IdLocalNegociacao = null;
                }

                DateTime dataImplantacaoNew = Convert.ToDateTime(textDataImplantacao.Text);
                if (dataImplantacaoNew > cliente.DataDia.Value)
                {
                    cliente.DataDia = Convert.ToDateTime(textDataImplantacao.Text);
                }

                cliente.DataImplantacao = dataImplantacaoNew;
                cliente.RegimeEspecialTributacao = dropTipo.SelectedItem.Value.Equals("800") ? (dropRegimeEspecialTributacao.SelectedItem != null ? dropRegimeEspecialTributacao.SelectedItem.Value.ToString() : string.Empty) : string.Empty;
                if (string.IsNullOrEmpty(cliente.RegimeEspecialTributacao))
                    cliente.RegimeEspecialTributacao = null;
            }
            #endregion

            #region tabPage - ClienteBolsa
            ASPxComboBox dropTipoCotacao = pageControl.FindControl("dropTipoCotacao") as ASPxComboBox;
            ASPxComboBox dropIsentoIRBolsa = pageControl.FindControl("dropIsentoIRBolsa") as ASPxComboBox;
            ASPxComboBox dropAssessorBolsa = pageControl.FindControl("dropAssessorBolsa") as ASPxComboBox;
            ASPxComboBox dropCustodianteAcoes = pageControl.FindControl("dropCustodianteAcoes") as ASPxComboBox;
            ASPxComboBox dropCustodianteOpcoes = pageControl.FindControl("dropCustodianteOpcoes") as ASPxComboBox;
            ASPxTextBox textCodigoSinacorBolsa = pageControl.FindControl("textCodigoSinacorBolsa") as ASPxTextBox;
            TextBox textCodigoSinacorBolsa2 = pageControl.FindControl("textCodigoSinacorBolsa2") as TextBox;

            ClienteBolsa clienteBolsa = new ClienteBolsa();
            if (clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                if (dropTipoCotacao.SelectedIndex > -1)
                {
                    clienteBolsa.TipoCotacao = Convert.ToByte(dropTipoCotacao.SelectedItem.Value);
                }

                clienteBolsa.IsentoIR = "N";
                if (dropIsentoIRBolsa.SelectedIndex > -1)
                {
                    clienteBolsa.IsentoIR = Convert.ToString(dropIsentoIRBolsa.SelectedItem.Value);
                }

                if (dropAssessorBolsa.SelectedIndex > -1)
                {
                    clienteBolsa.IdAssessor = Convert.ToInt32(dropAssessorBolsa.SelectedItem.Value);
                }
                //
                if (dropCustodianteAcoes.SelectedIndex > 0)
                {
                    clienteBolsa.IdCustodianteAcoes = Convert.ToInt32(dropCustodianteAcoes.SelectedItem.Value);
                }
                else
                {
                    clienteBolsa.IdCustodianteAcoes = null;
                }
                //
                if (dropCustodianteOpcoes.SelectedIndex > 0)
                {
                    clienteBolsa.IdCustodianteOpcoes = Convert.ToInt32(dropCustodianteOpcoes.SelectedItem.Value);
                }
                else
                {
                    clienteBolsa.IdCustodianteOpcoes = null;
                }

                clienteBolsa.CodigoSinacor = textCodigoSinacorBolsa.Text.Trim();
                clienteBolsa.CodigoSinacor2 = textCodigoSinacorBolsa2.Text.Trim();

            }
            else
            {
                ClienteBolsa clienteBolsaInsert = new ClienteBolsa();
                clienteBolsaInsert.IdCliente = idCliente;

                clienteBolsaInsert.TipoCotacao = dropTipoCotacao.SelectedIndex != -1
                                           ? Convert.ToByte(dropTipoCotacao.SelectedItem.Value)
                                           : (byte)TipoCotacaoBolsa.Fechamento;

                clienteBolsaInsert.IsentoIR = dropIsentoIRBolsa.SelectedIndex != -1
                   ? Convert.ToString(dropIsentoIRBolsa.SelectedItem.Value) : "S";

                if (dropAssessorBolsa.SelectedIndex != -1)
                {
                    clienteBolsaInsert.IdAssessor = Convert.ToInt32(dropAssessorBolsa.SelectedItem.Value);
                }

                clienteBolsaInsert.CodigoSinacor = textCodigoSinacorBolsa.Text;
                clienteBolsaInsert.CodigoSinacor2 = textCodigoSinacorBolsa2.Text;
                clienteBolsaInsert.TipoCusto = 1;  //TODO ENGESSADO, MUDAR DEPOIS!!
                //
                if (dropCustodianteAcoes.SelectedIndex > -1)
                {
                    clienteBolsaInsert.IdCustodianteAcoes = Convert.ToInt32(dropCustodianteAcoes.SelectedItem.Value);
                }
                else
                {
                    clienteBolsaInsert.IdCustodianteAcoes = null;
                }
                //
                if (dropCustodianteOpcoes.SelectedIndex > -1)
                {
                    clienteBolsaInsert.IdCustodianteOpcoes = Convert.ToInt32(dropCustodianteOpcoes.SelectedItem.Value);
                }
                else
                {
                    clienteBolsaInsert.IdCustodianteOpcoes = null;
                }

                //
                clienteBolsaInsert.Save();
            }
            #endregion

            #region tabPage - ClienteBMF
            ASPxComboBox dropTipoPlataforma = pageControl.FindControl("dropTipoPlataforma") as ASPxComboBox;
            ASPxComboBox dropInvestidorInstitucional = pageControl.FindControl("dropInvestidorInstitucional") as ASPxComboBox;
            ASPxComboBox dropTipoCotacaoBMF = pageControl.FindControl("dropTipoCotacaoBMF") as ASPxComboBox;
            ASPxComboBox dropAssessorBMF = pageControl.FindControl("dropAssessorBMF") as ASPxComboBox;
            ASPxComboBox dropCustodianteBmf = pageControl.FindControl("dropCustodianteBmf") as ASPxComboBox;
            //        
            TextBox textCodigoSinacorBMF = pageControl.FindControl("textCodigoSinacorBMF") as TextBox;

            ClienteBMF clienteBMF = new ClienteBMF();
            if (clienteBMF.LoadByPrimaryKey(idCliente))
            {
                if (dropTipoPlataforma.SelectedIndex > -1)
                {
                    clienteBMF.TipoPlataforma = Convert.ToByte(dropTipoPlataforma.SelectedItem.Value);
                }
                if (dropInvestidorInstitucional.SelectedIndex > -1)
                {
                    clienteBMF.InvestidorInstitucional = Convert.ToString(dropInvestidorInstitucional.SelectedItem.Value);
                }
                if (dropTipoCotacaoBMF.SelectedIndex > -1)
                {
                    clienteBMF.TipoCotacao = Convert.ToByte(dropTipoCotacaoBMF.SelectedItem.Value);
                }
                if (dropAssessorBMF.SelectedIndex > -1)
                {
                    clienteBMF.IdAssessor = Convert.ToInt32(dropAssessorBMF.SelectedItem.Value);
                }

                if (dropCustodianteBmf.SelectedIndex > 0)
                {
                    clienteBMF.IdCustodianteBmf = Convert.ToInt32(dropCustodianteBmf.SelectedItem.Value);
                }
                else
                {
                    clienteBMF.IdCustodianteBmf = null;
                }
                clienteBMF.CodigoSinacor = textCodigoSinacorBMF.Text;
            }
            else
            {
                ClienteBMF clienteBMFInsert = new ClienteBMF();
                clienteBMFInsert.IdCliente = idCliente;

                clienteBMFInsert.TipoPlataforma = dropTipoPlataforma.SelectedIndex != -1
                                   ? Convert.ToByte(dropTipoPlataforma.SelectedItem.Value)
                                   : (byte)TipoPlataformaOperacional.Normal;

                clienteBMFInsert.Socio = "N";

                clienteBMFInsert.InvestidorInstitucional = dropInvestidorInstitucional.SelectedIndex != -1
                           ? Convert.ToString(dropInvestidorInstitucional.SelectedItem.Value)
                           : "N";

                clienteBMFInsert.TipoCotacao = dropTipoCotacaoBMF.SelectedIndex != -1
                                   ? Convert.ToByte(dropTipoCotacaoBMF.SelectedItem.Value)
                                   : (byte)TipoCotacaoBMF.Fechamento;

                if (dropAssessorBMF.SelectedIndex != -1)
                {
                    clienteBMFInsert.IdAssessor = Convert.ToInt32(dropAssessorBMF.SelectedItem.Value);
                }

                if (dropCustodianteBmf.SelectedIndex > 0)
                {
                    clienteBMFInsert.IdCustodianteBmf = Convert.ToInt32(dropCustodianteBmf.SelectedItem.Value);
                }
                else
                {
                    clienteBMFInsert.IdCustodianteBmf = null;
                }

                clienteBMFInsert.CodigoSinacor = textCodigoSinacorBMF.Text.Trim();
                clienteBMFInsert.Save();
            }
            #endregion

            #region tabPage - ClienteRendaFixa
            ASPxComboBox dropUsaCustoMedio = pageControl.FindControl("dropUsaCustoMedio") as ASPxComboBox;
            ASPxComboBox dropIsentoIR_RF = pageControl.FindControl("dropIsentoIR_RF") as ASPxComboBox;
            ASPxComboBox dropIsentoIOF_RF = pageControl.FindControl("dropIsentoIOF_RF") as ASPxComboBox;
            ASPxComboBox dropAssessorRF = pageControl.FindControl("dropAssessorRF") as ASPxComboBox;
            ASPxComboBox dropAtivoCetip = pageControl.FindControl("dropAtivoCetip") as ASPxComboBox;
            ASPxTextBox textCodigoRendaFixa = pageControl.FindControl("textCodigoRendaFixa") as ASPxTextBox;
            ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as ASPxTextBox;
            //        
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (clienteRendaFixa.LoadByPrimaryKey(idCliente))
            {
                if (dropUsaCustoMedio.SelectedIndex > -1)
                {
                    clienteRendaFixa.UsaCustoMedio = Convert.ToString(dropUsaCustoMedio.SelectedItem.Value);
                }
                if (dropIsentoIOF_RF.SelectedIndex > -1)
                {
                    clienteRendaFixa.IsentoIOF = Convert.ToString(dropIsentoIOF_RF.SelectedItem.Value);
                }
                if (dropIsentoIR_RF.SelectedIndex > -1)
                {
                    clienteRendaFixa.IsentoIR = Convert.ToString(dropIsentoIR_RF.SelectedItem.Value);
                }
                if (dropAssessorRF.SelectedIndex > -1)
                {
                    clienteRendaFixa.IdAssessor = Convert.ToInt32(dropAssessorRF.SelectedItem.Value);
                }
                clienteRendaFixa.AtivoCetip = dropAtivoCetip.SelectedItem.Value.ToString();
                clienteRendaFixa.CodigoInterface = textCodigoRendaFixa.Text;
                clienteRendaFixa.CodigoCetip = textCodigoCetip.Text;
            }

            else 
            {
                ClienteRendaFixa clienteRendaFixaInsert = new ClienteRendaFixa();
                clienteRendaFixaInsert.IdCliente = idCliente;

                clienteRendaFixaInsert.UsaCustoMedio = dropUsaCustoMedio.SelectedIndex != -1
                   ? Convert.ToString(dropUsaCustoMedio.SelectedItem.Value) : "N";

                clienteRendaFixaInsert.IsentoIOF = dropIsentoIOF_RF.SelectedIndex != -1
                   ? Convert.ToString(dropIsentoIOF_RF.SelectedItem.Value) : "S";

                clienteRendaFixaInsert.IsentoIR = dropIsentoIR_RF.SelectedIndex != -1
                   ? Convert.ToString(dropIsentoIR_RF.SelectedItem.Value) : "S";

                clienteRendaFixaInsert.CodigoInterface = textCodigoRendaFixa.Text;
                clienteRendaFixaInsert.CodigoCetip = textCodigoCetip.Text;

                if (dropAssessorRF.SelectedIndex != -1)
                {
                    clienteRendaFixaInsert.IdAssessor = Convert.ToInt32(dropAssessorRF.SelectedItem.Value);
                }
                clienteRendaFixaInsert.AtivoCetip = dropAtivoCetip.SelectedItem.Value.ToString();
                clienteRendaFixaInsert.Save();
            }
            #endregion

            #region tabPage - ClienteInterface
            TextBox textCodigoCVM = pageControl.FindControl("textCodigoCVM") as TextBox;
            TextBox textCodigoYMF = pageControl.FindControl("textCodigoYMF") as TextBox;
            TextBox textRegistroBovespa = pageControl.FindControl("textRegistroBovespa") as TextBox;
            TextBox textCodigoGazeta = pageControl.FindControl("textCodigoGazeta") as TextBox;
            TextBox textCodigoSwift = pageControl.FindControl("textCodigoSwift") as TextBox;
            TextBox textCodigosCC = pageControl.FindControl("textCodigosCC") as TextBox;
            TextBox textCodigoIsin = pageControl.FindControl("textCodigoIsin") as TextBox;
            TextBox textCodigoCusip = pageControl.FindControl("textCodigoCusip") as TextBox;
            //        
            ClienteInterface clienteInterface = new ClienteInterface();
            if (clienteInterface.LoadByPrimaryKey(idCliente))
            {
                clienteInterface.CodigoCVM = textCodigoCVM.Text;
                clienteInterface.CodigoYMF = textCodigoYMF.Text;
                clienteInterface.CodigoGazeta = textCodigoGazeta.Text;
                clienteInterface.CodigoSwift = textCodigoSwift.Text;
                clienteInterface.RegistroBovespa = textRegistroBovespa.Text;
                clienteInterface.CodigoIsin = textCodigoIsin.Text;
                clienteInterface.CodigoCusip = textCodigoCusip.Text;
                clienteInterface.InterfaceSinacorCC = textCodigosCC.Text;
            }
            else
            {
                ClienteInterface clienteInterfaceInsert = new ClienteInterface();
                clienteInterfaceInsert.IdCliente = idCliente;
                clienteInterfaceInsert.CodigoCVM = textCodigoCVM.Text;
                clienteInterfaceInsert.CodigoYMF = textCodigoYMF.Text;
                clienteInterfaceInsert.CodigoGazeta = textCodigoGazeta.Text;
                clienteInterfaceInsert.CodigoSwift = textCodigoSwift.Text;
                clienteInterfaceInsert.RegistroBovespa = textRegistroBovespa.Text;
                clienteInterfaceInsert.CodigoIsin = textCodigoIsin.Text;
                clienteInterfaceInsert.CodigoCusip = textCodigoCusip.Text;
                clienteInterfaceInsert.InterfaceSinacorCC = textCodigosCC.Text;

                clienteInterfaceInsert.Save();
            }
            #endregion

            #region tabPage - Cliente Outros
            ASPxComboBox dropCalculaGerencial = pageControl.FindControl("dropCalculaGerencial") as ASPxComboBox;
            ASPxComboBox dropZeraCaixa = pageControl.FindControl("dropZeraCaixa") as ASPxComboBox;
            ASPxComboBox dropDescontoTributo = pageControl.FindControl("dropDescontoTributo") as ASPxComboBox;
            ASPxComboBox dropGrossUP = pageControl.FindControl("dropGrossUP") as ASPxComboBox;
            ASPxComboBox dropMultiMoeda = pageControl.FindControl("dropMultiMoeda") as ASPxComboBox;
            ASPxComboBox dropIndiceAbertura = pageControl.FindControl("dropIndiceAbertura") as ASPxComboBox;
            ASPxComboBox dropAberturaIndexada = pageControl.FindControl("dropAberturaIndexada") as ASPxComboBox;
            //        
            if (clienteExiste)
            {
                cliente.CalculaGerencial = (string)dropCalculaGerencial.SelectedItem.Value;
                cliente.ZeraCaixa = (string)dropZeraCaixa.SelectedItem.Value;
                cliente.DescontaTributoPL = Convert.ToByte(dropDescontoTributo.SelectedItem.Value);
                cliente.GrossUP = Convert.ToByte(dropGrossUP.SelectedItem.Value);
                cliente.MultiMoeda = Convert.ToString(dropMultiMoeda.SelectedItem.Value);

                ASPxComboBox dropGrupoAfinidade = pageControl.FindControl("dropGrupoAfinidade") as ASPxComboBox;
                if (dropGrupoAfinidade.SelectedIndex > -1)
                {
                    cliente.IdGrupoAfinidade = Convert.ToInt32(dropGrupoAfinidade.SelectedItem.Value);
                }

                if (Convert.ToInt32(dropAberturaIndexada.SelectedItem.Value) != (int)TipoAberturaIndexada.NaoExecuta) 
                {
                    if (dropIndiceAbertura.SelectedIndex != -1)
                    {
                        cliente.IdIndiceAbertura = Convert.ToInt16(dropIndiceAbertura.SelectedItem.Value);
                    }
                    else
                    {
                        cliente.IdIndiceAbertura = ListaIndiceFixo.CDI;
                    }

                    cliente.AberturaIndexada = Convert.ToInt16(dropAberturaIndexada.SelectedItem.Value);
                }
                else
                {
                    cliente.IdIndiceAbertura = null;
                    cliente.AberturaIndexada = Convert.ToInt16(TipoAberturaIndexada.NaoExecuta);
                }

            }
            #endregion

            #region Saves
            using (esTransactionScope scope = new esTransactionScope())
            {

                if (cliente.IdCliente.HasValue)
                {

                    
                    cliente.DataAtualizacao = DateTime.Now;
                    cliente.Save();



                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();

                    string descricao = "Cadastro de Cliente - Operacao: Update Cliente: " + cliente.IdCliente + UtilitarioWeb.ToString(cliente);
                    if (descricao.Length > 2000)
                    {
                        descricao = descricao.Substring(0, 2000);
                    }

                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    descricao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }

                if (clienteBolsa.IdCliente.HasValue)
                {
                    clienteBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ClienteBolsa - Operacao: Update ClienteBolsa: " + clienteBolsa.IdCliente + UtilitarioWeb.ToString(clienteBolsa),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }



                if (clienteBMF.IdCliente.HasValue)
                {
                    clienteBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ClienteBMF - Operacao: Update ClienteBMF: " + clienteBMF.IdCliente + UtilitarioWeb.ToString(clienteBMF),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }



                if (clienteRendaFixa.IdCliente.HasValue)
                {
                    clienteRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ClienteRendaFixa - Operacao: Update ClienteRendaFixa: " + clienteRendaFixa.IdCliente + UtilitarioWeb.ToString(clienteRendaFixa),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }


                if (clienteInterface.IdCliente.HasValue)
                {
                    clienteInterface.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ClienteInterface - Operacao: Update ClienteInterface: " + clienteInterface.IdCliente + UtilitarioWeb.ToString(clienteInterface),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }

                //
                scope.Complete();
            }

            #endregion

            //Atualiza a carteira correspondente (statusAtivo)
            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(idCliente))
            {
                carteira.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
                carteira.Save();
            }

            //Testa se o tipoControle é somente cotação, se for, força a deleção de todos os permissionamentos
            //associados a este cliente (pois é desnecessário e pode gerar duplicação em algumas consultas)
            if (cliente.TipoControle.Value == (byte)TipoControleCliente.ApenasCotacao)
            {
                PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                permissaoClienteCollection.Query.Where(permissaoClienteCollection.Query.IdCliente.Equal(idCliente));
                permissaoClienteCollection.Query.Load();
                permissaoClienteCollection.MarkAllAsDeleted();
                permissaoClienteCollection.Save();
            }

            //
            e.Cancel = true;
            gridCadastro.CancelEdit();

            HistoricoLog historicoDebug = new HistoricoLog();
            historicoDebug.InsereHistoricoLog(DateTime.Now, DateTime.Now, "Sem erros no save",
                "", "", "", HistoricoLogOrigem.Outros);
        }
        catch (Exception ex)
        {
            string erro = "Erro save cadastro: " + ex.Message + "-" + ex.Source + "-" + ex.StackTrace + "-" + ex.InnerException;
            HistoricoLog historicoDebug = new HistoricoLog();
            historicoDebug.InsereHistoricoLog(DateTime.Now, DateTime.Now, erro,
                "", "", "", HistoricoLogOrigem.Outros);
            throw new Exception(erro);

        }
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxCallbackPanel cbPanel = pageControl.FindControl("cbPanel") as ASPxCallbackPanel;
        //
        #region tabPage - Informações Básicas
        ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropTipoControle = pageControl.FindControl("dropTipoControle") as ASPxComboBox;
        ASPxComboBox dropGrupoProcessamento = pageControl.FindControl("dropGrupoProcessamento") as ASPxComboBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocal = pageControl.FindControl("dropLocal") as ASPxComboBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropIsentoIR = pageControl.FindControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropApuraGanhoRV = pageControl.FindControl("dropApuraGanhoRV") as ASPxComboBox;
        ASPxDateEdit textDataImplantacao = pageControl.FindControl("textDataImplantacao") as ASPxDateEdit;
        ASPxComboBox dropRegimeEspecialTributacao = pageControl.FindControl("dropRegimeEspecialTributacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = pageControl.FindControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxTextBox textNomeCliente = pageControl.FindControl("textNomeCliente") as ASPxTextBox;
        ASPxTextBox textApelidoCliente = pageControl.FindControl("textApelidoCliente") as ASPxTextBox;
        ASPxTextBox textIdCliente = pageControl.FindControl("textIdCliente") as ASPxTextBox;
        ASPxComboBox dropAmortizacaoRendimentoJuros = pageControl.FindControl("dropAmortizacaoRendimentoJuros") as ASPxComboBox;
        //
        Cliente cliente = new Cliente();
        //

        int IdCliente = Convert.ToInt32(textIdCliente.Text);
        cliente.IdCliente = IdCliente;
        cliente.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);                
        cliente.Nome = textNomeCliente.Text;
        cliente.Apelido = textApelidoCliente.Text;
        //

        cliente.Status = (byte)StatusCliente.Aberto;
        cliente.DataDia = Convert.ToDateTime(textDataImplantacao.Text);
        //       
        cliente.TipoControle = Convert.ToByte(dropTipoControle.SelectedItem.Value);
        cliente.IdGrupoProcessamento = Convert.ToInt16(dropGrupoProcessamento.SelectedItem.Value);
        cliente.IsentoIR = (string)dropIsentoIR.SelectedItem.Value;
        cliente.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
        cliente.IdTipo = Convert.ToInt16(dropTipo.SelectedItem.Value);
        cliente.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);
        cliente.IdLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
        cliente.IsentoIOF = (string)dropIsentoIOF.SelectedItem.Value;
        cliente.ApuraGanhoRV = (string)dropApuraGanhoRV.SelectedItem.Value;

        if (dropAmortizacaoRendimentoJuros.SelectedIndex != -1)
        {
            cliente.AmortizacaoRendimentoJuros = (string)dropAmortizacaoRendimentoJuros.SelectedItem.Value;
        }
        
        //
        cliente.DataInicio = Convert.ToDateTime(textDataImplantacao.Text);
        cliente.DataImplantacao = Convert.ToDateTime(textDataImplantacao.Text);
        cliente.IsProcessando = "N";
        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
        cliente.CalculaRealTime = "N";
        cliente.RegimeEspecialTributacao = dropTipo.SelectedItem.Value.Equals("800") ? (dropRegimeEspecialTributacao.SelectedItem != null ? dropRegimeEspecialTributacao.SelectedItem.Value.ToString() : string.Empty) : string.Empty;
        if (string.IsNullOrEmpty(cliente.RegimeEspecialTributacao))
            cliente.RegimeEspecialTributacao = null;
        //

        if (Convert.ToInt32(dropTipo.SelectedItem.Value) == (int)TipoClienteFixo.Fundo)
        {
            cliente.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
        }
        else
        {
            cliente.IdLocalNegociacao = null;
        }
        #endregion

        #region tabPage - ClienteBolsa
        ASPxComboBox dropTipoCotacao = pageControl.FindControl("dropTipoCotacao") as ASPxComboBox;
        ASPxComboBox dropIsentoIRBolsa = pageControl.FindControl("dropIsentoIRBolsa") as ASPxComboBox;
        ASPxComboBox dropAssessorBolsa = pageControl.FindControl("dropAssessorBolsa") as ASPxComboBox;
        ASPxComboBox dropCustodianteAcoes = pageControl.FindControl("dropCustodianteAcoes") as ASPxComboBox;
        ASPxComboBox dropCustodianteOpcoes = pageControl.FindControl("dropCustodianteOpcoes") as ASPxComboBox;
        ASPxTextBox textCodigoSinacorBolsa = pageControl.FindControl("textCodigoSinacorBolsa") as ASPxTextBox;
        TextBox textCodigoSinacorBolsa2 = pageControl.FindControl("textCodigoSinacorBolsa2") as TextBox;

        ClienteBolsa clienteBolsa = new ClienteBolsa();
        clienteBolsa.IdCliente = IdCliente;

        clienteBolsa.TipoCotacao = dropTipoCotacao.SelectedIndex != -1
                                   ? Convert.ToByte(dropTipoCotacao.SelectedItem.Value)
                                   : (byte)TipoCotacaoBolsa.Fechamento;

        clienteBolsa.IsentoIR = dropIsentoIRBolsa.SelectedIndex != -1
           ? Convert.ToString(dropIsentoIRBolsa.SelectedItem.Value) : "S";

        if (dropAssessorBolsa.SelectedIndex != -1)
        {
            clienteBolsa.IdAssessor = Convert.ToInt32(dropAssessorBolsa.SelectedItem.Value);
        }

        if (dropCustodianteAcoes.SelectedIndex > 0)
        {
            clienteBolsa.IdCustodianteAcoes = Convert.ToInt32(dropCustodianteAcoes.SelectedItem.Value);
        }

        if (dropCustodianteOpcoes.SelectedIndex > 0)
        {
            clienteBolsa.IdCustodianteOpcoes = Convert.ToInt32(dropCustodianteOpcoes.SelectedItem.Value);
        }

        clienteBolsa.CodigoSinacor = textCodigoSinacorBolsa.Text.Trim();
        clienteBolsa.CodigoSinacor2 = textCodigoSinacorBolsa2.Text.Trim();
        //
        // HACK ClienteBolsa.TipoCusto - Forçado para custo Medio
        clienteBolsa.TipoCusto = 1;

        #endregion

        #region tabPage - ClienteBMF
        ASPxComboBox dropTipoPlataforma = pageControl.FindControl("dropTipoPlataforma") as ASPxComboBox;
        ASPxComboBox dropInvestidorInstitucional = pageControl.FindControl("dropInvestidorInstitucional") as ASPxComboBox;
        ASPxComboBox dropTipoCotacaoBMF = pageControl.FindControl("dropTipoCotacaoBMF") as ASPxComboBox;
        ASPxComboBox dropAssessorBMF = pageControl.FindControl("dropAssessorBMF") as ASPxComboBox;
        ASPxComboBox dropCustodianteBmf = pageControl.FindControl("dropCustodianteBmf") as ASPxComboBox;
        //        
        TextBox textCodigoSinacorBMF = pageControl.FindControl("textCodigoSinacorBMF") as TextBox;

        ClienteBMF clienteBMF = new ClienteBMF();
        clienteBMF.IdCliente = IdCliente;

        clienteBMF.TipoPlataforma = dropTipoPlataforma.SelectedIndex != -1
                           ? Convert.ToByte(dropTipoPlataforma.SelectedItem.Value)
                           : (byte)TipoPlataformaOperacional.Normal;

        clienteBMF.Socio = "N";

        clienteBMF.InvestidorInstitucional = dropInvestidorInstitucional.SelectedIndex != -1
                   ? Convert.ToString(dropInvestidorInstitucional.SelectedItem.Value)
                   : "N";

        clienteBMF.TipoCotacao = dropTipoCotacaoBMF.SelectedIndex != -1
                           ? Convert.ToByte(dropTipoCotacaoBMF.SelectedItem.Value)
                           : (byte)TipoCotacaoBMF.Fechamento;

        if (dropAssessorBMF.SelectedIndex != -1)
        {
            clienteBMF.IdAssessor = Convert.ToInt32(dropAssessorBMF.SelectedItem.Value);
        }

        clienteBMF.CodigoSinacor = textCodigoSinacorBMF.Text;

        if (dropCustodianteBmf.SelectedIndex > 0)
        {
            clienteBMF.IdCustodianteBmf = Convert.ToInt32(dropCustodianteBmf.SelectedItem.Value);
        }
        else
        {
            clienteBMF.IdCustodianteBmf = null;
        }
        #endregion

        #region tabPage - ClienteRendaFixa
        ASPxComboBox dropUsaCustoMedio = pageControl.FindControl("dropUsaCustoMedio") as ASPxComboBox;
        ASPxComboBox dropIsentoIR_RF = pageControl.FindControl("dropIsentoIR_RF") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF_RF = pageControl.FindControl("dropIsentoIOF_RF") as ASPxComboBox;
        ASPxComboBox dropAssessorRF = pageControl.FindControl("dropAssessorRF") as ASPxComboBox;
        ASPxComboBox dropAtivoCetip = pageControl.FindControl("dropAtivoCetip") as ASPxComboBox;
        ASPxTextBox textCodigoRendaFixa = pageControl.FindControl("textCodigoRendaFixa") as ASPxTextBox;
        ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as ASPxTextBox;
        //        
        ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
        clienteRendaFixa.IdCliente = IdCliente;

        clienteRendaFixa.UsaCustoMedio = dropUsaCustoMedio.SelectedIndex != -1
           ? Convert.ToString(dropUsaCustoMedio.SelectedItem.Value) : "N";

        clienteRendaFixa.IsentoIOF = dropIsentoIOF_RF.SelectedIndex != -1
           ? Convert.ToString(dropIsentoIOF_RF.SelectedItem.Value) : "S";

        clienteRendaFixa.IsentoIR = dropIsentoIR_RF.SelectedIndex != -1
           ? Convert.ToString(dropIsentoIR_RF.SelectedItem.Value) : "S";
        clienteRendaFixa.CodigoInterface = textCodigoRendaFixa.Text;
        clienteRendaFixa.CodigoCetip = textCodigoCetip.Text;

        if (dropAssessorRF.SelectedIndex != -1)
        {
            clienteRendaFixa.IdAssessor = Convert.ToInt32(dropAssessorRF.SelectedItem.Value);
        }
        if (dropAtivoCetip.SelectedIndex != -1)
        {
            clienteRendaFixa.AtivoCetip = dropAtivoCetip.SelectedItem.Value.ToString();
        }        
        #endregion

        #region tabPage - ClienteInterface
        TextBox textCodigoCVM = pageControl.FindControl("textCodigoCVM") as TextBox;
        TextBox textCodigoYMF = pageControl.FindControl("textCodigoYMF") as TextBox;
        TextBox textRegistroBovespa = pageControl.FindControl("textRegistroBovespa") as TextBox;
        TextBox textCodigoGazeta = pageControl.FindControl("textCodigoGazeta") as TextBox;
        TextBox textCodigoSwift = pageControl.FindControl("textCodigoSwift") as TextBox;
        TextBox textCodigosCC = pageControl.FindControl("textCodigosCC") as TextBox;

        TextBox textCodigoIsin = pageControl.FindControl("textCodigoIsin") as TextBox;
        TextBox textCodigoCusip = pageControl.FindControl("textCodigoCusip") as TextBox;
        //        
        ClienteInterface clienteInterface = new ClienteInterface();
        clienteInterface.IdCliente = IdCliente;
        clienteInterface.CodigoCVM = textCodigoCVM.Text;
        clienteInterface.CodigoYMF = textCodigoYMF.Text;
        clienteInterface.CodigoGazeta = textCodigoGazeta.Text;
        clienteInterface.RegistroBovespa = textRegistroBovespa.Text;
        clienteInterface.CodigoIsin = textCodigoIsin.Text;
        clienteInterface.CodigoCusip = textCodigoCusip.Text;
        clienteInterface.InterfaceSinacorCC = textCodigosCC.Text;

        #endregion

        #region tabPage - Cliente - Outros
        ASPxComboBox dropCalculaGerencial = pageControl.FindControl("dropCalculaGerencial") as ASPxComboBox;
        cliente.CalculaGerencial = Convert.ToString(dropCalculaGerencial.SelectedItem.Value);

        ASPxComboBox dropZeraCaixa = pageControl.FindControl("dropZeraCaixa") as ASPxComboBox;
        ASPxComboBox dropDescontoTributo = pageControl.FindControl("dropDescontoTributo") as ASPxComboBox;
        ASPxComboBox dropGrossUP = pageControl.FindControl("dropGrossUP") as ASPxComboBox;
        ASPxComboBox dropMultiMoeda = pageControl.FindControl("dropMultiMoeda") as ASPxComboBox;
        ASPxComboBox dropIndiceAbertura = pageControl.FindControl("dropIndiceAbertura") as ASPxComboBox;
        ASPxComboBox dropAberturaIndexada = pageControl.FindControl("dropAberturaIndexada") as ASPxComboBox;
        //
        cliente.ZeraCaixa = (string)dropZeraCaixa.SelectedItem.Value;
        cliente.DescontaTributoPL = Convert.ToByte(dropDescontoTributo.SelectedItem.Value);
        cliente.GrossUP = Convert.ToByte(dropGrossUP.SelectedItem.Value);
        cliente.MultiMoeda = Convert.ToString(dropMultiMoeda.SelectedItem.Value);

        ASPxComboBox dropGrupoAfinidade = pageControl.FindControl("dropGrupoAfinidade") as ASPxComboBox;
        if (dropGrupoAfinidade.SelectedIndex > -1)
        {
            cliente.IdGrupoAfinidade = Convert.ToInt32(dropGrupoAfinidade.SelectedItem.Value);
        }

        if (Convert.ToInt32(dropAberturaIndexada.SelectedItem.Value) != (int)TipoAberturaIndexada.NaoExecuta)
        {
            if (dropIndiceAbertura.SelectedIndex != -1)
            {
                cliente.IdIndiceAbertura = Convert.ToInt16(dropIndiceAbertura.SelectedItem.Value);
            }
            else
            {
                cliente.IdIndiceAbertura = ListaIndiceFixo.CDI;
            }

            cliente.AberturaIndexada = Convert.ToInt16(dropAberturaIndexada.SelectedItem.Value);
        }
        else
        {
            cliente.IdIndiceAbertura = null;
            cliente.AberturaIndexada = Convert.ToInt16(TipoAberturaIndexada.NaoExecuta);
        }

        #endregion

        #region Saves
        using (esTransactionScope scope = new esTransactionScope())
        {

            if (cliente.IdCliente.HasValue)
            {
                cliente.DataAtualizacao = DateTime.Now;

                cliente.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Cliente - Operacao: Insert Cliente: " + cliente.IdCliente + UtilitarioWeb.ToString(cliente),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            if (clienteBolsa.IdCliente.HasValue)
            {
                clienteBolsa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ClienteBolsa - Operacao: Insert ClienteBolsa: " + clienteBolsa.IdCliente + UtilitarioWeb.ToString(clienteBolsa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            if (clienteBMF.IdCliente.HasValue)
            {
                clienteBMF.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ClienteBMF - Operacao: Insert ClienteBMF: " + clienteBMF.IdCliente + UtilitarioWeb.ToString(clienteBMF),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            if (clienteRendaFixa.IdCliente.HasValue)
            {
                clienteRendaFixa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ClienteRendaFixa - Operacao: Insert ClienteRendaFixa: " + clienteRendaFixa.IdCliente + UtilitarioWeb.ToString(clienteRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            if (clienteInterface.IdCliente.HasValue)
            {
                clienteInterface.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de ClienteInterface - Operacao: Insert ClienteInterface: " + clienteInterface.IdCliente + UtilitarioWeb.ToString(clienteInterface),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            scope.Complete();
        }
        #endregion

        #region Cria contacorrente default associada ao cliente
        ContaCorrente contaCorrente = new ContaCorrente();
        contaCorrente.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        contaCorrente.Numero = btnEditCodigoPessoa.Text.ToString();

        TipoConta tipoConta = new TipoConta();
        tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
        tipoConta.Query.Load();
        if (!tipoConta.IdTipoConta.HasValue)
        {
            tipoConta = new TipoConta();
            tipoConta.Descricao = "Conta Depósito";
            tipoConta.Save();
        }
        contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

        contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
        contaCorrente.ContaDefault = "S";

        contaCorrente.Save();
        #endregion

        bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

        #region Se tem permisão automática para usuários internos, cria permissão para todos os usuários (internos) do sistema
        if (permissaoInternoAuto && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
        {
            GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
            usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Load(usuarioQuery);

            PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
            foreach (Usuario usuarioPermissao in usuarioCollection)
            {
                int idUsuario = usuarioPermissao.IdUsuario.Value;

                PermissaoCliente permissaoCliente = new PermissaoCliente();
                permissaoCliente.IdCliente = IdCliente;
                permissaoCliente.IdUsuario = idUsuario;
                permissaoClienteCollection.AttachEntity(permissaoCliente);
            }
            permissaoClienteCollection.Save();
        }
        #endregion

        if (cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
        {
            #region Cadastra novo usuário associado ao Cliente, cria PermissaoCliente associada ao usuário cadastrado
            TextBox textApelido = pageControl.FindControl("textApelido") as TextBox;
            TextBox textLogin = pageControl.FindControl("textLogin") as TextBox;
            //
            string login = textLogin.Text;

            if (!String.IsNullOrEmpty(login) && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
            {
                //Busca grupo que deve ser associado ao novo login
                int? idGrupo = null;
                string descricaoGrupo = WebConfig.AppSettings.GrupoCliente;
                GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
                grupoUsuarioCollection.Query.Select(grupoUsuarioCollection.Query.IdGrupo);
                grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.Descricao.Equal(descricaoGrupo));
                if (grupoUsuarioCollection.Query.Load())
                {
                    idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;
                }

                if (idGrupo.HasValue)
                {
                    //Deleta o login atual            
                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Query.Where(usuarioCollection.Query.Login.Equal(login));
                    usuarioCollection.Query.Load();
                    usuarioCollection.MarkAllAsDeleted();
                    usuarioCollection.Save();

                    Usuario usuario = new Usuario();
                    usuario.Save(idGrupo.Value, textApelido.Text, login, login, "", "S", (byte)TipoTravaUsuario.TravaCliente, cliente.IdCliente.Value);

                }
            }

            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

            


            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCliente = Convert.ToInt32(keyValuesId[i]);

                OperacaoRendaFixaCollection operacaoRendafixaContraparteCollection = new OperacaoRendaFixaCollection();
                operacaoRendafixaContraparteCollection.Query.Where(operacaoRendafixaContraparteCollection.Query.IdCarteiraContraparte == idCliente);
                operacaoRendafixaContraparteCollection.Query.Load();
                if (operacaoRendafixaContraparteCollection.Count > 0)                
                    throw new Exception("O cliente "+ idCliente +" não pode ser apagado pois é contraparte de operações de outros clientes.");

                using (esTransactionScope scope = new esTransactionScope())
                {
                    // Deleta PosicaoRendaFixa
                    PosicaoRendaFixaCollection p = new PosicaoRendaFixaCollection();
                    p.Query.Select(p.Query.IdPosicao)
                           .Where(p.Query.IdCliente == idCliente);

                    if (p.Query.Load())
                    {
                        p.MarkAllAsDeleted();
                        p.Save();
                    }

                    // Deleta PosicaoSwap
                    PosicaoSwapCollection posicaoSwap = new PosicaoSwapCollection();
                    posicaoSwap.Query.Select(posicaoSwap.Query.IdPosicao)
                           .Where(posicaoSwap.Query.IdCliente == idCliente);

                    if (posicaoSwap.Query.Load())
                    {
                        posicaoSwap.MarkAllAsDeleted();
                        posicaoSwap.Save();
                    }

                    TabelaExtratoClienteCollection tabelaExtratoClienteCollection = new TabelaExtratoClienteCollection();
                    tabelaExtratoClienteCollection.Query.Where(tabelaExtratoClienteCollection.Query.IdCliente.Equal(idCliente));
                    tabelaExtratoClienteCollection.Query.Load();
                    tabelaExtratoClienteCollection.MarkAllAsDeleted();
                    tabelaExtratoClienteCollection.Save();

                    TabelaCustosRendaFixaCollection tabelaCustosCollection = new TabelaCustosRendaFixaCollection();
                    tabelaCustosCollection.Query.Where(tabelaCustosCollection.Query.IdCliente.Equal(idCliente));
                    tabelaCustosCollection.Query.Load();
                    tabelaCustosCollection.MarkAllAsDeleted();
                    tabelaCustosCollection.Save();

                    DetalheResgateFundoCollection detalheResgateFundoCollection = new DetalheResgateFundoCollection();
                    detalheResgateFundoCollection.Query.Where(detalheResgateFundoCollection.Query.IdCliente.Equal(idCliente));
                    detalheResgateFundoCollection.Query.Load();
                    detalheResgateFundoCollection.MarkAllAsDeleted();
                    detalheResgateFundoCollection.Save();

                    OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
                    operacaoEmprestimoBolsaCollection.Query.Select(operacaoEmprestimoBolsaCollection.Query.IdOperacao)
                                                     .Where(operacaoEmprestimoBolsaCollection.Query.IdCliente == idCliente);

                    if (operacaoEmprestimoBolsaCollection.Query.Load())
                    {
                        operacaoEmprestimoBolsaCollection.MarkAllAsDeleted();
                        operacaoEmprestimoBolsaCollection.Save();
                    }

                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao)
                                                   .Where(operacaoCotistaCollection.Query.IdCarteira == idCliente);

                    if (operacaoCotistaCollection.Query.Load())
                    {
                        operacaoCotistaCollection.MarkAllAsDeleted();
                        operacaoCotistaCollection.Save();
                    }

                    OperacaoCambioCollection operacaoCambioCollection = new OperacaoCambioCollection();
                    operacaoCambioCollection.Query.Select(operacaoCambioCollection.Query.IdOperacao)
                                                   .Where(operacaoCambioCollection.Query.IdCliente.Equal(idCliente));

                    if (operacaoCambioCollection.Query.Load())
                    {
                        operacaoCambioCollection.MarkAllAsDeleted();
                        operacaoCambioCollection.Save();
                    }

                    //ContaCorrente
                    ContaCorrenteCollection contaCorrenteColl = new ContaCorrenteCollection();
                    contaCorrenteColl.Query.Where(contaCorrenteColl.Query.IdCliente.Equal(idCliente));
                    if (contaCorrenteColl.Query.Load())
                    {
                        foreach (ContaCorrente conta in contaCorrenteColl)
                        {
                            conta.IdCliente = null;
                            conta.ContaDefaultCliente = "N";
                        }

                        contaCorrenteColl.Save();
                    }

                    //                    
                    Carteira carteira = new Carteira();

                    


                    if (carteira.LoadByPrimaryKey(idCliente))
                    {
                        carteira.MarkAsDeleted();
                        carteira.Save();
                    }



                    //

                    #region Deleta todas as permissoesCliente remanescentes vinculadas a este cliente
                    PermissaoClienteCollection permissaoClienteCollectionDeletar = new PermissaoClienteCollection();
                    permissaoClienteCollectionDeletar.Query.Where(permissaoClienteCollectionDeletar.Query.IdCliente.Equal(idCliente));
                    permissaoClienteCollectionDeletar.Query.Load();

                    permissaoClienteCollectionDeletar.MarkAllAsDeleted();
                    permissaoClienteCollectionDeletar.Save();
                    #endregion

                    #region Deleta tudo de Cotista relacionado, mas somente para cotista com o mesmo código do cliente
                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollectionDeletar = new PosicaoCotistaAberturaCollection();
                    posicaoCotistaAberturaCollectionDeletar.Query.Where(posicaoCotistaAberturaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                                 posicaoCotistaAberturaCollectionDeletar.Query.IdCotista.Equal(idCliente));
                    posicaoCotistaAberturaCollectionDeletar.Query.Load();
                    posicaoCotistaAberturaCollectionDeletar.MarkAllAsDeleted();
                    posicaoCotistaAberturaCollectionDeletar.Save();

                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollectionDeletar = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollectionDeletar.Query.Where(posicaoCotistaHistoricoCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                                 posicaoCotistaHistoricoCollectionDeletar.Query.IdCotista.Equal(idCliente));
                    posicaoCotistaHistoricoCollectionDeletar.Query.Load();
                    posicaoCotistaHistoricoCollectionDeletar.MarkAllAsDeleted();
                    posicaoCotistaHistoricoCollectionDeletar.Save();

                    OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
                    operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                                 operacaoCotistaCollectionDeletar.Query.IdCotista.Equal(idCliente));
                    operacaoCotistaCollectionDeletar.Query.Load();
                    operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
                    operacaoCotistaCollectionDeletar.Save();

                    PrejuizoCotistaCollection prejuizoCotistaCollectionDeletar = new PrejuizoCotistaCollection();
                    prejuizoCotistaCollectionDeletar.Query.Where(prejuizoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                                 prejuizoCotistaCollectionDeletar.Query.IdCotista.Equal(idCliente));
                    prejuizoCotistaCollectionDeletar.Query.Load();
                    prejuizoCotistaCollectionDeletar.MarkAllAsDeleted();
                    prejuizoCotistaCollectionDeletar.Save();

                    PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
                    prejuizoCotistaHistoricoCollection.Query.Where(prejuizoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                                   prejuizoCotistaHistoricoCollection.Query.IdCotista.Equal(idCliente));
                    prejuizoCotistaHistoricoCollection.Query.Load();
                    prejuizoCotistaHistoricoCollection.MarkAllAsDeleted();
                    prejuizoCotistaHistoricoCollection.Save();

                    Cotista cotista = new Cotista();
                    if (cotista.LoadByPrimaryKey(idCliente))
                    {
                        cotista.MarkAsDeleted();
                        cotista.Save();
                    }
                    #endregion

                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Query.Select(usuarioCollection.Query.IdUsuario,
                                                   usuarioCollection.Query.IdCliente);
                    usuarioCollection.Query.Where(usuarioCollection.Query.IdCliente.Equal(idCliente));
                    usuarioCollection.Query.Load();

                    foreach (Usuario usuario in usuarioCollection)
                    {
                        usuario.IdCliente = null;
                    }
                    usuarioCollection.Save();

                    Cliente cliente = new Cliente();
                    if (cliente.LoadByPrimaryKey(idCliente))
                    {
                        //
                        Cliente clienteClone = (Cliente)Utilitario.Clone(cliente);
                        //

                        cliente.MarkAsDeleted();
                        cliente.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Cliente - Operacao: Delete Cliente: " + idCliente + UtilitarioWeb.ToString(clienteClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    scope.Complete();
                }
            }
            #endregion
        }
        else if (e.Parameters == "btnAtivar")
        {
            #region Ativa Cliente

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
            //
            // Se tiver cliente Selecionado
            if (keyValuesId.Count != 0)
            {
                #region Cliente
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.StatusAtivo)
                                 .Where(clienteCollection.Query.IdCliente.In(keyValuesId));

                clienteCollection.Query.Load();

                for (int i = 0; i < clienteCollection.Count; i++)
                {
                    clienteCollection[i].StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                }
                #endregion

                #region Carteira
                CarteiraCollection carteiraCollection = new CarteiraCollection();
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.StatusAtivo)
                                 .Where(carteiraCollection.Query.IdCarteira.In(keyValuesId));

                carteiraCollection.Query.Load();

                for (int i = 0; i < carteiraCollection.Count; i++)
                {
                    carteiraCollection[i].StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
                }
                #endregion

                using (esTransactionScope scope = new esTransactionScope())
                {
                    clienteCollection.Save();
                    carteiraCollection.Save();

                    foreach (Cliente c in clienteCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cliente - Operacao: Ativação: " + c.IdCliente + ";" + UtilitarioWeb.ToString(c),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    foreach (Carteira cart in carteiraCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Carteira - Operacao: Ativação: " + cart.IdCarteira + ";" + UtilitarioWeb.ToString(cart),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    scope.Complete();
                }
            }
            #endregion
        }
        else if (e.Parameters == "btnDesativar")
        {
            #region Desativa Cliente

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
            //
            // Se tiver cliente Selecionado
            if (keyValuesId.Count != 0)
            {
                #region Cliente
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.StatusAtivo)
                                 .Where(clienteCollection.Query.IdCliente.In(keyValuesId));

                clienteCollection.Query.Load();

                for (int i = 0; i < clienteCollection.Count; i++)
                {
                    clienteCollection[i].StatusAtivo = (byte)StatusAtivoCliente.Inativo;
                }
                #endregion

                #region Carteira
                CarteiraCollection carteiraCollection = new CarteiraCollection();
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.StatusAtivo)
                                 .Where(carteiraCollection.Query.IdCarteira.In(keyValuesId));

                carteiraCollection.Query.Load();

                for (int i = 0; i < carteiraCollection.Count; i++)
                {
                    carteiraCollection[i].StatusAtivo = (byte)StatusAtivoCarteira.Inativo;
                }
                #endregion

                using (esTransactionScope scope = new esTransactionScope())
                {
                    clienteCollection.Save();
                    carteiraCollection.Save();

                    foreach (Cliente c in clienteCollection)
                    {

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cliente - Operacao: Desativação: " + c.IdCliente + ";" + UtilitarioWeb.ToString(c),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    foreach (Carteira cart in carteiraCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Carteira - Operacao: Desativação: " + cart.IdCarteira + ";" + UtilitarioWeb.ToString(cart),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    scope.Complete();
                }

            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            if (pageControl != null)
            {
                ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
                e.Properties["cpBtnEditCodigoPessoa"] = btnEditCodigoPessoa.ClientID;
            }
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue("Status"));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        //
        //this.ControlaVisibilidadeCamposGrid();
        //
        bool isOnTabPage = true;
        this.FocaCampoGrid("btnEditCodigoPessoa", isOnTabPage);
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion

    /// <summary>
    /// Força combo de Cálculo Gerencial como N no modo Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    [Obsolete("Nao mais usada")]
    protected void dropCalculaGerencial_OnPreRender(object sender, EventArgs e)
    {
        //// Modo Inserção
        //if (gridCadastro.IsNewRowEditing) {
        //    ASPxComboBox aspxCombox = sender as ASPxComboBox;
        //    aspxCombox.SelectedIndex = 1; // "N"
        //    aspxCombox.SelectedItem.Value = "N";
        //    aspxCombox.Text = "N";
        //    //aspxCombox.DataBind();
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnInit(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues[ClienteMetadata.ColumnNames.CalculaGerencial] = "N";
        e.NewValues[ClienteMetadata.ColumnNames.ZeraCaixa] = "N";
        e.NewValues[ClienteMetadata.ColumnNames.MultiMoeda] = "N";
        e.NewValues[ClienteMetadata.ColumnNames.DescontaTributoPL] = "2";
        e.NewValues[ClienteMetadata.ColumnNames.GrossUP] = "1";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        int index = this.gridCadastro.EditingRowVisibleIndex;
        if (index >= 0)
        {
            int idCliente = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, ClienteMetadata.ColumnNames.IdCliente));
            if (idCliente > 0)
            {
                ASPxLabel lblClienteBloqueado = pageControl.FindControl("lblClienteBloqueado") as ASPxLabel;
                UtilitarioWeb util = new UtilitarioWeb();

                if (util.IsClienteBloqueadoSinacor(idCliente))
                {
                    lblClienteBloqueado.Visible = true;
                }
                else
                {
                    lblClienteBloqueado.Visible = false;
                }

            }
        }

    }

    private string RetornaLoginAssociado(int idCliente)
    {
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        usuarioQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        usuarioQuery.Where(permissaoClienteQuery.IdCliente.Equal(idCliente));
        usuarioQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCliente));

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Load(usuarioQuery);

        string login = "";
        if (usuarioCollection.Count > 0)
        {
            login = usuarioCollection[0].Login.ToString();
        }

        return login;
    }

    private List<DateTime> RetornaListaMinDates(int idCliente)
    {
        List<DateTime> minDates = new List<DateTime>();

        //Descobrir data mais antiga de posicao e operacao
        OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
        OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

        operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Data.Min());
        operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente == idCliente);
        operacaoBolsaCollection.Load(operacaoBolsaCollection.Query);
        if (operacaoBolsaCollection[0].Data.HasValue)
        {
            minDates.Add(operacaoBolsaCollection[0].Data.Value);
        }

        operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.DataOperacao.Min());
        operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente == idCliente);
        operacaoFundoCollection.Load(operacaoFundoCollection.Query);
        if (operacaoFundoCollection[0].DataOperacao.HasValue)
        {
            minDates.Add(operacaoFundoCollection[0].DataOperacao.Value);
        }

        operacaoRendaFixaCollection.Query.Select(operacaoRendaFixaCollection.Query.DataOperacao.Min());
        operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente == idCliente);
        operacaoRendaFixaCollection.Load(operacaoRendaFixaCollection.Query);
        if (operacaoRendaFixaCollection[0].DataOperacao.HasValue)
        {
            minDates.Add(operacaoRendaFixaCollection[0].DataOperacao.Value);
        }

        operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.DataOperacao.Min());
        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente),
                                              operacaoCotistaCollection.Query.IdCotista.NotEqual(idCliente));
        operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);
        if (operacaoCotistaCollection[0].DataOperacao.HasValue)
        {
            minDates.Add(operacaoCotistaCollection[0].DataOperacao.Value);
        }

        PosicaoRendaFixaAberturaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaAberturaCollection();
        PosicaoBolsaAberturaCollection posicaoBolsaCollection = new PosicaoBolsaAberturaCollection();
        PosicaoFundoAberturaCollection posicaoFundoCollection = new PosicaoFundoAberturaCollection();

        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.DataHistorico.Min());
        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente == idCliente);
        posicaoRendaFixaCollection.Load(posicaoRendaFixaCollection.Query);
        if (posicaoRendaFixaCollection[0].DataHistorico.HasValue)
        {
            minDates.Add(posicaoRendaFixaCollection[0].DataHistorico.Value);
        }

        posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.DataHistorico.Min());
        posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == idCliente);
        posicaoBolsaCollection.Load(posicaoBolsaCollection.Query);
        if (posicaoBolsaCollection[0].DataHistorico.HasValue)
        {
            minDates.Add(posicaoBolsaCollection[0].DataHistorico.Value);
        }

        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.DataHistorico.Min());
        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente == idCliente);
        posicaoFundoCollection.Load(posicaoFundoCollection.Query);
        if (posicaoFundoCollection[0].DataHistorico.HasValue)
        {
            minDates.Add(posicaoFundoCollection[0].DataHistorico.Value);
        }

        return minDates;
    }


    #region Funções relativas ao Logotipo Cliente

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackClienteLogotipo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        // Limpa a Sessão
        Session.Remove("idClientes");

        // Pega os Elementos Selecionados
        List<object> idClientesSelecionados = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (idClientesSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Id clientes.";
        }
        else
        {
            // Salva os idClientes Selecionados na Sessão para posterior uso pelo Metodo de Upload
            Session["idClientes"] = idClientesSelecionados;
        }
    }

    /// <summary>
    /// Reseta O logotipo do Usuário
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackClienteResetaLogotipo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        // Pega os Elementos Selecionados
        List<object> idClientesSelecionados = gridCadastro.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (idClientesSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Id clientes.";
            return;
        }
        else
        {
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente,
                                           clienteCollection.Query.Logotipo)
                                   .Where(clienteCollection.Query.IdCliente.In(idClientesSelecionados));
            //
            clienteCollection.Query.Load();
            //
            for (int j = 0; j < clienteCollection.Count; j++)
            {
                clienteCollection[j].Logotipo = null;
            }

            clienteCollection.Save();
        }

        e.Result = "Logotipo resetado com sucesso.";
    }

    /// <summary>
    /// Processa o salvamento do Logotipo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplLogotipoCliente_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoPNG(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: .png\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo BMP
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento imagem Logotipo

        try
        {
            // Salva Imagem do Logotipo no Diretório de Imagens Personalizadas e atualiza o campo Logotipo 
            // para os clientes escolhidos.
            this.SalvaLogotipo(sr);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Logotipo usuário - " + e2.Message;
            return;
        }
        #endregion

        // Limpa a Sessão
        Session.Remove("idClientes");
    }

    public bool isExtensaoPNG(string arquivo)
    {
        string extensao = new FileInfo(arquivo).Extension;
        if (extensao != ".png")
        {
            return false;
        }
        return true;
    }

    private void SalvaLogotipo(Stream streamImagem)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\imagensPersonalizadas\\";

        int i = 1;
        string imagemCliente = path + "logotipoCliente_1.png";

        // Não repete nome da imagem
        while (File.Exists(imagemCliente))
        {
            i++;
            imagemCliente = path + "logotipoCliente_" + i.ToString() + ".png";
        }

        this.SaveStreamToFile(imagemCliente, streamImagem);

        // Atualiza o campo Logotipo para todos os Clientes Escolhidos
        // Pega os idsClientes Selecionados
        List<Object> idClientes = (List<object>)Session["idClientes"];
        //
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente,
                                       clienteCollection.Query.Logotipo)
                               .Where(clienteCollection.Query.IdCliente.In(idClientes));
        //
        clienteCollection.Query.Load();
        //
        for (int j = 0; j < clienteCollection.Count; j++)
        {
            clienteCollection[j].Logotipo = imagemCliente.Substring(imagemCliente.LastIndexOf("\\") + 1);
        }

        clienteCollection.Save();
    }

    /// <summary>
    /// Salva Stream como imagem no disco
    /// </summary>
    /// <param name="fileFullPath"></param>
    /// <param name="stream"></param>
    private void SaveStreamToFile(string fileFullPath, Stream stream)
    {
        if (stream.Length == 0) return;

        // Create a FileStream object to write a stream to a file
        using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length))
        {
            // Fill the bytes[] array with the stream data
            byte[] bytesInStream = new byte[stream.Length];
            stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

            // Use FileStream object to write to the specified file
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
        }
    }

    #endregion


    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback na página
    /// e a mascara perca o seu valor.
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        //ASPxComboBox drop = (ASPxComboBox)source;
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxCallbackPanel cbPanel = pageControl.FindControl("cbPanel") as ASPxCallbackPanel;
        ASPxComboBox dropRegimeEspecialTributacao = cbPanel.FindControl("dropRegimeEspecialTributacao") as ASPxComboBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        
        //Investidor estrangeiro(2689)
        if (dropTipo.SelectedItem.Value.Equals("800"))
            dropRegimeEspecialTributacao.Enabled = true;
        else
            dropRegimeEspecialTributacao.Enabled = false;
    }

    protected void callBackIdCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        #region Busca Max Value Cliente
        Cliente cliente = new Cliente();
        cliente.Query.Select(cliente.Query.IdCliente.Max());
        int idCliente = 1;
        if (cliente.Query.Load())
            idCliente = cliente.IdCliente.GetValueOrDefault(0) + idCliente;
        #endregion

        e.Result = idCliente.ToString();
    }
	
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAberturaIndexada_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        if (pageControl != null)
        {
            ASPxComboBox dropAberturaIndexada = pageControl.FindControl("dropAberturaIndexada") as ASPxComboBox;
            if (dropAberturaIndexada != null)
            {
                dropAberturaIndexada.Items.Clear();
                foreach (int i in Enum.GetValues(typeof(TipoAberturaIndexada)))
                {
                    dropAberturaIndexada.Items.Add(TipoAberturaIndexadaDescricao.RetornaStringValue(i), i);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;

    }
}