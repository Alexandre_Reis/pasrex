﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista.Perfil;

public partial class CadastrosBasicos_Cliente_SuitabilityAlerta : CadastroBasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasEditControl = false;
        this.AllowDelete = false;
        this.AllowUpdate = false;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);
    }

    #region data sources
    protected void EsDSSuitabilityAlertas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("P");
        PessoaSuitabilityQuery pessoaSuitabilityQuery = new PessoaSuitabilityQuery("PS");
        SuitabilityPerfilInvestidorQuery suitabilityPerfilInvestidorQuery = new SuitabilityPerfilInvestidorQuery("SPI");
        
        //TODO Alterar quando efetuar merge para versão 1.1.X (GPS) - Relacionamento Pessoa x Cotista (1xN)

        cotistaQuery.Select(cotistaQuery.IdCotista, 
                            cotistaQuery.Nome, 
                            pessoaQuery.Cpfcnpj, 
                            pessoaSuitabilityQuery.Perfil, 
                            pessoaSuitabilityQuery.UltimaAlteracao,
                            suitabilityPerfilInvestidorQuery.Nivel,
                            suitabilityPerfilInvestidorQuery.Perfil.As("PerfilCotista"));
        cotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdCotista.Equal(pessoaQuery.IdPessoa));
        cotistaQuery.LeftJoin(pessoaSuitabilityQuery).On(cotistaQuery.IdCotista.Equal(pessoaSuitabilityQuery.IdPessoa));
        cotistaQuery.LeftJoin(suitabilityPerfilInvestidorQuery).On(pessoaSuitabilityQuery.Perfil.Equal(suitabilityPerfilInvestidorQuery.IdPerfilInvestidor));

        if (!String.IsNullOrEmpty(btnEditCodigoCotistaFiltro.Text))
        {
            cotistaQuery.Where(cotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotistaFiltro.Text)));
        }

        if (dropTipoAlertaFiltro.SelectedIndex > -1)
        {
            int tipoAlerta = Convert.ToInt32(dropTipoAlertaFiltro.Value);

            if (tipoAlerta.Equals((int)SuitabilityAlertas.PerfilExpiraAte30dias))
            {
                DateTime dataValidade = DateTime.Now.AddDays(30);
                cotistaQuery.Where(pessoaSuitabilityQuery.UltimaAlteracao.LessThanOrEqual(dataValidade));
            }

            if (tipoAlerta.Equals((int)SuitabilityAlertas.PerfilExpirado))
            {
                ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

                SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
                suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

                DateTime dataValidade = controllerPerfilCotista.retornaValidade(DateTime.Now);

                cotistaQuery.Where(pessoaSuitabilityQuery.UltimaAlteracao.GreaterThan(dataValidade));
            }

            if (tipoAlerta.Equals((int)SuitabilityAlertas.PerfilNaoInformado))
            {
                cotistaQuery.Where(pessoaSuitabilityQuery.Perfil.IsNull());
            }
        }


        cotistaQuery.OrderBy(cotistaQuery.IdCotista.Ascending);

        CotistaCollection coll = new CotistaCollection();
        
        coll.Load(cotistaQuery);

        if (dropTipoAlertaFiltro.SelectedIndex > -1 && Convert.ToInt32(dropTipoAlertaFiltro.Value).Equals((int)SuitabilityAlertas.Desenquadramento))
        {
            CotistaCollection collNaoEnquadrados = new CotistaCollection();

            foreach (Cotista cotista in coll)
            {
                if (cotista.GetColumn("Nivel").ToString() != "")
                {
                    int nivelPerfilCotista = Convert.ToInt32(cotista.GetColumn("Nivel"));

                    if (this.verificaEnquadramento(cotista.IdCotista.Value, nivelPerfilCotista))
                    {
                        coll.DetachEntity(cotista);
                    }
                }
                else
                {
                    coll.DetachEntity(cotista);
                }
            }
        }

        e.Collection = coll;

    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected bool verificaEnquadramento(int idCotista, int nivelPerfilCotista)
    {
        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();

        SuitabilityPerfilInvestidor SuitabilityPerfilInvestidorPosicao = posicaoCotistaCollection.SelecionaNovoPerfilSuitability(idCotista, 0, 0);

        if (SuitabilityPerfilInvestidorPosicao.Nivel.HasValue)
        {
            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

            if (suitabilityParametrosWorkflow.VerificaEnquadramento.Equals(SuitabilityVerificaEnquadramento.Exato))
            {
                if (!nivelPerfilCotista.Equals(SuitabilityPerfilInvestidorPosicao.Nivel))
                    return false;
            }
            else
            {
                if (nivelPerfilCotista < SuitabilityPerfilInvestidorPosicao.Nivel.Value)
                    return false;
            }
        }

        return true;
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TipoAlerta")
        {
            string retorno = "";

            if (e.GetListSourceFieldValue("Perfil") == null || e.GetListSourceFieldValue("Perfil").ToString() == "")
            {
                retorno = "Perfil não informado. ";
            }
            else
            {
                ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

                SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
                suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

                DateTime dataUltimaAlteracao = Convert.ToDateTime(e.GetListSourceFieldValue("UltimaAlteracao"));
                DateTime validade = controllerPerfilCotista.retornaValidade(dataUltimaAlteracao);

                if (validade < DateTime.Now)
                {
                    retorno = "Perfil expirado. ";
                }

                if (validade < DateTime.Now.AddDays(30))
                {
                    retorno += "Perfil expira em até 30 dias. ";
                }

                int nivelPerfil = Convert.ToInt32(e.GetListSourceFieldValue("Nivel"));
                int idCotista = Convert.ToInt32(e.GetListSourceFieldValue("IdCotista"));

                if (!this.verificaEnquadramento(idCotista, nivelPerfil))
                {
                    retorno += "Desenquadramento. ";
                }
            }
            
            e.Value = retorno;
        }

    }
}
