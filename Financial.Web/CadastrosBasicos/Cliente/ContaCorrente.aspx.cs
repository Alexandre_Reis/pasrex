﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using Financial.Export;
using System.Xml;
using Financial.Interfaces.Export;

public partial class CadastrosBasicos_ContaCorrente : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupPessoa = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { ContaCorrenteMetadata.ColumnNames.ContaDefault }));
    }

    #region DataSources
    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("L");

        contaCorrenteQuery.InnerJoin(pessoaQuery).On(contaCorrenteQuery.IdPessoa == pessoaQuery.IdPessoa);

        ContaCorrenteCollection coll = new ContaCorrenteCollection();
        coll.Load(contaCorrenteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSBanco_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        BancoCollection coll = new BancoCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgencia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxComboBox dropBanco = gridCadastro.FindEditFormTemplateControl("dropBanco") as ASPxComboBox;

        if (dropBanco != null && dropBanco.SelectedIndex != -1) {
            AgenciaCollection coll = new AgenciaCollection();
            coll.Query.Where(coll.Query.IdBanco.Equal(dropBanco.SelectedItem.Value));
            coll.Query.Load();

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            AgenciaCollection coll = new AgenciaCollection();
            coll.Query.Load();

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PessoaCollection coll = new PessoaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        MoedaCollection coll = new MoedaCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoContaCollection coll = new TipoContaCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoContaCollection coll = new GrupoContaCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void dropAgencia_Callback(object source, CallbackEventArgsBase e) {
        ASPxComboBox dropAgencia = source as ASPxComboBox;
        dropAgencia.DataBind();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        TextBox textNumero = gridCadastro.FindEditFormTemplateControl("textNumero") as TextBox;
        ASPxComboBox dropContaDefault = gridCadastro.FindEditFormTemplateControl("dropContaDefault") as ASPxComboBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoPessoa);
        controles.Add(textNumero);
        controles.Add(dropContaDefault);
        controles.Add(dropMoeda);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);

        if (gridCadastro.IsNewRowEditing) { // Insert
            if (dropContaDefault.SelectedItem.Value.ToString() == "S") {
                int idMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);

                ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa == idPessoa);
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.ContaDefault == "S");
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdMoeda == idMoeda);

                if (contaCorrenteCollection.Query.Load()) {
                    e.Result = "Já existe uma conta principal para esta moeda associada. Operação não pode ser realizada.";
                    return;
                }
            }
        }
        else if (gridCadastro.IsEditing) { // Update
            if (dropContaDefault.SelectedItem.Value.ToString() == "S") { // Se está trocando para sim
                int idConta = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdConta"));
                int idMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);

                // Carrega a Conta que está sendo feito o Update
                ContaCorrente contaCorrente= new ContaCorrente();
                contaCorrente.LoadByPrimaryKey(idConta);
                if (contaCorrente.ContaDefault == "S") {
                    // Se houver Troca de moeda com conta Default para outra moeda que já tenha conta Default
                    // Exemplo: Troca de ContaDefault = Sim, Moeda = Real para ContaDefault = Sim, Moeda = Dolar
                    
                    // Troca de Moeda
                    if (idMoeda != contaCorrente.IdMoeda) {
                        ContaCorrenteCollection contaCorrenteCollection1 = new ContaCorrenteCollection();
                        contaCorrenteCollection1.Query.Where(contaCorrenteCollection1.Query.IdPessoa == idPessoa);
                        contaCorrenteCollection1.Query.Where(contaCorrenteCollection1.Query.ContaDefault == "S");
                        contaCorrenteCollection1.Query.Where(contaCorrenteCollection1.Query.IdMoeda == idMoeda);
                        contaCorrenteCollection1.Query.Where(contaCorrenteCollection1.Query.IdConta.NotEqual(idConta));
                        if (contaCorrenteCollection1.Query.Load()) {
                            e.Result = "Já existe uma conta principal associada. Operação não pode ser realizada.";
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }

                // Para o caso de trocar de contaDefault não para sim faz a conferencia se já existe outra conta
                ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa == idPessoa);                
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.ContaDefault == "S");
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdMoeda == idMoeda);
                contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdConta.NotEqual(idConta));
                if (contaCorrenteCollection.Query.Load()) {
                    e.Result = "Já existe uma conta principal associada. Operação não pode ser realizada.";
                    return;
                }
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropBanco = gridCadastro.FindEditFormTemplateControl("dropBanco") as ASPxComboBox;
        ASPxComboBox dropAgencia = gridCadastro.FindEditFormTemplateControl("dropAgencia") as ASPxComboBox;
        TextBox textNumero = gridCadastro.FindEditFormTemplateControl("textNumero") as TextBox;
        TextBox textDigito = gridCadastro.FindEditFormTemplateControl("textDigito") as TextBox;
        ASPxComboBox dropContaDefault = gridCadastro.FindEditFormTemplateControl("dropContaDefault") as ASPxComboBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocal = gridCadastro.FindEditFormTemplateControl("dropLocal") as ASPxComboBox;
        ASPxComboBox dropTipoConta = gridCadastro.FindEditFormTemplateControl("dropTipoConta") as ASPxComboBox;
        ASPxComboBox dropGrupo = gridCadastro.FindEditFormTemplateControl("dropGrupo") as ASPxComboBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;

        ContaCorrente contaCorrente = new ContaCorrente();

        if (dropBanco.SelectedIndex != -1) {
            contaCorrente.IdBanco = Convert.ToInt32(dropBanco.SelectedItem.Value);
        }

        if (dropAgencia.SelectedIndex != -1) {
            contaCorrente.IdAgencia = Convert.ToInt32(dropAgencia.SelectedItem.Value);
        }

        contaCorrente.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        contaCorrente.Numero = Convert.ToString(textNumero.Text);
        contaCorrente.DigitoConta = Convert.ToString(textDigito.Text);
        contaCorrente.ContaDefault = Convert.ToString(dropContaDefault.SelectedItem.Value);
        contaCorrente.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);

        if (dropLocal.SelectedIndex != -1)
        {
            contaCorrente.IdLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
        }

        if (dropTipoConta.SelectedIndex != -1)
        {
            contaCorrente.IdTipoConta = Convert.ToInt32(dropTipoConta.SelectedItem.Value);
        }

        if (dropGrupo.SelectedIndex != -1)
        {
            contaCorrente.IdGrupo = Convert.ToInt32(dropGrupo.SelectedItem.Value);
        }

        contaCorrente.DescricaoCodigo = textDescricao.Text;

        contaCorrente.Save();

        #region CAC_XML
        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
        if (integraCotistaItau)
        {
            Itau itau = new Itau();
            itau.EnviaCci_XML((int)contaCorrente.IdPessoa, contaCorrente, "I");
            
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ContaCorrente - Operacao: Insert ContaCorrente: " + contaCorrente.IdConta + UtilitarioWeb.ToString(contaCorrente),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idPessoa);

            if (pessoa.str.Apelido != "") {
                nome = pessoa.str.Apelido;
            }
        }
        e.Result = nome;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idConta = (int)e.Keys[0];

        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropBanco = gridCadastro.FindEditFormTemplateControl("dropBanco") as ASPxComboBox;
        ASPxComboBox dropAgencia = gridCadastro.FindEditFormTemplateControl("dropAgencia") as ASPxComboBox;
        TextBox textNumero = gridCadastro.FindEditFormTemplateControl("textNumero") as TextBox;
        TextBox textDigito = gridCadastro.FindEditFormTemplateControl("textDigito") as TextBox;
        ASPxComboBox dropContaDefault = gridCadastro.FindEditFormTemplateControl("dropContaDefault") as ASPxComboBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocal = gridCadastro.FindEditFormTemplateControl("dropLocal") as ASPxComboBox;
        ASPxComboBox dropTipoConta = gridCadastro.FindEditFormTemplateControl("dropTipoConta") as ASPxComboBox;
        ASPxComboBox dropGrupo = gridCadastro.FindEditFormTemplateControl("dropGrupo") as ASPxComboBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;

        ContaCorrente contaCorrente = new ContaCorrente();

        if (contaCorrente.LoadByPrimaryKey(idConta)) {
            if (dropBanco.SelectedIndex != -1) {
                contaCorrente.IdBanco = Convert.ToInt32(dropBanco.SelectedItem.Value);
            }

            if (dropAgencia.SelectedIndex != -1) {
                contaCorrente.IdAgencia = Convert.ToInt32(dropAgencia.SelectedItem.Value);
            }

            contaCorrente.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
            contaCorrente.Numero = Convert.ToString(textNumero.Text);
            contaCorrente.DigitoConta = Convert.ToString(textDigito.Text);
            contaCorrente.ContaDefault = Convert.ToString(dropContaDefault.SelectedItem.Value);
            contaCorrente.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);

            if (dropLocal.SelectedIndex != -1)
            {
                contaCorrente.IdLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
            }

            if (dropTipoConta.SelectedIndex != -1)
            {
                contaCorrente.IdTipoConta = Convert.ToInt32(dropTipoConta.SelectedItem.Value);
            }

            if (dropGrupo.SelectedIndex != -1)
            {
                contaCorrente.IdGrupo = Convert.ToInt32(dropGrupo.SelectedItem.Value);
            }

            contaCorrente.DescricaoCodigo = textDescricao.Text;

            contaCorrente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ContaCorrente - Operacao: Update ContaCorrente: " + idConta + UtilitarioWeb.ToString(contaCorrente),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();
        
        e.Cancel = true;
        gridCadastro.CancelEdit();        
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdConta");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idConta = Convert.ToInt32(keyValuesId[i]);

                SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdConta.Equal(idConta));
                saldoCaixaCollection.Query.Load();
                saldoCaixaCollection.MarkAllAsDeleted();
                saldoCaixaCollection.Save();

                ContaCorrente contaCorrente = new ContaCorrente();
                if (contaCorrente.LoadByPrimaryKey(idConta)) {
                    //
                    ContaCorrente contaCorrenteClone = (ContaCorrente)Utilitario.Clone(contaCorrente);
                    //
                    contaCorrente.MarkAsDeleted();
                    contaCorrente.Save();
                    
                    #region CAC_XML
                    bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null? 
                        Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]): false;
                    if (integraCotistaItau)
                    {
                        Itau itau = new Itau();
                        itau.EnviaCci_XML((int)contaCorrenteClone.IdPessoa, contaCorrenteClone, "E");
                        
                    }
                    #endregion
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ContaCorrente - Operacao: Delete ContaCorrente: " + idConta + UtilitarioWeb.ToString(contaCorrenteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoPessoa"] = btnEditCodigoPessoa.ClientID;
        }
    }
   
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoPessoa");
        base.gridCadastro_PreRender(sender, e);
    }

    protected void btnEditCodigoPessoa_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
    }
}