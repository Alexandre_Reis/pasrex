﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.InvestidorCotista;
using EntitySpaces.Interfaces;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Enquadra;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Interfaces.Sinacor;
using Financial.CRM.Enums;
using Financial.Integracao.Excel;
using Financial.Bolsa.Enums;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using System.Web.Configuration;
using Financial.Export;
using System.Xml;
using Financial.Interfaces.Export;
using Financial.Util.Enums;

public partial class CadastrosBasicos_Pessoa : CadastroBasePage
{
    bool idAutomatico = ParametrosConfiguracaoSistema.Outras.IdAutomaticoPessoa.Equals("S");
    bool permiteDocumentoDuplicado = ParametrosConfiguracaoSistema.Outras.PermiteDuplDocumentoPessoa.Equals("S");
    bool propagaAlterNomeAgente = ParametrosConfiguracaoSistema.Outras.PropagaAlterNomeAgente.Equals("S");
    bool propagaAlterNomeCliente = ParametrosConfiguracaoSistema.Outras.PropagaAlterNomeCliente.Equals("S");

    bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;

    private bool _integracaoCrm;

    new protected void Page_Load(object sender, EventArgs e)
    {
        _integracaoCrm = WebConfigurationManager.AppSettings["IntegraCotistaCrm"] != null ? Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaCrm"]) : false;

        base.Page_Load(sender, e);
        
        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { PessoaMetadata.ColumnNames.Tipo }));

    }

    /// <summary>
    /// TabControl - Controla visibilidade da 4 aba
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tabCadastroOnLoad(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = sender as ASPxPageControl;

        if (integraCotistaItau)
        {
            pageControl.TabPages[3].ClientVisible = true;
        }
        else
        {
            pageControl.TabPages[3].ClientVisible = false;
        }
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaQuery pessoaQuery = new PessoaQuery("P");
        PessoaSuitabilityQuery pessoaSuitabilityQuery = new PessoaSuitabilityQuery("PS");

        pessoaQuery.Select(pessoaQuery, pessoaSuitabilityQuery);
        pessoaQuery.LeftJoin(pessoaSuitabilityQuery).On(pessoaQuery.IdPessoa.Equal(pessoaSuitabilityQuery.IdPessoa));
        pessoaQuery.OrderBy(pessoaQuery.Apelido.Ascending);

        PessoaCollection coll = new PessoaCollection();
        coll.Load(pessoaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPessoaEndereco_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxSpinEdit textIdPessoa = pageControl.FindControl("textIdPessoa") as ASPxSpinEdit;

            int idPessoa = Convert.ToInt32(textIdPessoa.Text);

            #region PessoaEndereco
            PessoaEnderecoCollection coll = new PessoaEnderecoCollection();
            coll.Query.Where(coll.Query.IdPessoa.Equal(idPessoa));
            coll.Query.OrderBy(coll.Query.RecebeCorrespondencia.Descending);
            coll.Query.Load();

            // Preenche apenas os 2 primeiros endereços
            if (coll.Count > 0)
            {
                #region Endereco 1
                //Guarda o Id do endereco 1
                HiddenField hiddenIdEndereco1 = pageControl.FindControl("hiddenIdEndereco1") as HiddenField;
                //
                TextBox textEndereco = pageControl.FindControl("textEndereco") as TextBox;
                TextBox textNumero = pageControl.FindControl("textNumero") as TextBox;
                TextBox textComplemento = pageControl.FindControl("textComplemento") as TextBox;
                TextBox textBairro = pageControl.FindControl("textBairro") as TextBox;
                TextBox textCidade = pageControl.FindControl("textCidade") as TextBox;
                TextBox textCEP = pageControl.FindControl("textCEP") as TextBox;
                TextBox textUF = pageControl.FindControl("textUF") as TextBox;
                TextBox textPais = pageControl.FindControl("textPais") as TextBox;
                ASPxComboBox dropRecebeCorrespondencia = pageControl.FindControl("dropRecebeCorrespondencia") as ASPxComboBox;
                ASPxComboBox dropTipoEndereco = pageControl.FindControl("dropTipoEndereco") as ASPxComboBox;

                hiddenIdEndereco1.Value = coll[0].IdEndereco.Value.ToString();

                textEndereco.Text = coll[0].Endereco;
                textNumero.Text = coll[0].Numero;

                if (!String.IsNullOrEmpty(coll[0].Complemento))
                {
                    textComplemento.Text = coll[0].Complemento;
                }

                textBairro.Text = coll[0].Bairro;
                textCidade.Text = coll[0].Cidade;
                textCEP.Text = coll[0].Cep;
                textUF.Text = coll[0].Uf;
                textPais.Text = coll[0].Pais;

                dropRecebeCorrespondencia.SelectedIndex = coll[0].RecebeCorrespondencia == "S" ? 0 : 1;

                if (!String.IsNullOrEmpty(coll[0].TipoEndereco))
                {
                    dropTipoEndereco.SelectedIndex = coll[0].TipoEndereco == "C" ? 0 : 1;
                }
                else
                {
                    dropTipoEndereco.SelectedIndex = -1;
                }
                #endregion
            }

            if (coll.Count > 1)
            {
                #region Endereco 2
                //Guarda o Id do endereco 2
                HiddenField hiddenIdEndereco2 = pageControl.FindControl("hiddenIdEndereco2") as HiddenField;
                //

                TextBox textEndereco2 = pageControl.FindControl("textEndereco2") as TextBox;
                TextBox textNumero2 = pageControl.FindControl("textNumero2") as TextBox;
                TextBox textComplemento2 = pageControl.FindControl("textComplemento2") as TextBox;
                TextBox textBairro2 = pageControl.FindControl("textBairro2") as TextBox;
                TextBox textCidade2 = pageControl.FindControl("textCidade2") as TextBox;
                TextBox textCEP2 = pageControl.FindControl("textCEP2") as TextBox;
                TextBox textUF2 = pageControl.FindControl("textUF2") as TextBox;
                TextBox textPais2 = pageControl.FindControl("textPais2") as TextBox;
                ASPxComboBox dropRecebeCorrespondencia2 = pageControl.FindControl("dropRecebeCorrespondencia2") as ASPxComboBox;
                //TextBox textFone2 = pageControl.FindControl("textFone2") as TextBox;
                //TextBox textEmail2 = pageControl.FindControl("textEmail2") as TextBox;
                //
                ASPxComboBox dropTipoEndereco2 = pageControl.FindControl("dropTipoEndereco2") as ASPxComboBox;

                hiddenIdEndereco2.Value = coll[1].IdEndereco.Value.ToString();

                textEndereco2.Text = coll[1].Endereco;
                textNumero2.Text = coll[1].Numero;

                if (!String.IsNullOrEmpty(coll[1].Complemento))
                {
                    textComplemento2.Text = coll[1].Complemento;
                }

                textBairro2.Text = coll[1].Bairro;
                textCidade2.Text = coll[1].Cidade;
                textCEP2.Text = coll[1].Cep;
                textUF2.Text = coll[1].Uf;
                textPais2.Text = coll[1].Pais;
                //textEmail2.Text = coll[1].Email;
                //textFone2.Text = coll[1].Fone;

                dropRecebeCorrespondencia2.SelectedIndex = coll[1].RecebeCorrespondencia == "S" ? 0 : 1;

                if (!String.IsNullOrEmpty(coll[1].TipoEndereco))
                {
                    dropTipoEndereco2.SelectedIndex = coll[1].TipoEndereco == "C" ? 0 : 1;
                }

                else
                {
                    dropTipoEndereco2.SelectedIndex = -1;
                }
                #endregion
            }
            #endregion

            #region PessoaTelefone
            PessoaTelefoneCollection collTelefone = new PessoaTelefoneCollection();
            collTelefone.Query.Where(collTelefone.Query.IdPessoa == idPessoa);
            collTelefone.Query.OrderBy(collTelefone.Query.TipoTelefone.Ascending);

            collTelefone.Query.Load();

            // Preenche apenas os 3 primeiros Telefones
            if (collTelefone.Count > 0)
            {
                #region Telefone 1
                //Guarda o Id do Telefone 1
                HiddenField hiddenIdTelefone1 = pageControl.FindControl("hiddenTelefone1") as HiddenField;
                //
                ASPxTextBox txtDDI = pageControl.FindControl("txtDDI") as ASPxTextBox;
                ASPxTextBox txtDDD = pageControl.FindControl("txtDDD") as ASPxTextBox;
                ASPxTextBox txtNumero = pageControl.FindControl("txtNumero") as ASPxTextBox;
                ASPxTextBox txtRamal = pageControl.FindControl("txtRamal") as ASPxTextBox;
                //
                ASPxComboBox dropTipoTelefone = pageControl.FindControl("dropTipoTelefone") as ASPxComboBox;
                //
                //
                hiddenIdTelefone1.Value = collTelefone[0].IdTelefone.Value.ToString();

                txtDDI.Text = collTelefone[0].Ddi;
                txtDDD.Text = collTelefone[0].Ddd;
                txtNumero.Text = collTelefone[0].Numero;

                if (!String.IsNullOrEmpty(collTelefone[0].Ramal))
                {
                    txtRamal.Text = collTelefone[0].Ramal;
                }

                if (collTelefone[0].TipoTelefone.Value == (byte)TipoTelefonePessoa.Residencial)
                {
                    dropTipoTelefone.SelectedIndex = ((int)TipoTelefonePessoa.Residencial) - 1;
                }
                else if (collTelefone[0].TipoTelefone.Value == (byte)TipoTelefonePessoa.Celular)
                {
                    dropTipoTelefone.SelectedIndex = ((int)TipoTelefonePessoa.Celular) - 1;
                }
                else if (collTelefone[0].TipoTelefone.Value == (byte)TipoTelefonePessoa.Comercial)
                {
                    dropTipoTelefone.SelectedIndex = ((int)TipoTelefonePessoa.Comercial) - 1;
                }
                else if (collTelefone[0].TipoTelefone.Value == (byte)TipoTelefonePessoa.Outros)
                {
                    dropTipoTelefone.SelectedIndex = ((int)TipoTelefonePessoa.Outros) - 1;
                }
                else
                {
                    dropTipoTelefone.SelectedIndex = -1;
                }
                #endregion
            }

            // Preenche apenas os 3 primeiros Telefones
            if (collTelefone.Count > 1)
            {
                #region Telefone 2
                //Guarda o Id do Telefone 2
                HiddenField hiddenIdTelefone2 = pageControl.FindControl("hiddenTelefone2") as HiddenField;
                //
                ASPxTextBox txtDDI1 = pageControl.FindControl("txtDDI1") as ASPxTextBox;
                ASPxTextBox txtDDD1 = pageControl.FindControl("txtDDD1") as ASPxTextBox;
                ASPxTextBox txtNumero1 = pageControl.FindControl("txtNumero1") as ASPxTextBox;
                ASPxTextBox txtRamal1 = pageControl.FindControl("txtRamal1") as ASPxTextBox;
                //
                ASPxComboBox dropTipoTelefone1 = pageControl.FindControl("dropTipoTelefone1") as ASPxComboBox;
                //
                //
                hiddenIdTelefone2.Value = collTelefone[1].IdTelefone.Value.ToString();

                txtDDI1.Text = collTelefone[1].Ddi;
                txtDDD1.Text = collTelefone[1].Ddd;
                txtNumero1.Text = collTelefone[1].Numero;

                if (!String.IsNullOrEmpty(collTelefone[1].Ramal))
                {
                    txtRamal1.Text = collTelefone[1].Ramal;
                }

                if (collTelefone[1].TipoTelefone.Value == (byte)TipoTelefonePessoa.Residencial)
                {
                    dropTipoTelefone1.SelectedIndex = ((int)TipoTelefonePessoa.Residencial) - 1;
                }
                else if (collTelefone[1].TipoTelefone.Value == (byte)TipoTelefonePessoa.Celular)
                {
                    dropTipoTelefone1.SelectedIndex = ((int)TipoTelefonePessoa.Celular) - 1;
                }
                else if (collTelefone[1].TipoTelefone.Value == (byte)TipoTelefonePessoa.Comercial)
                {
                    dropTipoTelefone1.SelectedIndex = ((int)TipoTelefonePessoa.Comercial) - 1;
                }
                else if (collTelefone[1].TipoTelefone.Value == (byte)TipoTelefonePessoa.Outros)
                {
                    dropTipoTelefone1.SelectedIndex = ((int)TipoTelefonePessoa.Outros) - 1;
                }
                else
                {
                    dropTipoTelefone1.SelectedIndex = -1;
                }
                #endregion
            }

            // Preenche apenas os 3 primeiros Telefones
            if (collTelefone.Count > 2)
            {
                #region Telefone 3
                //Guarda o Id do Telefone 3
                HiddenField hiddenIdTelefone3 = pageControl.FindControl("hiddenTelefone3") as HiddenField;
                //
                ASPxTextBox txtDDI2 = pageControl.FindControl("txtDDI2") as ASPxTextBox;
                ASPxTextBox txtDDD2 = pageControl.FindControl("txtDDD2") as ASPxTextBox;
                ASPxTextBox txtNumero2 = pageControl.FindControl("txtNumero2") as ASPxTextBox;
                ASPxTextBox txtRamal2 = pageControl.FindControl("txtRamal2") as ASPxTextBox;
                //
                ASPxComboBox dropTipoTelefone2 = pageControl.FindControl("dropTipoTelefone2") as ASPxComboBox;
                //
                //
                hiddenIdTelefone3.Value = collTelefone[2].IdTelefone.Value.ToString();

                txtDDI2.Text = collTelefone[2].Ddi;
                txtDDD2.Text = collTelefone[2].Ddd;
                txtNumero2.Text = collTelefone[2].Numero;

                if (!String.IsNullOrEmpty(collTelefone[2].Ramal))
                {
                    txtRamal2.Text = collTelefone[2].Ramal;
                }

                if (collTelefone[2].TipoTelefone.Value == (byte)TipoTelefonePessoa.Residencial)
                {
                    dropTipoTelefone2.SelectedIndex = ((int)TipoTelefonePessoa.Residencial) - 1;
                }
                else if (collTelefone[2].TipoTelefone.Value == (byte)TipoTelefonePessoa.Celular)
                {
                    dropTipoTelefone2.SelectedIndex = ((int)TipoTelefonePessoa.Celular) - 1;
                }
                else if (collTelefone[2].TipoTelefone.Value == (byte)TipoTelefonePessoa.Comercial)
                {
                    dropTipoTelefone2.SelectedIndex = ((int)TipoTelefonePessoa.Comercial) - 1;
                }
                else if (collTelefone[2].TipoTelefone.Value == (byte)TipoTelefonePessoa.Outros)
                {
                    dropTipoTelefone2.SelectedIndex = ((int)TipoTelefonePessoa.Outros) - 1;
                }
                else
                {
                    dropTipoTelefone2.SelectedIndex = -1;
                }
                #endregion
            }
            #endregion

            #region PessoaEmail
            PessoaEmailCollection collEmail = new PessoaEmailCollection();
            collEmail.Query.Where(collEmail.Query.IdPessoa == idPessoa);
            collEmail.Query.OrderBy(collEmail.Query.Email.Ascending);

            collEmail.Query.Load();

            // Preenche apenas os 2 primeiros Emails
            if (collEmail.Count > 0)
            {
                #region Email 1
                //Guarda o Id do Email 1
                HiddenField hiddenIdEmail1 = pageControl.FindControl("hiddenEmail1") as HiddenField;
                //
                TextBox txtEmail1 = pageControl.FindControl("txtEmail1") as TextBox;
                //
                hiddenIdEmail1.Value = collEmail[0].IdEmail.Value.ToString();

                txtEmail1.Text = collEmail[0].Email.Trim();
                #endregion
            }

            if (collEmail.Count > 1)
            {
                #region Email 2
                //Guarda o Id do Email 2
                HiddenField hiddenIdEmail2 = pageControl.FindControl("hiddenEmail2") as HiddenField;
                //
                TextBox txtEmail2 = pageControl.FindControl("txtEmail2") as TextBox;
                //
                hiddenIdEmail2.Value = collEmail[1].IdEmail.Value.ToString();
                //
                txtEmail2.Text = collEmail[1].Email.Trim();
                #endregion
            }

            #endregion

            #region PessoaDocumento
            PessoaDocumentoCollection collDocumento = new PessoaDocumentoCollection();
            collDocumento.Query.Where(collDocumento.Query.IdPessoa == idPessoa);
            collDocumento.Query.OrderBy(collDocumento.Query.NumeroDocumento.Ascending);

            collDocumento.Query.Load();

            // Preenche apenas os 3 primeiros Documentos
            if (collDocumento.Count > 0)
            {
                #region Documento 1
                //Guarda o Id do Documento 1
                HiddenField hiddenIdDocumento1 = pageControl.FindControl("hiddenDocumento1") as HiddenField;
                //
                ASPxComboBox dropTipoDocumento = pageControl.FindControl("dropTipoDocumento") as ASPxComboBox;
                ASPxComboBox dropOrgaoEmissor = pageControl.FindControl("dropOrgaoEmissor") as ASPxComboBox;
                ASPxComboBox dropUFEmissor = pageControl.FindControl("dropUFEmissor") as ASPxComboBox;
                //
                ASPxTextBox txtNumDocumento = pageControl.FindControl("txtNumDocumento") as ASPxTextBox;
                ASPxDateEdit textDataExpedicao = pageControl.FindControl("textDataExpedicao") as ASPxDateEdit;
                ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;
                //
                hiddenIdDocumento1.Value = collDocumento[0].IdDocumento.Value.ToString();
                //
                txtNumDocumento.Text = collDocumento[0].NumeroDocumento;

                if (collDocumento[0].DataExpedicao.HasValue)
                {
                    textDataExpedicao.Value = collDocumento[0].DataExpedicao.Value;
                }

                if (collDocumento[0].DataVencimento.HasValue)
                {
                    textDataVencimento.Value = collDocumento[0].DataVencimento.Value;
                }

                // Tipo Documento
                if (collDocumento[0].TipoDocumento.HasValue)
                {
                    dropTipoDocumento.SelectedIndex = (int)collDocumento[0].TipoDocumento.Value - 1;
                }
                else
                {
                    dropTipoDocumento.SelectedIndex = -1;
                }

                //  Orgão Emissor
                if (!String.IsNullOrEmpty(collDocumento[0].OrgaoEmissor))
                {
                    dropOrgaoEmissor.Value = collDocumento[0].OrgaoEmissor;
                }
                else
                {
                    dropOrgaoEmissor.SelectedIndex = -1;
                }

                // UF Emissor
                if (!String.IsNullOrEmpty(collDocumento[0].UFEmissor))
                {
                    dropUFEmissor.Value = collDocumento[0].UFEmissor;
                }
                else
                {
                    dropUFEmissor.SelectedIndex = -1;
                }
                #endregion
            }

            // Preenche apenas os 3 primeiros Documentos
            if (collDocumento.Count > 1)
            {
                #region Documento 2
                //Guarda o Id do Documento 2
                HiddenField hiddenIdDocumento2 = pageControl.FindControl("hiddenDocumento2") as HiddenField;
                //
                ASPxComboBox dropTipoDocumento1 = pageControl.FindControl("dropTipoDocumento1") as ASPxComboBox;
                ASPxComboBox dropOrgaoEmissor1 = pageControl.FindControl("dropOrgaoEmissor1") as ASPxComboBox;
                ASPxComboBox dropUFEmissor1 = pageControl.FindControl("dropUFEmissor1") as ASPxComboBox;
                //
                ASPxTextBox txtNumDocumento1 = pageControl.FindControl("txtNumDocumento1") as ASPxTextBox;
                ASPxDateEdit textDataExpedicao1 = pageControl.FindControl("textDataExpedicao1") as ASPxDateEdit;
                ASPxDateEdit textDataVencimento1 = pageControl.FindControl("textDataVencimento1") as ASPxDateEdit;
                //
                //
                hiddenIdDocumento2.Value = collDocumento[1].IdDocumento.Value.ToString();

                txtNumDocumento1.Text = collDocumento[1].NumeroDocumento;

                if (collDocumento[1].DataExpedicao.HasValue)
                {
                    textDataExpedicao1.Value = collDocumento[1].DataExpedicao.Value;
                }

                if (collDocumento[1].DataVencimento.HasValue)
                {
                    textDataVencimento1.Value = collDocumento[1].DataVencimento.Value;
                }

                // Tipo Documento
                if (collDocumento[1].TipoDocumento.HasValue)
                {
                    dropTipoDocumento1.SelectedIndex = (int)collDocumento[1].TipoDocumento.Value - 1;
                }
                else
                {
                    dropTipoDocumento1.SelectedIndex = -1;
                }

                //  Orgão Emissor
                if (!String.IsNullOrEmpty(collDocumento[1].OrgaoEmissor))
                {
                    dropOrgaoEmissor1.Value = collDocumento[1].OrgaoEmissor;
                }
                else
                {
                    dropOrgaoEmissor1.SelectedIndex = -1;
                }

                // UF Emissor
                if (!String.IsNullOrEmpty(collDocumento[1].UFEmissor))
                {
                    dropUFEmissor1.Value = collDocumento[1].UFEmissor;
                }
                else
                {
                    dropUFEmissor1.SelectedIndex = -1;
                }
                #endregion
            }

            // Preenche apenas os 3 primeiros Documentos
            if (collDocumento.Count > 2)
            {
                #region Documento 3
                //Guarda o Id do Documento 3
                HiddenField hiddenIdDocumento3 = pageControl.FindControl("hiddenDocumento3") as HiddenField;

                ASPxComboBox dropTipoDocumento2 = pageControl.FindControl("dropTipoDocumento2") as ASPxComboBox;
                ASPxComboBox dropOrgaoEmissor2 = pageControl.FindControl("dropOrgaoEmissor2") as ASPxComboBox;
                ASPxComboBox dropUFEmissor2 = pageControl.FindControl("dropUFEmissor2") as ASPxComboBox;
                //
                ASPxTextBox txtNumDocumento2 = pageControl.FindControl("txtNumDocumento2") as ASPxTextBox;
                ASPxDateEdit textDataExpedicao2 = pageControl.FindControl("textDataExpedicao2") as ASPxDateEdit;
                ASPxDateEdit textDataVencimento2 = pageControl.FindControl("textDataVencimento2") as ASPxDateEdit;
                //
                //
                hiddenIdDocumento3.Value = collDocumento[2].IdDocumento.Value.ToString();

                txtNumDocumento2.Text = collDocumento[2].NumeroDocumento;

                if (collDocumento[2].DataExpedicao.HasValue)
                {
                    textDataExpedicao2.Value = collDocumento[2].DataExpedicao.Value;
                }

                if (collDocumento[2].DataVencimento.HasValue)
                {
                    textDataVencimento2.Value = collDocumento[2].DataVencimento.Value;
                }

                // Tipo Documento
                if (collDocumento[2].TipoDocumento.HasValue)
                {
                    dropTipoDocumento2.SelectedIndex = (int)collDocumento[2].TipoDocumento.Value - 1;
                }
                else
                {
                    dropTipoDocumento2.SelectedIndex = -1;
                }

                //  Orgão Emissor
                if (!String.IsNullOrEmpty(collDocumento[2].OrgaoEmissor))
                {
                    dropOrgaoEmissor2.Value = collDocumento[2].OrgaoEmissor;
                }
                else
                {
                    dropOrgaoEmissor2.SelectedIndex = -1;
                }

                // UF Emissor
                if (!String.IsNullOrEmpty(collDocumento[2].UFEmissor))
                {
                    dropUFEmissor2.Value = collDocumento[2].UFEmissor;
                }
                else
                {
                    dropUFEmissor2.SelectedIndex = -1;
                }
                #endregion
            }
            #endregion
            e.Collection = coll;
        }
        else
        {
            e.Collection = new PessoaEnderecoCollection();
        }
    }

    protected void EsDSTemplateCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TemplateCarteiraCollection coll = new TemplateCarteiraCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTemplateCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TemplateCotistaCollection coll = new TemplateCotistaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilInvestidorCollection coll = new PerfilInvestidorCollection();
        coll.Query.OrderBy(coll.Query.IdPerfilInvestidor.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSDivisaoCnae_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        DivisaoCnaeCollection coll = new DivisaoCnaeCollection();

        if (integraCotistaItau)
        {
            coll.Query.OrderBy(coll.Query.CdDivisaoCnae.Ascending);
            coll.LoadAll();
        }

        e.Collection = coll;
    }

    protected void EsDSGrupoCnae_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoCnaeCollection coll = new GrupoCnaeCollection();

        if (integraCotistaItau && Session["cnaeDivisao"] != null)
        {
            GrupoCnaeQuery grupoCnaeQuery = new GrupoCnaeQuery();
            grupoCnaeQuery.Where(grupoCnaeQuery.CdDivisao == Session["cnaeDivisao"]);
            grupoCnaeQuery.OrderBy(grupoCnaeQuery.CdGrupo.Ascending);
            coll.Load(grupoCnaeQuery);
        }

        e.Collection = coll;
    }

    protected void EsDSClasseCnae_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClasseCnaeCollection coll = new ClasseCnaeCollection();

        if (integraCotistaItau && Session["cnaeGrupo"] != null)
        {
            ClasseCnaeQuery classeCnaeQuery = new ClasseCnaeQuery();
            classeCnaeQuery.Where(classeCnaeQuery.CdDivisao == Session["cnaeDivisao"],
                                    classeCnaeQuery.CdGrupo == Session["cnaeGrupo"]);
            classeCnaeQuery.OrderBy(classeCnaeQuery.CdClasse.Ascending);
            coll.Load(classeCnaeQuery);
        }

        e.Collection = coll;
    }

    protected void EsDSSubClasseCnae_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SubClasseCnaeCollection coll = new SubClasseCnaeCollection();

        if (integraCotistaItau && Session["cnaeClasse"] != null)
        {
            SubClasseCnaeQuery subClasseCnaeQuery = new SubClasseCnaeQuery();
            subClasseCnaeQuery.Where(subClasseCnaeQuery.CdDivisao == Session["cnaeDivisao"],
                                    subClasseCnaeQuery.CdGrupo == Session["cnaeGrupo"],
                                    subClasseCnaeQuery.CdClasse == Session["cnaeClasse"]);
            subClasseCnaeQuery.OrderBy(subClasseCnaeQuery.CdSubClasse.Ascending);
            coll.Load(subClasseCnaeQuery);
        }

        e.Collection = coll;
    }

    protected void EsDSTipoDispensa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityTipoDispensaCollection coll = new SuitabilityTipoDispensaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSuitabilityPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();
        coll.Query.Where(coll.Query.Nivel > 0);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaSuitabilityHistoricoCollection coll = new PessoaSuitabilityHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSValidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityValidacaoCollection coll = new SuitabilityValidacaoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }


    #endregion

    #region Controla Visibilidade de campos
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void chkTemplate_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxCheckBox).Visible = false;
        }
    }

    protected void labelTemplate_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
    }
    #endregion

    protected void gridCadastro_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idPessoa = (int)e.Keys[0];

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxTextBox textApelido = pageControl.FindControl("textApelido") as ASPxTextBox;
        TextBox textNome = pageControl.FindControl("textNome") as TextBox;
        ASPxTextBox textCPFCNPJ = pageControl.FindControl("textCPFCNPJ") as ASPxTextBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropEstadoCivil = pageControl.FindControl("dropEstadoCivil") as ASPxComboBox;
        ASPxComboBox dropSexo = pageControl.FindControl("dropSexo") as ASPxComboBox;
        TextBox textProfissao = pageControl.FindControl("textProfissao") as TextBox;
        ASPxComboBox dropNacionalidade = pageControl.FindControl("dropNacionalidade") as ASPxComboBox;
        ASPxDateEdit textDataNascimento = pageControl.FindControl("textDataNascimento") as ASPxDateEdit;
        ASPxDateEdit textDataVencimentoCadastro = pageControl.FindControl("textDataVencimentoCadastro") as ASPxDateEdit;
        TextBox textCodigoInterface = pageControl.FindControl("textCodigoInterface") as TextBox;
        ASPxComboBox dropAlertaCadastro = pageControl.FindControl("dropAlertaCadastro") as ASPxComboBox;
        TextBox textEmailAlerta = pageControl.FindControl("textEmailAlerta") as TextBox;
        //
        ASPxComboBox dropPessoaPoliticamenteExposta = pageControl.FindControl("dropPessoaPoliticamenteExposta") as ASPxComboBox;
        ASPxComboBox dropUFNaturalidade = pageControl.FindControl("estado") as ASPxComboBox;
        //
        TextBox textFiliacaoNomePai = pageControl.FindControl("filiacaoNomePai") as TextBox;
        TextBox textFiliacaoNomeMae = pageControl.FindControl("filiacaoNomeMae") as TextBox;
        //
        ASPxComboBox dropSituacaoLegal = pageControl.FindControl("dropSituacaoLegal") as ASPxComboBox;
        ASPxComboBox dropPessoaVinculada = pageControl.FindControl("dropPessoaVinculada") as ASPxComboBox;
        ASPxComboBox dropPerfilInvestidor = pageControl.FindControl("dropPerfilInvestidor") as ASPxComboBox;
        //
        ASPxComboBox dropNaturezaJuridica = pageControl.FindControl("dropNaturezaJuridica") as ASPxComboBox;
        ASPxComboBox dropTipoRenda = pageControl.FindControl("dropTipoRenda") as ASPxComboBox;
        ASPxComboBox dropIndicadorFatca = pageControl.FindControl("dropIndicadorFatca") as ASPxComboBox;
        TextBox textGiinTin = pageControl.FindControl("textGiinTin") as TextBox;
        ASPxComboBox dropJustificativaGiin = pageControl.FindControl("dropJustificativaGiin") as ASPxComboBox;
        ASPxComboBox dropCnaeDivisao = pageControl.FindControl("dropCnaeDivisao") as ASPxComboBox;
        ASPxComboBox dropCnaeGrupo = pageControl.FindControl("dropCnaeGrupo") as ASPxComboBox;
        ASPxComboBox dropCnaeClasse = pageControl.FindControl("dropCnaeClasse") as ASPxComboBox;
        ASPxComboBox dropCnaeSubClasse = pageControl.FindControl("dropCnaeSubClasse") as ASPxComboBox;
        ASPxCheckBox chkOffShore = pageControl.FindControl("chkOffShore") as ASPxCheckBox;

        //
        Pessoa pessoa = new Pessoa();
        string nomeOriginal = string.Empty;
        string documentoOriginal = string.Empty;

        if (pessoa.LoadByPrimaryKey(idPessoa))
        {
            nomeOriginal = pessoa.Nome;
            documentoOriginal = pessoa.Cpfcnpj;

            #region Salva Pessoa
            pessoa.Apelido = textApelido.Text.ToString();
            pessoa.Nome = textNome.Text.ToString();

            if (textCPFCNPJ.Text != "" && dropTipo.SelectedIndex != -1)
            {
                if (!string.IsNullOrEmpty(textCPFCNPJ.Text))
                    pessoa.Cpfcnpj = textCPFCNPJ.Text.ToString().Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");
                else
                    pessoa.Cpfcnpj = null;

                pessoa.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            }
            else
            {
                pessoa.Cpfcnpj = null;
                pessoa.Tipo = null;
            }

            if (dropEstadoCivil.SelectedIndex != -1)
            {
                pessoa.EstadoCivil = Convert.ToByte(dropEstadoCivil.SelectedItem.Value);
            }
            else
            {
                pessoa.EstadoCivil = null;
            }

            if (dropSexo.SelectedIndex != -1)
            {
                pessoa.Sexo = dropSexo.SelectedItem.Value.ToString();
            }

            pessoa.Profissao = textProfissao.Text.ToString();

            if (dropNacionalidade.SelectedIndex != -1)
            {
                pessoa.Nacionalidade = Convert.ToInt16(dropNacionalidade.SelectedItem.Value);
            }
            else
            {
                pessoa.Nacionalidade = null;
            }

            if (dropAlertaCadastro.SelectedIndex != -1)
            {
                pessoa.AlertaCadastro = dropAlertaCadastro.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.AlertaCadastro = "N";
            }

            pessoa.CodigoInterface = textCodigoInterface.Text.ToString();
            pessoa.EmailAlerta = textEmailAlerta.Text.ToString();

            if (textDataNascimento.Text != "")
            {
                pessoa.DataNascimento = Convert.ToDateTime(textDataNascimento.Text);
            }

            if (textDataVencimentoCadastro.Text != "")
            {
                pessoa.DataVencimentoCadastro = Convert.ToDateTime(textDataVencimentoCadastro.Text);
            }

            pessoa.DataUltimaAlteracao = DateTime.Now;

            if (dropPessoaPoliticamenteExposta.SelectedIndex >= 0)
            {
                pessoa.PessoaPoliticamenteExposta = dropPessoaPoliticamenteExposta.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.PessoaPoliticamenteExposta = null;
            }

            if (dropUFNaturalidade.SelectedIndex >= 0)
            {
                pessoa.UFNaturalidade = dropUFNaturalidade.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.UFNaturalidade = null;
            }

            if (dropSituacaoLegal.SelectedIndex >= 0)
            {
                pessoa.SituacaoLegal = Convert.ToByte(dropSituacaoLegal.SelectedItem.Value);
            }
            else
            {
                pessoa.SituacaoLegal = null;
            }

            if (dropPessoaVinculada.SelectedIndex >= 0)
            {
                pessoa.PessoaVinculada = Convert.ToString(dropPessoaVinculada.SelectedItem.Value);
            }
            else
            {
                pessoa.PessoaVinculada = null;
            }

            if (dropPerfilInvestidor.SelectedIndex >= 0)
            {
                pessoa.IdPerfilInvestidor = Convert.ToByte(dropPerfilInvestidor.SelectedItem.Value);
            }
            else
            {
                if (pessoa.IdPerfilInvestidor != null)
                {
                    SuitabilityRespostaCollection suitabilityRespostaCollection = new SuitabilityRespostaCollection();

                    suitabilityRespostaCollection.Query.Where(suitabilityRespostaCollection.Query.IdPessoa == idPessoa);
                    suitabilityRespostaCollection.Query.Load();

                    suitabilityRespostaCollection.MarkAllAsDeleted();
                    suitabilityRespostaCollection.Save();
                }

                pessoa.IdPerfilInvestidor = null;

            }

            pessoa.FiliacaoNomePai = textFiliacaoNomePai.Text.ToString().Trim();
            pessoa.FiliacaoNomeMae = textFiliacaoNomeMae.Text.ToString().Trim();

            if (dropNaturezaJuridica.SelectedIndex >= 0)
            {
                pessoa.NaturezaJuridica = Convert.ToInt16(dropNaturezaJuridica.SelectedItem.Value);
            }
            else
            {
                pessoa.NaturezaJuridica = null;
            }

            if (dropTipoRenda.SelectedIndex >= 0)
            {
                pessoa.TipoRenda = dropTipoRenda.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.TipoRenda = null;
            }

            if (dropIndicadorFatca.SelectedIndex >= 0)
            {
                pessoa.IndicadorFatca = dropIndicadorFatca.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.IndicadorFatca = null;
            }

            if (textGiinTin.Text != "")
            {
                pessoa.GiinTin = textGiinTin.Text;
            }
            else
            {
                pessoa.GiinTin = null;
            }

            if (dropJustificativaGiin.SelectedIndex >= 0)
            {
                pessoa.JustificativaGiin = dropJustificativaGiin.SelectedItem.Value.ToString();
            }
            else
            {
                pessoa.JustificativaGiin = null;
            }

            if (dropCnaeDivisao.SelectedIndex >= 0)
            {
                pessoa.CnaeDivisao = Convert.ToByte(dropCnaeDivisao.SelectedItem.Value);
            }
            else
            {
                pessoa.CnaeDivisao = null;
            }

            if (dropCnaeGrupo.SelectedIndex >= 0)
            {
                pessoa.CnaeGrupo = Convert.ToByte(dropCnaeGrupo.SelectedItem.Value);
            }
            else
            {
                pessoa.CnaeGrupo = null;
            }

            if (dropCnaeClasse.SelectedIndex >= 0)
            {
                pessoa.CnaeClasse = Convert.ToByte(dropCnaeClasse.SelectedItem.Value);
            }
            else
            {
                pessoa.CnaeClasse = null;
            }

            if (dropCnaeSubClasse.SelectedIndex >= 0)
            {
                pessoa.CnaeSubClasse = Convert.ToByte(dropCnaeSubClasse.SelectedItem.Value);
            }
            else
            {
                pessoa.CnaeSubClasse = null;
            }


            if (chkOffShore.Checked)
            {
                pessoa.OffShore = "S";
                pessoa.Tipo = null;
                pessoa.Cpfcnpj = null;
            }
            else
                pessoa.OffShore = "N";

            pessoa.Save();

            #endregion

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Pessoa - Operacao: Update Pessoa: " + idPessoa + UtilitarioWeb.ToString(pessoa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Cadastro de Enderecos 1
            TextBox textEndereco = pageControl.FindControl("textEndereco") as TextBox;
            TextBox textNumero = pageControl.FindControl("textNumero") as TextBox;
            TextBox textComplemento = pageControl.FindControl("textComplemento") as TextBox;
            TextBox textBairro = pageControl.FindControl("textBairro") as TextBox;
            TextBox textCidade = pageControl.FindControl("textCidade") as TextBox;
            TextBox textCEP = pageControl.FindControl("textCEP") as TextBox;
            TextBox textUF = pageControl.FindControl("textUF") as TextBox;
            TextBox textPais = pageControl.FindControl("textPais") as TextBox;
            ASPxComboBox dropRecebeCorrespondencia = pageControl.FindControl("dropRecebeCorrespondencia") as ASPxComboBox;

            //
            ASPxComboBox dropTipoEndereco = pageControl.FindControl("dropTipoEndereco") as ASPxComboBox;

            HiddenField hiddenIdEndereco1 = pageControl.FindControl("hiddenIdEndereco1") as HiddenField;

            if (textEndereco.Text != "" || textNumero.Text != "" || textBairro.Text != "" || textCidade.Text != "")
            {
                PessoaEndereco pessoaEndereco = new PessoaEndereco();
                if (hiddenIdEndereco1.Value != "")
                {
                    //Se tem o hidden, então carrego o objeto de endereço!
                    pessoaEndereco.LoadByPrimaryKey(Convert.ToInt32(hiddenIdEndereco1.Value), idPessoa);
                }
                //Preenche os dados, seja para insert ou para update (em caso de ter consultado antes)
                pessoaEndereco.IdPessoa = idPessoa;
                pessoaEndereco.Endereco = textEndereco.Text.ToString();
                pessoaEndereco.Numero = textNumero.Text.ToString();
                pessoaEndereco.Complemento = textComplemento.Text.ToString();
                pessoaEndereco.Bairro = textBairro.Text.ToString();
                pessoaEndereco.Cidade = textCidade.Text.ToString();
                pessoaEndereco.Cep = textCEP.Text.ToString();
                pessoaEndereco.Uf = textUF.Text.ToString();
                pessoaEndereco.Pais = textPais.Text.ToString();

                pessoaEndereco.RecebeCorrespondencia = dropRecebeCorrespondencia.SelectedIndex == -1
                                            ? "N" : Convert.ToString(dropRecebeCorrespondencia.SelectedItem.Value);
                //
                if (dropTipoEndereco.SelectedIndex >= 0)
                {
                    pessoaEndereco.TipoEndereco = Convert.ToString(dropTipoEndereco.SelectedItem.Value);
                }
                else
                {
                    pessoaEndereco.TipoEndereco = null;
                }
                //
                pessoaEndereco.Save();
            }
            #endregion

            #region Cadastro de Enderecos 2
            TextBox textEndereco2 = pageControl.FindControl("textEndereco2") as TextBox;
            TextBox textNumero2 = pageControl.FindControl("textNumero2") as TextBox;
            TextBox textComplemento2 = pageControl.FindControl("textComplemento2") as TextBox;
            TextBox textBairro2 = pageControl.FindControl("textBairro2") as TextBox;
            TextBox textCidade2 = pageControl.FindControl("textCidade2") as TextBox;
            TextBox textCEP2 = pageControl.FindControl("textCEP2") as TextBox;
            TextBox textUF2 = pageControl.FindControl("textUF2") as TextBox;
            TextBox textPais2 = pageControl.FindControl("textPais2") as TextBox;
            ASPxComboBox dropRecebeCorrespondencia2 = pageControl.FindControl("dropRecebeCorrespondencia2") as ASPxComboBox;
            //
            ASPxComboBox dropTipoEndereco2 = pageControl.FindControl("dropTipoEndereco2") as ASPxComboBox;

            HiddenField hiddenIdEndereco2 = pageControl.FindControl("hiddenIdEndereco2") as HiddenField;

            if (textEndereco2.Text != "" || textNumero2.Text != "" || textBairro2.Text != "" || textCidade2.Text != "")
            {
                PessoaEndereco pessoaEndereco = new PessoaEndereco();
                if (hiddenIdEndereco2.Value != "")
                {
                    //Se tem o hidden, então carrego o objeto de endereço!
                    pessoaEndereco.LoadByPrimaryKey(Convert.ToInt32(hiddenIdEndereco2.Value), idPessoa);
                }
                //Preenche os dados, seja para insert ou para update (em caso de ter consultado antes)
                pessoaEndereco.IdPessoa = idPessoa;
                pessoaEndereco.Endereco = textEndereco2.Text.ToString();
                pessoaEndereco.Numero = textNumero2.Text.ToString();
                pessoaEndereco.Complemento = textComplemento2.Text.ToString();
                pessoaEndereco.Bairro = textBairro2.Text.ToString();
                pessoaEndereco.Cidade = textCidade2.Text.ToString();
                pessoaEndereco.Cep = textCEP2.Text.ToString();
                pessoaEndereco.Uf = textUF2.Text.ToString();
                pessoaEndereco.Pais = textPais2.Text.ToString();

                pessoaEndereco.RecebeCorrespondencia = dropRecebeCorrespondencia2.SelectedIndex == -1
                            ? "N" : Convert.ToString(dropRecebeCorrespondencia2.SelectedItem.Value);

                //
                if (dropTipoEndereco2.SelectedIndex >= 0)
                {
                    pessoaEndereco.TipoEndereco = Convert.ToString(dropTipoEndereco2.SelectedItem.Value);
                }
                else
                {
                    pessoaEndereco.TipoEndereco = null;
                }

                pessoaEndereco.Save();
            }
            #endregion

            #region Cadastro de Telefone 1
            HiddenField hiddenIdTelefone1 = pageControl.FindControl("hiddenTelefone1") as HiddenField;
            //
            ASPxTextBox txtDDI = pageControl.FindControl("txtDDI") as ASPxTextBox;
            ASPxTextBox txtDDD = pageControl.FindControl("txtDDD") as ASPxTextBox;
            ASPxTextBox txtNumero = pageControl.FindControl("txtNumero") as ASPxTextBox;
            ASPxTextBox txtRamal = pageControl.FindControl("txtRamal") as ASPxTextBox;
            //
            ASPxComboBox dropTipoTelefone = pageControl.FindControl("dropTipoTelefone") as ASPxComboBox;

            if (txtDDI.Text != "" || txtDDD.Text != "" || txtNumero.Text != "" || txtRamal.Text != "" || dropTipoTelefone.SelectedIndex >= 0)
            {
                PessoaTelefone pessoaTelefone = new PessoaTelefone();

                if (hiddenIdTelefone1.Value != "")
                {
                    pessoaTelefone.Query
                                  .Where(pessoaTelefone.Query.IdTelefone == Convert.ToInt32(hiddenIdTelefone1.Value),
                                         pessoaTelefone.Query.IdPessoa == idPessoa);

                    pessoaTelefone.Query.Load();
                }
                //Preenche os dados, seja para insert ou para update
                pessoaTelefone.IdPessoa = idPessoa;
                pessoaTelefone.Ddi = txtDDI.Text.ToString();
                pessoaTelefone.Ddd = txtDDD.Text.ToString();
                pessoaTelefone.Numero = txtNumero.Text.ToString();
                pessoaTelefone.Ramal = txtRamal.Text.ToString();
                //
                pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone.SelectedItem.Value);
                //
                pessoaTelefone.Save();
            }
            else
            {
                if (hiddenIdTelefone1.Value != "")
                {
                    PessoaTelefone pessoaTelefone = new PessoaTelefone();
                    if (pessoaTelefone.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTelefone1.Value)))
                    {
                        pessoaTelefone.MarkAsDeleted();
                        pessoaTelefone.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Telefone 2
            HiddenField hiddenIdTelefone2 = pageControl.FindControl("hiddenTelefone2") as HiddenField;
            //
            ASPxTextBox txtDDI1 = pageControl.FindControl("txtDDI1") as ASPxTextBox;
            ASPxTextBox txtDDD1 = pageControl.FindControl("txtDDD1") as ASPxTextBox;
            ASPxTextBox txtNumero1 = pageControl.FindControl("txtNumero1") as ASPxTextBox;
            ASPxTextBox txtRamal1 = pageControl.FindControl("txtRamal1") as ASPxTextBox;
            //
            ASPxComboBox dropTipoTelefone1 = pageControl.FindControl("dropTipoTelefone1") as ASPxComboBox;

            if (txtDDI1.Text != "" || txtDDD1.Text != "" || txtNumero1.Text != "" || txtRamal1.Text != "" || dropTipoTelefone1.SelectedIndex >= 0)
            {
                PessoaTelefone pessoaTelefone = new PessoaTelefone();

                if (hiddenIdTelefone2.Value != "")
                {
                    pessoaTelefone.Query
                                  .Where(pessoaTelefone.Query.IdTelefone == Convert.ToInt32(hiddenIdTelefone2.Value),
                                         pessoaTelefone.Query.IdPessoa == idPessoa);

                    pessoaTelefone.Query.Load();
                }

                //Preenche os dados, seja para insert ou para update
                pessoaTelefone.IdPessoa = idPessoa;
                pessoaTelefone.Ddi = txtDDI1.Text.ToString();
                pessoaTelefone.Ddd = txtDDD1.Text.ToString();
                pessoaTelefone.Numero = txtNumero1.Text.ToString();
                pessoaTelefone.Ramal = txtRamal1.Text.ToString();
                //
                pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone1.SelectedItem.Value);
                //
                pessoaTelefone.Save();
            }
            else
            {
                if (hiddenIdTelefone2.Value != "")
                {
                    PessoaTelefone pessoaTelefone = new PessoaTelefone();
                    if (pessoaTelefone.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTelefone2.Value)))
                    {
                        pessoaTelefone.MarkAsDeleted();
                        pessoaTelefone.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Telefone 3
            HiddenField hiddenIdTelefone3 = pageControl.FindControl("hiddenTelefone3") as HiddenField;
            //
            ASPxTextBox txtDDI2 = pageControl.FindControl("txtDDI2") as ASPxTextBox;
            ASPxTextBox txtDDD2 = pageControl.FindControl("txtDDD2") as ASPxTextBox;
            ASPxTextBox txtNumero2 = pageControl.FindControl("txtNumero2") as ASPxTextBox;
            ASPxTextBox txtRamal2 = pageControl.FindControl("txtRamal2") as ASPxTextBox;
            //
            ASPxComboBox dropTipoTelefone2 = pageControl.FindControl("dropTipoTelefone2") as ASPxComboBox;

            if (txtDDI2.Text != "" || txtDDD2.Text != "" || txtNumero2.Text != "" || txtRamal2.Text != "" || dropTipoTelefone2.SelectedIndex >= 0)
            {
                PessoaTelefone pessoaTelefone = new PessoaTelefone();

                if (hiddenIdTelefone3.Value != "")
                {
                    pessoaTelefone.Query
                                  .Where(pessoaTelefone.Query.IdTelefone == Convert.ToInt32(hiddenIdTelefone3.Value),
                                         pessoaTelefone.Query.IdPessoa == idPessoa);

                    pessoaTelefone.Query.Load();
                }
                //Preenche os dados, seja para insert ou para update
                pessoaTelefone.IdPessoa = idPessoa;
                pessoaTelefone.Ddi = txtDDI2.Text.ToString();
                pessoaTelefone.Ddd = txtDDD2.Text.ToString();
                pessoaTelefone.Numero = txtNumero2.Text.ToString();
                pessoaTelefone.Ramal = txtRamal2.Text.ToString();
                //
                pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone2.SelectedItem.Value);
                //
                pessoaTelefone.Save();
            }
            else
            {
                if (hiddenIdTelefone3.Value != "")
                {
                    PessoaTelefone pessoaTelefone = new PessoaTelefone();
                    if (pessoaTelefone.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTelefone3.Value)))
                    {
                        pessoaTelefone.MarkAsDeleted();
                        pessoaTelefone.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Email1
            HiddenField hiddenEmail1 = pageControl.FindControl("hiddenEmail1") as HiddenField;
            //
            TextBox txtEmail1 = pageControl.FindControl("txtEmail1") as TextBox;
            //            
            if (txtEmail1.Text != "")
            {
                PessoaEmail pessoaEmail = new PessoaEmail();

                if (hiddenEmail1.Value != "")
                {
                    pessoaEmail.Query
                                  .Where(pessoaEmail.Query.IdEmail == Convert.ToInt32(hiddenEmail1.Value),
                                         pessoaEmail.Query.IdPessoa == idPessoa);

                    pessoaEmail.Query.Load();
                }
                //Preenche os dados, seja para insert ou para update
                pessoaEmail.IdPessoa = idPessoa;
                pessoaEmail.Email = txtEmail1.Text.ToString();
                //
                pessoaEmail.Save();
            }
            else
            {
                if (hiddenEmail1.Value != "")
                {
                    PessoaEmail pessoaEmail = new PessoaEmail();
                    if (pessoaEmail.LoadByPrimaryKey(Convert.ToInt32(hiddenEmail1.Value)))
                    {
                        pessoaEmail.MarkAsDeleted();
                        pessoaEmail.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Email2
            HiddenField hiddenEmail2 = pageControl.FindControl("hiddenEmail2") as HiddenField;
            //
            TextBox txtEmail2 = pageControl.FindControl("txtEmail2") as TextBox;
            //            
            if (txtEmail2.Text != "")
            {
                PessoaEmail pessoaEmail = new PessoaEmail();

                if (hiddenEmail2.Value != "")
                {
                    pessoaEmail.Query
                                  .Where(pessoaEmail.Query.IdEmail == Convert.ToInt32(hiddenEmail2.Value),
                                         pessoaEmail.Query.IdPessoa == idPessoa);

                    pessoaEmail.Query.Load();
                }
                //Preenche os dados, seja para insert ou para update
                pessoaEmail.IdPessoa = idPessoa;
                pessoaEmail.Email = txtEmail2.Text.ToString();
                //
                pessoaEmail.Save();
            }
            else
            {
                if (hiddenEmail2.Value != "")
                {
                    PessoaEmail pessoaEmail = new PessoaEmail();
                    if (pessoaEmail.LoadByPrimaryKey(Convert.ToInt32(hiddenEmail2.Value)))
                    {
                        pessoaEmail.MarkAsDeleted();
                        pessoaEmail.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Documento 1
            HiddenField hiddenIdDocumento1 = pageControl.FindControl("hiddenDocumento1") as HiddenField;
            //
            ASPxComboBox dropTipoDocumento = pageControl.FindControl("dropTipoDocumento") as ASPxComboBox;
            ASPxComboBox dropOrgaoEmissor = pageControl.FindControl("dropOrgaoEmissor") as ASPxComboBox;
            ASPxComboBox dropUFEmissor = pageControl.FindControl("dropUFEmissor") as ASPxComboBox;
            //
            ASPxTextBox txtNumDocumento = pageControl.FindControl("txtNumDocumento") as ASPxTextBox;
            ASPxDateEdit textDataExpedicao = pageControl.FindControl("textDataExpedicao") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;

            if (dropTipoDocumento.SelectedIndex >= 0 || dropOrgaoEmissor.SelectedIndex >= 0 || dropUFEmissor.SelectedIndex >= 0 ||
                txtNumDocumento.Text != "" || textDataExpedicao.Text != "")
            {
                PessoaDocumento pessoaDocumento = new PessoaDocumento();

                if (hiddenIdDocumento1.Value != "")
                {
                    pessoaDocumento.Query
                                  .Where(pessoaDocumento.Query.IdDocumento == Convert.ToInt32(hiddenIdDocumento1.Value),
                                         pessoaDocumento.Query.IdPessoa == idPessoa);

                    pessoaDocumento.Query.Load();
                }

                //Preenche os dados, seja para insert ou para update
                if (txtNumDocumento.Text != "")
                { // Se tem numero Documento
                    pessoaDocumento.IdPessoa = idPessoa;

                    pessoaDocumento.NumeroDocumento = txtNumDocumento.Text.ToString();

                    pessoaDocumento.TipoDocumento = null;
                    pessoaDocumento.DataExpedicao = null;
                    pessoaDocumento.DataVencimento = null;
                    pessoaDocumento.OrgaoEmissor = null;
                    pessoaDocumento.UFEmissor = null;

                    if (dropTipoDocumento.SelectedIndex >= 0)
                    {
                        pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento.SelectedItem.Value);
                    }

                    if (textDataExpedicao.Text != "")
                    {
                        pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao.Text);
                    }

                    if (textDataVencimento.Text != "")
                    {
                        pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                    }

                    if (dropOrgaoEmissor.SelectedIndex >= 0)
                    {
                        pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor.SelectedItem.Text;
                    }

                    if (dropUFEmissor.SelectedIndex >= 0)
                    {
                        pessoaDocumento.UFEmissor = dropUFEmissor.SelectedItem.Text;
                    }
                    //
                    pessoaDocumento.Save();
                }
            }
            else
            {
                if (hiddenIdDocumento1.Value != "")
                {
                    PessoaDocumento pessoaDocumento = new PessoaDocumento();
                    if (pessoaDocumento.LoadByPrimaryKey(Convert.ToInt32(hiddenIdDocumento1.Value)))
                    {
                        pessoaDocumento.MarkAsDeleted();
                        pessoaDocumento.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Documento 2
            HiddenField hiddenIdDocumento2 = pageControl.FindControl("hiddenDocumento2") as HiddenField;
            //
            ASPxComboBox dropTipoDocumento1 = pageControl.FindControl("dropTipoDocumento1") as ASPxComboBox;
            ASPxComboBox dropOrgaoEmissor1 = pageControl.FindControl("dropOrgaoEmissor1") as ASPxComboBox;
            ASPxComboBox dropUFEmissor1 = pageControl.FindControl("dropUFEmissor1") as ASPxComboBox;
            //
            ASPxTextBox txtNumDocumento1 = pageControl.FindControl("txtNumDocumento1") as ASPxTextBox;
            ASPxDateEdit textDataExpedicao1 = pageControl.FindControl("textDataExpedicao1") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento1 = pageControl.FindControl("textDataVencimento1") as ASPxDateEdit;

            if (dropTipoDocumento1.SelectedIndex >= 0 || dropOrgaoEmissor1.SelectedIndex >= 0 || dropUFEmissor1.SelectedIndex >= 0 ||
                txtNumDocumento1.Text != "" || textDataExpedicao1.Text != "")
            {
                PessoaDocumento pessoaDocumento = new PessoaDocumento();

                if (hiddenIdDocumento2.Value != "")
                {
                    pessoaDocumento.Query
                                  .Where(pessoaDocumento.Query.IdDocumento == Convert.ToInt32(hiddenIdDocumento2.Value),
                                         pessoaDocumento.Query.IdPessoa == idPessoa);

                    pessoaDocumento.Query.Load();
                }

                //Preenche os dados, seja para insert ou para update
                if (txtNumDocumento1.Text != "")
                { // Se tem numero Documento
                    pessoaDocumento.IdPessoa = idPessoa;

                    pessoaDocumento.NumeroDocumento = txtNumDocumento1.Text.ToString();

                    pessoaDocumento.TipoDocumento = null;
                    pessoaDocumento.DataExpedicao = null;
                    pessoaDocumento.DataVencimento = null;
                    pessoaDocumento.OrgaoEmissor = null;
                    pessoaDocumento.UFEmissor = null;

                    if (dropTipoDocumento1.SelectedIndex >= 0)
                    {
                        pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento1.SelectedItem.Value);
                    }

                    if (textDataExpedicao1.Text != "")
                    {
                        pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao1.Text);
                    }

                    if (textDataVencimento1.Text != "")
                    {
                        pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento1.Text);
                    }

                    if (dropOrgaoEmissor1.SelectedIndex >= 0)
                    {
                        pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor1.SelectedItem.Text;
                    }

                    if (dropUFEmissor1.SelectedIndex >= 0)
                    {
                        pessoaDocumento.UFEmissor = dropUFEmissor1.SelectedItem.Text;
                    }
                    //
                    pessoaDocumento.Save();
                }
            }
            else
            {
                if (hiddenIdDocumento2.Value != "")
                {
                    PessoaDocumento pessoaDocumento = new PessoaDocumento();
                    if (pessoaDocumento.LoadByPrimaryKey(Convert.ToInt32(hiddenIdDocumento2.Value)))
                    {
                        pessoaDocumento.MarkAsDeleted();
                        pessoaDocumento.Save();
                    }
                }
            }
            #endregion

            #region Cadastro de Documento 3
            HiddenField hiddenIdDocumento3 = pageControl.FindControl("hiddenDocumento3") as HiddenField;
            //
            ASPxComboBox dropTipoDocumento2 = pageControl.FindControl("dropTipoDocumento2") as ASPxComboBox;
            ASPxComboBox dropOrgaoEmissor2 = pageControl.FindControl("dropOrgaoEmissor2") as ASPxComboBox;
            ASPxComboBox dropUFEmissor2 = pageControl.FindControl("dropUFEmissor2") as ASPxComboBox;
            //
            ASPxTextBox txtNumDocumento2 = pageControl.FindControl("txtNumDocumento2") as ASPxTextBox;
            ASPxDateEdit textDataExpedicao2 = pageControl.FindControl("textDataExpedicao2") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento2 = pageControl.FindControl("textDataVencimento2") as ASPxDateEdit;

            if (dropTipoDocumento2.SelectedIndex >= 0 || dropOrgaoEmissor2.SelectedIndex >= 0 || dropUFEmissor2.SelectedIndex >= 0 ||
                txtNumDocumento2.Text != "" || textDataExpedicao2.Text != "")
            {
                PessoaDocumento pessoaDocumento = new PessoaDocumento();

                if (hiddenIdDocumento3.Value != "")
                {
                    pessoaDocumento.Query
                                  .Where(pessoaDocumento.Query.IdDocumento == Convert.ToInt32(hiddenIdDocumento3.Value),
                                         pessoaDocumento.Query.IdPessoa == idPessoa);

                    pessoaDocumento.Query.Load();
                }

                //Preenche os dados, seja para insert ou para update
                if (txtNumDocumento2.Text != "")
                { // Se tem numero Documento
                    pessoaDocumento.IdPessoa = idPessoa;

                    pessoaDocumento.NumeroDocumento = txtNumDocumento2.Text.ToString();

                    pessoaDocumento.TipoDocumento = null;
                    pessoaDocumento.DataExpedicao = null;
                    pessoaDocumento.DataVencimento = null;
                    pessoaDocumento.OrgaoEmissor = null;
                    pessoaDocumento.UFEmissor = null;

                    if (dropTipoDocumento2.SelectedIndex >= 0)
                    {
                        pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento2.SelectedItem.Value);
                    }

                    if (textDataExpedicao2.Text != "")
                    {
                        pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao2.Text);
                    }

                    if (textDataVencimento2.Text != "")
                    {
                        pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento2.Text);
                    }

                    if (dropOrgaoEmissor2.SelectedIndex >= 0)
                    {
                        pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor2.SelectedItem.Text;
                    }

                    if (dropUFEmissor2.SelectedIndex >= 0)
                    {
                        pessoaDocumento.UFEmissor = dropUFEmissor2.SelectedItem.Text;
                    }
                    //
                    pessoaDocumento.Save();
                }
            }
            else
            {
                if (hiddenIdDocumento3.Value != "")
                {
                    PessoaDocumento pessoaDocumento = new PessoaDocumento();
                    if (pessoaDocumento.LoadByPrimaryKey(Convert.ToInt32(hiddenIdDocumento3.Value)))
                    {
                        pessoaDocumento.MarkAsDeleted();
                        pessoaDocumento.Save();
                    }
                }
            }
            #endregion

        }

        bool nomeAlterado = !nomeOriginal.Equals(pessoa.Nome);
        bool documentoAlterado = !documentoOriginal.Equals(pessoa.Cpfcnpj);
        string agentesAlterados = string.Empty;
        string clientesAlterados = string.Empty;
        string emissoresAlterados = string.Empty;
        string bancosAlterados = string.Empty;
        string diretoresAlterados = string.Empty;
        string cotistasAlterados = string.Empty;

        if (nomeAlterado || documentoAlterado)
        {
            AgenteMercadoCollection agenteMercadoColl = new AgenteMercadoCollection();
            agenteMercadoColl.Query.Where(agenteMercadoColl.Query.IdPessoa.Equal(idPessoa));

            #region Agente de Mercado
            if (agenteMercadoColl.Query.Load())
            {
                foreach (AgenteMercado agenteMercado in agenteMercadoColl)
                {
                    agentesAlterados += agenteMercado.IdAgente.Value + ",";

                    if (propagaAlterNomeAgente)
                    {
                        agenteMercado.Nome = pessoa.Nome;
                        agenteMercado.Cnpj = pessoa.Cpfcnpj;
                    }

                }

                agentesAlterados = agentesAlterados.Remove(agentesAlterados.Length - 1);
                agenteMercadoColl.Save();
            }
            #endregion

            #region Banco
            BancoCollection bancoColl = new BancoCollection();
            bancoColl.Query.Where(bancoColl.Query.IdPessoa.Equal(idPessoa));

            if (bancoColl.Query.Load())
            {
                foreach (Banco banco in bancoColl)
                {
                    bancosAlterados += banco.IdBanco.Value + ",";
                    if (propagaAlterNomeAgente)
                    {
                        banco.Nome = pessoa.Nome;
                    }
                }

                bancosAlterados = bancosAlterados.Remove(bancosAlterados.Length - 1);
                bancoColl.Save();
            }
            #endregion

            #region Diretor
            DiretorCollection diretorColl = new DiretorCollection();
            diretorColl.Query.Where(diretorColl.Query.IdPessoa.Equal(idPessoa));

            if (diretorColl.Query.Load())
            {
                foreach (Diretor diretor in diretorColl)
                {
                    diretoresAlterados += diretor.IdDiretor.Value + ",";
                    if (propagaAlterNomeAgente)
                    {
                        diretor.Nome = pessoa.Nome;
                    }
                }

                diretoresAlterados = diretoresAlterados.Remove(diretoresAlterados.Length - 1);
                diretorColl.Save();

            }
            #endregion

            #region Emissor
            EmissorCollection emissorColl = new EmissorCollection();
            emissorColl.Query.Where(emissorColl.Query.IdPessoa.Equal(idPessoa));

            if (emissorColl.Query.Load())
            {
                foreach (Emissor emissor in emissorColl)
                {
                    emissoresAlterados += emissor.IdEmissor.Value + ",";
                    if (propagaAlterNomeAgente)
                    {
                        emissor.Nome = pessoa.Nome;
                        emissor.Cnpj = pessoa.Cpfcnpj;
                    }
                }

                emissoresAlterados = emissoresAlterados.Remove(emissoresAlterados.Length - 1);
                emissorColl.Save();
            }
            #endregion

            #region Cliente
            ClienteCollection clienteColl = new ClienteCollection();
            clienteColl.Query.Where(clienteColl.Query.IdPessoa.Equal(idPessoa));
            List<int> lstIdCliente = new List<int>(); //Usado para buscar carteira
            if (clienteColl.Query.Load())
            {
                foreach (Cliente cliente in clienteColl)
                {
                    clientesAlterados += cliente.IdCliente.Value + ",";
                    lstIdCliente.Add(cliente.IdCliente.Value);
                    if (propagaAlterNomeCliente)
                    {
                        cliente.Nome = pessoa.Nome;
                    }
                }

                clientesAlterados = clientesAlterados.Remove(clientesAlterados.Length - 1);
                clienteColl.Save();
            }
            #endregion

            #region Carteira
            if (lstIdCliente.Count > 0)
            {
                CarteiraCollection carteiraColl = new CarteiraCollection();
                carteiraColl.Query.Where(carteiraColl.Query.IdCarteira.In(lstIdCliente.ToArray()));

                if (carteiraColl.Query.Load())
                {
                    foreach (Carteira carteira in carteiraColl)
                    {
                        if (propagaAlterNomeCliente)
                        {
                            carteira.Nome = pessoa.Nome;
                        }
                    }

                    carteiraColl.Save();
                }
            }
            #endregion

            #region Cotista
            CotistaCollection cotistaColl = new CotistaCollection();
            cotistaColl.Query.Where(cotistaColl.Query.IdPessoa.Equal(idPessoa));

            if (cotistaColl.Query.Load())
            {
                foreach (Cotista cotista in cotistaColl)
                {
                    cotistasAlterados += cotista.IdCotista.Value + ",";
                    if (propagaAlterNomeCliente)
                    {
                        cotista.Nome = pessoa.Nome;
                    }
                }

                cotistasAlterados = cotistasAlterados.Remove(cotistasAlterados.Length - 1);
                cotistaColl.Save();
            }
            #endregion
        }

        #region Monta mensagem de Retorno
        StringBuilder mensagem = new StringBuilder();
        if (!string.IsNullOrEmpty(agentesAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!agentesAlterados.Contains(","))
                mensagem.Append("O agente de mercado ID-").Append(agentesAlterados);
            else
                mensagem.Append("Os agentes de mercado IDs-").Append(agentesAlterados);

            if (!propagaAlterNomeAgente)
                mensagem.Append(" não");

            if (!agentesAlterados.Contains(","))
                mensagem.Append(" teve nome e documento alterado! ");
            else
                mensagem.Append(" tiveram nomes e documentos alterados! ");
        }


        if (!string.IsNullOrEmpty(bancosAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!bancosAlterados.Contains(","))
                mensagem.Append("O banco ID-").Append(bancosAlterados);
            else
                mensagem.Append("Os Bancos IDs-").Append(bancosAlterados);

            if (!propagaAlterNomeAgente)
                mensagem.Append(" não");

            if (!bancosAlterados.Contains(","))
                mensagem.Append(" teve nome alterado! ");
            else
                mensagem.Append(" tiveram os nomes alterados! ");
        }

        if (!string.IsNullOrEmpty(diretoresAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!diretoresAlterados.Contains(","))
                mensagem.Append("O Diretor ID-").Append(diretoresAlterados);
            else
                mensagem.Append("Os Diretores IDs-").Append(diretoresAlterados);

            if (!propagaAlterNomeAgente)
                mensagem.Append(" não");

            if (!diretoresAlterados.Contains(","))
                mensagem.Append(" teve nome alterado! ");
            else
                mensagem.Append(" tiveram os nomes alterados! ");
        }


        if (!string.IsNullOrEmpty(emissoresAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!emissoresAlterados.Contains(","))
                mensagem.Append("O Emissor ID-").Append(emissoresAlterados);
            else
                mensagem.Append("Os Emissores IDs-").Append(emissoresAlterados);

            if (!propagaAlterNomeAgente)
                mensagem.Append(" não");

            if (!emissoresAlterados.Contains(","))
                mensagem.Append(" teve nome e documento alterado! ");
            else
                mensagem.Append(" tiveram nomes e documentos alterados! ");
        }

        if (!string.IsNullOrEmpty(clientesAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!clientesAlterados.Contains(","))
                mensagem.Append("O cliente/carteira ID-").Append(clientesAlterados);
            else
                mensagem.Append("Os clientes/carteiras IDs-").Append(clientesAlterados);

            if (!propagaAlterNomeCliente)
                mensagem.Append(" não");

            if (!clientesAlterados.Contains(","))
                mensagem.Append(" teve nome e documento alterado! ");
            else
                mensagem.Append(" tiveram nomes e documentos alterados! ");
        }

        if (!string.IsNullOrEmpty(cotistasAlterados))
        {
            if (mensagem.Length > 0)
                mensagem.Append("\n");

            if (!cotistasAlterados.Contains(","))
                mensagem.Append("O cotista ID-").Append(cotistasAlterados);
            else
                mensagem.Append("Os cotistas IDs-").Append(cotistasAlterados);

            if (!propagaAlterNomeCliente)
                mensagem.Append(" não");

            if (!cotistasAlterados.Contains(","))
                mensagem.Append(" teve nome e documento alterado! ");
            else
                mensagem.Append(" tiveram nomes e documentos alterados! ");
        }

        gridCadastro.JSProperties["cpMessage"] = mensagem.ToString();
        #endregion

	    #region tabPage - Cliente - Suitability
        ASPxComboBox dropPerfil = pageControl.FindControl("dropPerfil") as ASPxComboBox;
        ASPxComboBox dropNaoInformadoRecusa = pageControl.FindControl("dropNaoInformadoRecusa") as ASPxComboBox;
        ASPxComboBox dropDispensado = pageControl.FindControl("dropDispensado") as ASPxComboBox;
        ASPxComboBox dropTipoDispensa = pageControl.FindControl("dropTipoDispensa") as ASPxComboBox;
        ASPxComboBox dropValidacao = pageControl.FindControl("dropValidacao") as ASPxComboBox;

        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        pessoaSuitability.LoadByPrimaryKey(idPessoa);

        pessoaSuitability.IdPessoa = idPessoa;

        if (dropPerfil.SelectedIndex > -1)
            pessoaSuitability.Perfil = Convert.ToInt32(dropPerfil.SelectedItem.Value);

        if (dropValidacao.SelectedIndex > -1)
            pessoaSuitability.IdValidacao = Convert.ToInt32(dropValidacao.SelectedItem.Value);

        if (dropNaoInformadoRecusa.SelectedIndex > -1)
            pessoaSuitability.Recusa = Convert.ToString(dropNaoInformadoRecusa.SelectedItem.Value);
        else
            pessoaSuitability.Recusa = "N";

        if (dropDispensado.SelectedIndex > -1)
            pessoaSuitability.Dispensado = Convert.ToString(dropDispensado.SelectedItem.Value);
        else
            pessoaSuitability.Dispensado = "N";

        if (dropDispensado.SelectedIndex > -1 && Convert.ToString(dropDispensado.SelectedItem.Value).Equals("S"))
        {
            pessoaSuitability.Dispensa = Convert.ToInt32(dropTipoDispensa.SelectedItem.Value);
        }

        #region Cria historico
        PessoaSuitability pessoaSuitabilityCompare = new PessoaSuitability();
        pessoaSuitabilityCompare.LoadByPrimaryKey(idPessoa);
        if (!pessoaSuitability.compare(pessoaSuitabilityCompare))
        {
            string login = HttpContext.Current.User.Identity.Name;

            PessoaSuitabilityHistorico.createPessoaSuitabilityHistorico(pessoaSuitability, TipoOperacaoBanco.Alteração, login);
            pessoaSuitability.UltimaAlteracao = DateTime.Now;
        }
        #endregion

        pessoaSuitability.Save();

        #endregion

        #region Atualização de Apelido e Nome em Cliente, Carteira e Cotista
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Where(clienteCollection.Query.IdPessoa.Equal(idPessoa));
        clienteCollection.Query.Load();
        foreach(Cliente cliente in clienteCollection)
        {
            cliente.Apelido = textApelido.Text.ToString();
            cliente.Nome = textNome.Text.ToString();
            cliente.DataAtualizacao = DateTime.Now;
            

            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(cliente.IdCliente.Value))
            {
                carteira.Apelido = textApelido.Text.ToString();
                carteira.Nome = textNome.Text.ToString();
                carteira.Save();
            }

        }

        clienteCollection.Save();

        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.Query.Where(cotistaCollection.Query.IdPessoa.Equal(idPessoa));
        cotistaCollection.Query.Load();

        foreach(Cotista cotista in cotistaCollection)
        {
            cotista.Apelido = textApelido.Text.ToString();
            cotista.Nome = textNome.Text.ToString();
        }

        cotistaCollection.Save();
        #endregion

        #region Interface com CRM
        if (_integracaoCrm)
        {
            try
            {
                string url = WebConfigurationManager.AppSettings["IntegracaoCrmService.ClienteWebService"];
                string emailInterfaceCrm = WebConfigurationManager.AppSettings["EmailIntegracaoCrm"];
                string senhaInterfaceCrm = WebConfigurationManager.AppSettings["SenhaIntegracaoCrm"];
                pessoa.IntegraCrm(url, emailInterfaceCrm, senhaInterfaceCrm);

                #region Log do Processo
                HistoricoLog historicoLog1 = new HistoricoLog();
                historicoLog1.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Pessoa - Operacao: Integracao CRM: " + idPessoa + UtilitarioWeb.ToString(pessoa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
                
        }
        #endregion
        
        Session["cnaeDivisao"] = null;
        Session["cnaeGrupo"] = null;
        Session["cnaeClasse"] = null;
        Session["cnaeSubClasse"] = null;

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        Session["cnaeDivisao"] = null;
        Session["cnaeGrupo"] = null;
        Session["cnaeClasse"] = null;
        Session["cnaeSubClasse"] = null;

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CancelRowEditing(object sender, EventArgs e)
    {
        Session["cnaeDivisao"] = null;
        Session["cnaeGrupo"] = null;
        Session["cnaeClasse"] = null;
        Session["cnaeSubClasse"] = null;
    }

    protected void gridCadastro_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        int idPessoa = (int)e.EditingKeyValue;

        Session["cnaeDivisao"] = -1;
        Session["cnaeGrupo"] = -1;
        Session["cnaeClasse"] = -1;
        Session["cnaeSubClasse"] = -1;

        Pessoa pessoa = new Pessoa();
        if (pessoa.LoadByPrimaryKey(idPessoa))
        {
            Session["cnaeDivisao"] = pessoa.CnaeDivisao;
            Session["cnaeGrupo"] = pessoa.CnaeGrupo;
            Session["cnaeClasse"] = pessoa.CnaeClasse;
            Session["cnaeSubClasse"] = pessoa.CnaeSubClasse;
        }
    }

    /// <summary>
    /// Exibe formatação de cpf/cnpj na grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        //Inicia variáveis
        Int64 resultado = 0;

        //Verifica se a coluna é a de CNPJ
        if (e.Column.FieldName.Equals("Cpfcnpj"))
        {
            //Verifica se o valor já está formatado
            if (Int64.TryParse(e.Value.ToString(), out resultado))
            {
                //Verifica se o tipo de pessoa é FÍSICA
                if (e.GetFieldValue("Tipo").ToString().Equals("1"))
                    e.DisplayText = Convert.ToUInt64(e.Value).ToString(@"000\.000\.000\-00");

                //Verifica se o tipo de pessoa é JURÍDICA
                else if (e.GetFieldValue("Tipo").ToString().Equals("2"))
                    e.DisplayText = Convert.ToUInt64(e.Value).ToString(@"00\.000\.000\/0000\-00");
            }
        }
    }
    protected void dropCnaeGrupo_Callback(object sender, CallbackEventArgsBase e)
    {
        Session["cnaeDivisao"] = e.Parameter != "" ? Convert.ToInt32(e.Parameter) : -1;
        Session["cnaeGrupo"] = -1;
        Session["cnaeClasse"] = -1;
        Session["cnaeSubClasse"] = -1;

        EntitySpaces.Web.esDataSourceSelectEventArgs ea = new EntitySpaces.Web.esDataSourceSelectEventArgs();
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropCnaeGrupo = pageControl.FindControl("dropCnaeGrupo") as ASPxComboBox;

        EsDSGrupoCnae_esSelect(sender, ea);

        dropCnaeGrupo.DataBind();
    }

    protected void dropCnaeClasse_Callback(object sender, CallbackEventArgsBase e)
    {
        Session["cnaeGrupo"] = e.Parameter != "" ? Convert.ToInt32(e.Parameter) : -1;
        Session["cnaeClasse"] = -1;
        Session["cnaeSubClasse"] = -1;

        EntitySpaces.Web.esDataSourceSelectEventArgs ea = new EntitySpaces.Web.esDataSourceSelectEventArgs();
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropCnaeClasse = pageControl.FindControl("dropCnaeClasse") as ASPxComboBox;

        EsDSClasseCnae_esSelect(sender, ea);

        dropCnaeClasse.DataBind();
    }

    protected void dropCnaeSubClasse_Callback(object sender, CallbackEventArgsBase e)
    {
        Session["cnaeClasse"] = e.Parameter != "" ? Convert.ToInt32(e.Parameter) : -1;
        Session["cnaeSubClasse"] = -1;

        EntitySpaces.Web.esDataSourceSelectEventArgs ea = new EntitySpaces.Web.esDataSourceSelectEventArgs();
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropCnaeSubClasse = pageControl.FindControl("dropCnaeSubClasse") as ASPxComboBox;

        EsDSSubClasseCnae_esSelect(sender, ea);

        dropCnaeSubClasse.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnSinacor")
        {
            #region Sinacor
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(PessoaMetadata.ColumnNames.IdPessoa);
            //           
            List<int> idPessoas = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
            //
            if (keyValuesId.Count == 0)
            {
                throw new Exception("Escolha no Grid as pessoas desejadas para importar do Sinacor");
            }
            else
            {
                this.ImportarPessoaSinacor(idPessoas, false);
            }

            #endregion

            throw new Exception("Processo concluído.");
        }

        if (e.Parameters == "btnDelete")
        {
            #region Delete
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            CarteiraCollection carteiraCollectionDeletar = new CarteiraCollection();
            PessoaCollection pessoaCollectionDeletar = new PessoaCollection();

            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(PessoaMetadata.ColumnNames.IdPessoa);

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPessoa = Convert.ToInt32(keyValuesId[i]);

                //Deleta Pessoa
                Pessoa pessoa = new Pessoa();
                if (pessoa.LoadByPrimaryKey(idPessoa))
                {
                    bool achou = false;
                    //
                    ClienteCollection clienteCollection = new ClienteCollection();
                    clienteCollection.Query.Where(clienteCollection.Query.IdPessoa == idPessoa);
                    if (clienteCollection.Query.Load())
                    {
                        achou = true;
                    }
                    else
                    {
                        CotistaCollection cotistaCollection = new CotistaCollection();
                        cotistaCollection.Query.Where(cotistaCollection.Query.IdPessoa == idPessoa);
                        if (cotistaCollection.Query.Load())
                        {
                            achou = true;
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Cadastro " + pessoa.Apelido + " não pode ser excluído por ter Cliente e/ou Cotista relacionado.");
                    }

                    #region Trata deleção
                    using (esTransactionScope scope = new esTransactionScope())
                    {
                        // Deleta Todos os Usuários de PermissaoCotista Associados com idPessoa e TipoTrava = TravaCotista
                        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
                        UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                        permissaoCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
                        permissaoCotistaQuery.Where(permissaoCotistaQuery.IdCotista == idPessoa);
                        permissaoCotistaQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCotista));

                        UsuarioCollection usuarioCollectionDeletar = new UsuarioCollection();
                        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                        permissaoCotistaCollection.Load(permissaoCotistaQuery);

                        foreach (PermissaoCotista permissaoCotista in permissaoCotistaCollection)
                        {
                            int idUsuario = permissaoCotista.IdUsuario.Value;
                            Usuario usuario = new Usuario();
                            usuario.LoadByPrimaryKey(idUsuario);
                            usuarioCollectionDeletar.AttachEntity(usuario);
                        }
                        usuarioCollectionDeletar.MarkAllAsDeleted();
                        usuarioCollectionDeletar.Save();
                        //

                        //Deleta todos os usuários de PermissaoCliente associados com idCliente e TipoTrava = TravaCliente
                        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
                        usuarioQuery = new UsuarioQuery("U");

                        permissaoClienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);
                        permissaoClienteQuery.Where(permissaoClienteQuery.IdCliente.Equal(idPessoa));
                        permissaoClienteQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCliente));

                        usuarioCollectionDeletar = new UsuarioCollection();
                        PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                        permissaoClienteCollection.Load(permissaoClienteQuery);

                        foreach (PermissaoCliente permissaoCliente in permissaoClienteCollection)
                        {
                            int idUsuario = permissaoCliente.IdUsuario.Value;
                            Usuario usuario = new Usuario();
                            usuario.LoadByPrimaryKey(idUsuario);
                            usuarioCollectionDeletar.AttachEntity(usuario);
                        }
                        usuarioCollectionDeletar.MarkAllAsDeleted();
                        usuarioCollectionDeletar.Save();
                        //

                        // Deleta LogProcessamento
                        foreach (Cliente cliente in clienteCollection)
                        {
                            int idCliente = cliente.IdCliente.Value;
                            LogProcessamentoCollection logProcessamentoColl = new LogProcessamentoCollection();
                            logProcessamentoColl.Query.Where(logProcessamentoColl.Query.IdCliente.Equal(idCliente));
                            if (logProcessamentoColl.Query.Load())
                            {
                                logProcessamentoColl.MarkAllAsDeleted();
                                logProcessamentoColl.Save();
                            }

                            MemoriaCalculoRendaFixaQuery memoriaQuery = new MemoriaCalculoRendaFixaQuery("memoria");
                            MemoriaCalculoRendaFixaCollection memoriaColl = new MemoriaCalculoRendaFixaCollection();
                            OperacaoRendaFixaQuery operacaoQuery = new OperacaoRendaFixaQuery("operacao");
                            memoriaQuery.Select(memoriaQuery);
                            memoriaQuery.InnerJoin(operacaoQuery).On(operacaoQuery.IdOperacao.Equal(memoriaQuery.IdOperacao));
                            memoriaQuery.Where(operacaoQuery.IdCliente.Equal(idCliente));

                            if (memoriaColl.Load(memoriaQuery))
                            {
                                memoriaColl.MarkAllAsDeleted();
                                memoriaColl.Save();
                            }

                            PosicaoRendaFixaCollection posicaoRendaFixaColl = new PosicaoRendaFixaCollection();
                            posicaoRendaFixaColl.Query.Where(posicaoRendaFixaColl.Query.IdCliente.Equal(idCliente));

                            if (posicaoRendaFixaColl.Query.Load())
                            {
                                posicaoRendaFixaColl.MarkAllAsDeleted();
                                posicaoRendaFixaColl.Save();
                            }

                            PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaColl = new PosicaoRendaFixaAberturaCollection();
                            posicaoRendaFixaAberturaColl.Query.Where(posicaoRendaFixaAberturaColl.Query.IdCliente.Equal(idCliente));

                            if (posicaoRendaFixaAberturaColl.Query.Load())
                            {
                                posicaoRendaFixaAberturaColl.MarkAllAsDeleted();
                                posicaoRendaFixaAberturaColl.Save();
                            }

                            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoColl = new PosicaoRendaFixaHistoricoCollection();
                            posicaoRendaFixaHistoricoColl.Query.Where(posicaoRendaFixaHistoricoColl.Query.IdCliente.Equal(idCliente));

                            if (posicaoRendaFixaHistoricoColl.Query.Load())
                            {
                                posicaoRendaFixaHistoricoColl.MarkAllAsDeleted();
                                posicaoRendaFixaHistoricoColl.Save();
                            }

                            DetalhePosicaoAfetadaRFCollection detalhePosColl = new DetalhePosicaoAfetadaRFCollection();
                            detalhePosColl.Query.Where(detalhePosColl.Query.IdCliente.Equal(idCliente));

                            if (detalhePosColl.Query.Load())
                            {
                                detalhePosColl.MarkAllAsDeleted();
                                detalhePosColl.Save();
                            }

                            BoletoRetroativoCollection boletoRetroativoColl = new BoletoRetroativoCollection();
                            boletoRetroativoColl.Query.Where(boletoRetroativoColl.Query.IdCliente.Equal(idCliente));

                            if (boletoRetroativoColl.Query.Load())
                            {
                                boletoRetroativoColl.MarkAllAsDeleted();
                                boletoRetroativoColl.Save();
                            }

                            ClientePerfilCollection clientePerfilColl = new ClientePerfilCollection();
                            clientePerfilColl.Query.Where(clientePerfilColl.Query.IdCliente.Equal(idCliente));

                            if (clientePerfilColl.Query.Load())
                            {
                                clientePerfilColl.MarkAllAsDeleted();
                                clientePerfilColl.Save();
                            }
                        }

                        //Deleta OperacaoCotista
                        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                        operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao)
                                                       .Where(operacaoCotistaCollection.Query.IdCarteira == idPessoa);

                        if (operacaoCotistaCollection.Query.Load())
                        {
                            operacaoCotistaCollection.MarkAllAsDeleted();
                            operacaoCotistaCollection.Save();
                        }

                        EmissorCollection emissorColl = new EmissorCollection();
                        emissorColl.Query.Where(emissorColl.Query.IdPessoa.Equal(idPessoa));

                        if (emissorColl.Query.Load())
                        {
                            emissorColl.MarkAllAsDeleted();
                            emissorColl.Save();
                        }

                        BancoCollection bancoColl = new BancoCollection();
                        bancoColl.Query.Where(bancoColl.Query.IdPessoa.Equal(idPessoa));

                        if (bancoColl.Query.Load())
                        {
                            bancoColl.MarkAllAsDeleted();
                            bancoColl.Save();
                        }

                        DiretorCollection diretorColl = new DiretorCollection();
                        diretorColl.Query.Where(diretorColl.Query.IdPessoa.Equal(idPessoa));

                        if (diretorColl.Query.Load())
                        {
                            diretorColl.MarkAllAsDeleted();
                            diretorColl.Save();
                        }

                        AgenteMercadoCollection agenteMercadoColl = new AgenteMercadoCollection();
                        agenteMercadoColl.Query.Where(agenteMercadoColl.Query.IdPessoa.Equal(idPessoa));

                        if (agenteMercadoColl.Query.Load())
                        {
                            agenteMercadoColl.MarkAllAsDeleted();
                            agenteMercadoColl.Save();
                        }

                        //
                        Pessoa pessoaClone = (Pessoa)Utilitario.Clone(pessoa);
                        //
                        pessoa.MarkAsDeleted();
                        pessoa.Save();
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Pessoa - Operacao: Delete Pessoa: " + idPessoa + UtilitarioWeb.ToString(pessoaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                        scope.Complete();
                    }
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxSpinEdit textIdPessoa = pageControl.FindControl("textIdPessoa") as ASPxSpinEdit;
            e.Properties["cpTextIdPessoa"] = textIdPessoa.ClientID;

            ASPxTextBox textApelido = pageControl.FindControl("textApelido") as ASPxTextBox;
            e.Properties["cpTextApelido"] = textApelido.ClientID;
        }
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        bool isOnTabPage = true;
        this.FocaCampoGrid("textIdPessoa", "textApelido", isOnTabPage);
        base.gridCadastro_PreRender(sender, e);
    }

    private void SalvarNovo()
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        #region Campos Tela
        ASPxSpinEdit textIdPessoa = pageControl.FindControl("textIdPessoa") as ASPxSpinEdit;
        ASPxTextBox textApelido = pageControl.FindControl("textApelido") as ASPxTextBox;
        TextBox textNome = pageControl.FindControl("textNome") as TextBox;
        TextBox textDataCadastro = pageControl.FindControl("textDataCadastro") as TextBox;
        ASPxTextBox textCPFCNPJ = pageControl.FindControl("textCPFCNPJ") as ASPxTextBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropEstadoCivil = pageControl.FindControl("dropEstadoCivil") as ASPxComboBox;
        ASPxComboBox dropSexo = pageControl.FindControl("dropSexo") as ASPxComboBox;
        TextBox textProfissao = pageControl.FindControl("textProfissao") as TextBox;
        ASPxComboBox dropNacionalidade = pageControl.FindControl("dropNacionalidade") as ASPxComboBox;
        ASPxDateEdit textDataNascimento = pageControl.FindControl("textDataNascimento") as ASPxDateEdit;
        ASPxDateEdit textDataVencimentoCadastro = pageControl.FindControl("textDataVencimentoCadastro") as ASPxDateEdit;
        ASPxComboBox dropAlertaCadastro = pageControl.FindControl("dropAlertaCadastro") as ASPxComboBox;
        TextBox textEmailAlerta = pageControl.FindControl("textEmailAlerta") as TextBox;
        TextBox textCodigoInterface = pageControl.FindControl("textCodigoInterface") as TextBox;

        ASPxComboBox dropPessoaPoliticamenteExposta = pageControl.FindControl("dropPessoaPoliticamenteExposta") as ASPxComboBox;
        ASPxComboBox dropUFNaturalidade = pageControl.FindControl("estado") as ASPxComboBox;
        //
        ASPxComboBox dropSituacaoLegal = pageControl.FindControl("dropSituacaoLegal") as ASPxComboBox;
        ASPxComboBox dropPessoaVinculada = pageControl.FindControl("dropPessoaVinculada") as ASPxComboBox;
        ASPxComboBox dropPerfilInvestidor = pageControl.FindControl("dropPerfilInvestidor") as ASPxComboBox;
        //
        TextBox textFiliacaoNomePai = pageControl.FindControl("filiacaoNomePai") as TextBox;
        TextBox textFiliacaoNomeMae = pageControl.FindControl("filiacaoNomeMae") as TextBox;
        //
        ASPxComboBox dropNaturezaJuridica = pageControl.FindControl("dropNaturezaJuridica") as ASPxComboBox;
        ASPxComboBox dropTipoRenda = pageControl.FindControl("dropTipoRenda") as ASPxComboBox;
        ASPxComboBox dropIndicadorFatca = pageControl.FindControl("dropIndicadorFatca") as ASPxComboBox;
        TextBox textGiinTin = pageControl.FindControl("textGiinTin") as TextBox;
        ASPxComboBox dropJustificativaGiin = pageControl.FindControl("dropJustificativaGiin") as ASPxComboBox;
        ASPxComboBox dropCnaeDivisao = pageControl.FindControl("dropCnaeDivisao") as ASPxComboBox;
        ASPxComboBox dropCnaeGrupo = pageControl.FindControl("dropCnaeGrupo") as ASPxComboBox;
        ASPxComboBox dropCnaeClasse = pageControl.FindControl("dropCnaeClasse") as ASPxComboBox;
        ASPxComboBox dropCnaeSubClasse = pageControl.FindControl("dropCnaeSubClasse") as ASPxComboBox;
        ASPxCheckBox chkOffShore = pageControl.FindControl("chkOffShore") as ASPxCheckBox;

        #endregion

        #region Pessoa
        Pessoa pessoa = new Pessoa();

        pessoa.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
        pessoa.Apelido = textApelido.Text.ToString();
        pessoa.Nome = textNome.Text.ToString();

        if (textCPFCNPJ.Text != "" && dropTipo.SelectedIndex != -1)
        {
            if (!string.IsNullOrEmpty(textCPFCNPJ.Text))
                pessoa.Cpfcnpj = textCPFCNPJ.Text.ToString().Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");
            else
                pessoa.Cpfcnpj = null;

            pessoa.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        }
        else
        {
            pessoa.Cpfcnpj = null;
            pessoa.Tipo = null;
        }

        if (dropEstadoCivil.SelectedIndex != -1)
        {
            pessoa.EstadoCivil = Convert.ToByte(dropEstadoCivil.SelectedItem.Value);
        }
        if (dropSexo.SelectedIndex != -1)
        {
            pessoa.Sexo = dropSexo.SelectedItem.Value.ToString();
        }
        pessoa.Profissao = textProfissao.Text.ToString();

        if (dropAlertaCadastro.SelectedIndex != -1)
        {
            pessoa.AlertaCadastro = dropAlertaCadastro.SelectedItem.ValueString.ToString();
        }

        if (dropNacionalidade.SelectedIndex != -1)
        {
            pessoa.Nacionalidade = Convert.ToInt16(dropNacionalidade.SelectedItem.Value);
        }

        pessoa.CodigoInterface = textCodigoInterface.Text.ToString();
        pessoa.EmailAlerta = textEmailAlerta.Text.ToString();

        if (textDataNascimento.Text != "")
        {
            pessoa.DataNascimento = Convert.ToDateTime(textDataNascimento.Text);
        }
        if (textDataVencimentoCadastro.Text != "")
        {
            pessoa.DataVencimentoCadastro = Convert.ToDateTime(textDataVencimentoCadastro.Text);
        }

        pessoa.DataCadatro = DateTime.Now;
        pessoa.DataUltimaAlteracao = DateTime.Now;

        if (dropPessoaPoliticamenteExposta.SelectedIndex >= 0)
        {
            pessoa.PessoaPoliticamenteExposta = dropPessoaPoliticamenteExposta.SelectedItem.Value.ToString();
        }

        if (dropUFNaturalidade.SelectedIndex >= 0)
        {
            pessoa.UFNaturalidade = dropUFNaturalidade.SelectedItem.Value.ToString();
        }

        if (dropSituacaoLegal.SelectedIndex >= 0)
        {
            pessoa.SituacaoLegal = Convert.ToByte(dropSituacaoLegal.SelectedItem.Value);
        }

        if (dropPessoaVinculada.SelectedIndex >= 0)
        {
            pessoa.PessoaVinculada = Convert.ToString(dropPessoaVinculada.SelectedItem.Value);
        }

        if (dropPerfilInvestidor.SelectedIndex >= 0)
        {
            pessoa.IdPerfilInvestidor = Convert.ToByte(dropPerfilInvestidor.SelectedItem.Value);
        }

        if (!String.IsNullOrEmpty(textFiliacaoNomePai.Text))
        {
            pessoa.FiliacaoNomePai = textFiliacaoNomePai.Text.ToString().Trim();
        }
        if (!String.IsNullOrEmpty(textFiliacaoNomeMae.Text))
        {
            pessoa.FiliacaoNomeMae = textFiliacaoNomeMae.Text.ToString().Trim();
        }

        if (dropNaturezaJuridica.SelectedIndex >= 0)
        {
            pessoa.NaturezaJuridica = Convert.ToInt16(dropNaturezaJuridica.SelectedItem.Value);
        }
        else
        {
            pessoa.NaturezaJuridica = null;
        }

        if (dropTipoRenda.SelectedIndex >= 0)
        {
            pessoa.TipoRenda = dropTipoRenda.SelectedItem.Value.ToString();
        }
        else
        {
            pessoa.TipoRenda = null;
        }

        if (dropIndicadorFatca.SelectedIndex >= 0)
        {
            pessoa.IndicadorFatca = dropIndicadorFatca.SelectedItem.Value.ToString();
        }
        else
        {
            pessoa.IndicadorFatca = null;
        }

        if (textGiinTin.Text != "")
        {
            pessoa.GiinTin = textGiinTin.Text;
        }
        else
        {
            pessoa.GiinTin = null;
        }

        if (dropJustificativaGiin.SelectedIndex >= 0)
        {
            pessoa.JustificativaGiin = dropJustificativaGiin.SelectedItem.Value.ToString();
        }
        else
        {
            pessoa.JustificativaGiin = null;
        }

        if (dropCnaeDivisao.SelectedIndex >= 0)
        {
            pessoa.CnaeDivisao = Convert.ToByte(dropCnaeDivisao.SelectedItem.Value);
        }
        else
        {
            pessoa.CnaeDivisao = null;
        }

        if (dropCnaeGrupo.SelectedIndex >= 0)
        {
            pessoa.CnaeGrupo = Convert.ToByte(dropCnaeGrupo.SelectedItem.Value);
        }
        else
        {
            pessoa.CnaeGrupo = null;
        }

        if (dropCnaeClasse.SelectedIndex >= 0)
        {
            pessoa.CnaeClasse = Convert.ToByte(dropCnaeClasse.SelectedItem.Value);
        }
        else
        {
            pessoa.CnaeClasse = null;
        }

        if (dropCnaeSubClasse.SelectedIndex >= 0)
        {
            pessoa.CnaeSubClasse = Convert.ToByte(dropCnaeSubClasse.SelectedItem.Value);
        }
        else
        {
            pessoa.CnaeSubClasse = null;
        }

        if (chkOffShore.Checked)
        {
            pessoa.OffShore = "S";
            pessoa.Tipo = null;
            pessoa.Cpfcnpj = null;
        }
        else
            pessoa.OffShore = "N";

        pessoa.Save();
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Pessoa - Operacao: Insert Pessoa: " + pessoa.IdPessoa + UtilitarioWeb.ToString(pessoa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Cadastro de Enderecos 1
        TextBox textEndereco = pageControl.FindControl("textEndereco") as TextBox;
        TextBox textNumero = pageControl.FindControl("textNumero") as TextBox;
        TextBox textComplemento = pageControl.FindControl("textComplemento") as TextBox;
        TextBox textBairro = pageControl.FindControl("textBairro") as TextBox;
        TextBox textCidade = pageControl.FindControl("textCidade") as TextBox;
        TextBox textCEP = pageControl.FindControl("textCEP") as TextBox;
        TextBox textUF = pageControl.FindControl("textUF") as TextBox;
        TextBox textPais = pageControl.FindControl("textPais") as TextBox;
        ASPxComboBox dropRecebeCorrespondencia = pageControl.FindControl("dropRecebeCorrespondencia") as ASPxComboBox;
        ASPxComboBox dropTipoEndereco = pageControl.FindControl("dropTipoEndereco") as ASPxComboBox;

        if (textEndereco.Text != "" || textNumero.Text != "" || textBairro.Text != "" || textCidade.Text != "")
        {
            PessoaEndereco pessoaEndereco = new PessoaEndereco();

            pessoaEndereco.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaEndereco.Endereco = textEndereco.Text.ToString();
            pessoaEndereco.Numero = textNumero.Text.ToString();
            pessoaEndereco.Complemento = textComplemento.Text.ToString();
            pessoaEndereco.Bairro = textBairro.Text.ToString();
            pessoaEndereco.Cidade = textCidade.Text.ToString();
            pessoaEndereco.Cep = textCEP.Text.ToString();
            pessoaEndereco.Uf = textUF.Text.ToString();
            pessoaEndereco.Pais = textPais.Text.ToString();

            pessoaEndereco.RecebeCorrespondencia = dropRecebeCorrespondencia.SelectedIndex == -1
                                    ? "N" : Convert.ToString(dropRecebeCorrespondencia.SelectedItem.Value);

            if (dropTipoEndereco.SelectedIndex >= 0)
            {
                pessoaEndereco.TipoEndereco = Convert.ToString(dropTipoEndereco.SelectedItem.Value);
            }

            pessoaEndereco.Save();
        }
        #endregion

        #region Cadastro de Enderecos 2
        TextBox textEndereco2 = pageControl.FindControl("textEndereco2") as TextBox;
        TextBox textNumero2 = pageControl.FindControl("textNumero2") as TextBox;
        TextBox textComplemento2 = pageControl.FindControl("textComplemento2") as TextBox;
        TextBox textBairro2 = pageControl.FindControl("textBairro2") as TextBox;
        TextBox textCidade2 = pageControl.FindControl("textCidade2") as TextBox;
        TextBox textCEP2 = pageControl.FindControl("textCEP2") as TextBox;
        TextBox textUF2 = pageControl.FindControl("textUF2") as TextBox;
        TextBox textPais2 = pageControl.FindControl("textPais2") as TextBox;
        ASPxComboBox dropRecebeCorrespondencia2 = pageControl.FindControl("dropRecebeCorrespondencia2") as ASPxComboBox;
        ASPxComboBox dropTipoEndereco2 = pageControl.FindControl("dropTipoEndereco2") as ASPxComboBox;
        //

        if (textEndereco2.Text != "" || textNumero2.Text != "" || textBairro2.Text != "" || textCidade2.Text != "")
        {
            PessoaEndereco pessoaEndereco = new PessoaEndereco();

            pessoaEndereco.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaEndereco.Endereco = textEndereco2.Text.ToString();
            pessoaEndereco.Numero = textNumero2.Text.ToString();
            pessoaEndereco.Complemento = textComplemento2.Text.ToString();
            pessoaEndereco.Bairro = textBairro2.Text.ToString();
            pessoaEndereco.Cidade = textCidade2.Text.ToString();
            pessoaEndereco.Cep = textCEP2.Text.ToString();
            pessoaEndereco.Uf = textUF2.Text.ToString();
            pessoaEndereco.Pais = textPais2.Text.ToString();

            pessoaEndereco.RecebeCorrespondencia = dropRecebeCorrespondencia2.SelectedIndex == -1
                                   ? "N" : Convert.ToString(dropRecebeCorrespondencia2.SelectedItem.Value);

            if (dropTipoEndereco2.SelectedIndex >= 0)
            {
                pessoaEndereco.TipoEndereco = Convert.ToString(dropTipoEndereco2.SelectedItem.Value);
            }

            pessoaEndereco.Save();
        }
        #endregion

        #region Cadastro de Telefone 1
        ASPxTextBox txtDDI = pageControl.FindControl("txtDDI") as ASPxTextBox;
        ASPxTextBox txtDDD = pageControl.FindControl("txtDDD") as ASPxTextBox;
        ASPxTextBox txtNumero = pageControl.FindControl("txtNumero") as ASPxTextBox;
        ASPxTextBox txtRamal = pageControl.FindControl("txtRamal") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone = pageControl.FindControl("dropTipoTelefone") as ASPxComboBox;

        if (txtDDI.Text != "" || txtDDD.Text != "" || txtNumero.Text != "" || txtRamal.Text != "" || dropTipoTelefone.SelectedIndex >= 0)
        {
            PessoaTelefone pessoaTelefone = new PessoaTelefone();

            pessoaTelefone.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaTelefone.Ddi = txtDDI.Text.ToString();
            pessoaTelefone.Ddd = txtDDD.Text.ToString();
            pessoaTelefone.Numero = txtNumero.Text.ToString();
            pessoaTelefone.Ramal = txtRamal.Text.ToString();
            //
            pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone.SelectedItem.Value);
            //
            pessoaTelefone.Save();
        }
        #endregion

        #region Cadastro de Telefone 2
        ASPxTextBox txtDDI1 = pageControl.FindControl("txtDDI1") as ASPxTextBox;
        ASPxTextBox txtDDD1 = pageControl.FindControl("txtDDD1") as ASPxTextBox;
        ASPxTextBox txtNumero1 = pageControl.FindControl("txtNumero1") as ASPxTextBox;
        ASPxTextBox txtRamal1 = pageControl.FindControl("txtRamal1") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone1 = pageControl.FindControl("dropTipoTelefone1") as ASPxComboBox;

        if (txtDDI1.Text != "" || txtDDD1.Text != "" || txtNumero1.Text != "" || txtRamal1.Text != "" || dropTipoTelefone1.SelectedIndex >= 0)
        {
            PessoaTelefone pessoaTelefone = new PessoaTelefone();

            pessoaTelefone.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaTelefone.Ddi = txtDDI1.Text.ToString();
            pessoaTelefone.Ddd = txtDDD1.Text.ToString();
            pessoaTelefone.Numero = txtNumero1.Text.ToString();
            pessoaTelefone.Ramal = txtRamal1.Text.ToString();
            //
            pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone1.SelectedItem.Value);
            //
            pessoaTelefone.Save();
        }
        #endregion

        #region Cadastro de Telefone 3
        ASPxTextBox txtDDI2 = pageControl.FindControl("txtDDI2") as ASPxTextBox;
        ASPxTextBox txtDDD2 = pageControl.FindControl("txtDDD2") as ASPxTextBox;
        ASPxTextBox txtNumero2 = pageControl.FindControl("txtNumero2") as ASPxTextBox;
        ASPxTextBox txtRamal2 = pageControl.FindControl("txtRamal2") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone2 = pageControl.FindControl("dropTipoTelefone2") as ASPxComboBox;

        if (txtDDI2.Text != "" || txtDDD2.Text != "" || txtNumero2.Text != "" || txtRamal2.Text != "" || dropTipoTelefone2.SelectedIndex >= 0)
        {
            PessoaTelefone pessoaTelefone = new PessoaTelefone();

            pessoaTelefone.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaTelefone.Ddi = txtDDI2.Text.ToString();
            pessoaTelefone.Ddd = txtDDD2.Text.ToString();
            pessoaTelefone.Numero = txtNumero2.Text.ToString();
            pessoaTelefone.Ramal = txtRamal2.Text.ToString();
            //
            pessoaTelefone.TipoTelefone = Convert.ToByte(dropTipoTelefone2.SelectedItem.Value);
            //
            pessoaTelefone.Save();
        }
        #endregion

        #region Cadastro de Email1
        TextBox txtEmail1 = pageControl.FindControl("txtEmail1") as TextBox;
        //            
        if (txtEmail1.Text != "")
        {
            PessoaEmail pessoaEmail = new PessoaEmail();

            pessoaEmail.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaEmail.Email = txtEmail1.Text.ToString();
            //
            pessoaEmail.Save();
        }
        #endregion

        #region Cadastro de Email2
        TextBox txtEmail2 = pageControl.FindControl("txtEmail2") as TextBox;
        //            
        if (txtEmail2.Text != "")
        {
            PessoaEmail pessoaEmail = new PessoaEmail();

            pessoaEmail.IdPessoa = Convert.ToInt32(textIdPessoa.Text);
            pessoaEmail.Email = txtEmail2.Text.ToString();
            //
            pessoaEmail.Save();
        }
        #endregion

        #region Cadastro de Documento 1
        ASPxComboBox dropTipoDocumento = pageControl.FindControl("dropTipoDocumento") as ASPxComboBox;
        ASPxComboBox dropOrgaoEmissor = pageControl.FindControl("dropOrgaoEmissor") as ASPxComboBox;
        ASPxComboBox dropUFEmissor = pageControl.FindControl("dropUFEmissor") as ASPxComboBox;
        //
        ASPxTextBox txtNumDocumento = pageControl.FindControl("txtNumDocumento") as ASPxTextBox;
        ASPxDateEdit textDataExpedicao = pageControl.FindControl("textDataExpedicao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;

        if (dropTipoDocumento.SelectedIndex >= 0 || dropOrgaoEmissor.SelectedIndex >= 0 || dropUFEmissor.SelectedIndex >= 0 ||
            txtNumDocumento.Text != "" || textDataExpedicao.Text != "")
        {
            PessoaDocumento pessoaDocumento = new PessoaDocumento();

            //Preenche os dados, seja para insert ou para update
            if (txtNumDocumento.Text != "")
            { // Se tem numero Documento
                pessoaDocumento.IdPessoa = Convert.ToInt32(textIdPessoa.Text);

                pessoaDocumento.NumeroDocumento = txtNumDocumento.Text.ToString();

                pessoaDocumento.TipoDocumento = null;
                pessoaDocumento.DataExpedicao = null;
                pessoaDocumento.DataVencimento = null;
                pessoaDocumento.OrgaoEmissor = null;
                pessoaDocumento.UFEmissor = null;

                if (dropTipoDocumento.SelectedIndex >= 0)
                {
                    pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento.SelectedItem.Value);
                }

                if (textDataExpedicao.Text != "")
                {
                    pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao.Text);
                }

                if (textDataVencimento.Text != "")
                {
                    pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                }

                if (dropOrgaoEmissor.SelectedIndex >= 0)
                {
                    pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor.SelectedItem.Text;
                }

                if (dropUFEmissor.SelectedIndex >= 0)
                {
                    pessoaDocumento.UFEmissor = dropUFEmissor.SelectedItem.Text;
                }
                //
                pessoaDocumento.Save();
            }
        }
        #endregion

        #region Cadastro de Documento 2
        //
        ASPxComboBox dropTipoDocumento1 = pageControl.FindControl("dropTipoDocumento1") as ASPxComboBox;
        ASPxComboBox dropOrgaoEmissor1 = pageControl.FindControl("dropOrgaoEmissor1") as ASPxComboBox;
        ASPxComboBox dropUFEmissor1 = pageControl.FindControl("dropUFEmissor1") as ASPxComboBox;
        //
        ASPxTextBox txtNumDocumento1 = pageControl.FindControl("txtNumDocumento1") as ASPxTextBox;
        ASPxDateEdit textDataExpedicao1 = pageControl.FindControl("textDataExpedicao1") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento1 = pageControl.FindControl("textDataVencimento1") as ASPxDateEdit;

        if (dropTipoDocumento1.SelectedIndex >= 0 || dropOrgaoEmissor1.SelectedIndex >= 0 || dropUFEmissor1.SelectedIndex >= 0 ||
            txtNumDocumento1.Text != "" || textDataExpedicao1.Text != "")
        {
            PessoaDocumento pessoaDocumento = new PessoaDocumento();

            //Preenche os dados, seja para insert ou para update
            if (txtNumDocumento1.Text != "")
            { // Se tem numero Documento
                pessoaDocumento.IdPessoa = Convert.ToInt32(textIdPessoa.Text);

                pessoaDocumento.NumeroDocumento = txtNumDocumento1.Text.ToString();

                pessoaDocumento.TipoDocumento = null;
                pessoaDocumento.DataExpedicao = null;
                pessoaDocumento.DataVencimento = null;
                pessoaDocumento.OrgaoEmissor = null;
                pessoaDocumento.UFEmissor = null;

                if (dropTipoDocumento1.SelectedIndex >= 0)
                {
                    pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento1.SelectedItem.Value);
                }

                if (textDataExpedicao1.Text != "")
                {
                    pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao1.Text);
                }

                if (textDataVencimento1.Text != "")
                {
                    pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento1.Text);
                }

                if (dropOrgaoEmissor1.SelectedIndex >= 0)
                {
                    pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor1.SelectedItem.Text;
                }

                if (dropUFEmissor1.SelectedIndex >= 0)
                {
                    pessoaDocumento.UFEmissor = dropUFEmissor1.SelectedItem.Text;
                }
                //
                pessoaDocumento.Save();
            }
        }
        #endregion

        #region Cadastro de Documento 3
        ASPxComboBox dropTipoDocumento2 = pageControl.FindControl("dropTipoDocumento2") as ASPxComboBox;
        ASPxComboBox dropOrgaoEmissor2 = pageControl.FindControl("dropOrgaoEmissor2") as ASPxComboBox;
        ASPxComboBox dropUFEmissor2 = pageControl.FindControl("dropUFEmissor2") as ASPxComboBox;
        //
        ASPxTextBox txtNumDocumento2 = pageControl.FindControl("txtNumDocumento2") as ASPxTextBox;
        ASPxDateEdit textDataExpedicao2 = pageControl.FindControl("textDataExpedicao2") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento2 = pageControl.FindControl("textDataVencimento2") as ASPxDateEdit;

        if (dropTipoDocumento2.SelectedIndex >= 0 || dropOrgaoEmissor2.SelectedIndex >= 0 || dropUFEmissor2.SelectedIndex >= 0 ||
            txtNumDocumento2.Text != "" || textDataExpedicao2.Text != "")
        {
            PessoaDocumento pessoaDocumento = new PessoaDocumento();

            //Preenche os dados, seja para insert ou para update
            if (txtNumDocumento2.Text != "")
            { // Se tem numero Documento
                pessoaDocumento.IdPessoa = Convert.ToInt32(textIdPessoa.Text);

                pessoaDocumento.NumeroDocumento = txtNumDocumento2.Text.ToString();

                pessoaDocumento.TipoDocumento = null;
                pessoaDocumento.DataExpedicao = null;
                pessoaDocumento.DataVencimento = null;
                pessoaDocumento.OrgaoEmissor = null;
                pessoaDocumento.UFEmissor = null;

                if (dropTipoDocumento2.SelectedIndex >= 0)
                {
                    pessoaDocumento.TipoDocumento = Convert.ToByte(dropTipoDocumento2.SelectedItem.Value);
                }

                if (textDataExpedicao2.Text != "")
                {
                    pessoaDocumento.DataExpedicao = Convert.ToDateTime(textDataExpedicao2.Text);
                }

                if (textDataVencimento2.Text != "")
                {
                    pessoaDocumento.DataVencimento = Convert.ToDateTime(textDataVencimento2.Text);
                }

                if (dropOrgaoEmissor2.SelectedIndex >= 0)
                {
                    pessoaDocumento.OrgaoEmissor = dropOrgaoEmissor2.SelectedItem.Text;
                }

                if (dropUFEmissor2.SelectedIndex >= 0)
                {
                    pessoaDocumento.UFEmissor = dropUFEmissor2.SelectedItem.Text;
                }
                //
                pessoaDocumento.Save();
            }
        }
        #endregion

        #region tabPage - Cliente - Suitability
        ASPxComboBox dropPerfil = pageControl.FindControl("dropPerfil") as ASPxComboBox;
        ASPxComboBox dropNaoInformadoRecusa = pageControl.FindControl("dropNaoInformadoRecusa") as ASPxComboBox;
        ASPxComboBox dropDispensado = pageControl.FindControl("dropDispensado") as ASPxComboBox;
        ASPxComboBox dropTipoDispensa = pageControl.FindControl("dropTipoDispensa") as ASPxComboBox;
        ASPxComboBox dropValidacao = pageControl.FindControl("dropValidacao") as ASPxComboBox;

        PessoaSuitability pessoaSuitability = new PessoaSuitability();

        pessoaSuitability.IdPessoa = Convert.ToInt32(textIdPessoa.Text);

        if (dropPerfil.SelectedIndex > -1)
        {
            pessoaSuitability.Perfil = Convert.ToInt32(dropPerfil.SelectedItem.Value);
            pessoaSuitability.UltimaAlteracao = DateTime.Now;
        }

        if (dropValidacao.SelectedIndex > -1)
        {
            pessoaSuitability.IdValidacao = Convert.ToInt32(dropValidacao.SelectedItem.Value);
            pessoaSuitability.UltimaAlteracao = DateTime.Now;
        }

        if (dropNaoInformadoRecusa.SelectedIndex > -1)
            pessoaSuitability.Recusa = Convert.ToString(dropNaoInformadoRecusa.SelectedItem.Value);
        else
            pessoaSuitability.Recusa = "N";

        if (dropDispensado.SelectedIndex > -1)
            pessoaSuitability.Dispensado = Convert.ToString(dropDispensado.SelectedItem.Value);
        else
            pessoaSuitability.Dispensado = "N";

        if (dropDispensado.SelectedIndex > -1 && Convert.ToString(dropDispensado.SelectedItem.Value).Equals("S"))
        {
            pessoaSuitability.Dispensa = Convert.ToInt32(dropTipoDispensa.SelectedItem.Value);
        }

        #region Cria historico
        string login1 = HttpContext.Current.User.Identity.Name;

        PessoaSuitabilityHistorico.createPessoaSuitabilityHistorico(pessoaSuitability, TipoOperacaoBanco.Inclusão, login1);
        pessoaSuitability.UltimaAlteracao = DateTime.Now;
        #endregion

        pessoaSuitability.Save();

        #endregion

        #region Clonagem de Templates (Carteira ou Cotista)
        ASPxCheckBox chkTemplate = pageControl.FindControl("chkTemplate") as ASPxCheckBox;

        string login = Context.User.Identity.Name;

        if (chkTemplate.Checked)
        {
            #region Cria/Carteira, as tabelas de provisão/adm/pfee, ListaBenchmark e Enquadra, baseado no Template
            if (dropTemplateCarteira.SelectedIndex != -1)
            {
                int idCarteira = Convert.ToInt32(textIdPessoa.Text);
                int idCarteiraClonar = Convert.ToInt32(dropTemplateCarteira.SelectedItem.Value);
                //                                
                /*
                */
                #region Salva o cliente de acordo com um Template
                Cliente clienteReplicado = new Cliente();
                Cliente cliente = clienteReplicado.ReplicaCliente(idCarteira, idCarteiraClonar);
                // Troca o Nome e o Apelido
                cliente.Nome = textNome.Text.Trim();
                cliente.Apelido = textApelido.Text.Trim();
                cliente.DataDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                cliente.DataInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                cliente.Status = (byte)StatusCliente.Fechado;
                cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                cliente.CalculaRealTime = "N";
                cliente.IdPessoa = idCarteira;
                //
                cliente.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                //
                cliente.DataAtualizacao = DateTime.Now;
                cliente.Save();
                #endregion

                #region Salva ClienteBolsa de acordo com o template
                ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();
                clienteBolsaCollection.Query.Where(clienteBolsaCollection.Query.IdCliente == idCarteiraClonar);
                clienteBolsaCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (ClienteBolsa clienteBolsa in clienteBolsaCollection)
                {
                    clienteBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                    clienteBolsa.IdCliente = idCarteira;
                    clienteBolsa.CodigoSinacor = idCarteira.ToString();
                    clienteBolsa.CodigoSinacor2 = "";
                }
                if (clienteBolsaCollection.HasData)
                {
                    clienteBolsaCollection.Save();
                }
                #endregion

                #region Salva ClienteBMF de acordo com o template
                ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
                clienteBMFCollection.Query.Where(clienteBMFCollection.Query.IdCliente == idCarteiraClonar);
                clienteBMFCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (ClienteBMF clienteBMF in clienteBMFCollection)
                {
                    clienteBMF.MarkAllColumnsAsDirty(DataRowState.Added);
                    clienteBMF.IdCliente = idCarteira;
                    clienteBMF.CodigoSinacor = idCarteira.ToString();
                }
                if (clienteBMFCollection.HasData)
                {
                    clienteBMFCollection.Save();
                }
                #endregion

                #region Salva ClienteRendaFixa de acordo com o template
                ClienteRendaFixaCollection clienteRendaFixaCollection = new ClienteRendaFixaCollection();
                clienteRendaFixaCollection.Query.Where(clienteRendaFixaCollection.Query.IdCliente == idCarteiraClonar);
                clienteRendaFixaCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (ClienteRendaFixa clienteRendaFixa in clienteRendaFixaCollection)
                {
                    clienteRendaFixa.MarkAllColumnsAsDirty(DataRowState.Added);
                    clienteRendaFixa.IdCliente = idCarteira;
                }
                if (clienteRendaFixaCollection.HasData)
                {
                    clienteRendaFixaCollection.Save();
                }
                #endregion

                #region Salva ClienteInterface de acordo com o template
                ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
                clienteInterfaceCollection.Query.Where(clienteInterfaceCollection.Query.IdCliente == idCarteiraClonar);
                clienteInterfaceCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (ClienteInterface clienteInterface in clienteInterfaceCollection)
                {
                    clienteInterface.MarkAllColumnsAsDirty(DataRowState.Added);
                    clienteInterface.IdCliente = idCarteira;
                }
                if (clienteInterfaceCollection.HasData)
                {
                    clienteInterfaceCollection.Save();
                }
                #endregion

                #region Salva Contacorrente default associada ao cliente
                ContaCorrente contaCorrente = new ContaCorrente();
                contaCorrente.IdPessoa = idCarteira;
                contaCorrente.Numero = idCarteira.ToString();

                TipoConta tipoConta = new TipoConta();
                tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                tipoConta.Query.Load();
                if (!tipoConta.IdTipoConta.HasValue)
                {
                    tipoConta = new TipoConta();
                    tipoConta.Descricao = "Conta Depósito";
                    tipoConta.Save();
                }
                contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

                contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
                contaCorrente.ContaDefault = "S";

                contaCorrente.Save();
                #endregion

                #region Salva CodigoClienteAgente default associada ao cliente
                CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
                codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdCliente == idCarteiraClonar);
                codigoClienteAgenteCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (CodigoClienteAgente codigoClienteAgente in codigoClienteAgenteCollection)
                {
                    codigoClienteAgente.MarkAllColumnsAsDirty(DataRowState.Added);
                    codigoClienteAgente.IdCliente = idCarteira;
                    codigoClienteAgente.CodigoClienteBMF = idCarteira;
                    codigoClienteAgente.CodigoClienteBovespa = idCarteira;
                }
                if (codigoClienteAgenteCollection.HasData)
                {
                    codigoClienteAgenteCollection.Save();
                }
                #endregion

                #region Salva a carteira de acordo com um Template
                Carteira carteiraReplicada = new Carteira();
                Carteira carteira = carteiraReplicada.ReplicaCarteira(idCarteira, idCarteiraClonar);
                // Troca o Nome e o Apelido
                carteira.Nome = textNome.Text.Trim();
                carteira.Apelido = textApelido.Text.Trim();
                carteira.DataInicioCota = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                //
                carteira.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
                //
                carteira.Save();
                #endregion

                #region Salva TabelaTaxaAdministracao
                TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
                tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira == idCarteiraClonar);
                tabelaTaxaAdministracaoCollection.Query.Load();

                // Troca o idCarteira para o novo IdCarteira
                foreach (TabelaTaxaAdministracao tabelaTaxaAdministracao in tabelaTaxaAdministracaoCollection)
                {
                    tabelaTaxaAdministracao.MarkAllColumnsAsDirty(DataRowState.Added);
                    tabelaTaxaAdministracao.IdCarteira = idCarteira;
                    tabelaTaxaAdministracao.DataReferencia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }
                if (tabelaTaxaAdministracaoCollection.HasData)
                {
                    tabelaTaxaAdministracaoCollection.Save();
                }
                #endregion

                #region Salva ListaBenchmark
                ListaBenchmarkCollection listaBenchmarkCollection = new ListaBenchmarkCollection();
                listaBenchmarkCollection.Query.Where(listaBenchmarkCollection.Query.IdCarteira == idCarteiraClonar);
                listaBenchmarkCollection.Query.Load();
                //
                // Troca o idCarteira para o novo IdCarteira
                foreach (ListaBenchmark listaBenchmark in listaBenchmarkCollection)
                {
                    listaBenchmark.MarkAllColumnsAsDirty(DataRowState.Added);
                    listaBenchmark.IdCarteira = idCarteira;
                }
                if (listaBenchmarkCollection.HasData)
                {
                    listaBenchmarkCollection.Save();
                }
                #endregion

                #region Salva EnquadraRegras
                EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
                enquadraRegraCollection.Query.Where(enquadraRegraCollection.Query.IdCarteira == idCarteiraClonar);
                enquadraRegraCollection.Query.Load();
                //
                // Troca o idCarteira para o novo IdCarteira
                foreach (EnquadraRegra enquadraRegra in enquadraRegraCollection)
                {
                    enquadraRegra.MarkAllColumnsAsDirty(DataRowState.Added);
                    enquadraRegra.IdCarteira = idCarteira;
                }
                if (enquadraRegraCollection.HasData)
                {
                    enquadraRegraCollection.Save();
                }
                #endregion
                //
                bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

                #region Se tem permisão automática para usuários internos, salva a permissão
                if (permissaoInternoAuto && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
                {
                    GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                    usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                    usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Load(usuarioQuery);

                    PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                    foreach (Usuario usuarioPermissao in usuarioCollection)
                    {
                        int idUsuario = usuarioPermissao.IdUsuario.Value;

                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        permissaoCliente.IdCliente = Convert.ToInt32(textIdPessoa.Text);
                        permissaoCliente.IdUsuario = idUsuario;
                        permissaoClienteCollection.AttachEntity(permissaoCliente);
                    }
                    permissaoClienteCollection.Save();
                }
                #endregion

                //Checa se o cliente tem controle = Carteira com Cota ou Carteira Completa
                //e tb não é fundo de Inv nem clube de Inv.
                //Se sim, então cria cotista com o mesmo nr da carteira (testando antes se já não existe)
                if ((cliente.TipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                    cliente.TipoControle == (byte)TipoControleCliente.Completo) &&
                    (!cliente.IsTipoClienteFundo() && !cliente.IsTipoClienteClube()))
                {
                    int idCotista = 0;
                    Cotista cotistaExiste = new Cotista();
                    if (!cotistaExiste.LoadByPrimaryKey(cliente.IdCliente.Value))
                    {
                        Cotista cotistaNovo = new Cotista();
                        cotistaNovo.IdCotista = cliente.IdCliente.Value;
                        cotistaNovo.Nome = cliente.Nome;
                        cotistaNovo.Apelido = cliente.Apelido;
                        cotistaNovo.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                        cotistaNovo.IsentoIR = "S";
                        cotistaNovo.IsentoIOF = "S";
                        cotistaNovo.TipoTributacao = (byte)Financial.InvestidorCotista.Enums.TipoTributacaoCotista.Residente;

                        cotistaNovo.Save();

                        idCotista = cotistaNovo.IdCotista.Value;
                    }

                    #region Se tem permisão automática para usuários internos, salva a permissão
                    if (permissaoInternoAuto)
                    {
                        GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                        UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                        usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                        usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                        UsuarioCollection usuarioCollection = new UsuarioCollection();
                        usuarioCollection.Load(usuarioQuery);

                        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                        foreach (Usuario usuarioPermissao in usuarioCollection)
                        {
                            int idUsuario = usuarioPermissao.IdUsuario.Value;

                            PermissaoCotista permissaoCotista = new PermissaoCotista();
                            permissaoCotista.IdUsuario = idUsuario;
                            permissaoCotista.IdCotista = idCotista;
                            permissaoCotistaCollection.AttachEntity(permissaoCotista);
                        }
                        permissaoCotistaCollection.Save();
                    }
                    #endregion
                }
            }
            #endregion

            #region Cria Cotista baseado no Template
            if (dropTemplateCotista.SelectedIndex != -1)
            {
                int idCotista = Convert.ToInt32(textIdPessoa.Text);
                int idCotistaClonar = Convert.ToInt32(dropTemplateCotista.SelectedItem.Value);

                #region Salva o Cotista de acordo com um Template
                Cotista cotistaReplicado = new Cotista();
                Cotista cotista = cotistaReplicado.ReplicaCotista(idCotista, idCotistaClonar);
                // Troca o Nome e o Apelido
                cotista.Nome = textNome.Text.Trim();
                cotista.Apelido = textApelido.Text.Trim();

                cotista.CodigoInterface = "";
                cotista.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                //
                cotista.Save();
                #endregion

                #region Se tem permisão automática para usuários de back/admin, salva a permissão
                bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

                if (permissaoInternoAuto)
                {
                    GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                    usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                    usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Load(usuarioQuery);


                    PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                    foreach (Usuario usuarioPermissao in usuarioCollection)
                    {
                        int idUsuario = usuarioPermissao.IdUsuario.Value;

                        PermissaoCotista permissaoCotista = new PermissaoCotista();
                        permissaoCotista.IdUsuario = idUsuario;
                        permissaoCotista.IdCotista = Convert.ToInt32(textIdPessoa.Text);
                        permissaoCotistaCollection.AttachEntity(permissaoCotista);
                    }
                    permissaoCotistaCollection.Save();
                }
                #endregion
                //

                #region CAC_XML
                bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
                if (integraCotistaItau)
                {
                    Itau itau = new Itau();
                    cotista.CodigoInterface = itau.EnviaCac_XML(idCotista);
                    cotista.Save();
                }
                #endregion
            }
            #endregion
        }
        #endregion

    }

    #region Callbacks

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit textIdPessoa = pageControl.FindControl("textIdPessoa") as ASPxSpinEdit;
        ASPxTextBox textApelido = pageControl.FindControl("textApelido") as ASPxTextBox;
        TextBox textNome = pageControl.FindControl("textNome") as TextBox;
        ASPxComboBox dropTipo = pageControl.FindControl("dropTipo") as ASPxComboBox;
        ASPxTextBox textCPFCNPJ = pageControl.FindControl("textCPFCNPJ") as ASPxTextBox;
        ASPxComboBox dropValidacao = pageControl.FindControl("dropValidacao") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textIdPessoa);
        controles.Add(textApelido);
        controles.Add(textNome);

        ASPxCheckBox chkOffShore = pageControl.FindControl("chkOffShore") as ASPxCheckBox;
        if (!chkOffShore.Checked)
        {
            controles.Add(dropTipo);
            controles.Add(textCPFCNPJ);
        }

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Confere Tipo Validação Aba Suitability         
        
        // Tipo Validação é obrigatório se suitabilityParametrosWorkflow.AtivaWorkFlow = S

        SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
        suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

        if (suitabilityParametrosWorkflow.es.HasData && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.AtivaWorkFlow)) {
            
            if (suitabilityParametrosWorkflow.AtivaWorkFlow.Trim().ToUpper() == "S") {
                if (dropValidacao.SelectedIndex == -1) {
                    e.Result = "Tipo Validação (Aba Suitability) é obrigatório!";
                    return;
                }
            }        
        }
        #endregion

        #region Verifica CPF/CNPJ
        if (textCPFCNPJ.Text != "" && dropTipo.SelectedIndex != -1 && !chkOffShore.Checked)
        {
            if (dropTipo.SelectedItem.Value.Equals("1"))
            {
                if (!ValidaCPF(textCPFCNPJ.Text))
                {
                    e.Result = "CPF inválido!";
                    return;
                }
            }
            else
            {
                if (!ValidaCNPJ(textCPFCNPJ.Text))
                {
                    e.Result = "CNPJ inválido!";
                    return;
                }
            }
        }
        #endregion

        if (!permiteDocumentoDuplicado && !chkOffShore.Checked)
        {
            if (!string.IsNullOrEmpty(textCPFCNPJ.Text))
            {
                string documento = textCPFCNPJ.Text.Replace(".", "");
                documento = documento.Replace("-", "");
                documento = documento.Replace("/", "");

                PessoaCollection pessoaColl = new PessoaCollection();
                pessoaColl.Query.Where(pessoaColl.Query.IdPessoa.NotEqual(Convert.ToInt32(textIdPessoa.Text))
                                       & pessoaColl.Query.Cpfcnpj.Equal(documento));

                if (pessoaColl.Query.Load())
                {
                    e.Result = "Já existe pessoa cadastrada com o CPF/CNPJ informado!";
                    return;
                }

            }

        }

        #region Confere Tipo Telefone 1
        ASPxTextBox txtDDI = pageControl.FindControl("txtDDI") as ASPxTextBox;
        ASPxTextBox txtDDD = pageControl.FindControl("txtDDD") as ASPxTextBox;
        ASPxTextBox txtNumero = pageControl.FindControl("txtNumero") as ASPxTextBox;
        ASPxTextBox txtRamal = pageControl.FindControl("txtRamal") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone = pageControl.FindControl("dropTipoTelefone") as ASPxComboBox;

        if (txtDDI.Text.Trim() != "" ||
            txtDDD.Text.Trim() != "" ||
            txtNumero.Text.Trim() != "" ||
            txtRamal.Text.Trim() != "")
        {

            if (dropTipoTelefone.SelectedIndex == -1)
            {
                e.Result = "Tipo Telefone1 obrigatório!";
                return;
            }
        }
        #endregion

        #region Confere Tipo Telefone 2
        ASPxTextBox txtDDI1 = pageControl.FindControl("txtDDI1") as ASPxTextBox;
        ASPxTextBox txtDDD1 = pageControl.FindControl("txtDDD1") as ASPxTextBox;
        ASPxTextBox txtNumero1 = pageControl.FindControl("txtNumero1") as ASPxTextBox;
        ASPxTextBox txtRamal1 = pageControl.FindControl("txtRamal1") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone1 = pageControl.FindControl("dropTipoTelefone1") as ASPxComboBox;

        if (txtDDI1.Text.Trim() != "" ||
            txtDDD1.Text.Trim() != "" ||
            txtNumero1.Text.Trim() != "" ||
            txtRamal1.Text.Trim() != "")
        {

            if (dropTipoTelefone1.SelectedIndex == -1)
            {
                e.Result = "Tipo Telefone2 obrigatório!";
                return;
            }
        }
        #endregion

        #region Confere Tipo Telefone 3
        ASPxTextBox txtDDI2 = pageControl.FindControl("txtDDI2") as ASPxTextBox;
        ASPxTextBox txtDDD2 = pageControl.FindControl("txtDDD2") as ASPxTextBox;
        ASPxTextBox txtNumero2 = pageControl.FindControl("txtNumero2") as ASPxTextBox;
        ASPxTextBox txtRamal2 = pageControl.FindControl("txtRamal2") as ASPxTextBox;
        //
        ASPxComboBox dropTipoTelefone2 = pageControl.FindControl("dropTipoTelefone2") as ASPxComboBox;

        if (txtDDI2.Text.Trim() != "" ||
            txtDDD2.Text.Trim() != "" ||
            txtNumero2.Text.Trim() != "" ||
            txtRamal2.Text.Trim() != "")
        {

            if (dropTipoTelefone2.SelectedIndex == -1)
            {
                e.Result = "Tipo Telefone3 obrigatório!";
                return;
            }
        }
        #endregion

        int idPessoa = Convert.ToInt32(textIdPessoa.Text);
        Pessoa pessoa = new Pessoa();

        if (!string.IsNullOrEmpty(textCPFCNPJ.Text))
        {
            if (Convert.ToByte(dropTipo.SelectedItem.Value).Equals((byte)TipoPessoa.Fisica))
            { 
                if (!Utilitario.ValidaCPF(textCPFCNPJ.Text))
                {
                    e.Result = "CPF inválido!";
                    return;
                }
            }
            else
            {
                if (!Utilitario.ValidaCNPJ(textCPFCNPJ.Text))
                {
                    e.Result = "CNPJ inválido!";
                    return;
                }
            }
        }

        if (gridCadastro.IsNewRowEditing)
        {            
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                e.Result = "Registro já existente";
                return;
            }

            ASPxCheckBox chkTemplate = pageControl.FindControl("chkTemplate") as ASPxCheckBox;

            if (chkTemplate.Checked == true)
            {
                if (dropTemplateCarteira.SelectedIndex == -1 && dropTemplateCotista.SelectedIndex == -1)
                {
                    e.Result = "Algum tipo de template deve ser selecionado!";
                    return;
                }
            }
        }

        #region Validações tabPage - Suitability
        ASPxComboBox dropPerfil = pageControl.FindControl("dropPerfil") as ASPxComboBox;
        ASPxComboBox dropDispensado = pageControl.FindControl("dropDispensado") as ASPxComboBox;
        ASPxComboBox dropTipoDispensa = pageControl.FindControl("dropTipoDispensa") as ASPxComboBox;

        if (dropDispensado.SelectedIndex > -1 && Convert.ToString(dropDispensado.SelectedItem.Value).Equals("S") && dropTipoDispensa.SelectedIndex == -1)
        {
            e.Result = "Suitability - Favor informar o tipo de dispensa.";
            return;
        }

        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void callbackAddSinacor_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxComboBox dropTipoImport = popupAddSinacor.FindControl("dropTipoImport") as ASPxComboBox;
        TextBox textListaIdsImportaSinacor = popupAddSinacor.FindControl("textListaIdsImportaSinacor") as TextBox;
        //

        if (String.IsNullOrEmpty(textListaIdsImportaSinacor.Text.Trim()))
        {
            throw new Exception("Campos Obrigatórios.");
        }

        string[] lines = textListaIdsImportaSinacor.Text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        string[] codigosAux = StringExt.RemoveItensDuplicados(lines); // Remove itens Duplicados

        List<int> codigosValidos = new List<int>();

        #region Códigos Válidos
        foreach (string line in codigosAux)
        {
            if (string.IsNullOrEmpty(line))
            {
                continue;
            }
            int codigo;
            if (!Int32.TryParse(line, out codigo))
            {
                throw new Exception("Não foi possível processar o código: " + line + ".\n\nCertifique-se de que cada código está em uma linha separada e que contenha apenas números");
            }
            else
            {
                if (dropTipoImport.SelectedIndex == 0)
                { // Cliente Carteira

                    // Se a Pessoa já existe, não processa-la
                    Pessoa pessoa = new Pessoa();
                    if (!pessoa.LoadByPrimaryKey(codigo))
                    {
                        codigosValidos.Add(codigo);
                    }
                }
                else if (dropTipoImport.SelectedIndex == 1)
                { // Cotista                    
                    codigosValidos.Add(codigo);
                }
            }
        }
        #endregion

        List<string> codigosInvalidos = new List<string>();

        if (dropTipoImport.SelectedIndex == 0)
        { // Cliente Carteira

            #region Processa Monta Códigos Inválidos para Cliente/Carteira
            foreach (int codigo in codigosValidos)
            {
                try
                {
                    ImportarCadastroSinacorClienteCarteira(codigo, false);
                }
                catch (Exception e2)
                {
                    codigosInvalidos.Add(codigo.ToString() + "\n" + e2.Message); // pega inclusive do webservice..
                }
            }
            #endregion
        }
        else if (dropTipoImport.SelectedIndex == 1)
        {
            // Cotista

            #region Processa Monta Códigos Inválidos para Cotista
            foreach (int codigo in codigosValidos)
            {
                try
                {
                    ImportarCadastroSinacorCotista(codigo);
                }
                catch (Exception e2)
                {
                    codigosInvalidos.Add(codigo.ToString() + "\n" + e2.Message);
                }
            }
            #endregion
        }

        if (codigosInvalidos.Count > 0)
        {
            throw new Exception("Processamento concluído. Códigos com problemas: " + String.Join(",", codigosInvalidos.ToArray()));
        }
    }
    #endregion

    private WSRendaFixa.RendaFixa InitWSRendaFixa()
    {
        WSRendaFixa.RendaFixa wsRendaFixa = new WSRendaFixa.RendaFixa();
        WSRendaFixa.ValidateLogin ticket = new WSRendaFixa.ValidateLogin();

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);

        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        ticket.Username = usuario.Login;
        ticket.Password = financialMembershipProvider.UnEncodePassword(usuario.Senha);

        wsRendaFixa.ValidateLoginValue = ticket;
        return wsRendaFixa;
    }

    /// <summary>
    /// Cria Cliente/ Carteira
    /// </summary>
    /// <param name="codigoSinacor"></param>
    public void ImportarCadastroSinacorClienteCarteira(int codigoSinacor, bool fromWebService)
    {
        if (ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService && !fromWebService)
        {
            WSRendaFixa.RendaFixa wsRendaFixa = this.InitWSRendaFixa();

            string msgErro;
            wsRendaFixa.ImportaClienteSinacor(codigoSinacor, out msgErro);
            if (!string.IsNullOrEmpty(msgErro))
            {
                throw new Exception(msgErro);
            }
            return;
        }

        #region TSCCLIBOL
        TscclibolCollection tscclibolCollection = new TscclibolCollection();

        // Throws Exception
        tscclibolCollection.GetTscclibol(codigoSinacor);

        decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
        #endregion

        //Carregar informacoes do Sinacor
        ValoresExcelCadastroCliente item = new ValoresExcelCadastroCliente();
        item.IdCliente = codigoSinacor;

        // Se Achou no Sinacor                    
        #region TSCCLIGER
        // Pega CD_CPFCGC e TP_PESSOA em tsccliger

        TsccligerCollection collCliGer = new TsccligerCollection();
        collCliGer.es.Connection.Name = "Sinacor";
        collCliGer.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        collCliGer.Query.Select(collCliGer.Query.CdCpfcgc,
                                collCliGer.Query.TpPessoa,
                                collCliGer.Query.NmCliente,
                                collCliGer.Query.TpCliente,
                                collCliGer.Query.InPessVinc,
                                collCliGer.Query.DtNascFund,
                                collCliGer.Query.InPoliticoExp)
               .Where(collCliGer.Query.CdCpfcgc == cpfCGC);
        //
        collCliGer.Query.Load();

        if (collCliGer.Count == 0)
        {
            throw new Exception("Não foi possível encontrar informações do cliente na tabela CLIGER: " + codigoSinacor.ToString());
        }

        item.Nome = collCliGer[0].NmCliente;
        if (item.Nome.Length > 255)
        {
            item.Nome = item.Nome.Substring(0, 255);
        }

        item.Apelido = collCliGer[0].NmCliente;
        if (item.Apelido.Length > 40)
        {
            item.Apelido = item.Apelido.Substring(0, 40);
        }

        if (collCliGer[0].TpPessoa.ToUpper() == "F" || collCliGer[0].TpPessoa.ToUpper() == "J")
        {
            item.TipoPessoa = collCliGer[0].TpPessoa.ToUpper() == "F" ? TipoPessoa.Fisica : TipoPessoa.Juridica;
        }

        if (item.TipoPessoa == TipoPessoa.Fisica)
        {
            item.Cnpj = cpfCGC.ToString().PadLeft(11, '0');
        }
        else
        {
            item.Cnpj = cpfCGC.ToString().PadLeft(14, '0');
        }

        item.IsentoIOF = "N";
        item.IsentoIR = "N";
        item.StatusCliente = StatusAtivoCliente.Ativo;
        #endregion

        //Importar cliente utilizando rotinas de importacao 
        //Apesar de ser possivel importar varios registros de pessoa de uma vez, vamos importar um a um
        //pra poder ajustar endereço e email após importar cada pessoa
        ImportacaoBasePage importacaoBasePage = new ImportacaoBasePage();
        importacaoBasePage.ValoresExcelCadastroCliente.Add(item);

        int idTipo = 0;
        if (collCliGer[0].TpCliente == 1 || collCliGer[0].TpCliente == 2)
        {
            idTipo = (int)TipoClienteFixo.ClientePessoaFisica;
        }
        else if (collCliGer[0].TpCliente == 29)
        {
            idTipo = (int)TipoClienteFixo.InvestidorEstrangeiro;
        }
        else if (collCliGer[0].TpCliente == 17 || collCliGer[0].TpCliente == 26)
        {
            idTipo = (int)TipoClienteFixo.Fundo;
        }
        else if (collCliGer[0].TpCliente == 8)
        {
            idTipo = (int)TipoClienteFixo.Clube;
        }
        else
        {
            idTipo = (int)TipoClienteFixo.ClientePessoaJuridica;
        }

        importacaoBasePage.CarregaCadastroCliente(idTipo);

        #region Outras Informacoes Pessoa
        //Complementar importacao com campos que nao estah atualmente na planilha
        Pessoa pessoa = new Pessoa();
        pessoa.LoadByPrimaryKey(codigoSinacor);
        pessoa.DataNascimento = collCliGer[0].DtNascFund;
        pessoa.PessoaPoliticamenteExposta = collCliGer[0].InPoliticoExp;

        TscclicompCollection tscclicompCol = new TscclicompCollection();
        tscclicompCol.es.Connection.Name = "Sinacor";
        tscclicompCol.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        tscclicompCol.Query.Where(tscclicompCol.Query.CdCpfcgc == cpfCGC);
        tscclicompCol.Query.Load();

        if (tscclicompCol.Count > 0)
        {
            if (!String.IsNullOrEmpty(tscclicompCol[0].NmMae))
            {
                pessoa.FiliacaoNomeMae = StringExt.Truncate(tscclicompCol[0].NmMae, 100);
            }
            if (!String.IsNullOrEmpty(tscclicompCol[0].NmPai))
            {
                pessoa.FiliacaoNomeMae = StringExt.Truncate(tscclicompCol[0].NmPai, 100);
            }

            if (!String.IsNullOrEmpty(tscclicompCol[0].SgEstadoNasc))
            {
                pessoa.UFNaturalidade = tscclicompCol[0].SgEstadoNasc;
            }


            #region PessoaDocumento
            //
            PessoaDocumento p = new PessoaDocumento();
            p.IdPessoa = pessoa.IdPessoa;
            p.NumeroDocumento = StringExt.Truncate(tscclicompCol[0].CdDocIdent, 13);
            p.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;
            p.DataExpedicao = tscclicompCol[0].DtDocIdent;
            p.OrgaoEmissor = StringExt.Truncate(tscclicompCol[0].CdOrgEmit, 7);
            //
            p.Save();
            #endregion

            if (!String.IsNullOrEmpty(tscclicompCol[0].IdSexo))
            {
                if (tscclicompCol[0].IdSexo.ToUpper()[0] == 'M' ||
                   tscclicompCol[0].IdSexo.ToUpper()[0] == 'F')
                {

                    pessoa.Sexo = tscclicompCol[0].IdSexo.ToUpper();
                }
            }
        }

        pessoa.Save();
        #endregion

        #region TSCENDE
        TscendeCollection t1 = new TscendeCollection();
        t1.es.Connection.Name = "Sinacor";
        t1.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        t1.Query.Where(t1.Query.CdCpfcgc == cpfCGC);
        //

        t1.Query.Load();

        //
        // Se tem pelo menos 1 endereço para aquela pessoa no Sinacor
        if (t1.Count >= 1)
        {
            // Deleta do Financial e Insere os do Sinacor
            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            //
            pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa == codigoSinacor);
            pessoaEnderecoCollection.Query.Load();
            pessoaEnderecoCollection.MarkAllAsDeleted();

            PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
            //
            pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa == codigoSinacor);
            pessoaTelefoneCollection.Query.Load();
            pessoaTelefoneCollection.MarkAllAsDeleted();

            #region PessoaEndereco
            PessoaEnderecoCollection pEnderecoInsere = new PessoaEnderecoCollection();

            // Para cada Endereço do Sinacor Criar 1 endereço no Financial
            for (int j = 0; j < t1.Count; j++)
            {
                //              
                PessoaEndereco pessoaEndereco = pEnderecoInsere.AddNew();
                //

                pessoaEndereco.IdPessoa = codigoSinacor;

                if (t1[j].InEndeCorr != null)
                {
                    pessoaEndereco.RecebeCorrespondencia = t1[j].InEndeCorr.ToUpper();
                }
                else
                {
                    pessoaEndereco.RecebeCorrespondencia = "N";
                }

                pessoaEndereco.Endereco = t1[j].NmLogradouro.Trim();
                pessoaEndereco.Numero = t1[j].NrPredio.Trim();
                if (t1[j].NmCompEnde != null)
                {
                    pessoaEndereco.Complemento = t1[j].NmCompEnde.Trim();
                }

                pessoaEndereco.Bairro = t1[j].NmBairro.Trim();
                pessoaEndereco.Cidade = t1[j].NmCidade.Trim();

                pessoaEndereco.Cep = t1[j].CdCep.ToString().PadLeft(5, '0') + t1[j].CdCepExt.ToString().PadLeft(3, '0');

                pessoaEndereco.Uf = t1[j].SgEstado.ToUpper();
                pessoaEndereco.Pais = "Brasil";

                pessoaEndereco.TipoEndereco = t1[j].InTipoEnde;
            }

            if (pEnderecoInsere.Count >= 1)
            {

                if (!this.TemRecebeCorrespondencia(pEnderecoInsere))
                {
                    // Coloca RecebeCorrespondencia no primeiro
                    pEnderecoInsere[0].RecebeCorrespondencia = "S";
                }
            }
            #endregion

            #region PessoaTelefone

            PessoaTelefoneCollection pTelefoneInsere = new PessoaTelefoneCollection();

            // Para cada telefone do Sinacor Criar 1 telefone no Financial
            for (int j = 0; j < t1.Count; j++)
            {
                //              
                if (!String.IsNullOrEmpty(t1[j].NrTelefone.ToString()))
                {
                    PessoaTelefone pessoaTelefone = pTelefoneInsere.AddNew();
                    //
                    pessoaTelefone.IdPessoa = codigoSinacor;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = t1[j].CdDddTel.HasValue ? t1[j].CdDddTel.ToString() : "";
                    pessoaTelefone.Numero = t1[j].NrTelefone.ToString();
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                }
            }
            #endregion

            using (esTransactionScope scope = new esTransactionScope())
            {

                pessoaEnderecoCollection.Save(); // Deleta Financial
                //
                pEnderecoInsere.Save(); // Salva Os do Sinacor

                if (pTelefoneInsere.Count >= 1)
                {
                    pessoaTelefoneCollection.Save(); // Deleta Financial
                    //
                    pTelefoneInsere.Save(); // Salva Os do Sinacor
                }
                //
                scope.Complete();
            }
        }
        #endregion

        #region TSCEMAIL
        TscemailCollection t2 = new TscemailCollection();
        t2.es.Connection.Name = "Sinacor";
        t2.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        t2.Query.Where(t2.Query.CdCpfcgc == cpfCGC).OrderBy(t2.Query.InPrincipal.Descending);
        //
        t2.Query.Load();

        if (t2.Count >= 1)
        { // Se tem Email

            #region Deleta PessoaEmail por IdPessoa
            PessoaEmailCollection pessoaEmailCollectionDeletar = new PessoaEmailCollection();
            pessoaEmailCollectionDeletar.Query.Where(pessoaEmailCollectionDeletar.Query.IdPessoa == codigoSinacor);

            pessoaEmailCollectionDeletar.Query.Load();
            pessoaEmailCollectionDeletar.MarkAllAsDeleted();
            #endregion

            PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();

            for (int j = 0; j < t2.Count; j++)
            {
                PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                //
                pessoaEmail.IdPessoa = codigoSinacor;
                pessoaEmail.Email = t2[j].NmEMail.Trim();
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                //
                pessoaEmailCollectionDeletar.Save();
                //
                pessoaEmailCollection.Save();
                //                      
                scope.Complete();
            }
        }
        #endregion

        int idCliente = codigoSinacor;
        Cliente cliente = new Cliente();
        bool existeCliente = cliente.LoadByPrimaryKey(idCliente);

        ClienteBolsa clienteBolsa = new ClienteBolsa();
        bool existeClienteBolsa = clienteBolsa.LoadByPrimaryKey(idCliente);

        ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
        bool existeClienteRendaFixa = clienteRendaFixa.LoadByPrimaryKey(idCliente);

        #region TSCAFINIDADE (Preenche cliente.IdGrupoAfinidade)
        if (tscclibolCollection[0].CdAfinidade != null)
        {
            int idGrupoAfinidade = Convert.ToInt32(tscclibolCollection[0].CdAfinidade);
            GrupoAfinidade grupoAfinidade = new GrupoAfinidade();
            if (!grupoAfinidade.LoadByPrimaryKey(idGrupoAfinidade))
            {
                //Cadastrar grupo de afinidade
                TscafinidadeCollection tscAfinidadeCollection = new TscafinidadeCollection();
                tscAfinidadeCollection.es.Connection.Name = "Sinacor";
                tscAfinidadeCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                tscAfinidadeCollection.Query.Where(tscAfinidadeCollection.Query.CdAfinidade == tscclibolCollection[0].CdAfinidade);
                //
                tscAfinidadeCollection.Query.Load();

                if (tscAfinidadeCollection.Count >= 1)
                {
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.Append("SET IDENTITY_INSERT GrupoAfinidade ON ");
                    sqlBuilder.AppendFormat("INSERT INTO GrupoAfinidade (IdGrupo, Descricao) values ({0}, '{1}')",
                        idGrupoAfinidade, tscAfinidadeCollection[0].DsAfinidade);
                    sqlBuilder.Append("SET IDENTITY_INSERT GrupoAfinidade OFF ");
                    esUtility u = new esUtility();
                    u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
                }
            }
            cliente.IdGrupoAfinidade = idGrupoAfinidade;
        }
        else
        {
            cliente.IdGrupoAfinidade = null;
        }
        #endregion

        if (existeCliente)
        {
            TscdocsCollection t3 = new TscdocsCollection();
            t3.es.Connection.Name = "Sinacor";
            t3.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            //
            t3.Query.Where(t3.Query.CdCpfcgc == cpfCGC);
            //
            t3.Query.Load();

            if (t3.Count > 0)
            {
                cliente.DataAtualizacao = t3[0].DtFichCad;
            }
            else
            {
                cliente.DataAtualizacao = DateTime.Now;
            }

        
            cliente.Save();

        }

        #region TSCASSES (Preenche clienteBolsa.IdAssessor e clienteRendaFixa.IdAssessor)
        if (tscclibolCollection[0].CdAssessor != null)
        {
            int idAssessor = Convert.ToInt32(tscclibolCollection[0].CdAssessor);
            Assessor assessor = new Assessor();
            if (!assessor.LoadByPrimaryKey(idAssessor))
            {
                //Cadastrar assessor
                TscassesCollection tscAssesCollection = new TscassesCollection();
                tscAssesCollection.es.Connection.Name = "Sinacor";
                tscAssesCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                tscAssesCollection.Query.Where(tscAssesCollection.Query.CdAssessor == tscclibolCollection[0].CdAssessor);
                //
                tscAssesCollection.Query.Load();

                if (tscAssesCollection.Count >= 1)
                {
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.Append("SET IDENTITY_INSERT Assessor ON ");
                    sqlBuilder.AppendFormat("INSERT INTO Assessor (IdAssessor, Nome, Apelido) values ({0}, '{1}', '{2}')",
                        idAssessor, tscAssesCollection[0].NmAssessor, tscAssesCollection[0].NmResuAsses);
                    sqlBuilder.Append("SET IDENTITY_INSERT Assessor OFF ");
                    esUtility u = new esUtility();
                    u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
                }
            }
            clienteBolsa.IdAssessor = clienteRendaFixa.IdAssessor = idAssessor;
        }
        else
        {
            clienteBolsa.IdAssessor = clienteRendaFixa.IdAssessor = null;
        }
        #endregion

        if (existeClienteBolsa)
        {
            clienteBolsa.Save();
        }

        if (existeClienteRendaFixa)
        {
            clienteRendaFixa.Save();
        }

        #region TSCCLICTA (Preenche ContaCorrente)
        TscclictaCollection tscclictaCollection = new TscclictaCollection();
        tscclictaCollection.es.Connection.Name = "Sinacor";
        tscclictaCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        tscclictaCollection.Query.Where(tscclictaCollection.Query.CdCliente == tscclibolCollection[0].CdCliente,
            tscclictaCollection.Query.InInativa == "N", tscclictaCollection.Query.InPrincipal == "S");
        //
        tscclictaCollection.Query.Load();

        if (tscclictaCollection.Count >= 1)
        {

            //Ver se banco está cadastrado
            string codigoCompensacao = tscclictaCollection[0].CdBanco;
            if (codigoCompensacao.Length > 5)
            {
                codigoCompensacao = codigoCompensacao.Substring(codigoCompensacao.Length - 5);
            }

            Banco banco = new BancoCollection().BuscaBancoPorCodigoCompensacao(codigoCompensacao);
            if (banco == null)
            {
                //Cadastrar banco
                banco = new Banco();
                banco.CodigoCompensacao = codigoCompensacao;
                banco.Nome = codigoCompensacao;
                banco.Save();
            }

            //Ver se agencia está cadastrada
            string codigoAgencia = tscclictaCollection[0].CdAgencia;
            if (tscclictaCollection[0].DvAgencia != null)
            {
                codigoAgencia += "-" + tscclictaCollection[0].DvAgencia;
            }

            if (codigoAgencia.Length > 10)
            {
                codigoAgencia = codigoAgencia.Substring(codigoAgencia.Length - 10);
            }

            Agencia agencia = new AgenciaCollection().BuscaAgenciaPorBancoECodigo(banco.IdBanco.Value, codigoAgencia);
            if (agencia == null)
            {
                //Cadastrar agencia
                agencia = new Agencia();
                agencia.IdBanco = banco.IdBanco.Value;
                agencia.Codigo = codigoAgencia;
                agencia.Nome = codigoAgencia;
                agencia.Save();
            }

            //Loopar todas as contas do cliente
            string numeroConta = tscclictaCollection[0].NrConta;
            if (tscclictaCollection[0].DvConta != null)
            {
                numeroConta += "-" + tscclictaCollection[0].DvConta;
            }

            if (numeroConta.Length > 10)
            {
                numeroConta = numeroConta.Substring(numeroConta.Length - 10);
            }

            bool contaEncontrada = false;
            ContaCorrenteCollection contas = new ContaCorrenteCollection();
            contas.Query.Where(contas.Query.IdPessoa == idCliente);
            contas.Load(contas.Query);
            foreach (ContaCorrente conta in contas)
            {
                if (conta.IdBanco == banco.IdBanco.Value &&
                    conta.IdAgencia == agencia.IdAgencia.Value && conta.Numero == numeroConta)
                {
                    //Conta encontrada
                    contaEncontrada = true;
                    conta.ContaDefault = "S";
                }
                else
                {
                    conta.ContaDefault = "N";
                }
            }
            contas.Save();

            if (!contaEncontrada)
            {
                //Criar conta
                ContaCorrente contaCorrente = new ContaCorrente();
                contaCorrente.IdBanco = banco.IdBanco.Value;
                contaCorrente.IdAgencia = agencia.IdAgencia.Value;
                contaCorrente.IdPessoa = idCliente;
                contaCorrente.Numero = numeroConta;

                TipoConta tipoConta = new TipoConta();
                tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                tipoConta.Query.Load();
                if (!tipoConta.IdTipoConta.HasValue)
                {
                    tipoConta = new TipoConta();
                    tipoConta.Descricao = "Conta Depósito";
                    tipoConta.Save();
                }
                contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

                contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
                contaCorrente.ContaDefault = "S";

                contaCorrente.Save();
            }
        }
        #endregion
    }

    /// <summary>
    /// Cria Cotista
    /// </summary>
    /// <param name="codigoSinacor"></param>
    public void ImportarCadastroSinacorCotista(int codigoSinacor)
    {
        List<int> codigo = new List<int>();
        codigo.Add(codigoSinacor);

        try
        {
            new Cotista().ImportaCotistaSinacor(codigo);

            #region Se tem permisão automática para usuários de back/admin, salva a permissão
            bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

            if (permissaoInternoAuto)
            {
                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);


                PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (!permissaoCotista.LoadByPrimaryKey(idUsuario, codigoSinacor))
                    {
                        permissaoCotista = new PermissaoCotista();
                        permissaoCotista.IdUsuario = idUsuario;
                        permissaoCotista.IdCotista = codigoSinacor;
                        permissaoCotistaCollection.AttachEntity(permissaoCotista);
                    }
                }
                permissaoCotistaCollection.Save();
            }
            #endregion
        }
        catch (Exception e)
        {
            throw e;
        }

    }

    /// <summary>
    /// Indica se algum elemento da collection tem RecebeCorrespondencia = 'S'
    /// </summary>
    /// <param name="p"></param>
    private bool TemRecebeCorrespondencia(PessoaEnderecoCollection p)
    {
        bool retorno = false;

        for (int i = 0; i < p.Count; i++)
        {
            if (p[i].RecebeCorrespondencia == "S")
            {
                return true;
            }
        }
        return retorno;
    }

    /// <summary>
    /// Collection já ordenada por RecebeCorrespondencia - "S" e "N"
    /// </summary>
    /// <param name="p"></param>
    /// <param name="recebeCorrespondencia">S ou N</param>
    /// <returns>index do primeiro registro com RecebeCorrespondencia igual ao parametro passado
    /// -1 se não achou
    /// </returns>
    private int FindPrimeiroRecebeCorrespondencia(PessoaEnderecoCollection p, string recebeCorrespondencia)
    {
        int retorno = -1;

        for (int i = 0; i < p.Count; i++)
        {
            if (p[i].RecebeCorrespondencia == recebeCorrespondencia.ToUpper())
            {
                return i;
            }
        }
        return retorno;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idPessoas"></param>
    /// Atualizacao dos cadastros no Sinacor
    public void ImportarPessoaSinacor(List<int> idPessoas, bool fromWebService)
    {
        if (ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService && !fromWebService)
        {
            WSRendaFixa.RendaFixa wsRendaFixa = this.InitWSRendaFixa();

            string msgErro;
            wsRendaFixa.AtualizaClientesSinacor(idPessoas.ToArray(), out msgErro);
            if (!string.IsNullOrEmpty(msgErro))
            {
                throw new Exception(msgErro);
            }
            return;
        }

        // Para cada CheckBox selecionado
        for (int i = 0; i < idPessoas.Count; i++)
        {
            try
            {
                int idCliente = idPessoas[i];

                ClienteBolsa clienteBolsa = new ClienteBolsa();

                List<int> codigos = new List<int>();
                codigos = clienteBolsa.RetornaCodigosBovespa(idCliente);

                if (codigos.Count == 0)
                {
                    codigos.Add(idCliente);
                }

                string cpfCnpjPesquisa = "";
                Pessoa pessoaCPF = new Pessoa();
                pessoaCPF.LoadByPrimaryKey(idCliente);
                if (!String.IsNullOrEmpty(pessoaCPF.Cpfcnpj))
                {
                    cpfCnpjPesquisa = Utilitario.RemoveCaracteresEspeciais(pessoaCPF.Cpfcnpj);
                }

                TscclibolCollection tscclibolCollection = new TscclibolCollection();
                //
                tscclibolCollection.es.Connection.Name = "Sinacor";
                tscclibolCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                //tscclibolCollection.Query.Select(tscclibolCollection.Query.CdCpfcgc);

                if (cpfCnpjPesquisa != "")
                {
                    tscclibolCollection.Query.Where(tscclibolCollection.Query.CdCpfcgc.Equal(cpfCnpjPesquisa));
                }
                else
                {
                    tscclibolCollection.Query.Where(tscclibolCollection.Query.CdCliente.In(codigos));
                }

                tscclibolCollection.Query.Load();

                if (tscclibolCollection.Count > 0)
                {
                    // Se Achou no Sinacor                    
                    #region TSCCLIGER
                    // Pega CD_CPFCGC e TP_PESSOA em tsccliger
                    Tsccliger t = new Tsccliger();
                    t.es.Connection.Name = "Sinacor";
                    t.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                    //
                    t.Query.Select(t.Query.CdCpfcgc, t.Query.TpPessoa, t.Query.DtNascFund, t.Query.InPoliticoExp)
                           .Where(t.Query.CdCpfcgc == tscclibolCollection[0].CdCpfcgc);
                    //
                    if (t.Query.Load())
                    {
                        // Se encontrou registro Atualiza no Financial
                        // Carrega CPF e Tipo em Pessoa no Financial
                        Pessoa p = new Pessoa();
                        p.LoadByPrimaryKey(idPessoas[i]);

                        string cpfcnpj = t.CdCpfcgc.Value.ToString();

                        if (t.TpPessoa.ToUpper() == "F" || t.TpPessoa.ToUpper() == "J")
                        {
                            p.Tipo = t.TpPessoa.ToUpper() == "F" ? (byte)1 : (byte)2;
                        }

                        if (p.Tipo.Value == (byte)TipoPessoa.Fisica)
                        {
                            p.Cpfcnpj = t.CdCpfcgc.Value.ToString().PadLeft(11, '0');
                        }
                        else
                        {
                            p.Cpfcnpj = t.CdCpfcgc.Value.ToString().PadLeft(14, '0');
                        }

                        p.DataNascimento = t.DtNascFund;
                        p.PessoaPoliticamenteExposta = t.InPoliticoExp;

                        TscclicompCollection tscclicompCol = new TscclicompCollection();
                        tscclicompCol.es.Connection.Name = "Sinacor";
                        tscclicompCol.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                        //
                        tscclicompCol.Query.Where(tscclicompCol.Query.CdCpfcgc == tscclibolCollection[0].CdCpfcgc);
                        tscclicompCol.Query.Load();

                        if (tscclicompCol.Count > 0)
                        {
                            p.FiliacaoNomeMae = StringExt.Truncate(tscclicompCol[0].NmMae, 100);
                            p.FiliacaoNomePai = StringExt.Truncate(tscclicompCol[0].NmPai, 100);
                            p.UFNaturalidade = tscclicompCol[0].SgEstadoNasc;

                            #region Pessoa Documento
                            //p.DataEmissaoRG = tscclicompCol[0].DtDocIdent;
                            //p.NumeroRG = StringExt.Truncate(tscclicompCol[0].CdDocIdent, 20);
                            //p.EmissorRG = StringExt.Truncate(tscclicompCol[0].CdOrgEmit, 20);
                            //
                            PessoaDocumento p1 = new PessoaDocumento();
                            p1.IdPessoa = p.IdPessoa;
                            p1.NumeroDocumento = StringExt.Truncate(tscclicompCol[0].CdDocIdent, 13);
                            p1.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;
                            p1.DataExpedicao = tscclicompCol[0].DtDocIdent;
                            p1.OrgaoEmissor = StringExt.Truncate(tscclicompCol[0].CdOrgEmit, 7);
                            //
                            p1.Save();
                            //
                            #endregion
                        }

                        p.Save();
                    }
                    #endregion

                    #region TSCENDE
                    TscendeCollection t1 = new TscendeCollection();
                    t1.es.Connection.Name = "Sinacor";
                    t1.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                    //
                    t1.Query.Where(t1.Query.CdCpfcgc == tscclibolCollection[0].CdCpfcgc);
                    //
                    t1.Query.Load();
                    //
                    // Se tem pelo menos 1 endereço para aquela pessoa no Sinacor
                    if (t1.Count >= 1)
                    {

                        #region Deleta do Financial
                        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                        //
                        pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa == idPessoas[i]);
                        pessoaEnderecoCollection.Query.Load();
                        pessoaEnderecoCollection.MarkAllAsDeleted();

                        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
                        //
                        pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa == idPessoas[i]);
                        pessoaTelefoneCollection.Query.Load();
                        pessoaTelefoneCollection.MarkAllAsDeleted();
                        #endregion

                        #region PessoaEndereco
                        PessoaEnderecoCollection pEnderecoInsere = new PessoaEnderecoCollection();

                        // Para cada Endereço do Sinacor Criar 1 endereço no Financial                        
                        for (int j = 0; j < t1.Count; j++)
                        {
                            //                        
                            PessoaEndereco pessoaEndereco = pEnderecoInsere.AddNew();
                            //
                            pessoaEndereco.IdPessoa = idPessoas[i];
                            pessoaEndereco.RecebeCorrespondencia = t1[j].InEndeCorr.ToUpper();

                            pessoaEndereco.Endereco = t1[j].NmLogradouro.Trim();
                            pessoaEndereco.Numero = t1[j].NrPredio.Trim();
                            if (t1[j].NmCompEnde != null)
                            {
                                pessoaEndereco.Complemento = t1[j].NmCompEnde.Trim();
                            }
                            pessoaEndereco.Bairro = t1[j].NmBairro.Trim();
                            pessoaEndereco.Cidade = t1[j].NmCidade.Trim();
                            pessoaEndereco.Cep = t1[j].CdCep.ToString().PadLeft(5, '0') + t1[j].CdCepExt.ToString().PadLeft(3, '0');
                            pessoaEndereco.Uf = t1[j].SgEstado.ToUpper();
                            pessoaEndereco.Pais = "Brasil";
                            pessoaEndereco.TipoEndereco = t1[j].InTipoEnde;
                        }

                        if (pEnderecoInsere.Count >= 1)
                        {
                            if (!this.TemRecebeCorrespondencia(pEnderecoInsere))
                            {
                                // Coloca RecebeCorrespondencia no primeiro
                                pEnderecoInsere[0].RecebeCorrespondencia = "S";
                            }
                        }
                        #endregion

                        #region PessoaTelefone

                        PessoaTelefoneCollection pTelefoneInsere = new PessoaTelefoneCollection();

                        // Para cada telefone do Sinacor Criar 1 telefone no Financial
                        for (int k = 0; k < t1.Count; k++)
                        {
                            //              
                            if (!String.IsNullOrEmpty(t1[k].NrTelefone.ToString()))
                            {
                                PessoaTelefone pessoaTelefone = pTelefoneInsere.AddNew();
                                //

                                pessoaTelefone.IdPessoa = idPessoas[i];
                                pessoaTelefone.Ddi = "";
                                pessoaTelefone.Ddd = t1[k].CdDddTel.HasValue ? t1[k].CdDddTel.ToString() : "";
                                pessoaTelefone.Numero = t1[k].NrTelefone.ToString();
                                pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                            }
                        }
                        #endregion

                        using (esTransactionScope scope = new esTransactionScope())
                        {

                            pessoaEnderecoCollection.Save(); // Deleta Financial
                            //
                            pEnderecoInsere.Save(); // Salva Os do Sinacor

                            if (pTelefoneInsere.Count >= 1)
                            {
                                pessoaTelefoneCollection.Save(); // Deleta Financial
                                //
                                pTelefoneInsere.Save(); // Salva Os do Sinacor
                            }
                            //
                            scope.Complete();
                        }
                    }
                    #endregion

                    #region TSCEMAIL
                    TscemailCollection t2 = new TscemailCollection();
                    t2.es.Connection.Name = "Sinacor";
                    t2.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                    //
                    t2.Query.Where(t2.Query.CdCpfcgc == tscclibolCollection[0].CdCpfcgc)
                            .OrderBy(t2.Query.InPrincipal.Descending);
                    //
                    t2.Query.Load();

                    if (t2.Count >= 1)
                    { // Se tem Email
                        #region Deleta PessoaEmail por IdPessoa
                        PessoaEmailCollection pessoaEmailCollectionDeletar = new PessoaEmailCollection();
                        pessoaEmailCollectionDeletar.Query.Where(pessoaEmailCollectionDeletar.Query.IdPessoa == idPessoas[i]);

                        pessoaEmailCollectionDeletar.Query.Load();
                        pessoaEmailCollectionDeletar.MarkAllAsDeleted();
                        #endregion

                        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();

                        for (int j = 0; j < t2.Count; j++)
                        {
                            PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                            //
                            pessoaEmail.IdPessoa = idPessoas[i];
                            pessoaEmail.Email = t2[j].NmEMail.Trim();
                        }

                        using (esTransactionScope scope = new esTransactionScope())
                        {
                            //
                            pessoaEmailCollectionDeletar.Save();
                            //
                            pessoaEmailCollection.Save();
                            //                      
                            scope.Complete();
                        }
                    }
                    #endregion

                    Cliente cliente = new Cliente();
                    bool existeCliente = cliente.LoadByPrimaryKey(idCliente);
                    bool existeClienteBolsa = clienteBolsa.LoadByPrimaryKey(idCliente);

                    ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
                    bool existeClienteRendaFixa = clienteRendaFixa.LoadByPrimaryKey(idCliente);

                    #region TSCAFINIDADE
                    if (tscclibolCollection[0].CdAfinidade != null)
                    {
                        int idGrupoAfinidade = Convert.ToInt32(tscclibolCollection[0].CdAfinidade);
                        GrupoAfinidade grupoAfinidade = new GrupoAfinidade();
                        if (!grupoAfinidade.LoadByPrimaryKey(idGrupoAfinidade))
                        {
                            //Cadastrar grupo de afinidade
                            TscafinidadeCollection tscAfinidadeCollection = new TscafinidadeCollection();
                            tscAfinidadeCollection.es.Connection.Name = "Sinacor";
                            tscAfinidadeCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            //
                            tscAfinidadeCollection.Query.Where(tscAfinidadeCollection.Query.CdAfinidade == tscclibolCollection[0].CdAfinidade);
                            //
                            tscAfinidadeCollection.Query.Load();

                            if (tscAfinidadeCollection.Count >= 1)
                            {
                                StringBuilder sqlBuilder = new StringBuilder();
                                sqlBuilder.Append("SET IDENTITY_INSERT GrupoAfinidade ON ");
                                sqlBuilder.AppendFormat("INSERT INTO GrupoAfinidade (IdGrupo, Descricao) values ({0}, '{1}')",
                                    idGrupoAfinidade, tscAfinidadeCollection[0].DsAfinidade);
                                sqlBuilder.Append("SET IDENTITY_INSERT GrupoAfinidade OFF ");
                                esUtility u = new esUtility();
                                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
                            }
                        }
                        cliente.IdGrupoAfinidade = idGrupoAfinidade;
                    }
                    else
                    {
                        cliente.IdGrupoAfinidade = null;
                    }
                    #endregion
                    if (existeCliente)
                    {
                        TscdocsCollection t4 = new TscdocsCollection();
                        t4.es.Connection.Name = "Sinacor";
                        t4.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                        //
                        t4.Query.Where(t4.Query.CdCpfcgc == tscclibolCollection[0].CdCpfcgc);
                        //
                        t4.Query.Load();

                        if (t4.Count > 0)
                        {
                            cliente.DataAtualizacao = t4[0].DtFichCad;
                        }
                        else
                        {
                            cliente.DataAtualizacao = DateTime.Now;
                        }

                        cliente.Save();
                    }

                    #region TSCASSES
                    if (tscclibolCollection[0].CdAssessor != null)
                    {
                        int idAssessor = Convert.ToInt32(tscclibolCollection[0].CdAssessor);
                        Assessor assessor = new Assessor();
                        if (!assessor.LoadByPrimaryKey(idAssessor))
                        {
                            //Cadastrar assessor
                            TscassesCollection tscAssesCollection = new TscassesCollection();
                            tscAssesCollection.es.Connection.Name = "Sinacor";
                            tscAssesCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            //
                            tscAssesCollection.Query.Where(tscAssesCollection.Query.CdAssessor == tscclibolCollection[0].CdAssessor);
                            //
                            tscAssesCollection.Query.Load();

                            if (tscAssesCollection.Count >= 1)
                            {
                                StringBuilder sqlBuilder = new StringBuilder();
                                sqlBuilder.Append("SET IDENTITY_INSERT Assessor ON ");
                                sqlBuilder.AppendFormat("INSERT INTO Assessor (IdAssessor, Nome, Apelido) values ({0}, '{1}', '{2}')",
                                    idAssessor, tscAssesCollection[0].NmAssessor, tscAssesCollection[0].NmResuAsses);
                                sqlBuilder.Append("SET IDENTITY_INSERT Assessor OFF ");
                                esUtility u = new esUtility();
                                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
                            }
                        }
                        clienteBolsa.IdAssessor = clienteRendaFixa.IdAssessor = idAssessor;
                    }
                    else
                    {
                        clienteBolsa.IdAssessor = clienteRendaFixa.IdAssessor = null;
                    }

                    if (existeClienteBolsa)
                    {
                        clienteBolsa.Save();
                    }

                    if (existeClienteRendaFixa)
                    {
                        clienteRendaFixa.Save();
                    }
                    #endregion

                    #region TSCCLICTA
                    TscclictaCollection tscclictaCollection = new TscclictaCollection();
                    tscclictaCollection.es.Connection.Name = "Sinacor";
                    tscclictaCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                    //
                    tscclictaCollection.Query.Where(tscclictaCollection.Query.CdCliente == tscclibolCollection[0].CdCliente,
                        tscclictaCollection.Query.InInativa == "N", tscclictaCollection.Query.InPrincipal == "S");
                    //
                    tscclictaCollection.Query.Load();

                    if (tscclictaCollection.Count >= 1)
                    {

                        //Ver se banco está cadastrado
                        string codigoCompensacao = tscclictaCollection[0].CdBanco;

                        Banco banco = new BancoCollection().BuscaBancoPorCodigoCompensacao(codigoCompensacao);
                        if (banco == null)
                        {
                            //Cadastrar banco
                            banco = new Banco();
                            banco.CodigoCompensacao = codigoCompensacao;
                            banco.Nome = codigoCompensacao;
                            banco.Save();
                        }

                        //Ver se agencia está cadastrada
                        string codigoAgencia = tscclictaCollection[0].CdAgencia;
                        if (tscclictaCollection[0].DvAgencia != null)
                        {
                            codigoAgencia += "-" + tscclictaCollection[0].DvAgencia;
                        }

                        Agencia agencia = new AgenciaCollection().BuscaAgenciaPorBancoECodigo(banco.IdBanco.Value, codigoAgencia);
                        if (agencia == null)
                        {
                            //Cadastrar agencia
                            agencia = new Agencia();
                            agencia.IdBanco = banco.IdBanco.Value;
                            agencia.Codigo = codigoAgencia;
                            agencia.Nome = codigoAgencia;
                            agencia.Save();
                        }

                        //Loopar todas as contas do cliente
                        string numeroConta = tscclictaCollection[0].NrConta;
                        if (tscclictaCollection[0].DvConta != null)
                        {
                            numeroConta += "-" + tscclictaCollection[0].DvConta;
                        }

                        bool contaEncontrada = false;
                        ContaCorrenteCollection contas = new ContaCorrenteCollection();
                        contas.Query.Where(contas.Query.IdPessoa == idCliente);
                        contas.Load(contas.Query);
                        foreach (ContaCorrente conta in contas)
                        {
                            if (conta.IdBanco == banco.IdBanco.Value &&
                                conta.IdAgencia == agencia.IdAgencia.Value && conta.Numero == numeroConta)
                            {
                                //Conta encontrada
                                contaEncontrada = true;
                                conta.ContaDefault = "S";
                            }
                            else
                            {
                                conta.ContaDefault = "N";
                            }
                        }
                        contas.Save();

                        if (!contaEncontrada)
                        {
                            //Criar conta
                            ContaCorrente contaCorrente = new ContaCorrente();
                            contaCorrente.IdBanco = banco.IdBanco.Value;
                            contaCorrente.IdAgencia = agencia.IdAgencia.Value;
                            contaCorrente.IdPessoa = idCliente;
                            contaCorrente.Numero = numeroConta;

                            TipoConta tipoConta = new TipoConta();
                            tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                            tipoConta.Query.Load();
                            if (!tipoConta.IdTipoConta.HasValue)
                            {
                                tipoConta = new TipoConta();
                                tipoConta.Descricao = "Conta Depósito";
                                tipoConta.Save();
                            }
                            contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

                            contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
                            contaCorrente.ContaDefault = "S";

                            contaCorrente.Save();
                        }

                    }

                    #endregion
                }
            }
            catch (Exception)
            {
            }

        }


    }

    #region Controla Botão Sinacor
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSinacorOnPreRender(object sender, EventArgs e)
    {
        LinkButton b = sender as LinkButton;

        /* Verifica em Configurações do Sistema se IntegracaoBolsa, IntegracaoBMF ou IntegracaoContaCorrente = Sinacor */
        b.Visible = this.IsConfiguracaoSinacor();
    }

    /// <summary>
    /// Retorna True se IntegracaoBolsa, IntegracaoBMF ou IntegracaoContaCorrente = Sinacor em Configurações 
    /// </summary>
    /// <returns></returns>
    private bool IsConfiguracaoSinacor()
    {

        int integracaoBolsa = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa;
        IntegracaoBolsa integraBolsa = (IntegracaoBolsa)integracaoBolsa;

        int integracaoBMF = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBMF;
        IntegracaoBMF integraBMF = (IntegracaoBMF)integracaoBMF;

        int integracaoCC = ParametrosConfiguracaoSistema.Integracoes.IntegracaoCC;
        IntegracaoCC integraCC = (IntegracaoCC)integracaoCC;

        return integraBolsa == IntegracaoBolsa.Sinacor ||
               integraBMF == IntegracaoBMF.Sinacor ||
               integraCC == IntegracaoCC.Sinacor;
    }
    #endregion

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }
    
    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Verifica se o CPF é válido
    /// </summary>
    /// <param name="cpf"></param>
    /// <returns></returns>
    public static bool ValidaCPF(string vrCPF)
    {
        string valor = vrCPF.Replace(".", "");
        valor = valor.Replace("-", "");

        if (valor.Length != 11)
            return false;

        bool igual = true;
        for (int i = 1; i < 11 && igual; i++)
            if (valor[i] != valor[0])
                igual = false;

        if (igual || valor == "12345678909")
            return false;

        int[] numeros = new int[11];

        for (int i = 0; i < 11; i++)
            numeros[i] = int.Parse(valor[i].ToString());

        int soma = 0;
        for (int i = 0; i < 9; i++)
            soma += (10 - i) * numeros[i];

        int resultado = soma % 11;
        if (resultado == 1 || resultado == 0)
        {
            if (numeros[9] != 0) return false;
        }
        else if (numeros[9] != 11 - resultado) return false;

        soma = 0;
        for (int i = 0; i < 10; i++)
            soma += (11 - i) * numeros[i];

        resultado = soma % 11;
        if (resultado == 1 || resultado == 0)
        {
            if (numeros[10] != 0) return false;
        }
        else if (numeros[10] != 11 - resultado) return false;

        return true;
    }

    protected void callbackIdPessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (gridCadastro.IsNewRowEditing)
        {
            if (idAutomatico)
            {
                PessoaCollection pessoaColl = new PessoaCollection();
                pessoaColl.Query.Select(pessoaColl.Query.IdPessoa.Max());
                pessoaColl.Query.Load();

                int idPessoa = 1;
                idPessoa += Convert.ToInt32(pessoaColl[0].IdPessoa);
                e.Result = idPessoa.ToString();                
            }
        }
    }

    protected void callbackChkOffShore_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "N";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        if (pageControl != null)
        {
            ASPxSpinEdit textIdPessoa = pageControl.FindControl("textIdPessoa") as ASPxSpinEdit;

            if (textIdPessoa != null && !string.IsNullOrEmpty(textIdPessoa.Text))
            {
                int idPessoa = Convert.ToInt32(textIdPessoa.Text);

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(idPessoa);

                e.Result = pessoa.OffShore;
            }
        }
    }
}