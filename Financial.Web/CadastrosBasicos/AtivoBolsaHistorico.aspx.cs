﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

using DevExpress.Web;

using Financial.Bolsa;
using Financial.Common;
using Financial.Util;

using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Web.Common;
using System.Drawing;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_AtivoBolsaHistorico : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSAtivoBolsaHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaHistoricoCollection coll = new AtivoBolsaHistoricoCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending, coll.Query.DataReferencia.Ascending);
        coll.Query.Load();
               
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textCdAtivoBolsa);
        controles.Add(textDataVencimento);
        controles.Add(textDataReferencia);
        
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        string cdAtivoBolsa = Convert.ToString(textCdAtivoBolsa.Text).Trim();

        if (gridCadastro.IsNewRowEditing) {
            AtivoBolsaHistorico ativoBolsaHistorico = new AtivoBolsaHistorico();
            if (ativoBolsaHistorico.LoadByPrimaryKey(Convert.ToDateTime(textDataReferencia.Text), cdAtivoBolsa))
            {
                e.Result = "Registro já existente";
                return;
            }
        }

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
        {
            e.Result = "Código de ativo não existente";
            return;
        }
        

        if (!string.IsNullOrEmpty(textDataVencimento.Text)) {
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataVencimento.Text), LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) {
                e.Result = "Data de vencimento não é dia útil.";
                return;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCdAtivoBolsa_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxTextBox).Enabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        
        //
        AtivoBolsaHistorico ativoBolsaHistorico = new AtivoBolsaHistorico();
        //
        ativoBolsaHistorico.CdAtivoBolsa = textCdAtivoBolsa.Text.Trim().ToUpper();
        ativoBolsaHistorico.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        ativoBolsaHistorico.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);

        ativoBolsaHistorico.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AtivoBolsaHistorico - Operacao: Insert AtivoBolsaHistorico: " + ativoBolsaHistorico.CdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsaHistorico),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
                
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;

        //
        AtivoBolsaHistorico ativoBolsaHistorico = new AtivoBolsaHistorico();

        string cdAtivoBolsa = textCdAtivoBolsa.Text.ToString();

        if (ativoBolsaHistorico.LoadByPrimaryKey(Convert.ToDateTime(textDataReferencia.Text), cdAtivoBolsa)) 
        {
            ativoBolsaHistorico.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);

            ativoBolsaHistorico.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AtivoBolsaHistorico - Operacao: Update AtivoBolsaHistorico: " + cdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsaHistorico),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues(AtivoBolsaHistoricoMetadata.ColumnNames.DataReferencia);
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa);
            for (int i = 0; i < keyValues1.Count; i++) 
            {
                DateTime dataReferencia = Convert.ToDateTime(keyValues[i]);
                string cdAtivoBolsa = Convert.ToString(keyValues1[i]);

                AtivoBolsaHistorico ativoBolsa = new AtivoBolsaHistorico();
                if (ativoBolsa.LoadByPrimaryKey(dataReferencia, cdAtivoBolsa)) 
                {
                    AtivoBolsa ativoBolsaClone = (AtivoBolsa)Utilitario.Clone(ativoBolsa);
                    //
                    ativoBolsa.MarkAsDeleted();
                    ativoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AtivoBolsaHistorico - Operacao: Delete AtivoBolsaHistorico: " + cdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
            e.Properties["cpTextCdAtivoBolsa"] = textCdAtivoBolsa.ClientID;
        }
    }

     /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textCdAtivoBolsa", "textDataVencimento");
        base.gridCadastro_PreRender(sender, e);
    }
}