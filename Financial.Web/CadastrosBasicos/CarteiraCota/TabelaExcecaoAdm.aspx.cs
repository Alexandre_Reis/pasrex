﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using Financial.Util.Enums;
using Financial.Bolsa.Enums;
using Financial.Common;

public partial class CadastrosBasicos_TabelaExcecaoAdm : CadastroBasePage
{
    const int segmentoAcoes = 1;   
    const int segmentoAcoesAtivo = 2; 
    const int segmentoOpcoes = 10;  
    const int segmentoOpcoesAtivo = 11;  
    const int segmentoRendaFixa = 50;  
    const int segmentoRendaFixaPapel = 51;  
    const int segmentoRendaFixaTitulo = 52;  
    const int segmentoFundos = 80;  
    const int segmentoFundosAtivo = 81;  
    const int segmentoFundosGestor = 82;  

    new protected void Page_Load(object sender, EventArgs e) {
        this.FiltroGridTabelaAdm();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaExcecaoAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TabelaExcecaoAdmQuery tabelaExcecaoAdmQuery = new TabelaExcecaoAdmQuery("T");
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("Tta");
        CarteiraQuery carteira = new CarteiraQuery("C");

        tabelaExcecaoAdmQuery.Select(tabelaExcecaoAdmQuery, carteira.IdCarteira, carteira.Apelido,
            tabelaTaxaAdministracaoQuery.DataReferencia
            //, "<'' as DescricaoCompleta >"
            );

        tabelaExcecaoAdmQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(tabelaExcecaoAdmQuery.IdTabela == tabelaTaxaAdministracaoQuery.IdTabela);
        tabelaExcecaoAdmQuery.InnerJoin(carteira).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteira.IdCarteira);
        //
        tabelaExcecaoAdmQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaExcecaoAdmCollection coll = new TabelaExcecaoAdmCollection();
        coll.Load(tabelaExcecaoAdmQuery);

        e.Collection = coll;
    }

    #endregion

    #region PopUp Taxa Administração
    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.TipoCalculo.In((byte)TipoCalculoAdministracao.PercentualPL, (byte)TipoCalculoAdministracao.PercentualPLOuValorFixo));
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idTabela = (int)gridView.GetRowValues(visibleIndex, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);

        //e.Result = idTabela.ToString() + "|" + " - " + descricao;
        e.Result = idTabela.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridTabelaAdm.DataBind();
    }

    #endregion   

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTabela = Convert.ToInt32(e.Parameter);

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            //
            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela == idTabela);

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {
                //int idTabela = Convert.ToString(tab.IdTabela.Value);
                string idCarteira = Convert.ToString(tab.IdCarteira.Value);
                string apelido = Convert.ToString(tab.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                DateTime dataReferencia = tab.DataReferencia.Value;
                //
                texto = "Id:" + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");                        
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditTabelaAdm_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixaPL_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        //
        ASPxButtonEdit btnEditTabelaAdm = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdm") as ASPxButtonEdit;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxButtonEdit btnCodigoAtivo = gridCadastro.FindEditFormTemplateControl("btnCodigoAtivo") as ASPxButtonEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditTabelaAdm, dropSegmento });

        bool codigoAtivoBranco = string.IsNullOrEmpty(btnCodigoAtivo.Text);
        int segmento = Convert.ToInt32(dropSegmento.SelectedItem.Value);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        List<int> lstAtivoObrigatorio = new List<int>();
        lstAtivoObrigatorio.Add(segmentoAcoesAtivo);
        lstAtivoObrigatorio.Add(segmentoOpcoesAtivo);
        lstAtivoObrigatorio.Add(segmentoRendaFixaPapel);
        lstAtivoObrigatorio.Add(segmentoRendaFixaTitulo);
        lstAtivoObrigatorio.Add(segmentoFundosAtivo);

        if (lstAtivoObrigatorio.Contains(segmento) && codigoAtivoBranco)
        {
            e.Result = "Favor informar o Ativo";
            return;
        }

        if (segmentoFundosGestor == segmento && codigoAtivoBranco)
        {
            e.Result = "Favor informar o Gestor";
            return;
        }
        #endregion

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        TabelaExcecaoAdm tabelaExcecaoAdm = new TabelaExcecaoAdm();
        //
        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxButtonEdit btnCodigoAtivo = gridCadastro.FindEditFormTemplateControl("btnCodigoAtivo") as ASPxButtonEdit;
        //

        tabelaExcecaoAdm.IdTabela = Convert.ToInt32(hiddenIdTabela.Text);
        tabelaExcecaoAdm.TipoGrupo = Convert.ToInt32(dropSegmento.SelectedItem.Value);
        tabelaExcecaoAdm.CodigoAtivo = btnCodigoAtivo.Text;
        tabelaExcecaoAdm.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TtabelaEscalonamentoAdm - Operacao: Insert TabelaEscalonamentoAdm: " + tabelaExcecaoAdm.IdTabelaExcecao + "; " + UtilitarioWeb.ToString(tabelaExcecaoAdm),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "DescricaoCompleta")
        {
            int idTabela = Convert.ToInt32(e.GetListSourceFieldValue(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.IdCarteira));
            string apelido = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.Apelido));
            DateTime dataReferencia = Convert.ToDateTime(e.GetListSourceFieldValue(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia));
            //
            e.Value = "id: " + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTabelaExcecao = (int)e.Keys[0];
        TabelaExcecaoAdm tabelaExcecaoAdm = new TabelaExcecaoAdm();
        //
        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxButtonEdit btnCodigoAtivo = gridCadastro.FindEditFormTemplateControl("btnCodigoAtivo") as ASPxButtonEdit;
        //
        if (tabelaExcecaoAdm.LoadByPrimaryKey(idTabelaExcecao))
        {
            tabelaExcecaoAdm.IdTabela = Convert.ToInt32(hiddenIdTabela.Text);
            tabelaExcecaoAdm.TipoGrupo = Convert.ToInt32(dropSegmento.SelectedItem.Value);
            tabelaExcecaoAdm.CodigoAtivo = btnCodigoAtivo.Text;
            tabelaExcecaoAdm.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaExcecaoAdm - Operacao: Update TabelaExcecaoAdm: " + idTabelaExcecao,
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdTabela = gridCadastro.GetSelectedFieldValues(TabelaExcecaoAdmMetadata.ColumnNames.IdTabelaExcecao);

            for (int i = 0; i < keyValuesIdTabela.Count; i++) {
                int idTabelaExcecao = Convert.ToInt32(keyValuesIdTabela[i]);

                TabelaExcecaoAdm tabelaExcecaoAdm = new TabelaExcecaoAdm();
                if (tabelaExcecaoAdm.LoadByPrimaryKey(idTabelaExcecao))
                {
                    //
                    TabelaExcecaoAdm tabelaExcecaoAdmClone = (TabelaExcecaoAdm)Utilitario.Clone(tabelaExcecaoAdm);
                    //

                    tabelaExcecaoAdm.MarkAsDeleted();
                    tabelaExcecaoAdm.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaExcecaoAdm - Operacao: Delete TabelaExcecaoAdm: " + tabelaExcecaoAdmClone.IdTabela + "; " + UtilitarioWeb.ToString(tabelaExcecaoAdmClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {

        this.FocaCampoGrid("btnEditTabelaAdm", "textPercentual");
        base.gridCadastro_PreRender(sender, e);

    }

    /// <summary>
    /// Troca para Like no filtro campo Apelido Carteira.
    /// </summary>
    public void FiltroGridTabelaAdm() {
        GridViewDataColumn colApelidoCarteira = gridTabelaAdm.Columns[CarteiraMetadata.ColumnNames.Apelido] as GridViewDataColumn;
        if (colApelidoCarteira != null) {
            colApelidoCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }

    protected void callBackPopupCodigoAtivo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = e.Parameter;
    }

    protected void gridCodigoAtivo_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        string descricao = gridView.GetRowValues(visibleIndex, "CodigoAtivo").ToString();
        e.Result = descricao;
    }

    protected void EsDSCodigoAtivo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        e.Collection = new TabelaExcecaoAdmCollection();

        if (dropSegmento != null && dropSegmento.SelectedIndex != -1)
        {
            List<int> segmentoSemAtivoEspecificado = new List<int>();
            segmentoSemAtivoEspecificado.Add(segmentoAcoes);
            segmentoSemAtivoEspecificado.Add(segmentoFundos);
            segmentoSemAtivoEspecificado.Add(segmentoRendaFixa);
            segmentoSemAtivoEspecificado.Add(segmentoOpcoes);

            int segmento = Convert.ToInt32(dropSegmento.SelectedItem.Value);

            if (!segmentoSemAtivoEspecificado.Contains(segmento))
            {
                if (segmentoRendaFixaPapel == segmento)
                {
                    PapelRendaFixaCollection coll = new PapelRendaFixaCollection();
                    coll.Query.Select(coll.Query.IdPapel.As("CodigoAtivo"), coll.Query.Descricao);

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
                else if (segmentoRendaFixaTitulo == segmento)
                {
                    TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
                    coll.Query.Select(coll.Query.IdTitulo.As("CodigoAtivo"), coll.Query.DescricaoCompleta.As("Descricao"));

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
                else if (segmentoAcoesAtivo == segmento)
                {
                    AtivoBolsaCollection coll = new AtivoBolsaCollection();
                    coll.Query.Select(coll.Query.CdAtivoBolsa.As("CodigoAtivo"), coll.Query.Descricao);
                    coll.Query.Where(coll.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
                else if (segmentoOpcoesAtivo == segmento)
                {
                    AtivoBolsaCollection coll = new AtivoBolsaCollection();
                    coll.Query.Select(coll.Query.CdAtivoBolsa.As("CodigoAtivo"), coll.Query.Descricao);
                    coll.Query.Where(coll.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
                else if (segmentoFundosAtivo == segmento)
                {
                    CarteiraCollection coll = new CarteiraCollection();
                    coll.Query.Select(coll.Query.IdCarteira.As("CodigoAtivo"), coll.Query.Nome.As("Descricao"));

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
                else if (segmentoFundosGestor == segmento)
                {
                    AgenteMercadoCollection coll = new AgenteMercadoCollection();
                    coll.Query.Select(coll.Query.IdAgente.As("CodigoAtivo"), coll.Query.Nome.As("Descricao"));

                    if (coll.Query.Load())
                        e.Collection = coll;
                }
            }
        }
    }

    protected void gridCodigoAtivo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCodigoAtivo.DataBind();
    }

}