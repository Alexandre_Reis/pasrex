﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaEscalonaAdm.aspx.cs" Inherits="CadastrosBasicos_TabelaEscalonaAdm" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataTabelaAdm(data) {        
        hiddenIdTabela.SetValue(data);
        ASPxCallback2.SendCallback(data);
        popupTabelaAdm.HideWindow();
        btnEditTabelaAdm.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTabelaAdm.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>

    <dxpc:ASPxPopupControl ID="popupTabelaAdm" ClientInstanceName="popupTabelaAdm" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabelaAdm" runat="server" Width="100%"
                    ClientInstanceName="gridTabelaAdm"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaAdm" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabelaAdm_CustomDataCallback" 
                    OnCustomCallback="gridTabelaAdm_CustomCallback"
                    OnHtmlRowCreated="gridTabelaAdm_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" VisibleIndex="0" Width="14%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="60%"/>
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="2" Width="15%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridTabelaAdm.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTabelaAdm);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Taxa Adm." />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabelaAdm.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Escalonamento Adm."/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaEscalonamentoAdm"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdTabela" Visible="false"></dxwgv:GridViewDataColumn>
                                           
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Tabela Adm." UnboundType="String" VisibleIndex="1" Width="70%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="FaixaPL" Caption="Faixa PL" VisibleIndex="1" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"/>
                </dxwgv:GridViewDataSpinEditColumn>

                <dxwgv:GridViewDataSpinEditColumn FieldName="Percentual" Caption="Percentual" VisibleIndex="2" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.000000;(#,##0.000000);0.000000}" SpinButtons-ShowIncrementButtons="false"/>
                </dxwgv:GridViewDataSpinEditColumn>
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdTabela" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTabela")%>' ClientInstanceName="hiddenIdTabela" />
                        
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Taxa Adm.:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTabelaAdm" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditTabelaAdm" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompleta")%>' OnLoad="btnEditTabelaAdm_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTabelaAdm.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                                                                                                                                                                       
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelFaixaPL" runat="server" CssClass="labelRequired" Text="Faixa PL:"/>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="textFaixaPL" runat="server" ClientInstanceName="textFaixaPL"
                                        Text='<%# Eval("FaixaPL") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                        CssClass="textValor_5" OnLoad="textFaixaPL_Load">
                                    </dxe:ASPxSpinEdit>                                
                                </td>                                                        
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelPercentual" runat="server" CssClass="labelRequired" Text="Percentual:"/>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="textPercentual" runat="server" ClientInstanceName="textPercentual"
                                        Text='<%# Eval("Percentual") %>' NumberType="Float" MaxLength="10" DecimalPlaces="6"
                                        CssClass="textPerc">
                                    </dxe:ASPxSpinEdit>
                                </td>                                                        
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaEscalonamentoAdm" runat="server" OnesSelect="EsDSTabelaEscalonamentoAdm_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaAdm" runat="server" OnesSelect="EsDSTabelaAdm_esSelect" />        
    </form>
</body>
</html>