﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;

public partial class CadastrosBasicos_ParametroAdmFundoInvestimento : CadastroBasePage
{
    #region Instância
    //Collection
    AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
    ParametroAdministradorFundoInvestimentoCollection parametroAdministrador = new ParametroAdministradorFundoInvestimentoCollection();

    //Entidade
    ParametroAdministradorFundoInvestimento parametro = new ParametroAdministradorFundoInvestimento();
    #endregion

    #region esDataSources
    protected void EsDSAdministrador_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenteMercadoCollection = new AgenteMercadoCollection();

        agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.IdAgente, agenteMercadoCollection.Query.Nome);
        agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"));
        agenteMercadoCollection.Query.OrderBy(agenteMercadoCollection.Query.Nome.Ascending);
        agenteMercadoCollection.Query.Load();

        e.Collection = agenteMercadoCollection;
    }
    protected void EsDSParametro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        parametroAdministrador = new ParametroAdministradorFundoInvestimentoCollection();

        parametroAdministrador.Query.Select();
        parametroAdministrador.Query.OrderBy(parametroAdministrador.Query.IdParametro.Ascending);
        parametroAdministrador.Query.Load();

        e.Collection = parametroAdministrador;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// Verifica se os campos obrigatorios foram devidamente preenchidos
    /// </summary>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxComboBox dropAdministrador = gridCadastro.FindEditFormTemplateControl("dropAdministrador") as ASPxComboBox;
        ASPxRadioButtonList rblExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = gridCadastro.FindEditFormTemplateControl("rblAliquotaIR") as ASPxRadioButtonList;

        if (dropAdministrador.SelectedItem == null)
            e.Result = "Campo 'Administrador' deve ser preenchido!";
        else if (rblExecucaoRecolhimento.SelectedItem == null)
            e.Result = "Campo 'Execução do Recolhimento' deve ser preenchido!";
        else if (!rblExecucaoRecolhimento.SelectedItem.Value.Equals(1))
        {
            if (rblAliquotaIR.SelectedItem == null)
                e.Result = "Campo 'Alíquota IR' deve ser preenchido!";
        }
        if (gridCadastro.IsNewRowEditing)
        {
            if (VerificaAdministradorParametrizado(Convert.ToInt32(dropAdministrador.SelectedItem.Value)))
                e.Result = "Administrador já cadastrado!";
        }
    }

    /// <summary>
    /// Deleta os registros selecionados na gridCadastro
    /// </summary>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdParametro");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idParametro = Convert.ToInt32(keyValuesId[i]);
                parametro = new ParametroAdministradorFundoInvestimento();

                if (parametro.LoadByPrimaryKey(idParametro))
                {
                    ParametroAdministradorFundoInvestimento parametroClone = (ParametroAdministradorFundoInvestimento)Utilitario.Clone(parametro);

                    parametro.MarkAsDeleted();
                    parametro.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Parametro Administrador - Operacao: Delete Parametro Administrador: " + idParametro + "; " + UtilitarioWeb.ToString(parametroClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        //Instancia controles da tela
        ASPxComboBox dropAdministrador = gridCadastro.FindEditFormTemplateControl("dropAdministrador") as ASPxComboBox;
        ASPxRadioButtonList rblExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = gridCadastro.FindEditFormTemplateControl("rblAliquotaIR") as ASPxRadioButtonList;
        int? AliquotaIR = null;
        if (rblAliquotaIR.SelectedItem != null)
            AliquotaIR = Financial.Util.ConverteTipo.TryParseInt(rblAliquotaIR.SelectedItem.Value.ToString());

        int idParametro = (int)e.Keys[0];
        if (parametro.LoadByPrimaryKey(idParametro))
        {
            parametro.Administrador = Convert.ToInt32(dropAdministrador.SelectedItem.Value);
            parametro.ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
            parametro.AliquotaIR = AliquotaIR;

            parametro.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Parametro - Operacao: Update Parametro: " + parametro.IdParametro + UtilitarioWeb.ToString(parametro),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Atualiza o gridCadastro quando o usuário fechar o EditForm
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o tratamento dos campos
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Administrador")
        {
            agenteMercadoCollection = new AgenteMercadoCollection();

            agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.Nome);
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.IdAgente.Equal(Convert.ToInt32(e.Value)));
            agenteMercadoCollection.Query.Load();

            e.DisplayText = agenteMercadoCollection[0].Nome;
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void Cadastra()
    {
        ASPxComboBox dropAdministrador = gridCadastro.FindEditFormTemplateControl("dropAdministrador") as ASPxComboBox;
        ASPxRadioButtonList rblExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = gridCadastro.FindEditFormTemplateControl("rblAliquotaIR") as ASPxRadioButtonList;
        int? AliquotaIR = null;
        if (rblAliquotaIR.SelectedItem != null)
            AliquotaIR = Financial.Util.ConverteTipo.TryParseInt(rblAliquotaIR.SelectedItem.Value.ToString());

        parametro = new ParametroAdministradorFundoInvestimento();
        parametro.Administrador = Convert.ToInt32(dropAdministrador.SelectedItem.Value);
        parametro.ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
        parametro.AliquotaIR = AliquotaIR;

        parametro.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro parametro - Operacao: Insert parametro: " + parametro.IdParametro + UtilitarioWeb.ToString(parametro),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Verifica se já existe parametrização para o administrador
    /// </summary>
    /// <returns>Condição que verifica se já existe parametrização para o administrador</returns>
    private bool VerificaAdministradorParametrizado(int administrador)
    {
        bool existeParametrizacao = false;

        parametroAdministrador = new ParametroAdministradorFundoInvestimentoCollection();
        parametroAdministrador.Query.Where(parametroAdministrador.Query.Administrador == administrador);
        parametroAdministrador.Query.Load();

        //Verifica se o parâmetro existe para a carteira
        if (parametroAdministrador.Count > 0)
            existeParametrizacao = true;

        return existeParametrizacao;
    }
    #endregion
}