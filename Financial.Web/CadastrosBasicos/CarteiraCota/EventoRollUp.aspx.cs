﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Threading;

public partial class CadastrosBasicos_EventoRollUp : CadastroBasePage
{
    public static int idFundoOffShore = 0;
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region EsSelect
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSEventoRollUp_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SeriesOffShoreQuery serieOrigem = new SeriesOffShoreQuery("serieOrigem");
        SeriesOffShoreQuery serieDestino = new SeriesOffShoreQuery("serieDestino");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        EventoRollUpQuery rollUpQuery = new EventoRollUpQuery("rollUp");
        EventoRollUpCollection coll = new EventoRollUpCollection();

        rollUpQuery.Select(rollUpQuery.IdEventoRollUp, 
                          rollUpQuery.IdFundoOffShore,
                          carteiraQuery.Nome.As("DescricaoCarteira"),
                          rollUpQuery.IdSerieOrigem,
                          serieOrigem.Descricao.As("NomeSerieOrigem"),
                          rollUpQuery.IdSerieDestino,
                          serieDestino.Descricao.As("NomeSerieDestino"),
                          rollUpQuery.DataExecucao,
                          rollUpQuery.DataNav);
        rollUpQuery.InnerJoin(serieOrigem).On(rollUpQuery.IdSerieOrigem.Equal(serieOrigem.IdSeriesOffShore));
        rollUpQuery.InnerJoin(serieDestino).On(rollUpQuery.IdSerieDestino.Equal(serieDestino.IdSeriesOffShore));
        rollUpQuery.InnerJoin(carteiraQuery).On(rollUpQuery.IdFundoOffShore.Equal(carteiraQuery.IdCarteira));  
        rollUpQuery.OrderBy(rollUpQuery.DataExecucao.Descending);

        coll.Load(rollUpQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        ClienteQuery clienteQuery = new ClienteQuery("cliente");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");

        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(carteiraQuery.IdCarteira));
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.OffShore_PJ,(int)TipoClienteFixo.OffShore_PF));
        carteiraQuery.OrderBy(carteiraQuery.IdCarteira.Descending);

        coll.Load(carteiraQuery);
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSSerieOffShore_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SeriesOffShoreCollection coll = new SeriesOffShoreCollection();
        if (idFundoOffShore != 0)
        {
            coll.Query.Where(coll.Query.IdClassesOffShore.Equal(idFundoOffShore) & coll.Query.SerieBase.Coalesce("'N'").NotEqual("S"));
            coll.Query.Load();
        }

        e.Collection = coll;
    }
    #endregion

    #region GridCadastro

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        EventoRollUp eventoRollUp = new EventoRollUp();

        int IdEventoRollUp = (int)e.Keys[0];

        if (eventoRollUp.LoadByPrimaryKey(IdEventoRollUp))
        {
            ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;
            ASPxDateEdit textDataNav = gridCadastro.FindEditFormTemplateControl("textDataNav") as ASPxDateEdit;

            eventoRollUp.DataExecucao = Convert.ToDateTime(textDataExecucao.Text);
            eventoRollUp.DataNav = Convert.ToDateTime(textDataNav.Text);
            eventoRollUp.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EventoRollUp - Operacao: Update EventoRollUp: " + IdEventoRollUp + UtilitarioWeb.ToString(eventoRollUp),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdEventoRollUp");
            for (int i = 0; i < keyValuesId.Count; i++)
            {                
                EventoRollUp eventoRollUp = new EventoRollUp();
                int IdEventoRollUp = Convert.ToInt32(keyValuesId[i]);
                
                if (eventoRollUp.LoadByPrimaryKey(IdEventoRollUp))
                {
                    EventoRollUp eventoRollUpClone = (EventoRollUp)Utilitario.Clone(eventoRollUp);
                    //
                    eventoRollUp.MarkAsDeleted();
                    eventoRollUp.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EventoRollUp - Operacao: Delete EventoRollUp: " + IdEventoRollUp + UtilitarioWeb.ToString(eventoRollUpClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == EventoRollUpMetadata.ColumnNames.IdEventoRollUp)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == EventoRollUpMetadata.ColumnNames.IdEventoRollUp)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        ASPxTextBox hiddenSerieDestino = gridCadastro.FindEditFormTemplateControl("hiddenSerieDestino") as ASPxTextBox;
        ASPxGridLookup dropSerieOrigem = gridCadastro.FindEditFormTemplateControl("dropSerieOrigem") as ASPxGridLookup;
        ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;
        ASPxDateEdit textDataNav = gridCadastro.FindEditFormTemplateControl("textDataNav") as ASPxDateEdit;
        EventoRollUpCollection eventoRollUpColl = new EventoRollUpCollection();

        string[] strSeries = dropSerieOrigem.Text.Split(',');
        foreach (string strSerieOrigem in strSeries)
        {
            EventoRollUp eventoRollUp = eventoRollUpColl.AddNew();
            eventoRollUp.DataExecucao = Convert.ToDateTime(textDataExecucao.Text);
            eventoRollUp.DataNav = Convert.ToDateTime(textDataNav.Text);
            eventoRollUp.IdFundoOffShore = Convert.ToInt32(hiddenCarteira.Text);
            eventoRollUp.IdSerieOrigem = Convert.ToInt32(strSerieOrigem.Trim());
            eventoRollUp.IdSerieDestino = Convert.ToInt32(hiddenSerieDestino.Text); //Base
        }

        eventoRollUpColl.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EventoRollUp - Operacao: Insert EventoRollUp, Lote Séries origem" + dropSerieOrigem.Text,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    #region CallBacks

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieDestino = gridCadastro.FindEditFormTemplateControl("hiddenSerieDestino") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        ASPxGridLookup dropSerieOrigem = gridCadastro.FindEditFormTemplateControl("dropSerieOrigem") as ASPxGridLookup;
        ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;
        ASPxDateEdit textDataNav = gridCadastro.FindEditFormTemplateControl("textDataNav") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenCarteira);
        controles.Add(hiddenSerieDestino);
        controles.Add(textDataExecucao);
        controles.Add(textDataNav);  

        if (gridCadastro.IsNewRowEditing)
            controles.Add(dropSerieOrigem);
        else
            controles.Add(hiddenSerieOrigem);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            DateTime dataNav = Convert.ToDateTime(textDataNav.Text);
            DateTime dataExecucao = Convert.ToDateTime(textDataExecucao.Text);
            #region Valida transacoes x posicoes do mesmo dia
            string[] strSeries = dropSerieOrigem.Text.Split(',');
            List<int> idSeries = new List<int>();

            foreach(string idSerieOrigem in strSeries)
                idSeries.Add(Convert.ToInt32(idSerieOrigem.Trim()));

            EventoRollUpCollection coll = new EventoRollUpCollection();
            coll.Query.Where(coll.Query.IdSerieOrigem.In(idSeries.ToArray()));

            if(coll.Query.Load())
            {
                StringBuilder strRetorno = new StringBuilder();
                strRetorno.Append("Já existe cadastro de RollUp para as seguintes Séries: ");

                foreach(EventoRollUp rollUp in coll)
                {
                    strRetorno.Append(rollUp.IdSerieOrigem.Value).Append(",");
                }
                e.Result = strRetorno.ToString().Remove(strRetorno.Length - 1) + " !";
                return;
            }
            #endregion

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackSerieBase_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        if (!String.IsNullOrEmpty(hiddenCarteira.Text))
        {
            int idCarteira = Convert.ToInt32(hiddenCarteira.Text);
            SeriesOffShore serieOffShore = new SeriesOffShore();
            ClassesOffShoreQuery classeOffShoreQuery = new ClassesOffShoreQuery("classeOffShore");
            SeriesOffShoreQuery seriesOffShoreQuery = new SeriesOffShoreQuery("seriesOffShore");

            seriesOffShoreQuery.InnerJoin(classeOffShoreQuery).On(classeOffShoreQuery.IdClassesOffShore.Equal(seriesOffShoreQuery.IdClassesOffShore));
            seriesOffShoreQuery.Where(classeOffShoreQuery.IdClassesOffShore.Equal(idCarteira) & seriesOffShoreQuery.SerieBase.Equal("S"));

            if (serieOffShore.Load(seriesOffShoreQuery))
            {
                idFundoOffShore = serieOffShore.IdClassesOffShore.Value;
                string idSerieDescricao = serieOffShore.IdSeriesOffShore.Value + " - " + serieOffShore.Descricao;

                e.Result = serieOffShore.IdSeriesOffShore.Value + "|" + idSerieDescricao;
            }
        }               

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        string nome = "";
        string resultado = "";
        idFundoOffShore = 0;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {            
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (cliente.LoadByPrimaryKey(idCarteira) && (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF)))
            {
                carteira.LoadByPrimaryKey(idCarteira);    
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                        {
                            nome = carteira.str.Apelido;
                            resultado = nome + "|" + cliente.DataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idCarteira;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackPopupSerieOffShore_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {             
            SeriesOffShore seriesOffShore = new SeriesOffShore();
            if (seriesOffShore.LoadByPrimaryKey(Convert.ToInt32(e.Parameter)))
                e.Result = e.Parameter + " - " + seriesOffShore.Descricao;
        }
    }
    #endregion

    #region Load
    protected void dropSerieOrigem_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxGridLookup).ClientVisible = false;
        }
    }

    protected void labelSeries_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void lblSerieOrigem_Load(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
        else
        {
            (sender as Label).Visible = true;
        }
    }

    protected void textSerieOrigem_Load(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxTextBox).Visible = false;
        }
        else
        {
            (sender as ASPxTextBox).Visible = true;
        }
    }

    protected void btnEditCodigoCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
        }
    }    
    #endregion    
}