﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_TabelaEscalonaAdm : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.FiltroGridTabelaAdm();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaEscalonamentoAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TabelaEscalonamentoAdmQuery tabelaEscalonamentoAdmQuery = new TabelaEscalonamentoAdmQuery("T");
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("Tta");
        CarteiraQuery carteira = new CarteiraQuery("C");

        tabelaEscalonamentoAdmQuery.Select(tabelaEscalonamentoAdmQuery,  carteira.IdCarteira, carteira.Apelido,
            tabelaTaxaAdministracaoQuery.DataReferencia
            //, "<'' as DescricaoCompleta >"
            );
                        
        tabelaEscalonamentoAdmQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(tabelaEscalonamentoAdmQuery.IdTabela == tabelaTaxaAdministracaoQuery.IdTabela);
        tabelaEscalonamentoAdmQuery.InnerJoin(carteira).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteira.IdCarteira);
        //
        tabelaEscalonamentoAdmQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaEscalonamentoAdmCollection coll = new TabelaEscalonamentoAdmCollection();
        coll.Load(tabelaEscalonamentoAdmQuery);

        e.Collection = coll;
    }

    #endregion

    #region PopUp Taxa Administração
    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idTabela = (int)gridView.GetRowValues(visibleIndex, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);

        //e.Result = idTabela.ToString() + "|" + " - " + descricao;
        e.Result = idTabela.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridTabelaAdm.DataBind();
    }

    #endregion   

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTabela = Convert.ToInt32(e.Parameter);

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            //
            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela == idTabela);

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {                
                string idCarteira = Convert.ToString(tab.IdCarteira.Value);
                string apelido = Convert.ToString(tab.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                DateTime dataReferencia = tab.DataReferencia.Value;
                //
                texto = "Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");                        
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditTabelaAdm_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixaPL_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditTabelaAdm = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdm") as ASPxButtonEdit;
        ASPxSpinEdit textFaixaPL = gridCadastro.FindEditFormTemplateControl("textFaixaPL") as ASPxSpinEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditTabelaAdm, textFaixaPL, textPercentual });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        
        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing) {
            TabelaEscalonamentoAdm tabelaEscalonamentoAdm = new TabelaEscalonamentoAdm();
            if (tabelaEscalonamentoAdm.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTabela.Text), Convert.ToInt32(textFaixaPL.Text))) {
                e.Result = "Registro já existente.!";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        TabelaEscalonamentoAdm tabelaEscalonamentoAdm = new TabelaEscalonamentoAdm();
        //
        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        ASPxSpinEdit textFaixaPL = gridCadastro.FindEditFormTemplateControl("textFaixaPL") as ASPxSpinEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        //
        // Chaves
        int idTabela = Convert.ToInt32(hiddenIdTabela.Text);
        decimal faixaPL = Convert.ToDecimal(textFaixaPL.Text);
        decimal percentual = Convert.ToDecimal(textPercentual.Text);

        tabelaEscalonamentoAdm.IdTabela = idTabela;
        tabelaEscalonamentoAdm.FaixaPL = faixaPL;
        tabelaEscalonamentoAdm.Percentual = percentual;

        tabelaEscalonamentoAdm.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TtabelaEscalonamentoAdm - Operacao: Insert TabelaEscalonamentoAdm: " + tabelaEscalonamentoAdm.IdTabela + "; " + tabelaEscalonamentoAdm.FaixaPL + "; " + UtilitarioWeb.ToString(tabelaEscalonamentoAdm),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idTabela = Convert.ToString(e.GetListSourceFieldValue(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela));
            string faixaPL = Convert.ToString(e.GetListSourceFieldValue(TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL));
            e.Value = idTabela + faixaPL;
        }

        if (e.Column.FieldName == "DescricaoCompleta") {            
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.IdCarteira));
            string apelido = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.Apelido));
            DateTime dataReferencia = Convert.ToDateTime(e.GetListSourceFieldValue(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia));
            //
            e.Value = "Carteira: " +idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TabelaEscalonamentoAdm tabelaEscalonamentoAdm = new TabelaEscalonamentoAdm();
        //
        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        ASPxSpinEdit textFaixaPL = gridCadastro.FindEditFormTemplateControl("textFaixaPL") as ASPxSpinEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        //
        // Chaves
        int idTabela = Convert.ToInt32(hiddenIdTabela.Text);
        decimal faixaPL = Convert.ToDecimal(textFaixaPL.Text);
        //
        if (tabelaEscalonamentoAdm.LoadByPrimaryKey(idTabela, faixaPL)) {
            tabelaEscalonamentoAdm.Percentual = Convert.ToDecimal(textPercentual.Text);
            tabelaEscalonamentoAdm.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaEscalonamentoAdm - Operacao: Update TabelaEscalonamentoAdm: " + idTabela + "; " + faixaPL + "; " + UtilitarioWeb.ToString(tabelaEscalonamentoAdm),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdTabela = gridCadastro.GetSelectedFieldValues(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela);
            List<object> keyValuesFaixaPL = gridCadastro.GetSelectedFieldValues(TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL);

            for (int i = 0; i < keyValuesIdTabela.Count; i++) {
                int idTabela = Convert.ToInt32(keyValuesIdTabela[i]);
                int faixaPL = Convert.ToInt32(keyValuesFaixaPL[i]);

                TabelaEscalonamentoAdm tabelaEscalonamentoAdm = new TabelaEscalonamentoAdm();
                if (tabelaEscalonamentoAdm.LoadByPrimaryKey(idTabela, faixaPL)) {
                    //
                    TabelaEscalonamentoAdm tabelaEscalonamentoAdmClone = (TabelaEscalonamentoAdm)Utilitario.Clone(tabelaEscalonamentoAdm);
                    //

                    tabelaEscalonamentoAdm.MarkAsDeleted();
                    tabelaEscalonamentoAdm.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaEscalonamentoAdm - Operacao: Delete TabelaEscalonamentoAdm: " + tabelaEscalonamentoAdmClone.IdTabela + "; " + tabelaEscalonamentoAdmClone.FaixaPL + UtilitarioWeb.ToString(tabelaEscalonamentoAdmClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTabelaAdm", "textPercentual");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo Apleido Carteira.
    /// </summary>
    public void FiltroGridTabelaAdm() {
        GridViewDataColumn colApelidoCarteira = gridTabelaAdm.Columns[CarteiraMetadata.ColumnNames.Apelido] as GridViewDataColumn;
        if (colApelidoCarteira != null) {
            colApelidoCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}