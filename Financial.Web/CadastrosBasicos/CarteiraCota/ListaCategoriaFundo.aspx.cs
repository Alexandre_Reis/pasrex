﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_ListaCategoriaFundo : Financial.Web.Common.CadastroBasePage {
    protected void EsDSListaCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ListaCategoriaFundoCollection coll = new ListaCategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ListaCategoriaFundo listaCategoriaFundo = new ListaCategoriaFundo();
        int idLista = (int)e.Keys[0];

        if (listaCategoriaFundo.LoadByPrimaryKey(idLista)) {
            listaCategoriaFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            listaCategoriaFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ListaCategoriaFundo - Operacao: Update ListaCategoriaFundo: " + idLista + UtilitarioWeb.ToString(listaCategoriaFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ListaCategoriaFundo listaCategoriaFundo = new ListaCategoriaFundo();

        listaCategoriaFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        listaCategoriaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ListaCategoriaFundo - Operacao: Insert ListaCategoriaFundo: " + listaCategoriaFundo.IdLista + UtilitarioWeb.ToString(listaCategoriaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdLista");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idLista = Convert.ToInt32(keyValuesId[i]);

                ListaCategoriaFundo listaCategoriaFundo = new ListaCategoriaFundo();
                if (listaCategoriaFundo.LoadByPrimaryKey(idLista)) {
                    CategoriaFundoCollection categoriaFundoCollection = new CategoriaFundoCollection();
                    categoriaFundoCollection.Query.Where(categoriaFundoCollection.Query.IdLista.Equal(idLista));
                    if (categoriaFundoCollection.Query.Load()) {
                        throw new Exception("Lista " + listaCategoriaFundo.Descricao + " não pode ser excluído por ter categorias/subcategorias relacionadas.");
                    }

                    ListaCategoriaFundo listaCategoriaFundoClone = (ListaCategoriaFundo)Utilitario.Clone(listaCategoriaFundo);
                    //


                    listaCategoriaFundo.MarkAsDeleted();
                    listaCategoriaFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ListaCategoriaFundo - Operacao: Delete ListaCategoriaFundo: " + idLista + UtilitarioWeb.ToString(listaCategoriaFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}