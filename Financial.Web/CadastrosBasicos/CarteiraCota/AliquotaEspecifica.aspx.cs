﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Contabil;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_CarteiraCota_AliquotaEspecifica : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources

    protected void EsDSAliquotaEspecifica_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AliquotaEspecificaQuery aliquotaEspecificaQuery = new AliquotaEspecificaQuery("AE");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        aliquotaEspecificaQuery.Select(aliquotaEspecificaQuery, carteiraQuery.Nome);
        aliquotaEspecificaQuery.InnerJoin(carteiraQuery).On(aliquotaEspecificaQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        aliquotaEspecificaQuery.OrderBy(carteiraQuery.Nome.Ascending);

        AliquotaEspecificaCollection coll = new AliquotaEspecificaCollection();
        coll.Load(aliquotaEspecificaQuery);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    #region Insert/Update/Delete
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataValidade = gridCadastro.FindEditFormTemplateControl("textDataValidade") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        AliquotaEspecifica aliquotaEspecifica = new AliquotaEspecifica();
        aliquotaEspecifica.LoadByPrimaryKey(Convert.ToDateTime(textDataValidade.Text), Convert.ToInt32(btnEditCodigoCarteira.Text));

        aliquotaEspecifica.Taxa = Convert.ToDecimal(textTaxa.Text);

        aliquotaEspecifica.Save();        

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AliquotaEspecifica - Operacao: Update aliquotaEspecifica: " + btnEditCodigoCarteira.Text + " - " + textDataValidade + UtilitarioWeb.ToString(aliquotaEspecifica),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataValidade_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxDateEdit).Enabled = false;
            (sender as ASPxDateEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxDateEdit textDataValidade = gridCadastro.FindEditFormTemplateControl("textDataValidade") as ASPxDateEdit;

        AliquotaEspecifica aliquotaEspecifica = new AliquotaEspecifica();

        aliquotaEspecifica.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        aliquotaEspecifica.Taxa = Convert.ToDecimal(textTaxa.Text);
        aliquotaEspecifica.DataValidade = Convert.ToDateTime(textDataValidade.Text);

        aliquotaEspecifica.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AliquotaEspecifica - Operacao: Insert aliquotaEspecifica: " + aliquotaEspecifica.IdCarteira + "-" + aliquotaEspecifica.DataValidade.ToString() + UtilitarioWeb.ToString(aliquotaEspecifica),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(AliquotaEspecificaMetadata.ColumnNames.DataValidade);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(AliquotaEspecificaMetadata.ColumnNames.IdCarteira);
            for (int i = 0; i < keyValues1.Count; i++) {
                DateTime DataValidade = Convert.ToDateTime(keyValues1[i]);
                int idCarteira = Convert.ToInt32(keyValues2[i]);
                AliquotaEspecifica aliquotaEspecifica = new AliquotaEspecifica();
                if (aliquotaEspecifica.LoadByPrimaryKey(DataValidade, idCarteira))
                {
                    AliquotaEspecifica aliquotaEspecificaClone = (AliquotaEspecifica)Utilitario.Clone(aliquotaEspecifica);
                    //

                    aliquotaEspecifica.MarkAsDeleted();
                    aliquotaEspecifica.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Aliquota Especifica - Operacao: Delete aliquotaEspecifica: " + idCarteira.ToString() + "-" + DataValidade.ToString() + UtilitarioWeb.ToString(aliquotaEspecificaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion



                }

            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    #endregion


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(AliquotaEspecificaMetadata.ColumnNames.IdCarteira));
            string dataValidade = Convert.ToString(e.GetListSourceFieldValue(AliquotaEspecificaMetadata.ColumnNames.DataValidade));
            e.Value = idCarteira + "-" + dataValidade;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxDateEdit textDataValidade = gridCadastro.FindEditFormTemplateControl("textDataValidade") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textTaxa);
        controles.Add(textDataValidade);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

}