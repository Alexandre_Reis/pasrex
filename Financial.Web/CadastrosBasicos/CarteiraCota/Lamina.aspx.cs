﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using DevExpress.Web.Data;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Contabil;
using Financial.WebConfigConfiguration;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;


public partial class CadastrosBasicos_Lamina : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;

        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLamina_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        LaminaQuery laminaQuery = new LaminaQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        laminaQuery.Select(laminaQuery, carteiraQuery.IdCarteira, carteiraQuery.Apelido);
        laminaQuery.InnerJoin(carteiraQuery).On(laminaQuery.IdCarteira == carteiraQuery.IdCarteira);
        laminaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == laminaQuery.IdCarteira);
        laminaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        laminaQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                          permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        laminaQuery.OrderBy(carteiraQuery.IdCarteira.Ascending, laminaQuery.InicioVigencia.Ascending);

        LaminaCollection coll = new LaminaCollection();
        coll.Load(laminaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void EsDSLaminaClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        LaminaCollection coll = new LaminaCollection();

        LaminaQuery info = new LaminaQuery("e");
        CarteiraQuery c = new CarteiraQuery("C");

        info.Select(info.IdCarteira, info.InicioVigencia, c.Nome.Trim(), c.Nome.As("DataInicioVigenciaString"),
                   (info.IdCarteira.Cast(esCastType.String) + "-" + info.InicioVigencia.Cast(esCastType.String)).As("CompositeKey")
            );
        info.InnerJoin(c).On(info.IdCarteira == c.IdCarteira);
        //
        info.OrderBy(info.IdCarteira.Ascending, info.InicioVigencia.Descending);

        coll.Load(info);

        for (int i = 0; i < coll.Count; i++) {
            DateTime dataVigencia = Convert.ToDateTime(coll[i].GetColumn(LaminaMetadata.ColumnNames.InicioVigencia));
            //
            //DateTime novaData = new DateTime(dataVigencia.Year, dataVigencia.Month, dataVigencia.Day);

            coll[i].SetColumn("DataInicioVigenciaString", dataVigencia.ToString("d"));
        }

        //
        e.Collection = coll;
    }

    protected void EsDSCarteiraClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);
        //        
        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// Faz Clonagem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackClonar_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropLamina);
        controles.Add(dropCarteiraDestino);
        controles.Add(textDataClonar);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Confere registro já existente
        Lamina infoVelha = new Lamina();
        infoVelha.LoadByPrimaryKey(Convert.ToInt32(dropCarteiraDestino.Text.Trim()), Convert.ToDateTime(textDataClonar.Value));

        if (infoVelha.es.HasData) {
            e.Result = "Registro Já existente";
            return;
        }
        #endregion

        string selecionado = dropLamina.Text.Trim();
        string[] selecionadoAux = selecionado.Split(new Char[] { '-' });
        int idCarteira = Convert.ToInt32(selecionadoAux[0].Trim());
        DateTime data = Convert.ToDateTime(selecionadoAux[2].Trim());
        //        

        // Carrega Informação velha
        Lamina infoNova = new Lamina();
        infoNova.LoadByPrimaryKey(idCarteira, data);

        infoNova.MarkAllColumnsAsDirty(DataRowState.Added);

        // Acrescenta nova Carteira e Data
        infoNova.IdCarteira = Convert.ToInt32(dropCarteiraDestino.Text.Trim());
        infoNova.InicioVigencia = Convert.ToDateTime(textDataClonar.Value);
        //
        infoNova.Save();

        //
        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        #region Elementos
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxTextBox textEnderecoEletronico = pageControl.FindControl("textEnderecoEletronico") as ASPxTextBox;
        TextBox textDescricaoPublicoAlvo = pageControl.FindControl("textDescricaoPublicoAlvo") as TextBox;
        TextBox textDescricaoObjetivoFundo = pageControl.FindControl("textObjetivoFundo") as TextBox;
        TextBox textDescricaoPoliticaInvestimento = pageControl.FindControl("textDescricaoPoliticaInvestimento") as TextBox;
        ASPxComboBox dropRisco = pageControl.FindControl("dropRisco") as ASPxComboBox;
        ASPxSpinEdit textLimiteAplicacaoExterior = pageControl.FindControl("textLimiteAplicacaoExterior") as ASPxSpinEdit;
        TextBox textFundosInvestimento = pageControl.FindControl("textFundosInvestimento") as TextBox;
        ASPxSpinEdit textLimiteCreditoPrivado = pageControl.FindControl("textLimiteCreditoPrivado") as ASPxSpinEdit;
        TextBox textFundosInvestimentoCotas = pageControl.FindControl("textFundosInvestimentoCotas") as TextBox;
        ASPxComboBox dropDerivativosProtecaoCarteira = pageControl.FindControl("dropDerivativosProtecaoCarteira") as ASPxComboBox;
        ASPxSpinEdit textLimiteAlavancagem = pageControl.FindControl("textLimiteAlavancagem") as ASPxSpinEdit;
        //
        ASPxSpinEdit textLimiteConcentracaoEmissor = pageControl.FindControl("textLimiteConcentracaoEmissor") as ASPxSpinEdit;
        //
        ASPxComboBox dropEstrategiaPerdas = pageControl.FindControl("dropEstrategiaPerdas") as ASPxComboBox;
        TextBox textRegulamentoPerdasPatrimoniais = pageControl.FindControl("textRegulamentoPerdasPatrimoniais") as TextBox;
        TextBox textRegulamentoPatrimonioNegativo = pageControl.FindControl("textRegulamentoPatrimonioNegativo") as TextBox;
        TextBox textPrazoCarencia = pageControl.FindControl("textPrazoCarencia") as TextBox;
        TextBox textTaxaEntrada = pageControl.FindControl("textTaxaEntrada") as TextBox;
        TextBox textTaxaSaida = pageControl.FindControl("textTaxaSaida") as TextBox;
        ASPxComboBox dropFundoEstruturado = pageControl.FindControl("dropFundoEstruturado") as ASPxComboBox;
        TextBox textCenariosApuracaoRentabilidade = pageControl.FindControl("textCenariosApuracaoRentabilidade") as TextBox;
        TextBox textDesempenhoFundo = pageControl.FindControl("textDesempenhoFundo") as TextBox;
        TextBox textPoliticaDistribuicao = pageControl.FindControl("textPoliticaDistribuicao") as TextBox;
        ASPxTextBox textTelefone = pageControl.FindControl("textTelefone") as ASPxTextBox;
        ASPxTextBox textUrlAtendimento = pageControl.FindControl("textUrlAtendimento") as ASPxTextBox;
        TextBox textReclamacoes = pageControl.FindControl("textReclamacoes") as TextBox;

        ASPxCheckBox existeCarencia = pageControl.FindControl("existeCarencia") as ASPxCheckBox;
        ASPxCheckBox existeTaxaEntrada = pageControl.FindControl("existeTaxaEntrada") as ASPxCheckBox;
        ASPxCheckBox existeTaxaSaida = pageControl.FindControl("existeTaxaSaida") as ASPxCheckBox;

        ASPxCheckBox indicaVersao1 = pageControl.FindControl("chkVersao1") as ASPxCheckBox;
        #endregion

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();

        controles = new List<Control>(
                new Control[] { btnCarteira, textVigencia, textEnderecoEletronico, 
                            textDescricaoPublicoAlvo, textDescricaoObjetivoFundo,
                            textDescricaoPoliticaInvestimento, dropRisco,
                            textLimiteAplicacaoExterior, textFundosInvestimento,
                            textLimiteCreditoPrivado, textFundosInvestimentoCotas,
                            dropDerivativosProtecaoCarteira, textLimiteAlavancagem,
                            dropEstrategiaPerdas, textRegulamentoPerdasPatrimoniais,
                            textRegulamentoPatrimonioNegativo,                                 
                            dropFundoEstruturado, textCenariosApuracaoRentabilidade, 
                            textDesempenhoFundo, textPoliticaDistribuicao, 
                            textTelefone, textUrlAtendimento, textReclamacoes
                          });

        if (existeCarencia.Checked) {
            controles.Add(textPrazoCarencia);
        }
        if (existeTaxaEntrada.Checked) {
            controles.Add(textTaxaEntrada);
        }
        if (existeTaxaSaida.Checked) {
            controles.Add(textTaxaSaida);
        }
        // Se está checado valida LimiteConcentracaoEmissor
        if (indicaVersao1.Checked) {
            controles.Add(textLimiteConcentracaoEmissor);
        }

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idCarteira = Convert.ToInt32(btnCarteira.Text);
        DateTime data = Convert.ToDateTime(textVigencia.Text);

        if (gridCadastro.IsNewRowEditing) {
            Lamina lamina = new Lamina();
            if (lamina.LoadByPrimaryKey(idCarteira, data)) {
                e.Result = "Registro já existente.";
            }
        }
        else { // Conferencia de registro existente no Update
            
            ASPxTextBox chaveTextBox = pageControl.FindControl("CompositeKeyHidden") as ASPxTextBox;

            string chave = Convert.ToString(chaveTextBox.Text);
            string[] split = chave.Split('-');
            string idCarteiraOld = Convert.ToString(split[0]);
            string vigenciaOld = Convert.ToString(split[1]);

            string idCarteiraNova = btnCarteira.Text.ToString();
            string vigenciaNova = textVigencia.Text.ToString();

            // Houve mudança de Chave
            if (vigenciaOld.Substring(0, 10).Trim() != vigenciaNova.Substring(0, 10).Trim()) {
                // Confere registro já existente
                Lamina lamina1 = new Lamina();

                if (lamina1.LoadByPrimaryKey( Convert.ToInt32(idCarteira), Convert.ToDateTime(vigenciaNova) ) ) {
                    e.Result = "Registro já existente.";
                }
            }
        }

    }
    
    #region Modo Update/Insert

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataInicioVigencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            //(sender as ASPxDateEdit).Enabled = false;
        }
    }

    protected void btnEditCodigoCarteira_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
        }
    }

    protected void textPrazoCarencia_Init(object sender, EventArgs e) {
        // Se for Update
        if (!gridCadastro.IsNewRowEditing) {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxCheckBox existeCarencia = pageControl.FindControl("existeCarencia") as ASPxCheckBox;
            //
            (sender as TextBox).Enabled = existeCarencia.CheckState == DevExpress.Web.CheckState.Checked;
        }
        else { // Insert
            (sender as TextBox).Enabled = false;
        }
    }

    protected void textTaxaEntrada_Init(object sender, EventArgs e) {
        // Se for Update
        if (!gridCadastro.IsNewRowEditing) {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxCheckBox existeTaxaEntrada = pageControl.FindControl("existeTaxaEntrada") as ASPxCheckBox;
            //
            (sender as TextBox).Enabled = existeTaxaEntrada.CheckState == DevExpress.Web.CheckState.Checked;
        }
        else { // Insert
            (sender as TextBox).Enabled = false;
        }
    }

    protected void textTaxaSaida_Init(object sender, EventArgs e) {
        // Se for Update
        if (!gridCadastro.IsNewRowEditing) {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxCheckBox existeTaxaSaida = pageControl.FindControl("existeTaxaSaida") as ASPxCheckBox;
            //
            (sender as TextBox).Enabled = existeTaxaSaida.CheckState == DevExpress.Web.CheckState.Checked;
        }
        else { // Insert
            (sender as TextBox).Enabled = false;
        }
    }

    protected void versao_Init(object sender, EventArgs e) {
        // Se for Update
        if (!gridCadastro.IsNewRowEditing) {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxSpinEdit textLimiteConcentracaoEmissor = pageControl.FindControl("textLimiteConcentracaoEmissor") as ASPxSpinEdit;
            //
            (sender as ASPxCheckBox).Checked = !String.IsNullOrEmpty(textLimiteConcentracaoEmissor.Text.Trim());
        }
        else { // Insert
            (sender as ASPxCheckBox).Checked = false;
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        #region Elementos
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxTextBox textEnderecoEletronico = pageControl.FindControl("textEnderecoEletronico") as ASPxTextBox;
        TextBox textDescricaoPublicoAlvo = pageControl.FindControl("textDescricaoPublicoAlvo") as TextBox;
        TextBox textDescricaoObjetivoFundo = pageControl.FindControl("textObjetivoFundo") as TextBox;
        TextBox textDescricaoPoliticaInvestimento = pageControl.FindControl("textDescricaoPoliticaInvestimento") as TextBox;
        ASPxComboBox dropRisco = pageControl.FindControl("dropRisco") as ASPxComboBox;
        ASPxSpinEdit textLimiteAplicacaoExterior = pageControl.FindControl("textLimiteAplicacaoExterior") as ASPxSpinEdit;
        TextBox textFundosInvestimento = pageControl.FindControl("textFundosInvestimento") as TextBox;
        ASPxSpinEdit textLimiteCreditoPrivado = pageControl.FindControl("textLimiteCreditoPrivado") as ASPxSpinEdit;
        TextBox textFundosInvestimentoCotas = pageControl.FindControl("textFundosInvestimentoCotas") as TextBox;
        ASPxComboBox dropDerivativosProtecaoCarteira = pageControl.FindControl("dropDerivativosProtecaoCarteira") as ASPxComboBox;
        ASPxSpinEdit textLimiteAlavancagem = pageControl.FindControl("textLimiteAlavancagem") as ASPxSpinEdit;
        //
        ASPxSpinEdit textLimiteConcentracaoEmissor = pageControl.FindControl("textLimiteConcentracaoEmissor") as ASPxSpinEdit;
        //
        ASPxComboBox dropEstrategiaPerdas = pageControl.FindControl("dropEstrategiaPerdas") as ASPxComboBox;
        TextBox textRegulamentoPerdasPatrimoniais = pageControl.FindControl("textRegulamentoPerdasPatrimoniais") as TextBox;
        TextBox textRegulamentoPatrimonioNegativo = pageControl.FindControl("textRegulamentoPatrimonioNegativo") as TextBox;
        TextBox textPrazoCarencia = pageControl.FindControl("textPrazoCarencia") as TextBox;
        TextBox textTaxaEntrada = pageControl.FindControl("textTaxaEntrada") as TextBox;
        TextBox textTaxaSaida = pageControl.FindControl("textTaxaSaida") as TextBox;
        ASPxComboBox dropFundoEstruturado = pageControl.FindControl("dropFundoEstruturado") as ASPxComboBox;
        TextBox textCenariosApuracaoRentabilidade = pageControl.FindControl("textCenariosApuracaoRentabilidade") as TextBox;
        TextBox textDesempenhoFundo = pageControl.FindControl("textDesempenhoFundo") as TextBox;
        TextBox textPoliticaDistribuicao = pageControl.FindControl("textPoliticaDistribuicao") as TextBox;
        ASPxTextBox textTelefone = pageControl.FindControl("textTelefone") as ASPxTextBox;
        ASPxTextBox textUrlAtendimento = pageControl.FindControl("textUrlAtendimento") as ASPxTextBox;
        TextBox textReclamacoes = pageControl.FindControl("textReclamacoes") as TextBox;

        ASPxCheckBox existeCarencia = pageControl.FindControl("existeCarencia") as ASPxCheckBox;
        ASPxCheckBox existeTaxaEntrada = pageControl.FindControl("existeTaxaEntrada") as ASPxCheckBox;
        ASPxCheckBox existeTaxaSaida = pageControl.FindControl("existeTaxaSaida") as ASPxCheckBox;

        #endregion

        string chave = Convert.ToString(e.Keys[0]);
        string[] split = chave.Split('-');
        string idCarteiraOld = Convert.ToString(split[0]);
        string vigenciaOld = Convert.ToString(split[1]);

        string idCarteira = btnCarteira.Text.ToString();
        string vigencia = textVigencia.Text.ToString();
        //
        Lamina lamina = new Lamina();
        if (lamina.LoadByPrimaryKey(Convert.ToInt32(idCarteira), Convert.ToDateTime(vigenciaOld))) {

            #region Lamina
            //lamina.InicioVigencia = Convert.ToDateTime(textVigencia.Text);

            lamina.EnderecoEletronico = Convert.ToString(textEnderecoEletronico.Text);
            lamina.DescricaoPublicoAlvo = Convert.ToString(textDescricaoPublicoAlvo.Text);
            lamina.ObjetivoFundo = Convert.ToString(textDescricaoObjetivoFundo.Text);
            lamina.DescricaoPoliticaInvestimento = Convert.ToString(textDescricaoPoliticaInvestimento.Text);
            if (dropRisco.SelectedIndex > 0) {
                lamina.Risco = Convert.ToInt32(dropRisco.SelectedItem.Value);
            }
            else {
                lamina.Risco = null;
            }
            if (!String.IsNullOrEmpty(textLimiteAplicacaoExterior.Text)) {
                lamina.LimiteAplicacaoExterior = Convert.ToDecimal(textLimiteAplicacaoExterior.Text);
            }
            else {
                lamina.LimiteAplicacaoExterior = null;
            }
            lamina.FundosInvestimento = Convert.ToString(textFundosInvestimento.Text);

            if (!String.IsNullOrEmpty(textLimiteCreditoPrivado.Text)) {
                lamina.LimiteCreditoPrivado = Convert.ToDecimal(textLimiteCreditoPrivado.Text);
            }
            else {
                lamina.LimiteCreditoPrivado = null;
            }
            lamina.FundosInvestimentoCotas = Convert.ToString(textFundosInvestimentoCotas.Text);
            if (dropDerivativosProtecaoCarteira.SelectedIndex > 0) {
                lamina.DerivativosProtecaoCarteira = Convert.ToString(dropDerivativosProtecaoCarteira.SelectedItem.Value);
            }
            else {
                lamina.DerivativosProtecaoCarteira = null;
            }
            if (!String.IsNullOrEmpty(textLimiteAlavancagem.Text)) {
                lamina.LimiteAlavancagem = Convert.ToDecimal(textLimiteAlavancagem.Text);
            }
            else {
                lamina.LimiteAlavancagem = null;
            }
            if (!String.IsNullOrEmpty(textLimiteConcentracaoEmissor.Text)) {
                lamina.LimiteConcentracaoEmissor = Convert.ToDecimal(textLimiteConcentracaoEmissor.Text);
            }
            else {
                lamina.LimiteConcentracaoEmissor = null;
            }
            if (dropFundoEstruturado.SelectedIndex > 0) {
                lamina.FundoEstruturado = Convert.ToString(dropFundoEstruturado.SelectedItem.Value);
            }
            else {
                lamina.FundoEstruturado = null;
            }
            //
            lamina.ExisteCarencia = existeCarencia.CheckState == CheckState.Checked ? "S" : "N";
            lamina.ExisteTaxaEntrada = existeTaxaEntrada.CheckState == CheckState.Checked ? "S" : "N";
            lamina.ExisteTaxaSaida = existeTaxaSaida.CheckState == CheckState.Checked ? "S" : "N";
            //

            if (lamina.ExisteCarencia == "S") {
                lamina.PrazoCarencia = Convert.ToString(textPrazoCarencia.Text);
            }
            else {
                lamina.PrazoCarencia = null;
            }

            if (lamina.ExisteTaxaEntrada == "S") {
                lamina.TaxaEntrada = Convert.ToString(textTaxaEntrada.Text);
            }
            else {
                lamina.TaxaEntrada = null;
            }

            if (lamina.ExisteTaxaSaida == "S") {
                lamina.TaxaSaida = Convert.ToString(textTaxaSaida.Text);
            }
            else {
                lamina.TaxaSaida = null;
            }

            if (dropEstrategiaPerdas.SelectedIndex > 0) {
                lamina.EstrategiaPerdas = Convert.ToString(dropEstrategiaPerdas.SelectedItem.Value);
            }
            else {
                lamina.EstrategiaPerdas = null;
            }
            lamina.RegulamentoPerdasPatrimoniais = Convert.ToString(textRegulamentoPerdasPatrimoniais.Text);
            lamina.RegulamentoPatrimonioNegativo = Convert.ToString(textRegulamentoPatrimonioNegativo.Text);
            lamina.CenariosApuracaoRentabilidade = Convert.ToString(textCenariosApuracaoRentabilidade.Text);
            lamina.DesempenhoFundo = Convert.ToString(textDesempenhoFundo.Text);
            lamina.PoliticaDistribuicao = Convert.ToString(textPoliticaDistribuicao.Text);
            lamina.Telefone = Convert.ToString(textTelefone.Text);
            lamina.UrlAtendimento = Convert.ToString(textUrlAtendimento.Text);
            lamina.Reclamacoes = Convert.ToString(textReclamacoes.Text);
            #endregion

            // Não Houve mudança de Chave
            if (vigenciaOld.Substring(0, 10).Trim() == vigencia.Substring(0, 10).Trim()) {
                lamina.Save();
            }
            else {
                //Clona a Lamina - Deleta a Velha e Insere a Nova
                Lamina lNova = new Lamina();

                // Para cada Coluna de lamina copia para laminaNova
                foreach (esColumnMetadata col in lamina.es.Meta.Columns) {

                    esColumnMetadata colNova = lNova.es.Meta.Columns.FindByPropertyName(col.PropertyName);
                    if (lNova.GetColumn(colNova.Name) != null) {
                        lNova.SetColumn(colNova.Name, lamina.GetColumn(col.Name));
                    }
                }
                //
                lNova.InicioVigencia = Convert.ToDateTime(textVigencia.Text); // Altero a chave
                //
                using (esTransactionScope scope = new esTransactionScope()) {

                    lNova.Save(); // Insere
                    //
                    Lamina laminaVelha = new Lamina();
                    laminaVelha.LoadByPrimaryKey(Convert.ToInt32(idCarteira), Convert.ToDateTime(vigenciaOld));
                    //
                    laminaVelha.MarkAsDeleted();
                    laminaVelha.Save(); // Deleta
                    //
                    scope.Complete();
                }
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Lamina - Operacao: Update Lamina: " + idCarteira + " " + vigenciaOld + UtilitarioWeb.ToString(lamina),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        #region Elementos
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxTextBox textEnderecoEletronico = pageControl.FindControl("textEnderecoEletronico") as ASPxTextBox;
        TextBox textDescricaoPublicoAlvo = pageControl.FindControl("textDescricaoPublicoAlvo") as TextBox;
        TextBox textDescricaoObjetivoFundo = pageControl.FindControl("textObjetivoFundo") as TextBox;
        TextBox textDescricaoPoliticaInvestimento = pageControl.FindControl("textDescricaoPoliticaInvestimento") as TextBox;
        ASPxComboBox dropRisco = pageControl.FindControl("dropRisco") as ASPxComboBox;
        ASPxSpinEdit textLimiteAplicacaoExterior = pageControl.FindControl("textLimiteAplicacaoExterior") as ASPxSpinEdit;
        TextBox textFundosInvestimento = pageControl.FindControl("textFundosInvestimento") as TextBox;
        ASPxSpinEdit textLimiteCreditoPrivado = pageControl.FindControl("textLimiteCreditoPrivado") as ASPxSpinEdit;
        TextBox textFundosInvestimentoCotas = pageControl.FindControl("textFundosInvestimentoCotas") as TextBox;
        ASPxComboBox dropDerivativosProtecaoCarteira = pageControl.FindControl("dropDerivativosProtecaoCarteira") as ASPxComboBox;
        ASPxSpinEdit textLimiteAlavancagem = pageControl.FindControl("textLimiteAlavancagem") as ASPxSpinEdit;
        //
        ASPxSpinEdit textLimiteConcentracaoEmissor = pageControl.FindControl("textLimiteConcentracaoEmissor") as ASPxSpinEdit;
        //
        ASPxComboBox dropEstrategiaPerdas = pageControl.FindControl("dropEstrategiaPerdas") as ASPxComboBox;
        TextBox textRegulamentoPerdasPatrimoniais = pageControl.FindControl("textRegulamentoPerdasPatrimoniais") as TextBox;
        TextBox textRegulamentoPatrimonioNegativo = pageControl.FindControl("textRegulamentoPatrimonioNegativo") as TextBox;
        TextBox textPrazoCarencia = pageControl.FindControl("textPrazoCarencia") as TextBox;
        TextBox textTaxaEntrada = pageControl.FindControl("textTaxaEntrada") as TextBox;
        TextBox textTaxaSaida = pageControl.FindControl("textTaxaSaida") as TextBox;
        ASPxComboBox dropFundoEstruturado = pageControl.FindControl("dropFundoEstruturado") as ASPxComboBox;
        TextBox textCenariosApuracaoRentabilidade = pageControl.FindControl("textCenariosApuracaoRentabilidade") as TextBox;
        TextBox textDesempenhoFundo = pageControl.FindControl("textDesempenhoFundo") as TextBox;
        TextBox textPoliticaDistribuicao = pageControl.FindControl("textPoliticaDistribuicao") as TextBox;
        ASPxTextBox textTelefone = pageControl.FindControl("textTelefone") as ASPxTextBox;
        ASPxTextBox textUrlAtendimento = pageControl.FindControl("textUrlAtendimento") as ASPxTextBox;
        TextBox textReclamacoes = pageControl.FindControl("textReclamacoes") as TextBox;

        ASPxCheckBox existeCarencia = pageControl.FindControl("existeCarencia") as ASPxCheckBox;
        ASPxCheckBox existeTaxaEntrada = pageControl.FindControl("existeTaxaEntrada") as ASPxCheckBox;
        ASPxCheckBox existeTaxaSaida = pageControl.FindControl("existeTaxaSaida") as ASPxCheckBox;
        #endregion

        Lamina lamina = new Lamina();

        #region Lamina
        lamina.IdCarteira = Convert.ToInt32(btnCarteira.Text);
        lamina.InicioVigencia = Convert.ToDateTime(textVigencia.Text);
        lamina.EnderecoEletronico = Convert.ToString(textEnderecoEletronico.Text);
        lamina.DescricaoPublicoAlvo = Convert.ToString(textDescricaoPublicoAlvo.Text);
        lamina.ObjetivoFundo = Convert.ToString(textDescricaoObjetivoFundo.Text);
        lamina.DescricaoPoliticaInvestimento = Convert.ToString(textDescricaoPoliticaInvestimento.Text);
        if (dropRisco.SelectedIndex > 0) {
            lamina.Risco = Convert.ToInt32(dropRisco.SelectedItem.Value);
        }
        if (!String.IsNullOrEmpty(textLimiteAplicacaoExterior.Text)) {
            lamina.LimiteAplicacaoExterior = Convert.ToDecimal(textLimiteAplicacaoExterior.Text);
        }
        lamina.FundosInvestimento = Convert.ToString(textFundosInvestimento.Text);
        if (!String.IsNullOrEmpty(textLimiteCreditoPrivado.Text)) {
            lamina.LimiteCreditoPrivado = Convert.ToDecimal(textLimiteCreditoPrivado.Text);
        }
        lamina.FundosInvestimentoCotas = Convert.ToString(textFundosInvestimentoCotas.Text);
        if (dropDerivativosProtecaoCarteira.SelectedIndex > 0) {
            lamina.DerivativosProtecaoCarteira = Convert.ToString(dropDerivativosProtecaoCarteira.SelectedItem.Value);
        }
        if (!String.IsNullOrEmpty(textLimiteAlavancagem.Text)) {
            lamina.LimiteAlavancagem = Convert.ToDecimal(textLimiteAlavancagem.Text);
        }
        if (!String.IsNullOrEmpty(textLimiteConcentracaoEmissor.Text)) {
            lamina.LimiteConcentracaoEmissor = Convert.ToDecimal(textLimiteConcentracaoEmissor.Text);
        }
        else {
            lamina.LimiteConcentracaoEmissor = null;
        }
        if (dropFundoEstruturado.SelectedIndex > 0) {
            lamina.FundoEstruturado = Convert.ToString(dropFundoEstruturado.SelectedItem.Value);
        }
        //
        lamina.ExisteCarencia = existeCarencia.CheckState == CheckState.Checked ? "S" : "N";
        lamina.ExisteTaxaEntrada = existeTaxaEntrada.CheckState == CheckState.Checked ? "S" : "N";
        lamina.ExisteTaxaSaida = existeTaxaSaida.CheckState == CheckState.Checked ? "S" : "N";
        //

        if (lamina.ExisteCarencia == "S") {
            lamina.PrazoCarencia = Convert.ToString(textPrazoCarencia.Text);
        }
        else {
            lamina.PrazoCarencia = null;
        }

        if (lamina.ExisteTaxaEntrada == "S") {
            lamina.TaxaEntrada = Convert.ToString(textTaxaEntrada.Text);
        }
        else {
            lamina.TaxaEntrada = null;
        }

        if (lamina.ExisteTaxaSaida == "S") {
            lamina.TaxaSaida = Convert.ToString(textTaxaSaida.Text);
        }
        else {
            lamina.TaxaSaida = null;
        }
                
        if (dropEstrategiaPerdas.SelectedIndex > 0) {
            lamina.EstrategiaPerdas = Convert.ToString(dropEstrategiaPerdas.SelectedItem.Value);
        }
        lamina.RegulamentoPerdasPatrimoniais = Convert.ToString(textRegulamentoPerdasPatrimoniais.Text);
        lamina.RegulamentoPatrimonioNegativo = Convert.ToString(textRegulamentoPatrimonioNegativo.Text);
        lamina.CenariosApuracaoRentabilidade = Convert.ToString(textCenariosApuracaoRentabilidade.Text);
        lamina.DesempenhoFundo = Convert.ToString(textDesempenhoFundo.Text);
        lamina.PoliticaDistribuicao = Convert.ToString(textPoliticaDistribuicao.Text);
        lamina.Telefone = Convert.ToString(textTelefone.Text);
        lamina.UrlAtendimento = Convert.ToString(textUrlAtendimento.Text);
        lamina.Reclamacoes = Convert.ToString(textReclamacoes.Text);
        #endregion

        lamina.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Lamina - Operacao: Insert Lamina: " + lamina.IdCarteira + " " + textVigencia.Text + UtilitarioWeb.ToString(lamina),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {

            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(LaminaMetadata.ColumnNames.IdCarteira);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(LaminaMetadata.ColumnNames.InicioVigencia);

            for (int i = 0; i < keyValues1.Count; i++) {
                string idCarteira = Convert.ToString(keyValues1[i]);
                string vigencia = Convert.ToString(keyValues2[i]);

                Lamina lamina = new Lamina();
                if (lamina.LoadByPrimaryKey(Convert.ToInt32(idCarteira), Convert.ToDateTime(vigencia))) {
                    Lamina laminaClone = (Lamina)Utilitario.Clone(lamina);
                    //
                    lamina.MarkAsDeleted();
                    lamina.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Lamina - Operacao: Delete Lamina: " + laminaClone.IdCarteira + " " + laminaClone.InicioVigencia.ToString() + UtilitarioWeb.ToString(laminaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(LaminaMetadata.ColumnNames.IdCarteira));
            string data = Convert.ToString(e.GetListSourceFieldValue(LaminaMetadata.ColumnNames.InicioVigencia));
            e.Value = idCarteira + "-" + data;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {

            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        e.NewValues[LaminaMetadata.ColumnNames.EnderecoEletronico] = "http://";
        e.NewValues[LaminaMetadata.ColumnNames.UrlAtendimento] = "http://";

    }
}