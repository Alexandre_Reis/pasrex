﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

using Financial.Contabil;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;

using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using System.Threading;
using Financial.Investidor.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_CarteiraMae : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCarteiraMae_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;
        
        ClienteQuery cMaeQuery = new ClienteQuery("CMae");
        ClienteQuery cFilhaQuery = new ClienteQuery("CFilha");
        CarteiraMaeQuery cMaeFilhaQuery = new CarteiraMaeQuery("CMaeFilha");
        //
        PermissaoClienteQuery permissaoMaeQuery = new PermissaoClienteQuery("PM");
        PermissaoClienteQuery permissaoFilhaQuery = new PermissaoClienteQuery("PF");

        cMaeFilhaQuery.Select(cMaeQuery.IdCliente.As("IdClienteMae"), cMaeQuery.Apelido.As("ApelidoMae"),
                         cFilhaQuery.IdCliente.As("IdClienteFilha"), cFilhaQuery.Apelido.As("ApelidoFilha"));

        cMaeFilhaQuery.InnerJoin(cFilhaQuery).On(cMaeFilhaQuery.IdCarteiraFilha == cFilhaQuery.IdCliente);
        cMaeFilhaQuery.InnerJoin(cMaeQuery).On(cMaeFilhaQuery.IdCarteiraMae == cMaeQuery.IdCliente);
        //
        cMaeFilhaQuery.InnerJoin(permissaoMaeQuery).On(
            permissaoMaeQuery.IdCliente == cMaeFilhaQuery.IdCarteiraMae);
        //
        cMaeFilhaQuery.InnerJoin(permissaoFilhaQuery).On(
            permissaoFilhaQuery.IdCliente == cMaeFilhaQuery.IdCarteiraFilha);
        //
        cMaeFilhaQuery.Where(permissaoMaeQuery.IdUsuario == idUsuario &&
                                permissaoFilhaQuery.IdUsuario == idUsuario);

        //
        cMaeFilhaQuery.OrderBy(cMaeQuery.IdCliente.Ascending, cFilhaQuery.IdCliente.Ascending);
        //
        CarteiraMaeCollection col = new CarteiraMaeCollection();
        col.Load(cMaeFilhaQuery);
        //     
        e.Collection = col;
    }
    
    protected void EsDSCarteira1_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;
        
        ClienteQuery c = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        //

        c.Select(c.IdCliente, c.IdCliente.As("IdClienteMae"), c.IdCliente.As("IdClienteFilha"), c.Nome);
        //
        c.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == c.IdCliente);
        //
        c.Where(permissaoClienteQuery.IdUsuario == idUsuario);
        //
        c.OrderBy(c.IdCliente.Ascending);
        //
        ClienteCollection coll = new ClienteCollection();
        coll.Load(c);
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        //
        ASPxGridLookup dropCarteiraMae = gridCadastro.FindEditFormTemplateControl("dropCarteiraMae") as ASPxGridLookup;
        ASPxGridLookup dropCarteiraFilha = gridCadastro.FindEditFormTemplateControl("dropCarteiraFilha") as ASPxGridLookup;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            dropCarteiraMae, dropCarteiraFilha });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        // Carteira Mãe não pode ser igual Carteira Filha
        if (dropCarteiraFilha.Text.Trim() == dropCarteiraMae.Text.Trim()) {
            e.Result = "Carteira Filha não pode ser igual a Carteira Mãe.";
            return;
        }

        // Se for insert - Confere se carteira filha já tem mãe - não pode inserir
        if (gridCadastro.IsNewRowEditing) 
        {
            CarteiraMae c = new CarteiraMae();
            if (c.LoadByPrimaryKey(Convert.ToInt32(dropCarteiraFilha.Text), Convert.ToInt32(dropCarteiraMae.Text)))
            {
                e.Result = "Registro já existente.";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteiraMae = Convert.ToString(e.GetListSourceFieldValue("IdClienteMae"));
            string idCarteiraFilha = Convert.ToString(e.GetListSourceFieldValue("IdClienteFilha"));

            string apelidoMae = Convert.ToString(e.GetListSourceFieldValue("ApelidoMae"));
            string apelidoFilha = Convert.ToString(e.GetListSourceFieldValue("ApelidoFilha"));

            e.Value = idCarteiraMae + "|" + idCarteiraFilha;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ASPxGridLookup dropCarteiraMae = gridCadastro.FindEditFormTemplateControl("dropCarteiraMae") as ASPxGridLookup;
        ASPxGridLookup dropCarteiraFilha = gridCadastro.FindEditFormTemplateControl("dropCarteiraFilha") as ASPxGridLookup;
        //
        int idFilha = Convert.ToInt32(dropCarteiraFilha.Text.Trim());
        int idMae = Convert.ToInt32(dropCarteiraMae.Text.Trim());

        CarteiraMae c = new CarteiraMae();

        if (c.LoadByPrimaryKey(idFilha, idMae))
        {
            Exception e = new Exception("Esse registro já esta cadastrado");
            return;
        }

        //não permite mae-filha filha-mae
        if (c.LoadByPrimaryKey(idMae, idFilha))
        {
            Exception e = new Exception("Já esta cadastrado na ordem inversa");
            return;
        }
        
        c.IdCarteiraFilha = Convert.ToInt32(dropCarteiraFilha.Text.Trim());
        c.IdCarteiraMae = Convert.ToInt32(dropCarteiraMae.Text.Trim());
        
        c.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Carteira Mãe - Insert: " + c.IdCarteiraFilha + "-" + c.IdCarteiraMae + UtilitarioWeb.ToString(c),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
      
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            
            List<object> keyValuesIdClienteFilha = gridCadastro.GetSelectedFieldValues("IdClienteFilha");
            List<object> keyValuesIdClienteMae = gridCadastro.GetSelectedFieldValues("IdClienteMae");

            for (int i = 0; i < keyValuesIdClienteFilha.Count; i++) {
                int idFilha = Convert.ToInt32(keyValuesIdClienteFilha[i]);
                int idMae = Convert.ToInt32(keyValuesIdClienteMae[i]);

                CarteiraMae c = new CarteiraMae();
                c.LoadByPrimaryKey(idFilha, idMae);
                carteiraMaeCollection.AttachEntity(c);
            }
            //

            CarteiraMaeCollection carteiraMaeCollectionClone = (CarteiraMaeCollection)Utilitario.Clone(carteiraMaeCollection);

            
            carteiraMaeCollection.MarkAllAsDeleted();
            carteiraMaeCollection.Save();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollectionClone)
            {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de CarteiraMae - Delete: " + carteiraMae.IdCarteiraMae + "-" + carteiraMae.IdCarteiraFilha + UtilitarioWeb.ToString(carteiraMae),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        //this.FocaCampoGrid("btnEditCodigoCliente", "textValor");
        base.gridCadastro_PreRender(sender, e);
    }
}