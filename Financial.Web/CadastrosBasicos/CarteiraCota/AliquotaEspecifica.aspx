﻿DevExpress.Web.ASPxEditors.v14.1<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AliquotaEspecifica.aspx.cs" Inherits="CadastrosBasicos_CarteiraCota_AliquotaEspecifica" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" /> 

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;    
    var operacao = '';
          
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }
          
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
            
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
                }        
            "/>
        </dxcb:ASPxCallback>
            
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                    
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }          
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Alíquota Específica"></asp:Label>
                            </div>
                            <div id="mainContent">
                            
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {gridCadastro.PerformCallback('btnDelete');return false;} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>

                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSAliquotaEspecifica" 
                                        OnRowUpdating="gridCadastro_RowUpdating" 
                                        OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        SettingsBehavior-ColumnResizeMode="Control">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="15%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Nome" UnboundType="string" VisibleIndex="2" Width="40%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Taxa" Caption="Taxa (%)" VisibleIndex="3" Width="6%" HeaderStyle-HorizontalAlign="Center">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataValidade" Caption="Data" VisibleIndex="4" Width="10%"/>
                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>

                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>        
                                                                
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1" 
                                                                        ClientInstanceName="btnEditCodigoCarteira" OnLoad="btnEditCodigoCarteira_Load"
                                                                        Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer">            
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>                                
                                                                    </Buttons>  
                                                                    <ClientSideEvents                                                           
                                                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                                                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                                                            />               
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                        
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Nome")%>' ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td  class="td_Label">
                                                                    <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Validade:"  /></td> 
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataValidade" runat="server" ClientInstanceName="textDataValidade" Value='<%#Eval("DataValidade")%>'
                                                                        OnLoad="textDataValidade_Load"/>
                                                                </td>  
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td  class="td_Label">
                                                                    <asp:Label ID="labelTaxa" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                                                                </td>
                                                                <td>                                
                                                                    <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxa"
                                                                        NumberType="Float" MaxLength="5" DecimalPlaces="2"
                                                                        Text='<%# Eval("Taxa") %>' >                                                                                                                    
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>  
                                                            </tr>
                                                            

                                                        
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal9" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" LeftMargin="30" RightMargin="30"></dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSAliquotaEspecifica" runat="server" OnesSelect="EsDSAliquotaEspecifica_esSelect"/>
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
