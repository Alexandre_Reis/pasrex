﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Util;
using Financial.Common;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.Web.Common;

using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_ConsultaRollUp : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSConsultaRollUp_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TransferenciaSerieCollection coll = new TransferenciaSerieCollection();
        TransferenciaSerieQuery transferenciaQuery = new TransferenciaSerieQuery("transferencia");
        EventoRollUpQuery eventoQuery = new EventoRollUpQuery("evento");
        SeriesOffShoreQuery serieOrigemQuery = new SeriesOffShoreQuery("serieOrigem");
        SeriesOffShoreQuery serieDestinoQuery = new SeriesOffShoreQuery("serieDestino");
        PosicaoCotistaAberturaQuery posicaoQuery = new PosicaoCotistaAberturaQuery("posicao");
        OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("operacao");
        CotistaQuery cotistaQuery = new CotistaQuery("cotista");

        transferenciaQuery.Select(cotistaQuery.Nome,
                                  serieOrigemQuery.Descricao.As("DescricaoSerieOrigem"),
                                  serieDestinoQuery.Descricao.As("DescricaoSerieDestino"),
                                  posicaoQuery.DataAplicacao,
                                  eventoQuery.DataExecucao,
                                  eventoQuery.DataNav,
                                  posicaoQuery.Quantidade.As("QuantidadeCotas"),
                                  posicaoQuery.ValorLiquido,
                                  posicaoQuery.CotaAplicacao,
                                  posicaoQuery.CotaDia);
        transferenciaQuery.InnerJoin(eventoQuery).On(transferenciaQuery.IdEventoRollUp.Equal(eventoQuery.IdEventoRollUp));
        transferenciaQuery.InnerJoin(serieDestinoQuery).On(serieDestinoQuery.IdSeriesOffShore.Equal(eventoQuery.IdSerieDestino));
        transferenciaQuery.InnerJoin(serieOrigemQuery).On(serieOrigemQuery.IdSeriesOffShore.Equal(eventoQuery.IdSerieOrigem));
        transferenciaQuery.InnerJoin(posicaoQuery).On(transferenciaQuery.IdPosicao.Equal(posicaoQuery.IdPosicao) & posicaoQuery.DataHistorico.Equal(eventoQuery.DataNav));
        transferenciaQuery.InnerJoin(cotistaQuery).On(posicaoQuery.IdCotista.Equal(cotistaQuery.IdCotista));
        transferenciaQuery.InnerJoin(operacaoQuery).On(operacaoQuery.IdPosicaoResgatada.Equal(posicaoQuery.IdPosicao)
                                                       & operacaoQuery.DataConversao.Equal(eventoQuery.DataExecucao)
                                                       & operacaoQuery.TipoOperacao.Equal((int)TipoOperacaoCotista.ResgateCotasEspecial));

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            transferenciaQuery.Where(transferenciaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            transferenciaQuery.Where(eventoQuery.DataExecucao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            transferenciaQuery.Where(eventoQuery.DataExecucao.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicioNAV.Text))
        {
            transferenciaQuery.Where(eventoQuery.DataNav.GreaterThanOrEqual(textDataInicioNAV.Text));
        }

        if (!String.IsNullOrEmpty(textDataFimNAV.Text))
        {
            transferenciaQuery.Where(eventoQuery.DataNav.LessThanOrEqual(textDataFimNAV.Text));
        }

        coll.Load(transferenciaQuery);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        carteiraQuery.Select(carteiraQuery);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(carteiraQuery.IdCarteira));
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.OffShore_PJ,(int)TipoClienteFixo.OffShore_PF));

        coll.Load(carteiraQuery);
        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (String.IsNullOrEmpty(textDataInicio.Text) || String.IsNullOrEmpty(textDataFim.Text))
        {
            e.Result = "Período de Execução deve ser preenchido!";
            return;
        }

        e.Result = "";
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (cliente.LoadByPrimaryKey(idCarteira) && (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ)))
            {
                carteira.LoadByPrimaryKey(idCarteira);
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                        {
                            nome = carteira.str.Apelido;
                            resultado = nome + "|" + cliente.DataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idCarteira;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
}