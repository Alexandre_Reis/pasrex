﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_ListaBenchmark : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSListaBenchmark_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {       
        ListaBenchmarkQuery listaBenchmarkQuery = new ListaBenchmarkQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        PermissaoClienteQuery p = new PermissaoClienteQuery("p");
        UsuarioQuery u = new UsuarioQuery("u");
        //
        listaBenchmarkQuery.Select(listaBenchmarkQuery, carteiraQuery.IdCarteira, carteiraQuery.Apelido.As("Apelido"));
        listaBenchmarkQuery.InnerJoin(carteiraQuery).On(listaBenchmarkQuery.IdCarteira == carteiraQuery.IdCarteira);

        listaBenchmarkQuery.InnerJoin(p).On(p.IdCliente == listaBenchmarkQuery.IdCarteira);
        listaBenchmarkQuery.InnerJoin(u).On(u.IdUsuario == p.IdUsuario);
        listaBenchmarkQuery.Where(u.Login == HttpContext.Current.User.Identity.Name);
        //
        listaBenchmarkQuery.OrderBy(carteiraQuery.Apelido.Ascending, listaBenchmarkQuery.IdIndice.Ascending);

        ListaBenchmarkCollection coll = new ListaBenchmarkCollection();
        coll.Load(listaBenchmarkQuery);

        listaBenchmarkQuery = new ListaBenchmarkQuery("L");
        carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("I");
        //
        listaBenchmarkQuery.Select(listaBenchmarkQuery, carteiraQuery.IdCarteira, carteiraQuery.Apelido.As("Apelido"));
        listaBenchmarkQuery.InnerJoin(carteiraQuery).On(listaBenchmarkQuery.IdCarteira == carteiraQuery.IdCarteira);
        listaBenchmarkQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == listaBenchmarkQuery.IdCarteira);
        listaBenchmarkQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
        //
        listaBenchmarkQuery.OrderBy(carteiraQuery.Apelido.Ascending, listaBenchmarkQuery.IdIndice.Ascending);

        ListaBenchmarkCollection coll2 = new ListaBenchmarkCollection();
        coll2.Load(listaBenchmarkQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;        
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigo_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void dropIndice_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(dropIndice);
        
        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            short idIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);

            ListaBenchmark listaBenchmark = new ListaBenchmark();
            if (listaBenchmark.LoadByPrimaryKey(idIndice, idCarteira))
            {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Salvar();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string idIndice = Convert.ToString(e.GetListSourceFieldValue("IdIndice"));
            e.Value = idCarteira + "|" + idIndice;
        }
    }
        
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        Salvar();
    
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();    
    }

    private void Salvar()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        CheckBox chkReplica = gridCadastro.FindEditFormTemplateControl("chkReplica") as CheckBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        short idIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);

        ListaBenchmark listaBenchmark = new ListaBenchmark();

        listaBenchmark.IdIndice = idIndice;
        listaBenchmark.IdCarteira = idCarteira;
        listaBenchmark.Save();

        if (chkReplica.Checked)
        {
            this.ReplicaBenchmark(idIndice);
        }

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ListaBenchmark - Operacao: Insert ListaBenchmark: " + listaBenchmark.IdIndice + "; " + listaBenchmark.IdCarteira + UtilitarioWeb.ToString(listaBenchmark),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues("IdCarteira");
            List<object> keyValuesIdIndice = gridCadastro.GetSelectedFieldValues("IdIndice");
            for (int i = 0; i < keyValuesIdCarteira.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);
                short idIndice = Convert.ToInt16(keyValuesIdIndice[i]);

                ListaBenchmark listaBenchmark = new ListaBenchmark();
                if (listaBenchmark.LoadByPrimaryKey(idIndice, idCarteira))
                {
                    ListaBenchmark listaBenchmarkClone = (ListaBenchmark)Utilitario.Clone(listaBenchmark);
                    //

                    listaBenchmark.MarkAsDeleted();
                    listaBenchmark.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ListaBenchmark - Operacao: Delete ListaBenchmark: " + idCarteira + "; " + idIndice + UtilitarioWeb.ToString(listaBenchmarkClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing && gridView.IsNewRowEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.UniqueID;
        }
    }

    private void ReplicaBenchmark(short idIndice)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);

        foreach (Carteira carteira in coll)
        {
            int idCarteira = carteira.IdCarteira.Value;

            ListaBenchmark listaBenchmark = new ListaBenchmark();
            if (!listaBenchmark.LoadByPrimaryKey(idIndice, idCarteira))
            {
                ListaBenchmark listaBenchmarkInsert = new ListaBenchmark();
                listaBenchmarkInsert.IdIndice = idIndice;
                listaBenchmarkInsert.IdCarteira = idCarteira;
                listaBenchmarkInsert.Save();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }
}

