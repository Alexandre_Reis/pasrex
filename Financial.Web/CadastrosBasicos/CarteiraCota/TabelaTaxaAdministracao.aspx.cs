﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using DevExpress.Web.Data;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Contabil;
using Financial.WebConfigConfiguration;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_TabelaTaxaAdministracao : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        this.FiltroGridEvento();
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { TabelaTaxaAdministracaoMetadata.ColumnNames.TipoCalculo }));

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTabelaTaxaAdministracao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery, carteiraQuery.Apelido.As("Apelido"),
                                     "<'' as DescricaoEventoPagamento >", "<'' as DescricaoEventoProvisao >");
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaTaxaAdministracaoQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        tabelaTaxaAdministracaoQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                           permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();
        coll.Load(tabelaTaxaAdministracaoQuery);

        tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("L");
        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("Q");

        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery, carteiraQuery.Apelido.As("Apelido"),
                                     "<'' as DescricaoEventoPagamento >", "<'' as DescricaoEventoProvisao >");
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaTaxaAdministracaoQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                           clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));

        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll2 = new TabelaTaxaAdministracaoCollection();
        coll2.Load(tabelaTaxaAdministracaoQuery);

        coll.Combine(coll2);

        for (int i = 0; i < coll.Count; i++)
        {
            if (coll[i].IdEventoPagamento.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoPagamento", coll[i].IdEventoPagamento + " - " + coll[i].UpToContabRoteiroByIdEventoPagamento.Descricao);
            }

            if (coll[i].IdEventoProvisao.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoProvisao", coll[i].IdEventoProvisao + " - " + coll[i].UpToContabRoteiroByIdEventoProvisao.Descricao);
            }
        }

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCadastroTaxaAdministracao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroTaxaAdministracaoCollection coll = new CadastroTaxaAdministracaoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region PopUp ContabRoteiro
    /// <summary>
    /// PopUp ContabRoteiro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSEvento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        e.Collection = new TabelaTaxaAdministracaoCollection();
        
        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        Cliente cliente = new Cliente();

        int? idPlano = 0;
        int idCarteiraAtual = 0;
        if (btnEditCodigoCarteira != null)
        {
            idCarteiraAtual = Convert.ToInt32(btnEditCodigoCarteira.Text);
        }
        
        if (cliente.LoadByPrimaryKey(idCarteiraAtual))
        {
            idPlano = (int?)cliente.IdPlano;
        }

        if ( !idPlano.HasValue )
        {
            idPlano = 0;
        }

        coll.Query.Select(coll.Query.IdEvento, coll.Query.Descricao, coll.Query.ContaCredito, coll.Query.ContaDebito);
        coll.Query.Where(coll.Query.IdPlano == idPlano );
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);

        if (coll.Query.Load())
            e.Collection = coll;
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idEvento = (int)gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdEvento);
        string descricao = Convert.ToString(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Descricao));

        e.Result = idEvento.ToString() + "|" + " - " + descricao;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridEvento.DataBind();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropCadastro = gridCadastro.FindEditFormTemplateControl("dropCadastro") as ASPxComboBox;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxTextBox textCodigoAtivo = gridCadastro.FindEditFormTemplateControl("textCodigoAtivo") as ASPxTextBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(dropCadastro);
        controles.Add(dropTipoCalculo);
        controles.Add(dropContagemDias);
        controles.Add(textNumeroMesesRenovacao);
        controles.Add(textNumeroDiasPagamento);

        if (dropTipoCalculo.SelectedIndex == 0) //Percentual do PL
        {
            ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
            ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
            ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;

            if (dropBaseApuracao.SelectedIndex == -1)
            {
                e.Result = "Base de Apuração deve ser preenchida para o cálculo por % do PL.";
                return;
            }
            if (textTaxa.Text == "")
            {
                e.Result = "Taxa deve ser preenchida para o cálculo por % do PL.";
                return;
            }
            if (dropBaseAno.SelectedIndex == -1)
            {
                e.Result = "Base Ano deve ser preenchida para o cálculo por % do PL.";
                return;
            }
            if (dropTipoApropriacao.SelectedIndex == -1)
            {
                e.Result = "Apropriação deve ser preenchida para o cálculo por % do PL.";
                return;
            }
        }
        else //Valor Fixo
        {
            ASPxSpinEdit textValorFixoTotal = gridCadastro.FindEditFormTemplateControl("textValorFixoTotal") as ASPxSpinEdit;
            if (textValorFixoTotal.Text == "")
            {
                e.Result = "Valor Fixo deve ser preenchido para o cálculo por Valor Fixo.";
                return;
            }
        }

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion


        if (!string.IsNullOrEmpty(textDataFim.Text))
        {
            DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            if (dataFim.Date <= dataReferencia.Date)
            {
                e.Result = "Data fim deve ser maior que a data de referência";
                return;
            }
        }

        if (dropSegmento.SelectedIndex > -1)
        {
            int tipoGrupo = Convert.ToInt32(dropSegmento.SelectedItem.Value);
            if ((tipoGrupo == 2 || tipoGrupo == 11 || tipoGrupo == 51 || tipoGrupo == 2 || tipoGrupo == 81) && textCodigoAtivo.Text == "")
            {
                e.Result = "Para o segmento de cálculo de taxa adm, o código associado precisa ser escolhido.";
                return;
            }

            if (!(tipoGrupo == 2 || tipoGrupo == 11 || tipoGrupo == 51 || tipoGrupo == 2 || tipoGrupo == 81) && textCodigoAtivo.Text != "")
            {
                e.Result = "Para o segmento de cálculo de taxa adm, o código associado não deve ser escolhido.";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idTabela = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxSpinEdit textValorFixoTotal = gridCadastro.FindEditFormTemplateControl("textValorFixoTotal") as ASPxSpinEdit;
        ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;
        ASPxComboBox dropTipoLimitePL = gridCadastro.FindEditFormTemplateControl("dropTipoLimitePL") as ASPxComboBox;
        ASPxSpinEdit textValorLimite = gridCadastro.FindEditFormTemplateControl("textValorLimite") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimo = gridCadastro.FindEditFormTemplateControl("textValorMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textValorMaximo = gridCadastro.FindEditFormTemplateControl("textValorMaximo") as ASPxSpinEdit;
        ASPxComboBox dropImpactaPL = gridCadastro.FindEditFormTemplateControl("dropImpactaPL") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxComboBox dropCadastro = gridCadastro.FindEditFormTemplateControl("dropCadastro") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxTextBox textCodigoAtivo = gridCadastro.FindEditFormTemplateControl("textCodigoAtivo") as ASPxTextBox;
        ASPxComboBox dropEscalonaProporcional = gridCadastro.FindEditFormTemplateControl("dropEscalonaProporcional") as ASPxComboBox;
        ASPxComboBox dropConsolidadora = gridCadastro.FindEditFormTemplateControl("dropConsolidadora") as ASPxComboBox;
        //

        TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
        if (tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela))
        {
            tabelaTaxaAdministracao.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            tabelaTaxaAdministracao.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            tabelaTaxaAdministracao.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);

            if (dropTipoCalculo.SelectedIndex == 1) //Valor Fixo
            {
                tabelaTaxaAdministracao.BaseApuracao = null;
                tabelaTaxaAdministracao.Taxa = null;
                tabelaTaxaAdministracao.BaseAno = null;
                tabelaTaxaAdministracao.TipoApropriacao = null;
                tabelaTaxaAdministracao.TipoLimitePL = null;
                tabelaTaxaAdministracao.ValorLimite = null;

                tabelaTaxaAdministracao.ValorFixoTotal = Convert.ToDecimal(textValorFixoTotal.Text);


            }
            else //Percentual do PL e %PL ou Valor Fixo
            {
                if (dropBaseApuracao.SelectedIndex != -1)
                {
                    tabelaTaxaAdministracao.BaseApuracao = Convert.ToByte(dropBaseApuracao.SelectedItem.Value);
                }
                else
                {
                    tabelaTaxaAdministracao.BaseApuracao = (byte)BaseApuracaoAdministracao.PL_DiaAnterior; //Default!
                }

                if (textTaxa.Text != "")
                {
                    tabelaTaxaAdministracao.Taxa = Convert.ToDecimal(textTaxa.Text);
                }
                else
                {
                    tabelaTaxaAdministracao.Taxa = 0;
                }

                if (dropBaseAno.SelectedIndex != -1)
                {
                    tabelaTaxaAdministracao.BaseAno = Convert.ToInt16(dropBaseAno.SelectedItem.Value);
                }
                else
                {
                    tabelaTaxaAdministracao.BaseAno = (byte)BaseAnoAdministracao.Base252; //Default!;
                }

                if (dropTipoApropriacao.SelectedIndex != -1)
                {
                    tabelaTaxaAdministracao.TipoApropriacao = Convert.ToByte(dropTipoApropriacao.SelectedItem.Value);
                }
                else
                {
                    tabelaTaxaAdministracao.TipoApropriacao = (byte)TipoApropriacaoAdministracao.Linear; //Default!;
                }

                if (dropTipoLimitePL.SelectedIndex != -1)
                {
                    tabelaTaxaAdministracao.TipoLimitePL = Convert.ToByte(dropTipoLimitePL.SelectedItem.Value);
                }
                else
                {
                    tabelaTaxaAdministracao.TipoLimitePL = (byte)TipoLimitePLAdministracao.TotalPL;
                }

                if (textValorLimite.Text != "")
                {
                    tabelaTaxaAdministracao.ValorLimite = Convert.ToDecimal(textValorLimite.Text);
                }
                else
                {
                    tabelaTaxaAdministracao.ValorLimite = 0;
                }

                if (dropTipoCalculo.SelectedIndex == 0)  //Percentual do PL
                {
                    tabelaTaxaAdministracao.ValorFixoTotal = null;
                }
                else
                {
                    tabelaTaxaAdministracao.ValorFixoTotal = Convert.ToDecimal(textValorFixoTotal.Text);
                }

            }

            if (textValorMinimo.Text != "")
            {
                tabelaTaxaAdministracao.ValorMinimo = Convert.ToDecimal(textValorMinimo.Text);
            }
            else
            {
                tabelaTaxaAdministracao.ValorMinimo = 0;
            }

            if (textValorMaximo.Text != "")
            {
                tabelaTaxaAdministracao.ValorMaximo = Convert.ToDecimal(textValorMaximo.Text);
            }
            else
            {
                tabelaTaxaAdministracao.ValorMaximo = 0;
            }

            if (dropImpactaPL.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.ImpactaPL = Convert.ToString(dropImpactaPL.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.ImpactaPL = "S";
            }

            tabelaTaxaAdministracao.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
            tabelaTaxaAdministracao.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
            tabelaTaxaAdministracao.NumeroDiasPagamento = Convert.ToByte(textNumeroDiasPagamento.Text);
            tabelaTaxaAdministracao.IdCadastro = Convert.ToInt32(dropCadastro.SelectedItem.Value);

            if (textDataFim.Text != "")
            {
                tabelaTaxaAdministracao.DataFim = Convert.ToDateTime(textDataFim.Text);
            }
            else
            {
                tabelaTaxaAdministracao.DataFim = null;
            }

            if (ParametrosConfiguracaoSistema.Outras.CalculaContatil) {
                //
                ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
                ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;

                if (textIdProvisaoEvento.Text != "")
                {
                    string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                    tabelaTaxaAdministracao.IdEventoProvisao = Convert.ToInt32(dados[0].Trim());
                }

                if (textIdProvisaoPagamento.Text != "")
                {
                    string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                    tabelaTaxaAdministracao.IdEventoPagamento = Convert.ToInt32(dados[0].Trim());
                }
            }

            tabelaTaxaAdministracao.TipoGrupo = null;
            tabelaTaxaAdministracao.CodigoAtivo = "";
            if (dropSegmento.SelectedIndex > -1)
            {
                tabelaTaxaAdministracao.TipoGrupo = Convert.ToInt32(dropSegmento.SelectedItem.Value);
                tabelaTaxaAdministracao.CodigoAtivo = textCodigoAtivo.Text;
            }


            if (dropEscalonaProporcional.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.EscalonaProporcional = Convert.ToString(dropEscalonaProporcional.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.EscalonaProporcional = "N";
            }

            if (dropConsolidadora.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.Consolidadora = Convert.ToString(dropConsolidadora.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.Consolidadora = "N";
            }

            tabelaTaxaAdministracao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaTaxaAdministracao - Operacao: Update TabelaTaxaAdministracao: " + idTabela + UtilitarioWeb.ToString(tabelaTaxaAdministracao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo() 
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxSpinEdit textValorFixoTotal = gridCadastro.FindEditFormTemplateControl("textValorFixoTotal") as ASPxSpinEdit;
        ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;
        ASPxComboBox dropTipoLimitePL = gridCadastro.FindEditFormTemplateControl("dropTipoLimitePL") as ASPxComboBox;
        ASPxSpinEdit textValorLimite = gridCadastro.FindEditFormTemplateControl("textValorLimite") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimo = gridCadastro.FindEditFormTemplateControl("textValorMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textValorMaximo = gridCadastro.FindEditFormTemplateControl("textValorMaximo") as ASPxSpinEdit;
        ASPxComboBox dropImpactaPL = gridCadastro.FindEditFormTemplateControl("dropImpactaPL") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxComboBox dropCadastro = gridCadastro.FindEditFormTemplateControl("dropCadastro") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropSegmento = gridCadastro.FindEditFormTemplateControl("dropSegmento") as ASPxComboBox;
        ASPxTextBox textCodigoAtivo = gridCadastro.FindEditFormTemplateControl("textCodigoAtivo") as ASPxTextBox;
        ASPxComboBox dropEscalonaProporcional = gridCadastro.FindEditFormTemplateControl("dropEscalonaProporcional") as ASPxComboBox;
        ASPxComboBox dropConsolidadora = gridCadastro.FindEditFormTemplateControl("dropConsolidadora") as ASPxComboBox;

        //
        TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();

        tabelaTaxaAdministracao.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaTaxaAdministracao.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaTaxaAdministracao.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);

        if (dropTipoCalculo.SelectedIndex == 1) //Valor Fixo
        {
            tabelaTaxaAdministracao.BaseApuracao = null;
            tabelaTaxaAdministracao.Taxa = null;
            tabelaTaxaAdministracao.BaseAno = null;
            tabelaTaxaAdministracao.TipoApropriacao = null;
            tabelaTaxaAdministracao.TipoLimitePL = null;
            tabelaTaxaAdministracao.ValorLimite = null;

            tabelaTaxaAdministracao.ValorFixoTotal = Convert.ToDecimal(textValorFixoTotal.Text);

        }
        else //Percentula de PL e %PL ou Valor Fixo
        {
            if (dropBaseApuracao.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.BaseApuracao = Convert.ToByte(dropBaseApuracao.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.BaseApuracao = (byte)BaseApuracaoAdministracao.PL_DiaAnterior; //Default!
            }

            if (textTaxa.Text != "")
            {
                tabelaTaxaAdministracao.Taxa = Convert.ToDecimal(textTaxa.Text);
            }
            else
            {
                tabelaTaxaAdministracao.Taxa = 0;
            }

            if (dropBaseAno.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.BaseAno = Convert.ToInt16(dropBaseAno.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.BaseAno = (byte)BaseAnoAdministracao.Base252; //Default!;
            }

            if (dropTipoApropriacao.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.TipoApropriacao = Convert.ToByte(dropTipoApropriacao.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.TipoApropriacao = (byte)TipoApropriacaoAdministracao.Linear; //Default!;
            }

            if (dropTipoLimitePL.SelectedIndex != -1)
            {
                tabelaTaxaAdministracao.TipoLimitePL = Convert.ToByte(dropTipoLimitePL.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaAdministracao.TipoLimitePL = (byte)TipoLimitePLAdministracao.TotalPL;
            }

            if (textValorLimite.Text != "")
            {
                tabelaTaxaAdministracao.ValorLimite = Convert.ToDecimal(textValorLimite.Text);
            }
            else
            {
                tabelaTaxaAdministracao.ValorLimite = 0;
            }

            if (dropTipoCalculo.SelectedIndex == 0) //Percentual de PL
            {
                tabelaTaxaAdministracao.ValorFixoTotal = null;
            }
        }

        if (textValorMinimo.Text != "") {
            tabelaTaxaAdministracao.ValorMinimo = Convert.ToDecimal(textValorMinimo.Text);
        }
        else {
            tabelaTaxaAdministracao.ValorMinimo = 0;
        }

        if (textValorMaximo.Text != "") {
            tabelaTaxaAdministracao.ValorMaximo = Convert.ToDecimal(textValorMaximo.Text);
        }
        else {
            tabelaTaxaAdministracao.ValorMaximo = 0;
        }

        if (dropImpactaPL.SelectedIndex != -1) {
            tabelaTaxaAdministracao.ImpactaPL = Convert.ToString(dropImpactaPL.SelectedItem.Value);
        }
        else {
            tabelaTaxaAdministracao.ImpactaPL = "S";
        }

        tabelaTaxaAdministracao.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
        tabelaTaxaAdministracao.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
        tabelaTaxaAdministracao.NumeroDiasPagamento = Convert.ToByte(textNumeroDiasPagamento.Text);
        tabelaTaxaAdministracao.IdCadastro = Convert.ToInt32(dropCadastro.SelectedItem.Value);

        if (textDataFim.Text != "") {
            tabelaTaxaAdministracao.DataFim = Convert.ToDateTime(textDataFim.Text);
        }

        //if (WebConfig.AppSettings.CalculaContatil) {
        if (ParametrosConfiguracaoSistema.Outras.CalculaContatil) {
            ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
            ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;
            //
            if (textIdProvisaoEvento.Text != "") {
                string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                tabelaTaxaAdministracao.IdEventoProvisao = Convert.ToInt32(dados[0].Trim());
            }

            if (textIdProvisaoPagamento.Text != "") {
                string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                tabelaTaxaAdministracao.IdEventoPagamento = Convert.ToInt32(dados[0].Trim());
            }
        }

        tabelaTaxaAdministracao.TipoGrupo = null;
        tabelaTaxaAdministracao.CodigoAtivo = "";
        if (dropSegmento.SelectedIndex > -1)
        {
            tabelaTaxaAdministracao.TipoGrupo = Convert.ToInt32(dropSegmento.SelectedItem.Value);
            tabelaTaxaAdministracao.CodigoAtivo = textCodigoAtivo.Text;
        }
        //

        if (dropEscalonaProporcional.SelectedIndex != -1)
        {
            tabelaTaxaAdministracao.EscalonaProporcional = Convert.ToString(dropEscalonaProporcional.SelectedItem.Value);
        }
        else
        {
            tabelaTaxaAdministracao.EscalonaProporcional = "N";
        }

        if (dropConsolidadora.SelectedIndex != -1)
        {
            tabelaTaxaAdministracao.Consolidadora = Convert.ToString(dropConsolidadora.SelectedItem.Value);
        }
        else
        {
            tabelaTaxaAdministracao.Consolidadora = "N";
        }

        tabelaTaxaAdministracao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaTaxaAdministracao - Operacao: Insert TabelaTaxaAdministracao: " + tabelaTaxaAdministracao.IdTabela + UtilitarioWeb.ToString(tabelaTaxaAdministracao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTabela = Convert.ToInt32(keyValuesId[i]);

                TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
                if (tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela))
                {
                    CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
                    calculoAdministracaoCollection.Query.Where(calculoAdministracaoCollection.Query.IdTabela.Equal(idTabela));
                    if (calculoAdministracaoCollection.Query.Load())
                    {
                        throw new Exception("Tabela " + tabelaTaxaAdministracao.IdTabela + " não pode ser excluída por ter processos relacionados.");
                    }

                    TabelaTaxaAdministracao tabelaTaxaAdministracaoClone = (TabelaTaxaAdministracao)Utilitario.Clone(tabelaTaxaAdministracao);
                    //
                    tabelaTaxaAdministracao.MarkAsDeleted();
                    tabelaTaxaAdministracao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaTaxaAdministracao - Operacao: Delete TabelaTaxaAdministracao: " + tabelaTaxaAdministracaoClone.IdTabela + UtilitarioWeb.ToString(tabelaTaxaAdministracaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues[TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMinimo] = "0";
        e.NewValues[TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMaximo] = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        //if (!WebConfig.AppSettings.CalculaContatil) {
        if (!ParametrosConfiguracaoSistema.Outras.CalculaContatil) {

            Control c1 = this.gridCadastro.FindEditFormTemplateControl("divEventos");
            if (c1 != null)
            {
                c1.Visible = false;
            }
        }

        base.panelEdicao_Load(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo descricao.
    /// </summary>
    public void FiltroGridEvento()
    {
        GridViewDataColumn colDescricao = gridEvento.Columns["Descricao"] as GridViewDataColumn;
        if (colDescricao != null)
        {
            colDescricao.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}