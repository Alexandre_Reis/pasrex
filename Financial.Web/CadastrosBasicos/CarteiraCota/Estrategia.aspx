﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Estrategia.aspx.cs" Inherits="CadastrosBasicos_Estrategia" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = false;
    var operacao ='';
    document.onkeydown=onDocumentKeyDown;
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Estratégias"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdEstrategia"
                                        DataSourceID="EsDSEstrategia" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdEstrategia" Caption="Id.Estratégia" Width="7%"
                                                VisibleIndex="1" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Width="80%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="300">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Benchmark" FieldName="IdIndiceBenchmark"
                                                VisibleIndex="3" Width="15%" PropertiesComboBox-DropDownStyle="DropDownList">
                                                <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice">
                                                    <ValidationSettings RequiredField-ErrorText="">
                                                    </ValidationSettings>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ClientInstanceName="lblIdEstrategia" CssClass="labelNormal" runat="server"
                                                                    ID="lblIdEstrategia" Text="Id.Estratégia">
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textIdEstrategia" ClientInstanceName="textIdEstrategia" runat="server"
                                                                    CssClass="textButtonEdit" BackColor="#e9ebe8" Text='<%#Eval("IdEstrategia")%>'
                                                                    ClientEnabled="false">
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                    CssClass="textLongo" Text='<%# Eval("Descricao") %>'>
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblBenchmark" runat="server" CssClass="labelNormal" Text="Benchmark:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIndiceBenchmark" runat="server" ClientInstanceName="dropIndiceBenchmark"
                                                                    DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice" ShowShadow="False"
                                                                    DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%# Eval("IdIndiceBenchmark") %>'
                                                                    ValueType="System.String">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="480px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
                        if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }
                    }" EndCallback="function(s, e) {
                        if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
    </form>
</body>
</html>
