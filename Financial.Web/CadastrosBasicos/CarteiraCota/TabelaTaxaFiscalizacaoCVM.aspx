﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaTaxaFiscalizacaoCVM.aspx.cs" Inherits="CadastrosBasicos_TabelaTaxaFiscalizacaoCVM" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Taxa Fiscalização CVM"></asp:Label>
    </div>
    
    <div id="mainContent">
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaTaxaFiscalizacaoCVM"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"            
                >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True"/>
                
                <dxwgv:GridViewDataColumn FieldName="CompositeKey" UnboundType="String" Visible="False">                    
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCVM" VisibleIndex="1" Width="65%">
                    <PropertiesComboBox>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Companhia Aberta" />
                        <dxe:ListEditItem Value="2" Text="Sociedade Beneficiária" />
                        <dxe:ListEditItem Value="3" Text="Instituição Financeira" />
                        <dxe:ListEditItem Value="4" Text="Investidor Estrangeiro" />                        
                        <dxe:ListEditItem Value="5" Text="Fundo Investimento" />
                        <dxe:ListEditItem Value="6" Text="Fundo Aplicação Cotas" />                        
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="FaixaPL" VisibleIndex="2" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" DecimalPlaces="2"></PropertiesSpinEdit> 
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorTaxa" VisibleIndex="4" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" DecimalPlaces="2"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
            </Columns>
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
                        
        </dxwgv:ASPxGridView>            
        </div>    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaTaxaFiscalizacaoCVM" runat="server" OnesSelect="EsDSTabelaTaxaFiscalizacaoCVM_esSelect" />    
        
    </form>
</body>
</html>


