﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_TaxaGlobal : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.FiltroGridTabelaAdmGlobal();
        this.FiltroGridTabelaAdmAssociada();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTaxaGlobal_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TaxaGlobalQuery taxaGlobalQuery = new TaxaGlobalQuery("T");
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoGlobalQuery = new TabelaTaxaAdministracaoQuery("Ttag");
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoAssociadaQuery = new TabelaTaxaAdministracaoQuery("Ttaa");
        CarteiraQuery carteiraGlobal = new CarteiraQuery("Cg");
        CarteiraQuery carteiraAssociada = new CarteiraQuery("Ca");

        taxaGlobalQuery.Select(taxaGlobalQuery,
                               carteiraGlobal.IdCarteira,
                               carteiraGlobal.Apelido,
                               carteiraAssociada.IdCarteira, 
                               carteiraAssociada.Apelido, 
                               tabelaTaxaAdministracaoGlobalQuery.DataReferencia, 
                               tabelaTaxaAdministracaoAssociadaQuery.DataReferencia
            //, "<'' as DescricaoCompleta >"
            );

        taxaGlobalQuery.InnerJoin(tabelaTaxaAdministracaoGlobalQuery).On(taxaGlobalQuery.IdTabelaGlobal == tabelaTaxaAdministracaoGlobalQuery.IdTabela);
        taxaGlobalQuery.InnerJoin(carteiraGlobal).On(tabelaTaxaAdministracaoGlobalQuery.IdCarteira == carteiraGlobal.IdCarteira);
        taxaGlobalQuery.InnerJoin(tabelaTaxaAdministracaoAssociadaQuery).On(taxaGlobalQuery.IdTabelaAssociada == tabelaTaxaAdministracaoAssociadaQuery.IdTabela);
        taxaGlobalQuery.InnerJoin(carteiraAssociada).On(tabelaTaxaAdministracaoAssociadaQuery.IdCarteira == carteiraAssociada.IdCarteira);
        //
        taxaGlobalQuery.OrderBy(tabelaTaxaAdministracaoGlobalQuery.DataReferencia.Descending);

        TaxaGlobalCollection coll = new TaxaGlobalCollection();
        coll.Load(taxaGlobalQuery);

        e.Collection = coll;
    }

    #endregion

    #region PopUp Taxa Administração Global
    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdmCons_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.Consolidadora.Equal("S"));
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;
    }


    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdmAssoc_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        ASPxTextBox hiddenIdTabelaGlobal = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaGlobal") as ASPxTextBox;
        TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
        if (hiddenIdTabelaGlobal != null)
        {
            tabelaTaxaAdministracao.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTabelaGlobal.Value));
        }


        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //


        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        if (hiddenIdTabelaGlobal != null)
        {
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdCarteira.Equal(tabelaTaxaAdministracao.IdCarteira));
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela.NotEqual(tabelaTaxaAdministracao.IdTabela));
        }
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;
    }


    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdmAssoc_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e, string parameters)
    {

        ASPxTextBox hiddenIdTabelaGlobal = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaGlobal") as ASPxTextBox;

        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmGlobal_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idTabela = (int)gridView.GetRowValues(visibleIndex, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);

        e.Result = idTabela.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmGlobal_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmGlobal_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridTabelaAdmGlobal.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmAssociada_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idTabela = (int)gridView.GetRowValues(visibleIndex, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);

        e.Result = idTabela.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmAssociada_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdmAssociada_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridTabelaAdmAssociada.DataBind();
    }

    #endregion   

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTabela = Convert.ToInt32(e.Parameter);

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            //
            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela == idTabela);

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {
                //int idTabela = Convert.ToString(tab.IdTabela.Value);
                string idCarteira = Convert.ToString(tab.IdCarteira.Value);
                string apelido = Convert.ToString(tab.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                DateTime dataReferencia = tab.DataReferencia.Value;
                //
                texto = "Id:" + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");                        
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback_Associada(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTabela = Convert.ToInt32(e.Parameter);

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            //
            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela == idTabela);

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery))
            {
                //int idTabela = Convert.ToString(tab.IdTabela.Value);
                string idCarteira = Convert.ToString(tab.IdCarteira.Value);
                string apelido = Convert.ToString(tab.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                DateTime dataReferencia = tab.DataReferencia.Value;
                //
                texto = "Id:" + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditTabelaAdm_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textFaixaPL_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditTabelaAdmGlobal = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdmGlobal") as ASPxButtonEdit;
        ASPxButtonEdit btnEditTabelaAdmAssociada = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdmAssociada") as ASPxButtonEdit;
        ASPxTextBox textPrioridade = gridCadastro.FindEditFormTemplateControl("textPrioridade") as ASPxTextBox;
        ASPxTextBox hiddenIdTabelaGlobal = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaGlobal") as ASPxTextBox;
        ASPxTextBox hiddenIdTabelaAssociada = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaAssociada") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditTabelaAdmGlobal, btnEditTabelaAdmAssociada, textPrioridade });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        if (hiddenIdTabelaGlobal.Value.Equals(hiddenIdTabelaAssociada.Value))
        {
            e.Result = "As taxas de administração devem ser diferentes";
            return;
        }

        if (this.getTaxaAdm(Convert.ToInt32(hiddenIdTabelaGlobal.Text)).IdCarteira != this.getTaxaAdm(Convert.ToInt32(hiddenIdTabelaAssociada.Text)).IdCarteira)
        {
            e.Result = "As taxas de administração devem ser da mesma carteira.";
            return;
        }

        #endregion

    }

    private TabelaTaxaAdministracao getTaxaAdm(int idTabela)
    {
        TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
        tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela);
        return tabelaTaxaAdministracao;
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        TaxaGlobal taxaGlobal = new TaxaGlobal();
        //
        ASPxTextBox hiddenIdTabelaGlobal = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaGlobal") as ASPxTextBox;
        ASPxTextBox hiddenIdTabelaAssociada = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaAssociada") as ASPxTextBox;
        ASPxTextBox textPrioridade = gridCadastro.FindEditFormTemplateControl("textPrioridade") as ASPxTextBox;
        //

        taxaGlobal.IdTabelaGlobal = Convert.ToInt32(hiddenIdTabelaGlobal.Text);
        taxaGlobal.IdTabelaAssociada = Convert.ToInt32(hiddenIdTabelaAssociada.Text);
        taxaGlobal.Prioridade = Convert.ToInt32(textPrioridade.Text);

        taxaGlobal.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TaxaGlobal - Operacao: Insert TaxaGlobal: " + taxaGlobal.IdTaxaGlobal + "; " + UtilitarioWeb.ToString(taxaGlobal),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "DescricaoCompletaGlobal")
        {
            int idTabela = Convert.ToInt32(e.GetListSourceFieldValue(TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
            DateTime dataReferencia = Convert.ToDateTime(e.GetListSourceFieldValue("DataReferencia"));
            //
            e.Value = "id: " + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
        }

        if (e.Column.FieldName == "DescricaoCompletaAssociada")
        {
            int idTabela = Convert.ToInt32(e.GetListSourceFieldValue(TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira1"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido1"));
            DateTime dataReferencia = Convert.ToDateTime(e.GetListSourceFieldValue("DataReferencia1"));
            //
            e.Value = "id: " + idTabela + " - Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTaxaGlobal = (int)e.Keys[0];
        TaxaGlobal taxaGlobal = new TaxaGlobal();
        //
        ASPxTextBox hiddenIdTabelaGlobal = gridCadastro.FindEditFormTemplateControl("hiddenIdTabelaGlobal") as ASPxTextBox;
        ASPxTextBox textPrioridade = gridCadastro.FindEditFormTemplateControl("textPrioridade") as ASPxTextBox;
        //
        if (taxaGlobal.LoadByPrimaryKey(idTaxaGlobal))
        {
            taxaGlobal.IdTaxaGlobal = Convert.ToInt32(hiddenIdTabelaGlobal.Text);
            taxaGlobal.Prioridade = Convert.ToInt32(textPrioridade.Text);
            taxaGlobal.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TaxaGlobal - Operacao: Update TaxaGlobal: " + idTaxaGlobal,
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdTabela = gridCadastro.GetSelectedFieldValues(TaxaGlobalMetadata.ColumnNames.IdTaxaGlobal);

            for (int i = 0; i < keyValuesIdTabela.Count; i++) {
                int idTaxaGlobal = Convert.ToInt32(keyValuesIdTabela[i]);

                TaxaGlobal taxaGlobal = new TaxaGlobal();
                if (taxaGlobal.LoadByPrimaryKey(idTaxaGlobal))
                {
                    //
                    TaxaGlobal taxaGlobalClone = (TaxaGlobal)Utilitario.Clone(taxaGlobal);
                    //

                    taxaGlobal.MarkAsDeleted();
                    taxaGlobal.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TaxaGlobal - Operacao: Delete TaxaGlobal: " + taxaGlobalClone.IdTaxaGlobal + "; " + UtilitarioWeb.ToString(taxaGlobalClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTabelaAdm", "textPercentual");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo Tabela Adm Global.
    /// </summary>
    public void FiltroGridTabelaAdmGlobal() {
        GridViewDataColumn colApelidoCarteira = gridTabelaAdmGlobal.Columns[CarteiraMetadata.ColumnNames.Apelido] as GridViewDataColumn;
        if (colApelidoCarteira != null) {
            colApelidoCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }

    /// <summary>
    /// Troca para Like no filtro campo Tabela Adm Associada.
    /// </summary>
    public void FiltroGridTabelaAdmAssociada()
    {
        GridViewDataColumn colApelidoCarteira = gridTabelaAdmAssociada.Columns[CarteiraMetadata.ColumnNames.Apelido] as GridViewDataColumn;
        if (colApelidoCarteira != null)
        {
            colApelidoCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}