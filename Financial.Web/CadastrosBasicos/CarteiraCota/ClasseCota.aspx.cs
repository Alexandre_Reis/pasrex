﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Util;

public partial class CadastrosBasicos_ClasseCota : CadastroBasePage 
{
    #region DataSources
    protected void EsDSClasseCota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClasseCotaCollection coll = new ClasseCotaCollection();

        coll.Query.OrderBy(coll.Query.IdClasseCota.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ClasseCota classeCota = new ClasseCota();
        int idClasseCota = (int)e.Keys[0];

        if (classeCota.LoadByPrimaryKey(idClasseCota))
        {
            classeCota.Descricao = Convert.ToString(e.NewValues[ClasseCotaMetadata.ColumnNames.Descricao]);

            classeCota.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Classe de Cotas - Operacao: Update ClasseCota: " + idClasseCota + UtilitarioWeb.ToString(classeCota),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ClasseCota classeCota = new ClasseCota();

        classeCota.Descricao = Convert.ToString(e.NewValues[ClasseCotaMetadata.ColumnNames.Descricao]);

        classeCota.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Classes de Cotas - Operacao: Insert ClasseCota: " + classeCota.IdClasseCota + UtilitarioWeb.ToString(classeCota),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues(ClasseCotaMetadata.ColumnNames.IdClasseCota);
            for (int i = 0; i < keyValues.Count; i++)
            {
                int idClasseCota = Convert.ToInt32(keyValues[i]);

                ClasseCota classeCota = new ClasseCota();
                if (classeCota.LoadByPrimaryKey(idClasseCota))
                {

                    ClasseCota classeCotaClone = (ClasseCota)Utilitario.Clone(classeCota);

                    classeCota.MarkAsDeleted();
                    try
                    {
                        classeCota.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Classes de Cota - Operacao: Delete ClasseCota: " + idClasseCota + UtilitarioWeb.ToString(classeCotaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                    }
                    catch (Exception)
                    {
                        throw new Exception("Impossível Deletar ClasseCota. Classe de Cota Associada.");
                    }
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}