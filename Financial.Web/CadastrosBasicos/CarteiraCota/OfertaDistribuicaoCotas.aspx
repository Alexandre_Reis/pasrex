﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OfertaDistribuicaoCotas.aspx.cs" Inherits="CadastroBasicos_OfertaDistribuicaoCotas" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    } 
    
    function CalculaTotalEmissao(){
        var qtd = textQuantidadeTotalCotas.GetValue();
        var valorCota = textValorCota.GetValue();
        if(qtd != null && valorCota != null)
        {
            var total = qtd * valorCota;
            textValorTotalEmissao.SetValue(total);
        }
    }
    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
        
            var resultSplit = e.result.split('|');
         
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);
            
            dropApenasInvestidorProfissional.SetValue(resultSplit[1]);                 
            dropApenasInvestidorQualificado.SetValue(resultSplit[2]);
            }        
        "/>
    </dxcb:ASPxCallback>
        

    
    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Oferta de Distribuição de Cotas"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdOfertaDistribuicaoCotas" DataSourceID="EsDSOfertas"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn FieldName="IdOfertaDistribuicaoCotas" UnboundType="Integer" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="ID Carteira" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="left"></dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome" VisibleIndex="2" Width="32%" CellStyle-HorizontalAlign="left" UnboundType="String"></dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="PrimeiraEmissao" Caption="Primeira Emissão?" Width="7%" VisibleIndex="3">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="ApenasInvestidorProfissional" Caption="Apenas Investidor Profissional?" Width="10%" VisibleIndex="4">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="ApenasInvestidorQualificado" Caption="Apenas Investidor Qualificado?" Width="10%" VisibleIndex="6">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataInicialOferta" Caption="Início Oferta" VisibleIndex="7" Width="7%" /> 
                
                <dxwgv:GridViewDataDateColumn FieldName="DataFinalOferta" Caption="Final Oferta" VisibleIndex="8" Width="7%" />
                
                <dxwgv:GridViewDataTextColumn FieldName="QuantidadeTotalCotas" Caption="Quantidade Total Cotas" VisibleIndex="9" Width="8%">                    
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="ValorCota" Caption="Valor Cota" VisibleIndex="9" Width="8%"  >                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>           
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="TotalEmissao" Caption="Total da Emissão" VisibleIndex="10" Width="6%" UnboundType="Decimal">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>    
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Total de Aportes" VisibleIndex="11" Width="6%" UnboundType="Decimal">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>    
                </dxwgv:GridViewDataTextColumn>
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdCarteira" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCarteira")%>' ClientInstanceName="hiddenIdCarteira" />
                        
                        <table>
                   
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Fundo de Investimento:" />
                                </td>                                     
                                
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" style="float:left"
                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>' MaxLength="10"
                                        NumberType="Integer">
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>
                                        </Buttons>
                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                            NumberChanged=" function(s,e) { OnCarteiraChange(s); }" />
                                    </dxe:ASPxSpinEdit>

                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Nome")%>' ></asp:TextBox>
                                </td>
                                
                            </tr>  
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelDataProcessamento" runat="server" CssClass="labelRequired" Text="Primeira Emissão:"  />
                                </td> 
                                <td>
                                    <dxe:ASPxComboBox ID="dropPrimeiraEmissao" runat="server" ClientInstanceName="dropPrimeiraEmissao"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("PrimeiraEmissao")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Apenas Investidor Qualificado:"  />
                                </td> 
                                <td>
                                    <dxe:ASPxComboBox ID="dropApenasInvestidorQualificado" runat="server" ClientInstanceName="dropApenasInvestidorQualificado"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ApenasInvestidorQualificado")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelApenasInvestidorProfissional" runat="server" CssClass="labelRequired" Text="Apenas Investidor Profissional:"  />
                                </td> 
                                <td>
                                    <dxe:ASPxComboBox ID="dropApenasInvestidorProfissional" runat="server" ClientInstanceName="dropApenasInvestidorProfissional"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ApenasInvestidorProfissional")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataInicialOferta" runat="server" CssClass="labelRequired" Text="Data Inicial da Oferta:" />
                                </td>
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataInicialOferta" runat="server" ClientInstanceName="textDataInicialOferta"
                                        Value='<%#Eval("DataInicialOferta")%>' />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Data Final da Oferta:" />
                                </td>
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataFinalOferta" runat="server" ClientInstanceName="textDataFinalOferta"
                                        Value='<%#Eval("DataFinalOferta")%>' />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelQuantidadeTotalCotas" runat="server" CssClass="labelRequired" Text="Quantidade Total de Cotas:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textQuantidadeTotalCotas" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidadeTotalCotas"
                                        MaxLength="12" NumberType="Integer" Value='<%#Eval("QuantidadeTotalCotas")%>'>
                                    <ClientSideEvents ValueChanged="CalculaTotalEmissao" />
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelValorCota" runat="server" CssClass="labelRequired" Text="Valor da Cota:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textValorCota" runat="server" CssClass="textValor_5" ClientInstanceName="textValorCota"
                                        MaxLength="12" NumberType="float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" Value='<%#Eval("ValorCota")%>'>
                                    <ClientSideEvents ValueChanged="CalculaTotalEmissao" />
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelValorTotalEmissao" runat="server" CssClass="labelNormal" Text="Valor Total da Emissão:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textValorTotalEmissao" runat="server" CssClass="textValor_5" ClientInstanceName="textValorTotalEmissao"
                                        MaxLength="12" NumberType="float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" Enabled="false" Value='<%#Eval("TotalEmissao")%>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelCotaEspecifica" runat="server" CssClass="labelRequired" Text="Cota Especifíca:"  />
                                </td> 
                                <td>
                                    <dxe:ASPxComboBox ID="dropCotaEspecifica" runat="server" ClientInstanceName="dropCotaEspecifica"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("CotaEspecifica")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                                                      
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="700px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSOfertas" runat="server" OnesSelect="EsDSOfertas_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>