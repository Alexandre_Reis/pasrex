using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaTaxaFiscalizacaoCVM : Financial.Web.Common.CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaTaxaFiscalizacaoCVM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaFiscalizacaoCVMCollection coll = new TabelaTaxaFiscalizacaoCVMCollection();

        coll.Query.OrderBy(coll.Query.TipoCVM.Ascending, coll.Query.FaixaPL.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        TabelaTaxaFiscalizacaoCVM tabelaTaxaFiscalizacaoCVM = new TabelaTaxaFiscalizacaoCVM();

        if (!tabelaTaxaFiscalizacaoCVM.LoadByPrimaryKey(Convert.ToInt32(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM]), Convert.ToDecimal(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL]))) {
            tabelaTaxaFiscalizacaoCVM.TipoCVM = Convert.ToInt32(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM]);
            tabelaTaxaFiscalizacaoCVM.FaixaPL = Convert.ToDecimal(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL]);
            tabelaTaxaFiscalizacaoCVM.ValorTaxa = Convert.ToDecimal(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa]);
            tabelaTaxaFiscalizacaoCVM.Save();

            //
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaTaxaFiscalizacaoCVM - Operacao: Insert TabelaTaxaFiscalizacaoCVM: " + tabelaTaxaFiscalizacaoCVM.TipoCVM + " " + tabelaTaxaFiscalizacaoCVM.FaixaPL + " " + UtilitarioWeb.ToString(tabelaTaxaFiscalizacaoCVM),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TabelaTaxaFiscalizacaoCVM tabelaTaxaFiscalizacaoCVM = new TabelaTaxaFiscalizacaoCVM();
        int tipoCVM = (int)e.OldValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM];
        decimal faixaPL = (decimal)e.OldValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL];

        if (tabelaTaxaFiscalizacaoCVM.LoadByPrimaryKey(tipoCVM, faixaPL)) {
            tabelaTaxaFiscalizacaoCVM.ValorTaxa = Convert.ToDecimal(e.NewValues[TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa]);
            tabelaTaxaFiscalizacaoCVM.Save();
            //

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaTaxaFiscalizacaoCVM - Operacao: Update TabelaTaxaFiscalizacaoCVM: " + tabelaTaxaFiscalizacaoCVM.TipoCVM + " " + tabelaTaxaFiscalizacaoCVM.FaixaPL + " " + UtilitarioWeb.ToString(tabelaTaxaFiscalizacaoCVM),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesTipo = gridCadastro.GetSelectedFieldValues(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM);
            List<object> keyValuesFaixa = gridCadastro.GetSelectedFieldValues(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL);
            for (int i = 0; i < keyValuesTipo.Count; i++) {
                int tipoCVM = Convert.ToInt32(keyValuesTipo[i]);
                decimal faixaPL = Convert.ToDecimal(keyValuesFaixa[i]);

                TabelaTaxaFiscalizacaoCVM tabelaTaxaFiscalizacaoCVM = new TabelaTaxaFiscalizacaoCVM();
                if (tabelaTaxaFiscalizacaoCVM.LoadByPrimaryKey(tipoCVM, faixaPL)) {

                    TabelaTaxaFiscalizacaoCVM tabelaTaxaFiscalizacaoCVMClone = (TabelaTaxaFiscalizacaoCVM)Utilitario.Clone(tabelaTaxaFiscalizacaoCVM);

                    tabelaTaxaFiscalizacaoCVM.MarkAsDeleted();
                    tabelaTaxaFiscalizacaoCVM.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaTaxaFiscalizacaoCVM - Operacao: Delete TabelaTaxaFiscalizacaoCVM: " + tabelaTaxaFiscalizacaoCVM.TipoCVM + " " + tabelaTaxaFiscalizacaoCVM.FaixaPL + " " + UtilitarioWeb.ToString(tabelaTaxaFiscalizacaoCVMClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string tipoCVM = Convert.ToString(e.GetListSourceFieldValue(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM));
            string faixaPL = Convert.ToString(e.GetListSourceFieldValue(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL));
            e.Value = tipoCVM + "|" + faixaPL;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;

        if (!gridCadastro.IsNewRowEditing) {
            e.Editor.Enabled = false;
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
        }
    }
}