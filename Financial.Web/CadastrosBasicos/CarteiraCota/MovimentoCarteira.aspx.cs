﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Util;
using Financial.Common;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.Web.Common;

using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_MovimentoCarteira : CadastroBasePage
{
    //Objeto para controle de acesso(thread)
    private static readonly object locker = new object();
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { OperacaoCotistaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources
    protected void EsDSOperacaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        operacaoCotistaQuery.Select(operacaoCotistaQuery,
                                    cotistaQuery.Apelido.As("ApelidoCotista"),
                                    carteiraQuery.Apelido.As("ApelidoCarteira"));
        operacaoCotistaQuery.InnerJoin(cotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
        operacaoCotistaQuery.InnerJoin(carteiraQuery).On(operacaoCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);
        operacaoCotistaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        operacaoCotistaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoCotistaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario) &
                                   operacaoCotistaQuery.IdCotista.Equal(cotistaQuery.IdCotista) &
                                   operacaoCotistaQuery.IdCarteira.Equal(cotistaQuery.IdCarteira));

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira == Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao >= textDataInicio.Text);
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao <= textDataFim.Text);
        }

        if (dropTipoOperacaoFiltro.SelectedIndex > -1)
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao == Convert.ToByte(dropTipoOperacaoFiltro.SelectedItem.Value));
        }
        
        operacaoCotistaQuery.OrderBy(operacaoCotistaQuery.DataOperacao.Descending);

        OperacaoCotistaCollection coll = new OperacaoCotistaCollection();
        coll.Load(operacaoCotistaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;

        if (btnEditCodigoCarteira != null && btnEditCodigoCarteira.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdPessoa);
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                ContaCorrenteCollection coll = new ContaCorrenteCollection();
                coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa.Value) | coll.Query.IdCliente.Equal(idCliente));

                if (dropMoeda.SelectedIndex != -1)
                {
                    coll.Query.Where(coll.Query.IdMoeda.Equal(Convert.ToInt32(dropMoeda.SelectedItem.Value)));
                }

                coll.Query.OrderBy(coll.Query.Numero.Ascending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }

        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Busca o flag de multi-conta
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
            ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
            Label labelMoeda = gridCadastro.FindEditFormTemplateControl("labelMoeda") as Label;
            ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;

            if (!multiConta)
            {
                labelConta.Visible = false;
                dropConta.Visible = false;
                labelMoeda.Visible = false;
                dropMoeda.Visible = false;
            }
            else
            {
                try
                {
                    if (dropMoeda.SelectedIndex <= 0)
                    {
                        int idConta = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, LiquidacaoMetadata.ColumnNames.IdConta));

                        if (idConta > 0)
                        {
                            ContaCorrente contaCorrente = new ContaCorrente();
                            List<esQueryItem> campos = new List<esQueryItem>();
                            campos.Add(contaCorrente.Query.IdMoeda);
                            if (contaCorrente.LoadByPrimaryKey(campos, (int)idConta))
                            {
                                int idmoeda = contaCorrente.IdMoeda.Value;

                                if (idmoeda > 0)
                                {
                                    Moeda moeda = new Moeda();
                                    List<esQueryItem> mcampos = new List<esQueryItem>();
                                    mcampos.Add(moeda.Query.Nome);
                                    moeda.Query.Where(moeda.Query.IdMoeda.Equal(idmoeda));
                                    if (moeda.Query.Load())
                                    {
                                        dropMoeda.Text = moeda.Nome;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    
                }
            }

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCarteira = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, CarteiraMetadata.ColumnNames.IdCarteira));
            DateTime dataOperacao = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, OperacaoCotistaMetadata.ColumnNames.DataOperacao));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Carteira está fechada. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, dataOperacao) > 0)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data da carteira. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!gridCadastro.IsEditing)
        {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);
            List<object> keyDataOperacao = gridCadastro.GetSelectedFieldValues(OperacaoCotistaMetadata.ColumnNames.DataOperacao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime dataOperacao = Convert.ToDateTime(keyDataOperacao[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCarteira))
                {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado)
                    {
                        e.Result = "Carteira " + clienteDescricao + " está fechada! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, dataOperacao) > 0)
                    {
                        e.Result = "Lançamento com data " + dataOperacao.ToString().Substring(0, 10) + " anterior à data da carteira " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
            #endregion
        }
        else
        {
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCarteira);
            controles.Add(textDataOperacao);
            controles.Add(dropTipoOperacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Carteira está fechada! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textDataOperacao.Text)) > 0)
                {
                    e.Result = "Data informada anterior à data da carteira! Operação não pode ser realizada.";
                    return;
                }
            }

            byte tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            if (tipoOperacao != (byte)TipoOperacaoCotista.ResgateTotal && textValor.Text == "")
            {
                e.Result = "Para operação diferente de resgate total, é preciso informar o campo de Valor/Qtde.";
                return;
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                     posicaoCotistaCollection.Query.IdCotista.Equal(idCarteira));
                if (!posicaoCotistaCollection.Query.Load())
                {
                    e.Result = "O Cliente não possui saldo de aplicação para resgate.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        Carteira carteira = new Carteira();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name))
                        {

                            nome = carteira.str.Apelido;
                            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;

                            DateTime dataOperacao = (textDataOperacao != null && textDataOperacao.Text != "")
                                           ? Convert.ToDateTime(textDataOperacao.Text)
                                           : cliente.DataDia.Value;

                            resultado = nome + "|" + dataOperacao.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        OperacaoCotista operacaoCotista = new OperacaoCotista();
        if (operacaoCotista.LoadByPrimaryKey(idOperacao))
        {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DiasCotizacaoAplicacao);
            campos.Add(carteira.Query.DiasCotizacaoResgate);
            campos.Add(carteira.Query.DiasLiquidacaoAplicacao);
            campos.Add(carteira.Query.DiasLiquidacaoResgate);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            DateTime dataCotizacao = new DateTime();
            DateTime dataLiquidacao = new DateTime();
            if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.Aplicacao ||
                Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
                Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                dataCotizacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
                dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
            }
            else
            {
                dataCotizacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
            }

            operacaoCotista.IdCarteira = idCarteira;
            operacaoCotista.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            operacaoCotista.DataRegistro = operacaoCotista.DataOperacao;
            operacaoCotista.DataConversao = dataCotizacao;
            operacaoCotista.DataLiquidacao = dataLiquidacao;
            operacaoCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            operacaoCotista.Observacao = textObservacao.Text.ToString();

            //Salva o valor dependendo do tipo de operação, o resto força zerado
            //Resgate total força tudo zerado!
            operacaoCotista.ValorBruto = 0;
            operacaoCotista.ValorLiquido = 0;
            operacaoCotista.Quantidade = 0;

            switch (operacaoCotista.TipoOperacao)
            {
                case (byte)TipoOperacaoCotista.Aplicacao:
                case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
                case (byte)TipoOperacaoCotista.ResgateBruto:
                    operacaoCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                    operacaoCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                    break;
            }

            if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
            {
                operacaoCotista.TipoResgate = null;
            }
            else
            {
                // Se TipoOperacao = Resgate: TipoResgate = FIFO
                if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                    operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal ||
                    operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial)
                {

                    operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
                }
            }

            if (dropConta.SelectedIndex != -1)
            {
                operacaoCotista.IdConta = Convert.ToInt32(dropConta.SelectedItem.Value);
            }

            operacaoCotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoCotista - Operacao: Update OperacaoCotista: " + operacaoCotista.IdOperacao + UtilitarioWeb.ToString(operacaoCotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        int idCarteiraCotista = 0;
        int idCotista = 0;
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        Carteira carteira = new Carteira();
        List<esQueryItem> camposCarteira = new List<esQueryItem>();
        camposCarteira.Add(carteira.Query.DiasCotizacaoAplicacao);
        camposCarteira.Add(carteira.Query.DiasCotizacaoResgate);
        camposCarteira.Add(carteira.Query.DiasLiquidacaoAplicacao);
        camposCarteira.Add(carteira.Query.DiasLiquidacaoResgate);
        carteira.LoadByPrimaryKey(camposCarteira, idCarteira);

        OperacaoCotista operacaoCotista = new OperacaoCotista();

        //"Tranca" o processo até que o processamento seja concluído
        lock (locker)
        {
            #region Cria o Cotista se não existir
            //Cotista c = new Cotista();
            CotistaCollection c = new CotistaCollection();

            #region Retorna IdCarteira
            CotistaCollection cotistaCollection = new CotistaCollection();
            cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista.Max(), cotistaCollection.Query.IdCarteira);
            cotistaCollection.Query.Where(cotistaCollection.Query.IdCarteira == idCarteira);
            cotistaCollection.Query.GroupBy(cotistaCollection.Query.IdCotista.Max(), cotistaCollection.Query.IdCarteira);
            cotistaCollection.Query.Load();
            
            if (cotistaCollection.Count > 0 && cotistaCollection[0].IdCotista != null)
            {
                idCarteiraCotista = Convert.ToInt32(cotistaCollection[0].IdCarteira);
                idCotista = Convert.ToInt32(cotistaCollection[0].IdCotista);
            }
            #endregion

            //Não existe carteira
            if (idCarteiraCotista == 0)
            {
                Cotista cotistaAux = new Cotista();
                //            
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idCarteira);

                #region Busca Max Value Cotista
                CotistaCollection coll = new CotistaCollection();
                coll.Query.Select(coll.Query.IdCotista.Max());
                coll.Query.Load();
                idCotista = 0;
                if (coll.Count > 0)
                    idCotista = Convert.ToInt32(coll[0].IdCotista) + 1;
                #endregion

                cotistaAux.IdCotista = idCotista;
                cotistaAux.IdCarteira = idCarteira;
                cotistaAux.Nome = cliente.str.Apelido;
                cotistaAux.Apelido = cliente.str.Apelido;
                cotistaAux.IsentoIR = "S";
                cotistaAux.IsentoIOF = "S";
                cotistaAux.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                cotistaAux.TipoTributacao = (byte)TipoTributacaoCotista.Residente;
                cotistaAux.IdPessoa = Convert.ToInt32(cliente.str.IdPessoa);

                #region Verifica se IdCotista já existe/Salva o registro
                bool IdCotistaNovo = true;
                do
                {
                    try
                    {
                        cotistaAux.Save();
                        IdCotistaNovo = true;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        if (ex.Class == 14 && ex.Number == 2627)
                        {
                            IdCotistaNovo = false;
                            cotistaAux.IdCotista = cotistaAux.IdCotista + 1;
                        }
                        else
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                } while (IdCotistaNovo.Equals(false));
                #endregion

            }
            #endregion
        }

        operacaoCotista.IdCarteira = idCarteira;
        operacaoCotista.IdCotista = idCotista;
        operacaoCotista.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
        operacaoCotista.DataRegistro = operacaoCotista.DataOperacao;

        DateTime dataCotizacao = new DateTime();
        DateTime dataLiquidacao = new DateTime();
        if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.Aplicacao ||
            Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
            Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
        {
            dataCotizacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
            dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
        }
        else
        {
            dataCotizacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
            dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
        }

        operacaoCotista.DataConversao = dataCotizacao;
        operacaoCotista.DataLiquidacao = dataLiquidacao;
        operacaoCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        operacaoCotista.DataAgendamento = operacaoCotista.DataOperacao;

        // Se TipoOperacao = Resgate: TipoResgate = FIFO
        if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
            operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal ||
            operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial)
        {

            operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
        }
        operacaoCotista.Observacao = textObservacao.Text.ToString();

        //Salva o valor dependendo do tipo de operação, o resto força zerado
        //Resgate total força tudo zerado!
        operacaoCotista.ValorBruto = 0;
        operacaoCotista.ValorLiquido = 0;
        operacaoCotista.Quantidade = 0;

        switch (operacaoCotista.TipoOperacao)
        {
            case (byte)TipoOperacaoCotista.Aplicacao:
            case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
            case (byte)TipoOperacaoCotista.ResgateBruto:
                operacaoCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
            case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                operacaoCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                break;
        }

        operacaoCotista.IdFormaLiquidacao = 1; // Fixo

        Cliente clienteMae = new Cliente();
        bool isMae = clienteMae.IsMae(idCarteira);

        if (isMae)
        {
            operacaoCotista.Fonte = (byte)FonteOperacaoCotista.OperacaoMae ;
        }
        else
        {
            operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
        }

        if (dropConta.SelectedIndex != -1)
        {
            operacaoCotista.IdConta = Convert.ToInt32(dropConta.SelectedItem.Value);
        }       

        operacaoCotista.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoCotista - Operacao: Insert OperacaoCotista: " + operacaoCotista.IdOperacao + UtilitarioWeb.ToString(operacaoCotista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar
        Cliente clienteAux = new Cliente();
        clienteAux.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "TipoOperacao" }));
            if (!gridCadastro.IsNewRowEditing)
            {
                switch (tipoOperacao)
                {
                    case (byte)TipoOperacaoCotista.Aplicacao:
                    case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
                    case (byte)TipoOperacaoCotista.ResgateBruto:
                    case (byte)TipoOperacaoCotista.ResgateTotal:
                        decimal valorBruto = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorBruto" }));
                        textValor.Text = valorBruto.ToString();
                        break;
                    case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
                    case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                        decimal quantidade = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Quantidade" }));
                        textValor.Text = quantidade.ToString();
                        break;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoCotista operacaoCotista = new OperacaoCotista();
                if (operacaoCotista.LoadByPrimaryKey(idOperacao))
                {
                    int idCarteira = operacaoCotista.IdCarteira.Value;

                    DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
                    detalheResgateCotistaCollection.Query.Select(detalheResgateCotistaCollection.Query.IdOperacao,
                                                                 detalheResgateCotistaCollection.Query.IdPosicaoResgatada);
                    detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdOperacao.Equal(idOperacao));
                    detalheResgateCotistaCollection.Query.Load();
                    detalheResgateCotistaCollection.MarkAllAsDeleted();
                    detalheResgateCotistaCollection.Save();

                    //
                    OperacaoCotista operacaoCotistaClone = (OperacaoCotista)Utilitario.Clone(operacaoCotista);
                    //

                    operacaoCotista.MarkAsDeleted();
                    operacaoCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoCotista - Operacao: Delete OperacaoCotista: " + idOperacao + UtilitarioWeb.ToString(operacaoCotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Tipo = ").Append(dropTipoOperacaoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "ContaDescricao")
        {
            try
            {
                int idConta = Convert.ToInt32(e.GetListSourceFieldValue(LiquidacaoMetadata.ColumnNames.IdConta));
                ContaCorrente contaCorrente = new ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.Numero);
                contaCorrente.LoadByPrimaryKey(campos, idConta);

                e.Value = contaCorrente.Numero;
            }
            catch (Exception)
            {
                
            }
        }
    }

}