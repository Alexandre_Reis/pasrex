﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_SubCategoriaFundo : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {       
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSSubCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SubCategoriaFundoCollection coll = new SubCategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaFundoCollection coll = new CategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        const string DELETE = "btnDelete";

        if (e.Parameters == DELETE)
        {
            List<object> keyValuesIdCategoria = gridCadastro.GetSelectedFieldValues(SubCategoriaFundoMetadata.ColumnNames.IdCategoria);
            List<object> keyValuesIdSubCategoria = gridCadastro.GetSelectedFieldValues(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria);
            for (int i = 0; i < keyValuesIdCategoria.Count; i++)
            {
                int idCategoria = Convert.ToInt32(keyValuesIdCategoria[i]);
                int idSubCategoria = Convert.ToInt32(keyValuesIdSubCategoria[i]);

                SubCategoriaFundo subCategoriaFundo = new SubCategoriaFundo();
                if (subCategoriaFundo.LoadByPrimaryKey(idCategoria, idSubCategoria))
                {
                    if (idCategoria <= 348 || idSubCategoria <= 348) //Não permite deletar subcategorias da Ambima, nem as básicas (Carteira Adm e Clube)
                    {
                        throw new Exception("SubCategoria " + subCategoriaFundo.Descricao + " não pode ser excluída por ser primária do sistema.");
                    }

                    CarteiraCollection carteiraCollection = new CarteiraCollection();
                    carteiraCollection.Query.Where(carteiraCollection.Query.IdSubCategoria == idSubCategoria);

                    if (carteiraCollection.Query.Load())
                    {
                        throw new Exception("SubCategoria " + subCategoriaFundo.Descricao + " não pode ser excluída por ter carteiras relacionadas.");
                    }

                    SubCategoriaFundo subCategoriaFundoClone = (SubCategoriaFundo)Utilitario.Clone(subCategoriaFundo);
                    //

                    subCategoriaFundo.MarkAsDeleted();
                    subCategoriaFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de SubCategoriaFundo - Operacao: Delete SubCategoriaFundo: " + subCategoriaFundoClone.IdCategoria + "; " + subCategoriaFundoClone.IdSubCategoria + UtilitarioWeb.ToString(subCategoriaFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCategoria = Convert.ToString(e.GetListSourceFieldValue(SubCategoriaFundoMetadata.ColumnNames.IdCategoria));
            string idSubCategoria = Convert.ToString(e.GetListSourceFieldValue(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria));
            e.Value = idCategoria + "|" + idSubCategoria;
        }
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SubCategoriaFundoMetadata.ColumnNames.IdCategoria)
            {
                e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;

        SubCategoriaFundo categoriaFundo = new SubCategoriaFundo();
        string[] keys = e.Keys[0].ToString().Split('|');

        if (categoriaFundo.LoadByPrimaryKey(Convert.ToInt32(keys[0]), Convert.ToInt32(keys[1])))
        {
            categoriaFundo.Descricao = textDescricao.Text;
            categoriaFundo.IdCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);
            categoriaFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de SubCategoriaFundo - Operacao: Update SubCategoriaFundo: " + categoriaFundo.IdSubCategoria.Value + "|" + categoriaFundo.IdCategoria.Value + UtilitarioWeb.ToString(categoriaFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;

        SubCategoriaFundo categoriaFundo = new SubCategoriaFundo();

        categoriaFundo.Descricao = textDescricao.Text;
        categoriaFundo.IdCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);
        categoriaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SubCategoriaFundo - Operacao: Insert SubCategoriaFundo: " + categoriaFundo.IdSubCategoria.Value + "|" + categoriaFundo.IdCategoria.Value + UtilitarioWeb.ToString(categoriaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;

        SubCategoriaFundo categoriaFundo = new SubCategoriaFundo();

        categoriaFundo.Descricao = textDescricao.Text;
        categoriaFundo.IdCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);
        categoriaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SubCategoriaFundo - Operacao: Insert SubCategoriaFundo: " + categoriaFundo.IdSubCategoria.Value + "|" + categoriaFundo.IdCategoria.Value + UtilitarioWeb.ToString(categoriaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion


        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropCategoriaFundo);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }
}