﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiquidacaoAvancada.aspx.cs"
    Inherits="CadastrosBasicos_ParametroLiqAvancado" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript">
        var popup = true;
        var tipoData = ''; 
        var operacao = '';
        var btnCliente = '';
        var textDataRegistro = null;
        var textDataOperacao = null;
        var visibleIndex = '';
        document.onkeydown = onDocumentKeyDown;

        function verificaTipoParametro(visibleIndex) 
        {    
            gridParametroAnalitico.GetRowValues(visibleIndex, 'IdParametroLiqAvancado', desabilitaSegundoParametro);
        }

        function desabilitaSegundoParametro(Value)
        {
            if(Value == 'C' || Value == 'D')
            {
                ParametroDois.enabled = false;
            }
        }

        function setaVariaveis(pTipoData)
        {
            gridParametroAnalitico.CancelEdit();
            hiddenTipoData.SetValue(pTipoData); 
            tipoData = pTipoData;
            
            if(gridCadastro.IsNewRowEditing() == true)
                hiddenIsNewRowEditing.SetValue('S');
            else
                hiddenIsNewRowEditing.SetValue('N');
        }
        
        function setaCamposSimulacao(resultado)
        {
                var str = resultado;
                var res = str.split('|'); 
                                   
                DataOperacao.SetValue(res[0]);
                PrimeiraData.SetValue(res[1]);
                SegundaData.SetValue(res[2]);

        }

        function isNumber(n) 
        {
           return !isNaN(parseFloat(n)) && isFinite(n);
        }
        
        function OnGetDataCliente(data) 
        {          
            if(!isNumber(data))
            {
                alert(data);
                return false;
            }
            
            if(btnCliente == 'Clone')
            {
                btnEditCodigoClienteClone.SetValue(data);        
                popupClienteCallBack.SendCallback(btnEditCodigoClienteClone.GetValue());
                popupCliente.HideWindow();
                btnEditCodigoClienteClone.Focus();
            }
            else
            {
                btnEditCodigoCliente.SetValue(data);        
                popupClienteCallBack.SendCallback(btnEditCodigoCliente.GetValue());
                popupCliente.HideWindow();
                LimpaCampos();
                btnEditCodigoCliente.Focus();
            }
            
        }    

        function CloseGridParametroAnalitico() 
        {
            callBackBtnVoltar.SendCallback(tipoData);
            popupParametroAnalitico.HideWindow();
        }    
        
        function LimpaCampos() 
        {
            btnEditParametroOperacao.SetValue(null);
            btnEditParametroConversao.SetValue(null);
            btnEditParametroLiquidacao.SetValue(null);
        }   
      
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callBackBtnVoltar" runat="server" OnCallback="callBackBtnVoltar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
                                           {
                                                if (e.result != null)
                                                {
                                                            if(tipoData == '1')
                                                            {                                                         
                                                                btnEditParametroOperacao.SetValue(e.result);
                                                            }
                                                            else if (tipoData == '2')
                                                            {                                                        
                                                                btnEditParametroConversao.SetValue(e.result);
                                                            }
                                                            else if (tipoData == '3')
                                                            {
                                                                btnEditParametroLiquidacao.SetValue(e.result);
                                                            }
                                                } 
                                           } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="popupClienteCallBack" runat="server" OnCallback="popupCliente_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                
                if(btnCliente == 'Clone')
                {
                    textApelidoClone = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelidoClone');
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteClone, textApelidoClone);                                                               
                }
                else
                {                
                    textApelido = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido');
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textApelido); 
                }                                                              
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {    
                if(operacao == 'btnEditParametro')        
                {   
                    hiddenCarregaGridParametro.SetValue('N');
                }
                alert(e.result);                                              
            }
            else if (tabCadastro.activeTabIndex == '1')
            {
                callbackClonar.SendCallback();
            }
            else if(operacao == 'btnEditParametro')
            {
                gridParametroAnalitico.PerformCallback('btnRefresh');
                hiddenCarregaGridParametro.SetValue(null);
                
                if(tipoData == '1')
                {                                                         
                    popupParametroAnalitico.ShowAtElementByID('btnEditParametroOperacao');
                }
                else if (tipoData == '2')
                {                                                        
                    popupParametroAnalitico.ShowAtElementByID('btnEditParametroConversao');
                }
                else if (tipoData == '3')
                {
                    popupParametroAnalitico.ShowAtElementByID('btnEditParametroLiquidacao');
                }                                                           
            }
            else if (tabCadastro.activeTabIndex == '2')
            {
                callbackSimular.SendCallback();
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
                        
            operacao = '';
        }                
         
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackClonar" runat="server" OnCallback="callbackClonar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro clonado com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackSimular" runat="server" OnCallback="callbackSimular_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                setaCamposSimulacao(e.result);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupParametroAnalitico" ClientInstanceName="popupParametroAnalitico"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridParametroAnalitico" runat="server" Width="100%" ClientInstanceName="gridParametroAnalitico"
                            AutoGenerateColumns="False" DataSourceID="EsDSParametroAnalitico" KeyFieldName="IdParametroLiqAvancado"
                            OnRowDeleting="gridParametroAnalitico_RowDeleting" EnableRowsCache="False" OnRowUpdating="gridParametroAnalitico_RowUpdating"
                            SettingsText-CommandUpdate="Atualizar" OnRowValidating="gridParametroAnalitico_RowValidating"
                            SettingsText-CommandDelete="Apagar" SettingsText-CommandCancel="Cancelar">
                            <Columns>
                                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowDeleteButton="True"/>
                                <dxwgv:GridViewDataTextColumn FieldName="IdParametroLiqAvancado" ReadOnly="True"
                                    VisibleIndex="1" Caption="Parâmetro">
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="SignificadoParametro" VisibleIndex="2" Caption="Significado"
                                    ReadOnly="true">
                                    <EditFormSettings Visible="false" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="VariacaoComplemento" VisibleIndex="3" Caption="Complemento"
                                    ReadOnly="true">
                                    <EditFormSettings Visible="false" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ParametroUm" VisibleIndex="5" UnboundType="String"
                                    Caption="1° parâmetro" Width="10%" EditFormSettings-Caption="1° parâmetro">
                                    <PropertiesSpinEdit NumberType="Integer" />
                                    <EditFormCaptionStyle HorizontalAlign="Right" />
                                    <EditFormSettings Visible="true" />
                                    <CellStyle Font-Bold="true" />
                                    <EditCellStyle Font-Bold="true" HorizontalAlign="Left" />
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ParametroDois" VisibleIndex="5" UnboundType="String"
                                    Caption="2° parâmetro" Width="10%" EditFormSettings-Caption="2° parâmetro">
                                    <PropertiesSpinEdit NumberType="Integer" ClientInstanceName="ParametroDois" ClientSideEvents-LostFocus="verificaTipoParametro(visibleIndex)" />
                                    <EditFormSettings Visible="true" />
                                    <EditFormCaptionStyle HorizontalAlign="Right" />
                                    <CellStyle Font-Bold="true" />
                                    <EditCellStyle Font-Bold="true" HorizontalAlign="Left" />
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="OrdemProcessamento" VisibleIndex="5"
                                    UnboundType="String" Caption="Ordem de Execução" Width="10%" EditFormSettings-Caption="Ordem">
                                    <PropertiesSpinEdit NumberType="Integer" ClientInstanceName="OrdemProcessamento" />
                                    <EditFormSettings Visible="true" />
                                    <EditFormCaptionStyle HorizontalAlign="Right" />
                                    <CellStyle Font-Bold="true" />
                                    <EditCellStyle Font-Bold="true" HorizontalAlign="Left" />
                                </dxwgv:GridViewDataSpinEditColumn>
                            </Columns>
                            <Settings ShowFooter="True" />
                            <ClientSideEvents RowDblClick="function(s, e) {s.StartEditRow(e.visibleIndex); visibleIndex=e.visibleIndex; verificaTipoParametro(visibleIndex)}" />
                            <SettingsCommandButton>
                                <DeleteButton>
                                    <Image Url="~\imagens\delete.png">
                                    </Image>
                                </DeleteButton>
                            </SettingsCommandButton>
                        </dxwgv:ASPxGridView>
                    </div>
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <dxe:ASPxButton ID="Close" runat="server" Font-Overline="false" BackgroundImage-ImageUrl="~\imagens\ico_form_ok.gif"
                            BackgroundImage-Repeat="NoRepeat" BackgroundImage-HorizontalPosition="center"
                            BackgroundImage-VerticalPosition="center" AutoPostBack="false">
                            <ClientSideEvents Click="CloseGridParametroAnalitico" />
                        </dxe:ASPxButton>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração Avançada de Critérios de Liquidação Física e Financeira "></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupCCC_Filtro" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="CompositeKey"
                                        ClientInstanceName="gridCadastro" DataSourceID="EsDSParametroLiqAvancado" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnRowInserting="gridCadastro_RowInserting" OnLoad="gridCadastro_Load" AutoGenerateColumns="False"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" Caption="Id.Carteira" ReadOnly="True"
                                                Width="5%" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoCarteira" Caption="Nome" ReadOnly="True"
                                                Width="20%" VisibleIndex="1" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVigencia" Caption="Data Vigência" ReadOnly="True"
                                                VisibleIndex="2" Width="7%">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo Operação"
                                                Width="5%" ReadOnly="True" VisibleIndex="3">
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataOperacao" Caption="Parâmetro Operação"
                                                VisibleIndex="4" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="StatusOperacao" Caption="Status - Operação"
                                                VisibleIndex="5" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Confirmado" />
                                                        <dxe:ListEditItem Value="N" Text="Necessário Confirmação" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataConversao" Caption="Parâmetro Conversão"
                                                VisibleIndex="6" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="StatusConversao" Caption="Status - Conversão"
                                                VisibleIndex="7" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Confirmado" />
                                                        <dxe:ListEditItem Value="N" Text="Necessário Confirmação" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataLiquidacao" Caption="Parâmetro Liquidação"
                                                VisibleIndex="8" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="StatusLiquidacao" Caption="Status - Liquidação"
                                                VisibleIndex="9" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Confirmado" />
                                                        <dxe:ListEditItem Value="N" Text="Necessário Confirmação" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                        </Columns>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                            EnableHierarchyRecreation="True" ActiveTabIndex="0" Height="100%" Width="100%"
                                                            OnLoad="tabCadastroOn_Load" TabSpacing="0px" EnableClientSideAPI="true">
                                                            <TabPages>
                                                                <dxtc:TabPage Text="Parâmetros" Name="tab1">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <dxe:ASPxTextBox ID="hiddenTipoData" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenTipoData">
                                                                            </dxe:ASPxTextBox>
                                                                            <dxe:ASPxTextBox ID="hiddenIsNewRowEditing" runat="server" CssClass="hiddenField"
                                                                                ClientInstanceName="hiddenIsNewRowEditing">
                                                                            </dxe:ASPxTextBox>
                                                                            <dxe:ASPxTextBox ID="hiddenCarregaGridParametro" runat="server" CssClass="hiddenField"
                                                                                ClientInstanceName="hiddenCarregaGridParametro">
                                                                            </dxe:ASPxTextBox>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCarteira")%>' MaxLength="10"
                                                                                            NumberType="Integer" OnInit="btnEditCodigoCliente_Init">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {textApelido = 'gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido';
		                                                                                                      document.getElementById(textApelido).value = ''; btnCliente='Cadastro';} "
                                                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name); btnCliente='Cadastro';}"
                                                                                                ValueChanged="LimpaCampos" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, popupClienteCallBack, btnEditCodigoCliente); btnCliente='Cadastro';}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textApelido" Style="width: 272px;" runat="server" CssClass="textNome"
                                                                                            Enabled="false" Text='<%#Eval("DescricaoCarteira")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelDtVigencia" runat="server" CssClass="labelRequired" Text="Dt.Vigência:" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxDateEdit ID="textDtVigencia" runat="server" ClientInstanceName="textDtVigencia"
                                                                                            Width="150px" Value='<%#Eval("DataVigencia")%>' OnInit="textDtVigencia_Init"
                                                                                            ClientSideEvents-DateChanged="LimpaCampos" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labeldTipoOperacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoOperacao"
                                                                                            Text="Tipo Operação:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" CssClass="dropDownListLongo"
                                                                                            OnLoad="dropTipoOperacao_Load" Text='<%#Eval("TipoOperacao")%>' OnInit="dropTipoOperacao_Init"
                                                                                            ClientSideEvents-SelectedIndexChanged="LimpaCampos">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblDtOperacao" runat="server" CssClass="labelNormal" Text="Data Operação:" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxButtonEdit ID="btnEditParametroOperacao" runat="server" CssClass="textButtonEdit"
                                                                                            EnableClientSideAPI="True" ClientInstanceName="btnEditParametroOperacao" ReadOnly="true"
                                                                                            Width="380px" Text='<%#Eval("DataOperacao")%>'>
                                                                                            <Buttons>
                                                                                                <dxe:EditButton />
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {setaVariaveis('1'); hiddenCarregaGridParametro.SetValue(null); operacao='btnEditParametro'; callbackErro.SendCallback(operacao);}" />
                                                                                        </dxe:ASPxButtonEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblDtConversao" runat="server" CssClass="labelRequired" Text="Data Conversão:" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxButtonEdit ID="btnEditParametroConversao" runat="server" CssClass="textButtonEdit"
                                                                                            EnableClientSideAPI="True" ClientInstanceName="btnEditParametroConversao" ReadOnly="true"
                                                                                            Width="380px" Text='<%#Eval("DataConversao")%>'>
                                                                                            <Buttons>
                                                                                                <dxe:EditButton />
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {setaVariaveis('2'); hiddenCarregaGridParametro.SetValue(null); operacao='btnEditParametro'; callbackErro.SendCallback(operacao);}" />
                                                                                        </dxe:ASPxButtonEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblDtLiquidacao" runat="server" CssClass="labelRequired" Text="Data Liquidação:" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxButtonEdit ID="btnEditParametroLiquidacao" runat="server" CssClass="textButtonEdit"
                                                                                            EnableClientSideAPI="True" ClientInstanceName="btnEditParametroLiquidacao" ReadOnly="true"
                                                                                            Width="380px" Text='<%#Eval("DataLiquidacao")%>'>
                                                                                            <Buttons>
                                                                                                <dxe:EditButton />
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {setaVariaveis('3'); hiddenCarregaGridParametro.SetValue(null); operacao='btnEditParametro'; callbackErro.SendCallback(operacao);}" />
                                                                                        </dxe:ASPxButtonEdit>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(tabCadastro.activeTabIndex); return false;">
                                                                                                <asp:Literal ID="Literal9" runat="server" Text="Salvar" />
                                                                                                <div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Clona Parâmetros" Name="tab2">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteClone" runat="server" CssClass="textButtonEdit1"
                                                                                            ClientInstanceName="btnEditCodigoClienteClone" MaxLength="9" NumberType="Integer">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {textApelidoClone = 'gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelidoClone';
		                                                                                                      document.getElementById(textApelidoClone).value = ''; btnCliente='Clone';} "
                                                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name); btnCliente='Clone';}"
                                                                                                LostFocus="function(s, e) { OnLostFocus(popupMensagemCliente, popupClienteCallBack, btnEditCodigoClienteClone); btnCliente='Clone'; }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textApelidoClone" runat="server" CssClass="textNome" Enabled="false"
                                                                                            Width="240">
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Dt.Vigência:" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxDateEdit ID="textDtVigenciaClone" runat="server" ClientInstanceName="textDtVigencia"
                                                                                            Width="150px" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label3" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoOperacao"
                                                                                            Text="Tipo Operação:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoOperacaoClone" runat="server" CssClass="dropDownListLongo"
                                                                                            OnLoad="dropTipoOperacaoClone_Load">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                            <asp:LinkButton ID="btnClonar" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                OnClientClick="if (operacao != '') return false; operacao='clonar'; callbackErro.SendCallback(tabCadastro.activeTabIndex); return false;">
                                                                                                <asp:Literal ID="Literal10" runat="server" Text="Clonar" />
                                                                                                <div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Simulação" Name="tab3">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <tr>
                                                                                        <td class="td_Label_Curto">
                                                                                            <asp:Label ID="labelLocal" runat="server" CssClass="labelNormal" AssociatedControlID="dropLocal"
                                                                                                Text="Local de Feriado:" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropLocal" runat="server" DataSourceID="EsDSLocalFeriado" ValueField="IdLocal"
                                                                                                TextField="Nome" CssClass="dropDownList"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="lblDataReferencia" runat="server" CssClass="labelRequired" Text="Data de Referência:" />
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <dxe:ASPxDateEdit ID="DataReferenciaSimulacao" runat="server" ClientInstanceName="DataReferenciaSimulacao"
                                                                                                Width="150px" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="lblDataOperacao" runat="server" CssClass="labelRequired" Text="Data de Operação:" />
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <dxe:ASPxSpinEdit ID="DataOperacao" runat="server" Width="150px" ClientInstanceName="DataOperacao"
                                                                                                ReadOnly="true">
                                                                                            </dxe:ASPxSpinEdit>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <tr>
                                                                                            <td class="td_Label">
                                                                                                <asp:Label ID="lblPrimeiraData" runat="server" CssClass="labelRequired" Text="Primeira Data:" />
                                                                                            </td>
                                                                                            <td colspan="2">
                                                                                                <dxe:ASPxSpinEdit ID="PrimeiraData" runat="server" Width="150px" ClientInstanceName="PrimeiraData"
                                                                                                    ReadOnly="true">
                                                                                                </dxe:ASPxSpinEdit>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="td_Label">
                                                                                                <asp:Label ID="lblSegundaData" runat="server" CssClass="labelRequired" Text="Segunda Data:" />
                                                                                            </td>
                                                                                            <td colspan="2">
                                                                                                <dxe:ASPxSpinEdit ID="SegundaData" runat="server" Width="150px" ClientInstanceName="SegundaData"
                                                                                                    ReadOnly="true">
                                                                                                </dxe:ASPxSpinEdit>
                                                                                            </td>
                                                                                        </tr>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                        </SettingsCommandButton>
                                                                                    </tr>
                                                                                </tr>
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                            <asp:LinkButton ID="btnSimular" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                OnClientClick="if (operacao != '') return false; operacao='simular'; callbackErro.SendCallback(tabCadastro.activeTabIndex); return false;">
                                                                                                <asp:Literal ID="Literal11" runat="server" Text="Simular" />
                                                                                                <div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                            </TabPages>
                                                        </dxtc:ASPxPageControl>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                                <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                                <div>
                                                                </div>
                                                            </asp:LinkButton></div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSParametroLiqAvancado" runat="server" OnesSelect="EsDSParametroLiqAvancado_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSParametroAnalitico" runat="server" OnesSelect="EsDSParametroAnalitico_esSelect" />
        <cc1:esDataSource ID="EsDSLocalFeriado" runat="server" OnesSelect="EsDSLocalFeriado_esSelect" />    
    </form>
</body>
</html>
