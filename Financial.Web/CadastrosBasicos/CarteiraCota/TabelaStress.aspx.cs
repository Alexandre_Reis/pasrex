﻿using System;
using DevExpress.Web;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Financial.Web.Common;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class CadastrosBasicos_TabelaStress : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { TabelaStressMetadata.ColumnNames.TipoAtivo }));

    }

    #region DataSources
    protected void EsDSTabelaStress_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaStressQuery tabelaStressQuery = new TabelaStressQuery("T");

        tabelaStressQuery.OrderBy(tabelaStressQuery.TipoAtivo.Ascending,
                                  tabelaStressQuery.DataReferencia.Ascending);

        TabelaStressCollection coll = new TabelaStressCollection();
        coll.Load(tabelaStressQuery);
        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxTextBox IdAtivo = gridCadastro.FindEditFormTemplateControl("textIdAtivo") as ASPxTextBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoAtivo = gridCadastro.FindEditFormTemplateControl("dropTipoAtivo") as ASPxComboBox;
        //
        ASPxSpinEdit textStress = gridCadastro.FindEditFormTemplateControl("textStress") as ASPxSpinEdit;

        #region Campos obrigatórios
        //
        List<Control> controles = new List<Control>();
        controles.Add(IdAtivo);
        controles.Add(textDataReferencia);
        controles.Add(dropTipoAtivo);
        controles.Add(textStress);
        //
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
               
        if (gridCadastro.IsNewRowEditing) {
            TabelaStress tabelaStress = new TabelaStress();
            if (tabelaStress.LoadByPrimaryKey(IdAtivo.Text.Trim(),
                                              Convert.ToDateTime(textDataReferencia.Text), 
                                              Convert.ToByte(dropTipoAtivo.SelectedItem.Value))) {
                e.Result = "Registro já existente";
                return;
            }
        }
    }

    #region Controla Visibilidade Campos Update
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    ///<summary>     
    ///</summary>
    ///<param name="sender"></param>
    ///<param name="e"></param>
    protected void textIdAtivo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxTextBox).ClientEnabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoAtivo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxTextBox IdAtivo = gridCadastro.FindEditFormTemplateControl("textIdAtivo") as ASPxTextBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoAtivo = gridCadastro.FindEditFormTemplateControl("dropTipoAtivo") as ASPxComboBox;
        ASPxSpinEdit textStress = gridCadastro.FindEditFormTemplateControl("textStress") as ASPxSpinEdit;
        //                
        TabelaStress tabelaStress = new TabelaStress();
        //
        tabelaStress.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        //
        tabelaStress.IdAtivo = Convert.ToString(IdAtivo.Text);
        tabelaStress.TipoAtivo = Convert.ToByte(dropTipoAtivo.SelectedItem.Value);
        tabelaStress.Stress = Convert.ToDecimal(textStress.Text);
        //        
        tabelaStress.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaStress - Operacao: Insert TabelaStress: " + tabelaStress.DataReferencia + "; " + tabelaStress.IdAtivo + "; " + tabelaStress.TipoAtivo + UtilitarioWeb.ToString(tabelaStress),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxTextBox IdAtivo = gridCadastro.FindEditFormTemplateControl("textIdAtivo") as ASPxTextBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoAtivo = gridCadastro.FindEditFormTemplateControl("dropTipoAtivo") as ASPxComboBox;
        ASPxSpinEdit textStress = gridCadastro.FindEditFormTemplateControl("textStress") as ASPxSpinEdit;
        //
        TabelaStress t = new TabelaStress();

         if (t.LoadByPrimaryKey(IdAtivo.Text.Trim(), Convert.ToDateTime(textDataReferencia.Text), Convert.ToByte(dropTipoAtivo.SelectedItem.Value))) {
            t.Stress = Convert.ToDecimal(textStress.Text);
            //
            t.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaStress - Operacao: Update TabelaStress: " + t.DataReferencia + "; " + t.IdAtivo + "; " + t.TipoAtivo + UtilitarioWeb.ToString(t),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(TabelaStressMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesIdAtivo = gridCadastro.GetSelectedFieldValues(TabelaStressMetadata.ColumnNames.IdAtivo);
            List<object> keyValuesTipoAtivo = gridCadastro.GetSelectedFieldValues(TabelaStressMetadata.ColumnNames.TipoAtivo);
            //
            TabelaStressCollection t = new TabelaStressCollection();
            //
            for (int i = 0; i < keyValuesDataReferencia.Count; i++) {
                DateTime dataRef = Convert.ToDateTime(keyValuesDataReferencia[i]);
                string idAtivo = Convert.ToString(keyValuesIdAtivo[i]);
                int tipoAtivo = Convert.ToInt32(keyValuesTipoAtivo[i]);

                TabelaStress tabelaStress = new TabelaStress();
                if (tabelaStress.LoadByPrimaryKey(idAtivo, dataRef, (byte)tipoAtivo)) {
                    t.AttachEntity(tabelaStress);
                }
            }

            TabelaStressCollection tabelaStressClone = (TabelaStressCollection)Utilitario.Clone(t);
            //
            // Delete com Transação
            t.MarkAllAsDeleted();
            t.Save();

            foreach (TabelaStress tAux in tabelaStressClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaStress - Operacao: Delete TabelaStress: " + tAux.IdAtivo + "; " + tAux.DataReferencia + "; " + tAux.TipoAtivo + UtilitarioWeb.ToString(tAux),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }            
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaStressMetadata.ColumnNames.DataReferencia));
            string idAtivo = Convert.ToString(e.GetListSourceFieldValue(TabelaStressMetadata.ColumnNames.IdAtivo));
            string tipoAtivo = Convert.ToString(e.GetListSourceFieldValue(TabelaStressMetadata.ColumnNames.TipoAtivo));

            e.Value = data + idAtivo + tipoAtivo;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textIdAtivo", "textStress");
        base.gridCadastro_PreRender(sender, e);
    }
}