﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class CadastrosBasicos_CategoriaFundo : Financial.Web.Common.CadastroBasePage
{
    #region DataSources
    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaFundoCollection coll = new CategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSListaCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ListaCategoriaFundoCollection coll = new ListaCategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropListaCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropListaCategoriaFundo") as ASPxComboBox;

        CategoriaFundo categoriaFundo = new CategoriaFundo();
        int idCategoria = (int)e.Keys[0];

        if (categoriaFundo.LoadByPrimaryKey(idCategoria))
        {
            categoriaFundo.Descricao = textDescricao.Text;
            categoriaFundo.IdLista = Convert.ToInt32(dropListaCategoriaFundo.SelectedItem.Value);
            categoriaFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CategoriaFundo - Operacao: Update CategoriaFundo: " + idCategoria + UtilitarioWeb.ToString(categoriaFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropListaCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropListaCategoriaFundo") as ASPxComboBox;

        CategoriaFundo categoriaFundo = new CategoriaFundo();

        categoriaFundo.Descricao = textDescricao.Text;
        categoriaFundo.IdLista = Convert.ToInt32(dropListaCategoriaFundo.SelectedItem.Value);
        categoriaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CategoriaFundo - Operacao: Insert CategoriaFundo: " + categoriaFundo.IdCategoria + UtilitarioWeb.ToString(categoriaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropListaCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropListaCategoriaFundo") as ASPxComboBox;

        CategoriaFundo categoriaFundo = new CategoriaFundo();

        categoriaFundo.Descricao = textDescricao.Text;
        categoriaFundo.IdLista = Convert.ToInt32(dropListaCategoriaFundo.SelectedItem.Value);
        categoriaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CategoriaFundo - Operacao: Insert CategoriaFundo: " + categoriaFundo.IdCategoria + UtilitarioWeb.ToString(categoriaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCategoria");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCategoria = Convert.ToInt32(keyValuesId[i]);

                CategoriaFundo categoriaFundo = new CategoriaFundo();
                if (categoriaFundo.LoadByPrimaryKey(idCategoria))
                {
                    if (idCategoria <= 348) //Não permite deletar categorias da Ambima, nem as básicas (Carteira Adm e Clube)
                    {
                        throw new Exception("Categoria " + categoriaFundo.Descricao + " não pode ser excluída por ser primária do sistema.");
                    }

                    CarteiraCollection carteiraCollection = new CarteiraCollection();
                    carteiraCollection.Query.Where(carteiraCollection.Query.IdCategoria.Equal(idCategoria));
                    if (carteiraCollection.Query.Load())
                    {
                        throw new Exception("Categoria " + categoriaFundo.Descricao + " não pode ser excluída por ter carteiras relacionadas.");
                    }

                    CategoriaFundo categoriaFundoClone = (CategoriaFundo)Utilitario.Clone(categoriaFundo);
                    //

                    categoriaFundo.MarkAsDeleted();
                    categoriaFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CategoriaFundo - Operacao: Delete CategoriaFundo: " + categoriaFundoClone.IdCategoria + UtilitarioWeb.ToString(categoriaFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropListaCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropListaCategoriaFundo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropListaCategoriaFundo);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }
}