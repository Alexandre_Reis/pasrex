﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaTaxaAdministracao.aspx.cs" Inherits="CadastrosBasicos_TabelaTaxaAdministracao" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;     
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }      
    
    function TrataCampos(BasePL)
    {
        if (BasePL==0) //Percentual PL
        {
            textValorFixoTotal.SetText('');
            textValorFixoTotal.SetEnabled(false);            
            dropBaseApuracao.SetEnabled(true);
            textTaxa.SetEnabled(true);
            dropBaseAno.SetEnabled(true);
            dropTipoApropriacao.SetEnabled(true);
        }
        else if (BasePL==1) //Valor Fixo
        {            
            textValorFixoTotal.SetEnabled(true);
            dropBaseApuracao.SetText('');
            dropBaseApuracao.SetEnabled(false);
            textTaxa.SetText('');
            textTaxa.SetEnabled(false);
            dropBaseAno.SetText('');
            dropBaseAno.SetEnabled(false);
            dropTipoApropriacao.SetText('');
            dropTipoApropriacao.SetEnabled(false);
        }
        else //%PL ou Valor Fixo
        {
            textValorFixoTotal.SetEnabled(true);            
            dropBaseApuracao.SetEnabled(true);
            textTaxa.SetEnabled(true);
            dropBaseAno.SetEnabled(true);
            dropTipoApropriacao.SetEnabled(true);
        }
    }
    
    function OnGetDataEvento(data) {        
        /* idEvento, Descricao */
        var resultSplit = data.split('|');
        
        if(gridEventoOperacao == 'eventoProvisao') {
            btnEditEventoProvisao.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoProvisao.Focus();
        }
        else if(gridEventoOperacao == 'eventoPagamento') {        
            btnEditEventoPagamento.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoPagamento.Focus();
        }
    }    
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
          if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupEvento" ClientInstanceName="popupEvento" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridEvento" runat="server" Width="100%"
                    ClientInstanceName="gridEvento"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSEvento" KeyFieldName="IdEvento"
                    OnCustomDataCallback="gridEvento_CustomDataCallback" 
                    OnCustomCallback="gridEvento_CustomCallback"
                    OnHtmlRowCreated="gridEvento_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdEvento" VisibleIndex="0" Visible="false"/>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="3" Width="70%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="Conta Débito" VisibleIndex="1" Width="15%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="Conta Crédito" VisibleIndex="2" Width="15%"/>  
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
            gridEvento.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataEvento);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Roteiro Contábil" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridEvento.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Taxa Administração / Gestão"></asp:Label>
    </div>
        
    <div id="mainContent">
                       
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdTabela" DataSourceID="EsDSTabelaTaxaAdministracao"
                    OnInitNewRow="gridCadastro_InitNewRow"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >     
                
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdTabela" Caption="Id" VisibleIndex="1" Width="5%"
                                              HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="9%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="30%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdCadastro" Caption="Descrição" VisibleIndex="5" Width="10%">
                        <PropertiesComboBox DataSourceID="EsDSCadastroTaxaAdministracao" TextField="Descricao" ValueField="IdCadastro"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCalculo" Caption="Tipo" VisibleIndex="6" Width="8%">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Percentual PL'>Percentual PL</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Valor Fixo'>Valor Fixo</div>" />
                                <dxe:ListEditItem Value="3" Text="<div title='%PL ou Valor Fixo'>%PL ou Valor Fixo</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>                
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ValorFixoTotal" Caption="Vl. Fixo" VisibleIndex="7" Width="8%" HeaderStyle-HorizontalAlign="Center">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Taxa" Caption="Taxa (%)" VisibleIndex="8" Width="6%" HeaderStyle-HorizontalAlign="Center">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="BaseApuracao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="BaseAno" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="TipoApropriacao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="TipoLimitePL" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ValorLimite" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ContagemDias" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ImpactaPL" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroMesesRenovacao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroDiasPagamento" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ValorMinimo" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ValorMaximo" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="DataFim" Visible="false" />                    

                </Columns>
                
                <Templates>            
                <EditForm> 
                    
                    <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">                                               
                                                      
                    <div class="editForm">
                    
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1" ClientInstanceName="btnEditCodigoCarteira"
                                                                            Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                    
                            <td colspan="2">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                            </td>
                        </tr>
                                                             
                        <tr>       
                            <td  class="td_Label">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Referência:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>
                            </td>  
                            
                            <td class="td_Label">
                                <asp:Label ID="labelCadastro" runat="server" CssClass="labelRequired" Text="Cadastro:"></asp:Label>                    
                            </td>
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropCadastro" runat="server" DataSourceID="EsDSCadastroTaxaAdministracao" 
                                                    ShowShadow="false" CssClass="dropDownList"                                                 
                                                    TextField="Descricao" ValueField="IdCadastro"
                                                    Text='<%#Eval("IdCadastro")%>'>
                                </dxe:ASPxComboBox>
                            </td>    
                        </tr>                                
                    
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoCalculo" runat="server" CssClass="labelRequired" Text="Tipo Cálculo:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoCalculo" runat="server" ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("TipoCalculo")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Percentual PL" />
                                <dxe:ListEditItem Value="2" Text="Valor Fixo" />
                                <dxe:ListEditItem Value="3" Text="%PL ou Valor Fixo" />
                                </Items>    
                                <ClientSideEvents
                                     SelectedIndexChanged="function(s, e) {TrataCampos(s.GetSelectedIndex())}"      
                                     
                                     
                                     
                                    />                                                      
                                </dxe:ASPxComboBox>                                     
                                
                            </td>
                        
                            <td  class="td_Label">
                                <asp:Label ID="labelValorFixoTotal" runat="server" CssClass="labelNormal" Text="Valor Fixo:"></asp:Label>
                            </td>
                            <td>                                
                                <dxe:ASPxSpinEdit ID="textValorFixoTotal" runat="server" CssClass="textValor_5" ClientInstanceName="textValorFixoTotal"
                                                                            Text='<%# Eval("ValorFixoTotal") %>' MaxLength="10" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>                                  
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelContagemDias" runat="server" CssClass="labelRequired" Text="Contagem Dias:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropContagemDias" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("ContagemDias")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Dias Úteis" />
                                <dxe:ListEditItem Value="2" Text="Dias Corridos" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelImpactaPL" runat="server" CssClass="labelNormal" Text="Impacta PL:"></asp:Label>                    
                            </td>   
                            <td>      
                                <dxe:ASPxComboBox ID="dropImpactaPL" runat="server" ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("ImpactaPL")%>'>
                                <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelBaseApuracao" runat="server" CssClass="labelNormal" Text="Base Apuração:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropBaseApuracao" runat="server" 
                                                    ShowShadow="false" ClientInstanceName="dropBaseApuracao"
                                                    CssClass="dropDownListCurto_1" Text='<%#Eval("BaseApuracao")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="PL D-1" />
                                <dxe:ListEditItem Value="2" Text="Pat. Bruto D0" />
                                <dxe:ListEditItem Value="3" Text="Pat. Bruto D-1" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td  class="td_Label">
                                <asp:Label ID="labelTaxa" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                            </td>
                            <td>                                
                                <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxa"
                                                                    Text='<%# Eval("Taxa") %>' MaxLength="16" NumberType="Float">
                                                                                
                                </dxe:ASPxSpinEdit>
                            </td>                                  
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelBaseAno" runat="server" CssClass="labelNormal" Text="Base Ano:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropBaseAno" runat="server" 
                                                    ShowShadow="false" ClientInstanceName="dropBaseAno"
                                                    CssClass="dropDownListCurto_1" Text='<%#Eval("BaseAno")%>'>
                                <Items>
                                <dxe:ListEditItem Value="252" Text="252" />
                                <dxe:ListEditItem Value="360" Text="360" />
                                <dxe:ListEditItem Value="365" Text="365" />
                                <dxe:ListEditItem Value="1" Text="Úteis Exatos" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelTipoApropriacao" runat="server" CssClass="labelNormal" Text="Apropriação:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropTipoApropriacao" runat="server" 
                                                    ShowShadow="false" ClientInstanceName="dropTipoApropriacao"
                                                    CssClass="dropDownListCurto_4" Text='<%#Eval("TipoApropriacao")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Exponencial" />
                                <dxe:ListEditItem Value="2" Text="Linear" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Segmento:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropSegmento" runat="server" 
                                                    ShowShadow="false" ClientInstanceName="dropSegmento"
                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoGrupo")%>'>
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="Ações" />
                                    <dxe:ListEditItem Value="2" Text="Ações Ativo" />
                                    <dxe:ListEditItem Value="10" Text="Opções" />
                                    <dxe:ListEditItem Value="11" Text="Opções Ativo" />
                                    <dxe:ListEditItem Value="50" Text="Renda Fixa" />
                                    <dxe:ListEditItem Value="51" Text="Renda Fixa Papel" />
                                    <dxe:ListEditItem Value="52" Text="Renda Fixa Titulo" />
                                    <dxe:ListEditItem Value="80" Text="Fundos" />
                                    <dxe:ListEditItem Value="81" Text="Fundos Ativo" />                                    
                                </Items>
                                
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Código Associado:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxTextBox ID="textCodigoAtivo" runat="server" CssClass="textNormal_5" MaxLength="8"
                                ClientInstanceName="textCodigoAtivo" Text='<%#Eval("CodigoAtivo")%>' />
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoLimitePL" runat="server" CssClass="labelNormal" Text="Tipo Limite:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoLimitePL" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("TipoLimitePL")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Total PL" />
                                <dxe:ListEditItem Value="2" Text="Limite Superior" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td  class="td_Label">
                                <asp:Label ID="labelValorLimite" runat="server" CssClass="labelNormal" Text="Valor Limite:"></asp:Label>
                            </td>
                            <td>                                
                                <dxe:ASPxSpinEdit ID="textValorLimite" runat="server" CssClass="textValor_5" ClientInstanceName="textValorLimite"
                                                            Text='<%# Eval("ValorLimite") %>' MaxLength="10" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>       
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelValorMinimo" runat="server" CssClass="labelNormal" Text="Vl Mínimo:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textValorMinimo" runat="server" CssClass="textCurto" ClientInstanceName="textValorMinimo"
		                                  MaxLength="10" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('ValorMinimo')%>" AllowNull="false" MinValue="0.00">		                              
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelValorMaximo" runat="server" CssClass="labelNormal" Text="Vl Máximo:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textValorMaximo" runat="server" CssClass="textCurto" ClientInstanceName="textValorMaximo"
		                                  MaxLength="10" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('ValorMaximo')%>" AllowNull="false" MinValue="0.00">            
                                </dxe:ASPxSpinEdit>                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelNumeroMesesRenovacao" runat="server" CssClass="labelRequired" Text="Meses Renov.:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroMesesRenovacao" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroMesesRenovacao"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroMesesRenovacao')%>">            
                                </dxe:ASPxSpinEdit>                                                        
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelNumeroDiasPagamento" runat="server" CssClass="labelRequired" Text="Dias Pagto:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroDiasPagamento" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroDiasPagamento"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroDiasPagamento')%>">            
                                </dxe:ASPxSpinEdit>                                                        
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Data Fim:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Value='<%#Eval("DataFim")%>'/>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelEscalonaProporcional" runat="server" CssClass="labelNormal" Text="Escalona Proporcional:"></asp:Label>
                            </td>   
                            <td>      
                                <dxe:ASPxComboBox ID="dropEscalonaProporcional" runat="server" ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("EscalonaProporcional")%>'>
                                    <Items>
                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                        <dxe:ListEditItem Value="N" Text="Não" />
                                    </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Consolidadora:"></asp:Label>
                            </td>   
                            <td>      
                                <dxe:ASPxComboBox ID="dropConsolidadora" runat="server" ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("Consolidadora")%>'>
                                    <Items>
                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                        <dxe:ListEditItem Value="N" Text="Não" />
                                    </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>                            
                        </tr>
                        
                     <div id="divEventos" runat="server" visible="true">
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelEventoProvisao" runat="server" CssClass="labelNormal" Text="Evento Provisão:" />
                            </td>
                            <td colspan="3">
                            
                                 <dxe:ASPxButtonEdit ID="btnEditEventoProvisao" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditEventoProvisao" 
                                            ReadOnly="true" Width="420px"  Text='<%# Eval("DescricaoEventoProvisao") %>' >                                                                                                               
                                <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                                   <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoProvisao'; popupEvento.ShowAtElementByID(s.name); gridEvento.PerformCallback('btnRefresh');}" />                                                                   
                                </dxe:ASPxButtonEdit>
                            </td>                                                               
                        </tr>

                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelEventoPagamento" runat="server" CssClass="labelNormal" Text="Evento Pagamento:" />
                            </td>
                            <td colspan="3">
                            
                                 <dxe:ASPxButtonEdit ID="btnEditEventoPagamento" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditEventoPagamento" 
                                            ReadOnly="true" Width="420px" Text='<%# Eval("DescricaoEventoPagamento") %>' >                                                                                                               
                                <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                                   <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoPagamento'; popupEvento.ShowAtElementByID(s.name); gridEvento.PerformCallback('btnRefresh');}" />                                                                   
                                </dxe:ASPxButtonEdit>
                            </td>                                                               
                        </tr>                        
                     </div>
                     
                    </table>
                    
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                    
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                    
                    </div>                        

                    </asp:Panel>
                    
                </EditForm>                    
                </Templates>                
                
                <SettingsPopup EditForm-Width="500" />                        
                
                <ClientSideEvents

                    BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }"

                    EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }"
                />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>
            
        </div>                                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSTabelaTaxaAdministracao" runat="server" OnesSelect="EsDSTabelaTaxaAdministracao_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCadastroTaxaAdministracao" runat="server" OnesSelect="EsDSCadastroTaxaAdministracao_esSelect" />
    <cc1:esDataSource ID="EsDSEvento" runat="server" OnesSelect="EsDSEvento_esSelect" LowLevelBind="true" />
    </form>
</body>
</html>