﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ParametroAdmFundoInvestimento.aspx.cs"
    Inherits="CadastrosBasicos_ParametroAdmFundoInvestimento" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Parametrização Administrador Fundo de Investimento"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnRowUpdating="gridCadastro_RowUpdating"
                                    DataSourceID="EsDSParametro" KeyFieldName="IdParametro" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomCallback="gridCadastro_CustomCallback" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                    OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Administrador" FieldName="Administrador">
                                            <PropertiesComboBox DataSourceID="EsDSAdministrador" TextField="Nome" ValueField="IdAgente" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Execução de Recolhimento" FieldName="ExecucaoRecolhimento">
                                            <PropertiesComboBox>
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                    <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                    <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Alíquota IR" FieldName="AliquotaIR">
                                            <PropertiesComboBox>
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                    <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <table border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="labelAdministrador" runat="server" CssClass="labelRequired" Text="Administrador:"> </asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropAdministrador" runat="server" ClientInstanceName="dropAdministrador"
                                                                Value='<%#Eval("Administrador")%>' ShowShadow="false" DropDownStyle="DropDown"
                                                                CssClass="dropDownListLongo" DataSourceID="EsDSAdministrador" TextField="Nome"
                                                                ValueType="System.Int32" ValueField="IdAgente">
                                                                <ClientSideEvents Init="function(s,e)
                                                                {
                                                                    if(!gridCadastro.IsNewRowEditing())
                                                                        s.SetEnabled(false);
                                                                }
                                                                " />
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <hr class="linhaH" />
                                                <fieldset>
                                                    <legend>Padrão de Recolhimento de IR(Come-Cotas) para Eventos Corporativos, Transformações
                                                        e Desenquadramentos de Fundos</legend>
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <fieldset>
                                                                    <legend style="text-align: left">Execução do Recolhimento</legend>
                                                                    <dxe:ASPxRadioButtonList ID="rblExecucaoRecolhimento" runat="server" ClientInstanceName="rblExecucaoRecolhimento"
                                                                        Value='<%#Eval("ExecucaoRecolhimento")%>' ValueType="System.Int32">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                                            <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                                            <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                                        </Items>
                                                                         <ClientSideEvents Init="function(s,e)
                                                                         {
                                                                            if(s.GetValue() == 1)
                                                                            {
                                                                                rblAliquotaIR.SetEnabled(false);
                                                                            }
                                                                         }
                                                                         " />
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e)
                                                                        {
                                                                            if(s.GetValue() == 1)
                                                                            {
                                                                                rblAliquotaIR.SetValue(null);
                                                                                rblAliquotaIR.SetEnabled(false);
                                                                            }
                                                                            else
                                                                            {
                                                                                rblAliquotaIR.SetEnabled(true);
                                                                            }
                                                                        }
                                                                        " />
                                                                    </dxe:ASPxRadioButtonList>
                                                                </fieldset>
                                                            </td>
                                                            <td class="td_Label">
                                                                <fieldset>
                                                                    <legend style="text-align: left">Alíquota IR</legend>
                                                                    <dxe:ASPxRadioButtonList ID="rblAliquotaIR" ClientInstanceName="rblAliquotaIR" runat="server"
                                                                        Value='<%#Eval("AliquotaIR")%>' ValueType="System.Int32">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                                            <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                                        </Items>
                                                                    </dxe:ASPxRadioButtonList>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                        OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal11" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                </div>
                                            </asp:Panel>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="40%" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSAdministrador" runat="server" OnesSelect="EsDSAdministrador_esSelect" />
        <cc1:esDataSource ID="EsDSParametro" runat="server" OnesSelect="EsDSParametro_esSelect" />
    </form>
</body>
</html>
