﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CadastroTaxaAdministracao : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSCadastroTaxaAdministracao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroTaxaAdministracaoCollection coll = new CadastroTaxaAdministracaoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        CadastroTaxaAdministracao cadastroTaxaAdministracao = new CadastroTaxaAdministracao();
        int idCadastro = (int)e.Keys[0];

        if (cadastroTaxaAdministracao.LoadByPrimaryKey(idCadastro))
        {
            cadastroTaxaAdministracao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            cadastroTaxaAdministracao.Tipo = Convert.ToByte(e.NewValues["Tipo"]);
            cadastroTaxaAdministracao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroTaxaAdministracao - Operacao: Update CadastroTaxaAdministracao: " + idCadastro + UtilitarioWeb.ToString(cadastroTaxaAdministracao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        CadastroTaxaAdministracao cadastroTaxaAdministracao = new CadastroTaxaAdministracao();

        cadastroTaxaAdministracao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        cadastroTaxaAdministracao.Tipo = Convert.ToByte(e.NewValues["Tipo"]);
        cadastroTaxaAdministracao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CadastroTaxaAdministracao - Operacao: Insert CadastroTaxaAdministracao: " + cadastroTaxaAdministracao.IdCadastro + UtilitarioWeb.ToString(cadastroTaxaAdministracao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();    
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCadastro");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCadastro = Convert.ToInt32(keyValuesId[i]);

                CadastroTaxaAdministracao cadastroTaxaAdministracao = new CadastroTaxaAdministracao();
                if (cadastroTaxaAdministracao.LoadByPrimaryKey(idCadastro))
                {
                    TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
                    tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCadastro.Equal(idCadastro));
                    if (tabelaTaxaAdministracaoCollection.Query.Load())
                    {
                        throw new Exception("Cadastro " + cadastroTaxaAdministracao.Descricao + " não pode ser excluído por ter tabelas relacionadas.");
                    }

                    CadastroTaxaAdministracao cadastroTaxaAdministracaoClone = (CadastroTaxaAdministracao)Utilitario.Clone(cadastroTaxaAdministracao);
                    //

                    cadastroTaxaAdministracao.MarkAsDeleted();
                    cadastroTaxaAdministracao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CadastroTaxaAdministracao - Operacao: Delete CadastroTaxaAdministracao: " + idCadastro + UtilitarioWeb.ToString(cadastroTaxaAdministracaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

}

