﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using DevExpress.Web.Data;
using Financial.Web.Common;
using Financial.Contabil;
using Financial.BMF;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.WebConfigConfiguration;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_TabelaProvisao : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        this.FiltroGridEvento();
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { TabelaProvisaoMetadata.ColumnNames.TipoCalculo }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTabelaProvisao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        tabelaProvisaoQuery.Select(tabelaProvisaoQuery, carteiraQuery.Apelido.As("Apelido"),
                                   "<'' as DescricaoEventoPagamento >", "<'' as DescricaoEventoProvisao >");
        tabelaProvisaoQuery.InnerJoin(carteiraQuery).On(tabelaProvisaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaProvisaoQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        tabelaProvisaoQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                  permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaProvisaoQuery.OrderBy(tabelaProvisaoQuery.DataReferencia.Descending);

        TabelaProvisaoCollection coll = new TabelaProvisaoCollection();
        coll.Load(tabelaProvisaoQuery);

        for (int i = 0; i < coll.Count; i++)
        {
            if (coll[i].IdEventoPagamento.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoPagamento", coll[i].IdEventoPagamento + " - " + coll[i].UpToContabRoteiroByIdEventoPagamento.Descricao);
            }

            if (coll[i].IdEventoProvisao.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoProvisao", coll[i].IdEventoProvisao + " - " + coll[i].UpToContabRoteiroByIdEventoProvisao.Descricao);
            }
        }

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCadastroProvisao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroProvisaoCollection coll = new CadastroProvisaoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region PopUp ContabRoteiro
    /// <summary>
    /// PopUp ContabRoteiro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSEvento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        e.Collection = new TabelaTaxaAdministracaoCollection();

        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        Cliente cliente = new Cliente();

        int? idPlano = 0;
        int idCarteiraAtual = 0;
        if (btnEditCodigoCarteira != null)
        {
            idCarteiraAtual = Convert.ToInt32(btnEditCodigoCarteira.Text);
        }

        if (cliente.LoadByPrimaryKey(idCarteiraAtual))
        {
            idPlano = (int?)cliente.IdPlano;
        }

        if (!idPlano.HasValue)
        {
            idPlano = 0;
        }

        coll.Query.Select(coll.Query.IdEvento, coll.Query.Descricao, coll.Query.ContaCredito, coll.Query.ContaDebito);
        coll.Query.Where(coll.Query.IdPlano == idPlano);
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);

        if (coll.Query.Load())
            e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idEvento = (int)gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdEvento);
        string descricao = Convert.ToString(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Descricao));

        e.Result = idEvento.ToString() + "|" + " - " + descricao;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridEvento.DataBind();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropProvisao = gridCadastro.FindEditFormTemplateControl("dropProvisao") as ASPxComboBox;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxSpinEdit textValorTotal = gridCadastro.FindEditFormTemplateControl("textValorTotal") as ASPxSpinEdit;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(dropProvisao);
        controles.Add(dropTipoCalculo);
        controles.Add(textValorTotal);
        controles.Add(dropContagemDias);
        controles.Add(textDiaRenovacao);
        controles.Add(textNumeroMesesRenovacao);
        controles.Add(textNumeroDiasPagamento);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        if (Convert.ToInt32(textNumeroMesesRenovacao.Value) == 0)
        {
            if (string.IsNullOrEmpty(textDataFim.Text))
            {
                e.Result = "Preencher a Data Fim";
                return;
            }
        }

        if (!string.IsNullOrEmpty(textDataFim.Text))
        {
            DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            if(dataFim.Date <= dataReferencia)
            {
                e.Result = "Data fim deve ser maior que a data de referência";
                return;
            }
        }

        if (!gridCadastro.IsNewRowEditing)
        {
            int idTabela = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "IdTabela" }));
            int idCarteira = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "IdCarteira" }));

            TabelaProvisao tabelaProvisao = new TabelaProvisao();
            tabelaProvisao.LoadByPrimaryKey(idTabela);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            if (cliente.DataDia.Value > tabelaProvisao.DataReferencia.Value)
            {
                e.Result = "O cliente - " + cliente.Apelido.Trim() + " deve ter a data de processamento menor ou igual a data de referência da provisão (" + tabelaProvisao.DataReferencia.Value.ToString("dd/MM/yyyy") + ")";
                return;
            }

        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido
                           : "no_access";
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idTabela = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxSpinEdit textValorTotal = gridCadastro.FindEditFormTemplateControl("textValorTotal") as ASPxSpinEdit;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxComboBox dropProvisao = gridCadastro.FindEditFormTemplateControl("dropProvisao") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        TabelaProvisao tabelaProvisao = new TabelaProvisao();
        if (tabelaProvisao.LoadByPrimaryKey(idTabela))
        {
            if (textDataFim.Text != "")
            {
                tabelaProvisao.DataFim = Convert.ToDateTime(textDataFim.Text);
            }
            else
            {
                tabelaProvisao.DataFim = null;
            }

            if (ParametrosConfiguracaoSistema.Outras.CalculaContatil)
            {
                //
                ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
                ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;

                if (textIdProvisaoEvento.Text != "")
                {
                    string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                    tabelaProvisao.IdEventoProvisao = Convert.ToInt32(dados[0].Trim());
                }

                if (textIdProvisaoPagamento.Text != "")
                {
                    string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                    tabelaProvisao.IdEventoPagamento = Convert.ToInt32(dados[0].Trim());
                }
            }

            tabelaProvisao.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            tabelaProvisao.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            tabelaProvisao.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);
            tabelaProvisao.ValorTotal = Convert.ToDecimal(textValorTotal.Text);
            tabelaProvisao.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
            tabelaProvisao.DiaRenovacao = Convert.ToByte(textDiaRenovacao.Text);
            tabelaProvisao.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
            tabelaProvisao.NumeroDiasPagamento = Convert.ToInt16(textNumeroDiasPagamento.Text);
            tabelaProvisao.IdCadastro = Convert.ToInt32(dropProvisao.SelectedItem.Value);

            tabelaProvisao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaProvisao - Operacao: Update TabelaProvisao: " + idTabela + UtilitarioWeb.ToString(tabelaProvisao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxSpinEdit textValorTotal = gridCadastro.FindEditFormTemplateControl("textValorTotal") as ASPxSpinEdit;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxComboBox dropProvisao = gridCadastro.FindEditFormTemplateControl("dropProvisao") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        TabelaProvisao tabelaProvisao = new TabelaProvisao();
        tabelaProvisao.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaProvisao.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaProvisao.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);
        tabelaProvisao.ValorTotal = Convert.ToDecimal(textValorTotal.Text);
        tabelaProvisao.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
        tabelaProvisao.DiaRenovacao = Convert.ToByte(textDiaRenovacao.Text);
        tabelaProvisao.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
        tabelaProvisao.NumeroDiasPagamento = Convert.ToInt16(textNumeroDiasPagamento.Text);
        tabelaProvisao.IdCadastro = Convert.ToInt32(dropProvisao.SelectedItem.Value);

        if (textDataFim.Text != "")
        {
            tabelaProvisao.DataFim = Convert.ToDateTime(textDataFim.Text);
        }

        //if (WebConfig.AppSettings.CalculaContatil) {
        if (ParametrosConfiguracaoSistema.Outras.CalculaContatil)
        {
            //
            ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
            ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;
            //
            if (textIdProvisaoEvento.Text != "")
            {
                string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                int idEvento = Convert.ToInt32(dados[0].Trim());
                tabelaProvisao.IdEventoProvisao = idEvento;
            }

            if (textIdProvisaoPagamento.Text != "")
            {
                string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                int idEvento = Convert.ToInt32(dados[0].Trim());
                tabelaProvisao.IdEventoPagamento = idEvento;
            }
        }

        tabelaProvisao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaProvisao - Operacao: Insert TabelaProvisao: " + tabelaProvisao.IdTabela + UtilitarioWeb.ToString(tabelaProvisao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TabelaProvisaoMetadata.ColumnNames.IdTabela);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTabela = Convert.ToInt32(keyValuesId[i]);

                TabelaProvisao tabelaProvisao = new TabelaProvisao();
                if (tabelaProvisao.LoadByPrimaryKey(idTabela))
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(tabelaProvisao.IdCarteira.Value);

                    if (cliente.DataDia.Value > tabelaProvisao.DataReferencia.Value)
                    {
                        throw new Exception("O cliente - " + cliente.Apelido.Trim() + " deve ter a data de processamento menor ou igual a data de referência da provisão (" + tabelaProvisao.DataReferencia.Value.ToString("dd/MM/yyyy") + ")");
                    }

                    using (esTransactionScope scope = new esTransactionScope())
                    {
                        CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
                        calculoProvisaoCollection.Query.Where(calculoProvisaoCollection.Query.IdTabela.Equal(idTabela));
                        if (calculoProvisaoCollection.Query.Load())
                        {
                            calculoProvisaoCollection.MarkAllAsDeleted();
                            calculoProvisaoCollection.Save();
                        }

                        TabelaProvisao tabelaProvisaoClone = (TabelaProvisao)Utilitario.Clone(tabelaProvisao);
                        //
                        tabelaProvisao.MarkAsDeleted();
                        tabelaProvisao.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de TabelaProvisao - Operacao: Delete TabelaProvisao: " + idTabela + UtilitarioWeb.ToString(tabelaProvisaoClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        scope.Complete();
                    }
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            if (btnEditCodigoCarteira != null)
                e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        //if (!WebConfig.AppSettings.CalculaContatil) {
        if (!ParametrosConfiguracaoSistema.Outras.CalculaContatil)
        {
            //Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
            //ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
            //labelConta.Visible = false;
            //dropConta.Visible = false;

            Control c1 = this.gridCadastro.FindEditFormTemplateControl("divEventos");
            if (c1 != null)
            {
                c1.Visible = false;
            }
        }

        base.panelEdicao_Load(sender, e);

        if (!this.gridCadastro.IsNewRowEditing)
        {
            int idTabela = Convert.ToInt32(this.gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdTabela"));
            int idCarteira = Convert.ToInt32(this.gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCarteira"));

            TabelaProvisao tabelaProvisao = new TabelaProvisao();
            if (tabelaProvisao.LoadByPrimaryKey(idTabela))
            {
                ASPxLabel labelPeriodoApropriacao = gridCadastro.FindEditFormTemplateControl("labelPeriodoApropriacao") as ASPxLabel;

                if (tabelaProvisao.DataFim.HasValue)
                {
                    labelPeriodoApropriacao.Text = this.GetLabelPeriodoApropriacao(tabelaProvisao.DataReferencia.Value, tabelaProvisao.DataFim.Value);
                }

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                if (cliente.DataDia.Value > tabelaProvisao.DataReferencia.Value)
                {
                    ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
                    ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
                    ASPxComboBox dropProvisao = gridCadastro.FindEditFormTemplateControl("dropProvisao") as ASPxComboBox;
                    ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
                    ASPxSpinEdit textValorTotal = gridCadastro.FindEditFormTemplateControl("textValorTotal") as ASPxSpinEdit;
                    ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
                    ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
                    ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
                    ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
                    ASPxButtonEdit btnEditEventoProvisao = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
                    ASPxButtonEdit btnEditEventoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;

                    btnEditCodigoCarteira.ClientEnabled = false;
                    textDataReferencia.ClientEnabled = false;
                    dropProvisao.ClientEnabled = false;
                    dropTipoCalculo.ClientEnabled = false;
                    textValorTotal.ClientEnabled = false;
                    dropContagemDias.ClientEnabled = false;
                    textDiaRenovacao.ClientEnabled = false;
                    textNumeroMesesRenovacao.ClientEnabled = false;
                    textNumeroDiasPagamento.ClientEnabled = false;
                    btnEditEventoProvisao.ClientEnabled = false;
                    btnEditEventoPagamento.ClientEnabled = false;
                }

            }
        }
    }

    private string GetLabelPeriodoApropriacao(DateTime dataReferencia, DateTime dataFimRenovacao)
    {
        DateTime dataFimApropriacao = Calendario.SubtraiDiaUtil(dataFimRenovacao, 1);
        return "Período Apropriação: " + dataReferencia.ToShortDateString() + " a " + dataFimApropriacao.ToShortDateString();
    }

    protected void callbackUpdatePeriodoApropriacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        if (textDataReferencia.Text != "" && textDataFim.Text != "")
        {
            e.Result = GetLabelPeriodoApropriacao(Convert.ToDateTime(textDataReferencia.Text), Convert.ToDateTime(textDataFim.Text));
        }
        else
        {
            e.Result = "";
        }
    }

    /// <summary>
    /// Troca para Like no filtro campo descricao.
    /// </summary>
    public void FiltroGridEvento()
    {
        GridViewDataColumn colDescricao = gridEvento.Columns["Descricao"] as GridViewDataColumn;
        if (colDescricao != null)
        {
            colDescricao.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}