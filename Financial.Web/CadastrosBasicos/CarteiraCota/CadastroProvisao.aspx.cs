﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CadastroProvisao : CadastroBasePage {
    protected void EsDSCadastroProvisao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CadastroProvisaoCollection coll = new CadastroProvisaoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CadastroProvisao cadastroProvisao = new CadastroProvisao();
        int idCadastro = (int)e.Keys[0];

        if (cadastroProvisao.LoadByPrimaryKey(idCadastro)) {
            cadastroProvisao.Descricao = Convert.ToString(e.NewValues[CadastroProvisaoMetadata.ColumnNames.Descricao]);
            cadastroProvisao.Tipo = Convert.ToByte(e.NewValues[CadastroProvisaoMetadata.ColumnNames.Tipo]);
            cadastroProvisao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroProvisao - Operacao: Update CadastroProvisao: " + idCadastro + UtilitarioWeb.ToString(cadastroProvisao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        CadastroProvisao cadastroProvisao = new CadastroProvisao();

        cadastroProvisao.Descricao = Convert.ToString(e.NewValues[CadastroProvisaoMetadata.ColumnNames.Descricao]);
        cadastroProvisao.Tipo = Convert.ToByte(e.NewValues[CadastroProvisaoMetadata.ColumnNames.Tipo]);
        cadastroProvisao.Save();
        
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CadastroProvisao - Operacao: Insert CadastroProvisao: " + cadastroProvisao.IdCadastro + UtilitarioWeb.ToString(cadastroProvisao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCadastro");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCadastro = Convert.ToInt32(keyValuesId[i]);

                CadastroProvisao cadastroProvisao = new CadastroProvisao();
                if (cadastroProvisao.LoadByPrimaryKey(idCadastro)) {
                    TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
                    tabelaProvisaoCollection.Query.Where(tabelaProvisaoCollection.Query.IdCadastro.Equal(idCadastro));
                    if (tabelaProvisaoCollection.Query.Load()) {
                        throw new Exception("Cadastro " + cadastroProvisao.Descricao + " não pode ser excluído por ter tabelas relacionadas.");
                    }

                    //
                    CadastroProvisao cadastroProvisaoClone = (CadastroProvisao)Utilitario.Clone(cadastroProvisao);
                    //

                    cadastroProvisao.MarkAsDeleted();
                    cadastroProvisao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CadastroProvisao - Operacao: Delete CadastroProvisao: " + idCadastro + UtilitarioWeb.ToString(cadastroProvisaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}