﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using Financial.Fundo;
using Financial.Fundo.Enums;
using DevExpress.Web.Data;
using Financial.Common.Enums;

public partial class CadastrosBasicos_ParametroLiqAvancado : CadastroBasePage
{
    #region Campos do Grid
    ASPxDateEdit textDtVigencia;
    ASPxSpinEdit btnEditCodigoCliente;
    ASPxTextBox hiddenTipoData;
    ASPxTextBox hiddenIsNewRowEditing;
    ASPxTextBox hiddenCarregaGridParametro;
    ASPxComboBox dropTipoOperacao;
    ASPxButtonEdit btnEditParametroConversao;
    ASPxButtonEdit btnEditParametroLiquidacao;
    #endregion

    #region Grid
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void gridParametroAnalitico_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        string parametro = e.Keys[gridParametroAnalitico.KeyFieldName].ToString();
        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        hiddenTipoData = pageControl.FindControl("hiddenTipoData") as ASPxTextBox;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
        hiddenIsNewRowEditing = pageControl.FindControl("hiddenIsNewRowEditing") as ASPxTextBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoData = Convert.ToInt32(hiddenTipoData.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

        #region Analitico
        ParametroLiqAvancadoAnaliticoCollection parametroAnaliticoColl = new ParametroLiqAvancadoAnaliticoCollection();
        parametroAnaliticoColl.Query.Where(parametroAnaliticoColl.Query.IdCarteira.Equal(idCarteira)
                                  & parametroAnaliticoColl.Query.DataVigencia.Equal(dataVigencia)
                                  & parametroAnaliticoColl.Query.TipoOperacao.Equal(tipoOperacao)
                                  & parametroAnaliticoColl.Query.IdParametroLiqAvancado.Equal(parametro)
                                  & parametroAnaliticoColl.Query.TipoData.Equal(tipoData));

        if (parametroAnaliticoColl.Query.Load())
        {
            parametroAnaliticoColl.MarkAllAsDeleted();
            parametroAnaliticoColl.Save();
        }
        #endregion

        #region Composto
        ParametroLiqAvancadoCollection parametroColl = new ParametroLiqAvancadoCollection();
        parametroColl.Query.Where(parametroColl.Query.IdCarteira.Equal(idCarteira)
                                  & parametroColl.Query.DataVigencia.Equal(dataVigencia)
                                  & parametroColl.Query.TipoOperacao.Equal(tipoOperacao)
                                  & parametroColl.Query.TipoData.Equal(tipoData));

        if (parametroColl.Query.Load())
        {
            parametroColl.MarkAllAsDeleted();
            parametroColl.Save();
        }
        #endregion

        this.ExecutaMontagemParametro(idCarteira, dataVigencia, tipoOperacao, tipoData, "N");

        e.Cancel = true;
        gridParametroAnalitico.CancelEdit();
        gridParametroAnalitico.DataBind();

    }

    protected void gridParametroAnalitico_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string parametroUm = string.Empty;
        string parametroDois = string.Empty;
        string parametro = string.Empty;

        parametro = Convert.ToString(e.Keys[0]);

        if (e.NewValues["ParametroUm"] != null && !string.IsNullOrEmpty(e.NewValues["ParametroUm"].ToString()))
            parametroUm = e.NewValues["ParametroUm"].ToString();

        if (e.NewValues["ParametroDois"] != null && !string.IsNullOrEmpty(e.NewValues["ParametroDois"].ToString()))
            parametroDois = e.NewValues["ParametroDois"].ToString();

        foreach (GridViewColumn column in gridParametroAnalitico.Columns)
        {
            GridViewDataColumn dataColumn = column as GridViewDataColumn;

            if (dataColumn == null)
                continue;

            if (dataColumn.FieldName.Equals("ParametroUm"))
            {
                if (string.IsNullOrEmpty(parametroUm))
                    e.Errors[dataColumn] = "1° Parâmetro deve ser preenchido";
            }

            if (dataColumn.FieldName.Equals("ParametroDois"))
            {
                if (!parametro.Equals("C") && !parametro.Equals("D"))
                {
                    if (string.IsNullOrEmpty(parametroDois))
                        e.Errors[dataColumn] = "2° Parâmetro deve ser preenchido";
                }
            }


            #region OrdemProcessamento
            if (dataColumn.FieldName.Equals("OrdemProcessamento"))
            {

                if (e.NewValues[dataColumn.FieldName] == null)
                {
                    e.Errors[dataColumn] = "Ordem de Execução devem ser preenchida";
                }
                else
                {
                    int orderm = Convert.ToInt32(e.NewValues[dataColumn.FieldName]);
                    if (orderm <= 0)
                    {
                        e.Errors[dataColumn] = "Ordem de Execução deve ser maior que zero";
                    }
                    else
                    {
                        #region Valida número do Ordem
                        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

                        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
                        hiddenTipoData = pageControl.FindControl("hiddenTipoData") as ASPxTextBox;
                        hiddenIsNewRowEditing = pageControl.FindControl("hiddenIsNewRowEditing") as ASPxTextBox;
                        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

                        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
                        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
                        int tipoData = Convert.ToInt32(hiddenTipoData.Text);
                        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

                        ParametroLiqAvancadoAnaliticoCollection parametroColl = new ParametroLiqAvancadoAnaliticoCollection();
                        parametroColl.Query.Where(parametroColl.Query.IdCarteira.Equal(idCarteira)
                                                  & parametroColl.Query.DataVigencia.Equal(dataVigencia)
                                                  & parametroColl.Query.TipoOperacao.Equal(tipoOperacao)
                                                  & parametroColl.Query.OrdemProcessamento.Equal(orderm)
                                                  & parametroColl.Query.IdParametroLiqAvancado.NotEqual(parametro)
                                                  & parametroColl.Query.TipoData.Equal(tipoData));

                        if (parametroColl.Query.Load())
                        {
                            e.Errors[dataColumn] = "Ordem de Execução já preenchida para outro Parâmetro";
                        }
                        #endregion
                    }

                }
            }
            #endregion
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(ParametroLiqAvancadoMetadata.ColumnNames.IdCarteira));
            string tipoData = Convert.ToString(e.GetListSourceFieldValue(ParametroLiqAvancadoMetadata.ColumnNames.TipoData));
            string dataVigencia = Convert.ToString(e.GetListSourceFieldValue(ParametroLiqAvancadoMetadata.ColumnNames.DataVigencia));
            string tipoOperacao = Convert.ToString(e.GetListSourceFieldValue(ParametroLiqAvancadoMetadata.ColumnNames.TipoOperacao));
            e.Value = idCarteira + "|" + dataVigencia + "|" + tipoOperacao;
        }
    }

    protected void gridParametroAnalitico_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        hiddenTipoData = pageControl.FindControl("hiddenTipoData") as ASPxTextBox;
        hiddenIsNewRowEditing = pageControl.FindControl("hiddenIsNewRowEditing") as ASPxTextBox;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

        string idParametroLiqAvancado = Convert.ToString(e.Keys[0]);
        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoData = Convert.ToInt32(hiddenTipoData.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

        string parametroUm = (e.NewValues[0] != null ? e.NewValues[0].ToString() : null);
        string parametroDois = (e.NewValues[1] != null ? e.NewValues[1].ToString() : null);
        string ordem = (e.NewValues[2] != null ? e.NewValues[2].ToString() : null);

        ParametroLiqAvancadoAnalitico parametroLiqAvancadoAnalitico = new ParametroLiqAvancadoAnalitico();
        if (!parametroLiqAvancadoAnalitico.LoadByPrimaryKey(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao))
            parametroLiqAvancadoAnalitico = new ParametroLiqAvancadoAnalitico();

        parametroLiqAvancadoAnalitico.IdCarteira = idCarteira;
        parametroLiqAvancadoAnalitico.IdParametroLiqAvancado = idParametroLiqAvancado;
        parametroLiqAvancadoAnalitico.DataVigencia = dataVigencia;
        parametroLiqAvancadoAnalitico.TipoData = tipoData;
        parametroLiqAvancadoAnalitico.TipoOperacao = tipoOperacao;
        parametroLiqAvancadoAnalitico.ParametroUm = parametroUm;
        parametroLiqAvancadoAnalitico.OrdemProcessamento = Convert.ToInt32(ordem);

        if (!string.IsNullOrEmpty(parametroDois))
        {
            parametroLiqAvancadoAnalitico.ParametroDois = parametroDois;
            parametroLiqAvancadoAnalitico.ParametroComposto = idParametroLiqAvancado + parametroLiqAvancadoAnalitico.ParametroUm + "/" + parametroLiqAvancadoAnalitico.ParametroDois;
        }
        else
        {
            int parametro = Convert.ToInt32(parametroLiqAvancadoAnalitico.ParametroUm);
            char sinal = (parametro < 0 ? '-' : '+');

            parametroLiqAvancadoAnalitico.ParametroDois = string.Empty;
            parametroLiqAvancadoAnalitico.ParametroComposto = idParametroLiqAvancado + sinal + Math.Abs(parametro);
        }

        parametroLiqAvancadoAnalitico.Save();

        this.ExecutaMontagemParametro(idCarteira, dataVigencia, tipoOperacao, tipoData, "N");

        e.Cancel = true;
        gridParametroAnalitico.CancelEdit();
        gridParametroAnalitico.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

        this.ExecutaMontagemParametro(idCarteira, dataVigencia, tipoOperacao, null, "S");

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

        this.ExecutaMontagemParametro(idCarteira, dataVigencia, tipoOperacao, null, "S");

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
    #endregion

    #region _esSelect
    protected void EsDSParametroAnalitico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        if (pageControl == null)
        {
            e.Collection = new TipoParametroLiqAvancadoCollection();
            return;
        }

        TipoParametroLiqAvancadoCollection coll = new TipoParametroLiqAvancadoCollection();
        ParametroLiqAvancadoAnaliticoQuery parametroLiqAnalitico = new ParametroLiqAvancadoAnaliticoQuery("ParametrosLiq");
        TipoParametroLiqAvancadoQuery tipoParametroLiqAnalitico = new TipoParametroLiqAvancadoQuery("TipoParametroLiq");
        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        hiddenTipoData = pageControl.FindControl("hiddenTipoData") as ASPxTextBox;
        hiddenIsNewRowEditing = pageControl.FindControl("hiddenIsNewRowEditing") as ASPxTextBox;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

        bool clienteVazio = (btnEditCodigoCliente == null || string.IsNullOrEmpty(btnEditCodigoCliente.Text));
        bool dataVazia = (textDtVigencia == null || string.IsNullOrEmpty(textDtVigencia.Text));
        bool tipoDataVazio = (hiddenTipoData == null || string.IsNullOrEmpty(hiddenTipoData.Text));
        bool tipoOperacaoVazio = (dropTipoOperacao == null || dropTipoOperacao.Value == null || string.IsNullOrEmpty(dropTipoOperacao.Value.ToString()));

        if (clienteVazio || dataVazia || tipoDataVazio || tipoOperacaoVazio)
        {
            e.Collection = new TipoParametroLiqAvancadoCollection();
            return;
        }

        hiddenCarregaGridParametro = pageControl.FindControl("hiddenCarregaGridParametro") as ASPxTextBox;
        bool carregaGridParametro = true;
        if (!string.IsNullOrEmpty(hiddenCarregaGridParametro.Text))
            carregaGridParametro = !hiddenCarregaGridParametro.Text.Equals("N");

        if (!carregaGridParametro)
        {
            e.Collection = new TipoParametroLiqAvancadoCollection();
            return;
        }

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoData = Convert.ToInt32(hiddenTipoData.Text.ToString());
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value.ToString());

        tipoParametroLiqAnalitico.Select(tipoParametroLiqAnalitico,
                                         parametroLiqAnalitico.ParametroUm,
                                         parametroLiqAnalitico.ParametroDois,
                                         parametroLiqAnalitico.OrdemProcessamento);
        tipoParametroLiqAnalitico.LeftJoin(parametroLiqAnalitico).On(tipoParametroLiqAnalitico.IdParametroLiqAvancado.Equal(parametroLiqAnalitico.IdParametroLiqAvancado)
                                                                      & parametroLiqAnalitico.IdCarteira.Equal(idCarteira.ToString())
                                                                      & parametroLiqAnalitico.DataVigencia.Equal("'" + dataVigencia.ToString("yyyyMMdd") + "'")
                                                                      & parametroLiqAnalitico.TipoOperacao.Equal(tipoOperacao.ToString())
                                                                      & parametroLiqAnalitico.TipoData.Equal(tipoData.ToString()));
        tipoParametroLiqAnalitico.OrderBy(tipoParametroLiqAnalitico.IdParametroLiqAvancado.Ascending);

        coll.Load(tipoParametroLiqAnalitico);

        e.Collection = coll;
    }

    protected void EsDSParametroLiqAvancado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ParametroLiqAvancadoCollection parametroLiqAvancadoColl = new ParametroLiqAvancadoCollection();
        ParametroLiqAvancadoQuery parametroDtOperacaoQuery = new ParametroLiqAvancadoQuery("ParametroLiqOperacao");
        ParametroLiqAvancadoQuery parametroDtConversaoQuery = new ParametroLiqAvancadoQuery("ParametroLiqConversaoMoeda");
        ParametroLiqAvancadoQuery parametroDtLiquidacaoQuery = new ParametroLiqAvancadoQuery("ParametroLiqLiquidacao");
        CarteiraQuery carteiraQuery = new CarteiraQuery("Carteira");

        parametroDtOperacaoQuery.Select(parametroDtOperacaoQuery.IdCarteira,
                                        parametroDtOperacaoQuery.DataVigencia,
                                        parametroDtOperacaoQuery.TipoOperacao,                                        
                                        parametroDtOperacaoQuery.Parametro.As("DataOperacao"),
                                        parametroDtOperacaoQuery.ParametroValido.As("StatusOperacao"),
                                        parametroDtConversaoQuery.Parametro.As("DataConversao"),
                                        parametroDtConversaoQuery.ParametroValido.Coalesce("'N'").As("StatusConversao"),
                                        parametroDtLiquidacaoQuery.Parametro.As("DataLiquidacao"),
                                        parametroDtLiquidacaoQuery.ParametroValido.Coalesce("'N'").As("StatusLiquidacao"),
                                        carteiraQuery.Nome.As("DescricaoCarteira"));
        parametroDtOperacaoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira.Equal(parametroDtOperacaoQuery.IdCarteira));
        parametroDtOperacaoQuery.LeftJoin(parametroDtConversaoQuery).On(parametroDtConversaoQuery.DataVigencia.Equal(parametroDtOperacaoQuery.DataVigencia)
                                                                            & parametroDtConversaoQuery.IdCarteira.Equal(parametroDtOperacaoQuery.IdCarteira)
                                                                            & parametroDtConversaoQuery.TipoOperacao.Equal(parametroDtOperacaoQuery.TipoOperacao)
                                                                            & parametroDtConversaoQuery.TipoData.Equal(((int)TipoParametroLiquidacao.DataConversao).ToString()));
        parametroDtOperacaoQuery.LeftJoin(parametroDtLiquidacaoQuery).On(parametroDtLiquidacaoQuery.DataVigencia.Equal(parametroDtOperacaoQuery.DataVigencia)
                                                                            & parametroDtLiquidacaoQuery.IdCarteira.Equal(parametroDtOperacaoQuery.IdCarteira)
                                                                            & parametroDtLiquidacaoQuery.TipoOperacao.Equal(parametroDtOperacaoQuery.TipoOperacao)
                                                                            & parametroDtLiquidacaoQuery.TipoData.Equal((int)TipoParametroLiquidacao.DataLiquidacao));
        parametroDtOperacaoQuery.Where(parametroDtOperacaoQuery.TipoData.Equal((int)TipoParametroLiquidacao.DataOperacao));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            parametroDtOperacaoQuery.Where(parametroDtOperacaoQuery.DataVigencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            parametroDtOperacaoQuery.Where(parametroDtOperacaoQuery.DataVigencia.LessThanOrEqual(textDataFim.Text));
        }

        parametroDtOperacaoQuery.OrderBy(parametroDtOperacaoQuery.IdCarteira.Ascending, parametroDtOperacaoQuery.DataVigencia.Descending);

        parametroLiqAvancadoColl.Load(parametroDtOperacaoQuery);

        e.Collection = parametroLiqAvancadoColl;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;

    }

    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region _CallBack
    protected void popupCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            resultado = cliente.str.Apelido;
        }

        e.Result = resultado;
    }

    protected void callBackBtnVoltar_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        hiddenIsNewRowEditing = pageControl.FindControl("hiddenIsNewRowEditing") as ASPxTextBox;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
        hiddenCarregaGridParametro = pageControl.FindControl("hiddenCarregaGridParametro") as ASPxTextBox;

        bool carregaGridParametro = true;
        if (!string.IsNullOrEmpty(hiddenCarregaGridParametro.Text))
            carregaGridParametro = !hiddenCarregaGridParametro.Text.Equals("N");

        if (!carregaGridParametro)
            return;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text.ToString());
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value.ToString());
        int tipoData = Convert.ToInt32(e.Parameter.ToString());
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);

        ParametroLiqAvancadoCollection coll = new ParametroLiqAvancadoCollection();
        e.Result = coll.MontaStringParametroLiqAvanc(idCarteira, dataVigencia, tipoData, tipoOperacao, string.Empty, false);
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues("CompositeKey");

            for (int i = 0; i < keyValues.Count; i++)
            {
                string[] keys = (keyValues[i].ToString()).Split('|');
                int idCarteira = Convert.ToInt32(keys[0]);
                DateTime dataVigencia = Convert.ToDateTime(keys[1]);
                int tipoOperacao = Convert.ToInt32(keys[2]);

                ParametroLiqAvancadoCollection parametroColl = new ParametroLiqAvancadoCollection();
                parametroColl.Query.Where(parametroColl.Query.IdCarteira.Equal(idCarteira)
                                          & parametroColl.Query.DataVigencia.Equal(dataVigencia)
                                          & parametroColl.Query.TipoOperacao.Equal(tipoOperacao));

                if (parametroColl.Query.Load())
                {
                    parametroColl.MarkAllAsDeleted();
                    parametroColl.Save();
                }

                ParametroLiqAvancadoAnaliticoCollection parametroAnaliticoColl = new ParametroLiqAvancadoAnaliticoCollection();
                parametroAnaliticoColl.Query.Where(parametroAnaliticoColl.Query.IdCarteira.Equal(idCarteira)
                                          & parametroAnaliticoColl.Query.DataVigencia.Equal(dataVigencia)
                                          & parametroAnaliticoColl.Query.TipoOperacao.Equal(tipoOperacao));

                if (parametroAnaliticoColl.Query.Load())
                {
                    parametroAnaliticoColl.MarkAllAsDeleted();
                    parametroAnaliticoColl.Save();
                }

            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        if (pageControl.ActiveTabIndex == 1) //Clonar
        {
            ASPxDateEdit textDtVigenciaClone = pageControl.FindControl("textDtVigenciaClone") as ASPxDateEdit;
            ASPxSpinEdit btnEditCodigoClienteClone = pageControl.FindControl("btnEditCodigoClienteClone") as ASPxSpinEdit;
            ASPxComboBox dropTipoOperacaoClone = pageControl.FindControl("dropTipoOperacaoClone") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(textDtVigenciaClone);
            controles.Add(btnEditCodigoClienteClone);
            controles.Add(dropTipoOperacaoClone);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos (Clone) com * são obrigatórios!";
                return;
            }
            #endregion

            int idCarteiraClone = Convert.ToInt32(btnEditCodigoClienteClone.Text);
            DateTime dataVigenciaClone = Convert.ToDateTime(textDtVigenciaClone.Text);
            int tipoOperacaoClone = Convert.ToInt32(dropTipoOperacaoClone.Value);

            ParametroLiqAvancadoCollection ParametroLiqAvancadoColl = new ParametroLiqAvancadoCollection();
            ParametroLiqAvancadoColl.Query.Where(ParametroLiqAvancadoColl.Query.IdCarteira.Equal(idCarteiraClone) &
                                                 ParametroLiqAvancadoColl.Query.DataVigencia.Equal(dataVigenciaClone) &
                                                 ParametroLiqAvancadoColl.Query.TipoOperacao.Equal(tipoOperacaoClone));

            if (ParametroLiqAvancadoColl.Query.Load())
            {
                e.Result = "Registro já existente.!";
            }
        }
        else if (pageControl.ActiveTabIndex == 2) //Simular
        {
            ASPxDateEdit DataReferenciaSimulacao = pageControl.FindControl("DataReferenciaSimulacao") as ASPxDateEdit;
            textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(DataReferenciaSimulacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos (Clone) com * são obrigatórios!";
                return;
            }

            DateTime dataReferencia = Convert.ToDateTime(DataReferenciaSimulacao.Text);
            #endregion
        }
        else if (pageControl.ActiveTabIndex == 0) //Cadastro
        {
            textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
            btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
            dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
            btnEditParametroConversao = pageControl.FindControl("btnEditParametroConversao") as ASPxButtonEdit;
            btnEditParametroLiquidacao = pageControl.FindControl("btnEditParametroLiquidacao") as ASPxButtonEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(textDtVigencia);
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropTipoOperacao);

            if (!e.Parameter.Equals("btnEditParametro"))
            {
                controles.Add(btnEditParametroConversao);
                controles.Add(btnEditParametroLiquidacao);
            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos (Cadastro) com * são obrigatórios!";
                return;
            }
            #endregion

            ParametroLiqAvancadoCollection ParametroLiqAvancadoColl = new ParametroLiqAvancadoCollection();
            // Somente se for Insert
            if (gridCadastro.IsNewRowEditing)
            {
                int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
                DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
                int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

                ParametroLiqAvancadoColl.Query.Where(ParametroLiqAvancadoColl.Query.IdCarteira.Equal(idCarteira) &
                                                     ParametroLiqAvancadoColl.Query.DataVigencia.Equal(dataVigencia) &
                                                     ParametroLiqAvancadoColl.Query.TipoOperacao.Equal(tipoOperacao));

                if (e.Parameter.Equals("btnEditParametro"))
                {
                    hiddenTipoData = pageControl.FindControl("hiddenTipoData") as ASPxTextBox;
                    int tipoData = Convert.ToInt32(hiddenTipoData.Text);
                    ParametroLiqAvancadoColl.Query.Where(ParametroLiqAvancadoColl.Query.TipoData.Equal(tipoData));
                }
                else
                {
                    ParametroLiqAvancadoColl.Query.Where(ParametroLiqAvancadoColl.Query.ParametroValido.Equal("'S'"));
                }

                if (ParametroLiqAvancadoColl.Query.Load())
                {
                    e.Result = "Registro já existente.!";
                    return;
                }
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);

        this.ExecutaMontagemParametro(idCarteira, dataVigencia, tipoOperacao, null, "S");

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    protected void callbackSimular_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        ASPxDateEdit aspxDataReferenciaSimulacao = pageControl.FindControl("DataReferenciaSimulacao") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropLocal = pageControl.FindControl("dropLocal") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);
        DateTime DataReferenciaSimulacao = Convert.ToDateTime(aspxDataReferenciaSimulacao.Text);
        int parametroDtOperacao = (int)TipoParametroLiquidacao.DataOperacao;
        int parametroDtConversao = (int)TipoParametroLiquidacao.DataConversao;
        int parametroDtLiquidacao = (int)TipoParametroLiquidacao.DataLiquidacao;

        DateTime dataOperacao = new DateTime();
        DateTime primeiraData = new DateTime();
        DateTime SegundaData = new DateTime();

        ParametroLiqAvancadoAnaliticoCollection coll = new ParametroLiqAvancadoAnaliticoCollection();
        ParametroLiqAvancadoAnaliticoCollection collOperacao = coll.RetornaParametrosNaDataVigente(dataVigencia, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataOperacao);
        ParametroLiqAvancadoAnaliticoCollection collLiquidacao = coll.RetornaParametrosNaDataVigente(dataVigencia, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataLiquidacao);
        ParametroLiqAvancadoAnaliticoCollection collConvesao = coll.RetornaParametrosNaDataVigente(dataVigencia, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataConversao);

        int? localFeriado = null;
        if (dropLocal.SelectedIndex != -1)
            localFeriado = Convert.ToInt32(dropLocal.SelectedItem.Value);


        if (tipoOperacao == (int)TipoParametroLiqOperacao.Aplicacao)
        {
            dataOperacao = coll.RetornaDataCalculada(DataReferenciaSimulacao, DataReferenciaSimulacao, collOperacao, parametroDtOperacao, 0, localFeriado);
            primeiraData = coll.RetornaDataCalculada(DataReferenciaSimulacao, dataOperacao, collLiquidacao, parametroDtLiquidacao, 0, localFeriado);
            SegundaData = coll.RetornaDataCalculada(DataReferenciaSimulacao, primeiraData, collConvesao, parametroDtConversao, 0, localFeriado);
        }
        else if (tipoOperacao == (int)TipoParametroLiqOperacao.Resgate)
        {
            dataOperacao = coll.RetornaDataCalculada(DataReferenciaSimulacao, DataReferenciaSimulacao, collOperacao, parametroDtOperacao, 0, localFeriado);
            primeiraData = coll.RetornaDataCalculada(DataReferenciaSimulacao, dataOperacao, collConvesao, parametroDtConversao, 0, localFeriado);
            SegundaData = coll.RetornaDataCalculada(DataReferenciaSimulacao, primeiraData, collLiquidacao, parametroDtLiquidacao, 0, localFeriado);
        }

        string Retorno = String.Format("{0:dd/MM/yyyy}", dataOperacao) + "|";
        Retorno = string.Concat(Retorno, String.Format("{0:dd/MM/yyyy}", primeiraData) + "|");
        Retorno = string.Concat(Retorno, String.Format("{0:dd/MM/yyyy}", SegundaData));

        e.Result = Retorno;
    }

    protected void callbackClonar_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        textDtVigencia = pageControl.FindControl("textDtVigencia") as ASPxDateEdit;
        btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
        ASPxDateEdit textDtVigenciaClone = pageControl.FindControl("textDtVigenciaClone") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteClone = pageControl.FindControl("btnEditCodigoClienteClone") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacaoClone = pageControl.FindControl("dropTipoOperacaoClone") as ASPxComboBox;

        //Cadastrado
        int idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDtVigencia.Text);
        int tipoOperacao = Convert.ToInt32(dropTipoOperacao.Value);
        //Clone
        int idCarteiraClone = Convert.ToInt32(btnEditCodigoClienteClone.Text);
        DateTime dataVigenciaClone = Convert.ToDateTime(textDtVigenciaClone.Text);
        int tipoOperacaoClone = Convert.ToInt32(dropTipoOperacaoClone.Value);

        ParametroLiqAvancadoCollection parametroCollOrig = new ParametroLiqAvancadoCollection();
        ParametroLiqAvancadoAnaliticoCollection parametroCollAnaliticoOrig = new ParametroLiqAvancadoAnaliticoCollection();

        ParametroLiqAvancadoCollection parametroCollNovo = new ParametroLiqAvancadoCollection();
        ParametroLiqAvancadoAnaliticoCollection parametroCollAnaliticoNovo = new ParametroLiqAvancadoAnaliticoCollection();

        #region Parametro Composto
        parametroCollOrig.Query.Where(parametroCollOrig.Query.IdCarteira.Equal(idCarteira)
                                      & parametroCollOrig.Query.DataVigencia.Equal(dataVigencia)
                                      & parametroCollOrig.Query.TipoOperacao.Equal(tipoOperacao));

        if (parametroCollOrig.Query.Load())
        {
            foreach (ParametroLiqAvancado parametroOrig in parametroCollOrig)
            {
                ParametroLiqAvancado parametroNovo = parametroCollNovo.AddNew();
                parametroNovo.DataVigencia = dataVigenciaClone;
                parametroNovo.IdCarteira = idCarteiraClone;
                parametroNovo.TipoOperacao = tipoOperacaoClone;

                parametroNovo.TipoData = parametroOrig.TipoData;
                parametroNovo.Parametro = parametroOrig.Parametro;
                parametroNovo.ParametroValido = parametroOrig.ParametroValido;
            }
        }
        #endregion

        #region Parametro Analitico
        parametroCollAnaliticoOrig.Query.Where(parametroCollAnaliticoOrig.Query.IdCarteira.Equal(idCarteira)
                                      & parametroCollAnaliticoOrig.Query.DataVigencia.Equal(dataVigencia)
                                      & parametroCollAnaliticoOrig.Query.TipoOperacao.Equal(tipoOperacao));

        if (parametroCollAnaliticoOrig.Query.Load())
        {
            foreach (ParametroLiqAvancadoAnalitico parametroAnaliticoOrig in parametroCollAnaliticoOrig)
            {
                ParametroLiqAvancadoAnalitico parametroAnaliticoNovo = parametroCollAnaliticoNovo.AddNew();
                parametroAnaliticoNovo.DataVigencia = dataVigenciaClone;
                parametroAnaliticoNovo.IdCarteira = idCarteiraClone;
                parametroAnaliticoNovo.TipoOperacao = tipoOperacaoClone;

                parametroAnaliticoNovo.TipoData = parametroAnaliticoOrig.TipoData;
                parametroAnaliticoNovo.IdParametroLiqAvancado = parametroAnaliticoOrig.IdParametroLiqAvancado;
                parametroAnaliticoNovo.OrdemProcessamento = parametroAnaliticoOrig.OrdemProcessamento;
                parametroAnaliticoNovo.ParametroComposto = parametroAnaliticoOrig.ParametroComposto;
                parametroAnaliticoNovo.ParametroDois = parametroAnaliticoOrig.ParametroDois;
                parametroAnaliticoNovo.ParametroUm = parametroAnaliticoOrig.ParametroUm;
            }
        }
        #endregion

        parametroCollAnaliticoNovo.Save();
        parametroCollNovo.Save();
        gridCadastro.DataBind();

        e.Result = "Cadastro clonado com sucesso.";
    }
    #endregion

    #region _Load
    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn TipoOperacao = gridCadastro.Columns["TipoOperacao"] as GridViewDataComboBoxColumn;
        if (TipoOperacao != null)
        {
            TipoOperacao.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Fundo.Enums.TipoParametroLiqOperacao)))
            {
                TipoOperacao.PropertiesComboBox.Items.Add(Financial.Fundo.Enums.TipoParametroLiqOperacaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropTipoOperacao_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropTipoOperacao = pageControl.FindControl("dropTipoOperacao") as ASPxComboBox;
        if (dropTipoOperacao != null)
        {
            dropTipoOperacao.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Fundo.Enums.TipoParametroLiqOperacao)))
            {
                dropTipoOperacao.Items.Add(Financial.Fundo.Enums.TipoParametroLiqOperacaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropTipoOperacaoClone_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropTipoOperacaoClone = pageControl.FindControl("dropTipoOperacaoClone") as ASPxComboBox;
        if (dropTipoOperacaoClone != null)
        {
            dropTipoOperacaoClone.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Fundo.Enums.TipoParametroLiqOperacao)))
            {
                dropTipoOperacaoClone.Items.Add(Financial.Fundo.Enums.TipoParametroLiqOperacaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void tabCadastroOn_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = sender as ASPxPageControl;

        //Somente no Update
        if (gridCadastro.IsNewRowEditing)
        {
            pageControl.TabPages[1].ClientVisible = false;
            pageControl.TabPages[2].ClientVisible = false;
        }
        else
        {
            string compositeKey = Convert.ToString(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, "CompositeKey"));
            string[] keys = compositeKey.Split('|');

            int idCarteira = Convert.ToInt32(keys[0]);
            DateTime dataVigencia = Convert.ToDateTime(keys[1]);
            int tipoOperacao = Convert.ToInt32(keys[2]);
            int tipoData = (int)TipoParametroLiquidacao.DataOperacao;

            ParametroLiqAvancadoCollection parametroColl = new ParametroLiqAvancadoCollection();
            parametroColl.Query.Select(parametroColl.Query.ParametroValido);
            parametroColl.Query.Where(parametroColl.Query.IdCarteira.Equal(idCarteira)
                                      & parametroColl.Query.DataVigencia.Equal(dataVigencia)
                                      & parametroColl.Query.ParametroValido.Equal("S")
                                      & parametroColl.Query.TipoOperacao.Equal(tipoOperacao));

            pageControl.TabPages[1].ClientVisible = false;
            if (parametroColl.Query.Load())
            {
                pageControl.TabPages[1].ClientVisible = true;
                if (parametroColl.Count < 3)
                    pageControl.TabPages[1].ClientVisible = false;
            }
            
            Label lblPrimeiraData = pageControl.TabPages[2].FindControl("lblPrimeiraData") as Label;
            Label lblSegundaData = pageControl.TabPages[2].FindControl("lblSegundaData") as Label;

            if (tipoOperacao == (int)TipoParametroLiqOperacao.Aplicacao)
            {
                lblPrimeiraData.Text = "Data de Liquidação";
                lblSegundaData.Text = "Data de Conversão";
            }
            else if (tipoOperacao == (int)TipoParametroLiqOperacao.Resgate)
            {
                lblPrimeiraData.Text = "Data de Conversão";
                lblSegundaData.Text = "Data de Liquidação";
            }

        }
    }
    #endregion

    #region _Init
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void btnEditCodigoCliente_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).Enabled = false;
    }

    protected void textDtVigencia_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void dropTipoOperacao_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }
    #endregion

    #region Outros
    private void ExecutaMontagemParametro(int idCarteira, DateTime dataVigencia, int tipoOperacao, int? tipoData, string parametroValido)
    {
        ParametroLiqAvancadoCollection coll = new ParametroLiqAvancadoCollection();
        if (tipoData.HasValue)
        {
            coll.MontaStringParametroLiqAvanc(idCarteira, dataVigencia, tipoData.Value, tipoOperacao, parametroValido, true);
        }
        else
        {
            coll.MontaStringParametroLiqAvanc(idCarteira, dataVigencia, (int)TipoParametroLiquidacao.DataConversao, tipoOperacao, parametroValido, true);
            coll.MontaStringParametroLiqAvanc(idCarteira, dataVigencia, (int)TipoParametroLiquidacao.DataLiquidacao, tipoOperacao, parametroValido, true);
            coll.MontaStringParametroLiqAvanc(idCarteira, dataVigencia, (int)TipoParametroLiquidacao.DataOperacao, tipoOperacao, parametroValido, true);
        }
    }

    /*protected void gridParametroAnalitico_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        if (e.GetValue("IdParametroLiqAvancado") != null)
        {
            string parametro = e.GetValue("IdParametroLiqAvancado").ToString();
            if (parametro == "C" || parametro == "D")
            {
                foreach (object cell in e.Row.Cells)
                {
                    Type type = cell.GetType();

                    if (type.FullName.Equals("DevExpress.Web.Rendering.GridViewTableDataCell"))
                    {                        
                        DevExpress.Web.Rendering.GridViewTableDataCell cellEditing = (DevExpress.Web.Rendering.GridViewTableDataCell)cell;
                        if (cellEditing.DataColumn.FieldName.Equals("ParametroDois"))
                        {
                            ((DevExpress.Web.Rendering.GridViewTableDataCell)cell).DataColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                        }
                    }
                }
            }

        }
    }*/


    #endregion
}