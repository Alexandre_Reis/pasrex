﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaExpurgoAdministracao.aspx.cs" Inherits="CadastrosBasicos_TabelaExpurgoAdministracao" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataTabelaAdm(data) {        
        hiddenIdTabela.SetValue(data);
        ASPxCallback2.SendCallback(data);
        popupTabelaAdm.HideWindow();
        btnEditTabelaAdm.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                  
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTabelaAdm.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>

    <dxpc:ASPxPopupControl ID="popupTabelaAdm" ClientInstanceName="popupTabelaAdm" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabelaAdm" runat="server" Width="100%"
                    ClientInstanceName="gridTabelaAdm"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaAdm" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabelaAdm_CustomDataCallback" 
                    OnCustomCallback="gridTabelaAdm_CustomCallback"
                    
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" VisibleIndex="0" Width="14%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="60%" Settings-AutoFilterCondition="Contains" />
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="2" Width="15%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridTabelaAdm.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTabelaAdm);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Taxa Adm." />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabelaAdm.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Expurgo para Taxa de Administração"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaExpurgoAdm"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"                
                OnRowInserting="gridCadastro_RowInserting"                
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                                                                
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" UnboundType="String" Caption="Tabela Adm." VisibleIndex="1" Width="30%" ExportWidth="350" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="2" Width="15%" ExportWidth="100">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Ações'>Ações</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Ações Ativo'>Ações Ativo</div>" /> 
                        <dxe:ListEditItem Value="10" Text="<div title='Opções'>Opções</div>" />
                        <dxe:ListEditItem Value="11" Text="<div title='Indicação'>Opções Ativo</div>" />
                        <dxe:ListEditItem Value="50" Text="<div title='Renda Fixa'>Renda Fixa</div>" />
                        <dxe:ListEditItem Value="51" Text="<div title='Renda Fixa Papel'>Renda Fixa Papel</div>" />
                        <dxe:ListEditItem Value="52" Text="<div title='Renda Fixa Titulo'>Renda Fixa Titulo</div>" />
                        <dxe:ListEditItem Value="80" Text="<div title='Fundos'>Fundos</div>" />
                        <dxe:ListEditItem Value="81" Text="<div title='Fundos Ativo'>Fundos Ativo</div>" />                                                                                                                                                        
                    </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                                               
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="3" Width="10%"/>                                                
                <dxwgv:GridViewDataTextColumn FieldName="Codigo" Caption="Codigo" VisibleIndex="4" Width="35%" CellStyle-HorizontalAlign="left"/>
                                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdTabela" Visible="false"/>
                <%--<dxwgv:GridViewDataColumn FieldName="TipoTraduzido" UnboundType="String" Caption="Tipo" VisibleIndex="2" Width="30%" />--%>
                
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdTabela" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTabela")%>' ClientInstanceName="hiddenIdTabela" />
                        
                         <table border="0">
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Taxa Adm.:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTabelaAdm" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditTabelaAdm" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompleta")%>' OnLoad="btnEditTabelaAdm_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTabelaAdm.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                          
                            </tr>
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Tipo:" />
                                </td>                
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropTipo" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("Tipo")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Ações" />
                                    <dxe:ListEditItem Value="2" Text="Ações Ativo" />
                                    <dxe:ListEditItem Value="10" Text="Opções" />        
                                    <dxe:ListEditItem Value="11" Text="Opções Ativo" />
                                    <dxe:ListEditItem Value="50" Text="Renda Fixa" />
                                    <dxe:ListEditItem Value="51" Text="Renda Fixa Papel" />
                                    <dxe:ListEditItem Value="52" Text="Renda Fixa Titulo" />
                                    <dxe:ListEditItem Value="80" Text="Fundos" />
                                    <dxe:ListEditItem Value="81" Text="Fundos Ativo" />
                                    </Items>                                         
                                    </dxe:ASPxComboBox>                                                                                                                                                                                                                                                                                                                                                  
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataRef" runat="server" CssClass="labelRequired" Text="Data Referência:"/>
                                </td>
                                                        
                                <td>                        
                                    <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>
                                </td>  
                            </tr>
                            
                            <tr>                            
                                <td class="td_Label">
                                    <asp:Label ID="labelCodigo" runat="server" CssClass="labelNormal" Text="Código:" />
                                </td>
                                
                                <td>
                                    <dxe:ASPxTextBox ID="textCodigo" runat="server" ClientInstanceName="textCodigo" CssClass="textNormal_5" MaxLength="14" Text='<%#Eval("Codigo")%>' />
                                </td>
                            </tr>
                            
                            </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" LeftMargin="70" />
        
    <cc1:esDataSource ID="EsDSTabelaExpurgoAdm" runat="server" OnesSelect="EsDSTabelaExpurgoAdm_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaAdm" runat="server" OnesSelect="EsDSTabelaAdm_esSelect" />        
    </form>
</body>
</html>