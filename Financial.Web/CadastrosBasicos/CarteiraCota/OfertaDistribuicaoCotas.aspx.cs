﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util.Enums;
using Financial.InvestidorCotista;

public partial class CadastroBasicos_OfertaDistribuicaoCotas : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSOfertas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
        OfertaDistribuicaoCotasQuery ofertaDistribuicaoCotasQuery = new OfertaDistribuicaoCotasQuery("ODC");
        AplicacaoOfertaDistribuicaoCotasQuery aplicacaoOfertaDistribuicaoCotasQuery = new AplicacaoOfertaDistribuicaoCotasQuery("AODC");
        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");

        ofertaDistribuicaoCotasQuery.Select(ofertaDistribuicaoCotasQuery.IdOfertaDistribuicaoCotas,
                                            ofertaDistribuicaoCotasQuery.IdCarteira,
                                            ofertaDistribuicaoCotasQuery.PrimeiraEmissao,
                                            ofertaDistribuicaoCotasQuery.ApenasInvestidorProfissional,
                                            ofertaDistribuicaoCotasQuery.ApenasInvestidorQualificado,
                                            ofertaDistribuicaoCotasQuery.DataInicialOferta,
                                            ofertaDistribuicaoCotasQuery.DataFinalOferta,
                                            ofertaDistribuicaoCotasQuery.QuantidadeTotalCotas,
                                            ofertaDistribuicaoCotasQuery.CotaEspecifica,
                                            ofertaDistribuicaoCotasQuery.ValorCota,
                                            carteiraQuery.Nome, 
                                            (ofertaDistribuicaoCotasQuery.ValorCota * ofertaDistribuicaoCotasQuery.QuantidadeTotalCotas).As("TotalEmissao"),
                                            operacaoCotistaQuery.ValorBruto.Sum());
        ofertaDistribuicaoCotasQuery.LeftJoin(aplicacaoOfertaDistribuicaoCotasQuery).On(ofertaDistribuicaoCotasQuery.IdOfertaDistribuicaoCotas.Equal(aplicacaoOfertaDistribuicaoCotasQuery.IdOfertaDistribuicaoCotas));
        ofertaDistribuicaoCotasQuery.LeftJoin(operacaoCotistaQuery).On(aplicacaoOfertaDistribuicaoCotasQuery.IdOperacao.Equal(operacaoCotistaQuery.IdOperacao));
        ofertaDistribuicaoCotasQuery.InnerJoin(carteiraQuery).On(ofertaDistribuicaoCotasQuery.IdCarteira == carteiraQuery.IdCarteira);
        ofertaDistribuicaoCotasQuery.GroupBy(ofertaDistribuicaoCotasQuery.IdOfertaDistribuicaoCotas,
                                            ofertaDistribuicaoCotasQuery.IdCarteira,
                                            ofertaDistribuicaoCotasQuery.PrimeiraEmissao,
                                            ofertaDistribuicaoCotasQuery.ApenasInvestidorProfissional,
                                            ofertaDistribuicaoCotasQuery.ApenasInvestidorQualificado,
                                            ofertaDistribuicaoCotasQuery.DataInicialOferta,
                                            ofertaDistribuicaoCotasQuery.DataFinalOferta,
                                            ofertaDistribuicaoCotasQuery.QuantidadeTotalCotas,
                                            ofertaDistribuicaoCotasQuery.CotaEspecifica,
                                            ofertaDistribuicaoCotasQuery.ValorCota,
                                            carteiraQuery.Nome);
        OfertaDistribuicaoCotasCollection coll = new OfertaDistribuicaoCotasCollection();
        coll.Load(ofertaDistribuicaoCotasQuery);

        e.Collection = coll;
    }

    

    /// <summary>
    /// PopUp Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        CarteiraCollection carteiraCollection = new CarteiraCollection();

        carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira,
                                        carteiraCollection.Query.Apelido);
        carteiraCollection.Query.Where(carteiraCollection.Query.TipoFundo.Equal(TipoFundo.Fechado));
        
        carteiraCollection.Query.Load();

        e.Collection = carteiraCollection;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);

            Carteira carteira = new Carteira();

            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                string apelido = Convert.ToString(carteira.Apelido);
                string apenasInvestidorProfissional = carteira.ApenasInvestidorProfissional;
                string apenasInvestidorQualificado = carteira.ApenasInvestidorQualificado;
                texto = apelido + "|" + apenasInvestidorProfissional + "|" + apenasInvestidorQualificado;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        
        ASPxComboBox dropPrimeiraEmissao = gridCadastro.FindEditFormTemplateControl("dropPrimeiraEmissao") as ASPxComboBox;
        ASPxComboBox dropApenasInvestidorQualificado = gridCadastro.FindEditFormTemplateControl("dropApenasInvestidorQualificado") as ASPxComboBox;
        ASPxComboBox dropApenasInvestidorProfissional = gridCadastro.FindEditFormTemplateControl("dropApenasInvestidorProfissional") as ASPxComboBox;

        ASPxDateEdit textDataInicialOferta = gridCadastro.FindEditFormTemplateControl("textDataInicialOferta") as ASPxDateEdit;

        ASPxSpinEdit textQuantidadeTotalCotas = gridCadastro.FindEditFormTemplateControl("textQuantidadeTotalCotas") as ASPxSpinEdit;
        ASPxSpinEdit textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;

        ASPxComboBox dropCotaEspecifica = gridCadastro.FindEditFormTemplateControl("dropCotaEspecifica") as ASPxComboBox;
        
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditCodigoCarteira, 
            dropPrimeiraEmissao, dropApenasInvestidorQualificado, dropApenasInvestidorProfissional,
            textDataInicialOferta, textQuantidadeTotalCotas, textValorCota, dropCotaEspecifica});

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        OfertaDistribuicaoCotas ofertaDistribuicaoCotas = new OfertaDistribuicaoCotas();

        Salvar(ofertaDistribuicaoCotas);

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void Salvar(OfertaDistribuicaoCotas ofertaDistribuicaoCotas)
    {
        //
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropPrimeiraEmissao = gridCadastro.FindEditFormTemplateControl("dropPrimeiraEmissao") as ASPxComboBox;
        ASPxComboBox dropApenasInvestidorQualificado = gridCadastro.FindEditFormTemplateControl("dropApenasInvestidorQualificado") as ASPxComboBox;
        ASPxComboBox dropApenasInvestidorProfissional = gridCadastro.FindEditFormTemplateControl("dropApenasInvestidorProfissional") as ASPxComboBox;
        ASPxDateEdit textDataInicialOferta = gridCadastro.FindEditFormTemplateControl("textDataInicialOferta") as ASPxDateEdit;
        ASPxDateEdit textDataFinalOferta = gridCadastro.FindEditFormTemplateControl("textDataFinalOferta") as ASPxDateEdit;
        ASPxSpinEdit textQuantidadeTotalCotas = gridCadastro.FindEditFormTemplateControl("textQuantidadeTotalCotas") as ASPxSpinEdit;
        ASPxSpinEdit textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        ASPxComboBox dropCotaEspecifica = gridCadastro.FindEditFormTemplateControl("dropCotaEspecifica") as ASPxComboBox;
        //

        ofertaDistribuicaoCotas.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        ofertaDistribuicaoCotas.PrimeiraEmissao = Convert.ToString(dropPrimeiraEmissao.Value);
        ofertaDistribuicaoCotas.ApenasInvestidorQualificado = Convert.ToString(dropApenasInvestidorQualificado.Value);
        ofertaDistribuicaoCotas.ApenasInvestidorProfissional = Convert.ToString(dropApenasInvestidorProfissional.Value);
        ofertaDistribuicaoCotas.DataInicialOferta = Convert.ToDateTime(textDataInicialOferta.Text);
        if(textDataFinalOferta.Text != "")
            ofertaDistribuicaoCotas.DataFinalOferta = Convert.ToDateTime(textDataFinalOferta.Text);
        ofertaDistribuicaoCotas.QuantidadeTotalCotas = Convert.ToInt32(textQuantidadeTotalCotas.Text);
        ofertaDistribuicaoCotas.ValorCota = Convert.ToDecimal(textValorCota.Text);
        ofertaDistribuicaoCotas.CotaEspecifica = Convert.ToString(dropCotaEspecifica.Value);

        ofertaDistribuicaoCotas.Save();

    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOfertaDistribuicaoCotas = (int)e.Keys[0];
        OfertaDistribuicaoCotas ofertaDistribuicaoCotas = new OfertaDistribuicaoCotas();

        if (ofertaDistribuicaoCotas.LoadByPrimaryKey(idOfertaDistribuicaoCotas))
        {
            this.Salvar(ofertaDistribuicaoCotas);

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Travamento de Cotas - Operacao: Update TravamentoCotas: " + ofertaDistribuicaoCotas.IdOfertaDistribuicaoCotas + "; " + UtilitarioWeb.ToString(ofertaDistribuicaoCotas),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        OfertaDistribuicaoCotas ofertaDistribuicaoCotas = new OfertaDistribuicaoCotas();
        Salvar(ofertaDistribuicaoCotas);

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Travamento de Cotas - Operacao: Insert TravamentoCotas: " + ofertaDistribuicaoCotas.IdOfertaDistribuicaoCotas + "; " + UtilitarioWeb.ToString(ofertaDistribuicaoCotas),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdPerfil = gridCadastro.GetSelectedFieldValues("IdOfertaDistribuicaoCotas");

            for (int i = 0; i < keyValuesIdPerfil.Count; i++)
            {
                int idOfertaDistribuicaoCotas = Convert.ToInt32(keyValuesIdPerfil[i]);

                OfertaDistribuicaoCotas ofertaDistribuicaoCotas = new OfertaDistribuicaoCotas();
                if (ofertaDistribuicaoCotas.LoadByPrimaryKey(idOfertaDistribuicaoCotas))
                {
                    //
                    OfertaDistribuicaoCotas ofertaDistribuicaoCotasClone = (OfertaDistribuicaoCotas)Utilitario.Clone(ofertaDistribuicaoCotas);
                    //

                    ofertaDistribuicaoCotas.MarkAsDeleted();
                    ofertaDistribuicaoCotas.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OfertaDistribuicaoCotas - Operacao: Delete OfertaDistribuicaoCotas: " + ofertaDistribuicaoCotasClone.IdOfertaDistribuicaoCotas + "; " + UtilitarioWeb.ToString(ofertaDistribuicaoCotasClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }
}