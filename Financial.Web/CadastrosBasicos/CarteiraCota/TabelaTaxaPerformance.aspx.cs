﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using DevExpress.Web.Data;
using Financial.Fundo.Enums;
using System.Text.RegularExpressions;
using Financial.Web.Common;
using Financial.Contabil;
using Financial.WebConfigConfiguration;
using Financial.Web.Util;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;

public partial class CadastrosBasicos_TabelaTaxaPerformance : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        this.FiltroGridEvento();
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculo }));

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTabelaTaxaPerformance_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaTaxaPerformanceQuery tabelaTaxaPerformanceQuery = new TabelaTaxaPerformanceQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        tabelaTaxaPerformanceQuery.Select(tabelaTaxaPerformanceQuery, carteiraQuery.Apelido.As("Apelido"),
                        "<'' as DescricaoEventoPagamento >", "<'' as DescricaoEventoProvisao >");
        tabelaTaxaPerformanceQuery.InnerJoin(carteiraQuery).On(tabelaTaxaPerformanceQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaPerformanceQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaTaxaPerformanceQuery.IdCarteira);
        tabelaTaxaPerformanceQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        tabelaTaxaPerformanceQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                         permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaTaxaPerformanceQuery.OrderBy(tabelaTaxaPerformanceQuery.DataReferencia.Descending);

        TabelaTaxaPerformanceCollection coll = new TabelaTaxaPerformanceCollection();
        coll.Load(tabelaTaxaPerformanceQuery);

        tabelaTaxaPerformanceQuery = new TabelaTaxaPerformanceQuery("L");
        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("Q");

        tabelaTaxaPerformanceQuery.Select(tabelaTaxaPerformanceQuery, carteiraQuery.Apelido.As("Apelido"),
                        "<'' as DescricaoEventoPagamento >", "<'' as DescricaoEventoProvisao >");
        tabelaTaxaPerformanceQuery.InnerJoin(carteiraQuery).On(tabelaTaxaPerformanceQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaPerformanceQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaTaxaPerformanceQuery.IdCarteira);
        tabelaTaxaPerformanceQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                         clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));

        tabelaTaxaPerformanceQuery.OrderBy(tabelaTaxaPerformanceQuery.DataReferencia.Descending);

        TabelaTaxaPerformanceCollection coll2 = new TabelaTaxaPerformanceCollection();
        coll2.Load(tabelaTaxaPerformanceQuery);

        coll.Combine(coll2);

        for (int i = 0; i < coll.Count; i++)
        {
            if (coll[i].IdEventoPagamento.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoPagamento", coll[i].IdEventoPagamento + " - " + coll[i].UpToContabRoteiroByIdEventoPagamento.Descricao);
            }

            if (coll[i].IdEventoProvisao.HasValue)
            {
                coll[i].SetColumn("DescricaoEventoProvisao", coll[i].IdEventoProvisao + " - " + coll[i].UpToContabRoteiroByIdEventoProvisao.Descricao);
            }
        }

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region PopUp ContabRoteiro
    /// <summary>
    /// PopUp ContabRoteiro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSEvento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabRoteiroCollection coll = new ContabRoteiroCollection();

        coll.Query.Select(coll.Query.IdEvento, coll.Query.Descricao, coll.Query.ContaCredito, coll.Query.ContaDebito)
               .OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idEvento = (int)gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.IdEvento);
        string descricao = Convert.ToString(gridView.GetRowValues(visibleIndex, ContabRoteiroMetadata.ColumnNames.Descricao));

        e.Result = idEvento.ToString() + "|" + " - " + descricao;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEvento_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridEvento.DataBind();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textPercentualIndice = gridCadastro.FindEditFormTemplateControl("textPercentualIndice") as ASPxSpinEdit;
        ASPxComboBox dropTipoApropriacaoJuros = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacaoJuros") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
        ASPxComboBox dropContagemDiasJuros = gridCadastro.FindEditFormTemplateControl("dropContagemDiasJuros") as ASPxComboBox;
        ASPxComboBox dropBaseAnoJuros = gridCadastro.FindEditFormTemplateControl("dropBaseAnoJuros") as ASPxComboBox;
        ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
        ASPxSpinEdit textTaxaPerformance = gridCadastro.FindEditFormTemplateControl("textTaxaPerformance") as ASPxSpinEdit;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxComboBox dropCalculaBenchmarkNegativo = gridCadastro.FindEditFormTemplateControl("dropCalculaBenchmarkNegativo") as ASPxComboBox;
        ASPxComboBox dropZeraPerformanceNegativa = gridCadastro.FindEditFormTemplateControl("dropZeraPerformanceNegativa") as ASPxComboBox;
        ASPxComboBox dropCalculaPerformanceResgate = gridCadastro.FindEditFormTemplateControl("dropCalculaPerformanceResgate") as ASPxComboBox;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamentoResgate = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamentoResgate") as ASPxSpinEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(dropTipoCalculo);
        controles.Add(dropIndice);
        controles.Add(textPercentualIndice);
        controles.Add(dropTipoApropriacaoJuros);
        controles.Add(textTaxaJuros);
        controles.Add(dropContagemDiasJuros);
        controles.Add(dropBaseAnoJuros);
        controles.Add(dropBaseApuracao);
        controles.Add(textTaxaPerformance);
        controles.Add(textDiaRenovacao);
        controles.Add(textNumeroMesesRenovacao);
        controles.Add(dropCalculaBenchmarkNegativo);
        controles.Add(dropZeraPerformanceNegativa);
        controles.Add(dropCalculaPerformanceResgate);
        controles.Add(textNumeroDiasPagamento);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        if (dropCalculaPerformanceResgate.SelectedIndex == 0)
        {
            ASPxComboBox dropTipoCalculoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoCalculoResgate") as ASPxComboBox;
            if (dropTipoCalculoResgate.SelectedIndex == -1)
            {
                e.Result = "Tipo de Cálculo Resgate deve ser preenchido para Cálculo Resgate = Sim.";
                return;
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idTabela = (int)e.Keys[0];
        //
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textPercentualIndice = gridCadastro.FindEditFormTemplateControl("textPercentualIndice") as ASPxSpinEdit;
        ASPxComboBox dropTipoApropriacaoJuros = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacaoJuros") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
        ASPxComboBox dropContagemDiasJuros = gridCadastro.FindEditFormTemplateControl("dropContagemDiasJuros") as ASPxComboBox;
        ASPxComboBox dropBaseAnoJuros = gridCadastro.FindEditFormTemplateControl("dropBaseAnoJuros") as ASPxComboBox;
        ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
        ASPxSpinEdit textTaxaPerformance = gridCadastro.FindEditFormTemplateControl("textTaxaPerformance") as ASPxSpinEdit;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxComboBox dropCalculaBenchmarkNegativo = gridCadastro.FindEditFormTemplateControl("dropCalculaBenchmarkNegativo") as ASPxComboBox;
        ASPxComboBox dropZeraPerformanceNegativa = gridCadastro.FindEditFormTemplateControl("dropZeraPerformanceNegativa") as ASPxComboBox;
        ASPxComboBox dropCalculaPerformanceResgate = gridCadastro.FindEditFormTemplateControl("dropCalculaPerformanceResgate") as ASPxComboBox;
        ASPxComboBox dropTipoCalculoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoCalculoResgate") as ASPxComboBox;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamentoResgate = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamentoResgate") as ASPxSpinEdit;
        ASPxComboBox dropImpactaPL = gridCadastro.FindEditFormTemplateControl("dropImpactaPL") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;

        TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
        if (tabelaTaxaPerformance.LoadByPrimaryKey(idTabela))
        {
            #region TabelaTaxaPerformance
            tabelaTaxaPerformance.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            tabelaTaxaPerformance.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            tabelaTaxaPerformance.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);
            tabelaTaxaPerformance.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
            tabelaTaxaPerformance.PercentualIndice = Convert.ToDecimal(textPercentualIndice.Text);
            tabelaTaxaPerformance.TipoApropriacaoJuros = Convert.ToByte(dropTipoApropriacaoJuros.SelectedItem.Value);
            tabelaTaxaPerformance.TaxaJuros = Convert.ToDecimal(textTaxaJuros.Text);
            tabelaTaxaPerformance.ContagemDiasJuros = Convert.ToByte(dropContagemDiasJuros.SelectedItem.Value);
            tabelaTaxaPerformance.BaseAnoJuros = Convert.ToInt16(dropBaseAnoJuros.SelectedItem.Value);
            tabelaTaxaPerformance.BaseApuracao = Convert.ToByte(dropBaseApuracao.SelectedItem.Value);
            tabelaTaxaPerformance.TaxaPerformance = Convert.ToDecimal(textTaxaPerformance.Text);
            tabelaTaxaPerformance.DiaRenovacao = Convert.ToByte(textDiaRenovacao.Text);
            tabelaTaxaPerformance.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
            tabelaTaxaPerformance.CalculaBenchmarkNegativo = Convert.ToByte(dropCalculaBenchmarkNegativo.SelectedItem.Value);
            tabelaTaxaPerformance.ZeraPerformanceNegativa = Convert.ToString(dropZeraPerformanceNegativa.SelectedItem.Value);
            tabelaTaxaPerformance.CalculaPerformanceResgate = Convert.ToString(dropCalculaPerformanceResgate.SelectedItem.Value);

            if (dropCalculaPerformanceResgate.SelectedIndex == 0)
            {
                tabelaTaxaPerformance.TipoCalculoResgate = Convert.ToByte(dropTipoCalculoResgate.SelectedItem.Value);
            }
            else
            {
                tabelaTaxaPerformance.TipoCalculoResgate = 0; //Joga qq valor pq o campo eh Not null
            }

            tabelaTaxaPerformance.NumeroDiasPagamento = Convert.ToByte(textNumeroDiasPagamento.Text);

            if (textNumeroDiasPagamentoResgate.Text != "")
            {
                tabelaTaxaPerformance.NumeroDiasPagamentoResgate = Convert.ToByte(textNumeroDiasPagamentoResgate.Text);
            }
            else
            {
                tabelaTaxaPerformance.NumeroDiasPagamentoResgate = 0;
            }

            tabelaTaxaPerformance.ImpactaPL = Convert.ToString(dropImpactaPL.SelectedItem.Value);

            if (textDataFim.Text != "")
            {
                tabelaTaxaPerformance.DataFim = Convert.ToDateTime(textDataFim.Text);
            }
            else
            {
                tabelaTaxaPerformance.DataFim = null;
            }

            if (ParametrosConfiguracaoSistema.Outras.CalculaContatil) {
                //
                ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
                ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;

                if (textIdProvisaoEvento.Text != "")
                {
                    string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                    tabelaTaxaPerformance.IdEventoProvisao = Convert.ToInt32(dados[0].Trim());
                }

                if (textIdProvisaoPagamento.Text != "")
                {
                    string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                    tabelaTaxaPerformance.IdEventoPagamento = Convert.ToInt32(dados[0].Trim());
                }
            }
            #endregion
            //
            ASPxDateEdit textDataFimApropriacao = gridCadastro.FindEditFormTemplateControl("textDataFimApropriacao") as ASPxDateEdit;
            ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
            ASPxDateEdit textDataUltimoPagamento = gridCadastro.FindEditFormTemplateControl("textDataUltimoPagamento") as ASPxDateEdit;
            //
            bool updateCalculoPerformance = false;
            CalculoPerformanceHistoricoCollection cpClone = new CalculoPerformanceHistoricoCollection();
            //
            if (textDataFimApropriacao.Text.Trim() == "" && textDataPagamento.Text.Trim() == "" && textDataUltimoPagamento.Text.Trim() == "")
            {
                tabelaTaxaPerformance.Save();
            }
            else
            {
                if (textDataFimApropriacao.Text.Trim() != "")
                {
                    tabelaTaxaPerformance.DataFimApropriacao = Convert.ToDateTime(textDataFimApropriacao.Text);
                }

                if (textDataPagamento.Text.Trim() != "")
                {
                    tabelaTaxaPerformance.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
                }

                // Update de CalculoPerformanceHistorico
                updateCalculoPerformance = true;
                CalculoPerformanceHistoricoQuery cphQuery = new CalculoPerformanceHistoricoQuery();
                cphQuery.Select(cphQuery.DataFimApropriacao.Max())
                        .Where(cphQuery.IdTabela == idTabela);
                //
                CalculoPerformanceHistoricoQuery cphQuery1 = new CalculoPerformanceHistoricoQuery();
                cphQuery1.Select(cphQuery1.IdTabela, 
                                 cphQuery1.IdCarteira,
                                 cphQuery1.DataHistorico,
                                 cphQuery1.DataFimApropriacao, 
                                 cphQuery1.DataPagamento)
                         .Where(cphQuery1.IdTabela == idTabela &&
                                cphQuery1.DataFimApropriacao == cphQuery);

                CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
                calculoPerformanceHistoricoCollection.Load(cphQuery1);

                for (int i = 0; i < calculoPerformanceHistoricoCollection.Count; i++)
                {
                    if (textDataFimApropriacao.Text.Trim() != "")
                    {
                        calculoPerformanceHistoricoCollection[i].DataFimApropriacao = Convert.ToDateTime(textDataFimApropriacao.Text);
                    }

                    if (textDataPagamento.Text.Trim() != "")
                    {
                        calculoPerformanceHistoricoCollection[i].DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
                    }

                    if (textDataUltimoPagamento.Text.Trim() != "")
                    {
                        int idCarteira = calculoPerformanceHistoricoCollection[i].IdCarteira.Value;
                        DateTime dataHistorico = calculoPerformanceHistoricoCollection[i].DataHistorico.Value;

                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                                      posicaoCotistaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico));
                        posicaoCotistaHistoricoCollection.Query.Load();

                        foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
                        {
                            posicaoCotistaHistorico.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
                        }
                        posicaoCotistaHistoricoCollection.Save();

                        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                        posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                                     posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(dataHistorico));
                        posicaoCotistaAberturaCollection.Query.Load();

                        foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                        {
                            posicaoCotistaAbertura.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
                        }
                        posicaoCotistaAberturaCollection.Save();


                        tabelaTaxaPerformance.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
                    }
                }

                cpClone = (CalculoPerformanceHistoricoCollection)Utilitario.Clone(calculoPerformanceHistoricoCollection);

                tabelaTaxaPerformance.Save();
                calculoPerformanceHistoricoCollection.Save();

                if (textDataUltimoPagamento.Text.Trim() != "")
                {
                    int idCarteira = tabelaTaxaPerformance.IdCarteira.Value;

                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
                    posicaoCotistaCollection.Query.Load();

                    foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                    {
                        posicaoCotista.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
                    }
                    posicaoCotistaCollection.Save();

                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCarteira);

                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                    posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                                 posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(cliente.DataDia.Value));
                    posicaoCotistaAberturaCollection.Query.Load();

                    foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                    {
                        posicaoCotistaAbertura.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
                    }
                    posicaoCotistaAberturaCollection.Save();
                }
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaTaxaPerformance - Operacao: Update TabelaTaxaPerformance: " + idTabela + UtilitarioWeb.ToString(tabelaTaxaPerformance),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);

            if (updateCalculoPerformance)
            {
                foreach (CalculoPerformanceHistorico cp in cpClone)
                {
                    #region Log do Processo
                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "CalculoPerformanceHistorico - Operacao: Update CalculoPerformanceHistorico: " + cp.IdTabela + UtilitarioWeb.ToString(cp),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTipoCalculo = gridCadastro.FindEditFormTemplateControl("dropTipoCalculo") as ASPxComboBox;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textPercentualIndice = gridCadastro.FindEditFormTemplateControl("textPercentualIndice") as ASPxSpinEdit;
        ASPxComboBox dropTipoApropriacaoJuros = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacaoJuros") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
        ASPxComboBox dropContagemDiasJuros = gridCadastro.FindEditFormTemplateControl("dropContagemDiasJuros") as ASPxComboBox;
        ASPxComboBox dropBaseAnoJuros = gridCadastro.FindEditFormTemplateControl("dropBaseAnoJuros") as ASPxComboBox;
        ASPxComboBox dropBaseApuracao = gridCadastro.FindEditFormTemplateControl("dropBaseApuracao") as ASPxComboBox;
        ASPxSpinEdit textTaxaPerformance = gridCadastro.FindEditFormTemplateControl("textTaxaPerformance") as ASPxSpinEdit;
        ASPxSpinEdit textDiaRenovacao = gridCadastro.FindEditFormTemplateControl("textDiaRenovacao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroMesesRenovacao = gridCadastro.FindEditFormTemplateControl("textNumeroMesesRenovacao") as ASPxSpinEdit;
        ASPxComboBox dropCalculaBenchmarkNegativo = gridCadastro.FindEditFormTemplateControl("dropCalculaBenchmarkNegativo") as ASPxComboBox;
        ASPxComboBox dropZeraPerformanceNegativa = gridCadastro.FindEditFormTemplateControl("dropZeraPerformanceNegativa") as ASPxComboBox;
        ASPxComboBox dropCalculaPerformanceResgate = gridCadastro.FindEditFormTemplateControl("dropCalculaPerformanceResgate") as ASPxComboBox;
        ASPxComboBox dropTipoCalculoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoCalculoResgate") as ASPxComboBox;
        ASPxSpinEdit textNumeroDiasPagamento = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamento") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroDiasPagamentoResgate = gridCadastro.FindEditFormTemplateControl("textNumeroDiasPagamentoResgate") as ASPxSpinEdit;
        ASPxComboBox dropImpactaPL = gridCadastro.FindEditFormTemplateControl("dropImpactaPL") as ASPxComboBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        
        TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();

        tabelaTaxaPerformance.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaTaxaPerformance.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        tabelaTaxaPerformance.TipoCalculo = Convert.ToByte(dropTipoCalculo.SelectedItem.Value);
        tabelaTaxaPerformance.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
        tabelaTaxaPerformance.PercentualIndice = Convert.ToDecimal(textPercentualIndice.Text);
        tabelaTaxaPerformance.TipoApropriacaoJuros = Convert.ToByte(dropTipoApropriacaoJuros.SelectedItem.Value);
        tabelaTaxaPerformance.TaxaJuros = Convert.ToDecimal(textTaxaJuros.Text);
        tabelaTaxaPerformance.ContagemDiasJuros = Convert.ToByte(dropContagemDiasJuros.SelectedItem.Value);
        tabelaTaxaPerformance.BaseAnoJuros = Convert.ToInt16(dropBaseAnoJuros.SelectedItem.Value);
        tabelaTaxaPerformance.BaseApuracao = Convert.ToByte(dropBaseApuracao.SelectedItem.Value);
        tabelaTaxaPerformance.TaxaPerformance = Convert.ToDecimal(textTaxaPerformance.Text);
        tabelaTaxaPerformance.DiaRenovacao = Convert.ToByte(textDiaRenovacao.Text);
        tabelaTaxaPerformance.NumeroMesesRenovacao = Convert.ToByte(textNumeroMesesRenovacao.Text);
        tabelaTaxaPerformance.CalculaBenchmarkNegativo = Convert.ToByte(dropCalculaBenchmarkNegativo.SelectedItem.Value);
        tabelaTaxaPerformance.ZeraPerformanceNegativa = Convert.ToString(dropZeraPerformanceNegativa.SelectedItem.Value);
        tabelaTaxaPerformance.CalculaPerformanceResgate = Convert.ToString(dropCalculaPerformanceResgate.SelectedItem.Value);

        //Joga qq valor pq o campo eh Not null
        tabelaTaxaPerformance.TipoCalculoResgate = dropCalculaPerformanceResgate.SelectedIndex == 0
                                                   ? Convert.ToByte(dropTipoCalculoResgate.SelectedItem.Value)
                                                   : (byte)0;

        tabelaTaxaPerformance.NumeroDiasPagamento = Convert.ToByte(textNumeroDiasPagamento.Text);

        tabelaTaxaPerformance.NumeroDiasPagamentoResgate = textNumeroDiasPagamentoResgate.Text != ""
                                                           ? Convert.ToByte(textNumeroDiasPagamentoResgate.Text)
                                                           : (byte)0;

        tabelaTaxaPerformance.ImpactaPL = dropImpactaPL.SelectedIndex != -1
                                          ? Convert.ToString(dropImpactaPL.SelectedItem.Value)
                                          : "S";

        if (textDataFim.Text != "") {
            tabelaTaxaPerformance.DataFim = Convert.ToDateTime(textDataFim.Text);
        }

        if (ParametrosConfiguracaoSistema.Outras.CalculaContatil) {
            //
            ASPxButtonEdit textIdProvisaoEvento = gridCadastro.FindEditFormTemplateControl("btnEditEventoProvisao") as ASPxButtonEdit;
            ASPxButtonEdit textIdProvisaoPagamento = gridCadastro.FindEditFormTemplateControl("btnEditEventoPagamento") as ASPxButtonEdit;
            //
            if (textIdProvisaoEvento.Text != "") {
                string[] dados = textIdProvisaoEvento.Text.Split(new Char[] { '-' });
                int idEvento = Convert.ToInt32(dados[0].Trim());
                tabelaTaxaPerformance.IdEventoProvisao = idEvento;
            }

            if (textIdProvisaoPagamento.Text != "") {
                string[] dados = textIdProvisaoPagamento.Text.Split(new Char[] { '-' });
                int idEvento = Convert.ToInt32(dados[0].Trim());
                tabelaTaxaPerformance.IdEventoPagamento = idEvento;
            }
        }


        ASPxDateEdit textDataFimApropriacao = gridCadastro.FindEditFormTemplateControl("textDataFimApropriacao") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxDateEdit textDataUltimoPagamento = gridCadastro.FindEditFormTemplateControl("textDataUltimoPagamento") as ASPxDateEdit;

        if (textDataFimApropriacao.Text != "")
        {
            tabelaTaxaPerformance.DataFimApropriacao = Convert.ToDateTime(textDataFimApropriacao.Text);
        }

        if (textDataPagamento.Text != "")
        {
            tabelaTaxaPerformance.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
        }

        if (textDataUltimoPagamento.Text != "")
        {
            tabelaTaxaPerformance.DataUltimoCortePfee = Convert.ToDateTime(textDataUltimoPagamento.Text);
        }

        tabelaTaxaPerformance.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaTaxaPerformance - Operacao: Insert TabelaTaxaPerformance: " + tabelaTaxaPerformance.IdTabela + UtilitarioWeb.ToString(tabelaTaxaPerformance),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TabelaTaxaPerformanceMetadata.ColumnNames.IdTabela);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTabela = Convert.ToInt32(keyValuesId[i]);

                TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
                if (tabelaTaxaPerformance.LoadByPrimaryKey(idTabela))
                {
                    CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
                    calculoPerformanceCollection.Query.Where(calculoPerformanceCollection.Query.IdTabela.Equal(idTabela));
                    if (calculoPerformanceCollection.Query.Load())
                    {
                        throw new Exception("Tabela " + tabelaTaxaPerformance.IdTabela + " não pode ser excluída por ter processos relacionados.");
                    }

                    TabelaTaxaPerformance tabelaTaxaPerformanceClone = (TabelaTaxaPerformance)Utilitario.Clone(tabelaTaxaPerformance);
                    //

                    tabelaTaxaPerformance.MarkAsDeleted();
                    tabelaTaxaPerformance.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaTaxaPerformance - Operacao: Delete TabelaTaxaPerformance: " + idTabela + UtilitarioWeb.ToString(tabelaTaxaPerformanceClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues[TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamentoResgate] = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
    }
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        //if (!WebConfig.AppSettings.CalculaContatil) {
        if (!ParametrosConfiguracaoSistema.Outras.CalculaContatil) {

            Control c1 = this.gridCadastro.FindEditFormTemplateControl("divEventos");
            if (c1 != null)
            {
                c1.Visible = false;
            }
        }
                
        base.panelEdicao_Load(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo descricao.
    /// </summary>
    public void FiltroGridEvento()
    {
        GridViewDataColumn colDescricao = gridEvento.Columns["Descricao"] as GridViewDataColumn;
        if (colDescricao != null)
        {
            colDescricao.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}