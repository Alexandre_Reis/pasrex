﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxaConsolidada.aspx.cs" Inherits="CadastrosBasicos_TaxaGlobal" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataTabelaAdmGlobal(data) {        
        hiddenIdTabelaGlobal.SetValue(data);
        ASPxCallbackGlobal.SendCallback(data);
        popupTabelaAdmGlobal.HideWindow();
        btnEditTabelaAdmGlobal.Focus();
        gridTabelaAdmAssociada.PerformCallback(data);
    }
    
    function OnGetDataTabelaAdmAssociada(data) {
        hiddenIdTabelaAssociada.SetValue(data);
        ASPxCallbackAssociada.SendCallback(data);
        popupTabelaAdmAssociada.HideWindow();
        btnEditTabelaAdmAssociada.Focus();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallbackGlobal" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTabelaAdmGlobal.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallbackAssociada" runat="server" OnCallback="ASPxCallback2_Callback_Associada">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTabelaAdmAssociada.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>

    <dxpc:ASPxPopupControl ID="popupTabelaAdmGlobal" ClientInstanceName="popupTabelaAdmGlobal" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabelaAdmGlobal" runat="server" Width="100%"
                    ClientInstanceName="gridTabelaAdmGlobal"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaAdmCons" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabelaAdmGlobal_CustomDataCallback" 
                    OnCustomCallback="gridTabelaAdmGlobal_CustomCallback"
                    OnHtmlRowCreated="gridTabelaAdmGlobal_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" VisibleIndex="0" Width="14%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="60%"/>
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="2" Width="15%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridTabelaAdmGlobal.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTabelaAdmGlobal); 
                }" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Taxa Adm." />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabelaAdmGlobal.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupTabelaAdmAssociada" ClientInstanceName="popupTabelaAdmAssociada" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabelaAdmAssociada" runat="server" Width="100%"
                    ClientInstanceName="gridTabelaAdmAssociada"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaAdmAssoc" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabelaAdmAssociada_CustomDataCallback" 
                    OnCustomCallback="gridTabelaAdmAssociada_CustomCallback"
                    OnHtmlRowCreated="gridTabelaAdmAssociada_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" VisibleIndex="0" Width="14%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="60%"/>
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="2" Width="15%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridTabelaAdmAssociada.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTabelaAdmAssociada);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Taxa Adm." />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabelaAdmAssociada.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Taxas de Administração Consolidada"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdTaxaGlobal" DataSourceID="EsDSTaxaGlobal"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdTaxaGlobal" Visible="false"></dxwgv:GridViewDataColumn>
                                           
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompletaGlobal" Caption="Tabela Adm. Global" UnboundType="String" VisibleIndex="1" Width="40%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompletaAssociada" Caption="Tabela Adm. Associada" UnboundType="String" VisibleIndex="1" Width="40%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataColumn FieldName="Prioridade" Caption="Prioridade" UnboundType="String" VisibleIndex="3" Width="8%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdTabelaGlobal" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTabelaGlobal")%>' ClientInstanceName="hiddenIdTabelaGlobal" />
                        <dxe:ASPxTextBox ID="hiddenIdTabelaAssociada" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTabelaAssociada")%>' ClientInstanceName="hiddenIdTabelaAssociada" />
                        
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelTabelaGlobal" runat="server" CssClass="labelRequired" Text="Taxa Adm. Global:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTabelaAdmGlobal" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditTabelaAdmGlobal" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompletaGlobal")%>' OnLoad="btnEditTabelaAdm_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTabelaAdmGlobal.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                            
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Taxa Adm. Associada:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTabelaAdmAssociada" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditTabelaAdmAssociada" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompletaAssociada")%>' OnLoad="btnEditTabelaAdm_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTabelaAdmAssociada.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Prioridade:"></asp:Label>                    
                                </td>                    
                                <td colspan="3"> 
                                    <dxe:ASPxTextBox ID="textPrioridade" runat="server" CssClass="textNormal_5" MaxLength="2" NumberType="integer"
                                    ClientInstanceName="textPrioridade" Text='<%#Eval("Prioridade")%>' />
                                </td>                                                      
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTaxaGlobal" runat="server" OnesSelect="EsDSTaxaGlobal_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaAdmCons" runat="server" OnesSelect="EsDSTabelaAdmCons_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaAdmAssoc" runat="server" OnesSelect="EsDSTabelaAdmAssoc_esSelect" />
    </form>
</body>
</html>