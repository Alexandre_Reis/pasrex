﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaTaxaPerformance.aspx.cs" Inherits="CadastrosBasicos_TabelaTaxaPerformance" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }
    
    function OnGetDataEvento(data) {        
        /* idEvento, Descricao */
        var resultSplit = data.split('|');
        
        if(gridEventoOperacao == 'eventoProvisao') {
            btnEditEventoProvisao.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoProvisao.Focus();
        }
        else if(gridEventoOperacao == 'eventoPagamento') {        
            btnEditEventoPagamento.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoPagamento.Focus();
        }
    }
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
         if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupEvento" ClientInstanceName="popupEvento" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridEvento" runat="server" Width="100%"
                    ClientInstanceName="gridEvento"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSEvento" KeyFieldName="IdEvento"
                    OnCustomDataCallback="gridEvento_CustomDataCallback" 
                    OnCustomCallback="gridEvento_CustomCallback"
                    OnHtmlRowCreated="gridEvento_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdEvento" VisibleIndex="0" Visible="false"/>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="3" Width="70%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="Conta Débito" VisibleIndex="1" Width="15%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="Conta Crédito" VisibleIndex="2" Width="15%"/>  
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
            gridEvento.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataEvento);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Roteiro Contábil" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridEvento.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Taxa de Performance"></asp:Label>
    </div>
        
    <div id="mainContent">
                 
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdTabela" DataSourceID="EsDSTabelaTaxaPerformance"
                    OnInitNewRow="gridCadastro_InitNewRow"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    >
                
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdTabela" Caption="Id" VisibleIndex="1" Width="5%"
                                              HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="5%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="15%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCalculo" Caption="Tipo" VisibleIndex="5" Width="10%" ExportWidth="130">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Por Certificado'>Por Certificado</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Cota global'>Cota global</div>" />
                                <dxe:ListEditItem Value="3" Text="<div title='Cota global dia'>Cota global dia</div>" />                                
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="6" Width="12%">
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="PercentualIndice" Caption="% Índice" VisibleIndex="7" Width="6%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="TaxaJuros" Caption="+ Taxa" VisibleIndex="8" Width="6%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="TaxaPerformance" Caption="Tx Perform." VisibleIndex="9" Width="6%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="DiaRenovacao" Caption="Dia Renov." VisibleIndex="10" Width="6%" CellStyle-HorizontalAlign="Center"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroMesesRenovacao" Caption="Period. Meses" VisibleIndex="11" Width="7%" CellStyle-HorizontalAlign="Center"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="TipoApropriacaoJuros" Visible="false" />
                    <dxwgv:GridViewDataColumn FieldName="ContagemDiasJuros" Visible="false" />
                    <dxwgv:GridViewDataColumn FieldName="BaseAnoJuros" Visible="false" />
                    <dxwgv:GridViewDataColumn FieldName="BaseApuracao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="CalculaBenchmarkNegativo" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ZeraPerformanceNegativa" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="CalculaPerformanceResgate" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="TipoCalculoResgate" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroDiasPagamento" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroDiasPagamentoResgate" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="ImpactaPL" Visible="false" />
                    <dxwgv:GridViewDataColumn FieldName="DataFim" Visible="false" />
                </Columns>
                
                <Templates>            
                <EditForm>

                 <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                    
                    <div class="editForm">
                    
                    <table border="0">
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1" ClientInstanceName="btnEditCodigoCarteira"
                                                                            Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                    
                            <td colspan="3">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                            </td>
                        </tr>
                    
                        <tr>       
                            <td class="td_Label">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Referência:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>                            
                            </td>  
                                                    
                            <td align="right">
                                <asp:Label ID="labelTipoCalculo" runat="server" CssClass="labelRequired" Text="Tipo Cálculo:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropTipoCalculo" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoCalculo")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Por Certificado" />
                                <dxe:ListEditItem Value="2" Text="Cota global" />                    
                                <dxe:ListEditItem Value="3" Text="Cota global dia" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>                                     
                                
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelIndice" runat="server" CssClass="labelRequired" Text="Índice:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxComboBox ID="dropIndice" runat="server" ClientInstanceName="dropIndice"
                                                    DataSourceID="EsDSIndice" ShowShadow="false" DropDownStyle="DropDownList"
                                                    CssClass="dropDownListCurto_1" TextField="Descricao" ValueField="IdIndice"
                                                    Text='<%#Eval("IdIndice")%>'>
                                </dxe:ASPxComboBox>         
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelPercentualIndice" runat="server" CssClass="labelRequired" Text="% Índice:"></asp:Label>
                            </td>
                            <td>                                
                                <dxe:ASPxSpinEdit ID="textPercentualIndice" runat="server" CssClass="textPerc" ClientInstanceName="textPercentualIndice"
                                                                    Text='<%# Eval("PercentualIndice") %>' MaxLength="6" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>                                  
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoApropriacaoJuros" runat="server" CssClass="labelRequired" Text="Cálc. Juros:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropTipoApropriacaoJuros" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("TipoApropriacaoJuros")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Exponencial" />
                                <dxe:ListEditItem Value="2" Text="Linear" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelTaxaJuros" runat="server" CssClass="labelRequired" Text="Tx Juros:"></asp:Label>
                            </td>
                            <td>
                                
                                <dxe:ASPxSpinEdit ID="textTaxaJuros" runat="server" CssClass="textPerc" ClientInstanceName="textTaxaJuros"
                                                                Text='<%# Eval("TaxaJuros") %>' MaxLength="6" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>                                  
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelContagemDiasJuros" runat="server" CssClass="labelRequired" Text="Contagem Dias:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropContagemDiasJuros" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("ContagemDiasJuros")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Úteis" />
                                <dxe:ListEditItem Value="2" Text="Corridos" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelBaseAnoJuros" runat="server" CssClass="labelRequired" Text="Base Ano:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropBaseAnoJuros" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("BaseAnoJuros")%>'>
                                <Items>
                                <dxe:ListEditItem Value="252" Text="252" />
                                <dxe:ListEditItem Value="360" Text="360" />
                                <dxe:ListEditItem Value="365" Text="365" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelBaseApuracao" runat="server" CssClass="labelRequired" Text="Base Apuração:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropBaseApuracao" runat="server" ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("BaseApuracao")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Cota Líquida D-1" />
                                <dxe:ListEditItem Value="2" Text="Cota Bruta D0" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelTaxaPerformance" runat="server" CssClass="labelRequired" Text="Tx Pfee:"></asp:Label>
                            </td>
                            <td>                                
                                <dxe:ASPxSpinEdit ID="textTaxaPerformance" runat="server" CssClass="textPerc" ClientInstanceName="textTaxaPerformance"
                                                                Text='<%# Eval("TaxaPerformance") %>' MaxLength="10" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>       
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelDiaRenovacao" runat="server" CssClass="labelRequired" Text="Dia Renovação:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textDiaRenovacao" runat="server" CssClass="textCurto" ClientInstanceName="textDiaRenovacao"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('DiaRenovacao')%>">            
                                </dxe:ASPxSpinEdit>                                
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelNumeroMesesRenovacao" runat="server" CssClass="labelRequired" Text="Meses Renov.:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroMesesRenovacao" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroMesesRenovacao"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroMesesRenovacao')%>">            
                                </dxe:ASPxSpinEdit>                                
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCalculaBenchmarkNegativo" runat="server" CssClass="labelRequired" Text="Bench Negativo:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropCalculaBenchmarkNegativo" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("CalculaBenchmarkNegativo")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Não calcula Pfee" />
                                <dxe:ListEditItem Value="2" Text="Apenas positivo" />                                
                                <dxe:ListEditItem Value="3" Text="Ganho total" />
                                <dxe:ListEditItem Value="4" Text="Ganho c/ recálculo" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelZeraPerformanceNegativa" runat="server" CssClass="labelRequired" Text="Zera Negativo:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropZeraPerformanceNegativa" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("ZeraPerformanceNegativa")%>'>
                                <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                        </tr> 
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCalculaPerformanceResgate" runat="server" CssClass="labelRequired" Text="Pfee Resgate?:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropCalculaPerformanceResgate" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("CalculaPerformanceResgate")%>'>
                                <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não" />                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelTipoCalculoResgate" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropTipoCalculoResgate" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoCalculoResgate")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Calculado" />
                                <dxe:ListEditItem Value="2" Text="Proporcional" />                                                                
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                        </tr> 
                        
                        <tr>                            
                            <td class="td_Label">
                                <asp:Label ID="labelNumeroDiasPagamento" runat="server" CssClass="labelRequired" Text="Dias Pagto:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroDiasPagamento" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroDiasPagamento"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroDiasPagamento')%>">            
                                </dxe:ASPxSpinEdit>                                                            
                            </td>

                            <td align="right">
                                <asp:Label ID="labelNumeroDiasPagamentoResgate" runat="server" CssClass="labelNormal" Text="Dias Pgto Resg:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroDiasPagamentoResgate" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroDiasPagamentoResgate"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroDiasPagamentoResgate')%>" AllowNull="false" MinValue="0">            
                                </dxe:ASPxSpinEdit>                                                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelImpactaPL" runat="server" CssClass="labelNormal" Text="Impacta PL:"></asp:Label>                    
                            </td>   
                            <td>      
                                <dxe:ASPxComboBox ID="dropImpactaPL" runat="server" ShowShadow="false" CssClass="dropDownListCurto_6" Text='<%#Eval("ImpactaPL")%>'>
                                <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td align="right">
                                <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Data Fim:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Value='<%#Eval("DataFim")%>'/>                                
                            </td>  
                        </tr>
                        
                    <div id="divEventos" runat="server" visible="true">
                        
                        <tr>
                        <td  class="td_Label">
                            <asp:Label ID="labelEventoProvisao" runat="server" CssClass="labelNormal" Text="Evento Provisão:" />
                        </td>
                        <td colspan="4">
                        
                             <dxe:ASPxButtonEdit ID="btnEditEventoProvisao" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditEventoProvisao" 
                                        ReadOnly="true" Width="430px"  Text='<%# Eval("DescricaoEventoProvisao") %>' >                                                                                                               
                            <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                               <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoProvisao'; popupEvento.ShowAtElementByID(s.name);}" />                                                                   
                            </dxe:ASPxButtonEdit>
                        </td>                                                               
                        </tr>
                    
                        <tr>
                        <td  class="td_Label">
                            <asp:Label ID="labelEventoPagamento" runat="server" CssClass="labelNormal" Text="Evento Pagamento:" />
                        </td>
                        <td colspan="4">
                        
                             <dxe:ASPxButtonEdit ID="btnEditEventoPagamento" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditEventoPagamento" 
                                        ReadOnly="true" Width="430px" Text='<%# Eval("DescricaoEventoPagamento") %>' >                                                                                                               
                            <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                               <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoPagamento'; popupEvento.ShowAtElementByID(s.name);}" />                                                                   
                            </dxe:ASPxButtonEdit>
                        </td>                                                               
                        </tr>
                        
                    </div>
                                        
                    <tr>
                    <td colspan="4"><hr></td>
                    </tr>                        
                    <tr>
                    <td class="td_Label">
                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Data Renov:" />
                    </td>
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFimApropriacao" runat="server" ClientInstanceName="textDataFimApropriacao" />
                    </td>  
                        
                    <td class="td_Label">
                        <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Data Próx. Pgto:" /></td> 
                    <td>
                        <dxe:ASPxDateEdit ID="textDataPagamento" runat="server" ClientInstanceName="textDataPagamento" />                            
                    </td>                                                                        
                    </tr>
                    
                    <tr>
                    <td class="td_Label">
                        <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Data Ùltimo Pgto:" /></td> 
                    <td>
                        <dxe:ASPxDateEdit ID="textDataUltimoPagamento" runat="server" ClientInstanceName="textDataUltimoPagamento" />
                    </td>    
                    </tr>                        
                                            
                    </table>
                    
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                    
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal1" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal4" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                    
                    </div> 
                 
                 </asp:Panel>
                               
                </EditForm>                    
                </Templates>
                
                <SettingsPopup EditForm-Width="600" />                        
                
                <ClientSideEvents

                    BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }"

                    EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }"
                />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>

            </dxwgv:ASPxGridView>
            
        </div>                                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSTabelaTaxaPerformance" runat="server" OnesSelect="EsDSTabelaTaxaPerformance_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
    <cc1:esDataSource ID="EsDSEvento" runat="server" OnesSelect="EsDSEvento_esSelect" LowLevelBind="true" />
    
    </form>
</body>
</html>