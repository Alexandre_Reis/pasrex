﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security.Enums;
using Financial.Fundo;

using DevExpress.Web;

using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;

public partial class CadastrosBasicos_TabelaExpurgoAdministracao : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo}));
    }

    #region DataSources
    protected void EsDSTabelaExpurgoAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TabelaExpurgoAdministracaoQuery tabelaExpurgoAdmQuery = new TabelaExpurgoAdministracaoQuery("T");
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("Tta");
        CarteiraQuery carteira = new CarteiraQuery("C");
        //
        tabelaExpurgoAdmQuery.Select(tabelaExpurgoAdmQuery, carteira.IdCarteira, carteira.Apelido,
            tabelaTaxaAdministracaoQuery.DataReferencia.As("DataTabelaTaxaAdministracao")            
            //(carteira.IdCarteira.Cast(esCastType.String) + " - " + carteira.Apelido + tabelaTaxaAdministracaoQuery.DataReferencia.Cast(esCastType.String)
            //).As("DescricaoCompleta")
            );

        tabelaExpurgoAdmQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(tabelaExpurgoAdmQuery.IdTabela == tabelaTaxaAdministracaoQuery.IdTabela);
        tabelaExpurgoAdmQuery.InnerJoin(carteira).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteira.IdCarteira);
        //
        tabelaExpurgoAdmQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaExpurgoAdministracaoCollection coll = new TabelaExpurgoAdministracaoCollection();
        coll.Load(tabelaExpurgoAdmQuery);

        e.Collection = coll;
    }

    #endregion

    #region PopUp Taxa Administração
    /// <summary>
    /// PopUp Taxa Administração
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaAdm_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        //
        tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela,
                                            tabelaTaxaAdministracaoQuery.DataReferencia,
                                            carteiraQuery.Apelido);
        tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxaAdministracaoQuery.OrderBy(tabelaTaxaAdministracaoQuery.DataReferencia.Descending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();

        coll.Load(tabelaTaxaAdministracaoQuery);

        e.Collection = coll;        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idTabela = (int)gridView.GetRowValues(visibleIndex, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);

        //e.Result = idTabela.ToString() + "|" + " - " + descricao;
        e.Result = idTabela.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabelaAdm_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridTabelaAdm.DataBind();
    }

    #endregion   

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTabela = Convert.ToInt32(e.Parameter);

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            //
            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia,
                                                carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdTabela == idTabela);

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {                
                string idCarteira = Convert.ToString(tab.IdCarteira.Value);
                string apelido = Convert.ToString(tab.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                DateTime dataReferencia = tab.DataReferencia.Value;
                //
                texto = "Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");                        
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditTabelaAdm_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditTabelaAdm = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdm") as ASPxButtonEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditTabelaAdm, dropTipo, textDataReferencia});

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;

        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing) {
            TabelaExpurgoAdministracao tabelaExpurgoAdministracao = new TabelaExpurgoAdministracao();
            //            
            if (tabelaExpurgoAdministracao.LoadByPrimaryKey(Convert.ToDateTime(textDataReferencia.Text),
                                                            Convert.ToInt32(hiddenIdTabela.Text),
                                                            Convert.ToInt32(dropTipo.SelectedItem.Value),
                                                            Convert.ToString(textCodigo.Text)
                                                           )
                ) {
                e.Result = "Registro já existente.!";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        TabelaExpurgoAdministracao tabelaExpurgoAdministracao = new TabelaExpurgoAdministracao();

        ASPxButtonEdit btnEditTabelaAdm = gridCadastro.FindEditFormTemplateControl("btnEditTabelaAdm") as ASPxButtonEdit;
        ASPxTextBox hiddenIdTabela = gridCadastro.FindEditFormTemplateControl("hiddenIdTabela") as ASPxTextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;

        // Chaves
        int idTabela = Convert.ToInt32(hiddenIdTabela.Text);
        DateTime dataRef = Convert.ToDateTime(textDataReferencia.Text);
        int tipo = Convert.ToInt32(dropTipo.SelectedItem.Value);
        string codigo = Convert.ToString(textCodigo.Text).Trim();

        //
        tabelaExpurgoAdministracao.IdTabela = idTabela;
        tabelaExpurgoAdministracao.DataReferencia = dataRef;
        tabelaExpurgoAdministracao.Tipo = tipo;

        if (String.IsNullOrEmpty(codigo))
        {
            codigo = "0";
        }

        tabelaExpurgoAdministracao.Codigo = codigo;
        tabelaExpurgoAdministracao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaExpurgoAdministracao - Operacao: Insert TabelaExpurgoAdministracao: " + UtilitarioWeb.ToString(tabelaExpurgoAdministracao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {

            //Compose a primary key value
            int idTabela = Convert.ToInt32(e.GetListSourceFieldValue(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela));
            DateTime dataRef = Convert.ToDateTime(e.GetListSourceFieldValue(TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia));
            int tipo = Convert.ToInt32(e.GetListSourceFieldValue(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo));
            string codigo = Convert.ToString(e.GetListSourceFieldValue(TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo));
            //
            e.Value = idTabela + dataRef.ToString("d") + tipo + codigo;
        }

        else if (e.Column.FieldName == "DescricaoCompleta") {
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.IdCarteira));
            string apelido = Convert.ToString(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.Apelido));
            DateTime dataReferencia = Convert.ToDateTime(e.GetListSourceFieldValue("DataTabelaTaxaAdministracao"));
            //
            e.Value = "Carteira: " + idCarteira + " - " + apelido + " - Data Ref.: " + dataReferencia.ToString("d");
        }
        //else if (e.Column.FieldName == "TipoTraduzido") {
        //    int tipo = Convert.ToInt32(e.GetListSourceFieldValue(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo));
        //    e.Value = StringEnum.GetStringValue((TipoExpurgoAdm)tipo);
        //}
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {                     
    //    e.Cancel = true;
    //    gridCadastro.CancelEdit();
    //    gridCadastro.DataBind();
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdTabela = gridCadastro.GetSelectedFieldValues(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela);
            List<object> keyValuesDataRef = gridCadastro.GetSelectedFieldValues(TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesTipo = gridCadastro.GetSelectedFieldValues(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo);
            List<object> keyValuesCodigo = gridCadastro.GetSelectedFieldValues(TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo);

            for (int i = 0; i < keyValuesIdTabela.Count; i++) {
                int idTabela = Convert.ToInt32(keyValuesIdTabela[i]);
                DateTime dataRef = Convert.ToDateTime(keyValuesDataRef[i]);
                int tipo = Convert.ToInt32(keyValuesTipo[i]);
                string codigo = Convert.ToString(keyValuesCodigo[i]);

                TabelaExpurgoAdministracao tabelaExpurgoAdministracao = new TabelaExpurgoAdministracao();
                if (tabelaExpurgoAdministracao.LoadByPrimaryKey(dataRef, idTabela, tipo, codigo)) {
                    //
                    TabelaExpurgoAdministracao tabelaExpurgoAdministracaoClone = (TabelaExpurgoAdministracao)Utilitario.Clone(tabelaExpurgoAdministracao);
                    //

                    tabelaExpurgoAdministracao.MarkAsDeleted();
                    tabelaExpurgoAdministracao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaExpurgoAdministracao - Operacao: Delete TabelaExpurgoAdministracao: " + UtilitarioWeb.ToString(tabelaExpurgoAdministracaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTabelaAdm");
        base.gridCadastro_PreRender(sender, e);
    }
}