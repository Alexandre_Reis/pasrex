﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TemplateCarteira : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTemplateCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TemplateCarteiraQuery templateCarteiraQuery = new TemplateCarteiraQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        templateCarteiraQuery.Select(templateCarteiraQuery, carteiraQuery.Apelido.As("Apelido"));
        templateCarteiraQuery.InnerJoin(carteiraQuery).On(templateCarteiraQuery.IdCarteira == carteiraQuery.IdCarteira);

        templateCarteiraQuery.OrderBy(templateCarteiraQuery.Descricao.Ascending);

        TemplateCarteiraCollection coll = new TemplateCarteiraCollection();
        coll.Load(templateCarteiraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTotal(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            TemplateCarteira templateCarteira = new TemplateCarteira();
            if (templateCarteira.LoadByPrimaryKey(idCarteira)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TemplateCarteira templateCarteira = new TemplateCarteira();

        templateCarteira.Descricao = Convert.ToString(textDescricao.Text);
        templateCarteira.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        templateCarteira.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TemplateCarteira - Operacao: Insert TemplateCarteira: " + templateCarteira.IdCarteira + UtilitarioWeb.ToString(templateCarteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idCarteira = (int)e.Keys[0];

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TemplateCarteira templateCarteira = new TemplateCarteira();

        if (templateCarteira.LoadByPrimaryKey(idCarteira)) {
            templateCarteira.Descricao = Convert.ToString(textDescricao.Text);
            templateCarteira.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TemplateCarteira - Operacao: Update TemplateCarteira: " + idCarteira + UtilitarioWeb.ToString(templateCarteira),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteira");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);

                TemplateCarteira templateCarteira = new TemplateCarteira();
                if (templateCarteira.LoadByPrimaryKey(idCarteira)) {

                    TemplateCarteira templateCarteiraClone = (TemplateCarteira)Utilitario.Clone(templateCarteira);
                    //

                    templateCarteira.MarkAsDeleted();
                    templateCarteira.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TemplateCarteira - Operacao: Delete TemplateCarteira: " + idCarteira + UtilitarioWeb.ToString(templateCarteiraClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;

            //TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
            //e.Properties["cpTextDescricao"] = textDescricao.ClientID;
        }
    }
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira", "textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
}