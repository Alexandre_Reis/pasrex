﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using DevExpress.Web;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_PortfolioPadrao : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        this.HasPopupCarteira1 = true;
        this.HasPopupCarteira2 = true;
        this.HasPanelFieldsLoading = true;

        base.Page_Load(sender, e);
    }

    protected void EsDSPortfolioPadrao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PortfolioPadraoCollection coll = new PortfolioPadraoCollection();

        PortfolioPadraoQuery portofolioPadraoQuery = new PortfolioPadraoQuery("portfolio");
        CarteiraQuery cartTaxaGestaoQuery = new CarteiraQuery("cartTaxaGestao");
        CarteiraQuery cartTaxaPerformanceQuery = new CarteiraQuery("cartTaxaPerformance");
        CarteiraQuery cartDespesasReceitasQuery = new CarteiraQuery("cartDespesasReceitas");

        portofolioPadraoQuery.Select(portofolioPadraoQuery,
                                     cartTaxaGestaoQuery.Apelido.As("CarteiraTaxaGestao"),
                                     cartTaxaPerformanceQuery.Apelido.As("CarteiraTaxaPerformance"),
                                     cartDespesasReceitasQuery.Apelido.As("CarteiraDespesasReceitas"));
        portofolioPadraoQuery.InnerJoin(cartTaxaGestaoQuery).On(portofolioPadraoQuery.IdCarteiraTaxaGestao.Equal(cartTaxaGestaoQuery.IdCarteira));
        portofolioPadraoQuery.InnerJoin(cartTaxaPerformanceQuery).On(portofolioPadraoQuery.IdCarteiraTaxaPerformance.Equal(cartTaxaPerformanceQuery.IdCarteira));
        portofolioPadraoQuery.InnerJoin(cartDespesasReceitasQuery).On(portofolioPadraoQuery.IdCarteiraDespesasReceitas.Equal(cartDespesasReceitasQuery.IdCarteira));


        coll.Load(portofolioPadraoQuery);

        e.Collection = coll;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.LoadAll();

        e.Collection = coll;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxSpinEdit btnEditTaxaGestao = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditTaxaPerformance = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira1") as ASPxSpinEdit;
        ASPxSpinEdit btnEditDespesasReceitas = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira2") as ASPxSpinEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;

        PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
        int idPortfolioPadrao = (int)e.Keys[0];

        if (portfolioPadrao.LoadByPrimaryKey(idPortfolioPadrao))
        {
            portfolioPadrao.IdCarteiraDespesasReceitas = Convert.ToInt32(btnEditDespesasReceitas.Text);
            portfolioPadrao.IdCarteiraTaxaGestao = Convert.ToInt32(btnEditTaxaGestao.Text);
            portfolioPadrao.IdCarteiraTaxaPerformance = Convert.ToInt32(btnEditTaxaPerformance.Text);
            portfolioPadrao.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);

            portfolioPadrao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PortfolioPadrao - Operacao: Update PortfolioPadrao: " + idPortfolioPadrao  + UtilitarioWeb.ToString(portfolioPadrao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();        

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(PortfolioPadraoMetadata.ColumnNames.IdPortfolioPadrao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPortfolioPadrao = Convert.ToInt32(keyValuesId[i]);

                PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
                if (portfolioPadrao.LoadByPrimaryKey(idPortfolioPadrao))
                {
                    PortfolioPadrao portfolioPadraoClone = (PortfolioPadrao)Utilitario.Clone(portfolioPadrao);
                    //
                    portfolioPadrao.MarkAsDeleted();
                    //
                    using (esTransactionScope scope = new esTransactionScope())
                    {
                        portfolioPadrao.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de PortfolioPadrao - Operacao: Delete PortfolioPadrao: " + idPortfolioPadrao + UtilitarioWeb.ToString(portfolioPadraoClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                        //
                        scope.Complete();
                    }
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        Carteira carteira = new Carteira();

        string resultado = "";
        DateTime? dataDia = null;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        resultado = carteira.str.Apelido;
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.SalvarNovo();
        
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira1 = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira1") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira2 = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira2") as ASPxSpinEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(btnEditCodigoCarteira1);
        controles.Add(btnEditCodigoCarteira2);
        controles.Add(textDataVigencia);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion        
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditTaxaGestao = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditTaxaPerformance = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira1") as ASPxSpinEdit;
        ASPxSpinEdit btnEditDespesasReceitas = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira2") as ASPxSpinEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;

        PortfolioPadrao portfolioPadrao = new PortfolioPadrao();

        portfolioPadrao.IdCarteiraDespesasReceitas = Convert.ToInt32(btnEditDespesasReceitas.Text);
        portfolioPadrao.IdCarteiraTaxaGestao = Convert.ToInt32(btnEditTaxaGestao.Text);
        portfolioPadrao.IdCarteiraTaxaPerformance = Convert.ToInt32(btnEditTaxaPerformance.Text);
        portfolioPadrao.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);

        portfolioPadrao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PortfolioPadrao - Operacao: Insert PortfolioPadrao: " + portfolioPadrao.IdPortfolioPadrao + UtilitarioWeb.ToString(portfolioPadrao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

}
