using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Tributo;
using Financial.Fundo;

public partial class CadastrosBasicos_TipoInvestidorCVM : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasEditControl = false;
        base.Page_Load(sender, e);
    }

    protected void EsDSTipoInvestidorCVM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoInvestidorCVMCollection coll = new TipoInvestidorCVMCollection();

        coll.Query.OrderBy(coll.Query.TipoCVM.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

}

