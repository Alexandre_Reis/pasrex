﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;

public partial class CadastrosBasicos_ParametroDistribuicaoCotas : CadastroBasePage 
{
    #region DataSources
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        coll.Query.Select(coll.Query.IdCarteira,
                          coll.Query.Nome,
                          coll.Query.ApenasInvestidorProfissional,
                          coll.Query.ApenasInvestidorQualificado,
                          coll.Query.RealizaOfertaSubscricao);
        coll.Query.Where(coll.Query.TipoFundo.Equal(TipoFundo.Fechado));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Carteira carteira = new Carteira();
        int idCarteira = (int)e.Keys[0];

        if (carteira.LoadByPrimaryKey(idCarteira)) {
            carteira.ApenasInvestidorProfissional = Convert.ToString(e.NewValues[CarteiraMetadata.ColumnNames.ApenasInvestidorProfissional]);
            carteira.ApenasInvestidorQualificado = Convert.ToString(e.NewValues[CarteiraMetadata.ColumnNames.ApenasInvestidorQualificado]);
            carteira.RealizaOfertaSubscricao = Convert.ToString(e.NewValues[CarteiraMetadata.ColumnNames.RealizaOfertaSubscricao]);

            carteira.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Parametro Distribuicao Cotas - Operacao: Update Carteira: " + idCarteira + UtilitarioWeb.ToString(carteira),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}