﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Interfaces.Import.RendaFixa;
using DevExpress.Web.Data;
using Financial.Captacao;
using Financial.Common.Enums;

public partial class CadastrosBasicos_Carteira : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { CarteiraMetadata.ColumnNames.StatusAtivo }));

        string buscaFundoEnterEvent = "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnBuscaFundo.UniqueID.Replace("$", "_") + "').onclick();return false;}} else {return true}; ";

        textBuscaFundoNomeFantasia.Attributes.Add("onkeydown", buscaFundoEnterEvent);
        textBuscaFundoCodigoAnbid.Attributes.Add("onkeydown", buscaFundoEnterEvent);
        textBuscaFundoCNPJ.Attributes.Add("onkeydown", buscaFundoEnterEvent);

        if (!this.IsPostBack)
        {
            bool idAutomatico = false;
            if (ParametrosConfiguracaoSistema.Outras.IdAutomaticoPessoa != null)
                idAutomatico = ParametrosConfiguracaoSistema.Outras.IdAutomaticoPessoa.Equals("S");

            StringBuilder jsMaxID = new StringBuilder();
            jsMaxID.Append("<script type=\"text/javascript\">  ");
            jsMaxID.Append(" var utilizaMaxID = " + idAutomatico.ToString().ToLower() + "; ");
            jsMaxID.Append("</script>");
            RegisterClientScriptBlock("maxID", jsMaxID.ToString());
        }

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClassesOffShoreQuery classesOffShoreQuery = new ClassesOffShoreQuery("CO");
        CarteiraComplementoQuery carteiraComplementoQuery = new CarteiraComplementoQuery("CC");
        PerfilClassificacaoFundoCarteiraQuery perfilClassificacaoFundoCarteiraQuery = new PerfilClassificacaoFundoCarteiraQuery("PC");
        PerfilClassificacaoFundoQuery perfilClassificacaoFundoQuery = new PerfilClassificacaoFundoQuery("PCF");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, classesOffShoreQuery, carteiraComplementoQuery, perfilClassificacaoFundoQuery.Codigo.As("CodigoPerfil"), perfilClassificacaoFundoQuery.Descricao.As("DescricaoPerfil"));
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.LeftJoin(classesOffShoreQuery).On(carteiraQuery.IdCarteira.Equal(classesOffShoreQuery.IdClassesOffShore));
        carteiraQuery.LeftJoin(carteiraComplementoQuery).On(carteiraQuery.IdCarteira.Equal(carteiraComplementoQuery.IdCarteira));

        carteiraQuery.LeftJoin(perfilClassificacaoFundoCarteiraQuery).On(carteiraQuery.IdCarteira.Equal(perfilClassificacaoFundoCarteiraQuery.IdCarteira));
        carteiraQuery.LeftJoin(perfilClassificacaoFundoQuery).On(perfilClassificacaoFundoQuery.IdPerfil == perfilClassificacaoFundoCarteiraQuery.IdPerfilClassificacao);

        carteiraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("L");
        classesOffShoreQuery = new ClassesOffShoreQuery("CO");
        carteiraComplementoQuery = new CarteiraComplementoQuery("CC");
        carteiraComplementoQuery = new CarteiraComplementoQuery("CC");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, classesOffShoreQuery, carteiraComplementoQuery);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.LeftJoin(classesOffShoreQuery).On(carteiraQuery.IdCarteira.Equal(classesOffShoreQuery.IdClassesOffShore));
        carteiraQuery.LeftJoin(carteiraComplementoQuery).On(carteiraQuery.IdCarteira.Equal(carteiraComplementoQuery.IdCarteira));
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll2 = new CarteiraCollection();
        coll2.Load(carteiraQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaFundoCollection coll = new CategoriaFundoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSubCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SubCategoriaFundoCollection coll = new SubCategoriaFundoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoEconomico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoEconomicoCollection coll = new GrupoEconomicoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        //
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        clienteQuery.Where(usuarioQuery.Login == Context.User.Identity.Name);
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        coll.Load(clienteQuery);


        //Query sem vinculo de permissionamento (fundos somente Cotação)
        clienteQuery = new ClienteQuery("C");
        usuarioQuery = new UsuarioQuery("U");

        clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSeries_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SeriesOffShoreCollection seriesOffShoreCollection = new SeriesOffShoreCollection();

        SeriesOffShoreQuery seriesOffShoreQuery = new SeriesOffShoreQuery("SOS");
        ClassesOffShoreQuery classesOffShoreQuery = new ClassesOffShoreQuery("COS");

        int idClassesOffShore = 0;

        if (!gridCadastro.IsNewRowEditing)
        {
            idClassesOffShore = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, CarteiraMetadata.ColumnNames.IdCarteira));
        }
        else
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
            if (btnEditCodigoCliente.Text != "") idClassesOffShore = Convert.ToInt32(btnEditCodigoCliente.Text);

        }

        seriesOffShoreQuery.Select(seriesOffShoreQuery, classesOffShoreQuery.Nome);
        seriesOffShoreQuery.InnerJoin(classesOffShoreQuery).On(seriesOffShoreQuery.IdClassesOffShore.Equal(classesOffShoreQuery.IdClassesOffShore));
        seriesOffShoreCollection.Query.Where(seriesOffShoreCollection.Query.IdClassesOffShore.Equal(idClassesOffShore));
        seriesOffShoreCollection.Query.Load();

        e.Collection = seriesOffShoreCollection;
    }

    protected void EsDSMoedas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection moedaCollection = new MoedaCollection();

        moedaCollection.Query.Load();

        e.Collection = moedaCollection;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection localNegociacaoCollection = new LocalNegociacaoCollection();

        localNegociacaoCollection.Query.Load();

        e.Collection = localNegociacaoCollection;
    }

    protected void EsDSGrupoPerfilMTM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoPerfilMTMCollection grupoPerfilMTMCollection = new GrupoPerfilMTMCollection();
        GrupoPerfilMTMQuery grupoPerfilMTMQuery = new GrupoPerfilMTMQuery("grupo");

        StringBuilder descricaoField = new StringBuilder();
        descricaoField.Append("< cast(grupo.[IdGrupoPerfilMTM] as varchar(20)) + ' - ' + grupo.descricao as Descricao >");

        grupoPerfilMTMQuery.Select(descricaoField.ToString(), grupoPerfilMTMQuery.IdGrupoPerfilMTM);
        grupoPerfilMTMQuery.OrderBy(grupoPerfilMTMQuery.IdGrupoPerfilMTM.Ascending);

        grupoPerfilMTMCollection.Load(grupoPerfilMTMQuery);

        e.Collection = grupoPerfilMTMCollection;
    }

    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EstrategiaCollection coll = new EstrategiaCollection();
        EstrategiaQuery estrategiaQuery = new EstrategiaQuery("estrategia");

        StringBuilder descricaoField = new StringBuilder();
        descricaoField.Append("< cast(estrategia.[IdEstrategia] as varchar(20)) + ' - ' + estrategia.descricao as Descricao >");

        estrategiaQuery.Select(descricaoField.ToString(), estrategiaQuery.IdEstrategia);
        estrategiaQuery.OrderBy(estrategiaQuery.IdEstrategia.Ascending);
        coll.Load(estrategiaQuery);

        e.Collection = coll;
    }

    protected void EsDSClasseCota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClasseCotaCollection classeCotaCollection = new ClasseCotaCollection();

        classeCotaCollection.Query.Load();

        e.Collection = classeCotaCollection;
    }

    protected void EsDSPerfilRisco_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();
        coll.Query.Where(coll.Query.Nivel > 0);
        coll.Query.OrderBy(coll.Query.Perfil.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraSuitabilityHistoricoCollection coll = new CarteiraSuitabilityHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropDistribuicaoDividendo_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        if (pageControl != null)
        {
            ASPxComboBox dropDistribuicaoDividendo = pageControl.FindControl("dropDistribuicaoDividendo") as ASPxComboBox;
            if (dropDistribuicaoDividendo != null)
            {
                dropDistribuicaoDividendo.Items.Clear();
                foreach (int i in Enum.GetValues(typeof(DistribuicaoDividendosCarteira)))
                {
                    dropDistribuicaoDividendo.Items.Add(DistribuicaoDividendosCarteiraDescricao.RetornaStringValue(i), i);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridBuscaFundo_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        int visibleIndex = Convert.ToInt32(e.Parameters);
        string fieldName = string.Empty;
        fieldName = ((GridViewDataColumn)gridBuscaFundo.VisibleColumns[0]).FieldName;
        string codFundo = (string)gridBuscaFundo.GetRowValues(visibleIndex, fieldName);

        Financial.Interfaces.Import.RendaFixa.SIAnbid.Fundo fundo = new Financial.Interfaces.Import.RendaFixa.SIAnbid.Fundo();
        fundo.LoadByCodFundo(codFundo, null, false);

        string fundoJSON = Newtonsoft.Json.JsonConvert.SerializeObject(fundo);

        e.Result = fundoJSON;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridBuscaFundo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridBuscaFundo.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridBuscaFundo_DataBinding(object sender, EventArgs e)
    {
        Financial.Interfaces.Import.RendaFixa.SIAnbid sianbid = new Financial.Interfaces.Import.RendaFixa.SIAnbid();
        List<Financial.Interfaces.Import.RendaFixa.SIAnbid.Fundo> fundos = sianbid.BuscaFundos(Utilitario.RemoveCaracteresEspeciais(textBuscaFundoCNPJ.Text), textBuscaFundoNomeFantasia.Text, textBuscaFundoCodigoAnbid.Text);
        gridBuscaFundo.DataSource = fundos;
    }

    protected void dropCalculaMTM_Load(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxComboBox dropCalculaMTM = pageControl.FindControl("dropCalculaMTM") as ASPxComboBox;
        if (dropCalculaMTM != null)
        {
            dropCalculaMTM.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Common.Enums.ExecutaFuncao)))
            {
                dropCalculaMTM.Items.Add(Financial.Common.Enums.ExecutaFuncaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackBuscaFundo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
    }

    protected string validaGridCadastro()
    {
        string resultado = "";

        #region campos
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropTipoCota = pageControl.FindControl("dropTipoCota") as ASPxComboBox;
        ASPxComboBox dropTruncaCota = pageControl.FindControl("dropTruncaCota") as ASPxComboBox;
        ASPxComboBox dropTipoCarteira = pageControl.FindControl("dropTipoCarteira") as ASPxComboBox;
        ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropBuscaCotaAnterior = pageControl.FindControl("dropBuscaCotaAnterior") as ASPxComboBox;
        ASPxComboBox dropProcCalculoRF = pageControl.FindControl("dropProcCalculoRF") as ASPxComboBox;
        ASPxComboBox dropIndiceBenchmark = pageControl.FindControl("dropIndiceBenchmark") as ASPxComboBox;
        ASPxComboBox dropTruncaFinanceiro = pageControl.FindControl("dropTruncaFinanceiro") as ASPxComboBox;
        ASPxComboBox dropTruncaQuantidade = pageControl.FindControl("dropTruncaQuantidade") as ASPxComboBox;
        ASPxDateEdit textDataInicio = pageControl.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxSpinEdit textCotaInicial = pageControl.FindControl("textCotaInicial") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisCota = pageControl.FindControl("textCasasDecimaisCota") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisQuantidade = pageControl.FindControl("textCasasDecimaisQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropCategoriaFundo = pageControl.FindControl("dropCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropSubCategoriaFundo = pageControl.FindControl("dropSubCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropAgenteAdministrador = pageControl.FindControl("dropAgenteAdministrador") as ASPxComboBox;
        ASPxComboBox dropAgenteGestor = pageControl.FindControl("dropAgenteGestor") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = pageControl.FindControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoCusto = pageControl.FindControl("dropTipoCusto") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = pageControl.FindControl("dropTipoTributacao") as ASPxComboBox;
        ASPxComboBox dropCompensacaoPrejuizo = pageControl.FindControl("dropCompensacaoPrejuizo") as ASPxComboBox;
        ASPxComboBox dropCalculaIOF = pageControl.FindControl("dropCalculaIOF") as ASPxComboBox;
        ASPxSpinEdit textCodigoAnbid = pageControl.FindControl("textCodigoAnbid") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoAplicacao = pageControl.FindControl("textDiasCotizacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoResgate = pageControl.FindControl("textDiasCotizacaoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoAplicacao = pageControl.FindControl("textDiasLiquidacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoResgate = pageControl.FindControl("textDiasLiquidacaoResgate") as ASPxSpinEdit;
        ASPxComboBox dropCobraTaxaFiscalizacaoCVM = pageControl.FindControl("dropCobraTaxaFiscalizacaoCVM") as ASPxComboBox;
        ASPxComboBox dropExplodeCotaDeFundo = pageControl.FindControl("dropExplodeCotaDeFundo") as ASPxComboBox;
        ASPxComboBox dropCalculaEnquadra = pageControl.FindControl("dropCalculaEnquadra") as ASPxComboBox;
        ASPxComboBox dropContagemDiasCotizacaoResgate = pageControl.FindControl("dropContagemDiasCotizacaoResgate") as ASPxComboBox;
        ASPxComboBox dropDistribuicaoDividendo = pageControl.FindControl("dropDistribuicaoDividendo") as ASPxComboBox;
        ASPxComboBox dropPrioridadeOperacao = pageControl.FindControl("dropPrioridadeOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoCalculoRetorno = pageControl.FindControl("dropTipoCalculoRetorno") as ASPxComboBox;
        ASPxComboBox dropLiberaAplicacao = pageControl.FindControl("dropLiberaAplicacao") as ASPxComboBox;
        ASPxComboBox dropLiberaResgate = pageControl.FindControl("dropLiberaResgate") as ASPxComboBox;
        ASPxComboBox dropPossuiResgateAutomatico = pageControl.FindControl("dropPossuiResgateAutomatico") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIR = pageControl.FindControl("textDiasAposResgateIR") as ASPxSpinEdit;
        ASPxSpinEdit textDiasAposComeCotasIR = pageControl.FindControl("textDiasAposComeCotasIR") as ASPxSpinEdit;
        ASPxComboBox dropCorretora = pageControl.FindControl("dropCorretora") as ASPxComboBox;

        ASPxComboBox dropProjecaoIRResgate = pageControl.FindControl("dropProjecaoIRResgate") as ASPxComboBox;
        ASPxComboBox dropProjecaoIRComeCotas = pageControl.FindControl("dropProjecaoIRComeCotas") as ASPxComboBox;
        ASPxComboBox dropRealizaCompensacao = pageControl.FindControl("dropRealizaCompensacao") as ASPxComboBox;
        ASPxTextBox textNomeClasse = pageControl.FindControl("textNomeClasse") as ASPxTextBox;
        ASPxDateEdit textDataInicioClasse = pageControl.FindControl("textDataInicioClasse") as ASPxDateEdit;
        ASPxComboBox dropFrequencia = pageControl.FindControl("dropFrequencia") as ASPxComboBox;
        ASPxSpinEdit textLockUp = pageControl.FindControl("textLockUp") as ASPxSpinEdit;
        ASPxComboBox dropHardSoft = pageControl.FindControl("dropHardSoft") as ASPxComboBox;
        ASPxSpinEdit textHoldBack = pageControl.FindControl("textHoldBack") as ASPxSpinEdit;
        ASPxDateEdit textAnivHoldBack = pageControl.FindControl("textAnivHoldBack") as ASPxDateEdit;
        ASPxComboBox dropTipoVisaoFundo = pageControl.FindControl("dropTipoVisaoFundo") as ASPxComboBox;
        ASPxComboBox dropContagemPrazoIOF = pageControl.FindControl("dropContagemPrazoIOF") as ASPxComboBox;
        ASPxComboBox dropDataInicioContIOF = pageControl.FindControl("dropDataInicioContIOF") as ASPxComboBox;
        ASPxComboBox dropDataFimContIOF = pageControl.FindControl("dropDataFimContIOF") as ASPxComboBox;
        ASPxComboBox dropProjecaoIOFResgate = pageControl.FindControl("dropProjecaoIOFResgate") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIOF = pageControl.FindControl("textDiasAposResgateIOF") as ASPxSpinEdit;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropCalculaMTM = pageControl.FindControl("dropCalculaMTM") as ASPxComboBox;
        ASPxComboBox dropGrupoPerfilMTM = pageControl.FindControl("dropGrupoPerfilMTM") as ASPxComboBox;
        ASPxComboBox dropTipoVisualizacaoResgCotista = pageControl.FindControl("dropTipoVisualizacaoResgCotista") as ASPxComboBox;
        #endregion

        bool insert = gridCadastro.IsNewRowEditing;
        bool update = !gridCadastro.IsNewRowEditing;

        #region Se Cliente Escolhido não é Clube/FDIC ou Fundo SobrePoe valores obrigatórios da 3 aba
        if (btnEditCodigoCliente.Text == "")
        {
            return "Favor informar o cliente";
        }

        Cliente clienteAux = new Cliente();
        clienteAux.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));

        bool isCliente = clienteAux.IdTipo.Value == TipoClienteFixo.Clube ||
                         clienteAux.IdTipo.Value == TipoClienteFixo.FDIC ||
                         clienteAux.IdTipo.Value == TipoClienteFixo.Fundo;
        #endregion


        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        if (update && isCliente || (insert && isCliente))
        { // Update
            controles = new List<Control>(new Control[] { 
                dropTipoCarteira, 
                dropStatusAtivo, 
                dropBuscaCotaAnterior,
                dropProcCalculoRF,
                dropIndiceBenchmark, 
                textDataInicio, 
                dropCategoriaFundo, 
                dropSubCategoriaFundo, 
                dropAgenteAdministrador, 
                dropAgenteGestor,
                dropTipoRentabilidade, 
                dropTipoCusto, 
                dropTipoTributacao, 
                dropCalculaIOF,
                textDiasCotizacaoAplicacao, 
                textDiasCotizacaoResgate, 
                textDiasLiquidacaoAplicacao, 
                textDiasLiquidacaoResgate,  
                dropCobraTaxaFiscalizacaoCVM, 
                dropCalculaEnquadra,       
                dropContagemDiasCotizacaoResgate,  
                dropDistribuicaoDividendo, 
                dropEstrategia,
                dropTipoVisaoFundo,
                dropPrioridadeOperacao,
                dropCompensacaoPrejuizo, 
                dropTipoCalculoRetorno,
                dropRealizaCompensacao,
                dropLiberaAplicacao,
                dropLiberaResgate,
                dropPossuiResgateAutomatico,
                dropExplodeCotaDeFundo,
                dropTipoVisualizacaoResgCotista,
                dropCalculaMTM,
                dropCorretora
            });

            if (dropProjecaoIRResgate != null &&
                dropProjecaoIRResgate.SelectedItem != null &&
                Convert.ToInt32(dropProjecaoIRResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                controles.Add(textDiasAposResgateIR);
            }

            if (dropProjecaoIRComeCotas != null &&
                dropProjecaoIRComeCotas.SelectedItem != null &&
                Convert.ToInt32(dropProjecaoIRComeCotas.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                controles.Add(textDiasAposComeCotasIR);
            }

            if (dropProjecaoIOFResgate != null &&
                dropProjecaoIOFResgate.SelectedItem != null &&
                Convert.ToInt32(dropProjecaoIOFResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                controles.Add(textDiasAposResgateIOF);
            }

            if (dropContagemPrazoIOF != null &&
                dropContagemPrazoIOF.SelectedItem != null &&
                Convert.ToInt32(dropContagemPrazoIOF.SelectedItem.Value) == (byte)ContagemDiasPrazoIOF.Personalizado)
            {
                controles.Add(dropDataInicioContIOF);
                controles.Add(dropDataFimContIOF);
            }

            if (dropCalculaMTM.SelectedItem != null)
                if (Convert.ToInt32(dropCalculaMTM.SelectedItem.Value) == (int)ExecutaFuncao.Sim)
                    controles.Add(dropGrupoPerfilMTM);

            if (base.TestaObrigatorio(controles) != "")
            {
                return "Campos com * são obrigatórios!";
            }
        }

        if (insert && !isCliente)
        {
            controles = new List<Control>(new Control[] { 
                dropTipoCarteira, 
                dropStatusAtivo, 
                dropBuscaCotaAnterior,
                dropProcCalculoRF,
                dropIndiceBenchmark, 
                textDataInicio, 
                dropCategoriaFundo, 
                dropSubCategoriaFundo, 
                dropAgenteAdministrador, 
                dropAgenteGestor,
                dropTipoRentabilidade,                                                                                                 
                dropCobraTaxaFiscalizacaoCVM, 
                dropCalculaEnquadra,                                                                       
                dropTipoCalculoRetorno, 
                dropCalculaMTM,
                dropExplodeCotaDeFundo,
                dropCorretora
            });

            if (dropCalculaMTM.SelectedItem != null)
                if (Convert.ToInt32(dropCalculaMTM.SelectedItem.Value) == (int)ExecutaFuncao.Sim)
                    controles.Add(dropGrupoPerfilMTM);

            if (base.TestaObrigatorio(controles) != "")
            {
                return "Campos com * são obrigatórios!";
            }
        }
        #endregion

        if (insert)
        {
            #region Especificos Inserts
            bool codigoClientePreenchido = (base.TestaObrigatorio(new List<Control>(new Control[] { btnEditCodigoCliente })) == "");
            bool codigoAnbidPreenchido = (base.TestaObrigatorio(new List<Control>(new Control[] { textCodigoAnbid })) == "");

            if (!codigoClientePreenchido && !codigoAnbidPreenchido)
            {
                return "Por favor informe um cliente ou, no caso de um fundo, informe o código Anbima dele.";
            }
            else if (codigoClientePreenchido && codigoAnbidPreenchido)
            {
                return "O código Anbima do fundo foi informado. Neste caso não deve ser informado o código do cliente uma vez que este será cadastrado automaticamente.";
            }

            int id = codigoClientePreenchido ? Convert.ToInt32(btnEditCodigoCliente.Text) : Convert.ToInt32(textCodigoAnbid.Text);

            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(id))
            {
                return "Registro já existente";
            }

            if (codigoAnbidPreenchido)
            {
                Pessoa pessoa = new Pessoa();
                if (pessoa.LoadByPrimaryKey(id))
                {
                    return "Registro já existente";
                }
            }
            #endregion
        }

        if (Convert.ToByte(dropTipoCalculoRetorno.SelectedItem.Value) == (byte)CalculoRetornoCarteira.TIR)
        {
            #region TIR
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));

            if (cliente.IdTipo.Value == TipoClienteFixo.Clube || cliente.IdTipo.Value == TipoClienteFixo.FDIC || cliente.IdTipo.Value == TipoClienteFixo.Fundo)
            {
                return "Fundos e clubes devem ser associados ao Cálculo de Retorno 'Por Cota'";
            }
            #endregion
        }
        else
        {
            #region Não TIR
            controles = new List<Control>(new Control[] { 
            dropTipoCota, dropTruncaCota, dropTruncaFinanceiro, dropTruncaQuantidade,
            textCasasDecimaisCota, textCasasDecimaisQuantidade, dropCalculaMTM,
            textCotaInicial, textCasasDecimaisCota, textCasasDecimaisQuantidade, dropExplodeCotaDeFundo, dropCorretora });

            if (dropCalculaMTM.SelectedItem != null)
                if (Convert.ToInt32(dropCalculaMTM.SelectedItem.Value) == (int)ExecutaFuncao.Sim)
                    controles.Add(dropGrupoPerfilMTM);

            if (clienteAux.IdTipo.Value != TipoClienteFixo.OffShore_PF && clienteAux.IdTipo.Value != TipoClienteFixo.OffShore_PJ)
                controles.Add(textCotaInicial);

            if (base.TestaObrigatorio(controles) != "")
            {
                return "Campos com * são obrigatórios!";
            }
            #endregion
        }

        #region Valida Classes Off Shore
        if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ)
        {
            controles = new List<Control>(new Control[] { textNomeClasse, textDataInicioClasse, dropFrequencia });

            if (base.TestaObrigatorio(controles) != "")
            {
                return "Campos com * são obrigatórios!";
            }

            if (textLockUp.Text != "" && Convert.ToInt32(textLockUp.Text) > 0 && dropHardSoft.Text == "") return "Você informou valor em carência de resgate. Favor indentificar se deverá ser Hard ou Soft.";

            if (textHoldBack.Text != "" && Convert.ToDecimal(textHoldBack.Text) > 0 && textAnivHoldBack.Text == "") return "Favor informar o Aniversário Hold Back.";
        }
        #endregion

        return resultado;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        e.Result = this.validaGridCadastro();

    }

    /// <summary>
    /// Retorna o apelido + boleano indicando se cliente = Clube, FDIC ou Fundo + Indicador de Fundo OffShore
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxLabel hiddenTipoFundoAtual = pageControl.FindControl("hiddenTipoFundoAtual") as ASPxLabel;
        ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;

        //string tipoFundo = TipoFundoDescricao.RetornaStringValue(RetornaCondominioCarteira(Convert.ToInt32(btnEditCodigoCliente.Text)));
        //string tipoTributacao = ClassificacaoTributariaDescricao.RetornaStringValue(RetornaTributacaoCarteira(Convert.ToInt32(btnEditCodigoCliente.Text), Convert.ToDateTime(ParametrosConfiguracaoSistema.Outras.DataBase)));

        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                    {

                        string tipoCliente = "";

                        if (cliente.IdTipo.Value == TipoClienteFixo.Clube ||
                           cliente.IdTipo.Value == TipoClienteFixo.FDIC ||
                           cliente.IdTipo.Value == TipoClienteFixo.Fundo) tipoCliente = "Clube";

                        if (cliente.IdTipo.Value == TipoClienteFixo.OffShore_PF || cliente.IdTipo.Value == TipoClienteFixo.OffShore_PJ)
                            tipoCliente = "OffShore";

                        nome = cliente.str.Apelido + "|" + tipoCliente + "|" + cliente.AmortizacaoRendimentoJuros;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);

            Carteira carteira = new Carteira();

            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                string apelido = Convert.ToString(carteira.Apelido);
                string apenasInvestidorProfissional = carteira.ApenasInvestidorProfissional;
                string apenasInvestidorQualificado = carteira.ApenasInvestidorQualificado;
                texto = apelido + "|" + apenasInvestidorProfissional + "|" + apenasInvestidorQualificado;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// Faz Update das Carteiras Selecionadas em Lote
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        bool ok = true;

        List<object> carteiraSelecionadas = gridCadastro.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);

        if (carteiraSelecionadas.Count == 0)
        {
            e.Result = "Selecione uma ou mais Carteiras.";
        }
        else
        {
            #region Se tem Carteira Selecionada

            if (this.dataInicioCota.Text == "" &&
                this.dropTruncaCota.SelectedIndex == 0 && this.dropTruncaQuantidade.SelectedIndex == 0 &&
                this.casasDecimaisCota.Text == "" && this.casasDecimaisQuantidade.Text == "" && !this.chkResetData.Checked &&
                this.dropIndiceBenchmarkLote.SelectedIndex == -1 &&
                this.dropCategoriaFundoLote.SelectedIndex == -1 &&
                this.dropSubCategoriaFundoLote.SelectedIndex == -1 &&
                this.dropAgenteAdministradorLote.SelectedIndex == -1 &&
                this.dropAgenteGestorLote.SelectedIndex == -1)
            {

                e.Result = "Preencha algum campo.";
                return;
            }

            for (int i = 0; i < carteiraSelecionadas.Count; i++)
            {
                int idCarteira = Convert.ToInt32(carteiraSelecionadas[i]);

                Carteira carteira = new Carteira();
                //
                if (carteira.LoadByPrimaryKey(idCarteira))
                {
                    if (dataInicioCota.Text != "")
                    {
                        carteira.DataInicioCota = Convert.ToDateTime(dataInicioCota.Text);
                    }
                    if (dropTruncaCota.SelectedIndex >= 1)
                    {
                        carteira.TruncaCota = Convert.ToString(dropTruncaCota.SelectedItem.Value);
                    }
                    if (dropTruncaQuantidade.SelectedIndex >= 1)
                    {
                        carteira.TruncaQuantidade = Convert.ToString(dropTruncaQuantidade.SelectedItem.Value);
                    }
                    if (casasDecimaisCota.Text != "")
                    {
                        carteira.CasasDecimaisCota = Convert.ToByte(casasDecimaisCota.Text);
                    }
                    if (casasDecimaisQuantidade.Text != "")
                    {
                        carteira.CasasDecimaisQuantidade = Convert.ToByte(casasDecimaisQuantidade.Text);
                    }
                    if (chkResetData.Checked)
                    {
                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.Query.Select(historicoCota.Query.Data.Min());
                        historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira));
                        historicoCota.Query.Load();

                        if (historicoCota.Data.HasValue)
                        {
                            carteira.DataInicioCota = historicoCota.Data.Value;
                        }
                    }

                    /*---------------------------------------------------------------------------------------------------*/

                    if (this.dropIndiceBenchmarkLote.SelectedIndex >= 0)
                    {
                        carteira.IdIndiceBenchmark = Convert.ToInt16(this.dropIndiceBenchmarkLote.SelectedItem.Value);
                    }
                    if (this.dropCategoriaFundoLote.SelectedIndex >= 0)
                    {
                        carteira.IdCategoria = Convert.ToInt32(this.dropCategoriaFundoLote.SelectedItem.Value);
                    }
                    if (this.dropSubCategoriaFundoLote.SelectedIndex >= 0)
                    {
                        carteira.IdSubCategoria = Convert.ToInt32(this.dropSubCategoriaFundoLote.SelectedItem.Value);
                    }
                    if (this.dropAgenteAdministradorLote.SelectedIndex >= 0)
                    {
                        carteira.IdAgenteAdministrador = Convert.ToInt32(this.dropAgenteAdministradorLote.SelectedItem.Value);
                        carteira.IdAgenteCustodiante = Convert.ToInt32(this.dropAgenteAdministradorLote.SelectedItem.Value);  //Adota Custodiante = Administrador
                    }
                    if (this.dropAgenteGestorLote.SelectedIndex >= 0)
                    {
                        carteira.IdAgenteGestor = Convert.ToInt32(this.dropAgenteGestorLote.SelectedItem.Value);
                    }
                    //
                    carteira.Save();
                }
            }
            e.Result = ok ? "Processo executado com sucesso." : "Processo executado parcialmente.";
            #endregion
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        //
        int idCarteira = (int)e.Keys[0];
        //
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        //
        #region TabPage Informações Basicas1
        ASPxComboBox dropTipoCalculoRetorno = pageControl.FindControl("dropTipoCalculoRetorno") as ASPxComboBox;
        ASPxComboBox dropTipoCota = pageControl.FindControl("dropTipoCota") as ASPxComboBox;
        ASPxComboBox dropTruncaCota = pageControl.FindControl("dropTruncaCota") as ASPxComboBox;
        ASPxComboBox dropTipoCarteira = pageControl.FindControl("dropTipoCarteira") as ASPxComboBox;
        ASPxComboBox dropTruncaQuantidade = pageControl.FindControl("dropTruncaQuantidade") as ASPxComboBox;
        ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropProcCalculoRF = pageControl.FindControl("dropProcCalculoRF") as ASPxComboBox;

        ASPxComboBox dropIndiceBenchmark = pageControl.FindControl("dropIndiceBenchmark") as ASPxComboBox;
        ASPxComboBox dropTruncaFinanceiro = pageControl.FindControl("dropTruncaFinanceiro") as ASPxComboBox;
        //
        ASPxDateEdit textDataInicio = pageControl.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxSpinEdit textCotaInicial = pageControl.FindControl("textCotaInicial") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisCota = pageControl.FindControl("textCasasDecimaisCota") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisQuantidade = pageControl.FindControl("textCasasDecimaisQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropPrioridadeOperacao = pageControl.FindControl("dropPrioridadeOperacao") as ASPxComboBox;
        ASPxComboBox dropBuscaCotaAnterior = pageControl.FindControl("dropBuscaCotaAnterior") as ASPxComboBox;
        ASPxSpinEdit textNumeroDiasBuscaCota = pageControl.FindControl("textNumeroDiasBuscaCota") as ASPxSpinEdit;
        ASPxSpinEdit textBandaVariacao = pageControl.FindControl("textBandaVariacao") as ASPxSpinEdit;
        ASPxComboBox dropRebateImpactaPL = pageControl.FindControl("dropRebateImpactaPL") as ASPxComboBox;
        ASPxCheckBox chbOffShore = pageControl.FindControl("chbOffShore") as ASPxCheckBox;
        ASPxTextBox textCodigoConsolidacaoExterno = pageControl.FindControl("textCodigoConsolidacaoExterno") as ASPxTextBox;
        ASPxSpinEdit textCodigoBDS = pageControl.FindControl("textCodigoBDS") as ASPxSpinEdit;


        #endregion

        #region TabPage Informações Basicas2
        ASPxComboBox dropCategoriaFundo = pageControl.FindControl("dropCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropSubCategoriaFundo = pageControl.FindControl("dropSubCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropAgenteAdministrador = pageControl.FindControl("dropAgenteAdministrador") as ASPxComboBox;
        ASPxComboBox dropAgenteGestor = pageControl.FindControl("dropAgenteGestor") as ASPxComboBox;
        ASPxComboBox dropAgenteCustodiante = pageControl.FindControl("dropAgenteCustodiante") as ASPxComboBox;
        ASPxComboBox dropGrupoEconomico = pageControl.FindControl("dropGrupoEconomico") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = pageControl.FindControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoCusto = pageControl.FindControl("dropTipoCusto") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = pageControl.FindControl("dropTipoTributacao") as ASPxComboBox;
        ASPxComboBox dropCalculaIOF = pageControl.FindControl("dropCalculaIOF") as ASPxComboBox;
        ASPxComboBox dropProjecaoIRResgate = pageControl.FindControl("dropProjecaoIRResgate") as ASPxComboBox;
        ASPxComboBox dropTipoVisaoFundo = pageControl.FindControl("dropTipoVisaoFundo") as ASPxComboBox;
        ASPxComboBox dropFundoExclusivo = pageControl.FindControl("dropFundoExclusivo") as ASPxComboBox;
        ASPxComboBox dropCalculaMTM = pageControl.FindControl("dropCalculaMTM") as ASPxComboBox;
        ASPxComboBox dropGrupoPerfilMTM = pageControl.FindControl("dropGrupoPerfilMTM") as ASPxComboBox;
        ASPxComboBox dropExcecaoRegraTxAdm = pageControl.FindControl("dropExcecaoRegraTxAdm") as ASPxComboBox;
        ASPxComboBox dropFIE = pageControl.FindControl("dropFIE") as ASPxComboBox;
        ASPxComboBox dropMesmoConglomerado = pageControl.FindControl("dropMesmoConglomerado") as ASPxComboBox;
        ASPxComboBox dropControladoriaAtivo = pageControl.FindControl("dropControladoriaAtivo") as ASPxComboBox;
        ASPxComboBox dropControladoriaPassivo = pageControl.FindControl("dropControladoriaPassivo") as ASPxComboBox;
        ASPxComboBox dropCategoriaAnbima = pageControl.FindControl("dropCategoriaAnbima") as ASPxComboBox;
        ASPxComboBox dropContratante = pageControl.FindControl("dropContratante") as ASPxComboBox;
        #endregion

        #region TabPage Informações Avançada
        ASPxTextBox textCodigoISIN = pageControl.FindControl("textCodigoISIN") as ASPxTextBox;
        ASPxSpinEdit textCodigoAnbid = pageControl.FindControl("textCodigoAnbid") as ASPxSpinEdit;
        ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCETIP") as ASPxTextBox;
        ASPxTextBox textCodigoBloomberg = pageControl.FindControl("textCodigoBloomberg") as ASPxTextBox;
        ASPxTextBox textCodigoSTI = pageControl.FindControl("textCodigoSTI") as ASPxTextBox;
        ASPxSpinEdit textValorMinimoAplicacao = pageControl.FindControl("textValorMinimoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoResgate = pageControl.FindControl("textValorMinimoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoSaldo = pageControl.FindControl("textValorMinimoSaldo") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoInicial = pageControl.FindControl("textValorMinimoInicial") as ASPxSpinEdit;
        ASPxSpinEdit textValorMaximoAplicacao = pageControl.FindControl("textValorMaximoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoAplicacao = pageControl.FindControl("textDiasCotizacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoResgate = pageControl.FindControl("textDiasCotizacaoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoAplicacao = pageControl.FindControl("textDiasLiquidacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoResgate = pageControl.FindControl("textDiasLiquidacaoResgate") as ASPxSpinEdit;
        ASPxComboBox dropCobraTaxaFiscalizacaoCVM = pageControl.FindControl("dropCobraTaxaFiscalizacaoCVM") as ASPxComboBox;
        ASPxComboBox dropExplodeCotaDeFundo = pageControl.FindControl("dropExplodeCotaDeFundo") as ASPxComboBox;
        ASPxComboBox dropTipoCVM = pageControl.FindControl("dropTipoCVM") as ASPxComboBox;
        ASPxComboBox dropCalculaEnquadra = pageControl.FindControl("dropCalculaEnquadra") as ASPxComboBox;
        ASPxComboBox dropContagemDiasCotizacaoResgate = pageControl.FindControl("dropContagemDiasCotizacaoResgate") as ASPxComboBox;
        ASPxComboBox dropDistribuicaoDividendo = pageControl.FindControl("dropDistribuicaoDividendo") as ASPxComboBox;
        ASPxComboBox dropTipoVisualizacaoResgCotista = pageControl.FindControl("dropTipoVisualizacaoResgCotista") as ASPxComboBox;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropContagemPrazoIOF = pageControl.FindControl("dropContagemPrazoIOF") as ASPxComboBox;
        ASPxComboBox dropCompensacaoPrejuizo = pageControl.FindControl("dropCompensacaoPrejuizo") as ASPxComboBox;
        ASPxTimeEdit textHorarioFim = pageControl.FindControl("textHorarioFim") as ASPxTimeEdit;
        ASPxTimeEdit textHorarioFimResgate = pageControl.FindControl("textHorarioFimResgate") as ASPxTimeEdit;
        ASPxCheckBox chkHorarioFim = pageControl.FindControl("chkHorarioFim") as ASPxCheckBox;
        ASPxCheckBox chkHorarioFimResgate = pageControl.FindControl("chkHorarioFimResgate") as ASPxCheckBox;
        ASPxComboBox dropLiberaAplicacao = pageControl.FindControl("dropLiberaAplicacao") as ASPxComboBox;
        ASPxComboBox dropLiberaResgate = pageControl.FindControl("dropLiberaResgate") as ASPxComboBox;
        ASPxComboBox dropPossuiResgateAutomatico = pageControl.FindControl("dropPossuiResgateAutomatico") as ASPxComboBox;
        ASPxComboBox dropPrazoMedio = pageControl.FindControl("dropPrazoMedio") as ASPxComboBox;
        ASPxComboBox dropTipoFundo = pageControl.FindControl("dropTipoFundo") as ASPxComboBox;
        ASPxComboBox dropRendimento = pageControl.FindControl("dropRendimento") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIR = pageControl.FindControl("textDiasAposResgateIR") as ASPxSpinEdit;
        ASPxComboBox dropPerfilRisco = pageControl.FindControl("dropPerfilRisco") as ASPxComboBox;
        ASPxComboBox dropComeCotasEntreRegatesConversao = pageControl.FindControl("dropComeCotasEntreRegatesConversao") as ASPxComboBox;
        ASPxComboBox dropExportaGalgo = pageControl.FindControl("dropExportaGalgo") as ASPxComboBox;
        ASPxComboBox dropCorretora = pageControl.FindControl("dropCorretora") as ASPxComboBox;
        ASPxComboBox dropProjecaoIRComeCotas = pageControl.FindControl("dropProjecaoIRComeCotas") as ASPxComboBox;
        ASPxSpinEdit textDiasAposComeCotasIR = pageControl.FindControl("textDiasAposComeCotasIR") as ASPxSpinEdit;
        ASPxComboBox dropRealizaCompensacao = pageControl.FindControl("dropRealizaCompensacao") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = pageControl.FindControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxComboBox dropDataInicioContIOF = pageControl.FindControl("dropDataInicioContIOF") as ASPxComboBox;
        ASPxComboBox dropDataFimContIOF = pageControl.FindControl("dropDataFimContIOF") as ASPxComboBox;
        ASPxComboBox dropContaPrzIOFVirtual = pageControl.FindControl("dropContaPrzIOFVirtual") as ASPxComboBox;
        ASPxComboBox dropProjecaoIOFResgate = pageControl.FindControl("dropProjecaoIOFResgate") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIOF = pageControl.FindControl("textDiasAposResgateIOF") as ASPxSpinEdit;
        ASPxComboBox dropInfluenciaGestorLocalCvm = pageControl.FindControl("dropInfluenciaGestorLocalCvm") as ASPxComboBox;
        ASPxComboBox dropInvestimentoColetivoCvm = pageControl.FindControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        #endregion

        #region TabPage Classes
        ASPxTextBox textNomeClasse = pageControl.FindControl("textNomeClasse") as ASPxTextBox;
        ASPxDateEdit textDataInicioClasse = pageControl.FindControl("textDataInicioClasse") as ASPxDateEdit;
        ASPxCheckBox chbSerieUnica = pageControl.FindControl("chbSerieUnica") as ASPxCheckBox;
        ASPxComboBox dropFrequencia = pageControl.FindControl("dropFrequencia") as ASPxComboBox;
        ASPxTextBox textPoliticaInvestimentos = pageControl.FindControl("textPoliticaInvestimentos") as ASPxTextBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropDomicilio = pageControl.FindControl("dropDomicilio") as ASPxComboBox;
        ASPxSpinEdit textTaxaAdm = pageControl.FindControl("textTaxaAdm") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCustodia = pageControl.FindControl("textTaxaCustodia") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaPerformance = pageControl.FindControl("textTaxaPerformance") as ASPxSpinEdit;
        ASPxComboBox dropIndexadorPerf = pageControl.FindControl("dropIndexadorPerf") as ASPxComboBox;
        ASPxComboBox dropFreqCalcPerformance = pageControl.FindControl("dropFreqCalcPerformance") as ASPxComboBox;
        ASPxSpinEdit textLockUp = pageControl.FindControl("textLockUp") as ASPxSpinEdit;
        ASPxComboBox dropHardSoft = pageControl.FindControl("dropHardSoft") as ASPxComboBox;
        ASPxSpinEdit textPenalidadeResgate = pageControl.FindControl("textPenalidadeResgate") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoAplicacao = pageControl.FindControl("textVlMinimoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoResgate = pageControl.FindControl("textVlMinimoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoPermanencia = pageControl.FindControl("textVlMinimoPermanencia") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasConvAplicacao = pageControl.FindControl("textQtdDiasConvAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasLiqFinAplic = pageControl.FindControl("textQtdDiasLiqFinAplic") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasConvResgate = pageControl.FindControl("textQtdDiasConvResgate") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasLiqFin = pageControl.FindControl("textQtdDiasLiqFin") as ASPxSpinEdit;
        ASPxSpinEdit textHoldBack = pageControl.FindControl("textHoldBack") as ASPxSpinEdit;
        ASPxDateEdit textAnivHoldBack = pageControl.FindControl("textAnivHoldBack") as ASPxDateEdit;
        ASPxCheckBox textSidePocket = pageControl.FindControl("textSidePocket") as ASPxCheckBox;
        ASPxSpinEdit textPrazoRepeticaoCotas = pageControl.FindControl("textPrazoRepeticaoCotas") as ASPxSpinEdit;
        ASPxSpinEdit textPrazoValidadePrevia = pageControl.FindControl("textPrazoValidadePrevia") as ASPxSpinEdit;
        ASPxComboBox dropIndexadorPrevia = pageControl.FindControl("dropIndexadorPrevia") as ASPxComboBox;

        #endregion
        //
        Carteira carteira = new Carteira();
        if (carteira.LoadByPrimaryKey(idCarteira))
        {

            #region Verifica se fundo é Off Shore
            Cliente clienteAux = new Cliente();
            clienteAux.LoadByPrimaryKey(carteira.IdCarteira.Value);

            //Nome e apelido devem vir direto de Pessoa
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(Convert.ToInt32(clienteAux.IdPessoa));

            if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ)
            {
                carteira.Nome = pessoa.Nome + " - " + textNomeClasse.Text;
                carteira.Apelido = pessoa.Apelido + " - " + textNomeClasse.Text;
            }
            #endregion

            #region TabPage Informações Basicas1
            carteira.TipoCalculoRetorno = Convert.ToByte(dropTipoCalculoRetorno.SelectedItem.Value);
            carteira.TipoCota = Convert.ToByte(dropTipoCota.SelectedItem.Value);
            carteira.TruncaCota = Convert.ToString(dropTruncaCota.SelectedItem.Value);
            carteira.TipoCarteira = Convert.ToByte(dropTipoCarteira.SelectedItem.Value);
            carteira.TruncaQuantidade = Convert.ToString(dropTruncaQuantidade.SelectedItem.Value);
            carteira.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
            carteira.ProcAutomatico = Convert.ToString(dropProcCalculoRF.SelectedItem.Value);
            carteira.IdIndiceBenchmark = Convert.ToInt16(dropIndiceBenchmark.SelectedItem.Value);
            carteira.Corretora = Convert.ToString(dropCorretora.SelectedItem.Value);
            carteira.TruncaFinanceiro = Convert.ToString(dropTruncaFinanceiro.SelectedItem.Value);
            carteira.DataInicioCota = Convert.ToDateTime(textDataInicio.Text);
            carteira.CotaInicial = Convert.ToDecimal(textCotaInicial.Text);
            carteira.CasasDecimaisCota = Convert.ToByte(textCasasDecimaisCota.Text);
            carteira.CasasDecimaisQuantidade = Convert.ToByte(textCasasDecimaisQuantidade.Text);
            carteira.PrioridadeOperacao = Convert.ToByte(dropPrioridadeOperacao.SelectedItem.Value);
            carteira.BuscaCotaAnterior = Convert.ToString(dropBuscaCotaAnterior.SelectedItem.Value);
            carteira.NumeroDiasBuscaCota = textNumeroDiasBuscaCota.Text != "" ? Convert.ToInt32(textNumeroDiasBuscaCota.Text) : 0;
            carteira.BandaVariacao = textBandaVariacao.Text != "" ? Convert.ToDecimal(textBandaVariacao.Text) : 0;

            if (dropCorretora.SelectedIndex != -1)
                carteira.Corretora = Convert.ToString(dropCorretora.SelectedItem.Value);

            if (dropRebateImpactaPL.SelectedIndex > -1)
            {
                carteira.RebateImpactaPL = Convert.ToString(dropRebateImpactaPL.SelectedItem.Value);
            }
            else
            {
                carteira.RebateImpactaPL = "N";
            }

            if (!string.IsNullOrEmpty(textCodigoConsolidacaoExterno.Text))
                carteira.CodigoConsolidacaoExterno = textCodigoConsolidacaoExterno.Text;
            else
                carteira.CodigoConsolidacaoExterno = null;

            int codigoBds = 0;
            if (int.TryParse(textCodigoBDS.Text, out codigoBds))
                carteira.CodigoBDS = codigoBds;
            else
                carteira.CodigoBDS = null;

            #endregion

            #region TabPage Informações Basicas2
            carteira.IdCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);
            carteira.IdSubCategoria = Convert.ToInt32(dropSubCategoriaFundo.SelectedItem.Value);
            carteira.IdAgenteAdministrador = Convert.ToInt32(dropAgenteAdministrador.SelectedItem.Value);
            carteira.IdAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);

            if (dropAgenteCustodiante.SelectedIndex > -1)
            {
                carteira.IdAgenteCustodiante = Convert.ToInt32(dropAgenteCustodiante.SelectedItem.Value);
            }
            else
            {
                carteira.IdAgenteCustodiante = carteira.IdAgenteAdministrador.Value;
            }

            if (dropGrupoEconomico.SelectedIndex > -1)
            {
                carteira.IdGrupoEconomico = Convert.ToInt32(dropGrupoEconomico.SelectedItem.Value);
            }

            carteira.TipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.SelectedItem.Value);
            carteira.TipoCusto = Convert.ToByte(dropTipoCusto.SelectedItem.Value);
            carteira.TipoTributacao = Convert.ToByte(dropTipoTributacao.SelectedItem.Value);
            carteira.CalculaIOF = Convert.ToString(dropCalculaIOF.SelectedItem.Value);
            carteira.ProjecaoIRResgate = Convert.ToByte(dropProjecaoIRResgate.SelectedItem.Value);
            carteira.ProjecaoIRComeCotas = Convert.ToByte(dropProjecaoIRComeCotas.SelectedItem.Value);
            carteira.ProjecaoIOFResgate = Convert.ToByte(dropProjecaoIOFResgate.SelectedItem.Value);
            carteira.TipoVisaoFundo = Convert.ToByte(dropTipoVisaoFundo.SelectedItem.Value);
            if (dropFundoExclusivo.SelectedIndex == -1)
            {
                carteira.FundoExclusivo = "N";
            }
            else
            {
                carteira.FundoExclusivo = dropFundoExclusivo.SelectedItem.Value.ToString();
            }

            if (dropCalculaMTM.SelectedIndex == -1)
            {
                carteira.CalculaMTM = Convert.ToInt32(ExecutaFuncao.Nao);
            }
            else
            {
                carteira.CalculaMTM = Convert.ToInt32(dropCalculaMTM.SelectedItem.Value);
            }
            if (dropMesmoConglomerado.SelectedIndex != -1)
                carteira.MesmoConglomerado = dropMesmoConglomerado.SelectedItem.Value.ToString();

            if (dropControladoriaAtivo.SelectedIndex != -1)
                carteira.ControladoriaAtivo = dropControladoriaAtivo.SelectedItem.Value.ToString();

            if (dropControladoriaPassivo.SelectedIndex != -1)
                carteira.ControladoriaPassivo = dropControladoriaPassivo.SelectedItem.Value.ToString();

            if (dropCategoriaAnbima.SelectedIndex != -1)
                carteira.CategoriaAnbima = int.Parse(dropCategoriaAnbima.SelectedItem.Value.ToString());

            if (dropContratante.SelectedIndex != -1)
                carteira.Contratante = int.Parse(dropContratante.SelectedItem.Value.ToString());

            if (Convert.ToInt32(dropCalculaMTM.SelectedItem.Value) == (int)ExecutaFuncao.Sim)
            {
                carteira.IdGrupoPerfilMTM = Convert.ToInt32(dropGrupoPerfilMTM.SelectedItem.Value);
            }
            else
            {
                carteira.IdGrupoPerfilMTM = null;
            }

            if (dropExcecaoRegraTxAdm.SelectedIndex == -1)
            {
                carteira.ExcecaoRegraTxAdm = "N";
            }
            else
            {
                carteira.ExcecaoRegraTxAdm = dropExcecaoRegraTxAdm.SelectedItem.Value.ToString();
            }

            if (dropPrazoMedio.SelectedIndex == 0)
            {
                carteira.CalculaPrazoMedio = "N";
            }
            else
            {
                carteira.CalculaPrazoMedio = dropPrazoMedio.SelectedItem.Value.ToString();
            }

            if (dropRealizaCompensacao.SelectedIndex == -1)
            {
                carteira.RealizaCompensacaoDePrejuizo = "N";
            }
            else
            {
                carteira.RealizaCompensacaoDePrejuizo = dropRealizaCompensacao.SelectedItem.Value.ToString();
            }

            if (dropFIE.SelectedIndex > -1)
            {
                carteira.Fie = Convert.ToString(dropFIE.SelectedItem.Value);
            }
            #endregion

            #region TabPage Informações Avançada
            carteira.CodigoIsin = textCodigoISIN.Text;
            carteira.CodigoAnbid = textCodigoAnbid.Text;
            carteira.CodigoCetip = textCodigoCetip.Text;
            carteira.CodigoBloomberg = textCodigoBloomberg.Text;
            carteira.CodigoSTI = textCodigoSTI.Text;
            carteira.ValorMinimoAplicacao = textValorMinimoAplicacao.Text != "" ? Convert.ToDecimal(textValorMinimoAplicacao.Text) : 0;
            carteira.ValorMinimoResgate = textValorMinimoResgate.Text != "" ? Convert.ToDecimal(textValorMinimoResgate.Text) : 0;
            carteira.ValorMinimoSaldo = textValorMinimoSaldo.Text != "" ? Convert.ToDecimal(textValorMinimoSaldo.Text) : 0;
            carteira.ValorMinimoInicial = textValorMinimoInicial.Text != "" ? Convert.ToDecimal(textValorMinimoInicial.Text) : 0;
            carteira.ValorMaximoAplicacao = textValorMaximoAplicacao.Text != "" ? Convert.ToDecimal(textValorMaximoAplicacao.Text) : 0;
            carteira.DiasCotizacaoAplicacao = Convert.ToByte(textDiasCotizacaoAplicacao.Text);
            carteira.DiasCotizacaoResgate = Convert.ToByte(textDiasCotizacaoResgate.Text);
            carteira.DiasLiquidacaoAplicacao = Convert.ToByte(textDiasLiquidacaoAplicacao.Text);
            carteira.DiasLiquidacaoResgate = Convert.ToByte(textDiasLiquidacaoResgate.Text);
            carteira.ContagemDiasConversaoResgate = Convert.ToByte(dropContagemDiasCotizacaoResgate.Value);
            //
            carteira.CobraTaxaFiscalizacaoCVM = Convert.ToString(dropCobraTaxaFiscalizacaoCVM.SelectedItem.Value);

            if (dropTipoCVM.SelectedIndex != -1)
            {
                carteira.TipoCVM = Convert.ToInt32(dropTipoCVM.SelectedItem.Value);
            }

            carteira.ExplodeCotasDeFundos = Convert.ToString(dropExplodeCotaDeFundo.SelectedItem.Value);

            //
            carteira.CalculaEnquadra = Convert.ToString(dropCalculaEnquadra.SelectedItem.Value);
            carteira.DistribuicaoDividendo = Convert.ToByte(dropDistribuicaoDividendo.SelectedItem.Value);
            carteira.TipoVisualizacaoResgCotista = Convert.ToString(dropTipoVisualizacaoResgCotista.SelectedItem.Value);

            if (dropEstrategia.SelectedIndex != -1)
            {
                carteira.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
            }
            else
            {
                carteira.IdEstrategia = null;
            }
            carteira.ContagemPrazoIOF = Convert.ToByte(dropContagemPrazoIOF.SelectedItem.Value);
            carteira.CompensacaoPrejuizo = Convert.ToByte(dropCompensacaoPrejuizo.SelectedItem.Value);
            if (chkHorarioFim.Checked)
            {
                carteira.HorarioFim = Convert.ToDateTime("1900-01-01 " + textHorarioFim.Text);
            }
            else
            {
                carteira.HorarioFim = null;
            }
            if (chkHorarioFimResgate.Checked)
            {
                carteira.HorarioFimResgate = Convert.ToDateTime("1900-01-01 " + textHorarioFimResgate.Text);
            }
            else
            {
                carteira.HorarioFimResgate = null;
            }

            carteira.TipoFundo = Convert.ToInt16(dropTipoFundo.SelectedItem.Value);
            carteira.Rendimento = Convert.ToInt16(dropRendimento.SelectedItem.Value);

            if (Convert.ToInt32(dropProjecaoIRResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                carteira.DiasAposResgateIR = Convert.ToInt32(textDiasAposResgateIR.Text);
            }
            else
            {
                carteira.DiasAposResgateIR = 0;
            }

            if (Convert.ToInt32(dropProjecaoIRComeCotas.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                carteira.DiasAposComeCotasIR = Convert.ToInt32(textDiasAposComeCotasIR.Text);
            }
            else
            {
                carteira.DiasAposComeCotasIR = 0;
            }

            if (dropProjecaoIOFResgate.SelectedIndex != -1)
            {
                if (Convert.ToInt32(dropProjecaoIOFResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
                {
                    carteira.DiasAposResgateIOF = Convert.ToInt32(textDiasAposResgateIOF.Text);
                }
                else
                {
                    carteira.DiasAposResgateIOF = 0;
                }
            }

            if (carteira.ContagemPrazoIOF.Value == (byte)ContagemDiasPrazoIOF.Personalizado)
            {
                carteira.DataInicioContIOF = Convert.ToInt16(dropDataInicioContIOF.SelectedItem.Value);
                carteira.DataFimContIOF = Convert.ToInt16(dropDataFimContIOF.SelectedItem.Value);
            }
            else
            {
                carteira.DataInicioContIOF = null;
                carteira.DataFimContIOF = null;
            }

            if (dropContaPrzIOFVirtual.SelectedIndex != -1)
                carteira.ContaPrzIOFVirtual = Convert.ToInt16(dropContaPrzIOFVirtual.SelectedItem.Value);

            int codicoCda = 0;
            if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
                carteira.CodigoCDA = codicoCda;

            carteira.CodigoSTI = textCodigoSTI.Text.ToString();


            carteira.LiberaAplicacao = Convert.ToString(dropLiberaAplicacao.SelectedItem.Value);
            carteira.LiberaResgate = Convert.ToString(dropLiberaResgate.SelectedItem.Value);
            carteira.PossuiResgateAutomatico = Convert.ToString(dropPossuiResgateAutomatico.SelectedItem.Value);

            if (dropPerfilRisco.SelectedIndex > -1)
            {
                carteira.PerfilRisco = Convert.ToInt16(dropPerfilRisco.SelectedItem.Value);

                #region Cria historico Suitability
                Carteira carteiraSuitability = new Carteira();
                carteiraSuitability.LoadByPrimaryKey(idCarteira);
                if (!carteiraSuitability.compareSuitability(carteira))
                {
                    CarteiraSuitabilityHistorico.createCarteiraSuitabilityHistorico(carteira, TipoOperacaoBanco.Alteração);
                }
                #endregion
            }


            if (dropComeCotasEntreRegatesConversao.SelectedIndex != -1)
            {
                carteira.ComeCotasEntreRegatesConversao = dropComeCotasEntreRegatesConversao.SelectedItem.Value.ToString();
            }

            if (dropExportaGalgo.SelectedIndex != -1)
            {
                carteira.ExportaGalgo = dropExportaGalgo.SelectedItem.Value.ToString();
            }

            if (dropInfluenciaGestorLocalCvm.SelectedIndex != -1)
            {
                carteira.InfluenciaGestorLocalCvm = dropInfluenciaGestorLocalCvm.SelectedItem.Value.ToString();
            }

            if (dropInvestimentoColetivoCvm.SelectedIndex != -1)
            {
                carteira.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
            }

            #endregion
            //        

            carteira.Save();

            #region Tratamento de classes
            if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF)
            {
                ClassesOffShore classesOffShore = new ClassesOffShore();
                classesOffShore.LoadByPrimaryKey(carteira.IdCarteira.Value);

                classesOffShore.IdClassesOffShore = carteira.IdCarteira;
                classesOffShore.Nome = Convert.ToString(textNomeClasse.Text);
                classesOffShore.DataInicio = Convert.ToDateTime(textDataInicioClasse.Text);
                if (chbSerieUnica.Checked)
                    classesOffShore.SerieUnica = "S";
                else
                    classesOffShore.SerieUnica = "N";

                if (dropFrequencia.SelectedIndex != -1)
                    classesOffShore.FrequenciaSerie = Convert.ToInt32(dropFrequencia.Value);

                if (dropMoeda.SelectedIndex != -1)
                    classesOffShore.Moeda = Convert.ToInt16(dropMoeda.Value);

                if (dropDomicilio.SelectedIndex != -1)
                    classesOffShore.Domicilio = Convert.ToInt32(dropDomicilio.Value);

                if (dropIndexadorPerf.SelectedIndex != -1)
                    classesOffShore.IndexadorPerf = Convert.ToInt16(dropIndexadorPerf.Value);

                if (dropFreqCalcPerformance.SelectedIndex != -1)
                    classesOffShore.FreqCalcPerformance = Convert.ToInt32(dropFreqCalcPerformance.Value);

                if (dropHardSoft.SelectedIndex != -1)
                    classesOffShore.HardSoft = Convert.ToInt32(dropHardSoft.Value);

                classesOffShore.PoliticaInvestimentos = Convert.ToString(textPoliticaInvestimentos.Text);
                classesOffShore.TaxaAdm = Convert.ToDecimal(textTaxaAdm.Value);
                classesOffShore.TaxaCustodia = Convert.ToDecimal(textTaxaCustodia.Value);
                classesOffShore.TaxaPerformance = Convert.ToDecimal(textTaxaPerformance.Value);
                classesOffShore.LockUp = Convert.ToInt32(textLockUp.Value);
                classesOffShore.PenalidadeResgate = Convert.ToDecimal(textPenalidadeResgate.Value);
                classesOffShore.VlMinimoAplicacao = Convert.ToDecimal(textVlMinimoAplicacao.Value);
                classesOffShore.VlMinimoResgate = Convert.ToDecimal(textVlMinimoResgate.Value);
                classesOffShore.VlMinimoPermanencia = Convert.ToDecimal(textVlMinimoPermanencia.Value);
                classesOffShore.QtdDiasConvAplicacao = Convert.ToInt32(textQtdDiasConvAplicacao.Value);
                classesOffShore.QtdDiasLiqFinAplic = Convert.ToInt32(textQtdDiasLiqFinAplic.Value);
                classesOffShore.QtdDiasConvResgate = Convert.ToInt32(textQtdDiasConvResgate.Value);
                classesOffShore.QtdDiasLiqFin = Convert.ToInt32(textQtdDiasLiqFin.Value);
                classesOffShore.HoldBack = Convert.ToDecimal(textHoldBack.Value);

                DateTime anivHoldBack = Convert.ToDateTime(textAnivHoldBack.Value);
                classesOffShore.AnivHoldBack = new DateTime(1900, anivHoldBack.Month, anivHoldBack.Day);

                if (textSidePocket.Checked)
                    classesOffShore.SidePocket = "S";
                else
                    classesOffShore.SidePocket = "N";

                if (textSidePocket.Checked)
                    classesOffShore.SidePocket = "S";
                else
                    classesOffShore.SidePocket = "N";

                if (!string.IsNullOrEmpty(textPrazoRepeticaoCotas.Text) && Convert.ToInt32(textPrazoRepeticaoCotas.Text) > 0)
                    classesOffShore.PrazoRepeticaoCotas = Convert.ToInt32(textPrazoRepeticaoCotas.Text);
                else
                    classesOffShore.PrazoRepeticaoCotas = null;

                if (dropIndexadorPrevia.SelectedIndex != -1)
                {
                    classesOffShore.IdIndicePrevia = Convert.ToInt16(dropIndexadorPrevia.SelectedItem.Value);
                }

                if (!string.IsNullOrEmpty(textPrazoValidadePrevia.Text) && Convert.ToInt32(textPrazoValidadePrevia.Text) > 0)
                    classesOffShore.PrazoValidadePrevia = Convert.ToInt32(textPrazoValidadePrevia.Text);
                else
                {
                    classesOffShore.PrazoValidadePrevia = null;
                    classesOffShore.IdIndicePrevia = null;
                }

                classesOffShore.Save();
            }
            #endregion

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Carteira - Operacao: Update Carteira: " + idCarteira + UtilitarioWeb.ToString(carteira),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion


            //Atualiza o cliente correspondente (statusAtivo)
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                cliente.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
                cliente.Save();
            }


        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackImportaFundo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string[] parameter = e.Parameter.Split('|');
        string codigoAnbid = parameter[0];
        string idCarteiraString = parameter[1];
        string cnpj = parameter[2];
        string idPessoaString = parameter[3];

        PessoaCollection pessoaColl = new PessoaCollection();
        if (!string.IsNullOrEmpty(cnpj) && ParametrosConfiguracaoSistema.Outras.PermiteDuplDocumentoPessoa.Equals("N"))
        {
            pessoaColl.Query.Where(pessoaColl.Query.Cpfcnpj.Equal(cnpj));

            if (pessoaColl.Query.Load())
            {
                e.Result = "CNPJ já cadastrado";
                return;
            }
        }

        Carteira carteira = new Carteira();
        int? idCarteira = null;
        if (!string.IsNullOrEmpty(idCarteiraString))
        {
            if (carteira.LoadByPrimaryKey(idCarteira.Value))
            {
                e.Result = "Já existe um fundo cadastrado com este código Anbima (ID)";
                return;
            }
        }

        int? idPessoa = null;
        if (!string.IsNullOrEmpty(idPessoaString))
        {
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa.Value))
            {
                e.Result = "Já existe uma pessoa cadastrada com este ID:" + idPessoa.Value.ToString();
                return;
            }
        }

        try
        {
            carteira.ImportaFundoSIAnbid(codigoAnbid, idCarteira, idPessoa);
        }
        catch (Exception ex)
        {
            e.Result = ex.Message;
            return;
        }

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Carteira - Operacao: Insert Carteira: " + idCarteira + UtilitarioWeb.ToString(carteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Result = "Importação concluída";

    }

    private bool IsInteger64(string valor)
    {
        try
        {
            Convert.ToInt64(valor);
            return true;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackImportaListaFundos_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string mensagem = "";

        if (String.IsNullOrEmpty(this.ASPxMemo1.Text.Trim()))
        {
            e.Result = "Preencha um código Anbid ou um CNPJ.";
            return;
        }

        String[] codigos = this.ASPxMemo1.Text.Trim().Split(new Char[] { '\n' });
        string[] codigosAux = this.RemoveDuplicados(codigos);

        for (int i = 0; i < codigosAux.Length; i++)
        {
            #region Para cada Entrada do TextArea
            string codigoAnbid = codigos[i];

            if (String.IsNullOrEmpty(codigoAnbid))
            {
                continue;
            }

            if (!this.IsInteger64(codigoAnbid))
            {
                mensagem += "Código Anbid " + codigoAnbid + " não carregado. Código deve ser um número inteiro.\n";
                continue;
            }

            try
            {
                #region Carteira

                SIAnbid.Fundo fundo = new SIAnbid.Fundo();
                if (!fundo.LoadByCodFundo(codigoAnbid, null, true))
                {

                    // consulta por cnpj
                    if (codigoAnbid.Length == 14)
                    {
                        SIAnbid s = new SIAnbid();
                        List<SIAnbid.Fundo> listaCnpj = s.BuscaFundos(codigoAnbid.ToString(), null, null);

                        if (listaCnpj.Count == 0)
                        {
                            mensagem += "Código Anbid " + codigoAnbid + " não carregado. Fundo não encontrado.\n";
                            continue;
                        }
                        else
                        {
                            // Achou por CNPJ
                            fundo = new SIAnbid.Fundo();
                            fundo.LoadByCodFundo(listaCnpj[0].CodigoFundo, null, true);
                        }
                    }
                    else
                    {
                        mensagem += "Código Anbid " + codigoAnbid + " não carregado. Fundo não encontrado.\n";
                        continue;
                    }
                }

                Carteira carteira = new Carteira();

                int idCarteira = Convert.ToInt32(fundo.CodigoFundo);
                if (carteira.LoadByPrimaryKey(idCarteira))
                {
                    mensagem += "Código Anbid " + codigoAnbid + " não carregado. Já existe um fundo cadastrado com este código Anbima.\n";
                    continue;
                }

                carteira.IdCarteira = idCarteira;
                carteira.Nome = fundo.NomeFantasia;
                carteira.Apelido = fundo.NomeFantasia;

                if (!string.IsNullOrEmpty(fundo.CotaAbertura))
                {
                    if (fundo.CotaAbertura.ToUpper() == "S")
                    {
                        carteira.TipoCota = (byte)TipoCotaFundo.Abertura;
                        carteira.TipoRentabilidade = (byte)TipoRentabilidadeFundo.PrimeiroPrimeiro;
                    }
                    else
                    {
                        carteira.TipoCota = (byte)TipoCotaFundo.Fechamento;
                        carteira.TipoRentabilidade = (byte)TipoRentabilidadeFundo.FinalFinal;
                    }
                }

                carteira.TipoCarteira = fundo.TipoCarteira;
                carteira.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
                carteira.IdIndiceBenchmark = fundo.IdIndiceBenchmark;
                carteira.CasasDecimaisCota = 8;
                carteira.CasasDecimaisQuantidade = 8;
                carteira.TruncaCota = "N";
                carteira.TruncaQuantidade = "N";
                carteira.TruncaFinanceiro = "N";
                carteira.CotaInicial = fundo.CotaInicial;
                carteira.ValorMinimoAplicacao = fundo.ValorMinimoAplicacao;
                carteira.ValorMinimoResgate = fundo.ValorMinimoResgate;
                carteira.ValorMinimoSaldo = fundo.ValorMinimoSaldo;
                carteira.ValorMinimoInicial = fundo.ValorMinimoInicial;
                carteira.TipoCusto = (byte)TipoCustoFundo.Aplicacao;
                carteira.DiasCotizacaoAplicacao = fundo.DiasCotizacaoAplicacao;
                carteira.DiasCotizacaoResgate = fundo.DiasCotizacaoResgate;
                carteira.DiasLiquidacaoAplicacao = fundo.DiasLiquidacaoAplicacao;
                carteira.DiasLiquidacaoResgate = fundo.DiasLiquidacaoResgate;
                carteira.TipoTributacao = fundo.TipoTributacao;
                carteira.CalculaIOF = fundo.CalculaIOF;
                carteira.DiasAniversario = 0;
                carteira.TipoAniversario = 0;
                carteira.IdCategoria = fundo.IdCategoria;

                // Se Categoria não existir exibe mensagem de erro
                CategoriaFundo c = new CategoriaFundo();
                if (!c.LoadByPrimaryKey(carteira.IdCategoria.Value))
                {
                    mensagem += "Código Anbid " + codigoAnbid + " não carregado. Categoria Fundo " + carteira.IdCategoria + " não cadastrada.\n";
                    continue;
                }

                carteira.IdSubCategoria = fundo.IdCategoria;
                carteira.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;

                //Criar agente mercado se nao existir
                AgenteMercado agenteAdministrador = new AgenteMercado();
                if (!agenteAdministrador.BuscaPorCodigoAnbid(fundo.CodigoAdministrador))
                {
                    agenteAdministrador.SaveAnbid(fundo.NomeAdministrador, fundo.CodigoAdministrador);
                }
                carteira.IdAgenteAdministrador = agenteAdministrador.IdAgente;
                carteira.IdAgenteCustodiante = agenteAdministrador.IdAgente; //Adota custodiante = administrador

                if (string.IsNullOrEmpty(fundo.CodigoGestor))
                {
                    carteira.IdAgenteGestor = agenteAdministrador.IdAgente;
                }
                else
                {
                    AgenteMercado agenteGestor = new AgenteMercado();
                    if (!agenteGestor.BuscaPorCodigoAnbid(fundo.CodigoGestor))
                    {
                        agenteGestor.SaveAnbid(fundo.NomeGestor, fundo.CodigoGestor);
                    }
                    carteira.IdAgenteGestor = agenteGestor.IdAgente;
                }

                carteira.CobraTaxaFiscalizacaoCVM = "N";
                carteira.ExplodeCotasDeFundos = "N";
                carteira.DataInicioCota = fundo.DataInicioCota;
                carteira.CalculaEnquadra = "N";

                carteira.ContagemDiasConversaoResgate = carteira.DiasCotizacaoResgate < 5
                                    ? (int)ContagemDiasLiquidacaoResgate.DiasUteis
                                    : (int)ContagemDiasLiquidacaoResgate.DiasCorridos;

                if (carteira.UpToAgenteMercadoByIdAgenteAdministrador.Nome.ToUpper().Contains("MELLON"))
                {
                    carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Mellon;
                }
                else
                {
                    carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.PrazoCorrido;
                }

                carteira.ProjecaoIRResgate = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;
                carteira.TipoVisaoFundo = (byte)Financial.Captacao.Enums.TipoVisaoFundoRebate.NaoTrata;
                carteira.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;
                carteira.CompensacaoPrejuizo = (byte)TipoCompensacaoPrejuizo.Padrao;
                carteira.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;
                carteira.TipoVisualizacaoResgCotista = TipoVisualizacaoResgCotista.Consolidado;

                Estrategia estrategia = new Estrategia();
                if (!estrategia.BuscaPorDescricao(fundo.Estrategia))
                {
                    estrategia.Save(fundo.Estrategia, false);
                }
                carteira.IdEstrategia = estrategia.IdEstrategia;

                carteira.CodigoAnbid = fundo.CodigoFundo;

                //Criar pessoa
                #region Criar pessoa automaticamente
                Pessoa pessoa = new Pessoa();
                pessoa.IdPessoa = carteira.IdCarteira;
                pessoa.Nome = fundo.NomeFantasia;
                pessoa.Apelido = fundo.NomeFantasia;
                pessoa.Tipo = (byte)Financial.CRM.Enums.TipoPessoa.Juridica;
                pessoa.DataCadatro = DateTime.Now;
                pessoa.DataUltimaAlteracao = DateTime.Now;
                pessoa.Cpfcnpj = fundo.CNPJ;
                #endregion

                #region Criar cliente
                Cliente cliente = new Cliente();
                cliente.IdCliente = carteira.IdCarteira;
                cliente.Nome = pessoa.Nome;
                cliente.Apelido = pessoa.Apelido;
                cliente.IdTipo = TipoClienteFixo.Fundo;
                cliente.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                cliente.Status = (byte)StatusCliente.Aberto;
                cliente.DataDia = cliente.DataInicio = cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                cliente.ApuraGanhoRV = "N";
                cliente.TipoControle = (byte)TipoControleCliente.ApenasCotacao;
                cliente.IsentoIR = "S";
                cliente.IsentoIOF = "S";
                cliente.IdMoeda = (int)Financial.Common.Enums.ListaMoedaFixo.Real;
                cliente.ZeraCaixa = "N";
                cliente.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
                cliente.GrossUP = (byte)GrossupCliente.NaoFaz;

                GrupoProcessamentoCollection grupoProcessamentoCollection = new GrupoProcessamentoCollection();
                grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.Descricao.Like("%fundo%"));
                grupoProcessamentoCollection.Query.Load();

                if (grupoProcessamentoCollection.Count == 0)
                {
                    grupoProcessamentoCollection.QueryReset();
                    grupoProcessamentoCollection.LoadAll();
                }

                cliente.IdGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
                cliente.IsProcessando = "N";
                cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                cliente.CalculaRealTime = "N";
                cliente.CalculaGerencial = "N";
                #endregion

                using (esTransactionScope scope = new esTransactionScope())
                {

                    pessoa.Save();
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Pessoa - Operacao: Insert Pessoa: " + pessoa.IdPessoa + UtilitarioWeb.ToString(pessoa),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    cliente.Save();
                    #region Log do Processo
                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Cliente - Operacao: Insert Cliente: " + cliente.IdCliente + UtilitarioWeb.ToString(cliente),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    carteira.Save();
                    #region Log do Processo
                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Carteira - Operacao: Insert Carteira: " + carteira.IdCarteira + UtilitarioWeb.ToString(carteira),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    scope.Complete();
                }
                #region Cadastro de Taxa de Administração
                if (fundo.TaxaAdministracao.HasValue && fundo.TaxaAdministracao.Value != 0)
                {
                    TabelaTaxaAdministracao taxaAdm = new TabelaTaxaAdministracao();
                    taxaAdm.IdCarteira = carteira.IdCarteira;
                    taxaAdm.DataReferencia = carteira.DataInicioCota;
                    taxaAdm.TipoCalculo = (byte)TipoCalculoAdministracao.PercentualPL;
                    taxaAdm.TipoApropriacao = (byte)TipoApropriacaoAdministracao.Linear;
                    taxaAdm.Taxa = fundo.TaxaAdministracao.Value;
                    taxaAdm.BaseApuracao = (byte)BaseApuracaoAdministracao.PL_DiaAnterior;
                    taxaAdm.BaseAno = (short)BaseAnoAdministracao.Base252;
                    taxaAdm.TipoLimitePL = (byte)TipoLimitePLAdministracao.TotalPL;
                    taxaAdm.ValorLimite = 0;
                    taxaAdm.ContagemDias = (byte)Financial.Util.Enums.ContagemDias.Uteis;
                    taxaAdm.ValorMinimo = 0;
                    taxaAdm.ValorMaximo = 0;
                    taxaAdm.NumeroMesesRenovacao = 1;
                    taxaAdm.NumeroDiasPagamento = 0;
                    taxaAdm.ImpactaPL = "S";
                    taxaAdm.IdCadastro = 1;
                    taxaAdm.ValorFixoTotal = null;
                    taxaAdm.DataFim = null;
                    taxaAdm.IdEventoProvisao = null;
                    taxaAdm.IdEventoPagamento = null;
                    taxaAdm.Save();
                }
                #endregion

                carteira.ImportaCotasSIAnbid();

                #endregion
            }
            catch (Exception e1)
            {
                mensagem += "Código Anbid " + codigoAnbid + " não carregado. Erro: " + e1.Message + "\n";
            }

            #endregion
        }

        e.Result = !String.IsNullOrEmpty(mensagem) ? mensagem : "Importação concluída";
    }

    /// <summary>
    /// Remove itens duplicados de um array de string
    /// </summary>
    /// <param name="myStringArray"></param>
    /// <returns></returns>
    private string[] RemoveDuplicados(string[] myStringArray)
    {
        List<String> myStringList = new List<string>();

        foreach (string s in myStringArray)
        {
            if (!myStringList.Contains(s))
            {
                myStringList.Add(s);
            }
        }
        return myStringList.ToArray();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void SalvarNovo()
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        #region TabPage Informações Basicas1
        ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textApelido = pageControl.FindControl("textApelido") as TextBox;
        TextBox textNome = pageControl.FindControl("textNome") as TextBox;
        ASPxComboBox dropTipoCota = pageControl.FindControl("dropTipoCota") as ASPxComboBox;
        ASPxComboBox dropTruncaCota = pageControl.FindControl("dropTruncaCota") as ASPxComboBox;
        ASPxComboBox dropProcCalculoRF = pageControl.FindControl("dropProcCalculoRF") as ASPxComboBox;
        ASPxComboBox dropTipoCarteira = pageControl.FindControl("dropTipoCarteira") as ASPxComboBox;
        ASPxComboBox dropTruncaQuantidade = pageControl.FindControl("dropTruncaQuantidade") as ASPxComboBox;
        ASPxComboBox dropStatusAtivo = pageControl.FindControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropIndiceBenchmark = pageControl.FindControl("dropIndiceBenchmark") as ASPxComboBox;
        ASPxComboBox dropTruncaFinanceiro = pageControl.FindControl("dropTruncaFinanceiro") as ASPxComboBox;
        ASPxDateEdit textDataInicio = pageControl.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxSpinEdit textCotaInicial = pageControl.FindControl("textCotaInicial") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisCota = pageControl.FindControl("textCasasDecimaisCota") as ASPxSpinEdit;
        ASPxSpinEdit textCasasDecimaisQuantidade = pageControl.FindControl("textCasasDecimaisQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropPrioridadeOperacao = pageControl.FindControl("dropPrioridadeOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoCalculoRetorno = pageControl.FindControl("dropTipoCalculoRetorno") as ASPxComboBox;
        ASPxComboBox dropCorretora = pageControl.FindControl("dropCorretora") as ASPxComboBox;
        ASPxComboBox dropBuscaCotaAnterior = pageControl.FindControl("dropBuscaCotaAnterior") as ASPxComboBox;
        ASPxSpinEdit textNumeroDiasBuscaCota = pageControl.FindControl("textNumeroDiasBuscaCota") as ASPxSpinEdit;
        ASPxSpinEdit textBandaVariacao = pageControl.FindControl("textBandaVariacao") as ASPxSpinEdit;
        ASPxComboBox dropRebateImpactaPL = pageControl.FindControl("dropRebateImpactaPL") as ASPxComboBox;
        ASPxCheckBox chbOffShore = pageControl.FindControl("chbOffShore") as ASPxCheckBox;
        ASPxTextBox textCodigoConsolidacaoExterno = pageControl.FindControl("textCodigoConsolidacaoExterno") as ASPxTextBox;
        ASPxSpinEdit textCodigoBDS = pageControl.FindControl("textCodigoBDS") as ASPxSpinEdit;
        #endregion

        #region TabPage Informações Basicas2
        ASPxComboBox dropCategoriaFundo = pageControl.FindControl("dropCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropSubCategoriaFundo = pageControl.FindControl("dropSubCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropAgenteAdministrador = pageControl.FindControl("dropAgenteAdministrador") as ASPxComboBox;
        ASPxComboBox dropAgenteGestor = pageControl.FindControl("dropAgenteGestor") as ASPxComboBox;
        ASPxComboBox dropAgenteCustodiante = pageControl.FindControl("dropAgenteCustodiante") as ASPxComboBox;
        ASPxComboBox dropGrupoEconomico = pageControl.FindControl("dropGrupoEconomico") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = pageControl.FindControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropCobraTaxaFiscalizacaoCVM = pageControl.FindControl("dropCobraTaxaFiscalizacaoCVM") as ASPxComboBox;
        ASPxComboBox dropExplodeCotaDeFundo = pageControl.FindControl("dropExplodeCotaDeFundo") as ASPxComboBox;
        ASPxComboBox dropTipoCVM = pageControl.FindControl("dropTipoCVM") as ASPxComboBox;
        ASPxComboBox dropTipoCusto = pageControl.FindControl("dropTipoCusto") as ASPxComboBox;
        ASPxComboBox dropFundoExclusivo = pageControl.FindControl("dropFundoExclusivo") as ASPxComboBox;
        ASPxComboBox dropMesmoConglomerado = pageControl.FindControl("dropMesmoConglomerado") as ASPxComboBox;
        ASPxComboBox dropControladoriaAtivo = pageControl.FindControl("dropControladoriaAtivo") as ASPxComboBox;
        ASPxComboBox dropControladoriaPassivo = pageControl.FindControl("dropControladoriaPassivo") as ASPxComboBox;
        ASPxComboBox dropCategoriaAnbima = pageControl.FindControl("dropCategoriaAnbima") as ASPxComboBox;
        ASPxComboBox dropCalculaMTM = pageControl.FindControl("dropCalculaMTM") as ASPxComboBox;
        ASPxComboBox dropGrupoPerfilMTM = pageControl.FindControl("dropGrupoPerfilMTM") as ASPxComboBox;
        ASPxComboBox dropExcecaoRegraTxAdm = pageControl.FindControl("dropExcecaoRegraTxAdm") as ASPxComboBox;
        ASPxComboBox dropFIE = pageControl.FindControl("dropFIE") as ASPxComboBox;
        ASPxComboBox dropContratante = pageControl.FindControl("dropContratante") as ASPxComboBox;

        #endregion

        #region TabPage Informações Avançada
        ASPxTextBox textCodigoISIN = pageControl.FindControl("textCodigoISIN") as ASPxTextBox;
        ASPxSpinEdit textCodigoAnbid = pageControl.FindControl("textCodigoAnbid") as ASPxSpinEdit;
        ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCETIP") as ASPxTextBox;
        ASPxTextBox textCodigoBloomberg = pageControl.FindControl("textCodigoBloomberg") as ASPxTextBox;
        ASPxTextBox textCodigoSTI = pageControl.FindControl("textCodigoSTI") as ASPxTextBox;
        ASPxSpinEdit textValorMinimoAplicacao = pageControl.FindControl("textValorMinimoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoResgate = pageControl.FindControl("textValorMinimoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoSaldo = pageControl.FindControl("textValorMinimoSaldo") as ASPxSpinEdit;
        ASPxSpinEdit textValorMinimoInicial = pageControl.FindControl("textValorMinimoInicial") as ASPxSpinEdit;
        ASPxSpinEdit textValorMaximoAplicacao = pageControl.FindControl("textValorMaximoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoAplicacao = pageControl.FindControl("textDiasCotizacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasCotizacaoResgate = pageControl.FindControl("textDiasCotizacaoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoAplicacao = pageControl.FindControl("textDiasLiquidacaoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacaoResgate = pageControl.FindControl("textDiasLiquidacaoResgate") as ASPxSpinEdit;
        ASPxComboBox dropCalculaEnquadra = pageControl.FindControl("dropCalculaEnquadra") as ASPxComboBox;
        ASPxComboBox dropContagemDiasCotizacaoResgate = pageControl.FindControl("dropContagemDiasCotizacaoResgate") as ASPxComboBox;
        ASPxComboBox dropDistribuicaoDividendo = pageControl.FindControl("dropDistribuicaoDividendo") as ASPxComboBox;
        ASPxComboBox dropTipoVisualizacaoResgCotista = pageControl.FindControl("dropTipoVisualizacaoResgCotista") as ASPxComboBox;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropContagemPrazoIOF = pageControl.FindControl("dropContagemPrazoIOF") as ASPxComboBox;
        ASPxComboBox dropCompensacaoPrejuizo = pageControl.FindControl("dropCompensacaoPrejuizo") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = pageControl.FindControl("dropTipoTributacao") as ASPxComboBox;
        ASPxComboBox dropCalculaIOF = pageControl.FindControl("dropCalculaIOF") as ASPxComboBox;
        ASPxComboBox dropProjecaoIRResgate = pageControl.FindControl("dropProjecaoIRResgate") as ASPxComboBox;
        ASPxComboBox dropTipoVisaoFundo = pageControl.FindControl("dropTipoVisaoFundo") as ASPxComboBox;
        ASPxTimeEdit textHorarioFim = pageControl.FindControl("textHorarioFim") as ASPxTimeEdit;
        ASPxTimeEdit textHorarioFimResgate = pageControl.FindControl("textHorarioFimResgate") as ASPxTimeEdit;
        ASPxCheckBox chkHorarioFim = pageControl.FindControl("chkHorarioFim") as ASPxCheckBox;
        ASPxCheckBox chkHorarioFimResgate = pageControl.FindControl("chkHorarioFimResgate") as ASPxCheckBox;
        ASPxComboBox dropLiberaAplicacao = pageControl.FindControl("dropLiberaAplicacao") as ASPxComboBox;
        ASPxComboBox dropLiberaResgate = pageControl.FindControl("dropLiberaResgate") as ASPxComboBox;
        ASPxComboBox dropPossuiResgateAutomatico = pageControl.FindControl("dropPossuiResgateAutomatico") as ASPxComboBox;
        ASPxComboBox dropPerfilRisco = pageControl.FindControl("dropPerfilRisco") as ASPxComboBox;
        ASPxComboBox dropComeCotasEntreRegatesConversao = pageControl.FindControl("dropComeCotasEntreRegatesConversao") as ASPxComboBox;
        ASPxComboBox dropPrazoMedio = pageControl.FindControl("dropPrazoMedio") as ASPxComboBox;
        ASPxComboBox dropTipoFundo = pageControl.FindControl("dropTipoFundo") as ASPxComboBox;
        ASPxComboBox dropRendimento = pageControl.FindControl("dropRendimento") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIR = pageControl.FindControl("textDiasAposResgateIR") as ASPxSpinEdit;
        ASPxComboBox dropProjecaoIRComeCotas = pageControl.FindControl("dropProjecaoIRComeCotas") as ASPxComboBox;
        ASPxSpinEdit textDiasAposComeCotasIR = pageControl.FindControl("textDiasAposComeCotasIR") as ASPxSpinEdit;
        ASPxComboBox dropRealizaCompensacao = pageControl.FindControl("dropRealizaCompensacao") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = pageControl.FindControl("textCodigoCDA") as ASPxSpinEdit;        
        ASPxComboBox dropDataInicioContIOF = pageControl.FindControl("dropDataInicioContIOF") as ASPxComboBox;
        ASPxComboBox dropDataFimContIOF = pageControl.FindControl("dropDataFimContIOF") as ASPxComboBox;
        ASPxComboBox dropContaPrzIOFVirtual = pageControl.FindControl("dropContaPrzIOFVirtual") as ASPxComboBox;
        ASPxComboBox dropProjecaoIOFResgate = pageControl.FindControl("dropProjecaoIOFResgate") as ASPxComboBox;
        ASPxSpinEdit textDiasAposResgateIOF = pageControl.FindControl("textDiasAposResgateIOF") as ASPxSpinEdit;
        ASPxComboBox dropExportaGalgo = pageControl.FindControl("dropExportaGalgo") as ASPxComboBox;
        ASPxComboBox dropInfluenciaGestorLocalCvm = pageControl.FindControl("dropInfluenciaGestorLocalCvm") as ASPxComboBox;
        ASPxComboBox dropInvestimentoColetivoCvm = pageControl.FindControl("dropInvestimentoColetivoCvm") as ASPxComboBox;
        #endregion

        #region TabPage Classes
        ASPxTextBox textNomeClasse = pageControl.FindControl("textNomeClasse") as ASPxTextBox;
        ASPxDateEdit textDataInicioClasse = pageControl.FindControl("textDataInicioClasse") as ASPxDateEdit;
        ASPxCheckBox chbSerieUnica = pageControl.FindControl("chbSerieUnica") as ASPxCheckBox;
        ASPxComboBox dropFrequencia = pageControl.FindControl("dropFrequencia") as ASPxComboBox;
        ASPxTextBox textPoliticaInvestimentos = pageControl.FindControl("textPoliticaInvestimentos") as ASPxTextBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropDomicilio = pageControl.FindControl("dropDomicilio") as ASPxComboBox;
        ASPxSpinEdit textTaxaAdm = pageControl.FindControl("textTaxaAdm") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaCustodia = pageControl.FindControl("textTaxaCustodia") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaPerformance = pageControl.FindControl("textTaxaPerformance") as ASPxSpinEdit;
        ASPxComboBox dropIndexadorPerf = pageControl.FindControl("dropIndexadorPerf") as ASPxComboBox;
        ASPxComboBox dropFreqCalcPerformance = pageControl.FindControl("dropFreqCalcPerformance") as ASPxComboBox;
        ASPxSpinEdit textLockUp = pageControl.FindControl("textLockUp") as ASPxSpinEdit;
        ASPxComboBox dropHardSoft = pageControl.FindControl("dropHardSoft") as ASPxComboBox;
        ASPxSpinEdit textPenalidadeResgate = pageControl.FindControl("textPenalidadeResgate") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoAplicacao = pageControl.FindControl("textVlMinimoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoResgate = pageControl.FindControl("textVlMinimoResgate") as ASPxSpinEdit;
        ASPxSpinEdit textVlMinimoPermanencia = pageControl.FindControl("textVlMinimoPermanencia") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasConvAplicacao = pageControl.FindControl("textQtdDiasConvAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasLiqFinAplic = pageControl.FindControl("textQtdDiasLiqFinAplic") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasConvResgate = pageControl.FindControl("textQtdDiasConvResgate") as ASPxSpinEdit;
        ASPxSpinEdit textQtdDiasLiqFin = pageControl.FindControl("textQtdDiasLiqFin") as ASPxSpinEdit;
        ASPxSpinEdit textHoldBack = pageControl.FindControl("textHoldBack") as ASPxSpinEdit;
        ASPxDateEdit textAnivHoldBack = pageControl.FindControl("textAnivHoldBack") as ASPxDateEdit;
        ASPxCheckBox textSidePocket = pageControl.FindControl("textSidePocket") as ASPxCheckBox;
        ASPxSpinEdit textPrazoRepeticaoCotas = pageControl.FindControl("textPrazoRepeticaoCotas") as ASPxSpinEdit;
        ASPxSpinEdit textPrazoValidadePrevia = pageControl.FindControl("textPrazoValidadePrevia") as ASPxSpinEdit;
        ASPxComboBox dropIndexadorPrevia = pageControl.FindControl("dropIndexadorPrevia") as ASPxComboBox;
        #endregion

        Carteira carteira = new Carteira();
        if (!carteira.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text)))
            carteira.IdCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        #region Indica se é Cliente Clube/FDIC/Fundo
        Cliente clienteAux = new Cliente();
        clienteAux.LoadByPrimaryKey(carteira.IdCarteira.Value);

        bool isCliente = clienteAux.IdTipo.Value == TipoClienteFixo.Clube ||
                         clienteAux.IdTipo.Value == TipoClienteFixo.FDIC ||
                         clienteAux.IdTipo.Value == TipoClienteFixo.Fundo;
        #endregion

        //Nome e apelido devem vir direto de Pessoa
        Pessoa pessoa = new Pessoa();
        Cliente c = new Cliente();
        c.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));
        pessoa.LoadByPrimaryKey(Convert.ToInt32(c.IdPessoa));

        if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF)
        {
            carteira.Nome = pessoa.Nome + " - " + textNomeClasse.Text;
            carteira.Apelido = pessoa.Apelido + " - " + textNomeClasse.Text;
        }
        else
        {
            carteira.Nome = pessoa.Nome;
            carteira.Apelido = pessoa.Apelido;
        }

        if (!string.IsNullOrEmpty(textCodigoConsolidacaoExterno.Text))
            carteira.CodigoConsolidacaoExterno = textCodigoConsolidacaoExterno.Text;
        else
            carteira.CodigoConsolidacaoExterno = null;

        //

        #region TabPage Informações Basicas1
        if (Convert.ToByte(dropTipoCalculoRetorno.SelectedItem.Value) == (byte)CalculoRetornoCarteira.TIR)
        {
            carteira.TipoCota = Convert.ToByte(TipoCotaFundo.Fechamento);
            carteira.TruncaCota = "N";
            carteira.TruncaQuantidade = "N";
            carteira.TruncaFinanceiro = "N";
            carteira.CotaInicial = 1;
            carteira.CasasDecimaisCota = 8;
            carteira.CasasDecimaisQuantidade = 8;
        }
        else
        {
            carteira.TipoCota = Convert.ToByte(dropTipoCota.SelectedItem.Value);
            carteira.TruncaCota = Convert.ToString(dropTruncaCota.SelectedItem.Value);
            carteira.TruncaQuantidade = Convert.ToString(dropTruncaQuantidade.SelectedItem.Value);
            carteira.TruncaFinanceiro = Convert.ToString(dropTruncaFinanceiro.SelectedItem.Value);
            if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF)
                carteira.CotaInicial = 1;
            else
                carteira.CotaInicial = Convert.ToDecimal(textCotaInicial.Text);
            carteira.CasasDecimaisCota = Convert.ToByte(textCasasDecimaisCota.Text);
            carteira.CasasDecimaisQuantidade = Convert.ToByte(textCasasDecimaisQuantidade.Text);
        }

        carteira.TipoCarteira = Convert.ToByte(dropTipoCarteira.SelectedItem.Value);
        carteira.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
        carteira.ProcAutomatico = Convert.ToString(dropProcCalculoRF.SelectedItem.Value);
        carteira.Corretora = Convert.ToString(dropCorretora.SelectedItem.Value);
        carteira.IdIndiceBenchmark = Convert.ToInt16(dropIndiceBenchmark.SelectedItem.Value);
        carteira.DataInicioCota = Convert.ToDateTime(textDataInicio.Text);
        carteira.TipoCalculoRetorno = Convert.ToByte(dropTipoCalculoRetorno.SelectedItem.Value);
        carteira.BuscaCotaAnterior = Convert.ToString(dropBuscaCotaAnterior.Value);
        carteira.NumeroDiasBuscaCota = textNumeroDiasBuscaCota.Text != "" ? Convert.ToInt32(textNumeroDiasBuscaCota.Text) : 0;
        carteira.BandaVariacao = textBandaVariacao.Text != "" ? Convert.ToDecimal(textBandaVariacao.Text) : 0;

        if (dropRebateImpactaPL.SelectedIndex > -1)
        {
            carteira.RebateImpactaPL = Convert.ToString(dropRebateImpactaPL.SelectedItem.Value);
        }
        else
        {
            carteira.RebateImpactaPL = "N";
        }

        int codigoBds = 0;
        if (int.TryParse(textCodigoBDS.Text, out codigoBds))
            carteira.CodigoBDS = codigoBds;
        else
            carteira.CodigoBDS = null;

        if (dropCorretora.SelectedIndex != -1)
            carteira.Corretora = Convert.ToString(dropCorretora.SelectedItem.Value);

        #endregion

        #region TabPage Informações Basicas2
        carteira.IdCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);
        carteira.IdSubCategoria = Convert.ToInt32(dropSubCategoriaFundo.SelectedItem.Value);
        carteira.IdAgenteAdministrador = Convert.ToInt32(dropAgenteAdministrador.SelectedItem.Value);
        carteira.IdAgenteGestor = Convert.ToInt32(dropAgenteGestor.SelectedItem.Value);

        if (dropAgenteCustodiante.SelectedIndex > -1)
        {
            carteira.IdAgenteCustodiante = Convert.ToInt32(dropAgenteCustodiante.SelectedItem.Value);
        }
        else
        {
            carteira.IdAgenteCustodiante = carteira.IdAgenteAdministrador.Value;
        }

        if (dropGrupoEconomico.SelectedIndex > -1)
        {
            carteira.IdGrupoEconomico = Convert.ToInt32(dropGrupoEconomico.SelectedItem.Value);
        }

        carteira.TipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.SelectedItem.Value);
        //
        carteira.CobraTaxaFiscalizacaoCVM = Convert.ToString(dropCobraTaxaFiscalizacaoCVM.SelectedItem.Value);
        carteira.ExplodeCotasDeFundos = Convert.ToString(dropExplodeCotaDeFundo.SelectedItem.Value);
        carteira.CalculaEnquadra = Convert.ToString(dropCalculaEnquadra.SelectedItem.Value);
        //
        if (dropTipoCVM.SelectedIndex != -1)
        {
            carteira.TipoCVM = Convert.ToInt32(dropTipoCVM.SelectedItem.Value);
        }
        if (dropFundoExclusivo.SelectedIndex == -1)
        {
            carteira.FundoExclusivo = "N";
        }
        else
        {
            carteira.FundoExclusivo = dropFundoExclusivo.SelectedItem.Value.ToString();
        }


        if (dropCalculaMTM.SelectedIndex == -1)
        {
            carteira.CalculaMTM = Convert.ToInt32(ExecutaFuncao.Nao);
        }
        else
        {
            carteira.CalculaMTM = Convert.ToInt32(dropCalculaMTM.SelectedItem.Value);
        }

        if (Convert.ToInt32(dropCalculaMTM.SelectedItem.Value) == (int)ExecutaFuncao.Sim)
        {
            carteira.IdGrupoPerfilMTM = Convert.ToInt32(dropGrupoPerfilMTM.SelectedItem.Value);
        }
        else
        {
            carteira.IdGrupoPerfilMTM = null;
        }
        if (dropMesmoConglomerado.SelectedIndex != -1)
            carteira.MesmoConglomerado = dropMesmoConglomerado.SelectedItem.Value.ToString();

        if (dropControladoriaAtivo.SelectedIndex != -1)
            carteira.ControladoriaAtivo = dropControladoriaAtivo.SelectedItem.Value.ToString();

        if (dropControladoriaPassivo.SelectedIndex != -1)
            carteira.ControladoriaPassivo = dropControladoriaPassivo.SelectedItem.Value.ToString();

        if (dropCategoriaAnbima.SelectedIndex != -1)
            carteira.CategoriaAnbima = int.Parse(dropCategoriaAnbima.SelectedItem.Value.ToString());

        if (dropContratante.SelectedIndex != -1)
            carteira.Contratante = int.Parse(dropContratante.SelectedItem.Value.ToString());





        if (dropExcecaoRegraTxAdm.SelectedIndex == -1)
        {
            carteira.ExcecaoRegraTxAdm = "N";
        }
        else
        {
            carteira.ExcecaoRegraTxAdm = dropExcecaoRegraTxAdm.SelectedItem.Value.ToString();
        }

        if (dropFIE.SelectedIndex > -1)
        {
            carteira.Fie = Convert.ToString(dropFIE.SelectedItem.Value);
        }

        #endregion

        #region TabPage Informações Avançada - Parâmetros Fundo/Clube - Tributação
        if (isCliente)
        {
            // Só pega se for Fundo/Clube/FDIC - Campos Obrigatórios
            carteira.CalculaIOF = Convert.ToString(dropCalculaIOF.SelectedItem.Value);
            carteira.ContagemPrazoIOF = Convert.ToByte(dropContagemPrazoIOF.SelectedItem.Value);
            carteira.ProjecaoIRResgate = Convert.ToByte(dropProjecaoIRResgate.SelectedItem.Value);
            carteira.ProjecaoIRComeCotas = Convert.ToByte(dropProjecaoIRComeCotas.SelectedItem.Value);
            carteira.ProjecaoIOFResgate = Convert.ToByte(dropProjecaoIOFResgate.SelectedItem.Value);
            carteira.TipoTributacao = Convert.ToByte(dropTipoTributacao.SelectedItem.Value);
            carteira.CompensacaoPrejuizo = Convert.ToByte(dropCompensacaoPrejuizo.SelectedItem.Value);
            carteira.TipoCusto = Convert.ToByte(dropTipoCusto.SelectedItem.Value);
        }
        else
        {
            // Valore Defaults para clientes não Fundo/Clube/Fdic
            carteira.CalculaIOF = "N";
            carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Mellon;
            carteira.ProjecaoIRResgate = 1;
            carteira.ProjecaoIRComeCotas = 1;
            carteira.ProjecaoIOFResgate = 1;
            carteira.TipoTributacao = (byte)TipoTributacaoFundo.CurtoPrazo;
            carteira.CompensacaoPrejuizo = 1;
            carteira.TipoCusto = (byte)TipoCustoFundo.Aplicacao;
        }
        #endregion

        #region TabPage Informações Avançada - Parâmetros Fundo/Clube
        if (isCliente)
        { // Só pega se for Fundo/Clube/FDIC - Campos Obrigatórios
            carteira.DiasCotizacaoAplicacao = Convert.ToByte(textDiasCotizacaoAplicacao.Text);
            carteira.DiasCotizacaoResgate = Convert.ToByte(textDiasCotizacaoResgate.Text);
            carteira.DiasLiquidacaoAplicacao = Convert.ToByte(textDiasLiquidacaoAplicacao.Text);
            carteira.DiasLiquidacaoResgate = Convert.ToByte(textDiasLiquidacaoResgate.Text);
            carteira.ContagemDiasConversaoResgate = Convert.ToByte(dropContagemDiasCotizacaoResgate.SelectedItem.Value);
            carteira.PrioridadeOperacao = Convert.ToByte(dropPrioridadeOperacao.SelectedItem.Value);
            carteira.TipoVisaoFundo = Convert.ToByte(dropTipoVisaoFundo.SelectedItem.Value);
            carteira.DistribuicaoDividendo = Convert.ToByte(dropDistribuicaoDividendo.SelectedItem.Value);
            carteira.TipoVisualizacaoResgCotista = Convert.ToString(dropTipoVisualizacaoResgCotista.SelectedItem.Value);
            carteira.LiberaAplicacao = Convert.ToString(dropLiberaAplicacao.SelectedItem.Value);
            carteira.LiberaResgate = Convert.ToString(dropLiberaResgate.SelectedItem.Value);
            carteira.PossuiResgateAutomatico = Convert.ToString(dropPossuiResgateAutomatico.SelectedItem.Value);
            carteira.ContaPrzIOFVirtual = Convert.ToInt16(dropTipoVisaoFundo.SelectedItem.Value);
        }
        else
        { // Valore Defaults para clientes não Fundo/Clube/Fdic
            carteira.DiasCotizacaoAplicacao = 0;
            carteira.DiasCotizacaoResgate = 0;
            carteira.ContagemDiasConversaoResgate = 0;
            carteira.DiasLiquidacaoAplicacao = 0;
            carteira.DiasLiquidacaoResgate = 0;
            carteira.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;
            carteira.TipoVisaoFundo = 1;
            carteira.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;
            carteira.TipoVisualizacaoResgCotista = TipoVisualizacaoResgCotista.Consolidado;
            carteira.LiberaAplicacao = "S";
            carteira.LiberaResgate = "S";
            carteira.PossuiResgateAutomatico = "N";
        }

        //
        carteira.CodigoIsin = textCodigoISIN.Text;
        carteira.CodigoAnbid = textCodigoAnbid.Text;
        carteira.CodigoCetip = textCodigoCetip.Text;
        carteira.CodigoBloomberg = textCodigoBloomberg.Text;
        carteira.CodigoSTI = textCodigoSTI.Text;
        carteira.ValorMinimoAplicacao = textValorMinimoAplicacao.Text != "" ? Convert.ToDecimal(textValorMinimoAplicacao.Text) : 0;
        carteira.ValorMinimoResgate = textValorMinimoResgate.Text != "" ? Convert.ToDecimal(textValorMinimoResgate.Text) : 0;
        carteira.ValorMinimoSaldo = textValorMinimoSaldo.Text != "" ? Convert.ToDecimal(textValorMinimoSaldo.Text) : 0;
        carteira.ValorMinimoInicial = textValorMinimoInicial.Text != "" ? Convert.ToDecimal(textValorMinimoInicial.Text) : 0;
        carteira.ValorMaximoAplicacao = textValorMaximoAplicacao.Text != "" ? Convert.ToDecimal(textValorMaximoAplicacao.Text) : 0; if (chkHorarioFim.Checked)
        {
            carteira.HorarioFim = Convert.ToDateTime("1900-01-01 " + textHorarioFim.Text);
        }
        else
        {
            carteira.HorarioFim = null;
        }
        if (chkHorarioFimResgate.Checked)
        {
            carteira.HorarioFimResgate = Convert.ToDateTime("1900-01-01 " + textHorarioFimResgate.Text);
        }
        else
        {
            carteira.HorarioFimResgate = null;
        }

        if (dropPerfilRisco.SelectedIndex > -1)
        {
            carteira.PerfilRisco = Convert.ToInt16(dropPerfilRisco.SelectedItem.Value);

            #region Atualiza histórico carteira suitability
            CarteiraSuitabilityHistorico.createCarteiraSuitabilityHistorico(carteira, TipoOperacaoBanco.Inclusão);
            #endregion
        }

        if (dropEstrategia.SelectedIndex != -1)
        {
            carteira.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
        }
        else
        {
            carteira.IdEstrategia = null;
        }

        if (dropPrazoMedio.SelectedIndex == 0)
        {
            carteira.CalculaPrazoMedio = "N";
        }
        else
        {
            carteira.CalculaPrazoMedio = dropPrazoMedio.SelectedItem.Value.ToString();
        }

        if (dropRealizaCompensacao.SelectedIndex == -1)
        {
            carteira.RealizaCompensacaoDePrejuizo = "N";
        }
        else
        {
            carteira.RealizaCompensacaoDePrejuizo = dropRealizaCompensacao.SelectedItem.Value.ToString();
        }

        if (dropProjecaoIRResgate.SelectedIndex != -1)
        {
            if (Convert.ToInt32(dropProjecaoIRResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                carteira.DiasAposResgateIR = Convert.ToInt32(textDiasAposResgateIR.Text);
            }
            else
            {
                carteira.DiasAposResgateIR = 0;
            }
        }

        if (dropProjecaoIRComeCotas.SelectedIndex != -1)
        {
            if (Convert.ToInt32(dropProjecaoIRComeCotas.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                carteira.DiasAposComeCotasIR = Convert.ToInt32(textDiasAposComeCotasIR.Text);
            }
            else
            {
                carteira.DiasAposComeCotasIR = 0;
            }
        }

        if (dropProjecaoIOFResgate.SelectedIndex != -1)
        {
            if (Convert.ToInt32(dropProjecaoIOFResgate.SelectedItem.Value) == (int)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                carteira.DiasAposResgateIOF = Convert.ToInt32(textDiasAposResgateIOF.Text);
            }
            else
            {
                carteira.DiasAposResgateIOF = 0;
            }
        }

        if (carteira.ContagemPrazoIOF.Value == (byte)ContagemDiasPrazoIOF.Personalizado)
        {
            carteira.DataInicioContIOF = Convert.ToInt16(dropDataInicioContIOF.SelectedItem.Value);
            carteira.DataFimContIOF = Convert.ToInt16(dropDataFimContIOF.SelectedItem.Value);
        }
        else
        {
            carteira.DataInicioContIOF = null;
            carteira.DataFimContIOF = null;
        }

        if (dropContaPrzIOFVirtual.SelectedIndex != -1)
            carteira.ContaPrzIOFVirtual = Convert.ToInt16(dropContaPrzIOFVirtual.SelectedItem.Value);

        int codicoCda = 0;
        if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
            carteira.CodigoCDA = codicoCda;

        if (dropTipoFundo.SelectedIndex > -1)
            carteira.TipoFundo = Convert.ToInt16(dropTipoFundo.SelectedItem.Value);

        if (dropRendimento.SelectedIndex > -1)
            carteira.Rendimento = Convert.ToInt16(dropRendimento.SelectedItem.Value);

        carteira.CodigoSTI = textCodigoSTI.Text.ToString();
        #endregion

        if (dropComeCotasEntreRegatesConversao.SelectedIndex != -1)
        {
            carteira.ComeCotasEntreRegatesConversao = dropComeCotasEntreRegatesConversao.SelectedItem.Value.ToString();
        }

        if (dropExportaGalgo.SelectedIndex != -1)
        {
            carteira.ExportaGalgo = dropExportaGalgo.SelectedItem.Value.ToString();
        }

        if (dropInfluenciaGestorLocalCvm.SelectedIndex != -1)
        {
            carteira.InfluenciaGestorLocalCvm = dropInfluenciaGestorLocalCvm.SelectedItem.Value.ToString();
        }

        if (dropInvestimentoColetivoCvm.SelectedIndex != -1)
        {
            carteira.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
        }

        #region Informações não presentes no Grid - carregado com Valores Defaults
        //HACK Preenchimento de Valores de Carteira
        carteira.TipoAniversario = 0;
        #endregion


        #region Atualiza histórico carteira suitability
        CarteiraSuitabilityHistorico.createCarteiraSuitabilityHistorico(carteira, TipoOperacaoBanco.Inclusão);
        #endregion

        carteira.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Carteira - Operacao: Insert Carteira: " + carteira.IdCarteira + UtilitarioWeb.ToString(carteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza o cliente correspondente (statusAtivo)
        Cliente cliente = new Cliente();
        if (cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text)))
        {
            cliente.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
            cliente.Save();
        }

        #region Atualiza Classe - Somente cliente for Off Shore
        if (clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PJ || clienteAux.IdTipo.Value == TipoClienteFixo.OffShore_PF)
        {

            ClassesOffShore classesOffShore = new ClassesOffShore();
            if (!classesOffShore.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text)))
                classesOffShore.IdClassesOffShore = carteira.IdCarteira;
            classesOffShore.Nome = Convert.ToString(textNomeClasse.Text);
            classesOffShore.DataInicio = Convert.ToDateTime(textDataInicioClasse.Text);
            if (chbSerieUnica.Checked)
                classesOffShore.SerieUnica = "S";
            else
                classesOffShore.SerieUnica = "N";

            if (dropFrequencia.SelectedIndex != -1)
                classesOffShore.FrequenciaSerie = Convert.ToInt32(dropFrequencia.Value);

            if (dropMoeda.SelectedIndex != -1)
                classesOffShore.Moeda = Convert.ToInt16(dropMoeda.Value);

            if (dropDomicilio.SelectedIndex != -1)
                classesOffShore.Domicilio = Convert.ToInt32(dropDomicilio.Value);

            if (dropIndexadorPerf.SelectedIndex != -1)
                classesOffShore.IndexadorPerf = Convert.ToInt16(dropIndexadorPerf.Value);

            if (dropFreqCalcPerformance.SelectedIndex != -1)
                classesOffShore.FreqCalcPerformance = Convert.ToInt32(dropFreqCalcPerformance.Value);

            if (dropHardSoft.SelectedIndex != -1)
                classesOffShore.HardSoft = Convert.ToInt32(dropHardSoft.Value);

            classesOffShore.PoliticaInvestimentos = Convert.ToString(textPoliticaInvestimentos.Text);
            classesOffShore.TaxaAdm = Convert.ToDecimal(textTaxaAdm.Value);
            classesOffShore.TaxaCustodia = Convert.ToDecimal(textTaxaCustodia.Value);
            classesOffShore.TaxaPerformance = Convert.ToDecimal(textTaxaPerformance.Value);
            classesOffShore.LockUp = Convert.ToInt32(textLockUp.Value);
            classesOffShore.PenalidadeResgate = Convert.ToDecimal(textPenalidadeResgate.Value);
            classesOffShore.VlMinimoAplicacao = Convert.ToDecimal(textVlMinimoAplicacao.Value);
            classesOffShore.VlMinimoResgate = Convert.ToDecimal(textVlMinimoResgate.Value);
            classesOffShore.VlMinimoPermanencia = Convert.ToDecimal(textVlMinimoPermanencia.Value);
            classesOffShore.QtdDiasConvAplicacao = Convert.ToInt32(textQtdDiasConvAplicacao.Value);
            classesOffShore.QtdDiasLiqFinAplic = Convert.ToInt32(textQtdDiasLiqFinAplic.Value);
            classesOffShore.QtdDiasConvResgate = Convert.ToInt32(textQtdDiasConvResgate.Value);
            classesOffShore.QtdDiasLiqFin = Convert.ToInt32(textQtdDiasLiqFin.Value);
            classesOffShore.HoldBack = Convert.ToDecimal(textHoldBack.Value);

            DateTime anivHoldBack = Convert.ToDateTime(textAnivHoldBack.Value);
            classesOffShore.AnivHoldBack = anivHoldBack.AddYears(1800);

            if (textSidePocket.Checked)
                classesOffShore.SidePocket = "S";
            else
                classesOffShore.SidePocket = "N";

            if (!string.IsNullOrEmpty(textPrazoRepeticaoCotas.Text) && Convert.ToInt32(textPrazoRepeticaoCotas.Text) > 0)
                classesOffShore.PrazoRepeticaoCotas = Convert.ToInt32(textPrazoRepeticaoCotas.Text);
            else
                classesOffShore.PrazoRepeticaoCotas = null;

            if (dropIndexadorPrevia.SelectedIndex != -1)
            {
                classesOffShore.IdIndicePrevia = Convert.ToInt16(dropIndexadorPrevia.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(textPrazoValidadePrevia.Text) && Convert.ToInt32(textPrazoValidadePrevia.Text) > 0)
                classesOffShore.PrazoValidadePrevia = Convert.ToInt32(textPrazoValidadePrevia.Text);
            else
            {
                classesOffShore.PrazoValidadePrevia = null;
                classesOffShore.IdIndicePrevia = null;
            }

            classesOffShore.Save();

            //Cria serie para a classe
            this.criaSerie(classesOffShore, carteira);
        }
        #endregion

    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        if (e.Column.FieldName == "FundoOffShore")
        {
            int idTipoCliente = Convert.ToInt32(e.GetListSourceFieldValue("IdTipo"));

            if (idTipoCliente == TipoClienteFixo.OffShore_PJ || idTipoCliente == TipoClienteFixo.OffShore_PF)
            {
                e.Value = "1";

            }
            else
            {
                e.Value = "2";
            }
        }
    }


    protected void gridCadastro_CustomButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCustomButtonEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        string idTipo = Convert.ToString(grid.GetRowValues(e.VisibleIndex, "IdTipo"));

        if (e.ButtonID == "Classe" && idTipo == "801")
        {
            e.Visible = DevExpress.Utils.DefaultBoolean.True;
        }
        else
        {
            e.Visible = DevExpress.Utils.DefaultBoolean.False;
        }
    }

    protected void gridCadastro_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {
        if (e.ButtonID != "Classe") return;

        ASPxGridView grid = sender as ASPxGridView;

        int idCarteira = Convert.ToInt32(grid.GetRowValues(e.VisibleIndex, "IdCarteira"));

        Cliente cliente = this.criaCliente(idCarteira);

        Carteira carteira = this.criaCarteira(idCarteira, cliente);

        ClassesOffShore classesOffShore = this.criaClasse(idCarteira, carteira);

        this.criaSerie(classesOffShore, carteira);

    }

    protected void criaSerie(ClassesOffShore classeOffShore, Carteira carteira)
    {
        SeriesOffShore serieOffShore = new SeriesOffShore();
        serieOffShore.IdClassesOffShore = classeOffShore.IdClassesOffShore;
        serieOffShore.Mnemonico = classeOffShore.Nome + " Inicial";
        serieOffShore.Descricao = classeOffShore.Nome + " Inicial";
        //serieOffShore.VlCotaInicial = carteira.CotaInicial.Value;
        serieOffShore.VlCotaBase = 1;
        serieOffShore.DataInicioAplicacao = classeOffShore.DataInicio.Value;
        serieOffShore.DataFimAplicacao = Calendario.RetornaUltimoDiaUtilMes(classeOffShore.DataInicio.Value).AddMonths(classeOffShore.FrequenciaSerie.Value - 1);
        serieOffShore.SerieBase = "S";
        serieOffShore.IdIndicePrevia = classeOffShore.IdIndicePrevia;
        serieOffShore.PrazoRepeticaoCotas = classeOffShore.PrazoRepeticaoCotas;
        serieOffShore.PrazoValidadePrevia = classeOffShore.PrazoValidadePrevia;

        serieOffShore.Save();
    }

    protected ClassesOffShore criaClasse(int idCarteira, Carteira carteira)
    {
        ClassesOffShore classeAntiga = new ClassesOffShore();
        classeAntiga.LoadByPrimaryKey(idCarteira);

        ClassesOffShore classesOffShore = new ClassesOffShore();
        #region Copia Classe
        foreach (esColumnMetadata colClasse in classeAntiga.es.Meta.Columns)
        {
            if (colClasse.Name.ToUpper().Trim().Equals("IDCLASSESOFFSHORE"))
            {
                classesOffShore.IdClassesOffShore = carteira.IdCarteira;
            }
            else if (colClasse.Name.ToUpper().Trim().Equals("NOME"))
            {
                classesOffShore.Nome = "Classe-" + carteira.IdCarteira.Value.ToString();
            }
            else if (colClasse.Name.ToUpper().Trim().Equals("DATAINICIO"))
            {
                classesOffShore.DataInicio = DateTime.Now;
            }
            else
                classesOffShore.SetColumn(colClasse.Name, classeAntiga.GetColumn(colClasse.Name));
        }
        #endregion

        classesOffShore.Save();


        //Nome e apelido devem vir direto de Pessoa
        Pessoa pessoa = new Pessoa();
        Cliente c = new Cliente();
        Carteira cart = new Carteira();
        c.LoadByPrimaryKey(Convert.ToInt32(carteira.IdCarteira.Value));
        cart.LoadByPrimaryKey(carteira.IdCarteira.Value);
        pessoa.LoadByPrimaryKey(Convert.ToInt32(c.IdPessoa));

        cart.Nome = pessoa.Nome + " - " + classesOffShore.Nome;
        cart.Apelido = pessoa.Apelido + " - " + classesOffShore.Nome;

        cart.Save();

        return classesOffShore;
    }

    /// <summary>
    /// Cria carteira para Fundos Off Shore
    /// </summary>
    /// <param name="idCarteiraAntiga"></param>
    /// <param name="cliente"></param>
    /// <returns></returns>
    protected Carteira criaCarteira(int idCarteiraAntiga, Cliente cliente)
    {
        Carteira carteiraAntiga = new Carteira();
        carteiraAntiga.LoadByPrimaryKey(idCarteiraAntiga);

        Carteira carteira = new Carteira();
        #region Copia carteira com ID novo
        foreach (esColumnMetadata colCarteira in carteiraAntiga.es.Meta.Columns)
        {
            if (colCarteira.Name.ToUpper().Trim().Equals("IDCARTEIRA"))
                carteira.IdCarteira = cliente.IdCliente.Value;
            else
                carteira.SetColumn(colCarteira.Name, carteiraAntiga.GetColumn(colCarteira.Name));
        }
        #endregion

        carteira.Save();

        return carteira;

    }

    /// <summary>
    /// Cria cliente para Fundos Off Shore
    /// </summary>
    /// <param name="idClienteAntigo"></param>
    /// <returns></returns>
    protected Cliente criaCliente(int idClienteAntigo)
    {
        Cliente clienteAntigo = new Cliente();
        clienteAntigo.LoadByPrimaryKey(idClienteAntigo);

        //Seleciona o último ID de cliente
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente.Max());
        clienteCollection.Query.Load();
        int idClienteNovo = clienteCollection[0].IdCliente.Value + 1;

        Cliente cliente = new Cliente(); ;
        #region Copia Cliente com ID novo
        foreach (esColumnMetadata colCliente in clienteAntigo.es.Meta.Columns)
        {
            if (colCliente.Name.ToUpper().Trim().Equals("IDCLIENTE"))
                cliente.IdCliente = idClienteNovo;
            else
                cliente.SetColumn(colCliente.Name, clienteAntigo.GetColumn(colCliente.Name));
        }
        #endregion

        cliente.Save();

        bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

        #region Se tem permisão automática para usuários internos, cria permissão para todos os usuários (internos) do sistema
        if (permissaoInternoAuto && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
        {
            GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
            usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Load(usuarioQuery);

            PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
            foreach (Usuario usuarioPermissao in usuarioCollection)
            {
                int idUsuario = usuarioPermissao.IdUsuario.Value;

                PermissaoCliente permissaoCliente = new PermissaoCliente();
                permissaoCliente.IdCliente = cliente.IdCliente;
                permissaoCliente.IdUsuario = idUsuario;
                permissaoClienteCollection.AttachEntity(permissaoCliente);
            }
            permissaoClienteCollection.Save();
        }
        #endregion

        return cliente;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {

        if (e.Parameters == "btnDelete")
        {
            #region Delete
            CarteiraCollection carteiraCollection = new CarteiraCollection();

            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);

                #region Taxa Global
                TaxaGlobalQuery taxaGlobalQuery = new TaxaGlobalQuery("taxaGlobal");
                TaxaGlobalCollection taxaGlobalColl = new TaxaGlobalCollection();
                TabelaTaxaAdministracaoQuery taxaADMQuery = new TabelaTaxaAdministracaoQuery("taxaADM");
                taxaGlobalQuery.es.Distinct = true;
                taxaGlobalQuery.Select(taxaGlobalQuery);
                taxaGlobalQuery.InnerJoin(taxaADMQuery).On(taxaGlobalQuery.IdTabelaGlobal.Equal(taxaADMQuery.IdTabela)
                                                           | taxaGlobalQuery.IdTabelaAssociada.Equal(taxaADMQuery.IdTabela));
                taxaGlobalQuery.Where(taxaADMQuery.IdCarteira.Equal(idCarteira));

                if (taxaGlobalColl.Load(taxaGlobalQuery))
                {
                    taxaGlobalColl.MarkAllAsDeleted();
                    taxaGlobalColl.Save();
                }
                #endregion

                #region CalculoAdministracaoAssociada
                CalculoAdministracaoAssociadaQuery calculoADMQuery = new CalculoAdministracaoAssociadaQuery("calculoADM");
                CalculoAdministracaoAssociadaCollection calculoADMColl = new CalculoAdministracaoAssociadaCollection();
                taxaADMQuery = new TabelaTaxaAdministracaoQuery("taxaADM");
                calculoADMQuery.es.Distinct = true;
                calculoADMQuery.Select(calculoADMQuery);
                calculoADMQuery.InnerJoin(taxaADMQuery).On(calculoADMQuery.IdTabela.Equal(taxaADMQuery.IdTabela));
                calculoADMQuery.Where(taxaADMQuery.IdCarteira.Equal(idCarteira));

                if (calculoADMColl.Load(calculoADMQuery))
                {
                    calculoADMColl.MarkAllAsDeleted();
                    calculoADMColl.Save();
                }
                #endregion

                #region CalculoAdministracaoAssociadaHistorico
                TabelaExcecaoAdmQuery tabelaExcecaoQuery = new TabelaExcecaoAdmQuery("calculoADM");
                TabelaExcecaoAdmCollection tabelaExcecaoColl = new TabelaExcecaoAdmCollection();
                taxaADMQuery = new TabelaTaxaAdministracaoQuery("taxaADM");
                tabelaExcecaoQuery.es.Distinct = true;
                tabelaExcecaoQuery.Select(tabelaExcecaoQuery);
                tabelaExcecaoQuery.InnerJoin(taxaADMQuery).On(tabelaExcecaoQuery.IdTabela.Equal(taxaADMQuery.IdTabela));
                tabelaExcecaoQuery.Where(taxaADMQuery.IdCarteira.Equal(idCarteira));

                if (tabelaExcecaoColl.Load(tabelaExcecaoQuery))
                {
                    tabelaExcecaoColl.MarkAllAsDeleted();
                    tabelaExcecaoColl.Save();
                }
                #endregion

                #region CalculoAdministracaoAssociadaHistorico
                CalculoAdministracaoAssociadaHistoricoQuery calculoADMHistQuery = new CalculoAdministracaoAssociadaHistoricoQuery("calculoADM");
                CalculoAdministracaoAssociadaHistoricoCollection calculoADMHistColl = new CalculoAdministracaoAssociadaHistoricoCollection();
                taxaADMQuery = new TabelaTaxaAdministracaoQuery("taxaADM");
                calculoADMHistQuery.es.Distinct = true;
                calculoADMHistQuery.Select(calculoADMHistQuery);
                calculoADMHistQuery.InnerJoin(taxaADMQuery).On(calculoADMHistQuery.IdTabela.Equal(taxaADMQuery.IdTabela));
                calculoADMHistQuery.Where(taxaADMQuery.IdCarteira.Equal(idCarteira));

                if (calculoADMHistColl.Load(calculoADMHistQuery))
                {
                    calculoADMHistColl.MarkAllAsDeleted();
                    calculoADMHistColl.Save();
                }
                #endregion

                #region FundoInvestimentoFormaCondominio
                FundoInvestimentoFormaCondominioCollection fundoInvestimentoFormaCondominioColl = new FundoInvestimentoFormaCondominioCollection();
                fundoInvestimentoFormaCondominioColl.Query.Where(fundoInvestimentoFormaCondominioColl.Query.IdCarteira.Equal(idCarteira));

                if (fundoInvestimentoFormaCondominioColl.Query.Load())
                {
                    fundoInvestimentoFormaCondominioColl.MarkAllAsDeleted();
                    fundoInvestimentoFormaCondominioColl.Save();
                }
                #endregion

                #region DesenquadramentoTributario
                DesenquadramentoTributarioCollection desenquadramentoTributarioColl = new DesenquadramentoTributarioCollection();
                desenquadramentoTributarioColl.Query.Where(desenquadramentoTributarioColl.Query.IdCarteira.Equal(idCarteira));

                if (desenquadramentoTributarioColl.Query.Load())
                {
                    desenquadramentoTributarioColl.MarkAllAsDeleted();
                    desenquadramentoTributarioColl.Save();
                }
                #endregion

                #region AgendaComeCotas
                AgendaComeCotasCollection agendaComeCotasColl = new AgendaComeCotasCollection();
                agendaComeCotasColl.Query.Where(agendaComeCotasColl.Query.IdCarteira.Equal(idCarteira));

                if (agendaComeCotasColl.Query.Load())
                {
                    agendaComeCotasColl.MarkAllAsDeleted();
                    agendaComeCotasColl.Save();
                }
                #endregion

                #region TravamentoCotas
                TravamentoCotasCollection travamentoCotasColl = new TravamentoCotasCollection();
                travamentoCotasColl.Query.Where(travamentoCotasColl.Query.IdCarteira.Equal(idCarteira));

                if (travamentoCotasColl.Query.Load())
                {
                    travamentoCotasColl.MarkAllAsDeleted();
                    travamentoCotasColl.Save();
                }
                #endregion

                #region EventoFundo
                CalculoRebateImpactaPLCollection calculoRebateImpactaPLColl = new CalculoRebateImpactaPLCollection();
                calculoRebateImpactaPLColl.Query.Where(calculoRebateImpactaPLColl.Query.IdCarteira.Equal(idCarteira));

                if (calculoRebateImpactaPLColl.Query.Load())
                {
                    calculoRebateImpactaPLColl.MarkAllAsDeleted();
                    calculoRebateImpactaPLColl.Save();
                }
                #endregion

                #region EventoFundo
                EventoFundoCollection eventoFundoColl = new EventoFundoCollection();
                eventoFundoColl.Query.Where(eventoFundoColl.Query.IdCarteiraDestino.Equal(idCarteira) | eventoFundoColl.Query.IdCarteiraOrigem.Equal(idCarteira));

                if (eventoFundoColl.Query.Load())
                {
                    eventoFundoColl.MarkAllAsDeleted();
                    eventoFundoColl.Save();
                }
                #endregion

                TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
                tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira.Equal(idCarteira));
                tabelaTaxaAdministracaoCollection.Query.Load();
                tabelaTaxaAdministracaoCollection.MarkAllAsDeleted();
                tabelaTaxaAdministracaoCollection.Save();

                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCarteira));
                tabelaTaxaPerformanceCollection.Query.Load();
                tabelaTaxaPerformanceCollection.MarkAllAsDeleted();
                tabelaTaxaPerformanceCollection.Save();

                TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
                tabelaProvisaoCollection.Query.Where(tabelaProvisaoCollection.Query.IdCarteira.Equal(idCarteira));
                tabelaProvisaoCollection.Query.Load();
                tabelaProvisaoCollection.MarkAllAsDeleted();
                tabelaProvisaoCollection.Save();

                SeriesOffShoreCollection seriesOffShoreCollection = new SeriesOffShoreCollection();
                seriesOffShoreCollection.Query.Where(seriesOffShoreCollection.Query.IdClassesOffShore.Equal(idCarteira));
                seriesOffShoreCollection.Query.Load();
                seriesOffShoreCollection.MarkAllAsDeleted();
                seriesOffShoreCollection.Save();

                ClassesOffShoreCollection classesOffShoreCollection = new ClassesOffShoreCollection();
                classesOffShoreCollection.Query.Where(classesOffShoreCollection.Query.IdClassesOffShore.Equal(idCarteira));
                classesOffShoreCollection.Query.Load();
                classesOffShoreCollection.MarkAllAsDeleted();
                classesOffShoreCollection.Save();

                PerfilClienteCollection perfilClienteColl = new PerfilClienteCollection();
                perfilClienteColl.Query.Where(perfilClienteColl.Query.IdCarteira.Equal(idCarteira));
                perfilClienteColl.Query.Load();
                perfilClienteColl.MarkAllAsDeleted();
                perfilClienteColl.Save();

                PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoColl = new PosicaoCotistaHistoricoCollection();
                posicaoCotistaHistoricoColl.Query.Where(posicaoCotistaHistoricoColl.Query.IdCarteira.Equal(idCarteira));
                posicaoCotistaHistoricoColl.Query.Load();
                posicaoCotistaHistoricoColl.MarkAllAsDeleted();
                posicaoCotistaHistoricoColl.Save();

                PosicaoCotistaAberturaCollection posicaoCotistaAberturaColl = new PosicaoCotistaAberturaCollection();
                posicaoCotistaAberturaColl.Query.Where(posicaoCotistaAberturaColl.Query.IdCarteira.Equal(idCarteira));
                posicaoCotistaAberturaColl.Query.Load();
                posicaoCotistaAberturaColl.MarkAllAsDeleted();
                posicaoCotistaAberturaColl.Save();

                CarteiraMaeCollection carteiraMaeColl = new CarteiraMaeCollection();
                carteiraMaeColl.Query.Where(carteiraMaeColl.Query.IdCarteiraFilha.Equal(idCarteira) || carteiraMaeColl.Query.IdCarteiraMae.Equal(idCarteira));

                if (carteiraMaeColl.Query.Load())
                {
                    carteiraMaeColl.MarkAllAsDeleted();
                    carteiraMaeColl.Save();
                }

                Carteira carteira = new Carteira();
                if (carteira.LoadByPrimaryKey(idCarteira))
                {
                    carteiraCollection.AttachEntity(carteira);
                }
            }

            CarteiraCollection carteiraCollectionClone = (CarteiraCollection)Utilitario.Clone(carteiraCollection);
            //

            // Delete
            carteiraCollection.MarkAllAsDeleted();
            carteiraCollection.Save();

            foreach (Carteira c in carteiraCollectionClone)
            {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Carteira - Operacao: Delete Carteira: " + c.IdCarteira + UtilitarioWeb.ToString(c),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }
        else if (e.Parameters == "btnAtivar")
        {
            #region Ativa Carteira

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);
            //
            // Se tiver carteira Selecionada
            if (keyValuesId.Count != 0)
            {
                #region Carteira
                CarteiraCollection carteiraCollection = new CarteiraCollection();
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.StatusAtivo)
                                  .Where(carteiraCollection.Query.IdCarteira.In(keyValuesId));

                carteiraCollection.Query.Load();

                for (int i = 0; i < carteiraCollection.Count; i++)
                {
                    carteiraCollection[i].StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
                }
                #endregion

                #region Cliente
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.StatusAtivo)
                                 .Where(clienteCollection.Query.IdCliente.In(keyValuesId));

                clienteCollection.Query.Load();

                for (int i = 0; i < clienteCollection.Count; i++)
                {
                    clienteCollection[i].StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                }
                #endregion

                using (esTransactionScope scope = new esTransactionScope())
                {
                    carteiraCollection.Save();
                    clienteCollection.Save();

                    foreach (Carteira cart in carteiraCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Carteira - Operacao: Ativação: " + cart.IdCarteira + ";" + UtilitarioWeb.ToString(cart),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    foreach (Cliente c in clienteCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cliente - Operacao: Ativação: " + c.IdCliente + ";" + UtilitarioWeb.ToString(c),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    scope.Complete();
                }
            }
            #endregion
        }
        else if (e.Parameters == "btnDesativar")
        {
            #region Desativa Cliente

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);
            //
            // Se tiver carteira Selecionada
            if (keyValuesId.Count != 0)
            {
                #region Carteira
                CarteiraCollection carteiraCollection = new CarteiraCollection();
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.StatusAtivo)
                                 .Where(carteiraCollection.Query.IdCarteira.In(keyValuesId));

                carteiraCollection.Query.Load();

                for (int i = 0; i < carteiraCollection.Count; i++)
                {
                    carteiraCollection[i].StatusAtivo = (byte)StatusAtivoCarteira.Inativo;
                }
                #endregion

                #region Cliente
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.StatusAtivo)
                                 .Where(clienteCollection.Query.IdCliente.In(keyValuesId));

                clienteCollection.Query.Load();

                for (int i = 0; i < clienteCollection.Count; i++)
                {
                    clienteCollection[i].StatusAtivo = (byte)StatusAtivoCliente.Inativo;
                }
                #endregion

                using (esTransactionScope scope = new esTransactionScope())
                {
                    carteiraCollection.Save();
                    clienteCollection.Save();

                    foreach (Carteira cart in carteiraCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Carteira - Operacao: Desativação: " + cart.IdCarteira + ";" + UtilitarioWeb.ToString(cart),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    foreach (Cliente c in clienteCollection)
                    {

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cliente - Operacao: Desativação: " + c.IdCliente + ";" + UtilitarioWeb.ToString(c),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    scope.Complete();
                }

            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            ASPxDateEdit textDataInicio = pageControl.FindControl("textDataInicio") as ASPxDateEdit;
            e.Properties["cpTextDataInicio"] = textDataInicio.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        bool isOnTabPage = true;
        this.FocaCampoGrid("btnEditCodigoCliente", "textDataInicio", isOnTabPage);
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// TabControl - Controla visibilidade da 3 aba
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void tabCadastroOnLoad(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = sender as ASPxPageControl;
        ASPxComboBox dropTipoFundo = (ASPxComboBox)pageControl.FindControl("dropTipoFundo");
        ASPxSpinEdit textCotaInicial = pageControl.FindControl("textCotaInicial") as ASPxSpinEdit;
        ASPxLabel labelTipoFundoAtual = (ASPxLabel)pageControl.FindControl("labelTipoFundoAtual");
        ASPxLabel labelTipoFundoAtualTitle = pageControl.FindControl("labelTipoFundoAtualTitulo") as ASPxLabel;
        ASPxComboBox dropTipoTributacao = pageControl.FindControl("dropTipoTributacao") as ASPxComboBox;
        ASPxLabel labelTributacaoAtualTitle = (ASPxLabel)pageControl.FindControl("labelTributacaoAtualTitle");
        ASPxLabel labelTributacaoAtual = (ASPxLabel)pageControl.FindControl("labelTributacaoAtual");

        labelTipoFundoAtual.Visible = false;
        labelTipoFundoAtualTitle.Visible = false;

        //Somente no Update
        if (!gridCadastro.IsNewRowEditing)
        {
            int idCarteira = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, CarteiraMetadata.ColumnNames.IdCarteira));

            #region Habilitar/Desabilitar dropTipoFundo


            if (ExisteEventoFormaCondominio(idCarteira))
            {
                dropTipoFundo.Enabled = false;
                dropTipoFundo.ToolTip = "Edição proibida: existem eventos de condominio cadastrados para esta carteira";

                int? condominio = RetornaCondominioCarteira(idCarteira);
                if (condominio != null)
                {
                    labelTipoFundoAtual.Text = TipoFundoDescricao.RetornaStringValue(condominio.Value);
                    labelTipoFundoAtual.Visible = true;
                    labelTipoFundoAtualTitle.Visible = true;
                }
            }

            #endregion

            #region   Habilitar/Desabilitar dropTipoTributacao


            if (ExisteEventoTributacao(idCarteira))
            {
                dropTipoTributacao.Enabled = false;
                dropTipoTributacao.ToolTip = "Edição proibida: existem eventos de tributação cadastrados para esta carteira";

                int? tributacao = RetornaTributacaoCarteira(idCarteira);
                if (tributacao != null)
                {
                    labelTributacaoAtual.Text = ClassificacaoTributariaDescricao.RetornaStringValue(tributacao.Value);
                    labelTributacaoAtual.Visible = true;
                    labelTributacaoAtualTitle.Visible = true;
                }
            }

            #endregion

            #region Carregar abas conforme tipo de cliente

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            pageControl.TabPages[2].ClientVisible = cliente.IdTipo.Value == TipoClienteFixo.Clube ||
                                                    cliente.IdTipo.Value == TipoClienteFixo.FDIC ||
                                                    cliente.IdTipo.Value == TipoClienteFixo.Fundo;

            pageControl.TabPages[3].ClientVisible = cliente.IdTipo.Value == TipoClienteFixo.Clube ||
                                                    cliente.IdTipo.Value == TipoClienteFixo.FDIC ||
                                                    cliente.IdTipo.Value == TipoClienteFixo.Fundo;

            pageControl.TabPages[4].ClientVisible = cliente.IdTipo.Value == TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == TipoClienteFixo.OffShore_PF;
            pageControl.TabPages[5].ClientVisible = cliente.IdTipo.Value == TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == TipoClienteFixo.OffShore_PF;

            textCotaInicial.ClientEnabled = cliente.IdTipo.Value != TipoClienteFixo.OffShore_PJ && cliente.IdTipo.Value != TipoClienteFixo.OffShore_PF;
            #endregion

        }
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridBuscaFundo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void textHorarioFim_DataBinding(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxTimeEdit textHorarioFim = pageControl.FindControl("textHorarioFim") as ASPxTimeEdit;
        ASPxCheckBox chkHorarioFim = pageControl.FindControl("chkHorarioFim") as ASPxCheckBox;
        if (textHorarioFim.Value == null)
        {
            textHorarioFim.ClientEnabled = false;
            chkHorarioFim.Checked = false;
        }
        else
        {
            textHorarioFim.ClientEnabled = true;
            chkHorarioFim.Checked = true;
        }
    }

    protected void textHorarioFimResgate_DataBinding(object sender, EventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxTimeEdit textHorarioFimResgate = pageControl.FindControl("textHorarioFimResgate") as ASPxTimeEdit;
        ASPxCheckBox chkHorarioFimResgate = pageControl.FindControl("chkHorarioFimResgate") as ASPxCheckBox;
        if (textHorarioFimResgate.Value == null)
        {
            textHorarioFimResgate.ClientEnabled = false;
            chkHorarioFimResgate.Checked = false;
        }
        else
        {
            textHorarioFimResgate.ClientEnabled = true;
            chkHorarioFimResgate.Checked = true;
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

    /// <summary>
    /// Carrega os tipos de Fundos Disponíveis(Aberto ou Fechado)
    /// </summary>
    protected void dropTipoFundo_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoFundo = (ASPxComboBox)sender;
        foreach (int r in Enum.GetValues(typeof(TipoFundo)))
        {
            dropTipoFundo.Items.Add(TipoFundoDescricao.RetornaStringValue(r), r.ToString());
        }
        dropTipoFundo.SelectedIndex = 1;
    }

    /// <summary>
    /// Verifica se a carteira possui um evento anterior a data de parâmetro
    /// </summary>
    public static int? RetornaCondominioCarteira(int idCarteira)
    {
        int? condominioCarteira = null;

        FundoInvestimentoFormaCondominioQuery formaCondominioQuery = new FundoInvestimentoFormaCondominioQuery("cond");
        ClienteQuery clienteQuery = new ClienteQuery("cli");
        formaCondominioQuery.InnerJoin(clienteQuery).On(formaCondominioQuery.IdCarteira == clienteQuery.IdCliente);
        formaCondominioQuery.es.Top = 1;
        formaCondominioQuery.OrderBy(formaCondominioQuery.DataInicioVigencia.Descending);
        formaCondominioQuery.Where(formaCondominioQuery.IdCarteira == idCarteira);
        formaCondominioQuery.Where(formaCondominioQuery.DataInicioVigencia.Date().LessThanOrEqual(clienteQuery.DataDia.Date()));

        FundoInvestimentoFormaCondominioCollection formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
        formaCondominioCollection.Load(formaCondominioQuery);

        if (formaCondominioCollection.Count > 0)
            condominioCarteira = (int)formaCondominioCollection[0].FormaCondominio;

        return condominioCarteira;
    }

    public static bool ExisteEventoFormaCondominio(int idCarteira)
    {
        FundoInvestimentoFormaCondominioCollection coll = new FundoInvestimentoFormaCondominioCollection();
        coll.Query.Where(coll.Query.IdCarteira == idCarteira);
        coll.Query.es.Top = 1;
        coll.Query.Load();
        return coll.Count > 0;

    }

    /// <summary>
    /// Verifica se a carteira possui um evento anterior a data de parâmetro
    /// </summary>
    public static int? RetornaTributacaoCarteira(int idCarteira)
    {
        int? tributacaoCarteira = null;

        DesenquadramentoTributarioQuery desenquadramentoTributarioQuery = new DesenquadramentoTributarioQuery("des");
        ClienteQuery clienteQuery = new ClienteQuery("cli");
        desenquadramentoTributarioQuery.InnerJoin(clienteQuery).On(desenquadramentoTributarioQuery.IdCarteira == clienteQuery.IdCliente);
        desenquadramentoTributarioQuery.es.Top = 1;
        desenquadramentoTributarioQuery.OrderBy(desenquadramentoTributarioQuery.Data.Descending);
        desenquadramentoTributarioQuery.Where(desenquadramentoTributarioQuery.IdCarteira == idCarteira);
        desenquadramentoTributarioQuery.Where(desenquadramentoTributarioQuery.Data.Date().LessThanOrEqual(clienteQuery.DataDia.Date()));

        DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
        desenquadramentoTributarioCollection.Load(desenquadramentoTributarioQuery);


        if (desenquadramentoTributarioCollection.Count > 0)
            tributacaoCarteira = (int)desenquadramentoTributarioCollection[0].ClassificacaoTributariaAtual;

        return tributacaoCarteira;
    }



    public static bool ExisteEventoTributacao(int idCarteira)
    {
        DesenquadramentoTributarioCollection coll = new DesenquadramentoTributarioCollection();
        coll.Query.Where(coll.Query.IdCarteira == idCarteira);
        coll.Query.es.Top = 1;
        coll.Query.Load();
        return coll.Count > 0;
    }



    /// <summary>
    /// Bloqueia campo caso ele possua algum evento cadastrado
    /// </summary>
    protected void dropTipoTributacao_OnDataBound(object sender, EventArgs e)
    {
        #region Desabilita Controle
        //Verifica se possui evento cadastrado, se já existir desabilita o controle

        #endregion
    }











    //protected void gridClasses_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    //{
    //    string errorTabCadastro = this.validaGridCadastro();
    //    if (errorTabCadastro != "") e.RowError = "Informações Básicas: " + errorTabCadastro;

    //    ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
    //    ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;

    //    if (btnEditCodigoCliente.Text == "") e.RowError = "Favor informar o cliente da carteira na pasta Informações Básicas I.";

    //    if (e.NewValues["Nome"] == null) e.RowError = "Favor preencher nome da classe.";
    //    if (e.NewValues["DataInicio"] == null) e.RowError = "Favor preencher Data de Início.";

    //    if (e.NewValues["LockUp"] != null && e.NewValues["HardSoft"] == null) e.RowError = "Você informou valor em carência de resgate. Favor indentificar se deverá ser Hard ou Soft.";

    //    if (e.NewValues["HoldBack"] != null && Convert.ToDecimal(e.NewValues["HoldBack"]) > 0 && e.NewValues["AnivHoldBack"] == null) e.RowError = "Favor informar o Aniversário Hold Back.";
    //}


    //protected void gridClasses_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    //{
    //    ASPxGridView grid = sender as ASPxGridView;

    //    ClassesOffShore classeOffShore = new ClassesOffShore();

    //    if (classeOffShore.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
    //    {
    //        this.AtualizaClasse(classeOffShore, e.NewValues);
    //    }

    //    e.Cancel = true;
    //    grid.CancelEdit();
    //    grid.DataBind();

    //}
    //protected void gridClasses_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    //{
    //    this.SalvarNovo();

    //    ClassesOffShore classeOffShore = new ClassesOffShore();

    //    this.AtualizaClasse(classeOffShore, e.NewValues);

    //    ASPxGridView grid = sender as ASPxGridView;

    //    e.Cancel = true;
    //    grid.CancelEdit();
    //    grid.DataBind();
    //}

    //private void AtualizaClasse(ClassesOffShore classeOffShore, System.Collections.Specialized.OrderedDictionary newValues)
    //{
    //    ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
    //    ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;

    //    classeOffShore.IdClassesOffShore = Convert.ToInt32(btnEditCodigoCliente.Text);
    //    classeOffShore.Nome = Convert.ToString(newValues["Nome"]);
    //    classeOffShore.DataInicio = Convert.ToDateTime(newValues["DataInicio"]);
    //    classeOffShore.SerieUnica = Convert.ToString(newValues["SerieUnica"]);
    //    classeOffShore.PoliticaInvestimentos = Convert.ToString(newValues["PoliticaInvestimentos"]);
    //    if (newValues["Moeda"] != null) classeOffShore.Moeda = Convert.ToInt16(newValues["Moeda"]);
    //    if (newValues["Domicilio"] != null) classeOffShore.Domicilio = Convert.ToInt16(newValues["Domicilio"]);
    //    classeOffShore.TaxaAdm = Convert.ToDecimal(newValues["TaxaAdm"]);
    //    classeOffShore.TaxaCustodia = Convert.ToDecimal(newValues["TaxaCustoDia"]);
    //    classeOffShore.TaxaPerformance = Convert.ToDecimal(newValues["TaxaPerformance"]);
    //    if (newValues["IndexadorPerf"] != null) classeOffShore.IndexadorPerf = Convert.ToInt16(newValues["IndexadorPerf"]);
    //    if (newValues["FreqCalcPerformance"] != null) classeOffShore.FreqCalcPerformance = Convert.ToInt32(newValues["FreqCalcPerformance"]);
    //    classeOffShore.LockUp = Convert.ToInt32(newValues["LockUp"]);
    //    classeOffShore.HardSoft = Convert.ToInt32(newValues["HardSoft"]);
    //    classeOffShore.PenalidadeResgate = Convert.ToDecimal(newValues["PenalidadeResgate"]);
    //    classeOffShore.VlMinimoAplicacao = Convert.ToDecimal(newValues["VlMinimoAplicacao"]);
    //    classeOffShore.VlMinimoResgate = Convert.ToDecimal(newValues["VlMinimoResgate"]);
    //    classeOffShore.VlMinimoPermanencia = Convert.ToDecimal(newValues["VlMinimoPermanencia"]);
    //    classeOffShore.QtdDiasConvResgate = Convert.ToInt32(newValues["QtdDiasConvResgate"]);
    //    classeOffShore.QtdDiasLiqFin = Convert.ToInt32(newValues["QtdDiasLiqFin"]);
    //    classeOffShore.HoldBack = Convert.ToDecimal(newValues["HoldBack"]);
    //    classeOffShore.SidePocket = Convert.ToString(newValues["SidePocket"]);

    //    if (newValues["AnivHoldBack"] != null)
    //    {
    //        DateTime anivHoldBack = Convert.ToDateTime(newValues["AnivHoldBack"]);
    //        classeOffShore.AnivHoldBack = anivHoldBack.AddYears(1800);
    //    }

    //    classeOffShore.Save();

    //    return;
    //}

    protected void gridSeries_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string errorTabCadastro = this.validaGridCadastro();
        if (errorTabCadastro != "") e.RowError = "Informações Básicas: " + errorTabCadastro;

        if (e.NewValues["VlCotaBase"] == null)
            e.RowError = "Favor informar valor d cota base";
    }

    protected void gridSeries_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "SerieInativa")
        {
            string inativa = Convert.ToString(e.GetListSourceFieldValue("Inativa"));
            if (inativa.Equals("S"))
            {
                e.Value = true;
            }
            else
            {
                e.Value = false;
            }
        }

        if (e.Column.FieldName == "Base")
        {
            string serieBase = Convert.ToString(e.GetListSourceFieldValue("SerieBase"));
            if (serieBase.Equals("S"))
            {
                e.Value = true;
            }
            else
            {
                e.Value = false;
            }
        }

    }

    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues[CarteiraMetadata.ColumnNames.TipoVisualizacaoResgCotista] = ParametrosConfiguracaoSistema.Outras.TipoVisualizacaoFundoResgateCotista;
    }

    protected void gridSeries_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        ASPxGridView grid = sender as ASPxGridView;

        SeriesOffShore seriesOffShore = new SeriesOffShore();

        this.AtualizaSeries(seriesOffShore, e.NewValues);

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    protected void gridSeries_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        SeriesOffShore seriesOffShore = new SeriesOffShore();

        if (seriesOffShore.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
        {
            this.AtualizaSeries(seriesOffShore, e.NewValues);
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    private void AtualizaSeries(SeriesOffShore seriesOffShore, System.Collections.Specialized.OrderedDictionary newValues)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnEditCodigoCliente = pageControl.FindControl("btnEditCodigoCliente") as ASPxSpinEdit;

        seriesOffShore.IdClassesOffShore = Convert.ToInt32(btnEditCodigoCliente.Text);
        seriesOffShore.Mnemonico = Convert.ToString(newValues["Mnemonico"]);
        seriesOffShore.Descricao = Convert.ToString(newValues["Descricao"]);
        seriesOffShore.VlCotaInicial = Convert.ToDecimal(newValues["VlCotaInicial"]);
        seriesOffShore.Isin = Convert.ToString(newValues["Isin"]);
        if (newValues["DataRollUp"] != null)
            seriesOffShore.DataRollUp = Convert.ToDateTime(newValues["DataRollUp"]);

        if (newValues["PrazoRepeticaoCotas"] != null && Convert.ToInt32(newValues["PrazoRepeticaoCotas"]) > 0)
            seriesOffShore.PrazoRepeticaoCotas = Convert.ToInt32(newValues["PrazoRepeticaoCotas"]);
        else
            seriesOffShore.PrazoRepeticaoCotas = null;

        if (newValues["IdIndicePrevia"] != null)
        {
            seriesOffShore.IdIndicePrevia = Convert.ToInt16(newValues["IdIndicePrevia"]);
        }

        if (newValues["PrazoValidadePrevia"] != null && Convert.ToInt32(newValues["PrazoValidadePrevia"]) > 0)
            seriesOffShore.PrazoValidadePrevia = Convert.ToInt32(newValues["PrazoValidadePrevia"]);
        else
        {
            seriesOffShore.PrazoValidadePrevia = null;
            seriesOffShore.IdIndicePrevia = null;
        }

        if (Convert.ToBoolean(newValues["SerieInativa"]))
        {
            seriesOffShore.Inativa = "S";
        }
        else
        {
            seriesOffShore.Inativa = "N";
        }



        seriesOffShore.Save();
    }

}