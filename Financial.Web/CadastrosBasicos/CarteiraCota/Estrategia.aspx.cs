﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Bolsa;

public partial class CadastrosBasicos_Estrategia : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EstrategiaCollection coll = new EstrategiaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {        
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropIndiceBenchmark = gridCadastro.FindEditFormTemplateControl("dropIndiceBenchmark") as ASPxComboBox;

        Estrategia estrategia = new Estrategia();
        int idEstrategia = Convert.ToInt32(e.Keys[0]);

        if (estrategia.LoadByPrimaryKey(idEstrategia))
        {
            estrategia.Descricao = textDescricao.Text;

            if (dropIndiceBenchmark.SelectedIndex != -1)
                estrategia.IdIndiceBenchmark = Convert.ToInt16(dropIndiceBenchmark.SelectedItem.Value);
            else
                estrategia.IdIndiceBenchmark = null;            

            estrategia.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Estrategia - Operacao: Update Estrategia: " + idEstrategia + UtilitarioWeb.ToString(estrategia),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropIndiceBenchmark = gridCadastro.FindEditFormTemplateControl("dropIndiceBenchmark") as ASPxComboBox;

        Estrategia estrategia = new Estrategia();

        estrategia.Descricao = textDescricao.Text;

        if (dropIndiceBenchmark.SelectedIndex != -1)
            estrategia.IdIndiceBenchmark = Convert.ToInt16(dropIndiceBenchmark.SelectedItem.Value);
        else
            estrategia.IdIndiceBenchmark = null;     

        estrategia.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Estrategia - Operacao: Insert Estrategia: " + estrategia.IdEstrategia + UtilitarioWeb.ToString(estrategia),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdEstrategia");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idEstrategia = Convert.ToInt32(keyValuesId[i]);
                                
                Estrategia estrategia = new Estrategia();
                if (estrategia.LoadByPrimaryKey(idEstrategia))
                {
                    bool temRelacionado = false;
                    TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
                    tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdEstrategia.Equal(idEstrategia));
                    if (tituloRendaFixaCollection.Query.Load())
                    {
                        temRelacionado = true;                        
                    }
                    if (!temRelacionado)
                    {
                        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                        ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.IdEstrategia.Equal(idEstrategia));
                        if (ativoBolsaCollection.Query.Load())
                        {
                            temRelacionado = true;
                        }
                    }
                    if (!temRelacionado)
                    {
                        CarteiraCollection carteiraCollection = new CarteiraCollection();
                        carteiraCollection.Query.Where(carteiraCollection.Query.IdEstrategia.Equal(idEstrategia));
                        if (carteiraCollection.Query.Load())
                        {
                            temRelacionado = true;
                        }
                    }

                    if (temRelacionado)
                    {
                        throw new Exception("Estratégia " + estrategia.Descricao + " não pode ser excluída por ter ativos relacionados.");
                    }

                    //
                    Estrategia estrategiaClone = (Estrategia)Utilitario.Clone(estrategia);
                    //
                    estrategia.MarkAsDeleted();
                    estrategia.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Estrategia - Operacao: Delete Estrategia: " + idEstrategia + UtilitarioWeb.ToString(estrategiaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

}

