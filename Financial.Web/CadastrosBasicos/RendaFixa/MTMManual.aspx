﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MTMManual.aspx.cs" Inherits="CadastrosBasicos_MTMManual" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';

        function OnGetDataOperacaoRendaFixa(data) {
            hiddenIdOperacao.SetValue(data);
            callBackPopupOperacaoRendaFixa.SendCallback(data);
            popupOperacaoRendaFixa.HideWindow();
            btnEditOperacaoRendaFixa.Focus();
        }
        
        function isNumber(n) {
          return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function OnGetDataCliente(data) {          
            if(!isNumber(data)){
                alert(data);
                return false;
            }
            btnEditCodigoCliente.SetValue(data);        
            callBackCliente.SendCallback(btnEditCodigoCliente.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoCliente.Focus();
        }            
        
        function OnGetDataTituloRendaFixa(data) {
            hiddenIdTitulo.SetValue(data);
            callBackPopupTitulo.SendCallback(data);
            popupTituloRendaFixa.HideWindow();
            btnEditTituloRendaFixa.Focus();
        } 
        
        function LimpaCampos(limpaTitulo, limpaOperacao)
        {
            if(limpaOperacao)
                btnEditOperacaoRendaFixa.SetValue(null);
                
            if(limpaTitulo)
                btnEditTituloRendaFixa.SetValue(null); 
                
        }       
                  
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callBackPopupTitulo" runat="server" OnCallback="callBackPopupTitulo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                                            if (e.result != null) 
                                                            {     
                                                                LimpaCampos(true, true);                                                      
                                                                btnEditTituloRendaFixa.SetValue(e.result); 
                                                            }
                                                           } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCliente" runat="server" OnCallback="callBackCliente_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                    
                           
            var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');               
            LimpaCampos(true, true);
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);                
                                  
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {        
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupOperacaoRendaFixa" runat="server" OnCallback="callBackPopupOperacaoRendaFixa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditOperacaoRendaFixa.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupTituloRendaFixa" ClientInstanceName="popupTituloRendaFixa"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridTituloRendaFixa" runat="server" Width="100%" ClientInstanceName="gridTituloRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSTituloRendaFixa" KeyFieldName="IdTitulo"
                            OnCustomDataCallback="gridTituloRendaFixa_CustomDataCallback" OnCustomCallback="gridTituloRendaFixa_CustomCallback"
                            OnHtmlRowCreated="gridTituloRendaFixa_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Dt.Vencimento"
                                    VisibleIndex="2" Width="5%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                                                                        gridTituloRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTituloRendaFixa);
                                                                      }" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Título." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridTituloRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupOperacaoRendaFixa" ClientInstanceName="popupOperacaoRendaFixa"
            runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentOperacaoRendaFixa" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridOperacaoRendaFixa" runat="server" Width="100%" ClientInstanceName="gridOperacaoRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSOperacaoRendaFixa" KeyFieldName="IdOperacao"
                            OnCustomDataCallback="gridOperacaoRendaFixa_CustomDataCallback" OnCustomCallback="gridOperacaoRendaFixa_CustomCallback"
                            OnLoad="gridOperacaoRendaFixa_load" OnHtmlRowCreated="gridOperacaoRendaFixa_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" VisibleIndex="0" ReadOnly="True">
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" VisibleIndex="1" Caption="Papel"
                                    Width="40%">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ClienteApelido" VisibleIndex="2" Caption="Cliente">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo de Operação"
                                    VisibleIndex="3">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data da Operação"
                                    VisibleIndex="4">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data de Vencimento"
                                    VisibleIndex="5">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="DataCliente" Caption="Data do Cliente" VisibleIndex="6">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="7" Visible="false">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" VisibleIndex="8" Visible="false">
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridOperacaoRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataOperacaoRendaFixa);}"
                                Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Operação Renda Fixa." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridOperacaoRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de MTM Manual"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdMTMManual" DataSourceID="EsDSMTMManual"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataDateColumn Caption="Dt.Vigência" FieldName="DtVigencia" VisibleIndex="1"
                                                Width="7%">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="2" Width="5%" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="3" Width="15%" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" VisibleIndex="4" Width="5%" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="DescricaoTitulo" FieldName="DescricaoTitulo"
                                                VisibleIndex="5" Width="15%" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" VisibleIndex="6" Width="5%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="PuMTM" VisibleIndex="7" Width="10%"
                                                HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00);0.00000000}">
                                                </PropertiesSpinEdit>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Right">
                                                </CellStyle>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="Taxa252" Caption="Taxa 252" VisibleIndex="8"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00);0.00000000}">
                                                </PropertiesSpinEdit>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Right">
                                                </CellStyle>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                        </Columns>
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdOperacao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdOperacao")%>'
                                                        ClientInstanceName="hiddenIdOperacao" />
                                                    <dxe:ASPxTextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>'
                                                        ClientInstanceName="hiddenIdTitulo" />                                                        
                                                    
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                    NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, callBackCliente, btnEditCodigoCliente);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNomeCliente" Style="width: 272px;" runat="server" CssClass="textNome"
                                                                    Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelbtnEditTituloRendaFixa" runat="server" CssClass="labelRequired"
                                                                    Text="Título:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxButtonEdit ID="btnEditTituloRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditTituloRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdTitulo") != null ? Eval("IdTitulo") + " - " + Eval("DescricaoTitulo") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRendaFixa.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                            <td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelBtnOperacao" runat="server" CssClass="labelNormal" Text="Operação:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxButtonEdit ID="btnEditOperacaoRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditOperacaoRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%# Eval("IdOperacao") %>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupOperacaoRendaFixa.ShowAtElementByID(s.name); gridOperacaoRendaFixa.PerformCallback('btnRefresh')}" 
                                                                     />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDtVigencia" runat="server" CssClass="labelRequired" Text="Dt.Vigência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDtVigencia" runat="server" ClientInstanceName="textDtVigencia"
                                                                    Width="150px" Value='<%#Eval("DtVigencia")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPuMTM" runat="server" CssClass="labelNormal" Text="PU MTM:"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxSpinEdit ID="textPuMTM" runat="server" ClientInstanceName="textPuMTM" CssClass="textValor_5"
                                                                    Width="150px" Text='<%# Eval("PuMTM") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTaxa252" runat="server" CssClass="labelNormal" Text="Taxa 252:"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxSpinEdit ID="textTaxa252" runat="server" ClientInstanceName="textTaxa252"
                                                                    CssClass="textValor_5" Text='<%# Eval("Taxa252") %>' NumberType="Float" MaxLength="16"
                                                                    Width="150px" DecimalPlaces="8">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSMTMManual" runat="server" OnesSelect="EsDSMTMManual_esSelect" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixa" runat="server" OnesSelect="EsDSTituloRendaFixa_esSelect" />
    </form>
</body>
</html>
