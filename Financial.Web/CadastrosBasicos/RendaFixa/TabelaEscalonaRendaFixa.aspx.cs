﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class CadastrosBasicos_TabelaEscalonaRF : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupTituloRF = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaEscalonamentoRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        TabelaEscalonamentoRFQuery tabelaEscalonamentoRFQuery = new TabelaEscalonamentoRFQuery("T");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("Ti");

        tabelaEscalonamentoRFQuery.Select(tabelaEscalonamentoRFQuery, tituloRendaFixaQuery.DescricaoCompleta);
        tabelaEscalonamentoRFQuery.InnerJoin(tituloRendaFixaQuery).On(tabelaEscalonamentoRFQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        
        TabelaEscalonamentoRFCollection coll = new TabelaEscalonamentoRFCollection();
        coll.Load(tabelaEscalonamentoRFQuery);

        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        //
        coll.Query.OrderBy(coll.Query.DataVencimento.Ascending);
        coll.Query.Load();
        //
        e.Collection = coll;
    }
    #endregion
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {            
            int idTitulo = Convert.ToInt32(e.Parameter);
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.Query.Select(tituloRendaFixa.Query.DescricaoCompleta);
            tituloRendaFixa.Query.Where(tituloRendaFixa.Query.IdTitulo == idTitulo);

            if(tituloRendaFixa.Query.Load()) {
                texto = tituloRendaFixa.DescricaoCompleta.Trim();
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditTituloRF_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textPrazo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";        
        //
        ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
        ASPxSpinEdit textPrazo = gridCadastro.FindEditFormTemplateControl("textPrazo") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditTituloRF, textPrazo, textTaxa });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        ASPxTextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        
        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing) {
            TabelaEscalonamentoRF tabelaEscalonamentoRF = new TabelaEscalonamentoRF();
            if (tabelaEscalonamentoRF.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTitulo.Text), Convert.ToInt32(textPrazo.Text))) {
                e.Result = "Registro já existente.!";
            }
        }
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idTitulo = Convert.ToString(e.GetListSourceFieldValue(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo));
            string prazo = Convert.ToString(e.GetListSourceFieldValue(TabelaEscalonamentoRFMetadata.ColumnNames.Prazo));
            e.Value = idTitulo + prazo;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {       
        TabelaEscalonamentoRF tabelaEscalonamentoRF = new TabelaEscalonamentoRF();
        //
        ASPxTextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        ASPxSpinEdit textPrazo = gridCadastro.FindEditFormTemplateControl("textPrazo") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        //
        // Chaves
        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        int prazo = Convert.ToInt32(textPrazo.Text);
        //
        if (tabelaEscalonamentoRF.LoadByPrimaryKey(idTitulo, prazo)) {
            tabelaEscalonamentoRF.Taxa = Convert.ToDecimal(textTaxa.Text);
            tabelaEscalonamentoRF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaEscalonamentoRF - Operacao: Update TabelaEscalonamentoRF: " + idTitulo + "; " + prazo + "; " + UtilitarioWeb.ToString(tabelaEscalonamentoRF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        TabelaEscalonamentoRF tabelaEscalonamentoRF = new TabelaEscalonamentoRF();
        //
        ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
        ASPxSpinEdit textPrazo = gridCadastro.FindEditFormTemplateControl("textPrazo") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        //
        ASPxTextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        //
        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        int prazo = Convert.ToInt32(textPrazo.Text);
        decimal taxa = Convert.ToDecimal(textTaxa.Text);

        tabelaEscalonamentoRF.IdTitulo = idTitulo;
        tabelaEscalonamentoRF.Prazo = prazo;
        tabelaEscalonamentoRF.Taxa = taxa;

        tabelaEscalonamentoRF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaEscalonamentoRF - Operacao: Insert TabelaEscalonamentoRF: " + tabelaEscalonamentoRF.IdTitulo + "; " + tabelaEscalonamentoRF.Prazo + "; " + UtilitarioWeb.ToString(tabelaEscalonamentoRF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdTitulo = gridCadastro.GetSelectedFieldValues(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo);
            List<object> keyValuesPrazo = gridCadastro.GetSelectedFieldValues(TabelaEscalonamentoRFMetadata.ColumnNames.Prazo);

            for (int i = 0; i < keyValuesIdTitulo.Count; i++) {
                int idTitulo = Convert.ToInt32(keyValuesIdTitulo[i]);
                int prazo = Convert.ToInt32(keyValuesPrazo[i]);

                TabelaEscalonamentoRF tabelaEscalonamentoRF = new TabelaEscalonamentoRF();
                if (tabelaEscalonamentoRF.LoadByPrimaryKey(idTitulo, prazo)){
                    //
                    TabelaEscalonamentoRF tabelaEscalonamentoRFClone = (TabelaEscalonamentoRF)Utilitario.Clone(tabelaEscalonamentoRF);
                    //

                    tabelaEscalonamentoRF.MarkAsDeleted();
                    tabelaEscalonamentoRF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaEscalonamentoRF - Operacao: Delete TabelaEscalonamentoRF: " + tabelaEscalonamentoRFClone.IdTitulo + "; " + tabelaEscalonamentoRFClone.Prazo + UtilitarioWeb.ToString(tabelaEscalonamentoRFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTituloRF", "textTaxa");
        base.gridCadastro_PreRender(sender, e);
    }
}