﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_BloqueioRendaFixa : CadastroBasePage {
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        this.AllowUpdate = false;
        base.Page_Load(sender, e);
        //
        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                 new List<string>(new string[] { BloqueioRendaFixaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSBloqueioRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        BloqueioRendaFixaQuery bloqueioRendaFixaQuery = new BloqueioRendaFixaQuery("B");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        bloqueioRendaFixaQuery.Select(bloqueioRendaFixaQuery, clienteQuery.Apelido.As("Apelido"));
        bloqueioRendaFixaQuery.InnerJoin(clienteQuery).On(bloqueioRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        bloqueioRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        bloqueioRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            bloqueioRendaFixaQuery.Where(bloqueioRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            bloqueioRendaFixaQuery.Where(bloqueioRendaFixaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            bloqueioRendaFixaQuery.Where(bloqueioRendaFixaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        bloqueioRendaFixaQuery.OrderBy(bloqueioRendaFixaQuery.DataOperacao.Descending);

        BloqueioRendaFixaCollection coll = new BloqueioRendaFixaCollection();
        coll.Load(bloqueioRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPosicaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCliente != null) {
            if (btnEditCodigoCliente.Text == "") {
                e.Collection = new PosicaoRendaFixaCollection();
                return;
            }
        }
        else {
            e.Collection = new PosicaoRendaFixaCollection();
            return;
        }

        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(campos, Convert.ToInt32(btnEditCodigoCliente.Text));
        DateTime data = cliente.DataDia.Value;

        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.GreaterThan(0));
        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));

        posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.DataVencimento.Descending);

        PosicaoRendaFixaCollection coll = new PosicaoRendaFixaCollection();
        coll.Load(posicaoRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("DataOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        selectedIndex = Convert.ToInt32(e.Parameter);

        ASPxSpinEdit textQuantidade = popupPosicaoRendaFixa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        if (textQuantidade.Text == "") {
            e.Result = "A quantidade deve ser preenchida.";
            return;
        }
        if (dropTipo.SelectedIndex == -1) {
            e.Result = "O tipo de operação deve ser preenchido.";
            return;
        }
        if (Convert.ToDecimal(textQuantidade.Text) <= 0) {
            e.Result = "A quantidade deve ser maior do que zero.";
            return;
        }

        ASPxGridView gridPosicaoRendaFixa = popupPosicaoRendaFixa.FindControl("gridPosicaoRendaFixa") as ASPxGridView;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        int idPosicao = Convert.ToInt32(gridPosicaoRendaFixa.GetRowValues(selectedIndex, "IdPosicao"));
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        byte tipo = Convert.ToByte(dropTipo.SelectedItem.Value);

        PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
        if (posicaoRendaFixaAbertura.LoadByPrimaryKey(idPosicao, data)) {
            if (tipo == (byte)TipoOperacaoBloqueio.Bloqueio) {
                if ((posicaoRendaFixaAbertura.Quantidade.Value - posicaoRendaFixaAbertura.QuantidadeBloqueada.Value) < Convert.ToDecimal(textQuantidade.Text)) {
                    e.Result = "Quantidade a ser bloqueada é maior que a quantidade disponível da posição! Operação não pode ser realizada.";
                    return;
                }
            }
            else if (tipo == (byte)TipoOperacaoBloqueio.Desbloqueio) {
                if (posicaoRendaFixaAbertura.QuantidadeBloqueada.Value < Convert.ToDecimal(textQuantidade.Text)) {
                    e.Result = "Quantidade a ser desbloqueada é maior que a quantidade bloqueada da posição! Operação não pode ser realizada.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") 
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;
                            byte status = cliente.Status.Value;

                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                            
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void InsertBloqueio(int selectedIndex) {
        ASPxPopupControl popupPosicaoRendaFixa = this.FindControl("popupPosicaoRendaFixa") as ASPxPopupControl;
        ASPxGridView gridPosicaoRendaFixa = popupPosicaoRendaFixa.FindControl("gridPosicaoRendaFixa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoRendaFixa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxMemo motivo = gridCadastro.FindEditFormTemplateControl("memoMotivo") as ASPxMemo;
            
        int idPosicao = Convert.ToInt32(gridPosicaoRendaFixa.GetRowValues(selectedIndex, "IdPosicao"));
        int idTitulo = Convert.ToInt32(gridPosicaoRendaFixa.GetRowValues(selectedIndex, "IdTitulo"));
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        //Deleta antes todos os bloqueios e desbloqueios do cliente na data e do IdPosicao
        BloqueioRendaFixaCollection bloqueioRendaFixaCollection = new BloqueioRendaFixaCollection();
        bloqueioRendaFixaCollection.Query.Where(bloqueioRendaFixaCollection.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)),
                                                bloqueioRendaFixaCollection.Query.DataOperacao.Equal(data),
                                                bloqueioRendaFixaCollection.Query.IdPosicao.Equal(idPosicao));
        bloqueioRendaFixaCollection.Query.Load();
        bloqueioRendaFixaCollection.MarkAllAsDeleted();
        bloqueioRendaFixaCollection.Save();
        //

        BloqueioRendaFixa bloqueioRendaFixa = new BloqueioRendaFixa();
        bloqueioRendaFixa.IdPosicao = idPosicao;
        bloqueioRendaFixa.IdTitulo = idTitulo;
        bloqueioRendaFixa.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        bloqueioRendaFixa.DataOperacao = data;
        bloqueioRendaFixa.Quantidade = quantidade;
        bloqueioRendaFixa.TipoOperacao = Convert.ToByte(dropTipo.SelectedItem.Value);

        bloqueioRendaFixa.Motivo = null;
        if (!String.IsNullOrEmpty(motivo.Text)) {
            bloqueioRendaFixa.Motivo = motivo.Text.Trim();
        }

        bloqueioRendaFixa.Save();


        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de BloqueioRendaFixa - Operacao: Insert BloqueioRendaFixa: " + bloqueioRendaFixa.IdBloqueio + UtilitarioWeb.ToString(bloqueioRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e) {
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(BloqueioRendaFixaMetadata.ColumnNames.IdBloqueio);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idBloqueio = Convert.ToInt32(keyValuesId[i]);

                BloqueioRendaFixa bloqueioRendaFixa = new BloqueioRendaFixa();
                if (bloqueioRendaFixa.LoadByPrimaryKey(idBloqueio)) {

                    BloqueioRendaFixa bloqueioRendaFixaClone = (BloqueioRendaFixa)Utilitario.Clone(bloqueioRendaFixa);
                    //

                    bloqueioRendaFixa.MarkAsDeleted();
                    bloqueioRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de BloqueioRendaFixa - Operacao: Delete BloqueioRendaFixa: " + idBloqueio + UtilitarioWeb.ToString(bloqueioRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
                
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoRendaFixa.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            InsertBloqueio(selectedIndex);
            gridPosicaoRendaFixa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }    
}