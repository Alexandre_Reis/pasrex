﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;

public partial class CadastrosBasicos_AjusteOperacaoRendaFixa : CadastroBasePage
{
    #region Enum
    public enum TipoRegistro {
        Operacao = 1,
        Liquidacao = 2
    }
    #endregion

    private int selectedIndex;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;        

        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OperacaoRendaFixaMetadata.ColumnNames.Status,
                                                  OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao,
                                                  "TipoLancamento"
                                                }));

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'Status', OpenPopup); }";
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        #region OperacaoRF
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery,
                                      tituloRendaFixaQuery.DescricaoCompleta,  
                                      clienteQuery.Apelido.As("Apelido"),
                                      "<'1' as TipoRegistro>",
                                      "<0 as TipoLancamento>"
                                      );
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario,
                                     operacaoRendaFixaQuery.DataOperacao >= clienteQuery.DataDia,
                                     operacaoRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                            (byte)TipoOperacaoTitulo.VendaTotal,
                                                                            (byte)TipoOperacaoTitulo.VendaCasada));
        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textIdOperacaoFiltro.Text)) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdOperacao == textIdOperacaoFiltro.Text);
        }

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);
        #endregion

        #region Liquidação
        LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery1 = new LiquidacaoRendaFixaQuery("L");
        PermissaoClienteQuery permissaoClienteQuery1 = new PermissaoClienteQuery("P");
        TituloRendaFixaQuery tituloRendaFixaQuery1 = new TituloRendaFixaQuery("T");
        ClienteQuery clienteQuery1 = new ClienteQuery("C");

        liquidacaoRendaFixaQuery1.Select(liquidacaoRendaFixaQuery1,
                                      tituloRendaFixaQuery1.DescricaoCompleta,
                                      clienteQuery1.Apelido.As("Apelido"),
                                      "<'2' as TipoRegistro>");
        liquidacaoRendaFixaQuery1.InnerJoin(clienteQuery1).On(liquidacaoRendaFixaQuery1.IdCliente == clienteQuery1.IdCliente);
        liquidacaoRendaFixaQuery1.InnerJoin(tituloRendaFixaQuery1).On(tituloRendaFixaQuery1.IdTitulo == liquidacaoRendaFixaQuery1.IdTitulo);
        liquidacaoRendaFixaQuery1.InnerJoin(permissaoClienteQuery1).On(permissaoClienteQuery1.IdCliente == clienteQuery1.IdCliente);
        //
        liquidacaoRendaFixaQuery1.Where(permissaoClienteQuery1.IdUsuario == idUsuario,
                                        liquidacaoRendaFixaQuery1.DataLiquidacao >= clienteQuery.DataDia,
                                        liquidacaoRendaFixaQuery1.TipoLancamento.In( 
                                                                            (int)TipoLancamentoLiquidacao.Vencimento,
                                                                            (int)TipoLancamentoLiquidacao.Amortizacao,
                                                                            (int)TipoLancamentoLiquidacao.Juros,
                                                                            (int)TipoLancamentoLiquidacao.PagtoPrincipal) 
                                       );
        //
        liquidacaoRendaFixaQuery1.OrderBy(liquidacaoRendaFixaQuery1.DataLiquidacao.Descending);

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            liquidacaoRendaFixaQuery1.Where(liquidacaoRendaFixaQuery1.DataLiquidacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            liquidacaoRendaFixaQuery1.Where(liquidacaoRendaFixaQuery1.DataLiquidacao.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textIdOperacaoFiltro.Text)) {
            liquidacaoRendaFixaQuery1.Where(liquidacaoRendaFixaQuery1.IdLiquidacao == textIdOperacaoFiltro.Text);
        }

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            liquidacaoRendaFixaQuery1.Where(liquidacaoRendaFixaQuery1.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        LiquidacaoRendaFixaCollection coll1 = new LiquidacaoRendaFixaCollection();
        coll1.Load(liquidacaoRendaFixaQuery1);

        for (int i = 0; i < coll1.Count; i++) {
            OperacaoRendaFixa oper = coll.AddNew();
            oper.IdOperacao = coll1[i].IdLiquidacao;
            oper.DataOperacao = coll1[i].DataLiquidacao;
            oper.IdCliente = coll1[i].IdCliente;
            oper.SetColumn(ClienteMetadata.ColumnNames.Apelido, coll1[i].GetColumn(ClienteMetadata.ColumnNames.Apelido));
            oper.SetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, coll1[i].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta));
            oper.SetColumn("TipoRegistro", coll1[i].GetColumn("TipoRegistro"));
            oper.SetColumn("TipoLancamento", coll1[i].GetColumn("TipoLancamento"));
            oper.TipoOperacao = (byte)0;
            oper.Status = coll1[i].Status;
            oper.Valor = coll1[i].ValorBruto;
            oper.ValorIR = coll1[i].ValorIR;
            oper.ValorIOF = coll1[i].ValorIOF;
            oper.Rendimento = coll1[i].Rendimento;
            oper.RendimentoNaoTributavel = coll1[i].RendimentoNaoTributavel;                       
        }
        #endregion

        e.Collection = coll;
    }

    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Query.OrderBy(coll.Query.DescricaoCompleta.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }
    
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                           ? cliente.str.Apelido
                           : "no_access";
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void textValor_Init(object sender, EventArgs e)
    {
        string tipoRegistro = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "TipoRegistro"));
        string tipoLancamento = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "TipoLancamento"));

        if (tipoRegistro == Convert.ToString((int)TipoRegistro.Operacao) ||
            tipoLancamento == Convert.ToString((byte)TipoLancamentoLiquidacao.Amortizacao) ||
            tipoLancamento == Convert.ToString((byte)TipoLancamentoLiquidacao.Juros))
        {
            (sender as ASPxSpinEdit).Visible = false;
        }
    }

    protected void labelValor_Init(object sender, EventArgs e)
    {
        string tipoRegistro = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "TipoRegistro"));
        string tipoLancamento = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "TipoLancamento"));

        if (tipoRegistro == Convert.ToString((int)TipoRegistro.Operacao) ||
            tipoLancamento == Convert.ToString((byte)TipoLancamentoLiquidacao.Amortizacao) ||
            tipoLancamento == Convert.ToString((byte)TipoLancamentoLiquidacao.Juros))
        {
            (sender as Label).Visible = false;
        }
    }

    /// <summary>
    /// Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string chave = Convert.ToString(e.Keys[0]); // chave composta

        string[] tipoRegistroAux = chave.Split('|');
        string tipoRegistro = tipoRegistroAux[5];

        int idOperacaoLiquidacao = Convert.ToInt32(tipoRegistroAux[0]);

        ASPxSpinEdit textValorIR = gridCadastro.FindEditFormTemplateControl("textValorIR") as ASPxSpinEdit;
        ASPxSpinEdit textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        ASPxSpinEdit textRendimento = gridCadastro.FindEditFormTemplateControl("textRendimento") as ASPxSpinEdit;
        ASPxSpinEdit textRendimentoNaoTributavel = gridCadastro.FindEditFormTemplateControl("textRendimentoNaoTributavel") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        if (tipoRegistro == Convert.ToString((int)TipoRegistro.Operacao))
        {
            #region OperacaoRendaFixa
            OperacaoRendaFixa dr = new OperacaoRendaFixa();
            //
            if (dr.LoadByPrimaryKey(idOperacaoLiquidacao)) 
            {
                dr.Rendimento = Convert.ToDecimal(textRendimento.Text);
                dr.RendimentoNaoTributavel = Convert.ToDecimal(textRendimentoNaoTributavel.Text);
                dr.ValorIR = Convert.ToDecimal(textValorIR.Text);
                dr.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
                dr.Status = (byte)StatusOperacaoRendaFixa.Ajustado;

                dr.Save();

                LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
                
                liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdOperacaoVenda.Equal(dr.IdOperacao.Value));
                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

                decimal totalQuantidade = dr.Quantidade.Value;
                decimal totalRendimento = dr.Rendimento.Value;
                decimal totalRendimentoNaoTributavel = dr.RendimentoNaoTributavel.Value;
                decimal totalIR = dr.ValorIR.Value;
                decimal totalIOF = dr.ValorIOF.Value;
                foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                {
                    decimal rendimento = Utilitario.Truncate((dr.Rendimento.Value * liquidacaoRendaFixa.Quantidade.Value) / dr.Quantidade.Value, 2);
                    decimal rendimentoNaoTributavel = Utilitario.Truncate((dr.RendimentoNaoTributavel.Value * liquidacaoRendaFixa.Quantidade.Value) / dr.Quantidade.Value, 2);
                    decimal ir = Utilitario.Truncate((dr.ValorIR.Value * liquidacaoRendaFixa.Quantidade.Value) / dr.Quantidade.Value, 2);
                    decimal iof = Utilitario.Truncate((dr.ValorIOF.Value * liquidacaoRendaFixa.Quantidade.Value) / dr.Quantidade.Value, 2);
                    
                    totalQuantidade -= liquidacaoRendaFixa.Quantidade.Value;

                    if (totalQuantidade == 0) //Garante o ajuste completo da operação para liquidação
                    {
                        rendimento = totalRendimento;
                        rendimentoNaoTributavel = totalRendimentoNaoTributavel;
                        ir = totalIR;
                        iof = totalIOF;
                    }                    

                    totalRendimento -= rendimento;
                    totalRendimentoNaoTributavel -= rendimentoNaoTributavel;
                    totalIR -= ir;
                    totalIOF -= iof;

                    liquidacaoRendaFixa.Rendimento = rendimento;
                    liquidacaoRendaFixa.RendimentoNaoTributavel = rendimentoNaoTributavel;
                    liquidacaoRendaFixa.ValorIR = ir;
                    liquidacaoRendaFixa.ValorIOF = iof;
                    liquidacaoRendaFixa.Status = (byte)StatusOperacaoRendaFixa.Ajustado;
                }

                liquidacaoRendaFixaCollection.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OperacaoRendaFixa - Operacao: Update OperacaoRendaFixa (AJUSTE): ",
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }

        else if (tipoRegistro == Convert.ToString((int)TipoRegistro.Liquidacao))
        {
            #region LiquidacaoRendaFixa
            LiquidacaoRendaFixa dr = new LiquidacaoRendaFixa();
            //
            if (dr.LoadByPrimaryKey(idOperacaoLiquidacao)) 
            {                
                dr.Rendimento = Convert.ToDecimal(textRendimento.Text);
                dr.RendimentoNaoTributavel = Convert.ToDecimal(textRendimentoNaoTributavel.Text);
                dr.ValorIR = Convert.ToDecimal(textValorIR.Text);
                dr.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
                dr.ValorLiquido = dr.ValorBruto.Value - dr.ValorIR.Value - dr.ValorIOF.Value;
                dr.Status = (byte)StatusLiquidacaoRendaFixa.Ajustado;

                if (dr.TipoLancamento.Value == (byte)TipoLancamentoLiquidacao.PagtoPrincipal || dr.TipoLancamento.Value == (byte)TipoLancamentoLiquidacao.Vencimento)
                {
                    dr.ValorBruto = Convert.ToDecimal(textValor.Text);                    
                }
                else
                {
                    dr.ValorBruto = dr.Rendimento.Value + dr.RendimentoNaoTributavel.Value;
                }

                dr.ValorLiquido = dr.ValorBruto.Value - dr.ValorIR.Value - dr.ValorIOF.Value;

                dr.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de LiquidacaoRendaFixa - Operacao: Update LiquidacaoRendaFixa (AJUSTE): ",
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        string chave = Convert.ToString(e.KeyValue);
        string[] tipoRegistroAux = chave.Split('|');
        string tipoRegistro = tipoRegistroAux[5];

        if (e.DataColumn.FieldName == "IdOperacao") {
            string value = Convert.ToString(e.GetValue("IdOperacao"));
            //
            if (tipoRegistro == Convert.ToString((int)TipoRegistro.Operacao)) {                
                e.Cell.Text = "idOper: " + value;
            }
            else {
                e.Cell.Text = "idLiq: " + value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idOperacao = Convert.ToString(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao));
            string dataOperacao = Convert.ToString(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao)).Substring(0, 10);
            string descricaoCompleta = Convert.ToString(e.GetListSourceFieldValue(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta));
            string valor = Convert.ToString(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.Valor));
            string valorIOF = Convert.ToString(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.ValorIOF));
            string tipoRegistro = Convert.ToString(e.GetListSourceFieldValue("TipoRegistro"));
            //
            e.Value = idOperacao + "|" + dataOperacao + "|" + descricaoCompleta + "|" + valor + "|" + valorIOF + "|" + tipoRegistro;
        }

        //else if (e.Column.FieldName == OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao) {
        //    int tipoOperacao = Convert.ToInt32(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao));
        //    string tipoRegistro = Convert.ToString(e.GetListSourceFieldValue("TipoRegistro"));

        //    if (tipoRegistro == Convert.ToString((int)TipoRegistro.Operacao)) {
        //        //string tipoOperacaoString = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

        //        //e.Value = tipoOperacaoString;      
        //    }
        //    else if (tipoRegistro == Convert.ToString((int)TipoRegistro.Liquidacao)) {
                
        //        TipoOperacaoTitulo t = (TipoOperacaoTitulo) Enum.Parse(typeof(TipoOperacaoTitulo),Convert.ToString(tipoOperacao));
        //        string tipoLiquidacaoString = StringEnum.GetStringValue(t);
        //        e.Value = tipoLiquidacaoString;
        //    }
        //}
    }


    /// <summary>
    /// Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxSpinEdit textRendimento = gridCadastro.FindEditFormTemplateControl("textRendimento") as ASPxSpinEdit;
        ASPxSpinEdit textRendimentoNaoTributavel = gridCadastro.FindEditFormTemplateControl("textRendimentoNaoTributavel") as ASPxSpinEdit;
        ASPxSpinEdit textValorIR = gridCadastro.FindEditFormTemplateControl("textValorIR") as ASPxSpinEdit;
        ASPxSpinEdit textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        
        List<Control> controles = new List<Control>(new Control[] {
                        textRendimento, textRendimentoNaoTributavel, textValorIR, textValorIOF
            });

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }        
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnLiberar")
        {
            #region Bloqueia Ajuste
            /* Pode ser idOperacao ou idLiquidacao */
            List<object> keyValuesIdOperacaoLiquidacao = gridCadastro.GetSelectedFieldValues(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
            List<object> keyValuesTipoRegistro = gridCadastro.GetSelectedFieldValues("TipoRegistro");
            //
            List<int> keyValuesIdOperacao = new List<int>();
            List<int> keyValuesLiquidacao = new List<int>();

            // Separa em dois Vetores de Acordo com o tipoRegistro
            for (int i = 0; i < keyValuesIdOperacaoLiquidacao.Count; i++) {
                if ( keyValuesTipoRegistro[i].ToString() == Convert.ToString((int)TipoRegistro.Operacao)) {
                    keyValuesIdOperacao.Add( Convert.ToInt32(keyValuesIdOperacaoLiquidacao[i]));
                }
                else {
                    keyValuesLiquidacao.Add( Convert.ToInt32(keyValuesIdOperacaoLiquidacao[i]));
                }
            }

            // Se tiver registro Selecionado
            if (keyValuesIdOperacaoLiquidacao.Count != 0) {

               #region Operacao
                if (keyValuesIdOperacao.Count != 0)
                {
                    OperacaoRendaFixaCollection operRFCollection = new OperacaoRendaFixaCollection();
                    operRFCollection.Query.Select(operRFCollection.Query.IdOperacao, operRFCollection.Query.Status)
                                          .Where(operRFCollection.Query.IdOperacao.In(keyValuesIdOperacao));

                    operRFCollection.Query.Load();

                    for (int i = 0; i < operRFCollection.Count; i++)
                    {
                        //Ignorar operacoes ja liberadas
                        if (operRFCollection[i].Status == (byte)StatusOperacaoRendaFixa.LiberadoComAjuste ||
                            operRFCollection[i].Status == (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste)
                        {
                            continue;
                        }

                        byte statusLiberacao = operRFCollection[i].Status == (byte)StatusOperacaoRendaFixa.Digitado ?
                            (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste :
                            (byte)StatusOperacaoRendaFixa.LiberadoComAjuste;

                        operRFCollection[i].Status = statusLiberacao;

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Operacao Renda Fixa: Liberação: " + operRFCollection[i].IdOperacao.Value + ";" + UtilitarioWeb.ToString(operRFCollection[i]),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                        LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                        liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.IdLiquidacao,
                                                                   liquidacaoRendaFixaCollection.Query.Status);
                        liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdOperacaoVenda.Equal(operRFCollection[i].IdOperacao.Value));
                        liquidacaoRendaFixaCollection.Query.Load();

                        foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                        {
                            liquidacaoRendaFixa.Status = statusLiberacao;
                        }
                        liquidacaoRendaFixaCollection.Save();
                    }

                    operRFCollection.Save();
                }
               #endregion

               #region Liquidacao
                if (keyValuesLiquidacao.Count != 0)
                {
                    LiquidacaoRendaFixaCollection liquidacaoRFCollection = new LiquidacaoRendaFixaCollection();
                    liquidacaoRFCollection.Query.Select(liquidacaoRFCollection.Query.IdLiquidacao, liquidacaoRFCollection.Query.Status)
                                                .Where(liquidacaoRFCollection.Query.IdLiquidacao.In(keyValuesLiquidacao));

                    liquidacaoRFCollection.Query.Load();

                    for (int i = 0; i < liquidacaoRFCollection.Count; i++)
                    {
                        //Ignorar operacoes ja liberadas
                        if (liquidacaoRFCollection[i].Status == (byte)StatusOperacaoRendaFixa.LiberadoComAjuste ||
                            liquidacaoRFCollection[i].Status == (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste)
                        {
                            continue;
                        }

                        byte statusLiberacao = liquidacaoRFCollection[i].Status == (byte)StatusLiquidacaoRendaFixa.Digitado ?
                            (byte)StatusLiquidacaoRendaFixa.LiberadoSemAjuste :
                            (byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste;

                        liquidacaoRFCollection[i].Status = statusLiberacao;
                    }

                    liquidacaoRFCollection.Save();

                    foreach (LiquidacaoRendaFixa liquidacaoRF in liquidacaoRFCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Liquidação Renda Fixa: Liberação: " + liquidacaoRF.IdLiquidacao + ";" + UtilitarioWeb.ToString(liquidacaoRF),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }
                }
                #endregion
            }
            else 
            {                     
                throw new Exception("Selecione uma ou mais Linhas do grid.");
            }
            #endregion
        }

        if (e.Parameters == "btnDesfazer")
        {
            #region Desfaz Ajuste
            /* Pode ser idOperacao ou idLiquidacao */
            List<object> keyValuesIdOperacaoLiquidacao = gridCadastro.GetSelectedFieldValues(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
            List<object> keyValuesTipoRegistro = gridCadastro.GetSelectedFieldValues("TipoRegistro");
            //
            List<int> keyValuesIdOperacao = new List<int>();
            List<int> keyValuesLiquidacao = new List<int>();

            // Separa em dois Vetores de Acordo com o tipoRegistro
            for (int i = 0; i < keyValuesIdOperacaoLiquidacao.Count; i++)
            {
                if (keyValuesTipoRegistro[i].ToString() == Convert.ToString((int)TipoRegistro.Operacao))
                {
                    keyValuesIdOperacao.Add(Convert.ToInt32(keyValuesIdOperacaoLiquidacao[i]));
                }
                else
                {
                    keyValuesLiquidacao.Add(Convert.ToInt32(keyValuesIdOperacaoLiquidacao[i]));
                }
            }

            // Se tiver registro Selecionado
            if (keyValuesIdOperacaoLiquidacao.Count != 0)
            {

                #region Operacao
                if (keyValuesIdOperacao.Count != 0)
                {
                    OperacaoRendaFixaCollection operRFCollection = new OperacaoRendaFixaCollection();
                    operRFCollection.Query.Select(operRFCollection.Query.IdOperacao, operRFCollection.Query.Status)
                                          .Where(operRFCollection.Query.IdOperacao.In(keyValuesIdOperacao));

                    operRFCollection.Query.Load();

                    for (int i = 0; i < operRFCollection.Count; i++)
                    {
                        operRFCollection[i].Status = (byte)StatusOperacaoRendaFixa.Digitado;

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Operacao Renda Fixa: Liberação Desfeita: " + operRFCollection[i].IdOperacao.Value + ";" + UtilitarioWeb.ToString(operRFCollection[i]),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                        LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                        liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.IdLiquidacao,
                                                                   liquidacaoRendaFixaCollection.Query.Status);
                        liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdOperacaoVenda.Equal(operRFCollection[i].IdOperacao.Value));
                        liquidacaoRendaFixaCollection.Query.Load();

                        foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                        {
                            liquidacaoRendaFixa.Status = (byte)StatusLiquidacaoRendaFixa.Digitado;
                        }
                        liquidacaoRendaFixaCollection.Save();
                    }

                    operRFCollection.Save();
                }
                #endregion

                #region Liquidacao
                if (keyValuesLiquidacao.Count != 0)
                {
                    LiquidacaoRendaFixaCollection liquidacaoRFCollection = new LiquidacaoRendaFixaCollection();
                    liquidacaoRFCollection.Query.Select(liquidacaoRFCollection.Query.IdLiquidacao, liquidacaoRFCollection.Query.Status)
                                                .Where(liquidacaoRFCollection.Query.IdLiquidacao.In(keyValuesLiquidacao));

                    liquidacaoRFCollection.Query.Load();

                    for (int i = 0; i < liquidacaoRFCollection.Count; i++)
                    {
                        liquidacaoRFCollection[i].Status = (byte)StatusLiquidacaoRendaFixa.Digitado;
                    }

                    liquidacaoRFCollection.Save();

                    foreach (LiquidacaoRendaFixa liquidacaoRF in liquidacaoRFCollection)
                    {
                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Liquidação Renda Fixa: Liberação Desfeita: " + liquidacaoRF.IdLiquidacao + ";" + UtilitarioWeb.ToString(liquidacaoRF),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }
                }
                #endregion
            }
            else
            {
                throw new Exception("Selecione uma ou mais Linhas do grid.");
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Chamado antes de Iniciar Grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        //
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        //
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textIdOperacaoFiltro != null && textIdOperacaoFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Id Operação/Id Liquidação: ").Append(textIdOperacaoFiltro.Text);
        }

        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}