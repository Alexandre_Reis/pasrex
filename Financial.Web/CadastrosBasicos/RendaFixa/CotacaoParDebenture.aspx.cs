﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CotacaoParDebenture : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoDebenture_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotacaoDebentureCollection coll = new CotacaoDebentureCollection();

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.Data.LessThanOrEqual(textDataFim.Text));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textCodigoPapel = gridCadastro.FindEditFormTemplateControl("textCodigoPapel") as TextBox;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDataReferencia);
        controles.Add(textCodigoPapel);
        controles.Add(textPU);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            string codigoPapel = Convert.ToString(textCodigoPapel.Text);

            CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
            if (cotacaoDebenture.LoadByPrimaryKey(dataReferencia, codigoPapel))
            {
                e.Result = "Registro já existente";
            }
        }
    }
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();

        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textCodigoPapel = gridCadastro.FindEditFormTemplateControl("textCodigoPapel") as TextBox;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        string codigoPapel = Convert.ToString(textCodigoPapel.Text).ToUpper().Trim();

        cotacaoDebenture.Data = dataReferencia;
        cotacaoDebenture.CodigoPapel = codigoPapel;
        cotacaoDebenture.Pu = Convert.ToDecimal(textPU.Text);
        cotacaoDebenture.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoDebenture - Operacao: Insert CotacaoDebenture: " + cotacaoDebenture.Data + "; " + cotacaoDebenture.CodigoPapel + UtilitarioWeb.ToString(cotacaoDebenture),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCodigoPapel_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as TextBox).Enabled = false;
            //(sender as TextBox).BackColor = Color.FromName("#EBEBEB");                        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoDebentureMetadata.ColumnNames.DataReferencia));
            string CodigoPapel = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoDebentureMetadata.ColumnNames.CodigoPapel));
            e.Value = dataReferencia + CodigoPapel;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();

        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textCodigoPapel = gridCadastro.FindEditFormTemplateControl("textCodigoPapel") as TextBox;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        string CodigoPapel = Convert.ToString(textCodigoPapel.Text);

        if (cotacaoDebenture.LoadByPrimaryKey(dataReferencia, CodigoPapel))
        {
            cotacaoDebenture.Pu = Convert.ToDecimal(textPU.Text);
            cotacaoDebenture.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoDebenture - Operacao: Update CotacaoDebenture: " + dataReferencia + "; " + CodigoPapel + UtilitarioWeb.ToString(cotacaoDebenture),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(CotacaoDebentureMetadata.ColumnNames.Data);
            List<object> keyValuesCodigoPapel = gridCadastro.GetSelectedFieldValues(CotacaoDebentureMetadata.ColumnNames.CodigoPapel);            
            
            for (int i = 0; i < keyValuesDataReferencia.Count; i++) {
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                string codigoPapel = Convert.ToString(keyValuesCodigoPapel[i]);

                CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
                if (cotacaoDebenture.LoadByPrimaryKey(dataReferencia, codigoPapel)) 
                {
                    CotacaoDebenture cotacaoDebentureClone = (CotacaoDebenture)Utilitario.Clone(cotacaoDebenture);
                    //

                    cotacaoDebenture.MarkAsDeleted();
                    cotacaoDebenture.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoDebenture - Operacao: Delete CotacaoDebenture: " + dataReferencia + "; " + codigoPapel + UtilitarioWeb.ToString(cotacaoDebentureClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textDataReferencia", "textPU");
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
            e.Properties["cpTextDataReferencia"] = textDataReferencia.ClientID;

            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            e.Properties["cpTextPU"] = textPU.ClientID;
        }
    }
}