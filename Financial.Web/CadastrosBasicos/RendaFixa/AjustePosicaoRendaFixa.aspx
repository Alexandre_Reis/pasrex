﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AjustePosicaoRendaFixa.aspx.cs" Inherits="CadastrosBasicos_AjustePosicaoRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown; 
           
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
        
    function FechaPopupRendaFixa()
    {
        textQuantidade.SetEnabled(false); 
        textQuantidade.SetText(''); 
        textPUOperacao.SetEnabled(false); 
        textPUOperacao.SetText(''); 
        textTaxaOperacao.SetEnabled(false); 
        textTaxaOperacao.SetText(''); 
        popupPosicaoRendaFixa.HideWindow();         
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var textNomeCliente = document.getElementById('textNomeCliente');
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:TextBox ID="textMsgNaoExiste" runat="server" style="display:none" Text="Inexistente"/>
    <asp:TextBox ID="textMsgInativo" runat="server" style="display:none" Text="Inativo"/>
    <asp:TextBox ID="textMsgUsuarioSemAcesso" runat="server" style="display:none" Text="Usuário sem acesso!"/>
    
    <dxpc:ASPxPopupControl ID="popupMensagemCliente" ShowHeader="false" PopupElementID="btnEditCodigoCliente" CloseAction="OuterMouseClick" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="" runat="server">
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupCliente" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCliente" runat="server" Width="100%"
                    ClientInstanceName="gridCliente"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                    OnCustomDataCallback="gridCliente_CustomDataCallback" 
                    OnHtmlRowCreated="gridCliente_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="0" Width="20%">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%">                    
                </dxwgv:GridViewDataTextColumn>                                
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>                    
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cliente" />
            </dxwgv:ASPxGridView>        
        </div>        
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCliente.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupPosicaoRendaFixa" runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="Center" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoRendaFixa" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoRendaFixa"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoRendaFixa" KeyFieldName="IdPosicao"                    
                    OnCustomCallback="gridPosicaoRendaFixa_CustomCallback"
                    OnHtmlRowCreated="gridPosicaoRendaFixa_HtmlRowCreated">               
            <Columns>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdPosicao" Caption="Id" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdTitulo" Caption="Título" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Operação" ReadOnly="true" VisibleIndex="6" Width="10%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="7" Width="10%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Right" />
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="PUOperacao" Caption="PU Operação" VisibleIndex="8" Width="14%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Right" />
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaOperacao" Caption="Taxa Oper." VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Right" />
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataComboBoxColumn Caption="Custodiante" FieldName="IdAgente" VisibleIndex="10"
                    Width="24%">
                    <EditFormSettings Visible="False" />
                    <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente" />
                    
                </dxwgv:GridViewDataComboBoxColumn>
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoNegociacao" Caption="Categoria" VisibleIndex="11"
                    Width="16%" ExportWidth="120">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="Negociação" />
                            <dxe:ListEditItem Value="2" Text="Disp. Venda" />
                            <dxe:ListEditItem Value="3" Text="Vencimento" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
            </Columns>            
            <Settings ShowTitlePanel="True" ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="280" />
            <SettingsPager PageSize="1000"></SettingsPager>
            <SettingsBehavior ColumnResizeMode="Disabled" AllowFocusedRow="true" />            
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Renda Fixa" />
            
            <ClientSideEvents RowDblClick="function(s, e) {textQuantidade.SetEnabled(true); textQuantidade.SetText('');
                                                           textPUOperacao.SetEnabled(true); textPUOperacao.SetText('');
                                                           textTaxaOperacao.SetEnabled(true); textTaxaOperacao.SetText('');
                                                           textQuantidade.Focus(); selectedIndex = e.visibleIndex; }" />            
            
            </dxwgv:ASPxGridView>
        
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="labelQtde" runat="server" CssClass="labelNormal" Text="Qtde Ajustar:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="8" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="PU Ajustar:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textPUOperacao" runat="server" CssClass="textValor" ClientInstanceName="textPUOperacao"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="25" NumberType="Float" DecimalPlaces="12" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Taxa Ajustar:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textTaxaOperacao" runat="server" CssClass="textValor" ClientInstanceName="textTaxaOperacao"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="25" NumberType="Float" DecimalPlaces="16" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="labelAgente" runat="server" CssClass="textValor" Text="Agente:"> </asp:Label>
                        </td>
                        <td>
                            <dxe:ASPxComboBox ID="dropAgenteCorretora" runat="server" ClientInstanceName="dropAgente"
                                DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="startswith"
                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" TextField="Nome"
                                ValueField="IdAgente">
                                <ClientSideEvents LostFocus="function(s, e) { 
                                    if(s.GetSelectedIndex() == -1)
                                        s.SetText(null); } " />
                            </dxe:ASPxComboBox>
                        </td>
                        <td colspan="3">
                            <asp:CheckBox ID="chkResetAgente" runat="server" Text="Desvincula custodiante" ToolTip="Retira da posição qualquer custodiante associado." />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelTipoNegociacao" runat="server" CssClass="labelNormal" Text="Categoria:"></asp:Label>
                        </td>
                        <td>
                            <dxe:ASPxComboBox ID="dropTipoNegociacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                CssClass="dropDownListCurto_2">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="Negociação" />
                                    <dxe:ListEditItem Value="2" Text="Disp. Venda" />
                                    <dxe:ListEditItem Value="3" Text="Vencimento" />
                                </Items>
                            </dxe:ASPxComboBox>
                        </td>
                        <td>
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"  
                                                   OnClientClick="gridPosicaoRendaFixa.PerformCallback(selectedIndex); return false;"><asp:Literal ID="Literal1" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                CssClass="btnCancel" OnClientClick="FechaPopupRendaFixa(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Fechar"/><div></div></asp:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoRendaFixa.PerformCallback(); 
                                                 textQuantidade.SetEnabled(false);
                                                 textPUOperacao.SetEnabled(false);
                                                 textTaxaOperacao.SetEnabled(false);
                                                 //dropAgenteCorretora.SetEnabled(false);
                                                  }"
                          CloseUp="function(s, e) {FechaPopupRendaFixa(); return false;}" />   
    </dxpc:ASPxPopupControl>
                                        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Posições de Renda Fixa"></asp:Label>
    </div>
        
    <div id="mainContent">
        
        <atk:AKRequiredValidator ID="RequiredValidator1" runat="server" ValidationGroup="ATK" >            
        </atk:AKRequiredValidator>
            
        <div class="reportFilter">
        
        <div class="dataMessage">
        <asp:ValidationSummary ID="validationSummary" runat="server" 
                         HeaderText="Campos com * são obrigatórios." EnableClientScript="true"
                         DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="ATK"/>
        </div>
        
        <table cellpadding="3">
        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                    ClientInstanceName="btnEditCodigoCliente">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCliente').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />              
            </td>         
            
            <td class="tdButton" width="170">
                <div class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnPosicaoRendaFixa" ForeColor="black" runat="server" CssClass="btnPopup" ValidationGroup="ATK" OnClientClick="if(Page_ClientValidate()) popupPosicaoRendaFixa.ShowAtElementByID(); 
                                                                                    return false;"><asp:Literal ID="Literal5" runat="server" Text="Ver Posições"/><div></div></asp:LinkButton>
                </div>
            </td>         
                                                                                        
            </tr>            
        </table>
        
        </div>
        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSPosicaoRendaFixa" runat="server" OnesSelect="EsDSPosicaoRendaFixa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />    
     <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />  
    
    </form>
</body>
</html>