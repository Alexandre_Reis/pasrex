﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.RendaFixa.Enums;

public partial class CadastrosBasicos_TaxaSerie : CadastroBasePage
{
    ASPxComboBox dropSerie;
    ASPxDateEdit textData;
    ASPxSpinEdit textTaxa;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTaxaSerie_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TaxaSerieCollection coll = new TaxaSerieCollection();

        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        FeederQuery feederQuery = new FeederQuery("feeder");
        TaxaSerieQuery taxaSerieQuery = new TaxaSerieQuery("taxaSerie");

        List<int> lstTipoTaxa = new List<int>();
        lstTipoTaxa.Add((int)TipoMTMTitulo.SerieTaxa);
        lstTipoTaxa.Add((int)TipoMTMTitulo.SerieTaxaPuFace);

        taxaSerieQuery.Select(taxaSerieQuery,
                              serieRendaFixaQuery.IdSerie,
                              serieRendaFixaQuery.IdFeeder,
                              (serieRendaFixaQuery.Descricao + " - " + feederQuery.Descricao).As("DescricaoCompleta"));
        taxaSerieQuery.InnerJoin(serieRendaFixaQuery).On(serieRendaFixaQuery.IdSerie == taxaSerieQuery.IdSerie);
        taxaSerieQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);
        taxaSerieQuery.Where(serieRendaFixaQuery.TipoTaxa.In(lstTipoTaxa.ToArray()));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            taxaSerieQuery.Where(taxaSerieQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            taxaSerieQuery.Where(taxaSerieQuery.Data.LessThanOrEqual(textDataFim.Text));
        }
        taxaSerieQuery.OrderBy(coll.Query.Data.Descending);

        coll.Load(taxaSerieQuery);

        e.Collection = coll;
    }

    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        FeederQuery feederQuery = new FeederQuery("feeder");

        List<int> lstTipoTaxa = new List<int>();
        lstTipoTaxa.Add((int)TipoMTMTitulo.SerieTaxa);
        lstTipoTaxa.Add((int)TipoMTMTitulo.SerieTaxaPuFace);

        serieRendaFixaQuery.Select(serieRendaFixaQuery,           
                                  (serieRendaFixaQuery.Descricao + " - " + feederQuery.Descricao).As("DescricaoCompleta"));
        serieRendaFixaQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);
        serieRendaFixaQuery.Where(serieRendaFixaQuery.TipoTaxa.In(lstTipoTaxa.ToArray()));

        serieRendaFixaQuery.OrderBy(coll.Query.Descricao.Ascending);        

        if (coll.Load(serieRendaFixaQuery))
        {
            foreach (SerieRendaFixa serie in coll)
                serie.SetColumn("DescricaoCompleta", serie.GetColumn("IdSerie").ToString() + " - " + serie.GetColumn("DescricaoCompleta").ToString());
        }

        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropSerie);
        controles.Add(textData);
        controles.Add(textTaxa);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            int idSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
            DateTime data = Convert.ToDateTime(textData.Text);

            TaxaSerie taxaSerie = new TaxaSerie();
            if (taxaSerie.LoadByPrimaryKey(data, idSerie))
            {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void textData_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void dropSerie_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TaxaSerieMetadata.ColumnNames.Data));
            string idSerie = Convert.ToString(e.GetListSourceFieldValue(TaxaSerieMetadata.ColumnNames.IdSerie));
            e.Value = data + idSerie;
        }
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn DigitadoImportado = gridCadastro.Columns["DigitadoImportado"] as GridViewDataComboBoxColumn;
        if (DigitadoImportado != null)
        {
            DigitadoImportado.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.DigitadoImportado)))
            {
                DigitadoImportado.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.DigitadoImportadoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TaxaSerie taxaSerie = new TaxaSerie();

        dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        int idSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal taxa = Convert.ToDecimal(textTaxa.Text);
        if (taxaSerie.LoadByPrimaryKey(data, idSerie))
        {
            taxaSerie.Taxa = taxa;
            taxaSerie.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TaxaSerie - Operacao: Update TaxaSerie: " + data + "; " + idSerie + UtilitarioWeb.ToString(taxaSerie),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        TaxaSerie taxaSerie = new TaxaSerie();

        dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        taxaSerie.IdSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
        taxaSerie.Data = Convert.ToDateTime(textData.Text);
        taxaSerie.Taxa = Convert.ToDecimal(textTaxa.Text);
        taxaSerie.DigitadoImportado = (int)Financial.RendaFixa.Enums.DigitadoImportado.Digitado;

        taxaSerie.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TaxaSerie - Operacao: Insert TaxaSerie: " + taxaSerie.Data + "; " + taxaSerie.IdSerie + UtilitarioWeb.ToString(taxaSerie),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TaxaSerieMetadata.ColumnNames.IdSerie);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TaxaSerieMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idSerie = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                TaxaSerie taxaSerie = new TaxaSerie();
                if (taxaSerie.LoadByPrimaryKey(data, idSerie))
                {

                    TaxaSerie TaxaSerieClone = (TaxaSerie)Utilitario.Clone(taxaSerie);
                    //

                    taxaSerie.MarkAsDeleted();
                    taxaSerie.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TaxaSerie - Operacao: Delete TaxaSerie: " + data + "; " + idSerie + UtilitarioWeb.ToString(TaxaSerieClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textTaxa");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
            e.Properties["cpTextTaxa"] = textTaxa.ClientID;
        }
    }
}