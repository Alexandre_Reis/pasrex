﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CotacaoMercadoAndima : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoMercadoAndima_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotacaoMercadoAndimaCollection coll = new CotacaoMercadoAndimaCollection();

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.DataReferencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.DataReferencia.LessThanOrEqual(textDataFim.Text));
        }

        coll.Query.OrderBy(coll.Query.DataReferencia.Descending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        TextBox textCodigoSELIC = gridCadastro.FindEditFormTemplateControl("textCodigoSELIC") as TextBox;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textTaxaIndicativa = gridCadastro.FindEditFormTemplateControl("textTaxaIndicativa") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDataReferencia);
        controles.Add(textDescricao);
        controles.Add(textCodigoSELIC);
        controles.Add(textDataEmissao);
        controles.Add(textDataVencimento);
        controles.Add(textTaxaIndicativa);
        controles.Add(textPU);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            string descricao = Convert.ToString(textDescricao.Text);
            string codigoSELIC = Convert.ToString(textCodigoSELIC.Text);
            DateTime dataEmissao = Convert.ToDateTime(textDataEmissao.Text);
            DateTime dataVencimento = Convert.ToDateTime(textDataVencimento.Text);

            CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
            if (cotacaoMercadoAndima.LoadByPrimaryKey(dataReferencia, descricao, codigoSELIC,
                                                      dataEmissao, dataVencimento)) {
                e.Result = "Registro já existente";
            }
        }
    }


    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();

        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        TextBox textCodigoSELIC = gridCadastro.FindEditFormTemplateControl("textCodigoSELIC") as TextBox;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textTaxaIndicativa = gridCadastro.FindEditFormTemplateControl("textTaxaIndicativa") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        string descricao = Convert.ToString(textDescricao.Text);
        string codigoSELIC = Convert.ToString(textCodigoSELIC.Text);
        DateTime dataEmissao = Convert.ToDateTime(textDataEmissao.Text);
        DateTime dataVencimento = Convert.ToDateTime(textDataVencimento.Text);

        cotacaoMercadoAndima.DataReferencia = dataReferencia;
        cotacaoMercadoAndima.Descricao = descricao;
        cotacaoMercadoAndima.CodigoSELIC = codigoSELIC;
        cotacaoMercadoAndima.DataEmissao = dataEmissao;
        cotacaoMercadoAndima.DataVencimento = dataVencimento;
        cotacaoMercadoAndima.TaxaIndicativa = Convert.ToDecimal(textTaxaIndicativa.Text);
        cotacaoMercadoAndima.Pu = Convert.ToDecimal(textPU.Text);
        cotacaoMercadoAndima.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoMercadoAndima - Operacao: Insert CotacaoMercadoAndima: " + cotacaoMercadoAndima.DataReferencia + "; " + cotacaoMercadoAndima.Descricao + "; " + cotacaoMercadoAndima.CodigoSELIC + "; " + cotacaoMercadoAndima.DataEmissao + "; " + cotacaoMercadoAndima.DataVencimento + UtilitarioWeb.ToString(cotacaoMercadoAndima),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDescricao_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as TextBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCodigoSELIC_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as TextBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataEmissao_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataVencimento_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia));
            string descricao = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoAndimaMetadata.ColumnNames.Descricao));
            string CodigoSELIC = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC));
            string dataEmissao = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao));
            string dataVencimento = Convert.ToString(e.GetListSourceFieldValue(CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento));
            e.Value = dataReferencia + descricao + CodigoSELIC + dataEmissao + dataVencimento;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();

        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        TextBox textCodigoSELIC = gridCadastro.FindEditFormTemplateControl("textCodigoSELIC") as TextBox;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textTaxaIndicativa = gridCadastro.FindEditFormTemplateControl("textTaxaIndicativa") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        string descricao = Convert.ToString(textDescricao.Text);
        string codigoSELIC = Convert.ToString(textCodigoSELIC.Text);
        DateTime dataEmissao = Convert.ToDateTime(textDataEmissao.Text);
        DateTime dataVencimento = Convert.ToDateTime(textDataVencimento.Text);
       
        if (cotacaoMercadoAndima.LoadByPrimaryKey(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento)) {
            cotacaoMercadoAndima.TaxaIndicativa = Convert.ToDecimal(textTaxaIndicativa.Text);
            cotacaoMercadoAndima.Pu = Convert.ToDecimal(textPU.Text);
            cotacaoMercadoAndima.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoMercadoAndima - Operacao: Update CotacaoMercadoAndima: " + dataReferencia + "; " + descricao + "; " + codigoSELIC + "; " + dataEmissao + "; " + dataVencimento + UtilitarioWeb.ToString(cotacaoMercadoAndima),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesDescricao = gridCadastro.GetSelectedFieldValues(CotacaoMercadoAndimaMetadata.ColumnNames.Descricao);
            List<object> keyValuesCodigoSELIC = gridCadastro.GetSelectedFieldValues(CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC);
            List<object> keyValuesDataEmissao = gridCadastro.GetSelectedFieldValues(CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao);
            List<object> keyValuesDataVencimento = gridCadastro.GetSelectedFieldValues(CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento);
            
            for (int i = 0; i < keyValuesDataReferencia.Count; i++) {
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                string descricao = Convert.ToString(keyValuesDescricao[i]);
                string codigoSELIC = Convert.ToString(keyValuesCodigoSELIC[i]);
                DateTime dataEmissao = Convert.ToDateTime(keyValuesDataEmissao[i]);
                DateTime dataVencimento = Convert.ToDateTime(keyValuesDataVencimento[i]);

                CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
                if (cotacaoMercadoAndima.LoadByPrimaryKey(dataReferencia, descricao, codigoSELIC,
                                                          dataEmissao, dataVencimento)) {

                    CotacaoMercadoAndima cotacaoMercadoAndimaClone = (CotacaoMercadoAndima)Utilitario.Clone(cotacaoMercadoAndima);
                    //

                    cotacaoMercadoAndima.MarkAsDeleted();
                    cotacaoMercadoAndima.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoMercadoAndima - Operacao: Delete CotacaoMercadoAndima: " + dataReferencia + "; " + descricao + "; " + codigoSELIC + "; " + dataEmissao + "; " + dataVencimento + UtilitarioWeb.ToString(cotacaoMercadoAndimaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textDataReferencia", "textPU");
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
            e.Properties["cpTextDataReferencia"] = textDataReferencia.ClientID;

            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            e.Properties["cpTextPU"] = textPU.ClientID;
        }
    }
}