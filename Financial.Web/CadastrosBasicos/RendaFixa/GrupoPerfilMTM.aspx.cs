﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.RendaFixa.Enums;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_GrupoPerfilMTM : CadastroBasePage
{
    ASPxTextBox textDescricao, hiddenIdGrupoPerfilMTM;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    #region DataSources
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSGrupoPerfilMTM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoPerfilMTMCollection coll = new GrupoPerfilMTMCollection();
        coll.LoadAll();
        e.Collection = coll;
    }    
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();

        textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        hiddenIdGrupoPerfilMTM = gridCadastro.FindEditFormTemplateControl("hiddenIdGrupoPerfilMTM") as ASPxTextBox;

        int IdGrupoPerfilMTM = Convert.ToInt32(hiddenIdGrupoPerfilMTM.Text);

        if (grupoPerfilMTM.LoadByPrimaryKey(IdGrupoPerfilMTM))
        {
            grupoPerfilMTM.Descricao = textDescricao.Text;
            grupoPerfilMTM.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoPerfilMTM - Operacao: Update GrupoPerfilMTM: " + grupoPerfilMTM.IdGrupoPerfilMTM.Value + "; " + grupoPerfilMTM.Descricao + UtilitarioWeb.ToString(grupoPerfilMTM),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();

        textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        grupoPerfilMTM.Descricao = textDescricao.Text;
        grupoPerfilMTM.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoPerfilMTM - Operacao: Insert GrupoPerfilMTM: " + grupoPerfilMTM.IdGrupoPerfilMTM + "; " + grupoPerfilMTM.Descricao,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupoPerfilMTM = Convert.ToInt32(keyValuesId[i]);

                GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();
                if (grupoPerfilMTM.LoadByPrimaryKey(idGrupoPerfilMTM))
                {
                    PerfilMTMCollection perfilMTMColl = new PerfilMTMCollection();
                    perfilMTMColl.Query.Where(perfilMTMColl.Query.IdGrupoPerfilMTM.Equal(idGrupoPerfilMTM));

                    if (perfilMTMColl.Query.Load())
                    {
                        perfilMTMColl.MarkAllAsDeleted();
                        perfilMTMColl.Save();
                    }

                    GrupoPerfilMTM grupoPerfilMTMClone = (GrupoPerfilMTM)Utilitario.Clone(grupoPerfilMTM);
                    //

                    grupoPerfilMTM.MarkAsDeleted();
                    grupoPerfilMTM.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoPerfilMTM - Operacao: Delete GrupoPerfilMTM: " + grupoPerfilMTMClone.IdGrupoPerfilMTM + "; " + grupoPerfilMTMClone.Descricao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnClonar")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupoOrigem = Convert.ToInt32(keyValuesId[i]);
                GrupoPerfilMTM grupoPerfilMTMOrigem = new GrupoPerfilMTM();
                grupoPerfilMTMOrigem.LoadByPrimaryKey(idGrupoOrigem);

                GrupoPerfilMTM grupoPerfilMTMDestino = new GrupoPerfilMTM();
                grupoPerfilMTMDestino.Descricao = grupoPerfilMTMOrigem.Descricao + " - CLONE do Id.Grupo Perfil MTM: " + idGrupoOrigem;
                grupoPerfilMTMDestino.Save();

                this.ClonaPerfilMTM(grupoPerfilMTMOrigem.IdGrupoPerfilMTM.Value, grupoPerfilMTMDestino.IdGrupoPerfilMTM.Value);
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    private void ClonaPerfilMTM(int idGrupoOrigem, int idGrupoDestino)
    {
        using (esTransactionScope scope = new esTransactionScope())
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" INSERT INTO PerfilMTM");
            sqlText.AppendLine(" SELECT DtReferencia,");
            sqlText.AppendLine(" IdPapel,");
            sqlText.AppendLine(" IdSerie,");
            sqlText.AppendLine(" IdTitulo,");
            sqlText.AppendLine(" IdOperacao,");
            sqlText.AppendLine(idGrupoDestino.ToString());
            sqlText.AppendLine(" FROM PerfilMTM ");
            sqlText.AppendLine(" WHERE IdGrupoPerfilMTM = " + idGrupoOrigem);

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlText.ToString());

            scope.Complete();
        }
    }

}