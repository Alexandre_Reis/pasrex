﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaEscalonaRendaFixa.aspx.cs" Inherits="CadastrosBasicos_TabelaEscalonaRF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataTituloRF(data) {
        hiddenIdTitulo.SetValue(data);
        ASPxCallback2.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTituloRF.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>

    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Escalonamento Renda Fixa"></asp:Label>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaEscalonamentoRF"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>                                
                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="true" VisibleIndex="1"></dxwgv:GridViewDataColumn>                
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" VisibleIndex="2" Width="60%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                <dxwgv:GridViewDataTextColumn FieldName="Prazo" Width="8%" VisibleIndex="3" HeaderStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit MaxLength="5"></PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>                                                                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Taxa" Caption="% Índice" VisibleIndex="4" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"/>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>' ClientInstanceName="hiddenIdTitulo" />
                        
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit" 
                                                        ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompleta")%>' OnLoad="btnEditTituloRF_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                            </tr>  
                                                                                                                                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelPrazo" runat="server" CssClass="labelRequired" Text="Prazo:"/>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="textPrazo" runat="server" ClientInstanceName="textPrazo"
                                        Text='<%# Bind("Prazo") %>' NumberType="integer" MaxLength="5" CssClass="textCurto"
                                        OnLoad="textPrazo_Load">
                                    </dxe:ASPxSpinEdit>                                
                                </td>                                                        
                            </tr>

                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTaxa" runat="server" CssClass="labelRequired" Text="% Índice:"/>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxSpinEdit ID="textTaxa" runat="server" ClientInstanceName="textTaxa"
                                        Text='<%# Eval("Taxa") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                        CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>                                
                                </td>                                                        
                            </tr>
                            
                        </table>
                        
                        <div class="linkButton linkButtonNoBorder" style="margin-left:80px;">
                        </div>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                    OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaEscalonamentoRF" runat="server" OnesSelect="EsDSTabelaEscalonamentoRF_esSelect" LowLevelBind="true"   />
    <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />        
    </form>
</body>
</html>