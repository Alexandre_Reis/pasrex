﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_MTMManual : Financial.Web.Common.CadastroBasePage
{

    #region Campos do Grid
    GridViewDataComboBoxColumn TipoOperacao;
    ASPxTextBox hiddenIdOperacao;
    ASPxDateEdit textDtVigencia;
    ASPxComboBox dropTipoTaxa;
    ASPxSpinEdit textPuMTM;
    ASPxSpinEdit textTaxa252;
    ASPxTextBox hiddenIdTitulo;
    ASPxSpinEdit btnEditCodigoCliente;
    #endregion

    #region PopUp Titulo Renda fixa
    protected void callBackPopupTitulo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);

            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery();
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();

            tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo, tituloRendaFixaQuery.IdPapel, tituloRendaFixaQuery.DescricaoCompleta);
            tituloRendaFixaQuery.Where(tituloRendaFixaQuery.IdTitulo == idTitulo);

            if (tituloRendaFixa.Load(tituloRendaFixaQuery))
                e.Result = tituloRendaFixa.IdTitulo + " - " + (string.IsNullOrEmpty(tituloRendaFixa.DescricaoCompleta) ? tituloRendaFixa.Descricao : tituloRendaFixa.DescricaoCompleta);
            //                            
        }
    }

    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.IdTitulo.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridTituloRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idTitulo = (int)gridView.GetRowValues(visibleIndex, TituloRendaFixaMetadata.ColumnNames.IdTitulo);
        e.Result = idTitulo.ToString();
    }

    protected void gridTituloRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridTituloRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridTituloRendaFixa.DataBind();
    }    

    #endregion

    #region Popup OperacaoRendaFixa
    protected void callBackPopupOperacaoRendaFixa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idOperacaoRendaFixa = Convert.ToInt32(e.Parameter);

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.IdOperacao,
                                        operacaoRendaFixa.Query.IdCliente,
                                        operacaoRendaFixa.Query.IdTitulo);
            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdOperacao == idOperacaoRendaFixa);

            if (operacaoRendaFixa.Load(operacaoRendaFixa.Query))
            {
                string strResult = string.Empty;
                strResult += operacaoRendaFixa.IdOperacao.ToString();

                e.Result = strResult;
            }
        }
    }

    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("posicao");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        OperacaoRendaFixaCollection operacaoRendaFixaColl = new OperacaoRendaFixaCollection();

        if (btnEditCodigoCliente != null && hiddenIdTitulo != null && !string.IsNullOrEmpty(btnEditCodigoCliente.Text) && !string.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.IdOperacao,
                                         operacaoRendaFixaQuery.IdCliente,
                                         operacaoRendaFixaQuery.IdTitulo,
                                         operacaoRendaFixaQuery.TipoOperacao,
                                         operacaoRendaFixaQuery.DataOperacao,
                                         posicaoRendaFixaQuery.DataVencimento,
                                         clienteQuery.DataDia.As("DataCliente"),
                                         tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoPapel"),
                                         clienteQuery.Apelido.As("ClienteApelido"));
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
            operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente.Equal(clienteQuery.IdCliente));
            operacaoRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdOperacao.Equal(posicaoRendaFixaQuery.IdOperacao));
            operacaoRendaFixaQuery.Where(tituloRendaFixaQuery.DataVencimento.GreaterThanOrEqual(clienteQuery.DataDia)
                                         & posicaoRendaFixaQuery.Quantidade.NotEqual(0)
                                         & posicaoRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text))
                                         & posicaoRendaFixaQuery.IdTitulo.Equal(Convert.ToInt32(hiddenIdTitulo.Text)));
            operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.IdOperacao.Ascending);

            operacaoRendaFixaColl.Load(operacaoRendaFixaQuery);
        }
        e.Collection = operacaoRendaFixaColl;

    }

    protected void gridOperacaoRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridOperacaoRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridOperacaoRendaFixa.DataBind();
    }

    protected void gridOperacaoRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idOperacaoRendaFixa = (int)gridView.GetRowValues(visibleIndex, OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
        e.Result = idOperacaoRendaFixa.ToString();
    }

    protected void gridOperacaoRendaFixa_load(object sender, EventArgs e)
    {
        TipoOperacao = gridOperacaoRendaFixa.Columns["TipoOperacao"] as GridViewDataComboBoxColumn;
        if (TipoOperacao != null)
        {
            TipoOperacao.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoOperacaoTitulo)))
            {
                TipoOperacao.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.TipoOperacaoTituloDescricao.RetornaStringValue(i), i);
            }
        }

    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;

        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void EsDSMTMManual_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MTMManualCollection objMTMManualColl = new MTMManualCollection();
        MTMManualQuery objMTMManualQuery = new MTMManualQuery("MTMManual");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        objMTMManualQuery.Select(objMTMManualQuery,
                                clienteQuery.Apelido,
                                tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoTitulo"),
                                clienteQuery.Apelido.As("DescricaoCliente"));
        objMTMManualQuery.LeftJoin(operacaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdOperacao.Equal(objMTMManualQuery.IdOperacao));
        objMTMManualQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo.Equal(objMTMManualQuery.IdTitulo));
        objMTMManualQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(objMTMManualQuery.IdCliente));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            objMTMManualQuery.Where(objMTMManualQuery.DtVigencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            objMTMManualQuery.Where(objMTMManualQuery.DtVigencia.LessThanOrEqual(textDataFim.Text));
        }

        objMTMManualColl.Load(objMTMManualQuery);

        e.Collection = objMTMManualColl;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        MTMManual objMTMManual = new MTMManual();
        int idMTMManual = (int)e.Keys[0];

        if (objMTMManual.LoadByPrimaryKey(idMTMManual))
        {

            hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
            hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
            btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            textDtVigencia = gridCadastro.FindEditFormTemplateControl("textDtVigencia") as ASPxDateEdit;
            textPuMTM = gridCadastro.FindEditFormTemplateControl("textPuMTM") as ASPxSpinEdit;
            textTaxa252 = gridCadastro.FindEditFormTemplateControl("textTaxa252") as ASPxSpinEdit;

            if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
                objMTMManual.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
            else
                objMTMManual.IdOperacao = null;

            objMTMManual.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
            objMTMManual.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            objMTMManual.DtVigencia = Convert.ToDateTime(textDtVigencia.Text);
            objMTMManual.PuMTM = Convert.ToDecimal(textPuMTM.Text); ;
            objMTMManual.Taxa252 = Convert.ToDecimal(textTaxa252.Text); ;

            objMTMManual.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de MTMManual - Operacao: Update MTMManual: " + UtilitarioWeb.ToString(objMTMManual),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdMTMManual");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idMTMManual = Convert.ToInt32(keyValuesId[i]);

                MTMManual objMTMManual = new MTMManual();
                if (objMTMManual.LoadByPrimaryKey(idMTMManual))
                {
                    MTMManual objMTMManualClone = (MTMManual)Utilitario.Clone(objMTMManual);
                    //
                    objMTMManual.MarkAsDeleted();
                    objMTMManual.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de MTM Manual - Operacao: Delete MTMManual: " + UtilitarioWeb.ToString(objMTMManualClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callBackCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        UtilitarioWeb util = new UtilitarioWeb();
                        if (util.IsClienteBloqueadoSinacor(idCliente))
                        {
                            resultado = "no_active";
                        }
                        else
                        {
                            PermissaoCliente permissaoCliente = new PermissaoCliente();
                            if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                            {
                                resultado = cliente.str.Apelido;
                            }
                            else
                            {
                                resultado = "no_access";
                            }
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        textDtVigencia = gridCadastro.FindEditFormTemplateControl("textDtVigencia") as ASPxDateEdit;
        textPuMTM = gridCadastro.FindEditFormTemplateControl("textPuMTM") as ASPxSpinEdit;
        textTaxa252 = gridCadastro.FindEditFormTemplateControl("textTaxa252") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenIdTitulo);
        controles.Add(btnEditCodigoCliente);
        controles.Add(textDtVigencia);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        decimal puMTM = string.IsNullOrEmpty(textPuMTM.Text) ? 0 : Convert.ToDecimal(textPuMTM.Text);
        decimal taxa252 = string.IsNullOrEmpty(textTaxa252.Text) ? 0 : Convert.ToDecimal(textTaxa252.Text);

        if (puMTM > 0 && taxa252 > 0)
        {
            e.Result = "Favor preencher somente um dos campos (PU MTM ou Taxa 252)";
            return;
        }

        if (puMTM + taxa252 == 0)
        {
            e.Result = "Favor informar PU MTM ou Taxa 252";
            return;
        }

        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing)
        {
            MTMManualCollection objMTMManualColl = new MTMManualCollection();

            objMTMManualColl.Query.Where(objMTMManualColl.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text))
                                         & objMTMManualColl.Query.IdTitulo.Equal(Convert.ToInt32(hiddenIdTitulo.Text))
                                         & objMTMManualColl.Query.DtVigencia.Equal(Convert.ToDateTime(textDtVigencia.Text)));

            if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
                objMTMManualColl.Query.Where(objMTMManualColl.Query.IdOperacao.Equal(Convert.ToInt32(hiddenIdOperacao.Text)));
            else
                objMTMManualColl.Query.Where(objMTMManualColl.Query.IdOperacao.IsNull());

            if (objMTMManualColl.Query.Load())
            {
                e.Result = "Registro já existente.!";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string IdOperacao = Convert.ToString(e.GetListSourceFieldValue(MTMManualMetadata.ColumnNames.IdOperacao));
            string DtVigencia = Convert.ToString(e.GetListSourceFieldValue(MTMManualMetadata.ColumnNames.DtVigencia));
            e.Value = IdOperacao + "|" + DtVigencia;
        }
    }

    private void SalvarNovo()
    {
        MTMManual objMTMManual = new MTMManual();

        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        textDtVigencia = gridCadastro.FindEditFormTemplateControl("textDtVigencia") as ASPxDateEdit;
        textPuMTM = gridCadastro.FindEditFormTemplateControl("textPuMTM") as ASPxSpinEdit;
        textTaxa252 = gridCadastro.FindEditFormTemplateControl("textTaxa252") as ASPxSpinEdit;

        objMTMManual = new MTMManual();

        if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
            objMTMManual.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
        else
            objMTMManual.IdOperacao = null;

        objMTMManual.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        objMTMManual.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        objMTMManual.DtVigencia = Convert.ToDateTime(textDtVigencia.Text);

        decimal puMTM = string.IsNullOrEmpty(textPuMTM.Text) ? 0 : Convert.ToDecimal(textPuMTM.Text);
        decimal taxa252 = string.IsNullOrEmpty(textTaxa252.Text) ? 0 : Convert.ToDecimal(textTaxa252.Text);

        objMTMManual.PuMTM = puMTM;
        objMTMManual.Taxa252 = taxa252;

        objMTMManual.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de MTMManual - Operacao: Insert MTMManual: " + objMTMManual.IdOperacao + "/" + objMTMManual.DtVigencia.ToString() + UtilitarioWeb.ToString(objMTMManual),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}