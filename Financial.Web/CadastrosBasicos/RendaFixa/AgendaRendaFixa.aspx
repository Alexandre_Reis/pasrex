﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgendaRendaFixa.aspx.cs" Inherits="CadastrosBasicos_AgendaRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    var isUpdateEdit = false;
    
    //Verifica qual botão está sendo clicado
    function VerificaBotaoClicado(btnClicado)
    {
        document.getElementById("hiddenBotaoClicado").value = btnClicado;
    }
    
    function OnGetDataTituloRF(data) {
    debugger;
        if(document.getElementById("hiddenBotaoClicado").value == "btnAdd")
        {
           document.getElementById(gridCadastro.cpHiddenIdTitulo).value = data;
        }
        else
        {
           document.getElementById("popupGerarAgenda_hiddenIdTituloAgenda").value = data;
        }
        ASPxCallback2.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }

    function OnClearFilterClick_Filtro()  { 
        //   
        dropTituloRendaFixa.SetValue(null);
        textDataInicio.SetValue(null);
        textDataFim.SetValue(null);
    }
    
    function Validate(s, e) {
        var gv = ASPxClientGridView.Cast(dropTituloRendaFixa.GetGridView());    
        var keyValue = gv.GetRowKey(gv.GetFocusedRowIndex());
        //   
        if(keyValue != '' && keyValue!=null) {
            var titulo = document.getElementById('popupFiltro_hiddenIdTituloFiltro_I');
            titulo.value = keyValue;
        }
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    <dxcb:ASPxCallback ID="callBackCalculaValor" runat="server" OnCallback="callBackCalculaValor_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { if(e.result != '' ) textValor.SetValue(e.result); }" />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackCalculaTaxa" runat="server" OnCallback="callBackCalculaTaxa_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { if(e.result != '' ) textTaxa.SetValue(e.result); }" />
    </dxcb:ASPxCallback>                    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != null) 
            {
                var array = e.result.split('|');
                var descricaoTitulo = array[0];
                var dataEmissao = array[1];
                var dataVencimento = array[2];
                
                if(document.getElementById('hiddenBotaoClicado').value == 'btnGerarAgenda')
                {
                    btnEditTituloRF_Aut.SetValue(descricaoTitulo); 
                    document.getElementById('popupGerarAgenda_textDataEmissaoAgenda').value = dataEmissao;
                    document.getElementById('popupGerarAgenda_textDataVencimentoAgenda').value = dataVencimento;
                }
                else                
                    btnEditTituloRF.SetValue(descricaoTitulo); 
                    

            }
        } 
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackFluxos" runat="server" OnCallback="callBackFluxos_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                 
            alert(e.result);
            gridCadastro.PerformCallback('btnRefresh');
        }        
        "/>
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackAgenda" runat="server" OnCallback="callBackAgenda_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                 
            alert(e.result);
            gridCadastro.PerformCallback('btnRefresh');
        }        
        " />
    </dxcb:ASPxCallback>
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Agenda de Eventos (Renda Fixa)"></asp:Label>
    </div>
        
    <div id="mainContent">

            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="460" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table border="0">
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelTitulo" runat="server" CssClass="labelNormal" Text="Título:"/>
                        </td>
                        <td colspan="3">
                             <dx:ASPxGridLookup ID="dropTituloRendaFixa" ClientInstanceName="dropTituloRendaFixa" runat="server" 
                                           SelectionMode="Single" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixaFiltro"
                                           Width="375px" TextFormatString="{4}" Font-Size="11px" IncrementalFilteringMode="contains"
                                           ClientSideEvents-ValueChanged="Validate" AllowUserInput="false" 
                                           >
                             <Columns>
                                 <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                 <dxwgv:GridViewDataColumn FieldName="IdTitulo" Caption="Id" Width="10%" CellStyle-Font-Size="X-Small"/>                         
                                 <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" CellStyle-Font-Size="X-Small" Width="60%" />
                                 <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Data Emissão" CellStyle-Font-Size="X-Small" Width="10%" />
                                 <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data Vencimento" CellStyle-Font-Size="X-Small" Width="10%" />                                 
                                 <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição Completa" CellStyle-Font-Size="X-Small" Settings-AutoFilterCondition="Contains" Visible="false" />
                            </Columns>
                            
                            <GridViewProperties>
                                <Settings ShowFilterRow="True" />
                            </GridViewProperties>
                                                        
                        </dx:ASPxGridLookup>
                                                    
                        </td>
                    </tr>
                                        
                    <tr>                    
                        <td class="td_Label"><asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Evento Início:"/></td>                                            
                        
                        <td colspan="3"><table border="0"><tr>
                        
                        <td><dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/></td>                      
                        <td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Evento Fim:"/></td>
                        <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td>
                        
                        </tr></table>                                                    
                        </td>                        
                        
                    </tr>
                    
                    <tr>
                    <td>                    
                    <dxe:ASPxTextBox ID="hiddenIdTituloFiltro" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdTituloFiltro" />                    
                    </td>                    
                    </tr>
                    </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_Filtro(); popupFiltro_hiddenIdTituloFiltro_I.value=''; return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>
        
            <dxpc:ASPxPopupControl ID="popupFluxos" AllowDragging="true" PopupElementID="popupLote" EnableClientSideAPI="True"
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Geração Automática de Fluxos de Pagamento" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label122" runat="server" CssClass="labelNormal" Text="Periodicidade (Meses):"> </asp:Label>
                            </td>
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="textPeriodicidade" runat="server" CssClass="textNormal_5"
                                            ClientInstanceName="textPeriodicidade" MaxLength="4" NumberType="Integer">
                                </dxe:ASPxSpinEdit>
                            </td>       
                                                        
                            <td class="td_Label">
                                <asp:Label ID="label7" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Id Título:"></asp:Label>
                            </td>
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="textIdTitulo" runat="server" CssClass="textNormal_5"
                                            ClientInstanceName="textIdTitulo" MaxLength="10" NumberType="Integer">
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label4" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Tipo:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoEventoGerar" runat="server" 
                                                                ShowShadow="false" DropDownStyle="DropDownList"
                                                                CssClass="dropDownListCurto_1" Text='<%#Eval("TipoEvento")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Juros" />
                                    <dxe:ListEditItem Value="2" Text="Juros+Correção" />
                                    <dxe:ListEditItem Value="3" Text="Amortização" />
                                    <dxe:ListEditItem Value="8" Text="Amort.Corrigida" />
                                    <dxe:ListEditItem Value="4" Text="Pagto Principal" />
                                    <dxe:ListEditItem Value="5" Text="Incorp. Correção" />
                                    <dxe:ListEditItem Value="6" Text="Incorp. Juros" />
                                    <dxe:ListEditItem Value="7" Text="Pagto PU" />
                                    </Items>                                                
                                </dxe:ASPxComboBox>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Taxa:"></asp:Label>
                            </td>
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="textTaxaGerar" runat="server" CssClass="textPerc"
                                            ClientInstanceName="textTaxaGerar" MaxLength="12" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="label5" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Valor:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="textValorGerar" runat="server" CssClass="textValor"
                                            ClientInstanceName="textValorGerar" MaxLength="20" NumberType="Float" DecimalPlaces="8">
                                </dxe:ASPxSpinEdit>
                            </td>
                        </tr>    
                    </table>        
                
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnProcessa" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="callBackFluxos.SendCallback(); return false;"><asp:Literal ID="Literal15" runat="server" Text="Gerar Fluxos"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>
            <dxpc:ASPxPopupControl ID="popupGerarAgenda" AllowDragging="true" PopupElementID="popupLote"
                EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Geração Automática de Agendas"
                runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <table>
                            <tr>
                                <asp:Label ID="label8" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                    Visible="false"></asp:Label>
                                <td class="td_Label">
                                    <asp:Label ID="label6" runat="server" CssClass="labelRequired" Text="Título:" />
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="hiddenIdTituloAgenda" runat="server" CssClass="hiddenField" />
                                    <dxe:ASPxButtonEdit ID="btnEditTituloRF_Aut" runat="server" CssClass="textButtonEdit"
                                        ClientInstanceName="btnEditTituloRF_Aut" ReadOnly="true" Width="380px">
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>
                                        </Buttons>
                                        <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />
                                    </dxe:ASPxButtonEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label9" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoEventoAgenda" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                        CssClass="dropDownListCurto_2">
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Juros" />
                                            <dxe:ListEditItem Value="2" Text="Juros+Correção" />
                                            <dxe:ListEditItem Value="3" Text="Amortização" />
                                            <dxe:ListEditItem Value="8" Text="Amort.Corrigida" />
                                            <dxe:ListEditItem Value="5" Text="Pagto Correção" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="labelPreencher" runat="server" CssClass="labelRequired" Text="Preencher a partir do:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropPreencher" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                        CssClass="dropDownListCurto_2">
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Vencimento" />
                                            <dxe:ListEditItem Value="2" Text="Emissão" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelPeriodicidade" runat="server" CssClass="labelRequired" Text="Periodicidade:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropPeriodicidadeAgenda" runat="server" ShowShadow="false"
                                        DropDownStyle="DropDownList" CssClass="dropDownListCurto_2">
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Mensal" />
                                            <dxe:ListEditItem Value="2" Text="Semestral" />
                                            <dxe:ListEditItem Value="3" Text="Anual" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                 <td class="td_Label">
                                    <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Data Limite:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataLimite" runat="server" ClientInstanceName="textDataLimite" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textTaxaAgenda" runat="server" CssClass="textPercCurto" ClientInstanceName="textTaxa"
                                        MaxLength="26" NumberType="Float" DecimalPlaces="20">
                                    </dxe:ASPxSpinEdit>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Valor:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textValorAgenda" runat="server" CssClass="textValor" ClientInstanceName="textValor"
                                        MaxLength="20" NumberType="Float" DecimalPlaces="8">
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label13" runat="server" CssClass="hiddenField" Text="Data Emissão:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="textDataEmissaoAgenda" runat="server" CssClass="hiddenField" />
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label14" runat="server" CssClass="hiddenField" Text="Data Vencimento:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="textDataVencimentoAgenda" runat="server" CssClass="hiddenField" />
                                </td>
                            </tr>
                        </table>
                        <div class="linhaH">
                        </div>
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                <asp:LinkButton ID="btnGerarListaAgenda" runat="server" Font-Overline="false" ForeColor="Black"
                                    CssClass="btnOK" OnClientClick="callBackAgenda.SendCallback(); return false;">
                                    <asp:Literal ID="Literal11" runat="server" Text="Gerar Agenda" /><div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>                    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); VerificaBotaoClicado('btnAdd'); return false; "><asp:Literal ID="Literal1" runat="server" Text="Novo" /><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.PerformCallback('btnDelete');} return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFluxos" runat="server" Font-Overline="false" Visible="false" ValidationGroup="ATK" CssClass="btnLote" OnClientClick="popupFluxos.ShowWindow(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Gerar Fluxos"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel"  OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnGerarAgenda" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnLote" OnClientClick="popupGerarAgenda.ShowWindow(); VerificaBotaoClicado('btnGerarAgenda'); return false;"><asp:Literal ID="Literal10" runat="server" Text="Gerar Agenda" /><div></div></asp:LinkButton>
            </div>
            <dxe:ASPxTextBox ID="hiddenBotaoClicado" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenBotaoClicado" />
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdAgenda" DataSourceID="EsDSAgendaRendaFixa"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"     
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"                        
                        OnPreRender="gridCadastro_PreRender"                        
                        >                    
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdAgenda" Caption="Evento" VisibleIndex="2" Width="5%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"></dxwgv:GridViewDataColumn>
                                                
                        <dxwgv:GridViewDataColumn FieldName="IdTitulo" Caption="Titulo" VisibleIndex="3" Width="5%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"/>
                        
                        <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Descrição" VisibleIndex="4" Width="40%" ExportWidth="430" Settings-AutoFilterCondition="Contains"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEvento" Caption="Tipo" VisibleIndex="6" Width="10%" ExportWidth="100">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Juros'>Juros</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Juros+Correção'>Juros+Correção</div>" />
                                    <dxe:ListEditItem Value="3" Text="<div title='Amortização'>Amortização</div>" />
                                    <dxe:ListEditItem Value="8" Text="<div title='Amort.Corrigida'>Amort.Corrigida</div>" />
                                    <dxe:ListEditItem Value="4" Text="<div title='Pagamento Principal'>Pagto Principal</div>" />
                                    <dxe:ListEditItem Value="5" Text="<div title='Pagamento Correção'>Pagto Correção</div>" />
                                    <dxe:ListEditItem Value="6" Text="<div title='Incorporação Juros'>Incorp. Juros</div>" />
                                    <dxe:ListEditItem Value="7" Text="<div title='Pagamento por PU'>Pagto PU</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataAgenda" VisibleIndex="7" Width="8%"/>
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataEvento" VisibleIndex="8" Width="8%"/>
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataPagamento" VisibleIndex="10" Width="8%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="12" Width="6%"
                                HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="14" Width="10%"
                                HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                                                                                                
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server">
                        
                        <div class="editForm">                            
                            
                            <table>
                                <tr>
                                    <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal" Visible="false"></asp:Label>
                                    
                                    <td  class="td_Label">
                                        <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                    </td>                                     
                                    
                                    <td colspan="3">
                                        <asp:TextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField"
                                                 Text='<%#Eval("IdTitulo")%>'    />
                                        <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit" 
                                                            ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px"
                                                            Text='<%#Eval("DescricaoCompleta")%>' > 
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>        
                                        <ClientSideEvents
                                                 ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}"                          
                                        />                            
                                        </dxe:ASPxButtonEdit>                                
                                    </td>
                                </tr>  
                          
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td >      
                                        <dxe:ASPxComboBox ID="dropTipoEvento" runat="server" ClientInstanceName="dropTipoEvento"
                                                            ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownListCurto_2" Text='<%#Eval("TipoEvento")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Juros" />
                                        <dxe:ListEditItem Value="2" Text="Juros+Correção" />
                                        <dxe:ListEditItem Value="3" Text="Amortização" />
                                        <dxe:ListEditItem Value="8" Text="Amort.Corrigida" />
                                        <dxe:ListEditItem Value="4" Text="Pagto Principal" />
                                        <dxe:ListEditItem Value="5" Text="Pagto Correção" />
                                        <dxe:ListEditItem Value="6" Text="Incorp. Juros" />
                                        <dxe:ListEditItem Value="7" Text="Pagto PU" />
                                        </Items>                                                
                                        </dxe:ASPxComboBox>                 
                                    </td>
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="labelDataAgenda" runat="server" CssClass="labelRequired" Text="Data Agenda:" />
                                    </td>                                     
                                    <td>
                                        <dxe:ASPxDateEdit ID="textDataAgenda" runat="server" ClientInstanceName="textDataAgenda" Value='<%#Eval("DataAgenda")%>' />
                                    </td>  
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelDataEvento" runat="server" CssClass="labelRequired" Text="Dt Evento:" />
                                    </td>                                     
                                    <td >
                                        <dxe:ASPxDateEdit ID="textDataEvento" runat="server" ClientInstanceName="textDataEvento" Value='<%#Eval("DataEvento")%>'/>
                                    </td> 
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Pagamento:" />
                                    </td>                                     
                                    <td >
                                        <dxe:ASPxDateEdit ID="textDataPagamento" runat="server" ClientInstanceName="textDataPagamento" Value='<%#Eval("DataPagamento")%>'/>
                                    </td>                                                                         
                                </tr>
                                    
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTaxa" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" Style="float: left;" ClientInstanceName="textTaxa"
                                                                         MaxLength="26" NumberType="Float" DecimalPlaces="20" DisplayFormatString="{0:#,##0.0000000000;(#,##0.0000000000);0.0000000000}"
                                                                         Text='<%#Eval("Taxa")%>' >  
                                        <ClientSideEvents NumberChanged="function(s, e) { var tipoEvento = dropTipoEvento.GetSelectedItem().value; 
                                                                                          if(tipoEvento == 1 || tipoEvento == 2 || tipoEvento == 3 || tipoEvento == 8) 
                                                                                            callBackCalculaValor.SendCallback(); 
                                                                                        }" />                                                 
                                        </dxe:ASPxSpinEdit>
                                    </td>                                    
                                    <td class="td_Label">
                                        <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Valor:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor" ClientInstanceName="textValor"
                                                                         MaxLength="20" NumberType="Float" DecimalPlaces="12" DisplayFormatString="N"
                                                                         Text='<%#Eval("Valor")%>' >            
                                        <ClientSideEvents NumberChanged="function(s, e) { var tipoEvento = dropTipoEvento.GetSelectedItem().value; 
                                                                                          if(tipoEvento == 1 || tipoEvento == 2 || tipoEvento == 3 || tipoEvento == 8) 
                                                                                            callBackCalculaTaxa.SendCallback(); 
                                                                                        }" />                                                                          
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>  
                                
                            </table>
                           
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal7" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>     
                        
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="400px"  />
                    <ClientSideEvents BeginCallback="function(s, e) {
						if (e.command == 'UPDATEEDIT')
                        {
                            isUpdateEdit = true;      
                        } 
                        						
                    }" EndCallback="function(s, e) {
						if (isUpdateEdit)
                        {
                            isUpdateEdit = false;
                            if(gridCadastro.cpMessage != '' && gridCadastro.cpMessage != null)
                            {
                                alert(gridCadastro.cpMessage);
                                gridCadastro.cpMessage = '';
                            }
                        }
                    }" />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                </dxwgv:ASPxGridView>            
            </div>                    
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSAgendaRendaFixa" runat="server" OnesSelect="EsDSAgendaRendaFixa_esSelect" LowLevelBind="true"/>
    <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
    <cc1:esDataSource ID="EsDSTituloRendaFixaFiltro" runat="server" OnesSelect="EsDSTituloRendaFixaFiltro_esSelect" />
    
    </form>
</body>
</html>