﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using Financial.Common.Enums;
using Financial.RendaFixa.Enums;

public partial class CadastrosBasicos_PerfilMTM : Financial.Web.Common.CadastroBasePage
{

    #region Campos do Grid
    ASPxTextBox hiddenIdOperacao;
    ASPxTextBox hiddenIdSerie;
    ASPxTextBox hiddenIdTitulo;
    ASPxTextBox hiddenIdPapel;
    ASPxTextBox hiddenIdGrupoPerfilMTM;
    ASPxDateEdit textDtReferencia;
    ASPxSpinEdit textPuMTM;
    ASPxSpinEdit textTaxa252;
    #endregion

    #region PopUp Serie Renda fixa
    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        coll = this.obterSeries(null);

        e.Collection = coll;
    }

    protected void gridSerieRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idSerie = (int)gridView.GetRowValues(visibleIndex, SerieRendaFixaMetadata.ColumnNames.IdSerie);
        e.Result = idSerie.ToString();
    }

    protected void gridSerieRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridSerieRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridSerieRendaFixa.DataBind();
    }

    protected void callBackPopupSerie_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idSerie = Convert.ToInt32(e.Parameter);

            SerieRendaFixa serieRendaFixa = this.obterSeries(idSerie)[0];

            if (serieRendaFixa.IdSerie.HasValue)
                e.Result = serieRendaFixa.IdSerie + " - " + serieRendaFixa.Descricao + " - " + serieRendaFixa.UpToFeederByIdFeeder.Descricao;
        }
    }

    #endregion

    #region PopUp Titulo Renda fixa
    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.IdTitulo.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridTituloRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idTitulo = (int)gridView.GetRowValues(visibleIndex, TituloRendaFixaMetadata.ColumnNames.IdTitulo);
        e.Result = idTitulo.ToString();
    }

    protected void gridTituloRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridTituloRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridTituloRendaFixa.DataBind();
    }

    protected void callBackPopupTitulo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);

            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery();
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();

            tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo, tituloRendaFixaQuery.IdPapel, tituloRendaFixaQuery.DescricaoCompleta);
            tituloRendaFixaQuery.Where(tituloRendaFixaQuery.IdTitulo == idTitulo);

            if (tituloRendaFixa.Load(tituloRendaFixaQuery))
                e.Result = tituloRendaFixa.IdTitulo + " - " + tituloRendaFixa.DescricaoCompleta + "|" + tituloRendaFixa.IdPapel.Value;
            //                            
        }
    }

    #endregion

    #region PopUp Papel Renda fixa
    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.IdPapel.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridPapelRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idPapel = (int)gridView.GetRowValues(visibleIndex, PapelRendaFixaMetadata.ColumnNames.IdPapel);
        e.Result = idPapel.ToString();
    }

    protected void gridPapelRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridPapelRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridPapelRendaFixa.DataBind();
    }

    protected void callBackPopupPapel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPapel = Convert.ToInt32(e.Parameter);

            PapelRendaFixaQuery PapelRendaFixaQuery = new PapelRendaFixaQuery();
            PapelRendaFixa PapelRendaFixa = new PapelRendaFixa();

            PapelRendaFixaQuery.Select(PapelRendaFixaQuery.IdPapel, PapelRendaFixaQuery.Descricao);
            PapelRendaFixaQuery.Where(PapelRendaFixaQuery.IdPapel == idPapel);

            if (PapelRendaFixa.Load(PapelRendaFixaQuery))
                e.Result = PapelRendaFixa.IdPapel + " - " + PapelRendaFixa.Descricao;
            //                            
        }
    }

    #endregion

    #region Popup OperacaoRendaFixa
    protected void callBackPopupOperacaoRendaFixa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idOperacaoRendaFixa = Convert.ToInt32(e.Parameter);

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.IdOperacao,
                                        operacaoRendaFixa.Query.IdCliente,
                                        operacaoRendaFixa.Query.IdTitulo);
            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdOperacao == idOperacaoRendaFixa);

            if (operacaoRendaFixa.Load(operacaoRendaFixa.Query))
            {
                string strResult = string.Empty;
                strResult += operacaoRendaFixa.IdOperacao + " - Cliente " + operacaoRendaFixa.IdCliente + " - Título " + operacaoRendaFixa.IdTitulo + "|" + operacaoRendaFixa.IdTitulo;

                e.Result = strResult;
            }
        }
    }

    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        List<int> lstTipoOperacao = new List<int>();
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.AntecipacaoRecompra);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.CompraCasada);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.CompraFinal);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.CompraRevenda);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.Deposito);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoCota);
        lstTipoOperacao.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoQtde);

        OperacaoRendaFixaCollection operacaoRendaFixaColl = new OperacaoRendaFixaCollection();

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.IdOperacao,
                                     operacaoRendaFixaQuery.IdCliente,
                                     operacaoRendaFixaQuery.IdTitulo,
                                     operacaoRendaFixaQuery.TipoOperacao,
                                     operacaoRendaFixaQuery.DataOperacao,
                                     operacaoRendaFixaQuery.DataLiquidacao,
                                     tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoPapel"),
                                     clienteQuery.Apelido.As("ClienteApelido"));
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente.Equal(clienteQuery.IdCliente));
        operacaoRendaFixaQuery.Where(tituloRendaFixaQuery.DataVencimento.GreaterThanOrEqual(clienteQuery.DataDia) & operacaoRendaFixaQuery.TipoOperacao.In(lstTipoOperacao));
        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.IdOperacao.Ascending);

        operacaoRendaFixaColl.Load(operacaoRendaFixaQuery);
        e.Collection = operacaoRendaFixaColl;

    }

    protected void gridOperacaoRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridOperacaoRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridOperacaoRendaFixa.DataBind();
    }

    protected void gridOperacaoRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idOperacaoRendaFixa = (int)gridView.GetRowValues(visibleIndex, OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
        e.Result = idOperacaoRendaFixa.ToString();
    }

    #endregion

    #region Popup Grupo Perfil MTM
    protected void gridGrupoPerfilMTM_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idGrupoPerfilMTM = (int)gridView.GetRowValues(visibleIndex, GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM);
        e.Result = idGrupoPerfilMTM.ToString();
    }

    protected void gridGrupoPerfilMTM_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridGrupoPerfilMTM.DataBind();
    }

    protected void gridGrupoPerfilMTM_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void callBackPopupGrupoPerfilMTM_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idGrupoPerfilMTM = Convert.ToInt32(e.Parameter);

            GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();

            grupoPerfilMTM.Query.Where(grupoPerfilMTM.Query.IdGrupoPerfilMTM.Equal(idGrupoPerfilMTM));

            if (grupoPerfilMTM.Query.Load())
            {
                string strResult = string.Empty;
                strResult += grupoPerfilMTM.IdGrupoPerfilMTM.Value + " - " + grupoPerfilMTM.Descricao;                    

                e.Result = strResult;
            }
        }
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void EsDSGrupoPerfilMTM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoPerfilMTMCollection grupoPerfilMTMColl = new GrupoPerfilMTMCollection();
        grupoPerfilMTMColl.LoadAll();

        e.Collection = grupoPerfilMTMColl;

    }
    protected void EsDSPerfilMTM_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilMTMCollection perfilMTMColl = new PerfilMTMCollection();
        PerfilMTMQuery perfilMTMQuery = new PerfilMTMQuery("PerfilMTM");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("papel");
        FeederQuery feederQuery = new FeederQuery("feeder");
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        GrupoPerfilMTMQuery grupoPerfilMTMQuery = new GrupoPerfilMTMQuery("grupoPerfil");

        perfilMTMQuery.Select(perfilMTMQuery,
                                tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoTitulo"),
                                tituloRendaFixaQuery.DataVencimento.As("DataVencimentoTitulo"),
                                papelRendaFixaQuery.Descricao.As("DescricaoPapel"),
                                operacaoRendaFixaQuery.IdCliente,
                                (serieRendaFixaQuery.Descricao + " - " + feederQuery.Descricao).As("DescricaoSerie"),
                                grupoPerfilMTMQuery.Descricao.As("DescricaoGrupo"));
        perfilMTMQuery.InnerJoin(grupoPerfilMTMQuery).On(grupoPerfilMTMQuery.IdGrupoPerfilMTM.Equal(perfilMTMQuery.IdGrupoPerfilMTM));
        perfilMTMQuery.InnerJoin(serieRendaFixaQuery).On(serieRendaFixaQuery.IdSerie.Equal(perfilMTMQuery.IdSerie));
        perfilMTMQuery.InnerJoin(feederQuery).On(feederQuery.IdFeeder.Equal(serieRendaFixaQuery.IdFeeder));
        perfilMTMQuery.LeftJoin(operacaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdOperacao.Equal(perfilMTMQuery.IdOperacao));
        perfilMTMQuery.LeftJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo.Equal(perfilMTMQuery.IdTitulo));
        perfilMTMQuery.LeftJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel.Equal(perfilMTMQuery.IdPapel));

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            perfilMTMQuery.Where(perfilMTMQuery.DtReferencia.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            perfilMTMQuery.Where(perfilMTMQuery.DtReferencia.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
        }

        perfilMTMQuery.OrderBy(perfilMTMQuery.DtReferencia.Descending);

        perfilMTMColl.Load(perfilMTMQuery);

        e.Collection = perfilMTMColl;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        PerfilMTM objPerfilMTM = new PerfilMTM();
        int idPerfilMTM = (int)e.Keys[0];

        if (objPerfilMTM.LoadByPrimaryKey(idPerfilMTM))
        {
            hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox;
            textDtReferencia = gridCadastro.FindEditFormTemplateControl("textDtReferencia") as ASPxDateEdit;
            hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
            hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
            hiddenIdPapel = gridCadastro.FindEditFormTemplateControl("hiddenIdPapel") as ASPxTextBox;
            hiddenIdGrupoPerfilMTM = gridCadastro.FindEditFormTemplateControl("hiddenIdGrupoPerfilMTM") as ASPxTextBox;

            objPerfilMTM.IdSerie = Convert.ToInt32(hiddenIdSerie.Text);
            objPerfilMTM.DtReferencia = Convert.ToDateTime(textDtReferencia.Text);

            if (!string.IsNullOrEmpty(hiddenIdGrupoPerfilMTM.Text))
                objPerfilMTM.IdGrupoPerfilMTM = Convert.ToInt32(hiddenIdGrupoPerfilMTM.Text);            

            OperacaoRendaFixa operacaoRF = new OperacaoRendaFixa();
            if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
            {
                objPerfilMTM.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
                operacaoRF.LoadByPrimaryKey(objPerfilMTM.IdOperacao.Value);
            }
            else
            {
                objPerfilMTM.IdOperacao = null;
            }

            if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
                objPerfilMTM.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
            else
            {
                if (operacaoRF.IdTitulo.HasValue)
                    objPerfilMTM.IdTitulo = operacaoRF.IdTitulo.Value;
                else
                    objPerfilMTM.IdTitulo = null;
            }

            if (!string.IsNullOrEmpty(hiddenIdPapel.Text))
                objPerfilMTM.IdPapel = Convert.ToInt32(hiddenIdPapel.Text);
            else
            {
                if (operacaoRF.IdTitulo.HasValue)
                    objPerfilMTM.IdPapel = operacaoRF.UpToTituloRendaFixaByIdTitulo.IdPapel.Value;
                else
                    objPerfilMTM.IdPapel = null;
            }

            objPerfilMTM.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PerfilMTM - Operacao: Update PerfilMTM: " + idPerfilMTM + UtilitarioWeb.ToString(objPerfilMTM),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilMTM");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPerfilMTM = Convert.ToInt32(keyValuesId[i]);

                PerfilMTM objPerfilMTM = new PerfilMTM();
                if (objPerfilMTM.LoadByPrimaryKey(idPerfilMTM))
                {
                    PerfilMTM objPerfilMTMClone = (PerfilMTM)Utilitario.Clone(objPerfilMTM);
                    //
                    objPerfilMTM.MarkAsDeleted();
                    objPerfilMTM.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de MTM Manual - Operacao: Delete PerfilMTM: " + idPerfilMTM + UtilitarioWeb.ToString(objPerfilMTMClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        if (e.Parameters == "btnResetar")
        {
            PerfilMTMCollection objPerfilMTMCollection = new PerfilMTMCollection();
            PerfilMTM objPerfilMTM = new PerfilMTM();
            DateTime dataBase = new DateTime();
            DateTime dataAlvo = new DateTime();

            objPerfilMTM.Query.Select(objPerfilMTM.Query.DtReferencia.Max());
            objPerfilMTM.Load(objPerfilMTM.Query);

            dataAlvo = (DateTime)objPerfilMTM.DtReferencia;
            dataBase = Calendario.SubtraiDiaUtil(dataAlvo, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            objPerfilMTMCollection.copiarPerfilMTM(dataBase, dataAlvo, true);

        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox;
        textDtReferencia = gridCadastro.FindEditFormTemplateControl("textDtReferencia") as ASPxDateEdit;
        hiddenIdPapel = gridCadastro.FindEditFormTemplateControl("hiddenIdPapel") as ASPxTextBox;
        hiddenIdGrupoPerfilMTM = gridCadastro.FindEditFormTemplateControl("hiddenIdGrupoPerfilMTM") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenIdSerie);
        controles.Add(textDtReferencia);
        controles.Add(hiddenIdPapel);
        controles.Add(hiddenIdGrupoPerfilMTM);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        hiddenIdPapel = gridCadastro.FindEditFormTemplateControl("hiddenIdPapel") as ASPxTextBox;

        PerfilMTM objPerfilMTM = new PerfilMTM();
        PerfilMTMQuery objPerfilMTMQuery = new PerfilMTMQuery();

        if (!string.IsNullOrEmpty(hiddenIdGrupoPerfilMTM.Text))
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdGrupoPerfilMTM.Equal(Convert.ToInt32(hiddenIdGrupoPerfilMTM.Text)));
        else
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdGrupoPerfilMTM.IsNull());

        if (!string.IsNullOrEmpty(textDtReferencia.Text))
            objPerfilMTMQuery.Where(objPerfilMTMQuery.DtReferencia.Equal( Convert.ToDateTime(textDtReferencia.Text)));
        else
            objPerfilMTMQuery.Where(objPerfilMTMQuery.DtReferencia.IsNull());

        if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdOperacao.Equal(Convert.ToInt32(hiddenIdOperacao.Text)));
        else
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdOperacao.IsNull());

        if (!string.IsNullOrEmpty(hiddenIdPapel.Text))
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdPapel.Equal(Convert.ToInt32(hiddenIdPapel.Text)));
        else
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdPapel.IsNull());

        if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdTitulo.Equal(Convert.ToInt32(hiddenIdTitulo.Text)));
        else
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdTitulo.IsNull());

        // Somente se for Update
        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxTextBox hiddenIdPerfilMTM = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfilMTM") as ASPxTextBox;
            int idPerfilMTM = Convert.ToInt32(hiddenIdPerfilMTM.Text);
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdPerfilMTM.NotEqual(idPerfilMTM));

        }


        if (objPerfilMTM.Load(objPerfilMTMQuery))
        {
            e.Result = "Registro já existente.!";
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        PerfilMTM objPerfilMTM = new PerfilMTM();

        hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox;
        textDtReferencia = gridCadastro.FindEditFormTemplateControl("textDtReferencia") as ASPxDateEdit;
        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxTextBox;
        hiddenIdPapel = gridCadastro.FindEditFormTemplateControl("hiddenIdPapel") as ASPxTextBox;
        hiddenIdGrupoPerfilMTM = gridCadastro.FindEditFormTemplateControl("hiddenIdGrupoPerfilMTM") as ASPxTextBox;

        objPerfilMTM.IdSerie = Convert.ToInt32(hiddenIdSerie.Text);
        objPerfilMTM.DtReferencia = Convert.ToDateTime(textDtReferencia.Text);

        if (!string.IsNullOrEmpty(hiddenIdGrupoPerfilMTM.Text))
            objPerfilMTM.IdGrupoPerfilMTM = Convert.ToInt32(hiddenIdGrupoPerfilMTM.Text);    

        OperacaoRendaFixa operacaoRF = new OperacaoRendaFixa();
        if (!string.IsNullOrEmpty(hiddenIdOperacao.Text))
        {
            objPerfilMTM.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
            operacaoRF.LoadByPrimaryKey(objPerfilMTM.IdOperacao.Value);
        }
        else
        {
            objPerfilMTM.IdOperacao = null;
        }

        if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
            objPerfilMTM.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        else
        {
            if (operacaoRF.IdTitulo.HasValue)
                objPerfilMTM.IdTitulo = operacaoRF.IdTitulo.Value;
            else
                objPerfilMTM.IdTitulo = null;
        }

        if (!string.IsNullOrEmpty(hiddenIdPapel.Text))
            objPerfilMTM.IdPapel = Convert.ToInt32(hiddenIdPapel.Text);
        else
        {
            if (operacaoRF.IdTitulo.HasValue)
                objPerfilMTM.IdPapel = operacaoRF.UpToTituloRendaFixaByIdTitulo.IdPapel.Value;
            else
                objPerfilMTM.IdPapel = null;
        }
        

        objPerfilMTM.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilMTM - Operacao: Insert PerfilMTM: " + objPerfilMTM.IdPerfilMTM + UtilitarioWeb.ToString(objPerfilMTM),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }

    private SerieRendaFixaCollection obterSeries(int? IdSerie)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        FeederQuery feederQuery = new FeederQuery("feeder");
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serieRendaFixa");
        CurvaRendaFixaQuery curvaQuery = new CurvaRendaFixaQuery("curva");

        serieRendaFixaQuery.Select(serieRendaFixaQuery,
                                   feederQuery.Descricao.As("DescricaoFeeder"));
        serieRendaFixaQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder.Equal(feederQuery.IdFeeder));
        serieRendaFixaQuery.LeftJoin(curvaQuery).On(serieRendaFixaQuery.IdSerie.Equal(curvaQuery.IdCurvaRendaFixa));

        if (IdSerie.HasValue)
            serieRendaFixaQuery.Where(serieRendaFixaQuery.IdSerie == IdSerie);
        else
            serieRendaFixaQuery.Where(curvaQuery.TipoCurva.IsNull() | curvaQuery.TipoCurva.NotIn((int)TipoCurva.Spread, (int)TipoCurva.Fator));

        serieRendaFixaQuery.OrderBy(serieRendaFixaQuery.IdSerie.Ascending);

        coll.Load(serieRendaFixaQuery);

        return coll;
    }
}