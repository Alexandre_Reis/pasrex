﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_RepactuacaoRendaFixa : Financial.Web.Common.CadastroBasePage
{

    #region Campos do Grid
    ASPxTextBox hiddenIdTituloNovo;
    ASPxTextBox hiddenIdTituloOrig;
    ASPxDateEdit textDtEvento;
    #endregion

    #region PopUp Titulo Renda fixa Novo
    protected void EsDSTituloRendaFixaNovo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.Where(coll.Query.PermiteRepactuacao.Equal("S"));
        coll.Query.OrderBy(coll.Query.IdTitulo.Ascending);        
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridTituloRendaFixaNovo_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idTitulo = (int)gridView.GetRowValues(visibleIndex, TituloRendaFixaMetadata.ColumnNames.IdTitulo);
        e.Result = idTitulo.ToString();
    }

    protected void gridTituloRendaFixaNovo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridTituloRendaFixaNovo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridTituloRendaFixaNovo.DataBind();
    }

    protected void callBackPopupTituloNovo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);

            TituloRendaFixaQuery TituloRendaFixaQuery = new TituloRendaFixaQuery();
            TituloRendaFixa TituloRendaFixa = new TituloRendaFixa();

            TituloRendaFixaQuery.Select(TituloRendaFixaQuery.IdTitulo, TituloRendaFixaQuery.DescricaoCompleta);
            TituloRendaFixaQuery.Where(TituloRendaFixaQuery.IdTitulo.Equal(idTitulo));

            if (TituloRendaFixa.Load(TituloRendaFixaQuery))
                e.Result = TituloRendaFixa.IdTitulo + " - " + TituloRendaFixa.DescricaoCompleta;
            //                            
        }
    }
    #endregion

    #region PopUp Titulo Renda fixa Orig
    protected void EsDSTituloRendaFixaOrig_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.Where(coll.Query.PermiteRepactuacao.Equal("S"));
        coll.Query.OrderBy(coll.Query.IdTitulo.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridTituloRendaFixaOrig_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idTitulo = (int)gridView.GetRowValues(visibleIndex, TituloRendaFixaMetadata.ColumnNames.IdTitulo);
        e.Result = idTitulo.ToString();
    }

    protected void gridTituloRendaFixaOrig_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridTituloRendaFixaOrig_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridTituloRendaFixaOrig.DataBind();
    }

    protected void callBackPopupTituloOrig_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);

            TituloRendaFixaQuery TituloRendaFixaQuery = new TituloRendaFixaQuery();
            TituloRendaFixa TituloRendaFixa = new TituloRendaFixa();

            TituloRendaFixaQuery.Select(TituloRendaFixaQuery.IdTitulo, TituloRendaFixaQuery.DescricaoCompleta);
            TituloRendaFixaQuery.Where(TituloRendaFixaQuery.IdTitulo.Equal(idTitulo));

            if (TituloRendaFixa.Load(TituloRendaFixaQuery))
                e.Result = TituloRendaFixa.IdTitulo + " - " + TituloRendaFixa.DescricaoCompleta;
            //                            
        }
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void EsDSRepactuacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        RepactuacaoRendaFixaCollection RepactuacaoRendaFixaColl = new RepactuacaoRendaFixaCollection();
        RepactuacaoRendaFixaQuery repactuacaoQuery = new RepactuacaoRendaFixaQuery("Repactuacao");
        TituloRendaFixaQuery tituloNovoQuery = new TituloRendaFixaQuery("TituloNovo");
        TituloRendaFixaQuery tituloOrigQuery = new TituloRendaFixaQuery("tituloOrig");

        repactuacaoQuery.Select(repactuacaoQuery,
                                tituloNovoQuery.DescricaoCompleta.As("DescricaoTituloNovo"),
                                tituloOrigQuery.DescricaoCompleta.As("DescricaoTituloOriginal"));
        repactuacaoQuery.InnerJoin(tituloNovoQuery).On(repactuacaoQuery.IdTituloNovo.Equal(tituloNovoQuery.IdTitulo));
        repactuacaoQuery.InnerJoin(tituloOrigQuery).On(repactuacaoQuery.IdTituloOriginal.Equal(tituloOrigQuery.IdTitulo));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            repactuacaoQuery.Where(repactuacaoQuery.DataEvento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            repactuacaoQuery.Where(repactuacaoQuery.DataEvento.LessThanOrEqual(textDataFim.Text));
        }

        repactuacaoQuery.OrderBy(repactuacaoQuery.IdRepactuacao.Descending);

        RepactuacaoRendaFixaColl.Load(repactuacaoQuery);

        e.Collection = RepactuacaoRendaFixaColl;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        RepactuacaoRendaFixa repactuacaoRendaFixa = new RepactuacaoRendaFixa();
        int idRepactuacaoRendaFixa = (int)e.Keys[0];

        if (repactuacaoRendaFixa.LoadByPrimaryKey(idRepactuacaoRendaFixa))
        {
            hiddenIdTituloNovo = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloNovo") as ASPxTextBox;
            hiddenIdTituloOrig = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloOrig") as ASPxTextBox;
            textDtEvento = gridCadastro.FindEditFormTemplateControl("textDtEvento") as ASPxDateEdit;

            repactuacaoRendaFixa.IdTituloNovo = Convert.ToInt32(hiddenIdTituloNovo.Text);
            repactuacaoRendaFixa.IdTituloOriginal = Convert.ToInt32(hiddenIdTituloOrig.Text);
            repactuacaoRendaFixa.DataEvento = Convert.ToDateTime(textDtEvento.Text);

            repactuacaoRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de RepactuacaoRendaFixa - Operacao: Update RepactuacaoRendaFixa: Titulo Orig - " + repactuacaoRendaFixa.IdTituloOriginal + "/" + "Titulo Novo - " + repactuacaoRendaFixa.IdTituloNovo + "/" + repactuacaoRendaFixa.DataEvento.ToString() + UtilitarioWeb.ToString(repactuacaoRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdRepactuacao");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idRepactuacao = Convert.ToInt32(keyValuesId[0]);

                RepactuacaoRendaFixa repactuacaoRendaFixa = new RepactuacaoRendaFixa();
                if (repactuacaoRendaFixa.LoadByPrimaryKey(idRepactuacao))
                {
                    RepactuacaoRendaFixa repactuacaoRendaFixaClone = (RepactuacaoRendaFixa)Utilitario.Clone(repactuacaoRendaFixa);
                    //
                    repactuacaoRendaFixa.MarkAsDeleted();
                    repactuacaoRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de RepactuacaoRendaFixa - Operacao: Delete RepactuacaoRendaFixa: Titulo Orig - " + repactuacaoRendaFixaClone.IdTituloOriginal + "/" + "Titulo Novo - " + repactuacaoRendaFixaClone.IdTituloNovo + "/" + repactuacaoRendaFixaClone.DataEvento.ToString() + UtilitarioWeb.ToString(repactuacaoRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        hiddenIdTituloNovo = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloNovo") as ASPxTextBox;
        hiddenIdTituloOrig = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloOrig") as ASPxTextBox;
        textDtEvento = gridCadastro.FindEditFormTemplateControl("textDtEvento") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenIdTituloNovo);
        controles.Add(hiddenIdTituloOrig);
        controles.Add(textDtEvento);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
        
        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing)
        {
            RepactuacaoRendaFixaCollection repactuacaoRendaFixaColl = new RepactuacaoRendaFixaCollection();
            int idTituloNovo = Convert.ToInt32(hiddenIdTituloNovo.Text);
            int idTituloOrig = Convert.ToInt32(hiddenIdTituloOrig.Text);
            DateTime DataEvento = Convert.ToDateTime(textDtEvento.Text);

            repactuacaoRendaFixaColl.Query.Where(repactuacaoRendaFixaColl.Query.IdTituloNovo.Equal(idTituloNovo) &
                                                 repactuacaoRendaFixaColl.Query.IdTituloOriginal.Equal(idTituloOrig) &
                                                 repactuacaoRendaFixaColl.Query.DataEvento.Equal(DataEvento));

            if (repactuacaoRendaFixaColl.Query.Load())
            {
                e.Result = "Registro já existente.!";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        RepactuacaoRendaFixa repactuacaoRendaFixa = new RepactuacaoRendaFixa();

        hiddenIdTituloNovo = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloNovo") as ASPxTextBox;
        hiddenIdTituloOrig = gridCadastro.FindEditFormTemplateControl("hiddenIdTituloOrig") as ASPxTextBox;
        textDtEvento = gridCadastro.FindEditFormTemplateControl("textDtEvento") as ASPxDateEdit;

        repactuacaoRendaFixa.IdTituloNovo = Convert.ToInt32(hiddenIdTituloNovo.Text);
        repactuacaoRendaFixa.IdTituloOriginal = Convert.ToInt32(hiddenIdTituloOrig.Text);
        repactuacaoRendaFixa.DataEvento = Convert.ToDateTime(textDtEvento.Text);

        repactuacaoRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de RepactuacaoRendaFixa - Operacao: Insert RepactuacaoRendaFixa: Titulo Orig - " + hiddenIdTituloOrig.Text + "/" + "Titulo Novo - " + hiddenIdTituloNovo.Text + "/" + repactuacaoRendaFixa.DataEvento.ToString() + UtilitarioWeb.ToString(repactuacaoRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }
}