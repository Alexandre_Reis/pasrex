﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.BibliotecaFincs.RendaFixa;
using Financial.RendaFixa.Controller;

public partial class CadastrosBasicos_AgendaRendaFixa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupTituloRF = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);

        if (!IsPostBack && !IsCallback)
            dropPreencher.SelectedIndex = 0; //Vencimento

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport, new List<string>(new string[] { AgendaRendaFixaMetadata.ColumnNames.TipoEvento }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSAgendaRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxDateEdit textDataEventoInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataEventoFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxGridLookup textDropTituloRendaFixa = popupFiltro.FindControl("dropTituloRendaFixa") as ASPxGridLookup;
        //
        ASPxTextBox idTituloFiltro = popupFiltro.FindControl("hiddenIdTituloFiltro") as ASPxTextBox;       
        //
        if (!Page.IsPostBack) { // zera na primeira vez
            textDataEventoInicio.Text = "";
            textDataEventoFim.Text = "";
        }

        //
        AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        agendaRendaFixaQuery.Select(agendaRendaFixaQuery, tituloRendaFixaQuery);
        agendaRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(agendaRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);

        if (textDataEventoInicio.Text != "") {
            agendaRendaFixaQuery.Where(agendaRendaFixaQuery.DataEvento >= textDataEventoInicio.Text);
        }

        if (textDataEventoFim.Text != "") {
            agendaRendaFixaQuery.Where(agendaRendaFixaQuery.DataEvento <= textDataEventoFim.Text);
        }

        if (!String.IsNullOrEmpty(textDropTituloRendaFixa.Text)) {
            agendaRendaFixaQuery.Where(agendaRendaFixaQuery.IdTitulo == Convert.ToInt32(idTituloFiltro.Text.Trim()));
        }

        agendaRendaFixaQuery.OrderBy(agendaRendaFixaQuery.IdTitulo.Ascending, 
                                     agendaRendaFixaQuery.DataEvento.Descending, 
                                     agendaRendaFixaQuery.IdAgenda.Descending);

        AgendaRendaFixaCollection coll = new AgendaRendaFixaCollection();
        coll.Load(agendaRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Query.OrderBy(coll.Query.DataVencimento.Ascending);

        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSTituloRendaFixaFiltro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Query.Select(coll.Query.IdTitulo, 
                          coll.Query.Descricao, 
                          coll.Query.DescricaoCompleta.Trim(), 
                          coll.Query.DataEmissao, 
                          coll.Query.DataVencimento);
        //
        coll.Query.OrderBy(coll.Query.IdTitulo.Ascending, coll.Query.DataVencimento.Ascending);
        //
        coll.Query.Load();
        //
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (e.Parameter.Equals("btnDelete"))
            return;
        
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataAgenda = gridCadastro.FindEditFormTemplateControl("textDataAgenda") as ASPxDateEdit;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditTituloRF);
        controles.Add(dropTipoEvento);
        controles.Add(textDataAgenda);
        controles.Add(textDataEvento);
        controles.Add(textDataPagamento);

        if (textTaxa.Text == "" && textValor.Text == "")
        {
            e.Result = "Taxa ou Valor do fluxo deve ser preenchido!";
            return;
        }

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (DateTime.Compare(Convert.ToDateTime(textDataAgenda.Text), Convert.ToDateTime(textDataEvento.Text)) > 0)
        {
            e.Result = "Data da agenda não pode ser maior que a data do evento.";
            return;
        }

        if (DateTime.Compare(Convert.ToDateTime(textDataEvento.Text), Convert.ToDateTime(textDataPagamento.Text)) > 0)
        {
            e.Result = "Data do evento não pode ser maior que a data do pagamento.";
            return;
        }

        AgendaRendaFixaCollection agendaRendaFixaColl = new AgendaRendaFixaCollection();
        agendaRendaFixaColl.Query.Where(agendaRendaFixaColl.Query.DataEvento.Equal(Convert.ToDateTime(textDataEvento.Text))
                                        & agendaRendaFixaColl.Query.TipoEvento.Equal(Convert.ToInt32(dropTipoEvento.SelectedItem.Value))
                                        & agendaRendaFixaColl.Query.IdTitulo.Equal(Convert.ToInt32(hiddenIdTitulo.Text)));

        if (!gridCadastro.IsNewRowEditing)        
        {            
            int editingRowVisibleIndex = gridCadastro.EditingRowVisibleIndex;
            int idAgenda = Convert.ToInt32(gridCadastro.GetRowValues(editingRowVisibleIndex, "IdAgenda"));

            agendaRendaFixaColl.Query.Where(agendaRendaFixaColl.Query.IdAgenda.NotEqual(idAgenda));
        }

        if (agendaRendaFixaColl.Query.Load())
        {
            e.Result = "Já existe esse tipo de evento para a data " + Convert.ToDateTime(textDataEvento.Text).ToShortDateString();
            return;
        }

        
    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackFluxos_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (textPeriodicidade.Text == "" ||
            textIdTitulo.Text == "" ||
            (textValorGerar.Text == "" && textTaxaGerar.Text == ""))
        {
            e.Result = "Deve ser preenchido ou valor ou taxa, os demais campos devem ser todos preenchidos.";
            return;
        }

        int idTitulo = Convert.ToInt32(textIdTitulo.Text);
        
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();        
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(tituloRendaFixa.Query.DataEmissao);
        campos.Add(tituloRendaFixa.Query.DataVencimento);
        campos.Add(tituloRendaFixa.Query.PUNominal);
        if (!tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo))
        {
            e.Result = "Título não existente.";
            return;
        }

        AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                              agendaRendaFixaCollection.Query.TipoEvento.Equal(Convert.ToByte(dropTipoEventoGerar.SelectedItem.Value)));
        agendaRendaFixaCollection.Query.Load();
        agendaRendaFixaCollection.MarkAllAsDeleted();
        agendaRendaFixaCollection.Save();

        DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
        DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;
        decimal puNominal = tituloRendaFixa.PUNominal.Value;

        DateTime dataAux = dataVencimento;

        int periodicidade = Convert.ToInt32(textPeriodicidade.Text);
        
        decimal valor = 0;
        if (textValorGerar.Text != "")
        {
            valor = Convert.ToDecimal(textValorGerar.Text);
        }

        decimal taxa = 0;
        if (textTaxaGerar.Text != "")
        {
            taxa = Convert.ToDecimal(textTaxaGerar.Text);
        }

        while (dataAux > dataEmissao)
        {
            DateTime dataPagamento = dataAux;
            if (!Calendario.IsDiaUtil(dataAux))
            {
                dataPagamento = Calendario.AdicionaDiaUtil(dataAux, 1);
            }

            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            agendaRendaFixa.DataAgenda = dataAux;
            agendaRendaFixa.DataEvento = dataAux;
            agendaRendaFixa.DataPagamento = dataPagamento;
            agendaRendaFixa.IdTitulo = idTitulo;
            agendaRendaFixa.Taxa = taxa;
            agendaRendaFixa.TipoEvento = Convert.ToByte(dropTipoEventoGerar.SelectedItem.Value);
            agendaRendaFixa.Valor = valor;
            agendaRendaFixa.Save();

            dataAux = dataAux.AddMonths(periodicidade * -1);
        }

        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo() 
    {
        List<int> lstEventoConversao = new List<int>();
        lstEventoConversao.Add((byte)TipoEventoTitulo.Amortizacao);
        lstEventoConversao.Add((byte)TipoEventoTitulo.AmortizacaoCorrigida);
        lstEventoConversao.Add((byte)TipoEventoTitulo.Juros);
        lstEventoConversao.Add((byte)TipoEventoTitulo.JurosCorrecao);

        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataAgenda = gridCadastro.FindEditFormTemplateControl("textDataAgenda") as ASPxDateEdit;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
        //
        agendaRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        agendaRendaFixa.TipoEvento = Convert.ToByte(dropTipoEvento.SelectedItem.Value);
        agendaRendaFixa.DataAgenda = Convert.ToDateTime(textDataAgenda.Text);
        agendaRendaFixa.DataEvento = Convert.ToDateTime(textDataEvento.Text);
        agendaRendaFixa.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);

        agendaRendaFixa.Taxa = 0;
        if (!string.IsNullOrEmpty(textTaxa.Text))            
            agendaRendaFixa.Taxa = Convert.ToDecimal(textTaxa.Text);        
               
        agendaRendaFixa.Valor = 0;
        if (!string.IsNullOrEmpty(textValor.Text))        
            agendaRendaFixa.Valor = Convert.ToDecimal(textValor.Text);

        if (lstEventoConversao.Contains(agendaRendaFixa.TipoEvento.Value))
        {
            if (agendaRendaFixa.Taxa == 0 && agendaRendaFixa.Valor != 0)
                agendaRendaFixa.Taxa = this.ConverteValorTaxa(agendaRendaFixa.Valor.Value, null);

            if (agendaRendaFixa.Taxa != 0 && agendaRendaFixa.Valor == 0)
                agendaRendaFixa.Valor = this.ConverteValorTaxa(null, agendaRendaFixa.Taxa.Value);
        }

        agendaRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AgendaRendaFixa - Operacao: Insert AgendaRendaFixa: " + agendaRendaFixa.IdAgenda + UtilitarioWeb.ToString(agendaRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTitulo = Convert.ToInt32(e.Parameter);
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.DescricaoCompleta, tituloRendaFixaCollection.Query.DataEmissao, tituloRendaFixaCollection.Query.DataVencimento);
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdTitulo == idTitulo);

            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.HasData) {
                string descricaoCompleta = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta).ToString();
                string DataEmissao = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao).ToString();
                string DataVencimento = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento).ToString();
                //Monta os dados para serem utilizados como um array no javascript
                texto = descricaoCompleta + "|" + DataEmissao + "|" + DataVencimento;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        List<int> lstEventoConversao = new List<int>();
        lstEventoConversao.Add((byte)TipoEventoTitulo.Amortizacao);
        lstEventoConversao.Add((byte)TipoEventoTitulo.AmortizacaoCorrigida);
        lstEventoConversao.Add((byte)TipoEventoTitulo.Juros);
        lstEventoConversao.Add((byte)TipoEventoTitulo.JurosCorrecao);

        int idAgenda = (int)e.Keys[0];
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataAgenda = gridCadastro.FindEditFormTemplateControl("textDataAgenda") as ASPxDateEdit;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        
        AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
        if (agendaRendaFixa.LoadByPrimaryKey(idAgenda))
        {
            agendaRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
            agendaRendaFixa.DataAgenda = Convert.ToDateTime(textDataAgenda.Text);
            agendaRendaFixa.DataEvento = Convert.ToDateTime(textDataEvento.Text);
            agendaRendaFixa.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
            agendaRendaFixa.TipoEvento = Convert.ToByte(dropTipoEvento.SelectedItem.Value);

            agendaRendaFixa.Taxa = 0;
            if (!string.IsNullOrEmpty(textTaxa.Text))
            {
                agendaRendaFixa.Taxa = Convert.ToDecimal(textTaxa.Text);                
            }

            agendaRendaFixa.Valor = 0;
            if (!string.IsNullOrEmpty(textValor.Text))
            {
                agendaRendaFixa.Valor = Convert.ToDecimal(textValor.Text);
            }

            if (lstEventoConversao.Contains(agendaRendaFixa.TipoEvento.Value))
            {
                if (agendaRendaFixa.Taxa == 0 && agendaRendaFixa.Valor != 0)
                    agendaRendaFixa.Taxa = this.ConverteValorTaxa(agendaRendaFixa.Valor.Value, null);

                if (agendaRendaFixa.Taxa != 0 && agendaRendaFixa.Valor == 0)
                    agendaRendaFixa.Valor = this.ConverteValorTaxa(null, agendaRendaFixa.Taxa.Value);
            }

            agendaRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AgendaRendaFixa - Operacao: Update AgendaRendaFixa: " + idAgenda + UtilitarioWeb.ToString(agendaRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdAgenda");
            for (int i = 0; i < keyValuesId.Count; i++) 
            {
                int idAgenda = Convert.ToInt32(keyValuesId[i]);

                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                if (agendaRendaFixa.LoadByPrimaryKey(idAgenda))
                {
                    AgendaRendaFixa agendaRendaFixaClone = (AgendaRendaFixa)Utilitario.Clone(agendaRendaFixa);

                    agendaRendaFixa.MarkAsDeleted();
                    agendaRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AgendaRendaFixa - Operacao: Delete AgendaRendaFixa: " + idAgenda + "; " + UtilitarioWeb.ToString(agendaRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) 
        {            
            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            e.Properties["cpHiddenIdTitulo"] = hiddenIdTitulo.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTituloRF");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (dropTituloRendaFixa != null && dropTituloRendaFixa.Text.Trim() != "") {           
            texto.Append(" Descrição Título: ").Append(dropTituloRendaFixa.Text.Trim());
        }

        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Evento >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCalculaValor_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        if (hiddenIdTitulo != null && !string.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTitulo.Text));

            if (tituloRendaFixa.ParametrosAvancados.Equals("S"))
            {
                ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
                if (!string.IsNullOrEmpty(textTaxa.Text))
                    e.Result = this.ConverteValorTaxa(null, Convert.ToDecimal(textTaxa.Text)).ToString().Replace(",", "."); ;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCalculaTaxa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        if (hiddenIdTitulo != null && !string.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTitulo.Text));

            if (tituloRendaFixa.ParametrosAvancados.Equals("S"))
            {
                ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
                if (!string.IsNullOrEmpty(textValor.Text))
                    e.Result = this.ConverteValorTaxa(Convert.ToDecimal(textValor.Text), null).ToString().Replace(",", "."); ;
            }
        }
    }


    /// <summary>
    /// Insere uma lista de agenda de eventos
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackAgenda_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        //Inicializa variáveis
        //Aplica a data de "Emissão" ou "Vencimento"
        DateTime dataEmissao = new DateTime();
        //Data limite para efetuar os cadastros 
        DateTime dataLimite = new DateTime();
        //Data Limite do pagamento
        DateTime dataVencimento = new DateTime();
        //Valor em dias de quando será efetuado o pagamento do titulo
        TimeSpan periodicidade = new TimeSpan(0, 0, 0, 0);

        //Faz a validação dos campos
        if (dropPeriodicidadeAgenda.Text == string.Empty
            || btnEditTituloRF_Aut.Text == string.Empty
            || dropTipoEventoAgenda.Text == string.Empty
            || (textValorAgenda.Text == "" && textTaxaAgenda.Text == ""))
        {
            e.Result = "Deve ser preenchido ou valor ou taxa, os demais campos devem ser todos preenchidos.";
            return;
        }

        int idTitulo = Convert.ToInt32(hiddenIdTituloAgenda.Text);
        byte tipoEvento = Convert.ToByte(dropTipoEventoAgenda.SelectedItem.Value);

        #region Busca Defasagem
        TituloRendaFixa titulo = new TituloRendaFixa();
        int defasagem = 0;
        if (titulo.LoadByPrimaryKey(idTitulo))
            defasagem = titulo.DefasagemLiquidacao.GetValueOrDefault(0);
        #endregion

        dataEmissao = Convert.ToDateTime(textDataEmissaoAgenda.Text);
        dataVencimento = Convert.ToDateTime(textDataVencimentoAgenda.Text);

        //Atribui os valores as datas a serem calculadas, avaliando o tipo de evento selecionado
        if (dropPreencher.SelectedItem.Text == "Emissão")
        {
            dataLimite = string.IsNullOrEmpty(textDataLimite.Text) ? dataVencimento : Convert.ToDateTime(textDataLimite.Text);
            if (dataLimite < dataEmissao)
            {
                e.Result = "Data Limite não pode ser maior que a Data de Emissão do título.";
                return;
            }

            this.DeletaAgendaAntiga(idTitulo, tipoEvento);

            if (dataVencimento < dataLimite)
                dataLimite = dataVencimento;

            do
            {
                //Soma o periodo selecionado
                dataEmissao = dataEmissao.AddMonths(VerificaPeriodicidade());

                if (dataEmissao > dataVencimento)
                    break;

                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                //Atribui o Id do titulo da agenda
                agendaRendaFixa.IdTitulo = idTitulo;
                //Atribui o indice dos tipos de eventos
                agendaRendaFixa.TipoEvento = tipoEvento;
                //Atribui a data corrida calculada com defasagem
                agendaRendaFixa.DataAgenda = dataEmissao;
                //Verifica se a data deve ser útil ou corrida
                agendaRendaFixa.DataEvento = Calendario.SubtraiDiaUtil(agendaRendaFixa.DataAgenda.Value, defasagem, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                //Atribui o mesmo preenchimento da data de evento
                agendaRendaFixa.DataPagamento = Calendario.IsDiaUtil(dataEmissao) ? agendaRendaFixa.DataAgenda : Calendario.AdicionaDiaUtil(dataEmissao, 1);

                agendaRendaFixa.Taxa = 0;
                if (textTaxaAgenda.Text != "")
                {
                    agendaRendaFixa.Taxa = Convert.ToDecimal(textTaxaAgenda.Text);
                }
                agendaRendaFixa.Valor = 0;
                if (textValorAgenda.Text != "")
                {
                    agendaRendaFixa.Valor = Convert.ToDecimal(textValorAgenda.Text);
                }

                agendaRendaFixa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de AgendaRendaFixa - Operacao: Insert AgendaRendaFixa: " + agendaRendaFixa.IdAgenda + UtilitarioWeb.ToString(agendaRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion

            } while (dataEmissao < dataLimite);

            //Processamento efetuado com sucesso
            e.Result = "Processo executado com sucesso.";
        }
        else if (dropPreencher.SelectedItem.Text == "Vencimento")
        {
            dataLimite = string.IsNullOrEmpty(textDataLimite.Text) ? dataEmissao : Convert.ToDateTime(textDataLimite.Text);
            if (dataLimite > dataVencimento)
            {
                e.Result = "Data Limite não pode ser maior que a Data de Vencimento do título.";
                return;
            }

            this.DeletaAgendaAntiga(idTitulo, tipoEvento);

            if (dataEmissao > dataLimite)
                dataLimite = dataEmissao;

            do
            {
                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                //Atribui o Id do titulo da agenda
                agendaRendaFixa.IdTitulo = idTitulo;
                //Atribui o indice dos tipos de eventos
                agendaRendaFixa.TipoEvento = tipoEvento;
                //Atribui a data corrida calculada
                agendaRendaFixa.DataAgenda = dataVencimento;
                //Verifica se a data deve ser útil ou corrida
                agendaRendaFixa.DataEvento = Calendario.SubtraiDiaUtil(agendaRendaFixa.DataAgenda.Value, defasagem, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                //Atribui o mesmo preenchimento da data de evento
                agendaRendaFixa.DataPagamento = Calendario.IsDiaUtil(dataVencimento) ? agendaRendaFixa.DataAgenda : Calendario.AdicionaDiaUtil(dataVencimento, 1);

                agendaRendaFixa.Taxa = 0;
                if (textTaxaAgenda.Text != "")
                {
                    agendaRendaFixa.Taxa = Convert.ToDecimal(textTaxaAgenda.Text);
                }
                agendaRendaFixa.Valor = 0;
                if (textValorAgenda.Text != "")
                {
                    agendaRendaFixa.Valor = Convert.ToDecimal(textValorAgenda.Text);
                }

                agendaRendaFixa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de AgendaRendaFixa - Operacao: Insert AgendaRendaFixa: " + agendaRendaFixa.IdAgenda + UtilitarioWeb.ToString(agendaRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion

                //Subtrai os dias na data de inicio até chegar a data final
                dataVencimento = dataVencimento.AddMonths(-VerificaPeriodicidade());

            } while (dataVencimento >= dataLimite);

            //Processamento efetuado com sucesso
            e.Result = "Processo executado com sucesso.";

        }
    }

    /// <summary>
    /// Verifica qual valor selecionado: Mensal, Semestral, Anual
    /// </summary>
    private int VerificaPeriodicidade()
    {
        int valorPeriodicidade = 0;
        if (dropPeriodicidadeAgenda.SelectedItem.Text.Equals("Mensal"))
        {
            valorPeriodicidade = 1;
        }
        else if (dropPeriodicidadeAgenda.SelectedItem.Text.Equals("Semestral"))
        {
            valorPeriodicidade = 6;
        }
        else if (dropPeriodicidadeAgenda.SelectedItem.Text.Equals("Anual"))
        {
            valorPeriodicidade = 12;
        }

        return valorPeriodicidade;
    }

    private void DeletaAgendaAntiga(int idTitulo, byte tipoEvento)
    {
        AgendaRendaFixaCollection agendaColl = new AgendaRendaFixaCollection();
        agendaColl.Query.Where(agendaColl.Query.IdTitulo.Equal(idTitulo) &
                               agendaColl.Query.TipoEvento.Equal(tipoEvento));

        if (agendaColl.Query.Load())
        {
            agendaColl.MarkAllAsDeleted();
            agendaColl.Save();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="agendaRendaFixa"></param>
    /// <param name="valor"></param>
    /// <param name="taxa"></param>
    /// <returns></returns>
    private decimal ConverteValorTaxa(decimal? valor, decimal? taxa)
    {
        AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        CalculoRendaFixaFincs calculoRendaFixaFincs = new CalculoRendaFixaFincs();
        Hashtable hsAux = new Hashtable();
        decimal? previa = null;

        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataAgenda = gridCadastro.FindEditFormTemplateControl("textDataAgenda") as ASPxDateEdit;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        if (string.IsNullOrEmpty(hiddenIdTitulo.Text))
            return 0;

        if (dropTipoEvento.SelectedIndex == -1)
            return 0;

        if (string.IsNullOrEmpty(textDataAgenda.Text))
            return 0;

        if (string.IsNullOrEmpty(textDataEvento.Text))
            return 0;

        if (string.IsNullOrEmpty(textDataPagamento.Text))
            return 0;

        agendaRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        agendaRendaFixa.TipoEvento = Convert.ToByte(dropTipoEvento.SelectedItem.Value);
        agendaRendaFixa.DataAgenda = Convert.ToDateTime(textDataAgenda.Text);
        agendaRendaFixa.DataEvento = Convert.ToDateTime(textDataEvento.Text);
        agendaRendaFixa.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
        agendaRendaFixa.Taxa = !taxa.HasValue ? 0 : taxa;
        agendaRendaFixa.Valor = !valor.HasValue ? 0 : valor;

        tituloRendaFixa.LoadByPrimaryKey(agendaRendaFixa.IdTitulo.Value);

        PapelRendaFixa papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
        DateTime dataEvento = agendaRendaFixa.DataEvento.Value;

        if (tituloRendaFixa.IdIndice.HasValue)
        {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);
            if (!previa.HasValue)
            {
                previa = (new CalculoRendaFixa()).CalculaPrevia(dataEvento, indice);
            }
            else
                previa = 0;
        }
        else
        {
            previa = 0;
        }

        if (!ControllerRendaFixa.RetornaListaClassesOffShore().Contains(papelRendaFixa.Classe.Value))
        {
            if (taxa.HasValue)
            {
                return calculoRendaFixaFincs.ConverteValorTaxa(agendaRendaFixa, papelRendaFixa, tituloRendaFixa, (double)previa.Value, (double)agendaRendaFixa.Valor.Value, (double)taxa.Value, null, ref hsAux);
            }
            else if (valor.HasValue)
            {
                return calculoRendaFixaFincs.ConverteValorTaxa(agendaRendaFixa, papelRendaFixa, tituloRendaFixa, (double)previa.Value, (double)agendaRendaFixa.Valor.Value, null, (double)valor.Value, ref hsAux);
            }
        }

        return 0;
    }          
}