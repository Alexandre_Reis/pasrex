﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Investidor;
using Financial.Common;
using DevExpress.Web;
using Financial.RendaFixa.Enums;
using FileHelpers;
using Financial.Interfaces.Import.RendaFixa;
using System.IO;
using Financial.Interfaces.Export.RendaFixa;
using Financial.Common.Enums;
using Financial.CRM;
using Financial.Util;
using System.Transactions;
using Financial.Fundo;
using System.Text;


public partial class CadastrosBasicos_BatimentoRendaFixa : CadastroBasePage
{
    //ASPxGridView gridConsulta;

    private void InitGrid(ASPxGridView grid)
    {

        //this.gridCadastro.ClientInstanceName = "gridCadastro";
        grid.AutoGenerateColumns = false;
        grid.Width = Unit.Percentage(100);
        grid.EnableViewState = true;

        //Troca de Like para Contains nos campos texto do grid
        UtilitarioGrid.GridFilterContains(grid);

        //Pager
        grid.SettingsPager.PageSize = 30;

        //Settings geral
        grid.Settings.ShowFilterRow = true;
        grid.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        grid.Settings.ShowVerticalScrollBar = true;

        foreach (GridViewColumn column in grid.Columns)
        {
            GridViewDataColumn coluna = column as GridViewDataColumn;
            if (coluna != null)
            {
                coluna.Settings.AllowAutoFilterTextInputTimer = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        grid.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;

        //SettingsText
        grid.SettingsText.EmptyDataRow = "0 registros";
        grid.SettingsText.PopupEditFormCaption = " ";
        grid.SettingsLoadingPanel.Text = "Carregando...";

        //Styles
        grid.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        grid.Styles.AlternatingRow.Enabled = DevExpress.Utils.DefaultBoolean.True;
        grid.Styles.Cell.Wrap = DevExpress.Utils.DefaultBoolean.False;
        grid.Styles.CommandColumn.Cursor = "hand";
    }

    new protected void Page_Load(object sender, EventArgs e)
    {


        ASPxGridView gridCadastro = this.FindControl("tabBatimento").FindControl("gridCadastro") as ASPxGridView;
        base.Page_Load(sender, e, gridCadastro);

        this.gridCadastro.Settings.ShowFilterRow = true;
        UtilitarioGrid.GridFilterContains(gridCadastro);

        this.gridCadastro1.Settings.ShowFilterRow = true;
        UtilitarioGrid.GridFilterContains(gridCadastro1);

        InitGrid(gridIPOCorretora);
        gridIPOCorretora.Settings.VerticalScrollableHeight = 100;

        InitGrid(gridIPOClientes);
        gridIPOClientes.Settings.VerticalScrollableHeight = 300;

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        ClienteQuery clienteEspelhoQuery = new ClienteQuery("CliEsp");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AG");
        OperacaoRendaFixaQuery operacaoEspelhoQuery = new OperacaoRendaFixaQuery("ESP");
        PosicaoRendaFixaAberturaQuery posicaoRendaFixaAberturaQuery = new PosicaoRendaFixaAberturaQuery("pos");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");




        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta, tituloRendaFixaQuery.DataEmissao,
            clienteEspelhoQuery.Apelido.As("ApelidoClienteContraparte"), clienteEspelhoQuery.IdCliente.As("IdClienteContraparte"), agenteMercadoQuery.Apelido.As("AgenteMercadoContraparte"),
            posicaoRendaFixaAberturaQuery.IdPosicao.As("IdPosicao"), posicaoRendaFixaAberturaQuery.DataOperacao.As("DataOperacaoPosicao"), posicaoRendaFixaAberturaQuery.Quantidade.As("QuantidadePosicao"), posicaoRendaFixaAberturaQuery.PUOperacao.As("PUOperacaoPosicao"));
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(operacaoEspelhoQuery).On(operacaoEspelhoQuery.IdOperacao == operacaoRendaFixaQuery.IdOperacaoEspelho);
        operacaoRendaFixaQuery.LeftJoin(clienteEspelhoQuery).On(operacaoRendaFixaQuery.IdCarteiraContraparte == clienteEspelhoQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(operacaoRendaFixaQuery.IdAgenteContraParte == agenteMercadoQuery.IdAgente);
        operacaoRendaFixaQuery.LeftJoin(posicaoRendaFixaAberturaQuery).On(operacaoRendaFixaQuery.IdPosicaoResgatada == posicaoRendaFixaAberturaQuery.IdPosicao && operacaoRendaFixaQuery.DataRegistro == posicaoRendaFixaAberturaQuery.DataHistorico);
        operacaoRendaFixaQuery.LeftJoin(carteiraQuery).On(operacaoRendaFixaQuery.IdCliente == carteiraQuery.IdCarteira);
        //
        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario &
            operacaoRendaFixaQuery.TipoOperacao.NotIn((byte)TipoOperacaoTitulo.Deposito, (byte)TipoOperacaoTitulo.Retirada) &
            operacaoRendaFixaQuery.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip) &
            operacaoRendaFixaQuery.IsIPO.NotEqual("S") &
            (operacaoRendaFixaQuery.StatusExportacao.IsNull() |
            operacaoRendaFixaQuery.StatusExportacao.NotIn(
                (int)StatusExportacaoRendaFixa.Exportado) &
                           operacaoRendaFixaQuery.StatusExportacao.NotIn(
                (int)StatusExportacaoRendaFixa.ProcessadoSucesso)
                )
             & (operacaoRendaFixaQuery.IdAgenteContraParte.IsNotNull() | carteiraQuery.Corretora == 'N'));

        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);


        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOperacaoRendaFixaAcompanhamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        ClienteQuery clienteEspelhoQuery = new ClienteQuery("CliEsp");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AG");
        OperacaoRendaFixaQuery operacaoEspelhoQuery = new OperacaoRendaFixaQuery("ESP");
        PosicaoRendaFixaAberturaQuery posicaoRendaFixaAberturaQuery = new PosicaoRendaFixaAberturaQuery("pos");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");


        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta, tituloRendaFixaQuery.DataEmissao,
            clienteEspelhoQuery.Apelido.As("ApelidoClienteContraparte"), agenteMercadoQuery.Apelido.As("AgenteMercadoContraparte"));
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.LeftJoin(operacaoEspelhoQuery).On(operacaoEspelhoQuery.IdOperacao == operacaoRendaFixaQuery.IdOperacaoEspelho);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(clienteEspelhoQuery).On(operacaoRendaFixaQuery.IdCarteiraContraparte == clienteEspelhoQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(operacaoRendaFixaQuery.IdAgenteContraParte == agenteMercadoQuery.IdAgente);
        operacaoRendaFixaQuery.LeftJoin(posicaoRendaFixaAberturaQuery).On(operacaoRendaFixaQuery.IdPosicaoResgatada == posicaoRendaFixaAberturaQuery.IdPosicao && operacaoRendaFixaQuery.DataRegistro == posicaoRendaFixaAberturaQuery.DataHistorico);
        operacaoRendaFixaQuery.LeftJoin(carteiraQuery).On(operacaoRendaFixaQuery.IdCliente == carteiraQuery.IdCarteira);
        //
        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario & operacaoRendaFixaQuery.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip) &
            operacaoRendaFixaQuery.IsIPO.NotEqual("S") &  operacaoRendaFixaQuery.StatusExportacao.IsNotNull() & 
            (operacaoRendaFixaQuery.IdAgenteContraParte.IsNotNull() | carteiraQuery.Corretora == 'N'));

        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOperacaoRendaFixaIPOCorretora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
        AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorCodigoBovespa(codigoBovespa);

        string cnpjAgenteMercado = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj);

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("E");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta, tituloRendaFixaQuery.DataEmissao);
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(carteiraQuery).On(operacaoRendaFixaQuery.IdCliente == carteiraQuery.IdCarteira);

        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario &
            operacaoRendaFixaQuery.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip) &
            pessoaQuery.Cpfcnpj.Equal(cnpjAgenteMercado) &
            operacaoRendaFixaQuery.IsIPO.Equal("S") &
                  (operacaoRendaFixaQuery.IdAgenteContraParte.IsNotNull() | carteiraQuery.Corretora == 'N'));

        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);
        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSOperacaoRendaFixaIPOClientes_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
        AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorCodigoBovespa(codigoBovespa);

        string cnpjAgenteMercado = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj);

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("E");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta, tituloRendaFixaQuery.DataEmissao);
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);

        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario,
            operacaoRendaFixaQuery.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip),
            pessoaQuery.Cpfcnpj.NotEqual(cnpjAgenteMercado),
            operacaoRendaFixaQuery.IsIPO.Equal("S"));

        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);
        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        e.Collection = new TituloRendaFixaCollection();
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.Select(coll.Query.IdIndice, coll.Query.Descricao);
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoCorretora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        using (TransactionScope transaction = new TransactionScope())
        {
            int idOperacao = (int)e.Keys[0];
            ASPxComboBox dropStatusExportacao = gridCadastro.FindEditFormTemplateControl("dropStatusExportacao") as ASPxComboBox;
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            if (operacaoRendaFixa.LoadByPrimaryKey(idOperacao))
            {
                operacaoRendaFixa.Observacao = textObservacao.Text;
                operacaoRendaFixa.DataModificacao = DateTime.Now;
                if (dropStatusExportacao.SelectedItem != null)
                    operacaoRendaFixa.StatusBatimento = Convert.ToInt32(dropStatusExportacao.SelectedItem.Value.ToString());

                OperacaoRendaFixa operacaoEspelho = new OperacaoRendaFixa();
                if (operacaoRendaFixa.IdOperacaoEspelho.HasValue && operacaoEspelho.LoadByPrimaryKey(operacaoRendaFixa.IdOperacaoEspelho.Value))
                {
                    operacaoEspelho.Observacao = operacaoRendaFixa.Observacao;
                    operacaoEspelho.DataModificacao = operacaoRendaFixa.DataModificacao;
                    operacaoEspelho.StatusBatimento = operacaoRendaFixa.StatusBatimento;
                    operacaoEspelho.Save();
                }

                operacaoRendaFixa.Save();
                
            }




            gridCadastro.CancelEdit();
            gridCadastro.DataBind();
            e.Cancel = true;

            transaction.Complete();
        }
    }

    protected void gridCadastro1_CustomUnboundColumnData(object sender,
    ASPxGridViewColumnDataEventArgs e)
    {



       
        if (e.Column.FieldName == "textoPosicao" && !(e.GetListSourceFieldValue("IdPosicao") is System.DBNull))
        {
            e.Value = "Id: " + e.GetListSourceFieldValue("IdPosicao").ToString() + "  -  Oper.: " + e.GetListSourceFieldValue("DataOperacaoPosicao").ToString() +
                                "  -  Qtde: " + Math.Round(decimal.Parse(e.GetListSourceFieldValue("QuantidadePosicao").ToString()), 0).ToString() + "  -  PU:" + Math.Round(decimal.Parse(e.GetListSourceFieldValue("PUOperacaoPosicao").ToString()), 8).ToString();
        }

        if (e.Column.FieldName == "TipoContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString()))
                e.Value = "1";
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho).ToString()) ||
                !string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdCliente).ToString()))
                e.Value = "2";
        }

        if (e.Column.FieldName == "NomeContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString()))
                e.Value = e.GetListSourceFieldValue("AgenteMercadoContraparte");
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho).ToString()) ||
                !string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte).ToString()))
                e.Value = e.GetListSourceFieldValue("ApelidoClienteContraparte");
        }




        if (e.Column.FieldName == "StatusExportacaoDesc")
        {
            int statusExportacao;
            if (!Int32.TryParse(Convert.ToString(e.GetListSourceFieldValue("StatusExportacao")), out statusExportacao))
            {
                statusExportacao = 0;
            }



            string statusExportacaoDesc = "";
            if (statusExportacao == 0)
            {
                statusExportacaoDesc = "Exportação Pendente";
            }
            else if (statusExportacao == 1)
            {
                statusExportacaoDesc = "Liberado para Exportação";
            }
            else if (statusExportacao == 5)
            {
                statusExportacaoDesc = "Exportado";
            }
            else if (statusExportacao == 100)
            {
                statusExportacaoDesc = "Processado c/ Sucesso";
            }
            else if (statusExportacao == 110)
            {
                statusExportacaoDesc = "Falta Liquidação";
            }
            else if (statusExportacao == 115)
            {
                statusExportacaoDesc = "Falta Papel";
            }
            else if (statusExportacao == 120)
            {
                statusExportacaoDesc = "Infos. Divergentes";
            }
            else if (statusExportacao == 125)
            {
                statusExportacaoDesc = "Erro no Bloco";
            }
            else if (statusExportacao == 130)
            {
                statusExportacaoDesc = "Erro no Serviço";
            }
            else if (statusExportacao == 135)
            {
                statusExportacaoDesc = "Erro no Domínio";
            }
            else
            {
                statusExportacaoDesc = "Outros Erros";
                string observacao = Convert.ToString(e.GetListSourceFieldValue("Observacao"));
                if (!string.IsNullOrEmpty(observacao))
                {
                    statusExportacaoDesc += "\n" + observacao;
                }
            }

            e.Value = statusExportacaoDesc;
        }
    }

    protected void gridIPOCorretora_CustomUnboundColumnData(object sender,
    ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusExportacaoDesc")
        {
            int statusExportacao;
            if (!Int32.TryParse(Convert.ToString(e.GetListSourceFieldValue("StatusExportacao")), out statusExportacao))
            {
                statusExportacao = 0;
            }

            string statusExportacaoDesc = "";
            if (statusExportacao == 0)
            {
                statusExportacaoDesc = "Não Exportado";
            }
            else if (statusExportacao == 5)
            {
                statusExportacaoDesc = "Exportado";
            }

            e.Value = statusExportacaoDesc;
        }
    }

    protected void gridIPOClientes_CustomUnboundColumnData(object sender,
    ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusExportacaoDesc")
        {
            int statusExportacao;
            if (!Int32.TryParse(Convert.ToString(e.GetListSourceFieldValue("StatusExportacao")), out statusExportacao))
            {
                statusExportacao = 0;
            }

            string statusExportacaoDesc = "";
            if (statusExportacao == 0)
            {
                statusExportacaoDesc = "Não Exportado";
            }
            else if (statusExportacao == 5)
            {
                statusExportacaoDesc = "Exportado";
            }

            e.Value = statusExportacaoDesc;
        }
    }

    protected void uplUpdateFinancial_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        FileHelperEngine engine = new FileHelperEngine(typeof(OperacaoProcessadaCetipViewModel));

        string content = (new StreamReader(e.UploadedFile.FileContent, System.Text.Encoding.Default)).ReadToEnd();

        OperacaoProcessadaCetipViewModel[] operacoesProcessadas = engine.ReadString(content) as OperacaoProcessadaCetipViewModel[];

        engine = new FileHelperEngine(typeof(RegistroOperacaoCetip));
        foreach (OperacaoProcessadaCetipViewModel operacaoProcessada in operacoesProcessadas)
        {
            RegistroOperacaoCetip[] operacoesOriginais = engine.ReadString(operacaoProcessada.TextoLinhaOriginal) as RegistroOperacaoCetip[];
            RegistroOperacaoCetip operacaoOriginal = operacoesOriginais[0];

            int idOperacao = operacaoOriginal.MeuNumero;

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            if (operacaoRendaFixa.LoadByPrimaryKey(idOperacao))
            {
                string descricaoMensagem = operacaoProcessada.DescricaoMensagem.ToUpper();
                if (descricaoMensagem == "EXECUCAO OK")
                {
                    operacaoRendaFixa.StatusExportacao = (int)StatusExportacaoRendaFixa.ProcessadoSucesso;
                }
                else if (descricaoMensagem == "ERRO NO BLOCO")
                {
                    operacaoRendaFixa.StatusExportacao = (int)StatusExportacaoRendaFixa.ProcessadoErroBloco;
                }
                else if (descricaoMensagem == "ERRO NO SERVIÇO")
                {
                    operacaoRendaFixa.StatusExportacao = (int)StatusExportacaoRendaFixa.ProcessadoErroServico;
                }
                else if (descricaoMensagem.Contains("DOMÍNIO INVÁLIDO"))
                {
                    operacaoRendaFixa.StatusExportacao = (int)StatusExportacaoRendaFixa.ProcessadoErroDominio;
                }
                else
                {
                    operacaoRendaFixa.StatusExportacao = (int)StatusExportacaoRendaFixa.ProcessadoErroOutros;
                    operacaoRendaFixa.Observacao = operacaoProcessada.DescricaoMensagem;
                }


                operacaoRendaFixa.Save();
            }
        }

        e.CallbackData = "Processamento concluído com sucesso";
    }

    protected void callbackUpdate_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    protected void callbackLiberarParaExportacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {

        List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues("IdOperacao", "DataModificacao");
        Dictionary<int,DateTime> idOperacoes = new Dictionary<int,DateTime>();
        
        keyValuesId.ForEach( delegate(object v){
            idOperacoes.Add((int)((object[])(v))[0], (DateTime)((object[])(v))[1]); 
        });

        int operacoesNaoModificadas = 0;

        foreach (int idOperacao in idOperacoes.Keys)
        {
            OperacaoRendaFixa operacao = new OperacaoRendaFixa();

            operacao.LoadByPrimaryKey(idOperacao);

            //impedir alteração de status de quem foi modificado
            if (operacao.DataModificacao.HasValue && RemoveMilliseconds(operacao.DataModificacao.Value) != RemoveMilliseconds(idOperacoes[idOperacao]))
            {
                operacoesNaoModificadas++;
                continue;
            }

            //Impedir alteracao de status de quem ja foi processado com sucesso/exportado
            if (operacao.StatusExportacao.HasValue && (operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.Exportado ||
                operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoSucesso ||
                operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.LiberadoExportacao ||
                !operacao.IdCliente.HasValue))
            {
                continue;
            }

            if (operacao.StatusExportacao.HasValue &&
                (operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroBloco ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroDominio ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroFaltaLiquidacao ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroFaltaPapel ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroInfoDivergente ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroOutros ||
operacao.StatusExportacao.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroServico))
            {
                OperacaoRendaFixa operacaoOld = operacao;
                operacao = new OperacaoRendaFixa();
                operacao.CodigoContraParte = operacaoOld.CodigoContraParte;
                operacao.DataLiquidacao = operacaoOld.DataLiquidacao;
                operacao.DataOperacao = operacaoOld.DataOperacao;
                operacao.DataRegistro = operacaoOld.DataRegistro;
                operacao.DataVolta = operacaoOld.DataVolta;
                operacao.Emolumento = operacaoOld.Emolumento;
                operacao.Fonte = operacaoOld.Fonte;
                operacao.IdAgenteCorretora = operacaoOld.IdAgenteCorretora;
                operacao.IdCliente = operacaoOld.IdCliente;
                operacao.IdCustodia = operacaoOld.IdCustodia;
                operacao.IdIndiceVolta = operacaoOld.IdIndiceVolta;
                operacao.IdLiquidacao = operacaoOld.IdLiquidacao;
                operacao.IdOperacaoResgatada = operacaoOld.IdOperacaoResgatada;
                operacao.IdOperacaoVinculo = operacaoOld.IdOperacaoVinculo;
                operacao.IdPosicaoResgatada = operacaoOld.IdPosicaoResgatada;
                operacao.IdTitulo = operacaoOld.IdTitulo;
                operacao.NumeroNota = operacaoOld.NumeroNota;
                operacao.Observacao = operacaoOld.Observacao;
                operacao.PUOperacao = operacaoOld.PUOperacao;
                operacao.PUVolta = operacaoOld.PUVolta;
                operacao.Quantidade = operacaoOld.Quantidade;
                operacao.Rendimento = operacaoOld.Rendimento;
                operacao.StatusExportacao = operacaoOld.StatusExportacao;
                operacao.TaxaNegociacao = operacaoOld.TaxaNegociacao;
                operacao.TaxaOperacao = operacaoOld.TaxaOperacao;
                operacao.TaxaVolta = operacaoOld.TaxaVolta;
                operacao.TipoNegociacao = operacaoOld.TipoNegociacao;
                operacao.TipoOperacao = operacaoOld.TipoOperacao;
                operacao.Valor = operacaoOld.Valor;
                operacao.ValorCorretagem = operacaoOld.ValorCorretagem;
                operacao.ValorIOF = operacaoOld.ValorIOF;
                operacao.ValorIR = operacaoOld.ValorIR;
                operacao.ValorISS = operacaoOld.ValorISS;
                operacao.ValorLiquido = operacaoOld.ValorLiquido;
                operacao.ValorVolta = operacaoOld.ValorVolta;
                operacaoOld.MarkAsDeleted();
                operacaoOld.Save();
            }

            operacao.StatusExportacao = (int)StatusExportacaoRendaFixa.LiberadoExportacao;

            operacao.Save();
        }

        if (idOperacoes.Count == 0)
        {
            e.Result = "Por favor, selecione uma ou mais operações";
        }
        else
        {
            e.Result = "Processamento concluído";
        }

        if (operacoesNaoModificadas > 0)
            e.Result = e.Result 
                + Environment.NewLine 
                + operacoesNaoModificadas.ToString() 
                + " operações não foram exportadas pois foram modificadas por outro usuario durante este processo e portanto estão inconsistentes." 
                + Environment.NewLine
                + "Recarregue a tela para exportar estas operações.";

        this.gridCadastro.DataBind();
        this.gridCadastro.Selection.UnselectAll();
        this.gridCadastro.CancelEdit();

    }

    protected void callbackExportarIPOCorretora_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {

        List<object> keyValuesId = this.gridIPOCorretora.GetSelectedFieldValues("IdOperacao");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Por favor, selecione uma operação";
            return;
        }

        if (keyValuesId.Count > 1)
        {
            e.Result = "Por favor, selecione apenas uma operação.";
            return;
        }

        List<int> idOperacoes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        int idOperacao = idOperacoes[0];

        OperacaoRendaFixa operacao = new OperacaoRendaFixa();
        operacao.LoadByPrimaryKey(idOperacao);

        string nomeArquivo = "cetip_ipo_corretora.txt";

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.ToLower().Replace("cadastrosbasicos/rendafixa/batimentorendafixa.aspx",
            "interfaces/exibeexportacao.aspx?Tipo=14&IdOperacao=" + operacao.IdOperacao.Value);
        
        e.Result = "redirect:" + newLocation;

    }

    protected void callbackExportarIPOClientes_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {

        List<object> keyValuesId = this.gridIPOCorretora.GetSelectedFieldValues("IdOperacao");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Por favor, selecione uma operação";
            return;
        }

        if (keyValuesId.Count > 1)
        {
            e.Result = "Por favor, selecione apenas uma operação.";
            return;
        }

        List<int> idOperacoes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        int idOperacao = idOperacoes[0];

        OperacaoRendaFixa operacao = new OperacaoRendaFixa();
        operacao.LoadByPrimaryKey(idOperacao);

        if (!operacao.IdOperacaoExterna.HasValue)
        {
            e.Result = "Antes de exportar as operações dos clientes é necessário informar o Id Cetip.";
            return;
        }

        string nomeArquivo = "cetip_ipo_clientes.txt";

        //
        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.ToLower().Replace("cadastrosbasicos/rendafixa/batimentorendafixa.aspx",
            "interfaces/exibeexportacao.aspx?Tipo=15&IdOperacao=" + operacao.IdOperacao.Value);

        e.Result = "redirect:" + newLocation;

    }

    protected void callbackPreencherIdCetip_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> keyValuesId = this.gridIPOCorretora.GetSelectedFieldValues("IdOperacao");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Por favor, selecione uma operação";
            return;
        }

        if (keyValuesId.Count > 1)
        {
            e.Result = "Por favor, selecione apenas uma operação.";
            return;
        }

        List<int> idOperacoes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        int idOperacao = idOperacoes[0];

        OperacaoRendaFixa operacao = new OperacaoRendaFixa();
        operacao.LoadByPrimaryKey(idOperacao);

        if (operacao.StatusExportacao != (int)StatusExportacaoRendaFixa.Exportado)
        {
            e.Result = "O Id Cetip não pode ser preenchido pois a operação ainda não foi exportada.";
            return;
        }

        long idCetip;
        if (Int64.TryParse(e.Parameter, out idCetip))
        {
            operacao.IdOperacaoExterna = idCetip;
            operacao.Save();
            e.Result = "Processamento concluído";
        }
        else
        {
            e.Result = "Id inválido. O Id deve ser numérico";
        }
    }

    protected void callbackDelete_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues("IdOperacao");
        List<int> idOperacoes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        foreach (int idOperacao in idOperacoes)
        {
            OperacaoRendaFixa operacao = new OperacaoRendaFixa();
            operacao.LoadByPrimaryKey(idOperacao);
            operacao.MarkAsDeleted();
            operacao.Save();
        }

        if (idOperacoes.Count == 0)
        {
            e.Result = "Por favor, selecione uma ou mais operações";
        }
        else
        {
            e.Result = "Registro(s) removido(s)";
        }
    }

    protected void callbackOpCetip_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        OperacaoRendaFixaQuery operacaoQuery = new OperacaoRendaFixaQuery("O");
        PapelRendaFixaQuery papelQuery = new PapelRendaFixaQuery("P");
        TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("T");
        PessoaQuery pessoaQuery = new PessoaQuery("Pe");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AM");




        operacaoQuery.Where(
            operacaoQuery.DataOperacao.Equal(DateTime.Now.Date),
            operacaoQuery.IdCustodia.Equal((byte)CustodiaTitulo.Cetip),
            operacaoQuery.StatusExportacao.Equal((int)StatusExportacaoRendaFixa.LiberadoExportacao),
            papelQuery.Classe.In(
                (int)ClasseRendaFixa.CCB, (int)ClasseRendaFixa.CCI, (int)ClasseRendaFixa.CDA, (int)ClasseRendaFixa.CDB, (int)ClasseRendaFixa.CDCA, (int)ClasseRendaFixa.CPR,
                (int)ClasseRendaFixa.CRA, (int)ClasseRendaFixa.CRI, (int)ClasseRendaFixa.Debenture, (int)ClasseRendaFixa.DPGE, (int)ClasseRendaFixa.LC, (int)ClasseRendaFixa.LCA,
                (int)ClasseRendaFixa.LCI, (int)ClasseRendaFixa.LF, (int)ClasseRendaFixa.LH
            )
        );

        operacaoQuery.InnerJoin(tituloQuery).On(tituloQuery.IdTitulo.Equal(operacaoQuery.IdTitulo));
        operacaoQuery.InnerJoin(papelQuery).On(papelQuery.IdPapel.Equal(tituloQuery.IdPapel));
        operacaoQuery.LeftJoin(pessoaQuery).On(pessoaQuery.IdPessoa.Equal(operacaoQuery.IdCliente));
        operacaoQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente.Equal(operacaoQuery.IdAgenteContraParte));
        operacaoQuery.OrderBy(operacaoQuery.TipoOperacao.Ascending);

        operacaoQuery.Select(operacaoQuery, pessoaQuery.IdPessoa.As("IdPessoa"), pessoaQuery.Nome.As("NomePessoa"), pessoaQuery.Cpfcnpj.As("CpfCnpjPessoa"),
            agenteMercadoQuery.IdAgente.As("IdAgenteMercado"), agenteMercadoQuery.Nome.As("NomeAgente"), agenteMercadoQuery.CodigoCetip.As("CodigoCetipAgente"));

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoQuery);

        StringBuilder s = new StringBuilder();


        foreach (OperacaoRendaFixa operacao in coll)
        {
            if (!string.IsNullOrEmpty(operacao.GetColumn("IdPessoa").ToString()) && string.IsNullOrEmpty(operacao.GetColumn("CpfCnpjPessoa").ToString()))
            {
                s.AppendLine("-Pessoa (Id:" + operacao.GetColumn("IdPessoa") + " Nome: " + operacao.GetColumn("NomePessoa") + ") da operação Id:" + operacao.IdOperacao + " esta com o campo CpfCnpj em branco. Favor preencher para prosseguir");
            }
            if (!string.IsNullOrEmpty(operacao.GetColumn("IdAgenteMercado").ToString()) && string.IsNullOrEmpty(operacao.GetColumn("CodigoCetipAgente").ToString()))
            {
                s.AppendLine("-Agente (Id:" + operacao.GetColumn("IdAgenteMercado") + " Nome: " + operacao.GetColumn("IdPessoa") + ") da operação Id:" + operacao.IdOperacao + " esta com o campo CodigoCetip em branco. Favor preencher para prosseguir");
            }
        }

        e.Result = s.ToString();
        return;

    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        TextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as TextBox;
        TextBox hiddenDataModificacao = gridCadastro.FindEditFormTemplateControl("hiddenDataModificacao") as TextBox;


        int? idOperacao = null;
        if (hiddenIdOperacao.Text != "")
        {
            idOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
        }


        if (idOperacao.HasValue && hiddenDataModificacao.Text != "")
        {
            DateTime dataModificacao = Convert.ToDateTime(hiddenDataModificacao.Text);
            OperacaoRendaFixa operacao = new OperacaoRendaFixa();
            if (operacao.LoadByPrimaryKey(idOperacao.Value))
            {
                
                if (RemoveMilliseconds(operacao.DataModificacao.Value) != RemoveMilliseconds(dataModificacao))
                {
                    e.Result = "Atenção esta operação foi modificada por outro usuario. Favor fechar e abrir novamente para poder salvar";
                    return;
                }
            }

        }


        e.Result = "";
    }

    public DateTime RemoveMilliseconds(DateTime date)
    {
        return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);
    }
    

}
