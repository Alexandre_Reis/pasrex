﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaCustosRendaFixa.aspx.cs"
    Inherits="CadastrosBasicos_TabelaCustosRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }      
    
    function OnGetDataTituloRF(data) {
        document.getElementById(gridCadastro.cpHiddenIdTitulo).value = data;
        ASPxCallback2.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }
    </script>

    <script type="text/javascript" language="javascript">

    function OnPapelChanged(papel) {
              btnEditTituloRF.SetValue(null);
              document.getElementById(gridCadastro.cpHiddenIdTitulo).value = null;
              
               if (papel != null && papel!= '') {

                var classe = papel.GetSelectedItem().value.toString();
                txtClasseHidden.SetValue(classe);                                   
        }

        else {
            txtClasseHidden.SetValue(null);
        }        

    }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);                        
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTituloRF.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Tabela de Custos de Renda Fixa"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal2" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal3" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdTabela"
                                        DataSourceID="EsDSTabelaCustosRendaFixa" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdTabela" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="20%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="3"
                                                Width="18%">
                                                <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4"
                                                Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Papel" FieldName="Classe" VisibleIndex="4"
                                                Width="10%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="" Text="" />
                                                        <dxe:ListEditItem Value="1" Text="LFT" />
                                                        <dxe:ListEditItem Value="2" Text="LTN" />
                                                        <dxe:ListEditItem Value="10" Text="PréFixado Desconto" />
                                                        <dxe:ListEditItem Value="12" Text="PréFixado c/ Fluxo" />
                                                        <dxe:ListEditItem Value="15" Text="PréFixado Atualiza" />
                                                        <dxe:ListEditItem Value="20" Text="NTNs" />
                                                        <dxe:ListEditItem Value="30" Text="Pós-Fixado" />
                                                        <dxe:ListEditItem Value="35" Text="Pós-Fixado Truncado" />
                                                        <dxe:ListEditItem Value="45" Text="Pós-Fixado c/ Fluxo Corrigido" />
                                                        <dxe:ListEditItem Value="1000" Text="CCB" />
                                                        <dxe:ListEditItem Value="1005" Text="CCE" />
                                                        <dxe:ListEditItem Value="1010" Text="NCE" />
                                                        <dxe:ListEditItem Value="1015" Text="ExportNote" />
                                                        <dxe:ListEditItem Value="1100" Text="CDB" />
                                                        <dxe:ListEditItem Value="1105" Text="DPGE" />
                                                        <dxe:ListEditItem Value="1110" Text="LF" />
                                                        <dxe:ListEditItem Value="1200" Text="CRI" />
                                                        <dxe:ListEditItem Value="1205" Text="CCI" />
                                                        <dxe:ListEditItem Value="1300" Text="LCI" />
                                                        <dxe:ListEditItem Value="1400" Text="LH" />
                                                        <dxe:ListEditItem Value="1500" Text="Nota Comercial" />
                                                        <dxe:ListEditItem Value="1700" Text="LC" />
                                                        <dxe:ListEditItem Value="2000" Text="Debenture" />
                                                        <dxe:ListEditItem Value="2100" Text="LCA" />
                                                        <dxe:ListEditItem Value="2105" Text="CRA" />
                                                        <dxe:ListEditItem Value="2110" Text="CDCA" />
                                                        <dxe:ListEditItem Value="2115" Text="CPR" />
                                                        <dxe:ListEditItem Value="2120" Text="CDA" />
                                                        <dxe:ListEditItem Value="5000" Text="TDA" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" VisibleIndex="6"
                                                Width="17%" Settings-AutoFilterCondition="Contains">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaAno" Caption="Taxa a.a." VisibleIndex="7"
                                                Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorMensal" Caption="Valor Fixo" VisibleIndex="8"
                                                Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="txtClasseHidden" ClientInstanceName="txtClasseHidden" runat="server"
                                                        Text="" ClientVisible="false" />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%# Eval("IdCliente") %>' MaxLength="10"
                                                                    NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAgente" runat="server" CssClass="labelRequired" AssociatedControlID="dropAgente"
                                                                    Text="Corretora:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropAgente" runat="server" DataSourceID="EsDSAgenteMercado"
                                                                    ValueField="IdAgente" TextField="Nome" CssClass="dropDownList" Text='<%#Eval("IdAgente")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Dt Referência:" /></td>
                                                            <td colspan="2">
                                                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia"
                                                                    Value='<%#Eval("DataReferencia")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPapel" runat="server" CssClass="labelNormal" Text="Papel:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropClasse" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("Classe")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="" Text="" />
                                                                        <dxe:ListEditItem Value="1" Text="LFT" />
                                                                        <dxe:ListEditItem Value="2" Text="LTN" />
                                                                        <dxe:ListEditItem Value="10" Text="PréFixado Desconto" />
                                                                        <dxe:ListEditItem Value="12" Text="PréFixado c/ Fluxo" />
                                                                        <dxe:ListEditItem Value="15" Text="PréFixado Atualiza" />
                                                                        <dxe:ListEditItem Value="20" Text="NTNs" />
                                                                        <dxe:ListEditItem Value="30" Text="Pós-Fixado" />
                                                                        <dxe:ListEditItem Value="35" Text="Pós-Fixado Truncado" />
                                                                        <dxe:ListEditItem Value="45" Text="Pós-Fixado c/ Fluxo Corrigido" />
                                                                        <dxe:ListEditItem Value="1000" Text="CCB" />
                                                                        <dxe:ListEditItem Value="1005" Text="CCE" />
                                                                        <dxe:ListEditItem Value="1010" Text="NCE" />
                                                                        <dxe:ListEditItem Value="1015" Text="ExportNote" />
                                                                        <dxe:ListEditItem Value="1100" Text="CDB" />
                                                                        <dxe:ListEditItem Value="1105" Text="DPGE" />
                                                                        <dxe:ListEditItem Value="1110" Text="LF" />
                                                                        <dxe:ListEditItem Value="1200" Text="CRI" />
                                                                        <dxe:ListEditItem Value="1205" Text="CCI" />
                                                                        <dxe:ListEditItem Value="1300" Text="LCI" />
                                                                        <dxe:ListEditItem Value="1400" Text="LH" />
                                                                        <dxe:ListEditItem Value="1500" Text="Nota Comercial" />
                                                                        <dxe:ListEditItem Value="1700" Text="LC" />
                                                                        <dxe:ListEditItem Value="2000" Text="Debenture" />
                                                                        <dxe:ListEditItem Value="2100" Text="LCA" />
                                                                        <dxe:ListEditItem Value="2105" Text="CRA" />
                                                                        <dxe:ListEditItem Value="2110" Text="CDCA" />
                                                                        <dxe:ListEditItem Value="2115" Text="CPR" />
                                                                        <dxe:ListEditItem Value="2120" Text="CDA" />
                                                                        <dxe:ListEditItem Value="5000" Text="TDA" />
                                                                    </Items>
                                                                    <ClientSideEvents KeyDown="function(s, e) {if (e.htmlEvent.keyCode == 46) {s.SetSelectedIndex(-1); } }" />
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnPapelChanged(s); }"></ClientSideEvents>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                                                Visible="false"></asp:Label>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTitulo" runat="server" CssClass="labelNormal" Text="Título:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:TextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>' />
                                                                <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditTituloRF" Width="380px" Text='<%#Eval("DescricaoCompleta")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTaxaAno" runat="server" CssClass="labelRequired" Text="Taxa a.a.:"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxSpinEdit ID="textTaxaAno" runat="server" ClientInstanceName="textTaxaAno"
                                                                    Text='<%# Eval("TaxaAno") %>' NumberType="Float" MaxLength="10" DecimalPlaces="4"
                                                                    CssClass="textPercCurto">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorMensal" runat="server" CssClass="labelRequired" Text="Valor Mensal:"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxSpinEdit ID="textValorMensal" runat="server" ClientInstanceName="ValorMensal"
                                                                    Text='<%# Eval("ValorMensal") %>' NumberType="Float" MaxLength="6" DecimalPlaces="2"
                                                                    CssClass="textValor">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal12" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSTabelaCustosRendaFixa" runat="server" OnesSelect="EsDSTabelaCustosRendaFixa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixaFiltro" runat="server" OnesSelect="EsDSTituloRendaFixaFiltro_esSelect" />
    </form>
</body>
</html>
