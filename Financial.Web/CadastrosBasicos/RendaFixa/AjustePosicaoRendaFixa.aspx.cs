﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.RendaFixa;

public partial class CadastrosBasicos_AjustePosicaoRendaFixa : Page {
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCliente.Focus();
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (btnEditCodigoCliente.Text != "" && textData.Text != "") 
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) <= 0) {
                PosicaoRendaFixaCollection coll = new PosicaoRendaFixaCollection();

                coll.Query.Where(coll.Query.Quantidade.GreaterThan(0));
                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);

                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();

                coll.Query.Where(coll.Query.Quantidade.GreaterThan(0));
                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.Where(coll.Query.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);
                
                coll.Query.Load();
                
                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoRendaFixaCollection();
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;

        ASPxComboBox dropAgenteCorretora = popupPosicaoRendaFixa.FindControl("dropAgenteCorretora") as ASPxComboBox;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCliente.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void AtualizaPosicao(int selectedIndex)
    {
        ASPxPopupControl popupPosicaoRendaFixa = this.FindControl("popupPosicaoRendaFixa") as ASPxPopupControl;
        ASPxGridView gridPosicaoRendaFixa = popupPosicaoRendaFixa.FindControl("gridPosicaoRendaFixa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoRendaFixa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPUOperacao = popupPosicaoRendaFixa.FindControl("textPUOperacao") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaOperacao = popupPosicaoRendaFixa.FindControl("textTaxaOperacao") as ASPxSpinEdit;
        ASPxComboBox dropAgenteCorretora = popupPosicaoRendaFixa.FindControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropTipoNegociacao = popupPosicaoRendaFixa.FindControl("dropTipoNegociacao") as ASPxComboBox;
        CheckBox chkResetAgente = popupPosicaoRendaFixa.FindControl("chkResetAgente") as CheckBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        DateTime dataDia = cliente.DataDia.Value;
        byte status = cliente.Status.Value;

        int idPosicao = Convert.ToInt32(gridPosicaoRendaFixa.GetRowValues(selectedIndex, "IdPosicao"));
        
        decimal quantidade = 0;
        decimal puOperacao = 0;
        decimal taxaOperacao = 0;
        int? idAgente = null;
        byte tipoNegociacao = 0; 

        bool alteraQuantidade = false;
        if (textQuantidade.Text != "")
        {
            quantidade = Convert.ToDecimal(textQuantidade.Text);
            alteraQuantidade = true;
        }

        bool alteraPUOperacao = false;
        if (textPUOperacao.Text != "")
        {
            puOperacao = Convert.ToDecimal(textPUOperacao.Text);
            alteraPUOperacao = true;
        }

        bool alteraTaxaOperacao = false;
        if (textTaxaOperacao.Text != "")
        {
            taxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
            alteraTaxaOperacao = true;
        }

        bool alteraAgente = false;
        if (dropAgenteCorretora.Text != "" || chkResetAgente.Checked)
        {
            if (!chkResetAgente.Checked)
            {
                idAgente = Convert.ToInt16(dropAgenteCorretora.SelectedItem.Value);
            }
            alteraAgente = true;
        }

        bool alteraTipo = false;
        if (dropTipoNegociacao.Text != "")
        {
            tipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);
            alteraTipo = true;
        }

        DateTime data = Convert.ToDateTime(textData.Text);

        if (data > dataDia)
        {
            data = dataDia;
        }

        if (dataDia > data)
        {
            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            if (posicaoRendaFixaHistorico.LoadByPrimaryKey(idPosicao, data)) 
            {
                if (alteraQuantidade)
                {
                    decimal diferenca = quantidade - posicaoRendaFixaHistorico.Quantidade.Value;
                    posicaoRendaFixaHistorico.Quantidade += diferenca;
                }

                if (alteraPUOperacao)
                {
                    posicaoRendaFixaHistorico.PUOperacao = puOperacao;
                }

                if (alteraTaxaOperacao)
                {
                    posicaoRendaFixaHistorico.TaxaOperacao = taxaOperacao;
                }
                
                if (alteraAgente)
                {
                    if (chkResetAgente.Checked)
                    {
                        posicaoRendaFixaHistorico.IdAgente = null;
                    }
                    else
                    {
                        posicaoRendaFixaHistorico.IdAgente = idAgente;
                    }
                }

                if (alteraTipo)
                {
                    posicaoRendaFixaHistorico.TipoNegociacao = tipoNegociacao;
                }

                posicaoRendaFixaHistorico.Save();
            }
        }
        else 
        {
            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            if (posicaoRendaFixa.LoadByPrimaryKey(idPosicao)) 
            {
                if (alteraQuantidade)
                {
                    decimal diferenca = quantidade - posicaoRendaFixa.Quantidade.Value;
                    posicaoRendaFixa.Quantidade += diferenca;
                }

                if (alteraPUOperacao)
                {
                    posicaoRendaFixa.PUOperacao = puOperacao;
                }

                if (alteraTaxaOperacao)
                {
                    posicaoRendaFixa.TaxaOperacao = taxaOperacao;
                }

                if (alteraAgente)
                {
                    if (chkResetAgente.Checked)
                    {
                        posicaoRendaFixa.IdAgente = null;
                    }
                    else
                    {
                        posicaoRendaFixa.IdAgente = idAgente;
                    }
                }

                if (alteraTipo)
                {
                    posicaoRendaFixa.TipoNegociacao = tipoNegociacao;
                }


                posicaoRendaFixa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Ajuste PosicaoRendaFixa - Operacao: Update Ajuste PosicaoRendaFixa: " + idPosicao + UtilitarioWeb.ToString(posicaoRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            if (status == (byte)StatusCliente.Divulgado) 
            {
                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                if (posicaoRendaFixaHistorico.LoadByPrimaryKey(idPosicao, data)) 
                {
                    if (alteraQuantidade)
                    {
                        decimal diferenca = quantidade - posicaoRendaFixaHistorico.Quantidade.Value;
                        posicaoRendaFixaHistorico.Quantidade += diferenca;
                    }

                    if (alteraPUOperacao)
                    {
                        posicaoRendaFixaHistorico.PUOperacao = puOperacao;
                    }

                    if (alteraTaxaOperacao)
                    {
                        posicaoRendaFixaHistorico.TaxaOperacao = taxaOperacao;
                    }

                    if (alteraAgente)
                    {
                        if (chkResetAgente.Checked)
                        {
                            posicaoRendaFixaHistorico.IdAgente = null;
                        }
                        else
                        {
                            posicaoRendaFixaHistorico.IdAgente = idAgente;
                        }
                    }

                    if (alteraTipo)
                    {
                        posicaoRendaFixaHistorico.TipoNegociacao = tipoNegociacao;
                    }


                    posicaoRendaFixaHistorico.Save();
                }
            }
        }

        PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
        if (posicaoRendaFixaAbertura.LoadByPrimaryKey(idPosicao, data)) 
        {
            if (alteraQuantidade)
            {
                decimal diferenca = quantidade - posicaoRendaFixaAbertura.Quantidade.Value;
                posicaoRendaFixaAbertura.Quantidade += diferenca;
            }

            if (alteraPUOperacao)
            {
                posicaoRendaFixaAbertura.PUOperacao = puOperacao;
            }

            if (alteraTaxaOperacao)
            {
                posicaoRendaFixaAbertura.TaxaOperacao = taxaOperacao;
            }

            if (alteraAgente)
            {
                if (chkResetAgente.Checked)
                {
                    posicaoRendaFixaAbertura.IdAgente = null;
                }
                else
                {
                    posicaoRendaFixaAbertura.IdAgente = idAgente;
                }
            }

            if (alteraTipo)
            {
                posicaoRendaFixaAbertura.TipoNegociacao = tipoNegociacao;
            }


            posicaoRendaFixaAbertura.Save();
        }

        dropAgenteCorretora.SelectedIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoRendaFixa.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            AtualizaPosicao(selectedIndex);
            gridPosicaoRendaFixa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }
}