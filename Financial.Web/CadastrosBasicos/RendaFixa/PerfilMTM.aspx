﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerfilMTM.aspx.cs" Inherits="CadastrosBasicos_PerfilMTM" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';

        function OnGetDataOperacaoRendaFixa(data) {
            hiddenIdOperacao.SetValue(data);
            callBackPopupOperacaoRendaFixa.SendCallback(data);
            popupOperacaoRendaFixa.HideWindow();
            btnEditOperacaoRendaFixa.Focus();
        }
        
        function OnGetDataGrupoPerfilMTM(data) {
            hiddenIdGrupoPerfilMTM.SetValue(data);
            callBackPopupGrupoPerfilMTM.SendCallback(data);
            popupGrupoPerfilMTM.HideWindow();
            btnEditGrupoPerfilMTM.Focus();
        }

        function OnGetDataSerieRendaFixa(data) {
            hiddenIdSerie.SetValue(data);
            callBackPopupSerie.SendCallback(data);
            popupSerieRendaFixa.HideWindow();
            btnEditSerieRendaFixa.Focus();
        }

        function OnGetDataTituloRendaFixa(data) {
            hiddenIdTitulo.SetValue(data);
            callBackPopupTitulo.SendCallback(data);
            popupTituloRendaFixa.HideWindow();
            btnEditTituloRendaFixa.Focus();
        }

        function OnGetDataPapelRendaFixa(data) {
            hiddenIdPapel.SetValue(data);
            callBackPopupPapel.SendCallback(data);
            popupPapelRendaFixa.HideWindow();
            btnEditPapelRendaFixa.Focus();
        }                 
                                      
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {        
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupOperacaoRendaFixa" runat="server" OnCallback="callBackPopupOperacaoRendaFixa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                                             if (e.result != null)
                                                             {
                                                                var resultSplit = e.result.split('|');
                                                                btnEditOperacaoRendaFixa.SetValue(resultSplit[0]); 
                                                                callBackPopupTitulo.SendCallback(resultSplit[1]);
                                                                hiddenIdTitulo.SetValue(resultSplit[1]);
                                                             } 
                                                           }" />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupGrupoPerfilMTM" ClientInstanceName="popupGrupoPerfilMTM"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridGrupoPerfilMTM" runat="server" Width="100%" ClientInstanceName="gridGrupoPerfilMTM"
                            AutoGenerateColumns="False" DataSourceID="EsDSGrupoPerfilMTM" KeyFieldName="IdSerie"
                            OnCustomDataCallback="gridGrupoPerfilMTM_CustomDataCallback" OnCustomCallback="gridGrupoPerfilMTM_CustomCallback"
                            OnHtmlRowCreated="gridGrupoPerfilMTM_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdGrupoPerfilMTM" Caption="Série" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridGrupoPerfilMTM.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataGrupoPerfilMTM);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Grupo Perfil MTM." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridGrupoPerfilMTM.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupOperacaoRendaFixa" ClientInstanceName="popupOperacaoRendaFixa"
            runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentOperacaoRendaFixa" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridOperacaoRendaFixa" runat="server" Width="100%" ClientInstanceName="gridOperacaoRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSOperacaoRendaFixa" KeyFieldName="CompositeKey"
                            OnCustomDataCallback="gridOperacaoRendaFixa_CustomDataCallback" OnCustomCallback="gridOperacaoRendaFixa_CustomCallback"
                            OnHtmlRowCreated="gridOperacaoRendaFixa_HtmlRowCreated" SettingsPager-PageSize="5">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" VisibleIndex="0" ReadOnly="True">
                                    <EditFormSettings Visible="False" Caption="Id.Operação" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" VisibleIndex="1"
                                    Width="35%" Caption="Descrição Título">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ClienteApelido" VisibleIndex="2" Caption="Cliente" Width="35%">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo de Operação"
                                    VisibleIndex="3">
                                    <PropertiesComboBox ValueType="System.String" EncodeHtml="false">
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                            <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                            <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                            <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                            <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                            <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                            <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                            <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                            <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                            <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                            <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                            <dxe:ListEditItem Value="22" Text="<div title='Ingresso em Ativos com Impacto na Quantidade'>Ingresso em Ativos com Impacto na Quantidade</div>" />
                                            <dxe:ListEditItem Value="23" Text="<div title='Ingresso em Ativos com Impacto na Cota'>Ingresso em Ativos com Impacto na Cota</div>" />
                                            <dxe:ListEditItem Value="24" Text="<div title='Retirada em Ativos com Impacto na Quantidade'>Retirada em Ativos com Impacto na Quantidade</div>" />
                                            <dxe:ListEditItem Value="25" Text="<div title='Retirada em Ativos com Impacto na Cota'>Retirada em Ativos com Impacto na Cota</div>" />
                                            <dxe:ListEditItem Value="26" Text="<div title='Exercício Opção'>Exercício Opção</div>" />
                                        </Items>
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data da Operação"
                                    VisibleIndex="4">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="DataLiquidacao" Caption="Data da Liquidação"
                                    VisibleIndex="5">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="6" Visible="false">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" VisibleIndex="7" Visible="false">
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridOperacaoRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataOperacaoRendaFixa);}"
                                Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Operação Renda Fixa." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridOperacaoRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxcb:ASPxCallback ID="callBackPopupSerie" runat="server" OnCallback="callBackPopupSerie_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditSerieRendaFixa.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupGrupoPerfilMTM" runat="server" OnCallback="callBackPopupGrupoPerfilMTM_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditGrupoPerfilMTM.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupSerieRendaFixa" ClientInstanceName="popupSerieRendaFixa"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridSerieRendaFixa" runat="server" Width="100%" ClientInstanceName="gridSerieRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSSerieRendaFixa" KeyFieldName="IdSerie"
                            OnCustomDataCallback="gridSerieRendaFixa_CustomDataCallback" OnCustomCallback="gridSerieRendaFixa_CustomCallback"
                            OnHtmlRowCreated="gridSerieRendaFixa_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdSerie" Caption="Série" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                                <dxwgv:GridViewDataTextColumn FieldName="DescricaoFeeder" Caption="Feeder" VisibleIndex="2" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridSerieRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataSerieRendaFixa);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Série." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridSerieRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxcb:ASPxCallback ID="callBackPopupTitulo" runat="server" OnCallback="callBackPopupTitulo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                                            if (e.result != null) 
                                                            {        
                                                                var resultSplit = e.result.split('|');                                                    
                                                                btnEditTituloRendaFixa.SetValue(resultSplit[0]); 
                                                                callBackPopupPapel.SendCallback(resultSplit[1]);
                                                                hiddenIdPapel.SetValue(resultSplit[1]);
                                                            }
                                                           } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupTituloRendaFixa" ClientInstanceName="popupTituloRendaFixa"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridTituloRendaFixa" runat="server" Width="100%" ClientInstanceName="gridTituloRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSTituloRendaFixa" KeyFieldName="IdTitulo"
                            OnCustomDataCallback="gridTituloRendaFixa_CustomDataCallback" OnCustomCallback="gridTituloRendaFixa_CustomCallback"
                            OnHtmlRowCreated="gridTituloRendaFixa_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Dt.Vencimento"
                                    VisibleIndex="2" Width="5%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                                                                        btnEditOperacaoRendaFixa.SetValue(null); 
                                                                        hiddenIdOperacao.SetValue(null);
                                                                        gridTituloRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTituloRendaFixa);
                                                                      }" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Título." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridTituloRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxcb:ASPxCallback ID="callBackPopupPapel" runat="server" OnCallback="callBackPopupPapel_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditPapelRendaFixa.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupPapelRendaFixa" ClientInstanceName="popupPapelRendaFixa"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridPapelRendaFixa" runat="server" Width="100%" ClientInstanceName="gridPapelRendaFixa"
                            AutoGenerateColumns="False" DataSourceID="EsDSPapelRendaFixa" KeyFieldName="IdPapel"
                            OnCustomDataCallback="gridPapelRendaFixa_CustomDataCallback" OnCustomCallback="gridPapelRendaFixa_CustomCallback"
                            OnHtmlRowCreated="gridPapelRendaFixa_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdPapel" Caption="Papel" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                                                                        btnEditOperacaoRendaFixa.SetValue(null); 
                                                                        hiddenIdOperacao.SetValue(null);
                                                                        btnEditTituloRendaFixa.SetValue(null);
                                                                        hiddenIdTitulo.SetValue(null);
                                                                        gridPapelRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataPapelRendaFixa);
                                                                        
                                                                      }" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Papel." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridPapelRendaFixa.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Perfil MTM"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                        EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                        runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                        CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                        <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                        CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                        <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                </div>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                    </dxpc:ASPxPopupControl>
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                        <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                            CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                            <asp:Literal ID="Literal9" runat="server" Text="Filtro" /><div>
                                            </div>
                                        </asp:LinkButton>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnResetar" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" ValidationGroup="ATK" OnClientClick=" if (confirm('Tem certeza que quer resetar perfil?')==true) gridCadastro.PerformCallback('btnResetar');return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Resetar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdPerfilMTM" DataSourceID="EsDSPerfilMTM"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdPerfilMTM" ReadOnly="True" Width="5%"
                                                VisibleIndex="0" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DtReferencia" Width="5%" Caption="Dt.Referência"
                                                VisibleIndex="1">
                                            </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdGrupoPerfilMTM" ReadOnly="True" Width="5%"
                                                VisibleIndex="2" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoGrupo" Width="10%" Caption="Grupo"
                                                VisibleIndex="3">
                                            </dxwgv:GridViewDataTextColumn>                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoSerie" Width="10%" Caption="Série"
                                                VisibleIndex="4">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" Width="10%" Caption="Papel"
                                                VisibleIndex="5">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Width="5%" VisibleIndex="6">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoTitulo" Width="10%" Caption="Título"
                                                VisibleIndex="7">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimentoTitulo" Caption="Dt.Vencimento"
                                                Width="5%" VisibleIndex="8">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" Width="5%" VisibleIndex="9">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdPapel" Visible="false" VisibleIndex="10">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdSerie" Visible="false" VisibleIndex="11">
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdOperacao" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdOperacao"
                                                        Text='<%#Eval("IdOperacao")%>' />
                                                    <dxe:ASPxTextBox ID="hiddenIdSerie" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdSerie"
                                                        Text='<%#Eval("IdSerie")%>' />
                                                    <dxe:ASPxTextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdTitulo"
                                                        Text='<%#Eval("IdTitulo")%>' />
                                                    <dxe:ASPxTextBox ID="hiddenIdPapel" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdPapel"
                                                        Text='<%#Eval("IdPapel")%>' />
                                                    <dxe:ASPxTextBox ID="hiddenIdPerfilMTM" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdPerfilMTM"
                                                        Text='<%#Eval("IdPerfilMTM")%>' />
                                                    <dxe:ASPxTextBox ID="hiddenIdGrupoPerfilMTM" runat="server" CssClass="hiddenField"
                                                        ClientInstanceName="hiddenIdGrupoPerfilMTM" Text='<%#Eval("IdGrupoPerfilMTM")%>' />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelGrupoPerfilMTM" runat="server" CssClass="labelRequired" Text="Grupo Perfil MTM:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditGrupoPerfilMTM" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditGrupoPerfilMTM" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdGrupoPerfilMTM") != null ? Eval("IdGrupoPerfilMTM") + " - " + Eval("DescricaoGrupo") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupGrupoPerfilMTM.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Série:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditSerieRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditSerieRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdSerie") != null ? Eval("IdSerie") + " - " + Eval("DescricaoSerie") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupSerieRendaFixa.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelBtnPapel" runat="server" CssClass="labelRequired" Text="Papel:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditPapelRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditPapelRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdPapel") != null ? Eval("IdPapel") + " - " + Eval("DescricaoPapel") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupPapelRendaFixa.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Título:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditTituloRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditTituloRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdTitulo") != null ? Eval("IdTitulo") + " - " + Eval("DescricaoTitulo") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRendaFixa.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxButton ID="btnLimparTituloRendaFixa" runat="server" AutoPostBack="false"
                                                                    BackgroundImage-ImageUrl="~\imagens\delete.png" BackgroundImage-HorizontalPosition="center"
                                                                    BackgroundImage-VerticalPosition="center" BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdTitulo.SetValue(null); btnEditTituloRendaFixa.SetValue(null); hiddenIdOperacao.SetValue(null); btnEditOperacaoRendaFixa.SetValue(null)}">
                                                                </dxe:ASPxButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelBtnOperacao" runat="server" CssClass="labelNormal" Text="Operação:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditOperacaoRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditOperacaoRendaFixa" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdOperacao") != null ? Eval("IdOperacao") + " - Cliente " + Eval("IdCliente") + " - Título " + Eval("IdTitulo"): " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupOperacaoRendaFixa.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxButton ID="btnLimparOperacaoRendaFixa" runat="server" AutoPostBack="false"
                                                                    BackgroundImage-ImageUrl="~\imagens\delete.png" BackgroundImage-HorizontalPosition="center"
                                                                    BackgroundImage-VerticalPosition="center" BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdOperacao.SetValue(null); btnEditOperacaoRendaFixa.SetValue(null); }">
                                                                </dxe:ASPxButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDtReferencia" runat="server" CssClass="labelRequired" Text="Dt.Referência:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxDateEdit ID="textDtReferencia" runat="server" ClientInstanceName="textDtReferencia"
                                                                    Width="150px" Value='<%#Eval("DtReferencia")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSPerfilMTM" runat="server" OnesSelect="EsDSPerfilMTM_esSelect" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSSerieRendaFixa" runat="server" OnesSelect="EsDSSerieRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixa" runat="server" OnesSelect="EsDSTituloRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSPapelRendaFixa" runat="server" OnesSelect="EsDSPapelRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoPerfilMTM" runat="server" OnesSelect="EsDSGrupoPerfilMTM_esSelect" />
    </form>
</body>
</html>
