﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_SerieRendaFixa : Financial.Web.Common.CadastroBasePage
{

    #region Campos do Grid
    GridViewDataComboBoxColumn TipoTaxa;
    ASPxTextBox hiddenIdFeeder;
    ASPxTextBox textDescricao;
    ASPxComboBox dropTipoTaxa;
    ASPxSpinEdit textCodigoBDS;
    #endregion

    #region Popup Feeder
    protected void callBackPopupFeeder_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idFeeder = Convert.ToInt32(e.Parameter);

            FeederQuery feederQuery = new FeederQuery();
            Feeder feeder = new Feeder();

            feederQuery.Select(feederQuery.IdFeeder, feederQuery.Descricao);
            feederQuery.Where(feederQuery.IdFeeder == idFeeder);

            if (feeder.Load(feederQuery))
                e.Result = "Feeder: " + feeder.IdFeeder.ToString() + " - " + feeder.Descricao.ToString();
            //                            
        }
    }

    protected void EsDSFeeder_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FeederCollection feederColl = new FeederCollection();

        feederColl.Query.Select(feederColl.Query.IdFeeder,
                                feederColl.Query.Descricao);
        feederColl.Query.OrderBy(feederColl.Query.IdFeeder.Ascending);
        feederColl.LoadAll();
        e.Collection = feederColl;
    }

    protected void gridFeeder_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridFeeder_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridFeeder.DataBind();
    }

    protected void gridFeeder_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idFeeder = (int)gridView.GetRowValues(visibleIndex, FeederMetadata.ColumnNames.IdFeeder);
        e.Result = idFeeder.ToString();
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        popularASPxComboBoxGrid();

    }
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        FeederQuery feederQuery = new FeederQuery("feeder");
        SerieRendaFixaQuery serieQuery = new SerieRendaFixaQuery("serie");
        SerieRendaFixaCollection serieColl = new SerieRendaFixaCollection();

        serieQuery.Select(serieQuery, 
                         feederQuery.Descricao.As("DescricaoFeeder"));
        serieQuery.InnerJoin(feederQuery).On(serieQuery.IdFeeder == feederQuery.IdFeeder);
        serieQuery.OrderBy(serieQuery.IdSerie.Ascending);

        serieColl.Load(serieQuery);

        e.Collection = serieColl;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SerieRendaFixa serieRendaFixa = new SerieRendaFixa();
        int idSerie = (int)e.Keys[0];

        if (serieRendaFixa.LoadByPrimaryKey(idSerie))
        {

            dropTipoTaxa = gridCadastro.FindEditFormTemplateControl("dropTipoTaxa") as ASPxComboBox;
            hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox; 
            textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;

            serieRendaFixa.IdFeeder = Convert.ToInt32(hiddenIdFeeder.Text);
            serieRendaFixa.TipoTaxa = Convert.ToInt32(dropTipoTaxa.SelectedItem.Value);
            serieRendaFixa.Descricao = textDescricao.Text;

            if (!string.IsNullOrEmpty(textCodigoBDS.Text))
                serieRendaFixa.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
            else
                serieRendaFixa.CodigoBDS = null;

            serieRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de SerieRendaFixa - Operacao: Update SerieRendaFixa: " + idSerie + UtilitarioWeb.ToString(serieRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdSerie");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idSerie = Convert.ToInt32(keyValuesId[i]);

                SerieRendaFixa serieRendaFixa = new SerieRendaFixa();
                if (serieRendaFixa.LoadByPrimaryKey(idSerie))
                {
                    SerieRendaFixa serieRendaFixaClone = (SerieRendaFixa)Utilitario.Clone(serieRendaFixa);
                    //
                    serieRendaFixa.MarkAsDeleted();
                    serieRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de SerieRendaFixa - Operacao: Delete SerieRendaFixa: " + idSerie + UtilitarioWeb.ToString(serieRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        dropTipoTaxa = gridCadastro.FindEditFormTemplateControl("dropTipoTaxa") as ASPxComboBox;
        hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox; ;
        textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        if (e.Parameter.Equals("btnDelete"))
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues("IdSerie");
            for (int i = 0; i < keyValues.Count; i++)
            {
                int IdSerie = Convert.ToInt32(keyValues[i]);

                SerieRendaFixa serieRendaFixa = new SerieRendaFixa();
                serieRendaFixa.LoadByPrimaryKey(IdSerie);

                CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
                if (curvaRendaFixa.LoadByPrimaryKey(IdSerie))
                {
                    e.Result = "Série " + serieRendaFixa.Descricao + " não pode ser excluída pois há registros de curva para a mesma!";
                }

                PerfilMTMCollection perfilMTMCollection = new PerfilMTMCollection();
                perfilMTMCollection.Query.Where(perfilMTMCollection.Query.IdSerie == IdSerie);
                if (perfilMTMCollection.Query.Load())
                {
                    e.Result = "Série " + serieRendaFixa.Descricao + " não pode ser excluída pois há registros de perfis de MTM para a mesma!";
                }
            }
        }
        else
        {
            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(hiddenIdFeeder);
            controles.Add(dropTipoTaxa);
            controles.Add(textDescricao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        popularASPxComboBoxGrid();
        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    private void popularASPxComboBoxGrid()
    {
        TipoTaxa = gridCadastro.Columns["TipoTaxa"] as GridViewDataComboBoxColumn;
        if (TipoTaxa != null)
        {
            TipoTaxa.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoMTMTitulo)))
            {
                TipoTaxa.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.TipoMTMTituloDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropTipoTaxa_Init(object sender, EventArgs e)
    {
        (sender as ASPxComboBox).Items.Clear();
        foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoMTMTitulo)))
        {
            (sender as ASPxComboBox).Items.Add(Financial.RendaFixa.Enums.TipoMTMTituloDescricao.RetornaStringValue(i), i);
        }
    }

    private void SalvarNovo()
    {
        dropTipoTaxa = gridCadastro.FindEditFormTemplateControl("dropTipoTaxa") as ASPxComboBox;
        hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox; ;
        textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;

        SerieRendaFixa serieRendaFixa = new SerieRendaFixa();

        serieRendaFixa.Descricao = textDescricao.Text;
        serieRendaFixa.TipoTaxa = Convert.ToInt32(dropTipoTaxa.SelectedItem.Value);
        serieRendaFixa.IdFeeder = Convert.ToInt32(hiddenIdFeeder.Text);

        if (!string.IsNullOrEmpty(textCodigoBDS.Text))
            serieRendaFixa.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
        else
            serieRendaFixa.CodigoBDS = null;

        serieRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de SerieRendaFixa - Operacao: Insert SerieRendaFixa: " + serieRendaFixa.IdSerie + UtilitarioWeb.ToString(serieRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }
}