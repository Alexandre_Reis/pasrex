﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.RendaFixa;

public partial class CadastrosBasicos_TabelaCustosRendaFixa : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        this.HasPopupTituloRF = true;

        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaCustosRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaCustosRendaFixaQuery tabelaCustosRendaFixaQuery = new TabelaCustosRendaFixaQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        tabelaCustosRendaFixaQuery.Select(tabelaCustosRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta);
        tabelaCustosRendaFixaQuery.LeftJoin(clienteQuery).On(tabelaCustosRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        tabelaCustosRendaFixaQuery.LeftJoin(tituloRendaFixaQuery).On(tabelaCustosRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);

        tabelaCustosRendaFixaQuery.OrderBy(tabelaCustosRendaFixaQuery.IdCliente.Ascending, tabelaCustosRendaFixaQuery.IdAgente.Ascending);

        TabelaCustosRendaFixaCollection coll = new TabelaCustosRendaFixaCollection();
        coll.Load(tabelaCustosRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRendaFixaFiltro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.Descricao,
                                    tituloRendaFixaQuery.DataEmissao,
                                    tituloRendaFixaQuery.DataVencimento,
                                    tituloRendaFixaQuery.DescricaoCompleta.Trim(),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
        tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdTitulo.Ascending, tituloRendaFixaQuery.DataVencimento.Ascending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(tituloRendaFixaQuery);

        //
        e.Collection = coll;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.DescricaoCompleta);
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdTitulo == idTitulo);

            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.HasData)
            {
                string descricaoCompleta = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta).ToString();

                texto = descricaoCompleta.ToString();
            }
        }
        e.Result = texto;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        if (dropClasse == null || dropClasse.SelectedIndex <= 0)
        {
            e.Collection = new TituloRendaFixaCollection();
            return;
        }
        
        int classe = Convert.ToInt32(dropClasse.SelectedItem.Value);

        if (gridCadastro.IsEditing)
        {
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
            tituloRendaFixaQuery.Select(tituloRendaFixaQuery, papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
            tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            tituloRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal(classe));
            tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.DataVencimento.Ascending);

            TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
            coll.Load(tituloRendaFixaQuery);

            e.Collection = coll;
        }
        else
        {
            e.Collection = new TituloRendaFixaCollection();
        }
    }


    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textTaxaAno = gridCadastro.FindEditFormTemplateControl("textTaxaAno") as ASPxSpinEdit;
        ASPxSpinEdit textValorMensal = gridCadastro.FindEditFormTemplateControl("textValorMensal") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropAgente);
        controles.Add(textDataReferencia);

        if (textTaxaAno.Text == "" && textValorMensal.Text == "")
        {
            e.Result = "Deve ser informada a taxa ano ou o valor mensal.";
            return;
        }

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        ASPxSpinEdit textTaxaAno = gridCadastro.FindEditFormTemplateControl("textTaxaAno") as ASPxSpinEdit;
        ASPxSpinEdit textValorMensal = gridCadastro.FindEditFormTemplateControl("textValorMensal") as ASPxSpinEdit;
        //
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            

        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);

        int? idCliente = null;
        if (btnEditCodigoCliente.Text != "")
        {
            idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        }

        int? classe = null;
        if (dropClasse.SelectedIndex > 0)
        {
            classe = Convert.ToInt32(dropClasse.SelectedItem.Value);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        }

        decimal taxaAno = textTaxaAno.Text != "" ? Convert.ToDecimal(textTaxaAno.Text) : 0;
        decimal valorMensal = textValorMensal.Text != "" ? Convert.ToDecimal(textValorMensal.Text) : 0;

        int idTabela = (int)e.Keys[0];

        if (tabelaCustosRendaFixa.LoadByPrimaryKey(idTabela))
        {
            tabelaCustosRendaFixa.Classe = classe;
            tabelaCustosRendaFixa.DataReferencia = dataReferencia;
            tabelaCustosRendaFixa.IdAgente = idAgente;
            tabelaCustosRendaFixa.IdCliente = idCliente;
            tabelaCustosRendaFixa.TaxaAno = taxaAno;
            tabelaCustosRendaFixa.ValorMensal = valorMensal;

            tabelaCustosRendaFixa.IdTitulo = idTitulo;

            tabelaCustosRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaCustosRendaFixa - Operacao: Update TabelaCustosRendaFixa: " + idCliente + "; " + idAgente + "; " + dataReferencia + UtilitarioWeb.ToString(tabelaCustosRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        ASPxSpinEdit textTaxaAno = gridCadastro.FindEditFormTemplateControl("textTaxaAno") as ASPxSpinEdit;
        ASPxSpinEdit textValorMensal = gridCadastro.FindEditFormTemplateControl("textValorMensal") as ASPxSpinEdit;
        //
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);

        int? idCliente = null;
        if (btnEditCodigoCliente.Text != "")
        {
            idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        }

        int? classe = null;
        if (dropClasse.SelectedIndex > 0)
        {
            classe = Convert.ToInt32(dropClasse.SelectedItem.Value);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        }

        decimal taxaAno = textTaxaAno.Text != "" ? Convert.ToDecimal(textTaxaAno.Text) : 0;
        decimal valorMensal = textValorMensal.Text != "" ? Convert.ToDecimal(textValorMensal.Text) : 0;

        tabelaCustosRendaFixa.Classe = classe;
        tabelaCustosRendaFixa.DataReferencia = dataReferencia;
        tabelaCustosRendaFixa.IdAgente = idAgente;
        tabelaCustosRendaFixa.IdCliente = idCliente;
        tabelaCustosRendaFixa.TaxaAno = taxaAno;
        tabelaCustosRendaFixa.ValorMensal = valorMensal;
        //
        tabelaCustosRendaFixa.IdTitulo = idTitulo;
        //
        tabelaCustosRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaCustosRendaFixa - Operacao: Insert TabelaCustosRendaFixa: " + tabelaCustosRendaFixa.IdCliente + "; " + tabelaCustosRendaFixa.IdAgente + "; " + tabelaCustosRendaFixa.DataReferencia + UtilitarioWeb.ToString(tabelaCustosRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdTabela = gridCadastro.GetSelectedFieldValues(TabelaCustosRendaFixaMetadata.ColumnNames.IdTabela);

            for (int i = 0; i < keyValuesIdTabela.Count; i++)
            {
                int idTabela = Convert.ToInt32(keyValuesIdTabela[i]);

                TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
                if (tabelaCustosRendaFixa.LoadByPrimaryKey(idTabela))
                {
                    //
                    TabelaCustosRendaFixa tabelaCustosRendaFixaClone = (TabelaCustosRendaFixa)Utilitario.Clone(tabelaCustosRendaFixa);
                    //

                    tabelaCustosRendaFixa.MarkAsDeleted();
                    tabelaCustosRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaCustosRendaFixa - Operacao: Delete TabelaCustosRendaFixa: " + tabelaCustosRendaFixaClone.IdCliente + "; " + tabelaCustosRendaFixaClone.IdAgente + "; " + tabelaCustosRendaFixaClone.DataReferencia + UtilitarioWeb.ToString(tabelaCustosRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            e.Properties["cpHiddenIdTitulo"] = hiddenIdTitulo.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
    }
}