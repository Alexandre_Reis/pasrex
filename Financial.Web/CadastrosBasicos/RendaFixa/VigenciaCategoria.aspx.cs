﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_VigenciaCategoria : Financial.Web.Common.CadastroBasePage
{

    #region Campos do Grid
    GridViewDataComboBoxColumn TipoOperacao;
    ASPxTextBox hiddenIdOperacao;
    ASPxDateEdit textDataVigencia;
    ASPxComboBox dropTipoVigenciaCategoria;
    #endregion

    #region Popup OperacaoRendaFixa
    protected void callBackPopupOperacaoRendaFixa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idOperacaoRendaFixa = Convert.ToInt32(e.Parameter);

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.IdOperacao,
                                        operacaoRendaFixa.Query.IdCliente,
                                        operacaoRendaFixa.Query.IdTitulo);
            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdOperacao == idOperacaoRendaFixa);

            if (operacaoRendaFixa.Load(operacaoRendaFixa.Query))
            {
                string strResult = string.Empty;
                strResult += operacaoRendaFixa.IdOperacao.ToString();

                e.Result = strResult;
            }
        }
    }

    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        OperacaoRendaFixaCollection operacaoRendaFixaColl = new OperacaoRendaFixaCollection();

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.IdOperacao,
                                     operacaoRendaFixaQuery.IdCliente,
                                     operacaoRendaFixaQuery.IdTitulo,
                                     operacaoRendaFixaQuery.TipoOperacao,
                                     operacaoRendaFixaQuery.DataOperacao,
                                     operacaoRendaFixaQuery.DataLiquidacao,
                                     tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoPapel"),
                                     clienteQuery.Apelido.As("ClienteApelido"));
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.Where(tituloRendaFixaQuery.DataVencimento >= clienteQuery.DataDia);
        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.IdOperacao.Ascending);

        operacaoRendaFixaColl.Load(operacaoRendaFixaQuery);
        e.Collection = operacaoRendaFixaColl;

    }

    protected void gridOperacaoRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridOperacaoRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridOperacaoRendaFixa.DataBind();
    }

    protected void gridOperacaoRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idOperacaoRendaFixa = (int)gridView.GetRowValues(visibleIndex, OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
        e.Result = idOperacaoRendaFixa.ToString();
    }

    protected void gridOperacaoRendaFixa_load(object sender, EventArgs e)
    {
        TipoOperacao = gridOperacaoRendaFixa.Columns["TipoOperacao"] as GridViewDataComboBoxColumn;
        if (TipoOperacao != null)
        {
            TipoOperacao.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoOperacaoTitulo)))
            {
                TipoOperacao.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.TipoOperacaoTituloDescricao.RetornaStringValue(i), i);
            }
        }

    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void callbackClienteFiltro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";        
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)Financial.Investidor.Enums.StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        UtilitarioWeb util = new UtilitarioWeb();
                        if (util.IsClienteBloqueadoSinacor(idCliente))
                        {
                            resultado = "no_active";
                        }
                        else
                        {
                            PermissaoCliente permissaoCliente = new PermissaoCliente();
                            if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                            {
                                nome = cliente.str.Apelido;                                
                                resultado = nome;
                            }
                            else
                            {
                                resultado = "no_access";
                            }
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void dropTipoVigenciaCategoria_Load(object sender, EventArgs e)
    {
        dropTipoVigenciaCategoria = gridCadastro.FindEditFormTemplateControl("dropTipoVigenciaCategoria") as ASPxComboBox;
        if (dropTipoVigenciaCategoria != null)
        {
            dropTipoVigenciaCategoria.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoVigenciaCategoria)))
            {
                dropTipoVigenciaCategoria.Items.Add(Financial.RendaFixa.Enums.TipoVigenciaCategoriaDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn TipoVigenciaCategoria = gridCadastro.Columns["TipoVigenciaCategoria"] as GridViewDataComboBoxColumn;
        if (TipoVigenciaCategoria != null)
        {
            TipoVigenciaCategoria.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoVigenciaCategoria)))
            {
                TipoVigenciaCategoria.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.TipoVigenciaCategoriaDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void EsDSVigenciaCategoria_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        VigenciaCategoriaCollection objVigenciaCategoriaColl = new VigenciaCategoriaCollection();
        VigenciaCategoriaQuery objVigenciaCategoriaQuery = new VigenciaCategoriaQuery("VigenciaCategoria");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        objVigenciaCategoriaQuery.Select(objVigenciaCategoriaQuery,
                                tituloRendaFixaQuery.DescricaoCompleta.As("DescricaoTitulo"),
                                clienteQuery.Apelido.As("DescricaoCliente"));
        objVigenciaCategoriaQuery.InnerJoin(operacaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdOperacao == objVigenciaCategoriaQuery.IdOperacao);
        objVigenciaCategoriaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
        objVigenciaCategoriaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == operacaoRendaFixaQuery.IdCliente);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            objVigenciaCategoriaQuery.Where(objVigenciaCategoriaQuery.DataVigencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            objVigenciaCategoriaQuery.Where(objVigenciaCategoriaQuery.DataVigencia.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            objVigenciaCategoriaQuery.Where(clienteQuery.IdCliente == Convert.ToInt32(btnEditCodigoClienteFiltro.Text));
        }

        objVigenciaCategoriaColl.Load(objVigenciaCategoriaQuery);

        e.Collection = objVigenciaCategoriaColl;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        VigenciaCategoria objVigenciaCategoria = new VigenciaCategoria();
        int idOVigenciaCategoria = (int)e.Keys[0];

        if (objVigenciaCategoria.LoadByPrimaryKey(idOVigenciaCategoria))
        {
            hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
            textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
            dropTipoVigenciaCategoria = gridCadastro.FindEditFormTemplateControl("dropTipoVigenciaCategoria") as ASPxComboBox;

            objVigenciaCategoria.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
            objVigenciaCategoria.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);
            objVigenciaCategoria.TipoVigenciaCategoria = Convert.ToInt32(dropTipoVigenciaCategoria.SelectedItem.Value);

            objVigenciaCategoria.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Vigencia Categoria - Operacao: Update VigenciaCategoria: " + idOVigenciaCategoria + UtilitarioWeb.ToString(objVigenciaCategoria),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdVigenciaCategoria");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int IdVigenciaCategoria = Convert.ToInt32(keyValuesId[i]);

                VigenciaCategoria objVigenciaCategoria = new VigenciaCategoria();
                if (objVigenciaCategoria.LoadByPrimaryKey(IdVigenciaCategoria))
                {
                    VigenciaCategoria objVigenciaCategoriaClone = (VigenciaCategoria)Utilitario.Clone(objVigenciaCategoria);
                    //
                    objVigenciaCategoria.MarkAsDeleted();
                    objVigenciaCategoria.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Vigencia Categoria - Operacao: Delete VigenciaCategoria: " + IdVigenciaCategoria + UtilitarioWeb.ToString(objVigenciaCategoriaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == SerieRendaFixaMetadata.ColumnNames.IdSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void btnEditOperacaoRendaFixa_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).Enabled = false;
    }

    protected void textDataVigencia_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
        dropTipoVigenciaCategoria = gridCadastro.FindEditFormTemplateControl("dropTipoVigenciaCategoria") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenIdOperacao);
        controles.Add(textDataVigencia);
        controles.Add(dropTipoVigenciaCategoria);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idOperacaoRF = Convert.ToInt32(hiddenIdOperacao.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDataVigencia.Text);
        OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

        if (operacaoRendaFixa.LoadByPrimaryKey(idOperacaoRF))
        {
            if(operacaoRendaFixa.DataOperacao.Value >= dataVigencia)
                e.Result = "A data da operação não pode ser maior ou igual a data de vigência!";
        }

        // Somente se for Insert
        if (gridCadastro.IsNewRowEditing)
        {
            VigenciaCategoria objVigenciaCategoria = new VigenciaCategoria();
            int TipoVigenciaCategoria = Convert.ToInt32(dropTipoVigenciaCategoria.SelectedItem.Value);

            objVigenciaCategoria.Query.Where(objVigenciaCategoria.Query.IdOperacao.Equal(idOperacaoRF)
                                             & objVigenciaCategoria.Query.DataVigencia.Equal(dataVigencia));

            if (objVigenciaCategoria.Load(objVigenciaCategoria.Query))
            {
                e.Result = "Registro já existente.!";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        VigenciaCategoria objVigenciaCategoria = new VigenciaCategoria();

        hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
        dropTipoVigenciaCategoria = gridCadastro.FindEditFormTemplateControl("dropTipoVigenciaCategoria") as ASPxComboBox;

        objVigenciaCategoria.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
        objVigenciaCategoria.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);
        objVigenciaCategoria.TipoVigenciaCategoria = Convert.ToInt32(dropTipoVigenciaCategoria.SelectedItem.Value);

        objVigenciaCategoria.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de VigenciaCategoria - Operacao: Insert VigenciaCategoria: " + objVigenciaCategoria.IdVigenciaCategoria + UtilitarioWeb.ToString(objVigenciaCategoria),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }
}