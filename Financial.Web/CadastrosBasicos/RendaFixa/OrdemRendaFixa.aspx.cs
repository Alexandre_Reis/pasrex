﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Util.Enums;
using Financial.Fundo.Enums;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Transactions;

public partial class CadastrosBasicos_OrdemRendaFixa : CadastroBasePage
{
    bool aprovacao = false;

    


    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupTituloRF = true;
        this.HasPopupCarteira = true;
        this.HasPopupAgenteMercado = true;
        base.Page_Load(sender, e);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;
        GrupoUsuario grupoUsuario = new GrupoUsuario();
        grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

        aprovacao = grupoUsuario.Front == (byte)PermissaoFront.Aprovacao;

        if (!aprovacao)
        {
            btnAprova.Visible = false;
            btnCancela.Visible = false;
            btnFilter.Visible = false;
        }

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { OrdemRendaFixaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOrdemRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OrdemRendaFixaQuery ordemRendaFixaQuery = new OrdemRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AG");

        
        ordemRendaFixaQuery.Select(ordemRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.Descricao,
                                   tituloRendaFixaQuery.DescricaoCompleta, 
                                   carteiraQuery.Apelido.As("CarteiraContraparte"),
                                   agenteMercadoQuery.Apelido.As("AgenteMercadoContraparte"),
                                   agenteMercadoQuery.IdAgente.As("IdAgenteContraparte"),
                                   carteiraQuery.IdCarteira.As("IdCarteiraContraparte"));
        ordemRendaFixaQuery.InnerJoin(clienteQuery).On(ordemRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        ordemRendaFixaQuery.LeftJoin(tituloRendaFixaQuery).On(ordemRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        ordemRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        ordemRendaFixaQuery.LeftJoin(carteiraQuery).On(ordemRendaFixaQuery.IdCarteiraContraParte == carteiraQuery.IdCarteira);
        ordemRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(ordemRendaFixaQuery.IdAgenteContraParte == agenteMercadoQuery.IdAgente);        
        ordemRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));


        
        

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            ordemRendaFixaQuery.Where(ordemRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            ordemRendaFixaQuery.Where(ordemRendaFixaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            ordemRendaFixaQuery.Where(ordemRendaFixaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        ordemRendaFixaQuery.OrderBy(ordemRendaFixaQuery.IdOperacao.Descending);

        OrdemRendaFixaCollection coll = new OrdemRendaFixaCollection();
        coll.Load(ordemRendaFixaQuery);

        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!aprovacao)
        {
            e.Collection = new TituloRendaFixaCollection();
            return;
        }

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (gridCadastro.IsEditing)
        {
            if (btnEditCodigoCliente.Text != "")
            {
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    DateTime dataDia = cliente.DataDia.Value;

                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
                    tituloRendaFixaQuery.Select(tituloRendaFixaQuery, papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
                    tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                    tituloRendaFixaQuery.Where(tituloRendaFixaQuery.DataVencimento.GreaterThanOrEqual(dataDia));
                    tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.DataVencimento.Ascending);

                    TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
                    coll.Load(tituloRendaFixaQuery);

                    e.Collection = coll;
                }
                else
                {
                    e.Collection = new TituloRendaFixaCollection();
                }
            }
            else
            {
                e.Collection = new TituloRendaFixaCollection();
            }
        }
        else
        {
            e.Collection = new TituloRendaFixaCollection();
        }
    }

    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.LoadAll();
        e.Collection = coll;        
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.LoadAll();
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string RetornaCalculoCompromisso()
    {
        string textRetorno = "";
        if (gridCadastro.IsEditing)
        {
            DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
            ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;

            if (textDataVolta.Text != "" && textTaxaVolta.Text != "" &&
                textPUOperacao.Text != "" && textQuantidade.Text != "")
            {
                DateTime dataOperacao = hoje;
                decimal puOperacao = Convert.ToDecimal(textPUOperacao.Text);
                DateTime dataVolta = Convert.ToDateTime(textDataVolta.Text);
                decimal taxaVolta = Convert.ToDecimal(textTaxaVolta.Text);
                decimal quantidade = Convert.ToDecimal(textQuantidade.Text);

                int dias = Calendario.NumeroDias(dataOperacao, dataVolta, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                if (dias < 0)
                {
                    return "";
                }

                decimal fator = CalculoFinanceiro.CalculaFatorPreExponencial(dias, taxaVolta, Financial.Util.Enums.BaseCalculo.Base252);

                decimal puVolta = Utilitario.Truncate(puOperacao * fator, 8);
                decimal valorVolta = Utilitario.Truncate(quantidade * puVolta, 2);

                textRetorno = puVolta.ToString().Replace(",", ".") + "|" + valorVolta.ToString().Replace(",", ".");
            }
        }
        return textRetorno;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = RetornaCalculoCompromisso();

        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackTaxa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;

        if (hiddenIdTitulo.Text == "")
        {
            e.Result = "Título deve ser informado para o cálculo da taxa.";
            return;
        }

        if (textPUOperacao.Text == "")
        {
            e.Result = "PU Operação deve ser informado para o cálculo da taxa.";
            return;
        }

        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);

        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(tituloRendaFixa.Query.IdPapel);
        tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
        int idPapel = tituloRendaFixa.IdPapel.Value;

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
        campos = new List<esQueryItem>();
        campos.Add(papelRendaFixa.Query.ContagemDias);
        papelRendaFixa.LoadByPrimaryKey(campos, idPapel);
        byte contagemDias = papelRendaFixa.ContagemDias.Value;

        AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.Valor.Sum(), agendaRendaFixaCollection.Query.DataEvento);
        agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                              agendaRendaFixaCollection.Query.DataEvento.GreaterThan(hoje));
        agendaRendaFixaCollection.Query.GroupBy(agendaRendaFixaCollection.Query.DataEvento);
        agendaRendaFixaCollection.Query.Load();

        List<CalculoFinanceiro.CashFlow> cashFlowList = new List<CalculoFinanceiro.CashFlow>();
        cashFlowList.Add(new CalculoFinanceiro.CashFlow(Convert.ToDouble(textPUOperacao.Text) * (double)-1M, hoje));
        foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
        {
            cashFlowList.Add(new CalculoFinanceiro.CashFlow((double)agendaRendaFixa.Valor.Value, agendaRendaFixa.DataEvento.Value));
        }

        decimal TIR = Utilitario.Truncate((decimal)(CalculoFinanceiro.CalculaTIR(cashFlowList, hoje, (ContagemDias)contagemDias) * (double)100M), 16);


        e.Result = TIR.ToString().Replace(",", ".");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        if (!aprovacao)
        {
            Label labelTitulo = gridCadastro.FindEditFormTemplateControl("labelTitulo") as Label;
            Label labelTaxaOperacao = gridCadastro.FindEditFormTemplateControl("labelTaxaOperacao") as Label;
            ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            LinkButton btnCalculaTaxa = gridCadastro.FindEditFormTemplateControl("btnCalculaTaxa") as LinkButton;

            btnEditTituloRF.Visible = false;
            textTaxaOperacao.Visible = false;
            labelTitulo.Visible = false;
            labelTaxaOperacao.Visible = false;
            btnCalculaTaxa.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            byte status = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Status"));

            if (!aprovacao)
            {
                if ((byte)status == (byte)StatusOrdemRendaFixa.Aprovado)
                {
                    labelEdicao.Text = "Ordem está aprovada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
                else if ((byte)status == (byte)StatusOrdemRendaFixa.Cancelado)
                {
                    labelEdicao.Text = "Ordem está cancelada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
            }

            else if ((byte)status == (byte)StatusOrdemRendaFixa.Processado)
            {
                labelEdicao.Text = "Ordem está processada! Dados não podem ser alterados.";
                travaPainel = true;
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TimeSpan horaLimite = DateTime.Now.TimeOfDay;

        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        if (tabelaParametrosFrontCollection.Count > 0)
        {
            if (tabelaParametrosFrontCollection[0].HorarioFimRendaFixa.HasValue)
            {
                if (horaLimite > tabelaParametrosFrontCollection[0].HorarioFimRendaFixa.Value.TimeOfDay)
                {
                    e.Result = "Fora do Horário permitido. Operação não pode ser realizada.";
                    return;
                }
            }
        }

        if (!gridCadastro.IsEditing)
        {
            List<object> keyId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyStatus = gridCadastro.GetSelectedFieldValues("Status");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyId[i]);
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                byte status = Convert.ToByte(keyStatus[i]);

                if (e.Parameter == "deletar")
                {
                    if (!aprovacao)
                    {
                        if ((byte)status == (byte)StatusOrdemRendaFixa.Aprovado)
                        {
                            e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                            return;
                        }
                        else if ((byte)status == (byte)StatusOrdemRendaFixa.Cancelado)
                        {
                            e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                            return;
                        }
                    }
                }
                else if (e.Parameter == "aprovar" || e.Parameter == "cancelar")
                {
                    if ((byte)status == (byte)StatusOrdemRendaFixa.Aprovado)
                    {
                        e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                        return;
                    }
                    else if ((byte)status == (byte)StatusOrdemRendaFixa.Cancelado)
                    {
                        e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                        return;
                    }
                }

                if ((byte)status == (byte)StatusOrdemRendaFixa.Processado)
                {
                    e.Result = "Ordem " + idOperacao + " está processada! Operação não pode ser realizada.";
                    return;
                }
            }
        }
        else
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);

            if (aprovacao)
            {
                controles.Add(btnEditTituloRF);
                controles.Add(textTaxaOperacao);
            }

            controles.Add(dropTipoOperacao);

            if (aprovacao)
            {
                if (textQuantidade.Text == "" || textPUOperacao.Text == "")
                {
                    e.Result = "Para o caso de perfil do tipo Boletagem os campos Quantidade e PU são obrigatórios";
                    return;
                }
            }

            controles.Add(textValor);
            controles.Add(dropFormaLiquidacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        }
    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo()
    {
        using (TransactionScope transaction = new TransactionScope())
        {
            DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxSpinEdit btnCarteiraContraparte = gridCadastro.FindEditFormTemplateControl("btnCarteiraContraparte") as ASPxSpinEdit;
            ASPxSpinEdit btnAgenteContraParte = gridCadastro.FindEditFormTemplateControl("btnAgenteContraParte") as ASPxSpinEdit;
            ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;


            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;

            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropTipoNegociacao = gridCadastro.FindEditFormTemplateControl("dropTipoNegociacao") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
            ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
            //
            OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
            //
        //
        TituloRendaFixa titulo = new TituloRendaFixa();
        titulo.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTitulo.Text));
        //
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ordemRendaFixa.IdCliente = idCliente;

            if (dropMercadoCliente.SelectedItem != null && dropMercadoCliente.SelectedItem.Value == "1" && btnAgenteContraParte.Value != null)
                ordemRendaFixa.IdAgenteContraParte = Convert.ToInt32(btnAgenteContraParte.Number);

            

            if (aprovacao)
            {
                ordemRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
                ordemRendaFixa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
            }

            ordemRendaFixa.DataOperacao = hoje;
            ordemRendaFixa.DataLiquidacao = hoje;
            ordemRendaFixa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            if (textQuantidade.Text != "")
            {
                ordemRendaFixa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            }

            if (textPUOperacao.Text != "")
            {
                ordemRendaFixa.PUOperacao = Convert.ToDecimal(textPUOperacao.Text);
            }

            ordemRendaFixa.Valor = Convert.ToDecimal(textValor.Text);
            ordemRendaFixa.IdLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
        int tipoPapel = (int)titulo.UpToPapelRendaFixaByIdPapel.TipoPapel.Value;
        ordemRendaFixa.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            ordemRendaFixa.TipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);

        if (dropTrader.SelectedIndex > -1)
            ordemRendaFixa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            ordemRendaFixa.IdTrader = null;

            //Operação Compromissada
            if (ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
            {
                ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;

                if (textDataVolta.Text != "")
                {
                    ordemRendaFixa.DataVolta = Convert.ToDateTime(textDataVolta.Text);
                }

                if (textTaxaVolta.Text != "")
                {
                    ordemRendaFixa.TaxaVolta = Convert.ToDecimal(textTaxaVolta.Text);
                }

                if (textPUVolta.Text != "")
                {
                    ordemRendaFixa.PUVolta = Convert.ToDecimal(textPUVolta.Text);
                }

                if (textValorVolta.Text != "")
                {
                    ordemRendaFixa.ValorVolta = Convert.ToDecimal(textValorVolta.Text);
                }
            }
            else
            {
                ordemRendaFixa.DataVolta = null;
                ordemRendaFixa.TaxaVolta = null;
                ordemRendaFixa.PUVolta = null;
                ordemRendaFixa.ValorVolta = null;
            }

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                ordemRendaFixa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                ordemRendaFixa.IdCategoriaMovimentacao = null;
            }

            ordemRendaFixa.Observacao = textObservacao.Text;

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;
            GrupoUsuario grupoUsuario = new GrupoUsuario();
            grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

            if (grupoUsuario.Front == (byte)PermissaoFront.Boletagem)
            {
                ordemRendaFixa.Status = (byte)StatusOrdemRendaFixa.Digitado;
            }
            else
            {
                ordemRendaFixa.Status = (byte)StatusOrdemRendaFixa.Aprovado;
            }
            //

            ordemRendaFixa.Save();

            if (dropMercadoCliente.SelectedItem != null && dropMercadoCliente.SelectedItem.Value == "2" && btnCarteiraContraparte.Value != null)
            {
                //ordemRendaFixa.IdCarteiraContraParte = Convert.ToInt32(btnCarteiraContraparte.Number);


                OrdemRendaFixa ordemRendaFixaContraParte = new OrdemRendaFixa();
                ordemRendaFixaContraParte.IdCliente = Convert.ToInt32(btnCarteiraContraparte.Number);
                ordemRendaFixaContraParte.IdTitulo = ordemRendaFixa.IdTitulo;
                ordemRendaFixaContraParte.TipoOperacao = TipoOperacaoContraParte(ordemRendaFixa.TipoOperacao);
                ordemRendaFixaContraParte.TaxaOperacao = ordemRendaFixa.TaxaOperacao;
                ordemRendaFixaContraParte.IdLiquidacao = ordemRendaFixa.IdLiquidacao;
                ordemRendaFixaContraParte.TipoNegociacao = ordemRendaFixa.TipoNegociacao;
                ordemRendaFixaContraParte.DataVolta = ordemRendaFixa.DataVolta;
                ordemRendaFixaContraParte.TaxaVolta = ordemRendaFixa.TaxaVolta;
                ordemRendaFixaContraParte.Quantidade = ordemRendaFixa.Quantidade;
                ordemRendaFixaContraParte.PUOperacao = ordemRendaFixa.PUOperacao;
                ordemRendaFixaContraParte.PUVolta = ordemRendaFixa.PUVolta;
                ordemRendaFixaContraParte.Valor = ordemRendaFixa.Valor;
                ordemRendaFixaContraParte.ValorVolta = ordemRendaFixa.ValorVolta;
                ordemRendaFixaContraParte.DataOperacao = ordemRendaFixa.DataOperacao;
                ordemRendaFixaContraParte.DataLiquidacao = ordemRendaFixa.DataLiquidacao;
                ordemRendaFixaContraParte.Status = ordemRendaFixa.Status;
                ordemRendaFixaContraParte.Observacao = ordemRendaFixa.Observacao;
                ordemRendaFixaContraParte.IdCustodia = ordemRendaFixa.IdCustodia;
                ordemRendaFixaContraParte.Save();

            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemRendaFixa - Operacao: Insert OrdemRendaFixa: " + ordemRendaFixa.IdOperacao + UtilitarioWeb.ToString(ordemRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            this.EnviaEmail(ordemRendaFixa);

            transaction.Complete();
            
        }

       
    }

    public byte? TipoOperacaoContraParte(byte? id)
    {
        switch (id)
        {
            case 1: return 2;
            case 2: return 1;
            case 3: return 4;
            case 4: return 3;
            case 6: return 1;
            case 10: return 11;
            case 11: return 10;
            case 12: return 13;
            case 13: return 12;
            default: return 0;
                break;
        }
    }

    private void EnviaEmail(OrdemRendaFixa ordemRendaFixa)
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        string from = settings.Smtp.From.ToString();

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuario(Context.User.Identity.Name))
        {
            return;
        }

        string to = "";
        if (!String.IsNullOrEmpty(usuario.Email))
        {
            to = usuario.Email;
        }

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(ordemRendaFixa.IdCliente.Value);

        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailRendaFixa))
        {
            if (!String.IsNullOrEmpty(to))
            {
                to = to + ",";
            }

            to = to + tabelaParametrosFrontCollection[0].EmailRendaFixa;
        }

        if (String.IsNullOrEmpty(to.Trim()))
            return;

        string body = "";
        body = "Data: " + ordemRendaFixa.DataOperacao.Value.ToShortDateString() + "\n\n";
        body += "Cliente: " + cliente.Apelido + "\n\n";        

        string valor = "";
        if (ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal)
        {
            valor = "Valor Bruto: " + String.Format("{0:C}", ordemRendaFixa.Valor.Value) + "\n\n";
        }
        else
        {
            valor = "Valor Bruto: " + String.Format("{0:C}", ordemRendaFixa.Valor.Value) + "\n\n"; //AJUSTAR FUTURAMENTE SE PRECISAR
        }
        body += valor;

        if (!String.IsNullOrEmpty(ordemRendaFixa.Observacao))
        {
            body += ordemRendaFixa.Observacao;
        }

        string subject = "";
        if (ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal)
        {
            subject = "Boleto de Aplicação (Renda Fixa)";
        }
        else
        {
            subject = "Boleto de Resgate (Renda Fixa)";
        }

        EmailUtil e = new EmailUtil();
        e.SendMail(body, subject, from, to);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") 
            {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;

                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }                
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.DescricaoCompleta);
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdTitulo == idTitulo);

            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.HasData)
            {
                string descricaoCompleta = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta).ToString();
                texto = descricaoCompleta.ToString();
            }
        }
        e.Result = texto;
    }

    protected void CallbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            e.Result = carteira.str.Apelido;
            
        }



    }

    protected void CallbackAgenteMercado_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idAgenteMercado = Convert.ToInt32(e.Parameter);
            AgenteMercado agenteMercado = new AgenteMercado();
            agenteMercado.LoadByPrimaryKey(idAgenteMercado);
            e.Result = agenteMercado.str.Apelido;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        
        ASPxSpinEdit btnCarteiraContraparte = gridCadastro.FindEditFormTemplateControl("btnCarteiraContraparte") as ASPxSpinEdit;
        ASPxSpinEdit btnAgenteContraParte = gridCadastro.FindEditFormTemplateControl("btnAgenteContraParte") as ASPxSpinEdit;
        ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;

        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropTipoNegociacao = gridCadastro.FindEditFormTemplateControl("dropTipoNegociacao") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;

        OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
        if (ordemRendaFixa.LoadByPrimaryKey(idOperacao))
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ordemRendaFixa.IdCliente = idCliente;

            if (dropMercadoCliente.SelectedItem.Value == "1" && btnAgenteContraParte.Value != null)
            {
                ordemRendaFixa.IdCarteiraContraParte = null;
                ordemRendaFixa.IdAgenteContraParte = Convert.ToInt32(btnAgenteContraParte.Number);
            }

            if (dropMercadoCliente.SelectedItem.Value == "2" && btnCarteiraContraparte.Value != null)
            {
                ordemRendaFixa.IdAgenteContraParte = null;
                ordemRendaFixa.IdCarteiraContraParte = Convert.ToInt32(btnCarteiraContraparte.Number);
            }

            if (aprovacao)
            {
                ordemRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
                ordemRendaFixa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
            }

            ordemRendaFixa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            if (textQuantidade.Text != "")
            {
                ordemRendaFixa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            }

            if (textPUOperacao.Text != "")
            {
                ordemRendaFixa.PUOperacao = Convert.ToDecimal(textPUOperacao.Text);
            }

            ordemRendaFixa.Valor = Convert.ToDecimal(textValor.Text);
            ordemRendaFixa.IdLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
            ordemRendaFixa.IdCustodia = Convert.ToByte(dropLocalCustodia.SelectedItem.Value);
            ordemRendaFixa.TipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);

            if (dropTrader.SelectedIndex > -1)
                ordemRendaFixa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                ordemRendaFixa.IdTrader = null;

            //Operação Compromissada
            if (ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                ordemRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
            {
                ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;

                if (textDataVolta.Text != "")
                {
                    ordemRendaFixa.DataVolta = Convert.ToDateTime(textDataVolta.Text);
                }

                if (textTaxaVolta.Text != "")
                {
                    ordemRendaFixa.TaxaVolta = Convert.ToDecimal(textTaxaVolta.Text);
                }

                if (textPUVolta.Text != "")
                {
                    ordemRendaFixa.PUVolta = Convert.ToDecimal(textPUVolta.Text);
                }

                if (textValorVolta.Text != "")
                {
                    ordemRendaFixa.ValorVolta = Convert.ToDecimal(textValorVolta.Text);
                }
            }
            else
            {
                ordemRendaFixa.DataVolta = null;
                ordemRendaFixa.TaxaVolta = null;
                ordemRendaFixa.PUVolta = null;
                ordemRendaFixa.ValorVolta = null;
            }

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                ordemRendaFixa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                ordemRendaFixa.IdCategoriaMovimentacao = null;
            }

            ordemRendaFixa.Observacao = textObservacao.Text;
            //

            ordemRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemRendaFixa - Operacao: Update OrdemRendaFixa: " + idOperacao + UtilitarioWeb.ToString(ordemRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
                if (ordemRendaFixa.LoadByPrimaryKey(idOperacao))
                {
                    int idCliente = ordemRendaFixa.IdCliente.Value;

                    //
                    OrdemRendaFixa ordemRendaFixaClone = (OrdemRendaFixa)Utilitario.Clone(ordemRendaFixa);
                    //

                    ordemRendaFixa.MarkAsDeleted();
                    ordemRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OrdemRendaFixa - Operacao: Delete OrdemRendaFixa: " + idOperacao + UtilitarioWeb.ToString(ordemRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnAprova" || e.Parameters == "btnCancela")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
                if (ordemRendaFixa.LoadByPrimaryKey(idOperacao))
                {
                    if (e.Parameters == "btnAprova")
                    {
                        ordemRendaFixa.Status = (byte)StatusOrdemRendaFixa.Aprovado;
                    }
                    else if (e.Parameters == "btnCancela")
                    {
                        ordemRendaFixa.Status = (byte)StatusOrdemRendaFixa.Cancelado;
                    }
                    ordemRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Aprovação de Ordem Renda Fixa: " + idOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            e.Properties["cpHiddenIdTitulo"] = hiddenIdTitulo.ClientID;
        }
    }

    protected void gridCadastro_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Observacao")
            if (e.Value != null)
                e.DisplayText = e.Value.ToString().Replace("\n", "<br />");
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TipoContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OrdemRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString()))
                e.Value = "1";
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte).ToString()))
                e.Value = "2";
        }

        if (e.Column.FieldName == "NomeContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString()))
                e.Value = e.GetListSourceFieldValue("AgenteMercadoContraparte");
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho).ToString()))
                e.Value = e.GetListSourceFieldValue("CarteiraContraparte");
        }


        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(OrdemRendaFixaMetadata.ColumnNames.Status));

            string statusDescricao = "";
            if (status == (byte)StatusOrdemRendaFixa.Digitado)
            {
                statusDescricao = "Digitado";
            }
            else if (status == (byte)StatusOrdemRendaFixa.Aprovado)
            {
                statusDescricao = "Aprovado";
            }
            else if (status == (byte)StatusOrdemRendaFixa.Processado)
            {
                statusDescricao = "Processado";
            }
            else if (status == (byte)StatusOrdemRendaFixa.Cancelado)
            {
                statusDescricao = "Cancelado";
            }

            e.Value = statusDescricao;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues["TipoNegociacao"] = "Negociação";
        e.NewValues["IdLiquidacao"] = "-";
        e.NewValues["IdCustodia"] = "-";
        e.NewValues["TaxaOperacao"] = "0";

        if (!aprovacao)
        {
            //Valores default do form
            TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
            tabelaParametrosFrontCollection.LoadAll();

            if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoRendaFixa))
            {
                e.NewValues["Observacao"] = tabelaParametrosFrontCollection[0].ObservacaoRendaFixa;
            }
        }
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void labelMercadoCliente_OnInit(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }


    protected void dropMercadoCliente_OnInit(object sender, EventArgs e)
    {
        ASPxComboBox dropMercadoCliente = (sender as ASPxComboBox);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            dropMercadoCliente.Visible = false;
        }
    }


    protected void btnAgenteContraParte_OnInit(object sender, EventArgs e)
    {
        ASPxSpinEdit btnAgenteContraParte = (sender as ASPxSpinEdit);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            btnAgenteContraParte.Visible = false;
        }
    }

    protected void btnCarteiraContraparte_OnInit(object sender, EventArgs e)
    {
        ASPxSpinEdit btnCarteiraContraparte = (sender as ASPxSpinEdit);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            btnCarteiraContraparte.Visible = false;
        }
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

}