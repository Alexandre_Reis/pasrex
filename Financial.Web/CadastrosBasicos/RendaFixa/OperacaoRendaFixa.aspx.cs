﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Util.Enums;
using DevExpress.XtraGrid;
using Newtonsoft.Json;
using Financial.Interfaces.Sinacor;
using Financial.RendaFixa.Controller;
using System.Transactions;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Processamento;
using System.IO;
using Financial.WebConfigConfiguration;

public partial class CadastrosBasicos_OperacaoRendaFixa : CadastroBasePage
{
    private const string DescricaoVirtual = "< case when ltrim(rtrim(isNull(T.[DescricaoCompleta],''))) = '' then T.[Descricao] else T.[DescricaoCompleta] end as DescricaoVirtual>";
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupTituloRF = true;
        this.HasPopupAgenteMercado = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
        //

        ASPxPopupControl popupCliente = this.FindControl("PopupCliente") as ASPxPopupControl;
        ASPxGridView gridPopupCliente = popupCliente.FindControl("PopupClienteGridCliente") as ASPxGridView;

        bool isClienteItau = ConfigurationManager.AppSettings["Cliente"] == "Itau";

        if (isClienteItau)
        {
            if (gridPopupCliente.Columns.Count < 3)
            {
                GridViewDataTextColumn BloqueadoColumn = new GridViewDataTextColumn();
                BloqueadoColumn.UnboundType = DevExpress.Data.UnboundColumnType.String;
                BloqueadoColumn.Visible = true;
                BloqueadoColumn.Caption = " ";
                BloqueadoColumn.CellStyle.ForeColor = Color.Red;
                BloqueadoColumn.FieldName = "IsBloqueadoSinacor";
                BloqueadoColumn.Width = Unit.Percentage(20);

                gridPopupCliente.Columns.Add(BloqueadoColumn);

            }

            gridPopupCliente.CustomUnboundColumnData += new ASPxGridViewColumnDataEventHandler(gridPopupCliente_CustomUnboundColumnData);
            gridPopupCliente.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(gridPopupCliente_CustomDataCallback);

            string calculoTriplo = "function CalculoTriplo(textPU, textQtde, textValor, fator)" +
"{    " +
"    if (textPU.GetValue() == null && textQtde.GetValue() != null && textValor.GetValue() != null && fator != 0)" +
"    {" +
"        textPU.SetValue(textValor.GetValue() / textQtde.GetValue() * fator);" +
"    }" +
"    else if (textQtde.GetValue() == null && textPU.GetValue() != null && textValor.GetValue() != null && fator != 0)" +
"    {" +
"        textQtde.SetValue((textValor.GetValue() * fator / textPU.GetValue()));" +
"    }" +
"    else if (textPU.GetValue() != null && textQtde.GetValue() != null && fator != 0)" +
"    {        " +
"        textValor.SetValue((textQtde.GetValue() * textPU.GetValue() / fator).toFixed(2));" +
"    }    " +
"}";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CalculoTriplo",
       "<script type=\"text/javascript\">" +
       calculoTriplo +
       "</script>");

        }

        this.dropTituloRendaFixa.GridView.Width = this.dropTituloRendaFixa.Width;

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao }));
    }

    protected void dropTipoOperacao_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;

        dropTipoOperacao.Items.Clear();
        foreach (int i in Enum.GetValues(typeof(TipoOperacaoTitulo)))
        {
            dropTipoOperacao.Items.Add(TipoOperacaoTituloDescricao.RetornaStringValue(i), i);
        }
    }

    protected void gridPopupCliente_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        //ASPxGridView gridView = sender as ASPxGridView;
        //int idCliente = Convert.ToInt32(gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente"));

        //UtilitarioWeb util = new UtilitarioWeb();
        //e.Result = util.IsClienteBloqueadoSinacor(idCliente) ? "Cliente bloquedo. Operação não efetuada" : idCliente.ToString();
    }

    protected void gridPopupCliente_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {

        GridView view = sender as GridView;
        if (e.Column.FieldName == "IsBloqueadoSinacor" && e.IsGetData)
        {
            int idCliente = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.IdCliente));
            UtilitarioWeb util = new UtilitarioWeb();
            e.Value = util.IsClienteBloqueadoSinacor(idCliente) ? "b" : "";
        }
    }


    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("PES");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        StringBuilder campos = new StringBuilder();
        campos.Append("< O.ValorIR + O.ValorIOF as ValorTributos, ");
        campos.Append(" (CASE WHEN O.ValorLiquido = 0 THEN O.Valor - O.ValorIR - O.ValorIOF ELSE O.ValorLiquido END) AS ValorLiq>");

        ClienteQuery clienteEspelhoQuery = new ClienteQuery("CliEsp");
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AG");
        OperacaoRendaFixaQuery operacaoEspelhoQuery = new OperacaoRendaFixaQuery("ESP");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        PosicaoRendaFixaQuery posQuery = new PosicaoRendaFixaQuery("PosQuery");

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, campos.ToString(), posQuery.IdPosicao.As("IdPosicaoResgatada"), clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta,
            carteiraQuery.Apelido.As("ApelidoCarteiraContraparte"), carteiraQuery.IdCarteira.As("IdCarteiraContraparte"), agenteMercadoQuery.Apelido.As("AgenteMercadoContraparte"),
            operacaoRendaFixaQuery.IdOperacaoEspelho, DescricaoVirtual, posQuery.IdPosicao);
        operacaoRendaFixaQuery.LeftJoin(operacaoEspelhoQuery).On(operacaoEspelhoQuery.IdOperacao == operacaoRendaFixaQuery.IdOperacaoEspelho);
        operacaoRendaFixaQuery.LeftJoin(clienteEspelhoQuery).On(operacaoEspelhoQuery.IdCliente == clienteEspelhoQuery.IdCliente);
        operacaoRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(operacaoRendaFixaQuery.IdAgenteContraParte == agenteMercadoQuery.IdAgente);
        operacaoRendaFixaQuery.LeftJoin(carteiraQuery).On(operacaoRendaFixaQuery.IdCarteiraContraparte == carteiraQuery.IdCarteira);
        operacaoRendaFixaQuery.LeftJoin(posQuery).On(operacaoRendaFixaQuery.IdOperacaoResgatada == posQuery.IdOperacao);

        //
        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente == Convert.ToInt32(btnEditCodigoClienteFiltro.Text));
        }

        if (!String.IsNullOrEmpty(dropTituloRendaFixa.Text))
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdTitulo == Convert.ToInt32(this.hiddenIdTituloFiltro.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        if (dropTipoCliente.SelectedIndex > -1)
            operacaoRendaFixaQuery.Where(clienteQuery.IdTipo.Equal(Convert.ToInt32(dropTipoCliente.SelectedItem.Value)));
 
        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();

        coll.Query.OrderBy(coll.Query.IdTipo.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRendaFixaFiltro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.Descricao,
                                    tituloRendaFixaQuery.DataEmissao,
                                    tituloRendaFixaQuery.DataVencimento,
                                    tituloRendaFixaQuery.DescricaoCompleta.Trim(),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapel"),
                                    DescricaoVirtual);
        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
        tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdTitulo.Ascending, tituloRendaFixaQuery.DataVencimento.Ascending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(tituloRendaFixaQuery);

        //
        e.Collection = coll;
    }

    public bool EhCompra(string tipoOperacao)
    {
        switch (tipoOperacao)
        {
            case "1": return true;
            case "3": return true;
            case "10": return true;
            case "20": return true;
            default: return false;
        }
    }

    public byte? TipoOperacaoContraParte(byte? id)
    {
        switch (id)
        {
            case 1: return 2;
            case 2: return 1;
            case 3: return 4;
            case 4: return 3;
            case 6: return 1;
            case 10: return 11;
            case 11: return 10;
            case 12: return 13;
            case 13: return 12;
            case 20: return 21;
            case 21: return 20;
            default: return 0;
                break;
        }
    }



    public int? ClienteSelecionado()
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;
        ASPxSpinEdit btnCarteiraContraparte = gridCadastro.FindEditFormTemplateControl("btnCarteiraContraparte") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;

        if (dropMercadoCliente.SelectedItem != null && dropTipoOperacao.SelectedItem != null)
        {
            if (dropMercadoCliente.SelectedItem.Value == "2" &&
                EhCompra(dropTipoOperacao.SelectedItem.Value.ToString()) &&
                btnCarteiraContraparte.Text != "")
            {
                return Convert.ToInt32(btnCarteiraContraparte.Text);
            }


        }

        return null;

    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        e.Collection = new TituloRendaFixaCollection();
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (gridCadastro.IsEditing)
        {

            int? idCliente = ClienteSelecionado();
            if (!idCliente.HasValue && btnEditCodigoCliente.Text != "")
            {
                idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            }

            Cliente cliente = new Cliente();
            if (idCliente.HasValue && cliente.LoadByPrimaryKey(idCliente.Value))
            {
                DateTime dataDia = cliente.DataDia.Value;

                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
                tituloRendaFixaQuery.Select(tituloRendaFixaQuery, papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
                tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                tituloRendaFixaQuery.Where(tituloRendaFixaQuery.DataVencimento.GreaterThanOrEqual(dataDia));
                tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.DataVencimento.Ascending);

                TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
                coll.Load(tituloRendaFixaQuery);

                e.Collection = coll;
            }
        }

    }

    protected void EsDSPosicao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        e.Collection = new PosicaoRendaFixaCollection();

        if (gridCadastro.IsEditing)
        {
            int? idCliente = ClienteSelecionado();
            if (!idCliente.HasValue)
            {
                idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            }


            if (idCliente.HasValue)
            {

                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery, tituloRendaFixaQuery.DescricaoCompleta, papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente));

                if (hiddenIdTitulo.Text != "")
                {
                    int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                }

                posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.DataOperacao.Ascending);

                PosicaoRendaFixaCollection coll = new PosicaoRendaFixaCollection();
                coll.Load(posicaoRendaFixaQuery);

                e.Collection = coll;
            }
        }
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.Select(coll.Query.IdIndice, coll.Query.Descricao);
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoCorretora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        //status == (byte)StatusCliente.Divulgado
        CarteiraCollection coll = new CarteiraCollection();
        CarteiraQuery carteiraQuery = new CarteiraQuery("ca");
        ClienteQuery clienteQuery = new ClienteQuery("cli");
        carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        carteiraQuery.Where(clienteQuery.Status != (byte)StatusCliente.Divulgado);
        coll.Load(carteiraQuery);
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string RetornaCalculoCompromisso()
    {
        string textRetorno = "";
        if (gridCadastro.IsEditing)
        {
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
            ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
            if (textDataVolta.Text != "" && textTaxaVolta.Text != "" &&
                textDataOperacao.Text != "" && textPUOperacao.Text != "" &&
                textQuantidade.Text != "")
            {
                DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
                decimal puOperacao = Convert.ToDecimal(textPUOperacao.Text);
                DateTime dataVolta = Convert.ToDateTime(textDataVolta.Text);
                decimal taxaVolta = Convert.ToDecimal(textTaxaVolta.Text);
                decimal quantidade = Convert.ToDecimal(textQuantidade.Text);

                int dias = Calendario.NumeroDias(dataOperacao, dataVolta, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                if (dias < 0)
                {
                    return "";
                }

                decimal fator = CalculoFinanceiro.CalculaFatorPreExponencial(dias, taxaVolta, Financial.Util.Enums.BaseCalculo.Base252);

                decimal puVolta = Utilitario.Truncate(puOperacao * fator, 8);
                decimal valorVolta = Utilitario.Truncate(quantidade * puVolta, 2);

                textRetorno = puVolta.ToString().Replace(",", ".") + "|" + valorVolta.ToString().Replace(",", ".");
            }
        }
        return textRetorno;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = RetornaCalculoCompromisso();

        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackTaxa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;

        if (textData.Text == "")
        {
            e.Result = "Data deve ser informada para o cálculo da taxa.";
            return;
        }

        if (hiddenIdTitulo.Text == "")
        {
            e.Result = "Título deve ser informado para o cálculo da taxa.";
            return;
        }

        if (textPUOperacao.Text == "")
        {
            e.Result = "PU Operação deve ser informado para o cálculo da taxa.";
            return;
        }

        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        tituloRendaFixa.LoadByPrimaryKey(idTitulo);
        int idPapel = tituloRendaFixa.IdPapel.Value;
        bool parametrosAvancados = tituloRendaFixa.ParametrosAvancados.Equals("S");

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
        papelRendaFixa.LoadByPrimaryKey(idPapel);
        byte contagemDias = papelRendaFixa.ContagemDias.Value;

        decimal TIR = 0;
        if (parametrosAvancados)
        {
            CalculoRendaFixa c = new CalculoRendaFixa();
            double taxaTitulo = (double)(tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0);
            int enumClasse = papelRendaFixa.Classe.Value;
            DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
            DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;

            int periodicidade = 0;
            if (tituloRendaFixa.Periodicidade.HasValue)
                periodicidade = tituloRendaFixa.Periodicidade.Value;

            Hashtable hsMemoria = new Hashtable();
            TIR = (c.CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, Convert.ToDecimal(textPUOperacao.Text), null, null, 0, 0, 0, false, ref hsMemoria)).Taxa;
        }
        else
        {
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.Valor.Sum(), agendaRendaFixaCollection.Query.DataEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(data));
            agendaRendaFixaCollection.Query.GroupBy(agendaRendaFixaCollection.Query.DataEvento);
            agendaRendaFixaCollection.Query.Load();

            List<CalculoFinanceiro.CashFlow> cashFlowList = new List<CalculoFinanceiro.CashFlow>();
            cashFlowList.Add(new CalculoFinanceiro.CashFlow(Convert.ToDouble(textPUOperacao.Text) * (double)-1M, data));
            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                cashFlowList.Add(new CalculoFinanceiro.CashFlow((double)agendaRendaFixa.Valor.Value, agendaRendaFixa.DataEvento.Value));
            }

            TIR = Utilitario.Truncate((decimal)(CalculoFinanceiro.CalculaTIR(cashFlowList, data, (ContagemDias)contagemDias) * (double)100M), 16);
        }


        e.Result = TIR.ToString().Replace(",", ".");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackPU_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;

        if (textData.Text == "")
        {
            e.Result = "Data deve ser informada para o cálculo da taxa.";
            return;
        }

        if (hiddenIdTitulo.Text == "")
        {
            e.Result = "Título deve ser informado para o cálculo da taxa.";
            return;
        }

        if (textTaxaOperacao.Text == "")
        {
            e.Result = "Taxa Operação deve ser informada para o cálculo do PU.";
            return;
        }

        if (btnEditCodigoCliente.Text == "")
        {
            e.Result = "Cliente deve ser informado para o cálculo do PU.";
            return;
        }

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal taxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);

        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        tituloRendaFixa.LoadByPrimaryKey(idTitulo);
        int idPapel = tituloRendaFixa.IdPapel.Value;
        bool parametrosAvancados = tituloRendaFixa.ParametrosAvancados.Equals("S");

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
        papelRendaFixa.LoadByPrimaryKey(idPapel);

        DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
        DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;

        decimal percentual = 0;
        if (tituloRendaFixa.IdIndice.HasValue && !tituloRendaFixa.Percentual.HasValue)
        {
            percentual = 100;
        }
        else
        {
            percentual = tituloRendaFixa.Percentual.Value;
        }

        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
        posicaoRendaFixa.DataOperacao = data;
        posicaoRendaFixa.DataVencimento = dataVencimento;
        posicaoRendaFixa.IdCliente = idCliente;
        posicaoRendaFixa.IdTitulo = idTitulo;
        posicaoRendaFixa.TaxaOperacao = taxaOperacao;

        Indice indice = new Indice();

        decimal puCalculado = 0;
        CalculoRendaFixa c = new CalculoRendaFixa();
        if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.PosFixado_FluxoCorrigido)
        {
            indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = c.CalculaPUFluxoCorrigido(data, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice);
            puCalculado = posicaoAtualizada.PuAtualizado;
        }
        else if (parametrosAvancados)
        {
            List<double> lstTaxas = new List<double>();
            lstTaxas.Add((double)taxaOperacao);

            Hashtable hsMemoria = new Hashtable();
            puCalculado = (c.CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, null, lstTaxas, null, 0, 0, 0, false, ref hsMemoria)).PuAtualizado;
        }
        else
        {
            decimal puAtualizado = 0;

            decimal fatorCorrecao = 1;
            #region Cálculo do fator de correção desde a emissão ou último pagto de correção
            DateTime dataBase = tituloRendaFixa.DataEmissao.Value;
            AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
            agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo == tituloRendaFixa.IdTitulo.Value &&
                                                        (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) ||
                                                        (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &&
                                                        (agendaRendaFixaCollectionCorrecao.Query.Taxa == 0 || agendaRendaFixaCollectionCorrecao.Query.Taxa == 100))));

            agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.DataPagamento.LessThanOrEqual(data));

            agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
            agendaRendaFixaCollectionCorrecao.Query.Load();

            if (agendaRendaFixaCollectionCorrecao.Count > 0)
            {
                dataBase = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
            }

            if (tituloRendaFixa.IdIndice.HasValue && data != dataBase)
            {
                indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

                if (indice.Tipo.Value == (byte)TipoIndice.Decimal && indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal)
                {
                    int diaAniversario = dataVencimento.Day;
                    List<DateTime> listaDatasIndice = new List<DateTime>();
                    if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Corridos || papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                    {
                        listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, data, diaAniversario);
                        fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual, diaAniversario, null, null);
                    }
                    else
                    {
                        listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, data, diaAniversario,
                                            (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual, diaAniversario, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                }
                else if (indice.Tipo.Value == (byte)TipoIndice.Decimal)
                {
                    fatorCorrecao = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataBase, data, indice.IdIndice.Value, tituloRendaFixa.Percentual.Value);
                }
                else
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                    fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataAnterior, indice, percentual, false);
                }
            }
            #endregion

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.TipoEvento,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.Taxa);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(data),
                                                  agendaRendaFixaCollection.Query.TipoEvento.NotEqual((byte)TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            bool existeFluxoDesconto = agendaRendaFixaCollection.Count > 0;

            if (existeFluxoDesconto)
            {
                PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = c.CalculaPUFluxoDesconto(data, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, taxaOperacao);
                puCalculado = posicaoAtualizada.PuAtualizado;
            }
            else if (!tituloRendaFixa.IdIndice.HasValue)
            {
                #region PRÉ-FIXADO SEM FLUXO
                int numeroDiasDesconto = 0;
                if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                {
                    numeroDiasDesconto = Calendario.NumeroDias(data, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                {
                    numeroDiasDesconto = Calendario.NumeroDias360(data, dataVencimento);
                }
                else
                {
                    numeroDiasDesconto = Calendario.NumeroDias(data, dataVencimento);
                }

                decimal fator = (decimal)Math.Pow((double)(1 + taxaOperacao / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                puCalculado = tituloRendaFixa.PUNominal.Value / fator;
                #endregion
            }
            else
            {
                puCalculado = tituloRendaFixa.PUNominal.Value * fatorCorrecao;
            }
        }

        puCalculado = Math.Round(puCalculado, papelRendaFixa.CasasDecimaisPU.Value);

        e.Result = puCalculado.ToString().Replace(",", ".");
    }



    protected void CallbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            e.Result = carteira.str.Apelido;
        }
    }

    protected void CallbackAgenteMercado_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idAgenteMercado = Convert.ToInt32(e.Parameter);
            AgenteMercado agenteMercado = new AgenteMercado();
            agenteMercado.LoadByPrimaryKey(idAgenteMercado);
            e.Result = agenteMercado.str.Apelido;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
        {
            ASPxDateEdit textdtRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            textdtRegistro.ClientEnabled = true;
        }

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            dropFormaLiquidacao.SelectedIndex = dropFormaLiquidacao.SelectedIndex < 0 ? 0 : dropFormaLiquidacao.SelectedIndex ;
        }

        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        if (!multiConta)
        {
            labelConta.Visible = false;
            dropConta.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            CheckBox chkOperacaoTermo = gridCadastro.FindEditFormTemplateControl("chkOperacaoTermo") as CheckBox;
            string operacaoTermo = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "OperacaoTermo").ToString();

            if (operacaoTermo == "S")
            {
                chkOperacaoTermo.Checked = true;
            }
            else
            {
                chkOperacaoTermo.Checked = false;
            }

            CheckBox chkIsIPO = gridCadastro.FindEditFormTemplateControl("chkIsIPO") as CheckBox;
            string IsIPO = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IsIPO").ToString();

            if (IsIPO == "S")
            {
                chkIsIPO.Checked = true;
            }
            else
            {
                chkIsIPO.Checked = false;
            }

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataRegistro"));
            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "TipoOperacao"));

            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
            {
                textQuantidade.ClientEnabled = false;
                textValor.ClientEnabled = false;
                textQuantidade.Text = "";
                textValor.Text = "";
            }
            else if (tipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda || tipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
            {
                ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;
                ASPxComboBox dropIndiceVolta = gridCadastro.FindEditFormTemplateControl("dropIndiceVolta") as ASPxComboBox;

                textDataVolta.ClientEnabled = true;
                textTaxaVolta.ClientEnabled = true;
                textPUVolta.ClientEnabled = true;
                textValorVolta.ClientEnabled = true;
                dropIndiceVolta.ClientEnabled = true;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                //else if (DateTime.Compare(dataDia, data) > 0)
                //{
                //    travaPainel = true;
                //    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                //}
            }

            string textoPosicao = "";
             if (!Convert.IsDBNull(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacaoResgatada")))
            {
                int idOperacaoResgatada = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacaoResgatada"));

                PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
                PosicaoRendaFixaAberturaQuery posicaoRendaFixaAberturaQuery = new PosicaoRendaFixaAberturaQuery("P");
                posicaoRendaFixaAberturaQuery.Where(posicaoRendaFixaAberturaQuery.IdOperacao.Equal(idOperacaoResgatada) && posicaoRendaFixaAberturaQuery.DataHistorico.Equal(data));
                
                if (posicaoRendaFixaAbertura.Load(posicaoRendaFixaAberturaQuery))
                {
                    textoPosicao = "Id: " + posicaoRendaFixaAbertura.IdPosicao.Value.ToString() + "  -  Oper.: " + posicaoRendaFixaAbertura.DataOperacao.Value.ToShortDateString() +
                            "  -  Qtde: " + Math.Round(posicaoRendaFixaAbertura.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(posicaoRendaFixaAbertura.PUOperacao.Value, 8).ToString();
                }

                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdOperacao == idOperacaoResgatada);
                if (posicaoRendaFixa.Load(posicaoRendaFixaQuery))
                {
                    textoPosicao = "Id: " + posicaoRendaFixa.IdPosicao.Value.ToString() + "  -  Oper.: " + posicaoRendaFixa.DataOperacao.Value.ToShortDateString() +
                            "  -  Qtde: " + Math.Round(posicaoRendaFixa.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(posicaoRendaFixa.PUOperacao.Value, 8).ToString();
                }

                ASPxButtonEdit btnEditPosicao = gridCadastro.FindEditFormTemplateControl("btnEditPosicao") as ASPxButtonEdit;
                btnEditPosicao.Text = textoPosicao;
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;

            LinkButton btnObservacao = gridCadastro.FindEditFormTemplateControl("btnObservacao") as LinkButton;
            btnObservacao.Visible = true;
        }
    }

    private decimal RetornaQuantidadeDisponivel(int idTitulo, int idCliente, DateTime dataOperacao, int? idOperacaoIgnorar)
    {
        //Verificar se existe quantidade suficiente para bloquear
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        tituloRendaFixa.LoadByPrimaryKey(idTitulo);
        return tituloRendaFixa.RetornaQuantidadeDisponivel(idCliente, dataOperacao, idOperacaoIgnorar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!gridCadastro.IsEditing)
        { //Testa deleção
            #region Deleção

            List<Int32> idOperacaoLista = gridCadastro.GetSelectedFieldValues("IdOperacao").ConvertAll<int>(
                new Converter<Object, Int32>(
                    delegate(Object obj)
                    {
                        return Convert.ToInt32(obj);
                    }
                )
            );

            OperacaoRendaFixaCollection operacaoCollection = new OperacaoRendaFixaCollection();
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("Op");
            OperacaoRendaFixaQuery operacaoEspelhoQuery = new OperacaoRendaFixaQuery("OpEsp");
            ClienteQuery clienteQuery = new ClienteQuery("Cli");

            operacaoRendaFixaQuery.Select(
                clienteQuery.Apelido.As("Apelido"),
                clienteQuery.IdCliente.As("IdCliente"),
                clienteQuery.Status.As("Status"),
                clienteQuery.DataDia.As("DataDia"),
                operacaoRendaFixaQuery.DataRegistro.As("DataRegistro"),
                operacaoEspelhoQuery.IdCliente.As("IdClienteEspelho"),
                operacaoEspelhoQuery.DataRegistro.As("DataRegistroEspelho")
            );


            operacaoRendaFixaQuery.es.Distinct = true;

            operacaoRendaFixaQuery
                .LeftJoin(operacaoEspelhoQuery)
                .On(operacaoRendaFixaQuery.IdOperacaoEspelho == operacaoEspelhoQuery.IdOperacao);

            operacaoRendaFixaQuery
                .LeftJoin(clienteQuery).On(
                    operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente |
                    operacaoEspelhoQuery.IdCliente == clienteQuery.IdCliente
                 );

            operacaoRendaFixaQuery.Where(
                operacaoRendaFixaQuery.IdOperacao.In(idOperacaoLista) && (
                    clienteQuery.DataDia > operacaoRendaFixaQuery.DataRegistro |
                    clienteQuery.DataDia > operacaoEspelhoQuery.DataRegistro |
                    clienteQuery.Status == (byte)StatusCliente.Divulgado
                )
            );

            operacaoCollection.Load(operacaoRendaFixaQuery);

            if (operacaoCollection.Count > 0)
            {
                StringBuilder s = new StringBuilder();
                foreach (OperacaoRendaFixa operacao in operacaoCollection)
                {
                    if (operacao.GetColumn("Status").ToString() == StatusCliente.Divulgado.ToString())
                    {
                        s.AppendLine("- Cliente " + operacao.GetColumn("Apelido") + " está fechado");
                    }
                    else
                    {
                        if (operacao.GetColumn("IdCliente").ToString() == operacao.GetColumn("IdClienteEspelho"))
                        {
                            s.AppendLine("- Lançamento espelho com data " + operacao.GetColumn("DataRegistroEspelho").ToString() +
                                " anterior à data do cliente " + operacao.GetColumn("Apelido") + " (" + operacao.GetColumn("IdCliente") + ")");
                        }
                        else
                        {
                            s.AppendLine("- Lançamento com data " + operacao.GetColumn("DataRegistro").ToString() +
                                " anterior à data do cliente " + operacao.GetColumn("Apelido") + " (" + operacao.GetColumn("IdCliente") + ")");
                        }
                    }
                }


                e.Result = s.ToString();
                return;
            }



            //List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            //List<object> keyData = gridCadastro.GetSelectedFieldValues("DataRegistro");
            //for (int i = 0; i < keyValuesId.Count; i++)
            //{
            //    int idCliente = Convert.ToInt32(keyValuesId[i]);
            //    DateTime data = Convert.ToDateTime(keyData[i]);

            //    Cliente cliente = new Cliente();
            //    if (cliente.LoadByPrimaryKey(idCliente))
            //    {
            //        DateTime dataDia = cliente.DataDia.Value;
            //        byte status = cliente.Status.Value;
            //        string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
            //        if (status == (byte)StatusCliente.Divulgado)
            //        {
            //            e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
            //            return;
            //        }
            //        else if (DateTime.Compare(dataDia, data) > 0)
            //        {
            //            e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
            //            return;
            //        }
            //    }
            //}
            #endregion
        }
        else
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxButtonEdit btnEditTituloRF = gridCadastro.FindEditFormTemplateControl("btnEditTituloRF") as ASPxButtonEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            TextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as TextBox;
            TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;
            TextBox hiddenIdOperacaoResgatada = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacaoResgatada") as TextBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            bool compraFinal = false;
            bool vendaFinal = false;
            bool vendaTotal = false;

            TextBox hiddenDataModificacao = gridCadastro.FindEditFormTemplateControl("hiddenDataModificacao") as TextBox;



            if (dropTipoOperacao.SelectedIndex != -1)
            {
                compraFinal = dropTipoOperacao.SelectedIndex == 0;
                vendaFinal = Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.VendaFinal || Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.ExercicioOpcao;
                vendaTotal = Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.VendaTotal;
            }

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditTituloRF);
            controles.Add(textDataLiquidacao);
            controles.Add(textDataOperacao);
            controles.Add(textDataRegistro);
            controles.Add(dropTipoOperacao);

            if (!vendaTotal)
            {
                controles.Add(textQuantidade);
                controles.Add(textValor);
            }

            controles.Add(textPUOperacao);

            if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia == (int)TipoControleCustodia.ControlaClearing)
            {
                ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;
                ASPxSpinEdit btnCarteiraContraparte = gridCadastro.FindEditFormTemplateControl("btnCarteiraContraparte") as ASPxSpinEdit;
                ASPxSpinEdit btnAgenteContraParte = gridCadastro.FindEditFormTemplateControl("btnAgenteContraParte") as ASPxSpinEdit;
                dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;

                controles.Add(dropMercadoCliente);

                if (dropMercadoCliente.SelectedItem != null)
                {
                    if (dropMercadoCliente.SelectedItem.Value == "1" && dropFormaLiquidacao.SelectedItem.Value != "3" )                        
                    {
                        controles.Add(btnAgenteContraParte);
                    }

                    if (dropMercadoCliente.SelectedItem.Value == "2" )
                    {

                        if (dropFormaLiquidacao.SelectedItem.Value != "3")
                        {
                            controles.Add(btnCarteiraContraparte);
                        }
                        

                        if (btnCarteiraContraparte.Number == btnEditCodigoCliente.Number)
                        {
                            e.Result = "Não é possivel fazer operações com o proprio cliente como contraparte.";
                            return;
                        }
                    }
                }

            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            int? idClienteContraparte = ClienteSelecionado();
            int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);

            DateTime dataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);


            if ((dataRegistro != dataOperacao) && (vendaFinal || vendaTotal))
            {
                e.Result = "Para operações de venda, a data de registro precisa ser igual à data de operação";
                return;
            }

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(idTitulo);

            if (tituloRendaFixa.IdMoeda.Value == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(dataOperacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(dataOperacao, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
            }

            if (dataOperacao > Convert.ToDateTime(textDataLiquidacao.Text))
            {
                e.Result = "Data da operação não pode ser maior que a data da liquidação.";
                return;
            }

            //No caso de opções Europeias, a movimentação de exercício será possível apenas na data de vencimento do título. 
            //    No caso de opções americanas, o exercício pode ocorrer em qualquer data entre a Data Início Exercício e a Data de Vencimento.

            if (tituloRendaFixa.OpcaoEmbutida.Equals("S") && Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.ExercicioOpcao)
            {
                if (Convert.ToInt32(tituloRendaFixa.TipoExercicio.Value) == (int)TipoExercicio.Americana)
                {
                    if (dataOperacao < tituloRendaFixa.DataInicioExercicio.Value && dataOperacao > tituloRendaFixa.DataVencimentoOpcao.Value)
                    {
                        e.Result = "Data da Operação fora do período de exercicio da opção (Americana)!!";
                        return;
                    }
                }
                else if (Convert.ToInt32(tituloRendaFixa.TipoExercicio.Value) == (int)TipoExercicio.Europeia)
                {
                    if (dataOperacao != tituloRendaFixa.DataVencimentoOpcao.Value)
                    {
                        e.Result = "Data da Operação fora do período de exercicio da opção (Européia)!!";
                        return;
                    }
                }
            }

            decimal qtde = 0;
            decimal pu = 0;
            decimal valor = 0;

            if (!string.IsNullOrEmpty(textQuantidade.Text))
                qtde = Convert.ToDecimal(textQuantidade.Text);

            if (!string.IsNullOrEmpty(textPUOperacao.Text))
                pu = Convert.ToDecimal(textPUOperacao.Text);

            if (!string.IsNullOrEmpty(textValor.Text))
                valor = Convert.ToDecimal(textValor.Text);

            if (Math.Round(pu * qtde, 2) != valor)
            {
                e.Result = "Pu * qtde não é igual ao financeiro (valor), também será necessário recalcular a Taxa!";
                return;
            }

            Cliente cliente = new Cliente();
            DateTime dataDia = new DateTime();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                dataDia = cliente.DataDia.Value;
                DateTime dataInicio = cliente.DataInicio.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataInicio > dataRegistro)
                {
                    e.Result = "Data informada não pode ser anterior a data de início do Cliente(" + dataInicio.ToString("dd/MM/yyyy") + ").";
                    return;
                }
                //else if (dataDia > dataRegistro)
                //{
                //    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                //    return;
                //}                
            }


            Cliente clienteContraparte = new Cliente();
            if (idClienteContraparte.HasValue && clienteContraparte.LoadByPrimaryKey(idClienteContraparte.Value))
            {
                DateTime dataDiaClienteContraparte = clienteContraparte.DataDia.Value;
                byte status = clienteContraparte.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Cliente contraparte está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataDiaClienteContraparte > dataRegistro)
                {
                    e.Result = "Data informada anterior à data do cliente contraparte! Operação não pode ser realizada.";
                    return;
                }
            }

            if (dataOperacao < tituloRendaFixa.DataEmissao.Value)
            {
                e.Result = "Título tem data de emissão posterior à data de operação. Operação não pode ser realizada.";
                return;
            }

            decimal quantidade = Convert.ToDecimal(textQuantidade.Value);

            //int? idPosicao = null;
            int? idOperacaoResgatada =null;
            if (hiddenIdPosicao.Text != "")
            {   
                int idPosicaoResgatada = Convert.ToInt32(hiddenIdPosicao.Text);

                PosicaoRendaFixa pos = new PosicaoRendaFixa();
                idOperacaoResgatada = pos.BuscaIdOperacaoResgatada(idPosicaoResgatada);
            }

            int? idOperacao = null;
            if (hiddenIdOperacao.Text != "")
            {
                idOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
            }



            OperacaoRendaFixa operacao = new OperacaoRendaFixa();
            if (idOperacao.HasValue) operacao.LoadByPrimaryKey(idOperacao.Value);

            #region validação quantidade venda
            
            
            if (vendaFinal && (!idOperacao.HasValue || operacao.IdTitulo != idTitulo || operacao.IdOperacaoResgatada != idOperacaoResgatada || operacao.Quantidade < quantidade))
            {
                DateTime dataReferencia = dataDia < dataOperacao ? dataDia : dataOperacao;
                decimal quantidadeDisponivel = RetornaQuantidadeDisponivel(idTitulo, idCliente, dataReferencia, idOperacao);

                if (quantidade > quantidadeDisponivel)
                {
                    e.Result = "A quantidade excede a quantidade disponível.";
                    return;
                }
            }
            else if (vendaTotal)
            {
                DateTime dataReferencia = dataDia < dataOperacao ? dataDia : dataOperacao;
                //Se for venda total não pode haver nenhuma posicao bloqueada! 
                decimal quantidadeBloqueada = tituloRendaFixa.RetornaQuantidadeBloqueada(idCliente, dataReferencia);

                if (quantidadeBloqueada > 0)
                {
                    e.Result = "A venda total não pode ser lançada pois existem posições bloqueadas deste título";
                    return;
                }
            }


            
            #endregion

            #region validação quantidade contraparte compra


            if (EhCompra(dropTipoOperacao.SelectedItem.Value.ToString()) &&
                idClienteContraparte.HasValue &&
                idClienteContraparte.Value != idCliente &&
                (!idOperacao.HasValue || operacao.IdTitulo != idTitulo || operacao.IdOperacaoResgatada != idOperacaoResgatada || operacao.Quantidade > quantidade))
            {

                Carteira carteira = new Carteira();
                if (carteira.LoadByPrimaryKey(idClienteContraparte.Value) && carteira.ProcAutomatico == "S")
                {
                    PosicaoRendaFixaCollection coll = new PosicaoRendaFixaCollection();
                    PosicaoRendaFixaQuery query = new PosicaoRendaFixaQuery();

                    query.Where(query.IdCliente == idClienteContraparte && query.IdTitulo == idTitulo);


                    if (idOperacaoResgatada.HasValue)
                        query.Where(query.IdOperacao == idOperacaoResgatada.Value);

                    coll.Load(query);

                    decimal quantidadeDisponivel = 0;
                    foreach (PosicaoRendaFixa pos in coll)
                    {
                        if (pos.Quantidade.HasValue)
                            quantidadeDisponivel += pos.Quantidade.Value;
                    }

                    if (quantidade > quantidadeDisponivel)
                    {
                        e.Result = "A quantidade excede a quantidade disponível.";
                        return;
                    }
                }
                else
                {
                decimal quantidadeDisponivel = RetornaQuantidadeDisponivel(idTitulo, idClienteContraparte.Value, dataOperacao, null);
                if ((quantidade > quantidadeDisponivel))
                {
                    e.Result = "A quantidade excede a quantidade disponivel do cliente contraparte.";
                }
            }
            }

            #endregion

            int? idPosicao = null;
            if (hiddenIdPosicao.Text != "")
            {
                idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
            }

            if (!string.IsNullOrEmpty(hiddenIdOperacaoResgatada.Text))
            {
                idOperacaoResgatada = Convert.ToInt32(hiddenIdOperacaoResgatada.Text);
            }

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            if (idPosicao.HasValue || idOperacaoResgatada.HasValue)
            {
                posicaoRendaFixa.Query.Select(posicaoRendaFixa.Query.IdPosicao, posicaoRendaFixa.Query.TipoOperacao);
                posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdPosicao.Equal(idPosicao.GetValueOrDefault(0)));
                if (!posicaoRendaFixa.Query.Load())
                {
                    posicaoRendaFixa = new PosicaoRendaFixa();
                    posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdOperacao.Equal(idOperacaoResgatada.Value));
                    posicaoRendaFixa.Query.Load();
                }
            }

            if (Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.CompraRevenda || Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.VendaRecompra) //Compromissada
            {
                ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;
                ASPxComboBox dropIndiceVolta = gridCadastro.FindEditFormTemplateControl("dropIndiceVolta") as ASPxComboBox;

                DateTime dataVolta = new DateTime();

                if (textDataVolta.Text == "")
                {
                    e.Result = "Data da volta precisa ser preenchida em operações compromisadas.";
                    return;
                }
                else
                {
                    dataVolta = Convert.ToDateTime(textDataVolta.Text);
                }

                if (dataVolta <= dataOperacao)
                {
                    e.Result = "Data da volta precisa ser maior que a data da operação.";
                    return;
                }

                if (dataVolta <= dataRegistro)
                {
                    e.Result = "Data da volta precisa ser maior que a data de registro da operação.";
                    return;
                }

                if (textTaxaVolta.Text == "")
                {
                    e.Result = "Taxa da volta precisa ser preenchida em operações compromisadas.";
                    return;
                }

                if (dropIndiceVolta.SelectedIndex == -1 && (textPUVolta.Text == "" || textValorVolta.Text == ""))
                {
                    e.Result = "Campos PU Volta e Valor Volta precisam ser preenchidos em operações compromisadas pré-fixadas.";
                    return;
                }

                if (dropIndiceVolta.SelectedIndex != -1 && (textPUVolta.Text != "" || textValorVolta.Text != ""))
                {
                    e.Result = "Escolha apenas uma opção (indice ou taxa e valor volta) para operações compromissadas.";
                    return;
                }

                if (!Calendario.IsDiaUtil(dataVolta, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da volta não é dia útil.";
                    return;
                }
            }
            else if (dropTipoOperacao.SelectedIndex == 11) //Revenda
            {
                if (idOperacaoResgatada == null)
                {
                    e.Result = "Para antecipação de revenda é necessário especificar a posição de compromisso para resgate.";
                    return;
                }
                else
                {
                    if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraFinal)
                    {
                        e.Result = "Para antecipação de revenda não pode ser escolhida uma posição final para resgate.";
                        return;
                    }
                }
            }
            else if (Convert.ToInt32(dropTipoOperacao.SelectedItem.Value) == (int)TipoOperacaoTitulo.VendaFinal) //Venda Final
            {
                if (idOperacaoResgatada.HasValue && posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    e.Result = "Para venda final não pode ser escolhida uma posição compromissada para resgate.";
                    return;
                }
            }

            #region Verifica se a Categoria foi alterada alguma vez
            if (idOperacao.HasValue)
            {
                ASPxComboBox dropTipoNegociacao = gridCadastro.FindEditFormTemplateControl("dropTipoNegociacao") as ASPxComboBox;
                VigenciaCategoriaCollection categoriaColl = new VigenciaCategoriaCollection();
                OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

                int tipoNegociacao = Convert.ToInt32(dropTipoNegociacao.SelectedItem.Value);

                operacaoRendaFixa.LoadByPrimaryKey(idOperacao.Value);
                categoriaColl.Query.Where(categoriaColl.Query.IdOperacao.Equal(idOperacao));
                if (categoriaColl.Query.Load())
                {
                    if (tipoNegociacao != operacaoRendaFixa.TipoNegociacao.Value)
                    {
                        e.Result = "Tipo de Negociação não pode ser alterado na operação, o mesmo já foi alterado no Cadastro de Vigência!";
                        return;
                    }
                }
            }
            #endregion

            #region TipoOperacao
            List<int> lstApenasParaFundo = new List<int>();
            lstApenasParaFundo.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoCota);
            lstApenasParaFundo.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoQtde);
            lstApenasParaFundo.Add((int)TipoOperacaoTitulo.RetiradaAtivoImpactoCota);
            lstApenasParaFundo.Add((int)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde);

            if (cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo && lstApenasParaFundo.Contains(Convert.ToInt32(dropTipoOperacao.SelectedItem.Value)))
            {
                UtilitarioWeb util = new UtilitarioWeb();
                string descricaoBloqueio = util.VerificaBloqueioOperacaoSinacor(idCliente, dataOperacao,
                    tituloRendaFixa.CodigoCBLC, dropTipoOperacao.SelectedIndex);

                if (!String.IsNullOrEmpty(descricaoBloqueio))
                {
                    e.Result = descricaoBloqueio;
                    return;
                }

                if (idClienteContraparte.HasValue)
                {
                    util = new UtilitarioWeb();
                    descricaoBloqueio = util.VerificaBloqueioOperacaoSinacor(idClienteContraparte.Value, dataOperacao,
                        tituloRendaFixa.CodigoCBLC, dropTipoOperacao.SelectedIndex);

                    if (!String.IsNullOrEmpty(descricaoBloqueio))
                    {
                        e.Result = descricaoBloqueio;
                        return;
                    }
                }
            }
            #endregion

            #region Operacao Retroativa
            if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
            {
                if (DateTime.Compare(dataRegistro, cliente.DataDia.Value) < 0)
                {
                    e.Result = "Data de registro anterior a data do Cliente. | Favor processar a carteira novamente a partir de " + dataRegistro.ToString("dd/MM/yyyy");
                    return;
                }
            }
            #endregion

            if (ParametrosConfiguracaoSistema.Outras.SuitabilityRendaFixa && idClienteContraparte.HasValue)
            {
                if (compraFinal)
                {
                    DadosOperacao dadosOperacao = new DadosOperacao();
                    dadosOperacao.DataOperacao = dataOperacao;
                    dadosOperacao.IdCliente = idCliente;
                    dadosOperacao.PapelDescricao = tituloRendaFixa.Descricao;
                    dadosOperacao.ValorOperacao = textValor.Text != "" ? Convert.ToDecimal(textValor.Text) : 0;
                    string json = JsonConvert.SerializeObject(dadosOperacao, Formatting.None);

                    e.Result = json;
                }
                else if(vendaFinal)
                {
                    DadosOperacao dadosOperacao = new DadosOperacao();
                    dadosOperacao.DataOperacao = dataOperacao;
                    dadosOperacao.IdCliente = idClienteContraparte.Value;
                    dadosOperacao.PapelDescricao = tituloRendaFixa.Descricao;
                    dadosOperacao.ValorOperacao = textValor.Text != "" ? Convert.ToDecimal(textValor.Text) : 0;
                    string json = JsonConvert.SerializeObject(dadosOperacao, Formatting.None);

                    e.Result = json;
                }
            }

            if (idOperacao.HasValue && hiddenDataModificacao.Text != "")
            {

                DateTime dataModificacao = Convert.ToDateTime(hiddenDataModificacao.Text);

                //compara se a diferença entre a data da tela e a data do banco tem menos de 1 segundo de diferença(o componente web não guarda milisegundos)
                if (operacao.DataModificacao.Value.Subtract(dataModificacao) > TimeSpan.FromMilliseconds(1000))
                {
                    e.Result = "Atenção esta operação foi modificada por outro usuario. Favor fechar e abrir novamente para poder salvar";
                    return;
                }
            }





        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textPercentualPuFace_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int idTitulo = 0;
            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;

            if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
                idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
            else
                idTitulo = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdTitulo"));

            if (idTitulo > 0)
            {
                List<int> lstGlobals = new List<int>();
                //Acrescentar também na função java script do aspx
                lstGlobals.Add((int)ClasseRendaFixa.GLOBALS);
                lstGlobals.Add((int)ClasseRendaFixa.TBILL);

                TituloRendaFixa titulo = new TituloRendaFixa();

                if (titulo.LoadByPrimaryKey(idTitulo))
                {
                    PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;
                    (sender as ASPxSpinEdit).ClientEnabled = (lstGlobals.Contains((int)papel.Classe.Value));
                }
            }
        }
    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    #region Campos Opcionais
    protected void textCodigoContraParte_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as TextBox).Visible = false;
        }
    }

    protected void labelCodigoContraParte_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void dropFormaLiquidacao_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as ASPxComboBox).ClientVisible = false;
        }
    }

    protected void labelFormaLiquidacao_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void textIdVinculo_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as ASPxSpinEdit).ClientVisible = false;
        }
    }

    protected void labelIdVinculo_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void dropLocalCustodia_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as ASPxComboBox).ClientVisible = false;
        }
    }

    protected void labelLocalCustodia_Init(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void labelMercadoCliente_OnInit(object sender, EventArgs e)
    {
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            (sender as Label).Visible = false;
        }
    }


    protected void dropMercadoCliente_OnInit(object sender, EventArgs e)
    {
        ASPxComboBox dropMercadoCliente = (sender as ASPxComboBox);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            dropMercadoCliente.ClientVisible = false;
        }
    }

    protected void btnAgenteContraParte_OnInit(object sender, EventArgs e)
    {
        ASPxSpinEdit btnAgenteContraParte = (sender as ASPxSpinEdit);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            btnAgenteContraParte.ClientVisible = false;
        }
    }

    protected void btnCarteiraContraparte_OnInit(object sender, EventArgs e)
    {
        ASPxSpinEdit btnCarteiraContraparte = (sender as ASPxSpinEdit);
        if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia != (int)TipoControleCustodia.ControlaClearing)
        {
            btnCarteiraContraparte.ClientVisible = false;
        }
    }

    protected void dropMercadoCliente_OnLoad(object sender, EventArgs e)    
    {
        ASPxComboBox dropMercadoCliente = (sender as ASPxComboBox);
        if(!gridCadastro.IsNewRowEditing)
            dropMercadoCliente.ClientEnabled = false;
    }


    protected void btnCarteiraContraparte_OnLoad(object sender, EventArgs e)
    {
        ASPxSpinEdit btnCarteiraContraparte = (sender as ASPxSpinEdit);
        if (!gridCadastro.IsNewRowEditing)
            btnCarteiraContraparte.ClientEnabled = false;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo()
    {
        int idCliente = 0;
        int? idClienteContraparte = null;
        using (TransactionScope transaction = new TransactionScope())
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxSpinEdit textValorCorretagem = gridCadastro.FindEditFormTemplateControl("textValorCorretagem") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropTipoNegociacao = gridCadastro.FindEditFormTemplateControl("dropTipoNegociacao") as ASPxComboBox;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
            ASPxSpinEdit textIdVinculo = gridCadastro.FindEditFormTemplateControl("textIdVinculo") as ASPxSpinEdit;
            TextBox textCodigoContraParte = gridCadastro.FindEditFormTemplateControl("textCodigoContraParte") as TextBox;
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
            CheckBox chkOperacaoTermo = gridCadastro.FindEditFormTemplateControl("chkOperacaoTermo") as CheckBox;
            CheckBox chkIsIPO = gridCadastro.FindEditFormTemplateControl("chkIsIPO") as CheckBox;
            TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;
            ASPxSpinEdit textPercentualPuFace = gridCadastro.FindEditFormTemplateControl("textPercentualPuFace") as ASPxSpinEdit;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
            ASPxSpinEdit btnCarteiraContraparte = gridCadastro.FindEditFormTemplateControl("btnCarteiraContraparte") as ASPxSpinEdit;
            ASPxSpinEdit btnAgenteContraParte = gridCadastro.FindEditFormTemplateControl("btnAgenteContraParte") as ASPxSpinEdit;
            ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;
            ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;


            //
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            //
            idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoRendaFixa.IdCliente = idCliente;
            operacaoRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
            operacaoRendaFixa.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            operacaoRendaFixa.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
            operacaoRendaFixa.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            operacaoRendaFixa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            operacaoRendaFixa.PUOperacao = Convert.ToDecimal(textPUOperacao.Text);
            operacaoRendaFixa.Quantidade = textQuantidade.Text != "" ? Convert.ToDecimal(textQuantidade.Text) : 0;
            operacaoRendaFixa.Valor = textValor.Text != "" ? Convert.ToDecimal(textValor.Text) : 0;
            operacaoRendaFixa.PercentualPuFace = !string.IsNullOrEmpty(textPercentualPuFace.Text) ? Convert.ToDecimal(textPercentualPuFace.Text) : 0;

            operacaoRendaFixa.ValorCorretagem = textValorCorretagem.Text != "" ? Convert.ToDecimal(textValorCorretagem.Text) : 0;

            operacaoRendaFixa.TaxaOperacao = 0;
            if (textTaxaOperacao.Text != "")
            {
                operacaoRendaFixa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
            }

            if (!string.IsNullOrEmpty(textCodigoContraParte.Text))
                operacaoRendaFixa.CodigoContraParte = textCodigoContraParte.Text;

            operacaoRendaFixa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
            if (dropAgenteCustodia.SelectedIndex != -1)
                operacaoRendaFixa.IdAgenteCustodia = Convert.ToInt32(dropAgenteCustodia.SelectedItem.Value);
            if (dropAgenteCorretora.SelectedIndex != -1)
                operacaoRendaFixa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            operacaoRendaFixa.TipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);
            operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.Manual;

            if (dropTrader.SelectedIndex > -1)
                operacaoRendaFixa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                operacaoRendaFixa.IdTrader = null;

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                operacaoRendaFixa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                operacaoRendaFixa.IdCategoriaMovimentacao = null;
            }

            if (dropMercadoCliente.SelectedItem != null && dropMercadoCliente.SelectedItem.Value == "1" && btnAgenteContraParte.Value != null)
                operacaoRendaFixa.IdAgenteContraParte = Convert.ToInt32(btnAgenteContraParte.Number);

            //if (dropMercadoCliente.SelectedItem != null && dropMercadoCliente.SelectedItem.Value == "2" && btnCarteiraContraparte.Value != null)
            //    operacaoRendaFixa.IdCarteiraContraParte = Convert.ToInt32(btnCarteiraContraparte.Number);

            if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia == (int)TipoControleCustodia.ControlaClearing)
            {
                if (dropFormaLiquidacao.SelectedIndex > -1)
                {
                    operacaoRendaFixa.IdLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
                    operacaoRendaFixa.IdCustodia = Convert.ToByte(dropLocalCustodia.SelectedItem.Value);
                }
                else
                {
                    operacaoRendaFixa.IdLiquidacao = (byte)LiquidacaoTitulo.NaoInformado;
                    operacaoRendaFixa.IdCustodia = (byte)CustodiaTitulo.NaoInformado;
                }

                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

                operacaoRendaFixa.IdAgenteCorretora = idAgenteMercado;
                operacaoRendaFixa.CodigoContraParte = textCodigoContraParte.Text;
            }
            else
            {
                operacaoRendaFixa.IdLiquidacao = (byte)LiquidacaoTitulo.NaoInformado;
                operacaoRendaFixa.IdCustodia = (byte)CustodiaTitulo.NaoInformado;
            }


            if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia == (int)TipoControleCustodia.ControlaCustodiante)
            {
                if (dropAgenteCorretora.SelectedIndex > -1)
                {
                    operacaoRendaFixa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
                }
            }

            operacaoRendaFixa.TipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);
            operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.Manual;

            if (hiddenIdPosicao.Text != "")
            {
                int idPosicaoResgatada = Convert.ToInt32(hiddenIdPosicao.Text);
                //operacaoRendaFixa.IdPosicaoResgatada = idPosicaoResgatada;

                PosicaoRendaFixa pos = new PosicaoRendaFixa();
                operacaoRendaFixa.IdOperacaoResgatada = pos.BuscaIdOperacaoResgatada(idPosicaoResgatada);
            }

            operacaoRendaFixa.Observacao = textObservacao.Text.Trim().Length > 200 ? textObservacao.Text.Trim().Substring(0, 200) : textObservacao.Text.Trim();
            operacaoRendaFixa.OperacaoTermo = chkOperacaoTermo.Checked ? "S" : "N";
            operacaoRendaFixa.IsIPO = chkIsIPO.Checked ? "S" : "N";

            //Operação Compromissada
            if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
            {
                ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;
                ASPxComboBox dropIndiceVolta = gridCadastro.FindEditFormTemplateControl("dropIndiceVolta") as ASPxComboBox;

                operacaoRendaFixa.DataVolta = Convert.ToDateTime(textDataVolta.Text);
                operacaoRendaFixa.TaxaVolta = Convert.ToDecimal(textTaxaVolta.Text);

                if (textPUVolta.Text != "" && textValorVolta.Text != "")
                {
                    operacaoRendaFixa.PUVolta = Convert.ToDecimal(textPUVolta.Text);
                    operacaoRendaFixa.ValorVolta = Convert.ToDecimal(textValorVolta.Text);
                }
                else
                {
                    operacaoRendaFixa.PUVolta = null;
                    operacaoRendaFixa.ValorVolta = null;
                }

                if (dropIndiceVolta.SelectedIndex != -1)
                {
                    operacaoRendaFixa.IdIndiceVolta = Convert.ToInt16(dropIndiceVolta.SelectedItem.Value);
                }
                else
                {
                    operacaoRendaFixa.IdIndiceVolta = null;
                }
            }
            else
            {
                operacaoRendaFixa.DataVolta = null;
                operacaoRendaFixa.TaxaVolta = null;
                operacaoRendaFixa.PUVolta = null;
                operacaoRendaFixa.ValorVolta = null;
                operacaoRendaFixa.IdIndiceVolta = null;
            }
            //
            if (textIdVinculo.Text != "")
            {
                operacaoRendaFixa.IdOperacaoVinculo = Convert.ToInt32(textIdVinculo.Value);
            }

            operacaoRendaFixa.DataModificacao = DateTime.Now;

        if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
        {
            operacaoRendaFixa.IdConta = Convert.ToInt32(dropConta.Value);
        }
        
            operacaoRendaFixa.Save();


            if (dropMercadoCliente.SelectedItem != null && dropTipoOperacao.SelectedItem != null)
            {
                if (dropMercadoCliente.SelectedItem.Value == "2" &&
                    btnCarteiraContraparte.Text != "")
                {
                    

                    OperacaoRendaFixa operacaoEspelho = new OperacaoRendaFixa();
                    operacaoEspelho.IdCliente = Convert.ToInt32(btnCarteiraContraparte.Text);
                    operacaoEspelho.IdTitulo = operacaoRendaFixa.IdTitulo;
                    operacaoEspelho.IdPosicaoResgatada = operacaoRendaFixa.IdPosicaoResgatada;
                    operacaoEspelho.IdOperacaoResgatada = operacaoRendaFixa.IdOperacaoResgatada;
                    operacaoEspelho.DataRegistro = operacaoRendaFixa.DataRegistro;
                    operacaoEspelho.TaxaOperacao = operacaoRendaFixa.TaxaOperacao;
                    operacaoEspelho.DataOperacao = operacaoRendaFixa.DataOperacao;
                    operacaoEspelho.DataLiquidacao = operacaoRendaFixa.DataLiquidacao;
                    operacaoEspelho.TipoOperacao = TipoOperacaoContraParte(operacaoRendaFixa.TipoOperacao);
                    operacaoEspelho.DataVolta = operacaoRendaFixa.DataVolta;
                    operacaoEspelho.TipoNegociacao = operacaoRendaFixa.TipoNegociacao;
                    operacaoEspelho.IdIndiceVolta = operacaoRendaFixa.IdIndiceVolta;
                    operacaoEspelho.Quantidade = operacaoRendaFixa.Quantidade;
                    operacaoEspelho.TaxaVolta = operacaoRendaFixa.TaxaVolta;
                    operacaoEspelho.PUOperacao = operacaoRendaFixa.PUOperacao;
                    operacaoEspelho.PUVolta = operacaoRendaFixa.PUVolta;
                    operacaoEspelho.Valor = operacaoRendaFixa.Valor;
                    operacaoEspelho.ValorVolta = operacaoRendaFixa.ValorVolta;
                    operacaoEspelho.ValorCorretagem = operacaoRendaFixa.ValorCorretagem;
                    operacaoEspelho.Observacao = operacaoRendaFixa.Observacao;
                    operacaoEspelho.Fonte = operacaoRendaFixa.Fonte;
                    operacaoEspelho.IdCustodia = operacaoRendaFixa.IdCustodia;
                    operacaoEspelho.IdLiquidacao = operacaoRendaFixa.IdLiquidacao;
                    operacaoEspelho.IsIPO = operacaoRendaFixa.IsIPO;
                    operacaoEspelho.OperacaoTermo = operacaoRendaFixa.OperacaoTermo;
                    operacaoEspelho.IdOperacaoEspelho = operacaoRendaFixa.IdOperacao;
                    operacaoEspelho.DataModificacao = operacaoRendaFixa.DataModificacao;
                    operacaoEspelho.IdCarteiraContraparte = operacaoRendaFixa.IdCliente;
                    operacaoEspelho.Save();
                    
                    operacaoRendaFixa.IdCarteiraContraparte = operacaoEspelho.IdCliente;
                    operacaoRendaFixa.IdOperacaoEspelho = operacaoEspelho.IdOperacao;                    
                    operacaoRendaFixa.Save();


                    idClienteContraparte = operacaoEspelho.IdCliente.Value;
                }
            }

            idCliente = operacaoRendaFixa.IdCliente.Value;

            ProcessamentoAutomatico(idCliente);
            ProcessamentoAutomatico(idClienteContraparte);

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            if (DateTime.Compare(cliente.DataDia.Value, operacaoRendaFixa.DataRegistro.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(idCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoRendaFixa.DataRegistro.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacaoRendaFixa.DataRegistro.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = idCliente;
                    boletoRetroativo.DataBoleto = operacaoRendaFixa.DataRegistro.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.RendaFixa;
                }

                boletoRetroativo.Save();
            }
            #endregion

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoRendaFixa - Operacao: Insert OperacaoRendaFixa: " + operacaoRendaFixa.IdOperacao + UtilitarioWeb.ToString(operacaoRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            /* Atualiza StatusRealTime para executar */
            cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);

            transaction.Complete();
        }
        
    }

    public void ProcessamentoAutomatico(int? idCliente)
    {
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        #region validações
        if (!idCliente.HasValue)
            return;

        if (!carteira.LoadByPrimaryKey(idCliente.Value))
            throw new Exception("Carteira " + idCliente.Value + " não encontrada");

        if (!cliente.LoadByPrimaryKey(idCliente.Value))
            throw new Exception("Cliente: " + idCliente.Value + " não encontrado");

        if (!cliente.DataDia.HasValue)
            throw new Exception("Cliente sem DataDia preenchida");

        //if (cliente.Status == (byte)StatusCliente.FechadoComErro)
        //    throw new Exception("Ultimo calculo do cliente possui erros favor verificar antes de prosseguir");

        if (carteira.ProcAutomatico != "S")
            return;
        #endregion
        
        //Financial.Investidor.Controller.ControllerInvestidor controller = new Financial.Investidor.Controller.ControllerInvestidor();
        Financial.Investidor.Controller.ParametrosProcessamento parametros = new Financial.Investidor.Controller.ParametrosProcessamento();
        parametros.CravaCota = false;
        parametros.IgnoraOperacoes = false;
        parametros.IntegraBMF = false;
        parametros.IntegraBolsa = false;
        parametros.IntegraBTC = false;
        parametros.IntegracaoRendaFixa = 0;
        parametros.IntegraCC = false;
        parametros.MantemFuturo = false;
        parametros.ProcessaBMF = false;
        parametros.ProcessaBolsa = false;
        parametros.ProcessaCambio = false;
        parametros.ProcessaFundo =false;
        parametros.ProcessaRendaFixa = true;
        parametros.ProcessaSwap = false;
        parametros.RemuneraRF = false;
        parametros.ResetCusto = false;
        parametros.ListaTabelaInterfaceCliente = new List<int>();




        //controller.ExecutaFechamento(idCliente.Value, cliente.DataDia.Value, parametros);

        Financial.Investidor.Controller.ControllerInvestidor controller = new Financial.Investidor.Controller.ControllerInvestidor();
        controller.ExecutaFechamento(idCliente.Value, cliente.DataDia.Value, parametros);

        //Financial.RendaFixa.Controller.ControllerRendaFixa controller = new Financial.RendaFixa.Controller.ControllerRendaFixa();
        //controller.ExecutaCalculoAutomatico(idCliente.Value, cliente.DataDia.Value);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        string operacoesRetroativas = "N";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        UtilitarioWeb util = new UtilitarioWeb();
                        if (util.IsClienteBloqueadoSinacor(idCliente))
                        {
                            resultado = "no_active";
                        }
                        else
                        {
                            PermissaoCliente permissaoCliente = new PermissaoCliente();
                            if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                            {
                                nome = cliente.str.Apelido;
                                DateTime dataDia = cliente.DataDia.Value;

                                resultado = nome + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;

                                bool clienteItau = ConfigurationManager.AppSettings["Cliente"] == "Itau";
                                if (clienteItau)
                                {
                                    DateTime dataD1 = Calendario.AdicionaDiaUtil(dataDia, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                                    resultado = resultado + "|" + dataD1.ToShortDateString();
                                }
                                else
                                {
                                    resultado = resultado + "|" + dataDia.ToShortDateString();
                                }
                            }
                            else
                            {
                                resultado = "no_access";
                            }
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);
            int? idLocalNegociacao = null;
            int? idCustodia = null;
            int? idClearing = null;
            string descricaoCompleta = string.Empty;
            DateTime dataLiquidacao = new DateTime();
            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.DescricaoCompleta,
                                                   tituloRendaFixaCollection.Query.IdPapel,
                                                   tituloRendaFixaCollection.Query.DefasagemLiquidacao,
                                                    "< case when ltrim(rtrim(Coalesce(DescricaoCompleta,''))) = '' then Descricao else DescricaoCompleta end as DescricaoVirtual>");
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdTitulo == idTitulo);

            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.HasData)
            {
                descricaoCompleta = (string)tituloRendaFixaCollection[0].GetColumn("DescricaoVirtual").ToString();

                papelRendaFixa = new PapelRendaFixa();
                if (papelRendaFixa.LoadByPrimaryKey(tituloRendaFixaCollection[0].IdPapel.Value))
                {
                    idClearing = papelRendaFixa.IdClearing.Value;
                    ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
                    if (!string.IsNullOrEmpty(textDataOperacao.Text))
                    {
                        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
                        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCliente);

                        int defasagemLiquidacao = tituloRendaFixaCollection[0].DefasagemLiquidacao.GetValueOrDefault(0);
                        if (defasagemLiquidacao != 0)
                        {
                            Clearing clearing = new Clearing();  
                            dataLiquidacao = Convert.ToDateTime(textDataOperacao.Text);
                            
                                                                                                              
                            if (clearing.LoadByPrimaryKey(idClearing.Value))
                            {
                                int idLocalFeriado = clearing.IdLocalFeriado.Value;
                                TipoFeriado tipoFeriado = (clearing.IdLocalFeriado.Value == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);
                                dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, defasagemLiquidacao, idLocalFeriado, tipoFeriado);
                            }
                            else
                            {                                
                                if (cliente.IdLocal == LocalFeriadoFixo.NovaYork)
                                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, tituloRendaFixaCollection[0].DefasagemLiquidacao.Value, (byte)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
                                else
                                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, tituloRendaFixaCollection[0].DefasagemLiquidacao.Value, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                            }
                        }
                        else
                        {
                            dataLiquidacao = Convert.ToDateTime(textDataOperacao.Text);
                        }

                        idLocalNegociacao = papelRendaFixa.IdLocalNegociacao;
                        idCustodia = papelRendaFixa.IdLocalCustodia;
                        idClearing = papelRendaFixa.IdClearing;
                    }
                }
            }

            resultado = descricaoCompleta.ToString() + "|" + papelRendaFixa.Classe.Value + "|" + dataLiquidacao.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idLocalNegociacao + "|" + idCustodia + "|" + idClearing;
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackPercentualPuFace_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        ASPxSpinEdit textPercentualPuFace = gridCadastro.FindEditFormTemplateControl("textPercentualPuFace") as ASPxSpinEdit;

        if (string.IsNullOrEmpty(textData.Text))
            return;

        if (string.IsNullOrEmpty(hiddenIdTitulo.Text))
            return;

        if (string.IsNullOrEmpty(textPercentualPuFace.Text))
            return;

        int idTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        #region Carrega Campos - EntitySpaces
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(tituloRendaFixa.Query.IdPapel);
        campos.Add(tituloRendaFixa.Query.Taxa);
        campos.Add(tituloRendaFixa.Query.IdIndice);
        campos.Add(tituloRendaFixa.Query.IdTitulo);
        campos.Add(tituloRendaFixa.Query.DataEmissao);
        campos.Add(tituloRendaFixa.Query.ValorNominal);
        campos.Add(tituloRendaFixa.Query.TipoProRata);
        campos.Add(tituloRendaFixa.Query.ExecutaProRataEmissao);
        campos.Add(tituloRendaFixa.Query.PUNominal);
        campos.Add(tituloRendaFixa.Query.DataVencimento);
        campos.Add(tituloRendaFixa.Query.DefasagemLiquidacao);
        campos.Add(tituloRendaFixa.Query.Periodicidade);
        campos.Add(tituloRendaFixa.Query.ParametrosAvancados);
        tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
        int idPapel = tituloRendaFixa.IdPapel.Value;

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
        campos = new List<esQueryItem>();
        campos.Add(papelRendaFixa.Query.ContagemDias);
        campos.Add(papelRendaFixa.Query.CasasDecimaisPU);
        campos.Add(papelRendaFixa.Query.Classe);
        campos.Add(papelRendaFixa.Query.IdClearing);
        papelRendaFixa.LoadByPrimaryKey(campos, idPapel);
        #endregion
        
        PosicaoRendaFixa.PosicaoAtualizada posicao = new PosicaoRendaFixa.PosicaoAtualizada();
        if (tituloRendaFixa.ParametrosAvancados.Equals("S"))
        {
            CalculoRendaFixa c = new CalculoRendaFixa();
            double taxaTitulo = (double)tituloRendaFixa.Taxa.GetValueOrDefault(0);
            int enumClasse = papelRendaFixa.Classe.Value;
            DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
            DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;
            int periodicidade = tituloRendaFixa.Periodicidade.GetValueOrDefault(0);

            decimal pu = (Convert.ToDecimal(textPercentualPuFace.Text) / 100) * tituloRendaFixa.ValorNominal.Value;
            Hashtable hsMemoria = new Hashtable();
            posicao = c.CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, null, null, null, pu, 0, 0, 0, false, ref hsMemoria);
        }

        decimal puCalculado = Math.Round(posicao.PuAtualizado, papelRendaFixa.CasasDecimaisPU.Value);
        e.Result = puCalculado.ToString().Replace(",", ".") + "|" + posicao.Taxa.ToString().Replace(",", ".");
    }

    protected void callbackPosicaoOperacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        TextBox hiddenIdOperacaoResgatada = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacaoResgatada") as TextBox;
        string texto = string.Empty;

        if (hiddenIdOperacaoResgatada != null && !String.IsNullOrEmpty(hiddenIdOperacaoResgatada.Text))
        {
            int idOperacaoResgatada = Convert.ToInt32(hiddenIdOperacaoResgatada.Text);
            int? idTitulo = null;

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdOperacao.Equal(idOperacaoResgatada));
            
            if (posicaoRendaFixa.Query.Load())
            {
                texto = "Id.Pos: " + posicaoRendaFixa.IdPosicao.Value.ToString() + " - Id.Op: " + posicaoRendaFixa.IdOperacao.Value.ToString() + "  -  Oper.: " + posicaoRendaFixa.DataOperacao.Value.ToShortDateString() +
                    "  -  Qtde: " + Math.Round(posicaoRendaFixa.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(posicaoRendaFixa.PUOperacao.Value, 8).ToString();

                idTitulo = posicaoRendaFixa.IdTitulo;
            }


            if (string.IsNullOrEmpty(texto))
            {
                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistorico.Query.es.Top = 1;
                posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdOperacao.Equal(idOperacaoResgatada));

                if (posicaoRendaFixaHistorico.Query.Load())
                {
                    texto = "Id.Pos: " + posicaoRendaFixaHistorico.IdPosicao.Value.ToString() + " - Id.Op: " + posicaoRendaFixaHistorico.IdOperacao.Value.ToString() + "  -  Oper.: " + posicaoRendaFixaHistorico.DataOperacao.Value.ToShortDateString() +
                        "  -  Qtde: " + Math.Round(posicaoRendaFixaHistorico.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(posicaoRendaFixaHistorico.PUOperacao.Value, 8).ToString();

                    idTitulo = posicaoRendaFixaHistorico.IdTitulo;
                }
            }

            if (string.IsNullOrEmpty(texto))
            {
                OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();

                if (operacaoRendaFixa.LoadByPrimaryKey(idOperacaoResgatada))
                {
                    texto = "Id.Op: " + operacaoRendaFixa.IdOperacao.Value.ToString() + "  -  Oper.: " + operacaoRendaFixa.DataOperacao.Value.ToShortDateString() +
                        "  -  Qtde: " + Math.Round(operacaoRendaFixa.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(operacaoRendaFixa.PUOperacao.Value, 8).ToString();

                    idTitulo = operacaoRendaFixa.IdTitulo;
                }
            }

            if (idTitulo.HasValue)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.Query.Select(tituloRendaFixa.Query.DescricaoCompleta, tituloRendaFixa.Query.Descricao);
                tituloRendaFixa.Query.Where(tituloRendaFixa.Query.IdTitulo.Equal(idTitulo.Value));
                tituloRendaFixa.Query.Load();

                string descricaoCompleta = tituloRendaFixa.DescricaoCompleta.Trim();

                if (string.IsNullOrEmpty(descricaoCompleta))
                    descricaoCompleta = tituloRendaFixa.Descricao.Trim();

                texto = texto + "|" + descricaoCompleta.ToString() + "|" + idTitulo.Value.ToString();
            }
        }

        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback4_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPosicao = Convert.ToInt32(e.Parameter);
            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.LoadByPrimaryKey(idPosicao);

            texto = "Id.Pos: " + posicaoRendaFixa.IdPosicao.Value.ToString() + " - Id.Op: " + posicaoRendaFixa.IdOperacao.Value.ToString() + "  -  Oper.: " + posicaoRendaFixa.DataOperacao.Value.ToShortDateString() +
                    "  -  Qtde: " + Math.Round(posicaoRendaFixa.Quantidade.Value, 0).ToString() + "  -  PU:" + Math.Round(posicaoRendaFixa.PUOperacao.Value, 8).ToString();

            if (posicaoRendaFixa.IdTitulo.HasValue)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.Query.Select(tituloRendaFixa.Query.DescricaoCompleta);
                tituloRendaFixa.Query.Where(tituloRendaFixa.Query.IdTitulo == (posicaoRendaFixa.IdTitulo.Value));
                tituloRendaFixa.Query.Load();

                string descricaoCompleta = tituloRendaFixa.DescricaoCompleta.Trim();
                texto = texto + "|" + descricaoCompleta.ToString() + "|" + posicaoRendaFixa.IdTitulo.Value.ToString();
            }
        }
        e.Result = texto;
    }


    protected void gridCadastro_RowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ASPxComboBox dropMercadoCliente = gridCadastro.FindEditFormTemplateControl("dropMercadoCliente") as ASPxComboBox;
        dropMercadoCliente.ClientEnabled = false;
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        using (TransactionScope transaction = new TransactionScope())
        {
            int idOperacao = (int)e.Keys[0];
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPUOperacao = gridCadastro.FindEditFormTemplateControl("textPUOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxSpinEdit textValorCorretagem = gridCadastro.FindEditFormTemplateControl("textValorCorretagem") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropTipoNegociacao = gridCadastro.FindEditFormTemplateControl("dropTipoNegociacao") as ASPxComboBox;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxSpinEdit textIdVinculo = gridCadastro.FindEditFormTemplateControl("textIdVinculo") as ASPxSpinEdit;
            TextBox textCodigoContraParte = gridCadastro.FindEditFormTemplateControl("textCodigoContraParte") as TextBox;
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
            CheckBox chkOperacaoTermo = gridCadastro.FindEditFormTemplateControl("chkOperacaoTermo") as CheckBox;
            ASPxSpinEdit textPercentualPuFace = gridCadastro.FindEditFormTemplateControl("textPercentualPuFace") as ASPxSpinEdit;
            CheckBox chkIsIPO = gridCadastro.FindEditFormTemplateControl("chkIsIPO") as CheckBox;
            ASPxSpinEdit btnAgenteContraParte = gridCadastro.FindEditFormTemplateControl("btnAgenteContraParte") as ASPxSpinEdit;
            TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            if (operacaoRendaFixa.LoadByPrimaryKey(idOperacao))
            {
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                int ?idAgenteContraparte = null;
                if (btnAgenteContraParte.Text != "")
                {
                    idAgenteContraparte = Convert.ToInt32(btnAgenteContraParte.Text);
                }
                operacaoRendaFixa.IdCliente = idCliente;
                operacaoRendaFixa.IdAgenteContraParte = idAgenteContraparte;
                operacaoRendaFixa.IdTitulo = Convert.ToInt32(hiddenIdTitulo.Text);
                operacaoRendaFixa.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
                operacaoRendaFixa.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
                operacaoRendaFixa.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
                operacaoRendaFixa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
                operacaoRendaFixa.PUOperacao = Convert.ToDecimal(textPUOperacao.Text);
                operacaoRendaFixa.Quantidade = textQuantidade.Text != "" ? Convert.ToDecimal(textQuantidade.Text) : 0;
                operacaoRendaFixa.Valor = textValor.Text != "" ? Convert.ToDecimal(textValor.Text) : 0;
                operacaoRendaFixa.ValorCorretagem = textValorCorretagem.Text != "" ? Convert.ToDecimal(textValorCorretagem.Text) : 0;

                operacaoRendaFixa.TaxaOperacao = 0;
                if (textTaxaOperacao.Text != "")
                {
                    operacaoRendaFixa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
                }


                if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
                {
                    operacaoRendaFixa.IdConta = Convert.ToInt32(dropConta.Value);
                }

                if (dropCategoriaMovimentacao.SelectedIndex > -1)
                {
                    operacaoRendaFixa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);                    
                }
                else
                {
                    operacaoRendaFixa.IdCategoriaMovimentacao = null;
                }

                if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia == (int)TipoControleCustodia.ControlaClearing)
                {
                    if (dropFormaLiquidacao.SelectedIndex > -1)
                    {

                        operacaoRendaFixa.IdLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
                        operacaoRendaFixa.IdCustodia = Convert.ToByte(dropLocalCustodia.SelectedItem.Value);
                    }
                    else
                    {
                        operacaoRendaFixa.IdLiquidacao = (byte)LiquidacaoTitulo.NaoInformado;
                        operacaoRendaFixa.IdCustodia = (byte)CustodiaTitulo.NaoInformado;
                    }

                    int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                    AgenteMercado agenteMercado = new AgenteMercado();
                    int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

                    operacaoRendaFixa.IdAgenteCorretora = idAgenteMercado;
                    operacaoRendaFixa.CodigoContraParte = textCodigoContraParte.Text;
                }
                else
                {
                    operacaoRendaFixa.IdLiquidacao = (byte)LiquidacaoTitulo.NaoInformado;
                    operacaoRendaFixa.IdCustodia = (byte)CustodiaTitulo.NaoInformado;
                }


                if (ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia == (int)TipoControleCustodia.ControlaCustodiante)
                {
                    if (dropAgenteCorretora.SelectedIndex > -1)
                    {
                        operacaoRendaFixa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
                    }
                }

                operacaoRendaFixa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);

                if (dropAgenteCustodia.SelectedIndex != -1)
                    operacaoRendaFixa.IdAgenteCustodia = Convert.ToInt32(dropAgenteCustodia.SelectedItem.Value);
            
                operacaoRendaFixa.TipoNegociacao = Convert.ToByte(dropTipoNegociacao.SelectedItem.Value);

                if (dropTrader.SelectedIndex > -1)
                    operacaoRendaFixa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
                else
                    operacaoRendaFixa.IdTrader = null;

                if (hiddenIdPosicao.Text != "")
                {
                    int idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
                    int? idOperacaoResgate = operacaoRendaFixa.IdOperacaoResgatada;
                    int? idPosicaoResgatada = null;

                    PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                    if (!posicaoRendaFixa.LoadByPrimaryKey(idPosicao))
                    {
                        PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();

                        posicaoRendaFixaHistorico.Query.es.Top = 1;
                        posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdPosicao.Equal(idPosicao));

                        if (posicaoRendaFixaHistorico.Query.Load())
                        {
                            idOperacaoResgate = posicaoRendaFixaHistorico.IdOperacao;
                            idPosicaoResgatada = posicaoRendaFixaHistorico.IdPosicao;
                        }
                    }
                    else
                    {
                        idOperacaoResgate = posicaoRendaFixa.IdOperacao;
                        idPosicaoResgatada = posicaoRendaFixa.IdPosicao;
                    }

                    operacaoRendaFixa.IdOperacaoResgatada = idOperacaoResgate;
                    operacaoRendaFixa.IdPosicaoResgatada = idPosicaoResgatada;
                }

                operacaoRendaFixa.Observacao = textObservacao.Text.Trim().Length > 200 ? textObservacao.Text.Trim().Substring(0, 200) : textObservacao.Text.Trim();
                operacaoRendaFixa.OperacaoTermo = chkOperacaoTermo.Checked ? "S" : "N";
                operacaoRendaFixa.IsIPO = chkIsIPO.Checked ? "S" : "N";

                //Operação Compromissada
                if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
                {
                    ASPxDateEdit textDataVolta = gridCadastro.FindEditFormTemplateControl("textDataVolta") as ASPxDateEdit;
                    ASPxSpinEdit textTaxaVolta = gridCadastro.FindEditFormTemplateControl("textTaxaVolta") as ASPxSpinEdit;
                    ASPxSpinEdit textPUVolta = gridCadastro.FindEditFormTemplateControl("textPUVolta") as ASPxSpinEdit;
                    ASPxSpinEdit textValorVolta = gridCadastro.FindEditFormTemplateControl("textValorVolta") as ASPxSpinEdit;
                    ASPxComboBox dropIndiceVolta = gridCadastro.FindEditFormTemplateControl("dropIndiceVolta") as ASPxComboBox;

                    operacaoRendaFixa.DataVolta = Convert.ToDateTime(textDataVolta.Text);
                    operacaoRendaFixa.TaxaVolta = Convert.ToDecimal(textTaxaVolta.Text);
                    if (textPUVolta.Text != "" && textValorVolta.Text != "")
                    {
                        operacaoRendaFixa.PUVolta = Convert.ToDecimal(textPUVolta.Text);
                        operacaoRendaFixa.ValorVolta = Convert.ToDecimal(textValorVolta.Text);
                    }
                    else
                    {
                        operacaoRendaFixa.PUVolta = null;
                        operacaoRendaFixa.ValorVolta = null;
                    }

                    if (dropIndiceVolta.SelectedIndex != -1)
                    {
                        operacaoRendaFixa.IdIndiceVolta = Convert.ToInt16(dropIndiceVolta.SelectedItem.Value);
                    }
                    else
                    {
                        operacaoRendaFixa.IdIndiceVolta = null;
                    }
                }
                else
                {
                    operacaoRendaFixa.DataVolta = null;
                    operacaoRendaFixa.TaxaVolta = null;
                    operacaoRendaFixa.PUVolta = null;
                    operacaoRendaFixa.ValorVolta = null;
                    operacaoRendaFixa.IdIndiceVolta = null;
                }
                //

                if (textIdVinculo.Text != "")
                {
                    operacaoRendaFixa.IdOperacaoVinculo = Convert.ToInt32(textIdVinculo.Value);
                }
                else
                {
                    operacaoRendaFixa.IdOperacaoVinculo = null;
                }

                operacaoRendaFixa.DataModificacao = DateTime.Now;

                operacaoRendaFixa.Save();

                #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);
                if (DateTime.Compare(cliente.DataDia.Value, operacaoRendaFixa.DataRegistro.Value) > 0)
                {
                    BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                    if (boletoRetroativo.LoadByPrimaryKey(idCliente))
                    {
                        if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoRendaFixa.DataRegistro.Value) > 0)
                        {
                            boletoRetroativo.DataBoleto = operacaoRendaFixa.DataRegistro.Value;
                        }
                    }
                    else
                    {
                        boletoRetroativo.IdCliente = idCliente;
                        boletoRetroativo.DataBoleto = operacaoRendaFixa.DataRegistro.Value;
                        boletoRetroativo.TipoMercado = (int)TipoMercado.RendaFixa;
                    }

                    boletoRetroativo.Save();
                }
                #endregion

                /* Atualiza StatusRealTime para executar */
                cliente = new Cliente();
                cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);

                if (operacaoRendaFixa.IdOperacaoEspelho.HasValue)
                {
                    OperacaoRendaFixa operacaoEspelho = new OperacaoRendaFixa();
                    operacaoEspelho.LoadByPrimaryKey(operacaoRendaFixa.IdOperacaoEspelho.Value);

                    operacaoEspelho.IdTitulo = operacaoRendaFixa.IdTitulo;
                    operacaoEspelho.IdPosicaoResgatada = operacaoRendaFixa.IdPosicaoResgatada;
                    operacaoEspelho.IdOperacaoResgatada = operacaoRendaFixa.IdOperacaoResgatada;
                    operacaoEspelho.DataRegistro = operacaoRendaFixa.DataRegistro;
                    operacaoEspelho.TaxaOperacao = operacaoRendaFixa.TaxaOperacao;
                    operacaoEspelho.DataOperacao = operacaoRendaFixa.DataOperacao;
                    operacaoEspelho.DataLiquidacao = operacaoRendaFixa.DataLiquidacao;
                    operacaoEspelho.TipoOperacao = TipoOperacaoContraParte(operacaoRendaFixa.TipoOperacao);
                    operacaoEspelho.DataVolta = operacaoRendaFixa.DataVolta;
                    operacaoEspelho.TipoNegociacao = operacaoRendaFixa.TipoNegociacao;
                    operacaoEspelho.IdIndiceVolta = operacaoRendaFixa.IdIndiceVolta;
                    operacaoEspelho.Quantidade = operacaoRendaFixa.Quantidade;
                    operacaoEspelho.TaxaVolta = operacaoRendaFixa.TaxaVolta;
                    operacaoEspelho.PUOperacao = operacaoRendaFixa.PUOperacao;
                    operacaoEspelho.PUVolta = operacaoRendaFixa.PUVolta;
                    operacaoEspelho.Valor = operacaoRendaFixa.Valor;
                    operacaoEspelho.ValorVolta = operacaoRendaFixa.ValorVolta;
                    operacaoEspelho.ValorCorretagem = operacaoRendaFixa.ValorCorretagem;
                    operacaoEspelho.Observacao = operacaoRendaFixa.Observacao;
                    operacaoEspelho.Fonte = operacaoRendaFixa.Fonte;
                    operacaoEspelho.IdCustodia = operacaoRendaFixa.IdCustodia;
                    operacaoEspelho.IdLiquidacao = operacaoRendaFixa.IdLiquidacao;
                    operacaoEspelho.IsIPO = operacaoRendaFixa.IsIPO;
                    operacaoEspelho.OperacaoTermo = operacaoRendaFixa.OperacaoTermo;
                    operacaoEspelho.IdOperacaoEspelho = operacaoRendaFixa.IdOperacao;
                    operacaoEspelho.DataModificacao = operacaoRendaFixa.DataModificacao;

                    operacaoEspelho.Save();

                    ProcessamentoAutomatico(operacaoEspelho.IdCliente.Value);

                }

                ProcessamentoAutomatico(operacaoRendaFixa.IdCliente.Value);

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OperacaoRendaFixa - Operacao: Update OperacaoRendaFixa: " + idOperacao + UtilitarioWeb.ToString(operacaoRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion

                /* Atualiza StatusRealTime para executar */
                cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            }

            transaction.Complete();
        }
        

        //ProcessamentoAutomatico(idCliente);
        //ProcessamentoAutomatico(idClienteContraparte);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                List<Int32> idOperacaoLista = gridCadastro.GetSelectedFieldValues("IdOperacao").ConvertAll<int>(
                    new Converter<Object, Int32>(
                        delegate(Object obj)
                        {
                            return Convert.ToInt32(obj);
                        }
                    )
                );

                idOperacaoLista.AddRange(
                    gridCadastro.GetSelectedFieldValues("IdOperacaoEspelho")
                        .FindAll(
                            new Predicate<Object>(
                                delegate(Object obj)
                                {
                                    if ((obj as DBNull) == null)
                                        return true;
                                    else
                                        return false;
                                }
                            ))
                        .ConvertAll<int>(
                            new Converter<Object, Int32>(
                                delegate(Object obj)
                                {
                                    return Convert.ToInt32(obj);
                                }
                            )
                        )
                );

                MemoriaCalculoRendaFixaCollection memoriaCalculoColl = new MemoriaCalculoRendaFixaCollection();
                memoriaCalculoColl.Query.Where(memoriaCalculoColl.Query.IdOperacao.In(idOperacaoLista));

                if (memoriaCalculoColl.Query.Load())
                {
                    memoriaCalculoColl.MarkAllAsDeleted();
                    memoriaCalculoColl.Save();
                }

                DetalhePosicaoAfetadaRFCollection detalhePosAfetadaColl = new DetalhePosicaoAfetadaRFCollection();
                detalhePosAfetadaColl.Query.Where(detalhePosAfetadaColl.Query.IdOperacao.In(idOperacaoLista));

                if (detalhePosAfetadaColl.Query.Load())
                {
                    detalhePosAfetadaColl.MarkAllAsDeleted();
                    detalhePosAfetadaColl.Save();
                }

                #region apagando posicoes
                PosicaoRendaFixa posicao = new PosicaoRendaFixa();
                PosicaoRendaFixaQuery query = new PosicaoRendaFixaQuery();
                query.Where(query.IdOperacao.In(idOperacaoLista));
                if (posicao.Load(query))
                {
                    posicao.MarkAsDeleted();
                    posicao.Save();
                }

                PosicaoRendaFixaAbertura posicaoAbertura = new PosicaoRendaFixaAbertura();
                PosicaoRendaFixaAberturaQuery query2 = new PosicaoRendaFixaAberturaQuery();
                query2.Where(query2.IdOperacao.In(idOperacaoLista));
                if (posicaoAbertura.Load(query2))
                {
                    posicaoAbertura.MarkAsDeleted();
                    posicaoAbertura.Save();
                }
                

                PosicaoRendaFixaHistorico posicaoHistorico = new PosicaoRendaFixaHistorico();
                PosicaoRendaFixaHistoricoQuery query3 = new PosicaoRendaFixaHistoricoQuery();
                query3.Where(query3.IdOperacao.In(idOperacaoLista));
                if (posicaoHistorico.Load(query3))
                {
                    posicaoHistorico.MarkAsDeleted();
                    posicaoHistorico.Save();
                }
                #endregion

                OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
                OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery();
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdOperacao.In(idOperacaoLista));
                coll.Load(operacaoRendaFixaQuery);

                List<int> idClienteLista = new List<int>();

                foreach (OperacaoRendaFixa operacao in coll)
                {
                    idClienteLista.Add(operacao.IdCliente.Value);

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoRendaFixa - Operacao: Delete OperacaoRendaFixa: " + operacao.IdOperacao + UtilitarioWeb.ToString(operacao),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }

                coll.MarkAllAsDeleted();
                coll.Save();


                foreach (int idCliente in idClienteLista)
                {
                    ProcessamentoAutomatico(idCliente);
                }

                transaction.Complete();
            }
        }
        else if (e.Parameters == "btnObservacao")
        {
            int idOperacao = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacao"));
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

            OperacaoRendaFixa operacaoRendaFixaUpdate = new OperacaoRendaFixa();
            if (operacaoRendaFixaUpdate.LoadByPrimaryKey(idOperacao))
            {
                operacaoRendaFixaUpdate.Observacao = textObservacao.Text.Trim().Length > 200 ? textObservacao.Text.Trim().Substring(0, 200) : textObservacao.Text.Trim();
                operacaoRendaFixaUpdate.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OperacaoRendaFixa - Operacao: Update OperacaoRendaFixa: " + idOperacao + "Nova Observação: " + textObservacao.Text,
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
            e.Properties["cpHiddenIdTitulo"] = hiddenIdTitulo.ClientID;

            TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;
            e.Properties["cpHiddenIdPosicao"] = hiddenIdPosicao.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues["TipoNegociacao"] = "Negociação";
        e.NewValues["IdLiquidacao"] = "-";
        e.NewValues["IdCustodia"] = "-";
        e.NewValues["TaxaOperacao"] = "0";

        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }

        if (dropTituloRendaFixa != null && dropTituloRendaFixa.Text.Trim() != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Descrição Título: ").Append(dropTituloRendaFixa.Text.Trim());
        }

        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TipoContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString())) 
                e.Value = "1";
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho).ToString()) || 
                !string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdCliente).ToString()))
                e.Value = "2";
        }

        if (e.Column.FieldName == "NomeContraparte")
        {
            if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte).ToString()))
                e.Value = e.GetListSourceFieldValue("AgenteMercadoContraparte");
            else if (!string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho).ToString()) ||
                !string.IsNullOrEmpty(e.GetListSourceFieldValue(OperacaoRendaFixaMetadata.ColumnNames.IdCliente).ToString()))
                e.Value = e.GetListSourceFieldValue("ApelidoCarteiraContraparte");
        }
    }

    protected void gridPosicao_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        e.Result = gridPosicao.GetRowValues(Convert.ToInt32(e.Parameters), "IdPosicao");
    }

    protected void gridPosicao_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    public void gridPosicao_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        gridView.DataBind();
    }

    public class DadosOperacao
    {
        public int IdCliente;
        public string PapelDescricao;
        public DateTime DataOperacao;
        public decimal ValorOperacao;
        public DadosOperacao()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCalculaPremio_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        TextBox hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as TextBox;
        e.Result = string.Empty;
        if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
        {
            decimal pu = 0;
            decimal percentual = 0;

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(Convert.ToInt32(hiddenIdTitulo.Text));

            if (tituloRendaFixa.TipoPremio.Value == (short)TipoPremio.PercentualValorFace)
            {
                e.Result = Convert.ToString(tituloRendaFixa.ValorNominal.Value * (tituloRendaFixa.ValorPremio.Value / 100)).Replace(",", ".");                
            }

            if (tituloRendaFixa.TipoPremio.Value == (short)TipoPremio.ValorFixo)
            {
                e.Result = Convert.ToString(tituloRendaFixa.ValorPremio.Value).Replace(",",".");
            }
        }
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
            return;

        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
        {
            e.Collection = new ContaCorrenteCollection();
            return;
        }

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCarteira != null && btnEditCodigoCarteira.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdPessoa);
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                ContaCorrenteCollection coll = new ContaCorrenteCollection();
                
                coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa.Value) | coll.Query.IdCliente.Equal(idCliente));
                coll.Query.OrderBy(coll.Query.Numero.Ascending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }

        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }



    /// <summary>
    /// Abre o Manual na sessão de Renda Fixa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ManualRendaFixa_Click(object sender, EventArgs e) {
        string url1 = "help/Financial/module_8_4.htm";
        //
        string urlLink = "../../"+url1;
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\"+url1;

        if (!File.Exists(path)) {
            urlLink = "../../help/Financial/application.htm";
        }

        //string s = "window.open('" + urlLink + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
        //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        //Response.Redirect("<script type='text/javascript'>window.open('" + urlLink + "','_blank');</script>");
        Response.Redirect(urlLink, true);
        
        //Response.Write("<script>window.open('"+urlLink+"','_blank');</script>");

        //LinkButton1.Attributes.Add("href","../Test.aspx")
        //LinkButton1.Attributes.Add("target","_blank")
    }
}