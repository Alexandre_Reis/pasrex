﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;

public partial class CadastrosBasicos_PapelRendaFixa : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { PapelRendaFixaMetadata.ColumnNames.TipoPapel,
                                                                  PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade,
                                                                  PapelRendaFixaMetadata.ColumnNames.TipoCurva,
                                                                  PapelRendaFixaMetadata.ColumnNames.ContagemDias,
                                                                  PapelRendaFixaMetadata.ColumnNames.Classe
                                  }));
    }

    #region DataSources
    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as ASPxTextBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = gridCadastro.FindEditFormTemplateControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxSpinEdit textCasasDecimaisPU = gridCadastro.FindEditFormTemplateControl("textCasasDecimaisPU") as ASPxSpinEdit;
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        ASPxComboBox dropPagamentoJuros = gridCadastro.FindEditFormTemplateControl("dropPagamentoJuros") as ASPxComboBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropTipoPapel);
        controles.Add(dropTipoRentabilidade);
        controles.Add(dropTipoCurva);
        controles.Add(dropContagemDias);
        controles.Add(dropBaseAno);
        controles.Add(textCasasDecimaisPU);
        controles.Add(dropClasse);
        controles.Add(dropPagamentoJuros);
        controles.Add(dropIsentoIR);
        controles.Add(dropLocalCustodia);
        controles.Add(dropLocalNegociacao);
        controles.Add(dropClearing);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        PapelRendaFixaCollection papelRendaFixaColl = new PapelRendaFixaCollection();
        papelRendaFixaColl.Query.Where(papelRendaFixaColl.Query.TipoPapel.Equal(Convert.ToInt32(dropTipoPapel.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.TipoRentabilidade.Equal(Convert.ToInt32(dropTipoRentabilidade.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.TipoCurva.Equal(Convert.ToInt32(dropTipoCurva.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.BaseAno.Equal(Convert.ToInt32(dropBaseAno.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.ContagemDias.Equal(Convert.ToInt32(dropContagemDias.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.Classe.Equal(Convert.ToInt32(dropClasse.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.PagamentoJuros.Equal(Convert.ToInt32(dropPagamentoJuros.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.IsentoIR.Equal(Convert.ToInt32(dropIsentoIR.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.IdLocalCustodia.Equal(Convert.ToInt32(dropLocalCustodia.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.IdLocalNegociacao.Equal(Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.IdClearing.Equal(Convert.ToInt32(dropClearing.SelectedItem.Value))
                                        & papelRendaFixaColl.Query.CasasDecimaisPU.Equal(textCasasDecimaisPU.Text));                                        

        if (!string.IsNullOrEmpty(textCodigoInterface.Text))
            papelRendaFixaColl.Query.Where(papelRendaFixaColl.Query.CodigoInterface.Equal(textCodigoInterface.Text));

        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxTextBox textIdPapel = gridCadastro.FindEditFormTemplateControl("textIdPapel") as ASPxTextBox;
            papelRendaFixaColl.Query.Where(papelRendaFixaColl.Query.IdPapel.NotEqual(Convert.ToInt32(textIdPapel.Text)));
        }

        if (papelRendaFixaColl.Query.Load())
        {
            string strKeysPapel = string.Empty;
            foreach(PapelRendaFixa papel in papelRendaFixaColl)            
               strKeysPapel = papel.IdPapel.Value.ToString() + ",";

           strKeysPapel = strKeysPapel.Remove(strKeysPapel.Length - 1);

           e.Result = "Já existe um papel cadastrado com as mesmas características! (ID - " + strKeysPapel + ")";
            return;
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = gridCadastro.FindEditFormTemplateControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        ASPxSpinEdit textCasasDecimaisPU = gridCadastro.FindEditFormTemplateControl("textCasasDecimaisPU") as ASPxSpinEdit;
        ASPxComboBox dropPagamentoJuros = gridCadastro.FindEditFormTemplateControl("dropPagamentoJuros") as ASPxComboBox;
        ASPxTextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as ASPxTextBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;

        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;

        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();

        string descricao = textDescricao.Text.ToString();
        byte tipoPapel = Convert.ToByte(dropTipoPapel.SelectedItem.Value);
        byte tipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.SelectedItem.Value);
        byte tipoCurva = Convert.ToByte(dropTipoCurva.SelectedItem.Value);
        byte contagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
        int baseAno = Convert.ToInt32(dropBaseAno.SelectedItem.Value);
        int classe = Convert.ToInt32(dropClasse.SelectedItem.Value);
        byte casasDecimaisPU = Convert.ToByte(textCasasDecimaisPU.Text);
        byte pagamentoJuros = Convert.ToByte(dropPagamentoJuros.SelectedItem.Value);

        papelRendaFixa.Descricao = descricao;
        papelRendaFixa.TipoPapel = tipoPapel;
        papelRendaFixa.TipoRentabilidade = tipoRentabilidade;
        papelRendaFixa.TipoCurva = tipoCurva;
        papelRendaFixa.ContagemDias = contagemDias;
        papelRendaFixa.BaseAno = baseAno;
        papelRendaFixa.TipoVolume = (byte)TipoVolumeTitulo.Quantidade;
        papelRendaFixa.TipoCustodia = (byte)LocalCustodiaFixo.Cetip;
        papelRendaFixa.Classe = classe;
        papelRendaFixa.CasasDecimaisPU = casasDecimaisPU;
        papelRendaFixa.PagamentoJuros = pagamentoJuros;
        papelRendaFixa.CodigoInterface = textCodigoInterface.Text;
        papelRendaFixa.IsentoIR = Convert.ToInt32(dropIsentoIR.SelectedItem.Value);
        papelRendaFixa.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

        if (dropClearing.SelectedIndex > -1)
        {
            papelRendaFixa.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
        }
        else
        {
            papelRendaFixa.IdClearing = null;
        }

        if (dropLocalNegociacao.SelectedIndex > -1)
        {
            papelRendaFixa.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
        }
        else
        {
            papelRendaFixa.IdLocalNegociacao = null;
        }

        if (dropInvestimentoColetivoCvm.SelectedIndex > -1)
        {
            papelRendaFixa.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
        }

        papelRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PapelRendaFixa - Operacao: Insert PapelRendaFixa: " + papelRendaFixa.IdPapel + UtilitarioWeb.ToString(papelRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropTipoRentabilidade = gridCadastro.FindEditFormTemplateControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxComboBox dropClasse = gridCadastro.FindEditFormTemplateControl("dropClasse") as ASPxComboBox;
        ASPxSpinEdit textCasasDecimaisPU = gridCadastro.FindEditFormTemplateControl("textCasasDecimaisPU") as ASPxSpinEdit;
        ASPxComboBox dropPagamentoJuros = gridCadastro.FindEditFormTemplateControl("dropPagamentoJuros") as ASPxComboBox;
        ASPxTextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as ASPxTextBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;

        int idPapel = (int)e.Keys[0];

        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();

        if (papelRendaFixa.LoadByPrimaryKey(idPapel))
        {
            string descricao = textDescricao.Text.ToString();
            byte tipoPapel = Convert.ToByte(dropTipoPapel.SelectedItem.Value);
            byte tipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.SelectedItem.Value);
            byte tipoCurva = Convert.ToByte(dropTipoCurva.SelectedItem.Value);
            byte contagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
            int baseAno = Convert.ToInt32(dropBaseAno.SelectedItem.Value);
            int classe = Convert.ToInt32(dropClasse.SelectedItem.Value);
            byte casasDecimaisPU = Convert.ToByte(textCasasDecimaisPU.Text);
            byte pagamentoJuros = Convert.ToByte(dropPagamentoJuros.SelectedItem.Value);

            papelRendaFixa.Descricao = descricao;
            papelRendaFixa.TipoPapel = tipoPapel;
            papelRendaFixa.TipoRentabilidade = tipoRentabilidade;
            papelRendaFixa.TipoCurva = tipoCurva;
            papelRendaFixa.ContagemDias = contagemDias;
            papelRendaFixa.BaseAno = baseAno;
            papelRendaFixa.TipoVolume = (byte)TipoVolumeTitulo.Quantidade;
            papelRendaFixa.TipoCustodia = (byte)LocalCustodiaFixo.Cetip;
            papelRendaFixa.Classe = classe;
            papelRendaFixa.CasasDecimaisPU = casasDecimaisPU;
            papelRendaFixa.PagamentoJuros = pagamentoJuros;
            papelRendaFixa.CodigoInterface = textCodigoInterface.Text;
            papelRendaFixa.IsentoIR = Convert.ToInt32(dropIsentoIR.SelectedItem.Value);
            papelRendaFixa.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

            if (dropClearing.SelectedIndex > -1)
            {
                papelRendaFixa.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
            }
            else
            {
                papelRendaFixa.IdClearing = null;
            }

            if (dropLocalNegociacao.SelectedIndex > -1)
            {
                papelRendaFixa.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
            }
            else
            {
                papelRendaFixa.IdLocalNegociacao = null;
            }

            if (dropInvestimentoColetivoCvm.SelectedIndex > -1)
            {
                papelRendaFixa.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
            }

            papelRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PapelRendaFixa - Operacao: Update PapelRendaFixa: " + idPapel + UtilitarioWeb.ToString(papelRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues("IdPapel");
            for (int i = 0; i < keyValues1.Count; i++)
            {
                int idPapel = Convert.ToInt32(keyValues1[i]);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                if (papelRendaFixa.LoadByPrimaryKey(idPapel))
                {
                    TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
                    tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdPapel.Equal(idPapel));
                    if (tituloRendaFixaCollection.Query.Load())
                    {
                        throw new Exception("Papel " + papelRendaFixa.Descricao + " não pode ser excluído por ter títulos relacionados.");
                    }

                    PapelRendaFixa papelRendaFixaClone = (PapelRendaFixa)Utilitario.Clone(papelRendaFixa);
                    //

                    papelRendaFixa.MarkAsDeleted();
                    papelRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PapelRendaFixa - Operacao: Delete PapelRendaFixa: " + idPapel + UtilitarioWeb.ToString(papelRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            e.Properties["cpTextDescricao"] = textDescricao.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
}