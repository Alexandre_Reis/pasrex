﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;

public partial class CadastrosBasicos_CurvaRendaFixa : CadastroBasePage
{
    #region Campos do Grid
    GridViewDataComboBoxColumn criterioInterpolacao;
    GridViewDataComboBoxColumn ExpressaoTaxaZero;
    GridViewDataComboBoxColumn TipoCurva;
    ASPxTextBox hiddenIdSerie;
    ASPxTextBox hiddenIdCurvaBase;
    ASPxTextBox hiddenIdCurvaSpread;
    ASPxTextBox hiddenIdIndice;
    #endregion

    #region Campos de Edição
    ASPxComboBox dropPermiteInterpolacao;
    ASPxComboBox dropPermiteExtrapolacao;
    ASPxComboBox dropcriterioInterpolacao;
    ASPxComboBox dropExpressaoTaxaZero;
    ASPxComboBox dropTipoCurva;
    ASPxComboBox dropTipoComposicao;
    #endregion

    #region PopUp Indice
    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        short CDI = 1;
        IndiceCollection indiceColl = new IndiceCollection();
        indiceColl.Query.Where(indiceColl.Query.IdIndice == CDI);
        indiceColl.Query.Load();
        e.Collection = indiceColl;
    }

    protected void gridIndice_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idIndice = Convert.ToInt32(gridView.GetRowValues(visibleIndex, IndiceMetadata.ColumnNames.IdIndice));
        e.Result = idIndice.ToString();
    }

    protected void gridIndice_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridIndice_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridIndice.DataBind();
    }

    protected void btnEditIndice_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void callBackPopupIndice_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idIndice = Convert.ToInt32(e.Parameter);

            Indice indice = this.obterIndices(idIndice)[0];

            if (indice.IdIndice.HasValue)
            {
                e.Result = indice.IdIndice + " - " + indice.Descricao + " - " + indice.UpToFeederByIdFeeder.Descricao;
            }
        }
    }

    #endregion

    #region PopUp Serie Renda fixa
    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection serieRendaFixaColl = new SerieRendaFixaCollection();
        serieRendaFixaColl = this.obterSeries(null);
        e.Collection = serieRendaFixaColl;
    }

    protected void gridSerieRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idSerie = (int)gridView.GetRowValues(visibleIndex, SerieRendaFixaMetadata.ColumnNames.IdSerie);
        e.Result = idSerie.ToString();
    }

    protected void gridSerieRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridSerieRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridSerieRendaFixa.DataBind();
    }

    protected void btnEditSerieRendaFixa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void callBackPopupSerie_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idSerie = Convert.ToInt32(e.Parameter);

            SerieRendaFixa serieRendaFixa = this.obterSeries(idSerie)[0];

            if (serieRendaFixa.IdSerie.HasValue)
                e.Result = serieRendaFixa.IdSerie + " - " + serieRendaFixa.Descricao + " - " + serieRendaFixa.UpToFeederByIdFeeder.Descricao;
        }
    }

    #endregion

    #region Popup Tipo Curva RF

    protected void callBackPopupCurvaBase_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.CurvaGenerico_Callback(source, e);
    }

    protected void gridCurvaBase_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        this.CurvaGenerico_CustomDataCallback(sender, e);
    }

    protected void gridCurvaBase_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        this.CurvaGenerico_HtmlRowCreated(sender, e);
    }

    protected void EsDSCurvaBase_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        this.CurvaGenerico_esSelect(sender, e, true, false);
    }

    protected void gridCurvaBase_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCurvaBase.DataBind();
    }

    #endregion

    #region Popup Curva Spread

    protected void callBackPopupCurvaSpread_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.CurvaGenerico_Callback(source, e);
    }

    protected void gridCurvaSpread_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        this.CurvaGenerico_CustomDataCallback(sender, e);
    }

    protected void gridCurvaSpread_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        this.CurvaGenerico_HtmlRowCreated(sender, e);
    }

    protected void EsDSCurvaSpread_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        this.CurvaGenerico_esSelect(sender, e, false, true);
    }

    protected void gridCurvaSpread_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCurvaSpread.DataBind();
    }

    #endregion

    #region Genérico Curva Renda Fixa

    protected void CurvaGenerico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e, bool CurvaBase, bool CurvaSpread)
    {
        hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox; 
        CurvaRendaFixaCollection curvaRendaFixaColl = new CurvaRendaFixaCollection();

        curvaRendaFixaColl.Query.Select(curvaRendaFixaColl.Query.IdCurvaRendaFixa,
                                curvaRendaFixaColl.Query.Descricao);

        if (CurvaBase)
        {
            curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.TipoCurva.In(Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Pre),
                                                                                 Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Cupom_Limpo),
                                                                                 Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Composta),
                                                                                 Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Outros)));
        }
        else if(CurvaSpread)
        {
            curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.TipoCurva.In(Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Spread),
                                                                                 Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Fator)));
        }

        if(hiddenIdSerie != null && !string.IsNullOrEmpty(hiddenIdSerie.Text))
        {
            curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.IdCurvaRendaFixa != Convert.ToInt32(hiddenIdSerie.Text));
        }

        curvaRendaFixaColl.Query.OrderBy(curvaRendaFixaColl.Query.IdCurvaRendaFixa.Ascending);
        curvaRendaFixaColl.Load(curvaRendaFixaColl.Query);
        e.Collection = curvaRendaFixaColl;

    }

    protected void CurvaGenerico_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCurvaRendaFixa = Convert.ToInt32(e.Parameter);

            CurvaRendaFixaQuery curvaRendaFixaQuery = new CurvaRendaFixaQuery();
            CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();

            curvaRendaFixaQuery.Select(curvaRendaFixaQuery.IdCurvaRendaFixa, curvaRendaFixaQuery.Descricao);
            curvaRendaFixaQuery.Where(curvaRendaFixaQuery.IdCurvaRendaFixa == idCurvaRendaFixa);

            if (curvaRendaFixa.Load(curvaRendaFixaQuery))
                e.Result = curvaRendaFixa.IdCurvaRendaFixa.ToString() + " - " + curvaRendaFixa.Descricao.ToString();
            //                            
        }
    }

    protected void CurvaGenerico_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idCurvaRendaFixa = (int)gridView.GetRowValues(visibleIndex, CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa);
        e.Result = idCurvaRendaFixa.ToString();
    }

    protected void CurvaGenerico_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);
    }

    protected void EsDSCurvaRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CurvaRendaFixaQuery curvaRendaFixa = new CurvaRendaFixaQuery("curvaRendaFixa");
        CurvaRendaFixaQuery CurvaBaseQuery = new CurvaRendaFixaQuery("CurvaBase");
        IndiceQuery indiceQuery = new IndiceQuery("indiceQuery");
        CurvaRendaFixaQuery CurvaSpreadQuery = new CurvaRendaFixaQuery("curvaSpread");
        CurvaRendaFixaCollection curvaRendaFixaColl = new CurvaRendaFixaCollection();

        curvaRendaFixa.Select(curvaRendaFixa,
                                CurvaBaseQuery.Descricao.As("DescricaoCurvaBase"),
                                indiceQuery.Descricao.As("DescricaoIndice"),
                                CurvaSpreadQuery.Descricao.As("DescricaoCurvaSpread"));
        curvaRendaFixa.LeftJoin(indiceQuery).On(curvaRendaFixa.IdIndice == indiceQuery.IdIndice);
        curvaRendaFixa.LeftJoin(CurvaBaseQuery).On(curvaRendaFixa.IdCurvaBase == CurvaBaseQuery.IdCurvaRendaFixa);
        curvaRendaFixa.LeftJoin(CurvaSpreadQuery).On(curvaRendaFixa.IdCurvaSpread == CurvaSpreadQuery.IdCurvaRendaFixa);
        curvaRendaFixa.OrderBy(curvaRendaFixa.IdCurvaRendaFixa.Ascending);

        curvaRendaFixaColl.Load(curvaRendaFixa);

        e.Collection = curvaRendaFixaColl;
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        ExpressaoTaxaZero = gridCadastro.Columns["ExpressaoTaxaZero"] as GridViewDataComboBoxColumn;
        if (ExpressaoTaxaZero != null)
        {
            ExpressaoTaxaZero.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.ExpressaoTaxaZero)))
            {
                ExpressaoTaxaZero.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.ExpressaoTaxaZeroDescricao.RetornaStringValue(i), i);
            }
        }


        criterioInterpolacao = gridCadastro.Columns["CriterioInterpolacao"] as GridViewDataComboBoxColumn;
        if (criterioInterpolacao != null)
        {
            criterioInterpolacao.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.CriterioInterpolacao)))
            {
                criterioInterpolacao.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.CriterioInterpolacaoDescricao.RetornaStringValue(i), i);
            }
        }

        TipoCurva = gridCadastro.Columns["TipoCurva"] as GridViewDataComboBoxColumn;
        if (TipoCurva != null)
        {
            TipoCurva.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoCurva)))
            {
                TipoCurva.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.TipoCurvaDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idCurvaRendaFixa = (int)e.Keys[0];

        CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();

        if (curvaRendaFixa.LoadByPrimaryKey(idCurvaRendaFixa))
        {
            dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
            dropPermiteExtrapolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteExtrapolacao") as ASPxComboBox;
            dropcriterioInterpolacao = gridCadastro.FindEditFormTemplateControl("dropCriterioInterpolacao") as ASPxComboBox;
            dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
            dropTipoComposicao = gridCadastro.FindEditFormTemplateControl("dropTipoComposicao") as ASPxComboBox;
            dropExpressaoTaxaZero = gridCadastro.FindEditFormTemplateControl("dropExpressaoTaxaZero") as ASPxComboBox;
            hiddenIdCurvaBase = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaBase") as ASPxTextBox;
            hiddenIdCurvaSpread = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaSpread") as ASPxTextBox;
            hiddenIdIndice = gridCadastro.FindEditFormTemplateControl("hiddenIdIndice") as ASPxTextBox;

            curvaRendaFixa.PermiteInterpolacao = Convert.ToInt32(dropPermiteInterpolacao.SelectedItem.Value);
            curvaRendaFixa.PermiteExtrapolacao = Convert.ToInt32(dropPermiteExtrapolacao.SelectedItem.Value);
            curvaRendaFixa.CriterioInterpolacao = Convert.ToInt32(dropcriterioInterpolacao.SelectedItem.Value);
            curvaRendaFixa.TipoCurva = Convert.ToInt32(dropTipoCurva.SelectedItem.Value);
            curvaRendaFixa.ExpressaoTaxaZero = Convert.ToInt32(dropExpressaoTaxaZero.SelectedItem.Value);

            if (dropTipoComposicao.SelectedIndex == 0)
                curvaRendaFixa.TipoComposicao = null;
            else
                curvaRendaFixa.TipoComposicao = Convert.ToInt32(dropTipoComposicao.SelectedItem.Value);

            if (!string.IsNullOrEmpty(hiddenIdCurvaBase.Text))
                curvaRendaFixa.IdCurvaBase = Convert.ToInt32(hiddenIdCurvaBase.Text);
            else
                curvaRendaFixa.IdCurvaBase = null;

            if (!string.IsNullOrEmpty(hiddenIdCurvaSpread.Text))
                curvaRendaFixa.IdCurvaSpread = Convert.ToInt32(hiddenIdCurvaSpread.Text);
            else
                curvaRendaFixa.IdCurvaSpread = null;

            if (!string.IsNullOrEmpty(hiddenIdIndice.Text))
                curvaRendaFixa.IdIndice = Convert.ToInt16(hiddenIdIndice.Text);
            else
                curvaRendaFixa.IdIndice = null;

            curvaRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CurvaRendaFixa - Operacao: Update CurvaRendaFixa: " + idCurvaRendaFixa + UtilitarioWeb.ToString(curvaRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues("IdCurvaRendaFixa");
            for (int i = 0; i < keyValues.Count; i++)
            {
                int IdCurvaRendaFixa = Convert.ToInt32(keyValues[i]);

                CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
                if (curvaRendaFixa.LoadByPrimaryKey(IdCurvaRendaFixa))
                {
                    CurvaRendaFixa curvaRendaFixaClone = (CurvaRendaFixa)Utilitario.Clone(curvaRendaFixa);

                    curvaRendaFixa.MarkAsDeleted();
                    curvaRendaFixa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CurvaRendaFixa - Operacao: Delete CurvaRendaFixa: " + IdCurvaRendaFixa + UtilitarioWeb.ToString(curvaRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        #region Obrigatórios
        hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox;        
        dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
        dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
        dropcriterioInterpolacao = gridCadastro.FindEditFormTemplateControl("dropcriterioInterpolacao") as ASPxComboBox;
        dropExpressaoTaxaZero = gridCadastro.FindEditFormTemplateControl("dropExpressaoTaxaZero") as ASPxComboBox;
        #endregion

        #region Validações
        hiddenIdCurvaBase = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaBase") as ASPxTextBox;
        hiddenIdIndice = gridCadastro.FindEditFormTemplateControl("hiddenIdIndice") as ASPxTextBox;
        dropTipoComposicao = gridCadastro.FindEditFormTemplateControl("dropTipoComposicao") as ASPxComboBox;
        hiddenIdCurvaSpread = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaSpread") as ASPxTextBox;
        #endregion

        if (e.Parameter.Equals("btnDelete"))
        {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues("IdCurvaRendaFixa");
            for (int i = 0; i < keyValues.Count; i++)
            {
                int IdCurvaRendaFixa = Convert.ToInt32(keyValues[i]);
                CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();                

                if (curvaRendaFixa.LoadByPrimaryKey(IdCurvaRendaFixa))
                {
                    #region Verifica se Existe registro relacionado
                    CurvaRendaFixaCollection curvaRendaFixaCollection = new CurvaRendaFixaCollection();
                    curvaRendaFixaCollection.Query.Where(curvaRendaFixaCollection.Query.IdCurvaBase == IdCurvaRendaFixa);
                    curvaRendaFixaCollection.Query.Where(curvaRendaFixaCollection.Query.IdCurvaRendaFixa != IdCurvaRendaFixa);
                    curvaRendaFixaCollection.Load(curvaRendaFixaCollection.Query);

                    if (curvaRendaFixaCollection.Count > 0)
                    {
                        e.Result = "Curva" + curvaRendaFixa.Descricao + " não pode ser excluído pois está sendo usada como Curva Base em outro registro!";
                    }

                    curvaRendaFixaCollection = new CurvaRendaFixaCollection();
                    curvaRendaFixaCollection.Query.Where(curvaRendaFixaCollection.Query.IdCurvaSpread == IdCurvaRendaFixa);
                    curvaRendaFixaCollection.Query.Where(curvaRendaFixaCollection.Query.IdCurvaRendaFixa != IdCurvaRendaFixa);
                    curvaRendaFixaCollection.Load(curvaRendaFixaCollection.Query);

                    if (curvaRendaFixaCollection.Count > 0)
                    {
                        e.Result = "Curva" + curvaRendaFixa.Descricao + " não pode ser excluído pois está sendo usada Curva Spread em outro registro!";
                    }
                    #endregion
                }
            }
        }
        else
        {

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(hiddenIdSerie);
            controles.Add(dropPermiteInterpolacao);
            controles.Add(dropTipoCurva);
            controles.Add(dropPermiteInterpolacao);
            controles.Add(dropcriterioInterpolacao);
            controles.Add(dropExpressaoTaxaZero);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            // Somente se for Insert
            if (gridCadastro.IsNewRowEditing && !string.IsNullOrEmpty(hiddenIdSerie.Text))
            {
                CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
                if (curvaRendaFixa.LoadByPrimaryKey(Convert.ToInt32(hiddenIdSerie.Text)))
                {
                    e.Result = "Registro já existente.!";
                    return;
                }
            }

            int IdCurvaBase = (string.IsNullOrEmpty(hiddenIdCurvaBase.Text) ? 0 : Convert.ToInt32(hiddenIdCurvaBase.Text));
            int IdCurvaSpread = (string.IsNullOrEmpty(hiddenIdCurvaSpread.Text) ? 0 : Convert.ToInt32(hiddenIdCurvaSpread.Text));
            int IdIndice = (string.IsNullOrEmpty(hiddenIdIndice.Text) ? 0 : Convert.ToInt32(hiddenIdIndice.Text));

            if (Convert.ToInt32(dropTipoCurva.SelectedItem.Value) == Convert.ToInt32(Financial.RendaFixa.Enums.TipoCurva.Composta))
            {
                if (dropTipoComposicao.SelectedIndex == 0)
                {
                    e.Result = "Favor preencher Tipo de Composição!";
                    return;
                }

                if (IdCurvaSpread == 0)
                {
                    e.Result = "Favor preencher Curva Spread!";
                    return;
                }

                if (IdCurvaBase > 0 && IdIndice > 0)
                {
                    e.Result = "Favor preencher Curva Base ou Indexador Curva Base!";
                    return;
                }

                if (IdCurvaBase == 0 && IdIndice == 0)
                {
                    e.Result = "Favor preencher Informações sobre Curva Base!";
                    return;
                }
            }
            else
            {
                if (IdCurvaBase + IdCurvaSpread + IdIndice > 0 || dropTipoComposicao.SelectedIndex != 0)
                {
                    e.Result = "Campos Curva Base/Spread, Indexador de Curva Base e Tipo de Composição só podem ser preenchidos para Tipo de Curva 'Composta'!";
                    return;
                }
            }
        }
    }

    protected void dropPermiteInterpolacao_Load(object sender, EventArgs e)
    {
        dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
        if (dropPermiteInterpolacao != null)
        {
            dropPermiteInterpolacao.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Common.Enums.ExecutaFuncao)))
            {
                dropPermiteInterpolacao.Items.Add(Financial.Common.Enums.ExecutaFuncaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropPermiteExtrapolacao_Load(object sender, EventArgs e)
    {
        dropPermiteExtrapolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteExtrapolacao") as ASPxComboBox;
        if (dropPermiteExtrapolacao != null)
        {
            dropPermiteExtrapolacao.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.Common.Enums.ExecutaFuncao)))
            {
                dropPermiteExtrapolacao.Items.Add(Financial.Common.Enums.ExecutaFuncaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropCriterioInterpolacao_Load(object sender, EventArgs e)
    {
        dropcriterioInterpolacao = gridCadastro.FindEditFormTemplateControl("dropCriterioInterpolacao") as ASPxComboBox;
        if (dropcriterioInterpolacao != null)
        {
            dropcriterioInterpolacao.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.CriterioInterpolacao)))
            {
                dropcriterioInterpolacao.Items.Add(Financial.RendaFixa.Enums.CriterioInterpolacaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropExpressaoTaxaZero_Load(object sender, EventArgs e)
    {
        dropExpressaoTaxaZero = gridCadastro.FindEditFormTemplateControl("dropExpressaoTaxaZero") as ASPxComboBox;
        if (dropExpressaoTaxaZero != null)
        {
            dropExpressaoTaxaZero.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.ExpressaoTaxaZero)))
            {
                dropExpressaoTaxaZero.Items.Add(Financial.RendaFixa.Enums.ExpressaoTaxaZeroDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropTipoCurva_Load(object sender, EventArgs e)
    {
        dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        if (dropTipoCurva != null)
        {
            dropTipoCurva.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoCurva)))
            {
                dropTipoCurva.Items.Add(Financial.RendaFixa.Enums.TipoCurvaDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void dropTipoComposicao_Load(object sender, EventArgs e)
    {
        dropTipoComposicao = gridCadastro.FindEditFormTemplateControl("dropTipoComposicao") as ASPxComboBox;
        if (dropTipoComposicao != null)
        {
            dropTipoComposicao.Items.Clear();
            dropTipoComposicao.Items.Add("", 0);
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.TipoComposicao)))
            {
                dropTipoComposicao.Items.Add(Financial.RendaFixa.Enums.TipoComposicaoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        hiddenIdSerie = gridCadastro.FindEditFormTemplateControl("hiddenIdSerie") as ASPxTextBox;
        dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
        dropPermiteInterpolacao = gridCadastro.FindEditFormTemplateControl("dropPermiteInterpolacao") as ASPxComboBox;
        dropcriterioInterpolacao = gridCadastro.FindEditFormTemplateControl("dropcriterioInterpolacao") as ASPxComboBox;
        dropTipoCurva = gridCadastro.FindEditFormTemplateControl("dropTipoCurva") as ASPxComboBox;
        dropExpressaoTaxaZero = gridCadastro.FindEditFormTemplateControl("dropExpressaoTaxaZero") as ASPxComboBox;
        dropTipoComposicao = gridCadastro.FindEditFormTemplateControl("dropTipoComposicao") as ASPxComboBox;
        hiddenIdCurvaBase = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaBase") as ASPxTextBox;
        hiddenIdCurvaSpread = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaSpread") as ASPxTextBox;
        hiddenIdIndice = gridCadastro.FindEditFormTemplateControl("hiddenIdIndice") as ASPxTextBox;

        CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();

        int idSerie = Convert.ToInt32(hiddenIdSerie.Text);
        curvaRendaFixa.IdCurvaRendaFixa = idSerie;
        curvaRendaFixa.Descricao = obterDescricaoSerie(idSerie);
        curvaRendaFixa.PermiteExtrapolacao = Convert.ToInt32(dropPermiteInterpolacao.SelectedItem.Value);
        curvaRendaFixa.ExpressaoTaxaZero = Convert.ToInt32(dropExpressaoTaxaZero.SelectedItem.Value);
        curvaRendaFixa.CriterioInterpolacao = Convert.ToInt32(dropcriterioInterpolacao.SelectedItem.Value);
        curvaRendaFixa.PermiteInterpolacao = Convert.ToInt32(dropPermiteInterpolacao.SelectedItem.Value);
        curvaRendaFixa.TipoCurva = Convert.ToInt32(dropTipoCurva.SelectedItem.Value);

        if (dropTipoComposicao.SelectedIndex == 0)
            curvaRendaFixa.TipoComposicao = null;
        else
            curvaRendaFixa.TipoComposicao = Convert.ToInt32(dropTipoComposicao.SelectedItem.Value);

        if (!string.IsNullOrEmpty(hiddenIdCurvaBase.Text))
            curvaRendaFixa.IdCurvaBase = Convert.ToInt32(hiddenIdCurvaBase.Text);
        else
            curvaRendaFixa.IdCurvaBase = null;

        if (!string.IsNullOrEmpty(hiddenIdCurvaSpread.Text))
            curvaRendaFixa.IdCurvaSpread = Convert.ToInt32(hiddenIdCurvaSpread.Text);
        else
            curvaRendaFixa.IdCurvaSpread = null;

        if (!string.IsNullOrEmpty(hiddenIdIndice.Text))
            curvaRendaFixa.IdIndice = Convert.ToInt16(hiddenIdIndice.Text);
        else
            curvaRendaFixa.IdIndice = null;

        curvaRendaFixa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CurvaRendaFixa - Operacao: Insert CurvaRendaFixa: " + curvaRendaFixa.IdCurvaRendaFixa + UtilitarioWeb.ToString(curvaRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private string obterDescricaoSerie(int idSerie)
    {
        SerieRendaFixa serieRendaFixa = new SerieRendaFixa();
        serieRendaFixa = this.obterSeries(idSerie)[0];

        return serieRendaFixa.Descricao + " - " + serieRendaFixa.UpToFeederByIdFeeder.Descricao;
    }

    private SerieRendaFixaCollection obterSeries(int? IdSerie)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        FeederQuery feederQuery = new FeederQuery("feeder");
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serieRendaFixa");

        serieRendaFixaQuery.Select(serieRendaFixaQuery,
                                   feederQuery.Descricao.As("DescricaoFeeder"));
        serieRendaFixaQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);
        serieRendaFixaQuery.Where(serieRendaFixaQuery.TipoTaxa == (int)Financial.RendaFixa.Enums.TipoMTMTitulo.Curva);
        if (IdSerie.HasValue)
            serieRendaFixaQuery.Where(serieRendaFixaQuery.IdSerie == IdSerie);

        serieRendaFixaQuery.OrderBy(serieRendaFixaQuery.IdSerie.Ascending);

        coll.Load(serieRendaFixaQuery);

        return coll;
    }

    private string obterDescricaoIndice(int idIndice)
    {
        Indice indice = new Indice();
        indice = this.obterIndices(idIndice)[0];

        return indice.Descricao + " - " + indice.UpToFeederByIdFeeder.Descricao;
    }

    private IndiceCollection obterIndices(int? IdIndice)
    {
        IndiceCollection coll = new IndiceCollection();
        FeederQuery feederQuery = new FeederQuery("feeder");
        IndiceQuery indiceQuery = new IndiceQuery("Indice");

        indiceQuery.Select(indiceQuery,
                           feederQuery.Descricao.As("DescricaoFeeder"));
        indiceQuery.InnerJoin(feederQuery).On(indiceQuery.IdFeeder == feederQuery.IdFeeder);

        if (IdIndice.HasValue)
            indiceQuery.Where(indiceQuery.IdIndice == IdIndice);

        indiceQuery.OrderBy(indiceQuery.IdIndice.Ascending);

        coll.Load(indiceQuery);

        return coll;
    }
}