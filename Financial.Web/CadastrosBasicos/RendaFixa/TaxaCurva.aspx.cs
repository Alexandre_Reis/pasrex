﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;
using Financial.RendaFixa.Enums;

public partial class CadastrosBasicos_TaxaCurva : CadastroBasePage
{

    #region Grid Cadastro
    GridViewDataTextColumn IdCurvaRendaFixa;
    GridViewDataTextColumn DataBase;
    GridViewDataTextColumn DataVertice;
    GridViewDataTextColumn CodigoVertice;
    GridViewDataTextColumn PrazoDU;
    GridViewDataTextColumn PrazoDC;
    GridViewDataTextColumn Taxa;
    GridViewDataTextColumn CompositeKey;
    #endregion

    #region Template Edit
    ASPxDateEdit textDataBase;
    ASPxDateEdit textDataVertice;
    ASPxTextBox hiddenIdCurvaRendaFixa;
    ASPxTextBox textCodigoVertice;
    ASPxSpinEdit textPrazoDU;
    ASPxSpinEdit textPrazoDC;
    ASPxSpinEdit textTaxa;
    #endregion

    #region Popup Tipo Curva RF

    protected void callBackPopupCurvaRendaFixa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.CurvaRFGenerico_Callback(source, e);
    }

    protected void gridCurvaRendaFixa_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        this.CurvaRFGenerico_CustomDataCallback(sender, e);
    }

    protected void gridCurvaRendaFixa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        this.CurvaRFGenerico_HtmlRowCreated(sender, e);
    }

    protected void EsDSCurvaRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        this.CurvaRFGenerico_esSelect(sender, e);
    }

    protected void gridCurvaRendaFixa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCurvaRendaFixa.DataBind();
    }

    #endregion

    #region Genérico Curva Renda Fixa
    protected void CurvaRFGenerico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CurvaRendaFixaCollection curvaRendaFixaColl = new CurvaRendaFixaCollection();

        curvaRendaFixaColl.Query.Select(curvaRendaFixaColl.Query.IdCurvaRendaFixa,
                                curvaRendaFixaColl.Query.Descricao);
        curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.TipoCurva.NotEqual((int)TipoCurva.Composta));
        curvaRendaFixaColl.Query.OrderBy(curvaRendaFixaColl.Query.IdCurvaRendaFixa.Ascending);
        curvaRendaFixaColl.LoadAll();
        e.Collection = curvaRendaFixaColl;

    }

    protected void CurvaRFGenerico_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCurvaRendaFixa = Convert.ToInt32(e.Parameter);

            CurvaRendaFixaQuery curvaRendaFixaQuery = new CurvaRendaFixaQuery();
            CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();

            curvaRendaFixaQuery.Select(curvaRendaFixaQuery.IdCurvaRendaFixa, curvaRendaFixaQuery.Descricao);
            curvaRendaFixaQuery.Where(curvaRendaFixaQuery.IdCurvaRendaFixa == idCurvaRendaFixa);

            if (curvaRendaFixa.Load(curvaRendaFixaQuery))
                e.Result = curvaRendaFixa.IdCurvaRendaFixa.ToString() + " - " + curvaRendaFixa.Descricao.ToString();
            //                            
        }
    }

    protected void CurvaRFGenerico_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idCurvaRendaFixa = (int)gridView.GetRowValues(visibleIndex, CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa);
        e.Result = idCurvaRendaFixa.ToString();
    }

    protected void CurvaRFGenerico_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTaxaCurva_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TaxaCurvaCollection coll = new TaxaCurvaCollection();

        CurvaRendaFixaQuery curvaRendaFixaQuery = new CurvaRendaFixaQuery("CurvaRendaFixaQuery");
        TaxaCurvaQuery TaxaCurvaQuery = new TaxaCurvaQuery("TaxaCurva");

        TaxaCurvaQuery.Select(TaxaCurvaQuery,
                              curvaRendaFixaQuery.IdCurvaRendaFixa,
                              curvaRendaFixaQuery.Descricao.As("DescricaoCurvaRendaFixa"));
        TaxaCurvaQuery.InnerJoin(curvaRendaFixaQuery).On(curvaRendaFixaQuery.IdCurvaRendaFixa == TaxaCurvaQuery.IdCurvaRendaFixa);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            TaxaCurvaQuery.Where(TaxaCurvaQuery.DataBase.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            TaxaCurvaQuery.Where(TaxaCurvaQuery.DataBase.LessThanOrEqual(textDataFim.Text));
        }
        TaxaCurvaQuery.OrderBy(coll.Query.DataBase.Descending);

        coll.Load(TaxaCurvaQuery);

        e.Collection = coll;
    }

    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        FeederQuery feederQuery = new FeederQuery("feeder");

        serieRendaFixaQuery.Select(serieRendaFixaQuery,
                                  (serieRendaFixaQuery.Descricao + " - " + feederQuery.Descricao).As("DescricaoCompleta"));
        serieRendaFixaQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);

        serieRendaFixaQuery.OrderBy(coll.Query.Descricao.Ascending);
        coll.Load(serieRendaFixaQuery);

        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        textDataVertice = gridCadastro.FindEditFormTemplateControl("textDataVertice") as ASPxDateEdit;
        textDataBase = gridCadastro.FindEditFormTemplateControl("textDataBase") as ASPxDateEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        hiddenIdCurvaRendaFixa = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaRendaFixa") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDataVertice);
        controles.Add(textDataBase);
        controles.Add(textTaxa);
        controles.Add(hiddenIdCurvaRendaFixa);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        DateTime dataVertice = Convert.ToDateTime(textDataVertice.Text);
        DateTime dataBase = Convert.ToDateTime(textDataBase.Text);

        if (!Calendario.IsDiaUtil(dataVertice) || !Calendario.IsDiaUtil(dataBase))
        {
            e.Result = "As datas devem ser úteis!";
            return;
        }

        if (dataVertice <= dataBase)
        {
            e.Result = "Data Vértice deve ser maior que data Base!";
            return;
        } 

        if (gridCadastro.IsNewRowEditing)
        {
            int idCurvaRendaFixa = Convert.ToInt32(hiddenIdCurvaRendaFixa.Text);
            dataBase = Convert.ToDateTime(textDataBase.Text);

            TaxaCurva TaxaCurva = new TaxaCurva();
            if (TaxaCurva.LoadByPrimaryKey(dataBase, dataVertice, idCurvaRendaFixa))
            {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void textDataBase_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void btnEditCurvaRendaFixa_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxButtonEdit).Enabled = false;
    }

    protected void dropSerie_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn DigitadoImportado = gridCadastro.Columns["DigitadoImportado"] as GridViewDataComboBoxColumn;
        if (DigitadoImportado != null)
        {
            DigitadoImportado.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.DigitadoImportado)))
            {
                DigitadoImportado.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.DigitadoImportadoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TaxaCurva taxaCurva = new TaxaCurva();

        textDataVertice = gridCadastro.FindEditFormTemplateControl("textDataVertice") as ASPxDateEdit;
        textDataBase = gridCadastro.FindEditFormTemplateControl("textDataBase") as ASPxDateEdit;
        textPrazoDC = gridCadastro.FindEditFormTemplateControl("textPrazoDC") as ASPxSpinEdit;
        textPrazoDU = gridCadastro.FindEditFormTemplateControl("textPrazoDU") as ASPxSpinEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        hiddenIdCurvaRendaFixa = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaRendaFixa") as ASPxTextBox;
        textCodigoVertice = gridCadastro.FindEditFormTemplateControl("textCodigoVertice") as ASPxTextBox;

        int IdCurvaRendaFixa = Convert.ToInt32(hiddenIdCurvaRendaFixa.Text);
        DateTime dataBase = Convert.ToDateTime(textDataBase.Text);
        DateTime dataVertice = Convert.ToDateTime(textDataVertice.Text);
        if (taxaCurva.LoadByPrimaryKey(dataBase, dataVertice, IdCurvaRendaFixa))
        {
            DateTime DataBase = Convert.ToDateTime(textDataBase.Text);
            DateTime DataVertice = Convert.ToDateTime(textDataVertice.Text);

            taxaCurva.PrazoDC = Calendario.NumeroDias(DataBase, DataVertice);
            taxaCurva.PrazoDU = Calendario.NumeroDias(DataBase, DataVertice, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            taxaCurva.DataVertice = DataVertice;
            taxaCurva.Taxa = Convert.ToDecimal(textTaxa.Text);
            taxaCurva.CodigoVertice = textCodigoVertice.Text;
            taxaCurva.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TaxaCurva - Operacao: Update TaxaCurva: " + dataBase + "; " + IdCurvaRendaFixa + UtilitarioWeb.ToString(taxaCurva),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            this.ReprocessaCurva(DataBase, taxaCurva.IdCurvaRendaFixa.Value);
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        TaxaCurva taxaCurva = new TaxaCurva();

        textDataVertice = gridCadastro.FindEditFormTemplateControl("textDataVertice") as ASPxDateEdit;
        textDataBase = gridCadastro.FindEditFormTemplateControl("textDataBase") as ASPxDateEdit;
        textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        hiddenIdCurvaRendaFixa = gridCadastro.FindEditFormTemplateControl("hiddenIdCurvaRendaFixa") as ASPxTextBox;
        textCodigoVertice = gridCadastro.FindEditFormTemplateControl("textCodigoVertice") as ASPxTextBox;

        DateTime DataBase = Convert.ToDateTime(textDataBase.Text);
        DateTime DataVertice = Convert.ToDateTime(textDataVertice.Text);

        taxaCurva.IdCurvaRendaFixa = Convert.ToInt32(hiddenIdCurvaRendaFixa.Text);
        taxaCurva.PrazoDC = Calendario.NumeroDias(DataBase, DataVertice);
        taxaCurva.PrazoDU = Calendario.NumeroDias(DataBase, DataVertice, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        taxaCurva.DataVertice = DataVertice;
        taxaCurva.DataBase = DataBase;
        taxaCurva.CodigoVertice = textCodigoVertice.Text;
        taxaCurva.Taxa = Convert.ToDecimal(textTaxa.Text);

        taxaCurva.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TaxaCurva - Operacao: Insert TaxaCurva: " + taxaCurva.DataBase + "; " + taxaCurva.IdCurvaRendaFixa + UtilitarioWeb.ToString(taxaCurva),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        this.ReprocessaCurva(DataBase, taxaCurva.IdCurvaRendaFixa.Value);
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string IdCurvaRendaFixa = Convert.ToString(e.GetListSourceFieldValue(TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa));
            string DataBase = Convert.ToString(e.GetListSourceFieldValue(TaxaCurvaMetadata.ColumnNames.DataBase));
            string DataVertice = Convert.ToString(e.GetListSourceFieldValue(TaxaCurvaMetadata.ColumnNames.DataVertice));
            e.Value = IdCurvaRendaFixa + "|" + DataBase + "|" + DataVertice;
        }
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("CompositeKey");
            List<int> lstIDsCurvas = new List<int>();
            List<DateTime> lstDateTime = new List<DateTime>();
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                string[] chave = keyValuesId[i].ToString().Split('|');

                int IdCurvaRendaFixa = Convert.ToInt32(chave[0]);
                DateTime dataBase = Convert.ToDateTime(chave[1]);
                DateTime dataVertice = Convert.ToDateTime(chave[2]);

                TaxaCurva taxaCurva = new TaxaCurva();
                if (taxaCurva.LoadByPrimaryKey(dataBase, dataVertice, IdCurvaRendaFixa))
                {

                    TaxaCurva taxaCurvaClone = (TaxaCurva)Utilitario.Clone(taxaCurva);
                    //

                    taxaCurva.MarkAsDeleted();
                    taxaCurva.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TaxaCurva - Operacao: Delete TaxaCurva: " + dataBase + "; " + dataVertice+ IdCurvaRendaFixa + "; " + UtilitarioWeb.ToString(taxaCurvaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion   
                 
                    if (!lstIDsCurvas.Contains(IdCurvaRendaFixa))
                    {
                        lstIDsCurvas.Add(IdCurvaRendaFixa);
                        lstDateTime.Add(dataBase);
                    }
                }
            }

            for (int i = 0; i < lstIDsCurvas.Count; i++)
            {
                int IdCurvaRendaFixa = lstIDsCurvas[i];
                DateTime dataBase = lstDateTime[i];

                this.ReprocessaCurva(dataBase, IdCurvaRendaFixa);
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDataBase", "textTaxa");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        if (texto.Length > 0)
            labelFiltro.Text = texto.ToString();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textDataBase") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
            e.Properties["cpTextTaxa"] = textTaxa.ClientID;
        }
    }

    private void ReprocessaCurva(DateTime data, int idCurva)
    {
        CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
        curvaRendaFixa.LoadByPrimaryKey(idCurva);

        Hashtable hsTaxasCurvas= new Hashtable(); 
        Hashtable hsTaxasCurvasBase= new Hashtable();

        List<int> lstTipoCurvaSpread = new List<int>();
        lstTipoCurvaSpread.Add((int)Financial.RendaFixa.Enums.TipoCurva.Spread);
        lstTipoCurvaSpread.Add((int)Financial.RendaFixa.Enums.TipoCurva.Fator);

        bool CurvaSpread = lstTipoCurvaSpread.Contains(curvaRendaFixa.TipoCurva.Value);

        curvaRendaFixa.ReprocessaCurvaComposta(data, idCurva, CurvaSpread, ref hsTaxasCurvas, ref hsTaxasCurvasBase);

    }
}