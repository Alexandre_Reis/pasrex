﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxaCurva.aspx.cs" Inherits="CadastrosBasicos_TaxaCurva" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDownWithCallback;
        var operacao = '';

        function OnGetDataCurvaRendaFixa(data) {
            hiddenIdCurvaRendaFixa.SetValue(data);
            callBackPopupCurvaRendaFixa.SendCallback(data);
            popupCurvaRendaFixa.HideWindow();
            btnEditCurvaRendaFixa.Focus();
        }           
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackPopupCurvaRendaFixa" runat="server" OnCallback="callBackPopupCurvaRendaFixa_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCurvaRendaFixa.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupCurvaRendaFixa" ClientInstanceName="popupCurvaRendaFixa"
        runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlCurvaRendaFixa" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridCurvaRendaFixa" runat="server" Width="100%" ClientInstanceName="gridCurvaRendaFixa"
                        AutoGenerateColumns="False" DataSourceID="EsDSCurvaRendaFixa" KeyFieldName="IdCurvaRendaFixaPopup"
                        OnCustomDataCallback="gridCurvaRendaFixa_CustomDataCallback" OnCustomCallback="gridCurvaRendaFixa_CustomCallback"
                        OnHtmlRowCreated="gridCurvaRendaFixa_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" Caption="Curva RF" VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridCurvaRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCurvaRendaFixa);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Tipo Curva." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCurvaRendaFixa.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Taxas de Curvas - Renda Fixa"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                            <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                            </dxpc:ASPxPopupControl>
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                    <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                    CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton></div>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="CompositeKey"
                                    DataSourceID="EsDSTaxaCurva" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                    OnCustomJSProperties="gridCadastro_CustomJSProperties" OnLoad="gridCadastro_Load"
                                    OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"  
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" ReadOnly="True" VisibleIndex="1"
                                            Visible="true" Caption="ID" Width="3%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCurvaRendaFixa" Visible="true"
                                            Caption="Curva" Width="30%" VisibleIndex="2" />
                                        <dxwgv:GridViewDataDateColumn FieldName="DataBase" VisibleIndex="3" ReadOnly="True"
                                            Caption="Dt.Base" />
                                        <dxwgv:GridViewDataDateColumn FieldName="DataVertice" VisibleIndex="4" Caption="Dt.Vértice">
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="CodigoVertice" VisibleIndex="5" Caption="Código Vértice" />
                                        <dxwgv:GridViewDataTextColumn FieldName="PrazoDU" VisibleIndex="6">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="PrazoDC" VisibleIndex="7">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="8">
                                        </dxwgv:GridViewDataTextColumn>
                                         <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />   
                                    </Columns>
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                        <EditForm>
                                            <div class="editForm">
                                                <dxe:ASPxTextBox ID="hiddenIdCurvaRendaFixa" runat="server" CssClass="hiddenField"
                                                    ClientInstanceName="hiddenIdCurvaRendaFixa" Text='<%#Eval("IdCurvaRendaFixa")%>' />
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Curva:" />
                                                        </td>
                                                        <td colspan="3">
                                                            <dxe:ASPxButtonEdit ID="btnEditCurvaRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCurvaRendaFixa" ReadOnly="true"
                                                                OnInit="btnEditCurvaRendaFixa_Init" Width="100%" Text='<%# (Eval("IdCurvaRendaFixa") != null ? Eval("IdCurvaRendaFixa") + "-" + Eval("DescricaoCurvaRendaFixa") : " ") %>'>
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupCurvaRendaFixa.ShowAtElementByID(s.name);}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelDataBase" runat="server" CssClass="labelRequired" Text="Data Base:"></asp:Label>
                                                        </td>
                                                        <td colspan="1">
                                                            <dxe:ASPxDateEdit ID="textDataBase" runat="server" ClientInstanceName="textDataBase"
                                                                Value='<%#Eval("DataBase")%>' OnInit="textDataBase_Init" />
                                                        </td>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelDataVertice" runat="server" CssClass="labelRequired" Text="Data Vértice:"></asp:Label>
                                                        </td>
                                                        <td colspan="1">
                                                            <dxe:ASPxDateEdit ID="textDataVertice" runat="server" ClientInstanceName="textDataVertice"
                                                                Value='<%#Eval("DataVertice")%>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelTaxa" runat="server" CssClass="labelRequired" Text="Taxa:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textTaxa" runat="server" ClientInstanceName="textTaxa" CssClass="textTaxa_5"
                                                                Text='<%# Eval("Taxa") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCodigoVertice" runat="server" AssociatedControlID="textCodigoVertice"
                                                                CssClass="labelNormal" Text="Código Vértice:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textCodigoVertice" ClientInstanceName="textCodigoVertice" runat="server"
                                                                CssClass="textCurto" MaxLength="50" Text='<%#Eval("CodigoVertice")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal7" runat="server" Text="OK+" /><div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton></div>
                                            </div>
                                        </EditForm>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="250px" />
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                    </Templates>
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    <cc1:esDataSource ID="EsDSTaxaCurva" runat="server" OnesSelect="EsDSTaxaCurva_esSelect" />
    <cc1:esDataSource ID="EsDSCurvaRendaFixa" runat="server" OnesSelect="EsDSCurvaRendaFixa_esSelect" />
    </form>
</body>
</html>
