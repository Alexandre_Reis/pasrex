﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SerieRendaFixa.aspx.cs" Inherits="CadastrosBasicos_SerieRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';

        function OnGetDataFeeder(data) {
            hiddenIdFeeder.SetValue(data);
            callBackPopupFeeder.SendCallback(data);
            popupFeeder.HideWindow();
            btnEditFeeder.Focus();
        }
                
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackPopupFeeder" runat="server" OnCallback="callBackPopupFeeder_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditFeeder.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupFeeder" ClientInstanceName="popupFeeder" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControFeeder" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridFeeder" runat="server" Width="100%" ClientInstanceName="gridFeeder"
                        AutoGenerateColumns="False" DataSourceID="EsDSFeeder" KeyFieldName="IdFeederPopup"
                        OnCustomDataCallback="gridFeeder_CustomDataCallback" OnCustomCallback="gridFeeder_CustomCallback"
                        OnHtmlRowCreated="gridFeeder_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdFeeder" Caption="Feeder" VisibleIndex="1"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="2"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridFeeder.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataFeeder);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Feeder." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridFeeder.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Séries de Renda Fixa"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true)  
                                                                                               {callbackErro.SendCallback('btnDelete'); operacao='deletar';}
                                                                                               return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                    CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton></div><div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdSerie" DataSourceID="EsDSSerieRendaFixa"
                                    OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                    OnRowInserting="gridCadastro_RowInserting" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                    OnBeforeGetCallbackResult="gridCadastro_PreRender" AutoGenerateColumns="False">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdSerie" ReadOnly="True" VisibleIndex="0" Width="10%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Descrição" FieldName="Descricao" VisibleIndex="1">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Série" FieldName="TipoTaxa" VisibleIndex="2">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Feeder" FieldName="DescricaoFeeder" VisibleIndex="3">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdFeeder" Visible="False" VisibleIndex="4">
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <div class="editForm">
                                                <dxe:ASPxTextBox ID="hiddenIdSerie" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdSerie"
                                                    Text='<%#Eval("IdSerie")%>' />
                                                <dxe:ASPxTextBox ID="hiddenIdFeeder" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdFeeder"
                                                    Text='<%#Eval("IdFeeder")%>' />
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao"
                                                                CssClass="labelRequired" Text="Descrição:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                Width="100%" MaxLength="50" Text='<%#Eval("Descricao")%>'>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTipoTaxa" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoTaxa"
                                                                Text="Tipo Série:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipoTaxa" Width="100%" runat="server" CssClass="dropDownListCurto"
                                                                Text='<%#Eval("TipoTaxa")%>' OnInit="dropTipoTaxa_Init">
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelFeeder" runat="server" CssClass="labelRequired" Text="Feeder:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditFeeder" runat="server" CssClass="textButtonEdit" EnableClientSideAPI="True"
                                                                Text='<%#(Eval("IdFeeder") != null ? "Feeder: " + Eval("IdFeeder") + "-" + Eval("DescricaoFeeder") : " ")%>'
                                                                ClientInstanceName="btnEditFeeder" ReadOnly="true" Width="380px">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupFeeder.ShowAtElementByID(s.name);}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="lblCodigoBDS" runat="server" AssociatedControlID="lblCodigoBDS"
                                                                CssClass="labelNormal" Text="Código Integração:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textCodigoBDS" ClientInstanceName="textCodigoBDS" runat="server" NumberType="Integer"
                                                                MaxLength="50" Text='<%#Eval("CodigoBDS")%>' Width="100%">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>                                                                                                        
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                        <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton></div></div></EditForm></Templates>
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton></dxwgv:ASPxGridView></div></div></div></td></tr></table></div><dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    <cc1:esDataSource ID="EsDSSerieRendaFixa" runat="server" OnesSelect="EsDSSerieRendaFixa_esSelect" />
    <cc1:esDataSource ID="EsDSFeeder" runat="server" OnesSelect="EsDSFeeder_esSelect" />
    </form>
</body>
</html>
