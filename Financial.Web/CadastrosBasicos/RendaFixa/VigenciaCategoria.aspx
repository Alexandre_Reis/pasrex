﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VigenciaCategoria.aspx.cs"
    Inherits="CadastrosBasicos_VigenciaCategoria" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';

        function OnGetDataOperacaoRendaFixa(data) 
        {
            hiddenIdOperacao.SetValue(data);
            callBackPopupOperacaoRendaFixa.SendCallback(data);
            popupOperacaoRendaFixa.HideWindow();
            btnEditOperacaoRendaFixa.Focus();
        }
        
        function OnGetDataClienteFiltro(data) 
        {
            btnEditCodigoClienteFiltro.SetValue(data);        
            callbackClienteFiltro.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
        }                         
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {        
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    
     <dxcb:ASPxCallback ID="callbackClienteFiltro" runat="server" OnCallback="callbackClienteFiltro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {                    
                if (gridCadastro.cp_EditVisibleIndex == -1)
                {                 
                    var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                    OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
                }                               
            }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="callBackPopupOperacaoRendaFixa" runat="server" OnCallback="callBackPopupOperacaoRendaFixa_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditOperacaoRendaFixa.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupOperacaoRendaFixa" ClientInstanceName="popupOperacaoRendaFixa"
        runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentOperacaoRendaFixa" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridOperacaoRendaFixa" runat="server" Width="100%" ClientInstanceName="gridOperacaoRendaFixa"
                        AutoGenerateColumns="False" DataSourceID="EsDSOperacaoRendaFixa" KeyFieldName="CompositeKey"
                        OnCustomDataCallback="gridOperacaoRendaFixa_CustomDataCallback" OnCustomCallback="gridOperacaoRendaFixa_CustomCallback"
                        OnLoad="gridOperacaoRendaFixa_load" OnHtmlRowCreated="gridOperacaoRendaFixa_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" VisibleIndex="0" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" VisibleIndex="1" Caption="Papel"
                                Width="40%">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="ClienteApelido" VisibleIndex="2" Caption="Cliente">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo de Operação"
                                VisibleIndex="3">
                                <PropertiesComboBox ValueType="System.String">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data da Operação"
                                VisibleIndex="4">
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataDateColumn FieldName="DataLiquidacao" Caption="Data da Liquidação"
                                VisibleIndex="5">
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="6" Visible="false">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" VisibleIndex="7" Visible="false">
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridOperacaoRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataOperacaoRendaFixa);}"
                            Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Operação Renda Fixa." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridOperacaoRendaFixa.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Vigência de Categoria"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                        <table>
                                            <tr>
                                                <td class="td_Label_Longo">
                                                    <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                        ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                        <Buttons>
                                                            <dxe:EditButton>
                                                            </dxe:EditButton>
                                                        </Buttons>
                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                            ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, callbackClienteFiltro, btnEditCodigoClienteFiltro);}" />
                                                    </dxe:ASPxSpinEdit>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                            <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); textNomeClienteFiltro = '';  btnEditCodigoClienteFiltro.SetValue(null); return false;">
                                                <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                            </dxpc:ASPxPopupControl>
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                    <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                    CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton></div>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" DataSourceID="EsDSVigenciaCategoria"
                                    KeyFieldName="IdVigenciaCategoria" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomJSProperties="gridCadastro_CustomJSProperties" OnLoad="gridCadastro_Load"
                                    OnBeforeGetCallbackResult="gridCadastro_PreRender" AutoGenerateColumns="False">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdVigenciaCategoria" VisibleIndex="0" ReadOnly="True"
                                            Caption="Id Vigência de Categoria" Visible="false">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                       <dxwgv:GridViewDataTextColumn FieldName="DescricaoCliente" VisibleIndex="2" Caption="Cliente">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="DataVigencia" VisibleIndex="1" Caption="Dt.Vigência"
                                            Width="10%">
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" VisibleIndex="2" Caption="Id Operação">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Vigência de Categoria" FieldName="TipoVigenciaCategoria"
                                            VisibleIndex="3">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                    </Columns>
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                        <EditForm>
                                            <div class="editForm">
                                                <dxe:ASPxTextBox ID="hiddenIdOperacao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdOperacao")%>'
                                                    ClientInstanceName="hiddenIdOperacao" />
                                                <dxe:ASPxTextBox ID="hiddenIdSerie" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdSerie" />
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelBtnOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="btnEditOperacaoRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditOperacaoRendaFixa" ReadOnly="true"
                                                                Width="150px" Text='<%# Eval("IdOperacao") %>' OnInit="btnEditOperacaoRendaFixa_Init">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupOperacaoRendaFixa.ShowAtElementByID(s.name);}" />
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelDataVigencia" runat="server" CssClass="labelRequired" Text="Dt.Vigência:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textDataVigencia" runat="server" ClientInstanceName="textDataVigencia"
                                                                Width="150px" Value='<%#Eval("DataVigencia")%>' OnInit="textDataVigencia_Init" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTipoVigenciaCategoria" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropTipoVigenciaCategoria" Text="Categoria:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipoVigenciaCategoria" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropTipoVigenciaCategoria_Load" Text='<%#Eval("TipoVigenciaCategoria")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                        <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton></div>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="250px" />
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                    </Templates>
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSVigenciaCategoria" runat="server" OnesSelect="EsDSVigenciaCategoria_esSelect" />
    <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect" />
    </form>
</body>
</html>
