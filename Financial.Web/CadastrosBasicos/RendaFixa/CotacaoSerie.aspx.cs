﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CotacaoSerie : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoSerie_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        string descricao = "< CAST(serie.IdSerie as varchar) + ' - ' + serie.Descricao + ' - ' + feeder.Descricao as Descricao>";

        CotacaoSerieCollection coll = new CotacaoSerieCollection();

        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        FeederQuery feederQuery = new FeederQuery("feeder");
        CotacaoSerieQuery cotacaoSerieQuery = new CotacaoSerieQuery("cotacaoSerie");

        cotacaoSerieQuery.Select(cotacaoSerieQuery,
                              serieRendaFixaQuery.IdSerie,
                              serieRendaFixaQuery.IdFeeder,
                              descricao);
        cotacaoSerieQuery.InnerJoin(serieRendaFixaQuery).On(serieRendaFixaQuery.IdSerie == cotacaoSerieQuery.IdSerie);
        cotacaoSerieQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);
        cotacaoSerieQuery.Where(serieRendaFixaQuery.TipoTaxa == (int)Financial.RendaFixa.Enums.TipoMTMTitulo.Serie);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            cotacaoSerieQuery.Where(cotacaoSerieQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            cotacaoSerieQuery.Where(cotacaoSerieQuery.Data.LessThanOrEqual(textDataFim.Text));
        }
        cotacaoSerieQuery.OrderBy(coll.Query.Data.Descending);

        coll.Load(cotacaoSerieQuery);

        e.Collection = coll;
    }

    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();
        SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");
        FeederQuery feederQuery = new FeederQuery("feeder");

        serieRendaFixaQuery.Select(serieRendaFixaQuery,
                                  (serieRendaFixaQuery.Descricao + " - " + feederQuery.Descricao).As("DescricaoCompleta"));
        serieRendaFixaQuery.InnerJoin(feederQuery).On(serieRendaFixaQuery.IdFeeder == feederQuery.IdFeeder);
        serieRendaFixaQuery.Where(serieRendaFixaQuery.TipoTaxa == (int)Financial.RendaFixa.Enums.TipoMTMTitulo.Serie);

        serieRendaFixaQuery.OrderBy(serieRendaFixaQuery.IdSerie.Ascending);

        if (coll.Load(serieRendaFixaQuery))
        {
            foreach (SerieRendaFixa serie in coll)
                serie.SetColumn("DescricaoCompleta", serie.GetColumn("IdSerie").ToString() + " - " + serie.GetColumn("DescricaoCompleta").ToString());
        }

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropSerie);
        controles.Add(textData);
        controles.Add(textValor);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
            DateTime data = Convert.ToDateTime(textData.Text);

            CotacaoSerie cotacaoSerie = new CotacaoSerie();
            if (cotacaoSerie.LoadByPrimaryKey(data, idSerie)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn DigitadoImportado = gridCadastro.Columns["DigitadoImportado"] as GridViewDataComboBoxColumn;
        if (DigitadoImportado != null)
        {
            DigitadoImportado.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(Financial.RendaFixa.Enums.DigitadoImportado)))
            {
                DigitadoImportado.PropertiesComboBox.Items.Add(Financial.RendaFixa.Enums.DigitadoImportadoDescricao.RetornaStringValue(i), i);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropSerie_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(CotacaoSerieMetadata.ColumnNames.Data));
            string idSerie = Convert.ToString(e.GetListSourceFieldValue(CotacaoSerieMetadata.ColumnNames.IdSerie));
            e.Value = data + idSerie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CotacaoSerie cotacaoSerie = new CotacaoSerie();

        ASPxComboBox dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        int idSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);
        if (cotacaoSerie.LoadByPrimaryKey(data, idSerie)) {
            cotacaoSerie.Cotacao = valor;
            cotacaoSerie.DigitadoImportado = (int)Financial.RendaFixa.Enums.DigitadoImportado.Digitado;
            cotacaoSerie.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoSerie - Operacao: Update CotacaoSerie: " + data + "; " + idSerie + UtilitarioWeb.ToString(cotacaoSerie),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        CotacaoSerie cotacaoSerie = new CotacaoSerie();

        ASPxComboBox dropSerie = gridCadastro.FindEditFormTemplateControl("dropSerie") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        int idSerie = Convert.ToInt32(dropSerie.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);

        cotacaoSerie.IdSerie = idSerie;
        cotacaoSerie.Data = data;
        cotacaoSerie.Cotacao = valor;
        cotacaoSerie.DigitadoImportado = (int)Financial.RendaFixa.Enums.DigitadoImportado.Digitado;
        cotacaoSerie.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoSerie - Operacao: Insert CotacaoSerie: " + cotacaoSerie.Data + "; " + cotacaoSerie.IdSerie + UtilitarioWeb.ToString(cotacaoSerie),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CotacaoSerieMetadata.ColumnNames.IdSerie);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoSerieMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++) {
                int idSerie = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                CotacaoSerie cotacaoSerie = new CotacaoSerie();
                if (cotacaoSerie.LoadByPrimaryKey(data, idSerie)) {

                    CotacaoSerie cotacaoSerieClone = (CotacaoSerie)Utilitario.Clone(cotacaoSerie);
                    //
                    
                    cotacaoSerie.MarkAsDeleted();
                    cotacaoSerie.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoSerie - Operacao: Delete CotacaoSerie: " + data + "; " + idSerie + UtilitarioWeb.ToString(cotacaoSerieClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
        //        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            e.Properties["cpTextValor"] = textValor.ClientID;
        }
    }
}