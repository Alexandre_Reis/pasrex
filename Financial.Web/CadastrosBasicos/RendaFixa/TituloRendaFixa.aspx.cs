﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.Integracao.Excel.Util;
using System.IO;
using EntitySpaces.Interfaces;
using Financial.Integracao.Excel;
using EntitySpaces.Core;
using Financial.Common.Enums;
using DevExpress.Web.Data;
using Financial.BibliotecaFincs;

public partial class CadastrosBasicos_TituloRendaFixa : CadastroBasePage
{
    // Armazena o Conteudo do Excel Agenda RendaFixa
    private List<ValoresExcelAgendaRendaFixaReduzida> valoresExcelAgendaRendaFixa = new List<ValoresExcelAgendaRendaFixaReduzida>();

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupEmissor = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaQuery titulo = new TituloRendaFixaQuery("t");
        EmissorQuery emissor = new EmissorQuery("e");
        EmpresaSecuritizadaQuery empresa = new EmpresaSecuritizadaQuery("emp");
        PapelRendaFixaQuery papel = new PapelRendaFixaQuery("p");

        titulo.Select(papel.IsentoIR.As("PapelIsentoIR"), titulo, emissor.Nome.As("NomeEmissor"));
        titulo.InnerJoin(emissor).On(titulo.IdEmissor == emissor.IdEmissor);
        titulo.LeftJoin(papel).On(titulo.IdPapel == papel.IdPapel);
        titulo.OrderBy(titulo.IdTitulo.Descending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(titulo);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        Indice i = new Indice();
        i.IdIndice = null;
        i.Descricao = "";

        // Attach
        coll.AttachEntity(i);

        //Usado para o Lote
        if (!gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            i = new Indice();
            i.IdIndice = -9999;
            i.Descricao = "***Sem Indexador***";

            // Attach
            coll.AttachEntity(i);
        }

        coll.Sort = IndiceMetadata.ColumnNames.Descricao + " ASC";

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEmissorInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();
        EmissorQuery emissor = new EmissorQuery("e");

        TituloRendaFixaQuery titulo = new TituloRendaFixaQuery("t");
        emissor.Select(emissor.IdEmissor, emissor.Nome);
        emissor.InnerJoin(titulo).On(emissor.IdEmissor == titulo.IdEmissor);
        emissor.es.Distinct = true;

        //coll.Query.es.Top = 1;
        emissor.OrderBy(emissor.Nome.Ascending);
        coll.Load(emissor);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSerieRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SerieRendaFixaCollection coll = new SerieRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.IdMoeda.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EstrategiaCollection coll = new EstrategiaCollection();
        EstrategiaQuery estrategiaQuery = new EstrategiaQuery("estrategia");

        StringBuilder descricaoField = new StringBuilder();
        descricaoField.Append("< cast(estrategia.[IdEstrategia] as varchar(20)) + ' - ' + estrategia.descricao as Descricao >");

        estrategiaQuery.Select(descricaoField.ToString(), estrategiaQuery.IdEstrategia);
        estrategiaQuery.OrderBy(estrategiaQuery.IdEstrategia.Ascending);
        coll.Load(estrategiaQuery);

        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.OrderBy(coll.Query.IdAgente.Ascending);
        coll.Query.Where(coll.Query.EmpSecuritizada.Equal('S'));
        coll.LoadAll();

        e.Collection = coll;

    }
    #endregion

    protected void textIdTitulo_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
        }
    }

    protected void labelIdTitulo_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Enabled = false;
        }

        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxCheckBox chkParticipacao = pageControl.FindControl("chkParticipacao") as ASPxCheckBox;
            string participacao = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Participacao").ToString();

            if (participacao == "S")
            {
                chkParticipacao.Checked = true;
            }
            else
            {
                chkParticipacao.Checked = false;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="textDescricao"></param>
    /// <param name="textDataVencimento"></param>
    /// <param name="textPercentual"></param>
    /// <param name="dropIndice"></param>
    /// <param name="textTaxa"></param>
    /// <returns></returns>
    private string MontaDescricaoCompleta(ASPxTextBox textDescricao, ASPxDateEdit textDataVencimento, ASPxSpinEdit textPercentual,
                                          ASPxComboBox dropIndice, ASPxSpinEdit textTaxa)
    {
        StringBuilder descricaoCompleta = new StringBuilder();
        descricaoCompleta.Append(textDescricao.Text.Trim() + " - ");
        descricaoCompleta.Append(" Vcto: ");

        descricaoCompleta.Append(Convert.ToDateTime(textDataVencimento.Text).ToShortDateString());
        if (dropIndice.SelectedIndex > 0 || (!String.IsNullOrEmpty(textTaxa.Text) && Convert.ToDecimal(textTaxa.Text) != 0))
        {
            descricaoCompleta.Append(" -> ");
        }

        if (dropIndice.SelectedIndex > 0)
        {
            Indice indice = new Indice();
            if (indice.LoadByPrimaryKey(Convert.ToInt16(dropIndice.SelectedItem.Value)))
            {
                if (!String.IsNullOrEmpty(textPercentual.Text))
                {
                    descricaoCompleta.Append(String.Format("{0:n}", Convert.ToDecimal(textPercentual.Text)) + "% ");
                }
                else
                {
                    descricaoCompleta.Append("100% ");
                }

                descricaoCompleta.Append(indice.Descricao);
            }
        }

        if (!String.IsNullOrEmpty(textTaxa.Text) && Convert.ToDecimal(textTaxa.Text) != 0)
        {
            if (dropIndice.SelectedIndex > 0)
            {
                descricaoCompleta.Append(" + " + String.Format("{0:n}", Convert.ToDecimal(textTaxa.Text)) + "%");
            }
            else
            {
                descricaoCompleta.Append("Tx = " + String.Format("{0:n}", Convert.ToDecimal(textTaxa.Text)) + "%");
            }
        }

        return descricaoCompleta.ToString().Replace("/", "-");
    }

    /// <summary>
    /// Faz Update dos Titulos Selecionadas em Lote
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> titulosSelecionados = gridCadastro.GetSelectedFieldValues(TituloRendaFixaMetadata.ColumnNames.IdTitulo);

        if (titulosSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Titulos.";
        }
        else
        {
            #region Se tem Titulo Selecionado
            //if (dropPapelLote.Text.Trim()== "" ||
            //    dropIsentoIRLote.Text.Trim() == "" ||
            //    dropIsentoIOFLote.Text.Trim() == "" ||
            //    dropEmissorLote.Text.Trim() == "") {

            //    e.Result = "Campos Obrigatórios.";
            //    return;
            //}

            for (int i = 0; i < titulosSelecionados.Count; i++)
            {
                int idTitulo = Convert.ToInt32(titulosSelecionados[i]);

                TituloRendaFixa titulo = new TituloRendaFixa();
                //
                if (titulo.LoadByPrimaryKey(idTitulo))
                {
                    PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;
                    int classe = papel.Classe.Value;

                    if (dropPapelLote.SelectedIndex > -1)
                    {
                        titulo.IdPapel = Convert.ToInt32(dropPapelLote.SelectedItem.Value);
                    }
                    //
                    if (dropEstrategiaLote.SelectedIndex >= 0)
                    {
                        titulo.IdEstrategia = Convert.ToInt32(dropEstrategiaLote.SelectedItem.Value);
                    }
                    else
                    {
                        titulo.IdEstrategia = null;
                    }
                    //
                    if (dropIsentoIRLote.SelectedIndex > -1)
                    {
                        titulo.IsentoIR = Convert.ToByte(dropIsentoIRLote.SelectedItem.Value);
                    }
                    //
                    if (dropIsentoIOFLote.SelectedIndex > -1)
                    {
                        titulo.IsentoIOF = Convert.ToByte(dropIsentoIOFLote.SelectedItem.Value);
                    }
                    //
                    if (dropEmissorLote.Text.Trim() != "")
                    {
                        string emissor = dropEmissorLote.Text.Trim();
                        string[] emissorAux = emissor.Split(new Char[] { '-' });
                        //
                        titulo.IdEmissor = Convert.ToInt32(emissorAux[0].Trim());
                    }
                    //
                    if (!string.IsNullOrEmpty(textTaxaLote.Text))
                    {
                        titulo.Taxa = Convert.ToDecimal(textTaxaLote.Text);
                    }
                    //
                    if (dropIndiceLote.SelectedIndex != -1)
                    {
                        int idIndice = Convert.ToInt32(dropIndiceLote.SelectedItem.Value);

                        if (idIndice != 0)
                        {
                            if (idIndice != -9999) //Sem indexador                        
                                titulo.IdIndice = (short)idIndice;
                            else
                                titulo.IdIndice = null;
                        }
                    }
                    //
                    if (!string.IsNullOrEmpty(textPercentualLote.Text))
                    {
                        titulo.Percentual = Convert.ToDecimal(textPercentualLote.Text);
                    }
                    //
                    if (!string.IsNullOrEmpty(textPUNominalLote.Text))
                    {
                        titulo.PUNominal = Convert.ToDecimal(textPUNominalLote.Text);
                        titulo.ValorNominal = Convert.ToDecimal(textPUNominalLote.Text);
                    }
                    //
                    if (dropDebentureConversivelLote.SelectedIndex != -1)
                    {
                        if (classe == (int)ClasseRendaFixa.Debenture)
                            titulo.DebentureConversivel = dropDebentureConversivelLote.SelectedItem.Value.ToString();
                        else
                            titulo.DebentureConversivel = "N";
                    }
                    //
                    if (dropDebentureInfraLote.SelectedIndex != -1)
                    {
                        if (classe == (int)ClasseRendaFixa.Debenture)
                            titulo.DebentureInfra = dropDebentureInfraLote.SelectedItem.Value.ToString();
                        else
                            titulo.DebentureInfra = "N";
                    }
                    //
                    if (dropMoedaLote.SelectedIndex != -1)
                    {
                        titulo.IdMoeda = Convert.ToInt32(dropMoedaLote.SelectedItem.Value);
                    }
                    //
                    if (dropPeriodicidadeLote.SelectedIndex != -1)
                    {
                        titulo.Periodicidade = Convert.ToInt32(dropPeriodicidadeLote.SelectedItem.Value);
                    }
                    //
                    if (dropCriterioAmortizacaoLote.SelectedIndex != -1)
                    {
                        titulo.CriterioAmortizacao = Convert.ToInt32(dropCriterioAmortizacaoLote.SelectedItem.Value);
                    }
                    //
                    if (dropExecutaProRataEmissaoLote.SelectedIndex != -1)
                    {
                        titulo.ExecutaProRataEmissao = dropExecutaProRataEmissaoLote.SelectedItem.Value.ToString();
                    }
                    //
                    if (dropTipoProRataLote.SelectedIndex != -1)
                    {
                        titulo.TipoProRata = Convert.ToInt32(dropTipoProRataLote.SelectedItem.Value);
                    }
                    //
                    if (dropPermiteRepactuacaoLote.SelectedIndex != -1)
                    {
                        titulo.PermiteRepactuacao = dropPermiteRepactuacaoLote.SelectedItem.Value.ToString();
                    }
                    //
                    if (dropExecutaProRataLiquidacaoLote.SelectedIndex != -1)
                    {
                        titulo.ProRataLiquidacao = dropExecutaProRataLiquidacaoLote.SelectedItem.Value.ToString();
                    }
                    //
                    if (!string.IsNullOrEmpty(textProRataEmissaoDiaLote.Text))
                    {
                        titulo.ProRataEmissaoDia = Convert.ToInt32(textProRataEmissaoDiaLote.Text);
                    }
                    //
                    if (dropAtivoRegraLote.SelectedIndex != -1)
                    {
                        titulo.AtivoRegra = dropAtivoRegraLote.SelectedItem.Value.ToString();
                    }
                    //
                    if (!string.IsNullOrEmpty(textDefasagemLiquidacaoLote.Text))
                    {
                        titulo.DefasagemLiquidacao = Convert.ToInt32(textDefasagemLiquidacaoLote.Text);
                    }
                    //
                    if (drop_eJurosLote.SelectedIndex != -1)
                    {
                        titulo.EJuros = Convert.ToInt32(drop_eJurosLote.SelectedItem.Value);
                    }
                    //
                    if (!string.IsNullOrEmpty(textDefasagemMesesLote.Text))
                    {
                        titulo.DefasagemMeses = Convert.ToInt32(textDefasagemMesesLote.Text);
                    }
                    //
                    if (drop_eJurosTipoLote.SelectedIndex != -1)
                    {
                        titulo.EJurosTipo = Convert.ToInt32(drop_eJurosTipoLote.SelectedItem.Value);
                    }

                    //
                    titulo.Save();
                }
            }

            e.Result = "Processo executado com sucesso.";
            #endregion
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idEmissor = Convert.ToInt32(e.Parameter);
            Emissor emissor = new Emissor();

            if (emissor.LoadByPrimaryKey(idEmissor))
            {
                texto = emissor.IdEmissor + " - " + emissor.Nome;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxTextBox textDescricao = pageControl.FindControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropPapel = pageControl.FindControl("dropPapel") as ASPxComboBox;
        ASPxDateEdit textDataEmissao = pageControl.FindControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textPUNominal = pageControl.FindControl("textPUNominal") as ASPxSpinEdit;
        ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxButtonEdit btnEditEmissor = pageControl.FindControl("btnEditEmissor") as ASPxButtonEdit;
        ASPxComboBox dropDebentureConversivel = pageControl.FindControl("dropDebentureConversivel") as ASPxComboBox;
        ASPxComboBox dropDebentureInfra = pageControl.FindControl("dropDebentureInfra") as ASPxComboBox;
        ASPxComboBox dropCriterioAmortizacao = pageControl.FindControl("dropCriterioAmortizacao") as ASPxComboBox;
        TextBox textCodigoCusip = pageControl.FindControl("textCodigoCusip") as TextBox;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropOpcaoEmbutida = pageControl.FindControl("dropOpcaoEmbutida") as ASPxComboBox;
        ASPxComboBox dropTipoOpcao = pageControl.FindControl("dropTipoOpcao") as ASPxComboBox;
        ASPxComboBox dropTipoExercicio = pageControl.FindControl("dropTipoExercicio") as ASPxComboBox;
        ASPxDateEdit textDataInicioExercicio = pageControl.FindControl("textDataInicioExercicio") as ASPxDateEdit;
        ASPxDateEdit textDataVencimentoOpcao = pageControl.FindControl("textDataVencimentoOpcao") as ASPxDateEdit;
        ASPxComboBox dropTipoPremio = pageControl.FindControl("dropTipoPremio") as ASPxComboBox;
        ASPxSpinEdit textValorPremio = pageControl.FindControl("textValorPremio") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropPapel);
        controles.Add(textDataEmissao);
        controles.Add(textDataVencimento);
        controles.Add(btnEditEmissor);
        controles.Add(textPUNominal);
        controles.Add(dropMoeda);
        controles.Add(dropDebentureConversivel);
        controles.Add(dropDebentureInfra);
        controles.Add(dropCriterioAmortizacao);
        controles.Add(dropEstrategia);
        controles.Add(dropIsentoIOF);
        controles.Add(dropOpcaoEmbutida);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }


        if (dropOpcaoEmbutida.SelectedItem.Value.Equals("S"))
        {
            controles = new List<Control>();
            controles.Add(dropTipoOpcao);
            controles.Add(dropTipoExercicio);
            controles.Add(textDataInicioExercicio);
            controles.Add(dropTipoPremio);
            controles.Add(textValorPremio);
            controles.Add(textDataVencimentoOpcao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            if (!string.IsNullOrEmpty(textDataVencimentoOpcao.Text))
            {
                DateTime dataInicio = Convert.ToDateTime(textDataInicioExercicio.Text);
                DateTime dataVencimentoOpcao = Convert.ToDateTime(textDataVencimentoOpcao.Text);
                DateTime dataVencimentoTitulo = Convert.ToDateTime(textDataVencimento.Text);

                string result = string.Empty;

                if (dataVencimentoOpcao < dataInicio)
                {
                    e.Result = "Data Vencimento Opção deve ser maior ou igual à Data Início para Exercício!!";
                    return;
                }

                if (dataVencimentoOpcao > dataVencimentoTitulo)
                {
                    e.Result = "Data Vencimento Opção deve ser menor ou igual à Data de Vencimento do Título!!";
                    return;
                }
            }
        }
        #endregion

        if (textCodigoCusip.Text.Length < 9 && !string.IsNullOrEmpty(textCodigoCusip.Text))
        {
            e.Result = "Código Cusip deve possuir 9 caracteres.";
            return;
        }

        if (gridCadastro.IsNewRowEditing)
        {
            ASPxSpinEdit textIdTitulo = pageControl.FindControl("textIdTitulo") as ASPxSpinEdit;

            if (textIdTitulo.Text != "")
            {
                int idTitulo = Convert.ToInt32(textIdTitulo.Text);

                TituloRendaFixa titulo = new TituloRendaFixa();
                if (titulo.LoadByPrimaryKey(idTitulo))
                {
                    e.Result = "Id Título especificado já existente!";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int idTitulo = SalvaNovoTitulo();
        SalvaEmpresaSecuritizada(idTitulo);
        e.Result = "";
        if (e.Parameter == "clone")
        {
            e.Result = idTitulo.ToString();
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private int SalvaNovoTitulo()
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxTextBox textDescricao = pageControl.FindControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropPapel = pageControl.FindControl("dropPapel") as ASPxComboBox;
        ASPxDateEdit textDataEmissao = pageControl.FindControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = pageControl.FindControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropIndice = pageControl.FindControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textPercentual = pageControl.FindControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textPUNominal = pageControl.FindControl("textPUNominal") as ASPxSpinEdit;
        ASPxComboBox dropTipoMTM = pageControl.FindControl("dropTipoMTM") as ASPxComboBox;
        ASPxComboBox dropSerie = pageControl.FindControl("dropSerie") as ASPxComboBox;
        TextBox textCodigoCustodia = pageControl.FindControl("textCodigoCustodia") as TextBox;
        ASPxComboBox dropIsentoIR = pageControl.FindControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        ASPxSpinEdit textIdTitulo = pageControl.FindControl("textIdTitulo") as ASPxSpinEdit;
        TextBox textCodigoISIN = pageControl.FindControl("textCodigoISIN") as TextBox;
        TextBox textCodigoCBLC = pageControl.FindControl("textCodigoCBLC") as TextBox;
        TextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as TextBox;
        ASPxComboBox dropDebentureConversivel = pageControl.FindControl("dropDebentureConversivel") as ASPxComboBox;
        ASPxComboBox dropDebentureInfra = pageControl.FindControl("dropDebentureInfra") as ASPxComboBox;
        ASPxTextBox textCodigoInterface = pageControl.FindControl("textCodigoInterface") as ASPxTextBox;
        ASPxSpinEdit textPremioRebate = pageControl.FindControl("textPremioRebate") as ASPxSpinEdit;
        ASPxComboBox dropPeriodicidade = pageControl.FindControl("dropPeriodicidade") as ASPxComboBox;
        TextBox hiddenIdEmissor = pageControl.FindControl("hiddenIdEmissor") as TextBox;
        ASPxComboBox dropPermiteRepactuacao = pageControl.FindControl("dropPermiteRepactuacao") as ASPxComboBox;
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        TextBox textCodigoCusip = pageControl.FindControl("textCodigoCusip") as TextBox;
        ASPxSpinEdit textDefasagemLiquidacao = pageControl.FindControl("textDefasagemLiquidacao") as ASPxSpinEdit;
        //Fincs
        ASPxComboBox drop_eJuros = pageControl.FindControl("drop_eJuros") as ASPxComboBox;
        ASPxComboBox drop_eTaxa = pageControl.FindControl("drop_eTaxa") as ASPxComboBox;
        ASPxComboBox drop_eJurosTipo = pageControl.FindControl("drop_eJurosTipo") as ASPxComboBox;
        ASPxSpinEdit textDefasagemMeses = pageControl.FindControl("textDefasagemMeses") as ASPxSpinEdit;
        ASPxSpinEdit textAtivoRegra = pageControl.FindControl("textAtivoRegra") as ASPxSpinEdit;
        ASPxComboBox dropExecutaProRataLiquidacao = pageControl.FindControl("dropExecutaProRataLiquidacao") as ASPxComboBox;
        ASPxComboBox dropExecutaProRataEmissao = pageControl.FindControl("dropExecutaProRataEmissao") as ASPxComboBox;
        ASPxComboBox dropTipoProRata = pageControl.FindControl("dropTipoProRata") as ASPxComboBox;
        ASPxComboBox dropCriterioAmortizacao = pageControl.FindControl("dropCriterioAmortizacao") as ASPxComboBox;
        ASPxDateEdit textDataInicioCorrecao = pageControl.FindControl("textDataInicioCorrecao") as ASPxDateEdit;
        ASPxSpinEdit textProRataEmissaoDia = pageControl.FindControl("textProRataEmissaoDia") as ASPxSpinEdit;
        ASPxComboBox dropAtivoRegra = pageControl.FindControl("dropAtivoRegra") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = pageControl.FindControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = pageControl.FindControl("textCodigoBDS") as ASPxSpinEdit;

        ASPxComboBox dropOpcaoEmbutida = pageControl.FindControl("dropOpcaoEmbutida") as ASPxComboBox;
        ASPxComboBox dropTipoOpcao = pageControl.FindControl("dropTipoOpcao") as ASPxComboBox;
        ASPxComboBox dropTipoExercicio = pageControl.FindControl("dropTipoExercicio") as ASPxComboBox;
        ASPxDateEdit textDataInicioExercicio = pageControl.FindControl("textDataInicioExercicio") as ASPxDateEdit;
        ASPxDateEdit textDataVencimentoOpcao = pageControl.FindControl("textDataVencimentoOpcao") as ASPxDateEdit;
        ASPxComboBox dropTipoPremio = pageControl.FindControl("dropTipoPremio") as ASPxComboBox;
        ASPxSpinEdit textValorPremio = pageControl.FindControl("textValorPremio") as ASPxSpinEdit;
        ASPxCheckBox chkParticipacao = pageControl.FindControl("chkParticipacao") as ASPxCheckBox;


        tituloRendaFixa.OpcaoEmbutida = Convert.ToString(dropOpcaoEmbutida.SelectedItem.Value);
        tituloRendaFixa.TipoOpcao = null;
        tituloRendaFixa.TipoExercicio = null;
        tituloRendaFixa.DataInicioExercicio = null;
        tituloRendaFixa.DataVencimentoOpcao = null;
        tituloRendaFixa.TipoPremio = null;
        tituloRendaFixa.ValorPremio = null;
        if (tituloRendaFixa.OpcaoEmbutida.Equals("S"))
        {
            tituloRendaFixa.TipoOpcao = Convert.ToInt16(dropTipoOpcao.SelectedItem.Value);
            tituloRendaFixa.TipoExercicio = Convert.ToInt16(dropTipoExercicio.SelectedItem.Value);
            tituloRendaFixa.DataInicioExercicio = Convert.ToDateTime(textDataInicioExercicio.Text);
            if (!string.IsNullOrEmpty(textDataVencimentoOpcao.Text))
                tituloRendaFixa.DataVencimentoOpcao = Convert.ToDateTime(textDataVencimentoOpcao.Text);
            tituloRendaFixa.TipoPremio = Convert.ToInt16(dropTipoPremio.SelectedItem.Value);
            tituloRendaFixa.ValorPremio = Convert.ToDecimal(textValorPremio.Text);
        }

        tituloRendaFixa.Descricao = textDescricao.Text.ToString();
        tituloRendaFixa.DescricaoCompleta = MontaDescricaoCompleta(textDescricao, textDataVencimento, textPercentual,
                                                                   dropIndice, textTaxa);
        tituloRendaFixa.IdPapel = Convert.ToInt32(dropPapel.SelectedItem.Value);
        tituloRendaFixa.DataEmissao = Convert.ToDateTime(textDataEmissao.Text);
        tituloRendaFixa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        tituloRendaFixa.IdEmissor = Convert.ToInt32(hiddenIdEmissor.Text);
        tituloRendaFixa.IsentoIR = dropIsentoIR.SelectedItem != null ? Convert.ToByte(dropIsentoIR.SelectedItem.Value) : Convert.ToByte(0);
        tituloRendaFixa.IsentoIOF = Convert.ToByte(dropIsentoIOF.SelectedItem.Value);
        tituloRendaFixa.PremioRebate = (string.IsNullOrEmpty(textPremioRebate.Text) ? 0 : Convert.ToDecimal(textPremioRebate.Text));

        if (!string.IsNullOrEmpty(textCodigoBDS.Text))
            tituloRendaFixa.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
        else
            tituloRendaFixa.CodigoBDS = null;

        if (textTaxa.Text != "")
        {
            tituloRendaFixa.Taxa = Convert.ToDecimal(textTaxa.Text);
        }
        else
        {
            tituloRendaFixa.Taxa = 0;
        }

        if (dropIndice.SelectedIndex > 0)
        {
            tituloRendaFixa.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);

            if (textPercentual.Text != "")
            {
                tituloRendaFixa.Percentual = Convert.ToDecimal(textPercentual.Text);
            }
            else
            {
                tituloRendaFixa.Percentual = 100;
            }
        }
        else
        {
            tituloRendaFixa.Percentual = 0;
        }

        if (dropPeriodicidade.SelectedIndex > 0)
        {
            tituloRendaFixa.Periodicidade = Convert.ToInt32(dropPeriodicidade.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.Periodicidade = null;
        }

        if (drop_eJuros.SelectedIndex > -1)
        {
            tituloRendaFixa.EJuros = Convert.ToInt32(drop_eJuros.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.EJuros = null;
        }

        if (drop_eTaxa.SelectedIndex > -1)
        {
            tituloRendaFixa.ETaxa = Convert.ToInt32(drop_eTaxa.SelectedItem.Value);
        }

        if (drop_eJurosTipo.SelectedIndex > -1)
        {
            tituloRendaFixa.EJurosTipo = Convert.ToInt32(drop_eJurosTipo.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.EJurosTipo = null;
        }

        if (dropExecutaProRataLiquidacao.SelectedIndex > 0)
        {
            tituloRendaFixa.ProRataLiquidacao = Convert.ToString(dropExecutaProRataLiquidacao.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.ProRataLiquidacao = "N";
        }

        if (!string.IsNullOrEmpty(textDefasagemMeses.Text))
            tituloRendaFixa.DefasagemMeses = Convert.ToInt32(textDefasagemMeses.Text);
        else
            tituloRendaFixa.DefasagemMeses = null;

        if (dropAtivoRegra.SelectedIndex > -1)
        {
            tituloRendaFixa.AtivoRegra = Convert.ToString(dropAtivoRegra.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.AtivoRegra = "1"; //Sem regra especifica
        }

        if (!string.IsNullOrEmpty(textDataInicioCorrecao.Text))
            tituloRendaFixa.DataInicioCorrecao = Convert.ToDateTime(textDataInicioCorrecao.Text);
        else
            tituloRendaFixa.DataInicioCorrecao = null;

        if (!string.IsNullOrEmpty(textProRataEmissaoDia.Text))
            tituloRendaFixa.ProRataEmissaoDia = Convert.ToInt32(textProRataEmissaoDia.Text);
        else
            tituloRendaFixa.ProRataEmissaoDia = null;

        if (dropCriterioAmortizacao.SelectedIndex > -1)
        {
            tituloRendaFixa.CriterioAmortizacao = Convert.ToInt32(dropCriterioAmortizacao.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.CriterioAmortizacao = null;
        }

        if (dropPermiteRepactuacao.SelectedIndex > -1)
        {
            tituloRendaFixa.PermiteRepactuacao = dropPermiteRepactuacao.SelectedItem.Value.ToString(); ;
        }
        else
        {
            tituloRendaFixa.PermiteRepactuacao = "N";
        }

        if (dropExecutaProRataEmissao.SelectedIndex > -1)
        {
            tituloRendaFixa.ExecutaProRataEmissao = dropExecutaProRataEmissao.SelectedItem.Value.ToString(); ;
        }
        else
        {
            tituloRendaFixa.ExecutaProRataEmissao = "N";
        }

        if (dropTipoProRata.SelectedIndex > -1)
        {
            tituloRendaFixa.TipoProRata = Convert.ToInt32(dropTipoProRata.SelectedItem.Value);
        }
        else
        {
            tituloRendaFixa.TipoProRata = 0;
        }

        if (string.IsNullOrEmpty(textDefasagemLiquidacao.Text))
        {
            tituloRendaFixa.DefasagemLiquidacao = 0;
        }
        else
        {
            tituloRendaFixa.DefasagemLiquidacao = Convert.ToInt32(textDefasagemLiquidacao.Text);
        }

        tituloRendaFixa.PUNominal = Convert.ToDecimal(textPUNominal.Text);
        tituloRendaFixa.ValorNominal = Convert.ToDecimal(textPUNominal.Text);
        tituloRendaFixa.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);
        tituloRendaFixa.CodigoCustodia = textCodigoCustodia.Text.ToString();
        tituloRendaFixa.CodigoCBLC = textCodigoCBLC.Text.ToString();
        tituloRendaFixa.CodigoCetip = textCodigoCetip.Text.ToString();
        tituloRendaFixa.CodigoIsin = textCodigoISIN.Text.ToString();

        if (dropEstrategia.SelectedIndex != -1)
        {
            tituloRendaFixa.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
        }
        else
        {
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdEstrategia.IsNotNull());
            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.Count > 0 && tituloRendaFixaCollection[0].IdEstrategia.HasValue)
            {
                tituloRendaFixa.IdEstrategia = tituloRendaFixaCollection[0].IdEstrategia.Value;
            }
        }

        tituloRendaFixa.DebentureConversivel = Convert.ToString(dropDebentureConversivel.SelectedItem.Value);
        tituloRendaFixa.DebentureInfra = Convert.ToString(dropDebentureInfra.SelectedItem.Value);

        tituloRendaFixa.CodigoInterface = textCodigoInterface.Text;

        if (!string.IsNullOrEmpty(textCodigoCusip.Text))
            tituloRendaFixa.CodigoCusip = textCodigoCusip.Text;
        else
            tituloRendaFixa.CodigoCusip = null;

        int codicoCda = 0;
        if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
            tituloRendaFixa.CodigoCDA = codicoCda;

        tituloRendaFixa.Participacao = chkParticipacao.Checked ? "S" : "N";

        #region Save
        if (string.IsNullOrEmpty(textIdTitulo.Text))
        {
            tituloRendaFixa.Save();
        }
        else
        {
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.IdTitulo.Max());
            tituloRendaFixaCollection.Query.Load();

            int idTituloNovo = tituloRendaFixaCollection[0].IdTitulo.GetValueOrDefault(0) + 1;
            int idTituloOrig = Convert.ToInt32(textIdTitulo.Text);

            tituloRendaFixa.IdTitulo = idTituloNovo;
            tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.AttachEntity(tituloRendaFixa);
            //
            // Salva Titulo com o Novo Id
            new TituloRendaFixaCollection().InsereTituloRendaFixa(tituloRendaFixaCollection, true);

            AgendaRendaFixaCollection coll = new AgendaRendaFixaCollection();
            coll.Query.Where(coll.Query.IdTitulo.Equal(idTituloOrig));
            coll.Query.Load();

            foreach (AgendaRendaFixa agendaRendaFixa in coll)
            {
                AgendaRendaFixa agendaRendaFixaNew = new AgendaRendaFixa();

                agendaRendaFixaNew.DataAgenda = agendaRendaFixa.DataAgenda;
                agendaRendaFixaNew.DataEvento = agendaRendaFixa.DataEvento;
                agendaRendaFixaNew.DataPagamento = agendaRendaFixa.DataPagamento;
                agendaRendaFixaNew.IdTitulo = tituloRendaFixaCollection[0].IdTitulo.Value;
                agendaRendaFixaNew.Taxa = agendaRendaFixa.Taxa;
                agendaRendaFixaNew.TipoEvento = agendaRendaFixa.TipoEvento;
                agendaRendaFixaNew.Valor = agendaRendaFixa.Valor;

                agendaRendaFixaNew.Save();
            }

        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TituloRendaFixa - Operacao: Insert TituloRendaFixa: " + tituloRendaFixa.IdTitulo + UtilitarioWeb.ToString(tituloRendaFixa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        return tituloRendaFixa.IdTitulo.Value;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        int idTitulo = SalvaNovoTitulo();
        SalvaEmpresaSecuritizada(idTitulo);
        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idTitulo = 0;
        if (hiddenIdClone.Value != "")
        {
            idTitulo = Convert.ToInt32(hiddenIdClone.Value);
        }
        else
        {
            idTitulo = (int)e.Keys[0];
        }
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxTextBox textDescricao = pageControl.FindControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropPapel = pageControl.FindControl("dropPapel") as ASPxComboBox;
        ASPxDateEdit textDataEmissao = pageControl.FindControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = pageControl.FindControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = pageControl.FindControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropIndice = pageControl.FindControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textPercentual = pageControl.FindControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textPUNominal = pageControl.FindControl("textPUNominal") as ASPxSpinEdit;
        TextBox textCodigoCustodia = pageControl.FindControl("textCodigoCustodia") as TextBox;
        ASPxComboBox dropIsentoIR = pageControl.FindControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = pageControl.FindControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropMoeda = pageControl.FindControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropEstrategia = pageControl.FindControl("dropEstrategia") as ASPxComboBox;
        TextBox textCodigoISIN = pageControl.FindControl("textCodigoISIN") as TextBox;
        TextBox textCodigoCBLC = pageControl.FindControl("textCodigoCBLC") as TextBox;
        TextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as TextBox;
        ASPxComboBox dropDebentureConversivel = pageControl.FindControl("dropDebentureConversivel") as ASPxComboBox;
        ASPxComboBox dropDebentureInfra = pageControl.FindControl("dropDebentureInfra") as ASPxComboBox;
        ASPxTextBox textCodigoInterface = pageControl.FindControl("textCodigoInterface") as ASPxTextBox;
        ASPxSpinEdit textPremioRebate = pageControl.FindControl("textPremioRebate") as ASPxSpinEdit;
        //
        TextBox hiddenIdEmissor = pageControl.FindControl("hiddenIdEmissor") as TextBox;
        ASPxComboBox dropPeriodicidade = pageControl.FindControl("dropPeriodicidade") as ASPxComboBox;
        ASPxComboBox dropPermiteRepactuacao = pageControl.FindControl("dropPermiteRepactuacao") as ASPxComboBox;

        TextBox textCodigoCusip = pageControl.FindControl("textCodigoCusip") as TextBox;
        ASPxSpinEdit textDefasagemLiquidacao = pageControl.FindControl("textDefasagemLiquidacao") as ASPxSpinEdit;
        ASPxComboBox drop_eJuros = pageControl.FindControl("drop_eJuros") as ASPxComboBox;
        ASPxComboBox drop_eTaxa = pageControl.FindControl("drop_eTaxa") as ASPxComboBox;
        ASPxComboBox drop_eJurosTipo = pageControl.FindControl("drop_eJurosTipo") as ASPxComboBox;
        ASPxSpinEdit textDefasagemMeses = pageControl.FindControl("textDefasagemMeses") as ASPxSpinEdit;
        ASPxComboBox dropAtivoRegra = pageControl.FindControl("dropAtivoRegra") as ASPxComboBox;
        ASPxComboBox dropExecutaProRataLiquidacao = pageControl.FindControl("dropExecutaProRataLiquidacao") as ASPxComboBox;
        ASPxComboBox dropExecutaProRataEmissao = pageControl.FindControl("dropExecutaProRataEmissao") as ASPxComboBox;
        ASPxComboBox dropTipoProRata = pageControl.FindControl("dropTipoProRata") as ASPxComboBox;
        ASPxComboBox dropCriterioAmortizacao = pageControl.FindControl("dropCriterioAmortizacao") as ASPxComboBox;
        ASPxDateEdit textDataInicioCorrecao = pageControl.FindControl("textDataInicioCorrecao") as ASPxDateEdit;
        ASPxSpinEdit textProRataEmissaoDia = pageControl.FindControl("textProRataEmissaoDia") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoCDA = pageControl.FindControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = pageControl.FindControl("textCodigoBDS") as ASPxSpinEdit;
        ASPxComboBox dropOpcaoEmbutida = pageControl.FindControl("dropOpcaoEmbutida") as ASPxComboBox;
        ASPxComboBox dropTipoOpcao = pageControl.FindControl("dropTipoOpcao") as ASPxComboBox;
        ASPxComboBox dropTipoExercicio = pageControl.FindControl("dropTipoExercicio") as ASPxComboBox;
        ASPxDateEdit textDataInicioExercicio = pageControl.FindControl("textDataInicioExercicio") as ASPxDateEdit;
        ASPxDateEdit textDataVencimentoOpcao = pageControl.FindControl("textDataVencimentoOpcao") as ASPxDateEdit;
        ASPxComboBox dropTipoPremio = pageControl.FindControl("dropTipoPremio") as ASPxComboBox;
        ASPxSpinEdit textValorPremio = pageControl.FindControl("textValorPremio") as ASPxSpinEdit;
        ASPxCheckBox chkParticipacao = pageControl.FindControl("chkParticipacao") as ASPxCheckBox;

        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();

        if (tituloRendaFixa.LoadByPrimaryKey(idTitulo))
        {
            bool papelAlterado = tituloRendaFixa.IdPapel != Convert.ToInt32(dropPapel.SelectedItem.Value);
            tituloRendaFixa.Descricao = textDescricao.Text.ToString();
            tituloRendaFixa.DescricaoCompleta = MontaDescricaoCompleta(textDescricao, textDataVencimento, textPercentual, dropIndice, textTaxa);
            tituloRendaFixa.IdPapel = Convert.ToInt32(dropPapel.SelectedItem.Value);
            tituloRendaFixa.DataEmissao = Convert.ToDateTime(textDataEmissao.Text);

            if (Convert.ToDateTime(textDataVencimento.Text) != tituloRendaFixa.DataVencimento.Value)
            {
                #region Posicao RendaFixa
                PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdPosicao,
                                                        posicaoRendaFixaCollection.Query.DataVencimento);
                posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixaCollection.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    posicaoRendaFixa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                }
                posicaoRendaFixaCollection.Save();
                #endregion

                #region Posicao RendaFixaHistorico
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdPosicao,
                                                                 posicaoRendaFixaHistoricoCollection.Query.DataHistorico,
                                                                 posicaoRendaFixaHistoricoCollection.Query.DataVencimento);
                posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixaHistoricoCollection.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                {
                    posicaoRendaFixaHistorico.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                }
                posicaoRendaFixaHistoricoCollection.Save();
                #endregion

                #region Posicao RendaFixaAbertura
                PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
                posicaoRendaFixaAberturaCollection.Query.Select(posicaoRendaFixaAberturaCollection.Query.IdPosicao,
                                                                posicaoRendaFixaAberturaCollection.Query.DataHistorico,
                                                                posicaoRendaFixaAberturaCollection.Query.DataVencimento);
                posicaoRendaFixaAberturaCollection.Query.Where(posicaoRendaFixaAberturaCollection.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixaAberturaCollection.Query.Load();

                foreach (PosicaoRendaFixaAbertura posicaoRendaFixaAbertura in posicaoRendaFixaAberturaCollection)
                {
                    posicaoRendaFixaAbertura.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                }
                posicaoRendaFixaAberturaCollection.Save();

                #endregion
            }

            tituloRendaFixa.OpcaoEmbutida = Convert.ToString(dropOpcaoEmbutida.SelectedItem.Value);
            tituloRendaFixa.TipoOpcao = null;
            tituloRendaFixa.TipoExercicio = null;
            tituloRendaFixa.DataInicioExercicio = null;
            tituloRendaFixa.DataVencimentoOpcao = null;
            tituloRendaFixa.TipoPremio = null;
            tituloRendaFixa.ValorPremio = null;
            if (tituloRendaFixa.OpcaoEmbutida.Equals("S"))
            {
                tituloRendaFixa.TipoOpcao = Convert.ToInt16(dropTipoOpcao.SelectedItem.Value);
                tituloRendaFixa.TipoExercicio = Convert.ToInt16(dropTipoExercicio.SelectedItem.Value);
                tituloRendaFixa.DataInicioExercicio = Convert.ToDateTime(textDataInicioExercicio.Text);
                if (!string.IsNullOrEmpty(textDataVencimentoOpcao.Text))
                    tituloRendaFixa.DataVencimentoOpcao = Convert.ToDateTime(textDataVencimentoOpcao.Text);
                tituloRendaFixa.TipoPremio = Convert.ToInt16(dropTipoPremio.SelectedItem.Value);
                tituloRendaFixa.ValorPremio = Convert.ToDecimal(textValorPremio.Text);
            }

            tituloRendaFixa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            tituloRendaFixa.IdEmissor = Convert.ToInt32(hiddenIdEmissor.Text);
            tituloRendaFixa.IsentoIR = dropIsentoIR.SelectedItem != null ? Convert.ToByte(dropIsentoIR.SelectedItem.Value) : Convert.ToByte(0);
            tituloRendaFixa.IsentoIOF = Convert.ToByte(dropIsentoIOF.SelectedItem.Value);

            if (!string.IsNullOrEmpty(textCodigoBDS.Text))
                tituloRendaFixa.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
            else
                tituloRendaFixa.CodigoBDS = null;

            if (textTaxa.Text != "")
            {
                tituloRendaFixa.Taxa = Convert.ToDecimal(textTaxa.Text);
            }
            else
            {
                tituloRendaFixa.Taxa = null;
            }

            if (dropIndice.SelectedIndex > 0)
            {
                tituloRendaFixa.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);

                if (textPercentual.Text != "")
                {
                    tituloRendaFixa.Percentual = Convert.ToDecimal(textPercentual.Text);
                }
                else
                {
                    tituloRendaFixa.Percentual = 100;
                }
            }
            else
            {
                tituloRendaFixa.IdIndice = null;
                tituloRendaFixa.Percentual = 0;
            }

            if (drop_eJuros.SelectedIndex > -1)
            {
                tituloRendaFixa.EJuros = Convert.ToInt32(drop_eJuros.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.EJuros = null;
            }

            if (drop_eTaxa.SelectedIndex > -1)
            {
                tituloRendaFixa.ETaxa = Convert.ToInt32(drop_eTaxa.SelectedItem.Value);
            }

            if (drop_eJurosTipo.SelectedIndex > -1)
            {
                tituloRendaFixa.EJurosTipo = Convert.ToInt32(drop_eJurosTipo.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.EJurosTipo = null;
            }

            if (dropExecutaProRataLiquidacao.SelectedIndex > -1)
            {
                tituloRendaFixa.ProRataLiquidacao = Convert.ToString(dropExecutaProRataLiquidacao.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.ProRataLiquidacao = "N";
            }

            if (!string.IsNullOrEmpty(textDefasagemMeses.Text))
                tituloRendaFixa.DefasagemMeses = Convert.ToInt32(textDefasagemMeses.Text);
            else
                tituloRendaFixa.DefasagemMeses = null;

            if (dropAtivoRegra.SelectedIndex > -1)
            {
                tituloRendaFixa.AtivoRegra = Convert.ToString(dropAtivoRegra.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.AtivoRegra = "1"; //Sem regra especifica
            }

            if (!string.IsNullOrEmpty(textDataInicioCorrecao.Text))
                tituloRendaFixa.DataInicioCorrecao = Convert.ToDateTime(textDataInicioCorrecao.Text);
            else
                tituloRendaFixa.DataInicioCorrecao = null;

            if (!string.IsNullOrEmpty(textProRataEmissaoDia.Text))
                tituloRendaFixa.ProRataEmissaoDia = Convert.ToInt32(textProRataEmissaoDia.Text);
            else
                tituloRendaFixa.ProRataEmissaoDia = null;

            if (dropPeriodicidade.SelectedIndex > 0)
            {
                tituloRendaFixa.Periodicidade = Convert.ToInt32(dropPeriodicidade.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.Periodicidade = null;
            }

            if (dropExecutaProRataEmissao.SelectedIndex > -1)
            {
                tituloRendaFixa.ExecutaProRataEmissao = dropExecutaProRataEmissao.SelectedItem.Value.ToString(); ;
            }
            else
            {
                tituloRendaFixa.ExecutaProRataEmissao = "N";
            }

            if (dropTipoProRata.SelectedIndex > -1)
            {
                tituloRendaFixa.TipoProRata = Convert.ToInt32(dropTipoProRata.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.TipoProRata = 0;
            }

            if (dropCriterioAmortizacao.SelectedIndex > -1)
            {
                tituloRendaFixa.CriterioAmortizacao = Convert.ToInt32(dropCriterioAmortizacao.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.CriterioAmortizacao = null;
            }

            if (string.IsNullOrEmpty(textDefasagemLiquidacao.Text))
            {
                tituloRendaFixa.DefasagemLiquidacao = 0;
            }
            else
            {
                tituloRendaFixa.DefasagemLiquidacao = Convert.ToInt32(textDefasagemLiquidacao.Text);
            }

            tituloRendaFixa.PUNominal = Convert.ToDecimal(textPUNominal.Text);
            tituloRendaFixa.ValorNominal = Convert.ToDecimal(textPUNominal.Text);
            tituloRendaFixa.IdMoeda = Convert.ToInt32(dropMoeda.SelectedItem.Value);
            tituloRendaFixa.CodigoCustodia = textCodigoCustodia.Text.ToString();
            tituloRendaFixa.CodigoIsin = textCodigoISIN.Text.ToString();
            tituloRendaFixa.CodigoCBLC = textCodigoCBLC.Text.ToString();
            tituloRendaFixa.CodigoCetip = textCodigoCetip.Text.ToString();

            if (dropEstrategia.SelectedIndex != -1)
            {
                tituloRendaFixa.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.IdEstrategia = null;
            }

            tituloRendaFixa.DebentureConversivel = Convert.ToString(dropDebentureConversivel.SelectedItem.Value);
            tituloRendaFixa.DebentureInfra = Convert.ToString(dropDebentureInfra.SelectedItem.Value);
            tituloRendaFixa.CodigoInterface = textCodigoInterface.Text;
            tituloRendaFixa.PremioRebate = (string.IsNullOrEmpty(textPremioRebate.Text) ? 0 : Convert.ToDecimal(textPremioRebate.Text));

            if (dropPermiteRepactuacao.SelectedIndex > -1)
            {
                tituloRendaFixa.PermiteRepactuacao = dropPermiteRepactuacao.SelectedItem.Value.ToString(); ;
            }
            else
            {
                tituloRendaFixa.PermiteRepactuacao = "N";
            }

            if (!string.IsNullOrEmpty(textCodigoCusip.Text))
                tituloRendaFixa.CodigoCusip = textCodigoCusip.Text;
            else
                tituloRendaFixa.CodigoCusip = null;

            if (dropExecutaProRataEmissao.SelectedIndex > -1)
            {
                tituloRendaFixa.ExecutaProRataEmissao = dropExecutaProRataEmissao.SelectedItem.Value.ToString(); ;
            }
            else
            {
                tituloRendaFixa.ExecutaProRataEmissao = "N";
            }

            if (dropTipoProRata.SelectedIndex > -1)
            {
                tituloRendaFixa.TipoProRata = Convert.ToInt32(dropTipoProRata.SelectedItem.Value);
            }
            else
            {
                tituloRendaFixa.TipoProRata = 0;
            }

            int codicoCda = 0;
            if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
                tituloRendaFixa.CodigoCDA = codicoCda;

            if (papelAlterado)
            {
                PerfilMTMCollection perfilMTMColl = new PerfilMTMCollection();
                perfilMTMColl.Query.es.Top = 1;
                perfilMTMColl.Query.Where(perfilMTMColl.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value));

                if (perfilMTMColl.Query.Load())
                    this.AtualizaPerfilMTM(tituloRendaFixa.IdTitulo.Value, tituloRendaFixa.IdPapel.Value);
            }
            tituloRendaFixa.Participacao = chkParticipacao.Checked ? "S" : "N";

            tituloRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TituloRendaFixa - Operacao: Update TituloRendaFixa: " + idTitulo + UtilitarioWeb.ToString(tituloRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        SalvaEmpresaSecuritizada(idTitulo);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues("IdTitulo");
            for (int i = 0; i < keyValues1.Count; i++)
            {
                int idTitulo = Convert.ToInt32(keyValues1[i]);

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                if (tituloRendaFixa.LoadByPrimaryKey(idTitulo))
                {
                    bool achou = false;
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdTitulo.Equal(idTitulo));
                    if (posicaoRendaFixaCollection.Query.Load())
                    {
                        achou = true;
                    }

                    if (!achou)
                    {
                        PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
                        posicaoRendaFixaAberturaCollection.Query.Where(posicaoRendaFixaAberturaCollection.Query.IdTitulo.Equal(idTitulo));
                        if (posicaoRendaFixaAberturaCollection.Query.Load())
                        {
                            achou = true;
                        }
                        else
                        {
                            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
                            operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdTitulo.Equal(idTitulo));
                            if (operacaoRendaFixaCollection.Query.Load())
                            {
                                achou = true;
                            }
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Título " + tituloRendaFixa.Descricao.Trim() + " não pode ser excluído por ter posições e/ou movimentos relacionadas.");
                    }

                    TituloRendaFixa tituloRendaFixaClone = (TituloRendaFixa)Utilitario.Clone(tituloRendaFixa);
                    //
                    tituloRendaFixa.MarkAsDeleted();
                    tituloRendaFixa.Save();
                    //
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TituloRendaFixa - Operacao: Delete TituloRendaFixa: " + idTitulo + UtilitarioWeb.ToString(tituloRendaFixaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {

        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxTextBox textDescricao = pageControl.FindControl("textDescricao") as ASPxTextBox;
            e.Properties["cpTextDescricao"] = textDescricao.ClientID;

            TextBox hiddenIdEmissor = pageControl.FindControl("hiddenIdEmissor") as TextBox;
            e.Properties["cpHiddenIdEmissor"] = hiddenIdEmissor.ClientID;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }

    #region Funções relativas a AgendaEventosRendaFixa

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExcelImplantacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        // Limpa a Sessão
        Session.Remove("idTitulos");

        // Pega os Elementos Selecionados
        List<object> idTitulosSelecionados = gridCadastro.GetSelectedFieldValues(TituloRendaFixaMetadata.ColumnNames.IdTitulo);

        if (idTitulosSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Id Titulos.";
        }
        else
        {
            // Salva os idTitulos Selecionados na Sessão para posterior uso pelo Metodo de Upload
            Session["idTitulos"] = idTitulosSelecionados;
        }
    }

    /// <summary>
    /// Processa a Planilha de Agenda RendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplAgendaEventos_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação AgendaRendaFixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoAgendaEventos(sr);
            // Carrega Arquivo
            this.CarregaAgendaEventos();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Agenda Eventos - " + e2.Message;
            return;
        }
        #endregion

        // Limpa a Sessão
        Session.Remove("idTitulos");
    }

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoAgendaEventos(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "TipoEvento", "DataAgenda", "DataEvento", "DataPagamento", "Taxa", "Valor" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)TipoEvento     - 2)DataAgenda         - 3)DataEvento 
         *          4)DataPagamento  - 5)Taxa               - 6)Valor 
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;
        //
        try
        {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelAgendaRendaFixaReduzida item = new ValoresExcelAgendaEventosRendaFixa();
                //
                item.Tipo = (TipoEventoTitulo)workSheet.Cell(linha, coluna1).ValueAsInteger;
                item.DataAgenda = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                item.DataEvento = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                item.DataPagamento = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                //
                item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                //
                this.valoresExcelAgendaRendaFixa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5}", item.Tipo, item.DataAgenda.ToString("dd/MM/yyyy"),
                                    item.DataEvento.ToString("dd/MM/yyyy"), item.DataPagamento.ToString("dd/MM/yyyy"),
                                    item.Taxa, item.Valor);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega a Agenda de Eventos com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaAgendaEventos()
    {
        // Pega os idsTitulos Selecionados
        List<Object> idTitulos = (List<object>)Session["idTitulos"];

        AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        AgendaRendaFixaCollection agendaRendaFixaCollectionDeletar = new AgendaRendaFixaCollection();
        //
        for (int i = 0; i < idTitulos.Count; i++)
        {
            #region Registros Do Excel
            for (int j = 0; j < this.valoresExcelAgendaRendaFixa.Count; j++)
            {
                ValoresExcelAgendaRendaFixaReduzida valoresExcel = this.valoresExcelAgendaRendaFixa[j];

                #region TipoEvento Inconsistente
                if ((int)valoresExcel.Tipo < 1 || (int)valoresExcel.Tipo == 6 || (int)valoresExcel.Tipo > 7)
                {
                    throw new Exception("Tipo Evento Inconsistente. Valores Possíveis: 1, 2, 3, 4, 5, 7");
                }
                #endregion

                #region AgendaRendaFixa
                //
                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                agendaRendaFixa.IdTitulo = Convert.ToInt32(idTitulos[i]);
                agendaRendaFixa.TipoEvento = Convert.ToByte(valoresExcel.Tipo);
                agendaRendaFixa.DataAgenda = valoresExcel.DataAgenda;
                agendaRendaFixa.DataEvento = valoresExcel.DataEvento;
                agendaRendaFixa.DataPagamento = valoresExcel.DataPagamento;
                agendaRendaFixa.Valor = valoresExcel.Valor;
                agendaRendaFixa.Taxa = valoresExcel.Taxa;
                //
                agendaRendaFixaCollection.AttachEntity(agendaRendaFixa);

                #endregion
            }
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            if (idTitulos.Count != 0)
            { // Por Precaução só Deleta se tiver elementos selecionados no grid

                #region Deleta AgendaRendaFixa por Idtitulo
                agendaRendaFixaCollectionDeletar.Query.Where(agendaRendaFixaCollectionDeletar.Query.IdTitulo.In(idTitulos));
                agendaRendaFixaCollectionDeletar.Query.Load();
                agendaRendaFixaCollectionDeletar.MarkAllAsDeleted();
                agendaRendaFixaCollectionDeletar.Save();
                #endregion

                // Salva Agenda RendaFixa presente no Excel
                agendaRendaFixaCollection.Save();
            }

            scope.Complete();
        }
    }

    #endregion

    protected void dropPeriodicidade_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropPeriodicidade = (sender as ASPxComboBox);

        dropPeriodicidade.Items.Add(string.Empty, 0);
        foreach (int r in Enum.GetValues(typeof(Periodicidade)))
        {
            dropPeriodicidade.Items.Add(PeriodicidadeDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    protected void dropCriterioAmortizacao_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropCriterioAmortizacao = (sender as ASPxComboBox);

        foreach (int r in Enum.GetValues(typeof(CriterioAmortizacao)))
        {
            if (r == (int)CriterioAmortizacao.PUPar)
                continue;
            dropCriterioAmortizacao.Items.Add(CriterioAmortizacaoDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// Salva as empresas securitizadas vinculadas a emissão de ativos
    /// </summary>
    private void SalvaEmpresaSecuritizada(int idTitulo)
    {
        #region Deleta Empresa Securitizada
        EmpresaSecuritizadaCollection coll = new EmpresaSecuritizadaCollection();
        coll.Query.Where(coll.Query.IdTitulo.Equal(idTitulo));
        coll.LoadAll();
        coll.MarkAllAsDeleted();
        coll.Save();
        #endregion

        #region Inclui Empresa Securitizada
        EmpresaSecuritizada empresaSecuritizada = new EmpresaSecuritizada();
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxGridLookup gridLookupEmpresaSecuritizada = pageControl.FindControl("gridLookupEmpresaSecuritizada") as ASPxGridLookup;
        List<object> empresasSelecionadas = new List<object>();
        empresasSelecionadas = gridLookupEmpresaSecuritizada.GridView.GetSelectedFieldValues(AgenteMercadoMetadata.ColumnNames.IdAgente);

        foreach (object item in empresasSelecionadas)
        {
            empresaSecuritizada = new EmpresaSecuritizada();
            empresaSecuritizada.IdTitulo = idTitulo;
            empresaSecuritizada.IdAgenteMercado = Convert.ToInt32(item);
            empresaSecuritizada.Save();
        }
        #endregion
    }

    /// <summary>
    /// Carrega os items selecionados para a empresa securitizada
    /// </summary>
    protected void gridLookupEmpresaSecuritizada_OnDataBound(object source, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxSpinEdit textoIdTitulo = pageControl.FindControl("textIdTitulo") as ASPxSpinEdit;
            ASPxGridLookup gridLookupEmpresaSecuritizada = pageControl.FindControl("gridLookupEmpresaSecuritizada") as ASPxGridLookup;

            if (textoIdTitulo != null && !string.IsNullOrEmpty(textoIdTitulo.Text))
            {
                if (string.IsNullOrEmpty(gridLookupEmpresaSecuritizada.Text))
                {
                    EmpresaSecuritizadaCollection coll = new EmpresaSecuritizadaCollection();
                    coll.Query.Where(coll.Query.IdTitulo.Equal(Convert.ToInt32(textoIdTitulo.Text)));
                    coll.Query.Load();

                    foreach (EmpresaSecuritizada item in coll)
                    {
                        gridLookupEmpresaSecuritizada.GridView.Selection.SelectRowByKey(item.IdAgenteMercado);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Carrega lista para o controle
    /// Atribui valor em caso de uma solicitação de alteração
    /// </summary>
    protected void dropIsentoIR_OnLoad(object sender, EventArgs e)
    {
        //Instancia controle 
        ASPxComboBox dropIsentoIR = (ASPxComboBox)sender;

        //Verifica se o controle possui items carregados
        if (dropIsentoIR.Items.IsEmpty)
        {
            foreach (int r in Enum.GetValues(typeof(ListaIsentoIR)))
            {
                dropIsentoIR.Items.Add(ListaIsentoIRDescricao.RetornaStringValue(r), r.ToString());
            }
        }
        //Verifica se é uma alteração
        if (!gridCadastro.IsNewRowEditing)
        {
            //Atribui valor para o controle
            object papelIsentoIR = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "PapelIsentoIR");
            object isentoIR = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IsentoIR");
            if (papelIsentoIR.GetType().Name != "DBNull" && isentoIR == null)
            {
                dropIsentoIR.SelectedIndex = (Convert.ToInt32(papelIsentoIR) - 1);
            }
            else
            {
                if (isentoIR != null)
                    dropIsentoIR.SelectedIndex = (Convert.ToInt32(isentoIR) - 1);
            }
        }
    }

    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //Valores default do form
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.DebentureConversivel] = "N";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.DebentureInfra] = "N";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.EJuros] = Fincs_eJuros.DiasUteis252;
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.EJurosTipo] = Fincs_eJurosTipo.Composto;
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.ExecutaProRataEmissao] = "N";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.TipoProRata] = "0";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.PermiteRepactuacao] = "N";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.ProRataLiquidacao] = "N";
        e.NewValues[TituloRendaFixaMetadata.ColumnNames.AtivoRegra] = "1";
    }

    protected void callBackPapelLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (dropPapelLote.SelectedIndex != -1)
        {
            int idPapel = Convert.ToInt32(dropPapelLote.SelectedItem.Value);

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(idPapel);

            e.Result = papelRendaFixa.Classe.Value.ToString();
        }
    }
    protected void callBackPapel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        e.Result = string.Empty;
        if (pageControl != null)
        {
            ASPxComboBox dropPapel = pageControl.FindControl("dropPapel") as ASPxComboBox;
            if (dropPapel != null && dropPapel.SelectedIndex != -1)
            {
                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.LoadByPrimaryKey(Convert.ToInt32(dropPapel.SelectedItem.Value));


                DeParaTitRendaFixaJurosCollection deParaTitRFJuros_Coll = new DeParaTitRendaFixaJurosCollection();
                deParaTitRFJuros_Coll.Query.Where(deParaTitRFJuros_Coll.Query.TipoCurvaPAS.Equal(papelRendaFixa.TipoCurva)
                                                    & deParaTitRFJuros_Coll.Query.ContagemDiasPAS.Equal(papelRendaFixa.ContagemDias)
                                                    & deParaTitRFJuros_Coll.Query.BaseAnoPAS.Equal(papelRendaFixa.BaseAno)
                                                    & deParaTitRFJuros_Coll.Query.PagamentoJurosPAS.Equal(papelRendaFixa.PagamentoJuros));

                int eJuros = (int)Fincs_eJuros.DiasUteis252;
                int eJurosTipo = (int)Fincs_eJurosTipo.Composto;
                if (deParaTitRFJuros_Coll.Query.Load())
                {
                    eJuros = deParaTitRFJuros_Coll[0].EJurosFincs.Value;
                    eJurosTipo = deParaTitRFJuros_Coll[0].EJurosTipoFincs.Value;
                }

                StringBuilder strResult = new StringBuilder();

                strResult.Append(eJuros).Append("|");
                strResult.Append(eJurosTipo).Append("|");
                strResult.Append(papelRendaFixa.IsentoIR.GetValueOrDefault((int)TituloIsentoIR.Isento).ToString()).Append("|");
                strResult.Append(papelRendaFixa.Classe.Value);
                e.Result = strResult.ToString();
            }
        }
    }

    /// <summary>
    /// Atualiza todos os Perfil com o novo 
    /// </summary>
    /// <param name="idTitulo"></param>
    /// <param name="idPapelNovo"></param>
    private void AtualizaPerfilMTM(int idTitulo, int idPapelNovo)
    {
        using (esTransactionScope scope = new esTransactionScope())
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" UPDATE P ");
            sqlText.AppendLine(" SET P.IdPapel = ").Append(idPapelNovo);
            sqlText.AppendLine(" FROM PerfilMTM P ");
            sqlText.AppendLine(" INNER JOIN TituloRendaFixa T ON P.IdTitulo = T.IdTitulo");
            sqlText.AppendLine(" WHERE P.IdTitulo is not null");
            sqlText.AppendLine(" and P.IdTitulo = ").Append(idTitulo);

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlText.ToString());

            scope.Complete();
        }
    }
}