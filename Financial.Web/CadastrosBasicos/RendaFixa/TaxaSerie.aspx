﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxaSerie.aspx.cs" Inherits="CadastrosBasicos_TaxaSerie" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDownWithCallback;
        var operacao = '';
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Taxas (Séries)"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                            <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                            </dxpc:ASPxPopupControl>
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                    <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                    CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton></div>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="CompositeKey"
                                    DataSourceID="EsDSTaxaSerie" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                    OnLoad="gridCadastro_Load" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                    OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="1" Width="10%" />
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Série" FieldName="IdSerie" VisibleIndex="2"
                                            Width="50%">
                                            <PropertiesComboBox DataSourceID="EsDSSerieRendaFixa" TextField="DescricaoCompleta" ValueField="IdSerie" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataSpinEditColumn FieldName="Taxa" Caption="Taxa" VisibleIndex="3"
                                            Width="15%">
                                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00);0.00000000}">
                                            </PropertiesSpinEdit>
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            <CellStyle HorizontalAlign="Right">
                                            </CellStyle>
                                        </dxwgv:GridViewDataSpinEditColumn>
                                        <dxwgv:GridViewDataComboBoxColumn FieldName="DigitadoImportado" VisibleIndex="4">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                    </Columns>
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                        <EditForm>
                                            <div class="editForm">
                                                <table>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelSerie" runat="server" CssClass="labelRequired" AssociatedControlID="dropSerie"
                                                                Text="Série:">
                                                            </asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropSerie" runat="server" DataSourceID="EsDSSerieRendaFixa"
                                                                ValueField="IdSerie" TextField="DescricaoCompleta" CssClass="dropDownListLongo" Text='<%#Eval("IdSerie")%>'
                                                                OnLoad="dropSerie_Load">
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>                                                
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>'
                                                                OnInit="textData_Init" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelTaxa" runat="server" CssClass="labelRequired" Text="Taxa:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textTaxa" runat="server" ClientInstanceName="textTaxa" CssClass="textTaxa_5"
                                                                Text='<%# Eval("Taxa") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal7" runat="server" Text="OK+" /><div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton></div>
                                            </div>
                                        </EditForm>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="250px" />
                                    <Templates>
                                        <StatusBar>
                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                        </StatusBar>
                                    </Templates>
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    <cc1:esDataSource ID="EsDSTaxaSerie" runat="server" OnesSelect="EsDSTaxaSerie_esSelect" />
    <cc1:esDataSource ID="EsDSSerieRendaFixa" runat="server" OnesSelect="EsDSSerieRendaFixa_esSelect" />
    </form>
</body>
</html>
