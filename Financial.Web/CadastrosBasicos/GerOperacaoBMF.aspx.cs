﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Gerencial;

public partial class CadastrosBasicos_GerOperacaoBMF : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBMF = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSGerOperacaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GerOperacaoBMFQuery gerOperacaoBMFQuery = new GerOperacaoBMFQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        gerOperacaoBMFQuery.Select(gerOperacaoBMFQuery, clienteQuery.Apelido.As("Apelido"));
        gerOperacaoBMFQuery.InnerJoin(clienteQuery).On(gerOperacaoBMFQuery.IdCliente == clienteQuery.IdCliente);
        gerOperacaoBMFQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        gerOperacaoBMFQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text))
        {
            gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.CdAtivoBMF.Like("%" + textCdAtivo.Text + "%"));
        }

        if (!String.IsNullOrEmpty(textSerie.Text))
        {
            gerOperacaoBMFQuery.Where(gerOperacaoBMFQuery.Serie.Like("%" + textSerie.Text + "%"));
        }

        gerOperacaoBMFQuery.OrderBy(gerOperacaoBMFQuery.Data.Descending);

        GerOperacaoBMFCollection coll = new GerOperacaoBMFCollection();
        coll.Load(gerOperacaoBMFQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            //TipoMercado, Peso
            string cdAtivoBMF = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBMF").ToString();
            string serie = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Serie").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;

            AtivoBMF ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

            string tipoMercado = TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(ativoBMF.TipoMercado));
            string peso = ativoBMF.Peso.ToString();

            textTipoMercado.Text = tipoMercado;
            textPeso.Text = peso;
            //

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropTrader);
            controles.Add(dropTipoOperacao);
            controles.Add(btnEditAtivoBMF);
            controles.Add(textQuantidade);
            controles.Add(textPU);
            controles.Add(textValor);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data da operação não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }

            // Se Data está Preenchida e Existe Ativo compara a Data de Vencimento
            if (!String.IsNullOrEmpty(textData.Text)) {
                AtivoBMF a = new AtivoBMF();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(a.Query.DataVencimento);
                string cdAtivo = btnEditAtivoBMF.Text.Trim().Substring(0, 3);
                string serie = btnEditAtivoBMF.Text.Trim().Substring(3);

                if (a.LoadByPrimaryKey(campos, cdAtivo, serie)) {
                    if (a.DataVencimento.HasValue && a.DataVencimento < Convert.ToDateTime(textData.Text)) {
                        e.Result = "Ativo Vencido em: " + a.DataVencimento.Value.ToString("d");
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;

                        resultado = this.gridCadastro.IsNewRowEditing
                                   ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                   : nome;                       
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBMF = "";
        string serie = "";
        string peso = "";
        string tipoMercado = "";
        e.Result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") 
        {
            AtivoBMF ativoBMF = new AtivoBMF();
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(e.Parameter));
            string paramSerie = ativoBMF.RetornaSerieSplitada(Convert.ToString(e.Parameter));
            ativoBMF.LoadByPrimaryKey(paramAtivo, paramSerie);

            if (ativoBMF.str.CdAtivoBMF != "") {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                tipoMercado = TipoMercadoBMF.str.RetornaTexto(Convert.ToInt32(ativoBMF.TipoMercado));
                peso = ativoBMF.Peso.ToString();
                e.Result = cdAtivoBMF + serie + "|" + tipoMercado + "|" + peso;
            }            
        }

    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        //
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        GerOperacaoBMF operacaoBMF = new GerOperacaoBMF();
        operacaoBMF.IdCliente = idCliente;
        //
        AtivoBMF ativoBMF = new AtivoBMF();
        //
        string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
        string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
        operacaoBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        
        ativoBMF = new AtivoBMF();
        ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
        byte tipoMercado = ativoBMF.TipoMercado.Value;

        operacaoBMF.CdAtivoBMF = cdAtivoBMF;
        operacaoBMF.Serie = serie;
        operacaoBMF.TipoMercado = tipoMercado;
        operacaoBMF.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
        operacaoBMF.Pu = Convert.ToDecimal(textPU.Text);
        operacaoBMF.Valor = Convert.ToDecimal(textValor.Text);
        operacaoBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);
        operacaoBMF.Data = Convert.ToDateTime(textData.Text);
        operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
        operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Manual;

        operacaoBMF.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoBMF - Operacao: Insert OperacaoBMF: " + operacaoBMF.IdOperacao + UtilitarioWeb.ToString(operacaoBMF),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        /* Atualiza StatusRealTime para executar */
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textTipoMercado = gridCadastro.FindEditFormTemplateControl("textTipoMercado") as TextBox;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;

        GerOperacaoBMF operacaoBMF = new GerOperacaoBMF();
        if (operacaoBMF.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoBMF.IdCliente = idCliente;
            AtivoBMF ativoBMF = new AtivoBMF();
            string cdAtivoBMF = ativoBMF.RetornaCdAtivoSplitado(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
            string serie = ativoBMF.RetornaSerieSplitada(Convert.ToString(btnEditAtivoBMF.Text)).ToUpper();
            operacaoBMF.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            
            ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
            byte tipoMercado = ativoBMF.TipoMercado.Value;

            operacaoBMF.CdAtivoBMF = cdAtivoBMF;
            operacaoBMF.Serie = serie;
            operacaoBMF.TipoMercado = tipoMercado;
            operacaoBMF.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
            operacaoBMF.Pu = Convert.ToDecimal(textPU.Text);
            operacaoBMF.Valor = Convert.ToDecimal(textValor.Text);
            operacaoBMF.Quantidade = Convert.ToInt32(textQuantidade.Text);
            operacaoBMF.Data = Convert.ToDateTime(textData.Text);

            operacaoBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoBMF - Operacao: Update OperacaoBMF: " + operacaoBMF.IdOperacao + UtilitarioWeb.ToString(operacaoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                GerOperacaoBMF operacaoBMF = new GerOperacaoBMF();
                if (operacaoBMF.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoBMF.IdCliente.Value;

                    //
                    GerOperacaoBMF operacaoBMFClone = (GerOperacaoBMF)Utilitario.Clone(operacaoBMF);
                    //

                    operacaoBMF.MarkAsDeleted();
                    operacaoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoBMF - Operacao: Delete OperacaoBMF: " + idOperacao + UtilitarioWeb.ToString(operacaoBMFClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox textPeso = gridCadastro.FindEditFormTemplateControl("textPeso") as TextBox;
            e.Properties["cpTextPeso"] = textPeso.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e) {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxButtonEdit btnEditAtivoBMF = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBMF") as ASPxButtonEdit;

            string cdAtivoBMF = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "CdAtivoBMF" }));
            string serie = Convert.ToString(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Serie" }));

            btnEditAtivoBMF.Text = cdAtivoBMF + serie;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}