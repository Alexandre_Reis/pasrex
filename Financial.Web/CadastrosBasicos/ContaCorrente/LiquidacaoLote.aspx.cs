﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Contabil;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_LiquidacaoLote : CadastroBasePage
{       
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        //        
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoMetadata.ColumnNames.Fonte }));
        
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        liquidacaoQuery.Select(liquidacaoQuery, clienteQuery.Apelido.As("Apelido"));
        liquidacaoQuery.InnerJoin(clienteQuery).On(liquidacaoQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicioLancamento.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.GreaterThanOrEqual(textDataInicioLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataFimLancamento.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.LessThanOrEqual(textDataFimLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textDescricaoFiltro.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.Descricao.Like("%" + textDescricaoFiltro.Text + "%"));
        }

        liquidacaoQuery.Where(liquidacaoQuery.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao));
        liquidacaoQuery.OrderBy(liquidacaoQuery.DataVencimento.Descending, liquidacaoQuery.DataLancamento.Descending);

        LiquidacaoCollection coll = new LiquidacaoCollection();
        coll.Load(liquidacaoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region CallBacks
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") 
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;
                            //

                            nome = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (dropOrigem.SelectedIndex > -1 || textNovoVencimento.Text != "")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdLiquidacao");

            string mensagem = "";
            if (keyValuesId.Count > 0)
            {
                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    int idLiquidacao = Convert.ToInt32(keyValuesId[i]);

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.LoadByPrimaryKey(idLiquidacao);

                    Cliente cliente = new Cliente();
                    cliente.Query.Select(cliente.Query.DataDia);
                    cliente.Query.Select(cliente.Query.Status);
                    cliente.Query.Where(cliente.Query.IdCliente.Equal(liquidacao.IdCliente.Value));
                    cliente.Query.Load();

                    if (dropOrigem.SelectedIndex > -1)
                    {
                        liquidacao.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value);
                    }

                    if (textNovoVencimento.Text != "")
                    {
                        if (cliente.DataDia.Value > liquidacao.DataVencimento.Value)
                        {
                            mensagem = "Lançamento com descrição " + liquidacao.Descricao + " possui data de vencimento " + liquidacao.DataVencimento.Value.ToShortDateString() + " anterior à data dia (" + cliente.DataDia.Value.ToShortDateString() + ") do cliente " + liquidacao.IdCliente.Value.ToString();
                            break;
                        }
                        else if (cliente.DataDia.Value > Convert.ToDateTime(textNovoVencimento.Text))
                        {
                            mensagem = "Lançamento com descrição " + liquidacao.Descricao + " e data de vencimento " + textNovoVencimento.Text + " é posterior à data dia do cliente " + liquidacao.IdCliente.Value.ToString();
                            break;
                        }
                        else
                        {
                            liquidacao.DataVencimento = Convert.ToDateTime(textNovoVencimento.Text);
                        }
                    }

                    liquidacao.Save();

                    LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                    liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdLiquidacao.Equal(liquidacao.IdLiquidacao.Value));
                    liquidacaoHistoricoCollection.Query.Load();

                    foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection)
                    {
                        liquidacaoHistorico.Origem = liquidacao.Origem.Value;
                    }
                    liquidacaoHistoricoCollection.Save();

                    //Para lactos de pagto de tx adm/gestao e pfee, preciso alterar o vcto, descricao e valor em LiquidacaoAbertura
                    if (liquidacao.Fonte == (byte)FonteLancamentoLiquidacao.Interno &&
                        (liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao ||
                        liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao ||
                        liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance ||
                        liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros))
                    {
                        DateTime dataDia = cliente.DataDia.Value;

                        LiquidacaoAbertura liquidacaoAbertura = new LiquidacaoAbertura();
                        if (liquidacaoAbertura.LoadByPrimaryKey(liquidacao.IdLiquidacao.Value, dataDia))
                        {
                            if (dropOrigem.SelectedIndex > -1)
                            {
                                liquidacao.Origem = Convert.ToInt32(dropOrigem.SelectedItem.Value); ;
                            }

                            if (textNovoVencimento.Text != "")
                            {
                                liquidacaoAbertura.DataVencimento = Convert.ToDateTime(textNovoVencimento.Text);
                            }
                            
                            liquidacaoAbertura.Save();
                        }
                    }
                    
                }


                if (mensagem == "")
                {
                    e.Result = "Processo executado com sucesso.";
                }
                else
                {
                    e.Result = "Processo executado parcialmente. " + mensagem;
                }
            }
            else
            {
                e.Result = "Ao menos 1 linha precisa ser selecionada.";
            }
        }
        else
        {
            e.Result = "A nova origem ou nova data de vencimento deve ser escolhida.";
        }
        
    }
    #endregion
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Valor") {
            decimal value = (decimal)e.GetValue("Valor");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        if (e.DataColumn.FieldName == "Descricao") {
            e.Cell.Attributes.Add("title", e.CellValue.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        
        base.gridCadastro_PreRender(sender, e);
        //
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicioLancamento.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Lacto) >= ").Append(textDataInicioLancamento.Text.Substring(0, 10));
        }
        if (textDataFimLancamento.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Lacto) <= ").Append(textDataFimLancamento.Text.Substring(0, 10));
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Vcto) >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Vcto) <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textDescricaoFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Descrição Like %").Append(textDescricaoFiltro.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }    
    
}