﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OperacaoCambio.aspx.cs" Inherits="CadastrosBasicos_OperacaoCambio" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;      
    var operacao = '';
      
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }  
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }   
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackBanco" runat="server" OnCallback="callbackBanco_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var resultSplit = e.result.split('|');                        
                        
                if (resultSplit[0] == 'origem')
                {
                    textBancoOrigem.SetValue(resultSplit[1]);
                }
                else
                {
                    textBancoDestino.SetValue(resultSplit[1]);
                }                
            }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {                
                    gridCadastro.UpdateEdit();                      
                }                  
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                        
                if (gridCadastro.cp_EditVisibleIndex == -1)
                {  
                    var resultSplit = e.result.split('|');                        
                    e.result = resultSplit[0];
                    var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                    OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
                }
                else
                {
                    var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                    if (gridCadastro.cp_EditVisibleIndex == 'new')
                    {                    
                        OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textDataRegistro); 
                        textDataLiquidacao.SetValue(textDataRegistro.GetValue());
                    }
                    else
                    {
                        OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
                    }
                    
                    if(window.tmp_MultiContaPendente && window.tmp_MultiContaPendente === true){
                        //so vamos dar rebind no drop se o id do cliente tiver mudado
                        window.tmp_MultiContaPendente = false;
                        
                        dropContaOrigem.PerformCallback(); 
                        dropContaDestino.PerformCallback();
                        callbackBanco.SendCallback('origem');
                        callbackBanco.SendCallback('destino');
                    }
                }    
            
            
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Operações de Câmbio"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOperacaoCambio" 
                                        OnRowUpdating="gridCadastro_RowUpdating" 
                                        OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" 
                                        OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" 
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="22%"/>
                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" Caption="Registro" VisibleIndex="6" Width="8%" />
                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataLiquidacao" Caption="Liquidação" VisibleIndex="8" Width="8%" />
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdMoedaOrigem" Caption="Moeda Origem" VisibleIndex="10" Width="10%" HeaderStyle-HorizontalAlign="Center">
                                            <PropertiesComboBox DataSourceID="EsDSMoeda" TextField="Nome" ValueField="IdMoeda">
                                            </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="PrincipalOrigem" Caption="Principal Origem" VisibleIndex="12" Width="15%"
                                                HeaderStyle-HorizontalAlign="Center" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>                                                
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdMoedaDestino" Caption="Moeda Destino" VisibleIndex="14" Width="10%" HeaderStyle-HorizontalAlign="Center">
                                            <PropertiesComboBox DataSourceID="EsDSMoeda" TextField="Nome" ValueField="IdMoeda">
                                            </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="PrincipalDestino" Caption="Principal Destino" VisibleIndex="16" Width="15%"
                                                HeaderStyle-HorizontalAlign="Center" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>                                                
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Visible="false" />                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdContaOrigem" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdContaDestino" Visible="false" />
                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                    
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {window.tmp_MultiContaPendente = true; document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                                          ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                                                          
                                                                                          gotfocus="function(s, e) {
                                                                                            if(!window.tmp_CodigoCliente){
                                                                                                window.tmp_CodigoCliente=btnEditCodigoCliente.GetValue();
                                                                                            }
                                                                                            else if(window.tmp_CodigoCliente !== btnEditCodigoCliente.GetValue()){ 
                                                                                                window.tmp_CodigoCliente = btnEditCodigoCliente.GetValue();
                                                                                                window.tmp_MultiContaPendente = true; 
                                                                                            }
                                                                                         }"
                                                                                          valuechanged="function(s, e) {
                                                                                              dropContaOrigem.PerformCallback();
                                                                                              dropContaDestino.PerformCallback();
                                                                                            }"
                                                                                          LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataRegistro" runat="server" CssClass="labelRequired" Text="Registro:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                       Value='<%#Eval("DataRegistro")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelRequired" Text="Liquidação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao"
                                                                        Value='<%#Eval("DataLiquidacao")%>' />
                                                                </td>
                                                            </tr>   
                                                            
                                                            <tr>
                                                                <td class="td_Label" >
                                                                    <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Paridade:"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxSpinEdit ID="textParidade" runat="server" ClientInstanceName="textParidade" Text='<%# Eval("TaxaCambio") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="4" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            
                                                                                                                     
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="border-bottom: solid 1px #B7B7B7; width: 100%;">
                                                                        Origem</div>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label" >
                                                                    <asp:Label ID="labelMoedaOrigem" runat="server" CssClass="labelRequired" Text="Moeda:"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxComboBox ID="dropMoedaOrigem" runat="server" ClientInstanceName="dropMoedaOrigem"
                                                                        DataSourceID="EsDSMoeda" ShowShadow="false" CssClass="dropDownListCurto" TextField="Nome"
                                                                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                                        ValueField="IdMoeda" Text='<%#Eval("IdMoedaOrigem")%>'>
                                                                        <clientsideevents valuechanged="function(s, e) {
                                                                                dropContaOrigem.PerformCallback();
                                                                           }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelConta" runat="server" CssClass="labelRequired" Text="Conta:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropContaOrigem" runat="server" ClientInstanceName="dropContaOrigem" EnableSynchronizationOnPerformCallback="true"
                                                                        DataSourceID="EsDSContaCorrenteOrigem" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        OnCallback="dropContaOrigem_Callback" CssClass="dropDownListCurto" TextField="Numero" ValueField="IdContaOrigem" Text='<%#Eval("IdContaOrigem") %>'>
                                                                         <validationsettings>
                                                                        <RequiredField IsRequired="True" />
                                                                        </validationsettings>
                                                                        <ClientSideEvents ValueChanged="function(s, e) {  callbackBanco.SendCallback('origem');  }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>     
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Banco:"></asp:Label>
                                                                </td>                                                                                                                   
                                                                <td colspan="3">
                                                                    <dxe:ASPxTextBox ID="textBancoOrigem" runat="server" Width="400" ForeColor="black" ClientInstanceName="textBancoOrigem" Text="" BackColor="LightGray" ClientEnabled="false" OnInit="textBancoOrigem_Init">
                                                                    </dxe:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCalculo" runat="server" CssClass="labelNormal" Text="Principal:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textPrincipalOrigem" runat="server" ClientInstanceName="textPrincipalOrigem" Text='<%# Eval("PrincipalOrigem") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Custos:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textCustosOrigem" runat="server" ClientInstanceName="textCustosOrigem" Text='<%# Eval("CustosOrigem") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Tributos:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTributosOrigem" runat="server" ClientInstanceName="textTributosOrigem" Text='<%# Eval("TributosOrigem") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Total:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTotalOrigem" runat="server" ClientInstanceName="textTotalOrigem" Text='<%# Eval("TotalOrigem") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            
                                                            
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="border-bottom: solid 1px #B7B7B7; width: 100%;">
                                                                        Destino</div>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label"  >
                                                                    <asp:Label ID="labelMoedaDestino" runat="server" CssClass="labelRequired" Text="Moeda:"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxComboBox ID="dropMoedaDestino" runat="server" ClientInstanceName="dropMoedaDestino"
                                                                        DataSourceID="EsDSMoeda" ShowShadow="false" CssClass="dropDownListCurto" TextField="Nome"
                                                                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                                        ValueField="IdMoeda" Text='<%#Eval("IdMoedaDestino")%>'>
                                                                        <clientsideevents valuechanged="function(s, e) {
                                                                                dropContaDestino.PerformCallback();
                                                                           }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>                                                                
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Conta:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropContaDestino" runat="server" ClientInstanceName="dropContaDestino" EnableSynchronizationOnPerformCallback="true"
                                                                        DataSourceID="EsDSContaCorrenteDestino" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        OnCallback="dropContaDestino_Callback" CssClass="dropDownListCurto" TextField="Numero" ValueField="IdContaDestino" Text='<%#Eval("IdContaDestino") %>' >
                                                                        <validationsettings>
                                                                        <RequiredField IsRequired="True" />
                                                                        </validationsettings>
                                                                        <ClientSideEvents ValueChanged="function(s, e) {  callbackBanco.SendCallback('destino');  }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>     
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Banco:"></asp:Label>
                                                                </td>                                                                                                                   
                                                                <td colspan="3">
                                                                    <dxe:ASPxTextBox ID="textBancoDestino" runat="server" Width="400" ForeColor="black" ClientInstanceName="textBancoDestino" Text="" BackColor="LightGray" ClientEnabled="false" OnInit="textBancoDestino_Init">                                                                        
                                                                    </dxe:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                                                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Principal:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textPrincipalDestino" runat="server" ClientInstanceName="textPrincipalDestino" Text='<%# Eval("PrincipalDestino") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Custos:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textCustosDestino" runat="server" ClientInstanceName="textCustosDestino" Text='<%# Eval("CustosDestino") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Tributos:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTributosDestino" runat="server" ClientInstanceName="textTributosDestino" Text='<%# Eval("TributosDestino") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Total:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTotalDestino" runat="server" ClientInstanceName="textTotalDestino" Text='<%# Eval("TotalDestino") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal1" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                            
                                        </Templates>
                                        
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                        
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"/>
        
        <cc1:esDataSource ID="EsDSOperacaoCambio" runat="server" OnesSelect="EsDSOperacaoCambio_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrenteOrigem" runat="server" OnesSelect="EsDSContaCorrenteOrigem_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrenteDestino" runat="server" OnesSelect="EsDSContaCorrenteDestino_esSelect" />
        <cc1:esDataSource ID="EsDSParidadeCambial" runat="server" OnesSelect="EsDSParidadeCambial_esSelect" />        
    </form>
</body>
</html>