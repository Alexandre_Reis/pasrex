﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Contabil;
using Financial.Contabil.Enums;

public partial class CadastrosBasicos_Liquidacao : CadastroBasePage {

    #region Enums
    public enum IntegracaoBolsa {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoBMF {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoCC {
        NaoIntegra = 0,
        Sinacor = 1
    }
    #endregion
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        //        
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoMetadata.ColumnNames.Fonte }));

        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
        {
            gridCadastro.Columns[8].Visible = false;
        }

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {window.tmp_CodigoCliente = undefined; editFormOpen=true; gridCadastro.StartEditRow(e.visibleIndex);}";

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        liquidacaoQuery.Select(liquidacaoQuery, clienteQuery.Apelido.As("Apelido"));
        liquidacaoQuery.InnerJoin(clienteQuery).On(liquidacaoQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicioLancamento.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.GreaterThanOrEqual(textDataInicioLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataFimLancamento.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataLancamento.LessThanOrEqual(textDataFimLancamento.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textDescricaoFiltro.Text)) {
            liquidacaoQuery.Where(liquidacaoQuery.Descricao.Like("%" + textDescricaoFiltro.Text + "%"));
        }

        liquidacaoQuery.Where(liquidacaoQuery.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao));
        liquidacaoQuery.OrderBy(liquidacaoQuery.DataVencimento.Descending, liquidacaoQuery.DataLancamento.Descending);

        LiquidacaoCollection coll = new LiquidacaoCollection();
        coll.Load(liquidacaoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoLiquidante == 'S');
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropMoeda = this.gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;

        if (btnEditCodigoCliente != null && btnEditCodigoCliente.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ContaCorrenteCollection coll = new ContaCorrenteCollection();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa));

            if (dropMoeda.SelectedIndex != -1)
            {
                coll.Query.Where(coll.Query.IdMoeda.Equal(Convert.ToInt32(dropMoeda.SelectedItem.Value)));
            }

            coll.Query.OrderBy(coll.Query.Numero.Ascending);
            coll.Query.Load();

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }

    protected void EsDSEvento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        coll.Query.OrderBy(coll.Query.Origem.Ascending, coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEventoFinanceiro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EventoFinanceiroCollection coll = new EventoFinanceiroCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDS_Sinacor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {        
        if (!Page.IsPostBack) {
            #region Condição Inicial
            gridConsultaSinacor.Visible = false;
            e.Collection = new VTccmovtoCollection();
            #endregion
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            e.Collection = new VTccmovtoCollection();
            this.TrataCamposObrigatoriosSinacor();  // Tratamento de Cliente Nulo em uma segunda consulta
            //

            #region Consulta Sinacor
            int idCliente = Convert.ToInt32(this.btnEditCodigoClienteSinacorFiltro.Text);
            ClienteBolsa clienteBolsa = new ClienteBolsa();

            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                return;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Código Bovespa inválido ou inexistente para o IdCliente " + idCliente);
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Código Bovespa inválido ou inexistente para o IdCliente " + idCliente);
            }

            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
            }
            int codigoCliente2 = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            List<Int32> listaCodigos = new List<int>();
            listaCodigos.Add(codigoCliente);
            listaCodigos.Add(codigoCliente2);

            VTccmovtoCollection vtccMovtoCollection = new VTccmovtoCollection();
            DateTime? dataInicio = null;
            DateTime? dataFim = null;

            if (!String.IsNullOrEmpty(textDataInicioSinacor.Text))
            {
                dataInicio = Convert.ToDateTime(textDataInicioSinacor.Text);
            }

            if (!String.IsNullOrEmpty(textDataFimSinacor.Text))
            {
                dataFim = Convert.ToDateTime(textDataFimSinacor.Text);
            }
            
            vtccMovtoCollection.BuscaLiquidacoes(listaCodigos, dataInicio, dataFim);

            foreach (VTccmovto vtccMovto in vtccMovtoCollection) {
                vtccMovto.SetColumn("IdCliente", (int)idCliente);
            }

            e.Collection = vtccMovtoCollection;
            #endregion

            // Define Grid como Visivel
            gridConsultaSinacor.Visible = true;
            
            gridConsultaSinacor.Selection.UnselectAll();
            gridConsultaSinacor.CancelEdit();
        }              
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    #region CallBacks
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") 
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;
                            //

                            nome = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string retorno = "";
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        dataDia = cliente.DataDia.Value;
                        //

                        retorno = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|";

                        SaldoCaixa saldoCaixa = new SaldoCaixa();
                        saldoCaixa.Query.Select(saldoCaixa.Query.SaldoAbertura.Sum());
                        saldoCaixa.Query.Where(saldoCaixa.Query.IdCliente.Equal(idCliente),
                                               saldoCaixa.Query.Data.Equal(dataDia));
                        saldoCaixa.Query.Load();

                        decimal saldoAbertura = 0;
                        if (saldoCaixa.SaldoAbertura.HasValue) {
                            saldoAbertura = saldoCaixa.SaldoAbertura.Value;
                        }

                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
                        liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCliente),
                                                liquidacao.Query.DataVencimento.Equal(dataDia),
                                                liquidacao.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Normal));
                        liquidacao.Query.Load();

                        decimal vencimentos = 0;
                        if (liquidacao.Valor.HasValue) {
                            vencimentos = liquidacao.Valor.Value;
                        }

                        decimal saldoFechamento = saldoAbertura + vencimentos;

                        retorno = retorno + String.Format("{0:n}", saldoAbertura) + "|" + String.Format("{0:n}", vencimentos) + "|" + String.Format("{0:n}", saldoFechamento);
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = retorno;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallbackSinacor_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }   
    #endregion

    #region Insert/Update/Delete
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        int idLiquidacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxComboBox dropSituacao = gridCadastro.FindEditFormTemplateControl("dropSituacao") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;
        ASPxComboBox dropEventoFinanceiro = gridCadastro.FindEditFormTemplateControl("dropEventoFinanceiro") as ASPxComboBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        int idConta = 0;
        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        bool contabil = ParametrosConfiguracaoSistema.Outras.CalculaContatil;

        if (!multiConta) {
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
            contaCorrenteCollection.BuscaContaCorrente(idCliente);

            idConta = contaCorrenteCollection[0].IdConta.Value;
        }
        else {
            idConta = Convert.ToInt32(dropConta.SelectedItem.Value);
        }
        
        int? idAgente = null;
        if (dropAgenteMercado.SelectedIndex != -1) {
            idAgente = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
        }

        Liquidacao liquidacao = new Liquidacao();
        liquidacao.LoadByPrimaryKey(idLiquidacao);

        int contaOriginal = liquidacao.IdConta.Value;

        liquidacao.IdCliente = idCliente;
        liquidacao.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);

        bool isPendencia = dropSituacao.SelectedIndex == 1;
        if (isPendencia) {
            liquidacao.DataVencimento = new DateTime(4000, 02, 29);
        }
        else {
            liquidacao.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        liquidacao.Descricao = textDescricao.Text;
        liquidacao.Valor = Convert.ToDecimal(textValor.Text);
        liquidacao.IdAgente = idAgente;
        liquidacao.Situacao = Convert.ToByte(dropSituacao.SelectedItem.Value);
        liquidacao.IdConta = idConta;

        liquidacao.IdEvento = null;
        liquidacao.IdEventoFinanceiro = null;

        if (contabil)
        {
            if (dropEvento.SelectedIndex > -1)
            {
                liquidacao.IdEvento = Convert.ToInt32(dropEvento.SelectedItem.Value);
            }
            if (dropEventoFinanceiro.SelectedIndex > -1)
            {
                liquidacao.IdEventoFinanceiro = Convert.ToInt32(dropEventoFinanceiro.SelectedItem.Value);
            }
        }
            

        liquidacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Liquidacao - Operacao: Update Liquidacao: " + idLiquidacao + UtilitarioWeb.ToString(liquidacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Para lactos de pagto de tx adm/gestao e pfee, preciso alterar o vcto, descricao e valor em LiquidacaoAbertura
        if (liquidacao.Fonte == (byte)FonteLancamentoLiquidacao.Interno &&
            (liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao ||
            liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao ||
            liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance ||
            liquidacao.Origem == (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros)) {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            LiquidacaoAbertura liquidacaoAbertura = new LiquidacaoAbertura();
            if (liquidacaoAbertura.LoadByPrimaryKey(liquidacao.IdLiquidacao.Value, dataDia)) {
                liquidacaoAbertura.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                liquidacaoAbertura.Descricao = textDescricao.Text;
                liquidacaoAbertura.Valor = Convert.ToDecimal(textValor.Text);
                liquidacaoAbertura.Save();
            }
        }

        if (idConta != contaOriginal)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            LiquidacaoAbertura liquidacaoAbertura = new LiquidacaoAbertura();
            if (liquidacaoAbertura.LoadByPrimaryKey(liquidacao.IdLiquidacao.Value, dataDia))
            {
                liquidacaoAbertura.IdConta = idConta;
                liquidacaoAbertura.Save();
            }
        }

        //Atualiza StatusRealTime para executar***********
        Cliente clienteStatusRealTime = new Cliente();
        clienteStatusRealTime.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropSituacao = gridCadastro.FindEditFormTemplateControl("dropSituacao") as ASPxComboBox;
        ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;
        ASPxComboBox dropEventoFinanceiro = gridCadastro.FindEditFormTemplateControl("dropEventoFinanceiro") as ASPxComboBox;
        
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        int idConta = 0;
        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        bool contabil = ParametrosConfiguracaoSistema.Outras.CalculaContatil;

        if (!multiConta) {
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
            contaCorrenteCollection.BuscaContaCorrente(idCliente);

            idConta = contaCorrenteCollection[0].IdConta.Value;
        }
        else {
            idConta = Convert.ToInt32(dropConta.SelectedItem.Value);
        }

        int? idAgente = null;
        if (dropAgenteMercado.SelectedIndex != -1) {
            idAgente = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
        }

        Liquidacao liquidacao = new Liquidacao();
        liquidacao.IdCliente = idCliente;
        liquidacao.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);

        bool isPendencia = dropSituacao.SelectedIndex == 1;
        if (isPendencia) {
            liquidacao.DataVencimento = new DateTime(4000, 02, 29);
        }
        else {
            liquidacao.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        liquidacao.Descricao = textDescricao.Text;
        liquidacao.Valor = Convert.ToDecimal(textValor.Text);
        liquidacao.IdAgente = idAgente;
        liquidacao.Fonte = Convert.ToByte(FonteLancamentoLiquidacao.Manual);
        liquidacao.Origem = Convert.ToInt32(OrigemLancamentoLiquidacao.Outros);

        if (this.clienteGerencial(idCliente, Convert.ToDateTime(textDataLancamento.Text)))
            liquidacao.Origem = Convert.ToInt32(OrigemLancamentoLiquidacao.CarteiraGerencial);

        liquidacao.Situacao = Convert.ToByte(dropSituacao.SelectedItem.Value);
        liquidacao.IdConta = idConta;

        liquidacao.IdEvento = null;
        liquidacao.IdEventoFinanceiro = null;
        if (contabil)
        {
            if (dropEvento.SelectedIndex > -1)
            {
                liquidacao.IdEvento = Convert.ToInt32(dropEvento.SelectedItem.Value);
            }

            if (dropEventoFinanceiro.SelectedIndex > -1)
            {
                liquidacao.IdEventoFinanceiro = Convert.ToInt32(dropEventoFinanceiro.SelectedItem.Value);
            }
        }

        liquidacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Liquidacao - Operacao: Insert Liquidacao: " + liquidacao.IdLiquidacao + UtilitarioWeb.ToString(liquidacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idLiquidacao = Convert.ToInt32(keyValuesId[i]);

                Liquidacao liquidacao = new Liquidacao();
                if (liquidacao.LoadByPrimaryKey(idLiquidacao)) {
                    int idCliente = liquidacao.IdCliente.Value;

                    Liquidacao liquidacaoClone = (Liquidacao)Utilitario.Clone(liquidacao);
                    //

                    liquidacao.MarkAsDeleted();
                    liquidacao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Liquidacao - Operacao: Delete Liquidacao: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Deleta tb de LiquidacaoAbertura, para evitar que seja sobreposto em eventual novo cálculo
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.DataDia);
                    cliente.LoadByPrimaryKey(campos, idCliente);
                    DateTime dataDia = cliente.DataDia.Value;

                    LiquidacaoAbertura liquidacaoAbertura = new LiquidacaoAbertura();
                    if (liquidacaoAbertura.LoadByPrimaryKey(idLiquidacao, dataDia)) {
                        liquidacaoAbertura.MarkAsDeleted();
                        liquidacaoAbertura.Save();
                    }

                    //Atualiza StatusRealTime para executar***********
                    Cliente clienteStatusRealTime = new Cliente();
                    clienteStatusRealTime.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }

            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e) {
        
        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataLancamento_Init(object sender, EventArgs e) {
        
        if (ParametrosConfiguracaoSistema.Outras.LiberaDataLancamentoLiquidacao && gridCadastro.IsNewRowEditing) 
        {
            (sender as ASPxDateEdit).ClientEnabled = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        //Busca o flag de multi-conta
        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        
        if (!multiConta) 
        {
            Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
            ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
            labelConta.Visible = false;
            dropConta.Visible = false;

            Label labelMoeda = this.gridCadastro.FindEditFormTemplateControl("labelMoeda") as Label;
            ASPxComboBox dropMoeda = this.gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
            labelMoeda.Visible = false;
            dropMoeda.Visible = false;
        }

        //Busca o flag de multi-conta
        bool contabil = ParametrosConfiguracaoSistema.Outras.CalculaContatil;

        if (!contabil)
        {
            Label labelEvento = gridCadastro.FindEditFormTemplateControl("labelEvento") as Label;
            ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;
            labelEvento.Visible = false;
            dropEvento.Visible = false;

            Label labelEventoFinanceiro = gridCadastro.FindEditFormTemplateControl("labelEventoFinanceiro") as Label;
            ASPxComboBox dropEventoFinanceiro = gridCadastro.FindEditFormTemplateControl("dropEventoFinanceiro") as ASPxComboBox;
            labelEventoFinanceiro.Visible = false;
            dropEventoFinanceiro.Visible = false;
        }

        base.panelEdicao_Load(sender, e);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing && !Page.IsCallback && !Page.IsPostBack) 
        {
            ASPxComboBox dropSituacao = gridCadastro.FindEditFormTemplateControl("dropSituacao") as ASPxComboBox;
            dropSituacao.SelectedIndex = 0;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) 
        {
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime dataVencimento = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataVencimento"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, dataVencimento) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Lançamento já vencido. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) 
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyDataVencimento = gridCadastro.GetSelectedFieldValues("DataVencimento");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime dataVencimento = Convert.ToDateTime(keyDataVencimento[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, dataVencimento) > 0) {
                        e.Result = "Lançamento já vencido! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else 
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
            TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropSituacao = gridCadastro.FindEditFormTemplateControl("dropSituacao") as ASPxComboBox;            

            bool isPendencia = dropSituacao.SelectedIndex == 1;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(textDataLancamento);
            controles.Add(textDescricao);
            controles.Add(textValor);

            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            if (multiConta)
            {
                ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
                controles.Add(dropConta);
            }

            if (!isPendencia)
            {
                controles.Add(textDataVencimento);
            }

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) 
            {
                int idLocal = cliente.IdLocal.Value;

                DateTime dataVencimento = new DateTime();

                if (!isPendencia)
                {
                    dataVencimento = Convert.ToDateTime(textDataVencimento.Text);

                    if (!Calendario.IsDiaUtil(dataVencimento, idLocal, TipoFeriado.Outros))
                    {
                        e.Result = "Data de vencimento não é dia útil.";
                        return;
                    }
                }

                ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                if (!contaCorrenteCollection.BuscaContaCorrente(idCliente))
                {
                    e.Result = "Cliente não possui conta corrente vinculada.";
                    return;
                }

                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (!isPendencia && dataDia > Convert.ToDateTime(textDataVencimento.Text))
                {
                    e.Result = "Lançamento já vencido! Operação não pode ser realizada.";
                    return;
                }
                else if (gridCadastro.IsNewRowEditing && DateTime.Compare(dataDia, Convert.ToDateTime(textDataLancamento.Text)) > 0) {
                    e.Result = "Lançamento com data anterior à data atual do cliente! Operação não pode ser realizada.";
                    return;
                }
            }
            else
            {
                e.Result = "Cliente não encontrado!";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Valor") {
            decimal value = (decimal)e.GetValue("Valor");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        if (e.DataColumn.FieldName == "Descricao") {
            e.Cell.Attributes.Add("title", e.CellValue.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        e.NewValues[LiquidacaoMetadata.ColumnNames.Situacao] = "Normal";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        //
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicioLancamento.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Lacto) >= ").Append(textDataInicioLancamento.Text.Substring(0, 10));
        }
        if (textDataFimLancamento.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Lacto) <= ").Append(textDataFimLancamento.Text.Substring(0, 10));
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início (Vcto) >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim (Vcto) <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textDescricaoFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Descrição Like %").Append(textDescricaoFiltro.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

    #region Controla Botão Sinacor
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSinacorOnPreRender(object sender, EventArgs e) {
        LinkButton b = sender as LinkButton;

        /* Verifica em Configurações do Sistema se IntegracaoBolsa, IntegracaoBMF ou IntegracaoContaCorrente = Sinacor */
        b.Visible = this.IsConfiguracaoSinacor();
    }
    
    /// <summary>
    /// Retorna True se IntegracaoBolsa, IntegracaoBMF ou IntegracaoContaCorrente = Sinacor em Configurações 
    /// </summary>
    /// <returns></returns>
    private bool IsConfiguracaoSinacor() {

        int integracaoBolsa = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa;
        IntegracaoBolsa integraBolsa = (IntegracaoBolsa)integracaoBolsa;

        int integracaoBMF = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBMF;
        IntegracaoBMF integraBMF = (IntegracaoBMF)integracaoBMF;

        int integracaoCC = ParametrosConfiguracaoSistema.Integracoes.IntegracaoCC;
        IntegracaoCC integraCC = (IntegracaoCC)integracaoCC;

        return integraBolsa == IntegracaoBolsa.Sinacor ||
               integraBMF == IntegracaoBMF.Sinacor ||
               integraCC == IntegracaoCC.Sinacor;
    }
    #endregion

    #region Tratamento do Button Sinacor
    protected void btnSinacor_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatoriosSinacor();

        this.btnSinacor1.ClientVisible = true;

        this.gridConsultaSinacor.DataBind();
    }

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatoriosSinacor() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.btnEditCodigoClienteSinacorFiltro });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Coloca Tooltip no GridConsulta e cor Vermelha se ValorLiquidação for < 0
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsultaSinacor_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorLancamento") {
            decimal value = (decimal)e.GetValue("ValorLancamento");            
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }

        if (e.CellValue != null) {
            string tooltip = e.CellValue is DateTime
                             ? (Convert.ToDateTime(e.CellValue)).ToString("d")
                             : e.CellValue.ToString().Trim();

            e.Cell.Attributes.Add("title", tooltip);        
        } 
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsultaSinacor_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {            
            //Compose a primary key value

            string codigoCliente = Convert.ToString(e.GetListSourceFieldValue("IdCliente"));
            string descricaoLancamento = Convert.ToString(e.GetListSourceFieldValue("Descricao"));
            string dtLiquidacao = Convert.ToString(e.GetListSourceFieldValue("DtLiquidacao"));
            string valorLancamento = Convert.ToString(e.GetListSourceFieldValue("ValorLancamento"));
            //
            e.Value = codigoCliente + descricaoLancamento + dtLiquidacao + valorLancamento;
        }
    }

    /// <summary>
    /// Cria uma nova Liquidação baseada nos Dados do Sinacor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsultaSinacor_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnSinacor") {
            //
            List<object> keyIdCliente = gridConsultaSinacor.GetSelectedFieldValues("IdCliente");
            List<object> keyDataLiquidacao = gridConsultaSinacor.GetSelectedFieldValues("DtLiquidacao");
            List<object> keyDescricao = gridConsultaSinacor.GetSelectedFieldValues("Descricao");
            List<object> keyValorLancamento = gridConsultaSinacor.GetSelectedFieldValues("ValorLancamento");
            //  

            LiquidacaoCollection l = new LiquidacaoCollection();
            for (int i = 0; i < keyIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyIdCliente[i]);
                DateTime dataLiquidacao = Convert.ToDateTime(keyDataLiquidacao[i]);
                string descricao = Convert.ToString(keyDescricao[i]);
                decimal valorLancamento = Convert.ToDecimal(keyValorLancamento[i]);

                Liquidacao liquidacao = l.AddNew();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = dataLiquidacao;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorLancamento;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Outros;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Manual;

                // Lança exceção se conta não encontrada
                liquidacao.IdConta = new ContaCorrente().RetornaContaDefault(idCliente);
            }

            l.Save();
        }

        gridConsultaSinacor.DataBind();
        gridConsultaSinacor.Selection.UnselectAll();
        gridConsultaSinacor.CancelEdit();
    }

    /// <summary>
    /// Tratamento de Erro do Grid Consulta do Sinacor
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroSinacor_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        List<object> keyIdCliente = gridConsultaSinacor.GetSelectedFieldValues("IdCliente");
        List<object> keyDataLiquidacao = gridConsultaSinacor.GetSelectedFieldValues("DtLiquidacao");

        if (keyIdCliente.Count == 0) {
            e.Result = "Selecione um Lançamento!";
            return;
        }
        else {

            for (int i = 0; i < keyIdCliente.Count; i++) 
            {
                int idCliente = Convert.ToInt32(keyIdCliente[i]);
                DateTime dataLiquidacao = Convert.ToDateTime(keyDataLiquidacao[i]);

                #region Trata DataLiquidacao < DataDia do Cliente
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                campos.Add(cliente.Query.Status);

                if (cliente.LoadByPrimaryKey(campos, idCliente)) 
                {
                    // Exceção se DataLiquidacao < DataDia do Cliente
                    if (dataLiquidacao < cliente.DataDia.Value) {
                         e.Result = "Inserção não realizada. Data da liquidação " + dataLiquidacao.ToShortDateString() + " não pode ser menor que Data Dia do Cliente: " + cliente.DataDia.Value.ToShortDateString();
                         return;
                    }
                    else if (cliente.Status == (byte)StatusCliente.Divulgado) 
                    {
                         e.Result = "Inserção não realizada. Cliente com status fechado!";
                         return;
                    }
                }
                #endregion
            }
        }
    }
    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "ContaDescricao")
        {
            int idConta = Convert.ToInt32(e.GetListSourceFieldValue(LiquidacaoMetadata.ColumnNames.IdConta));
            ContaCorrente contaCorrente = new ContaCorrente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(contaCorrente.Query.Numero);
            contaCorrente.LoadByPrimaryKey(campos, idConta);
                
            e.Value = contaCorrente.Numero;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idCliente"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    private bool clienteGerencial(int idCliente, DateTime data)
    {
        PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
        portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(data);

        if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
        {
            if (portfolioPadrao.IdCarteiraDespesasReceitas.Value == idCliente)
                return true;

            if (portfolioPadrao.IdCarteiraTaxaGestao.Value == idCliente)
                return true;

            if (portfolioPadrao.IdCarteiraTaxaPerformance.Value == idCliente)
                return true;
        }

        TraderCollection traderColl = new TraderCollection();
        traderColl.Query.Where(traderColl.Query.IdCarteira.Equal(idCliente));

        if (traderColl.Query.Load())
            return true;

        return false;
    }
}