﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiquidacaoLote.aspx.cs" Inherits="CadastrosBasicos_LiquidacaoLote" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;    
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    
    function OnGetDataClienteFiltro(data){                   
        if (popupFiltro.IsVisible()) {
            btnEditCodigoClienteFiltro.SetValue(data);        
            ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
        }        
    }    
    </script>
</head>
<body>

    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {            
            var resultSplit = e.result.split('|');                        
            e.result = resultSplit[0];            
            var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');                
            OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            alert(e.result);
            gridCadastro.PerformCallback('btnRefresh'); 
        }        
        "/>
    </dxcb:ASPxCallback>
    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Liquidação Financeira (Ajuste em Lote)"></asp:Label>
    </div>
        
    <div id="mainContent">
                 
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table>        
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td  colspan="2" width="450">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>        
                    
                    <tr>
                        <td>
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Vcto Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Vcto Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                   
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Lcto Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicioLancamento" runat="server" ClientInstanceName="textDataInicioLancamento" />
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Lcto Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFimLancamento" runat="server" ClientInstanceName="textDataFimLancamento"/></td></tr>
                            </table>
                        </td>                                                                   
                    </tr>
                                        
                    </table>
                    
                    <table>
                    <tr>
                    <td>
                        <asp:Label ID="labelDescricao" runat="server" CssClass="labelNormal" Text="Descric:"></asp:Label>
                    </td>                
                    <td colspan="3">
                        <asp:TextBox ID="textDescricaoFiltro" runat="server" CssClass="textNormal"></asp:TextBox>
                    </td>    
                    </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>
        
        <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote" EnableClientSideAPI="True"
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Alteração em Lote de Origem" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" style="white-space:nowrap" Text="Origem:"></asp:Label>
                            </td>
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropOrigem" runat="server" 
                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownList">
                                    <Items>                                        
                                        <dxe:ListEditItem Value="1" Text="Bolsa - Compra Ações"/>
                                        <dxe:ListEditItem Value="2" Text="Bolsa - Venda Ações" />
                                        <dxe:ListEditItem Value="3" Text="Bolsa - Compra Opções" />
                                        <dxe:ListEditItem Value="4" Text="Bolsa - Venda Opções" />
                                        <dxe:ListEditItem Value="5" Text="Bolsa - Exercicio Compra" />
                                        <dxe:ListEditItem Value="6" Text="Bolsa - Compra Opções" />
                                        <dxe:ListEditItem Value="7" Text="Bolsa - Compra Termo" />
                                        <dxe:ListEditItem Value="8" Text="Bolsa - Compra Opções" />
                                        <dxe:ListEditItem Value="9" Text="Bolsa - Antecipação Termo" />
                                        <dxe:ListEditItem Value="10" Text="Bolsa - Empréstimo Doado" />
                                        <dxe:ListEditItem Value="11" Text="Bolsa - Emprestimo Tomado" />
                                        <dxe:ListEditItem Value="12" Text="Bolsa - Antecipação Empréstimo" />
                                        <dxe:ListEditItem Value="13" Text="Bolsa - Ajuste Operação Futuro" />
                                        <dxe:ListEditItem Value="14" Text="Bolsa - Ajuste Posição Futuro" />
                                        <dxe:ListEditItem Value="15" Text="Bolsa - Liquidação Final Futuro" />
                                        <dxe:ListEditItem Value="16" Text="Bolsa - Despesas Taxas" />
                                        <dxe:ListEditItem Value="17" Text="Bolsa - Corretagem" />
                                        <dxe:ListEditItem Value="18" Text="Bolsa - Dividendo" />
                                        <dxe:ListEditItem Value="19" Text="Bolsa - Juros Capital" />
                                        <dxe:ListEditItem Value="20" Text="Bolsa - Rendimento" />
                                        <dxe:ListEditItem Value="21" Text="Bolsa - Restituição Capital" />
                                        <dxe:ListEditItem Value="22" Text="Bolsa - Crédito Frações Ações" />
                                        <dxe:ListEditItem Value="23" Text="Bolsa - IR Proventos" />
                                        <dxe:ListEditItem Value="24" Text="Bolsa - Outros Proventos" />
                                        <dxe:ListEditItem Value="40" Text="Bolsa - Taxa Custódia" />
                                        <dxe:ListEditItem Value="100" Text="Bolsa - Outros" />
                                        <dxe:ListEditItem Value="201" Text="BMF - Compra Vista" />
                                        <dxe:ListEditItem Value="202" Text="BMF - Venda Vista" />
                                        <dxe:ListEditItem Value="203" Text="BMF - Compra Opções" />
                                        <dxe:ListEditItem Value="204" Text="BMF - Venda Opções" />
                                        <dxe:ListEditItem Value="205" Text="BMF - Exercicio Compra" />
                                        <dxe:ListEditItem Value="206" Text="BMF - Exercicio Venda" />
                                        <dxe:ListEditItem Value="220" Text="BMF - Despesas Taxas" />
                                        <dxe:ListEditItem Value="221" Text="BMF - Corretagem" />
                                        <dxe:ListEditItem Value="222" Text="BMF - Emolumento" />
                                        <dxe:ListEditItem Value="223" Text="BMF - Registro" />
                                        <dxe:ListEditItem Value="224" Text="BMF - Outros Custos" />
                                        <dxe:ListEditItem Value="250" Text="BMF - Ajuste Posição" />
                                        <dxe:ListEditItem Value="251" Text="BMF - Ajuste Operação" />
                                        <dxe:ListEditItem Value="260" Text="BMF - Taxa Permanência" />
                                        <dxe:ListEditItem Value="300" Text="BMF - Outros" />
                                        <dxe:ListEditItem Value="501" Text="Renda Fixa - Compra Final" />
                                        <dxe:ListEditItem Value="502" Text="Renda Fixa - Venda Final" />
                                        <dxe:ListEditItem Value="503" Text="Renda Fixa - Compra Revenda" />
                                        <dxe:ListEditItem Value="504" Text="Renda Fixa - Venda Recompra" />
                                        <dxe:ListEditItem Value="505" Text="Renda Fixa - Net Operação Casada" />
                                        <dxe:ListEditItem Value="510" Text="Renda Fixa - Vencimento" />
                                        <dxe:ListEditItem Value="511" Text="Renda Fixa - Revenda" />
                                        <dxe:ListEditItem Value="512" Text="Renda Fixa - Recompra" />
                                        <dxe:ListEditItem Value="520" Text="Renda Fixa - Juros" />
                                        <dxe:ListEditItem Value="521" Text="Renda Fixa - Amortização" />
                                        <dxe:ListEditItem Value="530" Text="Renda Fixa - IR" />
                                        <dxe:ListEditItem Value="531" Text="Renda Fixa - IOF" />
                                        <dxe:ListEditItem Value="600" Text="Renda Fixa - Outros" />
                                        <dxe:ListEditItem Value="701" Text="Swap - Liquidação Vencimento" />
                                        <dxe:ListEditItem Value="702" Text="Swap - Liquidação Antecipação" />
                                        <dxe:ListEditItem Value="703" Text="Swap - Despesas Taxas" />
                                        <dxe:ListEditItem Value="801" Text="Provisão - Taxa de Administração" />
                                        <dxe:ListEditItem Value="802" Text="Provisão - Pagto Taxa Administração" />
                                        <dxe:ListEditItem Value="810" Text="Provisão - Taxa Gestão" />
                                        <dxe:ListEditItem Value="811" Text="Provisão - Pagto Taxa Gestão" />
                                        <dxe:ListEditItem Value="821" Text="Provisão - Pagto Taxa Performance" />
                                        <dxe:ListEditItem Value="830" Text="Provisão - CPMF" />
                                        <dxe:ListEditItem Value="840" Text="Provisão - Taxa Fiscalização CVM" />
                                        <dxe:ListEditItem Value="841" Text="Provisão - Pagto Taxa Fiscalização CVM" />
                                        <dxe:ListEditItem Value="890" Text="Provisão - Provisão Outros" />
                                        <dxe:ListEditItem Value="891" Text="Provisão - Pagto Provisão Outros" />
                                        <dxe:ListEditItem Value="1001" Text="Fundo - Aplicação" />
                                        <dxe:ListEditItem Value="1002" Text="Fundo - Resgate" />
                                        <dxe:ListEditItem Value="1003" Text="Fundo - IR Resgate" />
                                        <dxe:ListEditItem Value="1004" Text="Fundo - IOF Resgate" />
                                        <dxe:ListEditItem Value="1005" Text="Fundo - Pfee Resgate" />
                                        <dxe:ListEditItem Value="1006" Text="Fundo - Come-Cotas" />
                                        <dxe:ListEditItem Value="1010" Text="Fundo - Aplicação Converter" />
                                        <dxe:ListEditItem Value="1101" Text="Cotista - Aplicação" />
                                        <dxe:ListEditItem Value="1102" Text="Cotista - Resgate" />
                                        <dxe:ListEditItem Value="1103" Text="Cotista - IR Resgate" />
                                        <dxe:ListEditItem Value="1104" Text="Cotista - IOF Resgate" />
                                        <dxe:ListEditItem Value="1105" Text="Cotista - Pfee Resgate" />
                                        <dxe:ListEditItem Value="1106" Text="Cotista - Come-cotas" />
                                        <dxe:ListEditItem Value="1110" Text="Cotista - Aplicação Converter" />
                                        <dxe:ListEditItem Value="2000" Text="IR - IR Fonte Operação" />
                                        <dxe:ListEditItem Value="2001" Text="IR - IR Fonte DayTrade" />
                                        <dxe:ListEditItem Value="2002" Text="IR - IR Renda Variável" />
                                        <dxe:ListEditItem Value="3000" Text="Margem - Chamada Margem" />
                                        <dxe:ListEditItem Value="3001" Text="Margem - Devolução Margem" />
                                        <dxe:ListEditItem Value="100000" Text="Outros" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>   
                        </tr>
                        
                        <tr>
                        <td>
                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Vencimento:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textNovoVencimento" runat="server" ClientInstanceName="textNovoVencimento" />
                        </td>
                    </tr>    
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="callBackLote.SendCallback(); return false;"><asp:Literal ID="Literal15" runat="server" Text="Processar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
        
        <div class="linkButton" >
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnLote" OnClientClick="popupLote.ShowWindow(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Altera em Lote"/><div></div></asp:LinkButton>           
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
        
        <div class="divDataGrid"> 
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdLiquidacao" DataSourceID="EsDSLiquidacao"
                    OnHtmlDataCellPrepared="gridCadastro_HtmlDataCellPrepared"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    >                     
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="15%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="3" Width="10%"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="5" Width="20%" />                    
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Fonte" VisibleIndex="7" Width="7%" ExportWidth="110">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Interno'>Interno</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Manual'>Manual</div>" />
                        <dxe:ListEditItem Value="3" Text="<div title='Sinacor'>Sinacor</div>" />
                        <dxe:ListEditItem Value="4" Text="<div title='CMDF'>CMDF</div>" />
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Origem" VisibleIndex="7" Width="15%" ExportWidth="110">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>                                        
                        <dxe:ListEditItem Value="1" Text="<div title='Bolsa - Compra Ações'>Bolsa - Compra Ações</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Bolsa - Venda Ações'>Bolsa - Venda Ações</div>" />
                        <dxe:ListEditItem Value="3" Text="<div title='Bolsa - Compra Opções'>Bolsa - Compra Opções</div>" />
                        <dxe:ListEditItem Value="4" Text="<div title='Bolsa - Venda Opções'>Bolsa - Venda Opções</div>" />
                        <dxe:ListEditItem Value="5" Text="<div title='Bolsa - Exercicio Compra'>Bolsa - Exercicio Compra</div>" />
                        <dxe:ListEditItem Value="6" Text="<div title='Bolsa - Compra Opções'>Bolsa - Compra Opções</div>" />
                        <dxe:ListEditItem Value="7" Text="<div title='Bolsa - Compra Termo'>Bolsa - Compra Termo</div>" />
                        <dxe:ListEditItem Value="8" Text="<div title='Bolsa - Compra Opções'>Bolsa - Compra Opções</div>" />
                        <dxe:ListEditItem Value="9" Text="<div title='Bolsa - Antecipação Termo'>Bolsa - Antecipação Termo</div>" />
                        <dxe:ListEditItem Value="10" Text="<div title='Bolsa - Empréstimo Doado'>Bolsa - Empréstimo Doado</div>" />
                        <dxe:ListEditItem Value="11" Text="<div title='Bolsa - Emprestimo Tomado'>Bolsa - Emprestimo Tomado</div>" />
                        <dxe:ListEditItem Value="12" Text="<div title='Bolsa - Antecipação Empréstimo'>Bolsa - Antecipação Empréstimo</div>" />
                        <dxe:ListEditItem Value="13" Text="<div title='Bolsa - Ajuste Operação Futuro'>Bolsa - Ajuste Operação Futuro</div>" />
                        <dxe:ListEditItem Value="14" Text="<div title='Bolsa - Ajuste Posição Futuro'>Bolsa - Ajuste Posição Futuro</div>" />
                        <dxe:ListEditItem Value="15" Text="<div title='Bolsa - Liquidação Final Futuro'>Bolsa - Liquidação Final Futuro</div>" />
                        <dxe:ListEditItem Value="16" Text="<div title='Bolsa - Despesas Taxas'>Bolsa - Despesas Taxas</div>" />
                        <dxe:ListEditItem Value="17" Text="<div title='Bolsa - Corretagem'>Bolsa - Corretagem</div>" />
                        <dxe:ListEditItem Value="18" Text="<div title='Bolsa - Dividendo'>Bolsa - Dividendo</div>" />
                        <dxe:ListEditItem Value="19" Text="<div title='Bolsa - Juros Capital'>Bolsa - Juros Capital</div>" />
                        <dxe:ListEditItem Value="20" Text="<div title='Bolsa - Rendimento'>Bolsa - Rendimento</div>" />
                        <dxe:ListEditItem Value="21" Text="<div title='Bolsa - Restituição Capital'>Bolsa - Restituição Capital</div>" />
                        <dxe:ListEditItem Value="22" Text="<div title='Bolsa - Crédito Frações Ações'>Bolsa - Crédito Frações Ações</div>" />
                        <dxe:ListEditItem Value="23" Text="<div title='Bolsa - IR Proventos'>Bolsa - IR Proventos</div>" />
                        <dxe:ListEditItem Value="24" Text="<div title='Bolsa - Outros Proventos'>Bolsa - Outros Proventos</div>" />
                        <dxe:ListEditItem Value="40" Text="<div title='Bolsa - Taxa Custódia'>Bolsa - Taxa Custódia</div>" />
                        <dxe:ListEditItem Value="100" Text="<div title='Bolsa - Outros'>Bolsa - Outros</div>" />
                        <dxe:ListEditItem Value="201" Text="<div title='BMF - Compra Vista'>BMF - Compra Vista</div>" />
                        <dxe:ListEditItem Value="202" Text="<div title='BMF - Venda Vista'>BMF - Venda Vista</div>" />
                        <dxe:ListEditItem Value="203" Text="<div title='BMF - Compra Opções'>BMF - Compra Opções</div>" />
                        <dxe:ListEditItem Value="204" Text="<div title='BMF - Venda Opções'>BMF - Venda Opções</div>" />
                        <dxe:ListEditItem Value="205" Text="<div title='BMF - Exercicio Compra'>BMF - Exercicio Compra</div>" />
                        <dxe:ListEditItem Value="206" Text="<div title='BMF - Exercicio Venda'>BMF - Exercicio Venda</div>" />
                        <dxe:ListEditItem Value="220" Text="<div title='BMF - Despesas Taxas'>BMF - Despesas Taxas</div>" />
                        <dxe:ListEditItem Value="221" Text="<div title='BMF - Corretagem'>BMF - Corretagem</div>" />
                        <dxe:ListEditItem Value="222" Text="<div title='BMF - Emolumento'>BMF - Emolumento</div>" />
                        <dxe:ListEditItem Value="223" Text="<div title='BMF - Registro'>BMF - Registro</div>" />
                        <dxe:ListEditItem Value="224" Text="<div title='BMF - Outros Custos'>BMF - Outros Custos</div>" />
                        <dxe:ListEditItem Value="250" Text="<div title='BMF - Ajuste Posição'>BMF - Ajuste Posição</div>" />
                        <dxe:ListEditItem Value="251" Text="<div title='BMF - Ajuste Operação'>BMF - Ajuste Operação</div>" />
                        <dxe:ListEditItem Value="260" Text="<div title='BMF - Taxa Permanência'>BMF - Taxa Permanência</div>" />
                        <dxe:ListEditItem Value="300" Text="<div title='BMF - Outros'>BMF - Outros</div>" />
                        <dxe:ListEditItem Value="501" Text="<div title='Renda Fixa - Compra Final'>Renda Fixa - Compra Final</div>" />
                        <dxe:ListEditItem Value="502" Text="<div title='Renda Fixa - Venda Final'>Renda Fixa - Venda Final</div>" />
                        <dxe:ListEditItem Value="503" Text="<div title='Renda Fixa - Compra Revenda'>Renda Fixa - Compra Revenda</div>" />
                        <dxe:ListEditItem Value="504" Text="<div title='Renda Fixa - Venda Recompra'>Renda Fixa - Venda Recompra</div>" />
                        <dxe:ListEditItem Value="505" Text="<div title='Renda Fixa - Net Operação Casada'>Renda Fixa - Net Operação Casada</div>" />
                        <dxe:ListEditItem Value="510" Text="<div title='Renda Fixa - Vencimento'>Renda Fixa - Vencimento</div>" />
                        <dxe:ListEditItem Value="511" Text="<div title='Renda Fixa - Revenda'>Renda Fixa - Revenda</div>" />
                        <dxe:ListEditItem Value="512" Text="<div title='Renda Fixa - Recompra'>Renda Fixa - Recompra</div>" />
                        <dxe:ListEditItem Value="520" Text="<div title='Renda Fixa - Juros'>Renda Fixa - Juros</div>" />
                        <dxe:ListEditItem Value="521" Text="<div title='Renda Fixa - Amortização'>Renda Fixa - Amortização</div>" />
                        <dxe:ListEditItem Value="530" Text="<div title='Renda Fixa - IR'>Renda Fixa - IR</div>" />
                        <dxe:ListEditItem Value="531" Text="<div title='Renda Fixa - IOF'>Renda Fixa - IOF</div>" />
                        <dxe:ListEditItem Value="600" Text="<div title='Renda Fixa - Outros'>Renda Fixa - Outros</div>" />
                        <dxe:ListEditItem Value="701" Text="<div title='Swap - Liquidação Vencimento'>Swap - Liquidação Vencimento</div>" />
                        <dxe:ListEditItem Value="702" Text="<div title='Swap - Liquidação Antecipação'>Swap - Liquidação Antecipação</div>" />
                        <dxe:ListEditItem Value="703" Text="<div title='Swap - Despesas Taxas'>Swap - Despesas Taxas</div>" />
                        <dxe:ListEditItem Value="801" Text="<div title='Provisão - Taxa de Administração'>Provisão - Taxa de Administração</div>" />
                        <dxe:ListEditItem Value="802" Text="<div title='Provisão - Pagto Taxa Administração'>Provisão - Pagto Taxa de Administração</div>" />
                        <dxe:ListEditItem Value="810" Text="<div title='Provisão - Taxa Gestão'>Provisão - Taxa Gestão</div>" />
                        <dxe:ListEditItem Value="811" Text="<div title='Provisão - Pagto Taxa Gestão'>Provisão - Pagto Taxa Gestão</div>" />
                        <dxe:ListEditItem Value="821" Text="<div title='Provisão - Pagto Taxa Performance'>Provisão - Pagto Taxa Performance</div>" />
                        <dxe:ListEditItem Value="830" Text="<div title='Provisão - CPMF'>Provisão - CPMF</div>" />
                        <dxe:ListEditItem Value="840" Text="<div title='Provisão - Taxa Fiscalização CVM'>Provisão - Taxa Fiscalização CVM</div>" />
                        <dxe:ListEditItem Value="841" Text="<div title='Provisão - Pagto Taxa Fiscalização CVM'>Provisão - Pagto Taxa Fiscalização CVM</div>" />
                        <dxe:ListEditItem Value="890" Text="<div title='Provisão - Provisão Outros'>Provisão - Provisão Outros</div>" />
                        <dxe:ListEditItem Value="891" Text="<div title='Provisão - Pagto Provisão Outros'>Provisão - Pagto Provisão Outros</div>" />
                        <dxe:ListEditItem Value="1001" Text="<div title='Fundo - Aplicação'>Fundo - Aplicação</div>" />
                        <dxe:ListEditItem Value="1002" Text="<div title='Fundo - Resgate'>Fundo - Resgaste</div>" />
                        <dxe:ListEditItem Value="1003" Text="<div title='Fundo - IR Resgate'>Fundo - IR Resgate</div>" />
                        <dxe:ListEditItem Value="1004" Text="<div title='Fundo - IOF Resgate'>Fundo - IOF Resgate</div>" />
                        <dxe:ListEditItem Value="1005" Text="<div title='Fundo - Pfee Resgate'>Fundo - Pfee Resgate</div>" />
                        <dxe:ListEditItem Value="1006" Text="<div title='Fundo - Come-Cotas'>Fundo - Come-Cotas</div>" />
                        <dxe:ListEditItem Value="1010" Text="<div title='Fundo - Aplicação Converter'>Fundo - Aplicação Coverter</div>" />
                        <dxe:ListEditItem Value="1101" Text="<div title='Cotista - Aplicação'>Cotista - Aplicação</div>" />
                        <dxe:ListEditItem Value="1102" Text="<div title='Cotista - Resgate'>Cotista - Resgate</div>" />
                        <dxe:ListEditItem Value="1103" Text="<div title='Cotista - IR Resgate'>Cotista - IR Resgate</div>" />
                        <dxe:ListEditItem Value="1104" Text="<div title='Cotista - IOF Resgate'>Cotista - IOF Resgate</div>" />
                        <dxe:ListEditItem Value="1105" Text="<div title='Cotista - Pfee Resgate'>Cotista - Pfee Resgate</div>" />
                        <dxe:ListEditItem Value="1106" Text="<div title='Cotista - Come-cotas'>Cotista - Come-cotas</div>" />
                        <dxe:ListEditItem Value="1110" Text="<div title='Cotista - Aplicação Converter'>Cotista - Aplicação Converter</div>" />
                        <dxe:ListEditItem Value="2000" Text="<div title='IR - IR Fonte Operação'>IR - IR Fonte Operação</div>" />
                        <dxe:ListEditItem Value="2001" Text="<div title='IR - IR Fonte DayTrade'>IR - IR Fonte DayTrade</div>" />
                        <dxe:ListEditItem Value="2002" Text="<div title='IR - IR Renda Variável'>IR - IR Renda Variável</div>" />
                        <dxe:ListEditItem Value="3000" Text="<div title='Margem - Chamada Margem'>Margem - Chamada Margem</div>" />
                        <dxe:ListEditItem Value="3001" Text="<div title='Margem - Devolução Margem'>Margem - Devolução Margem</div>" />
                        <dxe:ListEditItem Value="100000" Text="<div title='Outros'>Outros</div>" />                      
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="9" Width="10%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>                    
                    
                </Columns>
                
                <Templates>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>
                </Templates>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                
            </dxwgv:ASPxGridView>                
        </div>
                      
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" LeftMargin = "30" RightMargin = "30"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSLiquidacao" runat="server" OnesSelect="EsDSLiquidacao_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />    
    
    </form>
</body>
</html>