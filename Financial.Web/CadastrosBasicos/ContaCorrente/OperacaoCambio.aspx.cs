﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Swap;
using Financial.Web.Common;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Swap.Enums;

public partial class CadastrosBasicos_OperacaoCambio : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {window.tmp_CodigoCliente = undefined; editFormOpen=true; gridCadastro.StartEditRow(e.visibleIndex);}";
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoCambio_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoCambioQuery operacaoCambioQuery = new OperacaoCambioQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        operacaoCambioQuery.Select(operacaoCambioQuery, clienteQuery.Apelido.As("Apelido"));
        operacaoCambioQuery.InnerJoin(clienteQuery).On(operacaoCambioQuery.IdCliente == clienteQuery.IdCliente);
        operacaoCambioQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoCambioQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            operacaoCambioQuery.Where(operacaoCambioQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            operacaoCambioQuery.Where(operacaoCambioQuery.DataRegistro.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            operacaoCambioQuery.Where(operacaoCambioQuery.DataRegistro.LessThanOrEqual(textDataFim.Text));
        }

        operacaoCambioQuery.OrderBy(operacaoCambioQuery.DataRegistro.Descending);

        OperacaoCambioCollection coll = new OperacaoCambioCollection();
        coll.Load(operacaoCambioQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContaCorrenteOrigem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropMoedaOrigem = this.gridCadastro.FindEditFormTemplateControl("dropMoedaOrigem") as ASPxComboBox;

        if (btnEditCodigoCliente != null && btnEditCodigoCliente.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ContaCorrenteCollection coll = new ContaCorrenteCollection();

            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("CC");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            contaCorrenteQuery.Select(contaCorrenteQuery.IdConta.As("IdContaOrigem"), contaCorrenteQuery.Numero);
            contaCorrenteQuery.InnerJoin(clienteQuery).On(contaCorrenteQuery.IdPessoa.Equal(clienteQuery.IdPessoa));
            contaCorrenteQuery.Where(clienteQuery.IdCliente.Equal(idCliente));

            if (dropMoedaOrigem.SelectedIndex != -1)
            {
                contaCorrenteQuery.Where(contaCorrenteQuery.IdMoeda.Equal(Convert.ToInt32(dropMoedaOrigem.SelectedItem.Value)));
            }

            contaCorrenteQuery.OrderBy(contaCorrenteQuery.Numero.Ascending);
            coll.Load(contaCorrenteQuery);

            ContaCorrente c = new ContaCorrente();
            c.SetColumn("IdContaOrigem", null);
            c.Numero = "";

            // Attach
            coll.AttachEntity(c);
            coll.Sort = ContaCorrenteMetadata.ColumnNames.Numero + " ASC";

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }

    protected void EsDSContaCorrenteDestino_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropMoedaDestino = this.gridCadastro.FindEditFormTemplateControl("dropMoedaDestino") as ASPxComboBox;

        if (btnEditCodigoCliente != null && btnEditCodigoCliente.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ContaCorrenteCollection coll = new ContaCorrenteCollection();

            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("CC");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            contaCorrenteQuery.Select(contaCorrenteQuery.IdConta.As("IdContaDestino"), contaCorrenteQuery.Numero);
            contaCorrenteQuery.InnerJoin(clienteQuery).On(contaCorrenteQuery.IdPessoa.Equal(clienteQuery.IdPessoa));
            contaCorrenteQuery.Where(clienteQuery.IdCliente.Equal(idCliente));

            if (dropMoedaDestino.SelectedIndex != -1)
            {
                contaCorrenteQuery.Where(contaCorrenteQuery.IdMoeda.Equal(Convert.ToInt32(dropMoedaDestino.SelectedItem.Value)));
            }

            contaCorrenteQuery.OrderBy(contaCorrenteQuery.Numero.Ascending);
            coll.Load(contaCorrenteQuery);

            ContaCorrente c = new ContaCorrente();
            c.SetColumn("IdContaDestino", null);
            c.Numero = "";

            // Attach
            coll.AttachEntity(c);
            coll.Sort = ContaCorrenteMetadata.ColumnNames.Numero + " ASC";

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }

    protected void EsDSParidadeCambial_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ParidadeCambialCollection coll = new ParidadeCambialCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S")
        {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataRegistro"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }

            //#region Trata texto da conta origem
            ///*ASPxComboBox dropContaOrigem = gridCadastro.FindEditFormTemplateControl("dropContaOrigem") as ASPxComboBox;
            //int idContaOrigem = Convert.ToInt32(dropContaOrigem.SelectedItem.Value);*/
            //ASPxTextBox textBancoOrigem = gridCadastro.FindEditFormTemplateControl("textBancoOrigem") as ASPxTextBox;

            //int idContaOrigem = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdContaOrigem"));

            //ContaCorrente contaCorrente = new ContaCorrente();
            //contaCorrente.LoadByPrimaryKey(idContaOrigem);

            //string bancoNome = "";
            //Banco banco = new Banco();
            //if (contaCorrente.IdBanco.HasValue) {
            //    banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
            //    bancoNome = banco.Nome;
            //}

            //string agenciaNome = "";
            //Agencia agencia = new Agencia();
            //if (contaCorrente.IdAgencia.HasValue) {
            //    agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
            //    agenciaNome = agencia.Nome;
            //}

            //string localNome = "";
            //LocalFeriado localFeriado = new LocalFeriado();
            //if (contaCorrente.IdLocal.HasValue) {
            //    localFeriado.LoadByPrimaryKey(contaCorrente.IdLocal.Value);
            //    localNome = localFeriado.Nome;
            //}

            //textBancoOrigem.Text = bancoNome;

            //if (agenciaNome != "") {
            //    textBancoOrigem.Text = textBancoOrigem.Text + " >> Agência:" + agenciaNome;
            //}

            //if (localNome != "") {
            //    textBancoOrigem.Text = textBancoOrigem.Text + " >> Localidade:" + localNome;
            //}
            //#endregion

            //#region Trata texto da conta destino
            ///*ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;
            //int idContaDestino = Convert.ToInt32(dropContaDestino.SelectedItem.Value);*/
            //ASPxTextBox textBancoDestino = gridCadastro.FindEditFormTemplateControl("textBancoDestino") as ASPxTextBox;


            //int idContaDestino = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdContaDestino"));
            //contaCorrente = new ContaCorrente();
            //contaCorrente.LoadByPrimaryKey(idContaDestino);

            //bancoNome = "";
            //banco = new Banco();
            //if (contaCorrente.IdBanco.HasValue) {
            //    banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
            //    bancoNome = banco.Nome;
            //}

            //agenciaNome = "";
            //agencia = new Agencia();
            //if (contaCorrente.IdAgencia.HasValue) {
            //    agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
            //    agenciaNome = agencia.Nome;
            //}

            //localNome = "";
            //localFeriado = new LocalFeriado();
            //if (contaCorrente.IdLocal.HasValue) {
            //    localFeriado.LoadByPrimaryKey(contaCorrente.IdLocal.Value);
            //    localNome = localFeriado.Nome;
            //}

            //textBancoDestino.Text = bancoNome;

            //if (agenciaNome != "") {
            //    textBancoDestino.Text = textBancoDestino.Text + " >> Agência:" + agenciaNome;
            //}

            //if (localNome != "") {
            //    textBancoDestino.Text = textBancoDestino.Text + " >> Localidade:" + localNome;
            //}

            //#endregion

        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackBanco_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string tipo = e.Parameter;


        int idConta = 0;
        if (tipo == "origem")
        {
            ASPxComboBox dropContaOrigem = gridCadastro.FindEditFormTemplateControl("dropContaOrigem") as ASPxComboBox;
            if (dropContaOrigem.SelectedItem == null || dropContaOrigem.SelectedIndex == 0)
            {
                e.Result = tipo + "|";
                return;
            }
            idConta = Convert.ToInt32(dropContaOrigem.SelectedItem.Value);

        }
        else
        {
            ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;
            if (dropContaDestino.SelectedItem == null || dropContaDestino.SelectedIndex == 0)
            {
                e.Result = tipo + "|";
                return;
            }
            idConta = Convert.ToInt32(dropContaDestino.SelectedItem.Value);
        }

        ContaCorrente contaCorrente = new ContaCorrente();
        contaCorrente.LoadByPrimaryKey(idConta);

        string bancoNome = "";
        Banco banco = new Banco();
        if (contaCorrente.IdBanco.HasValue)
        {
            banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
            bancoNome = banco.Nome;
        }

        string agenciaNome = "";
        Agencia agencia = new Agencia();
        if (contaCorrente.IdAgencia.HasValue)
        {
            agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
            agenciaNome = agencia.Nome;
        }

        string localNome = "";
        LocalFeriado localFeriado = new LocalFeriado();
        if (contaCorrente.IdLocal.HasValue)
        {
            localFeriado.LoadByPrimaryKey(contaCorrente.IdLocal.Value);
            localNome = localFeriado.Nome;
        }

        e.Result = tipo + "|" + bancoNome;

        if (agenciaNome != "")
        {
            e.Result = e.Result + " >> Agência:" + agenciaNome;
        }

        if (localNome != "")
        {
            e.Result = e.Result + " >> Localidade:" + localNome;
        }

    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!gridCadastro.IsEditing)
        {
            #region Testa deleção
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("DataRegistro");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado)
                    {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0)
                    {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
            #endregion
        }
        else
        {
            #region Campos
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxComboBox dropContaOrigem = gridCadastro.FindEditFormTemplateControl("dropContaOrigem") as ASPxComboBox;
            ASPxComboBox dropMoedaOrigem = gridCadastro.FindEditFormTemplateControl("dropMoedaOrigem") as ASPxComboBox;
            ASPxComboBox dropMoedaDestino = gridCadastro.FindEditFormTemplateControl("dropMoedaDestino") as ASPxComboBox;
            ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;

            ASPxSpinEdit textParidade = gridCadastro.FindEditFormTemplateControl("textParidade") as ASPxSpinEdit;
            ASPxSpinEdit textPrincipalOrigem = gridCadastro.FindEditFormTemplateControl("textPrincipalOrigem") as ASPxSpinEdit;
            ASPxSpinEdit textPrincipalDestino = gridCadastro.FindEditFormTemplateControl("textPrincipalDestino") as ASPxSpinEdit;
            #endregion

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(textDataRegistro);
            controles.Add(textDataLiquidacao);
            //controles.Add(dropContaOrigem);
            controles.Add(dropMoedaOrigem);
            controles.Add(dropMoedaDestino);
            //controles.Add(dropContaDestino);
            controles.Add(textParidade);

            if (dropContaOrigem.SelectedIndex == 0 || dropContaDestino.SelectedIndex == 0)
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime dataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            DateTime dataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);

            #region Restrições de Datas
            if (!Calendario.IsDiaUtil(dataLiquidacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data de liquidação não é dia útil.";
                return;
            }

            if (dataRegistro > dataLiquidacao)
            {
                e.Result = "Data de liquidação deve ser superior ou igual à data de registro.";
                return;
            }
            #endregion

            #region Cliente Fechado e Data
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataDia > dataRegistro)
                {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }
            #endregion

            #region Ao menos Principal origem ou destino deve ser preenchido
            if (textPrincipalOrigem.Text == "" && textPrincipalDestino.Text == "")
            {
                e.Result = "Principal origem ou destino deve ser informado.";
                return;
            }
            #endregion

            #region Testa paridade existente
            ParidadeCambialCollection paridadeCambialCollection = new ParidadeCambialCollection();
            paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(Convert.ToInt16(dropMoedaOrigem.SelectedItem.Value)),
                                                  paridadeCambialCollection.Query.IdMoedaDenominador.Equal(Convert.ToInt16(dropMoedaDestino.SelectedItem.Value)));
            paridadeCambialCollection.Query.Load();

            if (paridadeCambialCollection.Count == 0)
            {
                paridadeCambialCollection = new ParidadeCambialCollection();
                paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(Convert.ToInt16(dropMoedaDestino.SelectedItem.Value)),
                                                      paridadeCambialCollection.Query.IdMoedaDenominador.Equal(Convert.ToInt16(dropMoedaOrigem.SelectedItem.Value)));
                paridadeCambialCollection.Query.Load();

                if (paridadeCambialCollection.Count == 0)
                {
                    e.Result = "Não existe cadastro de paridade associada às moedas da operação.";
                    return;
                }
            }
            #endregion
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {

                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                      ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                      : nome;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropContaOrigem = gridCadastro.FindEditFormTemplateControl("dropContaOrigem") as ASPxComboBox;
        ASPxComboBox dropMoedaOrigem = gridCadastro.FindEditFormTemplateControl("dropMoedaOrigem") as ASPxComboBox;
        ASPxComboBox dropMoedaDestino = gridCadastro.FindEditFormTemplateControl("dropMoedaDestino") as ASPxComboBox;
        ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;
        ASPxSpinEdit textParidade = gridCadastro.FindEditFormTemplateControl("textParidade") as ASPxSpinEdit;
        ASPxSpinEdit textPrincipalOrigem = gridCadastro.FindEditFormTemplateControl("textPrincipalOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textPrincipalDestino = gridCadastro.FindEditFormTemplateControl("textPrincipalDestino") as ASPxSpinEdit;
        ASPxSpinEdit textTributosOrigem = gridCadastro.FindEditFormTemplateControl("textTributosOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textCustosOrigem = gridCadastro.FindEditFormTemplateControl("textCustosOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textTributosDestino = gridCadastro.FindEditFormTemplateControl("textTributosDestino") as ASPxSpinEdit;
        ASPxSpinEdit textCustosDestino = gridCadastro.FindEditFormTemplateControl("textCustosDestino") as ASPxSpinEdit;

        decimal tributosOrigem = textTributosOrigem.Text == "" ? 0 : Convert.ToDecimal(textTributosOrigem.Text);
        decimal custosOrigem = textCustosOrigem.Text == "" ? 0 : Convert.ToDecimal(textCustosOrigem.Text);
        decimal tributosDestino = textTributosDestino.Text == "" ? 0 : Convert.ToDecimal(textTributosDestino.Text);
        decimal custosDestino = textCustosDestino.Text == "" ? 0 : Convert.ToDecimal(textCustosDestino.Text);

        OperacaoCambio operacaoCambio = new OperacaoCambio();
        if (operacaoCambio.LoadByPrimaryKey(idOperacao))
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoCambio.IdCliente = idCliente;
            operacaoCambio.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            operacaoCambio.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
            operacaoCambio.IdContaOrigem = Convert.ToInt32(dropContaOrigem.SelectedItem.Value);
            operacaoCambio.IdContaDestino = Convert.ToInt32(dropContaDestino.SelectedItem.Value);
            operacaoCambio.IdMoedaOrigem = Convert.ToInt16(dropMoedaOrigem.SelectedItem.Value);
            operacaoCambio.IdMoedaDestino = Convert.ToInt16(dropMoedaDestino.SelectedItem.Value);

            decimal taxaCambio = Convert.ToDecimal(textParidade.Text);
            decimal principalOrigem = 0;
            decimal principalDestino = 0;

            ParidadeCambialCollection paridadeCambialCollection = new ParidadeCambialCollection();
            paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaDenominador.Equal(operacaoCambio.IdMoedaDestino.Value));
            paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(operacaoCambio.IdMoedaOrigem.Value));
            paridadeCambialCollection.Query.Load();

            double m = paridadeCambialCollection.Count > 0 ? -1 : 1;
            if (textPrincipalOrigem.Text == "")
            {
                principalDestino = Convert.ToDecimal(textPrincipalDestino.Text);
                principalOrigem = principalDestino * (decimal)Math.Pow((double)taxaCambio, m);
            }
            else
            {
                principalOrigem = Convert.ToDecimal(textPrincipalOrigem.Text);
                principalDestino = principalOrigem / ((decimal)Math.Pow((double)taxaCambio, m));
            }

            operacaoCambio.PrincipalOrigem = principalOrigem;
            operacaoCambio.PrincipalDestino = principalDestino;
            operacaoCambio.TributosOrigem = tributosOrigem;
            operacaoCambio.TributosDestino = tributosDestino;
            operacaoCambio.CustosOrigem = custosOrigem;
            operacaoCambio.CustosDestino = custosDestino;
            operacaoCambio.TotalOrigem = principalOrigem + tributosOrigem + custosOrigem;
            operacaoCambio.TotalDestino = principalDestino + tributosDestino + custosDestino;
            operacaoCambio.TaxaCambio = taxaCambio;

            operacaoCambio.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoCambio - Operacao: Update OperacaoCambio: " + idOperacao + UtilitarioWeb.ToString(operacaoCambio),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropContaOrigem = gridCadastro.FindEditFormTemplateControl("dropContaOrigem") as ASPxComboBox;
        ASPxComboBox dropMoedaOrigem = gridCadastro.FindEditFormTemplateControl("dropMoedaOrigem") as ASPxComboBox;
        ASPxComboBox dropMoedaDestino = gridCadastro.FindEditFormTemplateControl("dropMoedaDestino") as ASPxComboBox;
        ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;
        ASPxSpinEdit textParidade = gridCadastro.FindEditFormTemplateControl("textParidade") as ASPxSpinEdit;
        ASPxSpinEdit textPrincipalOrigem = gridCadastro.FindEditFormTemplateControl("textPrincipalOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textPrincipalDestino = gridCadastro.FindEditFormTemplateControl("textPrincipalDestino") as ASPxSpinEdit;
        ASPxSpinEdit textTributosOrigem = gridCadastro.FindEditFormTemplateControl("textTributosOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textCustosOrigem = gridCadastro.FindEditFormTemplateControl("textCustosOrigem") as ASPxSpinEdit;
        ASPxSpinEdit textTributosDestino = gridCadastro.FindEditFormTemplateControl("textTributosDestino") as ASPxSpinEdit;
        ASPxSpinEdit textCustosDestino = gridCadastro.FindEditFormTemplateControl("textCustosDestino") as ASPxSpinEdit;

        OperacaoCambio operacaoCambio = new OperacaoCambio();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        decimal tributosOrigem = textTributosOrigem.Text == "" ? 0 : Convert.ToDecimal(textTributosOrigem.Text);
        decimal custosOrigem = textCustosOrigem.Text == "" ? 0 : Convert.ToDecimal(textCustosOrigem.Text);
        decimal tributosDestino = textTributosDestino.Text == "" ? 0 : Convert.ToDecimal(textTributosDestino.Text);
        decimal custosDestino = textCustosDestino.Text == "" ? 0 : Convert.ToDecimal(textCustosDestino.Text);

        operacaoCambio.IdCliente = idCliente;
        operacaoCambio.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        operacaoCambio.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
        operacaoCambio.IdContaOrigem = Convert.ToInt32(dropContaOrigem.SelectedItem.Value);
        operacaoCambio.IdContaDestino = Convert.ToInt32(dropContaDestino.SelectedItem.Value);
        operacaoCambio.IdMoedaOrigem = Convert.ToInt16(dropMoedaOrigem.SelectedItem.Value);
        operacaoCambio.IdMoedaDestino = Convert.ToInt16(dropMoedaDestino.SelectedItem.Value);

        decimal taxaCambio = Convert.ToDecimal(textParidade.Text);
        decimal principalOrigem = 0;
        decimal principalDestino = 0;

        ParidadeCambialCollection paridadeCambialCollection = new ParidadeCambialCollection();
        paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaDenominador.Equal(operacaoCambio.IdMoedaDestino.Value));
        paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(operacaoCambio.IdMoedaOrigem.Value));
        paridadeCambialCollection.Query.Load();

        double m = paridadeCambialCollection.Count > 0 ? -1 : 1;
        if (textPrincipalOrigem.Text == "")
        {
            principalDestino = Convert.ToDecimal(textPrincipalDestino.Text);
            principalOrigem = principalDestino * (decimal)Math.Pow((double)taxaCambio, m);
        }
        else
        {
            principalOrigem = Convert.ToDecimal(textPrincipalOrigem.Text);
            principalDestino = principalOrigem / ((decimal)Math.Pow((double)taxaCambio, m));
        }

        operacaoCambio.PrincipalOrigem = principalOrigem;
        operacaoCambio.PrincipalDestino = principalDestino;
        operacaoCambio.TributosOrigem = tributosOrigem;
        operacaoCambio.TributosDestino = tributosDestino;
        operacaoCambio.CustosOrigem = custosOrigem;
        operacaoCambio.CustosDestino = custosDestino;
        operacaoCambio.TotalOrigem = principalOrigem + tributosOrigem + custosOrigem;
        operacaoCambio.TotalDestino = principalDestino + tributosDestino + custosDestino;
        operacaoCambio.TaxaCambio = taxaCambio;

        operacaoCambio.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoCambio - Operacao: Insert OperacaoCambio: " + UtilitarioWeb.ToString(operacaoCambio),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************        

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoCambio operacaoCambio = new OperacaoCambio();
                if (operacaoCambio.LoadByPrimaryKey(idOperacao))
                {
                    int idCliente = operacaoCambio.IdCliente.Value;

                    //
                    OperacaoCambio operacaoCambioClone = (OperacaoCambio)Utilitario.Clone(operacaoCambio);
                    //

                    operacaoCambio.MarkAsDeleted();
                    operacaoCambio.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoCambio - Operacao: Delete OperacaoCambio: " + idOperacao + UtilitarioWeb.ToString(operacaoCambioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "")
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropContaOrigem_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void dropContaDestino_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    /// <summary>
    /// Carregamento no Update do banco Destino
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textBancoDestino_Init(object sender, EventArgs e)
    {
        // Se for Modo Update
        if (!gridCadastro.IsNewRowEditing)
        {

            #region Trata texto da conta destino
            //ASPxComboBox dropContaDestino = gridCadastro.FindEditFormTemplateControl("dropContaDestino") as ASPxComboBox;
            //int idContaDestino = Convert.ToInt32(dropContaDestino.SelectedItem.Value);

            ASPxTextBox textBancoDestino = sender as ASPxTextBox;

            int idContaDestino = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdContaDestino"));
            ContaCorrente contaCorrente = new ContaCorrente();
            contaCorrente.LoadByPrimaryKey(idContaDestino);

            string bancoNome = "";
            Banco banco = new Banco();
            if (contaCorrente.IdBanco.HasValue)
            {
                banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                bancoNome = banco.Nome;
            }

            string agenciaNome = "";
            Agencia agencia = new Agencia();
            if (contaCorrente.IdAgencia.HasValue)
            {
                agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                agenciaNome = agencia.Nome;
            }

            string localNome = "";
            LocalFeriado localFeriado = new LocalFeriado();
            if (contaCorrente.IdLocal.HasValue)
            {
                localFeriado.LoadByPrimaryKey(contaCorrente.IdLocal.Value);
                localNome = localFeriado.Nome;
            }

            textBancoDestino.Text = bancoNome;

            if (agenciaNome != "")
            {
                textBancoDestino.Text = textBancoDestino.Text + " >> Agência:" + agenciaNome;
            }

            if (localNome != "")
            {
                textBancoDestino.Text = textBancoDestino.Text + " >> Localidade:" + localNome;
            }

            #endregion
        }
    }

    /// <summary>
    /// Carregamento no Update do banco Origem
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textBancoOrigem_Init(object sender, EventArgs e)
    {
        // Se for Modo Update
        if (!gridCadastro.IsNewRowEditing)
        {

            #region Trata texto da conta origem
            ASPxTextBox textBancoOrigem = sender as ASPxTextBox;
            int idContaOrigem = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdContaOrigem"));

            ContaCorrente contaCorrente = new ContaCorrente();
            contaCorrente.LoadByPrimaryKey(idContaOrigem);

            string bancoNome = "";
            Banco banco = new Banco();
            if (contaCorrente.IdBanco.HasValue)
            {
                banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                bancoNome = banco.Nome;
            }

            string agenciaNome = "";
            Agencia agencia = new Agencia();
            if (contaCorrente.IdAgencia.HasValue)
            {
                agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                agenciaNome = agencia.Nome;
            }

            string localNome = "";
            LocalFeriado localFeriado = new LocalFeriado();
            if (contaCorrente.IdLocal.HasValue)
            {
                localFeriado.LoadByPrimaryKey(contaCorrente.IdLocal.Value);
                localNome = localFeriado.Nome;
            }

            textBancoOrigem.Text = bancoNome;

            if (agenciaNome != "")
            {
                textBancoOrigem.Text = textBancoOrigem.Text + " >> Agência:" + agenciaNome;
            }

            if (localNome != "")
            {
                textBancoOrigem.Text = textBancoOrigem.Text + " >> Localidade:" + localNome;
            }
            #endregion
        }
    }
}