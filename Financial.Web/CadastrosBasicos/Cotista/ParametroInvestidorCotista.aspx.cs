﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.InvestidorCotista;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;

public partial class CadastrosBasicos_ParametroInvestidorCotista : CadastroBasePage 
{
    #region DataSources
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();

        coll.Query.Select(coll.Query.IdCotista,
                          coll.Query.Nome,
                          coll.Query.InvestidorProfissional,
                          coll.Query.InvestidorQualificado);
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Cotista cotista = new Cotista();
        int idCotista = (int)e.Keys[0];

        if (cotista.LoadByPrimaryKey(idCotista))
        {
            cotista.InvestidorProfissional = Convert.ToString(e.NewValues[CotistaMetadata.ColumnNames.InvestidorProfissional]);
            cotista.InvestidorQualificado = Convert.ToString(e.NewValues[CotistaMetadata.ColumnNames.InvestidorQualificado]);

            cotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Parametrização de Investidores - Cotista - Operacao: Update Cotista: " + idCotista + UtilitarioWeb.ToString(cotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}