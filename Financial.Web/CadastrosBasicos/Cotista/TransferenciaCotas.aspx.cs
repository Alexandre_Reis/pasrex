﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Web.Common;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TransferenciaCotas : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { TransferenciaCotaMetadata.ColumnNames.Tipo }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTransferenciaCotas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TransferenciaCotaQuery transferenciaCotaQuery = new TransferenciaCotaQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        CotistaQuery cotistaQueryOrigem = new CotistaQuery("C1");
        CotistaQuery cotistaQueryDestino = new CotistaQuery("C2");

        transferenciaCotaQuery.Select(transferenciaCotaQuery.IdTransferencia, transferenciaCotaQuery.DataTransferencia,
                                      transferenciaCotaQuery.IdCotistaOrigem.As("IdCotistaOrigem"),
                                      transferenciaCotaQuery.IdCotistaDestino.As("IdCotistaDestino"),
                                      transferenciaCotaQuery.Tipo, 
                                      transferenciaCotaQuery.Quantidade,
                                      transferenciaCotaQuery.Taxa,
                                      transferenciaCotaQuery.IdCarteira, carteiraQuery.Apelido,
                                      cotistaQueryOrigem.Apelido.As("ApelidoCotistaOrigem"),
                                      cotistaQueryDestino.Apelido.As("ApelidoCotistaDestino"));
        transferenciaCotaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == transferenciaCotaQuery.IdCarteira);
        transferenciaCotaQuery.InnerJoin(cotistaQueryOrigem).On(cotistaQueryOrigem.IdCotista == transferenciaCotaQuery.IdCotistaOrigem);
        transferenciaCotaQuery.InnerJoin(cotistaQueryDestino).On(cotistaQueryDestino.IdCotista == transferenciaCotaQuery.IdCotistaDestino);

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text)) {
            transferenciaCotaQuery.Where(transferenciaCotaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            transferenciaCotaQuery.Where(transferenciaCotaQuery.DataTransferencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            transferenciaCotaQuery.Where(transferenciaCotaQuery.DataTransferencia.LessThanOrEqual(textDataFim.Text));
        }

        transferenciaCotaQuery.OrderBy(transferenciaCotaQuery.DataTransferencia.Descending);

        TransferenciaCotaCollection transferenciaCotaCollection = new TransferenciaCotaCollection();
        transferenciaCotaCollection.Load(transferenciaCotaQuery);

        //// Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = transferenciaCotaCollection;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e) {
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCarteira = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCarteira"));
            DateTime dataTransferencia = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataTransferencia"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Carteira está fechada. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, dataTransferencia) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data da carteira. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteira");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("DataTransferencia");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Carteira " + clienteDescricao + " está fechada! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Transferência com data " + data.ToString().Substring(0, 10) + " anterior à data da carteira " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxSpinEdit btnEditCodigoCotistaOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaOrigem") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCotistaDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaDestino") as ASPxSpinEdit;
            ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCarteira);
            controles.Add(btnEditCodigoCotistaOrigem);
            controles.Add(btnEditCodigoCotistaDestino);
            controles.Add(dropTipo);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data da operação não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Carteira está fechada! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data da carteira! Operação não pode ser realizada.";
                    return;
                }
            }

            if (dropTipo.SelectedIndex == 1 && textQuantidade.Text == "") //Por posições
            {
                e.Result = "Para transferências de posições por quantidade o campo quantidade deve ser informado.";
                return;
            }

            if (dropTipo.SelectedIndex == 2 && textTaxa.Text == "") //Por posições
            {
                e.Result = "Para transferências de posições por taxa o campo taxa deve ser informado.";
                return;
            }
        }
    }


    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotistaOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaOrigem") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotistaDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaDestino") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        TransferenciaCota transferenciaCota = new TransferenciaCota();

        transferenciaCota.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        transferenciaCota.IdCotistaOrigem = Convert.ToInt32(btnEditCodigoCotistaOrigem.Text);
        transferenciaCota.IdCotistaDestino = Convert.ToInt32(btnEditCodigoCotistaDestino.Text);
        transferenciaCota.DataTransferencia = Convert.ToDateTime(textData.Text);

        transferenciaCota.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);

        transferenciaCota.Quantidade = null;
        if (textQuantidade.Text != "")
        {
            transferenciaCota.Quantidade = Convert.ToDecimal(textQuantidade.Text);
        }

        transferenciaCota.Taxa = null;
        if (textTaxa.Text != "")
        {
            transferenciaCota.Taxa = Convert.ToDecimal(textTaxa.Text);
        }

        transferenciaCota.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TransferenciaCota - Operacao: Insert TransferenciaCota: " + transferenciaCota.IdTransferencia + UtilitarioWeb.ToString(transferenciaCota),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;

                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCarteira);

                        dataDia = cliente.DataDia.Value;

                        if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                        {
                            nome = nome + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else
                        {
                            nome = nome + "|status_closed";
                        }
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTransferencia = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotistaOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaOrigem") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotistaDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotistaDestino") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        TransferenciaCota transferenciaCota = new TransferenciaCota();

        if (transferenciaCota.LoadByPrimaryKey(idTransferencia)) 
        {
            transferenciaCota.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            transferenciaCota.IdCotistaOrigem = Convert.ToInt32(btnEditCodigoCotistaOrigem.Text);
            transferenciaCota.IdCotistaDestino = Convert.ToInt32(btnEditCodigoCotistaDestino.Text);

            transferenciaCota.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);

            transferenciaCota.Quantidade = null;
            if (textQuantidade.Text != "") {
                transferenciaCota.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            }
            transferenciaCota.Taxa = null;
            if (textTaxa.Text != "")
            {
                transferenciaCota.Taxa = Convert.ToDecimal(textTaxa.Text);
            }

            transferenciaCota.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TransferenciaCota - Operacao: Update TransferenciaCota: " + idTransferencia + UtilitarioWeb.ToString(transferenciaCota),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTransferencia");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idTransferencia = Convert.ToInt32(keyValuesId[i]);

                TransferenciaCota transferenciaCota = new TransferenciaCota();
                if (transferenciaCota.LoadByPrimaryKey(idTransferencia)) {

                    TransferenciaCota transferenciaCotaClone = (TransferenciaCota)Utilitario.Clone(transferenciaCota);
                    //
                    
                    transferenciaCota.MarkAsDeleted();
                    transferenciaCota.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TransferenciaCota - Operacao: Delete TransferenciaCota: " + idTransferencia + UtilitarioWeb.ToString(transferenciaCotaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "") {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoCarteiraFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}