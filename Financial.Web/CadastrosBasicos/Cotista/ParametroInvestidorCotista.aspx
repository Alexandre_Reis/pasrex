﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ParametroInvestidorCotista.aspx.cs" Inherits="CadastrosBasicos_ParametroInvestidorCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
		
        function InvestidorProfissionalChanged(s,e){
            var InvestidorQualificado_Attr =  document.getElementById("gridCadastro_DXEditor4_I");
            if(s.GetValue() == 'S')
            {
                InvestidorQualificado_Attr.disabled = true;
                InvestidorQualificado_Attr.value = "Sim";
            }
            else
            {
                InvestidorQualificado_Attr.disabled = false;
            }
        }
    </script>

</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Parametrização de Investidores - Cotista" />
    </div>
        
    <div id="mainContent">
            
        <div class="linkButton" >               
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal5" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true"
            KeyFieldName="IdCotista" DataSourceID="EsDSCotista"
            OnHtmlRowCreated="gridCadastro_HtmlRowCreated"  
            OnCustomCallback="gridCadastro_CustomCallback"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnCellEditorInitialize="gridCadastro_CellEditorInitialize">        
                    
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="10%" ShowClearFilterButton="True">
                    <HeaderTemplate>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdCotista" Caption="Id Cotista" VisibleIndex="1" Width="10%" ReadOnly="true" CellStyle-HorizontalAlign="Center">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome" VisibleIndex="2" Width="60%" ReadOnly="true">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="InvestidorProfissional" Caption="Investidor Profissional?" Width="10%" VisibleIndex="3">
                    <PropertiesComboBox EncodeHtml="false" ClientSideEvents-ValueChanged="InvestidorProfissionalChanged">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataComboBoxColumn FieldName="InvestidorQualificado" Caption="Investidor Qualificado?" Width="10%" VisibleIndex="3">
                    <PropertiesComboBox EncodeHtml="false" ClientInstanceName="InvestidorQualificado">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
            </Columns>            

            <ClientSideEvents

                BeginCallback="function(s, e) {
		            if (e.command == 'CUSTOMCALLBACK') {
                        isCustomCallback = true;
                    }						
                }"

                EndCallback="function(s, e) {
			        if (isCustomCallback) {
                        isCustomCallback = false;
                        s.Refresh();
                    }
                }"
            />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
                                    
            </dxwgv:ASPxGridView>            
        </div>
          
    </div>
    
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        
    </form>
</body>
</html>