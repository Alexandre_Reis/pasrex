﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Util;
using Financial.Common;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.Web.Common;

using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_RentabilidadeFundoOffShore : CadastroBasePage
{
    #region Filtros
    public const int filtroClasse = 1;
    public const int filtroSerie = 2;
    public const int filtroPortifolio = 3;
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSRentabilidadeSerie_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        RentabilidadeSerieCollection coll = new RentabilidadeSerieCollection();
        RentabilidadeFundoOffShoreCollection collFundo = new RentabilidadeFundoOffShoreCollection();
        RentabilidadeSerieQuery rentabilidadeQuery = new RentabilidadeSerieQuery("rentabilidade");
        RentabilidadeFundoOffShoreQuery rentabilidadeFundoQuery = new RentabilidadeFundoOffShoreQuery("rentabilidade");
        SeriesOffShoreQuery serieQuery = new SeriesOffShoreQuery("serie");

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text) &&
            dropFiltro != null && dropFiltro.SelectedIndex != -1 &&
            !String.IsNullOrEmpty(textDataInicio.Text) &&
            !String.IsNullOrEmpty(textDataFim.Text))
        {
            if (Convert.ToInt32(dropFiltro.SelectedItem.Value) != filtroPortifolio)
            {
                rentabilidadeQuery.Select(rentabilidadeQuery.IdFundo,
                                          serieQuery.Descricao.As("DescricaoSerie"),
                                          rentabilidadeQuery.QuantidadeSerie,
                                          rentabilidadeQuery.Data,
                                          rentabilidadeQuery.Rentabilidade,
                                          rentabilidadeQuery.RentabilidadeAcumuluda);
                rentabilidadeQuery.InnerJoin(serieQuery).On(serieQuery.IdSeriesOffShore.Equal(rentabilidadeQuery.IdSerie));
                rentabilidadeQuery.Where(rentabilidadeQuery.IdFundo.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text))
                                         & rentabilidadeQuery.Data.GreaterThanOrEqual(textDataInicio.Text)
                                         & rentabilidadeQuery.Data.LessThanOrEqual(textDataFim.Text));

                if (Convert.ToInt32(dropFiltro.SelectedItem.Value) == filtroClasse)
                    rentabilidadeQuery.Where(serieQuery.SerieBase.Coalesce("'N'").Equal("S"));
                else
                    rentabilidadeQuery.Where(serieQuery.SerieBase.Coalesce("'N'").NotEqual("S"));

                rentabilidadeQuery.OrderBy(serieQuery.DataInicioAplicacao.Ascending);

                coll.Load(rentabilidadeQuery);
                e.Collection = coll;
            }
            else
            {
                rentabilidadeFundoQuery.Select(rentabilidadeFundoQuery.IdFundo,
                                               rentabilidadeFundoQuery.Patrimonio,
                                               rentabilidadeFundoQuery.PatrimonioAnterior,
                                               rentabilidadeFundoQuery.Data,
                                               rentabilidadeFundoQuery.Rentabilidade,
                                               rentabilidadeFundoQuery.RentabilidadeAcumuluda,
                                               rentabilidadeFundoQuery.RentabilidadePercentual,
                                               rentabilidadeFundoQuery.ValorCotaAnterior,
                                               rentabilidadeFundoQuery.ValorCota);
                rentabilidadeFundoQuery.Where(rentabilidadeFundoQuery.IdFundo.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text))
                                         & rentabilidadeFundoQuery.Data.GreaterThanOrEqual(textDataInicio.Text)
                                         & rentabilidadeFundoQuery.Data.LessThanOrEqual(textDataFim.Text));

                collFundo.Load(rentabilidadeFundoQuery);
                e.Collection = collFundo;
            }
        }
        else
        {
            e.Collection = coll; // Collection vazio
        }
        
       
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        carteiraQuery.Select(carteiraQuery);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(carteiraQuery.IdCarteira));
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.OffShore_PJ, (int)TipoClienteFixo.OffShore_PF));

        coll.Load(carteiraQuery);
        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropFiltro_Load(object sender, EventArgs e)
    {
        if (dropFiltro != null)
        {
            dropFiltro.Items.Clear();
            dropFiltro.Items.Add("Classe", filtroClasse);
            dropFiltro.Items.Add("Série", filtroSerie);
            dropFiltro.Items.Add("Portifólio", filtroPortifolio);
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text) ||
            dropFiltro.SelectedIndex == -1 ||
            String.IsNullOrEmpty(textDataInicio.Text) ||
            String.IsNullOrEmpty(textDataFim.Text))
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        e.Result = "";
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (cliente.LoadByPrimaryKey(idCarteira) && (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF)))
            {
                carteira.LoadByPrimaryKey(idCarteira);
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                        {
                            nome = carteira.str.Apelido;
                            resultado = nome + "|" + cliente.DataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idCarteira;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

        /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if ((Convert.ToInt32(e.Parameters) != filtroPortifolio))
        {
            ((ASPxGridView)sender).Columns["DescricaoSerie"].Visible = true;
            ((ASPxGridView)sender).Columns["QuantidadeSerie"].Visible = true;
            ((ASPxGridView)sender).Columns["Patrimonio"].Visible = false;
            ((ASPxGridView)sender).Columns["ValorCota"].Visible = false;
            ((ASPxGridView)sender).Columns["PatrimonioAnterior"].Visible = false;
            ((ASPxGridView)sender).Columns["ValorCotaAnterior"].Visible = false;
            ((ASPxGridView)sender).Columns["RentabilidadePercentual"].Visible = false;
        }
        else
        {
            ((ASPxGridView)sender).Columns["DescricaoSerie"].Visible = false;
            ((ASPxGridView)sender).Columns["QuantidadeSerie"].Visible = false;
            ((ASPxGridView)sender).Columns["Patrimonio"].Visible = true;
            ((ASPxGridView)sender).Columns["ValorCota"].Visible = true;
            ((ASPxGridView)sender).Columns["PatrimonioAnterior"].Visible = true;
            ((ASPxGridView)sender).Columns["ValorCotaAnterior"].Visible = true;
            ((ASPxGridView)sender).Columns["RentabilidadePercentual"].Visible = true;
        }    
    }
}