﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.CRM;

public partial class CadastrosBasicos_ContaCorrenteCotista : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCotista = true;
        this.AllowUpdate = false;
        base.Page_Load(sender, e);
    }

    protected void callbackCotista_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            string nome = "";
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = idCotista.ToString() + " - " + cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
            e.Result = nome;
        }
    }

    #region EsSelect


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContaCorrenteCollection coll = new ContaCorrenteCollection();
        e.Collection = coll;

        int idCotistaFiltro = 0;
        if (btnEditCodigoCotistaFiltro != null && !string.IsNullOrEmpty(btnEditCodigoCotistaFiltro.Text))
            idCotistaFiltro = Convert.ToInt32(btnEditCodigoCotistaFiltro.Text);

        if (idCotistaFiltro != 0)
        {
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("ccQuery");
            BancoQuery bancoQuery = new BancoQuery("bancoQuery");
            AgenciaQuery agenciaQuery = new AgenciaQuery("agenciaQuery");
            MoedaQuery moedaQuery = new MoedaQuery("moedaQuery");
            CotistaQuery cotistaQuery = new CotistaQuery("CotistaQuery");

            StringBuilder contaDefaultField = new StringBuilder();
            contaDefaultField.Append("< CASE WHEN ccQuery.ContaDefaultCotista = 'S' AND ccQuery.[idCotista] <> ").Append(idCotistaFiltro);
            contaDefaultField.Append(" THEN 'N'");
            contaDefaultField.Append(" ELSE ccQuery.ContaDefaultCotista");
            contaDefaultField.Append(" END as ContaDefaultCotista>");

            contaCorrenteQuery.Select(contaCorrenteQuery.IdConta,
                                      bancoQuery.Nome.As("Banco"),
                                      agenciaQuery.Nome.As("Agencia"),
                                      contaCorrenteQuery.Numero,
                                      contaCorrenteQuery.DigitoConta,
                                      moedaQuery.Nome.As("Moeda"),
                                      contaCorrenteQuery.IdCotista,
                                      contaDefaultField.ToString());
            contaCorrenteQuery.LeftJoin(bancoQuery).On(bancoQuery.IdBanco.Equal(contaCorrenteQuery.IdBanco));
            contaCorrenteQuery.LeftJoin(agenciaQuery).On(agenciaQuery.IdAgencia.Equal(contaCorrenteQuery.IdAgencia));
            contaCorrenteQuery.LeftJoin(moedaQuery).On(moedaQuery.IdMoeda.Equal(contaCorrenteQuery.IdMoeda));
            contaCorrenteQuery.LeftJoin(cotistaQuery).On(cotistaQuery.IdPessoa.Equal(contaCorrenteQuery.IdPessoa));

            contaCorrenteQuery.Where(cotistaQuery.IdCotista.Equal(idCotistaFiltro));

            if (coll.Load(contaCorrenteQuery))
                e.Collection = coll;
        }
    }
    #endregion

    #region GridCadastro

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnVincular" || e.Parameters == "btnDesvincular")
        {
            int? idCotista = null;

            List<ContaCorrente> lstContasDefault = new List<ContaCorrente>();
            if (e.Parameters == "btnVincular")
                idCotista = Convert.ToInt32(btnEditCodigoCotistaFiltro.Text);

            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdConta");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                #region Busca CheckBox atualizado
                int rowIndex = (sender as ASPxGridView).FindVisibleIndexByKeyValue(keyValuesId[i]);
                ASPxCheckBox checkContaDefault = gridCadastro.FindRowCellTemplateControl(rowIndex, null, "cbContaDefault") as ASPxCheckBox;
                ((IPostBackDataHandler)checkContaDefault).LoadPostData("", Request.Params);
                #endregion

                ContaCorrente contaCorrente = new ContaCorrente();

                int idConta = Convert.ToInt32(keyValuesId[i]);

                if (contaCorrente.LoadByPrimaryKey(idConta))
                {
                    ContaCorrente contaCorrenteClone = (ContaCorrente)Utilitario.Clone(contaCorrente);
                    //

                    if (idCotista.HasValue)
                    {
                        if (contaCorrente.IdCotista.HasValue && contaCorrente.IdCotista.Value != idCotista.Value)
                            throw new Exception("Conta Corrente Id - " + idConta + " já foi está vinculada a outro Cotista!");

                        #region Valida Conta Default
                        ContaCorrenteCollection collContasDefault = new ContaCorrenteCollection();
                        collContasDefault.Query.Where(collContasDefault.Query.IdCotista.Equal(idCotista)
                                                     & collContasDefault.Query.ContaDefaultCotista.Equal("S"));

                        if (collContasDefault.Query.Load())
                            lstContasDefault = (List<ContaCorrente>)collContasDefault;

                        if (lstContasDefault.Count > 0 && checkContaDefault.Checked)
                        {
                            ContaCorrente contaDefault = lstContasDefault.Find(delegate(ContaCorrente x) { return x.IdMoeda == contaCorrente.IdMoeda && x.IdCotista == idCotista && x.IdConta != contaCorrente.IdConta; });
                            if (contaDefault != null && contaDefault.IdConta.GetValueOrDefault(0) != 0)
                            {
                                Moeda moeda = new Moeda();
                                moeda.LoadByPrimaryKey((short)contaDefault.IdMoeda.Value);
                                throw new Exception("Já existe conta default cadastrada para o Cotista e Moeda selecionados! \n" + " Id.Conta: " + contaCorrente.IdConta + " Moeda: " + moeda.Nome);
                            }
                        }
                        #endregion

                        contaCorrente.ContaDefaultCotista = checkContaDefault.Checked ? "S" : "N";
                    }
                    else
                    {
                        if (contaCorrente.IdCotista.HasValue && contaCorrente.IdCotista.Value != Convert.ToInt32(btnEditCodigoCotistaFiltro.Text))
                            throw new Exception("A conta já foi vinculada anteriormente e não pode ser desvinculada por outro cotista!!");

                        contaCorrente.ContaDefaultCotista = "N";
                    }

                    contaCorrente.IdCotista = idCotista;
                    

                    contaCorrente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Associação CC X Cotista - Operacao: Update ContaCorrente: " + idConta + UtilitarioWeb.ToString(contaCorrenteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion
}